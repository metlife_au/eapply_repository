package au.com.metlife.eapply.quote.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.quote.model.ConvertModel;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.ProductConfigBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.model.SelectItem;
import au.com.metlife.eapply.quote.service.QuoteService;
import au.com.metlife.eapply.quote.utility.GeneralHelper;
import au.com.metlife.eapply.quote.utility.QuoteConstants;

@RestController
@Component
public class QuoteController {

	private final QuoteService quoteService;

	@Autowired
    public QuoteController(final QuoteService quoteService) {
        this.quoteService = quoteService;
    }
	
	
	
	 @RequestMapping("/getIndustryList")
	  public ResponseEntity<List<SelectItem>> getIndustryList(@RequestParam(value="fundCode") String fundCode) {		
		 List<SelectItem> industryList=null;
		 List<SelectItem> indList=new ArrayList<SelectItem>();
		 try{
			 if(fundCode!=null){
				 industryList=quoteService.getIndustryList(fundCode);
					if(industryList!=null){
						for(int iterator=0; iterator<industryList.size();iterator++){
							SelectItem selectItem = (SelectItem)industryList.get(iterator);
							if(selectItem!=null){
								if(selectItem.getValue()!=null && !"".equalsIgnoreCase(selectItem.getValue())){
									if(selectItem.getValue().contains("_")){
										String label_ind = selectItem.getValue().replace("_", ",");
										selectItem.setValue(label_ind);
									}
								}
								indList.add(new SelectItem(selectItem.getKey(),selectItem.getValue()));
							}
						}
					}
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    if(industryList==null){
	        return new ResponseEntity<List<SelectItem>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<SelectItem>>(indList, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/getProductConfig")
	  public ResponseEntity<List<ProductConfigBO>> getProductConfig(@RequestParam(value="fundCode") String fundCode,
			  @RequestParam(value="memberType") String memberType,
			  @RequestParam(value="manageType") String manageType) {		
		InstProductRuleBO instProductRuleBO=null;
		List<ProductConfigBO> productList=null;
		ProductConfigBO productConfigBO=null;
		 try{
			 if(fundCode!=null){
				 instProductRuleBO=quoteService.getProductConfig(fundCode, memberType, manageType);
				 productConfigBO=new ProductConfigBO();
				 productConfigBO.setProductCode(instProductRuleBO.getProductCode());
				 productConfigBO.setMemberType(instProductRuleBO.getMemberType());
				 productConfigBO.setManageType(instProductRuleBO.getManageType());
				 productConfigBO.setDeathMaxAmount(instProductRuleBO.getDeathMaxAmt());
				 productConfigBO.setTpdMaxAmount(instProductRuleBO.getTpdMaxAmt());
				 productConfigBO.setIpMaxAmount(instProductRuleBO.getIpMaxAmt());
				 productConfigBO.setDeathTpdTransferMaxAmt(instProductRuleBO.getDeathTpdTransferMaxAmt());
				 productConfigBO.setDeathTpdTransferMaxUnits(instProductRuleBO.getDeathTpdTransferMaxUnits());
				 productConfigBO.setIpTransferMaxAmt(instProductRuleBO.getIpTransferMaxAmt());
				 productList=new ArrayList<ProductConfigBO>();
				 productList.add(productConfigBO);
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    if(productList==null){
	        return new ResponseEntity<List<ProductConfigBO>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<ProductConfigBO>>(productList, HttpStatus.OK);
	 }

//	 @RequestMapping(value = "/getIndustryList",method = RequestMethod.GET)
//	  public ResponseEntity<List<SelectItem>> getIndustryList(@RequestParam(value="fundCode") InputStream inputStream) {
//		 BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
//		 String line = null;	
//		 String string = "";		
//		 List<SelectItem> industryList=null;
//			try {
//				while ((line = in.readLine()) != null) {
//					string += line + "\n";
//					System.out.println("line>>"+line);	
//					if(line!=null && line.trim().length()>0){
//						JSONObject jsonObject = new JSONObject(string);
//		                System.out.println("fundCode--->>"+jsonObject.get("fundCode"));
//		                if(jsonObject.get("fundCode")!=null){
//		                	industryList=quoteService.getIndustryList(jsonObject.get("fundCode").toString());
//		                }
//					}
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			} catch (JSONException e) {
//				e.printStackTrace();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}	
//
//	    if(industryList==null){
//	        return new ResponseEntity<List<SelectItem>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//	    }
//	    return new ResponseEntity<List<SelectItem>>(industryList, HttpStatus.OK);
//		 
//	 }
	
	 @RequestMapping(value = "/calculateDeath", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateDeath(@RequestBody RuleModel ruleModel) {
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<QuoteResponse>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateDeath(ruleModel));
                 }
           } catch (Exception e) {
                 e.printStackTrace();
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<List<QuoteResponse>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
         }
         return new ResponseEntity<List<QuoteResponse>>(quoteResponseList, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateTPD", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateTPD(@RequestBody RuleModel ruleModel) {
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<QuoteResponse>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateTpd(ruleModel));
                 }
           } catch (Exception e) {
                 e.printStackTrace();
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<List<QuoteResponse>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
         }
         return new ResponseEntity<List<QuoteResponse>>(quoteResponseList, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateIP", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateIP(@RequestBody RuleModel ruleModel) {
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<QuoteResponse>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateIp(ruleModel));
                 }
           } catch (Exception e) {
                 e.printStackTrace();
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<List<QuoteResponse>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
         }
         return new ResponseEntity<List<QuoteResponse>>(quoteResponseList, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateAllOld", method = RequestMethod.POST)
	  public ResponseEntity<List<QuoteResponse>> calculateAllOld(InputStream inputStream) {	
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;	
			String string = "";			
			RuleModel ruleModel = null;
			List<QuoteResponse> quoteResponseList=null;
			try {
				quoteResponseList=new ArrayList<QuoteResponse>();
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					System.out.println("line>>"+line);	
					if(line!=null && line.trim().length()>0){
						JSONObject jsonObject = new JSONObject(string);
		                System.out.println("age--->>"+jsonObject.get("age"));
						ruleModel = GeneralHelper.convertToRuleModel(jsonObject);
					}
				}
				if(ruleModel!=null){
					quoteResponseList.add(quoteService.calculateDeath(ruleModel));
					quoteResponseList.add(quoteService.calculateTpd(ruleModel));
					quoteResponseList.add(quoteService.calculateIp(ruleModel));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}	
		    if(quoteResponseList==null){
		        return new ResponseEntity<List<QuoteResponse>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		    }
		    System.out.println("END--"+new  java.util.Date());
		    return new ResponseEntity<List<QuoteResponse>>(quoteResponseList, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/calculateAll", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateAll(@RequestBody RuleModel ruleModel) {
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<QuoteResponse>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateDeath(ruleModel));
                       quoteResponseList.add(quoteService.calculateTpd(ruleModel));
                       quoteResponseList.add(quoteService.calculateIp(ruleModel));
                 }
           } catch (Exception e) {
                 e.printStackTrace();
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<List<QuoteResponse>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
         }
         return new ResponseEntity<List<QuoteResponse>>(quoteResponseList, HttpStatus.OK);
   }

	 @RequestMapping(value = "/calculateTransfer", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateTransfer(@RequestBody RuleModel ruleModel) {
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<QuoteResponse>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateDeath(ruleModel));
                       quoteResponseList.add(quoteService.calculateTpd(ruleModel));
                       quoteResponseList.add(quoteService.calculateIp(ruleModel));
                 }
           } catch (Exception e) {
                 e.printStackTrace();
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<List<QuoteResponse>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
         }
         return new ResponseEntity<List<QuoteResponse>>(quoteResponseList, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/convertCover", method = RequestMethod.POST)
     public ResponseEntity<List<ConvertModel>> convertCover(@RequestBody RuleModel ruleModel) {
           List<ConvertModel> convertModelList=null;
           try {
        	   convertModelList=new ArrayList<ConvertModel>();
        	   ConvertModel convertModel=new ConvertModel();
                 if(ruleModel!=null){
                	 if(ruleModel.getExDeathCoverType()!=null){
                		 if(ruleModel.getExDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_UNITISED)){
                    		 convertModel.setExCoverType(ruleModel.getExDeathCoverType());
                    		 convertModel.setExCoverAmount(ruleModel.getDeathExistingAmount());
                    		 convertModel.setConvertedAmount(ruleModel.getDeathExistingAmount());
                		 }else if(ruleModel.getExDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_FIXED)){
                    		 convertModel=quoteService.convertDeathFixedToUnits(ruleModel);
                		 }
                	 }
                	 if(ruleModel.getExTpdCoverType()!=null){
                		 if(ruleModel.getExTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_UNITISED)){
                    		 convertModel.setExCoverType(ruleModel.getExTpdCoverType());
                    		 convertModel.setExCoverAmount(ruleModel.getTpdExistingAmount());
                    		 convertModel.setConvertedAmount(ruleModel.getTpdExistingAmount());
                		 }else if(ruleModel.getExTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_FIXED)){
                    		 convertModel=quoteService.convertTpdFixedToUnits(ruleModel);
                		 }
                	 }
                	 convertModelList.add(convertModel);
                 }
           } catch (Exception e) {
                 e.printStackTrace();
           }     
         if(convertModelList==null){
             return new ResponseEntity<List<ConvertModel>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
         }
         return new ResponseEntity<List<ConvertModel>>(convertModelList, HttpStatus.OK);
	 }

}
