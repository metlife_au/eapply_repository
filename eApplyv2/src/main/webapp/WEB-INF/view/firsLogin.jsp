<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="../../eapply/static/firstsuper/images/favicon.ico"/>
<title>First Super</title>

<!-- Bootstrap core CSS -->
<link href="../../eapply/static/firstsuper/css/bootstrap.css" rel="stylesheet">
<link href="../../eapply/static/firstsuper/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="../../eapply/static/firstsuper/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="../../eapply/static/firstsuper/css/bootstrap-select.css">
<script src="../../eapply/static/firstsuper/js/jquery.min.js"></script> 

<link href="../../eapply/static/firstsuper/css/custom.css" rel="stylesheet">

    <script type="text/javascript">
                 $(window).on('load', function () {
   						$('#memberId').attr('type', 'tel');
  					});					
    </script>
				<script type="text/javascript">
					var memberNumberValErr=true,firstNameValErr=true,surNameValErr=true,dobValErr=true;
					var errorStatus = false;	
				
				 		function validateFormFields(inputObj){
				 		if(inputObj!=null){
					 		if(inputObj.id=='memberId'){
					 			validateMemberNumber();
					 		}
					 		if(inputObj.id=='firstnameId'){
					 			validateMemberNumber();
					 			validateFirstName();
					 		}
					 		if(inputObj.id=='surnameId'){
					 			validateMemberNumber();
					 			validateFirstName();
					 			validateSurName();
					 		}
					 		if(inputObj.id=='dobId'){
					 			validateMemberNumber();
					 			validateFirstName();
					 			validateSurName();
					 			validateDobFormat();
					 		}
					 	 }
				 		}
				 		function validateLogin(savedApp){
				 		        validateMemberNumber();
					 			validateFirstName();
					 			validateSurName();
					 			validateDobFormat();
					 			/* alert(savedApp); */
					 			if(savedApp)
					 				{
					 				document.getElementById("retrieve").value = "Yes";
					 				}
					 			if(memberNumberValErr == true || firstNameValErr == true  || surNameValErr== true || dobValErr== true){
					 				errorStatus = true;
					 			}else{
					 				errorStatus =  false;
					 			}
					 			if(!errorStatus){
					 				document.instform.submit(); 
					 			}
					 			
				 		}
				 
				 function validateMemberNumber(){
				 	if((document.getElementById("memberId").value=='') || (document.getElementById("memberId").value== null)){
				 		document.getElementById('memberNoError').innerHTML = "Please enter your First Super member number";
				 		memberNumberValErr=true;
				 		showErrorMessage("member_div_id");
						}else{
							document.getElementById('memberNoError').innerHTML = "";
							memberNumberValErr=false;
							showRow("member_div_id");
						}
				 }
				 function validateFirstName(){
					 if((document.getElementById("firstnameId").value=='') || (document.getElementById("firstnameId").value== null)){
						 document.getElementById('firstNameError').innerHTML = "Please enter your first name";
					 		firstNameValErr=true;
							showErrorMessage("firstname_div_id");
						}else{
							document.getElementById('firstNameError').innerHTML = "";
							firstNameValErr=false;
							showRow("firstname_div_id");
						}
				 }
				 function validateSurName(){
				 if((document.getElementById("surnameId").value=='') || (document.getElementById("surnameId").value== null)){
					 document.getElementById('surNameError').innerHTML = "Please enter your surname";
				 		surNameValErr=true;
				 		showErrorMessage("surname_div_id");
						}else{
						document.getElementById('surNameError').innerHTML = "";
						surNameValErr=false;
						showRow("surname_div_id");
						}
				 }
				 
				 function validateDobFormat(){
				    var dobid=document.getElementById("dobId");
				  	if(checkdate(dobid)){
				  	 	    changemmddyyTommddyyyy(dobid);
							var input=dobid;
							if(input.value!=null && input.value.length > 0){
								var dayfield=input.value.split("/")[0]
								var monthfield=input.value.split("/")[1]
								var yearfield=input.value.split("/")[2]
								if(!isDateddmmyyyy(yearfield,monthfield,dayfield)){
									input.value="";
									/*dobValErr=true;*/
								}
								else{
									if((monthfield >= 1) && (monthfield <= 9) && (monthfield.length < 2)){
										monthfield="0"+monthfield;
									}
									if((dayfield >= 1) && (dayfield <= 9) &&  (dayfield.length < 2)){
										dayfield="0"+dayfield;
									}
									input.value=dayfield+"/"+monthfield+"/"+yearfield;
								}
							}
						}
						if(dobid){
							if(dobid.value){
								if(isFutureDate(dobid.value)){
									dobid.value="";
								}
							}
						}
						
						if((dobid.value=='') || (dobid.value== null)){
					 		 dobValErr=true;
					 		document.getElementById('dobError').innerHTML = "Please enter your date of birth";
					 		 showErrorMessage("dob_div_id");
					 		 if(navigator.appVersion.match(/MSIE [\d.]+/)){
								var placeholderText = 'dd/mm/yyyy';
								$('#dobId').val(placeholderText);
							}
						 }else{
						     dobValErr=false;
					 		document.getElementById('dobError').innerHTML = "";
					 		 showRow("dob_div_id");
						 }
				 }
				 function changemmddyyTommddyyyy(objtxtShipDate){
						var objtxtDate=objtxtShipDate;
						var regex = /^\d{1,2}\/\d{1,2}\/\d{1,2}$/;
						var regex2 = /^(\d\d)$/; // for yy
						if (objtxtShipDate.value.match(regex)){
							var dateArr = objtxtShipDate.value.split("/");
							var currentYear=null;
							var intYear=parseInt(dateArr[2]);
							if(isFuturDate("20"+dateArr[2])){
								currentYear="19";
							}
							else{
								currentYear="20";
							}
							if(currentYear!=null){
								dateArr[2] = dateArr[2].replace(regex2, currentYear + "$1");
								objtxtDate.value = dateArr.join("/");
							}
							else{
							objtxtDate.value = "";
							}
						}
					}
					function isFuturDate(year) {
						var c_mon = parseInt('')
						var c_dt = parseInt('')
						var c_yr = parseInt('')
						var Currentdate = new Date();
				        if(Currentdate.getFullYear() < parseInt(year)){
						 	return true;
						}else 
						 return false;
					}
					function checkdate(input){
						var validformat=/^\d{1,2}\/\d{1,2}\/\d{2}$/ //Basic check for format validity
						var validformat2=/^\d{1,2}\/\d{1,2}\/\d{4}$/
						var returnval=false
						if(validformat2.test(input.value)){
							var dayfield=input.value.split("/")[0]
							var monthfield=input.value.split("/")[1]
							var yearfield=input.value.split("/")[2]
							if(!isDateddmmyyyy(yearfield,monthfield,dayfield)){
							   input.value="";
							}
							else{
							if((monthfield >= 1) && (monthfield <= 9) && (monthfield.length < 2)){
								monthfield="0"+monthfield;
							}
							if((dayfield >= 1) && (dayfield <= 9) && (dayfield.length < 2)){
								dayfield="0"+dayfield;
							}
							input.value=dayfield+"/"+monthfield+"/"+yearfield;
							}
							return false;
						}
						if(!validformat.test(input.value))
							input.value="";
						else{ 
							returnval=true
						}
						return returnval
					}
					function isFutureDate(futuredate){
					 	var dteDate;
						var now = new Date();
						obj1 = futuredate.split("/");
						obj1[0] = parseInt(obj1[0], 10);
						obj1[1] = parseInt(obj1[1], 10)-1;
						obj1[2] = parseInt(obj1[2], 10);
						dteDate=new Date(obj1[2], obj1[1], obj1[0]);
						futuredate.value="";
						return now < dteDate;
					 }	
					
					function isDateddmmyyyy(y,m,d){
						var date = new Date(y,m-1,d);
						if(parseInt(date.getFullYear())==parseInt(y) && parseInt((date.getMonth()+1))==parseInt(m,10) && parseInt( date.getDate())==parseInt(d,10)){
							return true;
						}else
						 	return false;
					}
					 function showErrorMessage(divid){
						 document.getElementById(divid).className = "row rowcustom has-error";
						 }
						 function showRow(divid){
						 document.getElementById(divid).className = "row rowcustom";
						 }
				 
    
					</script>
</head>

<body class="tooltip-demo">
<div class="menut">
  <div class="container relative">
    <div class=" row bgsec">
      <div class="  c-mt10px clearfix">
        <div class="col-sm-3 hidden-xs ">
          <!-- <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wwrap fleft  hidden-xs ">
            <tbody>
              <tr>
                <td class="fleft"  ><span class="      ico icoSave colorsec     mr5px"></span></td>
                <td class="fleft note"  ><a href="#">Save &amp; Email</a></td>
                <td class="fleft  " >&nbsp;&nbsp; </td>
                <td class="fleft  "><span class="ico  ico icoPrint colorsec  mr5px"></span></td>
                <td class="fleft   note"  ><a href="#">Print</a></td>
              </tr>
            </tbody>
          </table> -->
        </div>
        <div class="col-sm-3 col-xs-5">
          <div class=" visible-xs clearfix  fleft  mb5px">
            <p class="telno  "> <span class="ico icoCall"></span> 1300 265 374 </p>
          </div>
          <p class="hidden-xs"> <strong>Level <span class="red">$20</span></strong> per month </p>
        </div>
        <div class="col-sm-3 col-xs-7">
          <p class="   note fright"> <strong>Quote ID: COLI-999-9999</strong></p>
        </div>
        <div class="col-sm-3 col-xs-12">
          <p class="visible-xs"> <strong>Level <span class="red">$20</span></strong> per month </p>
          <div class="fright note mr10px hidden-xs"> Call 1300 265 374 for help </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="c-header">
  <div class="container">
    <div class="col-sm-3 col-xs-6">
     <img src="static/firstsuper/images/logo.png" class="img-responsive  c-logo" alt="MetLife Australia" /> 
    </div>
    <div class="  col-xs-9 hidden-xs  ">
      <div class="logout noborderleft"> <a target="_blank" title="Contact us" href="http://www.firstsuper.com.au/Contact">Contact us</a></div>
      <div class="logout"><a target="_blank" title="About us" href="http://www.firstsuper.com.au/Insurance">About us</a></div>
    </div>
    <div class="col-xs-6 visible-xs text-right">
      <div class="mt25px"><a class="col-xs-12 form-group" target="_blank" title="About us" href="http://www.firstsuper.com.au/Insurance">About us</a>
        <a class="col-xs-12 form-group" target="_blank" title="Contact us" href="http://www.firstsuper.com.au/Contact">Contact us</a>
       </div>
    </div>
  </div>
</div>
<div class="container ">
  <p class="spacer">&nbsp;</p>
</div>
<div class="container ">
  <div class=" col-sm-3 fright hidden-xs">
    <div class="help-block">
      <h2 class="helpHead">need help?</h2>
      <p class="helpTxt"> We are happy to chat over the phone</p>
      <h3 class="helpNo"> 1300 360 988</h3>
    </div>
    
  </div>
  <div class=" col-sm-9">
   
    <div class="panel-group" id=" ">
      <div class="panel panel-default mt5px">
        <!--div class="panel-heading"> <a data-toggle="collapse" data-parent="#accordion" href=" " class=" ">
          <h4 class="panel-title"> Personal details </h4>
          </a> </div-->
        <div id="collapseOne" class="panel-collapse collapse in   ">
        <form name="instform" id="instform" autoComplete="off"
	          action="/eapply/commonLanding" method="post">
	          <%
	          
	      		String nonmv = request.getParameter("nonmv");
	          
	          
	          			String formStatus = "";
	          			String failedLoginText ="";
	          			String genericAppText ="";
	          			String fundId = null;
	          			String emailId= null;
	          			String inputIdentifier = null;
	          			
						if(null!=session.getAttribute("lodge_fund_id")){
							fundId = session.getAttribute("lodge_fund_id").toString();
	          			}
						if(null!=session.getAttribute("lodge_fund_email")){
							emailId = session.getAttribute("lodge_fund_email").toString();
						}
						if(null!=session.getAttribute("input_identifier")){
	          				inputIdentifier = session.getAttribute("input_identifier").toString();
	          			}
			          	if(null!=response.getHeader("Status")){
			          		formStatus = response.getHeader("Status");
			          	}
				          if("loginFailed".equalsIgnoreCase(formStatus)){
				        	  failedLoginText = "We were unable to validate your user credentials against our database. If you continue to experience issues, please access our generic application form by clicking on the following ";
					     }
				         if("yes".equalsIgnoreCase(nonmv)){
					    	   genericAppText = "Welcome to our generic online application. Please complete the information as requested on the following pages.";
					     }
			          String respUrl=null;
			      	respUrl= "/eapply/genLand"+"?lodge_fund_id="+fundId+"&lodge_fund_email="+emailId+"&input_identifier="+inputIdentifier+"&nonmv=yes";
			      	System.out.println(respUrl);
	        	  %>
	          
	          <input type="hidden" name=InputIdentifier id="InputIdentifier" value="<%=inputIdentifier%>" />
	           <input	type="hidden" name="fund_id" id="fund_id" value="<%=fundId%>" /> 
	           <input	type="hidden" name="emailID" id="emailID" value="<%=emailId%>" /> 
	           <input	type="hidden" name="nonmv" id="nonmv" value="<%=nonmv%>" />
	           <input	type="hidden" name="retrieve" id="retrieve" />
          <div class="panel-body"> 
            
              <div class="row rowcustom">
    	<div class="col-sm-12">
        <h2 class="title mt0">Login</h2>
        </div>
        </div>
             <hr class="primeline">
    <div class="row rowcustom">
    	<div class="col-sm-12">
    		<p>To access and manage your insurance cover, simply login below.</p>
        </div>
    </div>
    <hr class="primeline">
    
    <%if ("yes".equalsIgnoreCase(nonmv)) {%>
    
    
    <div class="row rowcustom">
    	<div class="col-sm-12">
			 <label class="control-label"><span id="newApp" style="font-size: 16px;">
			 <%=genericAppText%> 
			 </span>
        	 </label>
		 </div>
	</div>
	  <hr class="primeline">
	<%} %>
    
    <%if ("loginFailed".equalsIgnoreCase(formStatus)) {%>
    
    
    <div class="row rowcustom">
    	<div class="col-sm-12">
			 <label class="control-label"><span id="memberFailed" style="color:#FF2626;font-size: 13px;">
			 <%=failedLoginText%> 
			 <a onclick="window.open('<%=respUrl%>', '_self');" href="#">link.</a>
			 </span>

        	 </label>
		 </div>
	</div>
	  <hr class="primeline">
	<%} %>
			   			 
            <!-- Row starts -->
            <div id = "member_div_id" class="row rowcustom  ">
              <div class="col-sm-5">
                <label class="control-label"> Member number </label>
                <span id="tick 1" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
              <div class="col-sm-6 col-xs-12">
                <input id="memberId" name="memberId" type="text" class="form-control"   onkeyup="return isAlphaNumeric('memberId');"
                onkeypress="return isAlphaNumeric('memberId');" onblur="validateFormFields(this);" value=""/>
                <p id = "memberNoError" class="error" style="padding-top:5px;"></p>
              </div>
              
            </div>
            <!-- Row ends --> 
            <hr class="primeline">
            <!-- Row starts -->
            <div  id = "firstname_div_id" class="row rowcustom  ">
              <div class="col-sm-5">
                <label class="control-label"> First name </label>
                <span id="tick 1" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
              <div class="col-sm-6 col-xs-12">
                <input id="firstnameId" name="firstnameId" type="text" maxlength="50" class="form-control"  onkeyup="return isCharacterAlpha('firstnameId');"
                onkeypress="return isCharacterAlpha('firstnameId');" onblur="validateFormFields(this);" value=""  />
               <p id = "firstNameError" class="error" style="padding-top:5px;"></p>
              </div>
              
            </div>
            <!-- Row ends --> 
            <hr class="primeline">
            <!-- Row starts -->
            <div id = "surname_div_id"  class="row rowcustom  ">
              <div class="col-sm-5">
                <label class="control-label"> Surname </label>
                <span id="tick 1" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
              <div class="col-sm-6 col-xs-12">
                <input id="surnameId" name="surnameId"  type="text" maxlength="50" onkeyup="return isCharacterAlpha('surnameId');" onkeypress="return isCharacterAlpha('surnameId');"
                onblur="validateFormFields(this);"  class="form-control"  value="" />
             <p id = "surNameError" class="error" style="padding-top:5px;"></p>
              </div>
              
            </div>
            <!-- Row ends --> 
            <hr class="primeline">
            <!-- Row starts -->
            <div id="dob_div_id" class="row rowcustom  ">
              <div class="col-sm-5">
                <label class="control-label"> Date of birth </label>
                <span id="tick 1" style=" visibility:hidden"> <span class="tickforms visible-xs"></span><span class="tickforms mrm25px hidden-xs"></span></span> </div>
              <div class="col-sm-6 col-xs-12">
                <input id="dobId" name="dobId" type="text"  maxlength="10" class="form-control"  onkeyup="return isDateFormat('dobId');" 
                    onkeypress="return isDateFormat('dobId');" onblur="validateFormFields(this);" value="" />
                    <p id = "dobError" class="error" style="padding-top:5px;"></p>
              </div>
              
            </div>
            <!-- Row ends --> 
            <hr class="primeline">
       <!-- Row starts -->
    <div class="row ">
      <p class="spacer">&nbsp;</p>
      
      <div class="col-sm-5"><label class="control-label">  </label></div>
      <div class="col-sm-3" >
        <button onclick="validateLogin(false)" class="btn btn-primary w100p" id="loginID" type="button">Login </button>
      </div>
        <p class="spacer visible-xs"></p>
         <div class="col-sm-3" >
        <button onclick="validateLogin(true)" class="btn btn-primary w100p" id="retrieveID" type="button">Saved Applications </button>
      </div>
    </div>
    <!-- Row ends -->
      
      
            
          </div>
          </form>
        </div>
      </div>
      
    </div>
    
   
   
    
 
    
    <div class="row mt20px">
      <div class=" col-xs-12 visible-xs">
        <div class="help-block">
          <h2 class="helpHead aligncenter">need help?</h2>
          <p class="helpTxt aligncenter"> We are happy to chat over the phone</p>
          <h3 class="helpNo aligncenter"> 1300 360 988</h3>
          
        </div>
      </div>
    </div>
  </div>
  <!-- /container --> 
</div>
 
<div class="c-footer hidden-xs">
  <div class="container">
   <div class="col-sm-3   "> <p class="footertext">&nbsp;  </p> </div>
    <div class="col-sm-5   ">
      <p class="footertext"> &copy; Copyright First Super</p>
    </div>	
    <div class="col-sm-4   ">
      <div class="text-right  ">
        <p class="footerlinks"> <a href="http://www.firstsuper.com.au/privacy" target="_new" class=" ">Privacy statement</a> | <a href="http://www.firstsuper.com.au/disclaimer" target="_new" class=" ">Legal notice</a> </p>
      </div>
    </div>
  </div>
</div>

 
  <div class="container visible-xs">
 
      <div class=" col-xs-12 aligncenter ">
        <p class="footerlinks"> <a href="http://www.firstsuper.com.au/privacy" target="_new" class=" ">Privacy statement</a> | <a href="http://www.firstsuper.com.au/disclaimer" target="_new" class=" ">Legal notice</a> </p>
        <p class="footertext"> &copy; Copyright First Super</p>
      </div>
   <p class="spacer">&nbsp;</p>
  </div>
 

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../../eapply/static/firstsuper/js/jquery-ui.js"></script> 
<script src="../../eapply/static/firstsuper/js/bootstrap.min.js"></script> 
<script src="../../eapply/static/firstsuper/js/docs.min.js"></script> 
<script type="text/javascript" src="../../eapply/static/firstsuper/js/moment.js"></script> 
<script type="text/javascript" src="../../eapply/static/firstsuper/js/bootstrap-select.js"></script> 
<script src="../../eapply/static/firstsuper/js/custom.js"></script> 
<script type="text/javascript" src="../../eapply/static/firstsuper/js/validation.js"></script> 

 <script type="text/javascript">
	$(window).on('load', function () {
	$("#dobId").attr('placeholder', 'dd/mm/yyyy'); 
			   if(navigator.appVersion.match(/MSIE [\d.]+/)){
					  	var placeholderText = 'dd/mm/yyyy';
						if(document.getElementById('dobId').value==''){
					  	  $('#dobId').val(placeholderText);
					    }
					    $('#dobId').blur(function(){
					        $(this).val() == '' ? $(this).val(placeholderText) : false;
					    });
					    $('#dobId').focus(function(){
					        $(this).val() == placeholderText ? $(this).val('') : false;
					    });
				}
	
	 });
</script>

</body>
</html>
