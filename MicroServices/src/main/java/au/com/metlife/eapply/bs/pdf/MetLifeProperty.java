package au.com.metlife.eapply.bs.pdf;

/**
 * <p>
 * Property file reader using Properties class of java.util API .
 * </p>
 * 
 * @author Puneet Malode
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetLifeProperty {
	private static final Logger log = LoggerFactory.getLogger(MetLifeProperty.class);
    public static final String PACKAGE_NAME = "com.metlife.elodge.pdfgenerator"; /*$NON-NLS-1$*/
    
    /*Name given to this class.*/
    public static final String CLASS_NAME = "Property"; /*$NON-NLS-1$*/
    
    private String fileName;
    
    public String getFileName() {

        return fileName;
    }
    
    /*Set the property file name*/
    public void setFileName(String fileName) {

        this.fileName = fileName;
    }
    
    /* Get the value for key*/
    public String getValue(String key) {
        log.info("Getting value start");
      //Change as per Sonar fix "Try with Resource Added" start
        //FileInputStream inputStream = null;
        File file;
        String keyValue = null;
        file = new File( fileName);
        try(FileInputStream inputStream = new FileInputStream( file)) {
            /*file = new File( fileName);
            inputStream = new FileInputStream( file);*/
            Properties properties = new Properties();
            properties.load( inputStream);
            keyValue = properties.getProperty( key);
        } catch (FileNotFoundException e) {
        	log.error("Error in file: {}",e);
        } catch (IOException e) {
        	log.error("Error in input output: {}",e);
        }/*finally{
        	try {
	        	if(inputStream!=null){
					inputStream.close();
	        	}
			} catch (IOException e) {
				log.error("error in getvalue while closing the fileinputstream : {}",e);
			}
        }*/
      //Change as per Sonar fix "Try with Resource Added" ends
        log.info("Getting value finish");
        return keyValue;
    }
    
}
