package au.com.metlife.eapply.b2b.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import au.com.metlife.eapply.b2b.utility.B2bServiceHelper;

@Component
@PropertySource(value = "application.properties")
public class XMLValidationFilter implements Filter{

	
	 private final String prop;
	@Autowired
    public XMLValidationFilter(@Value("${spring.xsdpath}") String prop) {
        this.prop = prop;
        System.out.println("================== " + prop + "================== ");
    }	

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String secureString = request.getParameter("secureString");
		B2bServiceHelper b2bServiceHelper = new B2bServiceHelper();		
		try {
			
			String validXML = b2bServiceHelper.validateXMLString(secureString,
					prop + "CARE_WebService_schema_new.xsd");
			System.out.println("validXML>>"+validXML);
			if (!"valid".equalsIgnoreCase(validXML)) {
				throw new ServletException("Invalid XML Passed");
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		XMLValidationFilter filter = new XMLValidationFilter(prop);
		filterRegistrationBean.setFilter(filter);
		filterRegistrationBean.addUrlPatterns("/oauth/token");
		return filterRegistrationBean;
	}

}
