(function(angular){
'use strict';
angular.module('Metlife.corp.directives').directive('metlifeShrinkHeader', function ($window) {
  return function(scope, element, attrs) {
    angular.element($window).bind('scroll', function() {
      if (this.pageYOffset >= 50) {
        element.addClass('shrink-header');
      } else {
        element.removeClass('shrink-header');
      }
      scope.$apply();
    });
  };
});
})(angular);