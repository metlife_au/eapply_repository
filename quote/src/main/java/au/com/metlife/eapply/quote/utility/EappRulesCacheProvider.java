package au.com.metlife.eapply.quote.utility;

import java.io.Serializable;
import java.util.HashMap;

import org.drools.KnowledgeBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class EappRulesCacheProvider implements Serializable{
	final String CLASS_NAME = "EappRulesCacheProvider";
	
	private java.util.HashMap ruleMap;
	
	private String filePath;
    
    public String getFilePath() {
           return filePath;
    }

    public void setFilePath(String filePath) {
           this.filePath = filePath;
    }


	public java.util.HashMap getRuleMap() {
		return ruleMap;
	}

	public EappRulesCacheProvider(@Value("${spring.ruleFilePath}") String ruleFilePath){
		System.out.println("ruleFilePath>>"+ruleFilePath);
        this.filePath = ruleFilePath;
		loadProperties();
	}
		
	public void loadProperties()	{
		try{		
			ruleMap = getDroolsSet();
			if(ruleMap  == null){
				throw(new Exception("Drools not initialized properly. Terminating process."));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private HashMap getDroolsSet() throws Exception	{
		HashMap obj_props = new HashMap();
		//String RULE_FILE_LOC = "E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";
		try {
			obj_props.put(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID");
			obj_props.put(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_DEATH_UNT_CVR_AMT");
			obj_props.put(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_INDUSTRY_MAPPING");
			obj_props.put(QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_DEATH_FXD_COST");
			obj_props.put(QuoteConstants.RULE_NAME_INST_NORM_ALL_CVR_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_NORM_ALL_CVR_COST));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_NORM_ALL_CVR_COST");
			obj_props.put(QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_TPD_UNT_CVR_AMT");
			obj_props.put(QuoteConstants.RULE_NAME_INST_TPD_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_TPD_FXD_COST));
			System.out.println(">>>>Catching completed---RULE_NAME_INST_TPD_FXD_COST");
			obj_props.put(QuoteConstants.RULE_NAME_CARE_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_CARE_INST_IP_FXD_COST));
			System.out.println(">>>>Catching completed---RULE_NAME_CARE_INST_IP_FXD_COST");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj_props;
	}

	
	public KnowledgeBase getDroolsFromCache(String arg_property)	{	
		return (KnowledgeBase)ruleMap.get(arg_property);
	}
	

	

}
