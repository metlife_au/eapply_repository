package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPHealthOverviewPageObjects extends PageObjectLibrary{
		
	public static final By txtheight = getObjectIdentifaction("YBRIP.health.height.text.id");
	public static final By txtweight = getObjectIdentifaction("YBRIP.health.weight.text.id");
	public static final By chklast5yrsNone = getObjectIdentifaction("YBRIP.health.5yrs.none.chkbox.css");

	public static final By btnNext = getObjectIdentifaction("YBRIP.health.next.btn.css");

}
