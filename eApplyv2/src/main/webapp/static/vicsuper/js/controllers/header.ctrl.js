VicsuperApp.controller('header', ['$scope', '$rootScope', '$location', '$timeout','$window', 'ngDialog', function($scope, $rootScope, $location, $timeout, $window, ngDialog){
	
	$scope.hideHomebutton = true;
	
	$scope.init = function(){
		
		var url = $location.path();
		
		if(!url || url == "/" || url == "/landing"){
			$scope.hideHomebutton = true;
		}else{
			$scope.hideHomebutton = false;
		}
	}
	
	$scope.navigateToHomePage = function() {
	    ngDialog.openConfirm({
	        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary mr10px pr20px avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button></div></div>',
	        plain: true,
	        className: 'ngdialog-theme-plain custom-width'
	    }).then(function(){
	      $location.path("/landing");
	    }, function(e){
	      if(e=='oncancel'){
	        return false;
	      }
	    });
	  };
	
}]);
   /*Header Controller Ends*/
