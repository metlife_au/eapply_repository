package au.com.metlife.eapply.bs.utility;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import javax.mail.SendFailedException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import au.com.metlife.eapply.bs.email.MetLifeEmailService;
import au.com.metlife.eapply.bs.email.MetLifeEmailVO;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Eapplication;


public class EapplyHelper {
	
//	 Package name of this class
	public final static String PACKAGE_NAME = "com.metlife.eapply.services";	//$NON-NLS-1$
	// Name given to this class.
	public final static String CLASS_NAME = "EapplyHelper";	//$NON-NLS-1$
	
	private static final String APPLICANT_TYPE_CHILD = "child";
	
	public static final String APPLICATION_STATUS_SUBMITTED = "Submitted";
	public static final String APPLICATION_STATUS_PENDING = "Pending";
	
	 private static final String APPLICANT_TYPE_PRIMARY = "primary";
    
     
    public static String getDateExtension() {

        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "hhmmddMMyyyy");
        String theDate = formatter.format( currDate);
        formatter = null;
        currDate = null;
        return theDate;
    }
    
    public static String getTodaysDate() {

        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String theDate = formatter.format( currDate);
        formatter = null;
        currDate = null;
        return theDate;
    }
    
    public static String getDateInString(Date dte) {

        java.util.Date currDate = dte;
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String theDate = formatter.format( currDate);
        formatter = null;
        currDate = null;
        return theDate;
    }
    
    public static String getDateInHrsString(Date dte) {

        java.util.Date currDate = dte;
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy HH:mm");
        String theDate = formatter.format( currDate);
        formatter = null;
        currDate = null;
        return theDate;
    }
    
  /*  public static String addLoading(Eapplication applicantDTO) {

        Double totalPremium = 0.0;
        BigDecimal loading = BigDecimal.valueOf( 0.0);
        if (null != applicantDTO.get
                && applicantDTO.getLoading().trim().length() > 0) {
            loading = BigDecimal.valueOf( Double.valueOf( applicantDTO.getLoading()));
        }
        BigDecimal premium = BigDecimal.valueOf( 0.0);
        if (null != applicantDTO.getPremium()) {
            premium = BigDecimal.valueOf( applicantDTO.getPremium());
        }
        
        loading = loading.multiply( premium);
        loading = loading.divide( BigDecimal.valueOf( 100));
        premium = loading.add( premium);
        double p = Math.pow( 10, (double) 2);
        totalPremium = Math.round( Double.valueOf( premium.toString()) * p) / p;
        loading = null;
        premium = null;
        return totalPremium.toString();
    }*/
    
    public static String strTotalPremium(double totalPremium) {

        String str = String.valueOf( totalPremium);
        if (str.substring( str.lastIndexOf( ".") + 1, str.length()).length() == 0) {
            str = str + "00";
        } else if (str.substring( str.lastIndexOf( ".") + 1, str.length()).length() == 1) {
            str = str + "0";
        }
        return str;
    }
    
    public static double round(double num, int places) {

        double p = Math.pow( 10, (double) places);
        return Math.round( num * p) / p;
    }    
    public static String getLoadingAmount(Double premiumAmount,
            String loadingFactor) {        
        if (null != premiumAmount && null != loadingFactor) {
            try {
                premiumAmount = premiumAmount
                        - premiumAmount
                        / ( 1 + ( new BigDecimal( loadingFactor).doubleValue()) / 100);
            } catch (Exception e) {
                premiumAmount = 0.0;
            }
        } else {
            premiumAmount = 0.0;
        }
        
        if (premiumAmount <= 0.0) {
            return null;
        }
        
        return strTotalPremium( round( premiumAmount, 2));
    }    
       
    /**
     * Formats amount to $#,###,###.00 e.g. $1,000.00
     * 
     * @param amount
     * @return
     */
    public static String formatAmount(String amount) {

        String METHOD_NAME = "formatAmount";
        String formattedStr = null;
        NumberFormat formatter = null;
        try {
            formatter = new DecimalFormat( "$#,###,###.00");
            if (amount != null && !amount.trim().equalsIgnoreCase( "")
                    && formatter != null) {
                if (amount.contains( "$")) {
                    amount = amount.replace( "$", "").trim();
                }
                if (amount.contains( ",")) {
                    amount = amount.replace( ",", "").trim();
                }
                formattedStr = formatter.format( new BigDecimal( amount).doubleValue());
                if (formattedStr != null
                        && ( formattedStr.substring( 1, ( formattedStr.indexOf( ".")))).equals( "")) {
                    formattedStr = "$0."
                            + formattedStr.substring( ( formattedStr.indexOf( ".")) + 1);
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
            amount = null;
            formatter = null;
        }        
        return formattedStr;
    }
    
    public static String formatCoverAmount(String amount) {
    	
    	final String METHOD_NAME = "formatCoverAmount";
        String formattedStr = null;
        NumberFormat formatter = null;
        
        try {
            formatter = new DecimalFormat( "$#,###,###.00");
            
            if (amount != null && !amount.trim().equalsIgnoreCase( "")) {
                
                if (amount.contains( "$")) {
                    amount = amount.replace( "$", "").trim();
                }
                if (amount.contains( ",")) {
                    amount = amount.replace( ",", "").trim();
                }
                
                double coverAmount = new BigDecimal( amount).doubleValue();
                
                if (coverAmount == 0) {
                    return "$0";
                    
                } else {
                    formattedStr = formatter.format( coverAmount);
                    
                    if (formattedStr != null
                            && ( formattedStr.substring( 1, ( formattedStr.indexOf( ".")))).equals( "")) {
                        formattedStr = "$0."
                                + formattedStr.substring( ( formattedStr.indexOf( ".")) + 1);
                    }
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
            amount = null;
            formatter = null;
        }
        
        return formattedStr;
    }
    
    public static String getStrFromBoolean(boolean booleanVal) {

        String retVal = null;
        if (booleanVal) {
            retVal = "Yes";
        } else {
            retVal = "No";
        }
        return retVal;
    }   
     
    public static Boolean checkQuestionsForHide(String question,
            Vector questionHideVec) {
    	String questionText = null;
        Boolean isQuestionToHide = Boolean.FALSE;
        if (null != questionHideVec) {
            for (int itr = 0; itr < questionHideVec.size(); itr++) {
                questionText = (String) questionHideVec.get( itr);
                if (question.contains( questionText)) {
                    isQuestionToHide = Boolean.TRUE;
                }   

            }
        }
        
        return isQuestionToHide;
    }    
    public static String replaceDollar(String coverAmount) {

        String amount = "";
        if (null != coverAmount && coverAmount.contains( "$")) {
            amount = coverAmount.replace( "$", "");
        }
        return amount.trim();
    } 
   
    /**
     * Method to check if a String is <i>not null<i> and <i>not ""<i>
     * 
     * @param str
     * @return boolean
     */
    public static boolean isStringNotNullorEmpty(String str) {

        if (str != null && !"".equalsIgnoreCase( str)) {
            return true;
        }
        return false;
    }   
    
    
    
    public static Boolean isNoChange(Eapplication applicationDTO){
    	final String METHOD_NAME = "insertFreshApplicants()"; //$NON-NLS-1$
    	Applicant applicantDTO = null;
    	Cover coversDTO = null;
    	Boolean noChangeCover = Boolean.FALSE;
    	StringBuffer strBuffer = new StringBuffer();
    	if(null!=applicationDTO && null!=applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0){
    		for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
    			applicantDTO = (Applicant) iter.next();
    			if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){
    				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
    						.hasNext();) {
    					coversDTO = (Cover) iterator.next();	
    					if(null!=coversDTO){						
    						if(null!=coversDTO.getCoveroption() && "DECREASE".equalsIgnoreCase(coversDTO.getCoveroption())){
    							strBuffer.append(coversDTO.getCoveroption());
    						}
    						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains("Cancel")){
    							strBuffer.append("Cancel");							
    						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains("Increase") 
    								){
    							strBuffer.append("Increase");  							
    							
    						}
    					}
    						
    					
    				}
    			}
    		}
    		if(!strBuffer.toString().contains("Increase")){
        		noChangeCover = Boolean.TRUE;
        	}
    	}
    	
    	return noChangeCover;
    }
    
public static Boolean isWaitBenIPCoverChange(Cover coversDTO){
		
		Boolean ipFlagforWaitBenPer = Boolean.FALSE;
		Boolean ipFlagforBenfitBenPer = Boolean.FALSE;
		int exBenPeriod = 0;
		int addBenPeriod = 0;
		int exWaitPeriod = 0;
		int addWaitPeriod = 0;
		
		if(null!= coversDTO.getExistingipbenefitperiod() && !coversDTO.getExistingipbenefitperiod().equalsIgnoreCase(coversDTO.getAdditionalipbenefitperiod())){
			
			if(coversDTO.getExistingipbenefitperiod().contains("Years")){
				exBenPeriod = Integer.parseInt(coversDTO.getExistingipbenefitperiod().substring(0,1).trim());
			}else if(coversDTO.getExistingipbenefitperiod().contains("Age")){
				exBenPeriod = Integer.parseInt(coversDTO.getExistingipbenefitperiod().substring(4, 6).trim());
			}
			if(coversDTO.getAdditionalipbenefitperiod().contains("Years")){
				addBenPeriod = Integer.parseInt(coversDTO.getAdditionalipbenefitperiod().substring(0,1).trim());
			}else if(coversDTO.getAdditionalipbenefitperiod().contains("Age")){
				addBenPeriod = Integer.parseInt(coversDTO.getAdditionalipbenefitperiod().substring(4, 6).trim());
			}
			if(addBenPeriod>exBenPeriod){
				ipFlagforBenfitBenPer = Boolean.TRUE;
			}
		}
		
		if(null!=coversDTO.getExistingipwaitingperiod() && null!=coversDTO.getExistingipwaitingperiod() && !coversDTO.getExistingipwaitingperiod().equalsIgnoreCase(coversDTO.getAdditionalipwaitingperiod())){
			
			exWaitPeriod = Integer.parseInt(coversDTO.getExistingipwaitingperiod().substring(0, 2).trim());
			addWaitPeriod = Integer.parseInt(coversDTO.getAdditionalipwaitingperiod().substring(0, 2).trim());
			if(addWaitPeriod<exWaitPeriod){
				ipFlagforWaitBenPer = Boolean.TRUE;
			}
		}
		
		if(ipFlagforBenfitBenPer || ipFlagforWaitBenPer){
			return Boolean.TRUE;
		}else{
			return Boolean.FALSE;
		}
		
	}
    

    public static Date formatDateToString(String dateStr) throws Exception {
		
		Date dte = null;	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		if(null!=dateStr){			
			  java.util.Date parsedDate = dateFormat.parse(dateStr);
			  java.sql.Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			  dte = new Date(timestamp.getTime());
		}			 
		
		return dte;		
	}
    
    public static void sendMailForFailedMember(Eapplication applicationDTO,au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject pdfObject,String channel, String memberAction) throws IOException{
		 final String METHOD_NAME = "sendMailForFailedMember"; //$NON-NLS-1$
		MetLifeEmailVO eVO = new MetLifeEmailVO(); 
		 ArrayList mailList = new ArrayList();
		 HashMap placeHolders = new HashMap(); 
		StringBuilder applicantBulder = null;
		String brokerEmailId = null;
		String applicantName = null;
		String inputRetrieveString = null;
		String appDecision = null;
		String key = null;
		Boolean specialTerm = Boolean.FALSE;
		String emailSubject = null;
		try{
			 if (null!=pdfObject && null != pdfObject.getMemberDetails()) {
		            applicantBulder = new StringBuilder( "");
		            
		                        
		            brokerEmailId = pdfObject.getMemberDetails().getEmailId();
		            if (pdfObject.getMemberDetails().getTitle() != null) {
		            	  eVO.setTitle( pdfObject.getMemberDetails().getTitle());
		                applicantBulder.append( pdfObject.getMemberDetails().getTitle()
		                        + " ");
		            }else{
		            	eVO.setTitle("");
		            }
		            if (pdfObject.getMemberDetails().getMemberFName() != null) {
		            	 eVO.setFirstName( pdfObject.getMemberDetails().getMemberFName() );
		                applicantBulder.append( pdfObject.getMemberDetails().getMemberFName()
		                        + " ");
		            }else{
		            	eVO.setFirstName("");
		            }
		            if (pdfObject.getMemberDetails().getMemberLName() != null) {
		            	 eVO.setLastName( pdfObject.getMemberDetails().getMemberLName());
		                applicantBulder.append( pdfObject.getMemberDetails().getMemberLName());
		            }else{
		            	 eVO.setLastName( "");
		            }
		        }
		        if (null != applicantBulder) {
		            applicantName = applicantBulder.toString();
		        }
		        appDecision = applicationDTO.getAppdecision();
		        eVO.setApplicationId( pdfObject.getApplicationNumber());          
		        //** Email Parameters *//*    
		      if("ACC".equalsIgnoreCase( applicationDTO.getAppdecision()) && (pdfObject.isExclusionInd() || pdfObject.isLoadingInd())){
		        	specialTerm = Boolean.TRUE;
		        }
		        //eVO.setFromSender(ConfigurationHelper.getConfigurationValue(applicationDTO.getFundinfo().getFundId(), "SENDER_EMAIL"));      
		        eVO.setApplicationtype("Institutional"); 		      
		        placeHolders.put( "EMAIL_SMTP_HOST", "smtp.gmail.com");
		        placeHolders.put("EMAIL_SMTP_PORT", 25);		       
		        placeHolders.put( "APPLICATIONNUMBER", applicationDTO.getApplicationumber());
		        placeHolders.put( "TITLE", eVO.getTitle());
		        placeHolders.put( "FIRSTNAME", eVO.getFirstName());
		        placeHolders.put("LASTTNAME", eVO.getLastName());      
		        placeHolders.put( "EMAIL_IND_SUBJECT_NAME", applicantName);
		        placeHolders.put( "EMAIL_SUBJECT", "New Online Application received from - <%TITLE%> <%FIRSTNAME%> <%LASTNAME%>");
		        placeHolders.put( "ACTION", channel);        
		        
		        
		        if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
		        	placeHolders.put( "APP_DECISION", "decline");
		        }else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
		        	placeHolders.put( "APP_DECISION", "referred");
		        }else{
		        	placeHolders.put( "APP_DECISION", "accepted");
		        }		        
		      
		        if(!"eApply".equalsIgnoreCase(channel) && null!=memberAction && "Accept".equalsIgnoreCase(memberAction)){
		        	
		        }else if(!"eApply".equalsIgnoreCase(channel) && null!=memberAction && "Decline".equalsIgnoreCase(memberAction)){
		        	
		        }else{
		        	
		        }		          
		        eVO.setEmailDataElementMap( placeHolders);        
		        mailList.add(brokerEmailId);
		        eVO.setToRecipents( mailList); 
		        
		        MetLifeEmailService.getInstance().sendMail( eVO, pdfObject.getPdfAttachDir());		        
		       
		        
		}catch(SendFailedException emailException){
			emailException.printStackTrace();
       } 
       catch (Exception e) {
    	   e.printStackTrace();
       }finally{
       	 mailList = null;
            placeHolders = null;       
            brokerEmailId = null; 
            eVO = null;
            applicantName = null;
            applicantBulder = null;
       }
      
	}
    
	public static String getDCLReasons(List<Cover> coverList,String decision,boolean isDCLreasonFlag){
		Cover coversDTO = null;
		String productName = null;
		String temp = null;
		StringBuffer productNameBuffer = new StringBuffer();
		  for (int covItr = 0; covItr < coverList.size(); covItr++) {
          	coversDTO = (Cover) coverList.get( covItr);            
          	if(null!=coversDTO && decision.equalsIgnoreCase(coversDTO.getCoverdecision()) && !isDCLreasonFlag){
          		productNameBuffer.append(coversDTO.getCovertype());
          		productNameBuffer.append( ",");
          	//}else if(null!=coversDTO && null!=coversDTO.getCoverAuraDclReason() && !productNameBuffer.toString().contains(coversDTO.getCoverAuraDclReason())
        	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Death cover - ");
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Total & Permanent Disability cover - ");
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Income Protection cover - ");
          		}
          		/*if(null!=coversDTO.getCoverAuraDclReason() && coversDTO.getCoverAuraDclReason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverAuraDclReason().substring(0, coversDTO.getCoverAuraDclReason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverAuraDclReason());
          		}*/ 
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}
          		productNameBuffer.append( "\n");
          	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Death cover - ");
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Total & Permanent Disability cover - ");
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Income Protection cover - ");
          		}
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}          		
          		productNameBuffer.append( "\n");
          		
          //	}else if(null!=coversDTO && null!=coversDTO.getCoverOrgDclReason() && !productNameBuffer.toString().contains(coversDTO.getCoverOrgDclReason())
        	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Death cover - ");
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Total & Permanent Disability cover - ");
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Income Protection cover - ");
          		}
          		/*if(null!=coversDTO.getCoverOrgDclReason() && coversDTO.getCoverOrgDclReason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverOrgDclReason().substring(0, coversDTO.getCoverOrgDclReason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverOrgDclReason());
          		}*/ 
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}
          		productNameBuffer.append( "\n");
          	}
		  }
		  if(productNameBuffer.toString().contains(",") && !isDCLreasonFlag){			 
			  productName =productNameBuffer.substring(0, productNameBuffer.toString().lastIndexOf(",")) ;			  
			  if(productName.contains(",")){
				  temp = productName.substring(0, productName.lastIndexOf(","));				  
				  productName = temp +" and "+productName.substring(productName.lastIndexOf(",")+1) +" ";			 
			  
			  }
		  }else{
			  productName =productNameBuffer.toString();
		  }
		  coversDTO = null;		  
		  temp = null;
		  productNameBuffer = null;
		  return productName;
	}
    
    
    /*public static boolean determineSavedAppliactionValidatity(Eapplication applicationDTO,String userName, String authtoken,String fundId){
		final String METHOD_NAME = "determineSavedAppliactionValidatity";
		Date todayDate = new Date();
		Date dob = null;
		Date lastSavedDate = null;	
		int ageAtSaved = 0;
		int currentAge = 0;
		SimpleDateFormat dateformat = null;
		boolean birthDayPassed = false,thrityDaysPassed=false,auraVersionChanged=false,applicationExpired=false;
		long diff =0;
		long diffDays = 0;
    	try{
			if(applicationDTO!=null){
				dateformat = new SimpleDateFormat("dd/MM/yyyy");
				if(applicationDTO.getApplicant()!=null){
					dob = applicationDTO.getApplicant().get(0).getBirthdate();	
					lastSavedDate = applicationDTO.getLastupdatedate();
			    	ageAtSaved = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(lastSavedDate));
			    	currentAge = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(todayDate));
					if(ageAtSaved == currentAge){
						birthDayPassed = false;
					}else{
						birthDayPassed = true;
					}
					
					//30 Days Validation Business Rule - secound Rule
					todayDate = dateformat.parse(dateformat.format(todayDate));
					lastSavedDate = dateformat.parse(dateformat.format(lastSavedDate));
					diff = todayDate.getTime() - lastSavedDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);
					if(diffDays > 30){
						thrityDaysPassed = Boolean.TRUE;
					}else{
						thrityDaysPassed = Boolean.FALSE;
					}
				}			
				//Third Rule for Aura Verison Check
				if((applicationDTO.getIsNUWFlow()==null ||!applicationDTO.getIsNUWFlow()) && !"coverDtls".equalsIgnoreCase(applicationDTO.getLastSavedOn())){
					String  latestAuraVersion = null;
					LookupClientAgent lCA = (LookupClientAgent) ClientAgentHelper.getClientAgent(LookupClientAgent.CLASS_NAME); 
					List  auraVersionList = lCA.getLookupData(userName, authtoken,EapplicationConstant.AURAVERSION,EapplicationConstant.PARTNERNAME,fundId,Boolean.TRUE,null);
					for(int itr=0; itr<auraVersionList.size(); itr++){
		                latestAuraVersion = ((LookupData)auraVersionList.get(itr)).getCode();
					}
					if(null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails() 
							&& null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion() ){
						if(LogHelper.isDebugLogOn()){
							LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, EapplicationConstant.GETAURAVERSIONOUTSIDE+((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion());
						}
		                if(!latestAuraVersion.equalsIgnoreCase(((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion())){
		                	if(LogHelper.isDebugLogOn()){
		        			LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, EapplicationConstant.GETAURAVERSION+((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion());
		                	}
		                	auraVersionChanged = true;
		                }
					}
				}else{
					auraVersionChanged = false;
				}
                if(birthDayPassed || thrityDaysPassed || auraVersionChanged){
                	applicationExpired=true;
                }else{
                	applicationExpired=false;
                }
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
    	return applicationExpired;
    }*/
    
	public static int getAgeNextBirthDay(String dob, String dateCompare){
		final String METHOD_NAME = "getAgeNextBirthDay";
		int age = 0;
		Calendar birth = null;
		Calendar today = null;
		Date birthDate = null;
		Date currentDate = null;
		try {
			birth = new GregorianCalendar();
			today = new GregorianCalendar();
			birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
			currentDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateCompare);
			if (birth != null && today != null && birthDate != null
					&& currentDate != null) {
				birth.setTime(birthDate);
				today.setTime(currentDate);
				age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
				if (today.get(Calendar.DAY_OF_MONTH) == birth.get(Calendar.DAY_OF_MONTH)
						&& today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)) {
					age = age+1;

				} else if (today.get(Calendar.MONTH) < birth.get(Calendar.MONTH)) {
					age = age;
				} else if (today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)
						&& today.get(Calendar.DAY_OF_MONTH) < birth.get(Calendar.DAY_OF_MONTH)) {

				} else {
					age = age + 1;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			birth = null;
			birthDate = null;
			currentDate = null;
			dob = null;
			today = null;
		}

		return age;
	}
	
	public static String getDCLProductName(List<Cover> coverList,String decision,boolean isDCLreasonFlag){
		Cover coversDTO = null;
		String productName = null;
		String temp = null;
		StringBuffer productNameBuffer = new StringBuffer();
		  for (int covItr = 0; covItr < coverList.size(); covItr++) {
          	coversDTO = (Cover) coverList.get( covItr);            
          	if(null!=coversDTO && decision.equalsIgnoreCase(coversDTO.getCoverdecision()) && !isDCLreasonFlag){
          		productNameBuffer.append(coversDTO.getCovertype());
          		productNameBuffer.append( ",");
          	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Death cover - ");
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Total & Permanent Disability cover - ");
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Income Protection cover - ");
          		}
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}          		
          		productNameBuffer.append( "\n\n");
          		
          	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Death cover - ");
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Total & Permanent Disability cover - ");
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append("Income Protection cover - ");
          		}
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}       		
          		productNameBuffer.append( "\n\n");
          	}
		  }
		  if(productNameBuffer.toString().contains(",") && !isDCLreasonFlag){			 
			  productName =productNameBuffer.substring(0, productNameBuffer.toString().lastIndexOf(",")) ;			  
			  if(productName.contains(",")){
				  temp = productName.substring(0, productName.lastIndexOf(","));				  
				  productName = temp +" and "+productName.substring(productName.lastIndexOf(",")+1) +" ";			 
			  
			  }
		  }else{
			  productName =productNameBuffer.toString();
		  }
		  coversDTO = null;		  
		  temp = null;
		  productNameBuffer = null;
		  return productName;
	}
	
	

	
	/**
	 * @param lodgeFundId
	 * @param memberType
	 * @param ruleInfoMap
	 * @return
	 */
	
	
	/**
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str){
		if(str==null){
			return true;
		}else if(str!=null && str.trim().equalsIgnoreCase("")){
			return true;
		}else{
			return false;
		}
	}
	
	public static String formatIntAmount(String amount){
		final String METHOD_NAME = "formatAmount";
		
		String formattedStr=null;
		NumberFormat formatter=null;
		try{
			formatter = new DecimalFormat("$#,###,###");
			if(amount!=null && !amount.trim().equalsIgnoreCase("") && formatter!=null){
				formattedStr=formatter.format(new BigDecimal(amount).intValue());
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			amount=null;
			formatter=null;
		}
		
		return formattedStr;
	}
	
	  public static boolean determineSavedAppliactionValidatity(Eapplication eapplication,String fundId){
			final String METHOD_NAME = "determineSavedAppliactionValidatity";
			Date todayDate = new Date();
			Date dob = null;
			Date lastSavedDate = null;	
			int ageAtSaved = 0;
			int currentAge = 0;
			SimpleDateFormat dateformat = null;
			boolean birthDayPassed = false,thrityDaysPassed=false,auraVersionChanged=false,applicationExpired=false;
			long diff =0;
			long diffDays = 0;
	    	try{
				if(eapplication!=null){
					dateformat = new SimpleDateFormat("dd/MM/yyyy");
					if(eapplication.getApplicant()!=null){
						dob = eapplication.getApplicant().get(0).getBirthdate();	
						lastSavedDate = eapplication.getLastupdatedate();
				    	ageAtSaved = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(lastSavedDate));
				    	currentAge = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(todayDate));
						if(ageAtSaved == currentAge){
							birthDayPassed = false;
						}else{
							birthDayPassed = true;
						}
						
						//30 Days Validation Business Rule - secound Rule
						todayDate = dateformat.parse(dateformat.format(todayDate));
						lastSavedDate = dateformat.parse(dateformat.format(lastSavedDate));
						diff = todayDate.getTime() - lastSavedDate.getTime();
						diffDays = diff / (24 * 60 * 60 * 1000);
						if(diffDays > 30){
							thrityDaysPassed = Boolean.TRUE;
						}else{
							thrityDaysPassed = Boolean.FALSE;
						}
					}
					

					//Third Rule for Aura Verison Check
//					if((applicationDTO.getIsNUWFlow()==null ||!applicationDTO.getIsNUWFlow()) && !"coverDtls".equalsIgnoreCase(applicationDTO.getLastSavedOn())){
//						String  latestAuraVersion = null;
//						LookupClientAgent lCA = (LookupClientAgent) ClientAgentHelper.getClientAgent(LookupClientAgent.CLASS_NAME); 
//						List  auraVersionList = lCA.getLookupData(userName, authtoken,EapplicationConstant.AURAVERSION,EapplicationConstant.PARTNERNAME,fundId,Boolean.TRUE,null);
//						for(int itr=0; itr<auraVersionList.size(); itr++){
//			                latestAuraVersion = ((LookupData)auraVersionList.get(itr)).getCode();
//						}
//						if(null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails() 
//								&& null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion() ){
//			                if(!latestAuraVersion.equalsIgnoreCase(((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion())){
//			                	auraVersionChanged = true;
//			                }
//						}
//					}else{
//						auraVersionChanged = false;
//					}
					
	                if(birthDayPassed || thrityDaysPassed || auraVersionChanged){
	                	applicationExpired=true;
	                }else{
	                	applicationExpired=false;
	                }
				}
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	return applicationExpired;
	    }
    
}

