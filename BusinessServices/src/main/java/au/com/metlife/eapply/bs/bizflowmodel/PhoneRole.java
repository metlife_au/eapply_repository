package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhoneRole")

@XmlRootElement
public class PhoneRole {	
	
	private Phone Phone;

	public Phone getPhone() {
		return Phone;
	}

	public void setPhone(Phone phone) {
		Phone = phone;
	}

	
	
	
	
}
