/**
 * 
 */
package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.PersonalStatementPageObjects;

/**
 * @author sampath
 * 
 */
public class VICSPersonalStatementDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public VICSPersonalStatementDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public VICSPersonalStatementDetailsPage enterVICSPersonalStatementDetails() {
		fillInVICSDeatils();
		fillInVICSLifeStyleDetails();
		fillInVICSInsuranceDetails();
		return new VICSPersonalStatementDetailsPage(scriptHelper);
	}
	
	public VICSPersonalStatementDetailsPage enterVICSPersonalStatementDets() {
		fillIncreaseVICSPersonalDetails();
		return new VICSPersonalStatementDetailsPage(scriptHelper);
	}
	

	private void fillInVICSPersonalStatementDetails() {
		
		
		try{
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radStoppingDutiesNo),"Click","An injury or illness currently stopping you from performing all the normal and usual duties of your usual occupation?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radInsuranceHistNo),"Click","Have you ever had life or disability insurance refused, postponed or issued on special terms?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkYourHealthNota),"chkCheck","None of the above");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radRegularMedicationNo),"Click","Have you been prescribed or advised to take medication on a regular or ongoing basis?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radLostTheSightNo),"Click","Have you lost the sight of an eye or suffered the total and permanent loss of the use of a 'limb'?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radMadeAClaimNo),"Click","Are you currently pregnant?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radIllnessRestrictsNo),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinueVICSPersonalStatement),"MoveToElm","Continue");
			sleep();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinueVICSPersonalStatement),"Click","Continue");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInVICSInsuranceDetails() {
		
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strReceivedClaim = dataTable.getData("MetLife_Data", "personal.receivedClaim");
		
		
		try{			
			List<WebElement> radInsuranceDetailsNo = null;
			radInsuranceDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsNo);
			List<WebElement> radInsuranceDetailsYes = null;
			radInsuranceDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsYes);
			
			
			tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(0)):fluentWaitElement(radInsuranceDetailsNo.get(0)),"Click","Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined? - "+strInsuTPD);
			tryAction(strReceivedClaim.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(1)):fluentWaitElement(radInsuranceDetailsNo.get(1)),"Click","Are you contemplating or have you ever made a claim for or received sickness? - "+strReceivedClaim);
			if(radInsuranceDetailsYes.size()==3)
				tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(2)):fluentWaitElement(radInsuranceDetailsNo.get(2)),"Click","Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund? - " +strInsuTPD);
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkInsuranceDeatilsNota),"chkCheck","None of the above");
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinueVICSPersonalStatement),"MoveToElm","Continue");
			sleep();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinueVICSPersonalStatement),"Click","Continue");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
	
	private void fillInVICSDeatils(){
		try{
			
			String strHeight = dataTable.getData("MetLife_Data", "personal.height");
			String strHeightUnits = dataTable.getData("MetLife_Data", "personal.heightunits");
			String strWeight = dataTable.getData("MetLife_Data", "personal.weight");
			String strWeightUnits = dataTable.getData("MetLife_Data", "personal.weightunits");
			String strCitizen = dataTable.getData("MetLife_Data", "personal.citizen");
			String strGender = dataTable.getData("MetLife_Data", "change.gender");
			String strRegVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
			String str3YrDiagnosis = dataTable.getData("MetLife_Data", "last3YearsDiagnosied");
			String strSmoked = dataTable.getData("MetLife_Data", "personal.smoked");
			String strPregnent = dataTable.getData("MetLife_Data", "personal.pregnent");
			String[] array3YrDiagnosis = str3YrDiagnosis.split(",");
			String strAsthmaWorsen = dataTable.getData("MetLife_Data", "personal.Asthamaworesen");
			
			tryAction(strCitizen.equalsIgnoreCase("yes")?waitForClickableElement(PersonalStatementPageObjects.radCitizenPRYes):waitForClickableElement(PersonalStatementPageObjects.radCitizenPRNo),"Click","Are you a citizen or permanent resident of Australia? "+strCitizen);
			
			
			if(strGender.equalsIgnoreCase("female")){
				List<WebElement> radHealthDetailsNo = null;
				radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
				List<WebElement> radHealthDetailsYes = null;
				radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsYes);
				tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
				tryAction(strPregnent.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Are you currently pregnant? - "+strPregnent);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(2)):fluentWaitElement(radHealthDetailsNo.get(2)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}else{
				List<WebElement> radHealthDetailsNo = null;
				radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
				List<WebElement> radHealthDetailsYes = null;
				radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsYes);
				tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}
			
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtHeight),"SET","Height",strHeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstHeightUnits),"DropDownSelect","Height Units",strHeightUnits);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.txtWeight),"SET","Weight",strWeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstWeightUnits),"DropDownSelect","Weight Units",strWeightUnits);
			
			
			//List<WebElement> radHealthDetails = null;
			//radHealthDetails = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistNo),"Click","Family History");
			//sleep();
			//tryAction(fluentWaitElement(radHealthDetails.get(0)),"Click","Have you smoked in the past 12 months?");
			//sleep();
			//tryAction(fluentWaitElement(radHealthDetails.get(1)),"Click","Are you currently pregnant?");
			//tryAction(fluentWaitElement(radHealthDetails.get(2)),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			/*tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
			tryAction(fluentWaitElements(PersonalStatementPageObjects.chkAsthma),"chkCheck","Asthma");
			tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radAsthmaWorseNo),"Click","Is your asthma worsened by your occupation?");
			*/
			
			for(int i=0;i<array3YrDiagnosis.length;i++){
				System.out.println("array3YrDiagnosis[i] : "+array3YrDiagnosis[i]);
				if(array3YrDiagnosis[i].equalsIgnoreCase("Lung")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
					tryAction(fluentWaitElements(PersonalStatementPageObjects.chkAsthma),"chkCheck","Asthma");
					tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition");
					String strNextId = null;
					strNextId = getAttribute(waitForClickableElement(PersonalStatementPageObjects.radAsthmaMild), "name");
					String strRadioId =null;
					strRadioId = org.apache.commons.lang.StringUtils.substring(strNextId, 0, 15)+"1";
					System.out.println("strRadioId is :: "+strRadioId);
					tryAction(strAsthmaWorsen.equalsIgnoreCase("yes")?waitForClickableElement(getObject("input[type='radio'][name='"+strRadioId+"'][value='Yes']")):waitForClickableElement(getObject("input[type='radio'][name='"+strRadioId+"'][value='No']")),"Click","Is your asthma worsened by your occupation? "+strAsthmaWorsen );
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("none")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.notaLungSTFt),"chkCheck","Selected Values is None Of the Above ");
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("copd")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxCOPD),"chkCheck","Selected Values is Emphysema/COPD ");
				}
			}		
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBPCholNota),"Click","In the last 5 years have you suffered from?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBrainBoneNota),"Click","Have you ever suffered from, been diagnosed with or sought medical advice or treatment for?");
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		
	}
	
	private void fillIncreaseVICSPersonalDetails(){
		String strEmployeement = dataTable.getData("MetLife_Data", "personal.employeement");
		String strInsurance = dataTable.getData("MetLife_Data", "personal.insurancehistory");
		String strHealth = dataTable.getData("MetLife_Data", "personal.health");
		String strRegularMedi = dataTable.getData("MetLife_Data", "personal.regularmedication");
		String strLimbUse = dataTable.getData("MetLife_Data", "personal.limbuse");
		String strClaimfrmSrc = dataTable.getData("MetLife_Data", "personal.claimfromothersource");
		String strInjury = dataTable.getData("MetLife_Data", "personal.injuryinlast3yrs");
				
		
		try{
			
			tryAction(strEmployeement.equalsIgnoreCase("yes")?waitForClickableElement(PersonalStatementPageObjects.radEmployeementYes):waitForClickableElement(PersonalStatementPageObjects.radEmployeementNo),"Click","At the date of this application, is an injury or illness currently stopping you from performing all the normal and usual duties of your usual occupation? is : "+strEmployeement);
			tryAction(strInsurance.equalsIgnoreCase("yes")?waitForClickableElement(PersonalStatementPageObjects.radInsuranceYes):waitForClickableElement(PersonalStatementPageObjects.radInsuranceNo),"Click","Have you ever had life or disability insurance refused, postponed or issued on special terms (for example with a premium loading or an exclusion)? is : "+strInsurance);
			if(strHealth.equalsIgnoreCase("none of the above"))
				tryAction(fluentWaitElements(PersonalStatementPageObjects.chkboxHealthDeatilsNota),"chkCheck",strHealth);
			else if(strHealth.equalsIgnoreCase("Emphysema"))
				tryAction(fluentWaitElements(PersonalStatementPageObjects.chkboxEmphysema),"chkCheck",strHealth);
			else
				tryAction(fluentWaitElements(PersonalStatementPageObjects.chkboxHealthDeatilsNota),"chkCheck",strHealth);
			List<WebElement> radHealthDetailsNo = null;
			List<WebElement> radHealthDetailsYes = null;
			radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthNo);
			radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthYes);
			tryAction(strRegularMedi.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Other than for the conditions mentioned above, have you been prescribed or advised to take medication on a regular or ongoing basis (excluding contraception)? is : "+strRegularMedi);
			tryAction(strLimbUse.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Have you lost the sight of an eye or suffered the total and permanent loss of the use of a 'limb'? ('limb' means the whole hand or foot.) is : "+strLimbUse);
			tryAction(strClaimfrmSrc.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(2)):fluentWaitElement(radHealthDetailsNo.get(2)),"Click","Have you previously made a claim or are you intending to make a claim with any insurance company or social security department for disability insurance, Workers� Compensation, Motor Vehicle Accident Insurance or Veteran�s Affairs? is : "+strClaimfrmSrc);
			tryAction(strInjury.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(3)):fluentWaitElement(radHealthDetailsNo.get(3)),"Click","Is there any injury or illness which restricts you from carrying out all the duties of any occupation you have held in the last three years? is : "+strInjury);
			sleep(2000);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnVICSContinue),"Click","Continue");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action fillIncreaseVICSPersonalDetails " + e.getMessage(), Status.FAIL);
		}
		

		
		
		
	}

	private void fillInVICSLifeStyleDetails() {
		
		
		try{
			
			String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
			String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
			String strAlcoholConsu = dataTable.getData("MetLife_Data", "personal.alcohlConsumption");
			String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
			String strAbtHIV = dataTable.getData("MetLife_Data", "personal.HIV");
			
			List<WebElement> radLifeStyleDetailsNo = null;
			radLifeStyleDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsNo);
			List<WebElement> radLifeStyleDetailsYes = null;
			radLifeStyleDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsYes);
			
			
			
			tryAction(strTravelPlan.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(0)):fluentWaitElement(radLifeStyleDetailsNo.get(0)),"Click","Do you have firm plans to travel or reside in another country other than NZ?");
			tryAction(strDrugs.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(1)):fluentWaitElement(radLifeStyleDetailsNo.get(1)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you? - "+strDrugs);
			tryAction(strAlcoholConsu.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(2)):fluentWaitElement(radLifeStyleDetailsNo.get(2)),"Click","Have you ever been advised by a health professional to reduce your alcohol consumption? - "+strAlcoholConsu);
			tryAction(strAbtHIV.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(3)):fluentWaitElement(radLifeStyleDetailsNo.get(3)),"Click","Do you have HIV (Human Immunodeficiency Virus) that causes AIDS? - "+strAbtHIV);
			if(strAbtHIV.equalsIgnoreCase("no")){
				String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetailsYes.get(3).getAttribute("name")+"_1_0'][value='No']";
				//Click On No for HIV
				tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
			}
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkSportsNota),"Click","Do you regularly engage in or intend to engage in any of the following hazardous activities?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtAlcoholicDrinksConsumption),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnAlcoholicEnter),"CLICK","Enter");
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
	
}
