package au.com.metlife.eapply.utils;

import java.io.File;
import java.io.IOException;

import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.corporate.business.CorpEntityRequest;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.underwriting.model.AuraResponse;

public class TestUtils {
	
	public String createTestData(String fileName) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String json = null;

			File file = new ClassPathResource(fileName).getFile();
			CorpEntityRequest obj = mapper.readValue(file, CorpEntityRequest.class);
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		
		return json;

	}
	
	public  CorpEntityRequest createTestDataObject(String fileName) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
			File file = new ClassPathResource(fileName).getFile();
			CorpEntityRequest obj = mapper.readValue(file, CorpEntityRequest.class);
			
		
		return obj;

	}
	
	
	
	public String createEapplyInputTestData(String fileName) throws IOException{
		ObjectMapper mapper = new ObjectMapper();
		
		File file = new ClassPathResource(fileName).getFile();
		EapplyInput inputData = mapper.readValue(file, EapplyInput.class);
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(inputData);
		return json;
	}
	
	//for QuoteController /calculateAll
		public String createTestDataForQuote(String fileName) throws JsonParseException, JsonMappingException, IOException {
			ObjectMapper mapper = new ObjectMapper();
			String json = null;
	
				File file = new ClassPathResource(fileName).getFile();
				RuleModel obj = mapper.readValue(file, RuleModel.class);
			 json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			
			return json;

		}
		
		
		public String createTestDataForUnderwritting(String fileName) throws JsonParseException, JsonMappingException, IOException {
			ObjectMapper mapper = new ObjectMapper();
			String json = null;
	
				File file = new ClassPathResource(fileName).getFile();
				AuraResponse obj = mapper.readValue(file, AuraResponse.class);
			 json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			
			return json;

		}

}
