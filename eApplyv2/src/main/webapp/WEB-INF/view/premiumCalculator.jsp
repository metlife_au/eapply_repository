<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/bootstrap.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/docs.min.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/datepicker.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/bootstrap-select.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/ngDialog.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/ngDialog-theme-plain.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../eapply/static/premiumCalculator/src/assets/common/css/custom-ben.css">
    
    <script src="../../eapply/resources/js/lib/jQuery/jquery.3.2.1.min.js"></script>
    <script src="../../eapply/resources/js/lib/jQuery/jquery-ui.min.js"></script>
    <script src="../../eapply/resources/js/lib/bootstrap/bootstrap-4.0.0-beta.bundle.min.js"></script>
	<title>{{$root.pageTitle}}</title>
</head>
<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
String EncryptData = response.getHeader("EncryptData");
%>
<script type="text/javascript">
var inputData = <%=headerData%>;
var baseUrl = '<%= domainName%>';
var EncryptData = '<%= EncryptData%>';

console.log(inputData);

var fundID = inputData.partnerID;
var stylePath = '../../eapply/static/premiumCalculator/src/assets/'+fundID+'/css/custom.css';
var iconPath = '../../eapply/static/premiumCalculator/src/assets/'+fundID+'/images/favicon.ico';

function addStyle(filename, type){	
	/* var head = document.getElementsByTagName('head')[0];
	var script = document.createElement('link');
	script.href = filename;
	script.rel = 'stylesheet';
	head.append(script); */
	
	var fileref=document.createElement("link");    
    if(type == 'icon'){
    	fileref.setAttribute("rel", "shortcut icon");
    }else{
    	fileref.setAttribute("rel", "stylesheet");
    }
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    
    document.getElementsByTagName("head")[0].appendChild(fileref);
}

addStyle(stylePath, 'css');
addStyle(iconPath, 'icon');
</script>

<body class="tooltip-demo">
    
    <!-- THIS IS WHERE WE WILL INJECT OUR CONTENT ============================== -->
    <div ui-view autoscroll="true"></div>
    
    <!-- THIS IS PLACE LOADER OVER THE CONTENT ============================== -->
    <div id="body-mask" data-loading>
        <div class="body-mask-loader"></div>
    </div>
    <!-- Application source file -->
    <script src="../../eapply/static/premiumCalculator/build/main.js"></script>
    <script charset='UTF-8'>
            window['adrum-start-time'] = new Date().getTime();
            window['adrum-config'] = {
              userEventInfo: {
                  "PageView": function(context) {
                      return {
                          userPageName: "Insurance Calculator",
                          userData: {
                            first_name: 'Insurance Calculator First Name',
                            last_name: 'Insurance Calculator Last Name'
                          }
                      }
                  },
                  "VPageView": function(context) {
                      return {
                          userData: {
                            app_number: '1234567890',
                            first_name: 'Insurance Calculator First Name',
                            last_name: 'Insurance Calculator Last Name'
                          }
                      }
                  }
              }
            };
            (function(config){
                config.appKey = 'EUM-AAB-AUA';
                config.adrumExtUrlHttp = 'http://cdn.appdynamics.com';
                config.adrumExtUrlHttps = 'https://cdn.appdynamics.com';
                //config.beaconUrlHttp = 'https://uat.eum.metlife.com.au';
                //config.beaconUrlHttps = 'https://uat.eum.metlife.com.au';
                config.xd = {enable : false};
            })(window['adrum-config'] || (window['adrum-config'] = {}));
            if ('https:' === document.location.protocol) {
                document.write(unescape('%3Cscript')
             + " src='https://cdn.appdynamics.com/adrum/adrum-4.4.0.117.js' "
             + " type='text/javascript' charset='UTF-8'"
             + unescape('%3E%3C/script%3E'));
            } else {
                document.write(unescape('%3Cscript')
             + " src='http://cdn.appdynamics.com/adrum/adrum-4.4.0.117.js' "
             + " type='text/javascript' charset='UTF-8'"
             + unescape('%3E%3C/script%3E'));
            }
            </script>
  </body>
</html>