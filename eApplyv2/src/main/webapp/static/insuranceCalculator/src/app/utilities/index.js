import angular from 'angular';

// Create the module where our functionality can attach to
let filtersModule = angular.module('app.filters', []);


import {SplitFilter} from './common.filters';
filtersModule.filter('split', SplitFilter);

import {checkboxFilter} from './common.filters';
filtersModule.filter('checkboxFilter', checkboxFilter);

import {SectionSplitFilter} from './common.filters';
filtersModule.filter('sectionSplit', SectionSplitFilter);

import {dateTextSplitFilter} from './common.filters';
filtersModule.filter('dateTextSplit', dateTextSplitFilter);


export default filtersModule;
