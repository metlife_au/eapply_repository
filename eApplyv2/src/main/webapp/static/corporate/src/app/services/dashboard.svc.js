angular.module('CorporateApp')
	.factory('searchMemberDetailsService',['$http','$q', 'fetchTokenNumSvc',function($http,$q, fetchTokenNumSvc){
		var object={};
		object.getMemberDetails= function(url,fundCode,firstName,lastName,status,applicationNumber,fromDate,toDate){
			var defer= $q.defer();
			$http.get(url,{
				headers:{
					'Content-Type': 'application/json',
					'Authorization':fetchTokenNumSvc.getTokenId()
				},
				params:{
					'fundCode':fundCode,
					'firstName': firstName,
					'lastName': lastName,
					'status': status,
					'applicationNumber': applicationNumber,
					'fromDate': fromDate,
					'toDate':toDate
				}
			}).then(function(res){
				object= res.data;
				defer.resolve(res);
			},function(err){
				console.log('Something went wrong file fecthing member details...'+JSON.stringify(err));
				defer.reject(err);
			});
			return defer.promise;
		};
		return object;
	}])
	
	.factory('resendEmailService',['$http','$q','fetchTokenNumSvc',function($http,$q,fetchTokenNumSvc){
		var object = {};
		object.reSendEmail = function(url,fundCode,emailid,firstName,lastName,applicationNumber){
			var defer = $q.defer();
			var dataobj = {
					'fundCode':fundCode,
					'emailid':emailid,
					'firstName':firstName,
					'lastName':lastName,
					'applicationNumber':applicationNumber
				};
			$http.post(url,dataobj,{
				headers:{
					'Content-Type': 'application/json',
					'Authorization':fetchTokenNumSvc.getTokenId()
				}
			}).then(function(res){
				object = res.data;
				defer.resolve(res);
			},function(err){
				console.log('Something went wrong while resending email'+JSON.stringify(err));
				defer.reject(err);
			});
			return defer.promise;
		};
		return object;
	}])
	.factory('updateStatusService',['$http','$q', 'fetchTokenNumSvc', function($http,$q, fetchTokenNumSvc){
		var object = {};
		object.updateStatus = function(url,fundCode,status,firstName,lastName,applicationNumber){
			var defer = $q.defer();
			//var dataURL = url+"?fundCode="+fundCode+"&status="+status+"&firstName="+firstName+"&lastName="+lastName+"&applicationNumber="+applicationNumber;
			var dataobj = {
				'fundCode':fundCode,
				'status':status,
				'firstName':firstName,
				'lastName':lastName,
				'applicationNumber':applicationNumber
			};
			$http.put(url, dataobj, {
				headers:{
					'Content-Type': 'application/json',
					'Authorization':fetchTokenNumSvc.getTokenId()
				}				
			}).then(function(res){
				object = res.data;
				defer.resolve(res);
			},function(err){
				console.log('Something went wrong while udpating status'+JSON.stringify(err));
				defer.reject(err);
			});
			return defer.promise;
		};
		return object;
	}]);
//   .factory('DecryptContent',['$http','$q',function($http,$q){
//	var object = {};
//	object.getDecryptContent = function(url,data,fundCode){
//		var defer = $q.defer();
//		//var dataURL = url+"?fundCode="+fundCode+"&status="+status+"&firstName="+firstName+"&lastName="+lastName+"&applicationNumber="+applicationNumber;
//		$http.post(url,data,{
//			headers : {
//				'Content-Type': 'application/json'
//			},
//			params : {
//				'fundcode': fundCode
//			}
//		}).then(function(res) {
//			factory = res.data;
//			defer.resolve(res);
//		}, function(err) {
//			console.log('Error while fetching..' + JSON.stringify(err));
//			defer.reject(err);
//		});
//		return defer.promise;
//	};
//	return object;
//}]);
