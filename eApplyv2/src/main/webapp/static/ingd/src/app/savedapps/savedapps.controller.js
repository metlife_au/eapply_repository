class savedAppsCtrl {
	constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog, $timeout, $q, moment) {
	    'ngInject';
	    
	    this._CommonAPI = CommonAPI;
	    this._EapplyData = EapplyData;
	    this.appName = AppConstants.appName;
	    this._$scope = $scope;
	    this._$state = $state;
	    this._ngDialog = ngDialog;
	    this._$timeout = $timeout;
	    this._MomentAPI = moment;
	    this._$q = $q;
   }

  init() {
	  
	  const _this = this;
	  
	  _this.urlList = this._CommonAPI.getUrlList();
	  
	  _this._CommonAPI.fetchAllSavedApps(_this.urlList.retrieveSavedAppUrl).then(
			  (res) => {				  
				  _this.savedApplications = res;
				  
				  console.log(_this.savedApplications.length);
				  
				  angular.forEach(_this.savedApplications, function (Object, index) {
					  
					  var tempDate = new Date(_this.savedApplications[index].createdDate);
					  _this.savedApplications[index].createdDate = _this._MomentAPI(tempDate).format('DD/MM/YYYY');
					  
					  if(_this.savedApplications[index].requestType === 'CCOVER'){
							_this.savedApplications[index].requestType = 'Change Cover';
						}else if(_this.savedApplications[index].requestType === 'TCOVER'){
							_this.savedApplications[index].requestType = 'Transfer Cover';
						}else if(_this.savedApplications[index].requestType === 'CANCOVER'){
							_this.savedApplications[index].requestType = 'Cancel Cover';
						}
					  
				  });
				  
			  },
			  (err) => {
		            _this.errors = err.data.errors;
		        }
	  );
  }
  
  goToSavedApp(app){
	  this._CommonAPI.setAppNumber(app.applicationNumber);
      this._EapplyData.retrieveApp(app);
  }
    
}

export default savedAppsCtrl;
