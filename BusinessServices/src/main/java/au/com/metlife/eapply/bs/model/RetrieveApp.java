/**
 * 
 */
package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 199306
 *
 */
public class RetrieveApp implements Serializable{
	
	private String applicationStatus;
	
	private String applicationNumber;
	
	private String requestType;
	
	private Date createdDate;
	
	private String lastSavedOnPage;

	public String getLastSavedOnPage() {
		return lastSavedOnPage;
	}

	public void setLastSavedOnPage(String lastSavedOnPage) {
		this.lastSavedOnPage = lastSavedOnPage;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	

}
