package au.com.metlife.eapply.bs.pdf.pdfObject;

public class SpecialConditions {
	
	private String familyDiscountTxt = null;
	private String multiCoverDiscountTxt = null;
	private String loadingSpecialCondTxt = null;
	private String exclusionSpecialCondTxt = null;
	private String freeKidsTxt = null;
	
	
	/**
	 * @return the exclusionSpecialCondTxt
	 */
	public String getExclusionSpecialCondTxt() {
		return exclusionSpecialCondTxt;
	}
	/**
	 * @param exclusionSpecialCondTxt the exclusionSpecialCondTxt to set
	 */
	public void setExclusionSpecialCondTxt(String exclusionSpecialCondTxt) {
		this.exclusionSpecialCondTxt = exclusionSpecialCondTxt;
	}
	/**
	 * @return the familyDiscountTxt
	 */
	public String getFamilyDiscountTxt() {
		return familyDiscountTxt;
	}
	/**
	 * @param familyDiscountTxt the familyDiscountTxt to set
	 */
	public void setFamilyDiscountTxt(String familyDiscountTxt) {
		this.familyDiscountTxt = familyDiscountTxt;
	}
	/**
	 * @return the loadingSpecialCondTxt
	 */
	public String getLoadingSpecialCondTxt() {
		return loadingSpecialCondTxt;
	}
	/**
	 * @param loadingSpecialCondTxt the loadingSpecialCondTxt to set
	 */
	public void setLoadingSpecialCondTxt(String loadingSpecialCondTxt) {
		this.loadingSpecialCondTxt = loadingSpecialCondTxt;
	}
	/**
	 * @return the multiCoverDiscountTxt
	 */
	public String getMultiCoverDiscountTxt() {
		return multiCoverDiscountTxt;
	}
	/**
	 * @param multiCoverDiscountTxt the multiCoverDiscountTxt to set
	 */
	public void setMultiCoverDiscountTxt(String multiCoverDiscountTxt) {
		this.multiCoverDiscountTxt = multiCoverDiscountTxt;
	}
	public String getFreeKidsTxt() {
		return freeKidsTxt;
	}
	public void setFreeKidsTxt(String freeKidsTxt) {
		this.freeKidsTxt = freeKidsTxt;
	}
	
	

}
