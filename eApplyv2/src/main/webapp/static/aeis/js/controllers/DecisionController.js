/*Change cover decision page controllers starts*/

AeisApp.controller('changeCoverAcceptController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', 'fetchPersoanlDetailSvc', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService, fetchPersoanlDetailSvc){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	$scope.userDetails = fetchPersoanlDetailSvc.getMemberDetails();
	
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};
  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

  /*NPS survey popup*/

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

    $scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	  DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('changeCoverDeclineController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	//$scope.appNum = PersistenceService.getAppNumber();

  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			  //  var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			  var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  			  	console.log(url)
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});
    
    $scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	  DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};

    /*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('\\')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  		DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

AeisApp.controller('changeCoverRUWController',['$scope', '$rootScope', '$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope, $rootScope, $location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
  $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				 var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

    $scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	  DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('changeCoverACCSpclTermsController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService','appData', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService,appData){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	var decisionPageDetails = appData.getChangeCoverAuraDetails();
	
	$scope.loading = (decisionPageDetails.deathLoading != null && decisionPageDetails.deathLoading != '0.0') || (decisionPageDetails.tpdLoading != null && decisionPageDetails.tpdLoading != 0.0) || (decisionPageDetails.ipLoading != null && decisionPageDetails.ipLoading != 0.0);
	$scope.exclusions = (decisionPageDetails.deathExclusions != null && decisionPageDetails.deathExclusions != '0.0') || (decisionPageDetails.tpdExclusions != null && decisionPageDetails.tpdExclusions != 0.0) || (decisionPageDetails.ipExclusions != null && decisionPageDetails.ipExclusions != 0.0);
	$scope.loading_Exclusions = $scope.loading && $scope.exclusions;
	
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });
    */
 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});
    
    
    $scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	  DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('changeCoverMixedACCController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // added for session expiry
   /* $timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			   // var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

    $scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

/*Change cover decision page controllers ends*/

/*Transfer cover decision page controllers Starts*/

AeisApp.controller('transferAcceptController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };


 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			   // var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  				 $window.open(url, '_blank');
  		       // $location.path(url);
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('transferDeclineController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	//$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };


 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			   // var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('\\')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  		DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

/*Transfer cover decision page controllers Ends*/

/*Work Rating cover decision page controllers Starts*/

AeisApp.controller('workRatingAcceptController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			   // var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

AeisApp.controller('workRatingDeclineController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	//$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			  //  var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('\\')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  		DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

AeisApp.controller('workRatingMaintainController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

/*Work Rating cover decision page controllers Ends*/

/*New Member cover decision page controllers starts*/

AeisApp.controller('newMemberAcceptController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
   	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('newMemberDeclineController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	//$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});
  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('\\')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  		DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

AeisApp.controller('cancelController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','persoanlDetailService','urlService',
                                            function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,persoanlDetailService,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    }



  	var inputDetails = persoanlDetailService.getMemberDetails();
    var clientRefNum = inputDetails.clientRefNumber;

    //npsTokenUrl = "http://c001.cloudrecording.com.au/cgi-bin/UR/ursurvey_pub_eapply.pl?f=1&token=oRbpWevWskI6gmePupLy"
  	//npsTokenUrl+"&fundCode="+getFundId()+"&custRefNumber="+applicantDTO.getClientRefNo()+"&TransactionCode="+applicationDTO.getApplicationNumber())
  	//http://c001.cloudrecording.com.au/cgi-bin/UR/ursurvey_pub_eapply.pl?f=1&token=oRbpWevWskI6gmePupLy&fundCode=HOST&custRefNumber=clientRefNum&TransactionCode=appNum


  	
  	// NPS survey popup

   ngDialog.openConfirm({
  		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
		className: 'ngdialog-theme-plain custom-width',
		plain: true,
  	}).then(function (value) {
  		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				 var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
  	},function(value){
  		if(value == 'oncancel'){
  			return false;
  		}
  	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

/*New Member cover decision page controllers starts*/
AeisApp.controller('cancelRuwController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };


 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				 var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  	       if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
  	           window.navigator.msSaveBlob(res.data.response,filename);
  	       }else{
  	    	var fileURL = URL.createObjectURL(res.data.response);
   	        a.href = fileURL;
   	        a.download = filename;
   	        a.click();
  	       }

  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);
AeisApp.controller('cancelDecisionDeclineController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };


 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				 var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  	       if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
  	           window.navigator.msSaveBlob(res.data.response,filename);
  	       }else{
  	    	var fileURL = URL.createObjectURL(res.data.response);
   	        a.href = fileURL;
   	        a.download = filename;
   	        a.click();
  	       }

  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);


AeisApp.controller('lifeEventAcceptController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
   	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('lifeEventDeclineController',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	//$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});
  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('\\')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  		DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);

AeisApp.controller('lifeEventRUWController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				 var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  	       if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
  	           window.navigator.msSaveBlob(res.data.response,filename);
  	       }else{
  	    	var fileURL = URL.createObjectURL(res.data.response);
   	        a.href = fileURL;
   	        a.download = filename;
   	        a.click();
  	       }

  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

/*special cover decision controllers*/

AeisApp.controller('specialOfferAcceptCtlr',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
   	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

  	$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);

AeisApp.controller('specialOfferDeclineCtlr',['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService',function($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

  	//$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // NPS survey popup

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  			    var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});
  	/*$scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('\\')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  		DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};*/
}]);
/*special cover decision controllers*/


AeisApp.controller('convertAcceptController',['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService', function($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService){
	$scope.urlList = urlService.getUrlList();
	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	$scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};
  	$scope.appNum = PersistenceService.getAppNumber();
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

  /*NPS survey popup*/

    ngDialog.openConfirm({
   		template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value=='onYes'){
  			if(npsTokenUrl != null){
  			    //var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
  				var url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+npsTokenUrl;
  		        $window.open(url, '_blank');
  		        return true;
  			}
  		}
   	},function(value){
   		if(value == 'oncancel'){
   			return false;
   		}
   	});

    $scope.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement("a");
  	    document.body.appendChild(a);
  	  DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log("Error downloading the PDF " + err);
  		});
  	};
}]);
