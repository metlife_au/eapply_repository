package pages.metlife.Angular.FirstSuper;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_InsuranceCost extends MasterPage {
	WebDriverUtil driverUtil = null;

	public FirstSuper_Angular_InsuranceCost InsuranceCost_FirstSuper() {
		InsuranceCost();
	
		
		return new FirstSuper_Angular_InsuranceCost(scriptHelper);
	}
	
		
	

	public FirstSuper_Angular_InsuranceCost(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void InsuranceCost() {

		String dateofbirth = dataTable.getData("InsuranceCost_Data", "DOB");
		String Gender = dataTable.getData("InsuranceCost_Data", "Gender");
		String FifteenPlusQuestion=dataTable.getData("InsuranceCost_Data", "Morethan_15HoursWork");
		String workindustry=dataTable.getData("InsuranceCost_Data", "WorkIndustry");
		String occupationtype=dataTable.getData("InsuranceCost_Data", "OccupationType");
		String OtherOccupation=dataTable.getData("InsuranceCost_Data", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("InsuranceCost_Data", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("InsuranceCost_Data", "TertiaryQual");
		String Eightypercenttimeinoffice = dataTable.getData("InsuranceCost_Data", "Eightypercenttimeinoffice");
		
		String annualsalary=dataTable.getData("InsuranceCost_Data", "AnnualSalary");
		String costfrequncy=dataTable.getData("InsuranceCost_Data", "CostFrequency");
		String costtype=dataTable.getData("InsuranceCost_Data", "CostType");
		String deathamount=dataTable.getData("InsuranceCost_Data", "DeathAmount");
		String DeathYN=dataTable.getData("InsuranceCost_Data", "DeathYN");
		String TPDYN=dataTable.getData("InsuranceCost_Data", "TPDYN");
		String IPYN=dataTable.getData("InsuranceCost_Data", "IPYN");
		String tpdamount=dataTable.getData("InsuranceCost_Data", "TPDAmount");
		String deathunit=dataTable.getData("InsuranceCost_Data", "DeathUnit");
		String tpdunit=dataTable.getData("InsuranceCost_Data", "TPDUnit");		
		String waitingperiod=dataTable.getData("InsuranceCost_Data", "WaitingPeriod");
		String benefitperiod=dataTable.getData("InsuranceCost_Data", "BenefitPeriod");
		String coverageunit=dataTable.getData("InsuranceCost_Data", "CoverageUnit");
		String expecteddeathcover=dataTable.getData("InsuranceCost_Data", "ExpectedDeathCover");
		String expectedtpdcover=dataTable.getData("InsuranceCost_Data", "ExpectedTPDCover");
		String expectedipcover=dataTable.getData("InsuranceCost_Data", "ExpectedIpCover");
		
		
		
		try{
			
			
			
			//****Click on Transfer Your Cover Link*****\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[@class='color3 mt15px']"))).click();
			report.updateTestLog("Insurance Cost", "Clicked on Insurance Cost Link", Status.PASS);
			

			 sleep(1500);			
			 ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());		     
			 driver.switchTo().window(tabs.get(0));
			 driver.close();
			 driver.switchTo().window(tabs.get(1));			    
			 sleep(3000);
			 
			//****Enter the DOB*****\\
			 
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dob_1"))).clear();
			sleep(500);
			driver.findElement(By.id("dob_1")).sendKeys(dateofbirth);
			report.updateTestLog("Date Of Birth", dateofbirth+" is entered", Status.PASS);
			
			//****Select the Gender*****\\
			
			if(Gender.equalsIgnoreCase("Male")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("genderid:_0"))).click();
				report.updateTestLog("Gender", "Male is Selected", Status.PASS);	
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("genderid:_1"))).click();
				report.updateTestLog("Gender", "Female is Selected", Status.PASS);
			}
			 
		   //*****Do you work 15 or more hours per week?******\\
			
			if(FifteenPlusQuestion.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("ingFifteenRadio:_0"))).click();
				report.updateTestLog("Do you work 15 or more hours a week?", "Yes is Selected", Status.PASS);	
			}
			
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("ingFifteenRadio:_1"))).click();
				report.updateTestLog("Do you work 15 or more hours a week?", "No is Selected", Status.PASS);	
			}
			
			//*****What Industry do you work in*****\\
			
			 sleep(500);
			    Select Industry=new Select(driver.findElement(By.id("industryListId")));
			    Industry.selectByVisibleText(workindustry);
			    sleep(1000);
				report.updateTestLog("Industry", "Industry Selected is: "+workindustry, Status.PASS);
				driver.findElement(By.xpath("//label[text()='What is your current occupation?']")).click();
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='What is your current occupation?']"))).click();
				sleep(250);
			
           //*****What is your occupation?*****\\
			
				Select Occupation=new Select(driver.findElement(By.id("industryOccupListId")));
				Occupation.selectByVisibleText(occupationtype);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+occupationtype, Status.PASS);
				sleep(500);
			
			//*****Other Occupation *****\\	
			
			if(occupationtype.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}		
			
			
			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio2:_0"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio2:_1"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
			sleep(800);						
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio4:_0"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio4:_1"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}							
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
									
			//******Do you spend at least 80% of all working time in an office*****\\
			if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("spendTimeOutRadio:_0"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("spendTimeOutRadio:_1"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
			}
			sleep(500);							
			 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
					
					//***Select if your office Duties are Undertaken within an Office Environment?\\	
					    	
					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
					   
					    	wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio2:_0"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
												
					}							
					else{							

						wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio2:_1"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
												
					}							
						sleep(800);						
					      //*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){							
					       
					        	wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio4:_0"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   wait.until(ExpectedConditions.elementToBeClickable(By.id("workDutiesRadio4:_1"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}							
					}							
													
						
		}							
								    
          sleep(800);
			
			
			
        //*****What is your annual Salary******							
			
			
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("custAnualSal"))).sendKeys(annualsalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+annualsalary, Status.PASS);	
			
		//*****Display the cost of my insurance cover as****\\
		   
		    sleep(500);
		    Select costfre=new Select(driver.findElement(By.id("premiumPeriodId")));
		    costfre.selectByVisibleText(costfrequncy);
		    sleep(500);
		    report.updateTestLog("Insurance Cost Frequency", costfrequncy+" is Selected", Status.PASS);
		    sleep(200);
		   
		  //*****Select the Cost Type*****\\
		    if(DeathYN.equalsIgnoreCase("Yes")){
		    if(costtype.equalsIgnoreCase("Fixed")){
		    	
		    	  wait.until(ExpectedConditions.elementToBeClickable(By.id("addnlUnitFixedRadio:_1"))).click();
		    	  sleep(800);
				  report.updateTestLog("Cost Type", costtype+" is Selected", Status.PASS);
				  sleep(300);
				  
				  //****Enter the Death Amount*****\\
				  
				  wait.until(ExpectedConditions.elementToBeClickable(By.id("dcAddnlCvrIpTxt"))).sendKeys(deathamount);
		    	  sleep(1000);
		    	 // driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[1]")).click();
		    	 // sleep(1000);
				  report.updateTestLog("Death Cover Amount", deathamount+" is entered", Status.PASS);
				  sleep(200);
				  
               //****Enter the TPD Amount*****\\
				  
				 
				  wait.until(ExpectedConditions.elementToBeClickable(By.id("dc_tpdAddnlCvrIpTxt"))).sendKeys(tpdamount);
		    	  sleep(1000);
		    	 // driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[2]")).click();
		    	 // sleep(1000);
				  report.updateTestLog("TPD Cover Amount", tpdamount+" is entered", Status.PASS);
				  sleep(200);
				  
				  
		    }
		    
		    else{
		    	  wait.until(ExpectedConditions.elementToBeClickable(By.id("addnlUnitFixedRadio:_0"))).click();
		    	  sleep(800);
				   report.updateTestLog("Cost Type", costtype+" is Selected", Status.PASS);
				   sleep(300);
				   
					  //****Enter the Death Units*****\\
					  
					  wait.until(ExpectedConditions.elementToBeClickable(By.id("dcAddnlCvrIpTxt"))).sendKeys(deathunit);
			    	  sleep(1000);
			    	 // driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[1]")).click();
			    	 // sleep(1000);
					  report.updateTestLog("Death Cover Units", deathunit+" units is entered", Status.PASS);
					  sleep(200);
					  
	               //****Enter the TPD Units*****\\
					  
					 
					  wait.until(ExpectedConditions.elementToBeClickable(By.id("dc_tpdAddnlCvrIpTxt"))).sendKeys(tpdunit);
			    	  sleep(1000);
			    	 // driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[2]")).click();
			    	 // sleep(1000);
					  report.updateTestLog("TPD Cover Units", tpdunit+" units is entered", Status.PASS);
					  sleep(200);
					  
		    }
		    }
		    
		    
		    
		    //*****Enter the IP Cover Details*******\
		    sleep(800);
		  if(IPYN.equalsIgnoreCase("Yes")){
		   
		    //******Select the Waiting Period******\\
		    sleep(1000);
		    Select Waitingperiod=new Select(driver.findElement(By.id("ipWaitingPeriodLst")));
		    Waitingperiod.selectByVisibleText(waitingperiod);
		    sleep(500);
		    report.updateTestLog("IP Waiting Period",waitingperiod+"  is Selected", Status.PASS);
		    
		    //*****Select the Benefit Period******\\
		    sleep(800);
		    Select Benefitperiod=new Select(driver.findElement(By.id("ipBenefitPeriodLst")));
		    Benefitperiod.selectByVisibleText(benefitperiod);
		    sleep(500);
		    report.updateTestLog("IP Waiting Period",benefitperiod+"  is Selected", Status.PASS);
		    	
		    //*****Select the IP Cover Amount Required*****\\
		    
		    
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("ip_addnl_cover"))).sendKeys(coverageunit);
		    sleep(1000);	
		   // driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[3]")).click();
		    //sleep(1000);	
		    report.updateTestLog("IP Cover Amount",coverageunit+"  is entered", Status.PASS);
		  }
		    sleep(1000);
		   
		    //******Click on Calculate Quote*******\\
		    
		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calculate_quote']/span"))).click();
		    sleep(400);
		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
            sleep(3000);
            
          
          if(driver.findElement(By.xpath("//*[@id='calculate_quote']/span")).isDisplayed()){
        	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calculate_quote']/span"))).click();
  		    sleep(400);
  		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
           sleep(3000);
          }
          
          if(driver.findElement(By.xpath("//*[@id='calculate_quote']/span")).isDisplayed()){
        	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calculate_quote']/span"))).click();
  		    sleep(400);
  		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
           sleep(3000);
          }
          
      	
			///****Fetch the Death cover and TPD cover amounts****\\\
				
				
         
				sleep(500);
				
				if(DeathYN.equalsIgnoreCase("Yes")){
	            WebElement DeathCover=driver.findElement(By.id("dcCostDivId"));
	           
	            sleep(500);
				String DeathCoveramount=DeathCover.getText();
				
				
				if(DeathCoveramount.equalsIgnoreCase(expecteddeathcover)){
					
					System.out.println("Death cover is as expected ");
					System.out.println("Death cover amount displayed "+DeathCoveramount);
					System.out.println("Death cover amount expected "+expecteddeathcover);
					report.updateTestLog("Death Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expecteddeathcover+" Actual Premium: "+DeathCoveramount, Status.PASS);
				}
				else{
					
					System.out.println("Death cover is not as expected ");
					System.out.println("Death cover amount displayed "+DeathCoveramount);
					System.out.println("Death cover amount expected "+expecteddeathcover);
					report.updateTestLog("Death Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expecteddeathcover+" Actual Premium: "+DeathCoveramount, Status.FAIL);
				}
				}
				
				sleep(1000);
				if(TPDYN.equalsIgnoreCase("Yes")){
				
				WebElement TPDCover=driver.findElement(By.id("tpdCostDivId"));
			
				sleep(500);
				String TPDCoveramount=TPDCover.getText();
				if(TPDCoveramount.equalsIgnoreCase(expectedtpdcover)){
					System.out.println("TPD cover is as expected ");
					System.out.println("TPD cover amount displayed "+TPDCoveramount);
					System.out.println("TPD cover amount expected "+expectedtpdcover);
					report.updateTestLog("TPD Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedtpdcover+" Actual Premium: "+TPDCoveramount, Status.PASS);
					
				}
				else{
					System.out.println("TPD cover is not as expected ");
					System.out.println("TPD cover amount displayed "+TPDCoveramount);
					System.out.println("TPD cover amount expected "+expectedtpdcover);
					report.updateTestLog("TPD Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedtpdcover+" Actual Premium: "+TPDCoveramount, Status.FAIL);
				}
				}
				 sleep(1000);
				if(IPYN.equalsIgnoreCase("Yes")){
				WebElement IPCover=driver.findElement(By.id("ipCostDivId"));
				
				sleep(500);
				String IpCoverAmount=IPCover.getText();
				
				if(IpCoverAmount.equalsIgnoreCase(expectedipcover)){
					System.out.println("Ip cover is as expected ");
					System.out.println("Ip cover amount displayed "+IpCoverAmount);
					System.out.println("Ip cover amount expected "+expectedipcover);
					report.updateTestLog("IP Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedipcover+" Actual Premium: "+IpCoverAmount, Status.PASS);
					
				}
				else{
					System.out.println("Ip cover is not as expected ");
					System.out.println("Ip cover amount displayed "+IpCoverAmount);
					System.out.println("Ip cover amount expected "+expectedipcover);
					report.updateTestLog("IP Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedipcover+" Actual Premium: "+IpCoverAmount, Status.FAIL);
				} 
				}
          sleep(1000);


			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}


}
