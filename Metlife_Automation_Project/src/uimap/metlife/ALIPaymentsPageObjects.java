package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIPaymentsPageObjects extends PageObjectLibrary{
		
	public static final By rddebit = getObjectIdentifaction("ALI.payment.bank.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("ALI.payment.bank.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("ALI.payment.bank.accountnumber.text.id");
	public static final By txtbsbcode = getObjectIdentifaction("ALI.payment.bank.bsbcode.text.id");	
	public static final By txtbsbnumber = getObjectIdentifaction("ALI.payment.bank.bsbnumber.text.id");	
	public static final By chkacknowledge = getObjectIdentifaction("ALI.payment.agree.chkbox.css");
	public static final By btnsubmit = getObjectIdentifaction("ALI.payment.submit.btn.css");
}
