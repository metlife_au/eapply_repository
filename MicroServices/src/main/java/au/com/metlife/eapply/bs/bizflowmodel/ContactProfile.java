package au.com.metlife.eapply.bs.bizflowmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactProfile")
@XmlRootElement
public class ContactProfile {
	
	private List<AddressRoleList> AddressRoleList;
	private List<PhoneRoleList> PhoneRoleList;
	private List<ElectronicAddressRoleList> ElectronicAddressRoleList;

	public List<ElectronicAddressRoleList> getElectronicAddressRoleList() {
		return ElectronicAddressRoleList;
	}

	public void setElectronicAddressRoleList(
			List<ElectronicAddressRoleList> electronicAddressRoleList) {
		ElectronicAddressRoleList = electronicAddressRoleList;
	}

	public List<PhoneRoleList> getPhoneRoleList() {
		return PhoneRoleList;
	}

	public void setPhoneRoleList(List<PhoneRoleList> phoneRoleList) {
		PhoneRoleList = phoneRoleList;
	}

	public List<AddressRoleList> getAddressRoleList() {
		return AddressRoleList;
	}

	public void setAddressRoleList(List<AddressRoleList> addressRoleList) {
		AddressRoleList = addressRoleList;
	}
	
	
	
	
	
	
}
