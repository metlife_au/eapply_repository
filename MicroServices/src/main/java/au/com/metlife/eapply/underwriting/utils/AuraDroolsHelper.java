package au.com.metlife.eapply.underwriting.utils;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.underwriting.model.PostProcessRuleBO;
import au.com.metlife.eapply.underwriting.model.RefProductAuraQuestionFilterBO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuraDroolsHelper {
	private AuraDroolsHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(AuraDroolsHelper.class);
	
	
	public static RefProductAuraQuestionFilterBO invokeRuleProductAuraQuestMapping (RefProductAuraQuestionFilterBO refProdAuraQustFilter, String ruleFileLocation) {
		log.info("Invoke rule product aura question mapping start");
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		String ruleFileLoc = ruleFileLocation;
		try {
			ruleFileLoc=ruleFileLoc+refProdAuraQustFilter.getRuleFileName();
			
			if (null != refProdAuraQustFilter.getKbase()) {
				knowledgeBase = refProdAuraQustFilter.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase(ruleFileLoc);
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(refProdAuraQustFilter);
		} catch (Exception e) {
			log.error("Error in invoke rule Product aura question mapping {}",e);
		}
		log.info("Invoke rule product aura question mapping finish");
		return refProdAuraQustFilter;
	}
	
	public static PostProcessRuleBO invokepostProcessRule (PostProcessRuleBO postProcessRuleBO, String ruleFileLocation) {
		log.info("Invoke post process rule start");
		KnowledgeBase knowledgeBase = null;
		String ruleFileLoc = ruleFileLocation;
		StatelessKnowledgeSession knowledgeSession = null;
		try {
			ruleFileLoc=ruleFileLoc+postProcessRuleBO.getRuleFileName();
			
			if (null != postProcessRuleBO.getKbase()) {
				knowledgeBase = postProcessRuleBO.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase(ruleFileLoc);
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(postProcessRuleBO);
		} catch (Exception e) {
			log.error("Error in invoke post process rule {}",e);
		}
		log.info("Invoke post process rule finish");
		return postProcessRuleBO;
	}
	
	public static InstProductRuleBO invokeInstProductBusinessRule(InstProductRuleBO instProductRuleBO, String ruleFileLocation){
		log.info("Invoke Inst product business rule start");
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		String ruleFileLoc = ruleFileLocation;
		try {
			ruleFileLoc=ruleFileLoc+instProductRuleBO.getRuleFileName();
			if(instProductRuleBO.getKbase()!=null){
				kbase=instProductRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLoc);
				ksession = kbase.newStatelessKnowledgeSession();
			}
				ksession.execute(instProductRuleBO);
		} catch (Exception e) {
			log.error("Error in invoke inst product business rule: {}",e);
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		log.info("Invoke Inst product business rule finish");
		return instProductRuleBO;
	}
	
	public static KnowledgeBase getRuleSession(String ruleFileLoc,String ruleFileName){
		log.info("Get rule session start");
		KnowledgeBase kbase=null;
		String ruleFileLocation = ruleFileLoc;
		try {
			ruleFileLocation=ruleFileLocation+ruleFileName;
			kbase = readKnowledgeBase(ruleFileLocation);
		} catch (Exception e) {
			log.error("Error in get rule session: {}",e);
		}
		log.info("Get rule session finish");
		return kbase;
	}

	/**
	 * This method will read the rules from the sample.xls file.
	 * Each row in the excel sheet will represent as a business rule.
	 *
	 * @return KnowledgeBase object
	 * @throws Exception
	 */

	private static KnowledgeBase readKnowledgeBase(String ruleFileLoc) {
		log.info("Read knowledge base start");
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		config.setInputType(DecisionTableInputType.XLS);
		kbuilder.add(ResourceFactory.newFileResource(ruleFileLoc), ResourceType.DTABLE, config);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		/*if (errors.size() > 0) {*/
		if (!(errors.isEmpty())) {
			for (KnowledgeBuilderError error: errors) {
				log.error("Error in read knowledge base {}",error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		log.info("Read knowledge base finish");
		return kbase;
	}

}
