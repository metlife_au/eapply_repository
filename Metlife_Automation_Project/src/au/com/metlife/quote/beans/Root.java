package au.com.metlife.quote.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Root", propOrder = {
                "code", 
    "description",
    "agentsearch"
})
public class Root {
                
                @XmlElement(required = true)
                String code;
                @XmlElement(required = true)
                String description;
                @XmlElement(required = true)
                AgentSearch agentsearch;
                
                public AgentSearch getAgentsearch() {
                                return agentsearch;
                }
                public void setAgentsearch(AgentSearch agentsearch) {
                                this.agentsearch = agentsearch;
                }
                public String getCode() {
                                return code;
                }
                public void setCode(String code) {
                                this.code = code;
                }
                public String getDescription() {
                                return description;
                }
                public void setDescription(String description) {
                                this.description = description;
                }

}
