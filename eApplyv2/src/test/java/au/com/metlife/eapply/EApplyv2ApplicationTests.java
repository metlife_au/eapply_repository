package au.com.metlife.eapply;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.SecureRandom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import au.com.metlife.eapply.utility.AESencrp;
import au.com.metlife.eapply.utility.RSAUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EApplyv2ApplicationTests {
	private static final Logger log = LoggerFactory.getLogger(EApplyv2ApplicationTests.class);
	@Test
	public void contextLoads() {
	}
	
	/*@Test
	public void testRestCalculateAll1() {

		String input = "<request><adminPartnerID>CARE</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>CARE</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>CARE</partnerID><applicant><dateJoined>01/01/2011</dateJoined><welcomeLetterDate>01/01/2010</welcomeLetterDate><clientRefNumber>7</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>test</firstName><lastName>testB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>";
		URL obj;
		try {
			obj = new URL("http://localhost:8080/eapply/apply");
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			httpCon.setRequestProperty("Content-Type", "application/json");
			httpCon.setRequestProperty("Accept", "application/json");
			httpCon.setRequestProperty("InputData", "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRRNE5qUXlPVFF6TUN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJelpXUmlOell3WWkwMVlqUmpMVFF3WkdVdE9XVTVNeTB6WlRNMU5tUTVaR1ZoTURjaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS4xS3NGcHpFY25ZcFJUZWI3SmRyVjJsemtRdXNFZlR1YWh2N2huY29ndnIw");
			httpCon.setRequestMethod("GET");
			httpCon.connect();
			OutputStream os = httpCon.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			osw.write(input);
			osw.flush();
			osw.close();

			int responseCode = httpCon.getResponseCode();
			log.info("Response Code : " + responseCode);
			assertEquals(200, httpCon.getResponseCode());
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String inputLine;
			StringBuffer responseBuff = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				log.info(inputLine);
				responseBuff.append(inputLine);
			}
			in.close();

		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		} catch (ProtocolException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}*/
	
	@Test
	public void testRestCalculateAll2() {
         log.info("Test rest calculate all start");
		String input = "<request><adminPartnerID>CARE</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>CARE</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>CARE</partnerID><applicant><dateJoined>01/01/2011</dateJoined><welcomeLetterDate>01/01/2010</welcomeLetterDate><clientRefNumber>7</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>test</firstName><lastName>testB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>";
		URL obj;
		try {
			obj = new URL("http://localhost:8080/eapply/b2bservice");
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			httpCon.setRequestProperty("Content-Type", "application/json");
			httpCon.setRequestProperty("Accept", "application/json");
			httpCon.setRequestMethod("POST");
			httpCon.connect();
			OutputStream os = httpCon.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			osw.write(input);
			osw.flush();
			osw.close();

			int responseCode = httpCon.getResponseCode();
			log.info("Response Code : " + responseCode);
			assertEquals(200, httpCon.getResponseCode());
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String inputLine;
			StringBuffer responseBuff = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				log.info(inputLine);
				responseBuff.append(inputLine);
			}
			in.close();

		} catch (MalformedURLException e) {
			log.error("Error in malformed url"+e);
		} catch (ProtocolException e) {
			log.error("Error in protocol"+e);
		} catch (IOException e) {
			log.error("Error in input output"+e);
		}
		log.info("Test rest calculate all finish");
	}
	@Test
	public void testRestB2bService() {
		log.info("Test rest b2b service start");

		String input = "<request><adminPartnerID>IFSP</adminPartnerID><transRefGUID>73bbfa52-797d-43bd-85db-05f596d209e7</transRefGUID><transType>2</transType><transDate>24/11/2017</transDate><transTime>10:19:09</transTime><fundID>MTAA</fundID><fundEmail>Multifund_insurance@aas.com.au</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>MTAA</partnerID><applicant><dateJoined>18/10/2017</dateJoined><workCommencementDate>18/10/2017</workCommencementDate><firstContributionDate>30/11/2017</firstContributionDate><clientRefNumber>312658452</clientRefNumber><memberType>Employer Sponsored</memberType><applicantRole>1</applicantRole><personalDetails><title>MRS</title><firstName>Tina</firstName><lastName>Smith</lastName><dateOfBirth>19/12/1975</dateOfBirth><gender>Female</gender><priority>2</priority><smoker>2</smoker><applicantSubType /></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><amount>3000.00</amount>			<units>12</units><loading /><exclusions>Y</exclusions><waitingPeriod>90D</waitingPeriod><benefitPeriod>5YEARS</benefitPeriod><indexation>2</indexation></cover></existingCovers><address><addressType>2</addressType><line1>2 Park St</line1><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>AUSTRALIA</country></address><contactDetails><emailAddress>pkotian@metlife.com</emailAddress><mobilePhone>0412345678</mobilePhone></contactDetails></applicant></policy></request>";
		URL obj;
		String aesKey= null;
		try {
			/* generate AESkey*/
			
			SecureRandom sRandom = new SecureRandom();
			aesKey=  new BigInteger(80, sRandom).toString(32);	   
			
			log.info(aesKey);
			/*encrypt input data using AES key*/
			String encryptedDate = AESencrp.encrypt(input, aesKey);
			log.info("encryptedDate>>"+encryptedDate);
			/*encrypt AES key*/
			String encryptedAesToken =RSAUtil.encryptData(aesKey,"MTAA");
			log.info("encryptedAesToken>>"+encryptedAesToken);
			obj = new URL("http://localhost:8080/eapply/genericb2bservice");
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			httpCon.setRequestProperty("Content-Type", "application/json");
			httpCon.setRequestProperty("Accept", "application/json");			
			httpCon.setRequestProperty("Authorization", "Basic "+encryptedAesToken);
			httpCon.setRequestProperty("fundid", "MTAA");
			httpCon.setRequestMethod("POST");
			httpCon.connect();
			OutputStream os = httpCon.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			osw.write(encryptedDate);
			osw.flush();
			osw.close();

			int responseCode = httpCon.getResponseCode();
			System.out.println(responseCode);
			assertEquals(200, httpCon.getResponseCode());
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String inputLine;
			StringBuffer responseBuff = new StringBuffer();
			System.out.println("output>>"+in.readLine());
			while ((inputLine = in.readLine()) != null) {
				log.info(inputLine);
				responseBuff.append(inputLine);
			}
			in.close();

		} catch (MalformedURLException e) {
			log.error("Error in malformed url"+e);
		} catch (ProtocolException e) {
			log.error("Error protocol"+e);
		} catch (IOException e) {
			log.error("Error in input and output"+e);
		} catch (Exception e) {
			log.error("Error in rest b2b service"+e);
		}
		log.info("Test rest b2b service finish");
	}
	
}
