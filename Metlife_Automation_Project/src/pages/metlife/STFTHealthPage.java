package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.PersonalStatementPageObjects;
import uimap.metlife.STFTHealthPageObjects;

public class STFTHealthPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public STFTHealthPage yourHealth() {
		yourHealthDetails();
		return new STFTHealthPage(scriptHelper);
	}

	public STFTHealthPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}
	
	private void fillInLifeStyleDetails() {
		
		String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
		String strAlcoholConsu = dataTable.getData("MetLife_Data", "Alchohol.Reduce");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strAbtHIV = dataTable.getData("MetLife_Data", "personal.HIV");
		
		try{
			
			List<WebElement> radLifeStyleDetailsNo = null;
			radLifeStyleDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radSTFTLifeStyleDeatilsNo);
			List<WebElement> radLifeStyleDetailsYes = null;
			radLifeStyleDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radSTFTLifeStyleDeatilsYes);
	
			System.out.println("Test Life Style Questions ...");

			
			tryAction(strDrugs.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(0)):fluentWaitElement(radLifeStyleDetailsNo.get(0)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?-"+strDrugs);
			
/*			System.out.println("Test..."+radLifeStyleDetailsNo.get(0).getAttribute("name"));
			System.out.println("Test..."+radLifeStyleDetailsNo.get(1).getAttribute("name"));
			System.out.println("Test..."+radLifeStyleDetailsNo.get(2).getAttribute("name"));*/
			sleep(10000);
			//tryAction(strDrugs.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(1)):fluentWaitElement(radLifeStyleDetailsNo.get(1)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you? - "+strDrugs);
			tryAction(strAlcoholConsu.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(1)):fluentWaitElement(radLifeStyleDetailsNo.get(1)),"clkradio","Have you ever been advised by a health professional to reduce your alcohol consumption? - "+strAlcoholConsu);
			sleep(10000);
			tryAction(strAbtHIV.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(2)):fluentWaitElement(radLifeStyleDetailsNo.get(2)),"clkradio","Do you have HIV (Human Immunodeficiency Virus) that causes AIDS? - "+strAbtHIV);
			sleep();
			if(strAbtHIV.equalsIgnoreCase("no")){
				String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetailsYes.get(3).getAttribute("name")+"_1_0'][value='No']";
				//Click On No for HIV
				tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
			}
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.chkSportsNota),"Click","Do you regularly engage in or intend to engage in any of the following hazardous activities?");
			sleep();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtalchohol),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			sleep();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnAlcoholicEnter),"CLICK","Enter");
			
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}

	public void yourHealthDetails() {

		String height = dataTable.getData("MetLife_Data", "Height");
		String weight = dataTable.getData("MetLife_Data", "Weight");
		// exclusion
		String diagnosed = dataTable.getData("MetLife_Data", "Diagnosed");
		String Threeyears = dataTable.getData("MetLife_Data", "3yrs");
		String familyHistory = dataTable.getData("MetLife_Data", "FamilyHistory");
		String lifeStyle = dataTable.getData("MetLife_Data", "LifeStyleQuestions");
		
		String alchohol = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
		String alchoholReduce = dataTable.getData("MetLife_Data", "Alchohol.Reduce");
		String hiv = dataTable.getData("MetLife_Data", "personal.HIV");
		String hivRisk = dataTable.getData("MetLife_Data", "HIV.Risk");
		
		String general = dataTable.getData("MetLife_Data", "General.Questions");
		String insuranceExcl = dataTable.getData("MetLife_Data", "Exclusion");
		String insuranceClaim = dataTable.getData("MetLife_Data", "Claim");
		String strGender = dataTable.getData("MetLife_Data", "Gender");
		String strPregnent = dataTable.getData("MetLife_Data", "pregnant");
		
		

		try {
			tryAction(fluentWaitElement(PersonalStatementPageObjects.txtHeight), "SET", "Height", height);
			tryAction(fluentWaitElement(PersonalStatementPageObjects.txtHeight), "TAB", "Height");

			tryAction(fluentWaitElement(PersonalStatementPageObjects.txtWeight), "SET", "Weight", weight);
			tryAction(fluentWaitElement(PersonalStatementPageObjects.txtWeight), "TAB", "Weight");
			sleep(2000);
			if (Threeyears.equalsIgnoreCase("None")) {
				tryAction(fluentWaitElement(PersonalStatementPageObjects.notaLungSTFt), "Click",
						"Have you suffered,diagnosed in last 3 years - None");
				// loading
			} else if (Threeyears.equalsIgnoreCase("Lung")) {

				tryAction(waitForClickableElement(STFTHealthPageObjects.chklast3yrslungissue), "Click",
						"Have you suffered from, diagnosed with/sought medical advice - Lung or breathing condition");
				Thread.sleep(200);
				tryAction(waitForClickableElement(STFTHealthPageObjects.chklast3yrslungissueasthma), "Click",
						"Have you suffered from, diagnosed with/sought medical advice - Asthma");
				Thread.sleep(200);
				tryAction(fluentWaitElement(STFTHealthPageObjects.rdasthmaModerate), "clkradio",
						"Is your condition: Moderate");
				Thread.sleep(200);
				tryAction(fluentWaitElement(STFTHealthPageObjects.rdasthmaworsenedNo), "clkradio",
						"Is your asthma worsened by your occupation");

			}else{
				tryAction(fluentWaitElement(PersonalStatementPageObjects.notaLungSTFt), "Click",
						"Selected Default as datasheet is not matched with any condition - None Of The Above");
			}
			sleep(2000);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chklast5yrs), "Click",
					"Have you suffered,diagnosed in last 5 years - None");
			System.out.println("Here 5 "+diagnosed);

			sleep(1000);
			if (diagnosed.equalsIgnoreCase("None")) {
				sleep(2000);
				System.out.println("Hi");
				tryAction(waitForClickableElement(PersonalStatementPageObjects.chkmedicaladviceNota), "Click",
						"Have you suffered from, diagnosed with/sought medical advice - None");
				
			} else if (diagnosed.equalsIgnoreCase("Bone")) {

				tryAction(waitForClickableElement(STFTHealthPageObjects.chkmedicaladvicebone), "Click",
						"Have you suffered from, diagnosed with/sought medical advice - Bone, joint or limp");
				tryAction(waitForClickableElement(STFTHealthPageObjects.chkmedicaladvicelossbone), "Click",
						"Have you suffered from, diagnosed with/sought medical advice - Loss");
				tryAction(fluentWaitElement(STFTHealthPageObjects.rdadvicepractitionerYes), "clkradio",
						"Following  advice of regular practitioner");
				tryAction(fluentWaitElement(STFTHealthPageObjects.rdfracturesNo), "clkradio",
						"Have you suffered any fractures");
			}
			sleep();
			if(strGender.equalsIgnoreCase("female")){
				
				tryAction(strPregnent.equalsIgnoreCase("yes")?fluentWaitElement(PersonalStatementPageObjects.rsdFemaleYes):fluentWaitElement(PersonalStatementPageObjects.rsdFemaleNo), "clkradio","Are You Pregnent? - "+strPregnent);
				sleep(200);
				if (familyHistory.equalsIgnoreCase("No")) {
					tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamilyHistoryNo), "clkradio", "Family History");
				} else {
					tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamilyHistoryNo), "clkradio", "Family History");
				}
				sleep(200);
				if (lifeStyle.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdlifestyleNo), "clkradio", "LifeStyle");
				} else {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdlifestyleNo), "clkradio", "LifeStyle");
				}
				sleep(200);
				tryAction(waitForClickableElement(PersonalStatementPageObjects.txtalchohol), "SET", "AlchoholQuantity", alchohol);
				tryAction(waitForClickableElement(PersonalStatementPageObjects.txtalchohol), "TAB", "AlchoholQuantity");
				sleep(200);
				if (alchoholReduce.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdalchoholreduceNo), "clkradio", "AlchoholReduce");
				} else {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdalchoholreduceNo), "clkradio", "AlchoholReduce");
				}
				sleep(200);
				if (hiv.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdhivNo), "clkradio", "HIV");
				} else {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdhivYes), "clkradio", "HIV");
				}
				sleep(200);
				if (hivRisk.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdhivriskNo), "clkradio", "HIVRisk");
				}
				sleep(200);
				if (general.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdgeneralNo), "clkradio", "General");
				} else {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdgeneralNo), "clkradio", "General");
				}
				sleep(200);
				if (insuranceExcl.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdinsuranceexclusionNo), "clkradio",
							"Insurance Exclusion");
				} else {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdinsuranceexclusionNo), "clkradio",
							"Insurance Exclusion");
				}
				sleep(200);
				if (insuranceClaim.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdinsuranceclaimNo), "clkradio", "Insurance Details");
				} else {
					tryAction(fluentWaitElement(PersonalStatementPageObjects.rdinsuranceclaimNo), "clkradio", "Insurance Details");
				}
				
			} else {
				
				String strfamilyHistory = "input[value*='[11082]No']+*";
				String strlifeStyle = "input[id*='radio1-1-9:0']";
				String stralchoholReduce = "input[id*='radio1-1-11:0']";
				String strhivNo = "input[id*='radio1-1-12:0']";
				String strhivYes = "input[id*='radio1-0-12:0']";
				String strhivRisk = "input[id*='radio1-1-12_1_0:0']";
				String strgeneral = "input[id*='radio1-1-13:0']";
				String strinsuranceExcl ="input[id*='radio1-1-14:0']";
				String strinsuranceClaim ="input[id*='radio1-1-15:0']";
				
				tryAction(waitForClickableElement(By.cssSelector(strfamilyHistory)), "clkradio", "Family History");
				tryAction(fluentWaitElement(By.cssSelector(strlifeStyle)), "clkradio", "LifeStyle");
				tryAction(fluentWaitElement(By.cssSelector(stralchoholReduce)), "clkradio", "AlchoholReduce");
				
				tryAction(waitForClickableElement(PersonalStatementPageObjects.txtalchohol), "SET", "AlchoholQuantity", alchohol);
				tryAction(waitForClickableElement(PersonalStatementPageObjects.txtalchohol), "TAB", "AlchoholQuantity");

				if (hiv.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(By.cssSelector(strhivNo)), "clkradio", "HIV");
				} else {
					tryAction(fluentWaitElement(By.cssSelector(strhivYes)), "clkradio", "HIV");
				}
				
				if (hivRisk.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(By.cssSelector(strhivRisk)), "clkradio", "HIVRisk");
				}
				tryAction(fluentWaitElement(By.cssSelector(strgeneral)), "clkradio", "General");
				tryAction(fluentWaitElement(By.cssSelector(strinsuranceExcl)), "clkradio",
						"Insurance Exclusion");
				tryAction(fluentWaitElement(By.cssSelector(strinsuranceClaim)), "clkradio", "Insurance Details");
				//tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
				//String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetailsYes.get(3).getAttribute("name")+"_1_0'][value='No']";
			}						
			
			sleep(3000);
			
			//fillInLifeStyleDetails();
			
			//fillInLifeStyleDetails();
			//sleep(10000);			
	

			tryAction(fluentWaitElement(PersonalStatementPageObjects.btnNext), "MoveToElm", "Next");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnNext), "Click", "Next");
			
			/*
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(PersonalStatementPageObjects.btnNext)).click().perform();*/

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}
}
