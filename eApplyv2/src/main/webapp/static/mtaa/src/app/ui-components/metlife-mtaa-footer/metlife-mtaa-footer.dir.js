(function (angular) {
  'use strict';

  angular
    .module('Metlife.mtaa.uicomponents', ['mtaa.ui.constants'])
    .directive('metlifeMtaaFooter', metlifeMtaaFooter);

  function metlifeMtaaFooter(mtaaUIConstants) {
    var directive = {
      restrict: 'EA',
      templateUrl: mtaaUIConstants.path.uiComponents + 'metlife-mtaa-footer/metlife-mtaa-footer.tmpl.html',
      controller: footerCtrl,
      controllerAs: 'footer',
      bindToController: true
    };

    return directive;
  }

  footerCtrl.$inject = ['$scope', 'mtaaUIConstants'];

  function footerCtrl($scope, mtaaUIConstants) {
    var footer = this;
    footer.constants = {
      privacyUrl: mtaaUIConstants.navUrls.privacyUrl
    };
  }

})(angular);