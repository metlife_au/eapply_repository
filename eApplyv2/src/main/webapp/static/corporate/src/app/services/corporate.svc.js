angular.module('restAPI',[]);
angular.module('restAPI')
  .service('fetchUrlSvc',['$http','$q', 'fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc) {
    var _this = this;
    _this.headerConfig = {
       headers : {
		 'Content-Type': 'application/json',
		 'Authorization':fetchTokenNumSvc.getTokenId()
       }
     };

    _this.getUrls = function(path) {
      var defer = $q.defer();
      $http.get(path, _this.headerConfig).then(function(res) {
	       _this.setUrlList(res.data);
	       defer.resolve(res);
      }, function(err){
	       console.log('Error fetching urls' + JSON.stringify(err));
	       defer.reject(err);
      });
      return defer.promise;
    };

    _this.setUrlList = function(list) {
    	_this.urlList = list;
    };

    _this.getUrlList = function(){
    	return _this.urlList;
    };
 }]);
angular.module('CorporateApp').factory('urlService',['$http','$q', '$sessionstorage', 'fetchUrlSvc', 'fetchTokenNumSvc', function($http,$q, $sessionstorage, fetchUrlSvc, fetchTokenNumSvc){
    var headerConfig = {
        headers : {
			'Content-Type': 'application/json',
			'Authorization':fetchTokenNumSvc.getTokenId()
        }
    };
    var urlObject = {};
    urlObject.getUrls = function(){
        var defer = $q.defer();
        $http.get('CareSuperUrls.properties',headerConfig).then(function(res){
            urlObject = res.data;
            defer.resolve(res);
        }, function(err){
            console.log('Error fetching urls' + JSON.stringify(err));
            defer.reject(err);
        });
        return defer.promise;
    };
    urlObject.setUrlList = function(list){
        $sessionstorage.setItem('urlList',JSON.stringify(list));
    };
    urlObject.getUrlList = function(){
        return fetchUrlSvc.getUrlList();
    };
    return urlObject;
}])
.factory('beforeUnload', function ($rootScope, $window) {
    // Events are broadcast outside the Scope Lifecycle
    
    $window.onbeforeunload = function (e) {
        var confirmation = {};
        var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
        if (event.defaultPrevented) {
            return confirmation.message;
        }
    };
    
    $window.onunload = function () {
        $rootScope.$broadcast('onUnload');
    };
    return {};
});

angular.module('CorporateApp').factory('corporateFund',['$http','$q', 'fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc){
	var factory = {};
	factory.add = function(url, data, userId, fundCode){
		var defer = $q.defer();
		$http.post(url,data,{
			headers : {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
			params : {
				'userId': userId,
				'fundCode': fundCode
			}
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while saving..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	
	factory.update = function(url, data, userId, fundCode){
		var defer = $q.defer();
		$http.put(url,data,{
			headers : {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
			params : {
				'userId': userId,
				'fundCode': fundCode
			}
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while updating..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	
	factory.getDetails = function(url,fundName, fundCode){
		var defer = $q.defer();
		$http.get(url,{
			headers : {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
			params : {
				'fundName': fundName,
				'fundCode': fundCode
			}
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while fetching..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	
	factory.getDecryptContent = function(url, data,fundCode){
		var defer = $q.defer();
		$http.post(url,data,{
			headers : {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
			params : {
				'fundcode': fundCode
			}
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while fetching..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return factory;
}]);
