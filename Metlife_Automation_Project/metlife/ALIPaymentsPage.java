package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.ALIPaymentsPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class ALIPaymentsPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public ALIPaymentsPage yourPayment() {
		paymentDetails();
		return new ALIPaymentsPage(scriptHelper);
	}

	public ALIPaymentsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void paymentDetails() {

	
		String aacountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsbcode = dataTable.getData("MetLife_Data", "BSBCode");
		String bsbnumber = dataTable.getData("MetLife_Data", "BSBNumber");
		
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			tryAction(fluentWaitElement(ALIPaymentsPageObjects.rddebit), "clkradio", "Debit");
			Thread.sleep(20000);
			
			
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtaccountname), "SET", "AccountName", aacountName);
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtbsbcode), "SET", "BSB Code", bsbcode);
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtbsbcode), "TAB", "BSB Code");
			
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtbsbnumber), "SET", "BSB Number", bsbnumber);
			tryAction(waitForClickableElement(ALIPaymentsPageObjects.txtbsbnumber), "TAB", "BSB Number");

			tryAction(waitForClickableElement(ALIPaymentsPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Thread.sleep(500);
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ALIPaymentsPageObjects.btnsubmit)).click().perform();
			Thread.sleep(1000);	
			
			switchToActiveWindow();
			driver.manage().window().maximize();	
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
