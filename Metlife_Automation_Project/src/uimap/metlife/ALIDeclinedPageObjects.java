package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIDeclinedPageObjects extends PageObjectLibrary{
		
	public static final By btnfinish = getObjectIdentifaction("ALI.declined.finish.btn.css");
}
