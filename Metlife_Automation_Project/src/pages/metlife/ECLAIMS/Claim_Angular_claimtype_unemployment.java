package pages.metlife.ECLAIMS;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ScriptHelper;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class Claim_Angular_claimtype_unemployment extends MasterPage {
	WebDriverUtil driverUtil = null;

	public Claim_Angular_claimtype_unemployment Eclaims_involuntaryunemployment() {
		Eclaims_unemployment();
		Eclaims_Reviewclaims_unemployment();
		return new Claim_Angular_claimtype_unemployment(scriptHelper);
	}
	
	public Claim_Angular_claimtype_unemployment Eclaims_involuntaryunemploymentnegativetc() {
				Eclaims_unemployment_Negativescenario();
		return new Claim_Angular_claimtype_unemployment(scriptHelper);
	}

	public Claim_Angular_claimtype_unemployment(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void Eclaims_unemployment() {
		
		String Creditcard = dataTable.getData("Eclaims_unemployment", "Creditcard");
		String Title = dataTable.getData("Eclaims_unemployment", "Title");
		String DOB = dataTable.getData("Eclaims_unemployment", "DOB");
		String Name = dataTable.getData("Eclaims_unemployment", "Name");
		String Surname = dataTable.getData("Eclaims_unemployment", "Surname");
		String Address = dataTable.getData("Eclaims_unemployment", "Address1");
		String Addresss = dataTable.getData("Eclaims_unemployment", "Address2");
		String suburb = dataTable.getData("Eclaims_unemployment", "suburb");
		String state = dataTable.getData("Eclaims_unemployment", "State");
		String postcode = dataTable.getData("Eclaims_unemployment", "Postcode");
		String PreferredPhoneNO = dataTable.getData("Eclaims_unemployment", "PreferredPhoneNO");
		String Otherphoneno = dataTable.getData("Eclaims_unemployment", "Otherphoneno");
		String Email = dataTable.getData("Eclaims_unemployment", "Email");
		String Preferredmode = dataTable.getData("Eclaims_unemployment", "Preferredmode");
		String Contacttime = dataTable.getData("Eclaims_unemployment", "Contacttime");
		String Deathcertificate = dataTable.getData("Eclaims_unemployment", "Deathcertificate");
		String Attachpath = dataTable.getData("Eclaims_unemployment", "Attachpath");
		String Lastdayofwork = dataTable.getData("Eclaims_unemployment", "Lastdayofwork");
		String Lastdaydate = dataTable.getData("Eclaims_unemployment", "Lastdaydate");
		String dateofreturn = dataTable.getData("Eclaims_unemployment", "dateofreturn");
		String Employername = dataTable.getData("Eclaims_unemployment", "Employername");
		String Dateofcommencement = dataTable.getData("Eclaims_unemployment", "Dateofcommencement");
		String Employeraddress = dataTable.getData("Eclaims_unemployment", "Employeraddress");
		String Employercontactno = dataTable.getData("Eclaims_unemployment", "Employercontactno");
		String unemploymentReason = dataTable.getData("Eclaims_unemployment", "unemploymentReason");
		String Datenotified = dataTable.getData("Eclaims_unemployment", "Datenotified");
		String employment = dataTable.getData("Eclaims_unemployment", "employment");
		String benefitregistered = dataTable.getData("Eclaims_unemployment", "benefitregistered");
		String benefitcommenced = dataTable.getData("Eclaims_unemployment", "benefitcommenced");
		String Worked = dataTable.getData("Eclaims_unemployment", "Worked");
		String returntowork = dataTable.getData("Eclaims_unemployment", "returntowork");
		String dateofreturntowork = dataTable.getData("Eclaims_unemployment", "dateofreturntowork");
		String Evidence = dataTable.getData("Eclaims_unemployment", "Evidence");
		String Postcode = dataTable.getData("Eclaims_unemployment", "Postcode");

		try {

			WebDriverWait wait = new WebDriverWait(driver, 18);

			// ****Click on claim type*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-INVOUNEMPY']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Claim Type :", "Involuntary Unemployment is Selected", Status.PASS);

			// ****Enter creditcard number*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
			report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",
					Creditcard + " is entered", Status.PASS);

			// **********************\\
			// ****Insured Details*****\\
			// **************************\\

			// ****Select title*****\\

			Select titletype = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
			titletype.selectByVisibleText(Title);
			report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

			// ****Enter DOB*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
					.sendKeys(DOB);
			report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

			// ****Enter Name*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
					.sendKeys(Name);
			report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

			// ****Enter surname*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
					.sendKeys(Surname);
			report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

			// ****Enter Address1*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
					.sendKeys(Address);
			report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

			// ****Enter Address2*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
					.sendKeys(Addresss);
			report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

			// ****Enter Suburb*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
					.sendKeys(suburb);
			report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

			// ****Select State*****\\

			Select Stateselect = new Select(
					driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
			Stateselect.selectByVisibleText(state);
			report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
			sleep(500);

			// ****Enter Postal code*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
					.sendKeys(Postcode);
			sleep(500);
			report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
			sleep(300);

			// ****Enter Emailaddress*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ213_0")))
					.sendKeys(Email);
			report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

			// ****Enter Peferred phone number*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ211_1")))
					.sendKeys(PreferredPhoneNO);
			sleep(500);
			report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
			sleep(500);

			// ****Enter other phone number*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ210_2")))
					.sendKeys(Otherphoneno);
			sleep(500);
			report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
			sleep(500);

			JavascriptExecutor j = (JavascriptExecutor) driver;
			j.executeScript("window.scrollBy(0,200)", "");
			sleep(900);

			// ****Preferred mode of contact (Phone / Email / Letter)?*****\\

			if (Preferredmode.equalsIgnoreCase("Phone")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

				Select mode = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ215_1")));
				mode.selectByVisibleText(Contacttime);
				report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
						Contacttime + " is selected ", Status.PASS);
				sleep(500);
			}
			if (Preferredmode.equalsIgnoreCase("Email")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			}
			if (Preferredmode.equalsIgnoreCase("Letter")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			}

			JavascriptExecutor jj = (JavascriptExecutor) driver;
			jj.executeScript("window.scrollBy(0,200)", "");
			sleep(900);

			// ****Proceed button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary w100p']")))
					.click();
			sleep(500);
			report.updateTestLog("Proceed Button", "clicked", Status.PASS);

			// ***Additional Details*****\\

			// ***Employment Details*****\\

			// *****Were you continuously employed for at least 180 days prior
			// to your last day of work? ***//

			if (Lastdayofwork.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Employed for at least 180 days ", "Yes is selected", Status.PASS);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Employed for at least 180 days ", "No is selected", Status.PASS);
			}

			// ****Last day of work*******\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10031_0")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10031_0")))
					.sendKeys(Lastdaydate);
			sleep(500);
			report.updateTestLog("Last day of work: ", Lastdaydate + " is entered", Status.PASS);

			// ****Date of return to work (if known) ********\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10032_1")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10032_1")))
					.sendKeys(dateofreturn);
			sleep(500);
			report.updateTestLog("Date of return to work (if known):", dateofreturn + " is entered", Status.PASS);

			JavascriptExecutor na = (JavascriptExecutor) driver;
			na.executeScript("window.scrollBy(0,600)", "");
			sleep(900);

			// ****Employer name*******\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ703_0")))
					.sendKeys(Employername);
			sleep(500);
			report.updateTestLog("Enter Employer name:", Employername + " is entered", Status.PASS);

			// ****Date of commencement of employment ********\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ718_1")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ718_1")))
					.sendKeys(Dateofcommencement);
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ703_0")))
					.click();
			report.updateTestLog("Enter Date of commencement of employment:", Dateofcommencement + " is entered",
					Status.PASS);

			// ****Employer address ********\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ704_0")))
					.sendKeys(Employeraddress);
			sleep(500);
			report.updateTestLog("Enter Employer address : ", Employeraddress + " is entered", Status.PASS);

			// ****Employer contact number*******\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ705_1")))
					.sendKeys(Employercontactno);
			sleep(500);
			report.updateTestLog("Employer contact number :", Employercontactno + " is entered", Status.PASS);

			JavascriptExecutor nb = (JavascriptExecutor) driver;
			nb.executeScript("window.scrollBy(0,600)", "");
			sleep(900);

			// ****Reason for unemployment*******\\

			Select unemploy = new Select(driver.findElement(By.name("vm.additionalDetailsForm_custom-select_CQ706_0")));
			unemploy.selectByVisibleText(unemploymentReason);
			report.updateTestLog("Enter unemploymentReason :", unemploymentReason + " is selected ", Status.PASS);

			// ****Date notified of unemployment *******\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10039_1")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10039_1")))
					.sendKeys(Datenotified);
			sleep(500);
			report.updateTestLog("Date notified of unemployment :", Datenotified + " is entered", Status.PASS);

			// ***Are you actively seeking employment? ***//

			if (employment.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='Yes']/parent::*)[2]")))
						.click();
				sleep(500);
				report.updateTestLog("Are you actively seeking employment? ", "Yes is Selected", Status.PASS);

				// ******Date registered for Centrelink benefits ***//

				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10041_0")))
						.sendKeys(benefitregistered);
				sleep(500);
				report.updateTestLog("Enter Date registered for Centrelink benefits:",
						benefitregistered + " is entered", Status.PASS);

				// ******Date Centrelink Benefits commenced ***//

				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10042_1")))
						.sendKeys(benefitcommenced);
				sleep(500);
				report.updateTestLog("Enter Date Centrelink Benefits commenced:", benefitcommenced + " is entered",
						Status.PASS);

			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='No']/parent::*)[2]")))
						.click();
				sleep(500);
				report.updateTestLog("Are you actively seeking employment? ", "No is Selected", Status.PASS);
			}

			JavascriptExecutor nc = (JavascriptExecutor) driver;
			nc.executeScript("window.scrollBy(0,600)", "");
			sleep(900);

			// ******Have you worked in any capacity since the commencement of
			// Unemployment?*****//

			if (Worked.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='Yes']/parent::*)[3]")))
						.click();
				sleep(500);
				report.updateTestLog("Worked in any capacity since the commencement of Unemployment?:",
						"Yes is Selected", Status.PASS);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='No']/parent::*)[3]")))
						.click();
				sleep(500);
				report.updateTestLog("Worked in any capacity since the commencement of Unemployment?:",
						"No is Selected", Status.PASS);
			}

			// ********Do you expect to return to work within the next three
			// months?******//

			if (returntowork.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='Yes']/parent::*)[4]")))
						.click();
				sleep(500);
				report.updateTestLog("Do you expect to return to work within the next three months?:",
						"Yes is Selected", Status.PASS);
				sleep(300);

				// ***When do you expect to return to work?***//
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ9129_0")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ9129_0")))
						.sendKeys(dateofreturntowork);
				sleep(500);
				report.updateTestLog("Enter When do you expect to return to work?:", dateofreturntowork + " is entered",
						Status.PASS);
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[text()='When do you expect to return to work? ']")))
						.click();

			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='No']/parent::*)[4]")))
						.click();
				sleep(500);
				report.updateTestLog("Do you expect to return to work within the next three months?:", "No is Selected",
						Status.PASS);
			}
			JavascriptExecutor nd = (JavascriptExecutor) driver;
			nd.executeScript("window.scrollBy(0,300)", "");
			sleep(900);

			// *******Required Documentation******\\

			// ****Attachpath******\\

			if (Deathcertificate.equalsIgnoreCase("Yes")) {

				// ****Separation Certificate****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[1]")))
						.sendKeys(Attachpath);
				report.updateTestLog("Separation Certificate", "File uploaded successfully", Status.PASS);
				sleep(1300);
				JavascriptExecutor ng = (JavascriptExecutor) driver;
				ng.executeScript("window.scrollBy(0,600)", "");
				sleep(900);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
						.click();
				sleep(500);
				report.updateTestLog("Attached file button", "Attach Button is Selected", Status.PASS);

				// ****Letter of Registration with an Australian Government
				// Approved Recruitment Agency****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[2]")))
						.sendKeys(Attachpath);
				report.updateTestLog("Letter of Registration", "File uploaded successfully", Status.PASS);
				sleep(1300);
				JavascriptExecutor ne = (JavascriptExecutor) driver;
				ne.executeScript("window.scrollBy(0,600)", "");
				sleep(900);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
						.click();
				sleep(500);
				report.updateTestLog("Attached file button", "File uploaded successfully", Status.PASS);

				// ****Evidence Return to Work (Optional)****\\

				if (Evidence.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[3]")))
							.sendKeys(Attachpath);
					report.updateTestLog("Evidence Return to Work", "File uploaded successfully", Status.PASS);
					sleep(1300);
					JavascriptExecutor nf = (JavascriptExecutor) driver;
					nf.executeScript("window.scrollBy(0,600)", "");
					sleep(900);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
							.click();
					sleep(500);
					report.updateTestLog("Attached file button", "Attach Button is Selected", Status.PASS);
				}
				// ****Newstart Certification / Job search evidence****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[4]")))
						.sendKeys(Attachpath);
				report.updateTestLog("Newstart Certification / Job search evidence", "File uploaded successfully",
						Status.PASS);
				sleep(1300);
				JavascriptExecutor nj = (JavascriptExecutor) driver;
				nj.executeScript("window.scrollBy(0,600)", "");
				sleep(900);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
						.click();
				sleep(500);
				report.updateTestLog("Attached file button", "Attach Button is Selected", Status.PASS);

				// ****Other****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[5]")))
						.sendKeys(Attachpath);
				report.updateTestLog("Other documents", "File uploaded successfully", Status.PASS);
				sleep(1300);
				JavascriptExecutor nk = (JavascriptExecutor) driver;
				nk.executeScript("window.scrollBy(0,600)", "");
				sleep(900);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
						.click();
				sleep(500);
				report.updateTestLog("Attached file button", "Attach Button is Selected", Status.PASS);

			}

			JavascriptExecutor ne = (JavascriptExecutor) driver;
			ne.executeScript("window.scrollBy(0,900)", "");
			sleep(900);

			// ***Agree button**//

			wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
			sleep(500);
			report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);

			// ****Next button******\\

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12']/button[@type='submit']")))
					.click();
			sleep(500);
			report.updateTestLog("Acknowledgement checkbox", "Checkbox is clicked", Status.PASS);
			sleep(500);

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	public void Eclaims_Reviewclaims_unemployment() {

		String ClaimType = dataTable.getData("Eclaims_unemployment", "ClaimType");
		String Creditcard = dataTable.getData("Eclaims_unemployment", "Creditcard");
		String Title = dataTable.getData("Eclaims_unemployment", "Title");
		String DOB = dataTable.getData("Eclaims_unemployment", "DOB");
		String Name = dataTable.getData("Eclaims_unemployment", "Name");
		String Surname = dataTable.getData("Eclaims_unemployment", "Surname");
		String Address = dataTable.getData("Eclaims_unemployment", "Address1");
		String Addresss = dataTable.getData("Eclaims_unemployment", "Address2");
		String suburb = dataTable.getData("Eclaims_unemployment", "suburb");
		String state = dataTable.getData("Eclaims_unemployment", "State");
		String postcode = dataTable.getData("Eclaims_unemployment", "Postcode");
		String PreferredPhoneNO = dataTable.getData("Eclaims_unemployment", "PreferredPhoneNO");		
		String Email = dataTable.getData("Eclaims_unemployment", "Email");
		String Preferredmode = dataTable.getData("Eclaims_unemployment", "Preferredmode");
		String Contacttime = dataTable.getData("Eclaims_unemployment", "Contacttime");
		String Deathcertificate = dataTable.getData("Eclaims_unemployment", "Deathcertificate");		
		String Lastdayofwork = dataTable.getData("Eclaims_unemployment", "Lastdayofwork");
		String Lastdaydate = dataTable.getData("Eclaims_unemployment", "Lastdaydate");
		String dateofreturn = dataTable.getData("Eclaims_unemployment", "dateofreturn");
		String Employername = dataTable.getData("Eclaims_unemployment", "Employername");
		String Dateofcommencement = dataTable.getData("Eclaims_unemployment", "Dateofcommencement");
		String Employeraddress = dataTable.getData("Eclaims_unemployment", "Employeraddress");
		String Employercontactno = dataTable.getData("Eclaims_unemployment", "Employercontactno");
		String unemploymentReason = dataTable.getData("Eclaims_unemployment", "unemploymentReason");
		String Datenotified = dataTable.getData("Eclaims_unemployment", "Datenotified");
		String employment = dataTable.getData("Eclaims_unemployment", "employment");
		String benefitregistered = dataTable.getData("Eclaims_unemployment", "benefitregistered");
		String benefitcommenced = dataTable.getData("Eclaims_unemployment", "benefitcommenced");
		String Worked = dataTable.getData("Eclaims_unemployment", "Worked");
		String returntowork = dataTable.getData("Eclaims_unemployment", "returntowork");
		String dateofreturntowork = dataTable.getData("Eclaims_unemployment", "dateofreturntowork");		
		String Producttype = dataTable.getData("Eclaims_unemployment", "Producttype");
		String path = dataTable.getData("Eclaims_unemployment", "path");

		WebElement product = driver.findElement(By.xpath("//label[@class='ng-binding']"));
		WebElement claimtype = driver.findElement(By.xpath("//label[text()='Claim Type ']/following-sibling::label"));
		WebElement creditcard = driver.findElement(By.xpath(
				"//label[text()='Credit card / facility no. (Please enter the last 4 digits) ']/following-sibling::label"));
		WebElement title = driver.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[1]"));
		WebElement Dob = driver
				.findElement(By.xpath("//label[text()='Date of birth (dd/mm/yyyy) ']/following-sibling::label"));
		WebElement GivenName = driver
				.findElement(By.xpath("(//label[text()='Given name ']/following-sibling::label)[1]"));
		WebElement SurName = driver.findElement(By.xpath("(//label[text()='Surname ']/following-sibling::label)[1]"));
		WebElement Address1 = driver.findElement(By.xpath("(//label[text()='Address1 ']/following-sibling::label)[1]"));
		WebElement Address2 = driver
				.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[1]"));
		WebElement Suburb = driver.findElement(By.xpath("(//label[text()='Suburb ']/following-sibling::label)[1]"));
		WebElement State = driver.findElement(By.xpath("(//label[text()='State ']/following-sibling::label)[1]"));
		WebElement postcodes = driver
				.findElement(By.xpath("(//label[text()='Post Code ']/following-sibling::label)[1]"));
		WebElement Preferredphoneno = driver
				.findElement(By.xpath("//label[text()='Preferred phone no ']/following-sibling::label"));
		WebElement Emailaddress = driver
				.findElement(By.xpath("//label[text()='Email address ']/following-sibling::label"));

		String product_validation = product.getText();
		String claimtype_validation = claimtype.getText();
		String creditcard_validation = creditcard.getText();
		String title_validation = title.getText();
		String Dob_validation = Dob.getText();
		String GivenName_validation = GivenName.getText();
		String SurName_validation = SurName.getText();
		String Address1_validation = Address1.getText();
		String Address2_validation = Address2.getText();
		String Suburb_validation = Suburb.getText();
		String State_validation = State.getText();
		String postcode_validation = postcodes.getText();
		String Preferredphoneno_validation = Preferredphoneno.getText();
		String Emailaddress_validation = Emailaddress.getText();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 18);

			if (Producttype.contains(product_validation)) {

				report.updateTestLog("Product validation:", Producttype + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Product validation:", Producttype + " is not displayed as expected", Status.FAIL);
			}

			// ****Validate claim type*****\\
			if (ClaimType.contains(claimtype_validation)) {

				report.updateTestLog("Claim Type validation:", ClaimType + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Claim Type validation:", ClaimType + " is not displayed as expected",
						Status.FAIL);
			}

			JavascriptExecutor a = (JavascriptExecutor) driver;
			a.executeScript("window.scrollBy(0,100)", "");
			sleep(500);

			// ****Enter creditcard number*****\\

			if (Creditcard.contains(creditcard_validation)) {

				report.updateTestLog("Credit card validation:", Creditcard + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Credit card validation:", Creditcard + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Select title*****\\
			if (Title.contains(title_validation)) {

				report.updateTestLog("Title validation:", Title + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Title validation:", Title + " is not displayed as expected", Status.FAIL);
			}
			JavascriptExecutor b = (JavascriptExecutor) driver;
			b.executeScript("window.scrollBy(0,100)", "");
			sleep(500);

			// ****Enter DOB*****\\

			if (DOB.contains(Dob_validation)) {

				report.updateTestLog("DOB validation:", DOB + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("DOB validation:", DOB + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Name*****\\

			if (Name.contains(GivenName_validation)) {

				report.updateTestLog("Name validation:", Name + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Name validation:", Name + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter surname*****\\

			if (Surname.contains(SurName_validation)) {

				report.updateTestLog("Surname validation:", Surname + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Surname validation:", Surname + " is not displayed as expected", Status.FAIL);
			}

			JavascriptExecutor c = (JavascriptExecutor) driver;
			c.executeScript("window.scrollBy(0,150)", "");
			sleep(500);
			// ****Enter Address1*****\\

			if (Address.contains(Address1_validation)) {

				report.updateTestLog("Address 1 validation:", Address + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Address 1 validation:", Address + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Address2*****\\

			if (Addresss.contains(Address2_validation)) {

				report.updateTestLog("Address 2 validation:", Addresss + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Address 2 validation:", Addresss + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Suburb*****\\

			if (suburb.contains(Suburb_validation)) {

				report.updateTestLog("Suburb validation:", suburb + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Suburb validation:", suburb + " is not displayed as expected", Status.FAIL);
			}

			// ****Select State*****\\

			if (state.contains(State_validation)) {

				report.updateTestLog("State validation:", state + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("State validation:", state + " is not displayed as expected", Status.FAIL);
			}
			JavascriptExecutor d = (JavascriptExecutor) driver;
			d.executeScript("window.scrollBy(0,150)", "");
			sleep(500);

			// ****Enter Postal code*****\\

			if (postcode.contains(postcode_validation)) {

				report.updateTestLog("Post code validation:", postcode + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Post code validation:", postcode + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Emailaddress*****\\

			if (Email.contains(Emailaddress_validation)) {

				report.updateTestLog("Email validation:", Email + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Email validation:", Email + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Peferred phone number*****\\

			if (PreferredPhoneNO.contains(Preferredphoneno_validation)) {

				report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO + " is not displayed as expected",
						Status.FAIL);
			}

			WebElement Modeofcontact = driver.findElement(By.xpath(
					"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
			String Modeofcontact_validation = Modeofcontact.getText();

			// ****Preferred mode of contact (Phone / Email / Letter)?*****\\

			if (Preferredmode.equalsIgnoreCase("Phone")) {

				if (Preferredmode.contains(Modeofcontact_validation)) {

					report.updateTestLog("Preferred Mode of Contact validation:",
							Preferredmode + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Preferred Mode of Contact validation:",
							Preferredmode + " is not displayed as expected", Status.FAIL);
				}
				WebElement timeofcontact = driver.findElement(
						By.xpath("//label[text()='Preferred contact time: AM / PM ']/following-sibling::label"));
				String timeofcontact_validation = timeofcontact.getText();
				if (Contacttime.contains(timeofcontact_validation)) {

					report.updateTestLog("Time of contact validation :", Contacttime + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Time of contact validation :", Contacttime + " is not displayed as expected",
							Status.FAIL);
				}

			}
			if (Preferredmode.equalsIgnoreCase("Email")) {

				if (Preferredmode.contains(Modeofcontact_validation)) {

					report.updateTestLog("Email validation:", Preferredmode + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Email validation:", Preferredmode + " is not displayed as expected",
							Status.FAIL);
				}

			} else {
				if (Preferredmode.contains(Modeofcontact_validation)) {

					report.updateTestLog("Letter validation :", Preferredmode + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Letter validation :", Preferredmode + " is not displayed as expected",
							Status.FAIL);
				}

			}

			JavascriptExecutor e = (JavascriptExecutor) driver;
			e.executeScript("window.scrollBy(0,800)", "");
			sleep(500);

			// ***Additional Details*****\\

			// ***Employment Details*****\\

			// *****Were you continuously employed for at least 180 days prior
			// to your last day of work? ***//

			WebElement Lastdayofworkval = driver.findElement(By.xpath(
					"//label[text()='Were you continuously employed for at least 180 days prior to your last day of work? ']/following-sibling::label"));
			String Lastdayofwork_validation = Lastdayofworkval.getText();
			if (Lastdayofwork.equalsIgnoreCase("Yes")) {

				if (Lastdayofwork.contains(Lastdayofwork_validation)) {

					report.updateTestLog("Employed for at least 180 days validation :",
							Lastdayofwork + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Employed for at least 180 days validation :",
							Lastdayofwork + " is not displayed as expected", Status.FAIL);
				}

			} else {
				if (Lastdayofwork.contains(Lastdayofwork_validation)) {

					report.updateTestLog("Employed for at least 180 days validation :",
							Lastdayofwork + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Employed for at least 180 days validation :",
							Lastdayofwork + " is not displayed as expected", Status.FAIL);
				}
			}

			// ****Last day of work*******\\
			WebElement Lastdaydateval = driver
					.findElement(By.xpath("//label[text()='Last day of work ']/following-sibling::label"));
			String Lastdaydate_validation = Lastdaydateval.getText();
			if (Lastdaydate.contains(Lastdaydate_validation)) {

				report.updateTestLog("Last day of work validation :", Lastdaydate + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Last day of work validation :", Lastdaydate + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Date of return to work (if known) ********\\

			WebElement dateofreturnval = driver.findElement(
					By.xpath("//label[text()='Date of return to work (if known) ']/following-sibling::label"));
			String dateofreturn_validation = dateofreturnval.getText();
			if (dateofreturn.contains(dateofreturn_validation)) {

				report.updateTestLog("Date of return to work (if known) validation :",
						dateofreturn + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Date of return to work (if known) validation :",
						dateofreturn + " is not displayed as expected", Status.FAIL);
			}

			JavascriptExecutor na = (JavascriptExecutor) driver;
			na.executeScript("window.scrollBy(0,600)", "");
			sleep(900);

			// ****Employer name*******\\

			WebElement Employernameval = driver
					.findElement(By.xpath("//label[text()='Employer name ']/following-sibling::label"));
			String Employername_validation = Employernameval.getText();
			if (Employername.contains(Employername_validation)) {

				report.updateTestLog("Employer name validation :", Employername + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Employer name validation :", Employername + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Date of commencement of employment ********\\

			WebElement Dateofcommencementval = driver.findElement(
					By.xpath("//label[text()='Date of commencement of employment ']/following-sibling::label"));
			String Dateofcommencement_validation = Dateofcommencementval.getText();
			if (Dateofcommencement.contains(Dateofcommencement_validation)) {

				report.updateTestLog("Date of commencement of employment validation :",
						Dateofcommencement + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Date of commencement of employment validation :",
						Dateofcommencement + " is not displayed as expected", Status.FAIL);
			}

			// ****Employer address ********\\

			WebElement Employeraddressval = driver
					.findElement(By.xpath("//label[text()='Employer address ']/following-sibling::label"));
			String Employeraddress_validation = Employeraddressval.getText();
			if (Employeraddress.contains(Employeraddress_validation)) {

				report.updateTestLog("Employer address validation :", Employeraddress + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Employer address validation :", Employeraddress + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Employer contact number*******\\

			WebElement Employercontactnoval = driver
					.findElement(By.xpath("//label[text()='Employer contact number ']/following-sibling::label"));
			String Employercontactno_validation = Employercontactnoval.getText();
			if (Employercontactno.contains(Employercontactno_validation)) {

				report.updateTestLog("Employer address validation :", Employercontactno + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Employer address validation :",
						Employercontactno + " is not displayed as expected", Status.FAIL);
			}

			JavascriptExecutor nb = (JavascriptExecutor) driver;
			nb.executeScript("window.scrollBy(0,600)", "");
			sleep(900);

			// ****Reason for unemployment*******\\

			WebElement unemploymentReasonval = driver
					.findElement(By.xpath("//label[text()='Reason for unemployment ']/following-sibling::label"));
			String unemploymentReason_validation = unemploymentReasonval.getText();
			if (unemploymentReason.contains(unemploymentReason_validation)) {

				report.updateTestLog("unemploymentReason validation :",
						unemploymentReason + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("unemploymentReason validation :",
						unemploymentReason + " is not displayed as expected", Status.FAIL);
			}

			// ****Date notified of unemployment *******\\

			WebElement Datenotifiedval = driver
					.findElement(By.xpath("//label[text()='Date notified of unemployment ']/following-sibling::label"));
			String Datenotified_validation = Datenotifiedval.getText();
			if (Datenotified.contains(Datenotified_validation)) {

				report.updateTestLog("Date notified of unemployment validation :",
						Datenotified + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Date notified of unemployment validation :",
						Datenotified + " is not displayed as expected", Status.FAIL);
			}

			// ***Are you actively seeking employment? ***//

			if (employment.equalsIgnoreCase("Yes")) {
				WebElement employmentval = driver.findElement(
						By.xpath("//label[text()='Are you actively seeking employment? ']/following-sibling::label"));
				String employment_validation = employmentval.getText();
				if (employment.contains(employment_validation)) {

					report.updateTestLog("Are you actively seeking employment Y/N validation :",
							employment + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Are you actively seeking employment Y/N validation :",
							employment + " is not displayed as expected", Status.FAIL);
				}

				// ******Date registered for Centrelink benefits ***//

				WebElement benefitregisteredvald = driver.findElement(By
						.xpath("//label[text()='Date registered for Centrelink benefits ']/following-sibling::label"));
				String benefitregistered_validation = benefitregisteredvald.getText();
				if (benefitregistered.contains(benefitregistered_validation)) {

					report.updateTestLog("Date registered for Centrelink benefits validation :",
							benefitregistered + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Date registered for Centrelink benefits validation :",
							benefitregistered + " is not displayed as expected", Status.FAIL);
				}

				// ******Date Centrelink Benefits commenced ***//

				WebElement benefitcommencedval = driver.findElement(
						By.xpath("//label[text()='Date Centrelink Benefits commenced ']/following-sibling::label"));
				String benefitcommenced_validation = benefitcommencedval.getText();
				if (benefitcommenced.contains(benefitcommenced_validation)) {

					report.updateTestLog("Date Centrelink Benefits commenced validation :",
							benefitcommenced + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Date Centrelink Benefits commenced validation :",
							benefitcommenced + " is not displayed as expected", Status.FAIL);
				}

			} else {
				WebElement employmentval = driver.findElement(
						By.xpath("//label[text()='Are you actively seeking employment? ']/following-sibling::label"));
				String employment_validation = employmentval.getText();
				if (employment.contains(employment_validation)) {

					report.updateTestLog("Are you actively seeking employment Y/N validation :",
							employment + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Are you actively seeking employment Y/N validation :",
							employment + " is not displayed as expected", Status.FAIL);
				}
			}

			JavascriptExecutor nc = (JavascriptExecutor) driver;
			nc.executeScript("window.scrollBy(0,600)", "");
			sleep(900);

			// ******Have you worked in any capacity since the commencement of
			// Unemployment?*****//

			if (Worked.equalsIgnoreCase("Yes")) {

				WebElement Workedval = driver.findElement(By.xpath(
						"//label[text()='Have you worked in any capacity since the commencement of Unemployment? ']/following-sibling::label"));
				String Worked_validation = Workedval.getText();
				if (Worked.contains(Worked_validation)) {

					report.updateTestLog("Worked in any capacity since the commencement of Unemployment? validation :",
							Worked + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Worked in any capacity since the commencement of Unemployment? validation :",
							Worked + " is not displayed as expected", Status.FAIL);
				}

			} else {

				WebElement Workedval = driver.findElement(By.xpath(
						"//label[text()='Have you worked in any capacity since the commencement of Unemployment? ']/following-sibling::label"));
				String Worked_validation = Workedval.getText();
				if (Worked.contains(Worked_validation)) {

					report.updateTestLog("Worked in any capacity since the commencement of Unemployment? validation :",
							Worked + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Worked in any capacity since the commencement of Unemployment? validation :",
							Worked + " is not displayed as expected", Status.FAIL);
				}

			}

			// ********Do you expect to return to work within the next three
			// months?******//

			if (returntowork.equalsIgnoreCase("Yes")) {

				WebElement returntoworkval = driver.findElement(By.xpath(
						"//label[text()='Do you expect to return to work within the next three months? ']/following-sibling::label"));
				String returntowork_validation = returntoworkval.getText();
				if (returntowork.contains(returntowork_validation)) {

					report.updateTestLog("Return to work within the next three months? validation :",
							returntowork + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Return to work within the next three months? validation :",
							returntowork + " is not displayed as expected", Status.FAIL);
				}

				// ***When do you expect to return to work?***//

				WebElement dateofreturntoworkval = driver.findElement(
						By.xpath("//label[text()='When do you expect to return to work? ']/following-sibling::label"));
				String dateofreturntowork_validation = dateofreturntoworkval.getText();
				if (dateofreturntowork.contains(dateofreturntowork_validation)) {

					report.updateTestLog("When do you expect to return to work? validation :",
							dateofreturntowork + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("When do you expect to return to work? validation :",
							dateofreturntowork + " is not displayed as expected", Status.FAIL);
				}

			} else {
				WebElement returntoworkval = driver.findElement(By.xpath(
						"//label[text()='Do you expect to return to work within the next three months? ']/following-sibling::label"));
				String returntowork_validation = returntoworkval.getText();
				if (returntowork.contains(returntowork_validation)) {

					report.updateTestLog("Return to work within the next three months? validation :",
							returntowork + " is displayed as Expected", Status.PASS);
				} else {
				}
			}
			JavascriptExecutor nd = (JavascriptExecutor) driver;
			nd.executeScript("window.scrollBy(0,300)", "");
			sleep(900);

			if (Deathcertificate.equalsIgnoreCase("No")) {

				// ****Separation Certificate****\\

				WebElement Seperationcert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[2]"));
				String Seperationcert_validation = Seperationcert.getText();
				if (path.contains(Seperationcert_validation)) {
					report.updateTestLog("Separation Certificate name validation:", path + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Separation Certificate name validation:",
							path + " is not displayed as expected", Status.FAIL);
				}

				// ****Letter of Registration with an Australian Government
				// Approved Recruitment Agency****\\

				WebElement Lettercert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[5]"));
				String Lettercert_validation = Lettercert.getText();
				if (path.contains(Lettercert_validation)) {
					report.updateTestLog("Letter of Registration with an Australian Government validation:",
							path + " is displayed as Expected", Status.PASS);

				} else {
					report.updateTestLog("Letter of Registration with an Australian Government validation:",
							path + " is not displayed as expected", Status.FAIL);
				}

				// ****Evidence Return to Work (Optional)****\\

				WebElement Workcert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[8]"));
				String Workcert_validation = Workcert.getText();
				if (path.contains(Workcert_validation)) {
					report.updateTestLog("Evidence Return to Work name validation:", path + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Evidence Return to Work name validation:",
							path + " is not displayed as expected", Status.FAIL);
				}

				// ****Newstart Certification / Job search evidence****\\

				WebElement Jobcert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[11]"));
				String Jobcert_validation = Jobcert.getText();
				if (path.contains(Jobcert_validation)) {
					report.updateTestLog("Document name validation:", path + " is displayed as Expected", Status.PASS);

				} else {
					report.updateTestLog("Document name validation:", path + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Other****\\
				WebElement Othercert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[14]"));
				String Othercert_validation = Othercert.getText();
				if (path.contains(Othercert_validation)) {
					report.updateTestLog("Document name validation:", path + " is displayed as Expected", Status.PASS);

				} else {
					report.updateTestLog("Document name validation:", path + " is not displayed as expected",
							Status.FAIL);
				}
			}
			if (Deathcertificate.equalsIgnoreCase("No")) {

				// ****Newstart Certification / Job search evidence****\\

				WebElement Seperationcert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[2]"));
				String Seperationcert_validation = Seperationcert.getText();
				if (path.contains(Seperationcert_validation)) {
					report.updateTestLog("Document name validation:", path + " is displayed as Expected", Status.PASS);

				} else {
					report.updateTestLog("Document name validation:", path + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Other****\\
				WebElement Othercert = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[5]"));
				String Othercert_validation = Othercert.getText();
				if (path.contains(Othercert_validation)) {
					report.updateTestLog("Document name validation:", path + " is displayed as Expected", Status.PASS);

				} else {
					report.updateTestLog("Document name validation:", path + " is not displayed as expected",
							Status.FAIL);
				}

			}

			// ***Agree button**//

			JavascriptExecutor g = (JavascriptExecutor) driver;
			g.executeScript("window.scrollBy(0,3000)", "");
			sleep(900);

			wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
			sleep(500);
			report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);

			// ***Submit button**//

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12']/button[@type='submit']")))
					.click();
			sleep(500);
			report.updateTestLog("Acknowledgement checkbox", "Checkbox is clicked", Status.PASS);
			sleep(500);

			// ******Download the PDF***********\\
			properties = Settings.getInstance();
			String StrBrowseName = properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(1500);
			}
			if (StrBrowseName.equalsIgnoreCase("Firefox")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(1500);
				Robot roboobj = new Robot();
				roboobj.keyPress(KeyEvent.VK_ENTER);
			}

			if (StrBrowseName.equalsIgnoreCase("InternetExplorer")) {
				Robot roboobj = new Robot();
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();

				sleep(1500); 
			}

			report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

			sleep(1000);

			JavascriptExecutor t = (JavascriptExecutor) driver;
			t.executeScript("window.scrollBy(0,1500)", "");
			sleep(900);

			// ***Finish button**//

			wait.until(ExpectedConditions.elementToBeClickable(
					By.xpath("//div[@class='col-xs-6 col-sm-2 right-col']/button[@type='submit']"))).click();
			sleep(500);
			report.updateTestLog("Finish button:", "Finish button is clicked", Status.PASS);

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}


	
	public void Eclaims_unemployment_Negativescenario(){
		
		try{
			
			String Creditcard = dataTable.getData("Eclaims_unemployment", "Creditcard");
			String Title = dataTable.getData("Eclaims_unemployment", "Title");
			String DOB = dataTable.getData("Eclaims_unemployment", "DOB");
			String Name = dataTable.getData("Eclaims_unemployment", "Name");
			String Surname = dataTable.getData("Eclaims_unemployment", "Surname");
			String Address = dataTable.getData("Eclaims_unemployment", "Address1");
			String Addresss = dataTable.getData("Eclaims_unemployment", "Address2");
			String suburb = dataTable.getData("Eclaims_unemployment", "suburb");
			String state = dataTable.getData("Eclaims_unemployment", "State");
			String postcode = dataTable.getData("Eclaims_unemployment", "Postcode");
			String PreferredPhoneNO = dataTable.getData("Eclaims_unemployment", "PreferredPhoneNO");
			String Otherphoneno = dataTable.getData("Eclaims_unemployment", "Otherphoneno");
			String Email = dataTable.getData("Eclaims_unemployment", "Email");
			String Preferredmode = dataTable.getData("Eclaims_unemployment", "Preferredmode");
			String Contacttime = dataTable.getData("Eclaims_unemployment", "Contacttime");
			String Deathcertificate = dataTable.getData("Eclaims_unemployment", "Deathcertificate");
			String Attachpath = dataTable.getData("Eclaims_unemployment", "Attachpath");
			String Lastdayofwork = dataTable.getData("Eclaims_unemployment", "Lastdayofwork");
			String Lastdaydate = dataTable.getData("Eclaims_unemployment", "Lastdaydate");
			String dateofreturn = dataTable.getData("Eclaims_unemployment", "dateofreturn");
			String Employername = dataTable.getData("Eclaims_unemployment", "Employername");
			String Dateofcommencement = dataTable.getData("Eclaims_unemployment", "Dateofcommencement");
			String Employeraddress = dataTable.getData("Eclaims_unemployment", "Employeraddress");
			String Employercontactno = dataTable.getData("Eclaims_unemployment", "Employercontactno");
			String unemploymentReason = dataTable.getData("Eclaims_unemployment", "unemploymentReason");
			String Datenotified = dataTable.getData("Eclaims_unemployment", "Datenotified");
			String employment = dataTable.getData("Eclaims_unemployment", "employment");
			String benefitregistered = dataTable.getData("Eclaims_unemployment", "benefitregistered");
			String benefitcommenced = dataTable.getData("Eclaims_unemployment", "benefitcommenced");
			String Worked = dataTable.getData("Eclaims_unemployment", "Worked");
			String returntowork = dataTable.getData("Eclaims_unemployment", "returntowork");
			String dateofreturntowork = dataTable.getData("Eclaims_unemployment", "dateofreturntowork");
			String Evidence = dataTable.getData("Eclaims_unemployment", "Evidence");
			String Postcode = dataTable.getData("Eclaims_unemployment", "Postcode");
			String Doberror = dataTable.getData("Eclaims_unemployment", "Doberror");
			String emailerror = dataTable.getData("Eclaims_unemployment", "emailerror");
			String phonenoerror = dataTable.getData("Eclaims_unemployment", "phonenoerror");		
			String Otherphonenoerror = dataTable.getData("Eclaims_unemployment", "Otherphonenoerror");		
			String Postcodeerror = dataTable.getData("Eclaims_unemployment", "Postcodeerror");
		

				WebDriverWait wait = new WebDriverWait(driver, 18);

				// ****Click on claim type*****\\

				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-INVOUNEMPY']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Claim Type :", "Involuntary Unemployment is Selected", Status.PASS);

				// ****Enter creditcard number*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
				report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",
						Creditcard + " is entered", Status.PASS);

				// **********************\\
				// ****Insured Details*****\\
				// **************************\\

				// ****Select title*****\\

				Select titletype = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
				titletype.selectByVisibleText(Title);
				report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

				// ****Enter DOB*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
						.sendKeys(DOB);
				report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

				// ****Enter Name*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
						.sendKeys(Name);
				report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

				// ****Enter surname*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
						.sendKeys(Surname);
				report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

				// ****Enter Address1*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
						.sendKeys(Address);
				report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

				// ****Enter Address2*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
						.sendKeys(Addresss);
				report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

				// ****Enter Suburb*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
						.sendKeys(suburb);
				report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

				// ****Select State*****\\

				Select Stateselect = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
				Stateselect.selectByVisibleText(state);
				report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
				sleep(500);

				// ****Enter Postal code*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
						.sendKeys(Postcode);
				sleep(500);
				report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
				sleep(300);

				// ****Enter Emailaddress*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ213_0")))
						.sendKeys(Email);
				report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

				// ****Enter Peferred phone number*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ211_1")))
						.sendKeys(PreferredPhoneNO);
				sleep(500);
				report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
				sleep(500);

				// ****Enter other phone number*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ210_2")))
						.sendKeys(Otherphoneno);
				sleep(500);
				report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
				sleep(500);

				JavascriptExecutor j = (JavascriptExecutor) driver;
				j.executeScript("window.scrollBy(0,200)", "");
				sleep(900);

				// ****Preferred mode of contact (Phone / Email / Letter)?*****\\

				if (Preferredmode.equalsIgnoreCase("Phone")) {

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

					Select mode = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ215_1")));
					mode.selectByVisibleText(Contacttime);
					report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
							Contacttime + " is selected ", Status.PASS);
					sleep(500);
				}
				if (Preferredmode.equalsIgnoreCase("Email")) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
				}
				if (Preferredmode.equalsIgnoreCase("Letter")) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
				}

			
				
				// ****Proceed button******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary w100p']")))
						.click();
				sleep(500);
				report.updateTestLog("Proceed Button", "clicked", Status.PASS);
				
				
				WebElement Doberrormessage = driver.findElement(
						By.xpath("(//div[@class='error-message ng-binding ng-scope'])[1]"));
				WebElement emailerrormessage = driver.findElement(
						By.xpath("(//div[@class='error-message ng-binding ng-scope'])[2]"));
				WebElement postcodeerrormessage = driver.findElement(
						By.xpath("(//div[@class='ng-scope'])[2]"));
				WebElement phonenoerrormessage = driver.findElement(
						By.xpath("(//div[@class='error-message ng-binding ng-scope'])[3]"));
				WebElement otherphonenoerrormessage = driver.findElement(
						By.xpath("(//div[@class='error-message ng-binding ng-scope'])[4]"));
//				WebElement Deathcermessage = driver.findElement(
//						By.xpath("(//div[@class='error-message ng-binding ng-scope'])[5]"));
				String Doberror_validation = Doberrormessage.getText();
				String emailerror_validation = emailerrormessage.getText();
				String phonenoerror_validation = phonenoerrormessage.getText();
//				String Deathdateerror_validation = Deathcermessage.getText();
				String otherphonenoerror_validation  = otherphonenoerrormessage.getText();
				String postcodeerror_validation  = postcodeerrormessage.getText();
				
				
				
				if (Doberror.contains(Doberror_validation)) {

					report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is not displayed as expected",
							Status.FAIL);
				}
				
				if (Postcodeerror.contains(postcodeerror_validation)) {

					report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is not displayed as expected",
							Status.FAIL);
				}
				
				if (emailerror.contains(emailerror_validation)) {

					report.updateTestLog("Email address Error validation:", emailerror + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Email address Error validation:", emailerror + " is not displayed as expected",
							Status.FAIL);
				}
				if (phonenoerror.contains(phonenoerror_validation)) {

					report.updateTestLog("Phone number Error message validation:", phonenoerror + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Phone number Error message validation:", phonenoerror + " is not displayed as expected",
							Status.FAIL);
				}
				if (Otherphonenoerror.contains(otherphonenoerror_validation)) {

					report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is not displayed as expected",
							Status.FAIL);
				}

				
			
		
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		
		}
}
	
	