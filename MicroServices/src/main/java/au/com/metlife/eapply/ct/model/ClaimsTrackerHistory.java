package au.com.metlife.eapply.ct.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimsTrackerHistory extends JpaRepository<au.com.metlife.eapply.ct.model.CTHistoryBO, String>{

}
