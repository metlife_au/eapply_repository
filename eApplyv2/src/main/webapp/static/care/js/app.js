// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var CareSuperApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'googleAnalytics','ui.bootstrap', 'eApplyInterceptor','ngSanitize']);
// configure our routes
CareSuperApp.config(function($routeProvider,$httpProvider,$templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});
    $routeProvider
	    .when('/', {
	        templateUrl : 'static/care/landingpage.html',
	        controller  : 'login',
	       reloadOnSearch: false,
	       resolve: {
	    	   urls:function(urlService){
	    		   return urlService.getUrls();
	    	   }
	       }
	    })
	    .when('/landing', {
	        templateUrl : 'static/care/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(urlService){
	    		   return urlService.getUrls();
	    	   }
	        }
	    })
	    .when('/quote/:mode', {
	        templateUrl : 'static/care/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    .when('/newmemberquote/:mode', {
	      templateUrl : 'static/care/newMember_changeCover.html',
	       controller  : 'newmemberquote'
	    })
	   .when('/quotecancel/:mode', {
	        templateUrl : 'static/care/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/care/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })
	     .when('/quotetransfer/:mode', {           
	    	 templateUrl : 'static/care/coverDetails_transfer.html',
	        controller  :  'quotetransfer'
	    })
	     .when('/quoteoccchange/:mode', {
	        templateUrl : 'static/care/coverDetails_updateDetails.html',
	        controller  : 'quoteoccupdate'
	    })
	     .when('/auratransfer/:mode', {             
	    	 templateUrl : 'static/care/aura_transferCover.html',
	        controller  : 'auratransfer'
	    })
	    .when('/auraocc/:mode', {             
	    	 templateUrl : 'static/care/aura_updateDetails.html',
	        controller  : 'auraocc'
	    })
	    .when('/auraspecialoffer/:mode', {             
	    	 templateUrl : 'static/care/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/aura/:mode', {
	        templateUrl : 'static/care/aura.html',
	        controller  : 'aura'
	    })
	    .when('/summary/:mode', {
	        templateUrl : 'static/care/confirmation.html',
	        controller  : 'summary'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/care/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/care/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/care/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/care/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/care/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	    .when('/changecovernochange', {
	        templateUrl : 'static/care/changeCover_decision_acceptedwithNC.html',
	        controller  : 'changeCoverNoChangeController'
	    })
	     .when('/requirements/:clm', {
	        templateUrl : 'claimtracker/events.html',
	        controller  : 'requirements'
	    })
	    .when('/newsummary', {
	        templateUrl : 'static/care/newMember_confirmation.html',
	        controller  : 'newsummary'
	    })
	    .when('/workRatingSummary', {
	        templateUrl : 'static/care/workRating_confirmation.html',
	        controller  : 'workRatingSummary'
	    })
	    
	    .when('/workRatingAccept', {
	        templateUrl : 'static/care/workRating_decision_accepted.html',
	        controller  : 'workRatingAcceptController'
	    })
	    
	    .when('/workRatingDecline', {
	        templateUrl : 'static/care/workRating_decision_decline.html',
	        controller  : 'workRatingDeclineController'
	    })
	    
	    .when('/workRatingMaintain', {
	        templateUrl : 'static/care/workRating_decision_maintained.html',
	        controller  : 'workRatingMaintainController'
	    })
	    
	    .when('/transferSummary', {
	        templateUrl : 'static/care/transfer_confirmation.html',
	        controller  : 'transferSummary'
	    })
	    
	    .when('/transferAccept', {
	        templateUrl : 'static/care/transfer_decision_accepted.html',
	        controller  : 'transferAcceptController'
	    })
	    
	    .when('/transferDecline', {
	        templateUrl : 'static/care/transfer_decision_decline.html',
	        controller  : 'transferDeclineController'
	    })
	    .when('/newmemberaccept', {
	        templateUrl : 'static/care/newMember_decision_accepted.html',
	        controller  : 'newMemberAcceptController'
	    })
	    .when('/newmemberdecline', {
	        templateUrl : 'static/care/newMember_decision_decline.html',
	        controller  : 'newMemberDeclineController'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/care/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/care/cancel_decision.html',
	        controller  : 'cancelController'
	    })
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/care/timeout.html',
	        controller  : 'timeOutController'
	    })

});
CareSuperApp.constant('APP_CONSTANTS', function () {
	return {
		'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	}
});
CareSuperApp.run([
    'gaTracker', '$rootScope', function(gaTracker, $rootScope) {
      // subscribe to events
      $rootScope.$on('$routeChangeSuccess', function() {
      	gaTracker.sendPageview(sessionStorage.getItem('UniqueAppNum') || '');
      });
    }
  ]);

