package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALISaveandExitPageObjects extends PageObjectLibrary{
	public static final By btnSave = getObjectIdentifaction("ALI.payment.saveandexit.btn.css");
	public static final By btnConfirm = getObjectIdentifaction("ALI.payment.confirm.btn.css");
	public static final By btnFinish= getObjectIdentifaction("ALI.payment.finish.btn.css");
}
