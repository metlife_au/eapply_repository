export default class numbersOnly {
  constructor() {
    this.restrict = 'EA';
    this.replace = true;
    this.scope = true;
    this.require = 'ngModel';
  }

  link(scope, element, attrs, ngModelCtrl) {	  
	  element.on('change', function () {
          scope.$apply(function () {
        	  ngModelCtrl.$parsers.push(function (text) {    		
          		if (text) {
                      var transformedInput = text.replace(/[^0-9]/g, '');
                      if (transformedInput !== text) {
                          ngModelCtrl.$setViewValue(transformedInput);
                          ngModelCtrl.$render();
                      }
                      
                      return transformedInput;
                      
                  }
                  return undefined;
          	});
          });
        });
    }
}
