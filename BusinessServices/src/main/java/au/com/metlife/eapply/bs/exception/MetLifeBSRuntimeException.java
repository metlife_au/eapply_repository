/**
 * 
 */
package au.com.metlife.eapply.bs.exception;

/**
 * @author 199306
 *
 */
public class MetLifeBSRuntimeException extends RuntimeException{
	
	private String errCode;
	
	private String errMsg;
	
	public String getErrCode() {
		return errCode;
	}

	public MetLifeBSRuntimeException(String errCode, String errMsg) {
		super();
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}



}
