package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class HOSTPLUS_ChangeYourInsuranceCoverPageObjects extends PageObjectLibrary{
	
	public static final By  changeyourinsurancecover = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Link.xpath");
	public static final By dutyOfDisclosure = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.DutyOfDisclosure.xpath");
	public static final By privacyStatement = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.PrivacyStatement.xpath");
	public static final By contactType = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContactType.xpath");
	public static final By contactNumber = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContactNumber.id");
	public static final By morningContact = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.MorningContact.id");
	public static final By afternoonContact = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.AfternoonContact.id");
	public static final By male = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Gender.Male.id");
	public static final By female = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Gender.Female.id");
	public static final By contactDetailsContinue = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContactDetailsContinue.xpath");

}
