package au.com.metlife.eapply.b2b.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import au.com.metlife.eapply.b2b.utility.B2bServiceHelper;
import au.com.metlife.eapply.quote.utility.SystemProperty;

@Component
public class XMLValidationFilter implements Filter{
	private static final Logger log = LoggerFactory.getLogger(XMLValidationFilter.class);

	
	 private String prop;
	 SystemProperty sys;
	@Autowired
    public XMLValidationFilter() {
		try {
			sys = SystemProperty.getInstance();
			 this.prop = sys.getProperty("xsdpath");
		     log.info("==================  {}", prop);
		} catch (Exception e) {
			log.error("Error in xsd path {}",e);
		}
       
    }	

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//Do nothing as written for initialize filter
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String secureString = request.getParameter("secureString");
		B2bServiceHelper b2bServiceHelper = new B2bServiceHelper();	
		String fundid = null;
		try {
			System.out.println("secureString>>"+secureString);
			if(secureString!=null && secureString.contains("<fundID>") && secureString.contains("</fundID>")){
				fundid = secureString.substring(secureString.indexOf("<fundID>")+8, secureString.indexOf("</fundID>"));

			}		
			

			if(!"Corporate".equalsIgnoreCase(secureString)) {
			String validXML = b2bServiceHelper.validateXMLString(secureString,
					prop + fundid+"_WebService_schema_new.xsd");
			log.info("validXML>> {}",validXML);
			if (!"valid".equalsIgnoreCase(validXML)) {
				throw new ServletException("Invalid XML Passed");
			}
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
     //Do nothing as written for destroy filter
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		XMLValidationFilter filter = new XMLValidationFilter();
		filterRegistrationBean.setFilter(filter);
		filterRegistrationBean.addUrlPatterns("/oauth/token");
		return filterRegistrationBean;
	}

}
