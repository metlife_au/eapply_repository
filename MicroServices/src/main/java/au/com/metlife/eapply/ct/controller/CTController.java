package au.com.metlife.eapply.ct.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.ct.dto.ClaimData;
import au.com.metlife.eapply.ct.exception.MetlifeException;
import au.com.metlife.eapply.ct.model.DocumentBO;
import au.com.metlife.eapply.ct.model.UserInfoBO;
import au.com.metlife.eapply.ct.service.CTService;
import au.com.metlife.eapply.ct.webserivce.DoumentUploadHelper;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.webservices.PicsLodgementStub;


@RestController
@Component
public class CTController {
	
	private static final Logger log = LoggerFactory.getLogger(CTController.class);
	
	
	 private final CTService ctService;
	 
	 SystemProperty sys;

	  @Autowired
	    public CTController(final CTService ctService) {
	        this.ctService = ctService;	       
	    }
	
	
	
	 
	 @RequestMapping(value = "/saveDocument", method = RequestMethod.GET)
	  public void saveDocument() {		
		 /*b2bService.generateToken(eappstring.getSecurestring());*/
		 log.info("save Document start");
		 DocumentBO document = new DocumentBO();
		 document.setPolicyNum("123456");
		 document.setDocumentCode("description");
		 document.setDocumentlocation("test");
		 document.setUuId("123456");
		 log.info("save Document finish");
		 /*ctService.saveDocumentList(documentDto); */
	 }
	
	@RequestMapping(value = "/submitFiles", method = RequestMethod.POST)
    @ResponseBody
    public  void submitFiles(@RequestBody ClaimData claimData,HttpServletResponse response ) throws IllegalStateException, IOException, JSONException {
		//DocumentBO document;			
		log.info("Submitting Files.... start");

        /*if (null != claimData.getDocumentList() && claimData.getDocumentList().size()>0) {*/
		if (null != claimData.getDocumentList() && !(claimData.getDocumentList().isEmpty())) {
        	ctService.saveDocumentList(claimData.getDocumentList()); 
        	UserInfoBO userInfo = new UserInfoBO();
    		userInfo.setFundType(claimData.getFundId());
    		/*userInfo.setId("345678");*/
    		userInfo.setFirstname(claimData.getFirstName());
    		userInfo.setLastname(claimData.getSurName());
    		/*userInfo.setTitle("Mr");
    		userInfo.setGender("female");*/    		
    		userInfo.setDateOfBirth(new Date(claimData.getDob()));
    		/*userInfo.setClientRefNumber("9970");
    		userInfo.setDateJoinedFund(new Date());
    		userInfo.setAdditionalInfo("test");*/
    		userInfo.setApplicationId1(claimData.getClaimId());
    		if ("Group Life".equalsIgnoreCase(claimData.getProductType())) {
    			claimData.setProductType("GL");
    		} else {
    			claimData.setProductType("SCI");
    		}
    		
/*    		for (DocumentDto docDto :claimData.getDocumentList()) {		
    		 DocumentDto claimDocument=new DocumentDto();			
    		 docDto.setDocumentlocation("/shared/eLodg/4485801_25022016_173948.pdf");    		
   		 claimDocument.setDocumentCode("Birth Certificate");
   	    }
*/    		ArrayList claimDocumentVector = new ArrayList(claimData.getDocumentList());
    		userInfo.setElodgeDocument(claimDocumentVector);
    		try {
    			sys = SystemProperty.getInstance();
    		} catch (Exception e) {
    			log.error("Exception : {}" ,e.getMessage());
    		}    		
    		try {
    		/*DoumentUploadHelper  docHelper = new DoumentUploadHelper();
    		PicsLodgementStub.ReturnObject retObj = docHelper.doLodgeExistingClaims(userInfo, "", "", claimData.getSurName()+" "+claimData.getFirstName(), claimData.getProductType(), ""
    				,sys.getProperty("clientConfig"),sys.getProperty("picsUsername"),sys.getProperty("picsPassword"),sys.getProperty("picsUrl"));*/
    		PicsLodgementStub.ReturnObject retObj = DoumentUploadHelper.doLodgeExistingClaims(userInfo, "", "", claimData.getSurName()+" "+claimData.getFirstName(), claimData.getProductType(), ""
        				,sys.getProperty("clientConfig"),sys.getProperty("picsUsername"),sys.getProperty("picsPassword"),sys.getProperty("picsUrl"));
    		log.info("CodeDescription : {}",retObj.getCodeDescription());
    		log.info("Code : {}",retObj.getCode());
    		 response.setCharacterEncoding("UTF-8");
             response.setContentType("application/json");            
             response.flushBuffer();
    	} catch (MetlifeException e) {
    		log.error("Error message {},errorcode {} and messageerrorcode {}",e.getMessage(),e.getErrorCode(),e.getMsgErrorCode());
    	}
    }  
        log.info("Submitting Files.... end");
}
	@RequestMapping(value = "/initialFiles/{claimNo}", method = RequestMethod.GET)
    @ResponseBody
    public  void initialFiles(@PathVariable(value="claimNo") String claimNo,HttpServletResponse response) throws IllegalStateException, IOException, JSONException {
		
		log.info("initialFiles....start");
		
		JSONArray jsonArray = null;
		if (null != claimNo){
			List<DocumentBO> docList = ctService.retriveDocList(claimNo);
			if (null != docList) {
			    jsonArray = new JSONArray();
				for (DocumentBO doc : docList) {
					JSONObject json = new JSONObject();
					json.put("name", doc.getFileName());
					json.put("size", doc.getFileSize());
					json.put("id", doc.getFileId());
					json.put("uuid", doc.getUuId());
					json.put("description", doc.getDocumentCode());	
					jsonArray.put(json);
				}
			}
		}		
         response.setCharacterEncoding("UTF-8");
         response.setContentType("application/json");
         response.getWriter().print(jsonArray);
         response.flushBuffer();
         log.info("initialFiles....end");
	}
	
}