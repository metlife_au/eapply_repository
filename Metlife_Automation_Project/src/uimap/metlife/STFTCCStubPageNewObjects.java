package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTCCStubPageNewObjects extends PageObjectLibrary {
	public static final By txtareainput = getObjectIdentifaction("stft.stub.input.textarea.css");
	public static final By btnApply = getObjectIdentifaction("stft.stub.input.btn.css");
}
