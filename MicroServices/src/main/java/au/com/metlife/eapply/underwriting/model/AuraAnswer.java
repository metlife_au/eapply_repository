package au.com.metlife.eapply.underwriting.model;

import java.io.Serializable;
import java.util.List;

public class AuraAnswer implements Serializable{

	private String answerValue;
	private String answerText;
	private List<AuraQuestion>  childQuestions;
	private String answerID;
	private String helpText;
	public String getHelpText() {
		return helpText;
	}
	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}
	public String getAnswerID() {
		return answerID;
	}
	public void setAnswerID(String answerID) {
		this.answerID = answerID;
	}
	public String getAnswerValue() {
		return answerValue;
	}
	public void setAnswerValue(String answerValue) {
		this.answerValue = answerValue;
	}
	public String getAnswerText() {
		return answerText;
	}
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	public List<AuraQuestion> getChildQuestions() {
		return childQuestions;
	}
	public void setChildQuestions(List<AuraQuestion> childQuestions) {
		this.childQuestions = childQuestions;
	}

}
