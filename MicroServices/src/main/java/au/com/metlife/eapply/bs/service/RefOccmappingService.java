package au.com.metlife.eapply.bs.service;



import java.util.List;

import au.com.metlife.eapply.bs.model.RefOccmapping;



public interface RefOccmappingService {

	List<RefOccmapping> getNewOccupationName(String fundId,String occName);
}
