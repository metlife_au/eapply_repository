<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="eapplymain" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="../../eapply/static/host/images/favicon.ico">
<title>Hostplus</title>

<!-- Bootstrap core CSS -->
<link href="../../eapply/static/host/css/bootstrap.min.css" rel="stylesheet">
<link href="../../eapply/static/host/css/bootstrap-select.css" rel="stylesheet">
<link href="../../eapply/static/host/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="../../eapply/static/host/css/datepicker.css" />


<!-- Custom styles for this template -->
<link href="../../eapply/static/host/css/custom.css" rel="stylesheet">
<link href="../../eapply/static/host/css/custom-ben.css" rel="stylesheet">
<link href="../../eapply/static/host/css/ngDialog.css" rel="stylesheet">
<link rel="stylesheet" href="../../eapply/static/host/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="../../eapply/static/host/css/ngDialog-theme-plain.css">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../eapply/static/host/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.../../eapply/static/host/js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var baseUrl = '<%= domainName%>';
</script>


<body class="tooltip-demo">

<div ng-view autoscroll="true"></div>
<div id="body-mask" data-loading>
	<div class="body-mask-loader"></div>
</div>
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../../eapply/resources/js/lib/angular.min.js"></script>
<script src="../../eapply/resources/js/lib/angular-idle.min.js"></script>
<script src="../../eapply/static/host/js/angular-route.js"></script>
<script src="../../eapply/static/host/js/angular-resource.min.js"></script>
<script src="../../eapply/static/host/js/jquery.min.js"></script> 
<!--  <script src="../../eapply/static/host/js/jquery-ui.js"></script> -->
<script src="../../eapply/static/host/js/angular-sanitize.js"></script>
<script src="../../eapply/static/host/js/bootstrap.min.js"></script> 
<script src="../../eapply/static/host/js/docs.min.js"></script>
<script src="../../eapply/static/host/js/ng-file-upload-shim.js"></script>
<script src="../../eapply/static/host/js/ng-file-upload.js"></script>
<script src="../../eapply/static/host/js/ngDialog.js"></script>
<script src="../../eapply/static/host/js/ui-bootstrap-tpls-2.5.0.min.js"></script>

<script src="../../eapply/static/common/services/sessionTimeoutService.js"></script>
<script src="../../eapply/static/common/services/exceptionHandler.js"></script>
<script src="../../eapply/static/common/services/storageHandler.js"></script>
<script src="../../eapply/static/common/services/restAPI.js"></script>
<script src="../../eapply/static/common/services/appDataModel.js"></script>
<script src="../../eapply/static/common/services/eApplyInterceptor.js"></script>
<!-- CareSuperApp application file -->
<script src="../../eapply/static/host/js/app.js"></script>


<!-- <script src="https://assets.adobedtm.com/7bd378b63ae642286f5e272876265df98dfca74c/satelliteLib-9d49fffd8e96ae995aa8e242a15f1acf1428ecc8-staging.js"></script>
 -->

<!-- CareSuperApp controllers -->
<script type="text/javascript" src="../../eapply/static/host/js/controllers/aura.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/auraOcc.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/auraSpecialOffer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/auraTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/auraCancel.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/decision.ctrl.js"></script>
<!-- <script type="text/javascript" src="../../eapply/static/host/js/controllers/landing.ctrl.js"></script> -->
<script type="text/javascript" src="../../eapply/static/host/js/controllers/login.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/newMemberQuote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/quote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/quoteCancel.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/quoteOccUpdate.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/quoteTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/retrieveSavedApp.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/summary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/newMemberSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/workRatingSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/transferSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/DecisionController.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/cancelConfirmation.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/timeOutController.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/lifeEvent.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/quoteSpecial.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/specialOfferSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/lifeeventsummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/auraLifeEvent.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/quoteConvert.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/convertSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/controllers/header.ctrl.js"></script>

<script type="text/javascript" src="../../eapply/static/host/js/moment.js"></script> 
<script type="text/javascript" src="../../eapply/static/host/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="../../eapply/static/host/js/bootstrap-select.js"></script> 

<!-- CareSuperApp Services -->
<script type="text/javascript" src="../../eapply/static/host/js/services/service.js"></script>

<!-- CareSuperApp utilities -->
<script type="text/javascript" src="../../eapply/static/host/js/utils/filters.js"></script>
<script type="text/javascript" src="../../eapply/static/host/js/utils/directives.js"></script>


<script src="../../eapply/static/host/js/custom.js"></script> 
<script src="../../eapply/static/host/js/validation.js"></script>
<!--<script type="text/javascript">_satellite.pageBottom();</script> -->

<script>
//awful hack for angular-vertilize...
angular.element.prototype.outerWidth = function() {
    return jQuery(this[0]).outerWidth();
};
angular.element.prototype.height = function() {
    return jQuery(this[0]).height();
};
</script>
<script type="text/javascript" src="../../eapply/static/host/js/utils/verticalHeight.js"></script>
<script>
        var clientData;
        var applicantData = {
            clientRefNumber: null,
            age: null,
            gender: null,
            partnerID: null,
            app_number: null,
            dateOfBirth: null
        };
        window['adrum-start-time'] = new Date().getTime();
        window['adrum-config'] = {
          userEventInfo: {
            "PageView": function(context) {
                return {
                    userPageName: window.location.hash,
                    userData: {
                        partner_id: applicantData.partnerID.toString(),
                        member_number: applicantData.clientRefNumber.toString(),
                        gender: applicantData.gender,
                        //dateOfBirth: applicantData.dateOfBirth.toString()
                    }
                }
            },
            "VPageView": function(context) {
                return {
                    userPageName: window.location.hash,
                    userData: {
                        partner_id: applicantData.partnerID.toString(),
                        member_number: applicantData.clientRefNumber.toString(),
                        gender: applicantData.gender,
                        //dateOfBirth: applicantData.dateOfBirth.toString(),
                        app_number: applicantData.app_number.toString(),
                        age: applicantData.age.toString()
                    }
                }
            }
          }
        };
        (function(config){
            if(baseUrl === 'https://www.eapplication.metlife.com.au/'){
                config.appKey = 'EUM-AAB-AUN';
                config.beaconUrlHttp = 'https://eum.metlife.com.au';
                config.beaconUrlHttps = 'https://eum.metlife.com.au';
            } else{
                config.appKey = 'EUM-AAB-AUY';
                config.beaconUrlHttp = 'https://uat.eum.metlife.com.au';
                config.beaconUrlHttps = 'https://uat.eum.metlife.com.au';
            }
            config.urlCapture = {
                filterURLQuery: true
            }
            config.adrumExtUrlHttp = 'http://cdn.appdynamics.com';
            config.adrumExtUrlHttps = 'https://cdn.appdynamics.com';
            config.xd = {enable : true};
        })(window['adrum-config'] || (window['adrum-config'] = {}));
        if ('https:' === document.location.protocol) {
        var s = document.createElement( 'script' );
            s.setAttribute( 'src', 'https://cdn.appdynamics.com/adrum/adrum-4.4.2.452.js' );
            document.body.appendChild( s );


        } else {
        var s = document.createElement( 'script' );
            s.setAttribute( 'src', 'http://cdn.appdynamics.com/adrum/adrum-4.4.2.452.js' );
            document.body.appendChild( s );
        }
        </script>
</body>
</html>