package pages.metlife;

import org.openqa.selenium.interactions.Actions;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCGetQuotePageObjects;
import uimap.metlife.ColesCCStubPageObjects;
import uimap.metlife.ColesConfirmationPageObjects;
import uimap.metlife.STFTCCStubPageNewObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.DPStubPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import javax.swing.JOptionPane;

public class DPStubPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public DPStubPage selectProduct() {
		
		selectProductDetails();
		return new DPStubPage(scriptHelper);
	}

	public DPStubPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void selectProductDetails() {		
		
		String product = dataTable.getData("MetLife_Data", "Product");
		String brand = dataTable.getData("MetLife_Data", "Brand");
		try {
//			switchToActiveWindow();
//			driver.manage().window().maximize();
			
			
			driver.manage().window().maximize();			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			Thread.sleep(1000);
			tryAction(fluentWaitElement(DPStubPageObjects.drpdownProduct), "DropDownSelect", "Product", product);
			Thread.sleep(1000);
			// JOptionPane.showMessageDialog(null,"ALERT MESSAGE","TITLE",JOptionPane.WARNING_MESSAGE);	
			tryAction(fluentWaitElement(DPStubPageObjects.drpdownbrand), "DropDownSelect", "Brand", brand);
			Thread.sleep(1000);

			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(DPStubPageObjects.btnApply)).click().perform();
			Thread.sleep(4000);
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
