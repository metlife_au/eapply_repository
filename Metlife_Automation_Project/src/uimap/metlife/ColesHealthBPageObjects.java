package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesHealthBPageObjects extends PageObjectLibrary {
	public static final By radMoreThan30Yes = getObjectIdentifaction("coles.yourhealthb.smoke.morethan30.radio.yes.css");
	public static final By radMoreThan30No = getObjectIdentifaction("coles.yourhealthb.smoke.morethan30.radio.no.css");
	public static final By radMoreThan50Yes = getObjectIdentifaction("coles.yourhealthb.smoke.morethan50.radio.yes.css");
	public static final By radMoreThan50No = getObjectIdentifaction("coles.yourhealthb.smoke.morethan50.radio.no.css");
	public static final By chkSufferLung = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.lung.chkbox.css");
	public static final By chkSufferAsthma = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.asthma.chkbox.css");
	public static final By radMedicalAttentionYes = getObjectIdentifaction("coles.yourhealthb.abturhealth.medicalattention.yes.radio.css");
	public static final By radAsthmaMild = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.asthma.mild.radio.css");
	public static final By radAsthmaModerate = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.asthma.moderate.radio.css");
	public static final By radAsthmaSevere = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.asthma.severe.radio.css");
	public static final By txtAlcoholQntity = getObjectIdentifaction("coles.yourhealthb.abturhealth.AFContentCell.css");
	public static final By btnEnter = getObjectIdentifaction("coles.yourhealthb.abturhealth.AFContentCell.btn.enter.css");
	public static final By radListAbtUrLifeStyleNo = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturlifestyle.list.no.radio.css");
	public static final By radLifestyle2 = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturlifestyle.radio2.css");
	public static final By radLifestyle3 = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturlifestyle.radio3.css");
	public static final By radLifestyle4 = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturlifestyle.radio4.css");
	//public static final By chkSufferedFrom = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.none.checkbox.css");
	public static final By chkSufferedFrom = getObjectIdentifaction("coles.yourhealthb.abturhealth.sufferedfrom.none.checkbox.css");
	public static final By rdAbtUrStyleNo = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturlifestyle.no.radio.css");
	public static final By rdAbtMedicalHistoryNo = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturmedicalhistory.no.radio.css");
	public static final By rdAbtInsuranceHistoryNo = getObjectIdentifaction("coles.yourhealthb.abturhealth.abturinsurancehistory.no.radio.css");
	public static final By lnkNext = getObjectIdentifaction("coles.yourhealthb.next.link.id");
	public static final By radChgFlybuysYes = getObjectIdentifaction("coles.yourhealthb.flybuys.radio.yes.id");
	public static final By radChgFlybuysNo = getObjectIdentifaction("coles.yourhealthb.flybuys.radio.no.id");
	public static final By radChgJoinFlybuysYes = getObjectIdentifaction("coles.yourhealthb.joinbuys.radio.yes.id");
	public static final By radChgJoinFlybuysNo = getObjectIdentifaction("coles.yourhealthb.joinbuys.radio.no.id");
	public static final By txtFlybuysNumber = getObjectIdentifaction("coles.yourhealthb.flybuys.number.txt.id");
	public static final By chkboxAgree = getObjectIdentifaction("coles.yourhealthb.flybuys.agree.chkbox.id");
	
	
	
	
	
}
