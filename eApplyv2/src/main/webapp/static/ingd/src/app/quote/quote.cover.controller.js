class QuoteCoverCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog, $timeout, $q) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._ngDialog = ngDialog;
    this._$timeout = $timeout;
    this._$q = $q;
    this.applicantData = {};
    this.changeCoverData = {};
    this.exDthCover = {};
    this.exTpdCover = {};
    this.exIpCover = {};
    this.contactOpts = AppConstants.transfer.contactOpts;
    this.emailPattern = AppConstants.regEx.emailFormat;
    this.mobilePattern = AppConstants.regEx.phoneNumber;
    this.waitingOpts = ['30 Days','60 Days', '90 Days'];
    this.benefitOpts = ['2 Years', 'Age 67'];
    this.premiumOpts = ['Monthly', 'Yearly', 'Weekly'];
    this.PrefTimeOpts = [
		'Morning (9am \u002d 12pm)',
		'Afternoon (12pm \u002d 6pm)'];
    this.modelOptions = {updateOn: "blur"};
    this.changeCoverData = {};
    this.ipChanged = false;
    
    $timeout(function(){
    	
    	//watches
    	
    	$scope.$watchGroup(['formduty.$valid', 'privacyForm.$valid', 'memberDetails.$valid', 'occupationForm.$valid'], function(newVal, oldVal) {
    		
    		if($scope.formduty.$valid && $scope.privacyForm.$valid && $scope.memberDetails.$valid && $scope.occupationForm.$valid){
    			$('#privacy-statement').collapse('show');
    			$('#contact-details').collapse('show');
    			$('#occupation-details').collapse('show');
    			$('#cover-details').collapse('show'); 
    		}else if($scope.formduty.$valid && $scope.privacyForm.$valid && $scope.memberDetails.$valid){
    			$('#privacy-statement').collapse('show');
    			$('#contact-details').collapse('show');
    			$('#occupation-details').collapse('show');
    		}else if($scope.formduty.$valid && $scope.privacyForm.$valid){
    			$('#privacy-statement').collapse('show');
    			$('#contact-details').collapse('show');
    		}else if($scope.formduty.$valid){
    			$('#privacy-statement').collapse('show');
    		}
    		
    	});
    	
    	$scope.$watch('formduty.$invalid', function(newVal, oldVal) {    		
    		$('#privacy-statement').collapse('hide');
			$('#contact-details').collapse('hide');
			$('#occupation-details').collapse('hide');
			$('#cover-details').collapse('hide'); 
		});
    	
    	$scope.$watch('privacyForm.$invalid', function(newVal, oldVal) {    		
    		$('#contact-details').collapse('hide');
			$('#occupation-details').collapse('hide');
			$('#cover-details').collapse('hide');
		});
    	
    	$scope.$watch('memberDetails.$invalid', function(newVal, oldVal) {    		
    		$('#occupation-details').collapse('hide');
			$('#cover-details').collapse('hide');
		});
    	
    	$scope.$watch('occupationForm.$invalid', function(newVal, oldVal) {
    		$('#cover-details').collapse('hide');  		
    	});
    	
    /*	$scope.$watch('occupationForm.annualSalary.$invalid', function(newVal, oldVal) {
    		$('#cover-details').collapse('hide');  		
    	});*/
    	
    });  

  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
    this.IndustryOptions = this._CommonAPI.getIndustryList();
    
    this._CommonAPI.getAge65OccupationList(this.urlList.hostDecOccList).then(
    		(res) => {
    			this.ipRestrictedOccuaptionList = res;
    			console.log(this.ipRestrictedOccuaptionList);
    		},
    		(err) => {
		          this.errors = err.data.errors;		        
    		}	  
    );
    
    this.applicantData = this._EapplyData.getApplicantData() || this._EapplyData.exitOnError();
    this.applicantData.applicant.cover = [];
    this.applicantData.applicant.cover[0] = this._EapplyData.getExistingDeathCover();
    this.applicantData.applicant.cover[1] = this._EapplyData.getExistingTpdCover();
    this.applicantData.applicant.cover[2] = this._EapplyData.getExistingIpCover();
    
    this.changeCoverData = this._EapplyData.getChangeCoverData() ? angular.extend({}, this.changeCoverData, this._EapplyData.getChangeCoverData()) : angular.copy(this.applicantData);
    
    this._EapplyData.populateData(this.changeCoverData);
    
    console.log(JSON.stringify(this.changeCoverData));
    
    if(parseInt(this.changeCoverData.applicant.insuredSalary) === 75){
		this.insuredIP1 = true;
	}else if(parseInt(this.changeCoverData.applicant.insuredSalary) === 85){
		this.insuredIP2 = true;
	}
    
    if(this._EapplyData.getChangeCoverData()){
    	
    	this._CommonAPI.fetchOccupationList(this.urlList.occupationUrl, this.changeCoverData.applicant.industryType).then(
    			(res) => {
      		        this.occupationList = res;
      		      },
      		      (err) => {
      		        this.errors = err.data.errors;
      		      }
        );
    }
    
    if(!this.changeCoverData.applicant.cover[0].coverOption){
    	this.changeCoverData.applicant.cover[0].coverOption = 'No Change'
    	this.changeCoverData.applicant.cover[1].coverOption = 'No Change';
    }
    
    this.changeCoverData.applicant.cover[2].occupationRating = this.changeCoverData.applicant.cover[2].occupationRating ? this.changeCoverData.applicant.cover[2].occupationRating : this.changeCoverData.applicant.cover[0].occupationRating;
    this.changeCoverData.requestType = 'CCOVER';    
    
  }

  changePrefContactType() {
	    if (this.changeCoverData.applicant.contactDetails.preferedContacType == 'Home') {
	      this.changeCoverData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.homePhone;
	    } else if (this.changeCoverData.applicant.contactDetails.preferedContacType == 'Work') {
	      this.changeCoverData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.workPhone;
	    } else if (this.changeCoverData.applicant.contactDetails.preferedContacType == 'Mobile') {
	      this.changeCoverData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.mobilePhone;
	    } else {
	      this.changeCoverData.applicant.contactDetails.preferedContactNumber = '';
	    }	  
  }
  
  revertIpSection(){
	  
	  if(this.changeCoverData.applicant.workHours == 'No'){		  
		  this.changeCoverData.applicant.cover[2].additionalCoverAmount = this.changeCoverData.applicant.cover[2].existingCoverAmount;
		  this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod = this.changeCoverData.applicant.cover[2].existingIpBenefitPeriod;
		  this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod = this.changeCoverData.applicant.cover[2].existingIpWaitingPeriod;
		  this.benefitPeriodIPErrorFlag = false;
		  this.insuredIP1 = false
		  this.insuredIP2= false;
		  this.insuredMaxIPFlag = false;
		  this.calculate();		  
	  }else{
		  this.checkBenefitPeriod();
	  }
  }

  getOccupations() {
	  this.changeCoverData.applicant.occupation = '';
	  this.changeCoverData.applicant.otherOccupation = '';
	  this.changeCoverData.applicant.industryType = this.changeCoverData.applicant.industryType === '' ? null : this.changeCoverData.applicant.industryType;
    this._CommonAPI.fetchOccupationList(this.urlList.occupationUrl, this.changeCoverData.applicant.industryType).then(
      (res) => {
        this.occupationList = res;
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );
  }
  
  getOccupationRating(){
	  
	  if(this.changeCoverData.applicant.occupation !== undefined){
		  
		  var occName = this.changeCoverData.applicant.industryType + ':' + this.changeCoverData.applicant.occupation;
		  
		  this._CommonAPI.getOccupationRating(this.urlList.newOccupationUrl, 'INGD', occName).then(
				  (res) => {
					  this.changeCoverData.applicant.cover[0].occupationRating = res[0].deathfixedcategeory;
					  this.changeCoverData.applicant.cover[1].occupationRating = res[0].tpdfixedcategeory;
					  this.changeCoverData.applicant.cover[2].occupationRating = res[0].ipfixedcategeory;
					  this.checkBenefitPeriod();
				  },
			      (err) => {
			          this.errors = err.data.errors;
			        }
		  );
	   } 
  }
  
  changeFrequency(){
	  
	  this.changeCoverData.applicant.cover[1].frequencyCostType = this.changeCoverData.applicant.cover[0].frequencyCostType;
	  this.changeCoverData.applicant.cover[2].frequencyCostType = this.changeCoverData.applicant.cover[0].frequencyCostType;
	  
	  this.calculate();
	  
  }
  
  changeMode(type){
	  this.changeCoverData.applicant.cover[0].coverCategory = 'DcTailored';
	  this.changeCoverData.applicant.cover[1].coverCategory = 'TPDTailored';
	  this.changeCoverData.applicant.cover[2].coverCategory = 'IpTailored';
	  
	  var bothCoverChanged = false;
	  
	  if(this.changeCoverData.applicant.cover[0].coverOption !== 'No Change' && this.changeCoverData.applicant.cover[1].coverOption !== 'No Change'){
		  if(type === 'death'){
			  bothCoverChanged = true;
			  this.changeCoverData.applicant.cover[1].coverOption = this.changeCoverData.applicant.cover[0].coverOption;
		  }else if(type === 'tpd'){
			  bothCoverChanged = true;
			  this.changeCoverData.applicant.cover[0].coverOption = this.changeCoverData.applicant.cover[1].coverOption;  
		  }
		  
		  this.changeCoverData.applicant.cover[0].additionalCoverAmount = '';
		  this.changeCoverData.applicant.cover[1].additionalCoverAmount = '';
		  
		  this._$scope.coverCalculatorForm.deathRequireCover.$touched = true;
		  this._$scope.coverCalculatorForm.TPDrequireCover.$touched = true;
		  
		  if(this.changeCoverData.applicant.cover[0].coverOption === 'Life Stage'){
			  
			  this.minDeathErrorFlag = false;
			  this.minTPDErrorFlag = false;
			  this.maxTPDErrorFlag = false;
			  
			  this.calculateLifeStageDeath().then((res) => {
				  this.calculateLifeStageTPD().then((resp) => {
					  this.calculate();
				  });
			  });
		  }
	  }
	  
	  if(!bothCoverChanged){
		  
		  if(type === 'death'){
			  
			  if(this.changeCoverData.applicant.cover[0].coverOption === 'Life Stage'){
				  
				  this.minDeathErrorFlag = false;
				  
				  this.calculateLifeStageDeath().then((res) => {				  
					this.calculate();
				  });
				  
			  }else if(this.changeCoverData.applicant.cover[0].coverOption === 'No Change'){
				  
				  this.minDeathErrorFlag = false;
				  this.changeCoverData.applicant.cover[0].coverCategory = this.changeCoverData.applicant.cover[0].existingCoverCategory;
				  this.changeCoverData.applicant.cover[0].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
				  
				  if(this.changeCoverData.applicant.cover[1].coverOption === 'No Change'){
					  this.changeCoverData.applicant.cover[1].coverCategory = this.changeCoverData.applicant.cover[1].existingCoverCategory; 
				  }
				  
				  this.calculate();			  
			  }else{
				  this.changeCoverData.applicant.cover[0].additionalCoverAmount = '';
				  this._$scope.coverCalculatorForm.deathRequireCover.$touched = true;
			  }
			  
		  }
		  
		  if(type === 'tpd'){
			  
			  if(this.changeCoverData.applicant.cover[1].coverOption === 'Life Stage'){
				  
				  this.minTPDErrorFlag = false;
				  this.maxTPDErrorFlag = false;
				  
				  this.calculateLifeStageTPD().then((res) => {				  
					this.calculate();
				  });
			  }else if(this.changeCoverData.applicant.cover[1].coverOption === 'No Change'){
				  
				  this.minTPDErrorFlag = false;
				  this.maxTPDErrorFlag = false;
				  
				  this.changeCoverData.applicant.cover[1].coverCategory = this.changeCoverData.applicant.cover[1].existingCoverCategory;
				  this.changeCoverData.applicant.cover[1].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
				  
				  if(this.changeCoverData.applicant.cover[0].coverOption === 'No Change'){
					  this.changeCoverData.applicant.cover[0].coverCategory = this.changeCoverData.applicant.cover[0].existingCoverCategory; 
				  }
				  this.calculate();		  
			 }else{				 
				 this.changeCoverData.applicant.cover[1].additionalCoverAmount = '';
				 this._$scope.coverCalculatorForm.TPDrequireCover.$touched = true;
			 }
		  }		  
	  }
	  	 
  }
  
  /*changeMode(type){
	  
	  this.changeCoverData.applicant.cover[0].coverCategory = 'DcTailored';
	  this.changeCoverData.applicant.cover[1].coverCategory = 'TPDTailored';
	  this.changeCoverData.applicant.cover[2].coverCategory = 'IpTailored';
	  
	  if(this.changeCoverData.applicant.cover[0].existingCoverCategory === 'DcAutomatic'){
		  
		  this.changeCoverData.applicant.cover[0].additionalCoverAmount = '';
		  this.changeCoverData.applicant.cover[1].additionalCoverAmount = '';
		  
		  this._$scope.coverCalculatorForm.deathRequireCover.$touched = true;
		  this._$scope.coverCalculatorForm.TPDrequireCover.$touched = true;
		  
		  if(type === 'death'){
			  this.changeCoverData.applicant.cover[1].coverOption = this.changeCoverData.applicant.cover[0].coverOption;
		  }else if(type === 'tpd'){
			  this.changeCoverData.applicant.cover[0].coverOption = this.changeCoverData.applicant.cover[1].coverOption;  
		  }
		  
		  if(this.changeCoverData.applicant.cover[0].coverOption === 'No Change'){
			  this.minDeathErrorFlag = false;
			  this.minTPDErrorFlag = false;
			  this.maxTPDErrorFlag = false;
			  
			  this.changeCoverData.applicant.cover[0].coverCategory = this.changeCoverData.applicant.cover[0].existingCoverCategory;
			  this.changeCoverData.applicant.cover[1].coverCategory = this.changeCoverData.applicant.cover[1].existingCoverCategory;
			  
			  this.changeCoverData.applicant.cover[0].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
			  this.changeCoverData.applicant.cover[1].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
			  
			  this.calculate();
		  }
		  
		  if(this.changeCoverData.applicant.cover[0].coverOption === 'Life Stage'){
			  
			  this.calculateLifeStageDeath().then((res) => {
				  this.calculateLifeStageTPD().then((resp) => {
					  this.calculate();
				  });
			  });
		  }
		  
	  }else{
		  
		  if(type === 'death'){
			  this.changeCoverData.applicant.cover[0].additionalCoverAmount = '';
			  
			  if(this.changeCoverData.applicant.cover[0].coverOption === 'No Change'){
				  
				  this.minDeathErrorFlag = false;
				  this.changeCoverData.applicant.cover[0].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
				  this.changeCoverData.applicant.cover[0].coverCategory = this.changeCoverData.applicant.cover[0].existingCoverCategory;			
				  
				  if(this.changeCoverData.applicant.cover[1].coverOption === 'Life Stage'){
					  
					  this.changeCoverData.applicant.cover[1].coverOption = 'No Change';
					  this.changeCoverData.applicant.cover[1].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
				  }
				  
				  this.calculate();
				  
			  }else if(this.changeCoverData.applicant.cover[0].coverOption === 'Life Stage'){
				  
				  this.minDeathErrorFlag = false;
				  this.changeCoverData.applicant.cover[0].coverOption = 'Life Stage';
				  this.changeCoverData.applicant.cover[1].coverOption = 'Life Stage';
				  
				  this.calculateLifeStageDeath().then((res) => {
					  this.calculateLifeStageTPD().then((resp) => {
						  this.calculate();
					  });
				  });
			  }else{
				  
				  if(this.changeCoverData.applicant.cover[1].coverOption === 'Life Stage'){
					  
					  this.changeCoverData.applicant.cover[1].coverOption = 'No Change';
					  this.minTPDErrorFlag = false;
					  this.maxTPDErrorFlag = false;	 
					  this.changeCoverData.applicant.cover[1].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
				  }
				  
				  this.calculate();
			  }
			  
			  this._$scope.coverCalculatorForm.deathRequireCover.$touched = true;
			  
		  }else if(type === 'tpd'){
			  this.changeCoverData.applicant.cover[1].additionalCoverAmount = '';
			  
			  if(this.changeCoverData.applicant.cover[1].coverOption === 'No Change'){
				  
				  this.minTPDErrorFlag = false;
				  this.maxTPDErrorFlag = false;	  
				  this.changeCoverData.applicant.cover[1].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
				  this.changeCoverData.applicant.cover[1].coverCategory = this.changeCoverData.applicant.cover[1].existingCoverCategory;
				  
				  if(this.changeCoverData.applicant.cover[0].coverOption === 'Life Stage'){
					  
					  this.changeCoverData.applicant.cover[0].coverOption = 'No Change';
					  this.changeCoverData.applicant.cover[0].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
				  }
				  
				  this.calculate();
				  
			  }else if(this.changeCoverData.applicant.cover[1].coverOption === 'Life Stage'){
				  
				  this.minTPDErrorFlag = false;
				  this.maxTPDErrorFlag = false;	
				  this.changeCoverData.applicant.cover[0].coverOption = 'Life Stage';
				  this.changeCoverData.applicant.cover[1].coverOption = 'Life Stage';
				  
				  this.calculateLifeStageDeath().then((res) => {
					  this.calculateLifeStageTPD().then((resp) => {
						  this.calculate();
					  });
				  });
			  }else{
				  
				  if(this.changeCoverData.applicant.cover[0].coverOption === 'Life Stage'){
					  this.minDeathErrorFlag = false;
					  this.changeCoverData.applicant.cover[0].coverOption = 'No Change';
					  this.changeCoverData.applicant.cover[0].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
				  }
				  
				  this.calculate();
			  }
			  
			  this._$scope.coverCalculatorForm.TPDrequireCover.$touched = true;
			  
		  }
	  }
	  
	  
  }*/
  
  checkDeathValidation(){
	  
	  this.deathMinLimit = 10000;
	  this.minDeathErrorFlag = false;	  
	  
	  if(this.changeCoverData.applicant.cover[0].additionalCoverAmount < this.deathMinLimit){
		  this.minDeathErrorFlag = true;
	  }
	  
	  if(!this.minDeathErrorFlag){
		  this.changeCoverData.applicant.cover[0].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount);
		  this.calculate();
	  }else{
		  this.changeCoverData.applicant.cover[0].cost = 0;
		  this.calculateTotalPremium();
	  }
  }
  
  checkTPDPValidation(){
	  
	  this.TPDMinLimit = 10000;
	  this.TPDMaxLimit = 5000000;
	  
	  this.minTPDErrorFlag = false;
	  this.maxTPDErrorFlag = false;	  
	  
	  
	  if(this.changeCoverData.applicant.cover[1].additionalCoverAmount < this.TPDMinLimit){
		  this.minTPDErrorFlag = true;
	  }
	  
	  if(this.changeCoverData.applicant.cover[1].additionalCoverAmount > this.TPDMaxLimit){
		  this.maxTPDErrorFlag = true;
	  }
	  
	  if(!this.minTPDErrorFlag && !this.maxTPDErrorFlag){
		  this.changeCoverData.applicant.cover[1].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount);
		  this.calculate();
	  }else{
		  this.changeCoverData.applicant.cover[1].cost = 0;
		  this.calculateTotalPremium();
	  }  
	  
  }
  
  checkIPValidation(){	  
	  
	  if(this.ipChanged){
		  
		  if(parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount) === 0 && parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) === 0){
			  this.IPMinLimit = 0; 
		  }else{
			  this.IPMinLimit = 1000; 
		  }
		  
		  this.IPMaxLimit = 30000;
		  
		  this.minIPErrorFlag = false;
		  this.insuredMaxIPFlag = false;
		  
		  var calculatedAmount = Math.round((0.85 * (this.changeCoverData.applicant.annualSalary / 12)));
		  
		  if(calculatedAmount < this.changeCoverData.applicant.cover[2].additionalCoverAmount){
			  this.changeCoverData.applicant.cover[2].additionalCoverAmount = calculatedAmount;
			  this.insuredMaxIPFlag = true;
		  }
		  
		  if(this.changeCoverData.applicant.cover[2].additionalCoverAmount < this.IPMinLimit){
			  this.minIPErrorFlag = true;
		  }
		  
		  if(this.changeCoverData.applicant.cover[2].additionalCoverAmount > this.IPMaxLimit){
			  this.changeCoverData.applicant.cover[2].additionalCoverAmount  = this.IPMaxLimit ;
			  this.insuredMaxIPFlag = true;
		  }
		  
		  if(!this.minIPErrorFlag){
			  this.changeCoverData.applicant.cover[2].additionalCoverAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount);
			  this.calculate();
		  }else{
			  this.changeCoverData.applicant.cover[2].cost = 0;
			  this.calculateTotalPremium();
		  }
	  }else{
		  this.calculate();
	  }
	  
  }
  
  checkBenefitPeriod(){
	  
	  const _this = this;
	  var occSearch = true;
	  
	  if(_this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod == 'Age 67'){		  
		  
		  angular.forEach(_this.ipRestrictedOccuaptionList,function(value,key){			  
			  
			  if(occSearch){
				  if(key.toLowerCase() === _this.changeCoverData.applicant.occupation.toLowerCase()){
					  if(value.toLowerCase() == 'ip'){
						  _this.benefitPeriodIPErrorFlag = true;
	    				  occSearch = false;
	    			  }
	    		  }else{
	    			  _this.benefitPeriodIPErrorFlag = false;
			      }
			  }
		 });
		  if(!_this.benefitPeriodIPErrorFlag){
			  _this.calculate();
		  } 
		  
	  }else{
		  _this.benefitPeriodIPErrorFlag = false;
		  _this.calculate();
	  }
	  	  
  }
  
  checkValidSalary(){
	  this.salaryErrorFlag = false;
	  this.minSalaryErrorFlag = false;
	  this.maxSalaryErrorFlag = false;
	  
	  if(parseInt(this.changeCoverData.applicant.annualSalary) > 0 && parseInt(this.changeCoverData.applicant.annualSalary) <=500000){
		  this.changeCoverData.applicant.annualSalary = parseInt(this.changeCoverData.applicant.annualSalary);
		  if(this.insuredIP1 || this.insuredIP2){
			  this.insuredIPCalculation();
		  }else{
			  this.checkIPValidation();
		  }
	  }else if(parseInt(this.changeCoverData.applicant.annualSalary) > 500000){
		  this.changeCoverData.applicant.annualSalary = parseInt(this.changeCoverData.applicant.annualSalary);
		  this.salaryErrorFlag = true;
		  this.maxSalaryErrorFlag = true;
		  this._$timeout(function(){
			  $('#cover-details').collapse('hide'); 
		  }, 400);
	  }else{
		  //this.changeCoverData.applicant.annualSalary = ''; 
		  this.salaryErrorFlag = true;
		  this.minSalaryErrorFlag = true;
		  this._$timeout(function(){
			  $('#cover-details').collapse('hide'); 
		  }, 400);
		  
	  }	  
  }
  
  insuredIPCalculation(){
	  
	  this.insuredMaxIPFlag = false;
	  this.insuredIP1Value = 75;
	  this.insuredIP2Value = 85;
	  	  
	  if(this.insuredIP1){
		  this.insuredIP = this.insuredIP1Value;
		  this.changeCoverData.applicant.insuredSalary = this.insuredIP1Value;
	  }
	  
	  if(this.insuredIP2){
		  this.insuredIP = this.insuredIP2Value;
		  this.changeCoverData.applicant.insuredSalary = this.insuredIP2Value;
	  }
	  
	  if(!this.insuredIP1 && !this.insuredIP2){
		  this.changeCoverData.applicant.cover[2].additionalCoverAmount = ''; 
		  this.changeCoverData.applicant.insuredSalary = null;
		  return false;
	  }
	  
	  var IPMax = 30000;
	  
	  var calculatedAmount = Math.round((this.insuredIP/100) * (this.changeCoverData.applicant.annualSalary / 12));
	  
	  if(calculatedAmount < IPMax){
		  this.changeCoverData.applicant.cover[2].additionalCoverAmount  = calculatedAmount;
		  this.insuredMaxIPFlag = false;
	  }else{
		  this.changeCoverData.applicant.cover[2].additionalCoverAmount = IPMax;
		  this.insuredMaxIPFlag = true;
	  }
	  
	  this.checkIPValidation();
  }
  
  calculateLifeStageDeath(){
	  
	  let deferred = this._$q.defer();
	  
	  var deathLifeCoverPath = this.urlList.calculateINGDDeath +'?fundCode=INGD&age='+this.changeCoverData.applicant.age;
	  
	  this._CommonAPI.calculateINGLifeCover(deathLifeCoverPath).then(
			  (res) => {
				  this.changeCoverData.applicant.cover[0].additionalCoverAmount = res.coverAmount;
				  deferred.resolve({});
			  },
		      (err) => {
		          this.errors = err.data.errors;
		          deferred.reject(err);
		        }
	  );
	  
	  return deferred.promise;
  }
  
  calculateLifeStageTPD(){
	  
	  let deferred = this._$q.defer();
	  
	  var TPDLifeCoverPath  = this.urlList.calculateINGDTPD + '?fundCode=INGD&age='+this.changeCoverData.applicant.age;
	  
	  this._CommonAPI.calculateINGLifeCover(TPDLifeCoverPath).then(
			  (res) => {
				  this.changeCoverData.applicant.cover[1].additionalCoverAmount = res.coverAmount;
				  deferred.resolve({});
		      },
		      (err) => {
		          this.errors = err.data.errors;
		          deferred.reject(err);
		        }
	  );
	  
	  return deferred.promise;
  }

  calculate() {
	  
	  const calculateJSON = {  
			   'age': this.changeCoverData.applicant.age,
			   'fundCode': 'INGD',
			   'gender': this.changeCoverData.applicant.gender,
			   'deathOccCategory': this.changeCoverData.applicant.cover[0].occupationRating,
			   'tpdOccCategory': this.changeCoverData.applicant.cover[1].occupationRating,
			   'ipOccCategory': this.changeCoverData.applicant.cover[2].occupationRating,
			   'smoker': false,
			   'deathFixedAmount': this.changeCoverData.applicant.cover[0].additionalCoverAmount,
			   'tpdFixedAmount': this.changeCoverData.applicant.cover[1].additionalCoverAmount,
			   'annualSalary': this.changeCoverData.applicant.annualSalary,
			   'ipFixedAmount': this.changeCoverData.applicant.cover[2].additionalCoverAmount,
			   'premiumFrequency': this.changeCoverData.applicant.cover[0].frequencyCostType,
			   'deathCoverType': this.changeCoverData.applicant.cover[0].coverCategory,
			   'tpdCoverType': this.changeCoverData.applicant.cover[1].coverCategory,
			   'ipCoverType': this.changeCoverData.applicant.cover[2].coverCategory,
			   'manageType': 'CCOVER',
			   'ipBenefitPeriod': this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod,
			   'ipWaitingPeriod': this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod,
			   'stateCode': this.changeCoverData.applicant.contactDetails.state,
			   'deathExistingAmount': this.changeCoverData.applicant.cover[0].existingCoverAmount,			   
			   'tpdExistingAmount': this.changeCoverData.applicant.cover[1].existingCoverAmount,
			   'ipExistingAmount': this.changeCoverData.applicant.cover[2].existingCoverAmount,
			   'deathCoverStartDate': this.applicantData.applicant.cover[0].coverStartDate,	
			   'tpdCoverStartDate': this.applicantData.applicant.cover[1].coverStartDate,	
			   'ipCoverStartDate': this.applicantData.applicant.cover[2].coverStartDate,	
			}; 
	  
	  
    console.log(JSON.stringify(calculateJSON));
    
    this._CommonAPI.calculateINGAll(this.urlList.calculateINGDAll, calculateJSON).then(
      (res) => {
    	  
    	  var premium = res;
    	  
    	  
    	  for(var i = 0; i < premium.length; i++){
			   if(premium[i].coverType === 'DcTailored' || premium[i].coverType === 'DcAutomatic'){
				   this.changeCoverData.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
				   this.changeCoverData.applicant.cover[0].cost = premium[i].cost || 0;
			   }else if(premium[i].coverType === 'TPDTailored' || premium[i].coverType === 'TPDAutomatic'){
				   this.changeCoverData.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
				   this.changeCoverData.applicant.cover[1].cost = premium[i].cost || 0;
			   }else if(premium[i].coverType === 'IpTailored'){
				   this.changeCoverData.applicant.cover[2].additionalCoverAmount = premium[i].coverAmount||0.00;
				   this.changeCoverData.applicant.cover[2].cost = premium[i].cost || 0;
			   }
			}
    	  
    	  if(!this._CommonAPI.getAppNumber()){
    		  this._CommonAPI.generateAppNumber(this.urlList.appNumUrl).then(
    				  (res) => {
    					  this.changeCoverData.applicatioNumber = this._CommonAPI.getAppNumber();
    			  },
    			  (err) => {
    	              this.errors = err.data.errors;
    	            }
    		  );
    	  }else{
    		  this.changeCoverData.applicatioNumber = this._CommonAPI.getAppNumber();
    	  }
    	  
    	  this.calculateTotalPremium();
    	  this.checkAuraOption();
    	  
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );

  }
  
  calculateTotalPremium(){
	  this.changeCoverData.totalMonthlyPremium = parseFloat(this.changeCoverData.applicant.cover[0].cost) + parseFloat(this.changeCoverData.applicant.cover[1].cost) + parseFloat(this.changeCoverData.applicant.cover[2].cost);
	  
	  console.log(this.changeCoverData.applicant.cover[0].additionalCoverAmount, this.changeCoverData.applicant.cover[1].additionalCoverAmount, this.changeCoverData.applicant.cover[2].additionalCoverAmount);
	  console.log(this.changeCoverData.applicant.cover[0].cost, this.changeCoverData.applicant.cover[1].cost, this.changeCoverData.applicant.cover[2].cost);
    
  }
  
  checkAuraOption(){
	  
	  this.changeCoverData.auraDisabled = true;
	  
	  // to check Automatic to Tailored
	  if(this.changeCoverData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.changeCoverData.applicant.cover[0].coverCategory === 'DcTailored'){
		  this.changeCoverData.auraDisabled = false; 
	  }
	  
	  //Death increase
	  if(parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) > parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount)){
		  this.changeCoverData.auraDisabled = false; 
	  }
	  
	  //TPD increase
	  if(parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount) > parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount)){
		  this.changeCoverData.auraDisabled = false; 
	  }
	  
	  //IP increase
	  if(parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) > parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount)){
		  this.changeCoverData.auraDisabled = false; 
	  }
	  
	  //Benefit Period change
	  
	  if(this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod != this.changeCoverData.applicant.cover[2].existingIpBenefitPeriod){
		  this.changeCoverData.auraDisabled = false; 
	  }
	  
	  //Waiting Period change
	  
	  if(this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod != this.changeCoverData.applicant.cover[2].existingIpWaitingPeriod){
		  this.changeCoverData.auraDisabled = false; 
	  }
	  
	  if(this.changeCoverData.auraDisabled == false){
		 this.setAuraType(); 
	  }
  }
  
  setAuraType(){	  
	  
	  const auraTypeJSON = {  
			  'age': this.changeCoverData.applicant.age,
			  'fundCode':'INGD',
			  'annualSalary': parseInt(this.changeCoverData.applicant.annualSalary),
			  'deathFixedAmount': this.changeCoverData.applicant.cover[0].additionalCoverAmount,
			  'tpdFixedAmount': this.changeCoverData.applicant.cover[1].additionalCoverAmount,
			  'ipFixedAmount':this.changeCoverData.applicant.cover[2].additionalCoverAmount,
			  'deathCoverType':this.changeCoverData.applicant.cover[0].coverCategory,
			  'tpdCoverType':this.changeCoverData.applicant.cover[1].coverCategory,
			  'ipCoverType':this.changeCoverData.applicant.cover[2].coverCategory,
			  'deathExistingAmount':this.changeCoverData.applicant.cover[0].existingCoverAmount,	
			  'tpdExistingAmount':this.changeCoverData.applicant.cover[1].existingCoverAmount,	
			  'ipExistingAmount':this.changeCoverData.applicant.cover[2].existingCoverAmount,
			  'ipBenefitPeriod': this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod,
			  'ipWaitingPeriod':this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod
		};
	  
	  this._CommonAPI.getAuraType(this.urlList.getAuraTypeURL, auraTypeJSON).then(
		      (res) => {
		    	  this.changeCoverData.auraType = res;
		    	  console.log("Aura Type: "+this.changeCoverData.auraType);
		      },
		      (err) => {
		        this.errors = err.data.errors;
		      }
	  );
  }
  
  save() {
	  
	  if(!this.benefitPeriodIPErrorFlag && !this.minIPErrorFlag  && !this.maxTPDErrorFlag && !this.minTPDErrorFlag && !this.minDeathErrorFlag){
	  
		  this.changeCoverData.lastSavedOn = 'quotepage';
		  this.setOccupation();
		  
		  console.log(JSON.stringify(this.changeCoverData));
		  
		  this._CommonAPI.saveEapply(this.urlList.saveEapplyUrlNew, this.changeCoverData).then(
				  (res) => {
					  console.log(res);
					  this.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+this.changeCoverData.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
				  },
				  (err) => {
					  this.errors = err.data.errors;
				  }); 
	  }
  }
  
  setOccupation(){
	  
	  var industryType = this.changeCoverData.applicant.industryType;
	  
	  var selectedIndustry = this.IndustryOptions.filter(function(obj){
          return  industryType === obj.key;
      });
	  
	  this.changeCoverData.applicant.occupationCode = selectedIndustry[0].value;
      
  }
  
  saveAndExitPopUp(hhText) {
	  
	  this._ngDialog.openConfirm({
		  template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Finish &amp; Close Window </button></div></div>',
		  plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
      }).then(
      () => {
        this._$state.go('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }
  
  print() {
	    console.log(JSON.stringify(this.changeCoverData));
	    if(!this.benefitPeriodIPErrorFlag && !this.minIPErrorFlag  && !this.maxTPDErrorFlag && !this.minTPDErrorFlag && !this.minDeathErrorFlag){
		    this._CommonAPI.printEapply(this.urlList.printQuotePageNew, this.changeCoverData).then(
		      (res) => {
		        this.downloadPDF(res.clientPDFLocation);
		        console.log(res);
		      },
		      (err) => {
		        this.errors = err.data.errors;
		      }
		    );
	    }
  }
  
  downloadPDF(pdfLocation){
  	var filename = null;
  	var a = null;
  	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  	a = document.createElement('a');
  	document.body.appendChild(a);
  	
  	this._CommonAPI.downloadPDF(this.urlList.downloadUrl, pdfLocation).then(
			  (res) => {
				  if(navigator.appVersion.toString().indexOf('.NET') > 0) {
					  window.navigator.msSaveBlob(res.response,filename);
				  }else{
					  var fileURL = URL.createObjectURL(res.response);
					  a.href = fileURL;
					  a.download = filename;
					  a.click();
				  }
			  }, 
            (err) => {
              console.log('Error while Downloading the PDF' + err);
            }
	  );
  	
  }
  
  checkForProceed(){
	  this.stopProceed = false;
	  
	  if(this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod == this.changeCoverData.applicant.cover[2].existingIpWaitingPeriod && this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod == this.changeCoverData.applicant.cover[2].existingIpBenefitPeriod && parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) === parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount) && parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount) === parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount) && parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) === parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount)){
		  this.stopProceed = true;
	  }
  }
  
  checkForCoverDecrease(){
	  
	  var coverList;
	  var coverMsg;
	  this.deathDecreaseFlag = false;
	  this.TPDDecreaseFlag = false;
	  this.IPDecreaseFlag = false;
	  
	  if(parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) < parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount)){
		  this.deathDecreaseFlag = true;
	  }
	  
	  if(parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) == 0 && parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount) != 0){
		  this.deathDecreaseFlag = true;
	  }
	  
	  if(parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount) < parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount)){
		  this.TPDDecreaseFlag = true;
	  }
	  
	  if(parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount) == 0 && parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount) != 0){
		  this.TPDDecreaseFlag = true;
	  }
	  
	  if(parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) < parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount)){
		  this.IPDecreaseFlag = true;
	  }	
	  
	  if(parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) == 0 && parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount) != 0){
		  this.IPDecreaseFlag = true;
	  }
	  
	  if(this.deathDecreaseFlag && this.TPDDecreaseFlag && this.IPDecreaseFlag){
		  coverList = "Death, TPD & IP";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }else if(this.deathDecreaseFlag && this.TPDDecreaseFlag){
		  coverList = "Death & TPD";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }else if(this.deathDecreaseFlag && this.IPDecreaseFlag){
		  coverList = "Death & IP";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }else if(this.TPDDecreaseFlag && this.IPDecreaseFlag){
		  coverList = "TPD & IP";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }else if(this.deathDecreaseFlag){
		  coverList = "Death";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }else if(this.TPDDecreaseFlag){
		  coverList = "TPD";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }else if(this.IPDecreaseFlag){
		  coverList = "IP";
		  coverMsg="You are requesting to cancel/decrease your " + coverList+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
	  }
	  
	  if(this.deathDecreaseFlag || this.TPDDecreaseFlag || this.IPDecreaseFlag){
		  this.showDecreasePopup(coverMsg);
	  }else{
		  this.proceed(); 
	  }
	  
	}
  
  showDecreasePopup(coverMsg){
	  
	  this._ngDialog.openConfirm({
	      template:'<div class="ngdialog-content"><div class="modal-body"><div class="row"><div class="col-12"><h4 class="modal-title aligncenter">Change cover</h4><p>'+coverMsg+'</p> </div> </div></div></br><div class="ngdialog-footer"><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button></div></div>',
	      plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
	    }).then(
	      () => {
	    	  this.proceed();
	      }, 
	      (event) => {
	        if(event === 'oncancel') {
	          return false;
	        }
	      }
	    );
  }
  
  proceed(){
	  
	  if(this.changeCoverData.auraDisabled == false || this.changeCoverData.auraDisabled == "false"){
		  this._$state.go('app.quoteaura');
	  }else{
		  this._$state.go('app.quotesummary');
	  } 
  }

  continue() {
	  
	  if(!this.benefitPeriodIPErrorFlag && !this.minIPErrorFlag  && !this.maxTPDErrorFlag && !this.minTPDErrorFlag && !this.minDeathErrorFlag){
		  this.setOccupation();
		  this.checkForProceed();
		  
		  if(!this.stopProceed){
			  this._EapplyData.setChangeCoverData(this.changeCoverData);
			  this.checkForCoverDecrease();
			  
		  }else{
			  console.log("stop the application");
			  var templateContent = '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Cover status </h4>';
			  templateContent += '<div class="row rowcustom"><div class="col-sm-12">  <p class="aligncenter"></p>';
			  templateContent += '<div id="tips_text">Not allowed to procced further since there is no change in cover</div></div></div></div>';
			  templateContent += '<div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Close Window </button></div></div>';
			  
			  this._ngDialog.openConfirm({
				  template: templateContent,
				  plain: true,
			      showClose: false,
			      className: 'ngdialog-theme-plain custom-width'
		      }).then(
		      () => {	    	  
		      }, 
		      (event) => {	    	  
		      }
		    );
		  }	 
	  }
  }
  
  isCollapsable(element, event){
	  
	var formdutyValid = this._$scope.formduty.$valid;
  	var privacyFormValid = this._$scope.privacyForm.$valid;
  	var memberDetailsValid = this._$scope.memberDetails.$valid;
  	var occupationFormValid = this._$scope.occupationForm.$valid;
  	/*var salaryField = this._$scope.occupationForm.annualSalary.$valid;*/
  	var stopPropagation = false;
  	
  	if(element === 'privacy' && !formdutyValid){
  		this._$scope.formduty.$submitted = true;
		stopPropagation = true;
    }else if(element === 'memberDetailSection' && (!formdutyValid || !privacyFormValid)) {
    	this._$scope.privacyForm.$submitted = true;
    	stopPropagation = true;
    }else if(element === 'OccupationSection' && (!formdutyValid || !privacyFormValid || !memberDetailsValid)) {
    	this._$scope.memberDetails.$submitted = true;
    	stopPropagation = true;
    }else if(element === 'CoverSection' && (this.salaryErrorFlag || !formdutyValid || !privacyFormValid || !memberDetailsValid || !occupationFormValid)){
    	this._$scope.occupationForm.$submitted=true;
    	stopPropagation = true;
    }
    
    if(stopPropagation){
    	event.stopPropagation();
    	return false;
    }
  	
  }
  
  checkPreviousMandatoryFields(formName, elementName){
  	
	var memberDetailsFormFields = ['contactEmail','contactType','contactPhone','contactPrefTime','gender'];
	var occupationFormFields = ['fifteenHrsWork','industry','occupation','otherOccupation','annualSalary'];
	var coverCalculatorFormFields = ['deathRequireCover', 'TPDrequireCover', 'ipRequireCover'];
  	var currentFormFields = [];
  	
  	if(formName === 'memberDetails'){
  		currentFormFields = memberDetailsFormFields;
  	}else if(formName === 'occupationForm'){
  		currentFormFields = occupationFormFields;
    }else if(formName === 'coverCalculatorForm'){
  		currentFormFields = coverCalculatorFormFields;
  	}
  	
      var inx = currentFormFields.indexOf(elementName);	        
      
      if(inx > 0){
      	for(var i = 0; i < inx ; i++){
      		 if (this._$scope[formName][currentFormFields[i]]){
      			this._$scope[formName][currentFormFields[i]].$touched = true;
      		 }                        
      	}
      }
    }
  
  clickToOpen(hhText) {
	  this._ngDialog.open({
	      template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
	      className: 'ngdialog-theme-plain',
	      showClose: false,
	      plain: true
	    });
  }
}

export default QuoteCoverCtrl;
