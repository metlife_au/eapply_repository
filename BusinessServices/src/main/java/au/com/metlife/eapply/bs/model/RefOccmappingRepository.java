package au.com.metlife.eapply.bs.model;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
public interface  RefOccmappingRepository extends CrudRepository<RefOccmapping, Long>{
	public List<RefOccmapping> findByFundId(String fundId);	
	public List<RefOccmapping> findByFundIdAndOccName(String fundId, String occName);

}