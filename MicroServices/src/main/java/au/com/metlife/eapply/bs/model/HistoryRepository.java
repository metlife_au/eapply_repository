package au.com.metlife.eapply.bs.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
public interface  HistoryRepository extends JpaRepository<EappHistory, String>,QueryByExampleExecutor<EappHistory>{
	
		/*List<Applicant> find(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("dob") Timestamp dob);*/
}