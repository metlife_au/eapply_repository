package pages.metlife.Angular.HOSTPLUS;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_SpecialOffer extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_SpecialOffer SpecialOffer_HOSTPLUS() {
		SpecialOfferHOSTPLUS();
		healthandlifestyledetails();
		confirmation();

		return new HOSTPLUS_Angular_SpecialOffer(scriptHelper);
	}



	public HOSTPLUS_Angular_SpecialOffer(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void SpecialOfferHOSTPLUS() {

		String EmailId = dataTable.getData("HOSTPLUS_SpecialOffer", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_SpecialOffer", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_SpecialOffer", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_SpecialOffer", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_SpecialOffer", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_SpecialOffer", "FifteenHoursWork");
		String RegularIncome=dataTable.getData("HOSTPLUS_SpecialOffer", "RegularIncome");
		String Perform_WithoutRestriction=dataTable.getData("HOSTPLUS_SpecialOffer", "Perform_WithoutRestriction");
		String Work_WithoutLimitation=dataTable.getData("HOSTPLUS_SpecialOffer", "Work_WithoutLimitation");

		String Citizen = dataTable.getData("HOSTPLUS_SpecialOffer", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_SpecialOffer", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_SpecialOffer", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_SpecialOffer", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_SpecialOffer", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("HOSTPLUS_SpecialOffer", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_SpecialOffer", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_SpecialOffer", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_SpecialOffer", "AnnualSalary");

		String CostType = dataTable.getData("HOSTPLUS_SpecialOffer", "CostType");
		String PriorTPDBenefit = dataTable.getData("HOSTPLUS_SpecialOffer", "PriorTPDBenefit");

		String DeathYN = dataTable.getData("HOSTPLUS_SpecialOffer", "DeathYN");
		String TPDYN = dataTable.getData("HOSTPLUS_SpecialOffer", "TPDYN");
		String IPYN = dataTable.getData("HOSTPLUS_SpecialOffer", "IPYN");
		String DeathAmount = dataTable.getData("HOSTPLUS_SpecialOffer", "DeathAmount");
		String TPDAmount = dataTable.getData("HOSTPLUS_SpecialOffer", "TPDAmount");
		String IPAmount = dataTable.getData("HOSTPLUS_SpecialOffer", "IPAmount");
		String WaitingPeriod = dataTable.getData("HOSTPLUS_SpecialOffer", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("HOSTPLUS_SpecialOffer", "BenefitPeriod");





		try{



			//****Click on Special Offer for New Members Link*****\\

			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h2[contains(text(),'Special Offer For New Members')])[1]"))).click();
			report.updateTestLog("Special Offer For New Members", "Clicked on Special Offer for New members Link", Status.PASS);


			//*****Agree to Duty of disclosure*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);


            //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

            //*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


			//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

				//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath("(//div[@class='col-sm-6 pl0']/label/span)[1]")).click();

					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath("(//div[@class='col-sm-6 pl0']/label/span)[2]")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}


				//***********Select the Gender******************\\

				if(Gender.equalsIgnoreCase("Male")){

					driver.findElement(By.xpath("(//div[@class='col-sm-6 pl0']/label/span)[3]")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}
				else{
					driver.findElement(By.xpath("(//div[@class='col-sm-6 pl0']/label/span)[4]")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}

				//****Contact Details-Continue Button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);

				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\

					//*****Select the 15 Hours Question*******\\

				if(FifteenHoursWork.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

			//***Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*****\\

				if(RegularIncome.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
					sleep(500);
					report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
					sleep(300);

					//*****Perform duty without restriction due to illness or injury?*****\\

					if(Perform_WithoutRestriction.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
						sleep(500);
						report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected Yes", Status.PASS);
						sleep(300);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
						sleep(500);
						report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected No", Status.PASS);
						sleep(300);
					}
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
					sleep(500);
					report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
					sleep(300);

                       //*****Work Without Limitations *****\\

					if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
						sleep(500);
						report.updateTestLog("Work Without Limitations", "Selected Yes", Status.PASS);
						sleep(300);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
						sleep(500);
						report.updateTestLog("Work Without Limitations", "Selected No", Status.PASS);
						sleep(300);
					}

				}

				//*****Resident of Australia****\\

				/*if(Citizen.equalsIgnoreCase("Yes")){
					driver.findElement(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

					}
				else{
					driver.findElement(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

					}*/

				//*****Industry********\\
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("industry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(800);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
				sleep(500);

				//*****Occupation*****\\

				Select Occupation=new Select(driver.findElement(By.name("occupation")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(500);


				//*****Other Occupation *****\\

				if(OccupationType.equalsIgnoreCase("Other")){

					wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);
				}


				//*****Extra Questions*****\\

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[5]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[5]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[6]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[6]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			}
			}



			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



			//******Do you work in hazardous environment*****\\

			if(HazardousEnv.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[5]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[5]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			}

			//******Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?*******\\

			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[6]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[6]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
			}
			}


	          sleep(800);


			//*****What is your annual Salary******


			    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
			    sleep(500);
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);



			//*****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);

			//*************************************************\\
			//**************COVER CALCULATOR SECTION***************\\
			//**************************************************\\

			//*****Display the cost of my insurance cover as******\\

			Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
			CostofInsurance.selectByVisibleText(CostType);
			sleep(400);
			report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);


			//**** Have you previously been paid, or been entitled to receive a TPD benefit?******\\

			if(PriorTPDBenefit.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[7]"))).click();
				sleep(500);
				report.updateTestLog("Prior TPD benefit Paid?", "Yes is selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[7]"))).click();
				sleep(500);
				report.updateTestLog("Prior TPD benefit Paid?", "No is selected", Status.PASS);
			}





			//*****Death transfer amount******

			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);

			}

			//*****TPD transfer amount******

			if(TPDYN.equalsIgnoreCase("Yes")&&PriorTPDBenefit.equalsIgnoreCase("No")){

				wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

			}

			//****IP transfer amount*****\\



			if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){


			//*****Select The Waiting Period********\\

				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodAddnl']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(600);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);
				sleep(200);



			//*****Select The Benefit  Period********\\

				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodAddnl']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(600);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);
				sleep(200);


			//****Enter the IP amount*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar2"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);

			}



			//*****Click on calculate Quote*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
			sleep(2000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);


			if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4")).getText().equalsIgnoreCase("$0.00")){

			driver.findElement(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']")).click();
			sleep(4000);

			}

			if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4")).getText().equalsIgnoreCase("$0.00")){

				driver.findElement(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']")).click();
				sleep(4000);

			}

			//********************************************************************************************\\
			//************Capturing the premiums displayed************************************************\\
			//********************************************************************************************\\

			//*******Capturing the Death Cost********\\

			WebElement Death=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[1]"));
			Death.click();
			sleep(300);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-150)", "");
			sleep(500);
			String DeathCost=Death.getText();
			report.updateTestLog("Death Cover amount", CostType+" Cost is "+DeathCost, Status.SCREENSHOT);
			sleep(500);

			//*******Capturing the TPD Cost********\\

			WebElement tpd=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[2]"));
			tpd.click();
			sleep(500);
			String TPDCost=tpd.getText();
			report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);

			//*******Capturing the IP Cost********\\

			WebElement ip=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[3]"));
			ip.click();
			sleep(500);
			String IPCost=ip.getText();

			report.updateTestLog("IP Cover amount", CostType+" Cost is "+IPCost, Status.SCREENSHOT);

			//*****Scroll to the Bottom of the Page

		    jse.executeScript("window.scrollBy(0,250)", "");

		  //******Click on Continue******\\

		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[1]"))).click();
		 sleep(2500);
		 report.updateTestLog("Continue", "Continue button is selected", Status.PASS);





			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void healthandlifestyledetails() {

		String RestrictedIllness = dataTable.getData("HOSTPLUS_SpecialOffer", "RestrictedIllness");
		String PriorClaim = dataTable.getData("HOSTPLUS_SpecialOffer", "PriorClaim");
		String RestrictedFromWork = dataTable.getData("HOSTPLUS_SpecialOffer", "RestrictedFromWork");
		String DiagnosedIllness = dataTable.getData("HOSTPLUS_SpecialOffer", "DiagnosedIllness");
		String MedicalTreatment = dataTable.getData("HOSTPLUS_SpecialOffer", "MedicalTreatment");


		WebDriverWait wait=new WebDriverWait(driver,20);

			try{

				if(driver.getPageSource().contains("Eligibility Check")){

			//*******Are you restricted due to illness or injury*********\\

				if(RestrictedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(1000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(1000);

				}

            //*******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits,
				//Workers� Compensation or any other form of compensation due to illness or injury?*********\\

				if(PriorClaim.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-Yes", Status.PASS);
					sleep(1000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-No", Status.PASS);
					sleep(1000);

				}

                //*******Have you been restricted from work or unable to perform any of your regular duties for more than
				//seven consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?*********\\

				if(RestrictedFromWork.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-Yes", Status.PASS);
					sleep(1000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-No", Status.PASS);
					sleep(1000);

				}


			//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy
		    //to less than 3 years?

				if(DiagnosedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
					sleep(1000);
				}


				//*****Are you currently contemplating any medical treatment or advice for any illness or injury for which you
				//have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?


				if(MedicalTreatment.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
					sleep(1000);
				}


			//******Click on Continue Button********\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(2500);



				}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void confirmation() {

	WebDriverWait wait=new WebDriverWait(driver,20);

		try{
			if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){





		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);


		//******Click on Submit*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(1000);

		}

		//*******Feedback popup******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1500);

		if(driver.getPageSource().contains("Application number")){


		//*****Fetching the Application Status*****\\

		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);

		//*****Fetching the Application Number******\\

		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);



		//******Download the PDF***********\\
		properties = Settings.getInstance();
		String StrBrowseName=properties.getProperty("currentBrowser");
		if (StrBrowseName.equalsIgnoreCase("Chrome")){
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			sleep(1500);
  		}


		//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();



	  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
	  			sleep(1500);
	  			Robot roboobj=new Robot();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  		}

	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
	  			Robot roboobj=new Robot();
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();

	  			sleep(4000);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_ENTER);

	  		}

		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);

		sleep(1000);


		  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);


		 report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);
		}

		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}


}
