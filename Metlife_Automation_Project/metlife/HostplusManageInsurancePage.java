/**
 * 
 */
package pages.metlife;

import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.HostplusPersonalDetailsPageObjects;;

/**
 * @author sampath
 * 
 */
public class HostplusManageInsurancePage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HostplusManageInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public HostplusManageInsurancePage enterHostPlusApplyDeathCover() {
		fillInHostplusPersonalStatementDetails();
		return new HostplusManageInsurancePage(scriptHelper);
	} 

	private void fillInHostplusPersonalStatementDetails() {
		
		
		try{
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnapplyDeathCover),"Click","Apply Death Cover");
			//sleep(5000);
			//tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.),"Click","An injury or illness currently stopping you from performing all the normal and usual duties of your usual occupation?");
/*			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radStoppingDutiesNo),"Click","An injury or illness currently stopping you from performing all the normal and usual duties of your usual occupation?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radInsuranceHistNo),"Click","Have you ever had life or disability insurance refused, postponed or issued on special terms?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chkYourHealthNota),"chkCheck","None of the above");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radRegularMedicationNo),"Click","Have you been prescribed or advised to take medication on a regular or ongoing basis?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radLostTheSightNo),"Click","Have you lost the sight of an eye or suffered the total and permanent loss of the use of a 'limb'?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radMadeAClaimNo),"Click","Are you currently pregnant?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radIllnessRestrictsNo),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnContinueVICSPersonalStatement),"MoveToElm","Continue");
			sleep();
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnContinueVICSPersonalStatement),"Click","Continue");*/
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}	
	
}
