package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSConfirmationPageObjects extends PageObjectLibrary{
		
	public static final By rdowner = getObjectIdentifaction("OAMPS.confirmation.policyowner.radio.css");
	public static final By linkTitle = getObjectIdentifaction("OAMPS.confirmation.title.drpdown.css");
	public static final By txtlastName = getObjectIdentifaction("OAMPS.confirmation.lastname.text.id");
//	public static final By linktitleMr = getObjectIdentifaction("stft.confirmation.title.mr.linktext");
	public static final By txtContact = getObjectIdentifaction("OAMPS.confirmation.contact.text.id");
	public static final By txtcontactOther = getObjectIdentifaction("OAMPS.confirmation.contactother.text.id");
	public static final By txtemail = getObjectIdentifaction("OAMPS.confirmation.email.text.id");
	public static final By drpcontactTime = getObjectIdentifaction("OAMPS.confirmation.contacttime.dropdown.css");
	public static final By txtemailconfirm = getObjectIdentifaction("OAMPS.confirmation.confirm.email.text.id");
	public static final By txtaddress1 = getObjectIdentifaction("OAMPS.confirmation.address1.text.id");
	public static final By txtaddress2 = getObjectIdentifaction("OAMPS.confirmation.address2.text.id");
//	public static final By txtstreetNum = getObjectIdentifaction("stft.confirmation.streetnumber.text.id");
//	public static final By txtstreetName = getObjectIdentifaction("stft.confirmation.streetname.text.id");
	public static final By txtSuburb = getObjectIdentifaction("OAMPS.confirmation.suburb.text.id");
	public static final By drpState = getObjectIdentifaction("OAMPS.confirmation.state.dropdown.css");
//	public static final By linkState = getObjectIdentifaction("stft.confirmation.state.nsw.linktext");
	
	public static final By txtPostcode = getObjectIdentifaction("OAMPS.confirmation.postcode.txt.id");
	public static final By chkacknowledge = getObjectIdentifaction("OAMPS.confirmation.acknowledge.chkbox.css");
	public static final By btnNext = getObjectIdentifaction("OAMPS.confirmation.next.btn.css");
}
