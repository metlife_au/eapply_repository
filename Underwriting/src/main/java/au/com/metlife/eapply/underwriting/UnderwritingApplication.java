package au.com.metlife.eapply.underwriting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnderwritingApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnderwritingApplication.class, args);
    }
}
