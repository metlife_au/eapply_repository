package au.com.metlife.eapply.bs.pdf;

/**
 * <p>
 * Customized Event helper class implementing iText PdfPageEvent interface for
 * generating PDF of eLodgement Underwriting Application.
 * </p>
 * 
 * @author Puneet Malode
 */

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.quote.utility.SystemProperty;


public class EventHelper implements PdfPageEvent {
	private static final Logger log = LoggerFactory.getLogger(EventHelper.class);
    public static final String PACKAGE_NAME = "au.com.metlife.eapply.bs.pdf"; /*$NON-NLS-1$*/
    
    /*Name given to this class.*/
    public static final String CLASS_NAME = "EventHelper"; /*$NON-NLS-1$*/
    
    /*private String appType = null;*/
    
    private CostumPdfAPI pdfAPI1 = null;
    
    /*private MetLifeProperty property = new MetLifeProperty();*/
    private java.util.Properties property = new java.util.Properties();
    private PdfTemplate total;
    
    private BaseFont halv;
    
    private String fund = null;
    private String applicationType = null;
    SystemProperty sys;
  
    
    public EventHelper(String fundName, String appType, String pdfPath) {
    	fund = fundName;
    	applicationType = appType;
    	/*pdfproperty = pdfPath;*/
    }
    
    public EventHelper(String fundName){
    	fund = fundName;
    }
    
    public void onOpenDocument(PdfWriter writer, Document document) {

     
       
        
        total = writer.getDirectContent().createTemplate( 100, 100);
        
        
    }
    
    public void onStartPage(PdfWriter writer, Document document) {
        log.info("OnStart page start");
        
       
       
        try {
        	sys = SystemProperty.getInstance();
            String PdfPropertyFileLoc = sys.getProperty(MetlifeInstitutionalConstants.PROPERTY_FILE);/*ConfigurationHelper.getConfigurationValue( "PdfPropertyFileLoc", "PdfPropertyFileLoc"); "C:/MetLife/properties/PDFProperty.properties";*/            
            File file = new File( PdfPropertyFileLoc);
            InputStream url = new FileInputStream( file);
            property.load( url);
        } catch (Exception e1) {
        	log.error("Error in onStart page: {}",e1);
        }
        pdfAPI1=  new CostumPdfAPI(sys.getProperty(MetlifeInstitutionalConstants.PROPERTY_FILE));
        pdfAPI1.setFont( "HELVETICA_BOLD", 10, "blue");
        /*
         * Image gif; float[] fs = {.7f,.3f}; try { gif =
         * Image.getInstance(property.getProperty("metlifelogo")); PdfPTable
         * pdfPTable = new PdfPTable(fs); PdfPCell cell1 = new
         * PdfPCell(pdfAPI1.addParagraph("",6)); pdfAPI1.disableBorders(cell1);
         * pdfPTable.addCell(cell1); PdfPCell cell2 = new PdfPCell();
         * cell2.addElement(gif); pdfAPI1.disableBorders(cell2);
         * pdfPTable.addCell(cell2); document.add(pdfPTable); } catch
         * (DocumentException e) { MetlifeException exe = new
         * MetlifeException(); exe.setErrorCode("10021");
         * LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME,
         * exe.getErrorCode()+ "|" + exe); e.printStackTrace(); }
         * catch (MalformedURLException e) { MetlifeException exe = new
         * MetlifeException(); exe.setErrorCode("10026");
         * LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME,
         * exe.getErrorCode()+ "|" + exe); e.printStackTrace(); }
         * catch (IOException e) { MetlifeException exe = new
         * MetlifeException(); exe.setErrorCode("10025");
         * LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME,
         * exe.getErrorCode()+ "|" + exe); e.printStackTrace(); }
         */
        log.info("OnStart page finish");
    }
    
    public void onEndPage(PdfWriter writer, Document document) {
    	 log.info("OnEnd page start");
        try {
			sys = SystemProperty.getInstance();
			pdfAPI1=  new CostumPdfAPI(sys.getProperty(MetlifeInstitutionalConstants.PROPERTY_FILE));
	       
		} catch (Exception e1) {
			log.error("Error in onend page :{}",e1);
		}
        pdfAPI1.setFont( "Helvetica", 10, "black");
        Rectangle page = document.getPageSize();
        String pageNum = "Page: " + writer.getPageNumber() + " of";
        float [] f1={0.65f,0.25f, 0.1f};
        PdfPTable foot  = new PdfPTable(f1);
       /* PdfPTable foot = new PdfPTable( 1);
        PdfPCell pdfPCell = new PdfPCell( pdfAPI1.addParagraph( pageNum));
        pdfAPI1.disableBorders( pdfPCell);
        pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
         pdfPCell.setBackgroundColor(pdfAPI1.getColor("blue"));*/
     
       /* foot.addCell( pdfPCell);*/
        
        
        PdfPTable imageTable = new PdfPTable(f1);
		/*String productName=null;*/
		Image gif;
		
	    try{
	    	PdfPCell pdfPCell = new PdfPCell(); 
	    	 pdfAPI1.disableBorders( pdfPCell);	
	    	 foot.addCell( pdfPCell);
	    	pdfPCell = new PdfPCell(); 
	    	 pdfAPI1.disableBorders( pdfPCell);
    	   if("REIS".equalsIgnoreCase(fund) && applicationType!=null &&  null!=property.getProperty(fund+"_"+applicationType)){
    		  /*"http://localhost:9081/eApplication/xAdvisorWeb/CMS/Group/SA006.80HCT1.jpg" */   		   
    		   gif = Image.getInstance(property.getProperty(fund+"_"+applicationType));
    	       gif.setAlignment( Image.ALIGN_RIGHT);
    	       pdfPCell.setImage(gif);
    	   }    
	       pdfPCell.setPaddingRight(4);
	       /*pdfPCell.setPadding(4);*/
	       pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);		
    	   foot.addCell( pdfPCell);    	  
	       pdfPCell = new PdfPCell( pdfAPI1.addParagraph( pageNum));
	       pdfAPI1.disableBorders( pdfPCell);	
	       pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
	       imageTable.addCell( pdfPCell);
	       foot.addCell( pdfPCell);
	    }catch(Exception e){
	    	log.error("Error in one end page: {}",e);
	    }
	      
	    
        
        
         /*pdfPCell = new PdfPCell();
         pdfPCell.addElement(pdfAPI1.addParagraph("www.metlife.com.au"));
         pdfPCell.setFixedHeight(50);
         pdfPCell.setBackgroundColor(pdfAPI1.getColor("blue"));
         pdfAPI1.disableBorders(pdfPCell);
        foot.addCell(pdfPCell);
       	      
       	      
         pdfPCell = new PdfPCell();
         pdfPCell.addElement(pdfAPI1.addParagraph("MetLife eApplication "));
         pdfPCell.setFixedHeight(50);
         pdfPCell.setBackgroundColor(pdfAPI1.getColor("blue"));
         pdfAPI1.disableBorders(pdfPCell);
         foot.addCell(pdfPCell);
        	      
         pdfPCell = new PdfPCell();
         pdfAPI1.disableBorders(pdfPCell);
         pdfPCell.setBackgroundColor(pdfAPI1.getColor("blue"));
        foot.addCell(pdfPCell);*/
        
        foot.setTotalWidth( page.getWidth() - document.leftMargin()
                - document.rightMargin() - 59);
        foot.writeSelectedRows( 0, -1, document.leftMargin(), document.bottomMargin(), writer.getDirectContent());
        
        try {
            
            /* pdfAPI1.setFont("Helvetica", 8, "blue");*/            
            halv = pdfAPI1.getFont().getBaseFont();
            halv = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
        	log.error("Error in Page in end: {}",e);
        }
        total.setBoundingBox( new Rectangle( -5, -5, 100, 100));
        PdfContentByte cd = writer.getDirectContent();
        cd.setFontAndSize( halv, 5);
        cd.setColorFill( new Color( 0, 0, 0));
        cd.saveState();
        
        float textBase = document.bottom() - 15;
        float textSize = halv.getWidthPoint( pageNum, 12);
        cd.beginText();
        cd.setFontAndSize( halv, 12);
        cd.setTextMatrix( document.right() - textSize, textBase);
        /*cd.showText(pageNum);*/
        cd.endText();
        cd.addTemplate( total, document.right() - textSize, textBase);
        log.info("OnEnd page finish");
        
    }
    
    public void onCloseDocument(PdfWriter writer, Document document) {
    	 log.info("OnClose document start");
       
       
        
        total.beginText();
        try {
            
            if (null == halv) {
                halv = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            }
        } catch (Exception e) {
          log.error("Error in onclose document: {}",e);
    
        }
        total.setFontAndSize( halv, 10);
        total.setColorFill( pdfAPI1.getColor( "black"));
        total.setTextMatrix( 3, 3);
        total.showText( String.valueOf( writer.getPageNumber() - 1));
        total.endText();
        log.info("OnClose document finish");
        
    }
    
    public void onParagraph(PdfWriter arg0, Document arg1, float arg2) {

    //method written for future use
    
    }
    
    public void onParagraphEnd(PdfWriter arg0, Document arg1, float arg2) {

    	 //method written for future use
    
    }
    
    public void onChapter(PdfWriter arg0, Document arg1, float arg2,
            Paragraph arg3) {

    	 //method written for future use
    
    }
    
    public void onChapterEnd(PdfWriter arg0, Document arg1, float arg2) {

    	 //method written for future use
    
    }
    
    public void onSection(PdfWriter arg0, Document arg1, float arg2, int arg3,
            Paragraph arg4) {

    	 //method written for future use
    
    }
    
    public void onSectionEnd(PdfWriter arg0, Document arg1, float arg2) {

    	 //method written for future use
    
    }
    
    public void onGenericTag(PdfWriter arg0, Document arg1, Rectangle arg2,
            String arg3) {

    	 //method written for future use
    
    }
    
}
