package pages.metlife;

import supportlibraries.ScriptHelper;

public class MetLifeLoginPage extends MasterPage {

	public MetLifeLoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);

		if (!driver.getTitle().contains("Welcome to MetLife")) {
			throw new IllegalStateException(
					"MetLife Login page expected, but not displayed!");
		}
	}
}
