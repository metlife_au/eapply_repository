/*Retrieve Page controller Starts*/
CareSuperApp.controller('retrievesavedapplication',['$scope', '$location', '$timeout','$window','$rootScope','PersistenceService', 'persoanlDetailService', 'RetrieveAppService','urlService','auraInputService','ngDialog','submitAura','clientMatch','RetrieveAppDetailsService', '$q',  function($scope, $location, $timeout,$window,$rootScope, PersistenceService, persoanlDetailService, RetrieveAppService,urlService,auraInputService, ngDialog,submitAura,clientMatch,RetrieveAppDetailsService, $q){
	$scope.message = 'Look! I am an about page.';
	
	$scope.urlList = urlService.getUrlList();
	$scope.personalDetails = persoanlDetailService.getMemberDetails();
	var memberDetails = persoanlDetailService.getMemberDetails();
	var refNo = memberDetails[0].clientRefNumber;
	$scope.$on('retrieve', function() {
		alert('in retrieve');
	});
	$rootScope.$on('retrieve', function(event, savedApp) {
		alert('in retrieve');
	});
	RetrieveAppService.retrieveApp($scope.urlList.retrieveSavedAppUrl,"CARE",refNo).then(function(res){
		
	
	//RetrieveAppService.retrieveApp({fundCode: "CARE", clientRefNo: refNo}, function(res){
		$scope.apps = res.data;
		for(var i = 0; i < $scope.apps.length; i++){
			$scope.apps[i].hyperlink = false;
			
			var tempDate = new Date($scope.apps[i].createdDate);
			$scope.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');
			
			if($scope.apps[i].requestType == "CCOVER"){
				$scope.apps[i].requestType = "Change Cover";
			} else if($scope.apps[i].requestType == "TCOVER"){
				$scope.apps[i].requestType = "Transfer Cover";
			} else if($scope.apps[i].requestType == "UWCOVER"){
				$scope.apps[i].requestType = "Update Work Rating";
			} else if($scope.apps[i].requestType == "NCOVER"){
				$scope.apps[i].requestType = "New Member Cover";
			} else if($scope.apps[i].requestType == "CANCOVER"){
				$scope.apps[i].requestType = "Cancel Cover";
			}
			
			if($scope.apps[i].applicationStatus.toLowerCase() == "pending"){
				$scope.apps[i].hyperlink = true;
			}
		}
	}, function(err){
		console.log("Error while fetching the apps " + err);
	});
	
	$scope.go = function (path) {
		$timeout(function(){
			$location.path(path);
		}, 10);
	};
	$scope.navigateToLandingPage = function (){
		/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
			$location.path("/landing");
        }*/
		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });

    };
    
 // added for session expiry
  /*  $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
    
   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/
    
    
    $scope.setpersistenceservice = function(appnumber){
    	var defer = $q.defer();
    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,appnumber).then(function(res){
        	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
        		var result = res.data[0];
        		var coverDet = {},
        			coverStateObj = {},
        			occDet = {},
        			deathAddDet = {},
        			tpdAddDet = {},
        			ipAddDet = {};
        		
        		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
        		if(result.personalDetails.dateOfBirth)
    			{
    			coverDet.dob  = moment(result.personalDetails.dateOfBirth).format('DD/MM/YYYY');
    			}
        		else
    			{
    			coverDet.dob  = result.dob;
    			}
        		coverDet.email = result.email;
        		coverDet.contactPhone = result.contactPhone;
        		coverDet.contactType = result.contactType;        		
        		coverDet.contactPrefTime = result.contactPrefTime;
        		occDet.gender = result.personalDetails.gender;
        		coverDet.existingDeathAmt = parseFloat(result.existingDeathAmt);
        		coverDet.existingDeathUnits= parseFloat(result.existingDeathUnits);   
        		coverDet.existingTPDUnits= parseFloat(result.existingTPDUnits);
        		coverDet.deathOccCategory = result.deathOccCategory;
        		coverDet.existingTpdAmt =  parseFloat(result.existingTpdAmt);
        		coverDet.tpdOccCategory = result.tpdOccCategory;
        		coverDet.existingIPAmt =  parseFloat(result.existingIPAmount);
        		coverDet.existingIPAmount = parseFloat(result.existingIPAmount);
        		//coverDet.existingIPUnits = result.existingIPUnits;
        		coverDet.totalPremium =  parseFloat(result.totalPremium);
        		coverDet.ipOccCategory = result.ipOccCategory;
        		coverDet.appNum = result.appNum;
        		//need to change
        		coverDet.manageType = result.manageType;
        		coverDet.partnerCode ="CARE";
        		//need to change
        		coverDet.freqCostType = result.freqCostType;
        		coverDet.age = result.age;
        		if(result.auraDisabled && result.auraDisabled =="true"){
        			$scope.auraDisabled = true;
        		}else{
        			$scope.auraDisabled = false;
        		}
        		
        		coverDet.ipcheckbox = result.ipcheckbox == "true";
        		//coverDet.auraDisabled= result.auraDisabled;
        		if(result.auraDisabled && result.auraDisabled =="true"){
        			coverDet.auraDisabled = true;
        		}else{
        			coverDet.auraDisabled = false;
        		}
        		//coverDet.ipcheckbox = result.ipSalaryPercent == 85;
        		coverDet.ipSalaryPercent=result.ipSalaryPercent;
        		
        		$scope.changeCoverDetails = coverDet;
        		
        		occDet.fifteenHr = result.occupationDetails.fifteenHr;
        		occDet.citizenQue = result.occupationDetails.citizenQue;
        		occDet.industryName = result.occupationDetails.industryName;
        		occDet.occupation = result.occupationDetails.occupation;
        		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
        		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
        		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
        		occDet.industryCode = result.occupationDetails.industryCode;
        		occDet.salary = result.occupationDetails.salary;
        		$scope.changeCoverOccDetails = occDet;
        		
        		deathAddDet.deathFixedAmt =  parseFloat(result.addnlDeathCoverDetails.deathFixedAmt);
        		deathAddDet.deathCoverPremium =  parseFloat(result.addnlDeathCoverDetails.deathCoverPremium);
        		deathAddDet.deathLoadingCost =  parseFloat(result.addnlDeathCoverDetails.deathLoadingCost);
        		deathAddDet.deathCoverName = result.addnlDeathCoverDetails.deathCoverName;
        		
        		deathAddDet.deathCoverType = result.addnlDeathCoverDetails.deathCoverType;
        		deathAddDet.deathInputTextValue = result.addnlDeathCoverDetails.deathInputTextValue;
        		
        		$scope.deathAddnlDetails = deathAddDet;
        		
        		
        		//tpdAddDet.tpdCoverName = result.addnlTpdCoverDetails.tpdCoverName;
        		tpdAddDet.tpdCoverType = result.addnlTpdCoverDetails.tpdCoverType;        		
        		tpdAddDet.tpdInputTextValue = result.addnlTpdCoverDetails.tpdInputTextValue;      		
        		
        		tpdAddDet.tpdFixedAmt =  parseFloat(result.addnlTpdCoverDetails.tpdFixedAmt);
        		tpdAddDet.tpdCoverPremium =  parseFloat(result.addnlTpdCoverDetails.tpdCoverPremium);
        		tpdAddDet.tpdLoadingCost =  parseFloat(result.addnlTpdCoverDetails.tpdLoadingCost);
        		tpdAddDet.tpdCoverName = result.addnlTpdCoverDetails.tpdCoverName;
        		$scope.tpdAddnlDetails = tpdAddDet;
        		
        		ipAddDet.ipFixedAmt =  parseFloat(result.addnlIpCoverDetails.ipFixedAmt);
        		ipAddDet.waitingPeriod = result.addnlIpCoverDetails.waitingPeriod;
        		ipAddDet.benefitPeriod = result.addnlIpCoverDetails.benefitPeriod;
        		
        		$scope.benefitPeriodAddnl = result.addnlIpCoverDetails.benefitPeriod;
        		$scope.waitingPeriodAddnl = result.addnlIpCoverDetails.waitingPeriod;
        		
        		ipAddDet.ipCoverType = result.addnlIpCoverDetails.ipCoverType;
        		ipAddDet.ipCoverPremium =  parseFloat(result.addnlIpCoverDetails.ipCoverPremium);
        		ipAddDet.ipLoadingCost =  parseFloat(result.addnlIpCoverDetails.ipLoadingCost);
        		ipAddDet.ipCoverName = result.addnlIpCoverDetails.ipCoverName;
        		if(ipAddDet.ipCoverName == 'No change')
	    		{
        			ipAddDet.ipInputTextValue= result.addnlIpCoverDetails.ipFixedAmt;
        			if((parseInt(result.addnlIpCoverDetails.ipInputTextValue) != parseInt(coverDet.existingIPAmt)))
        			{
        				ipAddDet.ipAddnlUnits = result.addnlIpCoverDetails.ipInputTextValue;
        			}
	    		}
        		else
        			{
        			ipAddDet.ipInputTextValue= result.addnlIpCoverDetails.ipInputTextValue;
        			ipAddDet.ipAddnlUnits = result.addnlIpCoverDetails.ipInputTextValue;;
        			}
        		/*ipAddDet.ipInputTextValue= result.addnlIpCoverDetails.ipInputTextValue;*/
        		$scope.existingbenefitPeriod = result.existingIPBenefitPeriod;
        		$scope.existingWaitingPeriod = result.existingIPWaitingPeriod;
        		$scope.ipIncreaseFlag = false;
        		        		
        		$scope.isBenifitPeriodIncresed = function() {
        			if(($scope.existingbenefitPeriod == "") && ($scope.benefitPeriodAddnl == '2 Years' || $scope.benefitPeriodAddnl == '5 Years')) {
        				return true;
        			} else if($scope.existingbenefitPeriod == '2 Years' && $scope.benefitPeriodAddnl == '5 Years') {
        				return true;
        			} else {
        				return false;
        			}
        		}
        		
        		$scope.isWaitingPeriodIncresed = function() {
        			if(($scope.existingWaitingPeriod == "") && ($scope.waitingPeriodAddnl == '90 Days' || $scope.waitingPeriodAddnl == '60 Days' || $scope.waitingPeriodAddnl == '30 Days')) {
        				return true;
        			} else if($scope.existingWaitingPeriod == '90 Days' && ($scope.waitingPeriodAddnl == '60 Days' || $scope.waitingPeriodAddnl == '30 Days')) {
        				return true;
        			} else if($scope.existingWaitingPeriod == '60 Days' && $scope.waitingPeriodAddnl == '30 Days') {
        				return true;
        			} else {
        				return false;
        			}
        		}
        		
        		$scope.checkBenefitPeriod = function(){
        			if(parseInt(ipAddDet.ipFixedAmt) > parseInt(coverDet.existingIPAmt))
    				{
        				return true;
    				}else if($scope.existingWaitingPeriod == $scope.waitingPeriodAddnl && $scope.existingbenefitPeriod == $scope.benefitPeriodAddnl){
    					return false;        				
        			} else if($scope.isBenifitPeriodIncresed() || $scope.isWaitingPeriodIncresed()){
        				return true;       				
        			} else{
        				return false;      				
        			}        			
        		};        		
        		
        		
        		if(deathAddDet.deathCoverName == 'Cancel your cover' ){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isDCCoverRequiredDisabled= true;
        			//$scope.isTPDCoverRequiredDisabled = true;
        		}else if(deathAddDet.deathCoverName == 'Decrease your cover'){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isDCCoverRequiredDisabled= false;
        			//$scope.isTPDCoverRequiredDisabled = false;
        		}else if(deathAddDet.deathCoverName =='Increase your cover'){
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isDCCoverRequiredDisabled= false;
        			//$scope.isTPDCoverRequiredDisabled = false;
        		}else{
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isDCCoverRequiredDisabled= true;
        			//$scope.isTPDCoverRequiredDisabled = true;
        		}
        		
        		if(tpdAddDet.tpdCoverName == 'Cancel your cover' ){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isTPDCoverRequiredDisabled = true;
        		}else if(tpdAddDet.tpdCoverName == 'Decrease your cover'){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isTPDCoverRequiredDisabled = false;
        		}else if(tpdAddDet.tpdCoverName =='Increase your cover'){
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isTPDCoverRequiredDisabled = false;
        		}else{
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isTPDCoverRequiredDisabled = true;
        		}
        		
        		if(tpdAddDet.tpdCoverName == 'Cancel your cover' || tpdAddDet.tpdCoverName == 'Decrease your cover'){
        			$scope.tpdDecOrCancelFlag = true;
        			$scope.isTPDCoverNameDisabled = false;
        		}else if($scope.tpdCoverName == "Convert and maintain cover"){
        			$scope.isTPDCoverNameDisabled = true;
        		}else{
        			$scope.tpdDecOrCancelFlag = false;
        			$scope.isTPDCoverNameDisabled = false;
        		}
        		
        		
        		if(ipAddDet.ipCoverName == 'Increase your cover'){
    				$scope.ipIncreaseFlag = true;    
    				$scope.ipDecOrCancelFlag=false;
    				var anb = parseInt(moment().diff(moment($scope.changeCoverDetails.dob, 'DD-MM-YYYY'), 'years')) + 1;
    				if((parseInt($scope.changeCoverOccDetails.salary) < 16000 || $scope.changeCoverOccDetails.fifteenHr == 'No') || anb > 64){
    					$scope.isIPCoverNameDisabled=true;
    					$scope.isIpSalaryCheckboxDisabled = true;
    					$scope.isWaitingPeriodDisabled = true;
    					$scope.isBenefitPeriodDisabled = true;
    				}else{
    					$scope.isIPCoverNameDisabled=false;
    					$scope.isIpSalaryCheckboxDisabled = false;
    					$scope.isWaitingPeriodDisabled = false;
        				$scope.isBenefitPeriodDisabled = false;
    				}
    				if(result.ipSalaryPercent!=85){
    					$scope.isIPCoverRequiredDisabled= false;
    				}
    				
        		}else if(ipAddDet.ipCoverName == 'Decrease your cover' ){
        			if((parseInt($scope.changeCoverOccDetails.salary) < 16000 || $scope.changeCoverOccDetails.fifteenHr == 'No') || anb > 64){
        				$scope.isWaitingPeriodDisabled = true;
        				$scope.isBenefitPeriodDisabled = true;
        				$scope.isIPCoverNameDisabled=true;
        				$scope.isIpSalaryCheckboxDisabled = true;
        			}else{
        				$scope.isWaitingPeriodDisabled = false;
        				$scope.isBenefitPeriodDisabled = false;
        				$scope.isIPCoverNameDisabled=false;
        				$scope.isIpSalaryCheckboxDisabled = false;
        			}
        			if(result.ipSalaryPercent!=85){
        				$scope.isIPCoverRequiredDisabled= false;
        			}        			
        			$scope.ipDecOrCancelFlag=true;
        			$scope.isIPCoverNameDisabled=false;
        		}else if($scope.ipCoverName == 'Cancel your cover'){
        			$scope.ipDecOrCancelFlag=true;
        			$scope.isIPCoverNameDisabled=false;
        		}else{
        			$scope.isIpSalaryCheckboxDisabled = true;
        			$scope.ipIncreaseFlag = false; 
        			$scope.isIPCoverRequiredDisabled = true;
        			$scope.ipDecOrCancelFlag=false;
        			$scope.isIPCoverNameDisabled=false;
        		}
        		
        		
        		$scope.ipAddnlDetails = ipAddDet;
        		
        		$scope.details={};
    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
        		
    			var temp = angular.extend($scope.details,$scope.changeCoverOccDetails);
    			var submitObject = null;
    		
    			submitObject = angular.extend(temp, $scope.personalDetails);			
    			 
    			//state object
    			$scope.dcIncreaseFlag = $scope.details.addnlDeathCoverDetails.deathCoverName == "Increase your cover";
				$scope.tpdIncreaseFlag = $scope.details.addnlTpdCoverDetails.tpdCoverName == "Increase your cover";
				$scope.ipIncreaseFlag = $scope.details.addnlIpCoverDetails.ipCoverName == "Increase your cover"?true:$scope.checkBenefitPeriod();
				coverDet['indexationDeath'] = result.indexationDeath  == 'true';
				coverDet['indexationTpd'] = result.indexationTpd == 'true';
				coverStateObj['isIPCoverNameDisabled'] = $scope.isIPCoverNameDisabled;
				 coverStateObj['isIpSalaryCheckboxDisabled'] = $scope.isIpSalaryCheckboxDisabled;
				 coverStateObj['isIPCoverRequiredDisabled'] = $scope.isIPCoverRequiredDisabled;
				 coverStateObj['isWaitingPeriodDisabled'] = $scope.isWaitingPeriodDisabled;
	             coverStateObj['isBenefitPeriodDisabled'] = $scope.isBenefitPeriodDisabled;
	             coverStateObj['ipDecOrCancelFlag'] = $scope.ipDecOrCancelFlag;
	             coverStateObj['tpdDecOrCancelFlag'] = $scope.tpdDecOrCancelFlag;
	             coverStateObj['deathDecOrCancelFlag'] = $scope.deathDecOrCancelFlag;
	             coverStateObj['isDCCoverRequiredDisabled'] = $scope.isDCCoverRequiredDisabled;
	             coverStateObj['isTPDCoverRequiredDisabled'] = $scope.isTPDCoverRequiredDisabled;
	             coverStateObj['dynamicFlag'] = coverDet.totalPremium != 0;
	             
	             angular.forEach(persoanlDetailService.getMemberDetails(), function (applicant) {           		        	
	            		$scope.coverList = applicant.existingCovers;
	            		angular.forEach($scope.coverList, function (cover) {            		
	                		angular.forEach(cover, function(value, key) {
	                			$scope.benefitType = value.benefitType;            			
	                			 if($scope.benefitType =='1'){
	                				 $scope.deathCover = value;
	                				 $scope.isDCCoverTypeDisabled = $scope.deathCover.type == 2 ? true : false;
	                			 } else if($scope.benefitType =='2'){
	                				 $scope.tpdCover = value;     
	                				 $scope.isTPDCoverTypeDisabled = $scope.tpdCover.type == 2 ? true : false;
	                			 }else  if($scope.benefitType =='4'){
	                				 $scope.ipCover = value;   
	                			 }
	                			});
	                		
	                	});
	                });
	                
	                
	             	
                coverStateObj['isDCCoverTypeDisabled'] = $scope.isDCCoverTypeDisabled;                
                coverStateObj['isTPDCoverTypeDisabled'] = $scope.isTPDCoverTypeDisabled;
                coverStateObj['isTPDCoverNameDisabled'] = $scope.isTPDCoverNameDisabled;
                                                       
           
                
                coverStateObj['dcIncreaseFlag'] = $scope.dcIncreaseFlag;
                coverStateObj['tpdIncreaseFlag'] = $scope.tpdIncreaseFlag;
                coverStateObj['ipIncreaseFlag'] = $scope.ipIncreaseFlag ;				
				
                
                
    			PersistenceService.setChangeCoverDetails($scope.changeCoverDetails);
    	    	PersistenceService.setChangeCoverStateDetails(coverStateObj);
    	    	PersistenceService.setChangeCoverOccDetails($scope.changeCoverOccDetails);
    	    	PersistenceService.setDeathAddnlCoverDetails($scope.deathAddnlDetails);
    	    	PersistenceService.setTpdAddnlCoverDetails($scope.tpdAddnlDetails);
    	    	PersistenceService.setIpAddnlCoverDetails($scope.ipAddnlDetails);
    			
    	    	defer.resolve(res);
        	}, function(err){
        		console.log("Encountered an error while fetchimg the app details " + err);
        		defer.reject(err);
        	});
    	return defer.promise;
    }
	
	
    $scope.goToSavedApp = function(app){
    	PersistenceService.setAppNumToBeRetrieved(app.applicationNumber);
    	PersistenceService.setAppNumber(app.applicationNumber);
    	
    	$scope.setpersistenceservice(app.applicationNumber).then(function() {
    		if(app.auraCallRequired && app.lastSavedOnPage.toLowerCase()=='summarypage'){
    			auraInputService.setFund('CARE');		  		
			  	auraInputService.setAppnumber(parseInt(app.applicationNumber));
				auraInputService.setClientname('metaus');				
				auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
			  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
			  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
			  	$rootScope.$broadcast('disablepointer');
	  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {         			      				
	  				$scope.auraResponses = response.data    
	  				if($scope.auraResponses.overallDecision!='DCL'){
	  					clientMatch.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
	  						if(clientMatchResponse.data.matchFound){
	  							$scope.clientmatchreason = '';
	  							$scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
	  							$scope.auraResponses.overallDecision='RUW'
	  								$scope.information = clientMatchResponse.data.information
	  								angular.forEach($scope.information, function (infoObj,index) {      									
	  									if(index==$scope.information.length-1){
	  										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
	  										$scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))      										
	  									}else{
	  										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "	
	  									}     																	
	  									
	  								})
	  							$scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
	  							$scope.auraResponses.specialTerm = false;
	  						} 						
	      					PersistenceService.setChangeCoverAuraDetails($scope.auraResponses);          					
	      					switch(app.lastSavedOnPage.toLowerCase()){
	      	  		    	case "quotepage":
	      	  		    		$scope.go('/quote/3');
	      	  		    		break;
	      	  		    	case "aurapage":
	      	  		    		$scope.go('/aura/3');
	      	  		    		break;
	      	  		    	case "summarypage":
	      	  		    		$scope.go('/summary/3');
	      	  		    		break;
	      	  		    	default:
	      	  		    		break;
	      	  		    	}
	      				});
	  				}  				
	  			
	     		 });
    		}else{
    			switch(app.lastSavedOnPage.toLowerCase()){
  		    	case "quotepage":
  		    		$scope.go('/quote/3');
  		    		break;
  		    	case "aurapage":
  		    		$scope.go('/aura/3');
  		    		break;
  		    	case "summarypage":
  		    		$scope.go('/summary/3');
  		    		break;
  		    	default:
  		    		break;
  		    }
    		}
  		}, function() {
  			console.log('error');
  		})
    	
    	
		  	/*if(app.auraCallRequired && app.lastSavedOnPage.toLowerCase()=='summarypage'){
		  		//retrieve aura session			
				auraInputService.setFund('CARE');		  		
			  	auraInputService.setAppnumber(parseInt(app.applicationNumber));
				auraInputService.setClientname('metaus');				
			  	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
			  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
			  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
			  	$rootScope.$broadcast('disablepointer');
	  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {         			      				
	  				$scope.auraResponses = response.data    
	  				if($scope.auraResponses.overallDecision!='DCL'){
	  					clientMatch.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
	  						if(clientMatchResponse.data.matchFound){
	  							$scope.clientmatchreason = '';
	  							$scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
	  							$scope.auraResponses.overallDecision='RUW'
	  								$scope.information = clientMatchResponse.data.information
	  								angular.forEach($scope.information, function (infoObj,index) {      									
	  									if(index==$scope.information.length-1){
	  										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
	  										$scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))      										
	  									}else{
	  										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "	
	  									}     																	
	  									
	  								})
	  							$scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
	  							$scope.auraResponses.specialTerm = false;
	  						} 						
	      					PersistenceService.setChangeCoverAuraDetails($scope.auraResponses);          					
	      					switch(app.lastSavedOnPage.toLowerCase()){
	      	  		    	case "quotepage":
	      	  		    		$scope.go('/quote/3');
	      	  		    		break;
	      	  		    	case "aurapage":
	      	  		    		$scope.go('/aura/3');
	      	  		    		break;
	      	  		    	case "summarypage":
	      	  		    		$scope.go('/summary/3');
	      	  		    		break;
	      	  		    	default:
	      	  		    		break;
	      	  		    	}
	      				});
	  				}  				
	  			
	     		 });
		  	}else{
		  		$scope.setpersistenceservice(app.applicationNumber).then(function() {
		  			switch(app.lastSavedOnPage.toLowerCase()){
		  		    	case "quotepage":
		  		    		$scope.go('/quote/3');
		  		    		break;
		  		    	case "aurapage":
		  		    		$scope.go('/aura/3');
		  		    		break;
		  		    	case "summarypage":
		  		    		$scope.go('/summary/3');
		  		    		break;
		  		    	default:
		  		    		break;
		  		    }
		  		}, function() {
		  			console.log('error');
		  		})
		  		
		  	}*/
		  	
  			
		
		
	//
    	
    	
    };
}]);
/*Retrieve Page controller Ends*/