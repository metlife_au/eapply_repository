/*Retrieve Page controller Starts*/
FirstSuperApp.controller('retrievesavedapplication',['$scope', '$rootScope','$routeParams', '$location', '$timeout','$window','getclientData','tokenNumService','PersistenceService', 'fetchPersoanlDetailSvc', 'RetrieveAppService','fetchUrlSvc', 'ngDialog', 'fetchAppNumberSvc', 'RetrieveAppDetailsService', 'appData', 'extendAppModel',  function($scope, $rootScope,$routeParams, $location, $timeout,$window,getclientData,tokenNumService,PersistenceService, fetchPersoanlDetailSvc, RetrieveAppService,fetchUrlSvc, ngDialog, fetchAppNumberSvc, RetrieveAppDetailsService, appData, extendAppModel){
	$scope.message = 'Look! I am an about page.';
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = fetchUrlSvc.getUrlList();
	var refNo = null;
	tokenNumService.setTokenId(inputData);
	if($routeParams.mode == 3)
	{
			refNo = clintRef;
			getclientData.requestObj($scope.urlList.clientDataUrl).then(function(response) {    	
	    	$scope.partnerRequest = response.data;
	    	$scope.applicantlist = $scope.partnerRequest.applicant;
	    	fetchPersoanlDetailSvc.setMemberDetails($scope.applicantlist);
	    });
	  	   
			
		}
		else
			{
	var memberDetails = fetchPersoanlDetailSvc.getMemberDetails();
	refNo = memberDetails.clientRefNumber;
			}
	RetrieveAppService.retrieveApp($scope.urlList.retrieveSavedAppUrl,"FIRS",refNo).then(function(res){
	//RetrieveAppService.retrieveApp({fundCode: "FIRS", clientRefNo: refNo}, function(res){
		$scope.apps = res.data;
		for(var i = 0; i < $scope.apps.length; i++){
			$scope.apps[i].hyperlink = false;

			var tempDate = new Date($scope.apps[i].createdDate);
			$scope.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');

			if($scope.apps[i].requestType == "CCOVER"){
				$scope.apps[i].requestType = "Change Cover";
			}else if($scope.apps[i].requestType == "TCOVER"){
				$scope.apps[i].requestType = "Transfer Cover";
			}else if($scope.apps[i].requestType == "UWCOVER"){
				$scope.apps[i].requestType = "Update Work Rating";
			}else if($scope.apps[i].requestType == "NCOVER"){
				$scope.apps[i].requestType = "New Member Cover";
			}else if($scope.apps[i].requestType == "CANCOVER"){
				$scope.apps[i].requestType = "Cancel Cover";
			}else if($scope.apps[i].requestType == "ICOVER"){
				$scope.apps[i].requestType = "Life Event Cover";
			}else if($scope.apps[i].requestType == "SCOVER"){
				$scope.apps[i].requestType = "Special Cover";
			}if($scope.apps[i].requestType == "NMCOVER"){
				$scope.apps[i].requestType = "Non Member Cover";
			}

			if($scope.apps[i].applicationStatus.toLowerCase() == "pending"){
				$scope.apps[i].hyperlink = true;
			}
		}
	}, function(err){
		console.log("Error while fetching the apps " + err);
	});

	$scope.go = function (path) {
		$timeout(function(){
			$location.path(path);
		}, 10);
	};
	$scope.navigateToLandingPage = function (){
		/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
			$location.path("/landing");
        }*/
		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

 // added for session expiry
  /*  $timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/

    $scope.goToSavedApp = function(app){
			fetchAppNumberSvc.setAppNumber(app.applicationNumber);
    	PersistenceService.setAppNumToBeRetrieved(app.applicationNumber);
      PersistenceService.setAppNumber(app.applicationNumber);
			RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,app.applicationNumber).then(function(res){
				console.log(res);
				appData.setAppData(extendAppModel.extendObj(res.data[0]));
       		switch(app.lastSavedOnPage.toLowerCase()){
   	     	case "quotepage":
   	     		$scope.go('/quote');
   	     		break;
   	     	case "aurapage":
   	     		$scope.go('/aura');
   	     		break;
   	     	case "summarypage":
   	     		$scope.go('/summary');
   	     		break;
   	     	case "transferpage":
   	     		$scope.go('/quotetransfer/3');
   	     		break;
   	     	case "auratransferpage":
   	     		$scope.go('/auratransfer/3');
   	     		break;
   	     	case "summarytransferpage":
   	     		$scope.go('/transferSummary/3');
   	     		break;
   	     	case "quoteupdatepage":
   	     		$scope.go('/quoteoccchange/3');
   	     		break;
   	     	case "auraupdatepage":
   	     		$scope.go('/auraocc/3');
   	     		break;
   	     	case "summaryupdatepage":
   	     		$scope.go('/workRatingSummary/3');
   	     		break;
   	     	case "lifeeventpage":
   	     		$scope.go('/lifeevent/3');
   	     		break;
   	     	case "auralifeeventpage":
   	     		$scope.go('/auralifeevent/3');
   	     		break;
   	     	case "summarylifeeventpage":
   	     		$scope.go('/lifeeventsummary/3');
   	     		break;
   	     	case "specialquotepage":
   	     		$scope.go('/quotespecial/3');
   	     		break;
   	     	case "specialcoveraurapage":
   	     		$scope.go('/auraspecial/3');
   	     		break;
   	     	case "specialsummarypage":
   	     		$scope.go('/specialoffersummary/3');
   	     		break;
   	     	case "quotecancelpage":
   	     		$scope.go('/quotecancel/3');
   	     		break;
   	     	case "auracancelpage":
   	     		$scope.go('/auracancel/3');
   	     		break;
   	     	case "summarycancelpage":
   	     		$scope.go('/cancelConfirmation/3');
   	     		break;
   	     case "nonmemberquotepage":
	     		$scope.go('/nonmember/3');
	     		break;
	     	case "nonmemberaurapage":
	     		$scope.go('/auraNonMember/3');
	     		break;
	     	case "nonmembersummarypage":
	     		$scope.go('/nonMemberSummary/3');
	     		break;
   	     	default:
   	     		break;
       	}
			});
    };
}]);
/*Retrieve Page controller Ends*/
