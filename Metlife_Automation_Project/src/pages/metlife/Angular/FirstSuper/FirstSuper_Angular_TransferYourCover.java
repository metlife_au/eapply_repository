package pages.metlife.Angular.FirstSuper;


import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_TransferYourCover extends MasterPage {
	WebDriverUtil driverUtil = null;
	

	public FirstSuper_Angular_TransferYourCover TransferCover_FirstSuper() {
		FirstSuperPersonaldetails();
		healthandlifestyledetails();
		confirmation();
		
		return new FirstSuper_Angular_TransferYourCover(scriptHelper);
	}
	
	public FirstSuper_Angular_TransferYourCover TransferCover_FirstSuper_Negative() {
		TransferCoverFirstSuper_Negative();
		
		return new FirstSuper_Angular_TransferYourCover(scriptHelper);
	}

	public FirstSuper_Angular_TransferYourCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

public void FirstSuperPersonaldetails() {

		String EmailId = dataTable.getData("FirstSuper_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("FirstSuper_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("FirstSuper_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("FirstSuper_TransferCover", "TimeofContact");
		String Gender = dataTable.getData("FirstSuper_TransferCover", "Gender");
		String FifteenHoursWork = dataTable.getData("FirstSuper_TransferCover", "FifteenHoursWork");
		
		String IndustryType = dataTable.getData("FirstSuper_TransferCover", "IndustryType");
		String OccupationType = dataTable.getData("FirstSuper_TransferCover", "OccupationType");
		String OtherOccupation = dataTable.getData("FirstSuper_TransferCover", "OtherOccupation");
		String Eightypercenttimeinoffice = dataTable.getData("FirstSuper_TransferCover", "Eightypercenttimeinoffice");
		String OfficeEnvironment = dataTable.getData("FirstSuper_TransferCover", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("FirstSuper_TransferCover", "TertiaryQual");
		
		
		String AnnualSalary = dataTable.getData("FirstSuper_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("FirstSuper_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("FirstSuper_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("FirstSuper_TransferCover", "FormerFundNo");
		String DocumentaryEvidence = dataTable.getData("FirstSuper_TransferCover", "DocumentaryEvidence");
		String AttachmentSizeLimitTest	= dataTable.getData("FirstSuper_TransferCover", "AttachmentSizeLimitTest");
		String AttachmentTestPath = dataTable.getData("FirstSuper_TransferCover", "AttachmentTestPath");
		String OverSizeMessage = dataTable.getData("FirstSuper_TransferCover", "OverSizeMessage");
		String AttachPath = dataTable.getData("FirstSuper_TransferCover", "AttachPath");
		String CostType = dataTable.getData("FirstSuper_TransferCover", "CostType");
		String DeathYN = dataTable.getData("FirstSuper_TransferCover", "Death");
		String TPDYN = dataTable.getData("FirstSuper_TransferCover", "TPD");
		String IPYN = dataTable.getData("FirstSuper_TransferCover", "IP");
		String DeathAmount = dataTable.getData("FirstSuper_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("FirstSuper_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("FirstSuper_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("FirstSuper_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("FirstSuper_TransferCover", "BenefitPeriod");
		String Equivalentvalueinunits=dataTable.getData("FirstSuper_TransferCover", "Equivalentvalueinunits");
		String DutyOfDisclosure = dataTable.getData("FirstSuper_TransferCover", "DutyOfDisclosure");
		String PrivacyStatement = dataTable.getData("FirstSuper_TransferCover", "PrivacyStatement");
		//String ExpectedDeathcover=dataTable.getData("FirstSuper_TransferCover", "ExpectedDeathcover");
		//String ExpectedTPDcover=dataTable.getData("FirstSuper_TransferCover", "ExpectedTPDcover");
		//String ExpectedIPcover=dataTable.getData("FirstSuper_TransferCover", "ExpectedIPcover");
		
	
		try{
			
			
			
			//****Click on Transfer Your Cover Link*****\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Transfer your cover')]"))).click();
			report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);
			
			
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
			sleep(400);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);
			String dod=driver.findElement(By.xpath("(//div[@class=' col-sm-12'])[1]")).getText();
			
			if(dod.equalsIgnoreCase(DutyOfDisclosure)){
				report.updateTestLog("Duty Of Disclosure", "Wording is displayed as Expected", Status.PASS);
				
			}
			else{
				report.updateTestLog("Duty Of Disclosure", "Wording is not displayed as Expected", Status.FAIL);
				
			}
			sleep(200);
						
            //*****Agree to Privacy Statement*******\\
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyCkBoxLblId']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);
			sleep(250);
			String ps=driver.findElement(By.xpath("(//div[@class=' col-sm-12'])[2]")).getText();
			//System.out.println(ps);
			
			if(ps.equalsIgnoreCase(PrivacyStatement)){
				report.updateTestLog("Privacy Statement", "Wording is displayed as Expected", Status.PASS);
			}
			else{
				report.updateTestLog("Privacy Statement", "Wording is not displayed as Expected", Status.FAIL);
			}
			sleep(200);
			
            //*****Enter the Email Id*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\			
				
				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
				Contact.selectByVisibleText(TypeofContact);			
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
								
				
			//****Enter the Contact Number****\\	
				
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
					
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				//***********Select the Gender******************\\	
				
				if(Gender.equalsIgnoreCase("Male")){			
							
					driver.findElement(By.xpath("//div/label[text()=' Gender ']/../following-sibling::div/div/div[1]/label/span")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath("//div/label[text()=' Gender ']/../following-sibling::div/div/div[2]/label/span")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}	
							
				//****Contact Details-Continue Button*****\\			
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);			
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
					//*****Select the 15 Hours Question*******\\		
					
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week?']/../following-sibling::div/div/div[1]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week?']/../following-sibling::div/div/div[2]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
				
						
				//*****Industry********\\		
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(800);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
				sleep(500);
				
				//*****Occupation*****\\		
				
				Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(500);
						
						
				//*****Other Occupation *****\\	
				
				if(OccupationType.equalsIgnoreCase("Other")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);	
				}		
				
				
				//*****Extra Questions*****\\							
				
			    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
			    
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\	
			    	
			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
			   
			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(""
			    			+ "(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
										
			}							
			else{							

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
										
			}							
										
			      //*****Do You Hold a Tertiary Qualification******\\							
										
			        if(TertiaryQual.equalsIgnoreCase("Yes")){							
			       
			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}							
			   else{							
			       
				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				
			}		
			        sleep(500);
			        
			}							
										
										
										
			else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
										
				//******Do you spend at least 80% of all working time in an office*****\\
				if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[1]/label/span"))).click();
					report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[2]/label/span"))).click();
					report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
				}
				sleep(500);						
				 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){										

					 //***Select if your office Duties are Undertaken within an Office Environment?\\	
    	
    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
   
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
		report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
							
      }							
            else{							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
	report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
							
         }							
							
      //*****Do You Hold a Tertiary Qualification******\\							
							
        if(TertiaryQual.equalsIgnoreCase("Yes")){							
       
        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
			report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
}							
   else{							
       
	   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
		report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
}									
			}	
			}
									    
	          sleep(800);
			   
										
			//*****What is your annual Salary******							
										
			
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
			    sleep(500);		
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
										
			
			
			//*****Click on Continue*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		
			
			//******************************************\\
			//**********PREVIOUS COVER SECTION**********\\
			//*******************************************\\
			
			//*****What is the name of your previous fund or insurer?*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
			sleep(250);
			report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);
			
            //*****Please enter your fund member or insurance policy number*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
			sleep(250);
			report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);
			
            //*****Please enter your former fund SPIN number (if known)*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
			sleep(250);
			report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

			//*****Do You want to Attach documentary evidence****\\
			
			if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you wish to include documentary evidence of your previous insurance cover that you wish to transfer? ']/../following-sibling::div/div/div[1]/label/span"))).click();
				sleep(500);
				report.updateTestLog("Documentary Evidence", "Yes is selected", Status.PASS);
				
				if(AttachmentSizeLimitTest.equalsIgnoreCase("Yes")){
					properties = Settings.getInstance();	
					String StrBrowseName=properties.getProperty("currentBrowser");
					if (StrBrowseName.equalsIgnoreCase("Firefox")){
						 StringSelection sel = new StringSelection(AttachmentTestPath);
						 
						   // Copy to clipboard  
						 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
						 
						 //Click
						 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
						 
						 // Create object of Robot class
						 Robot robot = new Robot();
						 Thread.sleep(1000);
						      
						  // Press Enter
						 robot.keyPress(KeyEvent.VK_ENTER);
						 
						// Release Enter
						 robot.keyRelease(KeyEvent.VK_ENTER);
						 
						  // Press CTRL+V
						 robot.keyPress(KeyEvent.VK_CONTROL);
						 robot.keyPress(KeyEvent.VK_V);
						 
						// Release CTRL+V
						 robot.keyRelease(KeyEvent.VK_CONTROL);
						 robot.keyRelease(KeyEvent.VK_V);
						 Thread.sleep(1000);
						        
						        // Press Enter 
						 robot.keyPress(KeyEvent.VK_ENTER);
						 robot.keyRelease(KeyEvent.VK_ENTER);
						 
						sleep(500);
					}
					else{
						driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachmentTestPath);
						
					}
					
					//*****Let the attachment load*****\\		
					driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
					sleep(1000);
					
					//*****Add the Attachment*****\\
					
					WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
					sleep(1500);
					
					String OversizeMessage=driver.findElement(By.xpath("//label[text()='  File size should not be more than 10MB ']")).getText();
					sleep(250);
					String SizeLimit=OversizeMessage.trim();
					
					if(OverSizeMessage.equalsIgnoreCase(SizeLimit)){
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is Displayed", Status.PASS);
					}
					else{
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is not Displayed", Status.FAIL);
					}
					
					
				}
				
				//****Attach the file******	
				sleep(500);
				properties = Settings.getInstance();	
				String StrBrowseName=properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Firefox")){
					StringSelection sel = new StringSelection(AttachPath);
					 
					   // Copy to clipboard  
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
					 
					 //Click
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
					 
					 // Create object of Robot class
					 Robot robot = new Robot();
					 Thread.sleep(1000);
					      
					  // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);
					 
					// Release Enter
					 robot.keyRelease(KeyEvent.VK_ENTER);
					 
					  // Press CTRL+V
					 robot.keyPress(KeyEvent.VK_CONTROL);
					 robot.keyPress(KeyEvent.VK_V);
					 
					// Release CTRL+V
					 robot.keyRelease(KeyEvent.VK_CONTROL);
					 robot.keyRelease(KeyEvent.VK_V);
					 Thread.sleep(1000);
					        
					        // Press Enter 
					 robot.keyPress(KeyEvent.VK_ENTER);
					 robot.keyRelease(KeyEvent.VK_ENTER);
					 
					sleep(500);
				}
				else{
					driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
					
				}
				
				
			
				sleep(1500);
				
				//*****Let the attachment load*****\\		
				driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
				sleep(1000);
				
				//*****Add the Attachment*****\\
				
				WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1500);
				report.updateTestLog("Evidence Attachment", "File is successfully attached", Status.PASS);
								
			}
			else{
				
				//****No Attachment *****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you wish to include documentary evidence of your previous insurance cover that you wish to transfer? ']/../following-sibling::div/div/div[2]/label/span"))).click();
				sleep(500);
				report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);
				sleep(500);
				driver.findElement(By.xpath("//*[@id='acknowledgeDocAdressCheck']/span")).click();
				sleep(500);
				
				
			}
			
			//*****Choose the cost of Insurance Type******\\
			
			Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
			CostofInsurance.selectByVisibleText(CostType);
			sleep(400);
			report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);
			
			
			//****Click on Continue Button******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);
			

			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\
			
			//*****Death transfer amount******
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);
			
			}
			
			//*****TPD transfer amount******
			
			if(TPDYN.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);
			
			}
			
			//****IP transfer amount*****\\
			
			
			
			if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){
				

			//*****Select The Waiting Period********\\
			
				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);
			
			
			
			//*****Select The Benefit  Period********\\
			
				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);

				
			//****Enter the IP amount*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);
				
			}
			
			//****Equivalent units of Hostplus checkbox*****\\
			
			if(Equivalentvalueinunits.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Equivalent value in unitised scale? ']/../following-sibling::div/div/div[1]/label/span"))).click();
				report.updateTestLog("Equivalent Value in unitised scale", "Yes is selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Equivalent value in unitised scale? ']/../following-sibling::div/div/div[2]/label/span"))).click();
				report.updateTestLog("Equivalent Value in unitised scale", "No is selected", Status.PASS);
			}
			
			
			
			//*****Click on calculate Quote*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
			sleep(2000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
			 List<WebElement> 	elementTotalCost = listElements("h4.coverval.ng-binding");
			 System.out.println(elementTotalCost.size());
			if(elementTotalCost.get(elementTotalCost.size()-1).getText().equalsIgnoreCase("$0.00")){
			System.out.println("entered into if");
			driver.findElement(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']")).click();
			sleep(4000);
				
			}
			
			if(elementTotalCost.get(elementTotalCost.size()-1).getText().equalsIgnoreCase("$0.00")){
				
				driver.findElement(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']")).click();
				sleep(4000);
					
			}
			}

//			//********************************************************************************************\\
//			//************Capturing the premiums displayed************************************************\\
//			//********************************************************************************************\\
//			
//			//*******Capturing the Death Cost********\\
//			if(DeathYN.equalsIgnoreCase("Yes")){
//			WebElement Death=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[1]"));
//			Death.click();
//			sleep(300);
//			JavascriptExecutor jse = (JavascriptExecutor) driver;
//			jse.executeScript("window.scrollBy(0,-150)", "");
//			sleep(500);
//			String DeathCost=Death.getText();	
//			if(DeathCost.equalsIgnoreCase(ExpectedDeathcover)){
//				
//				report.updateTestLog("Death Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedDeathcover+" Actual Premium: "+DeathCost, Status.PASS);
//			}
//			else{
//				
//				report.updateTestLog("Death Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedDeathcover+" Actual Premium: "+DeathCost, Status.FAIL);
//			}
//			sleep(200);
//			}
//			//*******Capturing the TPD Cost********\\
//			
//			if(TPDYN.equalsIgnoreCase("Yes")){
//			WebElement tpd=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[2]"));
//			tpd.click();
//			sleep(500);
//			String TPDCost=tpd.getText();
//			if(TPDCost.equalsIgnoreCase(ExpectedTPDcover)){
//				
//				report.updateTestLog("TPD Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedTPDcover+" Actual Premium: "+TPDCost, Status.PASS);
//			}
//			else{
//				
//				report.updateTestLog("TPD Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedTPDcover+" Actual Premium: "+TPDCost, Status.FAIL);
//			}
//			sleep(250);
//			}
//			//*******Capturing the IP Cost********\\
//			if(IPYN.equalsIgnoreCase("Yes")){
//			WebElement ip=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[3]"));
//			ip.click();
//			sleep(500);
//			String IPCost=ip.getText();
//			
//			if(IPCost.equalsIgnoreCase(ExpectedIPcover)){
//				
//				report.updateTestLog("IP Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedIPcover+" Actual Premium: "+IPCost, Status.PASS);
//			}
//			else{
//				
//				report.updateTestLog("IP Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedIPcover+" Actual Premium: "+IPCost, Status.FAIL);
//			}
//			}
//			//*****Scroll to the Bottom of the Page
//			JavascriptExecutor jse = (JavascriptExecutor) driver;
//		    jse.executeScript("window.scrollBy(0,250)", "");	
//			
		  //******Click on Continue******\\
		    
		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[2]"))).click();
		 sleep(4500);
		 report.updateTestLog("Continue", "Continue button is selected", Status.PASS); 
		   
				
		

			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void healthandlifestyledetails() {
		
		String RestrictedIllness = dataTable.getData("FirstSuper_TransferCover", "RestrictedIllness");
		String PriorClaim = dataTable.getData("FirstSuper_TransferCover", "PriorClaim");
		String RestrictedFromWork = dataTable.getData("FirstSuper_TransferCover", "RestrictedFromWork");
		String DiagnosedIllness = dataTable.getData("FirstSuper_TransferCover", "DiagnosedIllness");
		String MedicalTreatment = dataTable.getData("FirstSuper_TransferCover", "MedicalTreatment");
		String DeclinedApplication = dataTable.getData("FirstSuper_TransferCover", "DeclinedApplication");
		String Fundpremloading = dataTable.getData("FirstSuper_TransferCover", "Fundpremloading");
			
		WebDriverWait wait=new WebDriverWait(driver,20);
			
			try{
				
				if(driver.getPageSource().contains("Eligibility Check")){
				
			//*******Are you restricted due to illness or injury*********\\
				
				if(RestrictedIllness.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(1000);
				
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(1000);
					
				}
				
            //*******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits,
				//Workers� Compensation or any other form of compensation due to illness or injury?*********\\
				
				if(PriorClaim.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-Yes", Status.PASS);
					sleep(1000);
				
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-No", Status.PASS);
					sleep(1000);
					
				}	
				
                //*******Have you been restricted from work or unable to perform any of your regular duties for more than 
				//seven consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?*********\\
				
				if(RestrictedFromWork.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-Yes", Status.PASS);
					sleep(1000);
				
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-No", Status.PASS);
					sleep(1000);
					
				}	
				
				
			//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy 
		    //to less than 3 years?
				
				if(DiagnosedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
					sleep(1000);
				}
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
					sleep(1000);
				}
				
				
				//*****Are you currently contemplating any medical treatment or advice for any illness or injury for which you
				//have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating? 
				
				
				if(MedicalTreatment.equalsIgnoreCase("Yes")){				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
					sleep(1000);
				}
		
				//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\
				
				if(DeclinedApplication.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[6]"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Declined Application-Yes", Status.PASS);
					sleep(1000);
				}
				else{				
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[6]"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Declined Application-No", Status.PASS);
					sleep(1000);
				}
				
				//*******Is your cover under the previous fund or individual insurer subject to any premium loadings 
				//and/or exclusions, including but not limited to pre-existing conditions exclusions, or restrictions in 
				//regards to medical or other conditions?
				
				if(Fundpremloading.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[7]"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-Yes", Status.PASS);
					sleep(1000);
				}
				else{				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[7]"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-No", Status.PASS);
					sleep(1000); 
				}
				
			//******Click on Continue Button********\\
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(3000);
			    									
				
				
				}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
	
private void confirmation() {
	String GeneralConsent = dataTable.getData("FirstSuper_TransferCover", "GeneralConsent");
	WebDriverWait wait=new WebDriverWait(driver,20);	
		
		try{
			if(driver.getPageSource().contains("By clicking on the tick box below, I declare that:")){
				
			
		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelTR']/span"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);
			
			String gs=driver.findElement(By.xpath("(//div[@class='col-sm-12'])[1]")).getText();
			//System.out.println(gs);
			
			if(gs.equalsIgnoreCase(GeneralConsent)){
				report.updateTestLog("General Consent", "Wording is displayed as Expected", Status.PASS);
				
			}
			else{
				report.updateTestLog("General Consent", "Wording is not displayed as Expected", Status.FAIL);
				
			}
			sleep(200);
			
			
		//******Click on Submit*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitTransferCover()']"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(1000);			
			
		}
		
		//*******Feedback Pop-up******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1500);
		
		
		
		
			
		
		//*****Fetching the Application Status*****\\
		
		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);
		report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);
		
		if(!Appstatus.equalsIgnoreCase("TRANSFER OF COVER")){
		//String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
		//System.out.println(Justification);
		sleep(400);
		
		//*****Fetching the Application Number******\\
		
		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);
		
		
		
		//******Download the PDF***********\\
		properties = Settings.getInstance();	
		String StrBrowseName=properties.getProperty("currentBrowser");
		if (StrBrowseName.equalsIgnoreCase("Chrome")){
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			sleep(1500);
  		}
		
		
		//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
		
		
		
	  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
	  			sleep(2500);
	  			Robot roboobj=new Robot();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);	
	  		}
	  		
	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
//	  			Robot roboobj=new Robot();
//	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
//	  				
//	  			sleep(4000);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_ENTER);	
	  			System.out.println("Not downloading the PDF");
	  			
	  		}
		
		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
		
		sleep(1000);
	
		 
		  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);
		
		
		 	
		}
		

		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}

public void TransferCoverFirstSuper_Negative() {

	String EmailId = dataTable.getData("FirstSuper_TransferCover", "EmailId");
	String TypeofContact = dataTable.getData("FirstSuper_TransferCover", "TypeofContact");
	String ContactNumber = dataTable.getData("FirstSuper_TransferCover", "ContactNumber");
	String TimeofContact = dataTable.getData("FirstSuper_TransferCover", "TimeofContact");
	String Gender = dataTable.getData("FirstSuper_TransferCover", "Gender");
	String FifteenHoursWork = dataTable.getData("FirstSuper_TransferCover", "FifteenHoursWork");
	
	String IndustryType = dataTable.getData("FirstSuper_TransferCover", "IndustryType");
	String OccupationType = dataTable.getData("FirstSuper_TransferCover", "OccupationType");
	String OtherOccupation = dataTable.getData("FirstSuper_TransferCover", "OtherOccupation");
	String OfficeEnvironment = dataTable.getData("FirstSuper_TransferCover", "OfficeEnvironment");
    String TertiaryQual = dataTable.getData("FirstSuper_TransferCover", "TertiaryQual");
	String Eightypercenttimeinoffice = dataTable.getData("FirstSuper_TransferCover", "Eightypercenttimeinoffice");
	
	String AnnualSalary = dataTable.getData("FirstSuper_TransferCover", "AnnualSalary");
	String PreviousInsurer = dataTable.getData("FirstSuper_TransferCover", "PreviousInsurer");
	String PolicyNo = dataTable.getData("FirstSuper_TransferCover", "PolicyNo");
	String FormerFundNo = dataTable.getData("FirstSuper_TransferCover", "FormerFundNo");
	String DocumentaryEvidence = dataTable.getData("FirstSuper_TransferCover", "DocumentaryEvidence");
	String AttachPath = dataTable.getData("FirstSuper_TransferCover", "AttachPath");
	String CostType = dataTable.getData("FirstSuper_TransferCover", "CostType");
	String DeathYN = dataTable.getData("FirstSuper_TransferCover", "Death");
	String TPDYN = dataTable.getData("FirstSuper_TransferCover", "TPD");
	
	String DeathAmount = dataTable.getData("FirstSuper_TransferCover", "DeathAmount");
	String TPDAmount = dataTable.getData("FirstSuper_TransferCover", "TPDAmount");
	String TPDAmtGreater = dataTable.getData("FirstSuper_TransferCover", "TPDAmtGreater");
	

	
	
	
	try{
		
		
		
		//****Click on Transfer Your Cover Link*****\\
		WebDriverWait wait=new WebDriverWait(driver,18);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Transfer your cover')]"))).click();
		report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);
		
	
		//*****Agree to Duty of disclosure*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
		sleep(250);
		report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);

		
        //*****Agree to Privacy Statement*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyCkBoxLblId']/span"))).click();
		sleep(250);
		report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);
	
        //*****Enter the Email Id*****\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
		sleep(250);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
		report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
		sleep(500);

		//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);
							
			
			//****Enter the Contact Number****\\			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
			
			//***Select the Preferred time of Contact*****\\			
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
						
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}		
			
			
			//***********Select the Gender******************\\	
			
			if(Gender.equalsIgnoreCase("Male")){			
						
				driver.findElement(By.xpath("//div/label[text()=' Gender ']/../following-sibling::div/div/div[1]/label/span")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//div/label[text()=' Gender ']/../following-sibling::div/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}	
						
			//****Contact Details-Continue Button*****\\			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);			
						
			//***************************************\\			
			//**********OCCUPATION SECTION***********\\			
			//****************************************\\			
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week?']/../following-sibling::div/div/div[1]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week?']/../following-sibling::div/div/div[2]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
		
						
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
					
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}		
			
			
			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			
		}		
		        sleep(500);
		        
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
									
			//******Do you spend at least 80% of all working time in an office*****\\
			if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[1]/label/span"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[2]/label/span"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
			}
			sleep(500);						
			 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){										

				 //***Select if your office Duties are Undertaken within an Office Environment?\\	
	
if(OfficeEnvironment.equalsIgnoreCase("Yes")){							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
	report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
						
  }							
        else{							

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
						
     }							
						
  //*****Do You Hold a Tertiary Qualification******\\							
						
    if(TertiaryQual.equalsIgnoreCase("Yes")){							
   
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
		report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
}							
else{							
   
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
	report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
}									
		}	
		}
								    
									
		//*****What is your annual Salary******							
									
		
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
									
		
		
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
	
		
		//******************************************\\
		//**********PREVIOUS COVER SECTION**********\\
		//*******************************************\\
		
		//*****What is the name of your previous fund or insurer?*****\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
		sleep(250);
		report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);
		
        //*****Please enter your fund member or insurance policy number*****\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
		sleep(250);
		report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);
		
        //*****Please enter your former fund SPIN number (if known)*****\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
		sleep(250);
		report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

		//*****Do You want to Attach documentary evidence****\\
		
		if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 pl0 col-xs-12']/label/span"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "Yes is selected", Status.PASS);
			
			
			
			//****Attach the file******		
			
			driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
			sleep(1500);
			
			//*****Let the attachment load*****\\		
			driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
			sleep(1000);
			
			//*****Add the Attachment*****\\
			
			WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
			sleep(1500);
			report.updateTestLog("Evidence Attachment", "File is successfully attached", Status.PASS);
							
		}
		else{
			
			//****No Attachment *****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12 pl0xs']/label/span"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);
			sleep(400);
			driver.findElement(By.xpath("//*[@id='acknowledgeDocAdressCheck']/span")).click();
			sleep(500);
			
			
		}
		
		//*****Choose the cost of Insurance Type******\\
		
		Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
		CostofInsurance.selectByVisibleText(CostType);
		sleep(400);
		report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);
		
		
		//****Click on Continue Button******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
		sleep(500);
		report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);
		

		//*************************************************\\
		//**************TRANSFER COVER SECTION***************\\
		//**************************************************\\
		
		//*****Death transfer amount******
		
		if(DeathYN.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
			sleep(500);
			report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);
		
		}
		
		//*****TPD transfer amount******
		
		if(TPDYN.equalsIgnoreCase("Yes")){
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
			sleep(500);
			report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);
		
		}
		
		String TPDError=driver.findElement(By.xpath("//*[@ng-if='coverAmtErrFlag']")).getText();
		
		if(TPDError.equalsIgnoreCase(TPDAmtGreater)){
			report.updateTestLog("TPD Error", "Expected Error Message: "+TPDAmtGreater, Status.PASS);
		}
		else{
			report.updateTestLog("TPD Error", "Error message is not displayed", Status.FAIL);
		}

		
			
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}

}
