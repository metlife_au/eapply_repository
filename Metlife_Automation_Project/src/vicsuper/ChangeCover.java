package vicsuper;

import java.math.BigDecimal;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import vicsSuper_Objects.ChangeCoverObjects;
import vicsSuper_Objects.LandingPageObject;
import vicsSuper_Objects.TransferCoverObjects;

public class ChangeCover extends MasterPage {
	WebDriverWait wait = new WebDriverWait(driver, 20);
	WebDriverUtil driverUtil = null;
	//JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	public ChangeCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	String XML;
	String SenarioType;
	String DataSheet;
	String occRating1;
	String occRating2;
	String occRating3;
	String BenefitPeriod;
	String SecondPage;

	// Actions action = new Actions(driver.getWebDriver());

	// ChangeCoverObjects PerDet= new ChangeCoverObjects(scriptHelper);

	public ChangeCover ChangeYourInsuranceCover_vicsuper(String DataSheetName, String Type) {

		SenarioType = Type;
		// PerDet.ChangeCoverObjectsInitialization(AppPage);
		XML = dataTable.getData("VicSuper_Login", "XML");
		DataSheet = DataSheetName;
		if (SenarioType.equalsIgnoreCase("Overall")) {

		} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {

			PremiumRateInputValidation();
		} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {

			occupationRatingInputValidation();
		} else {
			report.updateTestLog("Error Type", "Invalid Type value-" + SenarioType, Status.FAIL);
		}
		PersonalDetails();
		return new ChangeCover(scriptHelper);

	}

	public void PersonalDetails() {
		sleep(2000);
		String DutyOfDisclosure = null;
		String PrivacyStatement = null;
		if (SenarioType.equalsIgnoreCase("Overall")) {
		DutyOfDisclosure = dataTable.getData(DataSheet, "DutyOfDisclosure");
		PrivacyStatement = dataTable.getData(DataSheet, "PrivacyStatement");
		}
		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
		String Citizen = dataTable.getData(DataSheet, "Citizen");
		String IndustryType = dataTable.getData(DataSheet, "IndustryType");
		String OccupationType = dataTable.getData(DataSheet, "OccupationType");
		String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
		String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
		String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
		String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");

		try {

			sleep(500);

			wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.changeYourInsurence)).click();
			sleep(4000);
			// Popup Validation
			/*
			 * if (driver.findElement(By.xpath("//button[text()='Continue']")).
			 * isDisplayed() == true) {
			 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
			 * "//button[text()='Continue']"))).click(); sleep(5000); }
			 */
			sleep(5000);

			// -----------------------------------------------------PersonalDetails------------------------------------------------------------------------//
			// *****Agree to Duty of disclosure*******\\
			if (SenarioType.equalsIgnoreCase("Overall")) {
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_dutyOfDisclosure));


			String[] ArrayHeaderDD;
			int flag = 1;

			String part2;
			String[] value;
			String HeaderDD = null;

			String[] parts = DutyOfDisclosure.split("---");
			String DDTxt = driver.findElement(By.xpath(".//*[@name='formduty']//p[2]/strong")).getText();
			 if (DDTxt.equalsIgnoreCase(parts[0])) {
				}
				else {
					flag =0;
					report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
				}

			parts = parts[1].split("\\n\\n");

			int j=3;
			for (int a = 0; a < 9; a++) {

					if ( !(a==3) ) {
						HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//p)["+j+"]")).getText();
						j++;

						 if (HeaderDD.equalsIgnoreCase(parts[a])) {
							}
							else {
								flag =0;
								report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}


					}
				else {
					String[] parts1 = parts[a].split("\\n");
					int k=1;
					for (int b = 0; b < 5; b++) {

						System.out.println("K="+k);
						if ( !(b==4) ) {
							 HeaderDD = driver.findElement(By.xpath(".//*[@name='formduty']//ul/li["+k+"]")).getText();


						}
						else {
							 HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//strong)[2]")).getText();

						}
						 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

						 }
							else {
								flag =0;
								report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}

						k++;

					}

				}


			}



					if (flag==0) {
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is not as expected", Status.FAIL);
					}
					else {
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is present as expected", Status.PASS);

					}

			}
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_dutyOfDisclosure))
					.click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);



			// *****Agree to Privacy Statement*******\\
			if (SenarioType.equalsIgnoreCase("Overall")) {
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_privacyStatement));

			String[] ArrayHeaderDD;
			int flag = 1;

			String part2;
			String[] value;
			String HeaderDD = null;


			String[] parts = PrivacyStatement.split("---");
			String DDTxt = driver.findElement(By.xpath(".//*[@name='cancelprivacyPolicyForm']//p[2]/strong")).getText();
			 if (DDTxt.equalsIgnoreCase(parts[0])) {

				}
				else {
					flag =0;
					report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
				}

			parts = parts[1].split("\\n\\n\\n");
			int j=3;
				for (int a = 0; a < 2; a++) {

			HeaderDD = driver.findElement(By.xpath("(.//*[@name='cancelprivacyPolicyForm']//p)["+j+"]")).getText();

			 if (HeaderDD.equalsIgnoreCase(parts[a])) {

				}
				else {
					flag =0;
					report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
				}
			j++;
			}

				if (flag==0) {
					report.updateTestLog("Privacy statement", "Privacy statement - Statement is not as expected", Status.FAIL);

				}
				else {
					report.updateTestLog("Privacy statement", "Privacy statement - Statement is present as expected", Status.PASS);
				}

			}

			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_privacyStatement))
					.click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_email)).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_email))
					.sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
			sleep(300);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_contactType));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_phNO))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_phNO))
					.sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {
				wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Morning))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			} else if (TimeofContact.equalsIgnoreCase("Afternoon")) {
				wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Afternoon))
						.click();
				sleep(500);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for TimeofContact", Status.FAIL);
			}

			/*
			 * //***********Select the Gender******************\\
			 *
			 * if (Gender.equalsIgnoreCase("Male")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * ChangeCoverObjects.obj_PersonalDetails_Morning)).click();
			 * sleep(250); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS); } else if
			 * (Gender.equalsIgnoreCase("Female")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * ChangeCoverObjects.obj_PersonalDetails_Afternoon)).click();
			 * sleep(500); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS);
			 *
			 * } else { report.updateTestLog("DataTable",
			 * "Invalid value for TimeofContact", Status.FAIL); }
			 */
			// -----------------------------------------------------Occupation------------------------------------------------------------------------//

			// *****Select the 14 Hours Question*******\\

			if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_FourteenHrsYes))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
			} else if (FourteenHoursWork.equalsIgnoreCase("No")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_FourteenHrsNo))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for FourteenHoursWork", Status.FAIL);
			}

			// *****Resident of Australia****\\
			if (Citizen.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_CitizenYes))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
			} else if (Citizen.equalsIgnoreCase("No")) {
				wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_CitizenNo))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
			} else {
				report.updateTestLog("DataTable", "Invalid value for Citizen", Status.FAIL);
			}

			// *****Industry********\\

			sleep(500);
			Select Industry = new Select(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Industry));
			Industry.selectByVisibleText(IndustryType);
			sleep(500);
			report.updateTestLog("Industry", "Industry Selected is: " + IndustryType, Status.PASS);
			sleep(1500);

			// *****Occupation*****\\

			Select Occupation = new Select(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Occupation));
			Occupation.selectByVisibleText(OccupationType);
			sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: " + OccupationType, Status.PASS);
			sleep(1000);

			// *****Are the duties of your regular occupation limited to
			// either:*****\\

			if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

				// ***Select duties which are undertaken within an Office
				// Environment?\\
				if (EducationalInstitution.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AddQuest1aYes)).click();
					report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalInstitution.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AddQuest1aNo)).click();
					report.updateTestLog("Educational institution", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalInstitution", Status.FAIL);
				}

				// *****Educational duties performed within a school or other
				// educational institution******\\
				if (EducationalDuties.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AddQuest1bYes)).click();
					report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalDuties.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AddQuest1bNo)).click();
					report.updateTestLog("Educational duties", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalDuties", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

			}

			// Do you hold a tertiary qualification or work in a management role
			if (driver.getPageSource().contains("Do you:")) {

				// *****Do You Hold a Tertiary Qualification******\\
				if (TertiaryQual.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AddQuest2Yes)).click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (TertiaryQual.equalsIgnoreCase("No")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AddQuest2No))
							.click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for TertiaryQual", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Do you hold a tertiary qualification or work in a management role is not present",
						Status.FAIL);
			}

			// *****What is your annual Salary******

			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AnnualSal))
					.clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AnnualSal))
					.sendKeys(AnnualSalary);
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_AnnualSal))
					.sendKeys(Keys.ENTER);
			sleep(1000);
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);

			// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//
			if (SenarioType.equalsIgnoreCase("Overall")) {
				String CoverValidation = dataTable.getData(DataSheet, "CoverValidation");
				if (!(CoverValidation.equalsIgnoreCase(""))) {
					cover_Validations();
					return;
				}
				cover_Overall();
			} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {
				cover_Premium();
			} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
				cover_OccupationRating();
			}

			if ((SenarioType.equalsIgnoreCase("PremiumCalculation"))) {
				return;
			} else if ((SenarioType.equalsIgnoreCase("Overall"))) {
				String SaveExit_Personal = dataTable.getData(DataSheet, "SaveExit_PersonalDetails");
				if (SaveExit_Personal.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Save))
							.click();
					sleep(3000);
					// action.moveToElement(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Save)).click().build().perform();

					report.updateTestLog("Save and Exit", "Save and Exit button is selected", Status.PASS);
					saveAndExitPopUP();
					validateChangeCoverPersonalDetail();
				} else {
					String DeathAction = dataTable.getData(DataSheet, "DeathAction");
					String TPDAction = dataTable.getData(DataSheet, "TPDAction");
					String IPAction = dataTable.getData(DataSheet, "IPAction");

					wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Continue))
							.click();
					sleep(3000);

					report.updateTestLog("Continue - Personal Details",
							"Continue - Personal Details button is selected", Status.PASS);


						if ((DeathAction.equalsIgnoreCase("Decrease your cover"))
								|| (TPDAction.equalsIgnoreCase("Decrease your cover"))
								|| (IPAction.equalsIgnoreCase("Decrease your cover"))
								|| (DeathAction.equalsIgnoreCase("Cancel your cover"))
								|| (TPDAction.equalsIgnoreCase("Cancel your cover"))
								|| (IPAction.equalsIgnoreCase("Cancel your cover"))) {
							decisionPopUP_Negative();
						} else if (SecondPage.equalsIgnoreCase("Aura")) {
							HealthandLifeStyle();
						} else if (SecondPage.equalsIgnoreCase("Confirmation")) {
							confirmation();
						} else {
							report.updateTestLog("Error", "Invalid Second Page value-" + SecondPage, Status.FAIL);

						}


				}

			} else if ((SenarioType.equalsIgnoreCase("OccupationRating"))) {
				System.out.println("OccRating");
				wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Continue))
						.click();
				sleep(3000);
				report.updateTestLog("Continue - Personal Details", "Continue - Personal Details button is selected",
						Status.PASS);
				//HealthandLifeStyle();

				if (driver.getPageSource().contains(
						"What is your height?")) {
					HealthandLifeStyle();
				}else {
					confirmation();

				}

			}

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}

	public void HealthandLifeStyle() {
		sleep(3000);
		String Decline_DueToAura = null;
		if (SenarioType.equalsIgnoreCase("Overall")) {
			Decline_DueToAura = dataTable.getData(DataSheet, "Decline_DueToAura");

			String AuraDisplay = dataTable.getData(DataSheet, "AuraDisplay");
			if (AuraDisplay.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='heightval']")));
				report.updateTestLog("Aura Page", "Aura Page is displayed", Status.PASS);
				return;
			}

		}

		String Height = dataTable.getData(DataSheet, "Height");
		String HeightType = dataTable.getData(DataSheet, "HeightType");
		String Weight = dataTable.getData(DataSheet, "Weight");
		String WeightType = dataTable.getData(DataSheet, "WeightType");
		String Smoker = dataTable.getData(DataSheet, "Smoker");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String Pregnant = dataTable.getData(DataSheet, "Pregnant");

		String Exclusion_3Years_Question_for_TPDandIP = dataTable.getData(DataSheet,
				"Exclusion_3Years_Question_for_TPDandIP");
		String Loading_3Years_Questions_for_Death50_TPD50_IP100 = dataTable.getData(DataSheet,
				"Loading_3Years_Questions_for_Death50_TPD50_IP100");
		String Loading_3Years_Questions_for_Death100_TPD100_IP150 = dataTable.getData(DataSheet,
				"Loading_3Years_Questions_for_Death100_TPD100_IP150");

		String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore = dataTable.getData(DataSheet,
				"Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");

		String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP = dataTable.getData(DataSheet,
				"Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
		String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP = dataTable.getData(DataSheet,
				"Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");

		String UsualDoctororMedicalCentre = dataTable.getData(DataSheet, "UsualDoctororMedicalCentre");
		String UsualDoctor_Name = dataTable.getData(DataSheet, "UsualDoctor_Name");
		String UsualDoctor_ContactNumber = dataTable.getData(DataSheet, "UsualDoctor_ContactNumber");
		String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50 = dataTable.getData(DataSheet,
				"Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
		String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP = dataTable.getData(DataSheet,
				"Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
		String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP = dataTable.getData(DataSheet,
				"Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
		String Drugs_Last5Years = dataTable.getData(DataSheet, "Drugs_Last5Years");
		String Alcohol_DrinksPerDay = dataTable.getData(DataSheet, "Alcohol_DrinksPerDay");
		String Alcohol_Professionaladvice = dataTable.getData(DataSheet, "Alcohol_Professionaladvice");
		String HIVInfected = dataTable.getData(DataSheet, "HIVInfected");
		String PriorApplication = dataTable.getData(DataSheet, "PriorApplication");
		String PreviousClaims = dataTable.getData(DataSheet, "PreviousClaims");
		String PreviousClaims_PaidBenefit_terminalillness = dataTable.getData(DataSheet,
				"PreviousClaims_PaidBenefit_terminalillness");
		String CurrentPolicies = dataTable.getData(DataSheet, "CurrentPolicies");
		String DeathAction = null;
		String TPDAction = null;
		String IPAction = null;
		if (SenarioType.equalsIgnoreCase("Overall")) {
		DeathAction = dataTable.getData(DataSheet, "DeathAction");
		TPDAction = dataTable.getData(DataSheet, "TPDAction");
		IPAction = dataTable.getData(DataSheet, "IPAction");
		}
		try {


			if (SenarioType.equalsIgnoreCase("Overall")) {

			if (DeathAction.equalsIgnoreCase("Increase your cover") || TPDAction.equalsIgnoreCase("Increase your cover")
					|| IPAction.equalsIgnoreCase("Increase your cover")) {

				SecondPage="Aura";
			}
			else {
				SecondPage="Confirmation";

			}
			}
				// ******Enter the Height*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='heightval']")))
						.sendKeys(Height);
				report.updateTestLog("Height", Height + " is Entered", Status.PASS);
				sleep(250);

				WebDriverWait wait = new WebDriverWait(driver, 18);
				// ******Choose Height Type******\\

				Select heighttype = new Select(driver.findElement(By.xpath("//*[@ng-model='heightDropDown']")));
				heighttype.selectByVisibleText(HeightType);
				report.updateTestLog("Height Type", HeightType + " is Selected", Status.PASS);
				sleep(500);

				// ******Enter the Weight*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='weightVal']")))
						.sendKeys(Weight);
				report.updateTestLog("Weight", Weight + " is Entered", Status.PASS);
				sleep(250);

				// ******Choose Weight Type******\\

				Select weighttype = new Select(driver.findElement(By.xpath("//*[@ng-model='weightDropDown']")));
				weighttype.selectByVisibleText(WeightType);
				report.updateTestLog("Weight Type", WeightType + " is Selected", Status.PASS);
				sleep(500);

				// ******Have you smoked in the past 12 months?*******\\

				if (Smoker.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					report.updateTestLog("Smoker Question", "Yes is Selected", Status.PASS);

				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					report.updateTestLog("Smoker Question", "No is Selected", Status.PASS);
				}
				sleep(800);

				// ******Are you currently pregnant?*******\\
				if (Gender.equalsIgnoreCase("Female")) {

					if (Pregnant.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[1]/div/div/label")))
								.click();
						report.updateTestLog("Are you currently pregnant?", "Yes is Selected", Status.PASS);

					} else {
						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[2]/div/div/label")))
								.click();
						report.updateTestLog("Are you currently pregnant?", "No is Selected", Status.PASS);
					}
					sleep(800);
				}

				// **********************************************************************************************************************\\
				// *******In the last 3 years have you suffered from, been
				// diagnosed with or sought medical advice or treatment
				// for*******\\
				// ***********************************************************************************************************************\\

				if (Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")) {
					System.out.println("3yrs yes1");
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'Headaches or migraines')]]/span")))
							.click();
					report.updateTestLog("Last 3 Years Question", "Headaches or migraines is Selected", Status.PASS);
					sleep(800);

					// ******Are you currently under investigations or
					// contemplating investigations for your headaches?*****\\

					driver.findElement(By
							.xpath(" //div/label[text()='Are you currently under investigations or contemplating investigations for your headaches?']/../following-sibling::div/div/div[2]/div/div/label"))
							.click();
					report.updateTestLog("Headache or Migraines_Under Current Investigations", "No is Selected",
							Status.PASS);
					sleep(200);

					// ******How would you describe your headaches?******\\

					Select headaches = new Select(driver.findElement(By.xpath("//*[@ng-model='selectedName']")));
					headaches.selectByVisibleText("Recurring or severe episodes");
					report.updateTestLog("Type of Headache", "Recurring or severe episodes is Selected", Status.PASS);
					sleep(800);

					// ******Select the Headache Type*******\\

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")))
							.click();
					sleep(1500);

					// ***Have your headaches been fully investigated with all
					// underlying causes excluded?****\\

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath(" //div/label[text()='Have your headaches been fully investigated with all underlying causes excluded?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(1500);
					report.updateTestLog("Is headache fully investigated", "Yes is Selected", Status.PASS);
					sleep(800);

					// *****How many headaches do you suffer in a week?******\\

					Select headachesno = new Select(driver.findElement(By.xpath("(//*[@ng-model='selectedName'])[2]")));
					headachesno.selectByVisibleText("3 episodes or less");
					sleep(1500);
					report.updateTestLog("Number of Headaches", "3 episodes or less is Selected", Status.PASS);
					sleep(1500);

					driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
					sleep(800);

					// *******Is this easily controlled with over the counter
					// medication?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Is this easily controlled with over the counter medication?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(1500);
					report.updateTestLog("Can  your headache be controlled over Medication", "Yes is Selected",
							Status.PASS);
					sleep(1500);
					System.out.println("3yrs yes2");
				}

				if (Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")
						|| Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")) {
					System.out.println("3yrs yes3");

					wait.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//*[text()[contains(.,'Lung or breathing conditions')]]/span"))).click();
					sleep(1500);
					report.updateTestLog("Last 3 Years Question", "Lung or breathing conditions is Selected",
							Status.PASS);
					sleep(1500);

					// ****What was Your Diagnosis*****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Asthma')]]/span")))
							.click();
					sleep(1500);
					report.updateTestLog("Lung or breathing conditions", "Asthma is Selected", Status.PASS);
					sleep(1500);

					// ****Is your condition?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Moderate')]]")))
							.click();
					sleep(1500);
					report.updateTestLog("Lung or breathing conditions", "Moderate Asthma is Selected", Status.PASS);
					sleep(1500);

					if (Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")) {

						// ****Is your Asthma worsened by your condition****\\

						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath(" //div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[1]/div/div/label")))
								.click();
						sleep(1500);
						report.updateTestLog("Lung or breathing conditions", "Asthma is worsened by current occupation",
								Status.PASS);
						sleep(1500);

					} else {
						// ****Is your Asthma worsened by your condition****\\

						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath(" //div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[2]/div/div/label")))
								.click();
						sleep(1500);
						report.updateTestLog("Lung or breathing conditions",
								"Asthma is not worsened by current occupation", Status.PASS);
						sleep(1500);
					}

					System.out.println("3yrs yes4");
				}

				if (Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")
						&& Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")
						&& Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")) {
					System.out.println("3yrs no1");
					sleep(1500);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[1]"))).click();
					sleep(1500);
					report.updateTestLog("Last 3 Years Question", "None of the above is Selected", Status.PASS);
					sleep(1500);
					System.out.println("3yrs no1");
				}
				sleep(1000);

				// ********************************************************************************\\
				// *****************************5 Years
				// Question***********************************\\
				// ********************************************************************************\\

				if (Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")) {
					System.out.println("5yrs yes1");

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'High cholesterol')]]/span")))
							.click();
					sleep(1500);
					report.updateTestLog("Last 5 Years Question", "High Cholesterol is Selected", Status.PASS);
					sleep(1500);

					// ******How is your high Cholesterol being
					// treated??******\\

					Select cholesteroltreatment = new Select(
							driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
					cholesteroltreatment.selectByVisibleText("Medication prescribed by your doctor");
					sleep(1500);
					report.updateTestLog("High Cholesterol", "Treated by medication prescribed by your doctor",
							Status.PASS);
					sleep(1500);

					// ******Select the Treatment Type*******\\

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")))
							.click();
					sleep(1500);

					// ******When did you last have a reading taken?*****\\

					// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'12
					// months ago or less')]]/span"))).click();
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'12 months ago or less')]]")))
							.click();
					sleep(1500);
					report.updateTestLog("High Cholesterol", "Last reading taken 12 months ago or less", Status.PASS);
					sleep(1500);

					// ****How did your doctor describe your most recent
					// cholesterol reading?*****\\

					// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Elevated')]]/span"))).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Elevated')]]")))
							.click();
					sleep(1500);
					report.updateTestLog("High Cholesterol", "Last reading was described by doctor as Elevated",
							Status.PASS);
					sleep(1500);

					// ****Do you recall your last reading?***\\

					driver.findElement(By
							.xpath("//div/label[text()='Do you recall your last reading?']/../following-sibling::div/div/div[1]/div/div/label"))
							.click();
					sleep(1500);
					report.updateTestLog("High Cholesterol", "Do you remember the last Reading- Yes", Status.PASS);
					sleep(1500);

					// *****What was your most recent cholesterol reading taken
					// by your doctor?****\\

					driver.findElement(By.xpath("(//*[@ng-model='rangeText'])[1]")).sendKeys("7.1");
					sleep(1500);
					report.updateTestLog("High Cholesterol", "Last reading is 7.1 mmol/L", Status.PASS);
					sleep(1500);

					driver.findElement(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]")).click();
					sleep(1000);

					// *****Have you been advised that your triglycerides are
					// elevated?****\\

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you been advised that your triglycerides are elevated? ']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(1500);
					report.updateTestLog("High Cholesterol", "Tryglycerides is not Elevated", Status.PASS);
					sleep(800);

					System.out.println("5yrs yes2");
				}
				if (Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")) {
					System.out.println("5yrs no1");
					sleep(1500);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[2]"))).click();
					sleep(1000);
					report.updateTestLog("Last 5 Years Question", "None of the above is Selected", Status.PASS);
					sleep(800);
					System.out.println("5yrs no2");
				}
				sleep(800);

				// ****************************************************************************************************************************************\\
				// **********Have you ever suffered from, been diagnosed with or
				// sought medical advice or treatment for (Please tick all that
				// apply):*******\\
				// *****************************************************************************************************************************************\\

				if (Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("Yes")) {
					System.out.println("6yrs no1");

					wait.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//*[text()[contains(.,'Bone, joint or limb conditions')]]/span"))).click();
					sleep(1500);
					report.updateTestLog("Prior Treatment or Diagnosis Question",
							"Bone,Joint or limb conditions is Selected", Status.PASS);
					sleep(1000);

					// *******What was your diagnosis?********\\

					wait.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//*[text()[contains(.,'Amputation/missing or deformed limb')]]/span"))).click();
					sleep(1500);
					report.updateTestLog("What was your diagnosis", "Amputation/missing or deformed limb is Selected",
							Status.PASS);
					sleep(1000);

					// *******Which limbs were amputated/missing or
					// deformed?*****\\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Arm(s)')]]/span")))
							.click();
					sleep(1500);
					report.updateTestLog("Which limbs were amputated/missing or deformed", " Arm(s) is Selected",
							Status.PASS);
					sleep(1200);

					// ****was it?****\\

					Select arm = new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
					arm.selectByVisibleText("The left arm");
					sleep(1500);
					report.updateTestLog("Which Limb", "The left arm is Selected", Status.PASS);
					sleep(1500);

					driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
					sleep(1000);

				}

				if (Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")) {

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'Thyroid conditions')]]/span")))
							.click();
					sleep(1500);
					report.updateTestLog("Prior Treatment or Diagnosis Question", "Thyroid Conditions is Selected",
							Status.PASS);
					sleep(800);

					// *****What was the specific condition*****\\

					wait.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//*[text()[contains(.,'Hyperthyroidism (Overactive)')]]/span"))).click();
					sleep(1500);
					report.updateTestLog("Thyroid Conditions", " Hyperthyroidism (Overactive) is Selected",
							Status.PASS);
					sleep(800);

					// ****Has your condition been fully investigated?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath(" //div/label[text()='Has your condition been fully investigated?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(1500);
					report.updateTestLog("Thyroid Conditions", "Condition has been fully investigated is Selected",
							Status.PASS);
					sleep(800);

					// ****Is your condition Fully Controlled*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Is your condition fully controlled?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(1500);
					report.updateTestLog("Thyroid Conditions", "Condition is not fully controlled is Selected",
							Status.PASS);
					sleep(1000);

				}

				if (Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")
						&& Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP
								.equalsIgnoreCase("No")) {
					sleep(3000);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[3]"))).click();
					sleep(3000);
					report.updateTestLog("Prior Treatment or Diagnosis", "None of the above is Selected", Status.PASS);
					sleep(2000);
				}
				sleep(500);

				// ******Do you have a usual doctor or medical centre you
				// regularly visit?******\\

				if (UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("(//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div/div/div/label)[1]")))
							.click();
					sleep(3200);
					report.updateTestLog("Is there a usual Doctor/Medical Centre you visit", "Yes is Selected",
							Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']")))
							.sendKeys(UsualDoctor_Name);
					sleep(3200);
					report.updateTestLog("Usual Doctor Name", UsualDoctor_Name + " is Entered", Status.PASS);
					sleep(1500);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']"))).click();
					sleep(3200);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeText']")))
							.sendKeys(UsualDoctor_ContactNumber);
					sleep(3200);
					report.updateTestLog("Usual Doctor Contact NUmber", UsualDoctor_ContactNumber + " is Entered",
							Status.PASS);
					sleep(1000);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeText,x)']"))).click();
					sleep(3200);

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Add another Doctor?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(3200);

				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(3200);
					report.updateTestLog("Is there a usual Doctor/Medical Centre you visit", "No is Selected",
							Status.PASS);
				}

				// ******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[1]")))
						.click();
				report.updateTestLog("Continue", "Continue button is Clicked", Status.PASS);
				sleep(3000);

				// ******************************************************************************************************\\
				// *********************************Family
				// History*********************************************************\\
				// ******************************************************************************************************\\

				// ****Has your mother, father, any brother, or sister been
				// diagnosed under the age of 55 years with any of the following
				// conditions***\\

				if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(2600);
					report.updateTestLog("Family History Question", "Yes is Selected", Status.PASS);

					// ***Select the health condition****\\

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//*[text()[contains(.,'Diabetes')]]/span)[2]"))).click();
					sleep(2600);
					report.updateTestLog("Health condition diagnosed", " Diabetes is Selected", Status.PASS);
					sleep(1500);

					// ***How many family members were affected?****\\

					driver.findElement(By
							.xpath("//div/label[text()='How many family members were affected?']/../following-sibling::div/div[1]/input"))
							.sendKeys("3");
					sleep(2500);
					report.updateTestLog("Number of Family members affected", "3 is Entered", Status.PASS);
					sleep(400);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]"))).click();
					sleep(1500);

					// ***were two or more family members diagnosed over age
					// 19****\\

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath(" //div/label[text()='Were two or more family members diagnosed over the age of 19?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(2600);
					report.updateTestLog("Number of affected Family members over age 19", "Two or more is Selected",
							Status.PASS);
					sleep(1000);

				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(2600);
					report.updateTestLog("Family History Question", "No is Selected", Status.PASS);
					sleep(1500);
				}

				// ******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[2]")))
						.click();
				report.updateTestLog("Continue", "Continue button is Clicked", Status.PASS);
				sleep(2600);

				// ********************************************************************************************************\\
				// ******************************Lifestyle
				// Questions********************************************************\\
				// *********************************************************************************************************\\

				// *******Do you have firm plans to travel or reside in another
				// country*******\\
				if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(2500);
					report.updateTestLog("Plans to travel outside the country", "Yes is Selected", Status.PASS);
					sleep(1500);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(2500);
					report.updateTestLog("Plans to travel outside the country", "No is Selected", Status.PASS);
					sleep(1000);
				}

				// ******Do you regularly engage in or intend to engage in any
				// of the following hazardous activities*****\\

				if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")) {

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'Water sports')]]/span"))).click();
					sleep(3000);
					report.updateTestLog("Do you engage in any Hazardous activities", "Water sports is Selected",
							Status.PASS);
					sleep(2500);

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'Underwater diving')]]/span")))
							.click();
					sleep(3100);
					report.updateTestLog("Hazardous activity Type", "Underwater diving is Selected", Status.PASS);
					sleep(2000);

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[text()[contains(.,'More than 40 metres')]]"))).click();
					sleep(3100);
					report.updateTestLog("Dept of diving ", "More than 40 metres is Selected", Status.PASS);
					sleep(2000);
				}

				if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
					sleep(3500);
					if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")) {
						driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[5]")).click();
						sleep(2000);
					} else {
						driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[4]")).click();
						sleep(2000);
					}

					sleep(1500);
					report.updateTestLog("Do you engage in any Hazardous activities", "None of the above is Selected",
							Status.PASS);
					sleep(500);
				}

				// *****Have you within the last 5 years used any drugs that
				// were not prescribed to you (other than those available over
				// the counter)
				// or have you exceeded the recommended dosage for any
				// medication?

				if (Drugs_Last5Years.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(2500);
					report.updateTestLog("Have you sed any drugs in the last 5 years", "Yes is Selected", Status.PASS);
					sleep(800);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(2500);
					report.updateTestLog("Have you used any drugs in the last 5 years", "No is Selected", Status.PASS);
					sleep(800);
				}

				// *****How many alcoholic drinks you have in a day?*****\\
				sleep(1500);
				driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).click();
				sleep(2000);
				driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input"))
						.sendKeys(Alcohol_DrinksPerDay);
				sleep(2500);
				report.updateTestLog("How many alcoholic drinks per day", "1 is Selected", Status.PASS);
				sleep(1000);
				driver.findElement(By.xpath("//button[text()='Enter']")).click();
				sleep(2500);

				// ****Have you ever been advised by a health professional to
				// reduce your alcohol consumption?****\\

				if (Alcohol_Professionaladvice.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(2500);
					report.updateTestLog("Advised to reduce alcohol consumption", "Yes is Selected", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(2500);
					report.updateTestLog("Advised to reduce alcohol consumption", "No is Selected", Status.PASS);
					sleep(1000);
				}

				// ******Are you infected with HIV (Human Immunodeficiency
				// Virus******\\

				if (HIVInfected.equalsIgnoreCase("Yes")) {
					sleep(3000);
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(3000);
					report.updateTestLog("Are you infected with HIV", "Yes is Selected", Status.PASS);
					sleep(1500);

				} else {
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(4000);
					report.updateTestLog("Are you infected with HIV", "No is Selected", Status.PASS);
					sleep(3000);
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Have you been referred for or waiting on an HIV test result and/or are taking preventative medication?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					report.updateTestLog("Waiting on an HIV test result", "No is Selected", Status.PASS);

					sleep(3500);
				}

				// ******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[3]")))
						.click();
				report.updateTestLog("Continue", "Continue button is Clicked", Status.PASS);
				sleep(3500);

				// ******************************************************************************************************\\
				// ****************************General
				// Questions******************************************************\\
				// *******************************************************************************************************\\

				wait.until(ExpectedConditions.elementToBeClickable(By
						.xpath("//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label")))
						.click();
				sleep(3500);
				report.updateTestLog("Do you presently suffer from any condition", "No is Selected", Status.PASS);

				// ******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[4]")))
						.click();
				report.updateTestLog("Continue", "Continue button is Clicked", Status.PASS);
				sleep(3600);

				// ***********************************************************************************************\\
				// ***************************INSURANCE
				// DETAILS****************************************************\\
				// **************************************************************************************************\\

				// *****Has an application for Life, Trauma, TPD or Disability
				// Insurance on your life ever been declined, deferred or
				// accepted with a loading or exclusion or any other special
				// conditions or terms?
				sleep(2000);
				if (PriorApplication.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//*[@id='collapseOne']/div/div[1]/div/div[2]/div/div[1]/div/div/label"))).click();
					sleep(3000);
					report.updateTestLog("Has your prior application beend Delined/Accepted with Loading",
							"Yes is Selected", Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(2500);
				}

				else {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(3000);
					report.updateTestLog("Has your prior application beend Delined/Accepted with Loading",
							"No is Selected", Status.PASS);
				}

				// ****Are you contemplating or have you ever made a claim for
				// or received sickness, accident or disability benefits,
				// Workers�
				// Compensation, or any other form of compensation due to
				// illness or injury?
				sleep(2000);
				if (PreviousClaims.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[1]/div/div/label")))
							.click();
					sleep(3000);
					report.updateTestLog("Have you made a prior claim", "Yes is Selected", Status.PASS);

					if (PreviousClaims_PaidBenefit_terminalillness.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[1]/div/div/label/span")))
								.click();
						sleep(3000);
					} else {
						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[2]/div/div/label/span")))
								.click();
						sleep(3000);
					}

				} else {
					sleep(400);
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[2]/div/div/label")))
							.click();
					sleep(3000);
					report.updateTestLog("Have you made a prior claim", "No is Selected", Status.PASS);
				}

				if (driver.getPageSource().contains(
						"Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?")) {

					// *****Do you currently have or are you applying for any
					// Life, Trauma, TPD or Disability Insurance policies with
					// us or any other insurance company or superannuation fund?
					if (CurrentPolicies.equalsIgnoreCase("yes")) {
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-19:0']")))
								.click();
						sleep(3000);
						report.updateTestLog("Do you currently have any insurance policies", "Yes is Selected",
								Status.PASS);

						wait.until(
								ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-19_0_0:0']")))
								.click();
						sleep(3200);

						wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-19_0_0_0_0"))).sendKeys("90000");
						sleep(3200);

						wait.until(ExpectedConditions
								.elementToBeClickable(By.id("ran219_0_0_0_0-Insurance_Details-Insurance_Details-19")))
								.click();
						sleep(3200);
					} else {
						wait.until(ExpectedConditions.elementToBeClickable(By
								.xpath("//div/label[text()='Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?']/../following-sibling::div/div/div[2]/div/div/label")))
								.click();
						sleep(3200);
						report.updateTestLog("Do you currently have any insurance policies", "No is Selected",
								Status.PASS);
					}

				}

				// ****insurance detail click on Continue****\\

				/*
				 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				 * "(//button[@ng-click='collapseUncollapse($index,section.sectionName)'])[5]"
				 * ))).click();
				 * report.updateTestLog("Continue","Continue button is Clicked",
				 * Status.PASS); sleep(3000);
				 *
				 * sleep(300); JavascriptExecutor js = (JavascriptExecutor)
				 * driver; js.executeScript("window.scrollBy(0,500)", "");
				 * sleep(500);
				 */

				// ****Treatment question****\\

				/*
				 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				 * "(.//input[@class='ipradio']/parent::*)[11]"))).click();
				 * report.updateTestLog("Treatment Question"
				 * ,"No button is Clicked", Status.PASS); sleep(2500);
				 */

				if ((SenarioType.equalsIgnoreCase("Overall"))) {
					String SaveExit_Aura = dataTable.getData(DataSheet, "SaveExit_HealthLifestyle");
					if (SaveExit_Aura.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='saveAura()']")))
								.click();
						sleep(3000);
						report.updateTestLog("Save and Exit- Aura", "Save and Exit button is selected", Status.PASS);
						saveAndExitPopUP();
						validateChangeCoverAURA();
					} else {
						// ****Click on Continue****\\
						sleep(3000);
						wait.until(
								ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedToNext()']")))
								.click();
						report.updateTestLog("Continue", "Continue button is Clicked", Status.PASS);
						sleep(3000);
						if (Decline_DueToAura.equalsIgnoreCase("Yes")) {
					    	 declinePopUP();
						}
					     else {
					    	 confirmation();
						}
					}
				} else {
					// ****Click on Continue****\\
					sleep(3000);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedToNext()']")))
							.click();
					report.updateTestLog("Continue", "Continue button is Clicked", Status.PASS);
					sleep(3000);



					confirmation();
				}

				// sleep(20000);
				// waitForPageTitle("testing", 15);



		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void confirmation() {
		sleep(3000);
		System.out.println("confirmation");
		String Confirmation_DetailsValidations = dataTable.getData(DataSheet, "Confirmation_DetailsValidations");
		System.out.println(Confirmation_DetailsValidations);
		if (Confirmation_DetailsValidations.equalsIgnoreCase("Yes")
				|| Confirmation_DetailsValidations.equalsIgnoreCase("Occupation")) {
			detailsValidation();
		}

		if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			return;
		} else if (SenarioType.equalsIgnoreCase("Overall")) {
			String DecisionType = dataTable.getData(DataSheet, "DecisionType");
			String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
			String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");
			String LoadExc = dataTable.getData(DataSheet, "Loading_OR_Exclusion");
			String GeneralConsent = dataTable.getData(DataSheet, "GeneralConsent");
			if (LoadExc.equalsIgnoreCase("Yes")) {
				System.out.println("Inside Load Exc");

					// ******Agree to Loading and Exception *******\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='lodadingExclusionLabel']/span")))
							.click();
					report.updateTestLog("Loading and Exception", "selected the Checkbox", Status.PASS);
					sleep(3000);

			}

			try {


					// ******Agree to general Consent*******\\

				if (driver.getPageSource().contains("General consent") ) {
					String part2;
					String[] value;
					String HeaderDD = null;
					int flag=1;
							wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_Confirmation_TermsConditionsCheckBox));
							String[] parts = GeneralConsent.split("\\n\\n");



							 int j=1;
							for (int a = 0; a < 4; a++) {

								if ( !(a==3) ) {
									HeaderDD = driver.findElement(By.xpath("(.//*[@id='collapse4']/div/div/div/p)["+j+"]")).getText();
									j++;

									 if (HeaderDD.equalsIgnoreCase(parts[a])) {

										}
										else {
											flag =0;
											report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
										}

								}
								else {
									System.out.println("<><><><><><>");

									String[] parts1 = parts[a].split("\\n");
									System.out.println("1)"+parts1[0]);
									System.out.println("2)"+parts1[1]);
									System.out.println("3)"+parts1[2]);
									System.out.println("4)"+parts1[3]);
									System.out.println("5)"+parts1[4]);
									System.out.println("6)"+parts1[5]);
									System.out.println("7)"+parts1[6]);
									System.out.println("8)"+parts1[7]);
									System.out.println("9)"+parts1[8]);
									System.out.println("10)"+parts1[9]);


									int k=1;
									for (int b = 0; b < 10; b++) {


											 HeaderDD = driver.findElement(By.xpath(".//*[@id='collapse4']/div/div/div/ul/li["+k+"]")).getText();

										 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

										 }
											else {
												flag =0;
												report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
											}

										k++;

									}

								}
							}

							if (flag==0) {
								report.updateTestLog("General Consent", "General Consent - Statement is not as expected", Status.FAIL);

							}
							else {
								report.updateTestLog("General Consent", "General Consent - Statement is present as expected", Status.PASS);
							}

							sleep(2000);

								wait.until(ExpectedConditions
										.elementToBeClickable(ChangeCoverObjects.obj_Confirmation_TermsConditionsCheckBox)).click();
								report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
								sleep(3000);
				}


					if ((SenarioType.equalsIgnoreCase("Overall"))) {
						String SaveExit_Confirmation = dataTable.getData(DataSheet, "SaveExit_Confirmation");
						if (SaveExit_Confirmation.equalsIgnoreCase("Yes")) {
							wait.until(
									ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_Confirmation_Save))
									.click();
							sleep(3000);
							report.updateTestLog("Save and Exit- Confirmation", "Save and Exit button is selected",
									Status.PASS);
							saveAndExitPopUP();
							validateChangeCoverConfirm();
							return;
						} else {
							// ****Click on Continue****\\
							sleep(3000);
							wait.until(
									ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_Confirmation_Submit))
									.click();
							report.updateTestLog("Submit", "Submit button is Clicked", Status.PASS);
							sleep(5000);
						}
					}

				// *******Feedback popup******\\

				wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_DecisionPopUP_No)).click();
				report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
				sleep(2500);

				if (driver.getPageSource().contains("APPLICATION NUMBER")||driver.getPageSource().contains("Application Number")||driver.getPageSource().contains("Application number")) {

					// *****Fetching the Application Status*****\\

					String Appstatus = driver.findElement(ChangeCoverObjects.obj_DecisionPopUP_Appstatus).getText();
					sleep(500);

					if (Appstatus.equalsIgnoreCase(DecisionHeader)) {
						report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
					}

					String AppMsg = driver.findElement(ChangeCoverObjects.obj_DecisionPopUP_AppMsg).getText();
					sleep(500);
					System.out.println("////"+AppMsg+"////");

					/*if (DecisionType.equalsIgnoreCase("RUW")) {
						AppMsg= driver.findElement(By.xpath("//*[@id='collapseOne']/div/div/div")).getText();
						System.out.println("////"+AppMsg+"////");
					}*/
					if (AppMsg.equals(DecisionMsg)) {
						report.updateTestLog("Decision Message", "Decision Message - "+AppMsg+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+DecisionMsg+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
					}
						/*
						int flag1 =1;
						String part;

						String[] p = DecisionMsg.split("\\n\\n");
						String msg1 = p[0]+"\n\n"+p[1];

						String msg3 = p[3];
						if (AppMsg.equals(msg1)) {

						}else {
							flag1 = 0;
							report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+msg1+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
						}
						String[] p1 = p[2].split("\\n");
						int k=1;
						for (int i = 0; i <3; i++) {
							AppMsg = driver.findElement(By.xpath("//*[@id='collapseOne']/div/div/div/ul/li["+k+"]")).getText();
							if (AppMsg.equals(p1[i])) {

							}else {
								flag1 = 0;
								report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+p1[i]+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
							}
							k++;
						}
						String AppMsg1= driver.findElement(By.xpath("//*[@id='collapseOne']/div/div/div")).getText();
						System.out.println("???"+AppMsg1);
						AppMsg = driver.findElement(By.xpath("//*[@id='collapseOne']/div/div/div/br")).getText();
						System.out.println("3Val-"+AppMsg+"-----------");
						System.out.println("3Val-"+msg3+"-----------");
						if (AppMsg.equals(msg3)) {

						}else {
							flag1 = 0;
							report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+msg3+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
						}

						if (flag==0) {
							report.updateTestLog("Decision Message- RUW", "Decision Message- RUW - Statement is not as expected", Status.FAIL);
						}
						else {
							report.updateTestLog("Decision Message- RUW", "Decision Message- RUW - Statement is present as expected", Status.PASS);

						}

					}*/

					//else {


					//}
					// *****Fetching the Application Number******\\

					String App = driver.findElement(ChangeCoverObjects.obj_DecisionPopUP_App).getText();
					sleep(1000);

					wait.until(
							ExpectedConditions.presenceOfElementLocated(ChangeCoverObjects.obj_Confirmation_Download))
							.click();
					sleep(3000);

					report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

					sleep(1000);

					report.updateTestLog("Decision Page", "Application No: " + App, Status.PASS);
					sleep(500);

				}

				else {
					report.updateTestLog("Application Declined", "Application is declined", Status.FAIL);

				}

			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);

			}

		}

	}
private void declinePopUP() {
	String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
	String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");

		wait.until(ExpectedConditions.elementToBeClickable(TransferCoverObjects.obj_DecisionPopUP_No)).click();
		report.updateTestLog("Application Decline", "Application is declined", Status.PASS);
		report.updateTestLog("Application Decline", "Selected No on the Feedback popup", Status.PASS);

		sleep(2500);

		String Appstatus = driver.findElement(ChangeCoverObjects.obj_DecisionPopUP_Appstatus).getText();
		sleep(500);

		if (Appstatus.equalsIgnoreCase(DecisionHeader)) {
			report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
		}else {
			report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
		}

		String AppMsg = driver.findElement(ChangeCoverObjects.obj_DecisionPopUP_AppMsg).getText();
		sleep(500);
		System.out.println("////"+AppMsg+"////");
		if (AppMsg.equals(DecisionMsg)) {
			report.updateTestLog("Decision Message", "Decision Message - "+AppMsg+" is present", Status.PASS);
		}else {
			report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+DecisionMsg+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
		}




	}
	public void detailsValidation() {

		if (SenarioType.equalsIgnoreCase("Overall")) {
			String Gender = dataTable.getData(DataSheet, "Gender");
			String EmailId = dataTable.getData(DataSheet, "EmailId");
			String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
			String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
			String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
			String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
			String Citizen = dataTable.getData(DataSheet, "Citizen");
			String IndustryType = dataTable.getData(DataSheet, "IndustryType");
			String OccupationType = dataTable.getData(DataSheet, "OccupationType");
			String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
			String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
			String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
			String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");

			String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
			// String DeathAmount = dataTable.getData(DataSheet,
			// "DeathAmount");
			String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
			String IPAmount = dataTable.getData(DataSheet, "IPAmount");
			String WaitingPeriod = dataTable.getData(DataSheet, "WaitingPeriod");
			String BenefitPeriod = dataTable.getData(DataSheet, "BenefitPeriod");

			AnnualSalary = "$" + AnnualSalary;
			DeathAmount = "$" + DeathAmount;
			TPDAmount = "$" + TPDAmount;
			IPAmount = "$" + IPAmount;

			String XML = dataTable.getData("VicSuper_Login", "XML");

			String[] part;
			String part2;
			String[] value;

			// To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1];
			value = part2.split("</dateOfBirth>");
			String dob = value[0];

			// Existing Cover Death
			part = XML.split("<amount>");
			String part2Death = part[1];
			String part2Tpd = part[2];
			String part2IP = part[3];

			String[] valueDeath = part2Death.split("</amount>");
			String[] valueTpd = part2Tpd.split("</amount>");
			String[] valueIP = part2IP.split("</amount>");

			String Death = valueDeath[0];
			String TPD = valueTpd[0];
			String IP = valueIP[0];

			double dD = Double.parseDouble(Death);
			int D = (int) dD;
			double dT = Double.parseDouble(TPD);
			int T = (int) dT;
			double dI = Double.parseDouble(IP);
			int I = (int) dI;

			Death = "$" + D;
			TPD = "$" + T;
			IP = "$" + I;
			System.out.println(Death);
			System.out.println(TPD);
			System.out.println(IP);
			// First Name
			WebElement objFName = driver.findElement(ChangeCoverObjects.obj_Confirmation_FirstName);
			fieldValidation("WebElement", objFName, VicSuper__Login.firstName, "First Name");

			// Last Name
			WebElement objLName = driver.findElement(ChangeCoverObjects.obj_Confirmation_LastName);
			fieldValidation("WebElement", objLName, VicSuper__Login.lastName, "Last Name");

			// Gender
			WebElement ObjGender = driver.findElement(ChangeCoverObjects.obj_Confirmation_Gender);
			fieldValidation("WebElement", ObjGender, Gender, "Genderquestion");

			// Phone Number type
			WebElement objcontactType = driver.findElement(ChangeCoverObjects.obj_Confirmation_ContactType);
			fieldValidation("WebElement", objcontactType, TypeofContact, "contactType");

			// PhoneNumber
			WebElement objContactNo = driver.findElement(ChangeCoverObjects.obj_Confirmation_ContactNo);
			fieldValidation("WebElement", objContactNo, ContactNumber, "ContactNo");

			// Email
			WebElement objcontactEmail = driver.findElement(ChangeCoverObjects.obj_Confirmation_Email);
			fieldValidation("WebElement", objcontactEmail, EmailId, "Email");
			js.executeScript("window.scrollBy(0,500)", "");
			// DOB
			WebElement objDOB = driver.findElement(ChangeCoverObjects.obj_Confirmation_DOB);
			fieldValidation("WebElement", objDOB, dob, "DOB");

			// Morning Afternoon
			WebElement objTime = driver.findElement(ChangeCoverObjects.obj_Confirmation_Time);
			String eletext = objTime.getText();
			System.out.println(eletext);
			System.out.println(TimeofContact);
			if (eletext.contains(TimeofContact)) {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is displayed",
						Status.PASS);

			} else {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is not displayed",
						Status.FAIL);

			}

			// Do you work more than 14 hours per week?

			WebElement ObjWorkHRS = driver.findElement(ChangeCoverObjects.obj_Confirmation_HoursWork);
			fieldValidation("WebElement", ObjWorkHRS, FourteenHoursWork, "FourteenHoursWork");

			// Are you a citizen or permanent resident of Australia? Yes No

			WebElement objCitizen = driver.findElement(ChangeCoverObjects.obj_Confirmation_Citizen);
			fieldValidation("WebElement", objCitizen, Citizen, "Citizen");

			// TransferIndustry list box

			WebElement objtransferIndustry = driver.findElement(ChangeCoverObjects.obj_Confirmation_Industry);
			fieldValidation("WebElement", objtransferIndustry, IndustryType, "TransferIndustry");

			// Current Occupation
			WebElement objoccupationTransfer = driver.findElement(ChangeCoverObjects.obj_Confirmation_Occupation);
			fieldValidation("WebElement", objoccupationTransfer, OccupationType, "occupationTransfer");

			// office Duties are Undertaken within an Office Environment?
			WebElement eduIns = driver.findElement(ChangeCoverObjects.obj_Confirmation_AddQuest1a);
			fieldValidation("WebElement", eduIns, EducationalInstitution,
					"Are the duties of your regular occupation limited to Question1");

			WebElement eduDut = driver.findElement(ChangeCoverObjects.obj_Confirmation_AddQuest1b);
			fieldValidation("WebElement", eduDut, EducationalDuties,
					"Are the duties of your regular occupation limited to Question2");

			// TertiaryQualification / ManagementRole

			WebElement objQual = driver.findElement(ChangeCoverObjects.obj_Confirmation_TertiaryQualification);
			fieldValidation("WebElement", objQual, TertiaryQual, "TertiaryQualification / ManagementRole");

			// Current annual salary?
			WebElement objannualSalCCId = driver.findElement(ChangeCoverObjects.obj_Confirmation_AnnualIncome);
			fieldValidation("Amount Validation", objannualSalCCId, AnnualSalary, "AnnualSalary");

			Death = "$" + D;
			TPD = "$" + T;
			IP = "$" + I;
			js.executeScript("window.scrollBy(0,500)", "");

			// Death

			// Death Existing Amount

			WebElement deathEXTAmount = driver.findElement(ChangeCoverObjects.obj_Confirmation_Death_ExistingCover);
			//fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");

			String PagedeathEXTAmount = deathEXTAmount.getText();
			PagedeathEXTAmount = PagedeathEXTAmount.replaceFirst(",", "");
			//fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");
			System.out.println(PagedeathEXTAmount);
			System.out.println(Death);
if (PagedeathEXTAmount.contains(Death)) {
	report.updateTestLog("Death Existing Cover", "Death Existing Cover" + PagedeathEXTAmount + " is displayed",
			Status.PASS);

}
else {
	report.updateTestLog("Death Existing Cover", "Death Existing Cover" + PagedeathEXTAmount + " is not displayed",
			Status.FAIL);
}

			// Death Amount

			WebElement deathAmount = driver.findElement(ChangeCoverObjects.obj_Confirmation_Death_NewCover);
			//fieldValidation("Amount Validation", deathAmount, DeathAmount, "Death New Cover");

			String PagedeathAmount = deathAmount.getText();
			PagedeathAmount = PagedeathAmount.replaceFirst(",", "");
			//fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");
			System.out.println(PagedeathAmount);
			System.out.println(Death);
if (PagedeathAmount.contains(DeathAmount)) {
	report.updateTestLog("Death New Cover", "Death New Cover" + PagedeathAmount + " is displayed",
			Status.PASS);

}
else {
	report.updateTestLog("Death New Cover", "Death New Cover" + PagedeathAmount + " is not displayed",
			Status.FAIL);
}

			// amountValidator("Death Amount",deathAmount,DeathAmount );

			/*
			 * // Death Weekly Cost WebElement DeathWeeklycost =
			 * driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[1]//following::p"));
			 * fieldValidation("WebElement",DeathWeeklycost,
			 * DeathWeeklyCost,"Death Weekly Cost");
			 */

			// TPD Amount

			// TPD Existing Amount

			WebElement tpdExtAmount = driver.findElement(ChangeCoverObjects.obj_Confirmation_TPD_ExistingCover);
			//fieldValidation("Amount Validation", tpdExtAmount, TPD, "TPD Existing Cover");
			String PagetpdExtAmount = tpdExtAmount.getText();
			PagetpdExtAmount = PagetpdExtAmount.replaceFirst(",", "");
			System.out.println(PagetpdExtAmount);
			System.out.println(TPD);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			if (PagetpdExtAmount.contains(TPD)) {
				report.updateTestLog("TPD ExistingCover Cover", "TPD ExistingCover Cover" + PagetpdExtAmount + " is displayed",
						Status.PASS);

			}
			else {
				report.updateTestLog("TPD ExistingCover Cover", "TPD ExistingCover Cover" + PagetpdExtAmount + " is not displayed",
						Status.FAIL);
			}

			// TPD Amount

			WebElement tpdAmount = driver.findElement(ChangeCoverObjects.obj_Confirmation_TPD_NewCover);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			String PagetpdAmount = tpdAmount.getText();
			PagetpdAmount = PagetpdAmount.replaceFirst(",", "");
			System.out.println(PagetpdAmount);
			System.out.println(TPD);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			if (PagetpdAmount.contains(TPDAmount)) {
				report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdAmount + " is displayed",
						Status.PASS);

			}
			else {
				report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdAmount + " is not displayed",
						Status.FAIL);
			}
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// TPD Weekly Cost

			/*
			 * WebElement tpdWeeklycost = driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[2]//following::p"));
			 * fieldValidation("WebElement",tpdWeeklycost,
			 * TPDWeeklyCost,"TPD Weekly Cost");
			 */
//temp
			// IP Amount

			// IP Existing Amount

			WebElement ipExtAmount = driver.findElement(ChangeCoverObjects.obj_Confirmation_IP_ExistingCover);
			//fieldValidation("Amount Validation", ipAmount, IP, "IP Existing Cover");
			String PageipExtAmount = ipExtAmount.getText();
			PageipExtAmount = PageipExtAmount.replaceFirst(",", "");
			System.out.println(PageipExtAmount);
			System.out.println(IP);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			if (PageipExtAmount.contains(IP)) {
				report.updateTestLog("IP ExistingCover Cover", "IP ExistingCover Cover" + PageipExtAmount + " is displayed",
						Status.PASS);

			}
			else {
				report.updateTestLog("TPD ExistingCover Cover", "IP ExistingCover Cover" + PageipExtAmount + " is not displayed",
						Status.FAIL);
			}
			// IP Amount

			WebElement ipAmount = driver.findElement(ChangeCoverObjects.obj_Confirmation_IP_NewCover);
			//fieldValidation("Amount Validation", ipExtAmount, IPAmount, "IP New Cover");
			String PageipAmount = ipAmount.getText();
			PageipAmount = PageipAmount.replaceFirst(",", "");
			System.out.println(PageipAmount);
			System.out.println(IP);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			if (PageipAmount.contains(IPAmount)) {
				report.updateTestLog("IP New Cover", "IP New Cover" + PageipAmount + " is displayed",
						Status.PASS);

			}
			else {
				report.updateTestLog("TPD New Cover", "IP New Cover" + PageipAmount + " is not displayed",
						Status.FAIL);
			}
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// Waiting Period

			WebElement ipWaitingPeriod = driver.findElement(ChangeCoverObjects.obj_Confirmation_IP_WaitingPeriod);
			fieldValidation("WebElement", ipWaitingPeriod, WaitingPeriod, "Waiting Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// Benefit Period

			WebElement ipBenefitPeriod = driver.findElement(ChangeCoverObjects.obj_Confirmation_IP_BenefitPeriod);
			fieldValidation("WebElement", ipBenefitPeriod, BenefitPeriod, "Benefit Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );

		}

		else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			String OutcomeForDeath = dataTable.getData(DataSheet, "OutcomeForDeath");
			String OutcomeForTPD = dataTable.getData(DataSheet, "OutcomeForTPD");
			String OutcomeForIP = dataTable.getData(DataSheet, "OutcomeForIP");

			WebElement OccupationalCategory_Death = driver
					.findElement(ChangeCoverObjects.obj_Confirmation_Death_OccupationalCategory);
			js.executeScript("arguments[0].scrollIntoView(true);", OccupationalCategory_Death);
			fieldValidation("WebElement", OccupationalCategory_Death, OutcomeForDeath, "OccupationalCategory - Death");

			WebElement OccupationalCategory_tpd = driver
					.findElement(ChangeCoverObjects.obj_Confirmation_TPD_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_tpd, OutcomeForTPD, "OccupationalCategory - TPD");

			WebElement OccupationalCategory_ip = driver
					.findElement(ChangeCoverObjects.obj_Confirmation_IP_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_ip, OutcomeForIP, "OccupationalCategory - IP");

		}

	}

	public void occupationRatingInputValidation() {
		System.out.println("starttttttttt-" + DataSheet);
		String ExistingDeath = dataTable.getData(DataSheet, "ExistingDeath");
		String ExistingTPD = dataTable.getData(DataSheet, "ExistingTPD");
		String ExistingIP = dataTable.getData(DataSheet, "ExistingIP");
		System.out.println("enddddddddddddd");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;

		String[] value;

		// To get firstName
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(ExistingDeath))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + ExistingDeath + ".", Status.FAIL);
		}
		if (!(occRating2.equals(ExistingTPD))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + ExistingTPD + ".", Status.FAIL);
		}
		if (!(occRating3.equals(ExistingIP))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + ExistingIP + ".", Status.FAIL);
		}
		System.out.println("///valend");
	}

	public void PremiumRateInputValidation() {

		String DOB = dataTable.getData(DataSheet, "DOB");
		String Rating = dataTable.getData(DataSheet, "Rating");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;
		String DateOfBirth;
		String[] value;

		// To get DOB
		part = XML.split("<dateOfBirth>");
		DateOfBirth = part[1];
		value = DateOfBirth.split("</dateOfBirth>");
		String date = value[0];
		if (!(date.equals(DOB))) {
			report.updateTestLog("DOB",
					"DOB in XML does not match with the input DOB. Inupt-" + DOB + "/ Output-" + date + ".",
					Status.FAIL);
		}
		// To get occupation
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + Rating + ".", Status.FAIL);
		}
		if (!(occRating2.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + Rating + ".", Status.FAIL);
		}
		if (!(occRating3.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + Rating + ".", Status.FAIL);
		}
		System.out.println("///valend");
	}

	public void cover_Overall() {
		String CostType = dataTable.getData(DataSheet, "CostType");
		String CoverChange = dataTable.getData(DataSheet, "CoverChange");
		String DeathYN = dataTable.getData(DataSheet, "DeathYN");
		String AmountType = dataTable.getData(DataSheet, "AmountType");
		String DeathAction = dataTable.getData(DataSheet, "DeathAction");
		String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
		String DeathUnits = dataTable.getData(DataSheet, "DeathUnits");
		String TPDYN = dataTable.getData(DataSheet, "TPDYN");
		String TPDAction = dataTable.getData(DataSheet, "TPDAction");
		String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
		String TPDUnits = dataTable.getData(DataSheet, "TPDUnits");
		String IPYN = dataTable.getData(DataSheet, "IPYN");
		String IPAction = dataTable.getData(DataSheet, "IPAction");
		String Insure85Percent = dataTable.getData(DataSheet, "Insure85Percent");
		String WaitingPeriod = dataTable.getData(DataSheet, "WaitingPeriod");
		String BenefitPeriod = dataTable.getData(DataSheet, "BenefitPeriod");
		String IPAmount = dataTable.getData(DataSheet, "IPAmount");
		String IPUnits = dataTable.getData(DataSheet, "IPUnits");

		// ******Select cover Type*******\\

			Select COSTTYPE = new Select(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_PremiumFrequency));
			COSTTYPE.selectByVisibleText(CostType);
			sleep(1000);
			report.updateTestLog("Cost of Insurance", "Cost frequency selected is: " + CostType, Status.PASS);

		// *******Death Cover Section******\\
		if (CoverChange.equalsIgnoreCase("Yes")) {
			// if (!(SenarioType.equalsIgnoreCase("PremiumCalculation"))) {

			// }

			if ((DeathYN.equalsIgnoreCase("No")) && (TPDYN.equalsIgnoreCase("No")) && (IPYN.equalsIgnoreCase("No"))) {
				report.updateTestLog("DataTable",
						"Invalid value for CoverChange ( DeathYN, TPDYN, IPYN -Any one must be changed since ChangeCover is Yes) ",
						Status.FAIL);
			}

			else {
				SecondPage = "Aura";
				/*if ((DeathAction.equalsIgnoreCase("Increase your cover"))
						|| (TPDAction.equalsIgnoreCase("Increase your cover"))
						|| (IPAction.equalsIgnoreCase("Increase your cover"))) {
					SecondPage = "Aura";
				}*/
				if (DeathYN.equalsIgnoreCase("Yes")) {
					// To select Cover Type
					if (AmountType.equalsIgnoreCase("Fixed")) {
						wait.until(
								ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Fixed))
								.click();
						sleep(1000);
						report.updateTestLog("Amount Type", "Fixed radio button is Selected", Status.PASS);
						// indexation();

					} else if (AmountType.equalsIgnoreCase("unit")) {

						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Unitised)).click();
						report.updateTestLog("Amount Type", "Unitized radio button is Selected", Status.PASS);
						sleep(1000);
					} else {
						report.updateTestLog("DataTable", "Invalid value for AmountType", Status.FAIL);
					}

					// To enter Cover Amount
					if (AmountType.equalsIgnoreCase("Fixed")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed))
								.sendKeys(DeathAmount);
						sleep(800);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed))
								.click();
						sleep(1000);
						report.updateTestLog("Death Cover Amount", DeathAmount + " is entered in Fixed", Status.PASS);

					} else if (AmountType.equalsIgnoreCase("unit")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
								.clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
								.sendKeys(DeathUnits);
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
						.click();
						sleep(1000);
						report.updateTestLog("Death Cover Unit", DeathUnits + " is entered in Unitized",
								Status.PASS);
					} else {
						report.updateTestLog("DataTable", "Invalid value for AmountType", Status.FAIL);
					}

				}

				// ----------------------------------------
				if (TPDYN.equalsIgnoreCase("Yes")) {

					if (AmountType.equalsIgnoreCase("Fixed")) {
						// To enter Cover Amount
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed))
								.sendKeys(TPDAmount);
						sleep(800);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised))
						.click();
						sleep(1000);
						report.updateTestLog("TPD Cover Amount", TPDAmount + " is entered in Fixed", Status.PASS);


					} else if (AmountType.equalsIgnoreCase("unit")) {
						// To enter Cover Amount
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
								.sendKeys(TPDUnits);
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised))
						.click();
						sleep(1000);
						report.updateTestLog("TPD Cover Unit", TPDUnits + " is entered in Unitized", Status.PASS);
					} else {
						report.updateTestLog("DataTable", "Invalid value for AmountType", Status.FAIL);
					}

				}

				// ----------------------------------------
				if (IPYN.equalsIgnoreCase("Yes")) {

					// *****Insure 85% of My Salary******\\

					if (Insure85Percent.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Insure85OfMySalary))
								.click();
						report.updateTestLog("Insure 85%", "Insure 85% of My Salary is Selected", Status.PASS);
						sleep(500);
					}

					else {
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised))
								.sendKeys(IPUnits);
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_WaitingPeriod))
								.sendKeys(Keys.ENTER);
						sleep(1000);
						report.updateTestLog("IP Cover Amount", IPUnits + " is entered in Unitized", Status.PASS);
					}

					Select waitingperiod = new Select(
							driver.findElement(ChangeCoverObjects.obj_PersonalDetails_WaitingPeriod));
					waitingperiod.selectByVisibleText(WaitingPeriod);
					sleep(2000);
					report.updateTestLog("Waiting Period", "Waiting Period of: " + WaitingPeriod + " is Selected",
							Status.PASS);
					sleep(500);

					Select benefitperiod = new Select(
							driver.findElement(ChangeCoverObjects.obj_PersonalDetails_BenefitPeriod));
					benefitperiod.selectByVisibleText(BenefitPeriod);
					sleep(2000);
					report.updateTestLog("Benefit Period", "Benefit Period: " + BenefitPeriod + " is Selected",
							Status.PASS);
					sleep(2000);
					if ((BenefitPeriod.equalsIgnoreCase("5 Years")) || (BenefitPeriod.equalsIgnoreCase("Age 65"))) {
						// Check box Validation for the member is not a casual
						// or contractor employee


							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_CasualORContractor))
									.click();
							report.updateTestLog("Casual or contractor employee",
									"The member is not a casual or contractor employee is selected", Status.PASS);
							sleep(3000);

					}

					// To enter Cover Amount

				}
				/*
				 * else { SecondPage = "Confirmation"; }
				 */

			}

		} else if (CoverChange.equalsIgnoreCase("No")) {
			SecondPage = "Confirmation";
		}

	}

	public void cover_Premium() {
		String Age = dataTable.getData(DataSheet, "Age");
		String Type = dataTable.getData(DataSheet, "Type");

		if (Type.equalsIgnoreCase("GroupLife")) {

			String AmountType = dataTable.getData(DataSheet, "AmountType");
			String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
			String DeathCost = dataTable.getData(DataSheet, "DeathCost");
			String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
			String TPDCost = dataTable.getData(DataSheet, "TPDCost");
			//String DeathTPD = dataTable.getData(DataSheet, "DeathTPD");

			String DeathCostPage;
			String TPDCostPage;
			String DeathTPDCostPage;
			Double Total = 0.00;

			// Select Cost Type
			Select COSTTYPE = new Select(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_PremiumFrequency));
			//DecimalFormat f = new DecimalFormat("##0.00");
			int AgeValue = Integer.parseInt(Age);
			if (AmountType.equalsIgnoreCase("Fixed")) {

				COSTTYPE.selectByVisibleText("Yearly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Yearly is selected", Status.PASS);

				WebElement Death = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_DeathCost);
				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				double DeathCst = Double.parseDouble(DeathCost);
				//String DeathCost1 =  String.format("%.02f", DeathCst);

				amountValidation(DeathCostPage, DeathCst, "Death Cost");
				// Total += PageDeathCost;
				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					WebElement TPD = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPDCost);
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCst = Double.parseDouble(TPDCost);
					//String TPDCost1 = String.format("%.02f", TPDCst);

					amountValidation(TPDCostPage, TPDCst, "TPD Cost");
					// Total += PageTPDCost;
				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}
				/*DeathTPDCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TotalCostYearly))
						.getText();
				double DeathTPDCst = Double.parseDouble(DeathTPD);
				DeathTPD = String.format("%.02f", DeathTPDCst);

				amountValidation(DeathTPDCostPage, DeathTPD, "Total Cost");*/

				COSTTYPE.selectByVisibleText("Monthly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Monthly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostmonthly = Double.parseDouble(DeathCost);
				DeathCostmonthly = DeathCostmonthly / 12;
				//String DeathCost2 = String.format("%.02f", DeathCostmonthly);

				amountValidation(DeathCostPage, DeathCostmonthly, "Death Cost");
				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostmonthly = Double.parseDouble(TPDCost);
					TPDCostmonthly = TPDCostmonthly / 12;
					//String TPDCost2 = String.format("%.02f", TPDCostmonthly);

					amountValidation(TPDCostPage, TPDCostmonthly, "TPD Cost");
				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}

				COSTTYPE.selectByVisibleText("Weekly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Weekly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostweekly = Double.parseDouble(DeathCost);
				DeathCostweekly = DeathCostweekly / 52;
				//String DeathCost3 = String.format("%.02f", DeathCostweekly);

				amountValidation(DeathCostPage, DeathCostweekly, "Death Cost");

				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostweekly = Double.parseDouble(TPDCost);
					TPDCostweekly = TPDCostweekly / 52;
					//String TPDCost3 = String.format("%.02f", TPDCostweekly);

					amountValidation(TPDCostPage, TPDCostweekly, "TPD Cost");
				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}

			} else if (AmountType.equalsIgnoreCase("unit")) {

				COSTTYPE.selectByVisibleText("Weekly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Weekly is selected", Status.PASS);

				//Cost - Amount Validation
				WebElement Death = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_DeathCost);
				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				double DeathCst = Double.parseDouble(DeathCost);
				//String DeathCost1 =  String.format("%.02f", DeathCst);

				amountValidation(DeathCostPage, DeathCst, "Death Cost");

				sleep(3000);

				String DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmt = Double.parseDouble(DeathAmount);
				//String DeathAmt1 =  String.format("%.02f", DeathAmt);
				amountValidation(DeathAmountPage, DeathAmt, "Death Amount");



				// Total += PageDeathCost;
				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					WebElement TPD = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPDCost);
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCst = Double.parseDouble(TPDCost);
					//String TPDCost1 = String.format("%.02f", TPDCst);

					amountValidation(TPDCostPage, TPDCst, "TPD Cost");

					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmt = Double.parseDouble(TPDAmount);
					//String TPDAmt1 =  String.format("%.02f", TPDAmt);
					amountValidation(DeathAmountPage, TPDAmt, "TPD Amount");


				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}
				/*DeathTPDCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TotalCostWeekly))
						.getText();
				double DeathTPDCst = Double.parseDouble(DeathTPD);
				DeathTPD = String.format("%.02f", DeathTPDCst);

				amountValidation(DeathTPDCostPage, DeathTPD, "Total Cost");*/



				//Monthly


				COSTTYPE.selectByVisibleText("Monthly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Monthly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostmonthly = Double.parseDouble(DeathCost);
				DeathCostmonthly = DeathCostmonthly * 4.34;
				//String DeathCost2 = String.format("%.02f", DeathCostmonthly);

				amountValidation(DeathCostPage, DeathCostmonthly, "Death Cost");


				DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmtMonthly = Double.parseDouble(DeathAmount);
				//String DeathAmtMonthly1 =  String.format("%.02f", DeathAmtMonthly);
				amountValidation(DeathAmountPage, DeathAmtMonthly, "Death Amount");



				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostmonthly = Double.parseDouble(TPDCost);
					TPDCostmonthly = TPDCostmonthly * 4.34;
					//String TPDCost2 = String.format("%.02f", TPDCostmonthly);

					amountValidation(TPDCostPage, TPDCostmonthly, "TPD Cost");


					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmtMonthly = Double.parseDouble(TPDAmount);
					//String TPDAmtMonthly1 =  String.format("%.02f", TPDAmtMonthly);
					amountValidation(DeathAmountPage, TPDAmtMonthly, "TPD Amount");

				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}


				//Yearly
				COSTTYPE.selectByVisibleText("Yearly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Yearly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostYearly = Double.parseDouble(DeathCost);
				DeathCostYearly = DeathCostYearly * 52;
				//String DeathCostYearly1 = String.format("%.02f", DeathCostYearly);

				amountValidation(DeathCostPage, DeathCostYearly, "Death Cost");

				DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmtYearly = Double.parseDouble(DeathAmount);
				//String DeathAmtYearly1 =  String.format("%.02f", DeathAmtYearly);
				amountValidation(DeathAmountPage, DeathAmtYearly, "Death Amount");


				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostYearly = Double.parseDouble(TPDCost);
					TPDCostYearly = TPDCostYearly * 52;
					//String TPDCostYearly1 = String.format("%.02f", TPDCostYearly);

					amountValidation(TPDCostPage, TPDCostYearly, "TPD Cost");


					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmtYearly = Double.parseDouble(TPDAmount);
					//String TPDAmtYearly1 =  String.format("%.02f", TPDAmtYearly);
					amountValidation(DeathAmountPage, TPDAmtYearly, "TPD Amount");

				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}




			} else {
				report.updateTestLog("Data Sheet", "Invalid Amount Type ", Status.FAIL);

			}
			// Select Cost Type


		}
		else if (Type.equalsIgnoreCase("IP")) {

			String Rating = dataTable.getData(DataSheet, "Rating");
			String DeathUnit = dataTable.getData(DataSheet, "DeathUnit");
			String TPDUnit = dataTable.getData(DataSheet, "TPDUnit");
			String IPUnit = dataTable.getData(DataSheet, "IPUnit");
			String IPAmount = dataTable.getData(DataSheet, "IPAmount");

			String IPCost = null;
			String IPCostOwnOccupation = null;
			String Msg = null;

			Select BP = new Select(
					driver.findElement(ChangeCoverObjects.obj_PersonalDetails_BenefitPeriod));
			Select WP = new Select(
					driver.findElement(ChangeCoverObjects.obj_PersonalDetails_WaitingPeriod));
			Select COSTTYPE = new Select(driver.findElement(ChangeCoverObjects.obj_PersonalDetails_PremiumFrequency));
			//To enter Death Unit
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
					.clear();
			sleep(100);
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
					.sendKeys(DeathUnit);
			sleep(100);
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).click();
			sleep(1000);
			report.updateTestLog("Death Cover Unit", DeathUnit + " is entered in Unitized",
					Status.PASS);

			sleep(2000);

			//To enter TPD Unit
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).clear();
			sleep(100);
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
					.sendKeys(TPDUnit);
			sleep(100);
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised))
					.click();
			sleep(2000);
			report.updateTestLog("TPD Cover Unit", TPDUnit + " is entered in Unitized", Status.PASS);



			//To enter IP Unit
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised)).clear();
			sleep(100);
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised))
					.sendKeys(IPUnit);
			sleep(100);
			wait.until(ExpectedConditions
					.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
					.click();
			sleep(1000);
			report.updateTestLog("IP Cover Unit", IPUnit + " is entered in Unitized", Status.PASS);

			sleep(3000);

			for (int BenefitPeriod = 1; BenefitPeriod <= 3; BenefitPeriod++) {



				if (BenefitPeriod == 1) {
					BP.selectByVisibleText("2 Years");
					report.updateTestLog("Benefit Period", "Benefit Period: 2 Years is Selected",
							Status.PASS);
				}
				else if (BenefitPeriod == 2) {
					BP.selectByVisibleText("5 Years");
					report.updateTestLog("Benefit Period", "Benefit Period: 5 Years is Selected",
							Status.PASS);
				}
				else if (BenefitPeriod == 3) {
					BP.selectByVisibleText("Age 65");
					report.updateTestLog("Benefit Period", "Benefit Period: Age 65 is Selected",
							Status.PASS);
				}
				else {
					report.updateTestLog("Benefit Period", "Invalid Benefit Period",
							Status.FAIL);
				}

				sleep(3000);
				for (int WaitingPeriod = 1; WaitingPeriod <= 3; WaitingPeriod++) {

					if (WaitingPeriod == 1) {
						WP.selectByVisibleText("30 Days");
						report.updateTestLog("Waiting Period", "Waiting Period: 30 Days is Selected",
								Status.PASS);
					}
					else if (WaitingPeriod == 2) {
						WP.selectByVisibleText("60 Days");
						report.updateTestLog("Waiting Period", "Waiting Period: 60 Days is Selected",
								Status.PASS);
					}
					else if (WaitingPeriod == 3) {
						WP.selectByVisibleText("90 Days");
						report.updateTestLog("Waiting Period", "Waiting Period: 90 Days is Selected",
								Status.PASS);
					}
					else {
						report.updateTestLog("Waiting Period", "Invalid Waiting Period",
								Status.FAIL);
					}


					sleep(3000);


					if ( (BenefitPeriod == 1) &&  (WaitingPeriod == 1)) {
						Msg = "BenefitPeriod_2_WaitingPeriod_30";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_2_WaitingPeriod_30");
					}
					else if ( (BenefitPeriod == 1) &&  (WaitingPeriod == 2)) {
						Msg = "BenefitPeriod_2_WaitingPeriod_60";
						 IPCost = dataTable.getData(DataSheet, "BenefitPeriod_2_WaitingPeriod_60");

					}
					else if ( (BenefitPeriod == 1) &&  (WaitingPeriod == 3)) {
						Msg = "BenefitPeriod_2_WaitingPeriod_90";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_2_WaitingPeriod_90");

					}
					else if ( (BenefitPeriod == 2) &&  (WaitingPeriod == 1)) {
						Msg = "BenefitPeriod_5_WaitingPeriod_30";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_5_WaitingPeriod_30");
						if (Rating.equalsIgnoreCase("White Collar")) {
							IPCostOwnOccupation = dataTable.getData(DataSheet, "BenefitPeriod_5_WaitingPeriod_30_OwnOccupation");
						}
					}
					else if ( (BenefitPeriod == 2) &&  (WaitingPeriod == 2)) {
						Msg = "BenefitPeriod_5_WaitingPeriod_60";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_5_WaitingPeriod_60");
						if (Rating.equalsIgnoreCase("White Collar")) {
							IPCostOwnOccupation = dataTable.getData(DataSheet, "BenefitPeriod_5_WaitingPeriod_60_OwnOccupation");
						}
					}
					else if ( (BenefitPeriod == 2) &&  (WaitingPeriod == 3)) {
						Msg = "BenefitPeriod_5_WaitingPeriod_90";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_5_WaitingPeriod_90");
						if (Rating.equalsIgnoreCase("White Collar")) {
							IPCostOwnOccupation = dataTable.getData(DataSheet, "BenefitPeriod_5_WaitingPeriod_90_OwnOccupation");
						}
					}
					else if ( (BenefitPeriod == 3) &&  (WaitingPeriod == 1)) {
						Msg = "BenefitPeriod_65_WaitingPeriod_30";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_65_WaitingPeriod_30");
						if (Rating.equalsIgnoreCase("White Collar")) {
							IPCostOwnOccupation = dataTable.getData(DataSheet, "BenefitPeriod_65_WaitingPeriod_30_OwnOccupation");
						}
					}
					else if ( (BenefitPeriod == 3) &&  (WaitingPeriod == 2)) {
						Msg = "BenefitPeriod_65_WaitingPeriod_60";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_65_WaitingPeriod_60");
						if (Rating.equalsIgnoreCase("White Collar")) {
							IPCostOwnOccupation = dataTable.getData(DataSheet, "BenefitPeriod_65_WaitingPeriod_60_OwnOccupation");
						}
					}
					else if ( (BenefitPeriod == 3) &&  (WaitingPeriod == 3)) {
						Msg = "BenefitPeriod_65_WaitingPeriod_90";
						IPCost = dataTable.getData(DataSheet, "BenefitPeriod_65_WaitingPeriod_90");
						if (Rating.equalsIgnoreCase("White Collar")) {
							IPCostOwnOccupation = dataTable.getData(DataSheet, "BenefitPeriod_65_WaitingPeriod_90_OwnOccupation");
						}
					}




					for (int PremiumType = 1; PremiumType <= 3; PremiumType++) {

						String Msg1;
						String Message;
						if (PremiumType == 1) {
							Msg1 = "Weekly";
							Message = Msg+" / "+Msg1;
							COSTTYPE.selectByVisibleText("Weekly");
							sleep(3000);
							report.updateTestLog("Premium Type", "Premium Type: Weekly is Selected",
									Status.PASS);
							WebElement IP = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCost);
							js.executeScript("arguments[0].scrollIntoView(true);", IP);
							String IPCostPage = wait
									.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCost))
									.getText();
							double IPCst = Double.parseDouble(IPCost);
							//String IPCost1 =  String.format("%.02f", IPCst);

							amountValidation(IPCostPage, IPCst, "Death Cost"+Message);

							String IPAmountPage = wait
									.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPAmount))
									.getText();
							double DeathAmt = Double.parseDouble(IPAmount);
							//String DeathAmt1 =  String.format("%.02f", DeathAmt);
							amountValidation(IPAmountPage, DeathAmt, "Death Amount"+Message);


							if (Rating.equalsIgnoreCase("White Collar")) {
								if (!(BenefitPeriod == 1)) {
									Message = Message+"-OwnOccupation";
									wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationYes))
									.click();
									sleep(3000);
									report.updateTestLog("Own Occupation", "Own Occupation is Selected",
											Status.PASS);

									 IP = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCost);
									js.executeScript("arguments[0].scrollIntoView(true);", IP);
									 IPCostPage = wait
											.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCost))
											.getText();
									IPCst = Double.parseDouble(IPCostOwnOccupation);
									//IPCost1 =  String.format("%.02f", IPCst);

									amountValidation(IPCostPage, IPCst, "Death Cost"+Message);

									 IPAmountPage = wait
											.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPAmount))
											.getText();
									 DeathAmt = Double.parseDouble(IPAmount);
									// DeathAmt1 =  String.format("%.02f", DeathAmt);
									amountValidation(IPAmountPage, DeathAmt, "Death Amount"+Message);

									wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationNo))
									.click();

								}
							}

						}
						else if (PremiumType == 2) {
							Msg1 = "Monthly";
							Message = Msg+" / "+Msg1;
							COSTTYPE.selectByVisibleText("Monthly");
							report.updateTestLog("Premium Type", "Premium Type: Monthly is Selected",
									Status.PASS);
							sleep(3000);


							WebElement IP = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCost);
							js.executeScript("arguments[0].scrollIntoView(true);", IP);
							String IPCostPage = wait
									.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCost))
									.getText();
							double IPCst = Double.parseDouble(IPCost);
							IPCst = IPCst * 52;
							IPCst = IPCst / 12;
							//String IPCost1 =  String.format("%.02f", IPCst);

							amountValidation(IPCostPage, IPCst, "Death Cost"+Message);


							String IPAmountPage = wait
									.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPAmount))
									.getText();
							double DeathAmt = Double.parseDouble(IPAmount);
							//String DeathAmt1 =  String.format("%.02f", DeathAmt);
							amountValidation(IPAmountPage, DeathAmt, "Death Amount"+Message);

							if (Rating.equalsIgnoreCase("White Collar")) {
								if (!(BenefitPeriod == 1)) {
									Message = Message+"-OwnOccupation";
									wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationYes))
									.click();
									sleep(3000);
									report.updateTestLog("Own Occupation", "Own Occupation is Selected",
											Status.PASS);

									IP = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCost);
									js.executeScript("arguments[0].scrollIntoView(true);", IP);
									IPCostPage = wait
											.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCost))
											.getText();
									IPCst = Double.parseDouble(IPCostOwnOccupation);
									IPCst = IPCst * 52;
									IPCst = IPCst / 12;
									//IPCost1 =  String.format("%.02f", IPCst);

									amountValidation(IPCostPage, IPCst, "Death Cost"+Message);


									IPAmountPage = wait
											.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPAmount))
											.getText();
									DeathAmt = Double.parseDouble(IPAmount);
									//DeathAmt1 =  String.format("%.02f", DeathAmt);
									amountValidation(IPAmountPage, DeathAmt, "Death Amount"+Message);

									wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationNo))
									.click();

								}
							}

						}
						else if (PremiumType == 3) {
							Msg1 = "Yearly";
							Message = Msg+" / "+Msg1;
							COSTTYPE.selectByVisibleText("Yearly");
							report.updateTestLog("Premium Type", "Premium Type: Yearly is Selected",
									Status.PASS);

							sleep(3000);



							WebElement IP = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCost);
							js.executeScript("arguments[0].scrollIntoView(true);", IP);
							String IPCostPage = wait
									.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCost))
									.getText();
							double IPCst = Double.parseDouble(IPCost);
							IPCst = IPCst * 52;
							//String IPCost1 =  String.format("%.02f", IPCst);

							amountValidation(IPCostPage, IPCst, "Death Cost"+Message);

							String IPAmountPage = wait
									.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPAmount))
									.getText();
							double DeathAmt = Double.parseDouble(IPAmount);
							//String DeathAmt1 =  String.format("%.02f", DeathAmt);
							amountValidation(IPAmountPage, DeathAmt, "Death Amount"+Message);

							if (Rating.equalsIgnoreCase("White Collar")) {
								if (!(BenefitPeriod == 1)) {
									Message = Message+"-OwnOccupation";
									wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationYes))
									.click();
									sleep(3000);
									report.updateTestLog("Own Occupation", "Own Occupation is Selected",
											Status.PASS);

									IP = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCost);
									js.executeScript("arguments[0].scrollIntoView(true);", IP);
									IPCostPage = wait
											.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPCost))
											.getText();
									IPCst = Double.parseDouble(IPCostOwnOccupation);
									IPCst = IPCst * 52;
									//IPCost1 =  String.format("%.02f", IPCst);

									amountValidation(IPCostPage, IPCst, "Death Cost"+Message);

									IPAmountPage = wait
											.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_IPAmount))
											.getText();
									DeathAmt = Double.parseDouble(IPAmount);
									//DeathAmt1 =  String.format("%.02f", DeathAmt);
									amountValidation(IPAmountPage, DeathAmt, "Death Amount"+Message);


									wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationNo))
									.click();
								}
							}

						}
						else {
							report.updateTestLog("Premium Type", "Invalid Premium Type",
									Status.FAIL);
						}


						sleep(500);

					}

				}

			}

		}
		}

	public void amountValidation(String DollarAmount, Double newAmount, String Field) {

		String Amount = String.format("%.02f", newAmount);
		System.out.println(DollarAmount);
		System.out.println(Amount);
		String[] part;
		String amount1;
		String[] value;
		String a1;
		String a2;
		String amount;
		Amount = "$" + Amount;
		if (DollarAmount.contains(",")) {

			// To get firstName
			String[] p = DollarAmount.split(",");
			String p1 = p[0];
			String p2 = p[1];
			DollarAmount = p1 + "" + p2;
		}

		if (DollarAmount.equalsIgnoreCase(Amount)) {
			report.updateTestLog(Field, Field + " :" + Amount + " is present", Status.PASS);
		} else {
			Double decAmount = newAmount-0.01;
			Double incAmount = newAmount+0.01;
			String dAmount = String.format("%.02f", decAmount);
			String iAmount = String.format("%.02f", incAmount);


			dAmount = "$" + dAmount;
			iAmount = "$" + iAmount;

			/*if (DollarAmount.contains(",")) {

				// To get firstName
				String[] p = DollarAmount.split(",");
				String p1 = p[0];
				String p2 = p[1];
				DollarAmount = p1 + "" + p2;
			}*/

			if ( (DollarAmount.equalsIgnoreCase(dAmount))||(DollarAmount.equalsIgnoreCase(iAmount)) ) {
				report.updateTestLog(Field, Field + " :" + Amount + " is present with 0.01 corrected value", Status.PASS);
			} else {

				report.updateTestLog(Field,
						Field + " Expected Value:" + Amount + " / Value Present in page:" + DollarAmount, Status.FAIL);
			}

		}

	}

	public void cover_OccupationRating() {
		String ExistingIP = dataTable.getData(DataSheet, "ExistingIP");
		String OwnOccupation = dataTable.getData(DataSheet, "OwnOccupation");
		String[] part;
		String BP;
		String[] value;
		part = XML.split("<benefitPeriod>");
		BP = part[1];
		value = BP.split("</benefitPeriod>");
		BenefitPeriod = value[0];

		if ((BenefitPeriod.equalsIgnoreCase("5 Years")) || (BenefitPeriod.equalsIgnoreCase("Age 65"))) {
			// Check box Validation for the member is not a casual or contractor
			// employee

			wait.until(ExpectedConditions
						.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_CasualORContractor)).click();
				report.updateTestLog("Casual or contractor employee",
						"The member is not a casual or contractor employee is selected", Status.PASS);
				sleep(3000);

			// OwnOccupation Validation

			if (ExistingIP.equalsIgnoreCase("Own Occupation")) {
				WebElement OwnOccYes = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationYes);
				fieldValidation("WebButton", OwnOccYes, "Yes", "Own Occupation");
			} else {
				WebElement OwnOccYes = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationNo);
				fieldValidation("WebButton", OwnOccYes, "No", "Own Occupation");
			}

			// OwnOccupation selection


				if (OwnOccupation.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationYes)).click();
					sleep(3000);
					report.updateTestLog("Own Occupation", "Selected Yes", Status.PASS);
				} else if (OwnOccupation.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OwnOccupationNo)).click();
					sleep(3000);
					report.updateTestLog("Own Occupation", "Selected No", Status.PASS);

				} else {
					report.updateTestLog("DataTable", "Invalid value for OwnOccupation", Status.FAIL);
				}


		}
if (!(BenefitPeriod.equalsIgnoreCase("2 Years"))) {
	if (  ((ExistingIP.equalsIgnoreCase("Own Occupation")) && (OwnOccupation.equalsIgnoreCase("No"))) ||  ((!(ExistingIP.equalsIgnoreCase("Own Occupation"))) && (OwnOccupation.equalsIgnoreCase("Yes")))  ) {
		SecondPage = "Aura";
	} else {
		SecondPage = "Confirmation";
	}
}
	else {
		SecondPage = "Confirmation";
	}

	}

	public void saveAndExitPopUP() {
		if (driver.getPageSource().contains("Your application reference no")) {

			WebElement WErefNo = driver.findElement(ChangeCoverObjects.obj_SaveReferenceNo);
			String refNo = WErefNo.getText();

			String[] part = refNo.split("Your application reference no. is ");
			String SE = part[1];

			String[] part1 = SE.split("\\n\\n");
			String ref = part1[0];

			report.updateTestLog("Reference No", "Reference No:" + ref, Status.PASS);
			driver.findElement(ChangeCoverObjects.obj_SaveClose).click();
			sleep(2000);
			report.updateTestLog("Finish and close", "Finish and close button is clicked", Status.PASS);
			return;
		} else {
			System.out.println("ref no not present");
			report.updateTestLog("Saved And Exit", "Application Saved pop-up does not appear", Status.FAIL);
		}
	}

	public void decisionPopUP_Negative() {


		if (driver.findElement(ChangeCoverObjects.obj_PersonalDetails_CancelMsg).isDisplayed()) {
			// *******Feedback popup******\\
			wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_DecisionPopUP_Continue)).click();
			report.updateTestLog("Feedback", "Selected Continue on the Feedback popup", Status.PASS);
			sleep(2500);
			report.updateTestLog("Application Declined", "Application is declined", Status.PASS);
			String DeathAction = dataTable.getData(DataSheet, "DeathAction");
			String TPDAction = dataTable.getData(DataSheet, "TPDAction");
			String IPAction = dataTable.getData(DataSheet, "IPAction");
			if (DeathAction.equalsIgnoreCase("Increase your cover") || TPDAction.equalsIgnoreCase("Increase your cover")
					|| IPAction.equalsIgnoreCase("Increase your cover")) {

				HealthandLifeStyle();
			}
			else {
				confirmation();

			}

			//confirmation();
		} else {
			report.updateTestLog("Application Declined", "Cancel Msg is not displayed", Status.FAIL);
		}
	}

	public void premiumValidation() {
		String DeathYN = dataTable.getData(DataSheet, "DeathYN");
		String TPDYN = dataTable.getData(DataSheet, "TPDYN");
		String IPYN = dataTable.getData(DataSheet, "IPYN");
		String ExpectedDeathcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedDeathcover");
		String ExpectedTPDcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedTPDcover");
		String ExpectedIPcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedIPcover");
		ExpectedDeathcover = "$" + ExpectedDeathcover;
		ExpectedTPDcover = "$" + ExpectedTPDcover;
		ExpectedIPcover = "$" + ExpectedIPcover;

		// ExpectedDeathcover = amountFormatter(ExpectedDeathcover);
		System.out.println("Formatted amount :" + ExpectedDeathcover);

		if (DeathYN.equalsIgnoreCase("Yes")) {
			WebElement Death = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_DeathAmount);
			fieldValidation("Amount Validation", Death, ExpectedDeathcover, "Death Cover Premium");
		}

		if (TPDYN.equalsIgnoreCase("Yes")) {
			WebElement tpd = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPDAmount);
			fieldValidation("Amount Validation", tpd, ExpectedTPDcover, "TPD Cover Premium");
		}

		if (IPYN.equalsIgnoreCase("Yes")) {
			WebElement ip = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPAmount);
			fieldValidation("Amount Validation", ip, ExpectedIPcover, "IP Cover Premium");
		}

	}

	public void validateChangeCoverPersonalDetail() {

		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
		String Citizen = dataTable.getData(DataSheet, "Citizen");
		String IndustryType = dataTable.getData(DataSheet, "IndustryType");
		String OccupationType = dataTable.getData(DataSheet, "OccupationType");
		String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
		String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
		String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
		String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");
		String CostType = dataTable.getData(DataSheet, "CostType");
		String DeathYN = dataTable.getData(DataSheet, "DeathYN");
		String AmountType = dataTable.getData(DataSheet, "AmountType");
		String DeathAction = dataTable.getData(DataSheet, "DeathAction");
		String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
		String DeathUnits = dataTable.getData(DataSheet, "DeathUnits");
		String TPDYN = dataTable.getData(DataSheet, "TPDYN");
		String TPDAction = dataTable.getData(DataSheet, "TPDAction");
		String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
		String TPDUnits = dataTable.getData(DataSheet, "TPDUnits");
		String IPYN = dataTable.getData(DataSheet, "IPYN");
		String IPAction = dataTable.getData(DataSheet, "IPAction");
		String Insure85Percent = dataTable.getData(DataSheet, "Insure85Percent");
		String WaitingPeriod = dataTable.getData(DataSheet, "WaitingPeriod");
		String BenefitPeriod = dataTable.getData(DataSheet, "BenefitPeriod");
		String IPUnits = dataTable.getData(DataSheet, "IPUnits");
		String SaveExit_Personal = dataTable.getData(DataSheet, "SaveExit_PersonalDetails");
		sleep(2000);

		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);
		WebElement objDISCLOSURE1 = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_dutyOfDisclosure);
		fieldValidation("WebCheckBox", objDISCLOSURE1, "Checked", "DISCLOSURE1");
		js.executeScript("arguments[0].scrollIntoView(true);", objDISCLOSURE1);
		// *****Agree to Privacy Statement*******\\
		WebElement objDISCLOSURE2 = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_privacyStatement);
		fieldValidation("WebCheckBox", objDISCLOSURE2, "Checked", "DISCLOSURE2");
		js.executeScript("arguments[0].scrollIntoView(true);", objDISCLOSURE2);
		// Email
		WebElement objcontactEmail = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_email);
		fieldValidation("WebEdit", objcontactEmail, EmailId, "Email");

		// Phone Number type
		WebElement objcontactType = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_contactType);
		fieldValidation("WebList", objcontactType, TypeofContact, "ContactType");

		// PhoneNumber
		WebElement objpreferrednumCCId = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_phNO);
		fieldValidation("WebEdit", objpreferrednumCCId, ContactNumber, "Contact No");
		System.out.println("Ph----------------------------------------------------");

		// Morning Afternoon
		if (TimeofContact.equalsIgnoreCase("Morning")) {
			System.out.println("//Morn");
			WebElement objMorning = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Morning);
			fieldValidation("WebButton", objMorning, TimeofContact, "Time of contact");
		} else {
			System.out.println("//AN");
			WebElement objAfternoon = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Afternoon);
			fieldValidation("WebButton", objAfternoon, TimeofContact, "Time of contact");
		}

		// Male or Female

		if (Gender.equalsIgnoreCase("Male")) {
			System.out.println("//Male");
			WebElement ObjGenderMale = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Male);
			fieldValidation("WebButton", ObjGenderMale, Gender, "Genderquestion");
		} else if (Gender.equalsIgnoreCase("Female")) {
			System.out.println("//Female");
			WebElement ObjSmokerquestionNo = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Female);
			fieldValidation("WebButton", ObjSmokerquestionNo, Gender, "Genderquestion");
		} else {
			report.updateTestLog("Data table", "Invalid entry for Gender", Status.FAIL);

		}
		js.executeScript("arguments[0].scrollIntoView(true);", objpreferrednumCCId);
		// Do you work more than 14 hours per week?
		if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
			WebElement objfifteenHrsQuestionYes = driver
					.findElement(ChangeCoverObjects.obj_PersonalDetails_FourteenHrsYes);
			fieldValidation("WebButton", objfifteenHrsQuestionYes, FourteenHoursWork, "FourteenHoursWork");
		} else if (FourteenHoursWork.equalsIgnoreCase("No")) {
			WebElement objfifteenHrsQuestionNo = driver
					.findElement(ChangeCoverObjects.obj_PersonalDetails_FourteenHrsNo);
			fieldValidation("WebButton", objfifteenHrsQuestionNo, FourteenHoursWork, "FourteenHoursWork");
		} else {
			report.updateTestLog("Data table", "Invalid entry for FourteenHoursWork", Status.FAIL);

		}

		// Are you a citizen or permanent resident of Australia? Yes No
		if (Citizen.equalsIgnoreCase("Yes")) {
			WebElement objCitizenYes = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_CitizenYes);
			fieldValidation("WebButton", objCitizenYes, Citizen, "Citizen");
		} else if (Citizen.equalsIgnoreCase("No")) {
			WebElement objCitizenNo = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_CitizenNo);
			fieldValidation("WebButton", objCitizenNo, Citizen, "Citizen");
		} else {
			report.updateTestLog("Data table", "Invalid entry for Citizen", Status.FAIL);

		}
		// TransferIndustry list box
		WebElement objtransferIndustry = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Industry);
		fieldValidation("WebList", objtransferIndustry, IndustryType, "TransferIndustry");

		// OccupationTransfer list box
		WebElement objoccupationTransfer = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Occupation);
		fieldValidation("WebList", objoccupationTransfer, OccupationType, "occupationTransfer");

		js.executeScript("arguments[0].scrollIntoView(true);", objtransferIndustry);
		// office Duties are Undertaken within an Office Environment?
		if (EducationalInstitution.equalsIgnoreCase("Yes")) {
			WebElement eduInsYes = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AddQuest1aYes);
			fieldValidation("WebButton", eduInsYes, EducationalInstitution, "EducationalInstitution");
		} else if (EducationalInstitution.equalsIgnoreCase("No")) {
			WebElement eduInsNo = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AddQuest1aNo);
			fieldValidation("WebButton", eduInsNo, EducationalInstitution, "EducationalInstitution");
		} else {
			report.updateTestLog("Data table", "Invalid entry for EducationalInstitution", Status.FAIL);

		}

		if (EducationalDuties.equalsIgnoreCase("Yes")) {
			WebElement eduDutYes = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AddQuest1bYes);
			fieldValidation("WebButton", eduDutYes, EducationalDuties, "EducationalDuties");
			js.executeScript("arguments[0].scrollIntoView(true);", eduDutYes);
		} else if (EducationalDuties.equalsIgnoreCase("No")) {
			WebElement eduDutNo = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AddQuest1bNo);
			fieldValidation("WebButton", eduDutNo, EducationalDuties, "EducationalDuties");
			js.executeScript("arguments[0].scrollIntoView(true);", eduDutNo);
		} else {
			report.updateTestLog("Data table", "Invalid entry for EducationalDuties", Status.FAIL);

		}

		// TertiaryQualification / ManagementRole

		if (TertiaryQual.equalsIgnoreCase("Yes")) {
			WebElement objofficeEnvYes = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AddQuest2Yes);
			fieldValidation("WebButton", objofficeEnvYes, TertiaryQual, "TertiaryQualification / ManagementRole");
		} else if (TertiaryQual.equalsIgnoreCase("No")) {
			WebElement objofficeEnvNo = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AddQuest2No);
			fieldValidation("WebButton", objofficeEnvNo, TertiaryQual, "TertiaryQualification / ManagementRole");
		} else {
			report.updateTestLog("Data table", "Invalid entry for TertiaryQual", Status.FAIL);

		}

		// Current annual salary?
		WebElement objannualSalCCId = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_AnnualSal);
		fieldValidation("WebEdit", objannualSalCCId, AnnualSalary, "AnnualSalary");
		js.executeScript("arguments[0].scrollIntoView(true);", objannualSalCCId);

		// Cover Type/ Frequency
		WebElement coverType = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_PremiumFrequency);
		fieldValidation("WebList", coverType, CostType, "Premiun frequency");

		// Amount Type
		if (AmountType.equalsIgnoreCase("unit")) {
			WebElement objcoverTypeUnitised = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Unitised);
			fieldValidation("WebButton", objcoverTypeUnitised, AmountType, "AmountType");
		} else if (AmountType.equalsIgnoreCase("Fixed")) {
			WebElement objcoverTypeFixed = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_Fixed);
			fieldValidation("WebButton", objcoverTypeFixed, AmountType, "AmountType");
		} else {
			report.updateTestLog("Data table", "Invalid entry for AmountType", Status.FAIL);

		}
		// Death
		if (AmountType.equalsIgnoreCase("Fixed")) {
			WebElement objdollar = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed);
			fieldValidation("WebEdit", objdollar, DeathAmount, "DeathAmount");
			js.executeScript("arguments[0].scrollIntoView(true);", objdollar);
		} else if (AmountType.equalsIgnoreCase("unit")) {
			WebElement objnodollar = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised);
			fieldValidation("WebEdit", objnodollar, DeathUnits, "DeathUnits");
			js.executeScript("arguments[0].scrollIntoView(true);", objnodollar);
		} else {
			report.updateTestLog("Data table", "Invalid entry for AmountType", Status.FAIL);

		}

		// TPD
		if (AmountType.equalsIgnoreCase("Fixed")) {
			WebElement objdollarTPD = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed);
			fieldValidation("WebEdit", objdollarTPD, TPDAmount, "TPDAmount");
			js.executeScript("arguments[0].scrollIntoView(true);", objdollarTPD);
		} else if (AmountType.equalsIgnoreCase("unit")) {
			WebElement objnodollarTPD = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised);
			fieldValidation("WebEdit", objnodollarTPD, TPDUnits, "TPDUnits");
			js.executeScript("arguments[0].scrollIntoView(true);", objnodollarTPD);
		} else {
			report.updateTestLog("Data table", "Invalid entry for AmountType", Status.FAIL);

		}
		// IP
		/*
		 * if(Insure85Percent.equalsIgnoreCase("Yes")){ WebElement
		 * insure85Percent = driver.findElement(ChangeCoverObjects.
		 * obj_PersonalDetails_Insure85OfMySalary);
		 * fieldValidation("WebCheckBox",insure85Percent,"Checked","Insure 85%"
		 * ); js.executeScript("arguments[0].scrollIntoView(true);",
		 * insure85Percent); } else
		 * if(EducationalDuties.equalsIgnoreCase("No")){ WebElement
		 * insure85Percent = driver.findElement(ChangeCoverObjects.
		 * obj_PersonalDetails_Insure85OfMySalary);
		 * fieldValidation("WebCheckBox",insure85Percent,
		 * "UnChecked","Insure 85%");
		 * js.executeScript("arguments[0].scrollIntoView(true);",
		 * insure85Percent); }
		 */

		WebElement objWaitingPeriod = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_WaitingPeriod);
		fieldValidation("WebList", objWaitingPeriod, WaitingPeriod, "WaitingPeriod");

		WebElement objBenefitPeriod = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_BenefitPeriod);
		fieldValidation("WebList", objBenefitPeriod, BenefitPeriod, "BenefitPeriod");
		/*
		 * if( (BenefitPeriod.equalsIgnoreCase("5 Years")) ||
		 * (BenefitPeriod.equalsIgnoreCase("Age 65")) ){ WebElement
		 * objCasualORContract = driver.findElement(ChangeCoverObjects.
		 * obj_PersonalDetails_CasualORContractor);
		 * fieldValidation("WebCheckBox",objCasualORContract,
		 * "Checked","Casual OR Contract Employee");
		 * js.executeScript("arguments[0].scrollIntoView(true);",
		 * objCasualORContract); }
		 */

		WebElement objIPUnits = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_IPCoverUnitised);
		fieldValidation("WebEdit", objIPUnits, IPUnits, "IPUnits");

	}

	public void validateChangeCoverAURA() {

		String Height = dataTable.getData(DataSheet, "Height");
		String HeightType = dataTable.getData(DataSheet, "HeightType");
		String Weight = dataTable.getData(DataSheet, "Weight");
		String WeightType = dataTable.getData(DataSheet, "WeightType");
		String Smoker = dataTable.getData(DataSheet, "Smoker");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String Pregnant = dataTable.getData(DataSheet, "Pregnant");
		String Exclusion_3Years_Question_for_TPDandIP = dataTable.getData(DataSheet,
				"Exclusion_3Years_Question_for_TPDandIP");
		String Loading_3Years_Questions_for_Death50_TPD50_IP100 = dataTable.getData(DataSheet,
				"Loading_3Years_Questions_for_Death50_TPD50_IP100");
		String Loading_3Years_Questions_for_Death100_TPD100_IP150 = dataTable.getData(DataSheet,
				"Loading_3Years_Questions_for_Death100_TPD100_IP150");
		String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore = dataTable.getData(DataSheet,
				"Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");
		String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP = dataTable.getData(DataSheet,
				"Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
		String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP = dataTable.getData(DataSheet,
				"Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");
		String UsualDoctororMedicalCentre = dataTable.getData(DataSheet, "UsualDoctororMedicalCentre");
		String UsualDoctor_Name = dataTable.getData(DataSheet, "UsualDoctor_Name");
		String UsualDoctor_ContactNumber = dataTable.getData(DataSheet, "UsualDoctor_ContactNumber");
		String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50 = dataTable.getData(DataSheet,
				"Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
		String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP = dataTable.getData(DataSheet,
				"Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
		String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP = dataTable.getData(DataSheet,
				"Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
		String Drugs_Last5Years = dataTable.getData(DataSheet, "Drugs_Last5Years");
		String Alcohol_DrinksPerDay = dataTable.getData(DataSheet, "Alcohol_DrinksPerDay");
		String Alcohol_Professionaladvice = dataTable.getData(DataSheet, "Alcohol_Professionaladvice");
		String HIVInfected = dataTable.getData(DataSheet, "HIVInfected");
		String PriorApplication = dataTable.getData(DataSheet, "PriorApplication");
		String PreviousClaims = dataTable.getData(DataSheet, "PreviousClaims");
		String PreviousClaims_PaidBenefit_terminalillness = dataTable.getData(DataSheet,
				"PreviousClaims_PaidBenefit_terminalillness");
		String CurrentPolicies = dataTable.getData(DataSheet, "CurrentPolicies");
		String DeathAction = dataTable.getData(DataSheet, "DeathAction");
		String TPDAction = dataTable.getData(DataSheet, "TPDAction");
		String IPAction = dataTable.getData(DataSheet, "IPAction");
		String SaveExit_Aura = dataTable.getData(DataSheet, "SaveExit_HealthLifestyle");

		WebDriverWait wait = new WebDriverWait(driver, 20);
		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);

		// What is your height?
		WebElement objHeight = driver.findElement(By.xpath("//*[@ng-model='heightval']"));
		fieldValidation("WebEdit", objHeight, Height, "Height");

		WebElement objHeightType = driver.findElement(By.xpath("//*[@ng-model='heightDropDown']"));
		fieldValidation("WebList", objHeightType, HeightType, "HeightType");

		// What is your weight?
		WebElement objWeight = driver.findElement(By.xpath("//*[@ng-model='weightVal']"));
		fieldValidation("WebEdit", objWeight, Weight, "Weight");

		WebElement objWeightType = driver.findElement(By.xpath("//*[@ng-model='weightDropDown']"));
		fieldValidation("WebList", objWeightType, WeightType, "WeightType");

		// Are you smoker?
		if (Smoker.equalsIgnoreCase("Yes")) {
			WebElement objSmokerYes = driver.findElement(By.xpath(
					"//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objSmokerYes, Smoker, "Smoker");
		} else if (Smoker.equalsIgnoreCase("No")) {
			WebElement objSmokerNo = driver.findElement(By.xpath(
					"//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objSmokerNo, Smoker, "Smoker");
		} else {
			report.updateTestLog("Data Table", " Invalid input for Smoker", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");

		// Headaches or migraines
		if (Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")) {
			WebElement objHeadaches = driver.findElement(By.xpath("//*[text()[contains(.,'Headaches or migraines')]]"));
			fieldValidation("WebCheckBox", objHeadaches, "Checked", "Headaches or migraines");

			// ******Are you currently under investigations or contemplating
			// investigations for your headaches?*****\\
			WebElement objunderI = driver.findElement(By.xpath(
					"//div/label[text()='Are you currently under investigations or contemplating investigations for your headaches?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objunderI, "No", "Under Investigation");

			// ******How would you describe your headaches?******\\
			WebElement objRecurring = driver.findElement(By.xpath("//label[text()='Recurring or severe episodes']"));
			fieldValidation("WebElement", objRecurring, "Recurring or severe episodes", "Recurring or severe episodes");

			// ***Have your headaches been fully investigated with all
			// underlying causes excluded?****\\
			WebElement objHeadachesIn = driver.findElement(By.xpath(
					"//div/label[text()='Have your headaches been fully investigated with all underlying causes excluded?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objHeadachesIn, "Yes", "HeadachesIn");

			// How many headaches do you suffer in a week?
			WebElement objepisodes = driver.findElement(By.xpath("//label[text()='3 episodes or less']"));
			fieldValidation("WebElement", objepisodes, "3 episodes or less", "3 episodes or less");

			// *******Is this easily controlled with over the counter
			// medication?******\\
			WebElement objmedication = driver.findElement(By.xpath(
					"//div/label[text()='Is this easily controlled with over the counter medication?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objmedication, "Yes", "Easily Controlled with Meditation");
		}

		// Lung or breathing conditions
		else if (Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")) {
			WebElement objLung = driver
					.findElement(By.xpath("//*[text()[contains(.,'Lung or breathing conditions')]]/span"));
			fieldValidation("WebCheckBox", objLung, "Checked", "Lung or breathing conditions");
			// Asthma
			WebElement objAsthma = driver.findElement(By.xpath("//*[text()[contains(.,'Asthma')]]"));
			fieldValidation("WebCheckBox", objAsthma, "Checked", "Asthma");
			// ****Is your condition?******\\
			WebElement objModerate = driver.findElement(By.xpath("//*[text()[contains(.,'Moderate')]]"));
			fieldValidation("WebButton", objModerate, "Yes", "Is your condition");
			// ****Is your Asthma worsened by your Occupation****\\

			if (Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")) {
				WebElement objworsened = driver.findElement(By.xpath(
						"//div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[1]/div/div/label"));
				fieldValidation("WebButton", objworsened, "Yes", "Is your Asthma worsened by your Occupation");
			} else if (Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")) {
				WebElement objworsenedNo = driver.findElement(By.xpath(
						"//div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[2]/div/div/label"));
				fieldValidation("WebButton", objworsenedNo, "No", "Is your Asthma worsened by your Occupation");
			}
		}

		else if (Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")
				&& Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")
				&& Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")) {
			WebElement obj3Yrs = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[1]"));
			fieldValidation("WebCheckBox", obj3Yrs, "Checked", "3 YearsQuestion - None of these");
		} else {
			report.updateTestLog("Data Table",
					" Invalid input for Exclusion_3Years_Question_for_TPDandIP / Loading_3Years_Questions_for_Death100_TPD100_IP150",
					Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// 5 years have you suffered from

		if (Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")) {
			WebElement objcholesterol = driver.findElement(By.xpath("//*[text()[contains(.,'High cholesterol')]]"));
			fieldValidation("WebCheckBox", objcholesterol, "Checked", "High cholesterol");

			// How is your high cholesterol being treated?
			WebElement objMedicationPrescribed = driver
					.findElement(By.xpath("//label[text()='Medication prescribed by your doctor']"));
			fieldValidation("WebElement", objMedicationPrescribed, "Medication prescribed by your doctor",
					"How is your high cholesterol being treated?");

			// When did you last have a reading taken?
			WebElement objReading = driver.findElement(By.xpath(
					"//div/label[text()='When did you last have a reading taken?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objReading, "Yes", "when did u taken reading ? 12 months ago");

			// How did your doctor describe your most recent cholesterol
			// reading?
			WebElement objcholesterolReading = driver.findElement(By.xpath(
					"//div/label[text()='How did your doctor describe your most recent cholesterol reading?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objcholesterolReading, "Yes",
					"How did your doctor describe your most recent cholesterol reading?- Elevated");

			// ****Do you recall your last reading?***\\
			WebElement objRecall = driver.findElement(By.xpath(
					"//div/label[text()='Do you recall your last reading?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objRecall, "Yes", "Do you recall your last reading?");
			WebElement objreadingVal = driver.findElement(By.xpath("//label[text()='7.1']"));
			fieldValidation("WebElement", objreadingVal, "7.1", "High cholesterol Value");

			// *****Have you been advised that your triglycerides are
			// elevated?****\\
			WebElement objtriglycerides = driver.findElement(By.xpath(
					"//div/label[text()='Have you been advised that your triglycerides are elevated? ']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objtriglycerides, "No",
					"Have you been advised that your triglycerides are elevated");

		}

		else if (Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")) {
			WebElement obj5Yrs = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[2]"));
			fieldValidation("WebCheckBox", obj5Yrs, "Checked", "5 YearsQuestion - None of these");
		}

		else {
			report.updateTestLog("Data Table",
					" Invalid input for Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore", Status.FAIL);
		}
		sleep(800);
		js.executeScript("window.scrollBy(0,500)", "");

		if (Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")) {
			// Thyroid conditions
			WebElement objThyroid = driver.findElement(By.xpath("//*[text()[contains(.,'Thyroid conditions')]]"));
			fieldValidation("WebCheckBox", objThyroid, "Yes", "Thyroid conditions");
			// *****What was the specific condition*****\\
			WebElement objHyperthyroidism = driver
					.findElement(By.xpath("//*[text()[contains(.,'Hyperthyroidism (Overactive)')]]"));
			fieldValidation("WebCheckBox", objHyperthyroidism, "Yes", "Hyperthyroidism");
			// ****Has your condition been fully investigated?******\\
			WebElement objinvestigated = driver.findElement(By.xpath(
					"//div/label[text()='Has your condition been fully investigated?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objinvestigated, "Yes", "Has your condition been fully investigated");
			// Is your condition fully controlled?
			WebElement objcontrolled = driver.findElement(By.xpath(
					"//div/label[text()='Is your condition fully controlled?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objcontrolled, "No", "Is your condition fully controlled");

		}

		else if (Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")
				&& Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("No")) {
			WebElement objpriTre = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[3]"));
			fieldValidation("WebCheckBox", objpriTre, "Checked", "PriorTreatmentorDiagnosis - None of these");
		}

		else {
			report.updateTestLog("Data Table",
					" Invalid input for Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP / Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP",
					Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// Do you have a usual doctor or medical centre you regularly visit?
		if (UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")) {
			WebElement objdoctor = driver.findElement(By.xpath(
					"(//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div/div/div/label)[1]"));
			fieldValidation("WebButton", objdoctor, UsualDoctororMedicalCentre,
					"Do you have a usual doctor or medical centre you regularly visit");

			// Stevenson
			WebElement objStevenson = driver.findElement(By.xpath("//label[text()='Stevenson']"));
			fieldValidation("WebElement", objStevenson, UsualDoctor_Name, "UsualDoctor_Name");

			// PhoneNumber
			WebElement objreadingVal = driver.findElement(By.xpath("//label[text()='9178563574']"));
			fieldValidation("WebElement", objreadingVal, UsualDoctor_ContactNumber, "UsualDoctor_ContactNumber");

			// Add another Doctor?
			WebElement objAddAnotherDoc = driver.findElement(By.xpath(
					"//div/label[text()='Add another Doctor?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objAddAnotherDoc, "No", "AddAnotherDoc");
		}

		else if (UsualDoctororMedicalCentre.equalsIgnoreCase("No")) {
			WebElement obj5DMCenter = driver.findElement(By.xpath(
					"//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", obj5DMCenter, UsualDoctororMedicalCentre, "Usual Doctoror or Medical Centre");
		}

		else {
			report.updateTestLog("Data Table", " Invalid input for UsualDoctororMedicalCentre", Status.FAIL);
		}
		js.executeScript("window.scrollBy(0,500)", "");
		// Family History
		if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")) {
			WebElement objFamily = driver.findElement(By.xpath(
					"//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objFamily, "Yes", "FamilyHealthHistory");
			// ***Select the health condition****\\
			WebElement objDiabetes = driver.findElement(By.xpath("(//*[text()[contains(.,'Diabetes')]])[2]"));
			fieldValidation("WebCheckBox", objDiabetes, "Checked", "Diabetes");
			// ***How many family members were affected?****\\
			WebElement objAffectedNo = driver.findElement(By.xpath("//label[text()='3']"));
			fieldValidation("WebElement", objAffectedNo, "3", "AffectedNo");
			// ***were two or more family members diagnosed over age 19****\\
			WebElement objfamilymembers = driver.findElement(By.xpath(
					"//div/label[text()='Were two or more family members diagnosed over the age of 19?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objfamilymembers, Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50,
					"were two or more family members diagnosed over age 19");

		}

		else if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("No")) {
			WebElement obj5DMCenter = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[3]"));
			fieldValidation("WebButton", obj5DMCenter, Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50,
					"Family Health History");
		} else if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("UnKnown")) {
			WebElement obj5DMCenter = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[3]"));
			fieldValidation("WebButton", obj5DMCenter, Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50,
					"Family Health History");
		} else {
			report.updateTestLog("Data Table", " Invalid input for FamilyHealthHistory", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// Life Style questions
		// *******Do you have firm plans to travel or reside in another
		// country*******\\
		if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.trim().length() > 0) {
			if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")) {
				WebElement objOtherCountryYes = driver.findElement(By.xpath(
						"//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[1]/div/div/label"));
				fieldValidation("WebButton", objOtherCountryYes, "Yes",
						"travel or reside in another country other than NZ, the United States of America");
			} else if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
				WebElement objOtherCountryNo = driver.findElement(By.xpath(
						"//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[2]/div/div/label"));
				fieldValidation("WebButton", objOtherCountryNo, "No",
						"travel or reside in another country other than NZ, the United States of America");
			}

		}

		if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")) {

			// ******Do you regularly engage in or intend to engage in any of
			// the following hazardous activities*****\\
			WebElement ObjWatersports = driver.findElement(By.xpath("//*[text()[contains(.,'Water sports')]]"));
			fieldValidation("WebButton", ObjWatersports, "Yes", "Water sports");
			// UnderWater
			WebElement objUnderwaterdiving = driver
					.findElement(By.xpath("//*[text()[contains(.,'Underwater diving')]]"));
			fieldValidation("WebButton", objUnderwaterdiving, "Yes", "Underwater diving");
			// 40 meters
			WebElement obj40Meters = driver.findElement(By.xpath("//*[text()[contains(.,'More than 40 metres')]]"));
			fieldValidation("WebButton", obj40Meters, "Yes", "40Meters diving");

		}

		else if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
			WebElement objHazAct = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[4]"));
			fieldValidation("WebCheckBox", objHazAct, "Checked", "HazardousActivities - None of these");
		}

		else {
			report.updateTestLog("Data Table", " Invalid input for hazardous activities", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// *****Have you within the last 5 years used any drugs that were not
		// prescribed to you (other than those available over the counter)
		// or have you exceeded the recommended dosage for any medication?
		System.out.println("111111111111111111111" + Drugs_Last5Years);
		if (Drugs_Last5Years.equalsIgnoreCase("Yes")) {

			WebElement obj5YearsYes = driver.findElement(By.xpath(
					"//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", obj5YearsYes, Drugs_Last5Years,
					" have you exceeded the recommended dosage for any medication");
		} else if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
			WebElement obj5YearsNo = driver.findElement(By.xpath(
					"//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", obj5YearsNo, Drugs_Last5Years,
					" have you exceeded the recommended dosage for any medication");
		}

		else {
			report.updateTestLog("Data Table", " Invalid input for Drugs in Last 5 Years", Status.FAIL);
		}

		// *****How many alcoholic drinks you have in a day?*****\\
		WebElement objalcoholicmany = driver.findElement(By.xpath("//label[text()='1']"));
		fieldValidation("WebElement", objalcoholicmany, "1", "How many alcoholic drinks you have in a day");

		// ****Have you ever been advised by a health professional to reduce
		// your alcohol consumption?****\\
		if (Alcohol_Professionaladvice.equalsIgnoreCase("Yes")) {
			WebElement objprofessionalYes = driver.findElement(By.xpath(
					"//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objprofessionalYes, Alcohol_Professionaladvice,
					"Have you ever been advised by a health professional to reduce your alcohol consumption");
		} else if (Alcohol_Professionaladvice.equalsIgnoreCase("No")) {
			WebElement objprofessionalNo = driver.findElement(By.xpath(
					"//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objprofessionalNo, Alcohol_Professionaladvice,
					"Have you ever been advised by a health professional to reduce your alcohol consumption");
		} else {
			report.updateTestLog("Data Table", " Invalid input for Alcohol_Professionaladvice", Status.FAIL);
		}
		// ******Are you infected with HIV (Human Immunodeficiency Virus******\\
		if (HIVInfected.equalsIgnoreCase("Yes")) {
			WebElement objHIVyes = driver.findElement(By.xpath(
					"//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objHIVyes, HIVInfected,
					"Are you infected with HIV (Human Immunodeficiency Virus");
		} else if (HIVInfected.equalsIgnoreCase("No")) {
			WebElement objHIVNo = driver.findElement(By.xpath(
					"//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objHIVNo, HIVInfected,
					"Are you infected with HIV (Human Immunodeficiency Virus");
			WebElement objreferred = driver.findElement(By.xpath(
					"//div/label[text()='Have you been referred for or waiting on an HIV test result and/or are taking preventative medication?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objreferred, HIVInfected,
					"Have you been referred for or waiting on an HIV test result and/or are taking preventative medication");

		} else {
			report.updateTestLog("Data Table", " Invalid input for HIVInfected", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// General Questions
		WebElement ObjOther = driver.findElement(By.xpath(
				"//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label"));
		fieldValidation("WebButton", ObjOther, "No",
				"Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future");

		// Insurance Details
		// Has an application for Life, Trauma, TPD or Disability Insurance on
		// your life ever been declined
		if (PriorApplication.equalsIgnoreCase("Yes")) {
			WebElement objDisabilityYes = driver
					.findElement(By.xpath("//*[@id='collapseOne']/div/div[1]/div/div[2]/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objDisabilityYes, PriorApplication,
					"Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms");
		} else if (PriorApplication.equalsIgnoreCase("No")) {
			WebElement objDisabilityNo = driver.findElement(By.xpath(
					"//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objDisabilityNo, PriorApplication,
					"Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms");
		} else {
			report.updateTestLog("Data Table", " Invalid input for PriorApplication", Status.FAIL);
		}
		// Are you contemplating or have you ever made a claim for or received
		// sickness
		if (PreviousClaims.equalsIgnoreCase("Yes")) {
			WebElement objcontemplatingYes = driver.findElement(By.xpath(
					"//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objcontemplatingYes, PreviousClaims,
					"Are you contemplating or have you ever made a claim for or received sickness");
		} else if (PreviousClaims.equalsIgnoreCase("No")) {
			WebElement objcontemplatingNo = driver.findElement(By.xpath(
					"//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objcontemplatingNo, PreviousClaims,
					"Are you contemplating or have you ever made a claim for or received sickness");
		} else {
			report.updateTestLog("Data Table", " Invalid input for PreviousClaims", Status.FAIL);
		}

	}

	public void validateChangeCoverConfirm() {
		sleep(2000);

		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);

		detailsValidation();

	}

	public static double round(double value, int numberOfDigitsAfterDecimalPoint) {
		BigDecimal bigDecimal = new BigDecimal(value);
		bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint,
				BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
	}


	public void cover_Validations() {
		String CoverValidation = dataTable.getData(DataSheet, "CoverValidation");
		String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
		String TPDUnits = dataTable.getData(DataSheet, "TPDUnits");
		String Age = dataTable.getData(DataSheet, "Age");
		Integer ageint = Integer.parseInt(Age);
		Integer TPDUnitsInt = null;
		Integer TPDAmountInt = null;

	//	TPDUnitsInt = Integer.parseInt(TPDUnits);


		String Message1 = null;

		try{

		if ( (CoverValidation.equalsIgnoreCase("Tampering-Fixed")) || (CoverValidation.equalsIgnoreCase("Tampering-Unitised")) ) {
			Message1 = dataTable.getData(DataSheet, "ErrorMessage");

			/*if (ageint>64) {

				if (driver.getPageSource().contains(Message2)) {
					report.updateTestLog("IP Cover", "Error Message-"+Message3+" is displayed for age > 65", Status.PASS);
				}
				else {
					report.updateTestLog("IP Cover", "Error Message-"+Message3+" is not displayed age > 65", Status.FAIL);
				}
			}*/

			if (CoverValidation.equalsIgnoreCase("Tampering-Fixed")) {
				TPDAmountInt = Integer.parseInt(TPDAmount);
				WebElement TamperingWE = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPD_TamperingMSG);
					js.executeScript("arguments[0].scrollIntoView(true);", TamperingWE);
					String Msg = TamperingWE.getText();
					System.out.println(Msg);
					System.out.println(Message1);
					if (Msg.equalsIgnoreCase(Message1)) {

						report.updateTestLog("Tampering Message", "<BR><B>Expected Value: </B>"+Msg+" is displayed", Status.PASS);

						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Unitised)).click();
						report.updateTestLog("Amount Type", "Unitized radio button is Selected", Status.PASS);
						sleep(3000);
						if (driver.getPageSource().contains(Message1)) {
							report.updateTestLog("Tampering Message", "Tampering Message is displayed when cover is changed to unitized", Status.FAIL);
						}
						else {
							report.updateTestLog("Tampering Message", "Tampering Message is not displayed when cover is changed to unitized", Status.PASS);

						}
					}

			}
			else if (CoverValidation.equalsIgnoreCase("Tampering-Unitised")) {


				if (driver.getPageSource().contains(Message1)) {
					report.updateTestLog("Tampering Message", "Tampering Message is displayed when cover is changed to unitized", Status.FAIL);
				}
				else {
					report.updateTestLog("Tampering Message", "Tampering Message is not displayed when cover is changed to unitized", Status.PASS);


					wait.until(
							ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Fixed))
							.click();
					sleep(3000);
					report.updateTestLog("Amount Type", "Fixed radio button is Selected", Status.PASS);


					WebElement TamperingWE = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPD_TamperingMSG);
					if (TamperingWE.isDisplayed()) {
						js.executeScript("arguments[0].scrollIntoView(true);", TamperingWE);
						report.updateTestLog("Tampering Message", "Tampering Message is displayed", Status.PASS);
						String Msg = TamperingWE.getText();
						System.out.println(Msg);
						System.out.println(Message1);
						if (Msg.equalsIgnoreCase(Message1)) {

							report.updateTestLog("Tampering Message", "Tampering Message -"+Msg+" is displayed", Status.PASS);
						}
						else {
							report.updateTestLog("Tampering Message", "<BR><B>"+ "Expected Value: </B>"+Message1 + "<BR><B>Actual Value: </B>"+ Msg, Status.FAIL);
						}
					}


				}


					/*

					if (ageint>64) {

						//Fixed validation - Increace TPD Amount
						wait.until(
								ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Fixed))
								.click();
						sleep(3000);
						TPDAmountInt = TPDAmountInt+10;
						String TPDAmountStr = ""+TPDAmountInt;
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed))
								.sendKeys(TPDAmountStr);
						sleep(800);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed))
						.click();
						sleep(1000);

						if (driver.getPageSource().contains(Message2)) {
							report.updateTestLog("TPD Increase - Fixed", "Error Message is displayed when increasing the amout for age > 65", Status.PASS);
						}
						else {
							report.updateTestLog("TPD Increase - Fixed", "Error Message is not displayed when increasing the amout for age > 65", Status.FAIL);
						}


						TPDAmountInt = TPDAmountInt-10;
						TPDAmountStr = ""+TPDAmountInt;
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed))
								.sendKeys(TPDAmountStr);
						sleep(800);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed))
						.click();
						sleep(1000);

						if (driver.getPageSource().contains(Message2)) {
							report.updateTestLog("TPD Increase - Fixed", "Error Message is displayed when decreasing the amout for age > 65", Status.FAIL);
						}
						else {
							report.updateTestLog("TPD Increase - Fixed", "Error Message is not displayed when decreasing the amout for age > 65", Status.PASS);
						}





						//unitized Validation - Increace TPD Amount
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Unitised)).click();
						report.updateTestLog("Amount Type", "Unitized radio button is Selected", Status.PASS);
						sleep(1000);

						TPDUnitsInt = TPDUnitsInt+5;
						String TPDUnitsStr = ""+TPDUnitsInt;
						// To enter Cover Amount
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
								.sendKeys(TPDUnitsStr);
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
						.click();
						sleep(1000);
						report.updateTestLog("TPD Cover Unit", TPDUnits + " is entered in Unitized", Status.PASS);

						if (driver.getPageSource().contains(Message2)) {
							report.updateTestLog("TPD Increase - Unitized", "Error Message is displayed when increasing the amout for age > 65", Status.PASS);
						}
						else {
							report.updateTestLog("TPD Increase - Unitized", "Error Message is not displayed when increasing the amout for age > 65", Status.FAIL);
						}

						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
								.sendKeys("0");
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
						.click();
						sleep(1000);
						report.updateTestLog("TPD Cover Unit", TPDUnits + " is entered in Unitized", Status.PASS);

						if (driver.getPageSource().contains(Message2)) {
							report.updateTestLog("TPD Increase - Unitized", "Error Message is displayed when increasing the amout for age > 65", Status.FAIL);
						}
						else {
							report.updateTestLog("TPD Increase - Unitized", "Error Message is not displayed when increasing the amout for age > 65", Status.PASS);
						}


						*/



					}




			/*	wait.until(
								ExpectedConditions.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Fixed))
								.click();
						sleep(3000);
						TPDAmountInt = TPDAmountInt+10;
						String TPDAmountStr = ""+TPDAmountInt;
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed)).clear();
						sleep(100);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed))
								.sendKeys(TPDAmountStr);
						sleep(800);
						wait.until(ExpectedConditions
								.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed))
						.click();
						sleep(1000);
						report.updateTestLog("TPD Amount", "Death amount is increased by 10 from the original amount", Status.PASS);


						if (ageint>64) {

							//Fixed validation - Increace TPD Amount

							if (driver.getPageSource().contains(Message2)) {
								report.updateTestLog("TPD Increase - Fixed", "Error Message-"+Message2+" is displayed when increasing the amount for age > 65", Status.PASS);
							}
							else {
								report.updateTestLog("TPD Increase - Fixed", "Error Message-"+Message2+" is not displayed when increasing the amount for age > 65", Status.FAIL);
							}
						}else {
							if (driver.getPageSource().contains(Message2)) {
								report.updateTestLog("TPD Increase - Fixed", "Error Message-"+Message2+" is displayed when increasing the amount for age < 65", Status.FAIL);
							}
							else {
								report.updateTestLog("TPD Increase - Fixed", "Error Message-"+Message2+" is not displayed when increasing the amount for age < 65", Status.PASS);
							}
						}

							TPDAmountInt = TPDAmountInt-20;
							TPDAmountStr = ""+TPDAmountInt;
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed)).clear();
							sleep(100);
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverFixed))
									.sendKeys(TPDAmountStr);
							sleep(800);
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverFixed))
							.click();
							sleep(1000);

							report.updateTestLog("TPD Amount", "Death amount is decreases by 10 from the original amount", Status.PASS);


							if (driver.getPageSource().contains(Message2)) {
								report.updateTestLog("TPD Increase - Fixed", "Error Message is displayed when decreasing the amount ", Status.FAIL);
							}
							else {
								report.updateTestLog("TPD Increase - Fixed", "Error Message is not displayed when decreasing the amount", Status.PASS);
							}




							//unitized Validation - Increace TPD Amount
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_Unitised)).click();
							report.updateTestLog("Amount Type", "Unitized radio button is Selected", Status.PASS);
							sleep(1000);

							TPDUnits = driver.findElement(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised).getText();
							System.out.println("TPDUnits"+TPDUnits);
							TPDUnitsInt = Integer.parseInt(TPDUnits);
							TPDUnitsInt = TPDUnitsInt+2;
							String TPDUnitsStr = ""+TPDUnitsInt;
							// To enter Cover Amount
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).clear();
							sleep(100);
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
									.sendKeys(TPDUnitsStr);
							sleep(100);
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
							.click();
							sleep(1000);
							report.updateTestLog("TPD Amount", "Death amount is increased by 2Unit from the original unit of-"+TPDUnitsInt, Status.PASS);

							if (ageint>64) {

								if (driver.getPageSource().contains(Message2)) {
									report.updateTestLog("TPD Increase - Unitized", "Error Message is displayed when increasing the amout for age > 65", Status.PASS);
								}
								else {
									report.updateTestLog("TPD Increase - Unitized", "Error Message is not displayed when increasing the amout for age > 65", Status.FAIL);
								}


							}
							else {


								if (driver.getPageSource().contains(Message2)) {
									report.updateTestLog("TPD Increase - Unitized", "Error Message is displayed when increasing the amout for age < 65", Status.FAIL);
								}
								else {
									report.updateTestLog("TPD Increase - Unitized", "Error Message is not displayed when increasing the amout for age < 65", Status.PASS);
								}



							}





							TPDUnitsInt = TPDUnitsInt-2;
							TPDUnitsStr = ""+TPDUnitsInt;
							// To enter Cover Amount
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised)).clear();
							sleep(100);
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_TPDCoverUnitised))
									.sendKeys(TPDUnitsStr);
							sleep(100);
							wait.until(ExpectedConditions
									.elementToBeClickable(ChangeCoverObjects.obj_PersonalDetails_DeathCoverUnitised))
							.click();
							sleep(1000);
							report.updateTestLog("TPD Amount", "Death amount is increased by 2Unit from the original unit of-"+TPDUnitsInt, Status.PASS);

							if (driver.getPageSource().contains(Message2)) {
								report.updateTestLog("TPD Increase - Unitized", "Error Message is displayed when increasing the amount", Status.FAIL);
							}
							else {
								report.updateTestLog("TPD Increase - Unitized", "Error Message is not displayed when increasing the amount", Status.PASS);
							}

					*/

			}

		else if (CoverValidation.equalsIgnoreCase("IP Disabled")) {

			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Message1 = dataTable.getData(DataSheet, "CoverValidation");
			System.out.println(Message1);
			if (driver.getPageSource().contains(Message1)) {
				report.updateTestLog("IP Disabled", "IP is Disabled", Status.PASS);

			}
			else {
				report.updateTestLog("IP Disabled", "IP is not Disabled", Status.FAIL);
		}

		}




		}	 catch (Exception e) {

			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

}
