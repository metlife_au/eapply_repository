export default class format {
  constructor($filter) {
    this.restrict = 'A';
    this.replace = true;
    this.scope = true;
    this.require = 'ngModel';
    this._$filter = $filter;
  }

  link(scope, elem, attrs, ctrl) {
	  
	  const _this = this;
	  
      if (!ctrl) {return;}
      elem.bind('focus', function (event) {
        var newVal = $(this).val().replace(/,/g, '');
        $(this).val(newVal);
      });
      elem.bind('blur', function(event) {
        var newVal = $(this).val().replace(/,/g, '');
        if(newVal) {
          var newValue = _this._$filter(attrs.format)(newVal);
          if(newValue == ''){
        	  $(this).val('');  
          }else{
        	  $(this).val(newValue);
          }
        } else {
          elem.val();
        }  
      });
      ctrl.$formatters.unshift(function (a) {
    	  if(ctrl.$modelValue){
    		  return _this._$filter(attrs.format)(ctrl.$modelValue);
    	  }else if(ctrl.$modelValue == '0'){
    		  console.log(ctrl.$modelValue);
    		  return '0';
    	  }  
      });
      ctrl.$parsers.unshift(function (viewValue) {
        var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
        if(plainNumber) {
          elem.val(_this._$filter(attrs.format)(plainNumber));
        } else {
          elem.val(plainNumber);
        }  
        return plainNumber;
      });
    
  }
}
