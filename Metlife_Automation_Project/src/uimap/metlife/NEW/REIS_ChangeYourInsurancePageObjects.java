package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class REIS_ChangeYourInsurancePageObjects extends PageObjectLibrary{
	
	public static final By  TransferYourCoverLink = getObjectIdentifaction("REIS.ChangeYourInsurance.Link.id");
	public static final By  FifteenHoursYes = getObjectIdentifaction("REIS.ChangeYourInsurance.FifteenHoursYes.id");
	
	public static final By  FifteenHoursNo = getObjectIdentifaction("REIS.ChangeYourInsurance.FifteenHoursNo.id");
	public static final By  Industry = getObjectIdentifaction("REIS.ChangeYourInsurance.Industry.id");
	public static final By  Occupation = getObjectIdentifaction("REIS.ChangeYourInsurance.Occupation.id");
	public static final By  AnnualSalary = getObjectIdentifaction("REIS.ChangeYourInsurance.AnnualSalary.id");
	public static final By  PreviousInsurer = getObjectIdentifaction("REIS.ChangeYourInsurance.PreviousInsurer.id");
	public static final By  PolicyNumber = getObjectIdentifaction("REIS.ChangeYourInsurance.PolicyNumber.id");
	public static final By  SPINNumber = getObjectIdentifaction("REIS.ChangeYourInsurance.SPINNumber.id");
	public static final By  PremiumFrequency = getObjectIdentifaction("REIS.ChangeYourInsurance.PremiumFrequency.id");
	public static final By  DeathAmount = getObjectIdentifaction("REIS.ChangeYourInsurance.DeathAmount.id");
	public static final By  TPDAmount = getObjectIdentifaction("REIS.ChangeYourInsurance.TPDAmount.id");
	public static final By  IPAmount = getObjectIdentifaction("REIS.ChangeYourInsurance.IPAmount.id");
	public static final By  WaitingPeriod = getObjectIdentifaction("REIS.ChangeYourInsurance.WaitingPeriod.id");
	public static final By  BenefitPeriod = getObjectIdentifaction("REIS.ChangeYourInsurance.BenefitPeriod.id");
	public static final By  CalculateQuote = getObjectIdentifaction("REIS.ChangeYourInsurance.CalculateQuote.xpath");
	public static final By  FetchDeathAmt = getObjectIdentifaction("REIS.ChangeYourInsurance.FetchDeathAmt.id");
	public static final By  FetchTPDAmt = getObjectIdentifaction("REIS.ChangeYourInsurance.FetchTPDAmt.id");
	public static final By  FetchIPAmt = getObjectIdentifaction("REIS.ChangeYourInsurance.FetchIPAmt.id");
	public static final By  ContinueafterCoverdetails = getObjectIdentifaction("REIS.ChangeYourInsurance.ContinueafterCoverdetails.xpath");
	public static final By  RestrictedillnessYes = getObjectIdentifaction("REIS.ChangeYourInsurance.RestrictedillnessYes.id");
	public static final By  RestrictedillnessNo = getObjectIdentifaction("REIS.ChangeYourInsurance.RestrictedillnessNo.id");
	public static final By  PriorClaimYes = getObjectIdentifaction("REIS.ChangeYourInsurance.PriorClaimYes.id");
	public static final By  PriorClaimNo = getObjectIdentifaction("REIS.ChangeYourInsurance.PriorClaimNo.id");
	public static final By  RestrictedFromWorkYes = getObjectIdentifaction("REIS.ChangeYourInsurance.RestrictedFromWorkYes.id");
	public static final By  RestrictedFromWorkNo = getObjectIdentifaction("REIS.ChangeYourInsurance.RestrictedFromWorkNo.id");
	public static final By  DiagnosedillnessYes = getObjectIdentifaction("REIS.ChangeYourInsurance.DiagnosedillnessYes.id");
	public static final By  DiagnosedillnessNo = getObjectIdentifaction("REIS.ChangeYourInsurance.DiagnosedillnessNo.id");
	public static final By  MedicalTreatmentYes = getObjectIdentifaction("REIS.ChangeYourInsurance.MedicalTreatmentYes.id");
	public static final By  MedicalTreatmentNo = getObjectIdentifaction("REIS.ChangeYourInsurance.MedicalTreatmentNo.id");
	public static final By  DeclinedApplicationYes = getObjectIdentifaction("REIS.ChangeYourInsurance.DeclinedApplicationYes.id");
	public static final By  DeclinedApplicationNo = getObjectIdentifaction("REIS.ChangeYourInsurance.DeclinedApplicationNo.id");
	public static final By  PremFundLoadingYes = getObjectIdentifaction("REIS.ChangeYourInsurance.PremFundLoadingYes.id");
	public static final By  PremLoadDetails = getObjectIdentifaction("REIS.ChangeYourInsurance.PremLoadDetails.id");
	public static final By  EnterPremLoading = getObjectIdentifaction("REIS.ChangeYourInsurance.EnterPremLoading.id");
	public static final By  PremFundLoadingNo = getObjectIdentifaction("REIS.ChangeYourInsurance.PremFundLoadingNo.id");
	public static final By  SubmitHealthPage = getObjectIdentifaction("REIS.ChangeYourInsurance.SubmitHealthPage.xpath");

	public static final By  EvidenceofCoverchkbox = getObjectIdentifaction("REIS.ChangeYourInsurance.EvidenceofCoverchkbox.id");	
	public static final By  Declarationchkbox = getObjectIdentifaction("REIS.ChangeYourInsurance.Declarationchkbox.id");
	public static final By  PrivacyStatement = getObjectIdentifaction("REIS.ChangeYourInsurance.PrivacyStatement.id");
	public static final By  GeneralConsent = getObjectIdentifaction("REIS.ChangeYourInsurance.GeneralConsent.id");
	public static final By  PreferredContact = getObjectIdentifaction("REIS.ChangeYourInsurance.PreferredContact.id");
	public static final By  PreferredTime = getObjectIdentifaction("REIS.ChangeYourInsurance.PreferredTime.id");
	public static final By  ContactNumber = getObjectIdentifaction("REIS.ChangeYourInsurance.ContactNumber.id");
	public static final By  EmailAddress = getObjectIdentifaction("REIS.ChangeYourInsurance.EmailAddress.id");
	public static final By  SubmitDeclaration = getObjectIdentifaction("REIS.ChangeYourInsurance.SubmitDeclaration.xpath");

	
}
