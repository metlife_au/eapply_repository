package au.com.metlife.eapply.webservices;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import au.com.finsyn.acuritywebservices.services.InsuranceBenefitDetailsInput;
import au.com.finsyn.acuritywebservices.services.InsuranceBenefitNotesInput;
import au.com.finsyn.acuritywebservices.services.InsuranceMedicalHistoriesInput;
import au.com.finsyn.acuritywebservices.services.InsurancePolicyDetailsInput;
import au.com.finsyn.acuritywebservices.services.ObjectFactory;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetails;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetailsInput;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;

public class EapplicationtRequestMapper {
/**  Added for SFPS Web service call   
 * @throws DatatypeConfigurationException */
    
	
	public static UpdateInsuranceDetails doMapForSFPSFynSynTransfer(
    		Eapplication applicationDTO, EapplyInput eapplyInput
            ) throws DatatypeConfigurationException {
       
    	ObjectFactory factory = new ObjectFactory();
    	UpdateInsuranceDetails updateInsuranceDetails = new UpdateInsuranceDetails();
    	updateInsuranceDetails.setEndUser(eapplyInput.getClientRefNumber());    	
    	UpdateInsuranceDetailsInput input = new UpdateInsuranceDetailsInput(); 
//      Policy
    	InsurancePolicyDetailsInput insurancePolicyDetailsInput = null;		
		
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		XMLGregorianCalendar xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		Cover tempCoversDTO = null;
		
        if (null != applicationDTO && null!= applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0) { 	        	                                   
        	Applicant applicantDTO = null;            
        	for(int itr=0;itr<applicationDTO.getApplicant().size(); itr++){
        		        		
        		applicantDTO = (Applicant)applicationDTO.getApplicant().get(itr);
        		if(null!=applicantDTO && null!=(applicantDTO.getCovers()) && (applicantDTO.getCovers()).size()>0 
        				&& "Primary".equalsIgnoreCase(applicantDTO.getApplicanttype())){       			
                        	
        			insurancePolicyDetailsInput =assignPolicyDetails(applicantDTO.getClientrefnumber(), applicationDTO.getPolicynumber(), xgc,applicantDTO.getWorkhours());        			  			     			
    				input.setInsurancePolicyDetailsInput(factory.createUpdateInsuranceDetailsInputInsurancePolicyDetailsInput(insurancePolicyDetailsInput));    				    				
        			//start of insuranceBenefitDetailsInput			
    				List<Cover> updatedVector = EapplicationtRequestMapper.validCoverVectorWithTransfer(applicantDTO.getCovers());    				  				
        			if(updatedVector.size()>0){        							
        				InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr=null;
        				
            			for(int temp=0;temp<updatedVector.size();temp++){
        					tempCoversDTO = ((Cover)(updatedVector).get(temp));
        					insuranceBenefitDetailsInputArr =  new InsuranceBenefitDetailsInput();
        					assignInsuranceDetails(insuranceBenefitDetailsInputArr, tempCoversDTO, applicantDTO.getMembertype(), null, xgc);						
        					if("Death".equalsIgnoreCase(tempCoversDTO.getCovercode())){     						
        						assignTransferBenefit(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), xgc, tempCoversDTO);		        				
		        										        				        						
        					}else if("TPD".equalsIgnoreCase(tempCoversDTO.getCovercode())){        						
        						assignTransferBenefit(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), xgc, tempCoversDTO);   						
  								
  			        		}else if("IP".equalsIgnoreCase(tempCoversDTO.getCovercode())){
  			        			assignTransferBenefit(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), xgc, tempCoversDTO);      			
        					
  			        		}
        					input.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInputArr);
            			
        			}  		
        		}
        	}
        	} 
        }
        updateInsuranceDetails.setUpdateInsuranceDetailsInput(input);
        return updateInsuranceDetails;       	
    }
	
	public static UpdateInsuranceDetails doMapForSFPSFynSynCancellation(
    		Eapplication applicationDTO, EapplyInput eapplyInput
            ) throws DatatypeConfigurationException {
       
    	ObjectFactory factory = new ObjectFactory();
    	UpdateInsuranceDetails updateInsuranceDetails = new UpdateInsuranceDetails();
    	updateInsuranceDetails.setEndUser(eapplyInput.getClientRefNumber());    	
    	UpdateInsuranceDetailsInput input = new UpdateInsuranceDetailsInput(); 
//      Policy
    	InsurancePolicyDetailsInput insurancePolicyDetailsInput = null;		
		Boolean isIPLongTerm = Boolean.FALSE;
		
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		XMLGregorianCalendar xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		Cover tempCoversDTO = null;
		
        if (null != applicationDTO && null!= applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0) { 	        	                                   
        	Applicant applicantDTO = null;            
        	for(int itr=0;itr<applicationDTO.getApplicant().size(); itr++){
        		        		
        		applicantDTO = (Applicant)applicationDTO.getApplicant().get(itr);
        		if(null!=applicantDTO && null!=(applicantDTO.getCovers()) && (applicantDTO.getCovers()).size()>0 
        				&& "Primary".equalsIgnoreCase(applicantDTO.getApplicanttype())){       			
                        	
        			insurancePolicyDetailsInput =assignPolicyDetails(applicantDTO.getClientrefnumber(), applicationDTO.getPolicynumber(), xgc,applicantDTO.getWorkhours());        			  			     			
    				input.setInsurancePolicyDetailsInput(factory.createUpdateInsuranceDetailsInputInsurancePolicyDetailsInput(insurancePolicyDetailsInput));    				    				
        			//start of insuranceBenefitDetailsInput			
    				List<Cover> updatedVector = EapplicationtRequestMapper.validCoverVectorWithCancellation(applicantDTO.getCovers());
    				if(eapplyInput.getAddnlIpCoverDetails()!=null && "Age 65".equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod())) {
    					isIPLongTerm = Boolean.TRUE;
    				}  				
        			if(updatedVector.size()>0){        							
        				InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr=null;
        				InsuranceBenefitDetailsInput insuranceBenefitDetailsInputLong=null;        				
        				
            			for(int temp=0;temp<updatedVector.size();temp++){
        					tempCoversDTO = ((Cover)(updatedVector).get(temp));
        					insuranceBenefitDetailsInputArr =  new InsuranceBenefitDetailsInput();
        					assignInsuranceDetails(insuranceBenefitDetailsInputArr, tempCoversDTO, applicantDTO.getMembertype(), applicationDTO.getPolicynumber(), xgc);     
							
        					if("Death".equalsIgnoreCase(tempCoversDTO.getCovercode())){
        						assignDeathTpd(insuranceBenefitDetailsInputArr, tempCoversDTO, applicationDTO.getAppdecision(), eapplyInput.getManageType());
		        				if("0".equalsIgnoreCase(tempCoversDTO.getAddunitind())
		        						&& (new BigDecimal(eapplyInput.getDeathAmt()).subtract(new BigDecimal(eapplyInput.getTpdAmt()))).intValue()>0) {
		        					//BigDecimal deathOnlyAmt = new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()).subtract(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));		        					
		        					//change the dollar value to tpd value
		        					insuranceBenefitDetailsInputArr.setPIfDollarDefault(new BigDecimal(0));		        					
		        					input.getInsuranceBenefitDetailsInput().add(assignDeathOnlyFixed(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), applicationDTO.getSaindicator(), tempCoversDTO.getCoverexclusion(), applicationDTO, xgc, new BigDecimal(0), factory, eapplyInput.getManageType()));
		        				
		        				}else if("1".equalsIgnoreCase(tempCoversDTO.getAddunitind())
		        						&& ((new BigDecimal(tempCoversDTO.getAdditionalunit()).subtract(new BigDecimal(tempCoversDTO.getAdditionalunit()))).intValue()>0
		        							|| (new BigDecimal(eapplyInput.getDeathAmt()).subtract(new BigDecimal(eapplyInput.getTpdAmt()))).intValue()>0)
		        						) {
		        					//BigDecimal deathOnlyUnit = new BigDecimal(tempCoversDTO.getAddunitind()).subtract(new BigDecimal(tempCoversDTO.getAddunitind()));		        					
		        					//change the dollar value to tpd value
		        					insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(0));		        							        					
		        					input.getInsuranceBenefitDetailsInput().add(assignDeathOnlyUnits(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), applicationDTO.getSaindicator(), tempCoversDTO.getCoverexclusion(), applicationDTO, xgc, new BigDecimal(0), factory, eapplyInput.getManageType()));
		        				}		        				
		        										        				        						
        					}else if("TPD".equalsIgnoreCase(tempCoversDTO.getCovercode())	){        						
        						assignTPDOnly(insuranceBenefitDetailsInputArr, tempCoversDTO, applicationDTO.getAppdecision(), eapplyInput.getManageType());      						
  								
  			        		}else if("IP".equalsIgnoreCase(tempCoversDTO.getCovercode())){
  			        			assignIPShort(insuranceBenefitDetailsInputArr, tempCoversDTO, applicationDTO.getAppdecision(), eapplyInput.getManageType());
   								if(isIPLongTerm){   
   									insuranceBenefitDetailsInputLong = assignIPLong(insuranceBenefitDetailsInputLong, tempCoversDTO, xgc, insuranceBenefitDetailsInputArr.getPIzStatus(), eapplyInput.getManageType(),applicationDTO.getPolicynumber());   
   									insuranceBenefitDetailsInputLong.setPIcExclusionsApply("N");
   								}
   			        		}
        					insuranceBenefitDetailsInputArr.setPIcExclusionsApply("N");
        					input.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInputArr);
        					if(isIPLongTerm && insuranceBenefitDetailsInputLong!=null) {
        						input.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInputLong);
        					}
        					
        					
      					}
            			
        			}  		
        		}
        	}
        } 
        updateInsuranceDetails.setUpdateInsuranceDetailsInput(input);
        return updateInsuranceDetails;       	
    }
	
	
    public static UpdateInsuranceDetails doMapForSFPSFynSyn(
    		Eapplication applicationDTO, EapplyInput eapplyInput
            ) throws DatatypeConfigurationException {
       
    	ObjectFactory factory = new ObjectFactory();
    	UpdateInsuranceDetails updateInsuranceDetails = new UpdateInsuranceDetails();
    	updateInsuranceDetails.setEndUser(eapplyInput.getClientRefNumber());    	
    	UpdateInsuranceDetailsInput input = new UpdateInsuranceDetailsInput(); 
//      Policy
    	InsurancePolicyDetailsInput insurancePolicyDetailsInput = null;		
		Boolean isIPLongTerm = Boolean.FALSE;
		
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		XMLGregorianCalendar xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		Cover tempCoversDTO = null;
		List<Cover> medicalVector = null;
		List<Cover> updatedVector = null;
		
        if (null != applicationDTO && null!= applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0) { 	        	                                   
        	Applicant applicantDTO = null;            
        	for(int itr=0;itr<applicationDTO.getApplicant().size(); itr++){
        		        		
        		applicantDTO = (Applicant)applicationDTO.getApplicant().get(itr);
        		if(null!=applicantDTO && null!=(applicantDTO.getCovers()) && (applicantDTO.getCovers()).size()>0 
        				&& "Primary".equalsIgnoreCase(applicantDTO.getApplicanttype())){       			
                        	
        			insurancePolicyDetailsInput =assignPolicyDetails(applicantDTO.getClientrefnumber(), applicationDTO.getPolicynumber(), xgc,applicantDTO.getWorkhours());
        			//medical history- populate object only in case of loadings         			
        			if("True".equalsIgnoreCase(applicationDTO.getPaindicator())) {
        				medicalVector = EapplicationtRequestMapper.validCoverVectorWithLoading(applicantDTO.getCovers());
            			if(medicalVector!=null && medicalVector.size()>0){   					
        					insurancePolicyDetailsInput.getMedicalHistoriesInput().add(assignMedicalHistories(medicalVector, applicantDTO.getMembertype(), applicationDTO.getPolicynumber(), xgc));
        				} 
        			}   			     			
    				input.setInsurancePolicyDetailsInput(factory.createUpdateInsuranceDetailsInputInsurancePolicyDetailsInput(insurancePolicyDetailsInput));    				    				
        			//start of insuranceBenefitDetailsInput		
    				if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())) {
    					updatedVector = EapplicationtRequestMapper.validCoversForOccUpg(applicantDTO.getCovers());
    				}else {
    					updatedVector = EapplicationtRequestMapper.validCoverVectorWithUW(applicantDTO.getCovers());
    				}   				
    				
    				if(eapplyInput.getAddnlIpCoverDetails()!=null && "Age 65".equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod())) {
    					isIPLongTerm = Boolean.TRUE;
    				}  				
        			if(updatedVector.size()>0){        							
        				InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr=null;
        				InsuranceBenefitDetailsInput insuranceBenefitDetailsInputLong=null;        				
        				
            			for(int temp=0;temp<updatedVector.size();temp++){
        					tempCoversDTO = ((Cover)(updatedVector).get(temp));
        					insuranceBenefitDetailsInputArr =  new InsuranceBenefitDetailsInput();
        					assignInsuranceDetails(insuranceBenefitDetailsInputArr, tempCoversDTO, applicantDTO.getMembertype(), applicationDTO.getPolicynumber(), xgc);     
							
        					if("Death".equalsIgnoreCase(tempCoversDTO.getCovercode())){
        						assignDeathTpd(insuranceBenefitDetailsInputArr, tempCoversDTO, applicationDTO.getAppdecision(), eapplyInput.getManageType());
		        				/*if("0".equalsIgnoreCase(tempCoversDTO.getAddunitind())
		        						&& (new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()).subtract(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()))).intValue()>0) {
		        					BigDecimal deathOnlyAmt = new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()).subtract(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));		        					
		        					//change the dollar value to tpd value
		        					insuranceBenefitDetailsInputArr.setPIfDollarDefault(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));		        					
		        					input.getInsuranceBenefitDetailsInput().add(assignDeathOnlyFixed(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), applicationDTO.getSaindicator(), tempCoversDTO.getCoverexclusion(), applicationDTO, xgc, deathOnlyAmt, factory, eapplyInput.getManageType()));
		        				
		        				}else*/ if("1".equalsIgnoreCase(tempCoversDTO.getAddunitind())
		        						&& (new BigDecimal(tempCoversDTO.getAdditionalunit()).subtract(new BigDecimal(tempCoversDTO.getAdditionalunit()))).intValue()>0) {
		        					BigDecimal deathOnlyUnit = new BigDecimal(tempCoversDTO.getAdditionalunit()).subtract(new BigDecimal(tempCoversDTO.getAdditionalunit()));		        					
		        					//change the dollar value to tpd value
		        					insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(tempCoversDTO.getAdditionalunit()));		        							        					
		        					input.getInsuranceBenefitDetailsInput().add(assignDeathOnlyUnits(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), applicationDTO.getSaindicator(), tempCoversDTO.getCoverexclusion(), applicationDTO, xgc, deathOnlyUnit, factory, eapplyInput.getManageType()));
		        				}else if((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()).subtract(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()))).intValue()>0){
		        					BigDecimal deathOnlyAmt = new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()).subtract(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));		        					
		        					//change the dollar value to tpd value
		        					insuranceBenefitDetailsInputArr.setPIfDollarDefault(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));		        					
		        					input.getInsuranceBenefitDetailsInput().add(assignDeathOnlyFixed(insuranceBenefitDetailsInputArr, applicationDTO.getAppdecision(), applicationDTO.getSaindicator(), tempCoversDTO.getCoverexclusion(), applicationDTO, xgc, deathOnlyAmt, factory, eapplyInput.getManageType()));
		        				}
		        				
		        										        				        						
        					}else if("TPD".equalsIgnoreCase(tempCoversDTO.getCovercode())	){        						
        						assignTPDOnly(insuranceBenefitDetailsInputArr, tempCoversDTO, applicationDTO.getAppdecision(), eapplyInput.getManageType());      						
  								
  			        		}else if("IP".equalsIgnoreCase(tempCoversDTO.getCovercode())){
  			        			assignIPShort(insuranceBenefitDetailsInputArr, tempCoversDTO, applicationDTO.getAppdecision(), eapplyInput.getManageType());
   								if(isIPLongTerm){   
   									insuranceBenefitDetailsInputLong = assignIPLong(insuranceBenefitDetailsInputLong, tempCoversDTO, xgc, insuranceBenefitDetailsInputArr.getPIzStatus(), eapplyInput.getManageType(),applicationDTO.getPolicynumber());   									
   								}
   			        		}
        					// notes -- Exclusion --
        					if(("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())) && "True".equalsIgnoreCase(applicationDTO.getSaindicator())
        							&& tempCoversDTO.getCoverexclusion()!=null && tempCoversDTO.getCoverexclusion().length()>0){
        						insuranceBenefitDetailsInputArr.setPIcExclusionsApply("Y");
        						insuranceBenefitDetailsInputArr.setInsuranceBenefitNotesInput(factory.createInsuranceBenefitDetailsInputInsuranceBenefitNotesInput(getNotesObj(applicationDTO, tempCoversDTO.getCoverexclusion(), insuranceBenefitDetailsInputArr.getPIzInsuranceCat(), insuranceBenefitDetailsInputArr.getPIzBenefitType())));
        						if("IP".equalsIgnoreCase(tempCoversDTO.getCovercode()) && isIPLongTerm){
        							insuranceBenefitDetailsInputLong.setPIcExclusionsApply("Y");
        							insuranceBenefitDetailsInputLong.setInsuranceBenefitNotesInput(factory.createInsuranceBenefitDetailsInputInsuranceBenefitNotesInput(getNotesObj(applicationDTO, tempCoversDTO.getCoverexclusion(), insuranceBenefitDetailsInputLong.getPIzInsuranceCat(), insuranceBenefitDetailsInputArr.getPIzBenefitType())));
        						}
        					}        					
        					input.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInputArr);
        					if(isIPLongTerm && insuranceBenefitDetailsInputLong!=null) {
        						input.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInputLong);
        					}
        					
        					
      					}
            			
        			}  		
        		}
        	}
        } 
        updateInsuranceDetails.setUpdateInsuranceDetailsInput(input);
        return updateInsuranceDetails;       	
    }
    
    private static InsuranceBenefitNotesInput getNotesObj(Eapplication applicationDTO, String coverexclusion, String insuranceCategory, String benefitType) {
    	InsuranceBenefitNotesInput insuranceBenefitNotesInput = null;
    	if(("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())) && "True".equalsIgnoreCase(applicationDTO.getSaindicator())
				&& coverexclusion!=null && coverexclusion.length()>0){
			insuranceBenefitNotesInput = new InsuranceBenefitNotesInput();			
			insuranceBenefitNotesInput.setNXzDescription(coverexclusion);
				insuranceBenefitNotesInput.setNXzType("J");
				if(applicationDTO.getPolicynumber() != null && applicationDTO.getPolicynumber() != null
						&& benefitType !=null && benefitType.length()>0
						&& insuranceCategory !=null && insuranceCategory.length()>0){      								
					insuranceBenefitNotesInput.setNXzElement(applicationDTO.getPolicynumber().concat(benefitType.concat(insuranceCategory)));
				}	
			
		}	
    	return insuranceBenefitNotesInput;
    }
    
    
    private static List<Cover> validCoverVectorWithLoading(List<Cover> coverVec) {
  	List<Cover> updatedVector =null;
  	List<Cover> finalVector = null;      
      if (null != coverVec) {
          updatedVector = new ArrayList<>();
          for (int itr = 0; itr < coverVec.size(); itr++) {
        	  Cover coversDTO = (Cover) coverVec.get( itr);           
            	 
                  if ( null != coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0  
                		  && coversDTO.getCoverdecision().equalsIgnoreCase("ACC") 
                		  && coversDTO.getLoading()!=null) {
                	  updatedVector.add( coversDTO);
                  }          
              
          }
          Boolean isDeathAndTPDAdditionalZero = isDeathAndTPDAdditionalZero( updatedVector);
          finalVector = new ArrayList<>();
          for (int itr = 0; itr < updatedVector.size(); itr++) {
        	  Cover coversDTO = (Cover) updatedVector.get( itr);
              if (null != coversDTO) {
                  if (coversDTO.getCovercode().equalsIgnoreCase( "IP")
                          && isDeathAndTPDAdditionalZero) {
                      finalVector.add( coversDTO);
                      return finalVector;
                  }
              }
          }
      }
      return updatedVector;
  }
    
    private static Boolean isDeathAndTPDAdditionalZero(List<Cover> updatedVector) {
      Boolean deathAdditional = Boolean.TRUE;
      Boolean deathTPDAdditional = Boolean.TRUE;
      Boolean isDeathAndTPDRequested = Boolean.FALSE;
      for (int itr = 0; itr < updatedVector.size(); itr++) {
    	  Cover coversDTO = (Cover) updatedVector.get( itr);
          if (coversDTO.getCovercode().equalsIgnoreCase( "DEATH")
                  && "No change".equalsIgnoreCase(coversDTO.getCoveroption())) {
              deathAdditional = Boolean.FALSE;
          }
          if (coversDTO.getCovercode().equalsIgnoreCase( "TPD")
        		  && "No change".equalsIgnoreCase(coversDTO.getCoveroption())) {
              deathTPDAdditional = Boolean.FALSE;
          }
          
      }
      if (!deathAdditional && !deathTPDAdditional) {
          isDeathAndTPDRequested = Boolean.TRUE;
      }
       deathAdditional = null;
       deathTPDAdditional =null;
       
      return isDeathAndTPDRequested;
  }
    
    private static List<Cover> validCoverVectorWithTransfer(List<Cover> coverVec){
    	List<Cover> updatedVector = null;
    	  
        if (null != coverVec) {
            updatedVector = new ArrayList<>();
            for (int itr = 0; itr < coverVec.size(); itr++) {
            	Cover coversDTO = (Cover) coverVec.get( itr);       
                    if (coversDTO.getTransfercoveramount().intValue()>0) {                	  
                  	  updatedVector.add( coversDTO);
                    }
            }        
        }
        return updatedVector;
    }
    
    private static List<Cover> validCoverVectorWithCancellation(List<Cover> coverVec) {
  	  List<Cover> updatedVector = null;
  	  
      if (null != coverVec) {
          updatedVector = new ArrayList<>();
          for (int itr = 0; itr < coverVec.size(); itr++) {
          	Cover coversDTO = (Cover) coverVec.get( itr);           
                  if ("Cancel".equalsIgnoreCase(coversDTO.getCoveroption())) {                	  
                	  updatedVector.add( coversDTO);
                  }
          }        
      }
      return updatedVector;
  }
    private static List<Cover> validCoverVectorWithUW(List<Cover> coverVec) {
    	  List<Cover> updatedVector = null;
    	  List<Cover> finalVector = null;    	  
    	  
        if (null != coverVec) {
            updatedVector = new ArrayList<>();
            for (int itr = 0; itr < coverVec.size(); itr++) {
            	Cover coversDTO = (Cover) coverVec.get( itr);           
                    if ( null != coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0 
                    		&& !( "No change".equalsIgnoreCase(coversDTO.getCoveroption()) || MetlifeInstitutionalConstants.CANCEL.equalsIgnoreCase(coversDTO.getCoveroption()))
                    		) {                	  
                  	  updatedVector.add( coversDTO);
                    }
            }
            Boolean isDeathAndTPDAdditionalZero = isDeathAndTPDAdditionalZero( updatedVector);
            finalVector = new ArrayList<>();
            for (int itr = 0; itr < updatedVector.size(); itr++) {
            	Cover coversDTO = (Cover) updatedVector.get( itr);
                if (null != coversDTO) {
                    if (coversDTO.getCovercode().equalsIgnoreCase( "IP")
                            && isDeathAndTPDAdditionalZero) {
                        finalVector.add( coversDTO);
                        return finalVector;
                    }
                }
            }
        }
        return updatedVector;
    }
    
    private static InsurancePolicyDetailsInput assignPolicyDetails(String clientRef, String policyNumber, XMLGregorianCalendar xgc, String fifteenHr) {
    	InsurancePolicyDetailsInput insurancePolicyDetailsInput = new InsurancePolicyDetailsInput();
    	String client;
    	//Insurance Policy
		insurancePolicyDetailsInput.setPLzInsurer("METLFE");
		if(clientRef != null && clientRef.length()>0){							
			client = clientRef.substring(10);
			insurancePolicyDetailsInput.setPLzFund(clientRef.substring(0, 4)); 							
			insurancePolicyDetailsInput.setPLzMember(clientRef.substring(4, 10));								
			insurancePolicyDetailsInput.setPLzClient(client); 								
		}
		//This will be passed to MetLife and needs to be passed back to Acurity. If the date passed to MetLife is 0000-00-00 then MetLife should return the date of cover acceptance (current date).			
		if("0".equalsIgnoreCase(policyNumber)) {
			insurancePolicyDetailsInput.setPLdCommencement(xgc);
			if("Yes".equalsIgnoreCase(fifteenHr)) {
				insurancePolicyDetailsInput.setPLcAtWorkOnJoin("Y");	
			}else {
				insurancePolicyDetailsInput.setPLcAtWorkOnJoin("N");	
			}
		}
		
		if(policyNumber!=null && policyNumber.length()>0){
			insurancePolicyDetailsInput.setPLlPolicyNumber(Long.parseLong(policyNumber));
		}
    	return insurancePolicyDetailsInput;
    }
    
    private static InsuranceMedicalHistoriesInput assignMedicalHistories(List<Cover> medicalVector, String memberType, String policyNumber, XMLGregorianCalendar xgc) {
    	InsuranceMedicalHistoriesInput insuranceMedicalHistoriesInputArr = new InsuranceMedicalHistoriesInput();    	
    	Cover tempCoversDTO;
    	/*
		 * <MHz_Client> 451059</MHz_Client>		
		 */    	
 
    	insuranceMedicalHistoriesInputArr.setMHzRecordType("C");
		insuranceMedicalHistoriesInputArr.setMHdEffective(xgc);
		insuranceMedicalHistoriesInputArr.setMHcLoadWholeAmt("Y");
		if(memberType != null && memberType.length()>0){
			if("EMPLOYER SPONSORED".equalsIgnoreCase(memberType)){
				insuranceMedicalHistoriesInputArr.setMHzInsuranceCat("ES");
			}else if("Personal".equalsIgnoreCase(memberType)){
				insuranceMedicalHistoriesInputArr.setMHzInsuranceCat("PP");
			}
			/*
			 * 1.	Where the level of Death cover exceeds the level of TPD cover, the Death cover should be split into two return records. An ES or PP record for the amount of Death cover equal to the TPD cover and a DO record for the amount of Death cover in excess of TPD cover.
				2.	Where the IP cover has a Benefit Period of �L � To Age 65� two separate IP records should be returned for the same cover amount. One record will be unitised with an Insurance Category of ES or PP. The other record will be fixed cover with an Insurance Category of LT.

			 */
		}  
//		Policy Number
		if(policyNumber!=null && policyNumber.length()>0){
			insuranceMedicalHistoriesInputArr.setMHlPolicyNumber(Long.parseLong(policyNumber));
		}
		insuranceMedicalHistoriesInputArr.setMHzRecordType("C");
		//add logic for converting from list to array    								
		for(int temp=0;temp<medicalVector.size();temp++){
			tempCoversDTO = (Cover)medicalVector.get(temp);
			if(tempCoversDTO!=null){    							
				if("DEATH".equalsIgnoreCase(tempCoversDTO.getCovercode())){    								
					insuranceMedicalHistoriesInputArr.setMHfDTHPcntLoad(tempCoversDTO.getLoading());
					if("DcUnitised".equalsIgnoreCase(tempCoversDTO.getCovercategory())){
						insuranceMedicalHistoriesInputArr.setMHfDTHDollarLoad(new BigDecimal(tempCoversDTO.getAdditionalunit()));
						insuranceMedicalHistoriesInputArr.setMHfDthAutoUnits(new BigDecimal(tempCoversDTO.getAdditionalunit()));
					}else{
						insuranceMedicalHistoriesInputArr.setMHfDthAutoCover(tempCoversDTO.getAdditionalcoveramount());
						insuranceMedicalHistoriesInputArr.setMHfDTHAssessed(tempCoversDTO.getAdditionalcoveramount());
					}
				}else if("TPD".equalsIgnoreCase(tempCoversDTO.getCovercode())){    								
					insuranceMedicalHistoriesInputArr.setMHfTPDPcntLoad(tempCoversDTO.getLoading());
					if("TpdUnitised".equalsIgnoreCase(tempCoversDTO.getCovercategory())){
						insuranceMedicalHistoriesInputArr.setMHfTPDDollarLoad(new BigDecimal(tempCoversDTO.getAdditionalunit()));
						insuranceMedicalHistoriesInputArr.setMHfTPDAutoUnits(new BigDecimal(tempCoversDTO.getAdditionalunit()));
					}else{
						insuranceMedicalHistoriesInputArr.setMHfTPDAutoCover(tempCoversDTO.getAdditionalcoveramount());
						insuranceMedicalHistoriesInputArr.setMHfTPDAssessed(tempCoversDTO.getAdditionalcoveramount());
					}
				}else if("IP".equalsIgnoreCase(tempCoversDTO.getCovercode())){
					//insuranceMedicalHistoriesInputArr.setMHfIPAutoCover(tempCoversDTO.getAdditionalcoveramount());
					insuranceMedicalHistoriesInputArr.setMHfIPAutoUnits(new BigDecimal(tempCoversDTO.getAdditionalunit()));
					insuranceMedicalHistoriesInputArr.setMHfSCAssessed(tempCoversDTO.getAdditionalcoveramount());
					insuranceMedicalHistoriesInputArr.setMHfSCPcntLoad(tempCoversDTO.getLoading());  
				}
			}			
		}
		return insuranceMedicalHistoriesInputArr;
    }
    
    private static InsuranceBenefitDetailsInput assignDeathOnlyUnits(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, String appDecision, String exclusionInd, String exclusionText, Eapplication applicationDTO, XMLGregorianCalendar xgc, BigDecimal deathOnlyUnit, ObjectFactory factory, String manageType) {
    	InsuranceBenefitDetailsInput insuranceBenefitDetailsInputDOUnit =  new InsuranceBenefitDetailsInput();
		insuranceBenefitDetailsInputDOUnit.setPIzInsuranceCat("DO");		
		insuranceBenefitDetailsInputDOUnit.setPIdEffective(xgc); 
		insuranceBenefitDetailsInputDOUnit.setPIzOccupation(insuranceBenefitDetailsInputArr.getPIzOccupation());
		insuranceBenefitDetailsInputDOUnit.setPIlPolicyNumber(insuranceBenefitDetailsInputArr.getPIlPolicyNumber());
		insuranceBenefitDetailsInputDOUnit.setPIzBenefitType(insuranceBenefitDetailsInputArr.getPIzBenefitType());
		insuranceBenefitDetailsInputDOUnit.setPIzBenefitFeature(insuranceBenefitDetailsInputArr.getPIzBenefitFeature());		        					
		
		if("CANCOVER".equalsIgnoreCase(manageType)) {
			insuranceBenefitDetailsInputDOUnit.setPIzStatus("CA");
			insuranceBenefitDetailsInputDOUnit.setPIfDollarDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputDOUnit.setPIfUnitsDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("");
		}else {
			insuranceBenefitDetailsInputDOUnit.setPIfUnitsDefault(deathOnlyUnit);
			insuranceBenefitDetailsInputDOUnit.setPIzStatus(insuranceBenefitDetailsInputArr.getPIzStatus());			
		}		
		insuranceBenefitDetailsInputDOUnit.setPIzFinancialUwrite(insuranceBenefitDetailsInputArr.getPIzFinancialUwrite());
		if(("ACC".equalsIgnoreCase(appDecision)) && "True".equalsIgnoreCase(exclusionInd)
				&& exclusionText!=null && exclusionText.length()>0){
			insuranceBenefitDetailsInputDOUnit.setPIcExclusionsApply("Y");
			insuranceBenefitDetailsInputDOUnit.setInsuranceBenefitNotesInput(factory.createInsuranceBenefitDetailsInputInsuranceBenefitNotesInput(	getNotesObj(applicationDTO, exclusionText, "DO", insuranceBenefitDetailsInputArr.getPIzBenefitType())));
		}else {
			insuranceBenefitDetailsInputDOUnit.setPIcExclusionsApply("N");
		}
		return insuranceBenefitDetailsInputDOUnit;
    }
    
    private static InsuranceBenefitDetailsInput assignDeathOnlyFixed(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, String appDecision, String exclusionInd, String exclusionText, Eapplication applicationDTO, XMLGregorianCalendar xgc, BigDecimal deathOnlyAmt, ObjectFactory factory, String manageType) {
    	InsuranceBenefitDetailsInput insuranceBenefitDetailsInputDOFixed =  new InsuranceBenefitDetailsInput();
		insuranceBenefitDetailsInputDOFixed.setPIzInsuranceCat("DO");		        					
		insuranceBenefitDetailsInputDOFixed.setPIdEffective(xgc); 
		insuranceBenefitDetailsInputDOFixed.setPIzOccupation(insuranceBenefitDetailsInputArr.getPIzOccupation());
		insuranceBenefitDetailsInputDOFixed.setPIlPolicyNumber(insuranceBenefitDetailsInputArr.getPIlPolicyNumber());
		insuranceBenefitDetailsInputDOFixed.setPIzBenefitType(insuranceBenefitDetailsInputArr.getPIzBenefitType());
		insuranceBenefitDetailsInputDOFixed.setPIzBenefitFeature(insuranceBenefitDetailsInputArr.getPIzBenefitFeature());				        					
		insuranceBenefitDetailsInputDOFixed.setPIzFinancialUwrite(insuranceBenefitDetailsInputArr.getPIzFinancialUwrite());
		if("CANCOVER".equalsIgnoreCase(manageType)) {
			insuranceBenefitDetailsInputDOFixed.setPIzStatus("CA");
			insuranceBenefitDetailsInputDOFixed.setPIfDollarDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputDOFixed.setPIfUnitsDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("");
		}else {
			insuranceBenefitDetailsInputDOFixed.setPIfDollarDefault(deathOnlyAmt);
			insuranceBenefitDetailsInputDOFixed.setPIzStatus(insuranceBenefitDetailsInputArr.getPIzStatus());
			
		}
		
		if(("ACC".equalsIgnoreCase(appDecision)) && "True".equalsIgnoreCase(exclusionInd)
				&& exclusionText!=null && exclusionText.length()>0){
			insuranceBenefitDetailsInputDOFixed.setPIcExclusionsApply("Y");
			insuranceBenefitDetailsInputDOFixed.setInsuranceBenefitNotesInput(factory.createInsuranceBenefitDetailsInputInsuranceBenefitNotesInput(	getNotesObj(applicationDTO, exclusionText, "DO", insuranceBenefitDetailsInputArr.getPIzBenefitType())));
		}else {
			insuranceBenefitDetailsInputDOFixed.setPIcExclusionsApply("N");
		}
		return insuranceBenefitDetailsInputDOFixed;
    }
    
    private static void assignDeathTpd(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, Cover tempCoversDTO, String appdecision, String manageType) {
    	insuranceBenefitDetailsInputArr.setPIzBenefitType("D");	
		if("1".equalsIgnoreCase(tempCoversDTO.getAddunitind())){
			insuranceBenefitDetailsInputArr.setPIzBenefitFeature("U");	
			//if(tempCoversDTO.getAdditionalUnit() != null && tempCoversDTO.getAdditionalUnit() != ""){
				insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(tempCoversDTO.getAdditionalunit()));
			//}
		}else {//if("0".equalsIgnoreCase(tempCoversDTO.getAddunitind())) {
			insuranceBenefitDetailsInputArr.setPIzBenefitFeature("F");
			if(tempCoversDTO.getAdditionalcoveramount() != null ){
				insuranceBenefitDetailsInputArr.setPIfDollarDefault(tempCoversDTO.getAdditionalcoveramount());
			}
		}
		assignBenefitStatus(insuranceBenefitDetailsInputArr, appdecision, tempCoversDTO.getCoverdecision());
		if("CANCOVER".equalsIgnoreCase(manageType)) {
			insuranceBenefitDetailsInputArr.setPIzStatus("CA");
			insuranceBenefitDetailsInputArr.setPIfDollarDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("");
		}
		
    }
    
    private static void assignTPDOnly(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, Cover tempCoversDTO, String appdecision, String manageType) {
    	insuranceBenefitDetailsInputArr.setPIzBenefitType("T");		
		if("1".equalsIgnoreCase(tempCoversDTO.getAddunitind())){
			insuranceBenefitDetailsInputArr.setPIzBenefitFeature("U");	
				insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(tempCoversDTO.getAdditionalunit()));
		}else {// if("0".equalsIgnoreCase(tempCoversDTO.getAddunitind())) {
			insuranceBenefitDetailsInputArr.setPIzBenefitFeature("F");
			if(tempCoversDTO.getAdditionalcoveramount() != null ){
				insuranceBenefitDetailsInputArr.setPIfDollarDefault(tempCoversDTO.getAdditionalcoveramount());
			}
		}
		assignBenefitStatus(insuranceBenefitDetailsInputArr, appdecision, tempCoversDTO.getCoverdecision());
		if("CANCOVER".equalsIgnoreCase(manageType)) {
			insuranceBenefitDetailsInputArr.setPIzStatus("CA");			
			insuranceBenefitDetailsInputArr.setPIfDollarDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("");
		}
		
    }
    private static InsuranceBenefitDetailsInput assignIPLong(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputLong, Cover tempCoversDTO, XMLGregorianCalendar xgc, String pIzStatus, String manageType, String policyNumber) {
    	insuranceBenefitDetailsInputLong =  new InsuranceBenefitDetailsInput();   									
			insuranceBenefitDetailsInputLong.setPIdEffective(xgc);    									
			if(tempCoversDTO.getAdditionalcoveramount() != null ){
				insuranceBenefitDetailsInputLong.setPIfDollarDefault(tempCoversDTO.getAdditionalcoveramount());
			}			
			insuranceBenefitDetailsInputLong.setPIzBenefitFeature("F");	   	   								
			insuranceBenefitDetailsInputLong.setPIzBenefitPeriod("L");   	   								
			insuranceBenefitDetailsInputLong.setPIzBenefitType("I");   	   								
			insuranceBenefitDetailsInputLong.setPIzInsuranceCat("LT"); 								
			
			if(pIzStatus!=null && pIzStatus.length()>0){
				insuranceBenefitDetailsInputLong.setPIzStatus(pIzStatus);
			}	   								
			insuranceBenefitDetailsInputLong.setPIzWaitingPeriod("L");
			if("CANCOVER".equalsIgnoreCase(manageType)) {
				insuranceBenefitDetailsInputLong.setPIzStatus("CA");
				insuranceBenefitDetailsInputLong.setPIfDollarDefault(new BigDecimal(0));
				insuranceBenefitDetailsInputLong.setPIfUnitsDefault(new BigDecimal(0));
				insuranceBenefitDetailsInputLong.setPIzFinancialUwrite("");
			}
			//Policy Number
			if(policyNumber != null && policyNumber.length()>0){
				insuranceBenefitDetailsInputLong.setPIlPolicyNumber(Long.parseLong(policyNumber));
			}
			return insuranceBenefitDetailsInputLong;
    }
    private static void assignIPShort(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, Cover tempCoversDTO, String appdecision, String manageType) {
    	insuranceBenefitDetailsInputArr.setPIzBenefitType("I");	   						
		insuranceBenefitDetailsInputArr.setPIzBenefitFeature("U");   								
		insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(tempCoversDTO.getAdditionalunit()));								
		insuranceBenefitDetailsInputArr.setPIzWaitingPeriod("S");								
		insuranceBenefitDetailsInputArr.setPIzBenefitPeriod("S");		
		assignBenefitStatus(insuranceBenefitDetailsInputArr, appdecision, tempCoversDTO.getCoverdecision());
		if("CANCOVER".equalsIgnoreCase(manageType)) {
			insuranceBenefitDetailsInputArr.setPIzStatus("CA");
			insuranceBenefitDetailsInputArr.setPIfDollarDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIfUnitsDefault(new BigDecimal(0));
			insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("");
		}
    }
    
    private static void assignInsuranceDetails(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, Cover tempCoversDTO, String memberType, String policyNumber, XMLGregorianCalendar xgc) {
    	//Common for all Cover        					
		//Insurance Category
		if(memberType != null && memberType.length()>0){
			if("EMPLOYER SPONSORED".equalsIgnoreCase(memberType)){
				insuranceBenefitDetailsInputArr.setPIzInsuranceCat("ES");
			}else if(memberType.startsWith("Personal")){
				insuranceBenefitDetailsInputArr.setPIzInsuranceCat("PP");
			}			
		}    							
		//Effective
		insuranceBenefitDetailsInputArr.setPIdEffective(xgc); 
		//Occupation
		if(tempCoversDTO.getOccupationrating() != null && tempCoversDTO.getOccupationrating() != ""){
				if("Professional".equalsIgnoreCase(tempCoversDTO.getOccupationrating())){
				insuranceBenefitDetailsInputArr.setPIzOccupation("P");
			}else if("White Collar".equalsIgnoreCase(tempCoversDTO.getOccupationrating())){
				insuranceBenefitDetailsInputArr.setPIzOccupation("W");
			}else if("Standard".equalsIgnoreCase(tempCoversDTO.getOccupationrating())){
				insuranceBenefitDetailsInputArr.setPIzOccupation("S");
			}
		}		
		//Policy Number
		if(policyNumber != null && policyNumber.length()>0){
			insuranceBenefitDetailsInputArr.setPIlPolicyNumber(Long.parseLong(policyNumber));
		}
				
    }
    
    private static void assignBenefitStatus(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, String appdecision, String coverdecision) {
    	//Status and Financial Write
		/*Should be based on cover decision
		 * CA � Cancelled
		 */
    	if(appdecision!=null && appdecision.length()>0 && coverdecision!=null && coverdecision.length()>0){
			if("RUW".equalsIgnoreCase(appdecision)){	
				insuranceBenefitDetailsInputArr.setPIzStatus("PE");
				insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("P");   		        										
			}else if("ACC".equalsIgnoreCase(coverdecision)){
				insuranceBenefitDetailsInputArr.setPIzStatus("CU");
			}else if("DCL".equalsIgnoreCase(coverdecision)){
				insuranceBenefitDetailsInputArr.setPIzStatus("DE");
			}
		}
    }
    
    private static void assignTransferBenefit(InsuranceBenefitDetailsInput insuranceBenefitDetailsInputArr, String appdecision, XMLGregorianCalendar xgc, Cover tempCoversDTO) {
    		insuranceBenefitDetailsInputArr.setPIdEffective(xgc);			 
			insuranceBenefitDetailsInputArr.setPIzBenefitFeature("F");	
			insuranceBenefitDetailsInputArr.setPIzStatus("PE"); 
			if("ACC".equalsIgnoreCase(appdecision)) {
				insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("A");
			}else {
				insuranceBenefitDetailsInputArr.setPIzFinancialUwrite("P");
			}	  			      			
			insuranceBenefitDetailsInputArr.setPIfDollarDefault(tempCoversDTO.getAdditionalcoveramount());  			      						
			if("IP".equalsIgnoreCase(tempCoversDTO.getCovercode())) {
				insuranceBenefitDetailsInputArr.setPIzBenefitType("I");
				if("2 years".equalsIgnoreCase(tempCoversDTO.getTotalipbenefitperiod())) {
					insuranceBenefitDetailsInputArr.setPIzBenefitPeriod("S");
				 }else if("Age 65".equalsIgnoreCase(tempCoversDTO.getTotalipbenefitperiod())) {
					insuranceBenefitDetailsInputArr.setPIzBenefitPeriod("L");
				 }
				if("45 Days".equalsIgnoreCase(tempCoversDTO.getTotalipwaitingperiod())) {
					insuranceBenefitDetailsInputArr.setPIzWaitingPeriod("S");  
				}else {
					insuranceBenefitDetailsInputArr.setPIzWaitingPeriod("L");  
				}
			}else if("TPD".equalsIgnoreCase(tempCoversDTO.getCovercode())) {
				insuranceBenefitDetailsInputArr.setPIzBenefitType("T");
			}else if("DEATH".equalsIgnoreCase(tempCoversDTO.getCovercode())) {
				insuranceBenefitDetailsInputArr.setPIzBenefitType("D");
			}			  			      			
			insuranceBenefitDetailsInputArr.setPIcExclusionsApply("N");
			insuranceBenefitDetailsInputArr.setPIcBenefitRollIn("Y");
    }
    
    private static List<Cover> validCoversForOccUpg(List<Cover> coverVec) {
  	  List<Cover> updatedVector = null;
	  
      if (null != coverVec) {
          updatedVector = new ArrayList<>();
          for (int itr = 0; itr < coverVec.size(); itr++) {
          	Cover coversDTO = (Cover) coverVec.get( itr);           
                  if ( null != coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0 
                		  && coversDTO.getAdditionalcoveramount()!=null && coversDTO.getAdditionalcoveramount().intValue()>0                  		
                  		) {                	  
                	  updatedVector.add( coversDTO);
                  }
          }
         
      }
      return updatedVector;
  }
    
   
}
