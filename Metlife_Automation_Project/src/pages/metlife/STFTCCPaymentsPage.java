package pages.metlife;

import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCPaymentPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.STFTCCPaymentsPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class STFTCCPaymentsPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTCCPaymentsPage yourPayment() {
		paymentDetails();
		return new STFTCCPaymentsPage(scriptHelper);
	}

	public STFTCCPaymentsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void paymentDetails() {

		String aacountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsb = dataTable.getData("MetLife_Data", "BSB");
		String StrPaymentType= dataTable.getData("MetLife_Data", "PaymentType");
		String StrCardType= dataTable.getData("MetLife_Data", "CardType");
		String StrCCNumber= dataTable.getData("MetLife_Data", "CardNumber");
		String StrCCHname= dataTable.getData("MetLife_Data", "CardName");
		String StrCCExpMonth= dataTable.getData("MetLife_Data", "ExpiryMonth");
		String StrCCExpYear= dataTable.getData("MetLife_Data", "ExpiryYear");
		String cYSTest = dataTable.getData("MetLife_Data", "CYSTest");
	
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			if (StrPaymentType.equalsIgnoreCase("Credit")){
				
				tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdcredit), "clkradio", "Credit");
				Thread.sleep(500);
				
				switch(StrCardType){
				case "Visa":
					
					tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdvisa), "clkradio", "Visa");
				    Thread.sleep(500);
					 break;	 
				case "Master":
					
				    tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdmaster), "clkradio", "Master");
			        Thread.sleep(500);
					 break;
				case "Amex":
					
			        tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdamex), "clkradio", "Amex");
		            Thread.sleep(500);
					 break;
				case "DC":
					
				    tryAction(fluentWaitElement(STFTPaymentsPageObjects.rddc), "clkradio", "DC");
			        Thread.sleep(500);
				}
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccnumber), "SET", "CCNumber", StrCCNumber);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccnumber), "TAB", "CCNumer");
			sleep(1000);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccname), "SET", "CCName", StrCCHname);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccname), "TAB", "CCNumer");
			sleep(1000);
			tryAction(fluentWaitElement(STFTPaymentsPageObjects.selCCExpMonth),"specialDropdown", "Expiry Month", StrCCExpMonth);
			sleep(2000);
			tryAction(fluentWaitElement(STFTPaymentsPageObjects.selCcExpYr),"specialDropdown", "Expiry Year", StrCCExpYear);
				
			}
			else{
			
			tryAction(fluentWaitElement(STFTCCPaymentsPageObjects.rddebit), "clkradio", "Debit");
			Thread.sleep(500);
			
			
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.txtaccountname), "SET", "AccountName", aacountName);
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.txtbsb), "SET", "BSB", bsb);
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.txtbsb), "TAB", "BSB");
			
			}
			Thread.sleep(2000);	
			tryAction(waitForClickableElement(STFTCCPaymentsPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTCCPaymentsPageObjects.btnsubmit)).click().perform();
			Thread.sleep(1000);	
			driver.manage().window().maximize();
			List<WebElement> txtAreaOutput = null;
			txtAreaOutput = fluentWaitListElements(ColesCCPaymentPageObjects.txtAreaOutput);
			String StrOutput=fluentWaitElement(txtAreaOutput.get(1)).getText();
				//XML Parsing and reading
					DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource src = new InputSource();
					src.setCharacterStream(new StringReader(StrOutput));
					Document doc = builder.parse(src);
					String appNumber= doc.getElementsByTagName("insuredID").item(0).getTextContent();
					System.out.println(appNumber);
					dataTable.putData("MetLife_Data","AppNumber", appNumber);
					driver.manage().window().maximize();
					if (cYSTest.equalsIgnoreCase("Yes")) {	
						driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
						driver.manage().window().maximize();
					}		
			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			Thread.sleep(200);
				
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
