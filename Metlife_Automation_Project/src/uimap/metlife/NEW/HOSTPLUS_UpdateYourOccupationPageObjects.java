package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class HOSTPLUS_UpdateYourOccupationPageObjects extends PageObjectLibrary{
		
	public static final By UpdateOccupationLink = getObjectIdentifaction("HOSTPLUS.UpdateOccupation.LInk.xpath");
	public static final By	Email	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.Email.id");	
	public static final By	ContactTypeClick	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ContactTypeClick.xpath");
	public static final By	ContactNumber	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ContactNumber.id");
	public static final By	MorningContact	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.MorningContact.id");
	public static final By	AfterNoonContact	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.AfterNoonContact.id");
	public static final By	ContactDetailsContinue	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ContactDetailsContinue.xpath");
	public static final By	FifteenHoursYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.FifteenHoursYes.id");
	public static final By	FifteenHoursNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.FifteenHoursNo.id");
	public static final By	CitizenYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.CitizenYes.id");
	public static final By	CitizenNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.CitizenNo.id");
	public static final By	ClickIndustry	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ClickIndustry.xpath");
	public static final By	ClickOccupation	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ClickOccupation.xpath");
	public static final By	OtherOccupation	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.OtherOccupation.id");
	public static final By	OfficeEnvironmentYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.OfficeEnvironmentYes.id");
	public static final By	OfficeEnvironmentNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.OfficeEnvironmentNo.id");
	public static final By	TertiaryQualYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.TertiaryQualYes.id");
	public static final By	TertiaryQualNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.TertiaryQualNo.id");
	public static final By	HazardousEncYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.HazardousEncYes.id");
	public static final By	HazardousEncNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.HazardousEncNo.id");
	public static final By	OutsideOfcPercentYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.OutsideOfcPercentYes.id");
	public static final By	OutsideOfcPercentNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.OutsideOfcPercentNo.id");
	public static final By	AnnualSalary	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.AnnualSalary.id");
	public static final By	LoadSalaryLabel	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.LoadSalaryLabel.xpath");
	public static final By	ContinueOccupationDetails	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ContinueOccupationDetails.xpath");
	public static final By	RestrictedillnessYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.RestrictedillnessYes.xpath");
	public static final By	RestrictedillnessNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.RestrictedillnessNo.xpath");
	public static final By	PriorClaimYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.PriorClaimYes.xpath");
	public static final By	PriorClaimNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.PriorClaimNo.xpath");
	public static final By	ReducedLifeExpectancyYes	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ReducedLifeExpectancyYes.xpath");
	public static final By	ReducedLifeExpectancyNo	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ReducedLifeExpectancyNo.xpath");
	public static final By	ContinueAftHealth	=	getObjectIdentifaction("HOSTPLUS.UpdateOccupation.ContinueAftHealth.xpath");
	
	
	

	

	
}
