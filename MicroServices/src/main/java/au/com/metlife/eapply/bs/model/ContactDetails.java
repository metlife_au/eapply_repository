package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class ContactDetails implements Serializable {

	/**
	 * Author 175373	
	 */
	private static final long serialVersionUID = 1L;
	
	private String preferedContactTime;
	private String mobilePhone;
	private String workPhone;
	private String homePhone;
	private String preferedContactNumber;
	private String preferedContacType;
	private String country;
	private String addressLine1;
	private String addressLine2;
	private String postCode;
	private String state;
	private String suburb;
	private String otherContactType;
	private String otherContactNumber;
	
	public String getPreferedContactTime() {
		return preferedContactTime;
	}
	public void setPreferedContactTime(String preferedContactTime) {
		this.preferedContactTime = preferedContactTime;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getPreferedContactNumber() {
		return preferedContactNumber;
	}
	public void setPreferedContactNumber(String preferedContactNumber) {
		this.preferedContactNumber = preferedContactNumber;
	}
	public String getPreferedContacType() {
		return preferedContacType;
	}
	public void setPreferedContacType(String preferedContacType) {
		this.preferedContacType = preferedContacType;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSuburb() {
		return suburb;
	}
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	public String getOtherContactType() {
		return otherContactType;
	}
	public void setOtherContactType(String otherContactType) {
		this.otherContactType = otherContactType;
	}
	public String getOtherContactNumber() {
		return otherContactNumber;
	}
	public void setOtherContactNumber(String otherContactNumber) {
		this.otherContactNumber = otherContactNumber;
	}

	
	

}
