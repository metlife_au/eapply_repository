package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.Mortgage1300PaymentsObjects;
import uimap.metlife.Mortgage1300QuoteObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class Mortgage1300PaymentPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public Mortgage1300PaymentPage paymentDetails() {
		paymentCalculation();
		return new Mortgage1300PaymentPage(scriptHelper);
	}

	public Mortgage1300PaymentPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void paymentCalculation() {

		
		String poicyType =dataTable.getData("MetLife_Data", "PolicyType");
		String title = dataTable.getData("MetLife_Data", "Title");
		String title1 = dataTable.getData("MetLife_Data", "Title1");
		
		String lastname = dataTable.getData("MetLife_Data", "LastName");
		String lastname1 = dataTable.getData("MetLife_Data", "LastName1");
		
		String contact1 = dataTable.getData("MetLife_Data", "Contact");
		String contact11 = dataTable.getData("MetLife_Data", "Contact1");
		
		String contact2 = dataTable.getData("MetLife_Data", "ContactOther");
		String contact22 = dataTable.getData("MetLife_Data", "ContactOther1");
		
		String email = dataTable.getData("MetLife_Data", "Email");
		String email1 = dataTable.getData("MetLife_Data", "Email1");
		
		String address1 = dataTable.getData("MetLife_Data", "Address1");	
		String address2 = dataTable.getData("MetLife_Data", "Address2");	

		
		String suburb = dataTable.getData("MetLife_Data", "Suburb");
		String state = dataTable.getData("MetLife_Data", "State");
		String postcode = dataTable.getData("MetLife_Data", "PostCode");
		String acctname = dataTable.getData("MetLife_Data", "AccountName");
		String acctnumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsbcode = dataTable.getData("MetLife_Data", "BSBCode");
		String bsbnumber = dataTable.getData("MetLife_Data", "BSBNumber");
	
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			Thread.sleep(1000);
			
			if (poicyType.equalsIgnoreCase("Individual")) {
			
			tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.drpTitle), "DropDownSelect", "Title",title);
			Thread.sleep(1000);
			
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtLastname), "SET", "Last Name", lastname);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtLastname), "TAB", "Last Name");
			
			Thread.sleep(500);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContact), "SET", "Contact", contact1);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContact), "TAB", "Contact");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactOther), "SET", "Contact Other", contact2);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactOther), "TAB", "Contact Other");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailid), "SET", "Email Address", email);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailid), "TAB", "Email Address");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidconfirm), "SET", "Confirm Email Address", email);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidconfirm), "TAB", "Confirm Email Address");
			
			Thread.sleep(1000);
			
			} else if (poicyType.equalsIgnoreCase("Joint")) {
				
				tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.drpTitle), "DropDownSelect", "Title",title);
				Thread.sleep(1000);
				
				tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.drpTitler), "DropDownSelect", "Title",title1);
				Thread.sleep(500);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtLastname), "SET", "Last Name Applicant1", lastname);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtLastname), "TAB", "Last Name Applicant1");
				
				Thread.sleep(500);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtLastnamer), "SET", "Last Name Applicant2", lastname1);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtLastnamer), "TAB", "Last Name Applicant2");
				
				Thread.sleep(500);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContact), "SET", "Contact Applicant1", contact1);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContact), "TAB", "Contact Applicant1");
				
				Thread.sleep(500);
				
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactr), "SET", "Contact Applicant2", contact11);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactr), "TAB", "Contact Applicant2");
				
				Thread.sleep(500);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactOther), "SET", "Contact Other Applicant1", contact2);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactOther), "TAB", "Contact Other Applicant1");
				
				Thread.sleep(1000);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactOtherr), "SET", "Contact Other Applicant2", contact22);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtContactOtherr), "TAB", "Contact Other Applicant2");
				
				Thread.sleep(1000);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailid), "SET", "Email Address Applicant1", email);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailid), "TAB", "Email Address Applicant1");
				
				Thread.sleep(1000);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidr), "SET", "Email Address Applicant2", email1);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidr), "TAB", "Email Address Applicant2");
				
				Thread.sleep(1000);
				 
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidconfirm), "SET", "Confirm Email Address Applicant1", email);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidconfirm), "TAB", "Confirm Email Address Applicant1");
				
				Thread.sleep(1000);
				
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidconfirmr), "SET", "Confirm Email Address Applicant2", email1);
				tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtemailidconfirmr), "TAB", "Confirm Email Address Applicant2");
				
				Thread.sleep(1000);				
				
			}
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtaddress1), "SET", "Address 1", address1);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtaddress1), "TAB", "Address 1");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtaddress2), "SET", "Address 2", address2);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtaddress2), "TAB", "Address 2");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtsuburb), "SET", "Suburb", suburb);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtsuburb), "TAB", "Suburb");
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.drpstate), "DropDownSelect", "State", state);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtpostcode), "SET", "Postcode", postcode);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtpostcode), "TAB", "Postcode");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.rdbank), "clkradio", "Bank / Building Society / Credit Union");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtAccountName), "SET", "Account Name", acctname);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtAccountName), "TAB", "Account Name");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtAccountNumber), "SET", "Account Number", acctnumber);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtAccountNumber), "TAB", "Account Number");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtbsbCode), "SET", "BSB code", bsbcode);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtbsbCode), "TAB", "BSB code");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtbsbNumber), "SET", "BSB Number", bsbnumber);
			tryAction(waitForClickableElement(Mortgage1300PaymentsObjects.txtbsbNumber), "TAB", "BSB Number");
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.chkagree), "Click", "Agree");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.chkacknowledge), "Click", "Acknowledge");
			Thread.sleep(1000);
			
			
			sleep();
	//		Actions actions = new Actions(driver);
	//		actions.moveToElement(driver.findElement(Mortgage1300PaymentsObjects.btnSubmit)).click().perform();
			tryAction(fluentWaitElement(Mortgage1300PaymentsObjects.btnSubmit), "Click", "Submit");
			Thread.sleep(10000);

			switchToActiveWindow();
			driver.manage().window().maximize();
			Thread.sleep(1000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
