package supportlibraries;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.CraftDataTable;
import com.cognizant.framework.FrameworkParameters;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.SeleniumReport;

/**
 * Abstract base class for reusable libraries created by the user
 *
 * @author Cognizant
 */
public abstract class ReusableLibrary {

	private static final int WAIT_PAUSE_MS = 200;
	private static final int ONE_SECOND_MS = 1000;
	private static final int DEFAULT_WAIT_SECONDS = 30;
	private static final String fNEW_LINE = System.getProperty("line.separator");
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * The {@link CraftDataTable} object (passed from the test script)
	 */
	protected CraftDataTable dataTable;
	/**
	 * The {@link SeleniumReport} object (passed from the test script)
	 */
	protected SeleniumReport report;
	/**
	 * The {@link WebDriver} object
	 */
	protected WebDriver driver;
	/**
	 * The {@link ScriptHelper} object (required for calling one reusable
	 * library from another)
	 */
	protected ScriptHelper scriptHelper;

	/**
	 * The {@link Properties} object with settings loaded from the framework
	 * properties file
	 */
	protected Properties properties;
	/**
	 * The {@link FrameworkParameters} object
	 */
	protected FrameworkParameters frameworkParameters;

	/**
	 * Constructor to initialize the {@link ScriptHelper} object and in turn the
	 * objects wrapped by it
	 *
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object
	 */
	public ReusableLibrary(ScriptHelper scriptHelper) {
		this.scriptHelper = scriptHelper;

		this.dataTable = scriptHelper.getDataTable();
		this.report = scriptHelper.getReport();
		this.driver = scriptHelper.getDriver();

		properties = Settings.getInstance();
		frameworkParameters = FrameworkParameters.getInstance();
	}

	protected void storeSessionThreadData(String key, Object value) {
		scriptHelper.storeSessionThreadData(key, value);
	}

	protected String getStringValueFromSessionThreadData(String key) {
		return scriptHelper.getStringValueFromSessionThreadData(key);
	}

	protected Object getValueFromSessionThreadData(String key) {
		return scriptHelper.getValueFromSessionThreadData(key);
	}

	protected void switchToActiveWindow() {

		for (String winHandle : driver.getWindowHandles()) {
			//System.out.println(driver.getTitle());
			//if (driver.getTitle().contains("OnlineApplication")) {
				driver.switchTo().window(winHandle);
				//driver.switchTo().window(winHandle);
				sleep();
				driver.manage().window().maximize();
			//}
		}
	}

	protected void switchToActiveWindowAndClose() {

		for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				driver.switchTo().window(winHandle);
				sleep();
				driver.manage().window().maximize();
				driver.close();
		}
	}

	protected void quickSwitchWindows() {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			driver.manage().window().maximize();
		}
	}

	protected void switchToMainFrame() {
		driver.switchTo().defaultContent();
	}

	protected void switchToFrame(WebElement elmFrame) {
		driver.switchTo().frame(elmFrame);
	}

	protected WebElement fluentWaitElement(final By byType) {
		long timeout = Long.parseLong(properties.getProperty(
				"element.timeout.in.secs").trim());
		return fluentWaitElement(byType, timeout);
	}

	protected WebElement fluentWaitElement(final By byType, long timeout) {
		WebElement webElement = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElement = wait.until(ExpectedConditions
				.presenceOfElementLocated(byType));
		return webElement;
	}

	protected WebElement fluentWaitElement(final WebElement element) {
		long timeout = Long.parseLong(properties.getProperty(
				"element.timeout.in.secs").trim());
		return fluentWaitElement(element, timeout);
	}

	protected WebElement fluentWaitElement(final WebElement element,
			long timeout) {
		WebElement webElement = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElement = wait.until(ExpectedConditions.visibilityOf(element));
		fluentWaitClikableElement(webElement, timeout);
		return webElement;
	}

	protected WebElement fluentWaitClikableElement(final WebElement element,
			long timeout) {
		WebElement webElement = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElement = wait.until(ExpectedConditions
				.elementToBeClickable(element));

		return webElement;
	}

	protected List<WebElement> fluentWaitListElements(final By byType) {
		long timeout = Long.parseLong(properties.getProperty(
				"element.timeout.in.secs").trim());
		return fluentWaitListElements(byType, timeout);
		}

	protected WebElement getWebElementBasedOnText(final By byType, String strText){

		List<WebElement> elements = null;
		elements = fluentWaitListElements(byType);
		WebElement elmButton = null;

		for (WebElement weachDraw : fluentWaitListElements(byType)) {
			if (weachDraw.getText().equalsIgnoreCase(strText)) {
				elmButton=weachDraw;
				break;
			}
		}
		return elmButton;
	}


 	protected List<WebElement> fluentWaitListElements(final By byType,
			long timeout) {
		List<WebElement> webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byType));
		return webElements;
	}

	@SuppressWarnings("deprecation")
	protected String RandomDateOfBirth() {
		GregorianCalendar gc = new GregorianCalendar();
		String strDate = null;
		int year = randBetween(1960, 1990);
		gc.set(gc.YEAR, year);
		int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
		gc.set(gc.DAY_OF_YEAR, dayOfYear);
		strDate = gc.get(gc.DAY_OF_MONTH) + "/" + (gc.get(gc.MONTH) + 1) + "/"
				+ gc.get(gc.YEAR);
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String today = formatter.format(new Date(strDate));
		return today;
	}

	public static int randBetween(int start, int end) {
		return start + (int) Math.round(Math.random() * (end - start));
	}

	public boolean deleteMessages(String subjectToDelete) {
		boolean messageDeleted = false;
		properties.put("mail.imap.host", properties.getProperty("gmail.host"));
		properties.put("mail.imap.port", properties.getProperty("gmail.port"));
		properties.put("gmail.userName",
				properties.getProperty("gmail.user.name"));
		properties.put("gmail.pwd", properties.getProperty("gmail.password"));

		properties.setProperty("mail.imap.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.setProperty("mail.imap.socketFactory.fallback", "false");
		properties.setProperty("mail.imap.socketFactory.port",
				String.valueOf(properties.getProperty("gmail.port")));

		Session session = Session.getDefaultInstance(properties);

		try {
			// connects to the message store
			Store store = session.getStore("imap");
			store.connect(properties.getProperty("gmail.userName"),
					properties.getProperty("gmail.pwd"));

			// opens the inbox folder
			Folder folderInbox = store.getFolder("INBOX");
			folderInbox.open(Folder.READ_WRITE);

			// fetches new messages from server
			Message[] arrayMessages = folderInbox.getMessages();

			for (int i = 0; i < arrayMessages.length; i++) {
				Message message = arrayMessages[i];
				String subject = message.getSubject();
				// Check for the application Number, Read it from Subject and
				// delete it.
				if (subject.contains(subjectToDelete)) {
					message.setFlag(Flags.Flag.DELETED, true);
					messageDeleted = true;
					// System.out.println("Marked DELETE for message: " +
					// subject);
				}
			}

			boolean expunge = true;
			folderInbox.close(expunge);
			store.close();
		} catch (NoSuchProviderException ex) {
			// System.out.println("No provider.");
			ex.printStackTrace();
		} catch (MessagingException ex) {
			// System.out.println("Could not connect to the message store.");
			ex.printStackTrace();
		}
		return messageDeleted;
	}

	public String parsePDFDoc(String strFileName) {
		String strApplcationNumber = null;
		try {
			File file = null;
			PDDocument document = null;
			String target_file_path = null;
			String pathToScan = System.getProperty("user.home") + "/Downloads/";
			String target_file = null; // fileThatYouWantToFilter

			File folderToScan = new File(pathToScan);

			File[] listOfFiles = folderToScan.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					target_file = listOfFiles[i].getName();
					if (target_file.contains("SITNEWELLIAN")
							&& target_file.endsWith(".pdf")) {
						target_file_path = listOfFiles[i].getAbsolutePath();
						System.out.println("found" + " " + target_file);
					}
				}
			}
			file = new File(target_file_path);
			document = PDDocument.load(file);
			document.getClass();
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper Tstripper = new PDFTextStripper();
				Tstripper.setStartPage(1);
				Tstripper.setEndPage(2);
				String st = Tstripper.getText(document);
				final Pattern pattern = Pattern
						.compile("Application [n|N]umber[:| :] (.*?)"
								+ fNEW_LINE);
				final Matcher matcher = pattern.matcher(st);
				matcher.find();
				System.out
						.println("###############################################################################");
				System.out.println((matcher.group(1)).trim());
				strApplcationNumber = (matcher.group(1)).trim();
				System.out
						.println("###############################################################################");

				// System.out.println(file.delete());
				// if (file.delete())
				// return strApplcationNumber;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return strApplcationNumber;
	}

	protected String getPremiumDOB(int intYears) {
		Calendar now = Calendar.getInstance();
		String strDate = null;
		now = Calendar.getInstance();
		now.add(Calendar.YEAR, intYears);
		strDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE)
				+ "/" + now.get(Calendar.YEAR);
		Date date=new Date(strDate);
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String today = formatter.format(date);
		System.out.println(today);
		return today;
	}

	public static String getRandomText(int len) {
		StringBuilder b = new StringBuilder();
		Random r = new Random();
		for (int i = 0; i < len; i++) {
			char c = (char) (65 + r.nextInt(25));
			b.append(c);
		}
		return b.toString();
	}

	public static String getMonthNameMMM() {
		Format formatter = new SimpleDateFormat("MMM");
		String ss = null;
		ss = formatter.format(new Date());
		return ss.toUpperCase();
	}

	public static String getWeekDayNameEEE() {
		Format formatterDate = new SimpleDateFormat("EEE", Locale.getDefault());
		String strDate = null;
		strDate = formatterDate.format(new Date());
		return strDate.toUpperCase();
	}

	public static String getRandomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING
					.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	/**
	 *
	 * This is the Single Point of entry for all the actions to be performed
	 *
	 */
	public boolean tryAction(WebElement elmName, String action,
			String strObjeName) throws Exception {
		return tryAction(elmName, action, strObjeName, "");
	}

	/**
	 *
	 * This is the Overloaded Single Point of entry for all the actions to be
	 * performed
	 *
	 */
	public boolean tryAction(WebElement elmName, String action,
			String strObjeName, String strData) throws Exception {

		if (action.equalsIgnoreCase("click")) {
			if (elmName.isEnabled())
				elmName.click();
			report.updateTestLog("Click", "Clicking on " + strObjeName,	Status.PASS);
		} else if (action.equalsIgnoreCase("SplClick")) {
			elmName.click();
		} else if (action.equalsIgnoreCase("sendkeys") || action.equalsIgnoreCase("inputText") || action.equalsIgnoreCase("Enter") || action.equalsIgnoreCase("Set")) {
			elmName.clear();
			elmName.sendKeys(strData);
			report.updateTestLog("Set", "Entering Text " + strData + " on "	+ strObjeName, Status.PASS);
		} else if (action.equalsIgnoreCase("clear")) {
			elmName.clear();
		} else if (action.equalsIgnoreCase("splset")) {
			elmName.clear();
			elmName.sendKeys(strData);
			report.updateTestLog("Set", "Entering Text " + strData + " on " + strObjeName, Status.PASS);
		} else if (action.equalsIgnoreCase("specialDropdown")) {
			sleep();
			elmName.click();
			sleep(1000);
			fluentWaitElement(By.linkText(strData)).click();
			report.updateTestLog("Click", "Selected value from " + strObjeName 	+ " is " + strData, Status.PASS);
		} else if (action.equalsIgnoreCase("linktext")) {
			List<WebElement> elmWebElements = null;
			WebElement spanText=null;
			elmWebElements = fluentWaitListElements(By.cssSelector(".dropdown-menu.open .text"));
			for (int i = 0; i < elmWebElements.size(); i++) {
				spanText = elmWebElements.get(i);
				if (spanText.getText().contains(strData)) {
					//sleep(1000);
					spanText.click();
					break;
				}
			}
			report.updateTestLog("LinkText", "Selected Text is" + strData + " on " + strObjeName, Status.PASS);
		} else if (action.equalsIgnoreCase("clkradio")) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elmName);
			sleep();
			report.updateTestLog("Click", "Clicking on button" + strObjeName, Status.PASS);
		} else if (action.equalsIgnoreCase("TAB")) {
			elmName.sendKeys(Keys.TAB);
		} else if (action.equalsIgnoreCase("alertOk")) {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
			report.updateTestLog("Accept Alert", "Clicking on Alert OK",Status.PASS);
		} else if (action.equalsIgnoreCase("MoveToElm")) {
			int elementPosition = elmName.getLocation().getY();
			String js = String.format("window.scroll(0, %s)", elementPosition);
			((JavascriptExecutor) driver).executeScript(js);
			sleep();
			report.updateTestLog("MoveToElm", "Scrolled to " + strObjeName,	Status.PASS);
		} else if(action.equalsIgnoreCase("ListSelect")) {
			elmName.click();
			//new Select(elmName).selectByVisibleText(strData);
			//new Select(elmName).selectByIndex(1);
			sleep();
			new Select(elmName).selectByIndex(1);
			sleep(15000);
			new Select(elmName).selectByIndex(1);
			//report.updateTestLog("List Select",	"'"	+ strObjeName+ "' Element Present in the stipulated time and Selected data is "	+ strData, Status.PASS);
		} else if (action.equalsIgnoreCase("DropDownSelect")) {
			elmName.click();
			sleep();
			final Select select = new Select(elmName);
			select.selectByVisibleText(strData);
			report.updateTestLog(
					"DropDown",
					"'"
							+ strObjeName
							+ "' Element Present in the stipulated time and Selected data is "
							+ strData, Status.PASS);
		} else if (action.equalsIgnoreCase("submit")) {
			elmName.submit();
		} else if (action.equalsIgnoreCase("chkCheck")) {
			if (!elmName.isSelected()) {
				elmName.click();
				report.updateTestLog(
						"Checkbox",
						"'"
								+ strObjeName
								+ "' Element Present in the stipulated time and Checked",
						Status.PASS);
			}
		} else if (action.equalsIgnoreCase("chkUnCheck")) {
			if (elmName.isSelected()) {
				elmName.click();
				report.updateTestLog(
						"Checkbox",
						"'"
								+ strObjeName
								+ "' Element Present in the stipulated time and UnChecked",
						Status.PASS);
			}
		} else if (action.equalsIgnoreCase("setEncrypted")) {
			elmName.clear();
			byte[] valueDecoded = Base64.decodeBase64(new String(strData));
			elmName.sendKeys(new String(valueDecoded));
			report.updateTestLog("Set", "Entering Encrypted Text " + strData
					+ " on " + strObjeName, Status.DONE);
		} else {
			return false;
		}
		return true;
	}

	public WebElement fluentWaitElements(final By byType) {
		return fluentWaitElements(byType, DEFAULT_WAIT_SECONDS);
	}

	public WebElement fluentWaitElements(final By byType, long timeout) {
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions
				.presenceOfElementLocated(byType));
		waitForVisibilityOfElement(byType, timeout);
		// webElements =
		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(byType)));
		return webElements;
	}

	public WebElement waitForClickableElement(final By byType) {
		return waitForClickableElement(byType, DEFAULT_WAIT_SECONDS);
	}

	public WebElement waitForClickableElement(final By byType, long timeout) {
		sleep(1000);
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions
				.elementToBeClickable(byType));
		waitForPresenceOfElement(byType, timeout);

		return webElements;
	}

	public WebElement waitForClickableWebElement(final WebElement byType) {
		return waitForClickableWebElement(byType, DEFAULT_WAIT_SECONDS);
	}

	public WebElement waitForClickableWebElement(final WebElement byType, long timeout) {
		sleep(1000);
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions
				.elementToBeClickable(byType));
		waitForPresenceOfWebElement(byType, timeout);

		return webElements;
	}

	public WebElement waitForPresenceOfWebElement(final WebElement byType, long timeout) {
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions.visibilityOf(byType));
		return webElements;
	}

	public WebElement waitForPresenceOfElement(final By byType, long timeout) {
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions
				.presenceOfElementLocated(byType));
		waitForVisibilityOfElement(byType, timeout);
		return webElements;
	}

	public WebElement waitForVisibilityOfElement(final By byType, long timeout) {
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions.visibilityOf(driver
				.findElement(byType)));
		webElements = wait.until(ExpectedConditions
				.visibilityOfElementLocated(byType));
		waitForElemetClikable(webElements, timeout);
		return webElements;
	}

	public boolean waitForElemetEnanble(final WebElement element){
		return waitForElemetEnanble(element,DEFAULT_WAIT_SECONDS);
	}

	public boolean waitForElemetEnanble(final WebElement element,
			long timeout) {
		Boolean webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions.elementToBeSelected(element));
		return webElements;
	}

	public boolean waitForStalenessOfElement(final WebElement element){
		return waitForStalenessOfElement(element,DEFAULT_WAIT_SECONDS);
	}

	public boolean waitForStalenessOfElement(final WebElement element,
			long timeout) {
		Boolean webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions.stalenessOf(element));
		return webElements;
	}

	public WebElement waitForElemetClikable(final WebElement element,
			long timeout) {
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		webElements = wait.until(ExpectedConditions
				.elementToBeClickable(element));

		return webElements;
	}

	public WebElement waitForClickableListElement(final WebElement element) {
		if (waitForClickableListElement(element, DEFAULT_WAIT_SECONDS))
			return fluentWaitElement(element);
		return fluentWaitElement(element);
	}

	public boolean waitForClickableListElement(final WebElement element,
			long timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		return wait.until(ExpectedConditions.elementToBeSelected(element));
	}

	public WebElement waitForElementToHaveData(final By by) {
		return waitForElementToHaveData(by, null, DEFAULT_WAIT_SECONDS);
	}

	public WebElement waitForElementToHaveData(final By by,
			final SearchContext context, final int seconds) {
		int wait = 0;
		while (wait < seconds * ONE_SECOND_MS) {
			List<WebElement> roots = context == null ? driver.findElements(by)
					: context.findElements(by);

			if (!roots.isEmpty() && roots.get(0).isDisplayed()
					&& roots.get(0).isEnabled()
					&& StringUtils.isNotBlank(roots.get(0).getText())) {
				return roots.get(0);
			}
			waitForMs(WAIT_PAUSE_MS);
			wait += WAIT_PAUSE_MS;
		}
		report.updateTestLog("waitForElementToHaveData", "Element: '" + by
				+ "' not found, not visible or has no data.", Status.FAIL);
		return null;
	}

	public WebElement waitForDropDownElementToHaveData(final By byType) {
		return waitForDropDownElementToHaveData(byType, DEFAULT_WAIT_SECONDS);
	}

	public WebElement waitForDropDownElementToHaveData(final By byType,
			long timeout) {
		WebElement webElements = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		if (wait.until(ExpectedConditions
				.stalenessOf(fluentWaitElements(byType)))) {
			webElements = wait.until(ExpectedConditions
					.presenceOfElementLocated(byType));
		} else {
			report.updateTestLog("waitForElementToHaveData", "Element: '"
					+ byType + "' not found, not visible or has no data.",
					Status.FAIL);
		}
		return webElements;
	}

	public void waitForMs(final int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			Thread.interrupted();
		}
	}

	public void sleep() {
		waitForMs(2000);
	}

	public void sleep(int timeout) {
		waitForMs(timeout);
	}

	public String getAttribute(final WebElement elementName,
			String attributeName) {
		return elementName.getAttribute(attributeName);
	}

	/*
	 * Table Control to get the Specific Cell Value from data Sheet Author
	 * Sampath
	 */
	public String getCellValue(WebElement tblElement, String data) {
		String strValue = data;
		String[] splitVal = strValue.split("#");
		int RowNo = Integer.parseInt(splitVal[0]);
		int ColNo = Integer.parseInt(splitVal[1]);

		String vCellValue = null;
		WebElement tbody = tblElement.findElement(By.tagName("tbody"));
		List<WebElement> allRows = tbody.findElements(By.tagName("tr"));
		if (!(allRows.size() == 0)) {
			List<WebElement> Cells = allRows.get(RowNo).findElements(
					By.tagName("td"));
			List<WebElement> cellHdrs = allRows.get(RowNo).findElements(
					By.tagName("th"));
			if (!(Cells.size() == 0)) {
				vCellValue = Cells.get(ColNo).getText();
			} else if (!(cellHdrs.size() == 0)) {
				vCellValue = Cells.get(ColNo).getText();
			} else {
				report.updateTestLog("GetCellValue - Function",
						"Table Column size is zero", Status.FAIL);
				vCellValue = null;
			}
		} else {
			report.updateTestLog("GetCellValue - Function",
					"Table Row size is zero", Status.FAIL);
			vCellValue = null;
		}
		return vCellValue;
	}

	/*
	 * Assert Text Contains with 2 Strings Author : Sampath
	 */
	public void assertContainsText(String strSource, String strDest) {

		if (strSource.contains(strDest)) {
			report.updateTestLog("Assert Contains", strSource
					+ " is present in " + strDest, Status.PASS);
		} else {
			report.updateTestLog("Assert Contains", strSource
					+ " is not present in " + strDest, Status.FAIL);
		}
	}

	/*
	 * Assert Text Contains with WebElement and String Author : Sampath
	 */
	public void assertContainsText(WebElement elmSource, String data2) {

		if (elmSource.getText().contains(data2)) {
			report.updateTestLog("Assert Contains", elmSource.getText()
					+ " is present in " + data2, Status.PASS);
		} else {
			report.updateTestLog("Assert Contains", elmSource.getText()
					+ " Data is not present in " + data2, Status.FAIL);
		}
	}

	/*
	 * Assert Text Contains with WebElement and String without special characters Author : Sampath
	 */
	public void assertContainsTextwithoutSpecialChars(WebElement elmSource, String data2) {

		String strPremium = null;
		strPremium = elmSource.getText().replaceAll("[\\s+$]", "");
		//System.out.println("strPremium is :: "+strPremium);
		if (data2.contains(strPremium)) {
			report.updateTestLog("Assert Contains", strPremium + " is present in " + data2, Status.PASS);
		} else {
			report.updateTestLog("Assert Contains", strPremium
					+ " Data is not present in " + data2, Status.FAIL);
		}
	}


	/*
	 * Assert Text Contains with 2 Strings Author : Sampath
	 */
	public void assertEqualText(String strExcelSource, String strDest) {

		if (strExcelSource.equals(strDest)) {
			report.updateTestLog("Assert Equals", strExcelSource + " is equal to "
					+ strDest, Status.PASS);
		} else {
			report.updateTestLog("Assert Equals", strExcelSource
					+ " is not equal to " + strDest, Status.FAIL);
		}
	}

	/*
	 * Assert Text equal with WebElement and String Author : Sampath
	 */
	public void assertEqualText(WebElement elmSource, String data2) {

		if (elmSource.getText().equals(data2)) {
			report.updateTestLog("Assert Equals", elmSource.getText()
					+ " is equal to " + data2, Status.PASS);
		} else {
			report.updateTestLog("Assert Equals", elmSource.getText()
					+ " Data is not equal to " + data2, Status.FAIL);
		}
	}

	public boolean isElementPresent(By element){
		Boolean isPresent = false;
		if(driver.findElements(element).size()>0)
			isPresent = true;
		else
			isPresent = false;

		return isPresent;
	}

	public boolean isElementPresent(By element,int intTime){

		WebDriverWait waiter = new WebDriverWait(driver, intTime);
		waiter.until( ExpectedConditions.visibilityOfElementLocated(element));
		Boolean isPresent = false;
		if(driver.findElements(element).size()>0)
			isPresent = true;
		else
			isPresent = false;

		return isPresent;
	}

	public boolean waitForpageload(String sTitle, int TimeoutInseconds ){
	try{
		WebDriverWait wait = new WebDriverWait(driver, TimeoutInseconds);
		wait.until(ExpectedConditions.titleContains(sTitle));
		return true;
	}catch (Exception e){
	}
	return false;
	}

	public void assertCSVStrings(String csvExcelData, String csvApplHiddenValue) {
		String[] st = csvApplHiddenValue.split(",");
		for (int i = 0; i < st.length; i++) {
			if (csvExcelData.contains(st[i])) {
				report.updateTestLog("Assert Equals","Expected Value is : "+csvExcelData+ "Actual Value is : "+ st[i], Status.PASS);
			}
			else{
				report.updateTestLog("Assert not Equals", "Expected Value is : "+csvExcelData+ "Actual Value is : "+ st[i], Status.FAIL);
			}
		}
	}



	public int pagescrolldown(int i) throws AWTException{
		Robot robot = new Robot();
		for(i=0;i>=100;i++);
		robot.keyPress(KeyEvent.VK_DOWN);
		// Press TAB
return i;
	}

	public int pagescrollup(int i) throws AWTException{
		Robot robot = new Robot();
		for(i=0;i>=100;i++);
		robot.keyPress(KeyEvent.VK_UP);
		// Press TAB
return i;
	}

	public By getObject(String strIdentification){
		By returnByType = null;
		returnByType = By.cssSelector(strIdentification);
		return returnByType;
	}

	public String getWebElementText(WebElement elememnt){
		System.out.println("fNEW_LINE :: "+fNEW_LINE);
		final Pattern pattern = Pattern
				.compile("Your application reference no. is +([0-9]+).*");
		System.out.println(elememnt.getText());
		final Matcher matcher = pattern.matcher(elememnt.getText());
		//matcher.find();
		//System.out.println("(matcher.group(1)).trim() "+(matcher.group(1)).trim());
		//System.out.println("(matcher.group(0)).trim() "+(matcher.group(0)).trim());
		//if(matcher.find())
		if(matcher.find())
			report.updateTestLog("Application Number", "Application Number is " + (matcher.group(1)).trim(), Status.PASS);
		return (matcher.group(1)).trim();
	}
	public List<WebElement> listElements(String elementValue) {
		 List<WebElement> 	elementWeb = driver.findElements(By.cssSelector(elementValue));
		return elementWeb;
	}
	public String randomgeneration(){
		Random randomGenerator = new Random();
		int random = randomGenerator .nextInt(300);
		String randomString = String.valueOf(random);
		System.out.println("Random String->"+randomString);
		return randomString;
	}
	public boolean waitForPageTitle(String sTitle, int timeOutInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.titleContains(sTitle));
            return true;
        } catch (Exception e) {

            return false;
        }
    }

	public void fieldValidation(String ArgFieldType,WebElement ArgObject, String ArgDatatableval, String ArgFieldName){

		/*	JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", ArgObject);*/

			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ArgObject);


			if(ArgObject.isDisplayed()==true){
				switch(ArgFieldType){
				case "WebEdit":
					System.out.println("...WEdt");
					String strvalue =ArgObject.getAttribute("value");
					if (strvalue.contains(",")) {
						String[] part = strvalue.split(",");
						String amount1 = part[0];
						String amount2 = part[1];

						strvalue = amount1+""+amount2;
						System.out.println("amount:"+strvalue);
					}


					if(strvalue.trim().equalsIgnoreCase(ArgDatatableval.trim())){
						report.updateTestLog(ArgFieldName, strvalue+ "value is displayed", Status.PASS);
					}else{
						report.updateTestLog(ArgFieldName, "Expected value is not displayed in "+
								ArgFieldName +"<BR><B>Expected Value: </B>"+ ArgDatatableval +"<BR><B>Actual Value: </B>"+strvalue,
								Status.FAIL);
					}
					break;
				case "WebList":
					System.out.println("...WL");
					//String SelectedOption = ArgObject.getAttribute("value");
					/*Select se=new Select(ArgObject);
					se.getFirstSelectedOption();
					String SelectedOption = se.getText();
					System.out.println("!!!"+SelectedOption);*/


					Select se=new Select(ArgObject);

					//To get the first selected option in the dropdown
					WebElement firstOption = se.getFirstSelectedOption();
					String SelectedOption = firstOption.getText();
					System.out.println("!!!"+SelectedOption);





					//String SelectedOption = ArgObject.getText();
					if(SelectedOption.trim().equals(ArgDatatableval.trim())){
						report.updateTestLog(ArgFieldName, "Expected value selected in the list box "+
								ArgFieldName +"<BR><B>Expected Value: </B>"+ SelectedOption, Status.PASS);
					}
					else{
						report.updateTestLog(ArgFieldName, "Expected Option is not selected in the list box  "
								+ ArgFieldName +"<BR><B>"+ "Expected Value: </B>"+ArgDatatableval + "<BR><B>Actual Value: </B>"
									+ SelectedOption , Status.FAIL);
					}
				    break;
				case "WebCheckBox":
					System.out.println("...WC");
					//boolean strChecked = ArgObject.isSelected();
					String strElementActive = ArgObject.getAttribute("class");

					if(ArgDatatableval.trim().equals("Checked")){
						if(strElementActive.trim().contains("active")==true){
							report.updateTestLog(ArgFieldName, ArgFieldName +" checkbox is checked", Status.PASS);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +" checkbox is not checked", Status.FAIL);
						}
					}
					else if(ArgDatatableval.trim().equals("UnChecked")){
						if(strElementActive.trim().contains("active")==true){
							report.updateTestLog(ArgFieldName, ArgFieldName +" checkbox is checked", Status.FAIL);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +" checkbox is not checked", Status.PASS);
						}
					}

					 break;
				case "WebButton":
					System.out.println("...WB");
					// boolean booSelected = ArgObject.isSelected();
					String strButtonActive = ArgObject.getAttribute("class");
					//Yes or No
					System.out.println("11111111111"+ArgDatatableval);
					System.out.println("11111111111"+strButtonActive);
					System.out.println("11111111111"+ArgFieldName);
					if ((ArgDatatableval.trim().equals("Yes")) || (ArgDatatableval.trim().equals("No")) || (ArgDatatableval.trim().equals("UnKnown")) || (ArgDatatableval.trim().equals("Unsure")) || (ArgDatatableval.trim().equals("0")) || (ArgDatatableval.trim().equals("1")) || (ArgDatatableval.trim().equals("2")) ) {
						System.out.println("Yes/ no");
						if( ArgDatatableval.trim().equals("Yes") ){
							System.out.println("Yes");
						if(strButtonActive.trim().contains("active")==true){
							System.out.println("true");
							//report.updateTestLog(ArgFieldName, ArgFieldName +"-Yes Web button is selected", Status.PASS);
							report.updateTestLog(ArgFieldName,  "Yes Web button is selected", Status.PASS);
						}
						else{
							System.out.println("false");
							//report.updateTestLog(ArgFieldName, ArgFieldName +"-Yes  Web button is not selected", Status.FAIL);
							report.updateTestLog(ArgFieldName,  "Yes  Web button is not selected", Status.FAIL);
						}
					}
					else if( ArgDatatableval.trim().equals("No")){
						System.out.println("no");
						if(strButtonActive.trim().contains("active")==true){
							System.out.println("true");
							//report.updateTestLog(ArgFieldName, ArgFieldName +"- No  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "No  Web button is checked", Status.PASS);
						}
						else{
							System.out.println("false");
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- No  Web button is not checked", Status.FAIL);
							report.updateTestLog(ArgFieldName, "No  Web button is not checked", Status.FAIL);
						}
					}
					else if( ArgDatatableval.trim().equals("UnKnown")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Unknown  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "Unknown  Web button is checked", Status.PASS);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +"- Unknown  Web button is not checked", Status.FAIL);
						}
					}

					else if( ArgDatatableval.trim().equals("Unsure")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Unknown  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "Unsure  Web button is checked", Status.PASS);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +"- Unsure  Web button is not checked", Status.FAIL);
						}
					}
						//0
					else if( ArgDatatableval.trim().equals("0")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Unknown  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "0  Web button is checked", Status.PASS);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +"- 0  Web button is not checked", Status.FAIL);
						}
					}
						//1
					else if( ArgDatatableval.trim().equals("1")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Unknown  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "1  Web button is checked", Status.PASS);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +"- 1  Web button is not checked", Status.FAIL);
						}
					}
						//2
					else if( ArgDatatableval.trim().equals("2")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Unknown  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "2  Web button is checked", Status.PASS);
						}
						else{
							report.updateTestLog(ArgFieldName, ArgFieldName +"- 2  Web button is not checked", Status.FAIL);
						}
					}
					else {
						report.updateTestLog("Data Table", ArgFieldName +"Invalid input is given", Status.FAIL);
					}


					}

					//0 1 2 Units
					else if ((ArgDatatableval.trim().equals("0")) || (ArgDatatableval.trim().equals("1")) || (ArgDatatableval.trim().equals("2")) ) {
						if( ArgDatatableval.trim().equals("0")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Male Web button is selected", Status.PASS);
							report.updateTestLog(ArgFieldName,  "0 Web button is selected", Status.PASS);
						}
						else{
							//report.updateTestLog(ArgFieldName, ArgFieldName +"-Male Web button is selected  Web button is not selected", Status.FAIL);
							report.updateTestLog(ArgFieldName, "0 Web button is not selected", Status.FAIL);
						}
					}
						else if( ArgDatatableval.trim().equals("1")){
							if(strButtonActive.trim().contains("active")==true){
							//report.updateTestLog(ArgFieldName, ArgFieldName +"- Female  Web button is checked", Status.PASS);
								report.updateTestLog(ArgFieldName,  "1 Web button is selected", Status.PASS);

							}
							else{
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Female  Web button is not checked", Status.FAIL);
								report.updateTestLog(ArgFieldName, "1 Web button is not selected", Status.FAIL);
							}
						}
						else if( ArgDatatableval.trim().equals("2")){
							if(strButtonActive.trim().contains("active")==true){
							//report.updateTestLog(ArgFieldName, ArgFieldName +"- Female  Web button is checked", Status.PASS);
								report.updateTestLog(ArgFieldName,  "2 Web button is selected", Status.PASS);

							}
							else{
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Female  Web button is not checked", Status.FAIL);
								report.updateTestLog(ArgFieldName, "2 Web button is not selected", Status.FAIL);
							}
						}
						else {
							report.updateTestLog("Data Table", ArgFieldName +"Invalid input is given", Status.FAIL);
						}

						}


					//Male or female
					else if ((ArgDatatableval.trim().equals("Male")) || (ArgDatatableval.trim().equals("Female")) ) {
						if( ArgDatatableval.trim().equals("Male")){
						if(strButtonActive.trim().contains("active")==true){
						//	report.updateTestLog(ArgFieldName, ArgFieldName +"- Male Web button is selected", Status.PASS);
							report.updateTestLog(ArgFieldName,  "Male Web button is selected", Status.PASS);
						}
						else{
							//report.updateTestLog(ArgFieldName, ArgFieldName +"-Male Web button is selected  Web button is not selected", Status.FAIL);
							report.updateTestLog(ArgFieldName, "Male Web button is not selected", Status.FAIL);
						}
					}
					else if( ArgDatatableval.trim().equals("Female")){
						if(strButtonActive.trim().contains("active")==true){
						//report.updateTestLog(ArgFieldName, ArgFieldName +"- Female  Web button is checked", Status.PASS);
							report.updateTestLog(ArgFieldName,  "Female  Web button is selected", Status.PASS);

						}
						else{
							//report.updateTestLog(ArgFieldName, ArgFieldName +"- Female  Web button is not checked", Status.FAIL);
							report.updateTestLog(ArgFieldName, "Female  Web button is not selected", Status.FAIL);
						}
					}

					else {
						report.updateTestLog("Data Table", ArgFieldName +"Invalid input is given", Status.FAIL);
					}

					}

					//Morning or Afternoon
					else if ((ArgDatatableval.trim().equals("Morning")) || (ArgDatatableval.trim().equals("Afternoon")) ) {
						if( ArgDatatableval.trim().equals("Morning") ){
							if(strButtonActive.trim().contains("active")==true){
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Morning Web button is selected", Status.PASS);
								report.updateTestLog(ArgFieldName, "Morning Web button is selected", Status.PASS);
							}
							else{
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Morning Web button is selected  Web button is not selected", Status.FAIL);
								report.updateTestLog(ArgFieldName, "Morning Web button is selected  Web button is not selected", Status.FAIL);
							}
						}
						else if( ArgDatatableval.trim().equals("Afternoon") ){
							if(strButtonActive.trim().contains("active")==true){
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Afternoon Web button is checked", Status.PASS);
								report.updateTestLog(ArgFieldName,  "Afternoon Web button is checked", Status.PASS);
							}
							else{
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Afternoon  Web button is not checked", Status.FAIL);
								report.updateTestLog(ArgFieldName,  "Afternoon  Web button is not checked", Status.FAIL);
							}
						}

						else {
							report.updateTestLog("Data Table", ArgFieldName +"Invalid input is given", Status.FAIL);
						}
					}

					//Fixed or Unitized
					else if ((ArgDatatableval.trim().equalsIgnoreCase("Fixed")) || (ArgDatatableval.trim().equalsIgnoreCase("unit")) ) {

						if( ArgDatatableval.trim().equalsIgnoreCase("Fixed") ){
							if(strButtonActive.trim().contains("active")==true){
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Fixed Web button is selected", Status.PASS);
								report.updateTestLog(ArgFieldName, "Fixed Web button is selected", Status.PASS);
							}
							else{
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Fixed Web button is selected  Web button is not selected", Status.FAIL);
								report.updateTestLog(ArgFieldName, "Fixed Web button is selected  Web button is not selected", Status.FAIL);
							}
						}
						else if( ArgDatatableval.trim().equalsIgnoreCase("unit") ){
							if(strButtonActive.trim().contains("active")==true){
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Unitised  Web button is checked", Status.PASS);
								report.updateTestLog(ArgFieldName, "Unitised  Web button is checked", Status.PASS);
							}
							else{
								//report.updateTestLog(ArgFieldName, ArgFieldName +"- Unitised  Web button is not checked", Status.FAIL);
								report.updateTestLog(ArgFieldName, "Unitised  Web button is not checked", Status.FAIL);
							}
						}

						else {
							report.updateTestLog("Data Table", ArgFieldName +"Invalid input is given", Status.FAIL);
						}
					}

					else {
						report.updateTestLog("Data Table", ArgFieldName +"Invalid input is given", Status.FAIL);
					}

					break;

				case "WebElement":
					System.out.println("...WEle");
					String strWebElementValue = ArgObject.getText();
					System.out.println("ArgFieldName - "+ArgFieldName+"/Pg val - "+ArgDatatableval.trim()+"/IP value -"+strWebElementValue.trim());
					if(ArgDatatableval.trim().equals(strWebElementValue.trim())){
						//report.updateTestLog(ArgFieldName, ArgFieldName +strWebElementValue +"  WebElement value is displayed", Status.PASS);
						report.updateTestLog(ArgFieldName,  strWebElementValue +" value is displayed", Status.PASS);
					}
					else{
						//report.updateTestLog(ArgFieldName, ArgFieldName +strWebElementValue +"  WebElement value is not displayed", Status.FAIL);
						report.updateTestLog(ArgFieldName, "Expected Value-"+ArgDatatableval+",Value present-"+strWebElementValue +" value is not displayed", Status.FAIL);
					}

					break;

				case "Amount Validation":
					System.out.println("...Pre");
					String strAmountValue = ArgObject.getText();

					if (strAmountValue.contains(",")) {
						String[] part = strAmountValue.split(",");
						String amount1 = part[0];
						String amount2 = part[1];

						strAmountValue = amount1+""+amount2;
						System.out.println("amount:"+strAmountValue);
					}





					System.out.println("ArgFieldName - "+ArgFieldName+"/Pg val - "+ArgDatatableval.trim()+"/IP value -"+strAmountValue.trim());
					if(ArgDatatableval.trim().equals(strAmountValue.trim())){
						//report.updateTestLog(ArgFieldName, ArgFieldName +strWebElementValue +"  WebElement value is displayed", Status.PASS);
						report.updateTestLog(ArgFieldName, "Expected Premium: "+ ArgDatatableval + " Actual Premium: " + strAmountValue +" is displayed" , Status.PASS);
					}
					else{
						//report.updateTestLog(ArgFieldName, ArgFieldName +strWebElementValue +"  WebElement value is not displayed", Status.FAIL);
						report.updateTestLog(ArgFieldName, "Expected Premium: "+ ArgDatatableval + " Actual Premium: " + strAmountValue +" is displayed", Status.FAIL);
					}

					break;


				default:
					System.out.println("default");
				report.updateTestLog("objectLevelFunction", "Pass valid value to validate the field "
						, Status.FAIL);
				break;
				}
			}else{
				report.updateTestLog(ArgFieldName, ArgFieldName +" field is not displayed", Status.FAIL);
			}
	}

}