/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class PersonalDetailsPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public PersonalDetailsPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	
	public static final By txtClientRefId = getObjectIdentifaction("personalDetail.clientRefNum.id");
	public static final By txtFirstname = getObjectIdentifaction("personalDetail.firstName.id");
	public static final By txtSurname = getObjectIdentifaction("personalDetail.lastName.id");
	public static final By txtDateOfBirth = getObjectIdentifaction("personalDetail.DOB.id");
	public static final By lstMemberType = getObjectIdentifaction("personalDetail.memberType.id");
	public static final By txtCompaignCode = getObjectIdentifaction("pwc.personalDetail.campaigncode.txt.id");
	public static final By txtBrokerMailId = getObjectIdentifaction("pwc.personalDetail.emailid.txt.id");
	public static final By btnNewApplication = getObjectIdentifaction("personalDetail.btnNewApplication.css");
	public static final By btnRetrieveApplication = getObjectIdentifaction("personalDetail.btnRetrieveApplication.css");

}
