/**
 * This is a helper class created intending to help Aura intigration framework in reading/writing xml files.
 * Aura webservice expect an string UTF-8 format and returns an string as an output. Both strings parsed and
 * formatted here in this class considring both system's expectations and limitations.
 */		
package au.com.metlife.eapply.underwriting.utils;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;
import au.com.metlife.eapply.underwriting.response.model.ClientData;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Impairment;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.ItemData;
import au.com.metlife.eapply.underwriting.response.model.LookUp;
import au.com.metlife.eapply.underwriting.response.model.Product;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.underwriting.response.model.Rule;
import au.com.metlife.eapply.underwriting.response.model.RuleAnswer;
import au.com.metlife.eapply.underwriting.response.model.Rules;
import au.com.metlife.eapply.underwriting.response.model.UnitValues;

public class XMLHelper {
	private XMLHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(XMLHelper.class);

	
	

	/**
	 * This method receive node and attribute name. Following method parse the incoming Node and returns the attribute value.
	 * @param outputString
	 */
	private static String getAttributeValue(Node node, String attributeName) {
		
		
		String retVal = null;
		NamedNodeMap attributesMap = node.getAttributes();
		if (null != attributesMap) {
			Node childAttributeNode = attributesMap.getNamedItem(attributeName);
			
			if (null != childAttributeNode && null != childAttributeNode.getNodeValue()) {
				retVal = childAttributeNode.getNodeValue().trim();
			}
		}
		
		return retVal;
	}
	
	/**
	 * This method accepts xml in form of String and prepare a object structure out of it. This Object is exact replica of XML
	 * and this contains all values of XML.
	 * @param outputString
	 * @return
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 */
	public static ResponseObject prepareResponse(String outputString) throws SAXException, IOException, ParserConfigurationException {
		
		ResponseObject responseObj = null;
		Insured insured = new Insured();
		responseObj = initialize();
		Document doc;
		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(outputString)));

			if (null != doc) {
				parseNode(doc, responseObj, insured);
			}

		} catch (SAXException e) {
			log.error("Error in SAX : {}",e);
		throw e;
		} catch (IOException e) {
			log.error("Error in input and output : {}",e);
			throw e;
		} catch (ParserConfigurationException e) {
			log.error("Error in Prepare configuration: {}",e);
			throw e;
		}
		return responseObj;
	}

	private static void parseNode(Node parentNode, ResponseObject responseObj, Insured insured) {
		
		NodeList childNodes = parentNode.getChildNodes();
		int iterator = childNodes.getLength();
		
		while(iterator >= 0) {
			Node childNode = childNodes.item(iterator);
			
			if (null != childNode && null != childNode.getNodeName()) {
				if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_CLIENT_DATA)) {
					setClientData(parentNode, childNode, responseObj);
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_INSURED)) {
					setInsuredValues(parentNode, childNode, responseObj, insured);
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_OVERALL_DECISIONS)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListOverallDec().add(getDecision(childNode));
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_ORIGINAL_RULE_DECISIONS)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListOriginalRuleDec().add(getDecision(childNode));
					}						
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RULE_DECISIONS)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListRuleDec().add(getDecision(childNode));
					}						
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_GENDER_AGE_BMI_DECISIONS)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListGenderAgeBMIDec().add(getDecision(childNode));
					}						
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_GENDER_AGE_BMI_RANGE_TABLE)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListGenderAgeBMIRangeTable().add(getDecision(childNode));
					}						
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_GENDER_AGE_BMI_RANGE_TABLE_BILL_PROTECT)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListGenderAgeBMIRangeTable().add(getDecision(childNode));
					}						
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_GENDER_AGE_CALCULATED_BMI_RANGE_NONSMK)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListGenderAgeCalculatedBMIRangeMetlifeTable().add(getDecision(childNode));
					}						
				}	
				
				else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_AGE_COVERAGE)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListAgeCoverageDec().add(getDecision(childNode));
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_INSURED_DECISION)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListInsuredDec().add(getDecision(childNode));
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_IMPAIRMENT_DECISION)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_IMPAIRMENT)) {
						getImpairment(childNode, insured);
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DEPENDENT_RISK_DECISION)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						insured.getListDepRiskDec().add(getDecision(childNode));
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_XML)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RULES)) {
						Rules rules = new Rules();
						rules.setApplicationComplete(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_APPLICATION_COMPLETE));
						rules.setApplicationSubmitted(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_APPLICATION_SUBMITTED));
						rules.setPercentComplete(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_PERCENT_COMPLETE));
						responseObj.setRules(rules);
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RULES)) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RULE)) {
						responseObj.getRules().getListRule().add(getRule(childNode));
					}
				} else if (parentNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_LOOK_UPS)) {
					setLookUpValues(childNode, responseObj);
				}
				parseNode(childNode, responseObj, insured);
			}
			iterator --;
		}
	}

	private static void setClientData(Node parentNode, Node childNode, ResponseObject responseObj) {
		
		if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_ENGINE_VARS)) {
			responseObj.getClientData().getListIntEngineVar().addAll(getEngineVariables(childNode, ApplicationConstants.TAG_INTERNAL, true));
			responseObj.getClientData().getListExtEngineVar().addAll(getEngineVariables(childNode, ApplicationConstants.TAG_EXTERNAL, true));

		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_APPLICATION_VERSION)) {
			responseObj.getClientData().setQuestionFilter(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_QUESTION_FILTER));
			responseObj.getClientData().setSessionId(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_SESSION_ID));
			responseObj.getClientData().setAppExtractLan(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_EXTRACT_LANGUAGE));
			responseObj.getClientData().setAppIntMode(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_INTERVIEW_MODE));
			responseObj.getClientData().setAppSetCode(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_SET_CODE));
			responseObj.getClientData().setAppSubSetCode(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_SUB_SET_CODE));
			responseObj.getClientData().setAppVersion(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_AURA_VERSION));
		}
	}

	private static void setLookUpValues(Node childNode, ResponseObject responseObject) {
		LookUp lookUp = new LookUp();
		
		if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_CONSTANTS)) {
			getLookUpConstants(childNode, lookUp);
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_UNDERWRITING_DECISIONS)) {
			lookUp.getListUWD().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_UNDERWRITING_DECISION_PRE)) {
			lookUp.getListUWDPrecedence().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_REQUIREMENTS)) {
			lookUp.getListRequirement().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_SUB_CATEGORIES)) {
			lookUp.getListSubCategories().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_SUB_CATEGORIES_CLASS)) {
			lookUp.getListSubCategoryClass().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_IMPAIRMENTS)) {
			lookUp.getListImpairments().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RESULT_TYPES)) {
			lookUp.getListResultType().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_QUESTION_TYPES)) {
			lookUp.getListQuestionType().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_SEVERITIES)) {
			lookUp.getListSeverity().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_HEIGHT_WEIGHT_DECISIONS)) {
			lookUp.getListHeightWeightDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_AGE_COV_DECISIONS)) {
			lookUp.getListAgeCoverDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_AGE_BMI_DECISIONS)) {
			lookUp.getListAgeBMI().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_COV_AMT_DECISIONS)) {
			lookUp.getListCvgeAmtDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_GENDER_MERITAL_DECISIONS)) {
			lookUp.getListGenderMaritalDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_AGE_DISTR_CVG_DECISIONS)) {
			lookUp.getListAgeDistrCovDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION_TYPES)) {
			lookUp.getListDecType().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_CHOLESTEROL_DECISIONS)) {
			lookUp.getListCholesterolDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_HYPERTENSION_DECISIONS)) {
			lookUp.getListHyperTenDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_JUVENILE_HEIGHT_WEIGHT_DECISIONS)) {
			lookUp.getListJuvenileHWDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_GENDER_AGE_BMI_DECISIONS)) {
			lookUp.getListGenderAgeBMIDec().addAll(getChildItemValues(childNode, false));
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_AGE_COVERAGE)) {
			lookUp.getListAgeCov().addAll(getChildItemValues(childNode, false));
		}
		responseObject.setLookup(lookUp);
	}

	private static void setInsuredValues(Node parentNode, Node childNode, ResponseObject responseObj, Insured insured) {
		
		if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_PRODUCTS)) {
			insured.setAge(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_AGE));
			insured.setBmi(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_BMI));
			insured.setCholesterol(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_CHOLESTROL));
			insured.setDiastolic(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_DIASTOLIC));
			insured.setGender(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_GENDER));
			insured.setHdlCholesterol(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_HDL_CHOLESTROL));
			insured.setHeight(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_HEIGHT));
			insured.setInterviewSubmitted(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_INT_SUBMITTED));
			insured.setMaritalStatus(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_MERITAL_STATUS));
			insured.setName(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_NAME));
			insured.setPercentComplete(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_PERCENT_COMPLETE));
			insured.setSmoker(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_SMOKER));
			insured.setSystolic(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_SYSTOLIC));
			insured.setUniqueId(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_UNIQUE_ID));
			insured.setWaived(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_WAIVED));
			insured.setWeight(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_WEIGHT));
			getProducts(childNode, insured);
			ClientData clientData = new ClientData();
			clientData.getListInsured().add(insured);
			responseObj.setClientData(clientData);
		} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_ENGINE_VARS)) {
			insured.getListIntEngineVar().addAll(getEngineVariables(childNode, ApplicationConstants.TAG_INTERNAL, true));
			insured.getListExtEngineVar().addAll(getEngineVariables(childNode, ApplicationConstants.TAG_EXTERNAL, true));
		}
	}

	private static void getLookUpConstants(Node childNode, LookUp lookUp) {
		
		NodeList nodeList = childNode.getChildNodes();
		if (null != nodeList) {
			int itr = nodeList.getLength();
			while (itr >= 0) {
				Node constNode = nodeList.item(itr);
				if (null != constNode) {
					lookUp.getLookUpConstants().put(constNode.getNodeName(), constNode.getTextContent());
				}
				itr--;
			}
		}
	}

	private static void getImpairment(Node childNode, Insured insured) {
		
		Impairment impairment = new Impairment();
		impairment.setBase(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_BASE));
		impairment.setCat(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_CAT));
		impairment.setMed(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_MED));
		impairment.setName(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_NAME));
		impairment.setRiskType(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_RISK_TYPE));
		impairment.setSev(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_SEV));
		if (null != childNode.getChildNodes()) {
			NodeList impairmentDecisions = childNode.getChildNodes();
			int itr = impairmentDecisions.getLength();
			while(itr >= 0) {
				if (null != impairmentDecisions.item(itr)) {
					impairment.getListDecision().add(getDecision(impairmentDecisions.item(itr)));
				}
				itr--;
			}
		}
		insured.getListImpairmentDec().add(impairment);
		
		
	}
	
	private static ResponseObject initialize() {
		
		ResponseObject responseObj = new ResponseObject();
		
			if (null == responseObj.getClientData()) {
				responseObj.setClientData(new ClientData());
			}
			if (null == responseObj.getRules()) {
				responseObj.setRules(new Rules());
			}
			if (null == responseObj.getLookup()) {
				responseObj.setLookup(new LookUp());
			}
		return responseObj;
	}
	
	private static List<ItemData> getEngineVariables(Node parentNode, String enVarType, boolean enVarInd) {
		
		
		NodeList childEngVar = parentNode.getChildNodes();
		List<ItemData> retItemData = new ArrayList<>();
		
		if (null != childEngVar) {
			int itrChildEngVar = childEngVar.getLength();
			
			while(itrChildEngVar >= 0) {
				Node childNode = childEngVar.item(itrChildEngVar);
				
				if (null != childNode && null != childNode.getChildNodes()
						&& (childNode.getNodeName().equalsIgnoreCase(enVarType))) {
						retItemData = getChildItemValues(childNode, enVarInd);
				}
				itrChildEngVar --;
			}
		}
		
		return retItemData;
	}
	
	private static List<ItemData> getChildItemValues(Node tmpNode, boolean enVarInd) {
		
		
		List<ItemData> retItemData = new ArrayList<>();
		NodeList itemNodes = tmpNode.getChildNodes();
		
		if (null != itemNodes) {
			int iterator = itemNodes.getLength();
			
			while (iterator >= 0) {
				Node itemNode = itemNodes.item(iterator);
				if (null != itemNode) {
					ItemData item = new ItemData();

					if (enVarInd) {
						item.setCode(getAttributeValue(itemNode, ApplicationConstants.ATTRIBUTE_NAME));
					} else {
						item.setCode(getAttributeValue(itemNode, ApplicationConstants.ATTRIBUTE_CODE));
						item.setId(getAttributeValue(itemNode, ApplicationConstants.ATTRIBUTE_ID));
					}
					item.setValue(itemNode.getTextContent());
					retItemData.add(item);
				}
				iterator --;
			}
			
		}
		
		return retItemData;
	}
	
	private static void getProducts(Node parentNode, Insured insured) {
		
		
		NodeList productNodes = parentNode.getChildNodes();
		
		if (null != productNodes) {
			int iterator = productNodes.getLength();
			
			while (iterator >= 0) {
				Node productNode = productNodes.item(iterator);
				
				if (null != productNode) {
					Product product = new Product();
					product.setAgeCovAmt(getAttributeValue(productNode, ApplicationConstants.ATTRIBUTE_AGE_COVERAGE_AMT));
					product.setAgeDistCovAmt(getAttributeValue(productNode, ApplicationConstants.ATTRIBUTE_AGE_DISTR_COVERAGE_AMT));
					product.setCoverageAmt(getAttributeValue(productNode, ApplicationConstants.ATTRIBUTE_COVERAGE_AMT));
					product.setFinCovAmt(getAttributeValue(productNode, ApplicationConstants.ATTRIBUTE_FIN_COVERAGE_AMT));
					product.setValue(productNode.getTextContent());
					insured.getListProduct().add(product);
				}
				iterator --;
			}
		}
		
	}
	
	private static Decision getDecision(Node parentNode) {
	
		
		
		Decision decision = new Decision();
		decision.setProductName(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_PRODUCT));
		decision.setMat(getAttributeValue(parentNode, ApplicationConstants.ATTRIBUTE_MAT));
	
		int mExclusions = 0;
		int lExclusions = 0;
		
		NodeList decisionNodes = parentNode.getChildNodes();
		if (null != decisionNodes) {
			int iterator = decisionNodes.getLength();
			
			while (iterator >= 0) {
				Node decisionNode = decisionNodes.item(iterator);
				
				if (null != decisionNode) {
					if (decisionNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_UNDERWRITING_DECISION)) {
						decision.setDecision(getAttributeValue(decisionNode, ApplicationConstants.TAG_DECISION));
						decision.setReasons(getReasons(decisionNode));
					} else if (decisionNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RATING)) {
						Rating rating = new Rating();
						rating.setType(getAttributeValue(decisionNode, ApplicationConstants.ATTRIBUTE_TYPE));
						if (null != decisionNode.getFirstChild()) {
							rating.setValue(decisionNode.getFirstChild().getTextContent());
							decision.setTotalDebitsValue(decisionNode.getFirstChild().getTextContent());
							decision.setOriginalTotalDebitsValue(decisionNode.getFirstChild().getTextContent());
						}
						rating.setReason(getReasons(decisionNode)[0]);
						decision.getListRating().add(rating);
						
					} else if (decisionNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_TOTAL_DEBITS)) {
						if (null != decisionNode.getFirstChild()) {
							decision.setTotalDebitsValue(decisionNode.getFirstChild().getTextContent());
							decision.setOriginalTotalDebitsValue(decisionNode.getFirstChild().getTextContent());
						}
						decision.setTotalDebitsReason(getReasons(decisionNode)[0]);
					} else if (decisionNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_EXCLUSION)) {
						Exclusion exclusion = new Exclusion();
						exclusion.setName(getAttributeValue(decisionNode, ApplicationConstants.ATTRIBUTE_NAME));
						
						if(exclusion.getName().contains("EXCL_P")){
							lExclusions = lExclusions+1;
						}else if(exclusion.getName().contains("EXCL_M")){
							mExclusions = mExclusions+1;
						}
						
						if (null != decisionNode.getFirstChild()) {
							exclusion.setText(decisionNode.getFirstChild().getTextContent());
						}
						exclusion.setReason(getReasons(decisionNode));
						decision.getListExclusion().add(exclusion);
					}
				}
				iterator--;
			}
			
			decision.setMExclusions(mExclusions);
			decision.setLExclusions(lExclusions);
		}
		
		return decision;
	}
	
	private static String[] getReasons(Node parentnode) {
		
		
		NodeList reasonNodes = parentnode.getChildNodes();
		String[] reasons = new String[parentnode.getChildNodes().getLength()];
		
		if (null != reasonNodes && reasonNodes.getLength() > 0) {
			int itrReason = reasonNodes.getLength();
			int itrReasonArr = 0;
			while(itrReason >= 0) {
				Node reasonNode = reasonNodes.item(itrReason);
				if (null != reasonNode && null != reasonNode.getTextContent() 
						&& reasonNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_REASON)) {
					reasons[itrReasonArr] = reasonNode.getTextContent();
					itrReasonArr++;
				}
				itrReason--;
			}
		}

		return reasons;
	}
	
	private static Rule getRule(Node parentnode) {
		
		List<RuleAnswer> listAllAnswers = new ArrayList<>();
		
		Rule rule = new Rule();
		rule.setAlias(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_ALIAS));
		rule.setEv(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_EV));
		rule.setHt(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_HT));
		rule.setInsuredId(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_INSURED_ID));
		rule.setNumber(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_NUMBER));
		rule.setQuestionTypeId(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_QUEST_TYPE_ID));
		rule.setResultType(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_RESULT_TYPE));
		rule.setSeq(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_SEQ));
		rule.setVisible(Boolean.parseBoolean(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_VISIBLE)));
		rule.setWidth(getAttributeValue(parentnode, ApplicationConstants.ATTRIBUTE_WIDTH));
		if (null != parentnode.getFirstChild()) {
			rule.setValue(parentnode.getFirstChild().getTextContent());
		}
		
		NodeList ruleChildNodes = parentnode.getChildNodes();
		if (null != ruleChildNodes) {
			int iteratorRule = ruleChildNodes.getLength();
			while (iteratorRule >= 0) {
				Node childNode = ruleChildNodes.item(iteratorRule);
				if(null != childNode) {
					if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_ANSWER)) {
						getRuleAnswer(rule, childNode, listAllAnswers);
					} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_UNSELECTED_ANSWER)) {
						getRuleUnselectedAnswer(rule, childNode, listAllAnswers);
					} else if (childNode.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_VALUE)) {
						UnitValues unitVals = new UnitValues();
						unitVals.setPosition(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_POSITION));
						unitVals.setUnit(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_UNIT));
						if (null != childNode.getFirstChild()) {
							unitVals.setValue(childNode.getFirstChild().getTextContent());
						}
						rule.getListUnitValues().add(unitVals);
					}
				}
				iteratorRule--;
			}
		}
		Collections.reverse(listAllAnswers);
		rule.getListSelUnselAnswers().addAll(listAllAnswers);
		return rule;
	}

	private static void getRuleAnswer(Rule rule, Node childNode, List<RuleAnswer> listAllAnswers) {
		
		
		RuleAnswer answer = new RuleAnswer();
		answer.setAlias(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_ALIAS));
		answer.setImpId(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_IMP_ID));
		answer.setMed(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_MED));
		answer.setResultType(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_RESULT_TYPE));
		answer.setRiskType(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_RISK_TYPE));
		answer.setSev(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_SEV));
		answer.setSubCatId(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_SUB_CAT_ID));
		answer.setAnswered(Boolean.TRUE);
		if (null != childNode.getFirstChild()) {
			answer.setValue(childNode.getFirstChild().getTextContent());
		}
		NodeList childrenAnswer = childNode.getChildNodes();
		if (null != childrenAnswer) {
			int iterator = childrenAnswer.getLength();
			while (iterator >= 0) {
				Node childAnswer = childrenAnswer.item(iterator);
				if (null != childAnswer) { 
					if (childAnswer.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_DECISION)) {
						answer.getListUWDecisions().add(getDecision(childAnswer));
					} else if (childAnswer.getNodeName().equalsIgnoreCase(ApplicationConstants.TAG_RULE)) {
						answer.getListRule().add(getRule(childAnswer));
					}
				}
				iterator--;
			}
		}
		rule.setAnswer(answer);
		listAllAnswers.add(answer);
		
	}
	
	private static void getRuleUnselectedAnswer(Rule rule, Node childNode, List<RuleAnswer> listAllAnswers) {
		
		
		RuleAnswer unselectedAnswer = new RuleAnswer();
		unselectedAnswer.setAlias(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_ALIAS));
		unselectedAnswer.setSev(getAttributeValue(childNode, ApplicationConstants.ATTRIBUTE_SEV));
		unselectedAnswer.setAnswered(Boolean.FALSE);
		if (null != childNode.getFirstChild()) {
			unselectedAnswer.setValue(childNode.getFirstChild().getTextContent());
		}
		listAllAnswers.add(unselectedAnswer);
		
	}
}