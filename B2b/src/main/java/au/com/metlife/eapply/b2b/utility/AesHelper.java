package au.com.metlife.eapply.b2b.utility;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;



public class AesHelper {
	public static final String key = "Bar12345Bar12345"; // 128 bit key
	public static final String initVector = "RandomInitVector"; // 16 bytes IV
	
	 public static String encrypt(String value) {
	        try {
	            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

	            byte[] encrypted = cipher.doFinal(value.getBytes());
	            System.out.println("encrypted string: "
	                    + Base64.encodeBase64String(encrypted));

	            return Base64.encodeBase64String(encrypted);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        return null;
	    }

	    public static String decrypt(String key, String initVector, String encrypted) {
	        try {
	            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

	            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

	            return new String(original);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        return null;
	    }

	   /* public static void main(String[] args) {
	       
	        String value = "<request><adminPartnerID>NSFS</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>NSFS</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>NSFS</partnerID><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>7</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal Division</memberType><personalDetails><firstName>test</firstName><lastName>testB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>";
	       System.out.println(encrypt(key, initVector, value)); ;
	        System.out.println(decrypt(key, initVector,
	                encrypt(key, initVector, "Hello World")));
	    }*/

}
