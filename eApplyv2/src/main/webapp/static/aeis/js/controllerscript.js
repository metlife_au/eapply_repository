// create the module and name it scotchApp
        // also include ngRoute for all our routing needs
    var scotchApp = angular.module('eapplymain', ['ngRoute','ngResource']);
    
    // configure our routes
   scotchApp.config(function($routeProvider,$httpProvider) {
	   $httpProvider.defaults.withCredentials = true;
        $routeProvider   
        
        	//.when('/', {
          .when('/', {
                templateUrl : 'care/landingpage.html',
                controller  : 'login',
               reloadOnSearch: false
            })         
            
            .when('/landing', {
                templateUrl : 'care/landingpage.html',
                controller  : 'landing'
            })
            .when('/quote', {
                templateUrl : 'care/coverDetails_changeCover.html',
                controller  : 'quote'
            })
            
            .when('/newmemberquote', {
              templateUrl : 'care/newMember_changeCover.html',
               controller  : 'newmemberquote'
            })
            
           .when('/quotecancel', {
                templateUrl : 'care/coverDetails_cancelCover.html',
                controller  : 'quotecancel'
            })
            
            .when('/retrievesavedapplication', {
            	templateUrl : 'care/retrive_applications.html',
            	controller  : 'retrievesavedapplication'
            })
             .when('/quotetransfer', {           
            	 templateUrl : 'care/coverDetails_transfer.html',
                controller  :  'quotetransfer'
            })
             .when('/quoteoccchange', {
                templateUrl : 'care/coverDetails_updateDetails.html',
                controller  : 'quoteoccupdate'
            })
             .when('/auratransfer', {             
            	 templateUrl : 'care/aura_transferCover.html',
                controller  : 'auratransfer'
            })
            .when('/auraocc', {             
            	 templateUrl : 'care/aura_updateDetails.html',
                controller  : 'auraocc'
            })
            .when('/auraspecialoffer', {             
            	 templateUrl : 'care/aura_special_offer.html',
                controller  : 'auraspecialoffer'
            })
            .when('/aura', {
                templateUrl : 'care/aura.html',
                controller  : 'aura'
            })
            .when('/summary', {
                templateUrl : 'care/confirmation.html',
                controller  : 'summary'
            })
            .when('/descision', {
                templateUrl : 'care/decision.html',
                controller  : 'decision'
            })
             .when('/requirements/:clm', {
                templateUrl : 'claimtracker/events.html',
                controller  : 'requirements'
            })
         

            
    });
  
   /*Login Page Controller Starts*/  
    scotchApp.controller('login',['$scope','$routeParams', '$location', 'getclientData', 'tokenNumService','deathCoverService','tpdCoverService','ipCoverService','newMemberOfferService', '$http', '$window', function($scope, $routeParams, $location, getclientData, tokenNumService, deathCoverService, tpdCoverService, ipCoverService, newMemberOfferService, $http, $window){
    	
    	$scope.message = 'Look! I am an about page.';    
        tokenNumService.setTokenId($routeParams.InputData)     
       // $location.search( 'InputData', null );
          getclientData.requestObj().then(function(response) {    	
        	$scope.partnerRequest = response.data;
        	$scope.applicantlist = $scope.partnerRequest.applicant;         	
        	angular.forEach($scope.applicantlist, function (applicant) {        		
        		$scope.personalDetails = applicant.personalDetails          	
        		$scope.coverList = applicant.existingCovers;         		
        		var age = moment().diff(applicant.personalDetails.dateOfBirth, 'years');        		
        		$scope.newMemberOffer = false;
        		if(moment().diff(moment(applicant.welcomeLetterDate, 'DD-MM-YYYY'), 'days')<91 && age<61 && applicant.memberType=='Industry'){
        			$scope.newMemberOffer = true;
        		}else{
        			$scope.newMemberOffer = false;
        		}
        		newMemberOfferService.setNewMemberOfferFlag($scope.newMemberOffer);        		
        		angular.forEach($scope.coverList, function (cover) {            		
            		angular.forEach(cover, function(value, key) {
            			$scope.benefitType = value.benefitType;            			
            			 if($scope.benefitType =='1'){
            				 $scope.deathCover = value;
            				 deathCoverService.setDeathCover($scope.deathCover)            				          				 
            			 } else if($scope.benefitType =='2'){
            				 $scope.tpdCover = value;     
            				 tpdCoverService.setTpdCover($scope.tpdCover)            				 
            			 }else  if($scope.benefitType =='4'){
            				 $scope.ipCover = value;   
            				 ipCoverService.setIpCover( $scope.ipCover)            				
            			 }
            			});
            		
            	});
        	});
        });
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	$scope.tokenid = tokenNumService.getTokenId();
        $scope.openWindow = function(url) {
            $window.open(url);
        }
    }]); 
  /*Login Page Controller Ends*/  
   
  /*Landing Page Controller Starts*/    
    scotchApp.controller('landing',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      //Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
      	$scope.isEmpty = function(value){
      		return ((value == "" || value == null) || value == "0");
      	};
    }]); 
    /*Landing Page Controller Ends*/      
     
   /*Retrieve Page controller Starts*/
      scotchApp.controller('retrievesavedapplication',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        $scope.go = function ( path ) {
      	  $location.path( path );
      	};
      	
      	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
      	
    }]);
   /*Retrieve Page controller Ends*/
    
    
    
    /* Change Cover Controller,Progressive and Mandatory validations Starts  */
    scotchApp.controller('quote',['$scope', '$location', '$timeout', 'QuoteService', 'OccupationService','getclientData', 
                                  'deathCoverService','tpdCoverService','ipCoverService', 'CalculateService', 'NewOccupationService', 'MaxLimitService','APP_CONSTANTS',  
                                  function($scope, $location, $timeout, QuoteService, OccupationService, getclientData, deathCoverService,tpdCoverService,
                                		  ipCoverService, CalculateService, NewOccupationService, MaxLimitService, APP_CONSTANTS){
        $scope.message = 'Look! I am an about page.';
        $scope.phoneNumbr = '^0[1-8][0-9]{8}'; 
        $scope.emailFormat = APP_CONSTANTS.emailFormat; 
        QuoteService.getList({}, function(res){
        	$scope.IndustryOptions = res;
        });
        
      //Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
      	$scope.isEmpty = function(value){
      		return ((value == "" || value == null) || value == "0");
      	};
      	
      	var DCMaxAmount, TPDMaxAmount, IPMaxAmount;
      	getclientData.requestObj().then(function(response) {    	
        	$scope.partnerRequest = response.data;
        	$scope.applicantlist = $scope.partnerRequest.applicant;         	
        	angular.forEach($scope.applicantlist, function (applicant) {        		
        		$scope.personalDetails = applicant.personalDetails 
        		if(applicant.personalDetails.gender==null || applicant.personalDetails.gender==""){
        			$scope.genderFlag =  false;
    			}else{
    				$scope.genderFlag =  true;
    			}
        	});
        	var memberType = response.data.applicant[0].memberType;
        	MaxLimitService.getMaxLimits({fundCode:"CARE", memberType: memberType, manageType:"CCOVER"}, function(res){
            	var limits = res;
            	DCMaxAmount = limits[0].deathMaxAmount;
            	TPDMaxAmount = limits[0].tpdMaxAmount;
            	IPMaxAmount = limits[0].ipMaxAmount;
            }, function(error){
            	console.log('Something went wrong while fetching limits ' + error);
            });
        });
      	
      	$scope.checkLimits = function(){
        	if(parseInt($scope.requireCover) > parseInt(DCMaxAmount)){
        		$scope.deathMaxFlag = true;
        	}
        	$scope.calculateOnChange();
        };
        
        $scope.getOccupations = function(){
        	/*alert($scope.industry);*/
        	if(!$scope.industry){
        		$scope.industry = '';
        	}
        	OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.industry}, function(occupationList){
            	$scope.OccupationList = occupationList;
            	$scope.occupation = "";
            	$scope.calculateOnChange();
            });
        };
        
        $scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
        
        $scope.syncRadios = function(val){
        	$scope.requireCover = "";
        	$scope.TPDRequireCover = "";
        	var radios = $('label[radio-sync]');
        	var data = $('input[data-sync]');
    		data.filter('[data-sync="' + val + '"]').attr('checked','checked');
    		if(val == 'Fixed'){
    			showhide('dollar1','nodollar1');
    			$scope.coverType = 'DcFixed';
    			$scope.tpdCoverType = 'TPDFixed';
    		} else if(val == 'Unitised'){
    			showhide('nodollar1','dollar1');
    			$scope.coverType = 'DcUnitised';
    			$scope.tpdCoverType = 'TPDUnitised';
    		}
    		radios.removeClass('active');
    		radios.filter('[radio-sync="' + val + '"]').addClass('active');
    		$scope.calculateOnChange();
        };
        
        

         	$scope.deathCoverDetails = deathCoverService.getDeathCover();
			$scope.tpdCoverDetails = tpdCoverService.getTpdCover();
			$scope.ipCoverDetails = ipCoverService.getIpCover();

							if ($scope.deathCoverDetails
									&& $scope.deathCoverDetails.type) {
								if($scope.deathCoverDetails.type == "1"){
									$scope.coverType = "DcUnitised";
								} else if($scope.deathCoverDetails.type == "2"){
									$scope.coverType = "DcFixed";
								}
							}

							if ($scope.tpdCoverDetails
									&& $scope.tpdCoverDetails.type) {
								if($scope.tpdCoverDetails.type == "1"){
									$scope.tpdCoverType = "TPDUnitised";
								} else if($scope.tpdCoverDetails.type == "2"){
									$scope.tpdCoverType = "TPDFixed";
								}
							}
							
							if($scope.ipCoverDetails && $scope.ipCoverDetails.type){
								 $scope.IPRequireCover = $scope.ipCoverDetails.units;
							}
							
							if($scope.ipCoverDetails && $scope.ipCoverDetails.waitingPeriod && $scope.ipCoverDetails.waitingPeriod != ''){
 								$scope.waitingPeriodAddnl = $scope.ipCoverDetails.waitingPeriod;
							} else {
								$scope.waitingPeriodAddnl = '30 Days';
 							}
							
							if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitPeriod && $scope.ipCoverDetails.benefitPeriod != ''){
 								$scope.benefitPeriodAddnl = $scope.ipCoverDetails.benefitPeriod;
							} else{
								$scope.benefitPeriodAddnl = '2 Years';
 							}
							
							$scope.isDCCoverRequiredDisabled = false;
							$scope.isDCCoverTypeDisabled = false;
							$scope.isTPDCoverRequiredDisabled = true;
							$scope.isTPDCoverTypeDisabled = true;
							$scope.isTPDCoverNameDisabled = false;
							$scope.isWaitingPeriodDisabled = true;
							$scope.isBenefitPeriodDisabled = true;
							$scope.isIPCoverRequiredDisabled = true;
							$scope.isIPCoverNameDisabled = true;
							$scope.isIpSalaryCheckboxDisabled = true;
							$scope.checkDeathOption = function () {
								if($scope.coverName == 'Cancel your cover'){
									$scope.isDCCoverRequiredDisabled = true;
									$scope.isDCCoverTypeDisabled = true;
									$scope.isTPDCoverRequiredDisabled = true;
									$scope.isTPDCoverTypeDisabled = true;
									$scope.isTPDCoverNameDisabled = true;
									$scope.tpdCoverName = 'Cancel your cover';
									$scope.requireCover = '';
								} else if($scope.coverName == 'Increase your cover' || $scope.coverName == 'Decrease your cover'){
									$scope.isDCCoverRequiredDisabled = false;
									$scope.isDCCoverTypeDisabled = false;
									if($scope.isTPDCoverRequiredDisabled != true){
										$scope.isTPDCoverRequiredDisabled = false;
									}
									if($scope.isTPDCoverTypeDisabled != true){
										$scope.isTPDCoverTypeDisabled = false;
									}
									$scope.isTPDCoverNameDisabled = false;
									$scope.requireCover = '';
								} else if($scope.coverName == 'No change'){
									$scope.isDCCoverRequiredDisabled = true;
									$scope.isDCCoverTypeDisabled = true;
									if($scope.isTPDCoverRequiredDisabled != true){
										$scope.isTPDCoverRequiredDisabled = false;
									}
									if($scope.isTPDCoverTypeDisabled != false){
										$scope.isTPDCoverTypeDisabled = true;
									}
									if($scope.isTPDCoverNameDisabled != true){
										$scope.isTPDCoverNameDisabled = false;
									}
									
									if($scope.coverType == "DcUnitised"){
										$scope.requireCover = $scope.deathCoverDetails.units;
									} else if($scope.coverType == "DcFixed"){
										$scope.requireCover = $scope.deathCoverDetails.amount;
									}
 								}
								$scope.calculateOnChange();
					        }
							$scope.checkTPDOption = function () {
								if($scope.tpdCoverName == 'Cancel your cover'){
									if($scope.isDCCoverRequiredDisabled != true){
										$scope.isDCCoverRequiredDisabled = false;
									}
									if($scope.isDCCoverTypeDisabled != true){
										$scope.isDCCoverTypeDisabled = false;
									}
									$scope.isTPDCoverRequiredDisabled = true;
									$scope.isTPDCoverTypeDisabled = true;
									$scope.isTPDCoverNameDisabled = false;
									$scope.TPDRequireCover = '';
								} else if($scope.tpdCoverName == 'Increase your cover' || $scope.tpdCoverName == 'Decrease your cover'){
									if($scope.isDCCoverRequiredDisabled != true){
										$scope.isDCCoverRequiredDisabled = false;
									}
									if($scope.isDCCoverTypeDisabled != true){
										$scope.isDCCoverTypeDisabled = false;
									}
									$scope.isTPDCoverRequiredDisabled = false;
									$scope.isTPDCoverTypeDisabled = false;
									$scope.isTPDCoverNameDisabled = false;
									$scope.TPDRequireCover = '';
								} else if($scope.tpdCoverName == 'No change'){
									if($scope.isDCCoverRequiredDisabled != true){
										$scope.isDCCoverRequiredDisabled = false;
									}
									if($scope.isDCCoverTypeDisabled != true){
										$scope.isDCCoverTypeDisabled = false;
									}
									$scope.isTPDCoverRequiredDisabled = true;
									$scope.isTPDCoverTypeDisabled = true;
									$scope.isTPDCoverNameDisabled = false;
									
									if($scope.tpdCoverType == "TPDUnitised"){
										$scope.TPDRequireCover = $scope.tpdCoverDetails.units;
									} else if($scope.tpdCoverType == "TPDFixed"){
										$scope.TPDRequireCover = $scope.tpdCoverDetails.amount;
									}
								} else if($scope.tpdCoverName == 'Same as Death Cover'){
									if($scope.isDCCoverRequiredDisabled != true){
										$scope.isDCCoverRequiredDisabled = false;
									}
									if($scope.isDCCoverTypeDisabled != true){
										$scope.isDCCoverTypeDisabled = false;
									}
									$scope.isTPDCoverRequiredDisabled = true;
									$scope.isTPDCoverTypeDisabled = true;
									$scope.isTPDCoverNameDisabled = false;
									$scope.TPDRequireCover = $scope.requireCover;
 								}
					        }
							
							$scope.checkIPOption = function () {
								if($scope.ipCoverName == 'Cancel your cover'){
									$scope.isWaitingPeriodDisabled = true;
									$scope.isBenefitPeriodDisabled = true;
									$scope.IPRequireCover = "";
									$scope.isIPCoverRequiredDisabled = true;
									$scope.isIPCoverNameDisabled = false;
									$('#ipsalarycheck').removeAttr('checked');
									$('#ipsalarychecklabel').removeClass('active');
								} else if($scope.ipCoverName == 'Increase your cover'){
									$scope.isWaitingPeriodDisabled = false;
									$scope.isBenefitPeriodDisabled = false;
									$scope.isIPCoverRequiredDisabled = false;
									$scope.isIpSalaryCheckboxDisabled = false;
									$scope.isIPCoverNameDisabled = false;
									$scope.IPRequireCover = '';
								} else if($scope.ipCoverName == 'Decrease your cover'){
									$scope.isWaitingPeriodDisabled = false;
									$scope.isBenefitPeriodDisabled = false;
									$scope.isIPCoverRequiredDisabled = false;
									$scope.isIpSalaryCheckboxDisabled = true;
									$scope.isIPCoverNameDisabled = false;
									$scope.IPRequireCover = '';
									$('#ipsalarycheck').removeAttr('checked');
									$('#ipsalarychecklabel').removeClass('active');
								} else if($scope.ipCoverName == 'No change'){
									$scope.isWaitingPeriodDisabled = true;
									$scope.isBenefitPeriodDisabled = true;
									$scope.IPRequireCover = $scope.ipCoverDetails.units;
									$scope.isIPCoverRequiredDisabled = true;
									$scope.isIpSalaryCheckboxDisabled = true;
									$scope.isIPCoverNameDisabled = false;
									$('#ipsalarycheck').removeAttr('checked');
									$('#ipsalarychecklabel').removeClass('active');
 								} else if($scope.ipCoverName == 'Change Waiting and Benifit Period'){
 									$scope.isWaitingPeriodDisabled = false;
									$scope.isBenefitPeriodDisabled = false;
									$scope.IPRequireCover = $scope.ipCoverDetails.units;
									$scope.isIPCoverRequiredDisabled = true;
									$scope.isIpSalaryCheckboxDisabled = true;
									$scope.isIPCoverNameDisabled = false;
									$('#ipsalarycheck').removeAttr('checked');
									$('#ipsalarychecklabel').removeClass('active');
 								}
								$scope.calculateOnChange();
					        }
							
							$scope.checkAnnualSalary = function(){
								if(parseInt($scope.annualSalary) < 16000){
									$scope.ipCoverName = 'No change';
									$scope.isIPCoverNameDisabled = true;
									$scope.isWaitingPeriodDisabled = true;
									$scope.isBenefitPeriodDisabled = true;
									$scope.IPRequireCover = $scope.ipCoverDetails.units;
									$scope.isIPCoverRequiredDisabled = true;
									$scope.isIpSalaryCheckboxDisabled = true;
									$('#ipsalarycheck').removeAttr('checked');
									$('#ipsalarychecklabel').removeClass('active');
								} else if(parseInt($scope.annualSalary) > 16000){
									if($scope.ipCoverName == 'No change'){
										$scope.ipCoverName = '';
									}
									$scope.isIPCoverNameDisabled = false;
								}
								$scope.setCategory();
							}
			 
        $scope.go = function ( path ) {
        // claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	$scope.toggleIPCondition = function(){
      		$timeout(function(){
      			if($('#ipsalarycheck').prop('checked') == true){
    	      		if(parseInt($scope.annualSalary) < 423530){
    	      			$scope.IPRequireCover  = Math.ceil((0.85 * ($scope.annualSalary/12)) / 425);
    	      		} else if(parseInt($scope.annualSalary) > 423530 && parseInt($scope.annualSalary) < 623530){
    	      			$scope.IPRequireCover = Math.ceil((Math.round(((parseInt($scope.annualSalary) - 423530)/12)*0.6) + 30000)/425);
    	      		} else if(parseInt($scope.annualSalary) > 623530){
    	      			$scope.IPRequireCover = 95;
    	      		}
    	      		$scope.isIPCoverRequiredDisabled = true;
          		} else if($('#ipsalarycheck').prop('checked') == false){
          			$scope.IPRequireCover = '';
          		}
      			$scope.checkAnnualSalary();
      		}, 10);
      		//$scope.checkAnnualSalary();
      	};
      	$scope.continueToNextPage = function(){
      		$scope.getNewOccupation();
      		$location.path('/aura');
      	};
      	var occupationCategory;
      	$scope.getNewOccupation = function(){
      		var name = $scope.industry + ":" + $scope.occupation;
      		NewOccupationService.getOccupation({fundId:"CARE", occName:name},function(res){
      			occupationCategory = res;
      			$scope.deathOccupationCategory = occupationCategory[0].deathfixedcategeory;
      			$scope.tpdOccupationCategory = occupationCategory[0].tpdfixedcategeory;
      			$scope.ipOccupationCategory = occupationCategory[0].ipfixedcategeory;
      			
      			if($scope.deathOccupationCategory != "Professional" && $scope.withinOfficeQuestion == "Yes"){
      				$scope.deathOccupationCategory = "Office";
      				if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
      					$scope.deathOccupationCategory = "Professional";
      				}
      			}
      			if($scope.tpdOccupationCategory != "Professional" && $scope.withinOfficeQuestion == "Yes"){
      				$scope.tpdOccupationCategory = "Office";
      				if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
      					$scope.tpdOccupationCategory = "Professional";
      				}
      			}
      			if($scope.ipOccupationCategory != "Professional" && $scope.withinOfficeQuestion == "Yes"){
      				$scope.ipOccupationCategory = "Office";
      				if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
      					$scope.ipOccupationCategory = "Professional";
      				}
      			}
      			$scope.calculateOnChange();
      		}, function(e){
      			console.log(e);
      		});
      	};
      	
      	$scope.setCategory = function(){
      		var deathCat, tpdCat, ipCat;
      		
      		if(occupationCategory){
      			deathCat = occupationCategory[0].deathfixedcategeory;
      			tpdCat = occupationCategory[0].tpdfixedcategeory;
      			ipCat = occupationCategory[0].ipfixedcategeory;
      		}
      		
      		if(deathCat != "Professional" && $scope.withinOfficeQuestion == "Yes"){
  				$scope.deathOccupationCategory = "Office";
  				if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
  					$scope.deathOccupationCategory = "Professional";
  				}
  			}
  			if(tpdCat != "Professional" && $scope.withinOfficeQuestion == "Yes"){
  				$scope.tpdOccupationCategory = "Office";
  				if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
  					$scope.tpdOccupationCategory = "Professional";
  				}
  			}
  			if(ipCat != "Professional" && $scope.withinOfficeQuestion == "Yes"){
  				$scope.ipOccupationCategory = "Office";
  				if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
  					$scope.ipOccupationCategory = "Professional";
  				}
  			}
      		$scope.calculateOnChange();
      	};
      	
      	$scope.coltwo = false;
      	$scope.toggleTwo = function() {
            $scope.coltwo = !$scope.coltwo;
            $("a[data-target='#collapseTwo']").click();
        };
      	$scope.colthree = false;
      	$scope.toggleThree = function() {
            $scope.colthree = !$scope.colthree;
            $("a[data-target='#collapseThree']").click()
        };
           
        $scope.DeathCoverOptionsOne = [{
        	key: 'option1',
        	coverOptionName: 'Increase your cover'
        }, {
        	key: 'option2',
        	coverOptionName: 'Decrease your cover'
        },
        {
        	key: 'option3',
        	coverOptionName: 'Cancel your cover'
        },
        {
        	key: 'option4',
        	coverOptionName: 'Convert and maintain cover'
        },
        {
        	key: 'option5',
        	coverOptionName: 'No change'
        }
        ];
        
        $scope.TPDCoverOptionsOne = [{
        	key: 'option1',
        	coverOptionName: 'Increase your cover'
        }, {
        	key: 'option2',
        	coverOptionName: 'Decrease your cover'
        },
        {
        	key: 'option3',
        	coverOptionName: 'Cancel your cover'
        },
        {
        	key: 'option4',
        	coverOptionName: 'Convert and maintain cover'
        },
        {
        	key: 'option5',
        	coverOptionName: 'No change'
        },
        {
        	key: 'option6',
        	coverOptionName: 'Same as Death Cover'
        }
        ];
        
        $scope.IPCoverOptionsOne = [{
        	key: 'option1',
        	coverOptionName: 'Increase your cover'
        }, {
        	key: 'option2',
        	coverOptionName: 'Decrease your cover'
        },
        {
        	key: 'option3',
        	coverOptionName: 'Cancel your cover'
        },
        {
        	key: 'option5',
        	coverOptionName: 'No change'
        },
        {
        	key: 'option6',
        	coverOptionName: 'Change Waiting and Benifit Period'
        }
        ];
        
        $scope.coverOptionsTwo = [{
        	key: 'option1',
        	coverOptionName: 'Increase your cover'
        }, 
        {
        	key: 'option2',
        	coverOptionName: 'No change'
        }
        ];
        
       
        $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
        $scope.benefitPeriodOptions = ['2 Years', '5 Years'];
        
        $scope.regex = /[0-9]{1,3}/;
        $scope.dcCoverAmount = 0.00;
    	$scope.dcWeeklyCost = 0.00;
    	$scope.tpdCoverAmount = 0.00;
    	$scope.tpdWeeklyCost = 0.00;
    	$scope.ipCoverAmount = 0.00;
    	$scope.ipWeeklyCost = 0.00;
    	$scope.totalWeeklyCost = 0.00;
    	var dynamicFlag = false;
        $scope.calculate = function(){
        	var ruleModel = {
            		"age": 20,
            		"fundCode": "CARE",
            		"gender": "Male",
            		"deathOccCategory": $scope.deathOccupationCategory,
            		"tpdOccCategory": $scope.tpdOccupationCategory,
            		"ipOccCategory": $scope.ipOccupationCategory,
            		"smoker": false,
            		"deathUnits": parseInt($scope.requireCover),
            		"deathFixedAmount": parseInt($scope.requireCover),
            		"deathFixedCost": null,
            		"deathUnitsCost": null,
            		"tpdUnits": parseInt($scope.TPDRequireCover),
            		"tpdFixedAmount": parseInt($scope.TPDRequireCover),
            		"tpdFixedCost": null,
            		"tpdUnitsCost": null,
            		"ipUnits": parseInt($scope.IPRequireCover),
            		"ipFixedAmount": parseInt($scope.IPRequireCover),
            		"ipFixedCost": null,
            		"ipUnitsCost": null,
            		"premiumFrequency": "Weekly",
            		"memberType": null,
            		"manageType": "CCOVER",
            		"deathCoverType": $scope.coverType,
            		"tpdCoverType": $scope.tpdCoverType,
            		"ipCoverType": "IpUnitised",
            		"ipWaitingPeriod": $scope.waitingPeriodAddnl,
            		"ipBenefitPeriod": $scope.benefitPeriodAddnl
            	};
        	CalculateService.calculate({}, ruleModel, function(res){
        		var premium = res;
        		dynamicFlag = true;
        		for(var i = 0; i < premium.length; i++){
        			if(res[i].coverType == 'DcFixed' || res[i].coverType == 'DcUnitised'){
        				$scope.dcCoverAmount = res[i].coverAmount;
        				$scope.dcWeeklyCost = res[i].cost;
        			} else if(res[i].coverType == 'TPDFixed' || res[i].coverType == 'TPDUnitised'){
        				$scope.tpdCoverAmount = res[i].coverAmount;
        				$scope.tpdWeeklyCost = res[i].cost;
        			} else if(res[i].coverType == 'IpFixed' || res[i].coverType == 'IpUnitised'){
        				$scope.ipCoverAmount = res[i].coverAmount;
        				$scope.ipWeeklyCost = res[i].cost;
        			}
        		}
        		$scope.totalWeeklyCost = $scope.dcWeeklyCost+ $scope.tpdWeeklyCost+$scope.ipWeeklyCost;
        	});
        };
        
       $scope.calculateOnChange = function(){
    	   if(dynamicFlag){
    		   $scope.calculate();
    	   }
       };
	    
  
	  
	  // Progressive validation
	    var FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime'];
	    if($scope.genderFlag == false){
	    	var OccupationFormFields = ['gender','fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','managementRole','annualSalary'];
		    var OccupationOtherFormFields = ['gender','fifteenHrsQuestion','industry','occupation', 'otherOccupation', 'withinOfficeQuestion','tertiaryQuestion','managementRole','annualSalary'];	    
	    }else{
	    	var OccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','managementRole','annualSalary'];
		    var OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation', 'otherOccupation', 'withinOfficeQuestion','tertiaryQuestion','managementRole','annualSalary'];
		    	
	    }
	    //var OccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','managementRole','annualSalary'];
	    //var OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation', 'otherOccupation', 'withinOfficeQuestion','tertiaryQuestion','managementRole','annualSalary'];
	    var CoverCalculatorFormFields =['coverName','coverType','requireCover','tpdCoverName','tpdCoverType','TPDRequireCover','ipCoverName','IPRequireCover'];

	    $scope.checkPreviousMandatoryFields  = function (elementName,formName){
	    	var formFields;
	    	if(formName == 'formOne'){
	    		formFields = FormOneFields;
	    	} else if(formName == 'occupationForm'){
	    		if($scope.occupation != undefined && $scope.occupation == 'Other'){
	    			formFields = OccupationOtherFormFields;
	    		} else{
	    			formFields = OccupationFormFields;
	    		}
	    	} else if(formName == 'coverCalculatorForm'){
	    		formFields = CoverCalculatorFormFields;
	    	}
	      var inx = formFields.indexOf(elementName);
	     // console.log(elementName, inx);
	      if(inx > 0){
	        for(var i = 0; i < inx ; i++){
	          $scope[formName][formFields[i]].$touched = true;
	        }
	      }
	    };
      
	   // validate fields "on continue"
	    $scope.formOneSubmit =  function (form){
	      if(!form.$valid){
	    	 /* alert("invalid>>"+$scope["formOne"].$invalid);*/
	    	  form.$submitted=true; 
    	  } else{
    		  if(form.$name == 'formOne')
    			  {
	    	    $scope.toggleTwo();
    			  }
    		  else if(form.$name == 'occupationForm')
    			  {
    			  $scope.toggleThree();
    			  }
	       }
	     // console.log("Form Validation");
	    };
	    $('.selectpicker').selectpicker();
  }]); 
  
 /* Change Cover Controller,Progressive and Mandatory validations Ends  */ 
 
    /*New Member Change Cover Controller ,Progressive and Mandatory Validations Starts   */
        scotchApp.controller('newmemberquote',['$scope', '$location', 'QuoteService', 'OccupationService', 'deathCoverService','tpdCoverService','ipCoverService', 'CalculateService', 'NewOccupationService', 'APP_CONSTANTS', function($scope, $location, QuoteService, OccupationService, deathCoverService,tpdCoverService,ipCoverService, CalculateService, NewOccupationService, APP_CONSTANTS){
            $scope.message = 'Look! I am an about page.';
            $scope.emailFormat = APP_CONSTANTS.emailFormat;
            $scope.phoneNo = /^0[1-8][0-9]{8}$/ ;
            
            $scope.coltwo = false;
          	$scope.toggleTwo = function() {
                $scope.coltwo = !$scope.coltwo;
                $("a[data-target='#collapseTwo']").click();
            };
          	$scope.colthree = false;
          	$scope.toggleThree = function() {
                $scope.colthree = !$scope.colthree;
                $("a[data-target='#collapseThree']").click()
            };
            
            QuoteService.getList({}, function(res){
            	$scope.IndustryOptions = res;
            });
                                    $scope.isDeathDisabled = false;
                                    $scope.isTPDDisabled = false;
                                    $scope.isIPDisabled = false;
            $scope.collapseSection = function(){
            	            	if($scope.tpdClaim == "Yes"){
            	           		    $('#tpdsection').removeClass('active');
            	            		$('#ipsection').removeClass('active');
            	           		    toggle('tpd');
            	            		toggle('sc');
            	            		$scope.isDeathDisabled = false;
            	            		$scope.isTPDDisabled = true;
            	            		$scope.isIPDisabled = true;
            	            		if($scope.terminalIllnessClaim == "Yes"){
                	            		$('#deathsection').removeClass('active');
                	            		$('#tpdsection').removeClass('active');
                	           		    $('#ipsection').removeClass('active');
                	            		toggle('death');
                	            		toggle('tpd');
                	            		toggle('sc');
                	            		$scope.isDeathDisabled = true;
                	            		$scope.isTPDDisabled = true;
                	            		$scope.isIPDisabled = true;
                	            	}
            	           	}  if($scope.tpdClaim == "No" ){
            	            		$('#deathsection').addClass('active');
            	            		$('#tpdsection').addClass('active');
            	            		$('#ipsection').addClass('active');
            	            		toggle('death');
            	            		toggle('tpd');
            	            		toggle('sc');
            	            		$scope.isDeathDisabled = false;
            	            		$scope.isTPDDisabled = false;
            	            		$scope.isIPDisabled = false;
            	            		if( $scope.terminalIllnessClaim == 'No'){
            	            			$('#deathsection').addClass('active');
                	            		$('#tpdsection').addClass('active');
                	            		$('#ipsection').addClass('active');
                	            		toggle('death');
                	            		toggle('tpd');
                	            		toggle('sc');
                	            		$scope.isDeathDisabled = false;
                	            		$scope.isTPDDisabled = false;
                	            		$scope.isIPDisabled = false;
            	            		}
            	            	}
            	            	
            	            };
            	            
            	            $scope.checkIPCover = function(){
            	            	            	            	          	
            	            	         	if(parseInt($scope.newAnnualSalary) < 160000 || $scope.workTimeAndSalary == "No"){
            	            	            	          $('#ipsection').removeClass('active');
            	            	            	           toggle('sc');
            	            	            	           $scope.isIPDisabled = true;
            	            	            	           }else if(parseInt($scope.newAnnualSalary) > 16000 || parseInt($scope.newAnnualSalary) == 16000 || $scope.workTimeAndSalary == "Yes" ) 	{
            	            	            	            $('#ipsection').addClass('active');
            	            	            	            toggle('sc');
            	            	            	            $scope.isIPDisabled = false;
            	            	           	            	 }	
            	            	            	  };  
            $scope.navigateToLandingPage = function (){
            	if(window.confirm('Are you sure you want to navigate to Home Page?')){
            		$location.path("/landing");
            	}
            }
            
            $scope.getOccupations = function(){
            	if(!$scope.newIndustry){
            		$scope.newIndustry = '';
            	}
            	OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.newIndustry}, function(occupationList){
                	$scope.OccupationList = occupationList;
                	$scope.newOccupation = "";
               });
            };
            
            $scope.newMemberDeathCoverDetails = deathCoverService.getDeathCover();
    		$scope.newMemberTpdCoverDetails = tpdCoverService.getTpdCover();
    		$scope.newMemberIpCoverDetails = ipCoverService.getIpCover();
    		
    		if ($scope.newMemberDeathCoverDetails
					&& $scope.newMemberDeathCoverDetails.type) {
				if($scope.newMemberDeathCoverDetails.type == "1"){
					$scope.newDeathcoverType = "DcUnitised";
				} else if($scope.newMemberDeathCoverDetails.type == "2"){
					$scope.newDeathcoverType = "DcFixed";
				}
			}

			if ($scope.newMemberTpdCoverDetails
					&& $scope.newMemberTpdCoverDetails.type) {
				if($scope.newMemberTpdCoverDetails.type == "1"){
					$scope.newTpdCoverType = "TPDUnitised";
				} else if($scope.newMemberTpdCoverDetails.type == "2"){
					$scope.newTpdCoverType = "TPDFixed";
				}
			}
			
			if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.type){
				 $scope.newIPRequireCover = $scope.newMemberIpCoverDetails.units;
			}
			
			if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.waitingPeriod && $scope.newMemberIpCoverDetails.waitingPeriod != ''){
					$scope.waitingPeriodAddnl = $scope.newMemberIpCoverDetails.waitingPeriod;
			} else {
				$scope.waitingPeriodAddnl = '30 Days';
				}
			
			if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.benefitPeriod && $scope.newMemberIpCoverDetails.benefitPeriod != ''){
					$scope.benefitPeriodAddnl = $scope.newMemberIpCoverDetails.benefitPeriod;
			} else{
				$scope.benefitPeriodAddnl = '2 Years';
				}
    		
    		$scope.newWaitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
            $scope.newBenefitPeriodOptions = ['2 Years', '5 Years'];
            $scope.dcNewCoverAmount = 0.00;
        	$scope.dcNewWeeklyCost = 0.00;
        	$scope.tpdNewCoverAmount = 0.00;
        	$scope.tpdNewWeeklyCost = 0.00;
        	$scope.ipNewCoverAmount = 0.00;
        	$scope.ipNewWeeklyCost = 0.00;
        	$scope.totalNewWeeklyCost = 0.00;
        	
        	$scope.calculateQuote = function(){
        		            	var ruleModel = {
        		                 		"age": 20,
        		                 		"fundCode": "CARE",
        		                 		"gender": "Male",
        		                 		"deathOccCategory": "Professional",
        		                 		"tpdOccCategory": "Professional",
        		                 		"ipOccCategory": "Professional",
        		                 		"smoker": false,
        		                 		"deathUnits": parseInt($scope.newDeathrequireCover),
        		                 		"deathFixedAmount": parseInt($scope.newDeathrequireCover),
        		                 		"deathFixedCost": null,
        		                		"deathUnitsCost": null,
        		                 		"tpdUnits": parseInt($scope.newTPDRequireCover),
        		                 		"tpdFixedAmount": parseInt($scope.newTPDRequireCover),
        		                 		"tpdFixedCost": null,
        		                 		"tpdUnitsCost": null,
        		                 		"ipUnits": parseInt($scope.newIPRequireCover),
        		                 		"ipFixedAmount": parseInt($scope.newIPRequireCover),
        		                		"ipFixedCost": null,
        		                 		"ipUnitsCost": null,
        		                 		"premiumFrequency": "Weekly",
        		                 		"memberType": null,
        		                 		"manageType": "CCOVER",
        		                 		"deathCoverType": $scope.newDeathcoverType,
        		                 		"tpdCoverType": $scope.newTpdCoverType,
        		                 		"ipCoverType": "IpUnitised",
        		                 		"ipWaitingPeriod": $scope.waitingPeriodAddnl,
        		                 		"ipBenefitPeriod": $scope.benefitPeriodAddnl
        		                 	};
        		            	CalculateService.calculate({}, ruleModel, function(res){
        		             		var newPremium = res;
        		             		for(var i = 0; i < newPremium.length; i++){
        		             			if(res[i].coverType == 'DcFixed' || res[i].coverType == 'DcUnitised'){
        		             				$scope.dcNewCoverAmount = res[i].coverAmount;
        		             				$scope.dcNewWeeklyCost = res[i].cost;
        		             			} else if(res[i].coverType == 'TPDFixed' || res[i].coverType == 'TPDUnitised'){
        		             				$scope.tpdNewCoverAmount = res[i].coverAmount;
        		            				$scope.tpdNewWeeklyCost = res[i].cost;
        		            				} else if(res[i].coverType == 'IpFixed' || res[i].coverType == 'IpUnitised'){
        		             				$scope.ipNewCoverAmount = res[i].coverAmount;
        		             				$scope.ipNewWeeklyCost = res[i].cost;
        		             			}
        		             		}
        		             		$scope.totalNewWeeklyCost = $scope.dcNewWeeklyCost+ $scope.tpdNewWeeklyCost+$scope.ipNewWeeklyCost;
        		             	});
        		             };
    
            // Progressive validation
    	    var newMemberContactFormFields = ['newContactEmail', 'newContactPhone','newContactPrefTime'];
    	    var newMemberOccupationFormFields = ['workTimeAndSalary','newIndustry','newOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary','tpdClaim','terminalIllnessClaim'];
    	    var newMemberOccupationOtherFormFields = ['workTimeAndSalary','newIndustry','newOccupation','newOtherOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary','tpdClaim','terminalIllnessClaim'];
    	    var newMembercoverCalculatorFormFields =['newDeathcoverType','newDeathrequireCover','newTpdCoverType','newTPDRequireCover','newIPRequireCover'];
    
    	    $scope.checkNewMemberPreviousMandatoryFields  = function (elementName,formName){
    	    	var newFormFields;
    	    	if(formName == 'newMemberContactForm'){
    	    		newFormFields = newMemberContactFormFields;
    	    	} else if(formName == 'newMemberOccupationForm'){
    	    		if($scope.newOccupation != undefined && $scope.newOccupation == 'Other'){
    	    			newFormFields = newMemberOccupationOtherFormFields;
    	    		} else{
    	    			newFormFields = newMemberOccupationFormFields;
    	    		}
    	    	} else if(formName == 'newMembercoverCalculatorForm'){
    	    		newFormFields = newMembercoverCalculatorFormFields;
    	    	}
    	      var inx = newFormFields.indexOf(elementName);
    	    	      if(inx > 0){
    	        for(var i = 0; i < inx ; i++){
    	          $scope[formName][newFormFields[i]].$touched = true;
    	        }
    	      }
    	    };
          
    	   // validate fields "on continue"
   	    $scope.newMemberFormSubmit =  function (form){
    	      if(!form.$valid){
    	    	  form.$submitted=true; 
        	  } else{
        		  if(form.$name == 'newMemberContactForm')
        			  {
    	    	    $scope.toggleTwo();
        			  }
        		  else if(form.$name == 'newMemberOccupationForm')
        			  {
        			  $scope.toggleThree();
        			  }
    	       }
    	    
    	    };
    	    $('.selectpicker').selectpicker();
            
            
        }]); 
     
        
     /*New Member Change Cover Controller ,Progressive and Mandatory Validations Ends   */
           
          
 
 /* Transfer Cover Controller,Progressive and Mandatory validations Starts  */
    scotchApp.controller('quotetransfer',['$scope', '$location','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'CalculateService', 'TransferCalculateService', 'APP_CONSTANTS', function($scope, $location,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService,CalculateService, TransferCalculateService, APP_CONSTANTS){
        $scope.message = 'Look! I am an about page.';
        $scope.phoneNumbrTrans = '^0[1-8][0-9]{8}'; 
        $scope.emailFormatTrans = APP_CONSTANTS.emailFormat;
        QuoteService.getList({}, function(res){
        	$scope.IndustryOptions = res;
        });
        
        //Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
      	$scope.isEmpty = function(value){
      		return ((value == "" || value == null) || value == "0");
      	};
        
        $scope.getOccupations = function(){
        	/*alert($scope.transferIndustry);*/
        	OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.transferIndustry}, function(occupationList){
            	$scope.OccupationList = occupationList;
            });
        };
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	
      	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
      	
      	$scope.calculateTransfer = function(){
      		var ruleModel = {
            		"age": 20,
            		"fundCode": "CARE",
            		"gender": "Male",
            		"deathOccCategory": "Professional",
            		"tpdOccCategory": "Professional",
            		"ipOccCategory": "Professional",
            		"manageType": "TCOVER",
            		"deathCoverType": "DcFixed",
            		"tpdCoverType": "TpdFixed",
            		"ipCoverType": "IpUnitised",
            		"premiumFrequency":"Weekly",
            		"ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
            		"ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
            		"deathTransferAmount": parseInt($scope.TransDeathRequireCover),
            		"tpdTransferAmount": parseInt($scope.TransTPDRequireCover),
            		"ipTransferAmount": parseInt($scope.TransIPRequireCover),
            		"deathExistingAmount": parseInt($scope.deathCoverTransDetails.amount),
            		"tpdExistingAmount": parseInt($scope.tpdCoverTransDetails.amount),
            		"ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
            	};
      		
      		TransferCalculateService.calculate({}, ruleModel, function(res){
      			var premium = res;
      			for(var i = 0; i < premium.length; i++){
        			if(res[i].coverType == 'DcFixed'){
        				$scope.dcTransCoverAmount = res[i].coverAmount;
        				$scope.dcTransCost = res[i].cost;
        			} else if(res[i].coverType == 'TpdFixed'){
        				$scope.tpdTransCoverAmount = res[i].coverAmount;
        				$scope.tpdTransCost = res[i].cost;
        			} else if(res[i].coverType == 'IpUnitised'){
        				$scope.ipTransCoverAmount = res[i].coverAmount;
        				$scope.ipTransCost = res[i].cost;
        			}
				}
      			$scope.totalTransCost = parseFloat($scope.dcTransCost) + parseFloat($scope.tpdTransCost) + parseFloat($scope.ipTransCost);
      		});
      	};
      	
      	$scope.coltwo = false;
      	$scope.toggleTwo = function() {
            $scope.coltwo = !$scope.coltwo;  
            $("a[data-target='#collapseTwo']").click();
        };
      	$scope.colthree = false;
      	$scope.toggleThree = function() {
            $scope.colthree = !$scope.colthree;  
            $("a[data-target='#collapseThree']").click();
        };
        $scope.colfour = false;
      	$scope.toggleFour = function() {
            $scope.colfour = !$scope.colfour; 
            $("a[data-target='#collapseFour']").click();
        };
        
        var coverDetailsTransferFormFields = ['coverDetailsTransferEmail', 'coverDetailsTransferPhone','coverDetailsTransferPrefTime'];
        /*var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];*/
	    var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];
	    var occupationDetailsOtherTransferFormFields = ['areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','transferotherOccupation','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];
	    /*var previousCoverFormFields = ['gender','previousFundName','membershipNumber','spinNumber','documentName'];*/
	    var previousCoverFormFields = ['previousFundName','membershipNumber','spinNumber','documentName','tpdClaimTrans','terminalIllnessClaimTrans'];
	    var previousCoverFormFieldsWithChkBox = ['previousFundName','membershipNumber','spinNumber','documentName','ackDocument','tpdClaimTrans','terminalIllnessClaimTrans'];
	    var TranscoverCalculatorFormFields = ['TransDeathRequireCover','TransTPDRequireCover','TransIPRequireCover'];
	    $scope.checkCoverDetailsTransferFormPreviousMandatoryFields  = function (elementName,formName){
	    	var transferFormFields;
	    	if(formName == 'coverDetailsTransferForm'){
	    		transferFormFields = coverDetailsTransferFormFields;
	    	} else if(formName == 'occupationDetailsTransferForm'){
	    		//transferFormFields = occupationDetailsTransferFormFields;
	    		if($scope.occupationTransfer != undefined && $scope.occupationTransfer == 'Other'){
	    			transferFormFields = occupationDetailsOtherTransferFormFields;
	    		} else{
	    			transferFormFields = occupationDetailsTransferFormFields;
	    		}
	    	} else if(formName == 'previousCoverForm'){
	    		if($scope.documentName != undefined && $scope.documentName =='No'){
	    			transferFormFields = previousCoverFormFieldsWithChkBox;
	    		} else{
	    			transferFormFields = previousCoverFormFields;
	    		}
	    		//transferFormFields = previousCoverFormFields;
	    	}  else if(formName == 'TranscoverCalculatorForm'){
	    		transferFormFields = TranscoverCalculatorFormFields;
	    	}
	      var inx = transferFormFields.indexOf(elementName);
	      //console.log(elementName, inx);
	      if(inx > 0){
	        for(var i = 0; i < inx ; i++){
	          $scope[formName][transferFormFields[i]].$touched = true;
	        }
	      }
	    };

	    $scope.CoverDetailsTransferFormSubmit =  function (form){
	      if(!form.$valid){
	    	  form.$submitted=true; 
    	  } else{
    		  if(form.$name == 'coverDetailsTransferForm')
    			  {
	    	    $scope.toggleTwo();
    			  }
    		  else if(form.$name == 'occupationDetailsTransferForm')
    			  {
    			  $scope.toggleThree();
    			  }
    		  else if(form.$name == 'previousCoverForm')
    			  {
    			  $scope.toggleFour();
    			  }
	       }
	      //console.log("Form Validation");
	    };
	    
	  /*  $scope.industryTransferOptions = [{
        	options: 'option1',
        	industryName: 'Industry 1'
        }, {
        	options: 'option2',
        	industryName: 'Industry 2'
        }
        ];*/
        
        $scope.occupationTransferOptions = [{
        	options: 'option1',
        	occupationName: 'Occupation 1'
          }, {
        	  options: 'option2',
        	  occupationName: 'Occupation 2'
          }
        ];
        
       
       // UPLOAD THE FILES.
        $scope.uploadFiles = function () {
            //FILL FormData WITH FILE DETAILS.
            var data = new FormData();

            for (var i in $scope.files) {
                data.append("uploadedFile", $scope.files[i]);
            }

            // ADD LISTENERS.
            var objXhr = new XMLHttpRequest();

            // SEND FILE DETAILS TO THE API.
            //objXhr.open("POST", "/api/fileupload/");
            //objXhr.send(data);
        };   
        
        $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
		$scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
		$scope.ipCoverTransDetails = ipCoverService.getIpCover();
		
		$scope.waitingPeriodTransOptions = ["14 Days", "30 Days", "45 Days", "60 Days", "90 Days", "Not Listed"];
        $scope.benefitPeriodTransOptions = ["2 Years", "3 Years", "5 Years", "Age 60", "Age 65", "Not Listed"];
        
        $scope.waitingPeriodTransAdlnOptions = ["30 Days", "60 Days", "90 Days"];
        $scope.benefitPeriodTransAdlnOptions = ['2 Years', '5 Years'];
        
        $scope.preferredContactTransOptions = ['Mobile','Office','Home'];
        
        $scope.regex = /[0-9]{1,3}/;
        $scope.dcTransCoverAmount = 0.00;
    	$scope.dcTransCost = 0.00;
    	$scope.tpdTransCoverAmount = 0.00;
    	$scope.tpdTransCost = 0.00;
    	$scope.ipTransCoverAmount = 0.00;
    	$scope.ipTransCost = 0.00;
    	$scope.totalTransCost = 0.00;
    	
    	if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.waitingPeriod && $scope.ipCoverTransDetails.waitingPeriod != ''){
    			$scope.waitingPeriodTransPer = $scope.ipCoverTransDetails.waitingPeriod;
				$scope.waitingPeriodTransAddnl = $scope.ipCoverTransDetails.waitingPeriod;
		} else {
			$scope.waitingPeriodTransPer = '30 Days';
			$scope.waitingPeriodTransAddnl = '30 Days';
		}
		
		if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitPeriod && $scope.ipCoverTransDetails.benefitPeriod != ''){
				$scope.benefitPeriodTransPer = $scope.ipCoverTransDetails.benefitPeriod;
				$scope.benefitPeriodTransAddnl = $scope.ipCoverTransDetails.benefitPeriod;
		} else{
			$scope.benefitPeriodTransPer = '2 Years';
			$scope.benefitPeriodTransAddnl = '2 Years';
		}
    	
    	 $scope.changeWaitingPeriod = function() {
    		 if(($scope.waitingPeriodTransPer == '14 Days') || ($scope.waitingPeriodTransPer == '30 Days')){
					$scope.waitingPeriodTransAddnl = '30 Days';
    		 }else  if(($scope.waitingPeriodTransPer == '45 Days') || ($scope.waitingPeriodTransPer == '60 Days')){
					$scope.waitingPeriodTransAddnl = '60 Days';
    		 }else if($scope.waitingPeriodTransPer == '90 Days'){
    			    $scope.waitingPeriodTransAddnl = '90 Days';
    		 }
	      };
	      
	      $scope.changeBenefitPeriod = function() {
	    		 if(($scope.benefitPeriodTransPer == '2 Years') || ($scope.benefitPeriodTransPer == '3 Days')){
						$scope.benefitPeriodTransAddnl = '2 Years';
	    		 }else  if($scope.benefitPeriodTransPer == '5 Years'){
						$scope.benefitPeriodTransAddnl = '5 Years';
	    		 } 
		      };
        
    }]); 
    
 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */
  
   /* Work Rating Controller,Progressive and Mandatory validations Starts  */  
    
    scotchApp.controller('quoteoccupdate',['$scope', '$location','QuoteService','OccupationService','NewOccupationService', 'APP_CONSTANTS', function($scope, $location,QuoteService,OccupationService,NewOccupationService, APP_CONSTANTS){
        $scope.message = 'Look! I am an about page.';
        //$scope.validnumberregEx = '^0[1-8][0-9]{8}';
        //$scope.validAnnualSalaryregEx = '/^[0-9]*$/';
        $scope.phoneNumbrUpdate = '^0[1-8][0-9]{8}'; 
        $scope.emailFormatUpdate = APP_CONSTANTS.emailFormat;
        QuoteService.getList({}, function(res){
        	$scope.IndustryOptions = res;
        });
        
        $scope.getOccupations = function(){
        	if(!$scope.workRatingIndustry){
        		$scope.workRatingIndustry = '';
        	}
        	/*alert($scope.workRatingIndustry);*/
        	OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.workRatingIndustry}, function(occupationList){
            	$scope.OccupationList = occupationList;
            });
        };
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
    	$scope.coltwo = false;
      	$scope.toggleTwo = function() {
            $scope.coltwo = !$scope.coltwo; 
            $("a[data-target='#collapseTwo']").click();
        };
        
        $scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
        
        
       //Progressive validation
        var workRatingFormFields = ['workRatingEmail', 'wrkRatingPrefContactNo','workRatingPrefTime'];
        var workRatingOccupationFormFields = ['workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
        var workRatingOthrOccupationFormFields = ['workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
          $scope.checkPreviousMandatoryFieldsFrWorkRating  = function (workRatingElementName,workRatingFormName){
          	var WorkRatingformFields;
          	if(workRatingFormName == 'workRatingContactForm'){
          		WorkRatingformFields = workRatingFormFields;
        	}else if(workRatingFormName == 'workRatingOccupForm'){
         		//WorkRatingformFields = workRatingOccupationFormFields;
        		if($scope.workRatingoccupation != undefined && $scope.workRatingoccupation == 'Other'){
        			WorkRatingformFields = workRatingOthrOccupationFormFields;
	    		} else{
	    			WorkRatingformFields = workRatingOccupationFormFields;
	    		}
        	}
            var inx = WorkRatingformFields.indexOf(workRatingElementName);
            //console.log(workRatingElementName, inx);
            if(inx > 0){
              for(var i = 0; i < inx ; i++){
                $scope[workRatingFormName][WorkRatingformFields[i]].$touched = true;
              }
            }
          };

       // Validate fields "on continue"
         $scope.workRatingFormSubmit =  function (form){
            if(!form.$valid){
          	//  alert("invalid>>"+$scope["workRatingContactForm"].$invalid);
          	  form.$submitted=true; 
      	    }else{
      		  if(form.$name == 'workRatingContactForm'){
          	    $scope.toggleTwo();
      		  }else if(form.$name == 'workRatingOccupForm'){
      		 //	alert("aura");
      			$scope.continueWorkRatingPage();
      			/*$scope.go('/aura');*/
       		  }
           }
           // console.log("Form Validation");
         };
         
         $scope.continueWorkRatingPage = function(){
	       	 var occupationName = $scope.workRatingIndustry + ":" + $scope.workRatingoccupation;
	     		   NewOccupationService.getOccupation({fundId:"CARE", occName:occupationName},function(result){
		     			var occupationCategory = result;
		     			var deathCat = occupationCategory[0].deathfixedcategeory;
		      			var tpdCat = occupationCategory[0].tpdfixedcategeory;
		      			var ipCat = occupationCategory[0].ipfixedcategeory;
		      			var deathUpgrade = false;
		      			var tpdUpgrade = false;
		      			var ipUpgrade = false;
		      			
		     			if(deathCat != "Professional" && $scope.workRatingWithinOffcQue == "Yes"){
		     				deathCat = "Office";
		     				deathUpgrade = true;
		      				if(parseInt($scope.workRatingAnnualSal) > 100000 && ($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes")){
		      					deathCat = "Professional";
		      				}
		      			}
		      			if(tpdUpgrade != "Professional" && $scope.workRatingWithinOffcQue == "Yes"){
		      				$scope.tpdOccupationCategory = "Office";
		      				tpdUpgrade = true;
		      				if(parseInt($scope.workRatingAnnualSal) > 100000 && ($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes")){
		      					tpdCat = "Professional";
		      				}
		      			}
		      			if(ipCat != "Professional" && $scope.workRatingWithinOffcQue == "Yes"){
		      				$scope.ipOccupationCategory = "Office";
		      				ipUpgrade = true;
		      				if(parseInt($scope.workRatingAnnualSal) > 100000 && ($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes")){
		      					ipCat = "Professional";
		      				}
		      			}
		      			
		      			if(deathUpgrade || tpdUpgrade || ipUpgrade){
		          			$location.path('/aura');
		          		} else{
		          			$location.path('/summary');
		          		}
	        		}, function(e){
	        			console.log(e);
	        		});
    	};
         
        
    }]); 
    
    /* Work Rating Controller,Progressive and Mandatory validations Ends  */

    
/* Cancel cover Controller,Progressive and Mandatory validations Starts  */   
    scotchApp.controller('quotecancel',['$scope', '$location','QuoteService','OccupationService', 'APP_CONSTANTS', function($scope, $location,QuoteService,OccupationService, APP_CONSTANTS){
        $scope.message = 'Look! I am an about page.';
        //$scope.validnumberregEx = '^0[1-8][0-9]{8}';
        //$scope.validAnnualSalaryregEx = '/^[0-9]*$/';
        $scope.phoneNumbrCancel = '^0[1-8][0-9]{8}'; 
        $scope.emailFormatCancel = APP_CONSTANTS.emailFormat;
        QuoteService.getList({}, function(res){
        	$scope.IndustryOptions = res;
        });
        
        $scope.getOccupations = function(){
        	if(!$scope.workRatingIndustry){
        		$scope.workRatingIndustry = '';
        	}
        	
        	OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.workRatingIndustry}, function(occupationList){
            	$scope.OccupationList = occupationList;
            });
        };
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
    	
      	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
        
       //Progressive validation
        var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime'];
         $scope.checkPreviousMandatoryFieldsFrCancelCover  = function (cancelCoverElementName,cancelCoverFormName){
            var inx = cancelCoverFormFields.indexOf(cancelCoverElementName);
            if(inx > 0){
              for(var i = 0; i < inx ; i++){
                $scope[cancelCoverFormName][cancelCoverFormFields[i]].$touched = true;
              }
            }
         };

       // Validate fields "on continue"
         $scope.cancelContactDetailsFormSubmit =  function (form){
            if(!form.$valid){
              //alert("invalid>>"+$scope["cancelContactDetailsForm"].$invalid);
          	  form.$submitted=true; 
      	    }
            /*else{
      	       $scope.go('/summary');
            }*/
           // console.log("Form Validation");
         };
        
    }]); 
    /* Cancel cover Controller,Progressive and Mandatory validations Ends  */ 
    
    scotchApp.filter('split', function() {
            return function(input) {
            	console.log(input)
            	input = input.substring(input.indexOf(']')+1)                
                return input;
            }
        });
    
    /*aura transfer controller*/    
    scotchApp.controller('auratransfer',['$scope', '$location','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService',
                                         function($scope, $location,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService ){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {        	
      	  $location.path( path );
      	};
      	
      	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
      
      	auraInputService.setFund('CARE')
      	auraInputService.setMode('TransferCover')
      	auraInputService.setName('CARE')
      	auraInputService.setAge('CARE')
      	//below values has to be set from quote page, as of now taking from existing insurance
      	auraInputService.setAppnumber('1478222348039')
      	auraInputService.setDeathAmt('200000')
      	auraInputService.setTpdAmt('200000')
      	auraInputService.setIpAmt('2000')
      	auraInputService.setWaitingPeriod('30 Days')
      	auraInputService.setBenefitPeriod('2 Years')      	
    	 getAuraTransferData.requestObj().then(function(response) {    	
      		$scope.auraResponseDataList = response.data.questions;
      		console.log($scope.auraResponseDataList)
      		angular.forEach($scope.auraResponseDataList, function(Object) {
    			$scope.sectionname = Object.questionAlias.substring(3);            			
    			 
    			});
      	});
      	
      	 $scope.updateRadio = function (answerValue, questionObj){   
      		console.log($scope.auraResponseDataList)
      		$scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
      		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {   
      			//console.log($scope.selectedIndex)
      			//console.log(selectedIndex)
      			if($scope.selectedIndex>selectedIndex){
      				//branch complete false for previous questions
      				//console.log(Object.branchComplete)
      			}
      			
      		});
      		
      		questionObj.arrAns[0]=answerValue;
      		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
      		  auraResponseService.setResponse($scope.auraRes)    		 
      		  
      		  auraPostfactory.reqObj().then(function(response) { 
      			//console.log('cntrl>>'+response.data.questionText)
          	}, function () {
          		//console.log('failed');
          	});
      	 };
      	 
      
      	
      	$scope.collapseOne = true;
      	$scope.toggleOne = function() {
            $scope.collapseOne = !$scope.collapseOne;          
        };
      	$scope.collapseTwo = false;
      	$scope.toggleTwo = function() {
            $scope.collapseTwo = !$scope.collapseTwo;            
        };
        $scope.collapseThree = false;
      	$scope.toggleThree = function() {
            $scope.collapseThree = !$scope.collapseThree;            
        };
        $scope.collapseFour = false;
      	$scope.toggleFour = function() {
            $scope.collapseFour = !$scope.collapseFour;            
        };
        $scope.collapseFive = false;
      	$scope.toggleFive = function() {
            $scope.collapseFive = !$scope.collapseFive;            
        };
    }]); 
    ////
    /*aura occ upgrade controller*/    
    scotchApp.controller('auraocc',['$scope', '$location','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService',
                                         function($scope, $location,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService ){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {        	
      	  $location.path( path );
      	};
      	
    	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
      	
      	auraInputService.setFund('CARE')
      	auraInputService.setMode('WorkRating')
      	auraInputService.setName('CARE')
      	auraInputService.setAge('CARE')
      	//below values has to be set from quote page, as of now taking from existing insurance
      	auraInputService.setAppnumber('1478222348040')
      	auraInputService.setDeathAmt('200000')
      	auraInputService.setTpdAmt('200000')
      	auraInputService.setIpAmt('2000')
      	auraInputService.setWaitingPeriod('30 Days')
      	auraInputService.setBenefitPeriod('2 Years')   	
    	 getAuraTransferData.requestObj().then(function(response) {    	
      		$scope.auraResponseDataList = response.data.questions;
      		console.log($scope.auraResponseDataList)
      		angular.forEach($scope.auraResponseDataList, function(Object) {
    			$scope.sectionname = Object.questionAlias.substring(3);            			
    			 
    			});
      	});
      	
      	 $scope.updateRadio = function (answerValue, questionObj){   
      		console.log($scope.auraResponseDataList)
      		$scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
      		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {   
      			//console.log($scope.selectedIndex)
      			//console.log(selectedIndex)
      			if($scope.selectedIndex>selectedIndex){
      				//branch complete false for previous questions
      				//console.log(Object.branchComplete)
      			}
      			
      		});
      		
      		questionObj.arrAns[0]=answerValue;
      		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
      		  auraResponseService.setResponse($scope.auraRes)    		 
      		  
      		  auraPostfactory.reqObj().then(function(response) { 
      			//console.log('cntrl>>'+response.data.questionText)
          	}, function () {
          		//console.log('failed');
          	});
      	 };    
      	
      	
    }]); 
    /*aura special offer*/
    /*aura transfer controller*/    
    scotchApp.controller('auraspecialoffer',['$scope', '$location','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService',
                                         function($scope, $location,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService ){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {        	
      	  $location.path( path );
      	};
      	auraInputService.setFund('CARE')
      	auraInputService.setMode('SpecialOffer')
      	auraInputService.setName('CARE')
      	auraInputService.setAge('CARE')
      	//below values has to be set from quote page, as of now taking from existing insurance
      	auraInputService.setAppnumber('1478222348041')
      	auraInputService.setDeathAmt('200000')
      	auraInputService.setTpdAmt('200000')
      	auraInputService.setIpAmt('2000')
      	auraInputService.setWaitingPeriod('30 Days')
      	auraInputService.setBenefitPeriod('2 Years')         	
    	 getAuraTransferData.requestObj().then(function(response) {    	
      		$scope.auraResponseDataList = response.data.questions;
      		console.log($scope.auraResponseDataList)
      		angular.forEach($scope.auraResponseDataList, function(Object) {
    			$scope.sectionname = Object.questionAlias.substring(3);            			
    			 
    			});
      	});
      	
      	 $scope.updateRadio = function (answerValue, questionObj){   
      		console.log($scope.auraResponseDataList)
      		$scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
      		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {   
      			//console.log($scope.selectedIndex)
      			//console.log(selectedIndex)
      			if($scope.selectedIndex>selectedIndex){
      				//branch complete false for previous questions
      				//console.log(Object.branchComplete)
      			}
      			
      		});
      		
      		questionObj.arrAns[0]=answerValue;
      		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
      		  auraResponseService.setResponse($scope.auraRes)    		 
      		  
      		  auraPostfactory.reqObj().then(function(response) { 
      			console.log('cntrl>>'+response.data.questionText)
          	}, function () {
          		console.log('failed');
          	});
      	 };    
      	
      	
    }]);
    
    
    /////
        
   /*Aura Page Controller Starts*/
        
    /*scotchApp.controller('aura',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {        	
      	  $location.path( path );
      	};
      	$scope.collapseOne = true;
      	$scope.toggleOne = function() {
            $scope.collapseOne = !$scope.collapseOne;          
        };
      	$scope.collapseTwo = false;
      	$scope.toggleTwo = function() {
            $scope.collapseTwo = !$scope.collapseTwo;            
        };
        $scope.collapseThree = false;
      	$scope.toggleThree = function() {
            $scope.collapseThree = !$scope.collapseThree;            
        };
        $scope.collapseFour = false;
      	$scope.toggleFour = function() {
            $scope.collapseFour = !$scope.collapseFour;            
        };
        $scope.collapseFive = false;
      	$scope.toggleFive = function() {
            $scope.collapseFive = !$scope.collapseFive;            
        };
    }]); */
   
    /*aura change cover controller*/    
    scotchApp.filter('sectionSplit', function() {
        return function(input) {
        	//console.log(input.indexOf(']'))
        	input = input.substring(input.indexOf('=')+1)                
            return input;
        }
    });
    
    scotchApp.filter('checkboxFilter', function() {
        return function(input) {
        	//console.log(input)   
        	angular.forEach(input, function (Object,index) {
        		//console.log(Object.answerText)
        		if(Object.answerText.indexOf('11241')!==-1 || Object.answerText.indexOf('11242')!==-1 || Object.answerText.indexOf('11243')!==-1 || Object.answerText.indexOf('11251')!==-1 || Object.answerText.indexOf('11252')!==-1){
        			input.splice(index, 1);
        			//questionObj.arrAns.splice(i, 1);
        		}
        	});
        	/*if(input.indexOf('10325')!==-1){
        		input = input.substring(0,input.indexOf('dd/mm/yyyy')-1)   
        	}*/       	             
            return input;
        }
    });
    
    scotchApp.filter('dateTextSplit', function() {
        return function(input) {
        	console.log(input.indexOf('dd/mm/yyyy'))
        	if(input.indexOf('dd/mm/yyyy')!==-1){
        		input = input.substring(0,input.indexOf('dd/mm/yyyy')-1)   
        	}        	             
            return input;
        }
    });
    scotchApp.controller('aura',['$scope', '$location','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService',
                                         function($scope, $location,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService ){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.datePattern='/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i';
        $scope.go = function ( path ) {        	
      	  $location.path( path );
      	};
      	
    	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
      	
      	auraInputService.setFund('CARE')
      	auraInputService.setMode('change')
      	auraInputService.setName('CARE')
      	auraInputService.setAge('CARE')
      	//below values has to be set from quote page, as of now taking from existing insurance
      	auraInputService.setAppnumber('1578724071')
      	auraInputService.setDeathAmt('600000')
      	auraInputService.setTpdAmt('600000')
      	auraInputService.setIpAmt('6000')
      	auraInputService.setWaitingPeriod('30 Days')
      	auraInputService.setBenefitPeriod('2 Years')   
      	//console.log(angular.toJson(auraInputService))      	
      	//console.log(deathCoverService.getDeathCover().amount) 
    	 getAuraTransferData.requestObj().then(function(response) {    	
      		$scope.auraResponseDataList = response.data.sections;     
      		console.log(response.data)      		
      		angular.forEach($scope.auraResponseDataList, function (Object,index) {
      			$scope.questionAlias = Object.sectionName
      			$scope.questionlist = Object.questions      			
      			if($scope.questionAlias=='QG=Health Questions'){
      				$scope.auraResponseDataList[index].sectionStatus=true;      				
      			}else{
      				var keepGoing = true;
      				angular.forEach($scope.questionlist, function (Object,index1) {      					
      					if(keepGoing && Object.branchComplete){
      						$scope.auraResponseDataList[index].sectionStatus=true;
      						keepGoing = false;
      					}
      				});
      			}
      		});
      		//console.log($scope.auraResponseDataList)
      		$scope.updateHeightWeightValue();
      		
      	});    	
      	
      	 $scope.inchValue  = function(value){ 
      		console.log(value)
      		$scope.heightin = value;     		 
      	 }
      	 $scope.meterValue  = function(value,answer){       		
      		$scope.heightinMeter = value;
      		 
      	 }
      	$scope.heighOptions  = function(value){ 
      		console.log(value)   
      		if(value=='cm'){
      			$scope.heightOptions='m.cm'
      		}else{
      			$scope.heightOptions='ft.in'
      		}    		 
      	 }
      	$scope.weightValue = function(value){
      		console.log(value)
      		$scope.weightVal = value;
      	}
      	$scope.weightOptions= function(value,answer){
      		console.log(value)   
      		if(value=='Kilograms'){
      			$scope.weighOptions='kg'
      			$scope.weightDropDown = 'Kilograms'
      		}else if(value=='Pound'){
      			$scope.weighOptions='lb'
      			$scope.weightDropDown = 'Pound'
      		}else if(value=='Stones'){
      			$scope.weighOptions='st.lb'
      			$scope.weightDropDown = 'Stones'
      		}
      	}
      	$scope.lbsValue = function(value){
      		console.log(value)
      		$scope.lbsval = value;
      	}
      	
      	 $scope.submitHeightWeightQuestion = function(){      		
      		angular.forEach($scope.auraResponseDataList, function (Object,index) {      			
      			angular.forEach(Object.questions, function (Object) {         			
      				var serviceCallRequired= false;      				
      				if(Object.classType=='HeightQuestion'){      					
      					questionObj= Object;
          				if($scope.heightOptions=='m.cm'){
          					questionObj.arrAns=[];
              	   			questionObj.arrAns[0]= $scope.heightOptions;
              	   			questionObj.arrAns[1]= '0';
              	   			questionObj.arrAns[2]= $scope.heightinMeter; 
          				}else if($scope.heightOptions=='ft.in'){
          					console.log($scope.heightOptions)
          					console.log($scope.heightinMeter)
          					console.log($scope.heightin)
          					questionObj.arrAns=[];
              	   			questionObj.arrAns[0]= $scope.heightOptions;
              	   			questionObj.arrAns[1]= $scope.heightinMeter;
              	   			questionObj.arrAns[2]= $scope.heightin; 
          				}
      					
          	   			serviceCallRequired= true;
          			}else if(Object.classType=='WeightQuestion'){
          				questionObj= Object;
          				console.log($scope.weightDropDown)
          				console.log($scope.weighOptions)
          				console.log($scope.weightVal)
          				if($scope.weightDropDown=='Stones'){
          					console.log($scope.weightVal)
          					console.log($scope.lbsval)
          					questionObj.arrAns=[];
              	   			questionObj.arrAns[0]= 'st.lb';
              	   			questionObj.arrAns[1]= $scope.weightVal; 
              	   			questionObj.arrAns[2]= $scope.lbsval;
          				}else if($scope.weightDropDown=='Kilograms'){       					
          					questionObj.arrAns=[];
              	   			questionObj.arrAns[0]= 'kg';
              	   			questionObj.arrAns[1]= $scope.weightVal; 
          				}else if($scope.weightDropDown=='Pound'){          					
          					questionObj.arrAns=[];
              	   			questionObj.arrAns[0]= 'lb';
              	   			questionObj.arrAns[1]= $scope.weightVal; 
          				}
          				
          	   			serviceCallRequired= true;
          			}
      				if(serviceCallRequired){
      					 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};  
      					console.log($scope.auraRes)
              			 auraResponseService.setResponse($scope.auraRes)  
              			auraPostfactory.reqObj().then(function(response) {
                   			//questionObj.auraAnswer = response.data.auraAnswer  
              				console.log(response.data)
              				$scope.updateQuestionList(response.data.changedQuestion)
              				$scope.updateHeightWeightValue();
              				var keepGoing = true;      		
                      		angular.forEach($scope.auraResponseDataList[index].questions, function (Object,index1) {      			
                      			console.log(Object)
                      			if(keepGoing && !Object.branchComplete){
                      				keepGoing = false;
                      			}
                      			
                      		});
                      		console.log(keepGoing)
                      		if(keepGoing){
                      			index = index+1;
                      			console.log(index)
                          		$scope.auraResponseDataList[index].sectionStatus=true;                      			
                      		}
                       	}, function () {
                       		console.log('failed');
                       	});
      				}
          			
          		});
      		});    		
       	 }; 
       	 //updating height and weight questions
       	 $scope.updateHeightWeightValue = function(){
       		 
       		angular.forEach($scope.auraResponseDataList, function (Object,index) {
      			$scope.questionAlias = Object.sectionName
      			$scope.questionlist = Object.questions      			
      			if($scope.questionAlias=='QG=Health Questions'){     				
      				$scope.healthQuestion = $scope.auraResponseDataList[index].questions;
      				 angular.forEach($scope.healthQuestion, function (question) {
     					if(question.classType=='HeightQuestion'){      						  				
     						if(question.answerTest.indexOf('Metres')!==-1){    							
     							$scope.heightval=question.answerTest.substring(0,question.answerTest.indexOf('Metres')-1)+question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('cm')-1);
     							$scope.heightDropDown='cm'
     							$scope.heightOptions='m.cm'
     						}      				
     						if(question.answerTest.indexOf('Feet')!==-1){      							
     							$scope.heightDropDown='Feet'
     							$scope.heightval=question.answerTest.substring(0,question.answerTest.indexOf('Feet')-1)
     							$scope.inches= question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('in')-1);
     							$scope.heightOptions='ft.in'
     						}
     						
     					}else if(question.classType=='WeightQuestion'){      						 
     						if(question.answerTest.indexOf('Kilograms')!==-1){   
     							console.log(question.answerTest) 
     							$scope.weightDropDown='Kilograms';
     							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Kilograms')-1);      							
     							$scope.weighOptions='kg';
     						} 
     						if(question.answerTest.indexOf('Pound')!==-1){   
     							console.log(question.answerTest) 
     							$scope.weightDropDown='Pound';
     							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Pound')-1);      							
     							$scope.weighOptions='lb';
     						}
     						if(question.answerTest.indexOf('Stones')!==-1){   
     							console.log(question.answerTest) 
     							$scope.weightDropDown='Stones';
     							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Stones')-1);      							
     							$scope.weighOptions='st.lb';
     							$scope.lbsval=question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('lbs')-1);
     						}
     					}
     				});
      			}
       		});
       		
       		
       	 }
       	 
      	$scope.collapseUncollapse = function (index,sectionName){
      		console.log(sectionName)
      		if(sectionName=='QG=Health Questions'){
      			$scope.submitHeightWeightQuestion();
      		}else{
      			var keepGoing = true;      		
          		angular.forEach($scope.auraResponseDataList[index].questions, function (Object,index1) {      			
          			console.log(Object)
          			if(keepGoing && !Object.branchComplete){
          				keepGoing = false;
          			}
          			
          		});
          		console.log(keepGoing)
          		if(keepGoing){
          			index = index+1;
          			console.log(index)
              		$scope.auraResponseDataList[index].sectionStatus=true;
          			
          		}
      		}      		      		
      		
      		//console.log($scope.auraResponseDataList)
      	}
      	
      	$scope.stateChanged = function (qId) {      		 
      		if($scope.answers[qId]){ //If it is checked
      			 console.log('id>>'+qId)
      	   }
      	}
      	$scope.updateFreeText = function (answerValue, questionObj){
      		console.log('answer>'+answerValue)
      		questionObj.arrAns = [];
      		questionObj.arrAns[0]=answerValue;
      		questionObj.arrAns[1]='accept';
     		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};  
     		auraResponseService.setResponse($scope.auraRes)	  
    		  auraPostfactory.reqObj().then(function(response) {
    			  $scope.updateQuestionList(response.data.changedQuestion)    	
    		  });
      	}
      	$scope.updateQuestionList = function(changedQuestion){
      		angular.forEach($scope.auraResponseDataList, function (Object,index) {
      			$scope.questionlist = Object.questions
      			angular.forEach($scope.questionlist, function (Object,index1) {
      				if(Object.questionId == changedQuestion.questionId){
      					$scope.auraResponseDataList[index].questions[index1]= changedQuestion
      					console.log($scope.auraResponseDataList[index].questions[index1])
      				}
      			});
      		})
      	}
      	
      	 $scope.updateRadio = function (answerValue, questionObj){   
      		console.log('answer>'+answerValue)
      		questionObj.answerTest = answerValue;       		
      		
      		questionObj.arrAns[0]=answerValue;
      		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
      		  auraResponseService.setResponse($scope.auraRes)    		 
      		  
      		  auraPostfactory.reqObj().then(function(response) {      			
      		console.log(response.data)
      		$scope.updateQuestionList(response.data.changedQuestion)        			
      			
          	}, function () {
          		console.log('failed');
          	});
      	 };    
      	 
      	$scope.searchValSubmit = function (answerValue, questionObj,parentQuestionObj){
      		console.log(questionObj.answerValue.answerValue)
      		//console.log(questionObj)
      		console.log(parentQuestionObj)
      		questionObj.arrAns=[];
      		questionObj.arrAns[0] = questionObj.answerValue.answerValue;
      		 $scope.auraRes={"questionID":parentQuestionObj.questionId,"auraAnswers":questionObj.arrAns};
      		auraResponseService.setResponse($scope.auraRes)     		  
    		  auraPostfactory.reqObj().then(function(response) {
    			  $scope.updateQuestionList(response.data.changedQuestion)    		
    		  });
      	}
      	 
      	 $scope.updateCheckBox = function (answerValue, questionObj,index){      	
      		 console.log(questionObj.answerTest);
      		 if(questionObj.arrAns.length==0){
      			var answerArray = questionObj.answerTest.split(',');
      			if(answerArray.length>1){
      				questionObj.arrAns = answerArray;
      			}else{
      				questionObj.arrAns[0] = questionObj.answerTest;
      			}
      		 }
      		   		  
      		
      		 var addtoArray=true;
      		if(answerValue.indexOf('None of the above') !== -1){
      			questionObj.arrAns=[];
      			questionObj.arrAns[0]= answerValue;
      		}else{
      			for (var i=0;i<questionObj.arrAns.length;i++){               	 
      				 console.log(questionObj.arrAns[i])
      				 console.log(answerValue)
      				if(questionObj.arrAns[i]==answerValue ){
                       	questionObj.arrAns.splice(i, 1);
                       	addtoArray=false
                      }
                }
                if(addtoArray==true){
               	 questionObj.arrAns.splice(index, 0, answerValue);
                }
      		}            
             //removing none of the above, if there are other answers
      		if(questionObj.arrAns.length>1){
      			for (var i=0;i<questionObj.arrAns.length;i++){ 
          			if(questionObj.arrAns[i].indexOf('None of the above') !== -1){
          				questionObj.arrAns.splice(i, 1);
          			}
          		
      			}
      		}
             $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
             console.log(JSON.stringify(questionObj.arrAns))
             auraResponseService.setResponse($scope.auraRes)           
             auraPostfactory.reqObj().then(function(response) {            	
        			console.log(response.data)
        			$scope.updateQuestionList(response.data.changedQuestion)         	 
       				
           	}, function () {
           		console.log('failed');
           	});
      	 };
      	
    }]);
    
   /*Aura Page Controller Ends*/
    
    
    /*Summary Page Controller Starts*/ 
    scotchApp.controller('summary',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	$scope.collapse = false;
      	$scope.toggle = function() {
            $scope.collapse = !$scope.collapse;           
        };      	
        
        $scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
        
    }]); 
   /*Summary Page Controller Ends*/   
    
   /*Decision Page Controller Starts*/   
    scotchApp.controller('decision',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	
      	$scope.navigateToLandingPage = function (){
        	if(window.confirm('Are you sure you want to navigate to Home Page?')){
        		$location.path("/landing");
        	}
        }
      	
    }]); 
    /*Decision Page Controller Ends*/  
    
/*Decision Page Controller Ends*/  
    
    scotchApp.service('tokenNumService', function() {
    	var _self = this;
    	_self.setTokenId = function (tempToken) {
    		_self.tokenId = tempToken;
      }
    	_self.getTokenId = function () {
    	  return _self.tokenId;
      }
    });
    
    scotchApp.service('deathCoverService', function() {
    	var _self = this;
    	_self.setDeathCover = function (tempToken) {
    		//_self.deathCover = tempToken;
    		sessionStorage.setItem('deathCoverObject',JSON.stringify(tempToken));
      }
    	_self.getDeathCover = function () {
    		//return _self.deathCover;
    		return JSON.parse(sessionStorage.getItem('deathCoverObject'));
      }
    });
    
    scotchApp.service('tpdCoverService', function() {
    	var _self = this;
    	_self.setTpdCover = function (tempToken) {
    		//_self.tpdCover = tempToken;
    		sessionStorage.setItem('tpdCoverObject',JSON.stringify(tempToken));
      }
    	_self.getTpdCover = function () {
    		 //return _self.tpdCover;
    		return JSON.parse(sessionStorage.getItem('tpdCoverObject'));
      }
    });
    
    scotchApp.service('ipCoverService', function() {
    	var _self = this;
    	_self.setIpCover = function (tempToken) {
    		//_self.ipCover = tempToken;
    		sessionStorage.setItem('ipCoverObject',JSON.stringify(tempToken));
      }
    	_self.getIpCover = function () {
    		//return _self.ipCover;
    		return JSON.parse(sessionStorage.getItem('ipCoverObject'));
      }
    });
   
    scotchApp.service('newMemberOfferService', function() {
    	var _self = this;
    	_self.setNewMemberOfferFlag = function (tempToken) {
    		_self.newMemberOfferFlag = tempToken;
      }
    	_self.getNewMemberOfferFlag = function () {
    	  return _self.newMemberOfferFlag;
      }
    });
    
    scotchApp.factory('getclientData', function($http, $q,tokenNumService){     	
        //var defer = $q.defer();      
        var myFactObj = {};
        myFactObj.requestObj = function () {
        	var defer = $q.defer();
        	$http.defaults.headers.common['Authorization'] = 'C3PO R2D2';
    		$http.get('http://localhost:8083/getcustomerdata',{
    			headers: {'Authorization':  tokenNumService.getTokenId()},
    			params:{ tokenid: tokenNumService.getTokenId()}}).then(function(response) {
    				//response.headers = response.headers || {};
    				//response.headers.Authorization = '12345678';
    			defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });
    
    scotchApp.factory('getAuraTransferData', function($http, $q,auraTransferInitiateService,auraInputService){     	
       // var defer = $q.defer();      
        var myFactObj = {};
        myFactObj.requestObj = function () {
        	var defer = $q.defer();
        //	$http.defaults.headers.common['Authorization'] = 'C3PO R2D2';    
        	
    		$http.post('http://localhost:8084/initiate',auraInputService).then(function(response) {
    				
    			defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });
    
    scotchApp.service('auraTransferInitiateService', function() {
    	var _self = this;
    	_self.setInput = function (tempInput) {
    		_self.input = tempInput;
      }
    	_self.getInput = function () {
    	  return _self.input;
      }
    });
    
    scotchApp.factory('auraPostfactory', function($http, $q,auraResponseService){     	
            
        var myFactObj = {};       
       
        myFactObj.reqObj = function () {
        	 var defer = $q.defer(); 
    		$http.post('http://localhost:8084/underwriting',auraResponseService.getResponse()
    				).then(function(response) {
    				console.log('hi>>'+response.data.questionText)
    			defer.resolve(response);	
    				
    		}, function(response) {
    			defer.reject(response);
    			//console.log(response.data)
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });   
  

    
    scotchApp.service('auraResponseService', function() {
    	var _self = this;
    	_self.setResponse = function (tempInput) {
    		_self.response = tempInput;
      }
    	_self.getResponse = function () {
    	  return _self.response;
      }
    });
    
scotchApp.service('auraInputService', function() {   	
    	
    	var _self = this;
    	_self.setFund = function (tempInput) {
    		_self.fund = tempInput;
      }
    	_self.getFund = function () {
    	  return _self.fund;
      }
    	_self.setMode = function (tempInput) {
    		_self.mode = tempInput;
      }
    	_self.getMode = function () {
    	  return _self.mode;
      }
    	_self.setName = function (tempInput) {
    		_self.name = tempInput;
      }
    	_self.getName = function () {
    	  return _self.name;
      }
    	_self.setAge = function (tempInput) {
    		_self.age = tempInput;
      }
    	_self.getAge = function () {
    	  return _self.age;
      }    	
    	_self.setDeathAmt = function (tempInput) {
    		_self.deathAmt = tempInput;
      }
    	_self.getDeathAmt = function () {
    	  return _self.deathAmt;
      }
    	
    	_self.setTpdAmt = function (tempInput) {
    		_self.tpdAmt = tempInput;
      }
    	_self.getTpdAmt = function () {
    	  return _self.tpdAmt;
      }
    	
    	_self.setIpAmt = function (tempInput) {
    		_self.ipAmt = tempInput;
      }
    	_self.getIpAmt = function () {
    	  return _self.ipAmt;
      }
    	
    	_self.setWaitingPeriod = function (tempInput) {
    		_self.waitingPeriod = tempInput;
      }
    	_self.getWaitingPeriod = function () {
    	  return _self.waitingPeriod;
      }
    	
    	_self.setBenefitPeriod = function (tempInput) {
    		_self.benefitPeriod = tempInput;
      }
    	_self.getBenefitPeriod = function () {
    	  return _self.benefitPeriod;
      }
    	_self.setAppnumber = function (tempInput) {
    		_self.appnumber = tempInput;
      }
    	_self.getAppnumber = function () {
    	  return _self.appnumber;
      }
    	
    	
    });
    
   