package au.com.metlife.eapply.b2b.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 * An entity User composed by three fields (id, email, name).
 * The Entity annotation indicates that this class is a JPA entity.
 * The Table annotation specifies the name for the table in the db.
 *
 * @author netgloo
 */
@Entity
@Table(name = "b2bstring", schema="EAPPDB")
public class Eappstring {
	
	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private long id;
	
	  // The user's email
	  @NotNull
	  private String securestring;
	  
	  @NotNull
	  @Column(length=5000)
	  private String secureToken;
	  public String getSecureToken() {
		return secureToken;
	}

	public void setSecureToken(String secureToken) {
		this.secureToken = secureToken;
	}
	// The user's name	
	  @Size(max=10)
	  @Column(length=10)
	  private String status;
	  
	  private Date createdate;
	  @Size(max=20)
	  @Column(length=20)
	  private String applicationid;
	  
	  public Eappstring(){
		  
	  }
	  
	public Eappstring(String securestring, String secureToken, String status, Date createdate, String applicationid) {
		this.securestring = securestring;
		this.secureToken = secureToken;
		this.status = status;
		this.createdate = createdate;
		this.applicationid = applicationid;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSecurestring() {
		return securestring;
	}
	public void setSecurestring(String securestring) {
		this.securestring = securestring;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getApplicationid() {
		return applicationid;
	}
	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}
}
