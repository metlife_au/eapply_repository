VicsuperApp.controller('auraIndexation',['$scope', '$rootScope', '$location', '$timeout','$window','auraTransferInitiateSvc', 'fetchAuraTransferData','auraRespSvc', 'auraPostSvc','auraInputSvc','deathCoverService','tpdCoverService','ipCoverService', 'PersistenceService', 'submitAuraSvc','ngDialog','persoanlDetailService','saveEapplyData','clientMatchSvc','submitEapplySvc','fetchUrlSvc', 'fetchPersoanlDetailSvc', 'appData', 'fetchAppNumberSvc', 'fetchIndustryListVict','fetchDeathCoverSvc','fetchTpdCoverSvc', 'fetchIpCoverSvc',
                                     function($scope, $rootScope, $location, $timeout,$window,auraTransferInitiateSvc,fetchAuraTransferData,auraRespSvc,auraPostSvc,auraInputSvc,deathCoverService,tpdCoverService,ipCoverService, PersistenceService, submitAuraSvc, ngDialog,persoanlDetailService,saveEapplyData,clientMatchSvc,submitEapplySvc,fetchUrlSvc, fetchPersoanlDetailSvc, appData, fetchAppNumberSvc, fetchIndustryListVict, fetchDeathCoverSvc, fetchTpdCoverSvc, fetchIpCoverSvc){
	$scope.urlList = fetchUrlSvc.getUrlList();
  $scope.datePattern='/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i';
  $scope.ipIncreaseFlag = false;
  $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
  $scope.benefitPeriodOptions = ['2 Years','5 Years', 'Age 65'];
  $scope.freeText = {};
  $scope.isAuraErroneous = false;
  $scope.erroneousSections = [];
  $scope.go = function (path) {
    $timeout(function(){
      $location.path(path);
    }, 10);
  };

	$scope.navigateToLandingPage = function () {
		ngDialog.openConfirm({
	            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path("/landing");
	        }, function(e){
	        	if(e=='oncancel'){
	        		return false;
	        	}
	        });
    };

    $scope.deathCoverDetails = fetchDeathCoverSvc.getDeathCover();
	$scope.tpdCoverDetails = fetchTpdCoverSvc.getTpdCover();
    $scope.ipCoverDetails = fetchIpCoverSvc.getIpCover();
	
    $scope.quotePageDetails = {}; // will be replaced
    $scope.coverDetails = {}; // will be replaced
    $scope.deathAddnlDetails = {}; // will be replaced
    $scope.tpdAddnlDetails = {}; // will be replaced
    $scope.ipAddnlDetails = {}; // will be replaced
    $scope.changeCoverOccDetails = {}; // will be replaced

    angular.extend($scope.quotePageDetails, appData.getAppData());
    angular.extend($scope.coverDetails, $scope.quotePageDetails);
    angular.extend($scope.deathAddnlDetails, $scope.quotePageDetails.addnlDeathCoverDetails);
    angular.extend($scope.tpdAddnlDetails, $scope.quotePageDetails.addnlTpdCoverDetails);
    angular.extend($scope.ipAddnlDetails, $scope.quotePageDetails.addnlIpCoverDetails);
    angular.extend($scope.changeCoverOccDetails, $scope.quotePageDetails.occupationDetails);
    // Industry name
  //Commented as part of VICSUPER
    $scope.quotePageDetails.industryName = fetchIndustryListVict.getIndustryName($scope.quotePageDetails.occupationDetails.industryCode);
    $scope.personalDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
 
 
  	auraInputSvc.setFund('VICT');
  	auraInputSvc.setMode('Indexation');
    //setting deafult vaues for testing
  	auraInputSvc.setName($scope.personalDetails.personalDetails.firstName+" "+$scope.personalDetails.personalDetails.lastName);
  	//auraInputSvc.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
  	auraInputSvc.setAge($scope.coverDetails.age);
  	auraInputSvc.setAppnumber($scope.coverDetails.appNum)
  	auraInputSvc.setDeathAmt(parseFloat($scope.deathAddnlDetails.deathFixedAmt));
  	auraInputSvc.setTpdAmt(parseFloat($scope.tpdAddnlDetails.tpdFixedAmt));
  	auraInputSvc.setIpAmt(parseFloat($scope.ipAddnlDetails.ipFixedAmt));
  	
  	/*if(parseFloat($scope.deathAddnlDetails.deathFixedAmt) > parseFloat($scope.coverDetails.existingDeathAmt)){
  		auraInputSvc.setDeathAmt(parseFloat($scope.deathAddnlDetails.deathFixedAmt));
		}else{
			auraInputSvc.setDeathAmt(0);
		}*/

		/*if(parseFloat($scope.tpdAddnlDetails.tpdFixedAmt) > parseFloat($scope.coverDetails.existingTpdAmt)){
			auraInputSvc.setTpdAmt(parseFloat($scope.tpdAddnlDetails.tpdFixedAmt));
		}else{
			auraInputSvc.setTpdAmt(0);
		}*/
  	
  	/*if(parseFloat($scope.ipAddnlDetails.ipFixedAmt) > parseFloat($scope.coverDetails.existingIPAmount)){
  		auraInputSvc.setIpAmt(parseFloat($scope.ipAddnlDetails.ipFixedAmt));
  	}else if((parseFloat($scope.ipAddnlDetails.ipFixedAmt) <= parseFloat($scope.coverDetails.existingIPAmount)) && $scope.ipIncreaseFlag){
  		auraInputSvc.setIpAmt(parseFloat($scope.ipAddnlDetails.ipFixedAmt));
  	}else{
  		auraInputSvc.setIpAmt(0);
  	} */ 	
  
  	auraInputSvc.setWaitingPeriod($scope.ipAddnlDetails.waitingPeriod)
  	auraInputSvc.setBenefitPeriod($scope.ipAddnlDetails.benefitPeriod)
  	auraInputSvc.setMemberType("None")
  	//auraInputSvc.setMemberType("none")
  	if($scope.changeCoverOccDetails && $scope.changeCoverOccDetails.gender ){
  		auraInputSvc.setGender($scope.changeCoverOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputSvc.setGender($scope.personalDetails.gender);
  	}

  	auraInputSvc.setClientname('metaus')
  	auraInputSvc.setIndustryOcc("None")
  	//auraInputSvc.setIndustryOcc("001:Business Analyst/Consultant")
  /*	if($scope.changeCoverOccDetails.citizenQue=='Yes'){
  		auraInputSvc.setCountry('Australia')
  	}else{
  		auraInputSvc.setCountry($scope.changeCoverOccDetails.citizenQue)
  	} */
  	if($scope.quotePageDetails.occupationDetails.citizenQue=='Yes'){
  		auraInputSvc.setCountry('Australia');
  	}else{
  		auraInputSvc.setCountry($scope.quotePageDetails.occupationDetails.citizenQue);
  	}
  		 // residency que removed from quote page, so hard-coded
  	auraInputSvc.setSalary($scope.changeCoverOccDetails.salary)
  	auraInputSvc.setFifteenHr($scope.changeCoverOccDetails.fifteenHr)
  	auraInputSvc.setLastName($scope.personalDetails.personalDetails.lastName)
  	auraInputSvc.setFirstName($scope.personalDetails.personalDetails.firstName)
  	auraInputSvc.setDob($scope.personalDetails.personalDetails.dateOfBirth)
  	var termFlag = false;
    $scope.personalDetails.existingCovers = $scope.personalDetails.existingCovers || {}; 
    $scope.personalDetails.existingCovers.cover = $scope.personalDetails.existingCovers.cover || [];
  	for(var k = 0; k < $scope.personalDetails.existingCovers.cover.length; k++){
  		if(($scope.personalDetails.existingCovers.cover[k].exclusions && $scope.personalDetails.existingCovers.cover[k].exclusions != '') ||
  				($scope.personalDetails.existingCovers.cover[k].loading && $scope.personalDetails.existingCovers.cover[k].loading != '' &&
  						parseFloat($scope.personalDetails.existingCovers.cover[k].loading) > 0)){
  			termFlag = true;
  			break;
  		}
  	}
  	/*if(termFlag){
  		auraInputSvc.setExistingTerm(true);
  	} else{
  		auraInputSvc.setExistingTerm(false);
  	}*/
  	auraInputSvc.setExistingTerm(false);

  	fetchAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
	  		$scope.auraResponseDataList = response.data.questions;
	  		//console.log($scope.auraResponseDataList)
	  		angular.forEach($scope.auraResponseDataList, function(Object) {
				$scope.sectionname = Object.questionAlias.substring(3);
	
				});
	  	});


  	 $scope.updateRadio = function (answerValue, questionObj) {

  		questionObj.answerTest = answerValue;
  		questionObj.arrAns[0]=answerValue;
 		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
 		  auraRespSvc.setResponse($scope.auraRes)
 		   auraPostSvc.reqObj($scope.urlList.auraPostUrl).then(function(response) {
 			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
 			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
 	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
 	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
 	  				//branch complete false for previous questions
 	  				$scope.auraResponseDataList[selectedIndex].error=true;
 	  			}else if(Object.questionComplete){
 	  				$scope.auraResponseDataList[selectedIndex].error=false;
 	  			}

 	  		});

     	}, function () {
     		//console.log('failed');
     	});
 	 };

 	 $scope.proceedToNext = function () {
       		$scope.keepGoing = true;
      		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
      			if(!Object.questionComplete){
      				$scope.auraResponseDataList[selectedIndex].error=true;
      				$scope.keepGoing = false;
      			}else{
      				$scope.auraResponseDataList[selectedIndex].error=false;
      			}
      		});
      		if($scope.keepGoing){
//      			$rootScope.$broadcast('disablepointer');
      			submitAuraSvc.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
      				$scope.auraResponses = response.data;
                    if(response.status == 200) {
      				if($scope.auraResponses.overallDecision!='DCL'){
      					clientMatchSvc.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
      						if(clientMatchResponse.data.matchFound){
      							$scope.clientmatchreason = '';
      							$scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
      							$scope.auraResponses.overallDecision='RUW'
      								$scope.information = clientMatchResponse.data.information
      								angular.forEach($scope.information, function (infoObj,index) {
      									if(index==$scope.information.length-1){
      										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
      										$scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))
      									}else{
      										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
      									}

      								})
      							$scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
      							$scope.auraResponses.specialTerm = false;
      						}

          					appData.setChangeCoverAuraDetails($scope.auraResponses);
          					$location.path('/summary');
          				});
      				}else if($scope.auraResponses.overallDecision =='DCL'){
      					if($scope.coverDetails != null && $scope.changeCoverOccDetails != null && $scope.deathAddnlDetails != null &&
      		    				$scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.auraResponses != null && $scope.personalDetails != null){
      		    			$scope.coverDetails.lastSavedOn = '';
      		    			$scope.details={};
      		    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
      		    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
      		    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
      		    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
      		    			var temp = angular.extend($scope.details,$scope.coverDetails);
      		    			var aura = angular.extend(temp,$scope.auraResponses);
      		    			var submitObject = angular.extend(aura, $scope.personalDetails);
      		    			auraRespSvc.setResponse(submitObject);
      		    			submitEapplySvc.submitObj($scope.urlList.submitEapplyUrl).then(function(response) {
      		            		appData.setPDFLocation(response.data.clientPDFLocation);
      		            		appData.setNpsUrl(response.data.npsTokenURL);
      		            		$location.path('/changedecline');
      		            	}, function(err){
      		            		//console.log('Error saving the cover ' + err);
      		            	});
      		            }
      				}
      			}
      	  		});
      		}
      	}


      	// Save
      	var appNum;
        appNum = fetchAppNumberSvc.getAppNumber();
      	 $scope.saveAura = function (){
      		if($scope.coverDetails != null && $scope.changeCoverOccDetails != null &&  $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null &&  $scope.personalDetails != null){
        		$scope.coverDetails.lastSavedOn = 'AuraIndexPage';
        		$scope.details={};
        		$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
        		//$scope.auraDetails
        		var temp = angular.extend($scope.details,$scope.coverDetails);
            	var saveAuraObject = angular.extend(temp, $scope.personalDetails);
            	auraRespSvc.setResponse(saveAuraObject)
    	        saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, saveAuraObject).then(function(response) {
                //console.log(response.data);
                $scope.auraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ appNum +'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
    	        });
        	}
       };

       $scope.auraSaveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
				    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};

    }]);
   /*Aura Page Controller Ends*/
