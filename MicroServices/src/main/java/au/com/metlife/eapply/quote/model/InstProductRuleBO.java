/**
 *
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.drools.KnowledgeBase;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * @author 199306
 *
 */
@Component
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value="request")
public class InstProductRuleBO implements Serializable {

	/* Attributes for rendering bread crumbs */

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean rendCvrDetBrdCrumb = false;

	private boolean rendPersStmtBrdCrumb = false;

	private boolean rendCvrSummBrdCrumb = false;

	private boolean rendPersDtlsBrdCrumb = false;

	/* Attributes for rendering bread crumbs */
	
	/* Attributes for rendering command buttons */

	private boolean rendSaveBtnInCvrSumm = false;

	private boolean rendNextBtnInCvrSumm = false;

	private boolean rendSubmitBtnInCvrSumm = false;

	private boolean rendSubmitBtnInPersDtls = false;

	private boolean rendSaveBtnInPersDtls = false;

	private boolean rendNextBtnInPersDtls = false;
	
	private boolean rendUnitFixedLabel = false;
	
	private boolean renderPrintButton = false;

	/* Attributes for rendering command buttons */

	/* Attributes specific to Rules Configuration */

	private String ruleFileName = null;

   /* private transient StatelessKnowledgeSession kSession = null;*/
	
	private transient KnowledgeBase kbase = null;

	/* Attributes specific to Rules Configuration */

	private String productCode = null;


	private boolean smokerRenderFlag=false;

	private String smokerCellStyCls=null;
	
	/*Addedd By Prasanna Durga --MTAA PRDCT*/
	private boolean occupDutiesRenderFlag=false;

	private String occupDutiesCellStyCls=null;
	
	private boolean workDutiesRenderFlag=false;

	private String workDutiesCellStyCls=null;
	
	private boolean workDuties1RenderFlag=false;

	private String workDuties1CellStyCls=null;
	
	private boolean workDuties2RenderFlag=false;

	private String workDuties2CellStyCls=null;
	
	private boolean workDuties3RenderFlag=false;

	private String workDuties3CellStyCls=null;
	
	private boolean teritoryQualRenderFlag=false;

	private String teritoryQualCellStyCls="td_alt_white";
	
	private boolean spendTimeOutsideRenderFlag=false;

	private String spendTimeOutsideCellStyCls=null;
	
	private boolean currentEmplydRenderFlag=false;

	private String currentEmplydCellStyCls=null;
	
	private boolean salaryInsureRenderFlag=false;

	private String salaryInsureCellStyCls=null;	
	/*Addedd By Prasanna Durga --MTAA PRDCT*/

	private boolean occupationRenderFlag=false;
	
	private boolean askmanualqRenderFlag=false;
	
	private boolean askProfessionalQRenderFlag=false;

	private String occupCellStyCls=null;

	private boolean activelyEmpRenderFlag=false;

	private String activelyEmpRCellStyCls=null;

	private boolean permanentEmpRenderFlag=false;

	private String permanentEmpRCellStyCls=null;

	private boolean aalRenderFlag=false;

	private String aalRCellStyCls=null;

	private boolean moreThan15HrsRenderFlag=false;

	private String moreThan15HrsCellStyCls=null;

	private boolean permanentEmpMoreThan15HrsRenderFlag=false;

	private String permanentEmpMoreThan15HrsCellStyCls=null;

	private boolean anualSalRenderFlag=false;

	private String anualSalCellStyCls=null;

	private boolean permanentEmpTreeRenderFlag=false;

	private String permanentEmpTreeCellStyCls=null;

	private boolean moreThan15HrsTreeRenderFlag=false;

	private String moreThan15HrsTreeCellStyCls=null;

	private boolean replaceExistRenderFlag=false;

	private String replaceExistCellStyCls=null;

	private boolean insuranceCoverFreqRenderFlag=false;

	private String insuranceCoverFreqCellStyCls=null;

	private boolean dobRenderFlag=false;

	private String dobCellStyCls=null;

	private boolean empTypeRenderFlag=false;

	private String empTypeCellStyCls=null;
	
	private boolean genderRenderFlag=false;

	private String genderCellStyCls=null;

	private boolean citizenShipRenderFlag=false;

	private String citizenShipCellStyCls=null;

	private boolean partnerShipRenderFlag=false;

	private String partnerShipCellStyCls=null;

	private String ipWaitingPeriodTokens=null;

	private String ipBenefitPeriodTokens=null;
	
	private String transferWaitingPeriodTokens=null;

	private String transferBenefitPeriodTokens=null;

	private String premiumFrequencyTokens=null;

	private String defaultPremiumFreq=null;

	private boolean disablePremiumFreq=false;

	private String brandName=null;

	private String productName=null;

	private double deathCvrAmt=0;

	private double tpdCvrAmt=0;

	private double ipCvrAmt=0;

	private double traumaCvrAmt=0;

	private transient List questionIdsList=new ArrayList();

	private boolean occupDisableFlag=false;

	private boolean ipUnitisedFixedRadio = false;

	private boolean ipUnitisedRadio = false;

	private boolean ipFixedRadio = false;
	/*AUSW attributes for renedering*/
	private boolean firstNameRenderFlag = false;
	private String firstNameCellStyCls = null;
	private boolean lastNameRenderFlag = false;
	private String lastNameCellStyCls = null;
	private boolean saveButtonRenderFlag = false;
	private boolean emailSaveBtnRenderFlag=false;
	private boolean printButtonRenderFlag = false;
	private boolean levelCommissionRenderFlag = false;
	private String levelCommissionCellStyCls = null;
	
	/*Added by Prasanna REIS Changes*/
	private boolean memberTypeRenderFlag = false;
	private String memberTypeRenderCellStyCls = null;
	private String reasonForApplTokens=null;
	private boolean reasonForApplRenderFlag = false;
	private String reasonForApplRenderCellStyCls = null;
	/*Added by Prasanna REIS Changes*/
	
	/*Added for new corp eapp*/
	private boolean memberShipCatgRenderFlag = false;
	
	private boolean employeeNoRenderFlag = false;
	
	private boolean industryTypeRenderFlag = false;
	
	private boolean industryOccupRenderFlag = false;
	
	/*Added for new corp eapp*/
	
	private String memberType=null;
	
	private boolean rendExDtTxtFld=false;
	
	private boolean rendExTpdTxtFld=false;
	
	private boolean rendExTraumaTxtFld=false;
	
	private boolean rendExIpTxtFld=false;
	
	private boolean renderTraumaSection=false;
	
	private boolean rendEightyFifyPerSal=false;
	
	private int deathMinAge=0;
	
	private int deathMaxAge=0;
	
	private int tpdMinAge=0;
	
	private int tpdMaxAge=0;
	
	private int tpdMaxLimitAfterAge=0;
	
	private int traumaMinAge=0;
	
	private int traumaMaxAge=0;
	
	private int ipMinAge=0;
	
	private int ipMaxAge=0;
	
	private BigDecimal deathMinAmt=null;
	
	private BigDecimal deathMinUnits=null;
	
	private BigDecimal deathMaxAmt=null;
	
	private BigDecimal deathMaxUnits=null;
	
	private BigDecimal tpdMinAmt=null;
	
	private BigDecimal tpdMinUnits=null;
	
	private BigDecimal tpdMaxAmt=null;
	
	private BigDecimal tpdMaxAmtAfterXAge=null;
	
	private BigDecimal tpdMaxUnits=null;
	
	private BigDecimal traumaMinAmt=null;
	
	private BigDecimal traumaMaxAmt=null;
	
	private BigDecimal ipMinAmt=null;
	
	private BigDecimal ipMinAmtForDisp=null;
	
	private BigDecimal ipMaxAmt=null;
	
	private BigDecimal ipTransferMaxAmt=null;
	
	private BigDecimal deathTpdTransferMaxAmt=null;
	
	private BigDecimal deathTpdTransferMaxUnits=null;
	
	private double deathUnitCostMultFacStandard=0;
	
	private double deathUnitCostMultFacWhiteCollar=0;
	
	private double deathUnitCostMultFactProff=0;
	
	private double deathUnitCostMultipFactor=0;
	
	private double tpdUnitCostMultFacStandard=0;
	
	private double tpdUnitCostMultFacWhiteCollar=0;
	
	private double tpdUnitCostMultFacOwnWhiteCollar=0;
	
	private double deathUnitCostMultFacOwnWhiteCollar=0;
	
	private double tpdUnitCostMultFactProff=0;
	
	private double tpdUnitCostMultipFactor=0;
	
	private double tpdOnlyMultipFactor=0;
	
	private double deathOnlyMultipFactor=0;
	
	private double traumaUnitCostMultpFactor=0;
	
	private BigDecimal deathMultipleFactor;
	
	private BigDecimal tpdMultipleFactor;
	
	private BigDecimal ipMultipleFactor;
	
	private double ipUnitCostMultipFactor=0;
	
	private double ipUnitCvrAmtFor2Yrs=0;
	
	private double ipUnitCvrAmtForAge65=0;
	
	private double sfpsIpUnitCostStandardMultFac=0;
	
	private double sfpsIpUnitCostWhiteColMultFac=0;
	
	private double sfpsIpUnitCostProfMultFac=0;
	
	private double ipMultiPolicyDiscount=0;
	
	private boolean rendIpRadioBtns=false;
	
	private boolean rendIpUnitIndicator=false;
	
	private boolean rendIpFixedIndicator=false;
	
	private boolean disableDcUnitised=false;
	
	private boolean disableDcFixed=false;
	
	private boolean disableTpdUnitised=false;
	
	private boolean disableTpdFixed=false;
	
	private boolean disableIpUnitised=false;
	
	private boolean disableipFixed=false;
	
	private boolean disableTraumaUnitised=false;
	
	private boolean disableTraumaFixed=false;
	
	private boolean rendExWaitingPerdSelect=false;
	
	private boolean rendExBenefitPerSelect=false;
	
	private String defaultOccupation=null;
	
	private String defaultWaitingPeriod=null;
	
	private String defaultBenefitPeriod=null;
	
	private boolean disableExWaitingPeriod=false;
	
	private boolean disableExBenefitPeriod=false;
	
	private boolean disableAddnlWaitingPeriod=false;
	
	private boolean disableAddnlBenefitPeriod=false;
	
	private String addnlCvrTxtFieldInlineStyle=null;
	
	private String andnlCvrTxtLabelInlineStyle=null;
	
	private String addnlCvrRadioBtnsInlineStyle=null;
	
	private String exCvrAmtInlineStyle=null;
	
	private String addnlIpCvrTxtFldInlineStyle=null;
	
	private String addnlIpCvrLblInlineStyle=null;
	
	private String cvrTableWidth=null;
	
	private String memberTypeCellStyCls=null;
	
	private boolean disableIp=false;
	
	private boolean disableTpd=false;
	
	private boolean disableDeathSection=false;
	
	private boolean disableTpdSection=false;
	
	private boolean renderWarningSection=false;
	
	private String disclosureDeathCellStyCls = null;
	private String disclosureTPDCellStyCls = null;
	private String disclosureTraumaCellStyCls = null;
	private String disclosureIPCellStyCls = null;
	
	private boolean replaceExistingCvr=false;
	
	private boolean renderNewCoverTable=false;
	
	private boolean renderIngCoverTable=false;
	
	private boolean fulCover=false;
	
		
	private String meberCategeoryTokens=null;
	
	private String manageTypeTokens=null;
	
	private String uploadTypeTokens=null;
	
	private boolean transferExtInsCvrFlag = false;
	
	private String deathCoverTypeTokens=null;
	
	private String tpdCoverTypeTokens=null;
	
	private String ipCoverTypeTokens=null;
	
	private boolean addressSectionRenderFlag=false;
	
	private boolean deathTpdCoverRelationFlag = false;
	
	private boolean planCoverAmntRender = false;
	
	private boolean askManualFlag=false;/*This flag will be used to identify*/
	
	private boolean askProfessionalFlag=false;/*This flag will be used to identify*/
	
	private boolean otherOccupationRend = false;
	
	private boolean rendAddnlOccupTxtFld=false;
	
	private boolean renderDeclarationSection = false;
	
	private boolean breakDeathTpdRelation=false;
	
	private String annualSalForUpgradeVal = null;
	
	private boolean savePopUpButtonRender = false;
	
	private boolean autoQuoteCalculate = false;
	
	private boolean dobTxtFldRender=false;
	
	private boolean manageTypeRender = false;
	
	private String manageType=null;
	
	private boolean cancelCheckBoxRender = false;
	
	private boolean hrsPerWeekRender = false;
	
	private boolean nameOfPrevFundRender = false;
	
	private boolean fundMemInsPolicyNumRender = false;
	
	private boolean spinNumberRender = false;
	
	private boolean specialOfferTpdRender = false;
	
	private boolean regularIncomeRender = false;
	
	private boolean withOutLimitationRender = false;
	
	private boolean identifiableDutiesRender = false;
	
	private boolean lifeEventRsnRender = false;
	
	private boolean lifeEventDateRender = false;
	
	private String lifeEventReasons=null;
	
	private boolean coverTableRender = false;
	
	private boolean transferCoverRender=false;
	
	private boolean q8_PROFF_Q_Render=false;
	
	private boolean q9_PROFF_Q_Render=false;
	
	private boolean q10_PROFF_Q_Render=false;
	
	private boolean genConsentRender=true;
	private boolean privacyRender=true;
	private boolean dutyDisRender=true;
	
	private boolean quickQuoteRender = false;
	
	private BigDecimal deathTpdLifeEventThreshold;
	
	private BigDecimal deathTpdLifeEventMaxAfterThreshold;
	
	private int tpdLifeEventThresholdAge;
	
	private BigDecimal tpdLifeEventMaxAfterThrAge;
	
	private BigDecimal tpdLifeEventMaxUnitsAfterThrAge;
	
	private BigDecimal deathTpdLifeEventSalMultiplier;
	
	private int disableCoverRadiosAfterAge=0;
	
	private boolean lifeEventIncreaseCvrRender =  false;
	
	private BigDecimal lifeEventUnitsIncrement=null;
	
	private BigDecimal lifeEventFixedIncrement=null;
	
	private boolean cvrSummaryEmailRender=true;
	
	private boolean disableFixedRadioBtns=false;
	
	private BigDecimal annualSalMaxLmt=null;
	
	private int benefitPeriodMaxAge = 0;
	
	private int ipEligibleMaxAge = 0;
	
	/*MTAA Changes Start*/
	private double deathUnitCostGrtThan27=0;
	
	private double tpdUnitCostGrtThan27=0;
	/*MTAA Changes End*/
	
	public int getIpEligibleMaxAge() {
		return ipEligibleMaxAge;
	}

	public void setIpEligibleMaxAge(int ipEligibleMaxAge) {
		this.ipEligibleMaxAge = ipEligibleMaxAge;
	}

	public int getBenefitPeriodMaxAge() {
		return benefitPeriodMaxAge;
	}

	public void setBenefitPeriodMaxAge(int benefitPeriodMaxAge) {
		this.benefitPeriodMaxAge = benefitPeriodMaxAge;
	}

	public BigDecimal getAnnualSalMaxLmt() {
		return annualSalMaxLmt;
	}

	public void setAnnualSalMaxLmt(BigDecimal annualSalMaxLmt) {
		this.annualSalMaxLmt = annualSalMaxLmt;
	}

	public boolean isDisableFixedRadioBtns() {
		return disableFixedRadioBtns;
	}

	public void setDisableFixedRadioBtns(boolean disableFixedRadioBtns) {
		this.disableFixedRadioBtns = disableFixedRadioBtns;
	}

	public BigDecimal getLifeEventFixedIncrement() {
		return lifeEventFixedIncrement;
	}

	public void setLifeEventFixedIncrement(BigDecimal lifeEventFixedIncrement) {
		this.lifeEventFixedIncrement = lifeEventFixedIncrement;
	}

	public BigDecimal getLifeEventUnitsIncrement() {
		return lifeEventUnitsIncrement;
	}

	public void setLifeEventUnitsIncrement(BigDecimal lifeEventUnitsIncrement) {
		this.lifeEventUnitsIncrement = lifeEventUnitsIncrement;
	}

	public int getDisableCoverRadiosAfterAge() {
		return disableCoverRadiosAfterAge;
	}

	public void setDisableCoverRadiosAfterAge(int disableCoverRadiosAfterAge) {
		this.disableCoverRadiosAfterAge = disableCoverRadiosAfterAge;
	}

	public boolean isTransferCoverRender() {
		return transferCoverRender;
	}

	public void setTransferCoverRender(boolean transferCoverRender) {
		this.transferCoverRender = transferCoverRender;
	}

	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public boolean isDobTxtFldRender() {
		return dobTxtFldRender;
	}

	public void setDobTxtFldRender(boolean dobTxtFldRender) {
		this.dobTxtFldRender = dobTxtFldRender;
	}

	public boolean isBreakDeathTpdRelation() {
		return breakDeathTpdRelation;
	}

	public void setBreakDeathTpdRelation(boolean breakDeathTpdRelation) {
		this.breakDeathTpdRelation = breakDeathTpdRelation;
	}

	public boolean isRendAddnlOccupTxtFld() {
		return rendAddnlOccupTxtFld;
	}

	public void setRendAddnlOccupTxtFld(boolean rendAddnlOccupTxtFld) {
		this.rendAddnlOccupTxtFld = rendAddnlOccupTxtFld;
	}

	public String getDeathCoverTypeTokens() {
		return deathCoverTypeTokens;
	}

	public void setDeathCoverTypeTokens(String deathCoverTypeTokens) {
		this.deathCoverTypeTokens = deathCoverTypeTokens;
	}

	public String getIpCoverTypeTokens() {
		return ipCoverTypeTokens;
	}

	public void setIpCoverTypeTokens(String ipCoverTypeTokens) {
		this.ipCoverTypeTokens = ipCoverTypeTokens;
	}

	public String getTpdCoverTypeTokens() {
		return tpdCoverTypeTokens;
	}

	public void setTpdCoverTypeTokens(String tpdCoverTypeTokens) {
		this.tpdCoverTypeTokens = tpdCoverTypeTokens;
	}

	public boolean isReplaceExistingCvr() {
		return replaceExistingCvr;
	}

	public void setReplaceExistingCvr(boolean replaceExistingCvr) {
		this.replaceExistingCvr = replaceExistingCvr;
	}

	public String getDisclosureDeathCellStyCls() {
		return disclosureDeathCellStyCls;
	}

	public void setDisclosureDeathCellStyCls(String disclosureDeathCellStyCls) {
		this.disclosureDeathCellStyCls = disclosureDeathCellStyCls;
	}

	public String getDisclosureIPCellStyCls() {
		return disclosureIPCellStyCls;
	}

	public void setDisclosureIPCellStyCls(String disclosureIPCellStyCls) {
		this.disclosureIPCellStyCls = disclosureIPCellStyCls;
	}

	public String getDisclosureTPDCellStyCls() {
		return disclosureTPDCellStyCls;
	}

	public void setDisclosureTPDCellStyCls(String disclosureTPDCellStyCls) {
		this.disclosureTPDCellStyCls = disclosureTPDCellStyCls;
	}

	public String getDisclosureTraumaCellStyCls() {
		return disclosureTraumaCellStyCls;
	}

	public void setDisclosureTraumaCellStyCls(String disclosureTraumaCellStyCls) {
		this.disclosureTraumaCellStyCls = disclosureTraumaCellStyCls;
	}

	public boolean isRenderWarningSection() {
		return renderWarningSection;
	}

	public void setRenderWarningSection(boolean renderWarningSection) {
		this.renderWarningSection = renderWarningSection;
	}

	public boolean isDisableIp() {
		return disableIp;
	}

	public void setDisableIp(boolean disableIp) {
		this.disableIp = disableIp;
	}

	public boolean isDisableTpd() {
		return disableTpd;
	}

	public void setDisableTpd(boolean disableTpd) {
		this.disableTpd = disableTpd;
	}

	public String getMemberTypeCellStyCls() {
		return memberTypeCellStyCls;
	}

	public void setMemberTypeCellStyCls(String memberTypeCellStyCls) {
		this.memberTypeCellStyCls = memberTypeCellStyCls;
	}

	public String getAddnlCvrRadioBtnsInlineStyle() {
		return addnlCvrRadioBtnsInlineStyle;
	}

	public void setAddnlCvrRadioBtnsInlineStyle(String addnlCvrRadioBtnsInlineStyle) {
		this.addnlCvrRadioBtnsInlineStyle = addnlCvrRadioBtnsInlineStyle;
	}

	public String getAddnlCvrTxtFieldInlineStyle() {
		return addnlCvrTxtFieldInlineStyle;
	}

	public void setAddnlCvrTxtFieldInlineStyle(String addnlCvrTxtFieldInlineStyle) {
		this.addnlCvrTxtFieldInlineStyle = addnlCvrTxtFieldInlineStyle;
	}

	public String getAndnlCvrTxtLabelInlineStyle() {
		return andnlCvrTxtLabelInlineStyle;
	}

	public void setAndnlCvrTxtLabelInlineStyle(String andnlCvrTxtLabelInlineStyle) {
		this.andnlCvrTxtLabelInlineStyle = andnlCvrTxtLabelInlineStyle;
	}

	public String getCvrTableWidth() {
		return cvrTableWidth;
	}

	public void setCvrTableWidth(String cvrTableWidth) {
		this.cvrTableWidth = cvrTableWidth;
	}

	public String getExCvrAmtInlineStyle() {
		return exCvrAmtInlineStyle;
	}

	public void setExCvrAmtInlineStyle(String exCvrAmtInlineStyle) {
		this.exCvrAmtInlineStyle = exCvrAmtInlineStyle;
	}

	public boolean isRendExBenefitPerSelect() {
		return rendExBenefitPerSelect;
	}

	public void setRendExBenefitPerSelect(boolean rendExBenefitPerSelect) {
		this.rendExBenefitPerSelect = rendExBenefitPerSelect;
	}

	public boolean isRendExWaitingPerdSelect() {
		return rendExWaitingPerdSelect;
	}

	public void setRendExWaitingPerdSelect(boolean rendExWaitingPerdSelect) {
		this.rendExWaitingPerdSelect = rendExWaitingPerdSelect;
	}

	public boolean isDisableDcFixed() {
		return disableDcFixed;
	}

	public void setDisableDcFixed(boolean disableDcFixed) {
		this.disableDcFixed = disableDcFixed;
	}

	public boolean isDisableDcUnitised() {
		return disableDcUnitised;
	}

	public void setDisableDcUnitised(boolean disableDcUnitised) {
		this.disableDcUnitised = disableDcUnitised;
	}

	public boolean isDisableTpdFixed() {
		return disableTpdFixed;
	}

	public void setDisableTpdFixed(boolean disableTpdFixed) {
		this.disableTpdFixed = disableTpdFixed;
	}

	public boolean isDisableTpdUnitised() {
		return disableTpdUnitised;
	}

	public void setDisableTpdUnitised(boolean disableTpdUnitised) {
		this.disableTpdUnitised = disableTpdUnitised;
	}

	public boolean isDisableTraumaFixed() {
		return disableTraumaFixed;
	}

	public void setDisableTraumaFixed(boolean disableTraumaFixed) {
		this.disableTraumaFixed = disableTraumaFixed;
	}

	public boolean isDisableTraumaUnitised() {
		return disableTraumaUnitised;
	}

	public void setDisableTraumaUnitised(boolean disableTraumaUnitised) {
		this.disableTraumaUnitised = disableTraumaUnitised;
	}

	public boolean isRendIpRadioBtns() {
		return rendIpRadioBtns;
	}

	public void setRendIpRadioBtns(boolean rendIpRadioBtns) {
		this.rendIpRadioBtns = rendIpRadioBtns;
	}

	public double getDeathUnitCostMultipFactor() {
		return deathUnitCostMultipFactor;
	}

	public void setDeathUnitCostMultipFactor(double deathUnitCostMultipFactor) {
		this.deathUnitCostMultipFactor = deathUnitCostMultipFactor;
	}

	public double getIpUnitCostMultipFactor() {
		return ipUnitCostMultipFactor;
	}

	public void setIpUnitCostMultipFactor(double ipUnitCostMultipFactor) {
		this.ipUnitCostMultipFactor = ipUnitCostMultipFactor;
	}

	public double getTpdUnitCostMultipFactor() {
		return tpdUnitCostMultipFactor;
	}

	public void setTpdUnitCostMultipFactor(double tpdUnitCostMultipFactor) {
		this.tpdUnitCostMultipFactor = tpdUnitCostMultipFactor;
	}

	public double getTraumaUnitCostMultpFactor() {
		return traumaUnitCostMultpFactor;
	}

	public void setTraumaUnitCostMultpFactor(double traumaUnitCostMultpFactor) {
		this.traumaUnitCostMultpFactor = traumaUnitCostMultpFactor;
	}

	public int getDeathMaxAge() {
		return deathMaxAge;
	}

	public void setDeathMaxAge(int deathMaxAge) {
		this.deathMaxAge = deathMaxAge;
	}

	public BigDecimal getDeathMaxAmt() {
		return deathMaxAmt;
	}

	public void setDeathMaxAmt(BigDecimal deathMaxAmt) {
		this.deathMaxAmt = deathMaxAmt;
	}

	public int getDeathMinAge() {
		return deathMinAge;
	}

	public void setDeathMinAge(int deathMinAge) {
		this.deathMinAge = deathMinAge;
	}

	public BigDecimal getDeathMinAmt() {
		return deathMinAmt;
	}

	public void setDeathMinAmt(BigDecimal deathMinAmt) {
		this.deathMinAmt = deathMinAmt;
	}

	public int getIpMaxAge() {
		return ipMaxAge;
	}

	public void setIpMaxAge(int ipMaxAge) {
		this.ipMaxAge = ipMaxAge;
	}

	public BigDecimal getIpMaxAmt() {
		return ipMaxAmt;
	}

	public void setIpMaxAmt(BigDecimal ipMaxAmt) {
		this.ipMaxAmt = ipMaxAmt;
	}

	public int getIpMinAge() {
		return ipMinAge;
	}

	public void setIpMinAge(int ipMinAge) {
		this.ipMinAge = ipMinAge;
	}

	public BigDecimal getIpMinAmt() {
		return ipMinAmt;
	}

	public void setIpMinAmt(BigDecimal ipMinAmt) {
		this.ipMinAmt = ipMinAmt;
	}

	public int getTpdMaxAge() {
		return tpdMaxAge;
	}

	public void setTpdMaxAge(int tpdMaxAge) {
		this.tpdMaxAge = tpdMaxAge;
	}

	public BigDecimal getTpdMaxAmt() {
		return tpdMaxAmt;
	}

	public void setTpdMaxAmt(BigDecimal tpdMaxAmt) {
		this.tpdMaxAmt = tpdMaxAmt;
	}

	public int getTpdMinAge() {
		return tpdMinAge;
	}

	public void setTpdMinAge(int tpdMinAge) {
		this.tpdMinAge = tpdMinAge;
	}

	public BigDecimal getTpdMinAmt() {
		return tpdMinAmt;
	}

	public void setTpdMinAmt(BigDecimal tpdMinAmt) {
		this.tpdMinAmt = tpdMinAmt;
	}

	public int getTraumaMaxAge() {
		return traumaMaxAge;
	}

	public void setTraumaMaxAge(int traumaMaxAge) {
		this.traumaMaxAge = traumaMaxAge;
	}

	public BigDecimal getTraumaMaxAmt() {
		return traumaMaxAmt;
	}

	public void setTraumaMaxAmt(BigDecimal traumaMaxAmt) {
		this.traumaMaxAmt = traumaMaxAmt;
	}

	public int getTraumaMinAge() {
		return traumaMinAge;
	}

	public void setTraumaMinAge(int traumaMinAge) {
		this.traumaMinAge = traumaMinAge;
	}

	public BigDecimal getTraumaMinAmt() {
		return traumaMinAmt;
	}

	public void setTraumaMinAmt(BigDecimal traumaMinAmt) {
		this.traumaMinAmt = traumaMinAmt;
	}

	public boolean isRenderTraumaSection() {
		return renderTraumaSection;
	}

	public void setRenderTraumaSection(boolean renderTraumaSection) {
		this.renderTraumaSection = renderTraumaSection;
	}

	public boolean isRendExDtTxtFld() {
		return rendExDtTxtFld;
	}

	public void setRendExDtTxtFld(boolean rendExDtTxtFld) {
		this.rendExDtTxtFld = rendExDtTxtFld;
	}

	public boolean isRendExIpTxtFld() {
		return rendExIpTxtFld;
	}

	public void setRendExIpTxtFld(boolean rendExIpTxtFld) {
		this.rendExIpTxtFld = rendExIpTxtFld;
	}

	public boolean isRendExTpdTxtFld() {
		return rendExTpdTxtFld;
	}

	public void setRendExTpdTxtFld(boolean rendExTpdTxtFld) {
		this.rendExTpdTxtFld = rendExTpdTxtFld;
	}

	public boolean isRendExTraumaTxtFld() {
		return rendExTraumaTxtFld;
	}

	public void setRendExTraumaTxtFld(boolean rendExTraumaTxtFld) {
		this.rendExTraumaTxtFld = rendExTraumaTxtFld;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public boolean isFirstNameRenderFlag() {
		return firstNameRenderFlag;
	}

	public boolean isLastNameRenderFlag() {
		return lastNameRenderFlag;
	}

	public boolean isLevelCommissionRenderFlag() {
		return levelCommissionRenderFlag;
	}

	public boolean isPrintButtonRenderFlag() {
		return printButtonRenderFlag;
	}

	public boolean isSaveButtonRenderFlag() {
		return saveButtonRenderFlag;
	}

	public void setFirstNameRenderFlag(boolean firstNameRenderFlag) {
		this.firstNameRenderFlag = firstNameRenderFlag;
	}

	public void setLastNameRenderFlag(boolean lastNameRenderFlag) {
		this.lastNameRenderFlag = lastNameRenderFlag;
	}

	public void setLevelCommissionRenderFlag(boolean levelCommissionRenderFlag) {
		this.levelCommissionRenderFlag = levelCommissionRenderFlag;
	}

	public void setPrintButtonRenderFlag(boolean printButtonRenderFlag) {
		this.printButtonRenderFlag = printButtonRenderFlag;
	}

	public void setSaveButtonRenderFlag(boolean saveButtonRenderFlag) {
		this.saveButtonRenderFlag = saveButtonRenderFlag;
	}

	public String getFirstNameCellStyCls() {
		return firstNameCellStyCls;
	}

	public void setFirstNameCellStyCls(String firstNameCellStyCls) {
		this.firstNameCellStyCls = firstNameCellStyCls;
	}

	public String getLastNameCellStyCls() {
		return lastNameCellStyCls;
	}

	public void setLastNameCellStyCls(String lastNameCellStyCls) {
		this.lastNameCellStyCls = lastNameCellStyCls;
	}	

	public String getLevelCommissionCellStyCls() {
		return levelCommissionCellStyCls;
	}

	public void setLevelCommissionCellStyCls(String levelCommissionCellStyCls) {
		this.levelCommissionCellStyCls = levelCommissionCellStyCls;
	}

	public boolean isIpFixedRadio() {
		return ipFixedRadio;
	}

	public void setIpFixedRadio(boolean ipFixedRadio) {
		this.ipFixedRadio = ipFixedRadio;
	}

	public boolean isIpUnitisedRadio() {
		return ipUnitisedRadio;
	}

	public void setIpUnitisedRadio(boolean ipUnitisedRadio) {
		this.ipUnitisedRadio = ipUnitisedRadio;
	}

	public boolean isIpUnitisedFixedRadio() {
		return ipUnitisedFixedRadio;
	}

	public void setIpUnitisedFixedRadio(boolean ipUnitisedFixedRadio) {
		this.ipUnitisedFixedRadio = ipUnitisedFixedRadio;
	}

	/**
	 * @return the occupDisableFlag
	 */
	public boolean isOccupDisableFlag() {
		return occupDisableFlag;
	}

	/**
	 * @param occupDisableFlag the occupDisableFlag to set
	 */
	public void setOccupDisableFlag(boolean occupDisableFlag) {
		this.occupDisableFlag = occupDisableFlag;
	}

	/**
	 * @return the questionIdsList
	 */
	public List getQuestionIdsList() {
		return questionIdsList;
	}

	/**
	 * @param questionIdsList the questionIdsList to set
	 */
	public void setQuestionIdsList(List questionIdsList) {
		this.questionIdsList = questionIdsList;
	}

	/**
	 * @return the defaultPremiumFreq
	 */
	public String getDefaultPremiumFreq() {
		return defaultPremiumFreq;
	}

	/**
	 * @param defaultPremiumFreq the defaultPremiumFreq to set
	 */
	public void setDefaultPremiumFreq(String defaultPremiumFreq) {
		this.defaultPremiumFreq = defaultPremiumFreq;
	}

	/**
	 * @return the premiumFrequencyTokens
	 */
	public String getPremiumFrequencyTokens() {
		return premiumFrequencyTokens;
	}

	/**
	 * @param premiumFrequencyTokens the premiumFrequencyTokens to set
	 */
	public void setPremiumFrequencyTokens(String premiumFrequencyTokens) {
		this.premiumFrequencyTokens = premiumFrequencyTokens;
	}

	/**
	 * @return the citizenShipRenderFlag
	 */
	public boolean isCitizenShipRenderFlag() {
		return citizenShipRenderFlag;
	}

	/**
	 * @param citizenShipRenderFlag the citizenShipRenderFlag to set
	 */
	public void setCitizenShipRenderFlag(boolean citizenShipRenderFlag) {
		this.citizenShipRenderFlag = citizenShipRenderFlag;
	}

	/**
	 * @return the dobRenderFlag
	 */
	public boolean isDobRenderFlag() {
		return dobRenderFlag;
	}

	/**
	 * @param dobRenderFlag the dobRenderFlag to set
	 */
	public void setDobRenderFlag(boolean dobRenderFlag) {
		this.dobRenderFlag = dobRenderFlag;
	}

	/**
	 * @return the genderRenderFlag
	 */
	public boolean isGenderRenderFlag() {
		return genderRenderFlag;
	}

	/**
	 * @param genderRenderFlag the genderRenderFlag to set
	 */
	public void setGenderRenderFlag(boolean genderRenderFlag) {
		this.genderRenderFlag = genderRenderFlag;
	}

	/**
	 * @return the partnerShipRenderFlag
	 */
	public boolean isPartnerShipRenderFlag() {
		return partnerShipRenderFlag;
	}

	/**
	 * @param partnerShipRenderFlag the partnerShipRenderFlag to set
	 */
	public void setPartnerShipRenderFlag(boolean partnerShipRenderFlag) {
		this.partnerShipRenderFlag = partnerShipRenderFlag;
	}

	/**
	 * @return the activelyEmpRenderFlag
	 */
	public boolean isActivelyEmpRenderFlag() {
		return activelyEmpRenderFlag;
	}

	/**
	 * @param activelyEmpRenderFlag the activelyEmpRenderFlag to set
	 */
	public void setActivelyEmpRenderFlag(boolean activelyEmpRenderFlag) {
		this.activelyEmpRenderFlag = activelyEmpRenderFlag;
	}

	/**
	 * @return the anualSalRenderFlag
	 */
	public boolean isAnualSalRenderFlag() {
		return anualSalRenderFlag;
	}

	/**
	 * @param anualSalRenderFlag the anualSalRenderFlag to set
	 */
	public void setAnualSalRenderFlag(boolean anualSalRenderFlag) {
		this.anualSalRenderFlag = anualSalRenderFlag;
	}

	/**
	 * @return the insuranceCoverFreqRenderFlag
	 */
	public boolean isInsuranceCoverFreqRenderFlag() {
		return insuranceCoverFreqRenderFlag;
	}

	/**
	 * @param insuranceCoverFreqRenderFlag the insuranceCoverFreqRenderFlag to set
	 */
	public void setInsuranceCoverFreqRenderFlag(boolean insuranceCoverFreqRenderFlag) {
		this.insuranceCoverFreqRenderFlag = insuranceCoverFreqRenderFlag;
	}

	/**
	 * @return the moreThan15HrsRenderFlag
	 */
	public boolean isMoreThan15HrsRenderFlag() {
		return moreThan15HrsRenderFlag;
	}

	/**
	 * @param moreThan15HrsRenderFlag the moreThan15HrsRenderFlag to set
	 */
	public void setMoreThan15HrsRenderFlag(boolean moreThan15HrsRenderFlag) {
		this.moreThan15HrsRenderFlag = moreThan15HrsRenderFlag;
	}

	/**
	 * @return the moreThan15HrsTreeRenderFlag
	 */
	public boolean isMoreThan15HrsTreeRenderFlag() {
		return moreThan15HrsTreeRenderFlag;
	}

	/**
	 * @param moreThan15HrsTreeRenderFlag the moreThan15HrsTreeRenderFlag to set
	 */
	public void setMoreThan15HrsTreeRenderFlag(boolean moreThan15HrsTreeRenderFlag) {
		this.moreThan15HrsTreeRenderFlag = moreThan15HrsTreeRenderFlag;
	}

	/**
	 * @return the occupationRenderFlag
	 */
	public boolean isOccupationRenderFlag() {
		return occupationRenderFlag;
	}

	/**
	 * @param occupationRenderFlag the occupationRenderFlag to set
	 */
	public void setOccupationRenderFlag(boolean occupationRenderFlag) {
		this.occupationRenderFlag = occupationRenderFlag;
	}

	/**
	 * @return the permanentEmpMoreThan15HrsRenderFlag
	 */
	public boolean isPermanentEmpMoreThan15HrsRenderFlag() {
		return permanentEmpMoreThan15HrsRenderFlag;
	}

	/**
	 * @param permanentEmpMoreThan15HrsRenderFlag the permanentEmpMoreThan15HrsRenderFlag to set
	 */
	public void setPermanentEmpMoreThan15HrsRenderFlag(
			boolean permanentEmpMoreThan15HrsRenderFlag) {
		this.permanentEmpMoreThan15HrsRenderFlag = permanentEmpMoreThan15HrsRenderFlag;
	}

	/**
	 * @return the permanentEmpRenderFlag
	 */
	public boolean isPermanentEmpRenderFlag() {
		return permanentEmpRenderFlag;
	}

	/**
	 * @param permanentEmpRenderFlag the permanentEmpRenderFlag to set
	 */
	public void setPermanentEmpRenderFlag(boolean permanentEmpRenderFlag) {
		this.permanentEmpRenderFlag = permanentEmpRenderFlag;
	}

	/**
	 * @return the permanentEmpTreeRenderFlag
	 */
	public boolean isPermanentEmpTreeRenderFlag() {
		return permanentEmpTreeRenderFlag;
	}

	/**
	 * @param permanentEmpTreeRenderFlag the permanentEmpTreeRenderFlag to set
	 */
	public void setPermanentEmpTreeRenderFlag(boolean permanentEmpTreeRenderFlag) {
		this.permanentEmpTreeRenderFlag = permanentEmpTreeRenderFlag;
	}

	/**
	 * @return the replaceExistRenderFlag
	 */
	public boolean isReplaceExistRenderFlag() {
		return replaceExistRenderFlag;
	}

	/**
	 * @param replaceExistRenderFlag the replaceExistRenderFlag to set
	 */
	public void setReplaceExistRenderFlag(boolean replaceExistRenderFlag) {
		this.replaceExistRenderFlag = replaceExistRenderFlag;
	}

	/**
	 * @return the smokerRenderFlag
	 */
	public boolean isSmokerRenderFlag() {
		return smokerRenderFlag;
	}

	/**
	 * @param smokerRenderFlag the smokerRenderFlag to set
	 */
	public void setSmokerRenderFlag(boolean smokerRenderFlag) {
		this.smokerRenderFlag = smokerRenderFlag;
	}

	/**
	 * @return the rendCvrDetBrdCrumb
	 */
	public boolean isRendCvrDetBrdCrumb() {
		return rendCvrDetBrdCrumb;
	}

	/**
	 * @param rendCvrDetBrdCrumb
	 *            the rendCvrDetBrdCrumb to set
	 */
	public void setRendCvrDetBrdCrumb(boolean rendCvrDetBrdCrumb) {
		this.rendCvrDetBrdCrumb = rendCvrDetBrdCrumb;
	}

	/**
	 * @return the rendCvrSummBrdCrumb
	 */
	public boolean isRendCvrSummBrdCrumb() {
		return rendCvrSummBrdCrumb;
	}

	/**
	 * @param rendCvrSummBrdCrumb
	 *            the rendCvrSummBrdCrumb to set
	 */
	public void setRendCvrSummBrdCrumb(boolean rendCvrSummBrdCrumb) {
		this.rendCvrSummBrdCrumb = rendCvrSummBrdCrumb;
	}

	/**
	 * @return the rendNextBtnInCvrSumm
	 */
	public boolean isRendNextBtnInCvrSumm() {
		return rendNextBtnInCvrSumm;
	}

	/**
	 * @param rendNextBtnInCvrSumm
	 *            the rendNextBtnInCvrSumm to set
	 */
	public void setRendNextBtnInCvrSumm(boolean rendNextBtnInCvrSumm) {
		this.rendNextBtnInCvrSumm = rendNextBtnInCvrSumm;
	}

	/**
	 * @return the rendNextBtnInPersDtls
	 */
	public boolean isRendNextBtnInPersDtls() {
		return rendNextBtnInPersDtls;
	}

	/**
	 * @param rendNextBtnInPersDtls
	 *            the rendNextBtnInPersDtls to set
	 */
	public void setRendNextBtnInPersDtls(boolean rendNextBtnInPersDtls) {
		this.rendNextBtnInPersDtls = rendNextBtnInPersDtls;
	}

	/**
	 * @return the rendPersDtlsBrdCrumb
	 */
	public boolean isRendPersDtlsBrdCrumb() {
		return rendPersDtlsBrdCrumb;
	}

	/**
	 * @param rendPersDtlsBrdCrumb
	 *            the rendPersDtlsBrdCrumb to set
	 */
	public void setRendPersDtlsBrdCrumb(boolean rendPersDtlsBrdCrumb) {
		this.rendPersDtlsBrdCrumb = rendPersDtlsBrdCrumb;
	}

	/**
	 * @return the rendPersStmtBrdCrumb
	 */
	public boolean isRendPersStmtBrdCrumb() {
		return rendPersStmtBrdCrumb;
	}

	/**
	 * @param rendPersStmtBrdCrumb
	 *            the rendPersStmtBrdCrumb to set
	 */
	public void setRendPersStmtBrdCrumb(boolean rendPersStmtBrdCrumb) {
		this.rendPersStmtBrdCrumb = rendPersStmtBrdCrumb;
	}

	/**
	 * @return the rendSaveBtnInCvrSumm
	 */
	public boolean isRendSaveBtnInCvrSumm() {
		return rendSaveBtnInCvrSumm;
	}

	/**
	 * @param rendSaveBtnInCvrSumm
	 *            the rendSaveBtnInCvrSumm to set
	 */
	public void setRendSaveBtnInCvrSumm(boolean rendSaveBtnInCvrSumm) {
		this.rendSaveBtnInCvrSumm = rendSaveBtnInCvrSumm;
	}

	/**
	 * @return the rendSaveBtnInPersDtls
	 */
	public boolean isRendSaveBtnInPersDtls() {
		return rendSaveBtnInPersDtls;
	}

	/**
	 * @param rendSaveBtnInPersDtls
	 *            the rendSaveBtnInPersDtls to set
	 */
	public void setRendSaveBtnInPersDtls(boolean rendSaveBtnInPersDtls) {
		this.rendSaveBtnInPersDtls = rendSaveBtnInPersDtls;
	}

	/**
	 * @return the rendSubmitBtnInCvrSumm
	 */
	public boolean isRendSubmitBtnInCvrSumm() {
		return rendSubmitBtnInCvrSumm;
	}

	/**
	 * @param rendSubmitBtnInCvrSumm
	 *            the rendSubmitBtnInCvrSumm to set
	 */
	public void setRendSubmitBtnInCvrSumm(boolean rendSubmitBtnInCvrSumm) {
		this.rendSubmitBtnInCvrSumm = rendSubmitBtnInCvrSumm;
	}

	/**
	 * @return the rendSubmitBtnInPersDtls
	 */
	public boolean isRendSubmitBtnInPersDtls() {
		return rendSubmitBtnInPersDtls;
	}

	/**
	 * @param rendSubmitBtnInPersDtls
	 *            the rendSubmitBtnInPersDtls to set
	 */
	public void setRendSubmitBtnInPersDtls(boolean rendSubmitBtnInPersDtls) {
		this.rendSubmitBtnInPersDtls = rendSubmitBtnInPersDtls;
	}

	/**
	 * @return the ruleFileName
	 */
	public String getRuleFileName() {
		return ruleFileName;
	}

	/**
	 * @param ruleFileName
	 *            the ruleFileName to set
	 */
	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode
	 *            the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the activelyEmpRCellStyCls
	 */
	public String getActivelyEmpRCellStyCls() {
		return activelyEmpRCellStyCls;
	}

	/**
	 * @param activelyEmpRCellStyCls the activelyEmpRCellStyCls to set
	 */
	public void setActivelyEmpRCellStyCls(String activelyEmpRCellStyCls) {
		this.activelyEmpRCellStyCls = activelyEmpRCellStyCls;
	}

	/**
	 * @return the anualSalCellStyCls
	 */
	public String getAnualSalCellStyCls() {
		return anualSalCellStyCls;
	}

	/**
	 * @param anualSalCellStyCls the anualSalCellStyCls to set
	 */
	public void setAnualSalCellStyCls(String anualSalCellStyCls) {
		this.anualSalCellStyCls = anualSalCellStyCls;
	}

	/**
	 * @return the citizenShipCellStyCls
	 */
	public String getCitizenShipCellStyCls() {
		return citizenShipCellStyCls;
	}

	/**
	 * @param citizenShipCellStyCls the citizenShipCellStyCls to set
	 */
	public void setCitizenShipCellStyCls(String citizenShipCellStyCls) {
		this.citizenShipCellStyCls = citizenShipCellStyCls;
	}

	/**
	 * @return the dobCellStyCls
	 */
	public String getDobCellStyCls() {
		return dobCellStyCls;
	}

	/**
	 * @param dobCellStyCls the dobCellStyCls to set
	 */
	public void setDobCellStyCls(String dobCellStyCls) {
		this.dobCellStyCls = dobCellStyCls;
	}

	/**
	 * @return the genderCellStyCls
	 */
	public String getGenderCellStyCls() {
		return genderCellStyCls;
	}

	/**
	 * @param genderCellStyCls the genderCellStyCls to set
	 */
	public void setGenderCellStyCls(String genderCellStyCls) {
		this.genderCellStyCls = genderCellStyCls;
	}

	/**
	 * @return the insuranceCoverFreqCellStyCls
	 */
	public String getInsuranceCoverFreqCellStyCls() {
		return insuranceCoverFreqCellStyCls;
	}

	/**
	 * @param insuranceCoverFreqCellStyCls the insuranceCoverFreqCellStyCls to set
	 */
	public void setInsuranceCoverFreqCellStyCls(String insuranceCoverFreqCellStyCls) {
		this.insuranceCoverFreqCellStyCls = insuranceCoverFreqCellStyCls;
	}

	/**
	 * @return the moreThan15HrsCellStyCls
	 */
	public String getMoreThan15HrsCellStyCls() {
		return moreThan15HrsCellStyCls;
	}

	/**
	 * @param moreThan15HrsCellStyCls the moreThan15HrsCellStyCls to set
	 */
	public void setMoreThan15HrsCellStyCls(String moreThan15HrsCellStyCls) {
		this.moreThan15HrsCellStyCls = moreThan15HrsCellStyCls;
	}

	/**
	 * @return the moreThan15HrsTreeCellStyCls
	 */
	public String getMoreThan15HrsTreeCellStyCls() {
		return moreThan15HrsTreeCellStyCls;
	}

	/**
	 * @param moreThan15HrsTreeCellStyCls the moreThan15HrsTreeCellStyCls to set
	 */
	public void setMoreThan15HrsTreeCellStyCls(String moreThan15HrsTreeCellStyCls) {
		this.moreThan15HrsTreeCellStyCls = moreThan15HrsTreeCellStyCls;
	}

	/**
	 * @return the occupCellStyCls
	 */
	public String getOccupCellStyCls() {
		return occupCellStyCls;
	}

	/**
	 * @param occupCellStyCls the occupCellStyCls to set
	 */
	public void setOccupCellStyCls(String occupCellStyCls) {
		this.occupCellStyCls = occupCellStyCls;
	}

	/**
	 * @return the partnerShipCellStyCls
	 */
	public String getPartnerShipCellStyCls() {
		return partnerShipCellStyCls;
	}

	/**
	 * @param partnerShipCellStyCls the partnerShipCellStyCls to set
	 */
	public void setPartnerShipCellStyCls(String partnerShipCellStyCls) {
		this.partnerShipCellStyCls = partnerShipCellStyCls;
	}

	/**
	 * @return the permanentEmpMoreThan15HrsCellStyCls
	 */
	public String getPermanentEmpMoreThan15HrsCellStyCls() {
		return permanentEmpMoreThan15HrsCellStyCls;
	}

	/**
	 * @param permanentEmpMoreThan15HrsCellStyCls the permanentEmpMoreThan15HrsCellStyCls to set
	 */
	public void setPermanentEmpMoreThan15HrsCellStyCls(
			String permanentEmpMoreThan15HrsCellStyCls) {
		this.permanentEmpMoreThan15HrsCellStyCls = permanentEmpMoreThan15HrsCellStyCls;
	}

	/**
	 * @return the permanentEmpRCellStyCls
	 */
	public String getPermanentEmpRCellStyCls() {
		return permanentEmpRCellStyCls;
	}

	/**
	 * @param permanentEmpRCellStyCls the permanentEmpRCellStyCls to set
	 */
	public void setPermanentEmpRCellStyCls(String permanentEmpRCellStyCls) {
		this.permanentEmpRCellStyCls = permanentEmpRCellStyCls;
	}

	/**
	 * @return the permanentEmpTreeCellStyCls
	 */
	public String getPermanentEmpTreeCellStyCls() {
		return permanentEmpTreeCellStyCls;
	}

	/**
	 * @param permanentEmpTreeCellStyCls the permanentEmpTreeCellStyCls to set
	 */
	public void setPermanentEmpTreeCellStyCls(String permanentEmpTreeCellStyCls) {
		this.permanentEmpTreeCellStyCls = permanentEmpTreeCellStyCls;
	}

	/**
	 * @return the replaceExistCellStyCls
	 */
	public String getReplaceExistCellStyCls() {
		return replaceExistCellStyCls;
	}

	/**
	 * @param replaceExistCellStyCls the replaceExistCellStyCls to set
	 */
	public void setReplaceExistCellStyCls(String replaceExistCellStyCls) {
		this.replaceExistCellStyCls = replaceExistCellStyCls;
	}

	/**
	 * @return the smokerCellStyCls
	 */
	public String getSmokerCellStyCls() {
		return smokerCellStyCls;
	}

	/**
	 * @param smokerCellStyCls the smokerCellStyCls to set
	 */
	public void setSmokerCellStyCls(String smokerCellStyCls) {
		this.smokerCellStyCls = smokerCellStyCls;
	}

	/**
	 * @return the ipBenefitPeriodTokens
	 */
	public String getIpBenefitPeriodTokens() {
		return ipBenefitPeriodTokens;
	}

	/**
	 * @param ipBenefitPeriodTokens the ipBenefitPeriodTokens to set
	 */
	public void setIpBenefitPeriodTokens(String ipBenefitPeriodTokens) {
		this.ipBenefitPeriodTokens = ipBenefitPeriodTokens;
	}

	/**
	 * @return the ipWaitingPeriodTokens
	 */
	public String getIpWaitingPeriodTokens() {
		return ipWaitingPeriodTokens;
	}

	/**
	 * @param ipWaitingPeriodTokens the ipWaitingPeriodTokens to set
	 */
	public void setIpWaitingPeriodTokens(String ipWaitingPeriodTokens) {
		this.ipWaitingPeriodTokens = ipWaitingPeriodTokens;
	}

	/**
	 * @return the disablePremiumFreq
	 */
	public boolean isDisablePremiumFreq() {
		return disablePremiumFreq;
	}

	/**
	 * @param disablePremiumFreq the disablePremiumFreq to set
	 */
	public void setDisablePremiumFreq(boolean disablePremiumFreq) {
		this.disablePremiumFreq = disablePremiumFreq;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the deathCvrAmt
	 */
	public double getDeathCvrAmt() {
		return deathCvrAmt;
	}

	/**
	 * @param deathCvrAmt the deathCvrAmt to set
	 */
	public void setDeathCvrAmt(double deathCvrAmt) {
		this.deathCvrAmt = deathCvrAmt;
	}

	/**
	 * @return the ipCvrAmt
	 */
	public double getIpCvrAmt() {
		return ipCvrAmt;
	}

	/**
	 * @param ipCvrAmt the ipCvrAmt to set
	 */
	public void setIpCvrAmt(double ipCvrAmt) {
		this.ipCvrAmt = ipCvrAmt;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the tpdCvrAmt
	 */
	public double getTpdCvrAmt() {
		return tpdCvrAmt;
	}

	/**
	 * @param tpdCvrAmt the tpdCvrAmt to set
	 */
	public void setTpdCvrAmt(double tpdCvrAmt) {
		this.tpdCvrAmt = tpdCvrAmt;
	}

	/**
	 * @return the traumaCvrAmt
	 */
	public double getTraumaCvrAmt() {
		return traumaCvrAmt;
	}

	/**
	 * @param traumaCvrAmt the traumaCvrAmt to set
	 */
	public void setTraumaCvrAmt(double traumaCvrAmt) {
		this.traumaCvrAmt = traumaCvrAmt;
	}

	public boolean isAalRenderFlag() {
		return aalRenderFlag;
	}

	public void setAalRenderFlag(boolean aalRenderFlag) {
		this.aalRenderFlag = aalRenderFlag;
	}

	public String getAalRCellStyCls() {
		return aalRCellStyCls;
	}

	public void setAalRCellStyCls(String aalRCellStyCls) {
		this.aalRCellStyCls = aalRCellStyCls;
	}

	/*public StatelessKnowledgeSession getKSession() {
		return kSession;
	}

	public void setKSession(StatelessKnowledgeSession session) {
		kSession = session;
	}*/

	public KnowledgeBase getKbase() {
		return kbase;
	}

	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}

	public String getCurrentEmplydCellStyCls() {
		return currentEmplydCellStyCls;
	}

	public void setCurrentEmplydCellStyCls(String currentEmplydCellStyCls) {
		this.currentEmplydCellStyCls = currentEmplydCellStyCls;
	}

	public boolean isCurrentEmplydRenderFlag() {
		return currentEmplydRenderFlag;
	}

	public void setCurrentEmplydRenderFlag(boolean currentEmplydRenderFlag) {
		this.currentEmplydRenderFlag = currentEmplydRenderFlag;
	}

	public String getOccupDutiesCellStyCls() {
		return occupDutiesCellStyCls;
	}

	public void setOccupDutiesCellStyCls(String occupDutiesCellStyCls) {
		this.occupDutiesCellStyCls = occupDutiesCellStyCls;
	}

	public boolean isOccupDutiesRenderFlag() {
		return occupDutiesRenderFlag;
	}

	public void setOccupDutiesRenderFlag(boolean occupDutiesRenderFlag) {
		this.occupDutiesRenderFlag = occupDutiesRenderFlag;
	}

	public String getSalaryInsureCellStyCls() {
		return salaryInsureCellStyCls;
	}

	public void setSalaryInsureCellStyCls(String salaryInsureCellStyCls) {
		this.salaryInsureCellStyCls = salaryInsureCellStyCls;
	}

	public boolean isSalaryInsureRenderFlag() {
		return salaryInsureRenderFlag;
	}

	public void setSalaryInsureRenderFlag(boolean salaryInsureRenderFlag) {
		this.salaryInsureRenderFlag = salaryInsureRenderFlag;
	}

	public String getSpendTimeOutsideCellStyCls() {
		return spendTimeOutsideCellStyCls;
	}

	public void setSpendTimeOutsideCellStyCls(String spendTimeOutsideCellStyCls) {
		this.spendTimeOutsideCellStyCls = spendTimeOutsideCellStyCls;
	}

	public boolean isSpendTimeOutsideRenderFlag() {
		return spendTimeOutsideRenderFlag;
	}

	public void setSpendTimeOutsideRenderFlag(boolean spendTimeOutsideRenderFlag) {
		this.spendTimeOutsideRenderFlag = spendTimeOutsideRenderFlag;
	}

	public String getWorkDutiesCellStyCls() {
		return workDutiesCellStyCls;
	}

	public void setWorkDutiesCellStyCls(String workDutiesCellStyCls) {
		this.workDutiesCellStyCls = workDutiesCellStyCls;
	}

	public boolean isWorkDutiesRenderFlag() {
		return workDutiesRenderFlag;
	}

	public void setWorkDutiesRenderFlag(boolean workDutiesRenderFlag) {
		this.workDutiesRenderFlag = workDutiesRenderFlag;
	}

	public String getMemberTypeRenderCellStyCls() {
		return memberTypeRenderCellStyCls;
	}

	public void setMemberTypeRenderCellStyCls(String memberTypeRenderCellStyCls) {
		this.memberTypeRenderCellStyCls = memberTypeRenderCellStyCls;
	}

	public boolean isMemberTypeRenderFlag() {
		return memberTypeRenderFlag;
	}

	public void setMemberTypeRenderFlag(boolean memberTypeRenderFlag) {
		this.memberTypeRenderFlag = memberTypeRenderFlag;
	}

	public String getWorkDuties1CellStyCls() {
		return workDuties1CellStyCls;
	}

	public void setWorkDuties1CellStyCls(String workDuties1CellStyCls) {
		this.workDuties1CellStyCls = workDuties1CellStyCls;
	}

	public boolean isWorkDuties1RenderFlag() {
		return workDuties1RenderFlag;
	}

	public void setWorkDuties1RenderFlag(boolean workDuties1RenderFlag) {
		this.workDuties1RenderFlag = workDuties1RenderFlag;
	}

	public String getWorkDuties2CellStyCls() {
		return workDuties2CellStyCls;
	}

	public void setWorkDuties2CellStyCls(String workDuties2CellStyCls) {
		this.workDuties2CellStyCls = workDuties2CellStyCls;
	}

	public boolean isWorkDuties2RenderFlag() {
		return workDuties2RenderFlag;
	}

	public void setWorkDuties2RenderFlag(boolean workDuties2RenderFlag) {
		this.workDuties2RenderFlag = workDuties2RenderFlag;
	}

	public String getWorkDuties3CellStyCls() {
		return workDuties3CellStyCls;
	}

	public void setWorkDuties3CellStyCls(String workDuties3CellStyCls) {
		this.workDuties3CellStyCls = workDuties3CellStyCls;
	}

	public boolean isWorkDuties3RenderFlag() {
		return workDuties3RenderFlag;
	}

	public void setWorkDuties3RenderFlag(boolean workDuties3RenderFlag) {
		this.workDuties3RenderFlag = workDuties3RenderFlag;
	}

	public boolean isRendIpUnitIndicator() {
		return rendIpUnitIndicator;
	}

	public void setRendIpUnitIndicator(boolean rendIpUnitIndicator) {
		this.rendIpUnitIndicator = rendIpUnitIndicator;
	}

	public String getTeritoryQualCellStyCls() {
		return teritoryQualCellStyCls;
	}

	public void setTeritoryQualCellStyCls(String teritoryQualCellStyCls) {
		this.teritoryQualCellStyCls = teritoryQualCellStyCls;
	}

	public boolean isTeritoryQualRenderFlag() {
		return teritoryQualRenderFlag;
	}

	public void setTeritoryQualRenderFlag(boolean teritoryQualRenderFlag) {
		this.teritoryQualRenderFlag = teritoryQualRenderFlag;
	}

	public String getDefaultBenefitPeriod() {
		return defaultBenefitPeriod;
	}

	public void setDefaultBenefitPeriod(String defaultBenefitPeriod) {
		this.defaultBenefitPeriod = defaultBenefitPeriod;
	}

	public String getDefaultOccupation() {
		return defaultOccupation;
	}

	public void setDefaultOccupation(String defaultOccupation) {
		this.defaultOccupation = defaultOccupation;
	}

	public String getDefaultWaitingPeriod() {
		return defaultWaitingPeriod;
	}

	public void setDefaultWaitingPeriod(String defaultWaitingPeriod) {
		this.defaultWaitingPeriod = defaultWaitingPeriod;
	}

	public boolean isDisableAddnlBenefitPeriod() {
		return disableAddnlBenefitPeriod;
	}

	public void setDisableAddnlBenefitPeriod(boolean disableAddnlBenefitPeriod) {
		this.disableAddnlBenefitPeriod = disableAddnlBenefitPeriod;
	}

	public boolean isDisableAddnlWaitingPeriod() {
		return disableAddnlWaitingPeriod;
	}

	public void setDisableAddnlWaitingPeriod(boolean disableAddnlWaitingPeriod) {
		this.disableAddnlWaitingPeriod = disableAddnlWaitingPeriod;
	}

	public boolean isDisableExBenefitPeriod() {
		return disableExBenefitPeriod;
	}

	public void setDisableExBenefitPeriod(boolean disableExBenefitPeriod) {
		this.disableExBenefitPeriod = disableExBenefitPeriod;
	}

	public boolean isDisableExWaitingPeriod() {
		return disableExWaitingPeriod;
	}

	public void setDisableExWaitingPeriod(boolean disableExWaitingPeriod) {
		this.disableExWaitingPeriod = disableExWaitingPeriod;
	}

	public boolean isDisableipFixed() {
		return disableipFixed;
	}

	public void setDisableipFixed(boolean disableipFixed) {
		this.disableipFixed = disableipFixed;
	}

	public boolean isDisableIpUnitised() {
		return disableIpUnitised;
	}

	public void setDisableIpUnitised(boolean disableIpUnitised) {
		this.disableIpUnitised = disableIpUnitised;
	}

	public String getAddnlIpCvrLblInlineStyle() {
		return addnlIpCvrLblInlineStyle;
	}

	public void setAddnlIpCvrLblInlineStyle(String addnlIpCvrLblInlineStyle) {
		this.addnlIpCvrLblInlineStyle = addnlIpCvrLblInlineStyle;
	}

	public String getAddnlIpCvrTxtFldInlineStyle() {
		return addnlIpCvrTxtFldInlineStyle;
	}

	public void setAddnlIpCvrTxtFldInlineStyle(String addnlIpCvrTxtFldInlineStyle) {
		this.addnlIpCvrTxtFldInlineStyle = addnlIpCvrTxtFldInlineStyle;
	}

	public double getSfpsIpUnitCostProfMultFac() {
		return sfpsIpUnitCostProfMultFac;
	}

	public void setSfpsIpUnitCostProfMultFac(double sfpsIpUnitCostProfMultFac) {
		this.sfpsIpUnitCostProfMultFac = sfpsIpUnitCostProfMultFac;
	}

	public double getSfpsIpUnitCostStandardMultFac() {
		return sfpsIpUnitCostStandardMultFac;
	}

	public void setSfpsIpUnitCostStandardMultFac(
			double sfpsIpUnitCostStandardMultFac) {
		this.sfpsIpUnitCostStandardMultFac = sfpsIpUnitCostStandardMultFac;
	}

	public double getSfpsIpUnitCostWhiteColMultFac() {
		return sfpsIpUnitCostWhiteColMultFac;
	}

	public void setSfpsIpUnitCostWhiteColMultFac(
			double sfpsIpUnitCostWhiteColMultFac) {
		this.sfpsIpUnitCostWhiteColMultFac = sfpsIpUnitCostWhiteColMultFac;
	}

	public boolean isRendEightyFifyPerSal() {
		return rendEightyFifyPerSal;
	}

	public void setRendEightyFifyPerSal(boolean rendEightyFifyPerSal) {
		this.rendEightyFifyPerSal = rendEightyFifyPerSal;
	}

	public BigDecimal getIpMinAmtForDisp() {
		return ipMinAmtForDisp;
	}

	public void setIpMinAmtForDisp(BigDecimal ipMinAmtForDisp) {
		this.ipMinAmtForDisp = ipMinAmtForDisp;
	}

	public double getIpUnitCvrAmtFor2Yrs() {
		return ipUnitCvrAmtFor2Yrs;
	}

	public void setIpUnitCvrAmtFor2Yrs(double ipUnitCvrAmtFor2Yrs) {
		this.ipUnitCvrAmtFor2Yrs = ipUnitCvrAmtFor2Yrs;
	}

	public double getIpUnitCvrAmtForAge65() {
		return ipUnitCvrAmtForAge65;
	}

	public void setIpUnitCvrAmtForAge65(double ipUnitCvrAmtForAge65) {
		this.ipUnitCvrAmtForAge65 = ipUnitCvrAmtForAge65;
	}

	public BigDecimal getDeathMinUnits() {
		return deathMinUnits;
	}

	public void setDeathMinUnits(BigDecimal deathMinUnits) {
		this.deathMinUnits = deathMinUnits;
	}

	public BigDecimal getTpdMinUnits() {
		return tpdMinUnits;
	}

	public void setTpdMinUnits(BigDecimal tpdMinUnits) {
		this.tpdMinUnits = tpdMinUnits;
	}
	
	public String getReasonForApplRenderCellStyCls() {
		return reasonForApplRenderCellStyCls;
	}

	public void setReasonForApplRenderCellStyCls(
			String reasonForApplRenderCellStyCls) {
		this.reasonForApplRenderCellStyCls = reasonForApplRenderCellStyCls;
	}

	public boolean isReasonForApplRenderFlag() {
		return reasonForApplRenderFlag;
	}

	public void setReasonForApplRenderFlag(boolean reasonForApplRenderFlag) {
		this.reasonForApplRenderFlag = reasonForApplRenderFlag;
	}

	public String getReasonForApplTokens() {
		return reasonForApplTokens;
	}

	public void setReasonForApplTokens(String reasonForApplTokens) {
		this.reasonForApplTokens = reasonForApplTokens;
	}

	public BigDecimal getDeathMaxUnits() {
		return deathMaxUnits;
	}

	public void setDeathMaxUnits(BigDecimal deathMaxUnits) {
		this.deathMaxUnits = deathMaxUnits;
	}

	public BigDecimal getTpdMaxUnits() {
		return tpdMaxUnits;
	}

	public void setTpdMaxUnits(BigDecimal tpdMaxUnits) {
		this.tpdMaxUnits = tpdMaxUnits;
	}

	public boolean isEmployeeNoRenderFlag() {
		return employeeNoRenderFlag;
	}

	public void setEmployeeNoRenderFlag(boolean employeeNoRenderFlag) {
		this.employeeNoRenderFlag = employeeNoRenderFlag;
	}

	public boolean isIndustryOccupRenderFlag() {
		return industryOccupRenderFlag;
	}

	public void setIndustryOccupRenderFlag(boolean industryOccupRenderFlag) {
		this.industryOccupRenderFlag = industryOccupRenderFlag;
	}

	public boolean isIndustryTypeRenderFlag() {
		return industryTypeRenderFlag;
	}

	public void setIndustryTypeRenderFlag(boolean industryTypeRenderFlag) {
		this.industryTypeRenderFlag = industryTypeRenderFlag;
	}

	public boolean isMemberShipCatgRenderFlag() {
		return memberShipCatgRenderFlag;
	}

	public void setMemberShipCatgRenderFlag(boolean memberShipCatgRenderFlag) {
		this.memberShipCatgRenderFlag = memberShipCatgRenderFlag;
	}

	public boolean isRenderNewCoverTable() {
		return renderNewCoverTable;
	}

	public void setRenderNewCoverTable(boolean renderNewCoverTable) {
		this.renderNewCoverTable = renderNewCoverTable;
	}

	public boolean isFulCover() {
		return fulCover;
	}

	public void setFulCover(boolean fulCover) {
		this.fulCover = fulCover;
	}


	public String getMeberCategeoryTokens() {
		return meberCategeoryTokens;
	}

	public void setMeberCategeoryTokens(String meberCategeoryTokens) {
		this.meberCategeoryTokens = meberCategeoryTokens;
	}

	public boolean isEmailSaveBtnRenderFlag() {
		return emailSaveBtnRenderFlag;
	}

	public void setEmailSaveBtnRenderFlag(boolean emailSaveBtnRenderFlag) {
		this.emailSaveBtnRenderFlag = emailSaveBtnRenderFlag;
	}

	public boolean isTransferExtInsCvrFlag() {
		return transferExtInsCvrFlag;
	}

	public void setTransferExtInsCvrFlag(boolean transferExtInsCvrFlag) {
		this.transferExtInsCvrFlag = transferExtInsCvrFlag;
	}

	public boolean isRenderIngCoverTable() {
		return renderIngCoverTable;
	}

	public void setRenderIngCoverTable(boolean renderIngCoverTable) {
		this.renderIngCoverTable = renderIngCoverTable;
	}

	public boolean isDeathTpdCoverRelationFlag() {
		return deathTpdCoverRelationFlag;
	}

	public void setDeathTpdCoverRelationFlag(boolean deathTpdCoverRelationFlag) {
		this.deathTpdCoverRelationFlag = deathTpdCoverRelationFlag;
	}

	public boolean isAddressSectionRenderFlag() {
		return addressSectionRenderFlag;
	}

	public void setAddressSectionRenderFlag(boolean addressSectionRenderFlag) {
		this.addressSectionRenderFlag = addressSectionRenderFlag;
	}

	public String getTransferBenefitPeriodTokens() {
		return transferBenefitPeriodTokens;
	}

	public void setTransferBenefitPeriodTokens(String transferBenefitPeriodTokens) {
		this.transferBenefitPeriodTokens = transferBenefitPeriodTokens;
	}

	public String getTransferWaitingPeriodTokens() {
		return transferWaitingPeriodTokens;
	}

	public void setTransferWaitingPeriodTokens(String transferWaitingPeriodTokens) {
		this.transferWaitingPeriodTokens = transferWaitingPeriodTokens;
	}

	public BigDecimal getDeathTpdTransferMaxAmt() {
		return deathTpdTransferMaxAmt;
	}

	public void setDeathTpdTransferMaxAmt(BigDecimal deathTpdTransferMaxAmt) {
		this.deathTpdTransferMaxAmt = deathTpdTransferMaxAmt;
	}

	public BigDecimal getIpTransferMaxAmt() {
		return ipTransferMaxAmt;
	}

	public void setIpTransferMaxAmt(BigDecimal ipTransferMaxAmt) {
		this.ipTransferMaxAmt = ipTransferMaxAmt;
	}

	public double getIpMultiPolicyDiscount() {
		return ipMultiPolicyDiscount;
	}

	public void setIpMultiPolicyDiscount(double ipMultiPolicyDiscount) {
		this.ipMultiPolicyDiscount = ipMultiPolicyDiscount;
	}

	public boolean isAskmanualqRenderFlag() {
		return askmanualqRenderFlag;
	}

	public void setAskmanualqRenderFlag(boolean askmanualqRenderFlag) {
		this.askmanualqRenderFlag = askmanualqRenderFlag;
	}

	public boolean isPlanCoverAmntRender() {
		return planCoverAmntRender;
	}

	public void setPlanCoverAmntRender(boolean planCoverAmntRender) {
		this.planCoverAmntRender = planCoverAmntRender;
	}

	public boolean isAskProfessionalQRenderFlag() {
		return askProfessionalQRenderFlag;
	}

	public void setAskProfessionalQRenderFlag(boolean askProfessionalQRenderFlag) {
		this.askProfessionalQRenderFlag = askProfessionalQRenderFlag;
	}

	public boolean isAskManualFlag() {
		return askManualFlag;
	}

	public void setAskManualFlag(boolean askManualFlag) {
		this.askManualFlag = askManualFlag;
	}

	public boolean isAskProfessionalFlag() {
		return askProfessionalFlag;
	}

	public void setAskProfessionalFlag(boolean askProfessionalFlag) {
		this.askProfessionalFlag = askProfessionalFlag;
	}
	
	public boolean isOtherOccupationRend() {
		return otherOccupationRend;
	}

	public void setOtherOccupationRend(boolean otherOccupationRend) {
		this.otherOccupationRend = otherOccupationRend;
	}

	public boolean isRendUnitFixedLabel() {
		return rendUnitFixedLabel;
	}

	public void setRendUnitFixedLabel(boolean rendUnitFixedLabel) {
		this.rendUnitFixedLabel = rendUnitFixedLabel;
	}

	public boolean isRendIpFixedIndicator() {
		return rendIpFixedIndicator;
	}

	public void setRendIpFixedIndicator(boolean rendIpFixedIndicator) {
		this.rendIpFixedIndicator = rendIpFixedIndicator;
	}

	public boolean isRenderDeclarationSection() {
		return renderDeclarationSection;
	}

	public void setRenderDeclarationSection(boolean renderDeclarationSection) {
		this.renderDeclarationSection = renderDeclarationSection;
	}

	public String getAnnualSalForUpgradeVal() {
		return annualSalForUpgradeVal;
	}

	public void setAnnualSalForUpgradeVal(String annualSalForUpgradeVal) {
		this.annualSalForUpgradeVal = annualSalForUpgradeVal;
	}

	public boolean isSavePopUpButtonRender() {
		return savePopUpButtonRender;
	}

	public void setSavePopUpButtonRender(boolean savePopUpButtonRender) {
		this.savePopUpButtonRender = savePopUpButtonRender;
	}

	public boolean isAutoQuoteCalculate() {
		return autoQuoteCalculate;
	}

	public void setAutoQuoteCalculate(boolean autoQuoteCalculate) {
		this.autoQuoteCalculate = autoQuoteCalculate;
	}

	public double getTpdOnlyMultipFactor() {
		return tpdOnlyMultipFactor;
	}

	public void setTpdOnlyMultipFactor(double tpdOnlyMultipFactor) {
		this.tpdOnlyMultipFactor = tpdOnlyMultipFactor;
	}

	public String getManageTypeTokens() {
		return manageTypeTokens;
	}

	public void setManageTypeTokens(String manageTypeTokens) {
		this.manageTypeTokens = manageTypeTokens;
	}

	public boolean isRenderPrintButton() {
		return renderPrintButton;
	}

	public void setRenderPrintButton(boolean renderPrintButton) {
		this.renderPrintButton = renderPrintButton;
	}

	public boolean isCancelCheckBoxRender() {
		return cancelCheckBoxRender;
	}

	public void setCancelCheckBoxRender(boolean cancelCheckBoxRender) {
		this.cancelCheckBoxRender = cancelCheckBoxRender;
	}

	public boolean isFundMemInsPolicyNumRender() {
		return fundMemInsPolicyNumRender;
	}

	public void setFundMemInsPolicyNumRender(boolean fundMemInsPolicyNumRender) {
		this.fundMemInsPolicyNumRender = fundMemInsPolicyNumRender;
	}

	public boolean isHrsPerWeekRender() {
		return hrsPerWeekRender;
	}

	public void setHrsPerWeekRender(boolean hrsPerWeekRender) {
		this.hrsPerWeekRender = hrsPerWeekRender;
	}

	public boolean isLifeEventDateRender() {
		return lifeEventDateRender;
	}

	public void setLifeEventDateRender(boolean lifeEventDateRender) {
		this.lifeEventDateRender = lifeEventDateRender;
	}

	public boolean isLifeEventRsnRender() {
		return lifeEventRsnRender;
	}

	public void setLifeEventRsnRender(boolean lifeEventRsnRender) {
		this.lifeEventRsnRender = lifeEventRsnRender;
	}

	public boolean isNameOfPrevFundRender() {
		return nameOfPrevFundRender;
	}

	public void setNameOfPrevFundRender(boolean nameOfPrevFundRender) {
		this.nameOfPrevFundRender = nameOfPrevFundRender;
	}

	public boolean isSpinNumberRender() {
		return spinNumberRender;
	}

	public void setSpinNumberRender(boolean spinNumberRender) {
		this.spinNumberRender = spinNumberRender;
	}

	public boolean isManageTypeRender() {
		return manageTypeRender;
	}

	public void setManageTypeRender(boolean manageTypeRender) {
		this.manageTypeRender = manageTypeRender;
	}

	public boolean isCoverTableRender() {
		return coverTableRender;
	}

	public void setCoverTableRender(boolean coverTableRender) {
		this.coverTableRender = coverTableRender;
	}

	public String getLifeEventReasons() {
		return lifeEventReasons;
	}

	public void setLifeEventReasons(String lifeEventReasons) {
		this.lifeEventReasons = lifeEventReasons;
	}

	public String getUploadTypeTokens() {
		return uploadTypeTokens;
	}

	public void setUploadTypeTokens(String uploadTypeTokens) {
		this.uploadTypeTokens = uploadTypeTokens;
	}

	public boolean isQ10_PROFF_Q_Render() {
		return q10_PROFF_Q_Render;
	}

	public void setQ10_PROFF_Q_Render(boolean render) {
		q10_PROFF_Q_Render = render;
	}

	public boolean isQ8_PROFF_Q_Render() {
		return q8_PROFF_Q_Render;
	}

	public void setQ8_PROFF_Q_Render(boolean render) {
		q8_PROFF_Q_Render = render;
	}

	public boolean isQ9_PROFF_Q_Render() {
		return q9_PROFF_Q_Render;
	}

	public void setQ9_PROFF_Q_Render(boolean render) {
		q9_PROFF_Q_Render = render;
	}

	public boolean isDutyDisRender() {
		return dutyDisRender;
	}

	public void setDutyDisRender(boolean dutyDisRender) {
		this.dutyDisRender = dutyDisRender;
	}

	public boolean isGenConsentRender() {
		return genConsentRender;
	}

	public void setGenConsentRender(boolean genConsentRender) {
		this.genConsentRender = genConsentRender;
	}

	public boolean isPrivacyRender() {
		return privacyRender;
	}

	public void setPrivacyRender(boolean privacyRender) {
		this.privacyRender = privacyRender;
	}

	public BigDecimal getDeathTpdLifeEventMaxAfterThreshold() {
		return deathTpdLifeEventMaxAfterThreshold;
	}

	public void setDeathTpdLifeEventMaxAfterThreshold(
			BigDecimal deathTpdLifeEventMaxAfterThreshold) {
		this.deathTpdLifeEventMaxAfterThreshold = deathTpdLifeEventMaxAfterThreshold;
	}

	public BigDecimal getDeathTpdLifeEventSalMultiplier() {
		return deathTpdLifeEventSalMultiplier;
	}

	public void setDeathTpdLifeEventSalMultiplier(
			BigDecimal deathTpdLifeEventSalMultiplier) {
		this.deathTpdLifeEventSalMultiplier = deathTpdLifeEventSalMultiplier;
	}

	public BigDecimal getDeathTpdLifeEventThreshold() {
		return deathTpdLifeEventThreshold;
	}

	public void setDeathTpdLifeEventThreshold(BigDecimal deathTpdLifeEventThreshold) {
		this.deathTpdLifeEventThreshold = deathTpdLifeEventThreshold;
	}

	public BigDecimal getTpdLifeEventMaxAfterThrAge() {
		return tpdLifeEventMaxAfterThrAge;
	}

	public void setTpdLifeEventMaxAfterThrAge(BigDecimal tpdLifeEventMaxAfterThrAge) {
		this.tpdLifeEventMaxAfterThrAge = tpdLifeEventMaxAfterThrAge;
	}

	public int getTpdLifeEventThresholdAge() {
		return tpdLifeEventThresholdAge;
	}

	public void setTpdLifeEventThresholdAge(int tpdLifeEventThresholdAge) {
		this.tpdLifeEventThresholdAge = tpdLifeEventThresholdAge;
	}

	public BigDecimal getTpdLifeEventMaxUnitsAfterThrAge() {
		return tpdLifeEventMaxUnitsAfterThrAge;
	}

	public void setTpdLifeEventMaxUnitsAfterThrAge(
			BigDecimal tpdLifeEventMaxUnitsAfterThrAge) {
		this.tpdLifeEventMaxUnitsAfterThrAge = tpdLifeEventMaxUnitsAfterThrAge;
	}

	public double getDeathUnitCostMultFacStandard() {
		return deathUnitCostMultFacStandard;
	}

	public void setDeathUnitCostMultFacStandard(double deathUnitCostMultFacStandard) {
		this.deathUnitCostMultFacStandard = deathUnitCostMultFacStandard;
	}

	public double getDeathUnitCostMultFactProff() {
		return deathUnitCostMultFactProff;
	}

	public void setDeathUnitCostMultFactProff(double deathUnitCostMultFactProff) {
		this.deathUnitCostMultFactProff = deathUnitCostMultFactProff;
	}

	public double getDeathUnitCostMultFacWhiteCollar() {
		return deathUnitCostMultFacWhiteCollar;
	}

	public void setDeathUnitCostMultFacWhiteCollar(
			double deathUnitCostMultFacWhiteCollar) {
		this.deathUnitCostMultFacWhiteCollar = deathUnitCostMultFacWhiteCollar;
	}

	public double getTpdUnitCostMultFacStandard() {
		return tpdUnitCostMultFacStandard;
	}

	public void setTpdUnitCostMultFacStandard(double tpdUnitCostMultFacStandard) {
		this.tpdUnitCostMultFacStandard = tpdUnitCostMultFacStandard;
	}

	public double getTpdUnitCostMultFactProff() {
		return tpdUnitCostMultFactProff;
	}

	public void setTpdUnitCostMultFactProff(double tpdUnitCostMultFactProff) {
		this.tpdUnitCostMultFactProff = tpdUnitCostMultFactProff;
	}

	public double getTpdUnitCostMultFacWhiteCollar() {
		return tpdUnitCostMultFacWhiteCollar;
	}

	public void setTpdUnitCostMultFacWhiteCollar(
			double tpdUnitCostMultFacWhiteCollar) {
		this.tpdUnitCostMultFacWhiteCollar = tpdUnitCostMultFacWhiteCollar;
	}

	public BigDecimal getDeathMultipleFactor() {
		return deathMultipleFactor;
	}

	public void setDeathMultipleFactor(BigDecimal deathMultipleFactor) {
		this.deathMultipleFactor = deathMultipleFactor;
	}

	public BigDecimal getIpMultipleFactor() {
		return ipMultipleFactor;
	}

	public void setIpMultipleFactor(BigDecimal ipMultipleFactor) {
		this.ipMultipleFactor = ipMultipleFactor;
	}

	public BigDecimal getTpdMultipleFactor() {
		return tpdMultipleFactor;
	}

	public void setTpdMultipleFactor(BigDecimal tpdMultipleFactor) {
		this.tpdMultipleFactor = tpdMultipleFactor;
	}

	public boolean isQuickQuoteRender() {
		return quickQuoteRender;
	}

	public void setQuickQuoteRender(boolean quickQuoteRender) {
		this.quickQuoteRender = quickQuoteRender;
	}

	public String getEmpTypeCellStyCls() {
		return empTypeCellStyCls;
	}

	public void setEmpTypeCellStyCls(String empTypeCellStyCls) {
		this.empTypeCellStyCls = empTypeCellStyCls;
	}

	public boolean isEmpTypeRenderFlag() {
		return empTypeRenderFlag;
	}

	public void setEmpTypeRenderFlag(boolean empTypeRenderFlag) {
		this.empTypeRenderFlag = empTypeRenderFlag;
	}

	public boolean isLifeEventIncreaseCvrRender() {
		return lifeEventIncreaseCvrRender;
	}

	public void setLifeEventIncreaseCvrRender(boolean lifeEventIncreaseCvrRender) {
		this.lifeEventIncreaseCvrRender = lifeEventIncreaseCvrRender;
	}

	public double getDeathOnlyMultipFactor() {
		return deathOnlyMultipFactor;
	}

	public void setDeathOnlyMultipFactor(double deathOnlyMultipFactor) {
		this.deathOnlyMultipFactor = deathOnlyMultipFactor;
	}

	public boolean isCvrSummaryEmailRender() {
		return cvrSummaryEmailRender;
	}

	public void setCvrSummaryEmailRender(boolean cvrSummaryEmailRender) {
		this.cvrSummaryEmailRender = cvrSummaryEmailRender;
	}

	public boolean isDisableDeathSection() {
		return disableDeathSection;
	}

	public void setDisableDeathSection(boolean disableDeathSection) {
		this.disableDeathSection = disableDeathSection;
	}

	public boolean isDisableTpdSection() {
		return disableTpdSection;
	}

	public void setDisableTpdSection(boolean disableTpdSection) {
		this.disableTpdSection = disableTpdSection;
	}

	public BigDecimal getDeathTpdTransferMaxUnits() {
		return deathTpdTransferMaxUnits;
	}

	public void setDeathTpdTransferMaxUnits(BigDecimal deathTpdTransferMaxUnits) {
		this.deathTpdTransferMaxUnits = deathTpdTransferMaxUnits;
	}

	public BigDecimal getTpdMaxAmtAfterXAge() {
		return tpdMaxAmtAfterXAge;
	}

	public void setTpdMaxAmtAfterXAge(BigDecimal tpdMaxAmtAfterXAge) {
		this.tpdMaxAmtAfterXAge = tpdMaxAmtAfterXAge;
	}

	public int getTpdMaxLimitAfterAge() {
		return tpdMaxLimitAfterAge;
	}

	public void setTpdMaxLimitAfterAge(int tpdMaxLimitAfterAge) {
		this.tpdMaxLimitAfterAge = tpdMaxLimitAfterAge;
	}

	public boolean isSpecialOfferTpdRender() {
		return specialOfferTpdRender;
	}

	public void setSpecialOfferTpdRender(boolean specialOfferTpdRender) {
		this.specialOfferTpdRender = specialOfferTpdRender;
	}

	public boolean isIdentifiableDutiesRender() {
		return identifiableDutiesRender;
	}

	public void setIdentifiableDutiesRender(boolean identifiableDutiesRender) {
		this.identifiableDutiesRender = identifiableDutiesRender;
	}

	public boolean isRegularIncomeRender() {
		return regularIncomeRender;
	}

	public void setRegularIncomeRender(boolean regularIncomeRender) {
		this.regularIncomeRender = regularIncomeRender;
	}

	public boolean isWithOutLimitationRender() {
		return withOutLimitationRender;
	}

	public void setWithOutLimitationRender(boolean withOutLimitationRender) {
		this.withOutLimitationRender = withOutLimitationRender;
	}

	public double getDeathUnitCostGrtThan27() {
		return deathUnitCostGrtThan27;
	}

	public void setDeathUnitCostGrtThan27(double deathUnitCostGrtThan27) {
		this.deathUnitCostGrtThan27 = deathUnitCostGrtThan27;
	}

	public double getTpdUnitCostGrtThan27() {
		return tpdUnitCostGrtThan27;
	}

	public void setTpdUnitCostGrtThan27(double tpdUnitCostGrtThan27) {
		this.tpdUnitCostGrtThan27 = tpdUnitCostGrtThan27;
	}

	public double getTpdUnitCostMultFacOwnWhiteCollar() {
		return tpdUnitCostMultFacOwnWhiteCollar;
	}

	public void setTpdUnitCostMultFacOwnWhiteCollar(double tpdUnitCostMultFacOwnWhiteCollar) {
		this.tpdUnitCostMultFacOwnWhiteCollar = tpdUnitCostMultFacOwnWhiteCollar;
	}

	public double getDeathUnitCostMultFacOwnWhiteCollar() {
		return deathUnitCostMultFacOwnWhiteCollar;
	}

	public void setDeathUnitCostMultFacOwnWhiteCollar(double deathUnitCostMultFacOwnWhiteCollar) {
		this.deathUnitCostMultFacOwnWhiteCollar = deathUnitCostMultFacOwnWhiteCollar;
	}
	
	
}
