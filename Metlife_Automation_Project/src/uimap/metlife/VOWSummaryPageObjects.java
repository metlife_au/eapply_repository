package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class VOWSummaryPageObjects extends PageObjectLibrary{
	
	public static final By linkTitle1 = getObjectIdentifaction("vow.webresponse.summary.dropdown1.css");
	public static final By txtLastName1 = getObjectIdentifaction("vow.webresponse.summary.lastname1.text.id");
	
	public static final By linkTitle2 = getObjectIdentifaction("vow.webresponse.summary.dropdown2.css");
	public static final By txtLastName2 = getObjectIdentifaction("vow.webresponse.summary.lastname2.text.id");
	
	public static final By chkacknowledge = getObjectIdentifaction("vow.webresponse.summary.acknowledge.checkbox.id");
	
	public static final By rddebit = getObjectIdentifaction("vow.payment.debit.radio.css");
	public static final By rdcredit = getObjectIdentifaction("vow.payment.credit.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("vow.payment.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("vow.payment.accountnumber.text.id");
	public static final By txtbsb = getObjectIdentifaction("vow.payment.bsb.text.id");	
	public static final By chkauthorise = getObjectIdentifaction("vow.payment.authorise.checkbox.id");
	public static final By btnsubmit = getObjectIdentifaction("vow.payment.protectnow.btn.css");
	public static final By btnclosewindow = getObjectIdentifaction("vow.payment.closewindow.btn.css");
	public static final By rdvisa = getObjectIdentifaction("vow.payment.visa.radio.css");
	public static final By rdmaster = getObjectIdentifaction("vow.payment.master.radio.css");
	public static final By rdamex = getObjectIdentifaction("vow.payment.amex.radio.css");
	public static final By txtccnumber = getObjectIdentifaction("vow.payment.ccnumber.text.id");
	public static final By txtccname = getObjectIdentifaction("vow.payment.ccnname.text.id");
	public static final By selCCExpMonth = getObjectIdentifaction("vow.payment.ccexpmonth.dropdown.css");
	public static final By selCcExpYr = getObjectIdentifaction("vow.payment.ccexpyear.dropdown.css");
	public static final By textAppnum = getObjectIdentifaction("vow.decision.Appnumber.text.xpath");
		
}
