/*Timeout Controller Starts*/    
angular.module('MtaaApp').controller('timeOutController',['$scope', '$location', 'appData',  function($scope, $location, appData){
	var vm = this;
	// since onpopstate event is not detected by IE
	if (navigator.appVersion.toString().indexOf('.NET') > 0){
		window.onhashchange = function (e) { window.history.forward(1); };
	} else{
		window.onpopstate = function (e) { window.history.forward(1); };
	}
}]); 
/*Timeout Controller Ends*/