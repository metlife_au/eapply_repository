package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.YBRIPHealthOverviewPageObjects;
import uimap.metlife.OAMPSHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

public class YBRIPHealthOverviewPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRIPHealthOverviewPage yourHealthOverview() {
		yourHealthOverviewDetails();
		return new YBRIPHealthOverviewPage(scriptHelper);
	}

	public YBRIPHealthOverviewPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourHealthOverviewDetails() {

		String height = dataTable.getData("MetLife_Data", "Height");
		String weight = dataTable.getData("MetLife_Data", "Weight");
		String Fiveyears    = dataTable.getData("MetLife_Data", "5yrs");
			
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			tryAction(waitForClickableElement(YBRIPHealthOverviewPageObjects.txtheight), "SET", "Height", height);
			tryAction(waitForClickableElement(YBRIPHealthOverviewPageObjects.txtheight), "TAB", "Height");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(YBRIPHealthOverviewPageObjects.txtweight), "SET", "Weight", weight);
			tryAction(waitForClickableElement(YBRIPHealthOverviewPageObjects.txtweight), "TAB", "Weight");
			
			if (Fiveyears.equalsIgnoreCase("None")) {
			tryAction(waitForClickableElement(YBRIPHealthOverviewPageObjects.chklast5yrsNone),"Click", "Have you suffered,diagnosed in last 5 years - None");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPHealthOverviewPageObjects.btnNext), "Click", "Next");
			Thread.sleep(4000);
			
			}
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
