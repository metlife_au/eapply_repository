/**
 * 
 */
package pages.metlife.NEW;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.NSFS_TransferYourCoverPageObjects;

/**
 * @author Hemnath
 * 
 */
public class NSFS_TransferYourCoverPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public NSFS_TransferYourCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public NSFS_TransferYourCoverPage enternsfstransfercover() {
		transfercoverpersonaldetails();
		healthandlifestyledetails();
		confirmation();
		
		return new NSFS_TransferYourCoverPage(scriptHelper);
	} 
	public static String MTAADeathCost;
	
	private void transfercoverpersonaldetails() {
		
		String EmailId = dataTable.getData("NSFS_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("NSFS_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("NSFS_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("NSFS_TransferCover", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("NSFS_TransferCover", "FifteenHoursWork");
		String Citizen = dataTable.getData("NSFS_TransferCover", "Citizen");
		String IndustryType = dataTable.getData("NSFS_TransferCover", "IndustryType");
		String OccupationType = dataTable.getData("NSFS_TransferCover", "OccupationType");
		String OtherOccupation = dataTable.getData("NSFS_TransferCover", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("NSFS_TransferCover", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("NSFS_TransferCover", "TertiaryQual");
		String HazardousEnv = dataTable.getData("NSFS_TransferCover", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("NSFS_TransferCover", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("NSFS_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("NSFS_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("NSFS_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("NSFS_TransferCover", "FormerFundNo");
		String DocumentaryEvidence = dataTable.getData("NSFS_TransferCover", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("NSFS_TransferCover", "AttachPath");
		
		String DeathYN = dataTable.getData("NSFS_TransferCover", "Death");
		String TPDYN = dataTable.getData("NSFS_TransferCover", "TPD");
		String IPYN = dataTable.getData("NSFS_TransferCover", "IP");
		String DeathAmount = dataTable.getData("NSFS_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("NSFS_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("NSFS_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("NSFS_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("NSFS_TransferCover", "BenefitPeriod");
		
		
		
		try{
			
			driver.manage().window().maximize();
			
			//****Click on Transfer Your Cover Link*****\\
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.TransferYourCoverLink), "Click", "Transfer Your Cover Link");
			
			//*****Agree to Duty of disclosure*******\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "selected the Checkbox", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Privacy Statement", "selected the Checkbox", Status.PASS);
			
			//*****Enter the Email Id*****\\
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.EmailId), "SET", "Email Id",EmailId);
			
			
			//*****Click on the Contact Number Type****\\
			if(!TypeofContact.equalsIgnoreCase("Mobile")){
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContactTypeClick), "Click", "Preferred Type of Contact: "+TypeofContact);
						
			//*****Choose the Type of Contact*****\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();
			
			}
			
			//****Enter the Contact Number****\\
			waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);
			
			
			
			//***Select the Preferred time of Contact*****\\
			if(TimeofContact.equalsIgnoreCase("Morning")){
				
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");
			}
			else{
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.AfterNoonContact), "Click", "Preferred Contact Time is AfterNoon");
			}
			
			//****Contact Details-Continue Button*****\\			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");
			sleep(3200);
			

			//***************************************\\	
			//**********OCCUPATION SECTION***********\\
			//****************************************\\	
				
				//*****Select the 15 Hours Question*******\\
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){
				
				tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.FifteenHoursYes), "Click", "15 Hours a Week-Yes");
				
				}
				else{
				
				tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");
				
				}
				
				//*****Select if Resident of Australia****\\
				if(Citizen.equalsIgnoreCase("Yes")){
		
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.CitizenYes), "Click", "Australian Citizen-Yes");
					
					}
				else{
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.CitizenNo), "Click", "Australian Citizen-No");	
					
					}
				
				//*****Click on Industry********\\
				
				tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ClickIndustry), "Click", "Industry");
				sleep(800);
				
				//*****Choose the Industry Type*****\\
				driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();
				sleep(1000);
			
				//*****Click On  Occupation*****\\
				
	            tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ClickOccupation), "Click", "Occupation");
	            sleep(800);
	            
	            //****Choose the Occupation Type*****\\
				
				driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();
				sleep(1000);
				
				
				//*****Other Occupation *****\\
				if(OccupationType.equalsIgnoreCase("Other")){
					
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);
					sleep(1000);
				}
				
				
				//*****Extra Questions*****\\
				
				if(driver.getPageSource().contains("Do you work wholly within an office environment?")){
	
				
			//***Select if your office Duties are Undertaken within an Office Environment?\\
				if(OfficeEnvironment.equalsIgnoreCase("Yes")){
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");
					
				}
				else{
					
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");
					
				}
				
			//*****Do You Hold a Tertiary Qualification******\\
				
				if(TertiaryQual.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");				
				}						
			}
				
				
				
			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){
			
				
				
			//******Do you work in hazardous environment*****\\
				if(HazardousEnv.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");				
				}
				
			//******Do you spend 20% time working outside office*******\\
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");				
				}						
			}
			
				
			//*****What is your annual Salary******
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);
			sleep(500);
			
			//***Load the Salary Amount****\\
			waitForClickableElement(NSFS_TransferYourCoverPageObjects.LoadSalaryLabel).click();
			sleep(500);
			
			
		//*****Continue button after entering occupation details*******\\
			
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");
			sleep(1500);
			
			
			//*************************************************************\\	
			//************PREVIOUS COVER SECTION****************************\\
			//**************************************************************\\
			
				//*****Name of previous insurer*****\\	
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.PreviousInsurer), "SET", "Previous Insurer Name",PreviousInsurer);	
			
			
			//*****please enter your fund member of insurance policy number*****\\		
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.PolicyNumber), "SET", "Member Number of Policy Number",PolicyNo);
			
			
			//please enter your former fund USI number*****		
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.USINumber), "SET", "Former Fund USI Number",FormerFundNo);
			
			
			//*****8Do You want to Attach documentary evidence****\\
			if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
				
				waitForClickableElement(NSFS_TransferYourCoverPageObjects.DocumentEvidenceYes).click();
				sleep(500);
				
				waitForClickableElement(NSFS_TransferYourCoverPageObjects.DocumentEvidenceNo).click();
				sleep(500);
				
				tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.DocumentEvidenceYes), "Click", "Documentary Evidence-Yes");
				sleep(1000);
				
				
				//****Attach the file******		
				driver.findElement(By.id("transferCvrFileUploadAttach")).sendKeys(AttachPath);
				sleep(1500);
				
				//*****Let the attachment load*****\\		
				driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
				sleep(1000);
				
				//*****Add the Attachment*****\\
				
				WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1500);
				
				
				
			}
			else{
				
				//****No Attachment *****\\
				
				waitForClickableElement(NSFS_TransferYourCoverPageObjects.DocumentEvidenceYes).click();
				sleep(500);
				
				waitForClickableElement(NSFS_TransferYourCoverPageObjects.DocumentEvidenceNo).click();
				sleep(500);
				
				//Transfer check box
				
				tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.TransferChkbox), "Click", "Transfer Check Box");
				
			}
			
		
			
			//****Click on Continue Button******\\
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContinueAfterPreviousCover), "Click", "Continue After entering Previous Cover Details");
			sleep(1000);
			
			
			
			
			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\
			
			//*****Death transfer amount******
			
			if(DeathYN.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.DeathAmount), "SET", "Death transfer amount",DeathAmount);	
			sleep(500);
			waitForClickableElement(NSFS_TransferYourCoverPageObjects.LoadDeathAmount).click();
			sleep(500);
			}
			
			//*****TPD transfer amount******
			if(TPDYN.equalsIgnoreCase("Yes")){
			sleep(500);
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.TPDAmount), "SET", "TPD transfer amount",TPDAmount);	
			sleep(500);
			waitForClickableElement(NSFS_TransferYourCoverPageObjects.LoadTPDAmount).click();
			sleep(600);
			}
			//IP transfer amount*****\\
			if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.IPAmount), "SET", "IP transfer amount",IPAmount);	
			sleep(500);
			waitForClickableElement(NSFS_TransferYourCoverPageObjects.LoadIPAmount).click();
			sleep(500);
			

			//*****Select The Waiting Period********\\
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.Waitingperiod), "Click", "Waiting Period");
			sleep(800);	
			driver.findElement(By.xpath("//span[contains(text(),'"+WaitingPeriod+"')]")).click();
			sleep(500);
			
			
			
			//*****Select The Benefit  Period********\\
			
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.benefitperiod), "Click", "Benefit Period");
			sleep(800);		
			driver.findElement(By.xpath("//span[contains(text(),'"+BenefitPeriod+"')]")).click();
			sleep(500);
			}
				
			//*****Scroll to the Bottom of the Page
			JavascriptExecutor jse = (JavascriptExecutor) driver;
		    jse.executeScript("window.scrollBy(0,250)", "");
			
			
			//*****Click on Calculate Quote******\\
			sleep(1000);
			tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");
			sleep(3000);
			
			//********************************************************************************************\\
			//************Capturing the premiums displayed************************************************\\
			//********************************************************************************************\\
			
			//*******Capturing the Death Cost********\\
			WebElement Death=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[1]"));
			Death.click();
			sleep(300);
			jse.executeScript("window.scrollBy(0,-150)", "");
			sleep(500);
			String DeathCost=Death.getText();	
			report.updateTestLog("Death Cover amount","Monthly Cost is "+DeathCost, Status.SCREENSHOT);
			sleep(500);
			
			//*******Capturing the TPD Cost********\\
			WebElement tpd=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[2]"));
			tpd.click();
			sleep(500);
			String TPDCost=tpd.getText();
			report.updateTestLog("TPD Cover amount","Monthly Cost is "+TPDCost, Status.SCREENSHOT);
			
			//*******Capturing the IP Cost********\\
			WebElement ip=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[3]"));
			ip.click();
			sleep(500);
			String IPCost=ip.getText();
			
			report.updateTestLog("IP Cover amount","Monthly Cost is "+IPCost, Status.SCREENSHOT);
			
			//*****Scroll to the Bottom of the Page
			
		    jse.executeScript("window.scrollBy(0,250)", "");
		    
		    //******Click on Continue******\\
		    tryAction(waitForClickableElement(NSFS_TransferYourCoverPageObjects.ContinueAfterTYC), "Click", "Continue After entering Transfer Cover Details");
		    sleep(3000);


		    

				report.updateTestLog("Personal Details Page", "Sucessfully automated personal details section", Status.PASS);

			
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
	
private void healthandlifestyledetails() {
		
		String RestrictedIllness = dataTable.getData("NSFS_TransferCover", "RestrictedIllness");
		String TPDClaim = dataTable.getData("NSFS_TransferCover", "TPDClaim");
		String ReducedLifeExpectancy = dataTable.getData("NSFS_TransferCover", "ReducedLifeExpectancy");
		
			
		WebDriverWait wait=new WebDriverWait(driver,20);
			
			try{
				
			//*******Are you restricted due to illness or injury*********\\
				
				if(RestrictedIllness.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-0:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(800);
				
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-0:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(800);
					
				}
				
				
			//*****Have you ever been paid, or are you eligible to be paid, or have you lodged or do you intend to lodge a claim for a Total and Permanent Disablement (TPD) 
				//benefit from another superannuation fund or life insurance policy?
				
				if(TPDClaim.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-1:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior TPD Claim-Yes", Status.PASS);
					sleep(800);
				}
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-1:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior TPD Claim-Yes", Status.PASS);
					sleep(800);
				}
				
				
				//*****Have you been diagnosed with an illness that reduced your life expectancy to less than three years from today?
				
				
				if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-2:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Reduced Life Expectancy-Yes", Status.PASS);
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-2:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Reduced Life Expectancy-No", Status.PASS);
					sleep(800);
				}
		
			
				
			//******Click on Continue Button********\\
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='applyBtnId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(2500);
			    									
				
				
				
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
					
private void confirmation() {
	
	
	WebDriverWait wait=new WebDriverWait(driver,20);	
		
		try{
			if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){
		
			
			

			
		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(1000);
			
			
			
		//******Click on Submit*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(1000);			
			
		}
		
		//*******Feedback popup******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1000);
		
		if(driver.getPageSource().contains("Application number")){
		
		//*****Fetching the Application Status*****\\
		
		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);
		
		//*****Fetching the Application Number******\\
		
		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);
		
		
		
		//******Download the PDF***********\\
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
		sleep(1500);
		properties = Settings.getInstance();	
		String StrBrowseName=properties.getProperty("currentBrowser");
		System.out.println(StrBrowseName);
	  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			Robot roboobj=new Robot();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);	
	  		}
		
		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
		
		sleep(1000);
	
		 
		  report.updateTestLog("Confirmation Page", "Application No: "+App,Status.PASS);
		
			
		 report.updateTestLog("Confirmation Page", "Application Status: "+Appstatus,Status.PASS);	
		}

		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	




	
		



	
}
