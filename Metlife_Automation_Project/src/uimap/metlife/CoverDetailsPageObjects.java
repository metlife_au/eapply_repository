/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class CoverDetailsPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public CoverDetailsPageObjects() {
		// TODO Auto-generated constructor stub

	}

	public static final By radWrkHoursNo = getObjectIdentifaction("coverDetails.workhours.no.css");
	public static final By radWrkHoursYes = getObjectIdentifaction("coverDetails.workhours.yes.css");
	public static final By radMale = getObjectIdentifaction("pwc.coverDetails.gender.male.radio.css");
	public static final By radFemale = getObjectIdentifaction("pwc.coverDetails.gender.female.radio.css");
	public static final By lstIndustrytoWrk = getObjectIdentifaction("coverDetails.industry.css");
	public static final By lstOccupation = getObjectIdentifaction("coverDetails.occupation.id");
	public static final By txtSalary = getObjectIdentifaction("coverDetails.salary.id");
	public static final By txtIpCover = getObjectIdentifaction("pwc.coverDetails.ip.text.id");
	public static final By txtAddnlIpCover = getObjectIdentifaction("pwc.coverDetails.addnl.ip.text.id");
	public static final By lstPremFrequency = getObjectIdentifaction("coverDetails.premFreq.id");
	public static final By radOfficeEnvironmentYes = getObjectIdentifaction("coverDetails.workduties.whollywork.yes.css");
	public static final By radOfficeEnvironmentNo = getObjectIdentifaction("coverDetails.workduties.whollywork.no.css");
	public static final By radTertiaryOccupationYes = getObjectIdentifaction("coverDetails.tertiaryoccu.yes.css");
	public static final By radTertiaryOccupationNo = getObjectIdentifaction("coverDetails.tertiaryoccu.no.css");
	public static final By radTotalDeathCoverUnitised = getObjectIdentifaction("coverDetails.totalDeathCovReq.unitised.css");
	public static final By radTotalDeathCoverFixed = getObjectIdentifaction("coverDetails.totalDeathCovReq.fixed.css");
	public static final By radTotPermDisabilityUnitised = getObjectIdentifaction("coverDetails.permdisa.unitised.css");
	public static final By radTotPermDisabilityFixed = getObjectIdentifaction("coverDetails.permdisa.fixed.css");
	public static final By chkPercentInsureSal = getObjectIdentifaction("coverDetails.insureSal.id");
	public static final By txtTotalDeathCover = getObjectIdentifaction("coverDetails.TCR.death.id");
	public static final By txtTotalPermDisability = getObjectIdentifaction("coverDetails.TCR.TPD.id");
	public static final By lstWaitingPeriod = getObjectIdentifaction("coverDetails.TCR.wp.id");
	public static final By lstBenefirPeriod = getObjectIdentifaction("coverDetails.TCR.bp.id");
	public static final By btnCalcQuote = getObjectIdentifaction("coverDetails.btn.calcquote.css");
	public static final By btnPwcCalcQuote = getObjectIdentifaction("pwc.coverDetails.btn.calcquote.css");
	public static final By btnApply = getObjectIdentifaction("coverDetails.btn.apply.css");
	public static final By dyn_strDcCoverCost = getObjectIdentifaction("coverDetails.label.dccovercost.id");
	public static final By dyn_strTpdCost = getObjectIdentifaction("coverDetails.label.tpdcost.id");
	public static final By dyn_strIpCost = getObjectIdentifaction("coverDetails.label.ipcost.id");
	public static final By lstDeathSelectCover = getObjectIdentifaction("coverDetails.dropdown.deathSelectCover.css");
	public static final By lstTpdSelectCover = getObjectIdentifaction("coverDetails.dropdown.tpdSelectCover.css");
	public static final By lstIpSelectCover = getObjectIdentifaction("coverDetails.dropdown.ipSelectCover.css");
	public static final By btnContinue = getObjectIdentifaction("coverDetails.btn.continue.id");
	public static final By btnVicsContinue = getObjectIdentifaction("vics.coverDetails.btn.continue.css");
	public static final By lblAddnlCover = getObjectIdentifaction("pwc.coverDetails.label.id");
	public static final By radSmokerYes = getObjectIdentifaction("psup.coverDetails.smoker.yes.radio.css");
	public static final By radSmokerNo = getObjectIdentifaction("psup.coverDetails.smoker.no.radio.css");
	public static final By txtDeathExistingCover = getObjectIdentifaction("psup.coverDetails.deathexisitngcover.text.id");
	public static final By txtTpdExistingCover = getObjectIdentifaction("psup.coverDetails.tpdexisitngcover.text.id");
	public static final By txtIpExistingCover = getObjectIdentifaction("psup.coverDetails.ipexisitngcover.text.id");
	public static final By txtIpAddnlCover = getObjectIdentifaction("psup.coverDetails.ipaddnlcover.text.id");
	public static final By radWorkDutiesYes = getObjectIdentifaction("sfps.coverdtls.workDutiesYes.radio.css");
	public static final By radWorkDutiesNo = getObjectIdentifaction("sfps.coverdtls.workDutiesNo.radio.css");
	public static final By lstIndustrytoWrkId = getObjectIdentifaction("coverDetails.industry.id");
	public static final By radWrk35HoursYes = getObjectIdentifaction("sfps.coverdtls.radWrk35HoursYes.radio.css");
	public static final By radWrk35HoursNo = getObjectIdentifaction("sfps.coverdtls.radWrk35HoursNo.radio.css");
	public static final By radPermEmpYes = getObjectIdentifaction("sfps.coverdtls.permanentEmpYes.radio.css");
	public static final By radPermEmpNo = getObjectIdentifaction("sfps.coverdtls.permanentEmpNo.radio.css");
	public static final By radResidentYes = getObjectIdentifaction("sfps.coverdtls.residentYes.radio.css");
	public static final By radResidentNo = getObjectIdentifaction("sfps.coverdtls.residentNo.radio.css");
	public static final By lstPremiumFrequency = getObjectIdentifaction("sfps.coverdtls.frequency.lst.id");
	public static final By txtDcAddnlCvrIpTxt = getObjectIdentifaction("coverDetails.dcAddnlCvrIpTxt.txt.id");
	public static final By txttpdAddnlCvrIpTxt = getObjectIdentifaction("coverDetails.dc_tpdAddnlCvrIpTxt.txt.id");
	public static final By chksalaryInsure = getObjectIdentifaction("coverDetails.salaryInsure.chkbox.id");
	public static final By lnkIngChgCover = getObjectIdentifaction("metlife.ingdir.reviewpage.changeyourcover.link.id");
	
	//INGD
	public static final By selDeathselectcovertype = getObjectIdentifaction("metlife.ingdir.coverdtls.deathselectcovertype.dropdown.id");
	public static final By selTotalpermdisabilityselectcovertype = getObjectIdentifaction("metlife.ingdir.coverdtls.totalpermdisabilityselectcovertype.dropdown.id");
	public static final By selIncomeprotectionselectcovertype = getObjectIdentifaction("metlife.ingdir.coverdtls.incomeprotectionselectcovertype.dropdown.id");
	public static final By txtDeathunits = getObjectIdentifaction("metlife.ingdir.coverdtls.deathunits.text.id");
	public static final By txtTotalpermdisabilityunits = getObjectIdentifaction("metlife.ingdir.coverdtls.totalpermdisabilityunits.text.id");
	public static final By txtIncomeprotectionunits = getObjectIdentifaction("metlife.ingdir.coverdtls.incomeprotectionunits.text.id");
	public static final By lnkContinue = getObjectIdentifaction("metlife.ingdir.coverdtls.continue.link.id");
	public static final By lnkCalculatequote = getObjectIdentifaction("metlife.ingdir.coverdtls.calculatequote.link.id");
	
	
	//Employeement Status
	public static final By radEmpStatusEmployed = getObjectIdentifaction("coverDetails.empstatus.employed.radio.css");
	public static final By radEmpStatusSelfEmployed = getObjectIdentifaction("coverDetails.empstatus.selfemployed.radio.css");
	public static final By radEmpStatusUnEmployed = getObjectIdentifaction("coverDetails.empstatus.unemployed.radio.css");
	
	//Premium Calculations related
	public static final By lblDeathAddlCoverAmt = getObjectIdentifaction("metlife.coverdetails.dcAddnlCvrAmtID.id");
	public static final By lblTPDAddlCoverAmt = getObjectIdentifaction("metlife.coverdetails.tpdAddnlCvrAmtID.id");
	public static final By lblIPAddlCoverAmt = getObjectIdentifaction("metlife.coverdetails.ipAddnlCvrAmtID.id");
	public static final By lblDCCostAmt = getObjectIdentifaction("metlife.coverdetails.dcCostDivId.id");
	public static final By lblTPDCostAmt = getObjectIdentifaction("metlife.coverdetails.tpdCostDivId.id");
	public static final By lblIPCostAmt = getObjectIdentifaction("metlife.coverdetails.ipCostDivId.id");
	
	public static final By txtIpAddlCover = getObjectIdentifaction("coverDetails.ipaadlcover.txt.id");
	public static final By lstBenefitPeriod = getObjectIdentifaction("coverDetails.ipBenefitPeriodLst.lst.id");
	public static final By listWaitingPeriod = getObjectIdentifaction("coverDetails.ipwaitingperiod.lst.css");
}
