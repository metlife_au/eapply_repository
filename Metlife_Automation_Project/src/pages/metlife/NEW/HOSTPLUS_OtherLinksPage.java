package pages.metlife.NEW;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_OtherLinksPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_OtherLinksPage HOSTPLUSOtherLinks() {
		OtherLinks();		
		
		
		return new HOSTPLUS_OtherLinksPage(scriptHelper);
	}

	public HOSTPLUS_OtherLinksPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void OtherLinks() {

		
		
		try{
			
			
			String MemberName = dataTable.getData("LandingPage_Links", "MemberName");
			String ApplyNowDeath = dataTable.getData("LandingPage_Links", "ApplyNowDeath");
			String ApplyNowTPD = dataTable.getData("LandingPage_Links", "ApplyNowTPD");
			String ApplyNowIP = dataTable.getData("LandingPage_Links", "ApplyNowIP");
			
			
			String DeathYN = dataTable.getData("LandingPage_Links", "DeathYN");
			String TPDYN = dataTable.getData("LandingPage_Links", "TPDYN");
			String IPYN = dataTable.getData("LandingPage_Links", "IPYN");
			
			String DeathAmount = dataTable.getData("LandingPage_Links", "DeathAmount");
			
			String DeathAmount_Type = dataTable.getData("LandingPage_Links", "DeathAmount_Type");
			String TPDAmount = dataTable.getData("LandingPage_Links", "TPDAmount");
			String TPDAmount_Type = dataTable.getData("LandingPage_Links", "TPDAmount_Type");
			String IPAmount = dataTable.getData("LandingPage_Links", "IPAmount");
			String IPAmount_Type = dataTable.getData("LandingPage_Links", "IPAmount_Type");
			String WaitingPeriod = dataTable.getData("LandingPage_Links", "WaitingPeriod");
			String BenefitPeriod = dataTable.getData("LandingPage_Links", "BenefitPeriod");
			
			//****Validate the Member Name*****\\	
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			String NameDisplay=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-xs-3 hidden-xs']/strong"))).getText();
			sleep(200);
			String NameDisplayed=NameDisplay.substring(7);
			NameDisplayed.trim();
			MemberName.trim();
			
			
			if(MemberName.equalsIgnoreCase(NameDisplayed)){
				
				report.updateTestLog("Member Name", MemberName+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("Member Name", MemberName+" is not displayed as expected", Status.FAIL);
			}
			
			sleep(500);
			
			
			//*****Check if any Apply Now is Displayed*****\\
			

			if(ApplyNowDeath.equalsIgnoreCase("Yes")){
				
				if(driver.findElement(By.xpath("//*[@id='deathbtnid']/span")).isDisplayed()){
					
					report.updateTestLog("Death-Apply Now", "is displayed as expected", Status.PASS);
				}
				
				else{
					
					report.updateTestLog("Death-Apply Now", "is not displayed as expected", Status.FAIL);
				}
			}
			
			if(ApplyNowTPD.equalsIgnoreCase("Yes")){
				
				if(driver.findElement(By.xpath("//*[@id='tpdbtnid']/span")).isDisplayed()){
					
					report.updateTestLog("TPD-Apply Now", "is displayed as expected", Status.PASS);
				}
				
				else{
					
					report.updateTestLog("TPD-Apply Now", "is not displayed as expected", Status.FAIL);
				}
			}
			
           if(ApplyNowIP.equalsIgnoreCase("Yes")){
				
				if(driver.findElement(By.xpath("//*[@id='ipbtnid']/span")).isDisplayed()){
					
					report.updateTestLog("IP-Apply Now", "is displayed as expected", Status.PASS);
				}
				
				else{
					
					report.updateTestLog("IP-Apply Now", "is not displayed as expected", Status.FAIL);
				}
			}
			
			
			//*******Validating the Death Cover Details********\\
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				
				String DeathAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[1]/p[1]"))).getText();
				String DeathAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[1]/p[2]"))).getText();
	            
				if(DeathAmount.equalsIgnoreCase(DeathAmountDisplayed)){
					
					report.updateTestLog("Death Cover", DeathAmount+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Death Cover", DeathAmount+" is not displayed as expected", Status.FAIL);
				}
				
	           if(DeathAmount_Type.equalsIgnoreCase(DeathAmount_Type_Displayed)){
					
					report.updateTestLog("Death Cover Type", DeathAmount_Type+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Death Cover Type", DeathAmount_Type+" is not displayed as expected", Status.FAIL);
				}
			}
			
			
           
           
           //*****Validating the TPD Cover Details******\\
           
			if(TPDYN.equalsIgnoreCase("Yes")){
				
				 String TPDAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[2]/p[1]"))).getText();
				   String TPDAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[2]/p[2]"))).getText();
		           
					if(TPDAmount.equalsIgnoreCase(TPDAmountDisplayed)){
						
						report.updateTestLog("TPD Cover", TPDAmount+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("TPD Cover", TPDAmount+" is not displayed as expected", Status.FAIL);
					}
					
		          if(TPDAmount_Type.equalsIgnoreCase(TPDAmount_Type_Displayed)){
						
						report.updateTestLog("TPD Cover Type", TPDAmount_Type+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("TPD Cover Type", TPDAmount_Type+" is not displayed as expected", Status.FAIL);
					}
		          
		          
			}
			
          
          //****Validating the IP Cover Details******\\
			
			if(IPYN.equalsIgnoreCase("Yes")){
				
				 String IPAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[3]/p[1]"))).getText();
				 sleep(250);
				   String IPAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[3]/p[2]"))).getText();
				   sleep(250);
				   String WaitingPeriod_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[3]/p[3]"))).getText();
				   sleep(250);
				   String BenefitPeriod_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='box mt10px    aligncenter'])[3]/p[4]"))).getText();
				   sleep(250);
		          
					if(IPAmount.equalsIgnoreCase(IPAmountDisplayed)){
						
						report.updateTestLog("IP Cover", IPAmount+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("IP Cover", IPAmount+" is not displayed as expected", Status.FAIL);
					}
					
		           if(IPAmount_Type.equalsIgnoreCase(IPAmount_Type_Displayed)){
						
						report.updateTestLog("IP Cover Type", IPAmount_Type+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("IP Cover Type", IPAmount_Type+" is not displayed as expected", Status.FAIL);
					}
		           
		           if(WaitingPeriod.equalsIgnoreCase(WaitingPeriod_Displayed)){
						
						report.updateTestLog("Waiting Period", WaitingPeriod+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("Waiting Period", WaitingPeriod+" is not displayed as expected", Status.FAIL);
					}
		           
		           if(BenefitPeriod.equalsIgnoreCase(BenefitPeriod_Displayed)){
						
						report.updateTestLog("Benefit Period", BenefitPeriod+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("Benefit Period", BenefitPeriod+" is not displayed as expected", Status.FAIL);
					}
		          
			}
          
          
           
           //****Validate the presence of links to other flows*****\\
           
           if(driver.getPageSource().contains("Change your insurance cover")){
        	  
        	   report.updateTestLog("Change Your Insurance cover", "Change Your Insurance cover is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Saved applications")){
         	  
        	   report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("transfer your cover")){
          	  
        	   report.updateTestLog("transfer your cover", "transfer your cover is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Increase your cover due to a life event")){
          	  
        	   report.updateTestLog("Life Event", "Increase your cover due to a life event is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Update Your Occupation")){
          	  
        	   report.updateTestLog("Update Your Occupation", "Update Your Occupation is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Cancel your cover")){
          	  
        	   report.updateTestLog("Cancel your cover", "Cancel your cover is displayed", Status.PASS);   
           }
           
			//****Click on Insurance Calculator Link*****\\
	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'How much Insurance ')]"))).click();
			
			report.updateTestLog("Insurance calculator", "Insurance calculator Link is Clicked", Status.PASS);
			
			sleep(1500);
		
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		     
			driver.switchTo().window(tabs.get(1));
			sleep(1500);
			driver.manage().window().maximize();
			sleep(1500);
			driver.findElement(By.xpath("//h2[contains(text(),'Insurance Calculator')]")).click();
			sleep(1000);
			String text=driver.findElement(By.xpath("//h2[contains(text(),'Insurance Calculator')]")).getText();
			
			report.updateTestLog("Insurance Calculator", text+" page is Displayed", Status.PASS);		 
			driver.close();
			driver.switchTo().window(tabs.get(0));
			sleep(1500);
			
			//****Click on Premium Calculator Link*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Get a quick quote on ')]"))).click();
			
			report.updateTestLog("Premium calculator", "Premium calculator Link is Clicked", Status.PASS);
			
			sleep(1500);
		
			ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		     
			driver.switchTo().window(tabs2.get(1));
			sleep(1500);
			driver.manage().window().maximize();
			sleep(1500);
			driver.findElement(By.xpath("//h2[contains(text(),' Premium Calculator ')]")).click();
			sleep(1000);
			String text2=driver.findElement(By.xpath("//h2[contains(text(),' Premium Calculator ')]")).getText();
			report.updateTestLog("Premium Calculator", text2+" page is Displayed", Status.PASS);		 
			driver.close();
			driver.switchTo().window(tabs.get(0));
		    
			sleep(2000);
			
	
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}





