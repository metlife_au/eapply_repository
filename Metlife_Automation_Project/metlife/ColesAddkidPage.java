package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesAddkidPageObjects;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesAddkidPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesAddkidPage enterColesAcceptExtraQuote() {
		ColesKidsDetails();
		return new ColesAddkidPage(scriptHelper);
	}
	
	public ColesAddkidPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void ColesKidsDetails(){
		try {
				String strRelation = dataTable.getData("MetLife_Data", "KidRelation");
				String strKFname = dataTable.getData("MetLife_Data", "KidFname");
				System.out.println(strKFname);
				String strKLname = dataTable.getData("MetLife_Data", "KidLname");
				System.out.println(strKLname);
				String strKDOB = dataTable.getData("MetLife_Data", "KDOB");
				System.out.println(strKDOB);
				System.out.println(strRelation);
				tryAction(fluentWaitElement(ColesAddkidPageObjects.selRelation),"specialDropdown","relationship ",strRelation);
				sleep(2000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.txtKFname), "SET","Kid First Name ",strKFname);
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.txtKLname), "SET","Kid Last Name " ,strKLname);
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.txtKDOB), "SET","Kid Date of Birth ",strKDOB);
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.radKgenMale), "Click","Male Gender");
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.lnkContinue), "Click","Continue" );
				sleep(2000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.chkDiagnosed), "Click","Has this child ever suffered from, been diagnosed?-None");
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.rdkidney), "Click","kidney disease?-No");
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.rdDrug), "Click","Drungs?-No");
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.rdHospital), "Click","Drungs?-No");
				sleep(1000);
				tryAction(waitForClickableElement(ColesAddkidPageObjects.lnkApply), "Click","Apply");
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
