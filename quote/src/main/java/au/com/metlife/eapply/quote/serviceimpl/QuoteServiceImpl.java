package au.com.metlife.eapply.quote.serviceimpl;

import java.math.BigDecimal;
import java.util.List;

import org.drools.KnowledgeBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.quote.model.ConvertModel;
import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.model.SelectItem;
import au.com.metlife.eapply.quote.service.QuoteService;
import au.com.metlife.eapply.quote.utility.DroolsHelper;
import au.com.metlife.eapply.quote.utility.EappRulesCacheProvider;
import au.com.metlife.eapply.quote.utility.QuoteConstants;
import au.com.metlife.eapply.quote.utility.QuoteHelper;

@Service
public class QuoteServiceImpl implements QuoteService {
	
	@Autowired(required = true)
	EappRulesCacheProvider eappRulesCacheProvider;

	@Override
	public QuoteResponse calculateDeath(RuleModel ruleModel) {
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER)){
						if(ruleModel.getDeathExistingAmount()!=null && ruleModel.getDeathTransferAmount()!=null){
							ruleModel.setDeathFixedAmount(ruleModel.getDeathExistingAmount().add(ruleModel.getDeathTransferAmount()));
						}
					}
//					else if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_CCOVER)){
//
//					}
					if(ruleModel.getDeathCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getDeathCoverType());
						if(ruleModel.getDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_UNITISED)){
							QuoteHelper.calculateDcUnitisedAmtAndCost(ruleModel,instProductRuleBO,quoteResponse,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}else if(ruleModel.getDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_FIXED)){
							QuoteHelper.calculateDeathFixedCost(instProductRuleBO,ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse);
						}
					}

				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return quoteResponse;
	}

	@Override
	public QuoteResponse calculateTpd(RuleModel ruleModel) {
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER)){
						if(ruleModel.getTpdExistingAmount()!=null && ruleModel.getTpdTransferAmount()!=null){
							ruleModel.setTpdFixedAmount(ruleModel.getTpdExistingAmount().add(ruleModel.getTpdTransferAmount()));
						}
					}
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					if(ruleModel.getTpdCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getTpdCoverType());
						if(ruleModel.getTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_UNITISED)){
							QuoteHelper.calculateTpdUnitisedAmtAndCost(ruleModel,instProductRuleBO,quoteResponse,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}else if(ruleModel.getTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_FIXED)){
							QuoteHelper.calculateTPDFixedCost(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse,instProductRuleBO);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return quoteResponse;
	}

	@Override
	public QuoteResponse calculateIp(RuleModel ruleModel) {
		QuoteResponse quoteResponse=null;
		BigDecimal ipUnts=null;
		BigDecimal ipUnitsCvrAmount;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
				if(ruleModel.getManageType()!=null){
					ipUnts=new BigDecimal(ruleModel.getIpUnits());
					System.out.println("ipUnts--->"+ipUnts);
					ipUnitsCvrAmount=ipUnts.multiply(new BigDecimal(instProductRuleBO.getIpUnitCostMultipFactor())).setScale(0, BigDecimal.ROUND_CEILING);
					ruleModel.setIpFixedAmount(ipUnitsCvrAmount);
					if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER)){
						if(ruleModel.getIpExistingAmount()!=null && ruleModel.getIpTransferAmount()!=null){
							ruleModel.setIpFixedAmount(ruleModel.getIpTransferAmount().add(ruleModel.getIpFixedAmount()));
						}
					}
					if(ruleModel.getIpCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getIpCoverType());
						if(ruleModel.getIpCoverType().equalsIgnoreCase(QuoteConstants.IP_UNITISED)){
							QuoteHelper.calculateIPUntCost(ruleModel,instProductRuleBO,quoteResponse,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}else if(ruleModel.getIpCoverType().equalsIgnoreCase(QuoteConstants.IP_FIXED)){
							/*SRKR for future use*/
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return quoteResponse;
	}

	@Override
	public List<SelectItem> getIndustryList(String fundCode) {
		CoverDetailsRuleBO industryRuleBO=null;
		try{
			if(fundCode!=null){
				industryRuleBO=new CoverDetailsRuleBO();
				industryRuleBO.setFundCode(fundCode);
				industryRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING);
				if(eappRulesCacheProvider.getRuleMap()!=null && eappRulesCacheProvider.getRuleMap().get(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING)!=null){
					industryRuleBO.setKbase((KnowledgeBase)eappRulesCacheProvider.getRuleMap().get(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING));
				}
				System.out.println("industry rule file name >>>>>>>"+industryRuleBO.getRuleFileName());
				industryRuleBO = DroolsHelper.invokeUC16BusinessRules(industryRuleBO,eappRulesCacheProvider.getFilePath());
				System.out.println("industry List >>>>>>>"+industryRuleBO.getIndustryList());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return industryRuleBO.getIndustryList();
	}

	@Override
	public InstProductRuleBO getProductConfig(String fundCode, String memberType,String manageType) {
		InstProductRuleBO instProductRuleBO=null;
		try{
			if(fundCode!=null && memberType!=null){
					instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(fundCode, memberType,manageType,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return instProductRuleBO;
	}

	@Override
	public ConvertModel convertDeathFixedToUnits(RuleModel ruleModel) {
		BigDecimal convertedUnits=null;
		ConvertModel convertModel=null;
		try{
			convertedUnits=QuoteHelper.convertDeathFixedToUnits(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath());
			convertModel=new ConvertModel();
			convertModel.setConvertedAmount(convertedUnits);
			convertModel.setExCoverAmount(ruleModel.getDeathExistingAmount());
			convertModel.setExCoverType(ruleModel.getExDeathCoverType());
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertModel;
	}

	@Override
	public ConvertModel convertTpdFixedToUnits(RuleModel ruleModel) {
		BigDecimal convertedUnits=null;
		ConvertModel convertModel=null;
		try{
			convertedUnits=QuoteHelper.convertTpdFixedToUnits(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath());
			convertModel=new ConvertModel();
			convertModel.setConvertedAmount(convertedUnits);
			convertModel.setExCoverAmount(ruleModel.getTpdExistingAmount());
			convertModel.setExCoverType(ruleModel.getExTpdCoverType());
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertModel;
	}
}
