package au.com.metlife.eapply.bs.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.SendFailedException;

import au.com.metlife.eapply.bs.email.MetLifeEmailService;
import au.com.metlife.eapply.bs.email.MetLifeEmailVO;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;

public class EmailHelper {
	
	
	public void sendEmail(PDFObject pdfObject,Eapplication applicationDTO, EapplyInput eapplyInput,
            String overAllDecision, String pdfAttachDir, String lob, Properties properties)
            {
        final String METHOD_NAME = "sendEmail"; //$NON-NLS-1$
        
        ArrayList mailList = new ArrayList();
        HashMap placeHolders = new HashMap();       
        String emailID = null; 
        String clientRefNum = null;
        MetLifeEmailVO eVO = new MetLifeEmailVO();      
        String applicantName = "";
        Applicant applicantDTO = null;
        Cover coversDTO = null;
        StringBuffer uwDecision = new StringBuffer();
        String acceptedCover = null;
        String declineCover = null;
        String declineReason = null;
        StringBuffer uwritingDecisionBlurb = new StringBuffer();
        boolean isSpecialTerm = Boolean.FALSE;
        String isCorpEapply=null;
        String htmlEmails=null;
        Boolean isChangeNo = null;
        Boolean mixedDecision = Boolean.FALSE;
        try {   	
        	mixedDecision = isMixedDecision(eapplyInput.getResponseObject());
            StringBuilder applicantBulder = null;
            if (null!=pdfObject && null != pdfObject.getMemberDetails()) {
                applicantBulder = new StringBuilder( "");
                
                if (pdfObject.getMemberDetails().getClientRefNum() != null) {
                	clientRefNum = pdfObject.getMemberDetails().getClientRefNum();
                }
                
                emailID = pdfObject.getMemberDetails().getEmailId();  
                
                if (pdfObject.getMemberDetails().getTitle() != null) {
                	  eVO.setTitle( pdfObject.getMemberDetails().getTitle());
                    applicantBulder.append( pdfObject.getMemberDetails().getTitle()
                            + " ");
                }else{
                	eVO.setTitle("");
                }
                if (pdfObject.getMemberDetails().getMemberFName() != null) {
                	 eVO.setFirstName( pdfObject.getMemberDetails().getMemberFName() );
                    applicantBulder.append( pdfObject.getMemberDetails().getMemberFName()
                            + " ");
                }else{
                	eVO.setFirstName("");
                }
                if (pdfObject.getMemberDetails().getMemberLName() != null) {
                	 eVO.setLastName( pdfObject.getMemberDetails().getMemberLName());
                    applicantBulder.append( pdfObject.getMemberDetails().getMemberLName());
                }else{
                	 eVO.setLastName( "");
                }
            }
            if (null != applicantBulder) {
                applicantName = applicantBulder.toString();
            }            
       if("Institutional".equalsIgnoreCase(lob)){
           if (null != applicationDTO && null != applicationDTO.getApplicant()) {
        	   
		        for (int itr = 0; itr < applicationDTO.getApplicant().size(); itr++) {
		        	applicantDTO = (Applicant)applicationDTO.getApplicant().get( itr); 
		        	applicantDTO = coversSelected(applicantDTO);
		            if (null != applicantDTO) {
		            	  if ("RUW".equalsIgnoreCase( applicationDTO.getAppdecision())) {
		                        uwDecision.append( "Referred to Underwriting ");                           
		                     
		                    }
		            	  else if ("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())) {
		            		  uwDecision.append( "Declined "); 		                                                                                 
		                        
		                    } else if ("ACC".equalsIgnoreCase( applicationDTO.getAppdecision()) 
		                    	) {
		                        uwDecision.append( "Accept ");        		                        
		                    }
		            	
		                for (int covItr = 0; covItr < applicantDTO.getCovers().size(); covItr++) {
		                	coversDTO = (Cover) applicantDTO.getCovers().get( covItr);                   	

		                    if ("DCL".equalsIgnoreCase(applicationDTO.getAppdecision()) && ((null != coversDTO.getCoverreason()
	                                && coversDTO.getCoverreason().length() > 0))) {	                                                                              
		                    				uwritingDecisionBlurb.append("\t"); 
		                                	if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
					                    		uwritingDecisionBlurb.append("Death cover - ");
					                  		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
					                  			uwritingDecisionBlurb.append("Total & Permanent Disability cover - ");
					                  		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
					                  			uwritingDecisionBlurb.append("Income Protection cover - ");
					                  		}		                                			                               
		                                	
		                                	if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 /*&& !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverReason())*/){
			                                	
			                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains("##") ){			
			                                		String declinereasons = coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf("##"));
			                                		declinereasons = declinereasons.replaceAll("##", " & ");
			                                		uwritingDecisionBlurb.append(declinereasons);			                                		
			                                		System.out
															.println("uwritingDecisionBlurb>>"+declinereasons);
			                                	}else{
			                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
			                                	}
			                                	
			                                }else if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 /*&& !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverReason())*/){
			                                	
			                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){	
			                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
			                                	}else{
			                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
			                                	}
			                                	
			                                }else if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 /*&& !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverOrgDclReason())*/){
			                                	//uwritingDecisionBlurb.append( "\n");                         	
			                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){	
			                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(",")));
			                                	}else{
			                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
			                                	}
			                                	
			                            }
		                                if(htmlEmails!=null && htmlEmails.equalsIgnoreCase("TRUE")){
		                                	uwritingDecisionBlurb.append("<br/><br/>");  
		                                	/*SRKR: 29/09/2014: For HTML emails add br for new line*/
		                                }else{
		                                	uwritingDecisionBlurb.append("\n\n");  
		                                }		                                
		                        
		                    }
		                    else if ("ACC".equalsIgnoreCase( applicationDTO.getAppdecision())
		                            && mixedDecision) {
		                    	 
		                    		acceptedCover = EapplyHelper.getDCLProductName(applicantDTO.getCovers(), "ACC", false);                   
			                        declineCover = EapplyHelper.getDCLProductName(applicantDTO.getCovers(),"DCL",false);			                    	
			                         declineReason = EapplyHelper.getDCLProductName(applicantDTO.getCovers(),"DCL",true);
			                         
			                         uwDecision.append( "Accept "+acceptedCover);   
		                        break;
		                    }
		                    uwDecision.append(coversDTO.getCovertype());
		                    if(covItr == applicantDTO.getCovers().size()-2){
		                    	uwDecision.append(" and ");
		                    }else if(covItr != applicantDTO.getCovers().size()-1){
		                    	uwDecision.append(", ");
		                    }
		                    
		                    
		                }
		                if("ACC".equalsIgnoreCase( applicationDTO.getAppdecision())
		                		&& ("true".equalsIgnoreCase(applicationDTO.getPaindicator() )|| pdfObject.isExclusionInd())){
		                	isSpecialTerm = Boolean.TRUE;                        	
                        }
		            }
		        }
		    } 
       }
            
         
            eVO.setApplicationId( pdfObject.getApplicationNumber());            
            
            //** Email Parameters *//*
        
	        eVO.setFromSender(properties.getProperty("fromsenderEmail"));      
	        eVO.setApplicationtype(lob);        
	        placeHolders.put( "EMAIL_SMTP_HOST", properties.getProperty("email_host"));
	        placeHolders.put( "EMAIL_SMTP_PORT", properties.getProperty("email_port"));
	       // placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SMTP_USERID, applicationDTO.getEmailSmtpUserId());
	       // placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SMTP_PASS, applicationDTO.getEmailSmtpPass());
	        
	        mailList.add( emailID);    
	     
	        if ("RUW".equalsIgnoreCase( overAllDecision)) {
	            eVO.setEmailTemplateId(properties.getProperty("email_body_ruw"));
	        } else if ("ACC".equalsIgnoreCase(overAllDecision)) {
                eVO.setEmailTemplateId(properties.getProperty("email_body_acc"));
            } else if ("DCL".equalsIgnoreCase(overAllDecision)) {
            	/*SRKR:15/10/2014 life events and transfer events decline flow is applicable only for VICS
            	 * For HOST REIS it wont come here.
            	 * */
            	if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty("email_body_uwcover_dcl"));
            	}else if("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty("email_body_icover_dcl"));
            	}else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty("email_body_tcover_dcl"));
            	}else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty("email_body_scover_dcl"));
            	}else{
            		eVO.setEmailTemplateId(properties.getProperty("email_body_dcl"));
            	}
                
            }else{
            	 eVO.setEmailTemplateId(properties.getProperty("email_body_acc"));
            }
	       
	     	        
	        String subject = null;

        	
      	  if ("RUW".equalsIgnoreCase( overAllDecision)) {
	        	  if(uwDecision.toString().contains("Referred to Underwriting")){
	        		  uwDecision = uwDecision.replace(uwDecision.indexOf("Referred to Underwriting"), uwDecision.indexOf("Referred to Underwriting")+25, "");  
	        	  }
      		  subject=properties.getProperty("email_brk_subj_ruw");
	          }else if ("DCL".equalsIgnoreCase(overAllDecision)) {
	        	
	        	if(uwDecision.toString().contains("Decline")){
	        		  uwDecision = uwDecision.replace(uwDecision.indexOf("Decline"), uwDecision.indexOf("Decline")+7, "");  
	        	}
	        	if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_uwcover_dcl");
	        	}else if("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_icover_dcl");
          	}else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
          		subject=properties.getProperty("email_brk_subj_tcover_dcl");
          	}else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
          		subject=properties.getProperty("email_brk_subj_scover_dcl");
          	}else{
          		subject=properties.getProperty("email_brk_subj_dcl");
          	}
	        	
            }else if ("ACC".equalsIgnoreCase(overAllDecision) && (pdfObject.isExclusionInd() || pdfObject.isLoadingInd())) {	            	  
          	  if(uwDecision.toString().contains("Mixed Acceptance and Decline")){
          		  uwDecision = uwDecision.replace(uwDecision.indexOf("Mixed Acceptance and Decline"), uwDecision.indexOf("Mixed Acceptance and Decline")+29, "Accept"); 
          	  }
          	  if(mixedDecision){
          		  subject=properties.getProperty("email_brk_subj_mxd_acc");          		
          		  if(null!=subject && subject.contains("<%SPL_COND%>")){
          			  subject=subject.replace("<%SPL_COND%>"," with special terms");
          		  }                		  
          		  System.out.println("subject special>>"+subject);
          	  }else if(pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){          		 
          		subject=properties.getProperty("email_brk_subj_load_exl_acc");
          	  }else if(pdfObject.isExclusionInd()){
          		  subject=properties.getProperty("email_brk_subj_exl_acc");
          	  }else if(pdfObject.isLoadingInd()){
          		subject=properties.getProperty("email_brk_subj_load_acc");
          	  }
            }else if ("ACC".equalsIgnoreCase(overAllDecision) ) {
          	  if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
          		subject = properties.getProperty("email_brk_subj_uwcover_acc"); 
	              }else if("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_icover_acc"); 
	              }else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_tcover_acc"); 
	              }else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_scover_acc"); 
	              }else if("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && mixedDecision){
	            	  subject=properties.getProperty("email_brk_subj_mxd_acc").replace("<%SPL_COND%>", "");
	              }else{
	            	  if(uwDecision.toString().contains("Mixed Acceptance and Decline")){
	            		  uwDecision = uwDecision.replace(uwDecision.indexOf("Mixed Acceptance and Decline"), uwDecision.indexOf("Mixed Acceptance and Decline")+29, "Accept"); 
	            	  }
	            	  subject=properties.getProperty("email_brk_subj_load_acc");
          	  }
            }else{
          	  subject=properties.getProperty("email_brk_subj_acc");
            }
          
      
	        isChangeNo = EapplyHelper.isNoChange(applicationDTO);
	        if("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo){
	        	subject = properties.getProperty("email_brk_subj_nochange_acc"); 
	        	 if(subject!=null && subject.contains("<%COVER%>")){
	        		 subject = subject.replace("<%COVER%>", EapplyHelper.getDCLProductName(applicantDTO.getCovers(), "ACC", false));	        		 
	        	 }
	        }       
	        
	        placeHolders.put( "TITLE", eVO.getTitle());
	        placeHolders.put( "FIRSTNAME", eVO.getFirstName());
	        placeHolders.put( "LASTNAME", eVO.getLastName());
	        placeHolders.put( "PDFLINK", pdfAttachDir);
	        placeHolders.put( "EMAIL_IND_SUBJECT_NAME", applicantName);
	        placeHolders.put("EMAIL_SUBJECT", subject);
	        /*SRKR; Adde new variable member_no to show in emails, usefull for GUIL and ACCS*/
	        if(pdfObject!=null && pdfObject.getMemberDetails()!=null && pdfObject.getMemberDetails().getClientRefNum()!=null){
	        	placeHolders.put( "MEMBER_NO", pdfObject.getMemberDetails().getClientRefNum());
	        }
	        
	        if("Institutional".equalsIgnoreCase(lob)){
	        	//eVO.setCcReciepent();        	
	        	
	        	placeHolders.put("UW_DECISION", uwDecision);
        		if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){        			
        			eVO.setEmailTemplateId(properties.getProperty("email_body_uwcover_acc"));	        			
	            }else if("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	 eVO.setEmailTemplateId(properties.getProperty("email_body_icover_acc"));
	            }else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	 eVO.setEmailTemplateId(properties.getProperty("email_body_tcover_acc"));
	            }else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	 eVO.setEmailTemplateId(properties.getProperty("email_body_scover_acc"));
	            }else if(pdfObject.isLoadingInd() && pdfObject.isExclusionInd()){
        			eVO.setEmailTemplateId(properties.getProperty("email_body_load_excl_acc"));
        		}else if(pdfObject.isLoadingInd()){
        			eVO.setEmailTemplateId(properties.getProperty("email_body_load_acc"));
        		}else if(pdfObject.isExclusionInd()){
        			eVO.setEmailTemplateId(properties.getProperty("email_body_excl_acc"));
        		}     		        	
	        	placeHolders.put("UW_DECISION_BLURB", "");	
	        	/*SRKR: 25/11/2013 Added !RUW because in case of mix acceptance and client match
	        	 * mixed accpetance flag is true and overall decision is RUW so add one more condition to check
	        	 * if it is not RUW than check for Mixed acceptance*/
	 	        if(!"RUW".equalsIgnoreCase(overAllDecision) && mixedDecision ){
	 	        	if(properties.getProperty("email_body_mxd").contains("<%ACC_COVER%>") && null!=acceptedCover){
	 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_mxd").replace("<%ACC_COVER%>", acceptedCover));
	 	        		 
	 	        	}
	 	        	if(properties.getProperty("email_body_mxd").contains("<%DCL_COVER%>") && null!=declineCover){
	 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_mxd").replace("<%DCL_COVER%>", declineCover));
	 	        	}
	 	        	if(properties.getProperty("email_body_mxd").contains("<%DCL_REASONS%>") && null!=declineReason){
	 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_mxd").replace("<%DCL_REASONS%>", declineReason));
	 	        	}	
	 	        	
	 	        	if(isSpecialTerm){
	 	        		if(properties.getProperty("email_body_mxd").contains("<%SPL_COND%>")){
		 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_mxd").replace("<%SPL_COND%>", " with special terms"));
		 	        	}
	 	        	}else{
	 	        		/*if(applicationDTO.getLoadingIndicator()  && eVO.getEmailTemplateId().contains("<%SPL_COND%>") ){
		        			eVO.setEmailTemplateId(eVO.getEmailTemplateId().replace("<%SPL_COND%>","with loadings"));
		        		}else*/ if(properties.getProperty("email_body_mxd").contains("<%SPL_COND%>")){
		 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_mxd").replace("<%SPL_COND%>", ""));
		 	        	}
	 	        	}        	
	 	        	
	 	        }else if ("DCL".equalsIgnoreCase(overAllDecision)) {
	            	/*SRKR:15/10/2014 life events and transfer events decline flow is applicable only for VICS
	            	 * For HOST REIS it wont come here.
	            	 * */
	 	        	if(applicationDTO.getRequesttype()!=null && "UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) ){ 	        		
	 	        		
	 	        		if(properties.getProperty("email_body_uwcover_dcl").contains("<%DCL_REASONS%>")){
	 	        			eVO.setEmailTemplateId(properties.getProperty("email_body_uwcover_dcl").replace("<%DCL_REASONS%>", uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}else if(applicationDTO.getRequesttype()!=null && "ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) ){ 	        		
	 	        		
	 	        		if(properties.getProperty("email_body_icover_dcl").contains("<%DCL_REASONS%>")){
	 	        			eVO.setEmailTemplateId(properties.getProperty("email_body_icover_dcl").replace("<%DCL_REASONS%>", uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}else if(applicationDTO.getRequesttype()!=null && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
		 	        	if(properties.getProperty("email_body_tcover_dcl").contains("<%DCL_REASONS%>")){
		 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_tcover_dcl").replace("<%DCL_REASONS%>", uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
		        	}else if(applicationDTO.getRequesttype()!=null && "SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
		 	        	if(properties.getProperty("email_body_scover_dcl").contains("<%DCL_REASONS%>")){
		 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_scover_dcl").replace("<%DCL_REASONS%>", uwritingDecisionBlurb.toString()));
		 	        	}		 	        	
		        	}else{
		 	        	if(properties.getProperty("email_body_dcl").contains("<%DCL_REASONS%>")){
		 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_dcl").replace("<%DCL_REASONS%>", uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}	 	        	
	 	        	
	 	        }else if("RUW".equalsIgnoreCase(overAllDecision)){
	 	        	eVO.setEmailTemplateId(properties.getProperty("email_body_ruw"));
	 	        }
	 	        
	        }
	        placeHolders.put( "APPLICATIONNUMBER", applicationDTO.getApplicationumber());	
	        

	        eVO.setEmailDataElementMap( placeHolders);       
	        eVO.setToRecipents( mailList); 	     
	       // String htmlEmails= null;
	        /*SRKR This flag determine whether to send html emails or not*/
	        eVO.setHtmlEmails(true);
	        /*SRKR: Added documments for APSD to fwd to fund administrator*/
	        if(applicationDTO.getDocuments()!=null){
	        	eVO.setDocumentList(applicationDTO.getDocuments());
	        }
	        if("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo ){
	        	eVO.setEmailTemplateId(properties.getProperty("email_body_nochange"));
	        }
	        
	        //bcc recipent
	       /*if(("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())|| "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())|| "SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) && null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	 eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        }else if("CCOVER".equalsIgnoreCase(applicationDTO.getRequestType()) && isChangeNo && null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	eVO.setEmailTemplateId(applicationDTO.getEmailBodyNoChange());
	        	eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        	// placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
	        }else if(null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        } */      
	        
	        eVO.setDocumentList(null);/*SRKR: Reset the documents so that it wont send for client.*/
	        
	        /**
	         * In institutional when the client email is present send the email to the client, 
	         */
	    	//REI in the case of client email 
        	if("Institutional".equalsIgnoreCase(lob)        						
        			&& 	null!=pdfObject.getMemberDetails().getClientEmailId() && pdfObject.getMemberDetails().getClientEmailId().length()>0){
        		mailList.clear();
        		/*SRKR:04/05/2015: Stopping client email for mixed acceptance: GEAU*/
        		if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())){
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId()); 
        			if( mixedDecision){   
        				
        				if(properties.getProperty("client_email_body_mxd").contains("<%SPL_COND%>") && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        					eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd").replace("<%SPL_COND%>", " Your cover has been accepted with an exclusion(s) and loading(s)  as advised in the Cover Summary."));
        				}else if(properties.getProperty("client_email_body_mxd").contains("<%SPL_COND%>") && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        					eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd").replace("<%SPL_COND%>", " Your cover has been accepted with loading(s) as advised in the Cover Summary."));
        				}else if(properties.getProperty("client_email_body_mxd").contains("<%SPL_COND%>") && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
        					eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd").replace("<%SPL_COND%>", " Your cover has been accepted with an exclusion(s) as advised in the Cover Summary."));
        				}else if(properties.getProperty("client_email_body_mxd").contains("<%SPL_COND%>")){
        					eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd").replace("<%SPL_COND%>", ""));
        				}
        				if(properties.getProperty("client_email_body_mxd").contains("<%ACC_COVER%>") && null!=acceptedCover){
         	        		eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd").replace("<%ACC_COVER%>", acceptedCover));         	        		 
         	        	}
         	        	if(properties.getProperty("client_email_body_mxd").contains("<%DCL_COVER%>") && null!=declineCover){
         	        		eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd").replace("<%DCL_COVER%>", declineCover));
         	        		
         	        	}         				
        				
        				placeHolders.put("UW_DECISION", "Acknowledgement");	
        				
        				
        			}else{
        				/*SRKR: From 17/10/2013 corp eapply onboarding created separate place holders for all possible combinations*/
        				 if("CARE".equalsIgnoreCase(pdfObject.getMemberDetails().getBrand())){        					
        					if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
        						 eVO.setEmailTemplateId(properties.getProperty("client_email_body_uwcover_acc") );        						 
        						 subject=  	properties.getProperty("client_email_subj_uwcover_acc"); 						 
        					}else if("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){        						
        						 eVO.setEmailTemplateId(properties.getProperty("client_email_body_icover_acc") );        						 
        						 subject=  	properties.getProperty("client_email_subj_icover_acc"); 
        					}else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){       						 	
       						 	eVO.setEmailTemplateId(properties.getProperty("client_email_body_tcover_acc") );        						 
       						 	subject=  	properties.getProperty("client_email_subj_tcover_acc"); 
        					}else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){       						 
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_scover_acc") );        						 
        						subject=  	properties.getProperty("client_email_subj_scover_acc"); 
        					}else if(pdfObject!=null && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_load_exl"));  
        						subject=  	properties.getProperty("client_email_subj_load_exl"); 
        					}else if(pdfObject!=null && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_load"));
        						subject=  	properties.getProperty("client_email_subj_load"); 
        					}else if(pdfObject!=null && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_exl"));
        						subject=  	properties.getProperty("client_email_subj_exl"); 
        					}else if(applicationDTO!=null && mixedDecision && pdfObject!=null){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_mxd"));  
        						subject=  	properties.getProperty("client_email_subj_mxd"); 
        					}else if(pdfObject!=null && !pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc"));
        						subject=  	properties.getProperty("client_email_subj_acc"); 
        					}

        				}/*else{
            				if(properties.getProperty("client_email_body_acc").contains("<%SPL_COND%>") && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace("<%SPL_COND%>", " Your cover has been accepted with an exclusion(s) and loading(s)  as advised in the Cover Summary."));
            				}else if(properties.getProperty("client_email_body_acc").contains("<%SPL_COND%>") && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace("<%SPL_COND%>", " Your cover has been accepted with loading(s) as advised in the Cover Summary."));
            				}else if(properties.getProperty("client_email_body_acc").contains("<%SPL_COND%>") && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace("<%SPL_COND%>", " Your cover has been accepted with an exclusion(s) as advised in the Cover Summary."));
            				}else if(properties.getProperty("client_email_body_acc").contains("<%SPL_COND%>")){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace("<%SPL_COND%>", ""));
            				}        				
        				}*/
        				placeHolders.put("UW_DECISION", uwDecision);	
        			}  	        	
        			
        		
        		}else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());         			
        			eVO.setEmailTemplateId(properties.getProperty("client_email_body_ruw"));
        			placeHolders.put("UW_DECISION", uwDecision);	
        		}else if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());         			
        			eVO.setEmailTemplateId(properties.getProperty("client_email_body_dcl"));
        			placeHolders.put("UW_DECISION", uwDecision);	
        		}
        		if(null!=mailList && mailList.size()>0){
        			/*For COrp Eapply Diff. email subj for Broker*/        			
    	        	  if ("RUW".equalsIgnoreCase( overAllDecision) && properties.getProperty("client_email_subj_ruw")!=null) {
    	        		  subject=properties.getProperty("client_email_subj_ruw");
    	  	          }else if ("DCL".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_subj_dcl")!=null) {
    	  	        	subject=properties.getProperty("client_email_subj_dcl");
    	              }else if ("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_subj_spl")!=null && (pdfObject.isExclusionInd() || pdfObject.isLoadingInd())) {
          				/*SRKR: From 17/10/2013 corp eapply onboarding created separate place holders for all possible combinations*/    	            	  
    	            	  subject=properties.getProperty("client_email_subj_spl");
    	            	  
    	            	  
    	              }else if ("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_subj_acc")!=null && !"UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && !"ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && !"TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())&& !"SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) {
    	            	  
    	            	  subject=properties.getProperty("client_email_subj_acc");
    	              }
    	        	  placeHolders.put("EMAIL_SUBJECT", subject);
        			eVO.setToRecipents( mailList); 	  
          		  eVO.setCcReciepent(null);
          		  eVO.setEmailDataElementMap( placeHolders); 
          		if("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo){          			
    	        	eVO.setEmailTemplateId(properties.getProperty("client_email_body_nochange"));    	        	
    	        }	
          		// For work rating update, don't send attachment to client
          		if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
          			pdfAttachDir=null;
          		}
          		  MetLifeEmailService.getInstance().sendMail( eVO, pdfAttachDir);	
        		}
        		 
           } 
	            
        }catch(SendFailedException emailException){
        	emailException.printStackTrace();
        } 
        catch (Exception e) {
        	e.printStackTrace();
        }finally{
        	 mailList = null;
             placeHolders = null;       
             emailID = null;          
        }
        
    }
	
	private Applicant coversSelected(Applicant applicantDTO){
    	Cover coversDTO = null;
    	List<Cover> validCvrList = new ArrayList<Cover>();
    	if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){
    		for(int itr=0;itr<applicantDTO.getCovers().size();itr++){
    			coversDTO = (Cover)applicantDTO.getCovers().get(itr);
    			if(null!=coversDTO && coversDTO.getCoverdecision()!=null && coversDTO.getCoverdecision().length()>0){
    				validCvrList.add(coversDTO);
    			}
    		}
    		applicantDTO.setCovers(validCvrList);
    	}
    	coversDTO = null;
    	validCvrList = null;
    	return applicantDTO;
    }
	
	private Boolean isMixedDecision(ResponseObject responseObject){
		Boolean mixedDecision = Boolean.FALSE;
		StringBuffer productdecisionBuffer = new StringBuffer();
		Insured insured = null;
		for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
			insured = (Insured) responseObject.getClientData().getListInsured().get(itr);					
			if (null != insured && null != insured.getListOverallDec()) {
				for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
					Decision decision = (Decision) insured.getListOverallDec().get(insItr);
					if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
						productdecisionBuffer.append(decision.getDecision());
					}
				}
			}
		}
		 if(null!=productdecisionBuffer && !"".equalsIgnoreCase(productdecisionBuffer.toString())){   
			 if(productdecisionBuffer.toString().contains("ACC") && productdecisionBuffer.toString().contains("DCL") && !productdecisionBuffer.toString().contains("RUW")){
				 mixedDecision = Boolean.TRUE;           	 
	            }
		  }
		
		return mixedDecision;
	}

}
