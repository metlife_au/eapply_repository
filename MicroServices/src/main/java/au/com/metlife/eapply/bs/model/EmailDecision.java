package au.com.metlife.eapply.bs.model;

public class EmailDecision {

		private String memberEmail;
		private String fundEmail;
		
		public String getMemberEmail() {
			return memberEmail;
		}
		public void setMemberEmail(String memberEmail) {
			this.memberEmail = memberEmail;
		}
		public String getFundEmail() {
			return fundEmail;
		}
		public void setFundEmail(String fundEmail) {
			this.fundEmail = fundEmail;
		}
}
