function authInterceptor(AppConstants, $window, $q) {
  'ngInject';

  return {
    // automatically attach Authorization header
    request: function(config) {
      let context = '';
      if(config.url.indexOf(AppConstants.urlFilePath) !== 0 && config._isTemplate) {
        config.url = config.url;
        config.headers.Authorization = inputData;
      } else if(config.url.indexOf(AppConstants.urlFilePath) !== 0 && config.url.indexOf(AppConstants.clientMatchUrl) !== 0 && !config._isTemplate) {
        context = baseUrl.indexOf('localhost') === -1  ? 'ebusiness/' : '';
        config.url = baseUrl + context + config.url;
        config.headers.Authorization = inputData;
        if(config.url.indexOf(AppConstants.appUrls.hostDecOccList) !== -1){
        	config.url = window.location.origin + '/eapply/'+AppConstants.appUrls.hostDecOccList;
        }
      }
      else if(config.url.indexOf(AppConstants.urlFilePath) === 0 && inputData) {
        config.headers.Authorization = inputData;
      }
      return config;
    },

    // Handle 401
    responseError: function(rejection) {
      if (rejection.status === 401) {
        // clear any JWT token being stored
        // JWT.destroy();
        // do a hard page refresh
        $window.location.reload();
      }
      return $q.reject(rejection);
    }

  };
}

export default authInterceptor;
