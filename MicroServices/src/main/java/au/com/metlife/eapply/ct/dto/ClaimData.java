package au.com.metlife.eapply.ct.dto;

import java.util.List;

public class ClaimData {
	private String uniqueId;
	private String claimId;
	private String surName;
	private String dob;
	private String status;
	private String statusDate;
	private String firstName;
	private String fundId;
	private String fundName;
	private String claimAssesor;
	private String productType;
	private List<DocumentDto> documentList;
	
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFundId() {
		return fundId;
	}
	public void setFundId(String fundId) {
		this.fundId = fundId;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getClaimAssesor() {
		return claimAssesor;
	}
	public void setClaimAssesor(String claimAssesor) {
		this.claimAssesor = claimAssesor;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}	
	public List<DocumentDto> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<DocumentDto> documentList) {
		this.documentList = documentList;
	}

}
