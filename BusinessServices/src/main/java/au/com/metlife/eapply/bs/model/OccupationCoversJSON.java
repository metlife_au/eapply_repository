package au.com.metlife.eapply.bs.model;

public class OccupationCoversJSON {
  private String citizenQue;
  
  private String fifteenHr;
  
  private String industryCode;
  
  private String industryName;
  
  private String occupation;
  
  private String managementRoleQue;
  
  private String salary;
  
  private String tertiaryQue;
  
  private String withinOfficeQue;
  
  private String gender;

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getCitizenQue() {
	return citizenQue;
}

public void setCitizenQue(String citizenQue) {
	this.citizenQue = citizenQue;
}

public String getFifteenHr() {
	return fifteenHr;
}

public void setFifteenHr(String fifteenHr) {
	this.fifteenHr = fifteenHr;
}

public String getIndustryCode() {
	return industryCode;
}

public void setIndustryCode(String industryCode) {
	this.industryCode = industryCode;
}

public String getIndustryName() {
	return industryName;
}

public void setIndustryName(String industryName) {
	this.industryName = industryName;
}

public String getOccupation() {
	return occupation;
}

public void setOccupation(String occupation) {
	this.occupation = occupation;
}

public String getManagementRoleQue() {
	return managementRoleQue;
}

public void setManagementRoleQue(String managementRoleQue) {
	this.managementRoleQue = managementRoleQue;
}

public String getSalary() {
	return salary;
}

public void setSalary(String salary) {
	this.salary = salary;
}

public String getTertiaryQue() {
	return tertiaryQue;
}

public void setTertiaryQue(String tertiaryQue) {
	this.tertiaryQue = tertiaryQue;
}

public String getWithinOfficeQue() {
	return withinOfficeQue;
}

public void setWithinOfficeQue(String withinOfficeQue) {
	this.withinOfficeQue = withinOfficeQue;
}
}
