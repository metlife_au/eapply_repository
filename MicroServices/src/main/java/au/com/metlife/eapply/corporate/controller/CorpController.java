package au.com.metlife.eapply.corporate.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.b2b.utility.BlowfishUtil;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.constants.EtoolkitConstants;
import au.com.metlife.eapply.corporate.business.CorpEntityRequest;
import au.com.metlife.eapply.corporate.business.CorpRequest;
import au.com.metlife.eapply.corporate.business.CorpResponse;
import au.com.metlife.eapply.corporate.business.DashboardEmailInput;
import au.com.metlife.eapply.corporate.business.DashboardResponse;
import au.com.metlife.eapply.corporate.service.CorpService;
import au.com.metlife.eapply.helper.CorporateHelper;
import au.com.metlife.eapply.mapper.CorpServiceMapper;
import au.com.metlife.eapply.repositories.MemberFileUploadRepo;
import au.com.metlife.eapply.repositories.model.MemberFileUpload;
import io.swagger.annotations.ApiOperation;


@RestController
public class CorpController {

	private static final Logger logger = LoggerFactory.getLogger(CorpController.class);
	@Autowired
	private CorpService corpService;
	@Autowired
	private EapplyService eapplyService;
	@Autowired
	private MemberFileUploadRepo memberRepo;

	/**
	 * 1. ExceptionHandler
	 */

	/**
	 * 2. Retrieve on-boarded fund
	 */
	/**
	 * Retrieve Fund REST Handler for http://localhost:8087/corporate/fund in
	 * GET(retrieve) This method is used to view the fund information for the given
	 * fund code.
	 * 
	 * @param fundCode
	 *            Type: String - Fund Code Identifier of the partner
	 * @param request
	 *            Type: {@link HttpServletRequest}
	 * @return Type: {@link ResponseEntity} : Response Object Wrapper
	 */
	@GetMapping("/corporate/fund")
	@ApiOperation(value = "Gets corporate fund details based on fund code and fund name", 
	notes = "Retrieves corporate fund details", response = CorpResponse.class)
	public ResponseEntity<CorpResponse> getFundsInformation(@RequestParam(value = "fundCode") String fundCode,
			@RequestParam(value = "fundName") String fundName, HttpServletRequest request) {

		CorpRequest corpRequest = CorpServiceMapper.mapRequestFromHttpRequest(request);
		if (null != fundCode && !fundCode.isEmpty()) {
			corpRequest.addParameter(EtoolkitConstants.FUND_CODE, fundCode);
			corpRequest.addParameter(EtoolkitConstants.FUND_NAME, fundName);
		}
		CorpResponse response = corpService.getOnboardedFunds(corpRequest);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Fund on-boarding
	 * 
	 * 
	 */
	@PostMapping("/corporate/fund")
	public ResponseEntity<CorpResponse> saveCorporateFund(@RequestParam(value = "userId") String userId,
			@RequestParam(value = "fundCode") String fundCode, @RequestBody CorpEntityRequest requestData,
			HttpServletRequest request) {

		CorpResponse response = null;

		CorpRequest corpRequest = CorpServiceMapper.mapRequestFromHttpRequest(request);
		corpRequest.addParameter(EtoolkitConstants.MODE_EDIT, "false");
		corpRequest.addParameter(EtoolkitConstants.FUND_CODE, fundCode);
		corpRequest.addParameter(EtoolkitConstants.USERID, userId);
		corpRequest.setRequestData(requestData);

		response = corpService.saveCorporateFund(corpRequest);

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	/**
	 * 3. Update on-boarded fund
	 */

	@PutMapping("/corporate/fund")
	public ResponseEntity<CorpResponse> updateCorporateFund(@RequestParam(value = "userId") String userId,
			@RequestParam(value = "fundCode") String fundCode, @RequestBody CorpEntityRequest requestData,
			HttpServletRequest request) {

		CorpResponse response = null;

		CorpRequest corpRequest = CorpServiceMapper.mapRequestFromHttpRequest(request);
		corpRequest.addParameter(EtoolkitConstants.MODE_EDIT, "true");
		corpRequest.addParameter(EtoolkitConstants.FUND_CODE, fundCode);
		corpRequest.addParameter(EtoolkitConstants.USERID, userId);
		corpRequest.setRequestData(requestData);

		response = corpService.saveCorporateFund(corpRequest);

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	/**
	 * Retrieve Fund REST Handler for http://localhost:8087/corporate/fund/cover in
	 * GET(retrieve) This method is used to view the fund information for the given
	 * fund code. (Cover page)
	 * 
	 * @param fundCode
	 *            Type: String - Fund Code Identifier of the partner
	 * @param request
	 *            Type: {@link HttpServletRequest}
	 * @return Type: {@link ResponseEntity} : Response Object Wrapper
	 */
	@GetMapping("/corporate/fund/cover")
	public ResponseEntity<CorpResponse> getFundCategoryInformation(@RequestParam(value = "fundCode") String fundCode,
			@RequestParam(value = "category") String category, HttpServletRequest request) {

		CorpRequest corpRequest = CorpServiceMapper.mapRequestFromHttpRequest(request);
		if (null != fundCode && !fundCode.isEmpty()) {
			corpRequest.addParameter(EtoolkitConstants.FUND_CODE, fundCode);
			corpRequest.addParameter(EtoolkitConstants.FUND_CATEGORY, category);
		}
		CorpResponse response = corpService.getFundCategoryInformation(corpRequest);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/corporate/login")
	public ResponseEntity<String> getLoginInformation(@RequestParam(value = "inputData") String inputData,
			@RequestParam(value = "inputIdentifier") String inputIdentifier,
			@RequestParam(value = "sdata") String sdata, @RequestParam(value = "c") String c)
			throws JSONException, ParseException {
		JSONObject fileuploadJson = new JSONObject();
		String sData = BlowfishUtil.decrypt(MetlifeInstitutionalConstants.GEN_KEY, sdata);
		if (!CorporateHelper.isNullOrEmpty(sData)) {
			sData = sData.trim();
			String[] splitParameters = null;
			splitParameters = sData.split(":");
			String applicationNumber=splitParameters[0];
	    	String dob=splitParameters[1];
	    	String lastName=splitParameters[2];
			/// Need to change the query to refine the result based on DOB as well as
			/// LastName
			MemberFileUpload upload = memberRepo.findByApplicationNumberAndSurname(applicationNumber, lastName);
			if(upload!=null) {
			if (upload.getMembershipCategory().contains("Category")) {
				upload.setMembershipCategory(upload.getMembershipCategory().substring(8));
			}
				upload.setDob(dob);
				fileuploadJson = CorporateHelper.LoginJson(upload);
			}
			else {
				String employeeNum= BlowfishUtil.decrypt(MetlifeInstitutionalConstants.GEN_KEY, inputData);
				String firstName = null;
				if(splitParameters.length>3) {
				firstName= splitParameters[3];
				}
				MemberFileUpload eviewsave = new MemberFileUpload();
				eviewsave.setDob(dob);
				eviewsave.setApplicationNumber(applicationNumber);
				eviewsave.setSurname(lastName);
				eviewsave.setFirstName(firstName);
				eviewsave.setEmployeeNumber(employeeNum);
				String inputIdentifiereviewSave= BlowfishUtil.decrypt(MetlifeInstitutionalConstants.GEN_KEY, inputIdentifier);
				if(inputIdentifiereviewSave!=null) {
				inputIdentifier=inputIdentifiereviewSave.trim();
				String[] fundCode= null;
				fundCode=inputIdentifier.split(":");
				if(fundCode.length>1)
				{
				eviewsave.setFundId(fundCode[0]);
				eviewsave.setMembershipCategory(fundCode[1]);
				eviewsave.setBrokerEmail(fundCode[2]);
				}
					eviewsave.setFundId(fundCode[0]);
				fileuploadJson = CorporateHelper.LoginJson(eviewsave);
				}
				}
		} else {
			if (!CorporateHelper.isNullOrEmpty(c)) {
				c = BlowfishUtil.decrypt(MetlifeInstitutionalConstants.CORPORATE_GEN_KEY, c);
				String inputdata = c.trim();
				fileuploadJson.put("AFUNDID", inputdata);
			}

		}
		return new ResponseEntity<>(fileuploadJson.toString(), HttpStatus.OK);
	}

	/**
	 * REST Handler for http://localhost:8087/corporate/memberdetails in
	 * GET(retrieve)
	 * 
	 * @param fundCode
	 * @param status
	 * @param firstName
	 * @param lastName
	 * @param applicationNumber
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws ParseException
	 *             This Method is used to Search for the application to be displayed
	 *             in the dashboard for a particular fund with parameters passed
	 */
	@GetMapping("/corporate/memberdetails")
	public ResponseEntity<List<DashboardResponse>> getFundStatus(@RequestParam(value = "fundCode") String fundCode,
			@RequestParam(value = "status") String status, @RequestParam(value = "firstName") String firstName,
			@RequestParam(value = "lastName") String lastName,
			@RequestParam(value = "applicationNumber") String applicationNumber,
			@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate)
			throws ParseException {
		java.sql.Date fromDatesSql = null;
		java.sql.Date toDatesSql = null;
		if (!CorporateHelper.isNullOrEmpty(fromDate) && !CorporateHelper.isNullOrEmpty(toDate)) {
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date fromdate = sdf1.parse(fromDate);
			fromDatesSql = new java.sql.Date(fromdate.getTime());
			java.util.Date todate = sdf1.parse(toDate);
			toDatesSql = new java.sql.Date(todate.getTime());
		}

		List<DashboardResponse> response = corpService.getMemberInformation(fundCode, status, firstName, lastName,
				applicationNumber, fromDatesSql, toDatesSql);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * REST Handler for http://localhost:8087/corporate/update in PUT(update)
	 * 
	 * @param fundCode
	 * @param status
	 * @param firstName
	 * @param lastName
	 * @param applicationNumber
	 * @return
	 * @throws ParseException
	 * 
	 *             This Method is used to update the Status for all the expired
	 *             application through dashboard to pending
	 * @throws JsonParseException
	 */

	@PutMapping("/corporate/update")
	public ResponseEntity<DashboardResponse> updateDB(@RequestBody String params)
			throws JsonMappingException, JsonParseException {
		DashboardResponse response = new DashboardResponse();
		ObjectMapper mapper = new ObjectMapper();
		String fundCode = null;
		String status = null;
		String firstName = null;
		String lastName = null;
		String applicationNumber = null;
		try {
			Map<String, Object> map = mapper.readValue(params, Map.class);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if ("fundCode".equalsIgnoreCase(entry.getKey())) {
					fundCode = (String) entry.getValue();
				}
				if ("status".equalsIgnoreCase(entry.getKey())) {
					status = (String) entry.getValue();
				}
				if ("firstName".equalsIgnoreCase(entry.getKey())) {
					firstName = (String) entry.getValue();
				}
				if ("lastName".equalsIgnoreCase(entry.getKey())) {
					lastName = (String) entry.getValue();
				}
				if ("applicationNumber".equalsIgnoreCase(entry.getKey())) {
					applicationNumber = (String) entry.getValue();
				}

			}
			response = corpService.updateDB(fundCode, status, firstName, lastName, applicationNumber);
		} catch (IOException e) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + e.getMessage());
		}
		//

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * REST Handler for http://localhost:8087/corporate/email in POST
	 * 
	 * @param fundCode
	 * @param status
	 * @param firstName
	 * @param lastName
	 * @param applicationNumber
	 * @return
	 * @throws ParseException
	 * 
	 *             This Method is used to resend email to the members who still has
	 *             an Active or Pending Application
	 * @throws FileNotFoundException
	 */
	@PostMapping("/corporate/email")
	public ResponseEntity<DashboardResponse> resendEmail(@RequestHeader(value = "Authorization") String authId,
			@RequestBody DashboardEmailInput dashboardEmailInput) throws FileNotFoundException {
		DashboardResponse response = corpService.sendEmail(dashboardEmailInput.getFundCode(),
				dashboardEmailInput.getEmailid(), dashboardEmailInput.getFirstName(), dashboardEmailInput.getLastName(),
				dashboardEmailInput.getApplicationNumber(), authId);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * @param applicationNumber
	 * @param fundCode
	 * @return
	 * 
	 * 		This Method is used to expire the Application on click of Cancel
	 */
	@RequestMapping(value = "/corporate/expireApplication", method = RequestMethod.POST)
	public ResponseEntity<Boolean> expireApplication(
			@RequestParam(value = "applicationNumber") String applicationNumber,
			@RequestParam(value = "fundCode") String fundCode) {
		Boolean expireApplication = Boolean.FALSE;
		if (applicationNumber != null) {
			Eapplication eapplication = eapplyService.expireApplication(applicationNumber);
			CorpResponse response = corpService.expireCorpApp(applicationNumber);
			if (eapplication != null && response != null) {
				expireApplication = Boolean.TRUE;
			}

		}
		return new ResponseEntity<>(expireApplication, HttpStatus.OK);
	}

	/**
	 * @param fundCode
	 * @return
	 * 
	 * 		This Method is used to expire the Application on click of Cancel
	 * @throws JSONException
	 */
	@PostMapping("/corporate/decryptFundCode")
	public ResponseEntity<String> decryptFundCode(@RequestParam(value = "fundcode") String fundcode,
			@RequestBody CorpEntityRequest requestData, HttpServletRequest request) throws JSONException {
		JSONObject fileuploadJson = new JSONObject();
		String inputdata = null;
		if (!CorporateHelper.isNullOrEmpty(fundcode)) {
			fundcode = BlowfishUtil.decrypt(MetlifeInstitutionalConstants.CORPORATE_GEN_KEY, fundcode);
			inputdata = fundcode.trim();
			fileuploadJson.put("FUNDID", inputdata);
		}
		return new ResponseEntity<>(fileuploadJson.toString(), HttpStatus.OK);
	}
}
