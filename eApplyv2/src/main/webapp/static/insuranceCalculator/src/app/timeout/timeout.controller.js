class TimeoutCtrl {
  constructor($scope, $state, ngDialog, $window, ConfigData) {
    'ngInject';

    this._$scope = $scope;
    this._$state = $state;
    this._$window = $window;
    this._ngDialog = ngDialog;
    this.config = ConfigData.getPartnerBasedConfiguration(inputData.partnerID);
   }

  init() {	  
	  if(navigator.appVersion.toString().indexOf('.NET') > 0){
		  window.onhashchange = function(e) {
			  window.history.forward(1);
		  };
	  }else{
			window.onpopstate = function(e){
				window.history.forward(1);
			};
	  }
	  
	  this._$scope.$on('$locationChangeStart', function(evnt, next, current){
	    	var currentURL = current.split("/");
	    	if(currentURL[currentURL.length-1] == 'timeout'){
	    		evnt.preventDefault();
	    	}    	
	  });
  }
    
}

export default TimeoutCtrl;
