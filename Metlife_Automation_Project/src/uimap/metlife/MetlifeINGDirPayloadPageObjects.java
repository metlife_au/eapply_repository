package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class MetlifeINGDirPayloadPageObjects extends PageObjectLibrary{
	public static final By txtInputstring = getObjectIdentifaction("metlife.ingdir.payloadpage.inputstring.text.id");
	public static final By btnProceednextpage = getObjectIdentifaction("metlife.ingdir.payloadpage.proceednextpage.button.css");
	public static final By btnNewapplication = getObjectIdentifaction("metlife.ingdir.payloadpage.newapplication.button.css");

	public static final By lnkChangeyourcover = getObjectIdentifaction("metlife.ingdir.reviewpage.changeyourcover.link.css");

	public static final By rdWork15hrsmoreYes = getObjectIdentifaction("metlife.ingdir.coverdtls.work15hrsmoreYes.radio.id");
	public static final By rdWork15hrsmoreNo = getObjectIdentifaction("metlife.ingdir.coverdtls.work15hrsmoreNo.radio.id");
	public static final By selIndustry = getObjectIdentifaction("metlife.ingdir.coverdtls.industry.dropdown.id");
	public static final By selCurroccupation = getObjectIdentifaction("metlife.ingdir.coverdtls.curroccupation.dropdown.id");
	public static final By txtCurrannualsal = getObjectIdentifaction("metlife.ingdir.coverdtls.currannualsal.text.id");
	public static final By selDeathselectcovertype = getObjectIdentifaction("metlife.ingdir.coverdtls.deathselectcovertype.dropdown.id");
	public static final By selTotalpermdisabilityselectcovertype = getObjectIdentifaction("metlife.ingdir.coverdtls.totalpermdisabilityselectcovertype.dropdown.id");
	public static final By selIncomeprotectionselectcovertype = getObjectIdentifaction("metlife.ingdir.coverdtls.incomeprotectionselectcovertype.dropdown.id");
	public static final By txtDeathunits = getObjectIdentifaction("metlife.ingdir.coverdtls.deathunits.text.id");
	public static final By txtTotalpermdisabilityunits = getObjectIdentifaction("metlife.ingdir.coverdtls.totalpermdisabilityunits.text.id");
	public static final By txtIncomeprotectionunits = getObjectIdentifaction("metlife.ingdir.coverdtls.incomeprotectionunits.text.id");
	public static final By lnkContinue = getObjectIdentifaction("metlife.ingdir.coverdtls.continue.link.id");
	public static final By lnkCalculatequote = getObjectIdentifaction("metlife.ingdir.coverdtls.calculatequote.link.id");
	public static final By lnkCancelCover = getObjectIdentifaction("metlife.ingdir.reviewpage.cancelyourcover.link.css");
	public static final By lnkCancelContinue = getObjectIdentifaction("metlife.ingdir.cancelpage.continuetocancel.link.css");
	public static final By lnkTransferCover = getObjectIdentifaction("metlife.ingdir.reviewpage.transfercover.link.css");
	public static final By txtTranDeathunits = getObjectIdentifaction("metlife.ingdir.trancover.deathunits.text.id");
	public static final By txtTranTPDunits = getObjectIdentifaction("metlife.ingdir.trancover.totalpermdisabilityunits.text.id");
	public static final By txtTranIPunits = getObjectIdentifaction("metlife.ingdir.trancover.incomeprotectionunits.text.id");
	public static final By selWaitPeriod = getObjectIdentifaction("metlife.ingdir.trancover.waitperiod.dropdown.id");
	public static final By selBenefitPeriod = getObjectIdentifaction("metlife.ingdir.trancover.benfitperiod.dropdown.id");
	public static final By rdTranHealthQuest = getObjectIdentifaction("metlife.ingdir.trancover.questions.radio.css");
	public static final By lnkCalquote = getObjectIdentifaction("metlife.ingdir.trancover.calculatequote.link.css");
	public static final By txtSaveRefNumber = getObjectIdentifaction("metlife.ingdir.savecover.referencenumber.text.css");
	public static final By lnkSaveFinish = getObjectIdentifaction("metlife.ingdir.savecover.finish.link.css");
	public static final By lnkRetrieve = getObjectIdentifaction("metlife.ingdir.savecover.retrieve.link.css");	
	public static final By tblmyapp = getObjectIdentifaction("metlife.ingdir.savecover.retrieve.table.css");

}
