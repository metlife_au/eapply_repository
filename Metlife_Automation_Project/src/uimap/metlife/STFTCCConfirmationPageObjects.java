package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTCCConfirmationPageObjects extends PageObjectLibrary{
		
	public static final By txtemail = getObjectIdentifaction("stft.cc.confirmation.email.text.id");
	public static final By txtemailconfirm = getObjectIdentifaction("stft.cc.confirmation.email.confirm.text.id");
	public static final By chkacknowledge = getObjectIdentifaction("stft.cc.confirmation.acknowledge.checkbox.id");
	public static final By btnNext = getObjectIdentifaction("stft.cc.confirmation.next.button.css");
}
