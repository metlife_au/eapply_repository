package au.com.metlife.eapply.bs.bizflowmodel;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contract")

@XmlRootElement
public class Contract {
	
	private String TransactionFunctionCode;
	private String ApplicationTypeCode;
	private String Description;
	private String LineOfBusinessCode;
	private String PolicyNumber;
	private String PolicyStatus;
	private String PolicyStatusDescription;
	private String ExternalReferenceNumber ;
	private String CaseNumber;
	private String ContractNumber;
	private String ContractStatus;
	private String ContractStatusDescription ;
	private Date EffectiveDate;
	private String MembershipTypeCode;
	private String CancellationTypeCode ;
	private String CancellationReasonCode ;
	private String ProductVariant ;
	private List<ContractList> ContractList;
	private List<Party> Party;
	private List<Coverage> Coverage;	
	private List<PartyInterest> PartyInterest;	
	private List<FileAttachment> FileAttachment;
	public String getApplicationTypeCode() {
		return ApplicationTypeCode;
	}
	public void setApplicationTypeCode(String applicationTypeCode) {
		ApplicationTypeCode = applicationTypeCode;
	}
	public String getCancellationReasonCode() {
		return CancellationReasonCode;
	}
	public void setCancellationReasonCode(String cancellationReasonCode) {
		CancellationReasonCode = cancellationReasonCode;
	}
	public String getCancellationTypeCode() {
		return CancellationTypeCode;
	}
	public void setCancellationTypeCode(String cancellationTypeCode) {
		CancellationTypeCode = cancellationTypeCode;
	}
	public String getCaseNumber() {
		return CaseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		CaseNumber = caseNumber;
	}
	public List<ContractList> getContractList() {
		return ContractList;
	}
	public void setContractList(List<ContractList> contractList) {
		ContractList = contractList;
	}
	public String getContractNumber() {
		return ContractNumber;
	}
	public void setContractNumber(String contractNumber) {
		ContractNumber = contractNumber;
	}
	public String getContractStatus() {
		return ContractStatus;
	}
	public void setContractStatus(String contractStatus) {
		ContractStatus = contractStatus;
	}
	public String getContractStatusDescription() {
		return ContractStatusDescription;
	}
	public void setContractStatusDescription(String contractStatusDescription) {
		ContractStatusDescription = contractStatusDescription;
	}
	public List<Coverage> getCoverage() {
		return Coverage;
	}
	public void setCoverage(List<Coverage> coverage) {
		Coverage = coverage;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Date getEffectiveDate() {
		return EffectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		EffectiveDate = effectiveDate;
	}
	public String getExternalReferenceNumber() {
		return ExternalReferenceNumber;
	}
	public void setExternalReferenceNumber(String externalReferenceNumber) {
		ExternalReferenceNumber = externalReferenceNumber;
	}
	public List<FileAttachment> getFileAttachment() {
		return FileAttachment;
	}
	public void setFileAttachment(List<FileAttachment> fileAttachment) {
		FileAttachment = fileAttachment;
	}
	public String getLineOfBusinessCode() {
		return LineOfBusinessCode;
	}
	public void setLineOfBusinessCode(String lineOfBusinessCode) {
		LineOfBusinessCode = lineOfBusinessCode;
	}
	public String getMembershipTypeCode() {
		return MembershipTypeCode;
	}
	public void setMembershipTypeCode(String membershipTypeCode) {
		MembershipTypeCode = membershipTypeCode;
	}
	public List<Party> getParty() {
		return Party;
	}
	public void setParty(List<Party> party) {
		Party = party;
	}
	public List<PartyInterest> getPartyInterest() {
		return PartyInterest;
	}
	public void setPartyInterest(List<PartyInterest> partyInterest) {
		PartyInterest = partyInterest;
	}
	public String getPolicyNumber() {
		return PolicyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		PolicyNumber = policyNumber;
	}
	public String getPolicyStatusDescription() {
		return PolicyStatusDescription;
	}
	public void setPolicyStatusDescription(String policyStatusDescription) {
		PolicyStatusDescription = policyStatusDescription;
	}
	public String getProductVariant() {
		return ProductVariant;
	}
	public void setProductVariant(String productVariant) {
		ProductVariant = productVariant;
	}
	public String getTransactionFunctionCode() {
		return TransactionFunctionCode;
	}
	public void setTransactionFunctionCode(String transactionFunctionCode) {
		TransactionFunctionCode = transactionFunctionCode;
	}
	public String getPolicyStatus() {
		return PolicyStatus;
	}
	public void setPolicyStatus(String policyStatus) {
		PolicyStatus = policyStatus;
	}
	
	
	
	
	
	
}
