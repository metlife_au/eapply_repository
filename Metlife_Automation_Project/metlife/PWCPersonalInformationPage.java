package pages.metlife;

import org.openqa.selenium.By;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.PersonalInformationPageObjects;

public class PWCPersonalInformationPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public PWCPersonalInformationPage enterPersonalInformationDetails() {
		fillPersonalInformationDetails();
		return new PWCPersonalInformationPage(scriptHelper);
	}
	
	public PWCPersonalInformationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillPersonalInformationDetails(){
		try {
			
			String strTitle = dataTable.getData("MetLife_Data", "title");
			String strConfidentialEmail = dataTable.getData("MetLife_Data", "personal.emailAddress");
			String strPreferredContactNumber = dataTable.getData("MetLife_Data", "personal.ContactDetails");
			String strPreferredContactTime = dataTable.getData("MetLife_Data", "personal.ContactTime");
						
			tryAction(fluentWaitElements(PersonalInformationPageObjects.listTitle),"DropDownSelect","Title",strTitle);
			tryAction(waitForClickableElement(PersonalInformationPageObjects.txtConfidentialEmailId),"SET","Preferred Confidential EmailId",strConfidentialEmail);
			tryAction(waitForClickableElement(PersonalInformationPageObjects.txtConfirmEmail),"SET","Confirm EmailId",strConfidentialEmail);
			tryAction(waitForClickableElement(PersonalInformationPageObjects.txtPreferredContact),"SET","Preferred Contact Number",strPreferredContactNumber);
			Boolean blnPcl=driver.findElement(By.id("pref_cont_list")).isDisplayed();
			//System.out.println(blnPcl);
			if(blnPcl==true){
			tryAction(waitForClickableElement(PersonalInformationPageObjects.lstPreferredContactTime),"DropDownSelect","Preferred Contact Time",strPreferredContactTime);
			}
			tryAction(waitForClickableElement(PersonalInformationPageObjects.btnSubmit),"click","Submit");
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
