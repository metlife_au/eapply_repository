(function (angular) {
	'use strict';
	/* global token, moment */
	/**
	 * @ngdoc controller
	 * @name CorporateApp.controller:onboardCtrl
	 *
	 * @description
	 * This is onboardCtrl description
	 */

	angular
		.module('CorporateApp')
		.controller('onboardCtrl', onboardCtrl);

	onboardCtrl.$inject= ['$scope', 'APP_CONSTANTS', 'corporateFund', 'fetchUrlSvc', '$stateParams','fetchTokenNumSvc'];

	function onboardCtrl($scope, APP_CONSTANTS,corporateFund,fetchUrlSvc,$stateParams, fetchTokenNumSvc) {
		var vm= this;
		vm.token = token || '';
		fetchTokenNumSvc.setTokenId(vm.token);
		vm.hideForm=false;
		vm.errorMinMax=0;
		vm.categories= [];
		vm.fundDetails= {};
		vm.fundDetails.partner= {};
		vm.fundDetails.partner.employer= {};
		vm.fundDetails.partner.fundCat= [];
		vm.modelOptions = {updateOn: 'blur'};
		vm.constants= {
			phoneNumber: APP_CONSTANTS.onboardDetails.phoneNumber,
			emailFormat: APP_CONSTANTS.onboardDetails.emailFormat,
			waitingPeriodOptions: APP_CONSTANTS.onboardDetails.waitingPeriodOptions,
			benefitPeriodOptions: APP_CONSTANTS.onboardDetails.benefitPeriodOptions
		};
		vm.urlList= fetchUrlSvc.getUrlList();	
		/**
		 * @ngdoc method
		 * @name have to add here
		 * @methodOf CorporateApp.controller:onboardCtrl
		 * @description
		 * To add all fund related details 
		 **/
		vm.init= function(){
//			vm.setFundDetails($stateParams);
			var fundDetails= $stateParams.fundcode.split('&');
			vm.fundCode= fundDetails[0].split('=')[1];
			corporateFund.getDecryptContent(vm.urlList.decryptContent,vm.fundDetails,vm.fundCode).then(
					function(res){
						var response= res.data;
						console.log(response.FUNDID);
						vm.fundDetails.partner.fundCode=response.FUNDID;
						console.log(vm.fundDetails.partner.fundCode);
						vm.eApplyUser= fundDetails[1].split('=')[1];
						vm.fundDetails.partner.partnerName= fundDetails[2].split('=')[1];
						vm.isAdd= true;
						console.log(vm.fundDetails.partner.fundCode);
						corporateFund.getDetails(vm.urlList.corporatePost, vm.fundDetails.partner.partnerName, vm.fundDetails.partner.fundCode).then(
								function(res){
									var response= res.data;
									vm.fundDetails=response;
									console.log('response>>'+JSON.stringify(response));
									if(response.partner.fundCat!= null && response.partner.fundCat.length){
										vm.categories= response.partner.fundCat;
										vm.isAdd= false;
										angular.forEach(vm.categories, function(value, key){
											if(value.deathCover=== null){
												value.deathCover= {};
											}
											value.deathCover.deathMaxAgeErr= false;
											value.deathCover.deathMaxSumInsErr= false;
											if(value.tpdCover=== null){
												value.tpdCover= {};
											}
											value.tpdCover.tpdMaxAgeErr= false;
											value.tpdCover.tpdMaxSumInsErr= false;
											if(value.ipCover=== null){
												value.ipCover= {};
											}
											value.ipCover.ipMaxAgeErr= false;
											value.ipCover.ipMaxSumInsErr= false;
											
										});	
									}else{
										vm.addCategory();
									}
								},
								function(err){
									console.info('Something went wrong while saving...'+ JSON.stringify(err));
							});
					},
						function(err){
					console.info('Something went wrong while saving...'+ JSON.stringify(err));
			});
			
		
			
		};
		vm.setFundDetails= function(params){
			var fundDetails= params.fundcode.split('&');
			vm.fundCode= fundDetails[0].split('=')[1];
			corporateFund.getDecryptContent(vm.urlList.decryptContent,vm.fundDetails,vm.fundCode).then(
					function(res){
						var response= res.data;
						console.log(response.FUNDID);
						vm.fundDetails.partner.fundCode=response.FUNDID;
						
					},
						function(err){
					console.info('Something went wrong while saving...'+ JSON.stringify(err));
			});
			console.log(vm.fundDetails.partner.fundCode);
			vm.eApplyUser= fundDetails[1].split('=')[1];
			vm.fundDetails.partner.partnerName= fundDetails[2].split('=')[1];
		};
		vm.checkForMinMax=function(input,min,max,index){
			if(input==='deathAge'){
				if(parseInt(min)>parseInt(max)){
					vm.categories[index].deathCover.deathMaxAgeErr=true;					
				}else{
					vm.categories[index].deathCover.deathMaxAgeErr=false;
				}
				
				if(parseInt(min)<15){
					vm.categories[index].deathCover.deathMinAgeLimit=true;					
				}else{
					vm.categories[index].deathCover.deathMinAgeLimit=false;
				}
				
				if(parseInt(max)>70){
					vm.categories[index].deathCover.deathMaxAgeLimit=true;					
				}else{
					vm.categories[index].deathCover.deathMaxAgeLimit=false;
				}
				
			}
			if(input==='deathSum'){
				if(parseInt(min)>parseInt(max)){
					vm.categories[index].deathCover.deathMaxSumInsErr=true;
				}else{
					vm.categories[index].deathCover.deathMaxSumInsErr=false;
				}
			}
			
			if(input==='deathAAl'){
				
				if(parseInt(min)>20000000){
					vm.categories[index].deathCover.deathAAlLimit=true;					
				}else{
					vm.categories[index].deathCover.deathAAlLimit=false;
				}
				
			}
			if(input==='tpdAge'){
				if(parseInt(min)>parseInt(max)){
					vm.categories[index].tpdCover.tpdMaxAgeErr=true;
				}else{
					vm.categories[index].tpdCover.tpdMaxAgeErr=false;
				}
				
				if(parseInt(min)<15){
					vm.categories[index].tpdCover.tpdMinAgeLimit=true;					
				}else{
					vm.categories[index].tpdCover.tpdMinAgeLimit=false;
				}
				
				if(parseInt(max)>64){
					vm.categories[index].tpdCover.tpdMaxAgeLimit=true;					
				}else{
					vm.categories[index].tpdCover.tpdMaxAgeLimit=false;
				}
			}
			if(input==='tpdSum'){
				if(parseInt(min)>parseInt(max)){
					vm.categories[index].tpdCover.tpdMaxSumInsErr=true;
				}else{
					vm.categories[index].tpdCover.tpdMaxSumInsErr=false;
				}
				
				if(parseInt(max)>10000000){
					vm.categories[index].tpdCover.tpdMaxSumInsLimit=true;					
				}else{
					vm.categories[index].tpdCover.tpdMaxSumInsLimit=false;
				}
			}
			
			if(input==='TPDAAl'){
				
				if(parseInt(min)>5000000){
					vm.categories[index].tpdCover.tpdAAlLimit=true;					
				}else{
					vm.categories[index].tpdCover.tpdAAlLimit=false;
				}
				
			}
			if(input==='ipAge'){
				if(parseInt(min)>parseInt(max)){
					vm.categories[index].ipCover.ipMaxAgeErr=true;
				}else{
					vm.categories[index].ipCover.ipMaxAgeErr=false;
				}
				
				if(parseInt(min)<15){
					vm.categories[index].ipCover.ipMinAgeLimit=true;					
				}else{
					vm.categories[index].ipCover.ipMinAgeLimit=false;
				}
				
				if(parseInt(max)>64){
					vm.categories[index].ipCover.ipMaxAgeLimit=true;					
				}else{
					vm.categories[index].ipCover.ipMaxAgeLimit=false;
				}
			}
			if(input==='ipSum'){
				if(parseInt(min)>parseInt(max)){
					vm.categories[index].ipCover.ipMaxSumInsErr=true;
				}else{
					vm.categories[index].ipCover.ipMaxSumInsErr=false;
				}
				
				if(parseInt(max)>30000){
					vm.categories[index].ipCover.ipMaxSumInsLimit=true;					
				}else{
					vm.categories[index].ipCover.ipMaxSumInsLimit=false;
				}
			}
			
			if(input==='ipAAl'){
				
				if(parseInt(min)>50000){
					vm.categories[index].ipCover.ipAAlLimit=true;					
				}else{
					vm.categories[index].ipCover.ipAAlLimit=false;
				}
				
			}
			
			if(input==='ipSalPer'){
				
				if(parseInt(min)>85){
					vm.categories[index].ipCover.ipSalPerLimit=true;					
				}else{
					vm.categories[index].ipCover.ipSalPerLimit=false;
				}
				
			}

			vm.checkErrorMinMax();
		};
		
		vm.checkErrorMinMax= function(){
			vm.errorMinMax= 0;						
			angular.forEach(vm.categories, function (category, key) {
				if(category.ipCover.ipSalPerLimit || category.ipCover.ipAAlLimit || category.ipCover.ipMaxSumInsLimit || category.ipCover.ipMinAgeLimit || category.ipCover.ipMaxAgeLimit || category.tpdCover.tpdAAlLimit || category.tpdCover.tpdMaxSumInsLimit || category.tpdCover.tpdMinAgeLimit || category.tpdCover.tpdMaxAgeLimit || category.deathCover.deathAAlLimit || category.deathCover.deathMaxAgeLimit || category.deathCover.deathMinAgeLimit || category.deathCover.deathMaxAgeErr || category.deathCover.deathMaxSumInsErr || category.tpdCover.tpdMaxAgeErr || category.tpdCover.tpdMaxSumInsErr || category.ipCover.ipMaxAgeErr || category.ipCover.ipMaxSumInsErr) {
					vm.errorMinMax++;
				}	
			});
			
		};
		
		vm.addCategory= function(){
			//var length= vm.categories.length + 1;
			vm.categories.push({
				id: '',
				category: null,
				deathReqd: 'N',
				tpdReqd: 'N',
				ipReqd: 'N',
				glPolicyNum: '',
				deathCover:{
					deathMinAge: null,
					deathMaxAge: null,
					deathMaxAgeErr:false,
					deathMinSumIns: null,
					deathMaxSumIns: null,
					deathMaxSumInsErr:false,
					deathAal: null
				},
				tpdCover: {
					tpdMinAge: null,
					tpdMaxAge: null,
					tpdMaxAgeErr:false,
					tpdMinSumIns: null,
					tpdMaxSumIns: null,
					tpdMaxSumInsErr:false,
					tpdAal: null
				},
				ipCover:{
					ipMinAge: null,
					ipMaxAge: null,
					ipMaxAgeErr:false,
					ipMinSumIns: null,
					ipMaxSumIns: null,
					ipMaxSumInsErr:false,
					ipSumInsType: null,
					ipFormula: null,
					ipAal: null,
					ipWp: vm.constants.waitingPeriod,
					ipBp: vm.constants.benefitPeriod,
					ipSciPolicyNum: null
				}
			});
			console.log(vm.categories);
		};
		vm.removeCategory= function(index){  
            vm.categories.splice(index,1); 
		};
		
		vm.reset= function(index,cover){
			if(cover==='death'){
				vm.categories[index].deathCover={};
			}
			if(cover==='tpd'){
				vm.categories[index].tpdCover={};
			}
			if(cover==='ip'){
				vm.categories[index].ipCover={};
			}
		};
		vm.submit= function() {
			if(vm.errorMinMax===0){
				vm.hideForm=true;
				vm.fundDetails.partner.fundCat= vm.categories;
				console.log(JSON.stringify(vm.fundDetails));
				if(vm.isAdd){ 
					
					corporateFund.add(vm.urlList.corporatePost,vm.fundDetails,vm.eApplyUser,vm.fundDetails.partner.fundCode).then(
							function(res){
								var response= res.data;
								vm.fundDetails=response;
								vm.isAdd= false;
								console.log('response>>'+JSON.stringify(response));
								vm.showMsg('success', 'The on boarding of the Corporate partner  '+vm.fundDetails.partner.partnerName+' has been successful.');
							},
							function(err){
								vm.showMsg('error', 'Submission failed, Please contact your administrator for further details');
								console.info('Something went wrong while saving...'+ JSON.stringify(err));
						});
				}else{
					corporateFund.update(vm.urlList.corporatePost,vm.fundDetails,vm.eApplyUser,vm.fundDetails.partner.fundCode).then(
							function(res){
								var response= res.data;
								vm.fundDetails=response;
								
								if(response.partner.fundCat!= null){
									vm.categories= response.partner.fundCat;
									vm.isAdd= false;
									angular.forEach(vm.categories, function(value, key){
										if(value.deathCover=== null){
											value.deathCover= {};
										}
										value.deathCover.deathMaxAgeErr= false;
										value.deathCover.deathMaxSumInsErr= false;
										if(value.tpdCover=== null){
											value.tpdCover= {};
										}
										value.tpdCover.tpdMaxAgeErr= false;
										value.tpdCover.tpdMaxSumInsErr= false;
										if(value.ipCover=== null){
											value.ipCover= {};
										}
										value.ipCover.ipMaxAgeErr= false;
										value.ipCover.ipMaxSumInsErr= false;
										
									});	
								}
								
								console.log('response>>'+JSON.stringify(response));
								vm.showMsg('success', 'The update of the Corporate partner ' +vm.fundDetails.partner.partnerName+ ' has been successful.');
							},
							function(err) {
								vm.showMsg('error', 'Submission failed,Please contact your administrator for further details');
								console.info('Something went wrong while updating...'+ JSON.stringify(err));
						});
				}
			}
		};
		vm.showMsg= function(type, text){
			vm.successMsg= '';
			vm.errorMsg= '';
			vm.showAlerts= true;
			if(type==='success'){
				vm.successMsg= text;
			}else{
				vm.errorMsg= text;
			}
		};
		
		vm.clearAlerts= function(){
			vm.successMsg= '';
			vm.errorMsg= '';
		};
	}
})(angular);
