class LandingCtrl {
  constructor(CommonAPI, AppConstants, $scope, $state, ngDialog, moment, $window, $q, $timeout, EapplyData) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._MomentAPI = moment;
    this._$window = $window;
    this._$q = $q;
    this._$timeout = $timeout;
    this._ngDialog = ngDialog;
    this._EapplyData = EapplyData;
    this.appicantData = {};

  }

  init() {
    let deferred = this._$q.defer();
    this._CommonAPI.setAuthToken(inputData);
    this.urlList = this._CommonAPI.getUrlList();
    /* jshint expr: true */
    !this._EapplyData.getClientData() && this._EapplyData.fetchClientData(this.urlList.clientDataUrl).then(
      (res) => {
        this.appicantData = res.applicant[0];
        this.appicantData.age = this._MomentAPI().diff(this._MomentAPI(this.appicantData.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years') + 1;
        this._EapplyData.setApplicantRawXmlData(this.appicantData);
        console.log(this.appicantData);
        this._CommonAPI.setAppCRN(this.appicantData.clientRefNumber);
        this.exDthCover = this._EapplyData.getExistingDeathCover();
        this.exTpdCover = this._EapplyData.getExistingTpdCover();
        this.exIpCover = this._EapplyData.getExistingIpCover();
        deferred.resolve({});
      },
      (err) => {
        this.errors = err.data.errors;
        deferred.reject(err);
      }
    );
    this._EapplyData.getClientData() && deferred.resolve({});

    return deferred.promise;
  }

  onScreenLoad() {
    this.init().then(
      (res) => {
        this.exDthCover = this._EapplyData.getExistingDeathCover() || {};
        this.exTpdCover = this._EapplyData.getExistingTpdCover() || {};
        this.exIpCover = this._EapplyData.getExistingIpCover() || {};
        this._$timeout(() => {
          $('#existing-cover').collapse('show');
          $('#manage-cover').collapse('show');
          $('#insurance-guide').collapse('show');
        });
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );
  }

  // Initiate application by launching cover flow
  launchCover(type) {
    let manageType = type.toUpperCase();
    if(manageType === 'CCOVER' || manageType === 'TCOVER' || manageType === 'CANCOVER') {
      this._EapplyData.checkDuplicateApps(this.urlList.dupAppUrl, manageType).then(
        (res) => {
          console.log(res);
    				if(res) {
    					this._ngDialog.openConfirm({
                template:'<div class="ngdialog-contentdup"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">YOU HAVE PREVIOUSLY SUBMITTED APPLICATION(S)</h4></div><div class="modal-body"><!-- Row starts --><div class="row"><div class="col-12"><p> Thank you for commencing your application, we note that you have recently lodged an application for additional insurance in the last 30 days that was referred to our underwriting team, please note that your application is currently under assessment and we will be in contact with you as soon as possible with an update.</p> <p>If you would like to initiate a different application to the one that is pending, please continue with this application.</p> </div></div></div></br><div class="ngdialog-footer"><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Close</button></div></div></div>',
                plain: true,
                showClose: false,
                className: 'ngdialog-theme-plain custom-width'
              }).then( () => {
                return false;
              }, (e) => {
                if(e=='oncancel') {
                  return false;
                }
              });
            } else {
              this.checkSavedApps(manageType);
            }
        },
        (err) => {
          this.errors = err.data.errors;
        }
      );
    }
  }
  
  routeSavedApps(savedApps, manageType) {
	  
	  let templateContent = '<div class="ngdialog-content saved-app-modal"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">You have previously saved application(s).</h4></div>';
	  templateContent += '<div class="modal-body"><div class="row"><div class="col-12"><p> Please continue with the saved application or cancel saved application to start a new application. </p> </div></div>';
	  templateContent += '<div class="row d-block d-sm-none"><div class="col-12">';
	  templateContent += '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr><th class="saved-table-head"><h6>Application no.</h6></th><th class="saved-table-head "><h6>Date saved</h6></th>';
	  templateContent += '<th class="saved-table-head"><h6>Service request type</h6> </th></tr>';
	  
	  for (let i = 0; i < savedApps.length; i++) {
		  templateContent += '<tr><td>'+ savedApps[i].applicationNumber +'</td>';
		  templateContent += '<td>'+ savedApps[i].createdDate +'</td>';
		  templateContent += '<td>'+ savedApps[i].requestType +'</td></tr>'; 
	  }
	  
	  templateContent += '</tbody> </table></div></div>';
	  
	  templateContent += '<div class="row d-none d-sm-block"><div class="col-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody>';
	  templateContent += '<tr><th width="20%" class="saved-table-head"><h6>Application no.</h6></th><th width="20%" class="saved-table-head"><h6>Date saved</h6></th><th width="20%" class="saved-table-head"><h6>Service request type</h6> </th></tr>';
	  
	  for (let i = 0; i < savedApps.length; i++) {
		  templateContent += '<tr><td>'+ savedApps[i].applicationNumber +'</td>';
		  templateContent += '<td>'+ savedApps[i].createdDate +'</td>';
		  templateContent += '<td>'+ savedApps[i].requestType +'</td></tr>'; 
	  }
		  
	  templateContent += '</tbody> </table></div></div></div></br>';
	  templateContent += '<div class="ngdialog-footer"><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button></div></div></div>';	  
	  this._ngDialog.openConfirm({
		  template : templateContent,         
        plain: true,
        showClose: false,
        className: 'ngdialog-theme-plain custom-width'
      }).then(  		
        () => {
          let resData = savedApps[0];		
          console.log(resData);
          this._CommonAPI.setAppNumber(resData.applicationNumber);
          this._EapplyData.retrieveApp(resData);
        }, 
        (event) => {
          if(event === 'oncancel') {
        	  
        	  this._CommonAPI.cancelSavedApps(this.urlList.cancelAppUrl, savedApps[0].applicationNumber).then(
        			  (res) => {
        				  //generate app number
        		            this._CommonAPI.generateAppNumber(this.urlList.appNumUrl).then(
        		              (res) => {
        		                switch(manageType) {
        		                  case "CCOVER":
        		                	this._EapplyData.setChangeCoverData(null);
        		                    this._$state.go('app.quote');
        		                    break;
        		                  case "TCOVER":
        		                	  this._EapplyData.setTransferData(null);
        		                    this._$state.go('app.transfercover');
        		                    break;
        		                  default:
        		                  break;
        		                }
        		              },
        		              (err) => {
        		                this.errors = err.data.errors;
        		              }
        		            );
        		      },
        		      (err) => {
        		          
        		        }
        	  );
          }
        }
      );	  
  }

  checkSavedApps(type) {
    let manageType = type.toUpperCase();
    if(manageType === 'CCOVER' || manageType === 'TCOVER') {
      this._CommonAPI.fetchSavedApps(this.urlList.savedAppUrl, manageType).then(
        (res) => {
          console.log(res);
          if(res.length > 0) {
        	  
        	  this.savedApplications = res;
        	  let resLength = this.savedApplications.length;
        	  for (let i = 0; i < resLength; i++) {
              let createdDate = new Date(this.savedApplications[i].createdDate);
              this.savedApplications[i].createdDate = this._MomentAPI(createdDate).format('DD/MM/YYYY');

              if (this.savedApplications[i].requestType === 'CCOVER') {
                this.savedApplications[i].requestType = 'Change Cover';
              } else if (this.savedApplications[i].requestType === 'TCOVER') {
                this.savedApplications[i].requestType = 'Transfer Cover';
              }
            }
        	  
        	  this.routeSavedApps(this.savedApplications, manageType);       	  
        	  
          } else {
        	  this._EapplyData.setChangeCoverData(null);
            switch(manageType) {
              case "CCOVER":
                this._$state.go('app.quote');
                break;
              case "TCOVER":
                this._$state.go('app.transfercover');
                break;
              case "CANCOVER":
                this._$state.go('app.cancellation');
                break;
              default:
              break;
            }
          }
        },
        (err) => {
          this.errors = err.data.errors;
        }
      );
    } else {
      switch(manageType) {
        case "CANCOVER":
          //generate app number
          this._CommonAPI.generateAppNumber(this.urlList.appNumUrl).then(
            (res) => {
              this._$state.go('app.cancellation');
            },
            (err) => {
              this.errors = err.data.errors;
            }
          );
          break;
        default:
        break;
      }
    }
  } 

 
  // Helpful hints show/hide using this method
  openHH(hhText) {
    this._ngDialog.open({
      template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
      className: 'ngdialog-theme-plain',
      showClose: false,
      plain: true
    });
  }
  
  goTo(path){
	  this._$state.go(path);
  }
  
  // Window.open to open PDF in new tab

  openWindow(url) {
    this._$window.open(url);
  }
    
}

export default LandingCtrl;
