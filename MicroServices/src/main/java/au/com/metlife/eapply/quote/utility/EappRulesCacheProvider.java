package au.com.metlife.eapply.quote.utility;

import java.io.Serializable;
import java.util.HashMap;

import org.drools.KnowledgeBase;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Scope("singleton")
public class EappRulesCacheProvider implements Serializable{
	private static final Logger log = LoggerFactory.getLogger(EappRulesCacheProvider.class);
	static final String CLASS_NAME = "EappRulesCacheProvider";
	
	private java.util.HashMap ruleMap;
	SystemProperty sys;
	private String filePath;
    
    public String getFilePath() {
           return filePath;
    }

    public void setFilePath(String filePath) {
           this.filePath = filePath;
    }


	public java.util.HashMap getRuleMap() {
		return ruleMap;
	}
	
	public EappRulesCacheProvider(){
		log.info("Eapp rule cache provider start");
		try{
			sys=SystemProperty.getInstance();
			filePath=sys.getProperty("ruleFilePath");
			loadProperties();
		}catch(Exception e){
			log.error("Error in eapp cache provider : {}",e);
		}		
		log.info("Eapp rule cache provider finish");
	}
	/*public EappRulesCacheProvider(@Value("${spring.ruleFilePath}") String ruleFilePath){
		log.info("ruleFilePath>> {}",ruleFilePath);
        this.filePath = ruleFilePath;
		loadProperties();
	}*/
		
	public void loadProperties()	{
		log.info("Loading properties start");
		try{		
			ruleMap = getDroolsSet();
			if(ruleMap.isEmpty()){
				throw(new Exception("Drools not initialized properly. Terminating process."));
			}
		}catch(Exception e){
			log.error("Error in loading properties : {}",e);
		}
		log.info("Loading properties finish");
	}
	
	private HashMap getDroolsSet(){
		log.info("Get drools set start");
		HashMap obj_props = new HashMap();
		/*String RULE_FILE_LOC = "E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";*/
		try {
			obj_props.put(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID));
			log.info(">>>>Catching completed---RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID");
			obj_props.put(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_DEATH_UNT_CVR_AMT");
			obj_props.put(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING));
			log.info(">>>>Catching completed---RULE_NAME_INST_INDUSTRY_MAPPING");
			obj_props.put(QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_INST_DEATH_FXD_COST");
			obj_props.put(QuoteConstants.RULE_NAME_INST_NORM_ALL_CVR_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_NORM_ALL_CVR_COST));
			log.info(">>>>Catching completed---RULE_NAME_INST_NORM_ALL_CVR_COST");
			obj_props.put(QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_TPD_UNT_CVR_AMT");
			obj_props.put(QuoteConstants.RULE_NAME_INST_TPD_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_TPD_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_INST_TPD_FXD_COST");
			obj_props.put(QuoteConstants.RULE_NAME_CARE_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_CARE_INST_IP_FXD_COST));

			log.info(">>>>Catching completed---RULE_NAME_CARE_INST_IP_FXD_COST");		
			
			obj_props.put(QuoteConstants.RULE_NAME_METL_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_METL_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_METL_INST_IP_FXD_COST");
			
			obj_props.put(QuoteConstants.RULE_NAME_AEIS_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_AEIS_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_AEIS_INST_IP_FXD_COST");
			
			obj_props.put(QuoteConstants.RULE_NAME_HOST_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_HOST_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_HOST_INST_IP_FXD_COST");
			
			obj_props.put(QuoteConstants.RULE_NAME_SFPS_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_SFPS_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_SFPS_INST_IP_FXD_COST");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_IP_THRESHOLD_LMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_IP_THRESHOLD_LMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_IP_THRESHOLD_LMT");
			
			obj_props.put(QuoteConstants.RULE_NAME_FIRS_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_FIRS_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_FIRS_INST_IP_FXD_COST");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS));
			log.info(">>>>Catching completed---RULE_NAME_INST_DEFAULT_UNITS");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_1,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_1));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_1");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_2,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_2));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_2");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_3,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_3));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_3");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_MASTER,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_MASTER));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_MASTER");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_BAND_LMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_BAND_LMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_BAND_LMT");

			obj_props.put(QuoteConstants.RULE_NAME_INST_FUL_LMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_FUL_LMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_FUL_LMT");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_STAMP_DUTY,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_STAMP_DUTY));
			log.info(">>>>Catching completed---RULE_NAME_INST_STAMP_DUTY");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_INGD_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_INGD_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_INST_INGD_IP_FXD_COST");
			
			log.info(">>>>Catching completed---RULE_NAME_CARE_INST_IP_FXD_COST");
			
			
			obj_props.put(QuoteConstants.RULE_NAME_METL_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_METL_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_METL_INST_IP_FXD_COST");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_1,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_1));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_1");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_2,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_2));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_2");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_3,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_3));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_3");
			
			obj_props.put(QuoteConstants.RULE_NAME_POST_PROCESSING_MASTER,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_POST_PROCESSING_MASTER));
			log.info(">>>>Catching completed---RULE_NAME_POST_PROCESSING_MASTER");

			obj_props.put(QuoteConstants.RULE_NAME_INST_BAND_LMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_BAND_LMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_BAND_LMT");

			obj_props.put(QuoteConstants.RULE_NAME_INST_FUL_LMT,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_FUL_LMT));
			log.info(">>>>Catching completed---RULE_NAME_INST_FUL_LMT");
			
			//Added for Vicsuper - Purnachandra K
			obj_props.put(QuoteConstants.RULE_NAME_VICS_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_VICS_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_VICS_INST_IP_FXD_COST");
			
			//MTAA Changes
			obj_props.put(QuoteConstants.RULE_NAME_MTAA_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_MTAA_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_MTAA_INST_IP_FXD_COST");
			
			//AAL change
			obj_props.put(QuoteConstants.RULE_NAME_SFPS_AAL_STATUS_FIXED,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_SFPS_AAL_STATUS_FIXED));
			log.info(">>>>Catching completed---RULE_NAME_SFPS_AAL_STATUS_FIXED");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_STAMP_DUTY,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_STAMP_DUTY));
			log.info(">>>>Catching completed---RULE_NAME_INST_STAMP_DUTY");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_INGD_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_INGD_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_INST_INGD_IP_FXD_COST");
			
			//GUILD Changes
			obj_props.put(QuoteConstants.RULE_NAME_GUIL_INST_IP_FXD_COST,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_GUIL_INST_IP_FXD_COST));
			log.info(">>>>Catching completed---RULE_NAME_GUIL_INST_IP_FXD_COST");
			
			//Insurance Calculator Changes
			obj_props.put(QuoteConstants.RULE_NAME_INST_CALCULATOR_TAX,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_CALCULATOR_TAX));
			log.info(">>>>Catching completed---RULE_NAME_INST_CALCULATOR_TAX");
			
			obj_props.put(QuoteConstants.RULE_NAME_INST_CALCULATOR_CONFIG,DroolsHelper.getRuleSession(getFilePath(),QuoteConstants.RULE_NAME_INST_CALCULATOR_CONFIG));
			log.info(">>>>Catching completed---RULE_NAME_INST_CALCULATOR_CONFIG");
			
		} catch (Exception e) {
			log.error("Error in get drools set: {}",e);
		}
		log.info("Get drools set finish");
		return obj_props;
	}

	
	public KnowledgeBase getDroolsFromCache(String arg_property)	{	
		return (KnowledgeBase)ruleMap.get(arg_property);
	}
	

	

}
