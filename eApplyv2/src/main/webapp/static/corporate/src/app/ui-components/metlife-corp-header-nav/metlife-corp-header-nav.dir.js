(function (angular) {
  'use strict';

  angular
    .module('Metlife.corp.uicomponents')
    .directive('metlifeCorpHeaderNav', metlifeCorpHeaderNav);

  function metlifeCorpHeaderNav(APP_CONSTANTS, $document) {
    var directive = {
      restrict: 'EA',
      templateUrl: APP_CONSTANTS.path.uiComponents + '/metlife-corp-header-nav/metlife-corp-header-nav.tmpl.html',
      link: link,
      controller: headerNavCtrl,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    function link(scope, element, attrs, vm) {
      element.find('.nav-item').bind('click', function (event) {
        element.find('.navbar-collapse').collapse('hide');
        event.stopPropagation();
      });
      $document.bind('click', function () {
        element.find('.collapse').collapse('hide');
      });
    }
  }

  headerNavCtrl.$inject = ['$scope', 'APP_CONSTANTS'];

  function headerNavCtrl($scope, APP_CONSTANTS) {
    var vm = this;
    vm.HeaderNav = {
      logoImgPath: APP_CONSTANTS.path.images + 'logo_header.png',
      logoUrl: APP_CONSTANTS.navUrls.logoUrl
    };
  }

})(angular);