/**
 * 
 */
package pages.metlife.NEW;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;
import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.NSFS_UpdateYourOccupationPageObjects;

/**
 * @author Hemnath
 * 
 */
public class NSFS_UpdateYourOccupationPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public NSFS_UpdateYourOccupationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public NSFS_UpdateYourOccupationPage enterNSFSupdateoccupation() {
		updateoccupation();
		HealthandLifestyle();
		Declaration();
		return new NSFS_UpdateYourOccupationPage(scriptHelper);
	} 
	
	private void updateoccupation() {
		
		String EmailId = dataTable.getData("NSFS_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("NSFS_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("NSFS_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("NSFS_UpdateOccupation", "TimeofContact");
		
		String Citizen = dataTable.getData("NSFS_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("NSFS_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("NSFS_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("NSFS_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("NSFS_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("NSFS_UpdateOccupation", "TertiaryQual");
		String HazardousEnv = dataTable.getData("NSFS_UpdateOccupation", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("NSFS_UpdateOccupation", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("NSFS_UpdateOccupation", "AnnualSalary");
	
		try{
			sleep(1000);
			driver.manage().window().maximize();
			sleep(500);
			
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.UpdateOccupationLink), "Click", "Update Occupation Link");
		
			//*****Agree to Duty of disclosure*******\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "selected the Checkbox", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Privacy Statement", "selected the Checkbox", Status.PASS);
			
			//*****Enter the Email Id*****\\										
			
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.EmailId), "SET", "Email Id",EmailId);							
										
										
			//*****Click on the Contact Number Type****\\							
			if(!TypeofContact.equalsIgnoreCase("Mobile")){							
										
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ContactTypeClick), "Click", "Preferred Type of Contact: "+TypeofContact);							
										
			//*****Choose the Type of Contact*****\							
										
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();							
										
			}							
										
			//****Enter the Contact Number****\\							
			waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));							
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);							
										
										
										
			//***Select the Preferred time of Contact*****\\							
			if(TimeofContact.equalsIgnoreCase("Morning")){							
										
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");							
			}							
			else{							
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.AfterNoonContact), "Click", "Preferred Contact Time is AfterNoon");							
			}							
										
			//****Contact Details-Continue Button*****\\							
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");							
			sleep(3200);							
										
										
			//***************************************\\							
			//**********OCCUPATION SECTION***********\\							
			//****************************************\\							
								
										
				//*****Select if Resident of Australia****\\						
				if(Citizen.equalsIgnoreCase("Yes")){						
										
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.CitizenYes), "Click", "Australian Citizen-Yes");					
										
					}					
				else{						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.CitizenNo), "Click", "Australian Citizen-No");					
										
					}					
										
				//*****Click on Industry********\\						
										
				tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ClickIndustry), "Click", "Industry");						
				sleep(800);						
										
				//*****Choose the Industry Type*****\\						
				driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();						
				sleep(1000);						
										
				//*****Click On  Occupation*****\\						
										
	            tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ClickOccupation), "Click", "Occupation");									
	            sleep(800);									
	            									
	            //****Choose the Occupation Type*****\\									
										
				driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();						
				sleep(1000);						
										
										
				//*****Other Occupation *****\\						
				if(OccupationType.equalsIgnoreCase("Other")){						
										
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);					
					sleep(1000);					
				}						
										
										
				//*****Extra Questions*****\\						
										
				if(driver.getPageSource().contains("Do you work wholly within an office environment?")){						
										
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\							
				if(OfficeEnvironment.equalsIgnoreCase("Yes")){						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");					
										
				}						
				else{						
										
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");					
										
				}						
										
			//*****Do You Hold a Tertiary Qualification******\\							
										
				if(TertiaryQual.equalsIgnoreCase("Yes")){						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");					
				}						
				else{						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");					
				}						
			}							
										
										
										
			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
										
										
										
			//******Do you work in hazardous environment*****\\							
				if(HazardousEnv.equalsIgnoreCase("Yes")){						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");					
				}						
				else{						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");					
				}						
										
			//******Do you spend 20% time working outside office*******\\							
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");					
				}						
				else{						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");					
				}						
			}							
				sleep(1000);						
				//*****Extra Questions*****\\						
				
				if(driver.getPageSource().contains("Do you work wholly within an office environment?")){						
										
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\							
				if(OfficeEnvironment.equalsIgnoreCase("Yes")){						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");					
										
				}						
				else{						
										
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");					
										
				}						
										
			//*****Do You Hold a Tertiary Qualification******\\							
										
				if(TertiaryQual.equalsIgnoreCase("Yes")){						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");					
				}						
				else{						
					tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");					
				}						
			}							
										
														
			//*****What is your annual Salary******							
										
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);							
			sleep(500);							
										
			//***Load the Salary Amount****\\							
			waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.LoadSalaryLabel).click();							
			sleep(500);							
										
										
		//*****Continue button after entering occupation details*******\\								
										
										
			tryAction(waitForClickableElement(NSFS_UpdateYourOccupationPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");							
			sleep(1500);							
							
			
		
							

			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void HealthandLifestyle() {
		String ManagementRole = dataTable.getData("NSFS_UpdateOccupation", "ManagementRole");
		String WhiteCollarTask = dataTable.getData("NSFS_UpdateOccupation", "WhiteCollarTask");
	try{
		WebDriverWait wait=new WebDriverWait(driver,18);
		if(driver.getPageSource().contains("Eligibility Check")){
		if(ManagementRole.equalsIgnoreCase("Yes")){
			
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-0:0']"))).click();
		report.updateTestLog("Health and Lifestyle Question", "Management Role-Yes", Status.PASS);
		sleep(500);
		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-0:0']"))).click();
			report.updateTestLog("Health and Lifestyle Question", "Management Role-No", Status.PASS);
			sleep(500);
		}
		
		if(WhiteCollarTask.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-1:0']"))).click();
			report.updateTestLog("Health and Lifestyle Question", "White Collar Job-Yes", Status.PASS);
			sleep(500);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-1:0']"))).click();
				report.updateTestLog("Health and Lifestyle Question", "White Collar Job-No", Status.PASS);
				sleep(500);
			}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='applyBtnId']/span"))).click();
		report.updateTestLog("Submit", "Click on Submit button", Status.PASS);
		
		}
		
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}
	
	private void Declaration() {
		try{

			//*****General Consent******\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);

			//******Submit******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
			report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

			//*******Feedback Pop-up******\\

				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1000);

			String Appstatus=driver.findElement(By.xpath("//h4[@class='panel-title']")).getText();
			report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	}

