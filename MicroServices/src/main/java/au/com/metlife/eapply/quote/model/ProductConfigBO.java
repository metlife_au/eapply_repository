/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 404141
 *
 */
public class ProductConfigBO implements Serializable{
	
	private String productCode;
	
	private String memberType;
	
	private String manageType;
	
	private String defaultPremFreq;
	
	private int deathMinAge;
	
	private int deathMaxAge;
	
	private int tpdMinAge;
	
	private int tpdMaxAge;
	
	private int ipMinAge;
	
	private int ipMaxAge;
	
	private BigDecimal deathMinAmount;
	
	private BigDecimal deathMaxAmount;
	
	private BigDecimal tpdMinAmount;
	
	private BigDecimal tpdMaxAmount;
	
	private BigDecimal ipMinAmount;
	
	private BigDecimal ipMaxAmount;
	
	private BigDecimal deathTpdTransferMaxAmt;
	
	private BigDecimal deathTpdTransferMaxUnits;
	
	private BigDecimal ipTransferMaxAmt;
	
	private String annualSalForUpgradeVal;
	
	private BigDecimal annualSalMaxLimit;
	
	private double ipUnitCostMulitiplier;

	public BigDecimal getAnnualSalMaxLimit() {
		return annualSalMaxLimit;
	}

	public void setAnnualSalMaxLimit(BigDecimal annualSalMaxLimit) {
		this.annualSalMaxLimit = annualSalMaxLimit;
	}

	public String getAnnualSalForUpgradeVal() {
		return annualSalForUpgradeVal;
	}

	public void setAnnualSalForUpgradeVal(String annualSalForUpgradeVal) {
		this.annualSalForUpgradeVal = annualSalForUpgradeVal;
	}

	public BigDecimal getDeathTpdTransferMaxAmt() {
		return deathTpdTransferMaxAmt;
	}

	public void setDeathTpdTransferMaxAmt(BigDecimal deathTpdTransferMaxAmt) {
		this.deathTpdTransferMaxAmt = deathTpdTransferMaxAmt;
	}

	public BigDecimal getDeathTpdTransferMaxUnits() {
		return deathTpdTransferMaxUnits;
	}

	public void setDeathTpdTransferMaxUnits(BigDecimal deathTpdTransferMaxUnits) {
		this.deathTpdTransferMaxUnits = deathTpdTransferMaxUnits;
	}

	public BigDecimal getIpTransferMaxAmt() {
		return ipTransferMaxAmt;
	}

	public void setIpTransferMaxAmt(BigDecimal ipTransferMaxAmt) {
		this.ipTransferMaxAmt = ipTransferMaxAmt;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public String getDefaultPremFreq() {
		return defaultPremFreq;
	}

	public void setDefaultPremFreq(String defaultPremFreq) {
		this.defaultPremFreq = defaultPremFreq;
	}

	public int getDeathMinAge() {
		return deathMinAge;
	}

	public void setDeathMinAge(int deathMinAge) {
		this.deathMinAge = deathMinAge;
	}

	public int getDeathMaxAge() {
		return deathMaxAge;
	}

	public void setDeathMaxAge(int deathMaxAge) {
		this.deathMaxAge = deathMaxAge;
	}

	public int getTpdMinAge() {
		return tpdMinAge;
	}

	public void setTpdMinAge(int tpdMinAge) {
		this.tpdMinAge = tpdMinAge;
	}

	public int getTpdMaxAge() {
		return tpdMaxAge;
	}

	public void setTpdMaxAge(int tpdMaxAge) {
		this.tpdMaxAge = tpdMaxAge;
	}

	public int getIpMinAge() {
		return ipMinAge;
	}

	public void setIpMinAge(int ipMinAge) {
		this.ipMinAge = ipMinAge;
	}

	public int getIpMaxAge() {
		return ipMaxAge;
	}

	public void setIpMaxAge(int ipMaxAge) {
		this.ipMaxAge = ipMaxAge;
	}

	public BigDecimal getDeathMinAmount() {
		return deathMinAmount;
	}

	public void setDeathMinAmount(BigDecimal deathMinAmount) {
		this.deathMinAmount = deathMinAmount;
	}

	public BigDecimal getDeathMaxAmount() {
		return deathMaxAmount;
	}

	public void setDeathMaxAmount(BigDecimal deathMaxAmount) {
		this.deathMaxAmount = deathMaxAmount;
	}

	public BigDecimal getTpdMinAmount() {
		return tpdMinAmount;
	}

	public void setTpdMinAmount(BigDecimal tpdMinAmount) {
		this.tpdMinAmount = tpdMinAmount;
	}

	public BigDecimal getTpdMaxAmount() {
		return tpdMaxAmount;
	}

	public void setTpdMaxAmount(BigDecimal tpdMaxAmount) {
		this.tpdMaxAmount = tpdMaxAmount;
	}

	public BigDecimal getIpMinAmount() {
		return ipMinAmount;
	}

	public void setIpMinAmount(BigDecimal ipMinAmount) {
		this.ipMinAmount = ipMinAmount;
	}

	public BigDecimal getIpMaxAmount() {
		return ipMaxAmount;
	}

	public void setIpMaxAmount(BigDecimal ipMaxAmount) {
		this.ipMaxAmount = ipMaxAmount;
	}

	public double getIpUnitCostMulitiplier() {
		return ipUnitCostMulitiplier;
	}

	public void setIpUnitCostMulitiplier(double ipUnitCostMulitiplier) {
		this.ipUnitCostMulitiplier = ipUnitCostMulitiplier;
	}


	

}
