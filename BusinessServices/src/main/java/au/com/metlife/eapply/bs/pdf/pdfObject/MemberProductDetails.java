package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;


public class MemberProductDetails implements Serializable{
	
	private String productType = null;
	private String productName = null;
	private String productAmount = null;
	private String productPremiumCost = null;
	private String productDecision = null;
	private String loadings = null;
	private String exclusions = null;
	private String existingCover = null;
	private String newExistingCover = null;
    /*SRKR:14/07/2016: New field to capture existing unitised cover amount based on new occupation category selected in Update work rating flow
     * Applicable for NSFS and MTAA in UWCOVER flow.
     * */
	private String waitingPeriod = null;
	private String benefitPeriod = null;
	private String occupationRating = null;
	private String prductDecReason = null;	
	private String productCategory = null;
	private String aalLevelStatus = null;
	private String loadingValue = null;
	private String fulCoverAmount = null;
	private String aalCoverAmount = null;
	private Boolean aalAppliedDuringDCL = false;
	/*private String transferCover = null;*/
	private String transferCoverAmnt = null;
	private String exWaitingPeriod = null;
	private String exBenefitPeriod = null;
	
	public String getExBenefitPeriod() {
		return exBenefitPeriod;
	}
	public void setExBenefitPeriod(String exBenefitPeriod) {
		this.exBenefitPeriod = exBenefitPeriod;
	}
	public String getExWaitingPeriod() {
		return exWaitingPeriod;
	}
	public void setExWaitingPeriod(String exWaitingPeriod) {
		this.exWaitingPeriod = exWaitingPeriod;
	}
	public String getAalLevelStatus() {
		return aalLevelStatus;
	}
	public void setAalLevelStatus(String aalLevelStatus) {
		this.aalLevelStatus = aalLevelStatus;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public String getPrductDecReason() {
		return prductDecReason;
	}
	public void setPrductDecReason(String prductDecReason) {
		this.prductDecReason = prductDecReason;
	}
	public String getOccupationRating() {
		return occupationRating;
	}
	public void setOccupationRating(String occupationRating) {
		this.occupationRating = occupationRating;
	}
	public String getBenefitPeriod() {
		return benefitPeriod;
	}
	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}
	public String getExistingCover() {
		return existingCover;
	}
	public void setExistingCover(String existingCover) {
		this.existingCover = existingCover;
	}
	public String getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	public String getExclusions() {
		return exclusions;
	}
	public void setExclusions(String exclusions) {
		this.exclusions = exclusions;
	}
	public String getLoadings() {
		return loadings;
	}
	public void setLoadings(String loadings) {
		this.loadings = loadings;
	}
	public String getProductAmount() {
		return productAmount;
	}
	public void setProductAmount(String productAmount) {
		this.productAmount = productAmount;
	}
	public String getProductDecision() {
		return productDecision;
	}
	public void setProductDecision(String productDecision) {
		this.productDecision = productDecision;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductPremiumCost() {
		return productPremiumCost;
	}
	public void setProductPremiumCost(String productPremiumCost) {
		this.productPremiumCost = productPremiumCost;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getLoadingValue() {
		return loadingValue;
	}
	public void setLoadingValue(String loadingValue) {
		this.loadingValue = loadingValue;
	}
	public String getFulCoverAmount() {
		return fulCoverAmount;
	}
	public void setFulCoverAmount(String fulCoverAmount) {
		this.fulCoverAmount = fulCoverAmount;
	}
	public String getAalCoverAmount() {
		return aalCoverAmount;
	}
	public void setAalCoverAmount(String aalCoverAmount) {
		this.aalCoverAmount = aalCoverAmount;
	}
	public Boolean getAalAppliedDuringDCL() {
		return aalAppliedDuringDCL;
	}
	public void setAalAppliedDuringDCL(Boolean aalAppliedDuringDCL) {
		this.aalAppliedDuringDCL = aalAppliedDuringDCL;
	}
	/*public String getTransferCover() {
		return transferCover;
	}
	public void setTransferCover(String transferCover) {
		this.transferCover = transferCover;
	}*/
	public String getTransferCoverAmnt() {
		return transferCoverAmnt;
	}
	public void setTransferCoverAmnt(String transferCoverAmnt) {
		this.transferCoverAmnt = transferCoverAmnt;
	}
	public String getNewExistingCover() {
		return newExistingCover;
	}
	public void setNewExistingCover(String newExistingCover) {
		this.newExistingCover = newExistingCover;
	}
	
	

}
