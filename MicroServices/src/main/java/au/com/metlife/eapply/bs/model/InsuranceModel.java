package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

import java.util.List;



public class InsuranceModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private ApplicantInfoIns applicant;
	private String fundId;
	private InsuranceRuleInfo insuranceRuleDetails;

	

	
	
	public InsuranceRuleInfo getInsuranceRuleDetails() {
		return insuranceRuleDetails;
	}

	public void setInsuranceRuleDetails(InsuranceRuleInfo insuranceRuleDetails) {
		this.insuranceRuleDetails = insuranceRuleDetails;
	}

	public String getFundId() {
		return fundId;
	}

	public void setFundId(String fundId) {
		this.fundId = fundId;
	}

	public ApplicantInfoIns getApplicant() {
		return applicant;
	}

	public void setApplicant(ApplicantInfoIns applicant) {
		this.applicant = applicant;
	}

}
