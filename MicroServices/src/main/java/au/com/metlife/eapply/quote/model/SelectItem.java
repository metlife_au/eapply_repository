/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;

/**
 * @author 199306
 *
 */
public class SelectItem implements Serializable {

	private String key;
	
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public SelectItem(){
		
	}
	
	public SelectItem(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
