package au.com.metlife.eapply.bs.email;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.model.Document;

/* -------------------------------------------------------------------------------------------------    
 * Description:   This class defines getter and setters for MetLifeEmailService 
 * -------------------------------------------------------------------------------------------------
 * Copyright @ 2009 MetLife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :    khirod.panda
 * Created:     May 01,2009
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * Date(MM/DD/YYYY)   july 03,2009              Author   anand.kishore                                 Description     code refactoring    
 *          
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
public class MetLifeEmailVO {
	private static final Logger log = LoggerFactory.getLogger(MetLifeEmailVO.class);
	/*toRecipents*/
	private ArrayList toRecipents;
	/*fromSender*/
	private String fromSender;
	/*subject*/
	private String subject;
	/*emailTemplateId*/
	private String emailTemplateId;
	/*title*/
	private String title= null;
	/*firstName*/
	private String firstName= null;
	/*lasttName*/
	private String lastName= null;
	/*applicationId*/
	private String applicationId= null;
	/*applicationtype*/
	private String applicationtype= null;
	/*fundCode*/
	private String fundCode= null;
	
	private String ccReciepent = null;
	
	private String bccReciepent = null;
	/*emailDataElementMap*/
	private HashMap emailDataElementMap;
	
	private Boolean isAgentEmailReq = Boolean.FALSE;
	
	private List attachments;
	
	private boolean htmlEmails=false;
	
	private List<Document> documentList=null;
	
	private String productNumber = null;

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public boolean isHtmlEmails() {
		return htmlEmails;
	}

	public void setHtmlEmails(boolean htmlEmails) {
		this.htmlEmails = htmlEmails;
	}

	public MetLifeEmailVO() {
		super();
		
	}

	/**
	 * @return Returns the emailTemplateId.
	 */
	public String getEmailTemplateId() {
		return emailTemplateId;
	}

	/**
	 * @param emailTemplateId
	 *            The emailTemplateId to set.
	 */
	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}

	/**
	 * @return Returns the fromSender.
	 * @throws AddressException
	 * @throws UnsupportedEncodingException 
	 */
	public InternetAddress getFromSender() throws AddressException, UnsupportedEncodingException {
		log.info("Getting from sender start");
		InternetAddress addressFrom = null;
		log.info("fromSender>> {}",fromSender);
		if("coleslifeinsurance@metlife.com".equalsIgnoreCase(fromSender)){
			addressFrom = new InternetAddress(fromSender,"Coles Life Insurance");
		}else{
			addressFrom = new InternetAddress(fromSender);
		}
		log.info("Getting from sender finish");
		return addressFrom;
	}

	/**
	 * @param fromSender
	 *            The fromSender to set.
	 */
	public void setFromSender(String fromSender) {
		this.fromSender = fromSender;
	}

	/**
	 * @return Returns the subject.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            The subject to set.
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return Returns the toRecipents.
	 * @throws Exception
	 */
	public InternetAddress[] getToRecipents() {
		log.info("Get to recipents start");
		InternetAddress[] addressTo = new InternetAddress[toRecipents.size()];
		for (int i = 0; i < toRecipents.size(); i++) {
			try {
				addressTo[i] = new InternetAddress((String) toRecipents.get(i));
			} catch (AddressException e) {
				log.error("Error in address: {}",e);
			}
		}
		log.info("Get to recipents finish");
		return addressTo;
	}

	/**
	 * @param toRecipents
	 *            The toRecipents to set.
	 */
	public void setToRecipents(ArrayList toRecipents) {
		this.toRecipents = toRecipents;
	}

	/**
	 * @return Returns the placeHolders.
	 */
	public HashMap getEmailDataElementMap() {
		return emailDataElementMap;
	}

	/**
	 * @param placeHolders
	 *            The placeHolders to set.
	 */
	public void setEmailDataElementMap(HashMap placeHolders) {
		this.emailDataElementMap = placeHolders;
	}
	/**
	 * @return Returns the applicationtype.
	 */
	public String getApplicationtype() {
		return applicationtype;
	}
	/**
	 * @param applicationtype
	 *            The applicationtype to set.
	 */
	public void setApplicationtype(String applicationtype) {
		this.applicationtype = applicationtype;
	}
	/**
	 * @return Returns the applicationId.
	 */
	public String getApplicationId() {
		return applicationId;
	}
	/**
	 * @param applicationId
	 *            The applicationId to set.
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public List getAttachments() {
		return attachments;
	}

	public void setAttachments(List attachments) {
		this.attachments = attachments;
	}

	public String getCcReciepent() {
		return ccReciepent;
	}

	public void setCcReciepent(String ccReciepent) {
		this.ccReciepent = ccReciepent;
	}

	public Boolean getIsAgentEmailReq() {
		return isAgentEmailReq;
	}

	public void setIsAgentEmailReq(Boolean isAgentEmailReq) {
		this.isAgentEmailReq = isAgentEmailReq;
	}

	public List<Document> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Document> documentList) {
		this.documentList = documentList;
	}

	public String getBccReciepent() {
		return bccReciepent;
	}

	public void setBccReciepent(String bccReciepent) {
		this.bccReciepent = bccReciepent;
	}

}
