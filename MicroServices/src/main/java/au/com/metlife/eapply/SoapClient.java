package au.com.metlife.eapply;

import javax.xml.bind.JAXBElement;
import javax.xml.soap.MessageFactory;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.xml.transform.StringResult;

import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetails;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetailsResponse;

public class SoapClient extends WebServiceGatewaySupport{
	
		
	public UpdateInsuranceDetailsResponse callStateWideService(UpdateInsuranceDetails request) throws Exception {
		JAXBElement<UpdateInsuranceDetailsResponse>  response = null;
		
		
	        SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory(
	                MessageFactory.newInstance());
	        messageFactory.afterPropertiesSet();

	        WebServiceTemplate webServiceTemplate = new WebServiceTemplate(
	                messageFactory);
	        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

	        marshaller.setContextPath("au.com.finsyn.acuritywebservices.services");
	        marshaller.afterPropertiesSet();
	        StringResult result = new StringResult();
	        marshaller.marshal(request, result);
	       System.out.println(result.toString());
	        webServiceTemplate.setMarshaller(marshaller);
	        webServiceTemplate.setUnmarshaller(marshaller);
	        webServiceTemplate.afterPropertiesSet();
	       
	        response = (JAXBElement<UpdateInsuranceDetailsResponse>)webServiceTemplate
	                            .marshalSendAndReceive( "https://webserviceuat.statewide.com.au/StatewideSWS_UATWebServices/IntegratedInsurance", request, new SoapActionCallback("http://services.acuritywebservices.finsyn.com.au/IntegratedInsurance/updateInsuranceDetailsResponse"));
	         System.out.println(response.getValue().getReturn().getRecordID().get(0));
	       
		return response.getValue();
	}
	
}
