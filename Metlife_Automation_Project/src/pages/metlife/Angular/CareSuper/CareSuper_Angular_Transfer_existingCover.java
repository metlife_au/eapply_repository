package pages.metlife.Angular.CareSuper;

/**
 * Author
 * Yuvaraj
 */

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class CareSuper_Angular_Transfer_existingCover extends MasterPage {
	WebDriverUtil driverUtil = null;

	public CareSuper_Angular_Transfer_existingCover TransferCover_CareSuper() {
		FirstSuperPersonaldetails();
		healthandlifestyledetails();
		confirmation();

		return new CareSuper_Angular_Transfer_existingCover(scriptHelper);
	}

	public CareSuper_Angular_Transfer_existingCover TransferCover_CareSuper_Negative() {
		TransferCoverFirstSuper_Negative();

		return new CareSuper_Angular_Transfer_existingCover(scriptHelper);
	}

	public CareSuper_Angular_Transfer_existingCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

public void FirstSuperPersonaldetails() {

		String EmailId = dataTable.getData("CareSuper_TransferyourCover", "EmailId");
		String TypeofContact = dataTable.getData("CareSuper_TransferyourCover", "TypeofContact");
		String ContactNumber = dataTable.getData("CareSuper_TransferyourCover", "ContactNumber");
		String TimeofContact = dataTable.getData("CareSuper_TransferyourCover", "TimeofContact");
		String Gender = dataTable.getData("CareSuper_TransferyourCover", "Gender");
		String FifteenHoursWork = dataTable.getData("CareSuper_TransferyourCover", "FifteenHoursWork");

		String IndustryType = dataTable.getData("CareSuper_TransferyourCover", "IndustryType");
		String OccupationType = dataTable.getData("CareSuper_TransferyourCover", "OccupationType");
		String OtherOccupation = dataTable.getData("CareSuper_TransferyourCover", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("CareSuper_TransferyourCover", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("CareSuper_TransferyourCover", "TertiaryQual");
	    String Managementrole = dataTable.getData("CareSuper_TransferyourCover", "Managementrole");

		String AnnualSalary = dataTable.getData("CareSuper_TransferyourCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("CareSuper_TransferyourCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("CareSuper_TransferyourCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("CareSuper_TransferyourCover", "FormerFundNo");
		String DocumentaryEvidence = dataTable.getData("CareSuper_TransferyourCover", "DocumentaryEvidence");
		String AttachmentSizeLimitTest	= dataTable.getData("CareSuper_TransferyourCover", "AttachmentSizeLimitTest");
		String AttachmentTestPath = dataTable.getData("CareSuper_TransferyourCover", "AttachmentTestPath");
		String OverSizeMessage = dataTable.getData("CareSuper_TransferyourCover", "OverSizeMessage");
		String AttachPath = dataTable.getData("CareSuper_TransferyourCover", "AttachPath");
		String CostType = dataTable.getData("CareSuper_TransferyourCover", "CostType");
		String DeathYN = dataTable.getData("CareSuper_TransferyourCover", "Death");
		String TPDYN = dataTable.getData("CareSuper_TransferyourCover", "TPD");
		String IPYN = dataTable.getData("CareSuper_TransferyourCover", "IP");
		String DeathAmount = dataTable.getData("CareSuper_TransferyourCover", "DeathAmount");
		String TPDAmount = dataTable.getData("CareSuper_TransferyourCover", "TPDAmount");
		String IPAmount = dataTable.getData("CareSuper_TransferyourCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("CareSuper_TransferyourCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("CareSuper_TransferyourCover", "BenefitPeriod");
		String Equivalentvalueinunits=dataTable.getData("CareSuper_TransferyourCover", "Equivalentvalueinunits");
		String DutyOfDisclosure = dataTable.getData("CareSuper_TransferyourCover", "DutyOfDisclosure");
		String PrivacyStatement = dataTable.getData("CareSuper_TransferyourCover", "PrivacyStatement");
		//String ExpectedDeathcover=dataTable.getData("CareSuper_TransferyourCover", "ExpectedDeathcover");
		//String ExpectedTPDcover=dataTable.getData("CareSuper_TransferyourCover", "ExpectedTPDcover");
		//String ExpectedIPcover=dataTable.getData("CareSuper_TransferyourCover", "ExpectedIPcover");
		String fixedcover = dataTable.getData("CareSuper_TransferyourCover", "fixedcover");


		try{

			JavascriptExecutor a = (JavascriptExecutor)driver;
			a.executeScript("window.scrollBy(0,750)", "");
			Thread.sleep(2000);
			//****Click on Transfer Your Cover Link*****\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Transfer your existing cover')]"))).click();
			report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);



			//*****Agree to Duty of disclosure*******\\
			 JavascriptExecutor b = (JavascriptExecutor)driver;
				b.executeScript("window.scrollBy(0,700)", "");
				Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
			sleep(400);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);

			sleep(200);

            //*****Agree to Privacy Statement*******\\
			JavascriptExecutor c = (JavascriptExecutor)driver;
			c.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(2000);


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyCkBoxLblId']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);
			sleep(250);

			sleep(200);

            //*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


			//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

				//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();

					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}



				//****Contact Details-Continue Button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);

				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\
				JavascriptExecutor d = (JavascriptExecutor)driver;
				d.executeScript("window.scrollBy(0,500)", "");
				Thread.sleep(2000);
					//*****Select the 15 Hours Question*******\\

				if(FifteenHoursWork.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}


				//*****Industry********\\
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(800);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
		//		sleep(500);

				//*****Occupation*****\\

				Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(500);

				JavascriptExecutor e = (JavascriptExecutor)driver;
				e.executeScript("window.scrollBy(0,500)", "");
				Thread.sleep(2000);
				//*****Other Occupation *****\\

				if(OccupationType.equalsIgnoreCase("Other")){

					wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);
				}


//**Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar 'white collar' tasks which do not
	//**		involve manual work and are undertaken entirely within an office environment (excluding travel time from one office environment to another)?**//


			if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

			}
		else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			}

			//**Do you hold a tertiary qualification or are you a member of a professional institute or registered as a practicing member of your profession by a government body?**//

			if(TertiaryQual.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

			}
		else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			}

			//**Are you in a management role?**//

			if(Managementrole.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			}


			//*****What is your annual Salary******


			    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
			    sleep(500);
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

			    JavascriptExecutor f = (JavascriptExecutor)driver;
				f.executeScript("window.scrollBy(0,500)", "");
				Thread.sleep(2000);

			//*****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


			//******************************************\\
			//**********PREVIOUS COVER SECTION**********\\
			//*******************************************\\

			//*****What is the name of your previous fund or insurer?*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
			sleep(250);
			report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

            //*****Please enter your fund member or insurance policy number*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
			sleep(250);
			report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

            //*****Please enter your former fund SPIN number (if known)*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
			sleep(250);
			report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

			 JavascriptExecutor g = (JavascriptExecutor)driver;
				g.executeScript("window.scrollBy(0,500)", "");
				Thread.sleep(2000);
			//*****Do You want to Attach documentary evidence****\\

			if(DocumentaryEvidence.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you wish to include documentary evidence of your previous insurance cover that you wish to transfer? ']/../following-sibling::div/div/div[1]/label/span"))).click();
				sleep(500);
				report.updateTestLog("Documentary Evidence", "Yes is selected", Status.PASS);
				 JavascriptExecutor h = (JavascriptExecutor)driver;
					h.executeScript("window.scrollBy(0,500)", "");
					Thread.sleep(2000);
				if(AttachmentSizeLimitTest.equalsIgnoreCase("Yes")){
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");
					if (StrBrowseName.equalsIgnoreCase("Firefox")){
						 StringSelection sel = new StringSelection(AttachmentTestPath);

						   // Copy to clipboard
						 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);

						 //Click
						 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();

						 // Create object of Robot class
						 Robot robot = new Robot();
						 Thread.sleep(1000);

						  // Press Enter
						 robot.keyPress(KeyEvent.VK_ENTER);

						// Release Enter
						 robot.keyRelease(KeyEvent.VK_ENTER);

						  // Press CTRL+V
						 robot.keyPress(KeyEvent.VK_CONTROL);
						 robot.keyPress(KeyEvent.VK_V);

						// Release CTRL+V
						 robot.keyRelease(KeyEvent.VK_CONTROL);
						 robot.keyRelease(KeyEvent.VK_V);
						 Thread.sleep(1000);

						        // Press Enter
						 robot.keyPress(KeyEvent.VK_ENTER);
						 robot.keyRelease(KeyEvent.VK_ENTER);

						sleep(500);
					}
					else{

						 JavascriptExecutor r = (JavascriptExecutor)driver;
							r.executeScript("window.scrollBy(0,500)", "");
							Thread.sleep(2000);
						driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachmentTestPath);

					}

					//*****Let the attachment load*****\\
		//			driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
		//			sleep(1000);

					//*****Add the Attachment*****\\

					WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
					sleep(1500);

					String OversizeMessage=driver.findElement(By.xpath("//label[text()='File size should not be more than 10MB']")).getText();
					sleep(250);
					String SizeLimit=OversizeMessage.trim();

					if(OverSizeMessage.equalsIgnoreCase(SizeLimit)){
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is Displayed", Status.PASS);
					}
					else{
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is not Displayed", Status.FAIL);
					}


				}

				//****Attach the file******
				sleep(500);
				properties = Settings.getInstance();
				String StrBrowseName=properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Firefox")){
					StringSelection sel = new StringSelection(AttachPath);

					   // Copy to clipboard
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);

					 //Click
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();

					 // Create object of Robot class
					 Robot robot = new Robot();
					 Thread.sleep(1000);

					  // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);

					// Release Enter
					 robot.keyRelease(KeyEvent.VK_ENTER);

					  // Press CTRL+V
					 robot.keyPress(KeyEvent.VK_CONTROL);
					 robot.keyPress(KeyEvent.VK_V);

					// Release CTRL+V
					 robot.keyRelease(KeyEvent.VK_CONTROL);
					 robot.keyRelease(KeyEvent.VK_V);
					 Thread.sleep(1000);

					        // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);
					 robot.keyRelease(KeyEvent.VK_ENTER);

					sleep(500);
				}
				else{
					driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);

				}



				sleep(1500);

				//*****Let the attachment load*****\\
	//			driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
		//		sleep(1000);

				//*****Add the Attachment*****\\

				WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1500);
				report.updateTestLog("Evidence Attachment", "File is successfully attached", Status.PASS);

			}
			else{

				//****No Attachment *****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you wish to include documentary evidence of your previous insurance cover that you wish to transfer? ']/../following-sibling::div/div/div[2]/label/span"))).click();
				sleep(500);
				report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);
				sleep(500);
				driver.findElement(By.xpath("//*[@id='acknowledgeDocAdressCheck']/span")).click();
				sleep(500);


			}



			//****Click on Continue Button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);

			 JavascriptExecutor h = (JavascriptExecutor)driver;
				h.executeScript("window.scrollBy(0,500)", "");
				Thread.sleep(2000);
			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\

			//*****Death transfer amount******

			//**I would like to index my fixed cover annually by 5% ***//
			if(fixedcover.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@id='indexation-death']/./following::span)[1]"))).click();
				sleep(800);
				report.updateTestLog("Index 5% of Death Cover","is Selected", Status.PASS);

			}


			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);

			}

			//*****TPD transfer amount******

			if(TPDYN.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

			}

			JavascriptExecutor w = (JavascriptExecutor)driver;
			w.executeScript("window.scrollBy(0,600)", "");
			Thread.sleep(2000);


			//****IP transfer amount*****\\



			if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){


			//*****Select The Waiting Period********\\

				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);



			//*****Select The Benefit  Period********\\

				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);


			//****Enter the IP amount*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);

			}

			JavascriptExecutor j = (JavascriptExecutor)driver;
			j.executeScript("window.scrollBy(0,1000)", "");
			Thread.sleep(2000);


			//*****Click on calculate Quote*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
			sleep(4000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);

			//****Equivalent units of CareSuper checkbox*****\\
			sleep(500);
			/*if(Equivalentvalueinunits.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='equivalentUnit']/span"))).click();
				report.updateTestLog("Equivalent Value in unitised scale", "Yes is selected", Status.PASS);
			}*/

			JavascriptExecutor z = (JavascriptExecutor)driver;
			z.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(2000);
			//**** I agree to all the above terms & conditions*******\\
			sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='transferTermsLabel']/span"))).click();
			report.updateTestLog("Continue", "Clicked on I agree to all the above terms & conditions", Status.PASS);
			sleep(2500);


		  //******Click on Continue******\\

		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[2]"))).click();
		 sleep(4500);
		 report.updateTestLog("Continue", "Continue button is selected", Status.PASS);





			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void healthandlifestyledetails() {

		String RestrictedIllness = dataTable.getData("CareSuper_TransferyourCover", "RestrictedIllness");
		String MedicalTreatment = dataTable.getData("CareSuper_TransferyourCover", "MedicalTreatment");
		String DiagnosedIllness = dataTable.getData("CareSuper_TransferyourCover", "DiagnosedIllness");
		String Fundpremloading = dataTable.getData("CareSuper_TransferyourCover", "Fundpremloading");



		WebDriverWait wait=new WebDriverWait(driver,20);

			try{

		//		if(driver.getPageSource().contains("Eligibility Check")){

			//*******Are you restricted due to illness or injury*********\\

				if(RestrictedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(1500);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(1500);

				}
				driver.findElement(By.cssSelector("body")).click();
				sleep(800);

				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,600)", "");
				sleep(500);
			//*****Have you been paid, or are you eligible to be paid, or have you lodged or are you going to lodge a claim for terminall illness
				//or disability from a superannuation fund, life insurance company or
				//any State or Federal Government body, such as workers compensation, social security, Veterans' affairs or a motor accident scheme?**//

				if(DiagnosedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
					sleep(1500);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
					sleep(1500);
				}

				driver.findElement(By.cssSelector("body")).click();
				sleep(800);
				//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy
			    //to less than 3 years?


				if(MedicalTreatment.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
					sleep(3000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
					sleep(3000);
				}

				driver.findElement(By.cssSelector("body")).click();
				sleep(800);

				if(Fundpremloading.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
					sleep(3000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
					sleep(3000);
				}

				driver.findElement(By.cssSelector("body")).click();
				sleep(800);





			//******Click on Continue Button********\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
			    sleep(500);
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(3000);


			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

public void confirmation() {
//	String GeneralConsent = dataTable.getData("CareSuper_TransferyourCover", "GeneralConsent");
	WebDriverWait wait=new WebDriverWait(driver,20);
	sleep(300);
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	jse.executeScript("window.scrollBy(0,4000)", "");
	sleep(500);
		try{
		if(driver.getPageSource().contains("General consent")){


		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#generalConsentLabelTR > span"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);


		//******Click on Submit*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitTransferCover()']"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(1000);

		}

		//*******Feedback Pop-up******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1500);






		//*****Fetching the Application Status*****\\

		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);
		report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);

		if(!Appstatus.equalsIgnoreCase("TRANSFER OF COVER")){
		//String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
		//System.out.println(Justification);
		sleep(400);

		//*****Fetching the Application Number******\\

		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);



		//******Download the PDF***********\\
		properties = Settings.getInstance();
		String StrBrowseName=properties.getProperty("currentBrowser");
		if (StrBrowseName.equalsIgnoreCase("Chrome")){
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			sleep(1500);
  		}


	  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
	  			sleep(2500);
	  			Robot roboobj=new Robot();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  		}

	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
//	  			Robot roboobj=new Robot();
//	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
//
//	  			sleep(4000);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_TAB);
//	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  			System.out.println("Not downloading the PDF");

	  		}

		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);

		sleep(1000);


		  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);


		}


		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}

public void TransferCoverFirstSuper_Negative() {

	String EmailId = dataTable.getData("CareSuper_TransferyourCover", "EmailId");
	String TypeofContact = dataTable.getData("CareSuper_TransferyourCover", "TypeofContact");
	String ContactNumber = dataTable.getData("CareSuper_TransferyourCover", "ContactNumber");
	String TimeofContact = dataTable.getData("CareSuper_TransferyourCover", "TimeofContact");
	String Gender = dataTable.getData("CareSuper_TransferyourCover", "Gender");
	String FifteenHoursWork = dataTable.getData("CareSuper_TransferyourCover", "FifteenHoursWork");

	String IndustryType = dataTable.getData("CareSuper_TransferyourCover", "IndustryType");
	String OccupationType = dataTable.getData("CareSuper_TransferyourCover", "OccupationType");
	String OtherOccupation = dataTable.getData("CareSuper_TransferyourCover", "OtherOccupation");
	String OfficeEnvironment = dataTable.getData("CareSuper_TransferyourCover", "OfficeEnvironment");
    String TertiaryQual = dataTable.getData("CareSuper_TransferyourCover", "TertiaryQual");
	String Managementrole = dataTable.getData("CareSuper_TransferyourCover", "Managementrole");

	String AnnualSalary = dataTable.getData("CareSuper_TransferyourCover", "AnnualSalary");
	String PreviousInsurer = dataTable.getData("CareSuper_TransferyourCover", "PreviousInsurer");
	String PolicyNo = dataTable.getData("CareSuper_TransferyourCover", "PolicyNo");
	String FormerFundNo = dataTable.getData("CareSuper_TransferyourCover", "FormerFundNo");
	String DocumentaryEvidence = dataTable.getData("CareSuper_TransferyourCover", "DocumentaryEvidence");
	String AttachPath = dataTable.getData("CareSuper_TransferyourCover", "AttachPath");
	String CostType = dataTable.getData("CareSuper_TransferyourCover", "CostType");
	String DeathYN = dataTable.getData("CareSuper_TransferyourCover", "Death");
	String TPDYN = dataTable.getData("CareSuper_TransferyourCover", "TPD");

	String DeathAmount = dataTable.getData("CareSuper_TransferyourCover", "DeathAmount");
	String TPDAmount = dataTable.getData("CareSuper_TransferyourCover", "TPDAmount");
	String TPDAmtGreater = dataTable.getData("CareSuper_TransferyourCover", "TPDAmtGreater");





	try{



		//****Click on Transfer Your Cover Link*****\\
		WebDriverWait wait=new WebDriverWait(driver,18);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Transfer your cover')]"))).click();
		report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);


		//*****Agree to Duty of disclosure*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
		sleep(250);
		report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);


        //*****Agree to Privacy Statement*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyCkBoxLblId']/span"))).click();
		sleep(250);
		report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

        //*****Enter the Email Id*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
		sleep(250);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
		report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
		sleep(500);

		//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


			//****Enter the Contact Number****\\
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}



			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);

			//***************************************\\
			//**********OCCUPATION SECTION***********\\
			//****************************************\\
			//*****Select the 15 Hours Question*******\\

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='fifteenHrsTransferQuestion']/./following::span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='fifteenHrsTransferQuestion']/./following::span)[2]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}


			//*****Industry********\\
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
			sleep(500);

			//*****Occupation*****\\

			Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);


			//*****Other Occupation *****\\

			if(OccupationType.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}

			//**Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar 'white collar' tasks which do not
			//*involve manual work and are undertaken entirely within an office environment (excluding travel time from one office environment to another)?		**//


					if(OfficeEnvironment.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

					//**Do you hold a tertiary qualification or are you a member of a professional institute or registered as a practicing member of your profession by a government body?**//

					if(TertiaryQual.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

					//**Are you in a management role?**//

					if(Managementrole.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}


		          sleep(800);


		//*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);



		//*****Click on Continue*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


		//******************************************\\
		//**********PREVIOUS COVER SECTION**********\\
		//*******************************************\\

		//*****What is the name of your previous fund or insurer?*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
		sleep(250);
		report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

        //*****Please enter your fund member or insurance policy number*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
		sleep(250);
		report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

        //*****Please enter your former fund SPIN number (if known)*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
		sleep(250);
		report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

		//*****Do You want to Attach documentary evidence****\\

		if(DocumentaryEvidence.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 pl0 col-xs-12']/label/span"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "Yes is selected", Status.PASS);



			//****Attach the file******

			driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
			sleep(1500);

			//*****Let the attachment load*****\\
	//		driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
	//		sleep(1000);

			//*****Add the Attachment*****\\

			WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
			sleep(1500);
			report.updateTestLog("Evidence Attachment", "File is successfully attached", Status.PASS);

		}
		else{

			//****No Attachment *****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12 pl0xs']/label/span"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);
			sleep(400);
			driver.findElement(By.xpath("//*[@id='acknowledgeDocAdressCheck']/span")).click();
			sleep(500);


		}


		//****Click on Continue Button******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
				sleep(500);
				report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);



		//*************************************************\\
		//**************TRANSFER COVER SECTION***************\\
		//**************************************************\\

		//*****Death transfer amount******

		if(DeathYN.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
			sleep(500);
			report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);

		}

		//*****TPD transfer amount******

		if(TPDYN.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
			sleep(500);
			report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

		}

		String TPDError=driver.findElement(By.xpath("//*[@ng-if='coverAmtErrFlag']")).getText();

		if(TPDError.equalsIgnoreCase(TPDAmtGreater)){
			report.updateTestLog("TPD Error", "Expected Error Message: "+TPDAmtGreater, Status.PASS);
		}
		else{
			report.updateTestLog("TPD Error", "Error message is not displayed", Status.FAIL);
		}



	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}

}