/**
 *
 */
package pages.metlife.Angular.HOSTPLUS;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Yuvaraj
 *
 */
public class HOSTPLUS_Angular_Login extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HOSTPLUS_Angular_Login(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}

	public HOSTPLUS_Angular_Login HOSTPLUS_Login() {
		enterlogindetails();
		return new HOSTPLUS_Angular_Login(scriptHelper);
	}

	private void enterlogindetails() {

		String XML = dataTable.getData("HOSTPLUS_Login", "XML");
		String saveflow = dataTable.getData("HOSTPLUS_Login", "Saveflow");
		try{
			WebDriverWait wait=new WebDriverWait(driver,18);

			//******Input the XML String******\\

			if(XML.contains("<clientRefNumber>clientunique</clientRefNumber>"))
			{
			    WebDriverWait waiting=new WebDriverWait(driver,18);
			    String split[] = XML.split("clientunique");
				String num = randomgeneration();
				String finalxml = split[0]+"clientunique"+num+split[1];
			/*	String split[] = XML.split("clientunique");
				String num = randomgeneration();
				String finalxml1 = split[0]+"clientunique"+num+split[1];
				String split1[] = finalxml1.split("HOST010");
				String nums = randomgeneration();
				String finalxml2 = split1[0]+"HOST010"+nums+split1[1];
				String split2[] = finalxml2.split("YUVI");
				String number = randomgeneration();
				String finalxml = split2[0]+"YUVI"+number+split2[1];*/
				wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(finalxml);
			//	System.out.println(finalxml);

		}else{

				wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(XML);
				report.updateTestLog("XML String", "XML String is entered", Status.PASS);
			}


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'PROCEED TO NEXT PAGE')]"))).click();
			report.updateTestLog("Proceed To Next Page", "Proceed To Next Page button is Clicked", Status.PASS);


			//****Web response button click*****\\
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Rwd Application')]"))).click();
			report.updateTestLog("RWD Application", "RWD Application button is Clicked", Status.PASS);
            sleep(9000);
           quickSwitchWindows();
     		sleep(4500);
     		/* sleep(8000);
             quickSwitchWindows();
       		sleep(3500); */

			report.updateTestLog("HOSTPLUS Login", "Sucessfully Logged into the application", Status.PASS);
			sleep(6500);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}



