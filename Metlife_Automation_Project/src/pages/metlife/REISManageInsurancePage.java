package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.REISManageInsurancePageObjects;

public class REISManageInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public REISManageInsurancePage selectREISInsuranceCoverOption() {
		manageREISInsuranceCover();
		return new REISManageInsurancePage(scriptHelper);
	}
	
	public REISManageInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void manageREISInsuranceCover(){
		try {
			tryAction(waitForClickableElement(REISManageInsurancePageObjects.lnkChgInsuranceCover),"Click", "Change Insurance Cover Link");
			sleep(10000);
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action manageREISInsuranceCover " + e.getMessage(), Status.FAIL);
		}
	}
}
