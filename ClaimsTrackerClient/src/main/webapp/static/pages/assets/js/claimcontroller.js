// create the module and name it claimApp
        // also include ngRoute for all our routing needs
    var claimApp = angular.module('claimTrackermain', ['ngRoute','ngResource','vcRecaptcha']);
    var count;
    var disableFields = false;
	var datesBetweenErrflag=false;
    // configure our routes
   claimApp.config(function($routeProvider,$httpProvider) {
	   $httpProvider.defaults.withCredentials = true;
        $routeProvider   
        
        	//.when('/', {
          .when('/:id/:fundid', {     
          
                templateUrl : 'static/pages/home.html',
                controller  : 'login',
               reloadOnSearch: false
            }) 
            .when('/', {     
          
                templateUrl : 'static/pages/home.html',
                controller  : 'login',
               reloadOnSearch: false
            })
            
           /* .when('/landing', {
                templateUrl : 'care/landingpage.html',
                controller  : 'landing'
            })*/
            .when('/timeline', {
                templateUrl : 'static/pages/timeline.html',
                controller  : 'timeline'
            })
            .when('/todo', {
                templateUrl : 'static/pages/to-dos.html',
                controller  : 'todo'
            })
             .when('/completed', {
                templateUrl : 'static/pages/completed.html',
                controller  : 'completed'
            })
            .when('/contact', {
                templateUrl : 'static/pages/contact.html',
                controller  : 'contact'
            })
            .when('/completedEmpty', {
                templateUrl : 'static/pages/completed-empty.html',
                controller  : 'completedEmpty'
            })
            .when('/todoEmpty', {
                templateUrl : 'static/pages/to-dos-empty.html',
                controller  : 'todoEmpty'
            })
            .when('/upload', {
                templateUrl : 'static/pages/upload.html',
                controller  : 'upload'
            })
            .when('/main', {
           
                templateUrl : 'static/main.html',
               controller  : 'mainPage'
           })
    });
   
   
   
   claimApp.controller('mainPage',['$scope', '$rootScope', '$location','claimFactory',  function($scope,$rootScope, $location,claimFactory){
	   //	alert('hi');
	          $scope.message = 'Look! I am an about page.';
	          $rootScope.headerSection=true;
	          $scope.claimData = claimFactory.getClaimData();
	          $scope.claimFactory = claimFactory;	          
	          $scope.go = function ( path ) {  
		          	//   alert('Hi1');
		           	  $location.path( path );
		           	};
		         $scope.logout= function () {
			     	  //  Session.clear();
			     	   $scope.go('/');
			     	};
	}]); 
  
 claimApp.controller('login',['$scope','$rootScope', '$location','$http','claimFactory','vcRecaptchaService', 'propertyService','$routeParams',  function($scope,$rootScope, $location, $http,claimFactory,vcRecaptchaService, propertyService,$routeParams){
	   var partner ='';
	   $scope.configObject = propertyService.getProperties();
	   claimFactory.setLabelProperties($scope.configObject);
	   $scope.partnerName = "Metlife";
	   if ( $routeParams.fundid !== undefined) {
		//   alert('login'+partner);
		   partner = $routeParams.fundid;
		   $scope.appName = $routeParams.fundid;
		  // $('#customId').attr('href',"static/pages/"+partner+"/dist/css/custom.css");
		  // 	$('#favIconId').attr('href',"static/pages/"+partner+"/assets/img/favicon-m.ico");
		  // 	$('#vendorImportId').attr('href',"static/"+partner+"/pages/dist/css/vendor-imports.min.css");
		  // 	$('#mainMinId').attr('href',"static/pages/"+partner+"/dist/css/main.min.css");
		  // 	$('#partnerId').attr('src',"static/pages/CareSuper/assets/img/MetLife.png");
		   $('#favIconId').attr('href',"static/pages/"+partner+"/img/favicon.ico");
		   $('#partnerId').attr('src',"static/pages/"+partner+"/img/header_logo.png");
		   $('#partnerId').attr('alt',partner+" logo");
		   $('#footerId').attr('src',"static/pages/"+partner+"/img/footer_logo.png");
		   $('#footerId').attr('data-src',"static/pages/"+partner+"/img/footer_logo.png");
		   $('#footerId').attr('alt',partner);
		   //$('.fundName').val(partner);
		   $scope.partnerName=partner;
		   if(partner.toLowerCase() == 'caresuper'){
			   $('body').attr('id', 'caresuper');
		   }
	   } 
	   claimFactory.setPartnerName($scope.partnerName);
	   $scope.configObject = claimFactory.getLabelProperties();
	   
	   	
	   
       $scope.message = '';       
       $rootScope.headerSection=false;
       $scope.data = {}; 
      // $scope.errorMsg=''  
       $scope.regex = /[0-9]{1,3}/;
       $scope.datePattern= /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
      // $scope.data = {};  
       $scope.user = {claimNo:'',lName:'',dob:'',publicKey:'6LcwfQwUAAAAAAFPj9T5zZosmPiYDXqBdWxWM2ec'};  
       $scope.check = "This is for check"       
   	   $scope.errorMsg=''   		
      // $scope.configObject = propertyService.getProperties();
       $scope.user.publicKey = "6LcwfQwUAAAAAAFPj9T5zZosmPiYDXqBdWxWM2ec";
       
       /*$http.get('./claimTracker_labels.properties').then(function (response) {
       	$scope.configData = response.data;
       	$scope.configData = $scope.configData.split('\n');
       	var arr = [];
       	var key;
       	var newArr = [];       	
       	
       	//var obj = {};
       	for(var i = 0; i < $scope.configData.length; i++){
       		var temp = $scope.configData[i].split('=');
       		newArr = newArr.concat(temp);
       	}
       	for(var i = 0; i < newArr.length; i++){
       		if(i % 2 == 0){
       			key = newArr[i];
       		} else if(i % 2 == 1){
       			var value = newArr[i];
       			$scope.configObject[key] = value;
       		}
       	}
       //	console.log(configObject);
         });*/
       
       $scope.go = function (form,user,path) {     	 	
       
       //alert('Hi12 '+vcRecaptchaService.getResponse($scope.recaptchaId));
    	   if(!form.$valid){             
    		 //alert(vcRecaptchaService.getResponse($scope.recaptchaId));
    		 if(vcRecaptchaService.getResponse($scope.recaptchaId) === ""){ //if string is empty
 			   	$scope.message = $scope.configObject.robotErr;
    		 }
    	   } else {
    		//   alert('captche :'+vcRecaptchaService.getResponse($scope.recaptchaId));
    		   if(vcRecaptchaService.getResponse($scope.recaptchaId) === ""){ //if string is empty
    			   	$scope.message = $scope.configObject.captcheFailedMsg;
             		 form.$submitted=true;
             		 form.$valid = false;
         		 }else {
             		//alert(vcRecaptchaService.getResponse())             		
		          	 var data = $.param({
		             		 secret : '6LcwfQwUAAAAAJAGiqaCbEZgo7mxikEL8sRha_wG',
		             		 response:vcRecaptchaService.getResponse($scope.recaptchaId),
		             		 remoteip:'ausydetkd03'
		             		 
		         		 });      

		//	alert('data '+data);
			claimFactory.verifyCaptche(data).then(function(response) {
				
				if (response.status == '200') {					
					claimFactory.getClaim(user).then(function(response) {  
						//alert( response);
			           	  $scope.data =  response.data;	
			           	 // alert('$scope.data '+$scope.data); 
			               if ($scope.data != null && '' != $scope.data) { 
			            	  // alert('H1i'+form.$valid); 
			            
			            	   var toDate= moment().format('DD/MM/YYYY');
			            	   var fromDate = $scope.data.statusDate;
			            	   var dt1 = fromDate.split('/'),
			                     dt2 = toDate.split('/'),
			                     one = new Date(dt1[2], dt1[1], dt1[0]),
			                     two = new Date(dt2[2], dt2[1], dt2[0]);
			                 
			            	   var millisecondsPerDay = 1000 * 60 * 60 * 24;
			            	   var millisBetween = two.getTime() - one.getTime();
			            	   var days = millisBetween / millisecondsPerDay;
			               
			            	   // var months = parseInt(moment().diff(moment([year, month, day]), 'months', true));
			           
			            	   	if ($scope.data.status =='Closed' && days > 365) {
			            	   // Write the logic here to show error if status date exceeds 12 months
			            		$scope.datesBetweenErrflag=true;
			            		$scope.datesBetweenMsg = $scope.configObject.datesBetweenMsg;
			            		$scope.emailLink = 'auservices@metlife.com';			   	     
			            	   } else if(($scope.data.status =='Closed' && days <= 365) || ($scope.data.status != 'Closed') ) {
			            		   $scope.datesBetweenErrflag=false;
			            	   		$scope.datesBetweenMsg = "";          
			            	   		claimFactory.setClaimData($scope.data); 
			            	   		//claimFactory.filterClaimData($scope.data);
			            	   		claimFactory.setUniqueId($scope.data.uniqueId);
			            	   		claimFactory.setClaimId($scope.data.claimId);
			            	   		claimFactory.setClaimStatus($scope.data.status);
			            	   	   claimFactory.getClaimActivities($scope.data.uniqueId).then(function(response) {  
			            	  	   $scope.claimData = response.data;			            	  	   
			            	  	   claimFactory.setActivitiesData($scope.claimData);
			            	  	   claimFactory.filterClaimData($scope.claimData);
			            	  	   $location.path( path ); 
			            	  	   //alert('hi');
			            	  	   },function (error) {
			            	         //	form.$submitted=true;
			            	  		//form.$valid = false; 
			            	  		//$scope.message = "Internal error,Please try again";
			            	      }); 
			            	   		      
			            	   }
			   			  	     				             
			           	   } else {
			           		   form.$submitted=true;
			           		   form.$valid = false;
			           		   $scope.message = $scope.configObjectinvalidLoginMsg;
			           	   }			           	    
			            },function (error) {
			            	form.$submitted=true;
			        		form.$valid = false; 
			        		$scope.message = $scope.configObject.internalErrMsg;
			            }); 
					
				} else {
					 $scope.message = 'captche server verification failed,please try again';
            		 form.$submitted=true;
            		 form.$valid = false;
				}								
			},function (error) {
				$scope.message = 'captche server verification failed,please try again';
            	form.$submitted=true;
        		form.$valid = false;        		
            });
       }					  
        	     	    	   
     };
   }  
 // added for disabling the timline, to-do's and complet     };
   $scope.needHelp = function(response) {
	   disableFields = true;
   }
   
   $scope.onCapctheCreate = function(widgetId) {	 
	//   alert('widgetId :'+widgetId);
	   $scope.recaptchaId = widgetId;
   }
   $scope.onCapctheSuccess = function() {
	   $scope.message ='';
   }
   $scope.onCapctheExpire = function() {
	 //  alert('onCapctheExpire');
   }
}]); 
   
 claimApp.controller('timeline',['$scope', '$rootScope','$window','$location','$http','claimFactory',  function($scope,$rootScope, $window, $http, $location,claimFactory){
	// alert('timeline');
	 $rootScope.headerSection=true;
	   $scope.claimData = claimFactory.getClaimData();
	   $scope.partnerName = claimFactory.getPartnerName();
	   $scope.uniqueId = claimFactory.getUniqueId();   
	   $scope.claimData = claimFactory.getActivitiesData();	  
	   
//function ( path ) {  
    	//   alert('Hi1');
     	//  $location.path( path );
     	//};
	   //back to top for help
	      	$scope.scrollToTop = function(){
	       	$window.scrollTo(0,0);
	      	}
   }]); 
   
   claimApp.controller('todo',['$scope','$rootScope','$window', '$location','claimFactory',  function($scope,$rootScope,$window, $location,claimFactory){
	   $scope.message = 'Look! I am an about page.';
       $rootScope.headerSection=true;
       //$scope.claimNo = claimNo
       $scope.claimData = claimFactory.getClaimData();
       $scope.noOutStanding = claimFactory.getNoOutStanding(); 
       $scope.activityData = claimFactory.getActivitiesData();
             
       $scope.scrollToTop = function(){
        	$window.scrollTo(0,0);
       }
     	
   }]); 
   
   claimApp.controller('completed',['$scope', '$rootScope','$window','$location','claimFactory',  function($scope,$rootScope,$window, $location,claimFactory){
	   $scope.message = 'Look! I am an about page.';
       $rootScope.headerSection=true;
       //$scope.claimNo = claimNo
       
       $scope.claimData = claimFactory.getClaimData();
       $scope.noCompleted = claimFactory.getNoCompleted();   
       $scope.activityData = claimFactory.getActivitiesData();
    
       $scope.scrollToTop = function(){
       	$window.scrollTo(0,0);
       }
     	
   }]); 
   
   claimApp.controller('contact',['$scope', '$rootScope','$location', '$window','$http','propertyService','claimFactory', function($scope,$rootScope, $location, $window, $http, propertyService,claimFactory){
       $scope.message = 'Look! I am an about page.';       
       
       $('.accordionHeader').on("click", function () {
    	   	   		var acc_con = $(this).next('.accordionContent');
    	   	   		if (acc_con.is(':visible')) {
    	   	   			$(this).removeClass('acc-open');
    	   	   			acc_con.slideUp(300);
    	   	   			$scope.acc_con = false;
    	   	   			$scope.$apply();
    	   	   		} else {
    	  	   			$(this).addClass('acc-open');
    	   	   			$(this).addClass('.accordionContent');
    	   	   			acc_con.slideDown(300);
    	   	   			 $scope.acc_con = true;
    	   	   			 $scope.$apply();
    	   	   		}
    	   	   	});
   
       $scope.configObject = claimFactory.getLabelProperties();
      
       $scope.scrollToTop = function(){
        	$window.scrollTo(0,0);
   	   }
     /// added for disabling the timline, to-do's and completed sections
          		
                  if(disableFields==true){
                	 // alert("disableFields----" +disableFields);
                	  $rootScope.headerSection=false;
                	  
        	          $('#disableTimeline').click(function(e) {
        	              $(this)
        	                 .css('cursor', 'default')
        	                 .css('text-decoration', 'none')
        	              
        	              return false;
        	          });
        	          $('#disabletodoField').click(function(e) {
        	              $(this)
        	                 .css('cursor', 'default')
        	                 .css('text-decoration', 'none')
                   
        	              return false;
        	          });
        	          $('#disableCompleted').click(function(e) {
        	              $(this)
        	                 .css('cursor', 'default')
        	                 .css('text-decoration', 'none')
        	          
        	              return false;
        	          });
                  }else{
                	//  alert("disableFields----" +disableFields);
                	  $rootScope.headerSection=true;
                  }
        
   }]);
   claimApp.controller('completedEmpty',['$scope','$rootScope', '$location',  function($scope, $rootScope,$location){
       $scope.message = 'Look! I am an about page.';
       $rootScope.headerSection=true;
       //$scope.claimNo = claimNo
       $scope.go = function ( path ) {        	
     	  $location.path( path );
     	};
     	
   }]);
   claimApp.controller('todoEmpty',['$scope','$rootScope', '$location',  function($scope,$rootScope,$location){
       $scope.message = 'Look! I am an about page.';
       //$scope.claimNo = claimNo
       $rootScope.headerSection=true;
       $scope.go = function ( path ) {        	
     	  $location.path( path );
     	};
     	
   }]);   
   
  
   function process(date){
	   var parts = date.split("/");
	   return new Date(parts[2], parts[1] - 1, parts[0]);
	}
   
   claimApp.controller('upload',['$scope','$rootScope','$window','$http', '$location','fileUpload', 'propertyService','claimFactory',  function($scope, $rootScope,$window, $http,$location,fileUpload, propertyService,claimFactory){
       $scope.message = 'Look! I am an about page.';
       //alert(claimFactory.getClaimId());
       $rootScope.headerSection=true;
       $scope.urls = claimFactory.getLabelProperties();
       //$scope.claimNo = claimNo
       $scope.go = function ( path ) {        	
     	  $location.path( path );
     	};
     	
//     	$scope.files = [];
//        $scope.selectedFile = null;
//        $scope.uploadFiles = function(files, errFiles) {
//          $scope.selectedFile =  files[0] ;
//          $scope.addFilesToStack();
//        };
//        
//        
//        var formdata = new FormData();
// 	    $scope.getTheFiles = function ($files) {
// 	        angular.forEach($files, function (value, key) {
// 	            formdata.append(key, value);
// 	        });
// 	    };
// 	    
//	      $scope.addFilesToStack = function () {
//	            $scope.files.push($scope.selectedFile);
//	            $scope.selectedFile = null;
//	      };	      
//          
//	     
//	      $scope.submitFiles = function () {
//	          angular.forEach($scope.files, function(file) {
//	        Upload.http({
//	              url: $scope.urls.urlFileUpload,
//	                headers : {
//	                  'Content-Type': file.name
//	                 // 'File-Name': file.name
//	                },
//	                data: file
//	              });
//	          });
//	      };
//	     
//	      $scope.uploadFilesTest = function(){
//              var file = $scope.myFile;
//              
//              console.log('file is ' );
//              console.dir(file);
//              
//              var uploadUrl = $scope.urls.urlFileUpload ;
//              fileUpload.uploadFileToUrl(file, uploadUrl);
//           };
	        
         
           $scope.scrollToTop = function(){
             	$window.scrollTo(0,0);
         	}
           var docList = new Array();
           
           angular.element(document).ready(function () {              
               var manualUploader = new qq.FineUploader({
			        element: document.getElementById('fine-uploader-manual-trigger'),
			        template: 'qq-template-manual-trigger',
			        request: {
			            endpoint: $scope.urls.urlUploadFile
			        },
			        thumbnails: {
			            placeholders: {
			                waitingPath: $scope.urls.urlWaitingPath,
			                notAvailablePath: $scope.urls.urlNotAvailablePath
			            }
			        },
			        params: { 
			        	dropDownVal:  'poorna'
			        },
			        session : {
			            endpoint : $scope.urls.urlEndpoint2+claimFactory.getClaimId(),
			            refreshOnRequest:true,
//			            params :{
//			            	claimNo:  claimFactory.getClaimId()
//			            },
			            thumbnails: {
				            placeholders: {
				                waitingPath: $scope.urls.urlWaitingPath,
				                notAvailablePath: $scope.urls.urlNotAvailablePath
				            }
				        },
			          },
			        autoUpload: false,
			        debug: true,

					validation: {
						allowedExtensions: ['pdf','txt','doc','docx','xls','xlsx','tif','jpeg', 'jpg', 'png'],
						// itemLimit: 3,
						 sizeLimit: 25000000 // 50 kB = 50 * 1024 bytes 
					},
					callbacks: {
			            onSubmit: function(id, fileName) {
			            	currentId = id;
			            },
			            onUpload: function(id, fileName) {
			          
			            	//var  qqEle = '.qq-file-id-'+id+' select :selected';
			            	console.log($('.qq-file-id-'+currentId).children('span.qq-document-type').children().val());
			            	if($('.qq-file-id-'+currentId).children('span.qq-document-type').children().val() != '0'){
								var  qqEle = '.qq-file-id-'+currentId+' select :selected';
								this.setParams({description:$(qqEle).text(),claimStatus:claimFactory.getClaimStatus()});
								console.log('qqEle1 :'+qqEle);
							} else{

							//	alert("Please select the document type");								

								

			            		//$('li.qq-file-id-'+id).append("<span style='color:red' class='qq-upload-status-text'>Please select the document type</span>");
			            		//throw new Error("Please select the document type");
			            		console.log('qqEle2 :'+qqEle);
							}
			            },
			            onProgress: function(id, fileName, loaded, total) {

			             //   alert('onProgress');

			            },
			            onComplete: function(id, fileName, responseJSON) {
			            //	alert('onComplete');
			            	 if(responseJSON.success){
			            		 //$scope.isDisabled = true;
			            		 //$scope.$apply();
			            		 $('select').prop('disabled',true);
			            		// $('li.qq-file-id-'+id).append("<span style='color:green' class='qq-upload-status-text'>Successfully Uploaded</span>");
			            	 } 
			            	 if (responseJSON.length != 0){
			            		//var document = {"documentlocation":"", "documentCode":"", "documentStatus":""};
			            		var loc = JSON.parse(responseJSON.doucment).documentlocation;
			            		var code = JSON.parse(responseJSON.doucment).documentCode;
			            		var status = JSON.parse(responseJSON.doucment).documentStatus;
			            		var fileName = JSON.parse(responseJSON.doucment).fileName;
			            		var fileSize = JSON.parse(responseJSON.doucment).fileSize;
			            		var uuId = JSON.parse(responseJSON.doucment).uuId;
			            		var policyNum = claimFactory.getClaimId();
			            		var doc = new Document(loc,code,status,policyNum,id,fileName,fileSize,uuId);
			            		
			            		docList.push(doc);
			            		
			            		console.log(responseJSON.doucment);
			            		console.log(JSON.parse(responseJSON.doucment).documentlocation);			            														 
			            	}
			            },
			            onAllComplete: function(id, fileName, responseJSON) {
			            	console.log("onAllComplete"+id+' fileName '+fileName+' responseJSON '+responseJSON);			            				            	
			            	var response = $http.post($scope.urls.urlSubmitFile,docList);			            	
							response.success(function(data, status, headers, config) {
								
							});
							response.error(function(data, status, headers, config) {
								
							});
							docList = new Array();
			               
			            },
			            onSessionRequestComplete: function(responseJSON,status) {
			            	console.log(JSON.stringify(responseJSON));	
			            	if (status === true ){
				            	 for (var i = 0;i < responseJSON.length; i++) {
				            		 var uuid = this.getUuid(i);
				            		 for (var j = 0;j < responseJSON.length; j++) {
				            			 if (uuid == responseJSON[j].uuid) {
				            				 console.log('uuid :'+uuid);
							            		var  qqEle = ".qq-file-id-"+j+" select option[value='"+responseJSON[j].description+"']";
							            		var  qqSel = ".qq-file-id-"+j+" select";
							            		console.log('qqEle :'+qqEle);
								            	//$(qqEle).val("1");
								            	$(qqEle).attr('selected','selected');
								            	$(qqSel).prop('disabled',true);
				            			 }
				            		 }
				            		 
				                }
			            	}
			            	
			            },
			            onSuccess: function(id, fileName, responseJSON) {
			            //	 alert('onSuccess'); 
			            }
			        }

				});

               qq(document.getElementById("trigger-upload")).attach("click", function() {	
            	   if (currentId != 'undefined') {
					if($('.qq-file-id-'+currentId).children('span.qq-document-type').children().val() != '0'){
							var  qqEle = '.qq-file-id-'+currentId+' select :selected';
							manualUploader.setParams({description:$(qqEle).text()});
						manualUploader.uploadStoredFiles();
						} else{
							alert("Please select the document type");
							manualUploader._storedIds.pop(currentId);
							//console.log('qqEle2 :'+qqEle);
		            		$('li.qq-file-id-'+currentId).append("<span style='color:red' class='qq-upload-status-text'>Please select the document type</span>");
		            		//throw new Error("Please select the document type");
						}
            	   }
				});
           });
   }]);
    
   function test(id){
	   
	  // alert('test'+$(this).val());
   }
    function Document(loc,code,status,policyNum,fileId,fileName,fileSize,uuId) {
	   this.documentlocation = loc;
	   this.documentCode = code;
	   this.documentStatus = status;
	   this.policyNum =  policyNum;	
	   this.fileId =  fileId;	
	   this.fileName =  fileName;	
	   this.fileSize =  fileSize;	
	   this.uuId =  uuId;		
		
	 }
   