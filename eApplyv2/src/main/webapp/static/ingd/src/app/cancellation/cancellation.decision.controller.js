class CancelDecisionCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $state, $stateParams, ngDialog, $scope) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$state = $state;
    this._$stateParams = $stateParams;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._ngDialog = ngDialog;
    this._$scope = $scope;
    this.cancelSubmitData = {};
  }

  init() {
		this.cancelSubmitData = this._EapplyData.getCancelData() || this._EapplyData.exitOnError();
    this.cancelWarning =  (this.cancelSubmitData.coverCancellation.death ? "Death" : "") +
                          (this.cancelSubmitData.coverCancellation.death && this.cancelSubmitData.coverCancellation.tpd && this.cancelSubmitData.coverCancellation.ip ? " , " : this.cancelSubmitData.coverCancellation.death && this.cancelSubmitData.coverCancellation.tpd && !this.cancelSubmitData.coverCancellation.ip ? " and " : "") +
                          (this.cancelSubmitData.coverCancellation.tpd ? "TPD" : this.cancelSubmitData.coverCancellation.death && !this.cancelSubmitData.coverCancellation.tpd && this.cancelSubmitData.coverCancellation.ip ? " and " : "") +
                          (this.cancelSubmitData.coverCancellation.tpd && this.cancelSubmitData.coverCancellation.ip ? " and " : "") +
													(this.cancelSubmitData.coverCancellation.ip ? "Income Protection" : "");
    this.takeSurvey();
    this._EapplyData.setCancelData(null);
    
    this._$scope.$on('$locationChangeStart', function(evnt, next, current){
    	var currentURL = current.split("/");
    	if(currentURL[currentURL.length-1] == 'cancelDecision'){
    		evnt.preventDefault();
    	}    	
    });
  }

  takeSurvey() {
    /*NPS survey popup*/
    const _this = this;
    this._ngDialog.openConfirm({
      template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title text-center"> We value your feedback</h4></div><div class="modal-body"><!-- Row starts --><div class="row">  <div class="col-12">  <p class="text-center">  </p><div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> <button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
      className: 'ngdialog-theme-plain custom-width',
      showClose: false,
      plain: true,
    }).then(
      (value) => {
        if(value=='onYes' && _this._$stateParams.npsTokenUrl != null) {
          let url = "http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk="+ _this._$stateParams.npsTokenUrl;
          $window.open(url, '_blank');
          return true;
        }
      }, 
      (err) => {
        if(err == 'oncancel'){
          return false;
        }
      });
   }
}

export default CancelDecisionCtrl;