package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Health")

@XmlRootElement
public class Health {
	
	private String SmokerIndicator;
	private String Height ;
	private String Weight ;
	private String BMI ;
	
	public String getSmokerIndicator() {
		return SmokerIndicator;
	}

	public void setSmokerIndicator(String smokerIndicator) {
		SmokerIndicator = smokerIndicator;
	}

	public String getBMI() {
		return BMI;
	}

	public void setBMI(String bmi) {
		BMI = bmi;
	}

	public String getHeight() {
		return Height;
	}

	public void setHeight(String height) {
		Height = height;
	}

	public String getWeight() {
		return Weight;
	}

	public void setWeight(String weight) {
		Weight = weight;
	}

	
	
}
