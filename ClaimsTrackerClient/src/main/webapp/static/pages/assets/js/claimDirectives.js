claimApp.directive('myDatePicker',function(){
	   return {
	     restrict: 'A',	
	     require: 'ngModel',
	     link: function($scope, elem, $attr,ngModel){
	    	 $( elem ).datepicker({
	    			dateFormat: "dd/mm/yy",
	    			onSelect:function (date) {	    				
	    				console.log(ngModel);    					    				 
     					$scope.$apply(function() {
     						ngModel.$setViewValue(date);         					
       				 	});
	    			},
	    			changeMonth: true,
	    			changeYear: true,
	    			minDate: "-100Y",
	    			maxDate: "+0D",
	    			yearRange: "-100:+0",
	    			showOn: "both",
	    			buttonText: "<i class='fa fa-calendar'></i>"
	    	    });
	     }
	   }
	 });	 
	
claimApp.directive('ngFiles', ['$parse', function ($parse) {

    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    };

    return {
        link: fn_link
    }
} ]);

claimApp.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);