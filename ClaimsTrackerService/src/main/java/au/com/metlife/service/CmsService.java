package au.com.metlife.service;

import java.util.HashMap;
import java.util.List;

import au.com.metlife.model.ClaimOutput;


public interface CmsService {
	
	
	
	//HashMap<String, Object> refreshCache();
	List<ClaimOutput> getClaimStatus(String claimNo,String surName,String dob);
	List<ClaimOutput> getClaimRequirements(String claimNo);
	List<ClaimOutput> getClaimEvents(String claimNo);
}
