package au.com.metlife.eapply.repositories;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import au.com.metlife.eapply.repositories.model.MemberFileUpload;

public interface MemberFileUploadRepo extends JpaRepository<MemberFileUpload, BigInteger> {
	
	public MemberFileUpload findByApplicationNumber(String applicationNumber);
	public MemberFileUpload findByApplicationNumberAndSurname(String applicationNumber,String surname);
}
