export default class CommonAPI {
  constructor(AppConstants, $http, $state, $q) {
    'ngInject';
    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;
    this.urlList = null;
  }

/*
* Application API URLs will be fetched (fetchApiUrls) and stored (setUrlList) here 
* to reference (getUrlList) through out the application
*/
fetchApiUrls(path) {
  let deferred = this._$q.defer();
  deferred.resolve(this._AppConstants.appUrls);
  this.setUrlList(this._AppConstants.appUrls);
  return deferred.promise;
}

  setUrlList(list) {
    this.urlList = list;
  }

  getUrlList() {
    return this.urlList || null;
  }

/*
* Application auth token will be saved here to utilise in every API call
*/

  setAuthToken(token) {
    this.authToken = token;
  }

  getAuthToken() {
    return this.authToken;
  }

/*
* Application auth token will be saved here to utilise in every API call
*/

  setAppCRN(appCRN) {
    this.appCRN = appCRN;
  }

  getCRN() {
    return this.appCRN;
  }

/*
* Application number will be retrieved here to utilise in every API call
*/
  generateAppNumber(path) {
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'GET',
      data: {
        tokenId: this.authToken
      }
    }).then(
      (res) => {
        this.setAppNumber(res.data);
        deferred.resolve(res.data);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }
  setAppNumber(appCRN) {
    this.appNumber = appCRN;
  }

  getAppNumber() {
    return this.appNumber;
  }


/*
* Application industry list will be fetched (fetchIndustryList) and stored (setIndustryList) here 
* to reference (getIndustryList) through out the application
*/
  fetchIndustryList(path, fundCode) {
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        fundCode: fundCode
      }
    }).then(
      (res) => {
        this.setIndustryList(res.data);
        deferred.resolve(res.data);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }

  setIndustryList(list) {
    this.industryList = list;
  }

  getIndustryList() {
    return this.industryList || null;
  }

/*
* Application industry list will be fetched (fetchIndustryList) and stored (setIndustryList) here 
* to reference (getIndustryList) through out the application
*/
  fetchOccupationList(path, industry, fundCode) {
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        fundId: fundCode,
        induCode: industry
      }
    }).then(
      (res) => {
        deferred.resolve(res.data);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }
  
  getOccupationRating(path, fundId, induCode) {
	    let deferred = this._$q.defer();
	    this._$http({
	      url: path,
	      method: 'GET',
	      headers: {
	        'Content-Type': 'application/json'
	      },
	      params: {
	    	  fundId: fundId,
	    	  occName: induCode,
	      }
	    }).then(
	      (res) => {
	        //this.setClientData(res.data);
	        deferred.resolve(res.data);
	      },

	      (err) => {
	        deferred.reject(err);
	      }
	    );
	    return deferred.promise;
	  }

  printEapply(path, data) {
	    let deferred = this._$q.defer();
	    this._$http({
	      url: path,
	      method: 'POST',
	      data: data
	    }).then(
	      (res) => {
	        deferred.resolve(res.data);
	      },

	      (err) => {
	        deferred.reject(err);
	      }
	    );
	    return deferred.promise;
	  }
  
  downloadPDF(path, fileName) {
	    let deferred = this._$q.defer();
	    this._$http({
	      url: path,
	      method: 'POST',
	      params:{
            'file_name': fileName
        },
        responseType: 'arraybuffer',
        transformResponse: function (data) {
      	  var pdf;
            if (data) {
                pdf = new Blob([data], {
                    type: 'application/pdf'
                });
            }
            return {
                response: pdf
            };
        }
	    }).then(
	      (res) => {
	        deferred.resolve(res.data);
	      },

	      (err) => {
	        deferred.reject(err);
	      }
	    );
	    return deferred.promise;
	  }
  
  calculateAll(path, data) {
	    let deferred = this._$q.defer();
	    this._$http({
	      url: path,
	      method: 'POST',
	      data: data
	    }).then(
	      (res) => {
	        deferred.resolve(res.data);
	      },

	      (err) => {
	        deferred.reject(err);
	      }
	    );
	    return deferred.promise;
	  }
}
