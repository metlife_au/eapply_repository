package au.com.metlife.eapply.quote.utility;

import java.math.BigDecimal;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.model.InsuranceModel;
import au.com.metlife.eapply.calculator.model.InsurCalculatorRuleBO;
import au.com.metlife.eapply.calculator.model.InsurCalculatorVO;
import au.com.metlife.eapply.calculator.serviceimpl.CalcServiceImpl;


public class CalculateHelper {
	private static final Logger log = LoggerFactory.getLogger(CalculateHelper.class);
    public static final String WEEKLY="Weekly";
	
	public static final String ANNUALLY="Annually";
	
	public static final String QUARTERLY="Quarterly";
	
	public static final String MONTHLY="MONTHLY";
	
	public static final String FORTNIGHTLY="FORTNIGHTLY";
	public static InsurCalculatorRuleBO calculateAverageTaxRate(InsurCalculatorRuleBO insurCalculatorRuleBO,
			Map ruleInfoMap,String ruleFileLocation,String ruleNameAsPerFund) {
		
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			
			insurCalculatorRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		} 
		insurCalculatorRuleBO = DroolsHelper.invokeInsurCalRue(insurCalculatorRuleBO,ruleFileLocation); 
				
	return insurCalculatorRuleBO;
	}
	
	public static InsurCalculatorRuleBO invokeInsuranceCalcRule(InsurCalculatorRuleBO insurCalculatorRuleBO,
			Map ruleInfoMap,String ruleFileLocation,String ruleNameAsPerFund) {
		
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			
			insurCalculatorRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		} 
		insurCalculatorRuleBO = DroolsHelper.invokeInsurCalRue(insurCalculatorRuleBO,ruleFileLocation); 
				
	return insurCalculatorRuleBO;
	}
	public InsurCalculatorVO calculateRecommendedIpCover(InsurCalculatorRuleBO insurCalculatorRuleBO,InsurCalculatorVO insurCalculatorVO,InsuranceModel ruleModel){
		log.info("calculateRecommendedIpCover start");
		String METHOD_NAME = "calculateRecommendedIpCover";
		
		BigDecimal recommendedIpCover=null,maxRecommendedIpCover=null,yearlyGrossSal,ipThresholdLimit=null;BigDecimal excessSal=null;
		
		try{
			insurCalculatorVO.setIpNotEligbileDueToSal(false);
			insurCalculatorVO.setIpNotEligibleDueToAge(false);
			if(insurCalculatorVO.getAge()!=null && (insurCalculatorVO.getAge() >= insurCalculatorRuleBO.getIpMinAge() && insurCalculatorVO.getAge() <= insurCalculatorRuleBO.getIpMaxAge())){
				if(insurCalculatorVO.getIpReplaceRatio()!=null && insurCalculatorVO.getIpBenefitPaidToSA()!=null && insurCalculatorVO.getGrossSalVal()!=null){
					/*SRKR: Reset the ip recommended cvr exceed flag, becuasue if the user change the required cover then we should not display the message*/
					insurCalculatorVO.setIpRecommendedCvrExceeded(false);
					/*Assumption is always the gross sal is anually.. so converting if the user enters other than annual freq.type*/
					//yearlyGrossSal=convertToYearlyAmount(insurCalculatorVO.getIncomeSalPeriodVal(), insurCalculatorVO.getGrossSalVal());
					yearlyGrossSal = insurCalculatorVO.getGrossSalVal();
					if("SFPS".equalsIgnoreCase(insurCalculatorRuleBO.getProductCode()) && (insurCalculatorVO.getIpBenefitPeriod()!= null &&insurCalculatorVO.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS))){
						if(yearlyGrossSal.compareTo(new BigDecimal(428001))< 0 && yearlyGrossSal.compareTo(new BigDecimal(400000))>0){
						excessSal = yearlyGrossSal.subtract(new BigDecimal(400000));
						recommendedIpCover=((insurCalculatorVO.getIpReplaceRatio().add(insurCalculatorVO.getIpBenefitPaidToSA()).multiply(new BigDecimal(0.01))).multiply(new BigDecimal(400000))).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP);
						recommendedIpCover = recommendedIpCover.add((excessSal.multiply(new BigDecimal(0.50)).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP)));
					}else{
						recommendedIpCover=((insurCalculatorVO.getIpReplaceRatio().add(insurCalculatorVO.getIpBenefitPaidToSA()).multiply(new BigDecimal(0.01))).multiply(yearlyGrossSal)).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP);
					}
						
					}else if("SFPS".equalsIgnoreCase(insurCalculatorRuleBO.getProductCode()) && (insurCalculatorVO.getIpBenefitPeriod()!= null && insurCalculatorVO.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_65))){ 
						if(yearlyGrossSal.compareTo(new BigDecimal(484001))< 0 && yearlyGrossSal.compareTo(new BigDecimal(320000))>0){
						excessSal = yearlyGrossSal.subtract(new BigDecimal(320000));
						recommendedIpCover=((insurCalculatorVO.getIpReplaceRatio().add(insurCalculatorVO.getIpBenefitPaidToSA()).multiply(new BigDecimal(0.01))).multiply(new BigDecimal(320000))).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP);						
						recommendedIpCover = recommendedIpCover.add((excessSal.multiply(new BigDecimal(0.50)).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP)));
					}else{
						recommendedIpCover=((insurCalculatorVO.getIpReplaceRatio().add(insurCalculatorVO.getIpBenefitPaidToSA()).multiply(new BigDecimal(0.01))).multiply(yearlyGrossSal)).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP);
					}
						
					}else{
						recommendedIpCover=((insurCalculatorVO.getIpReplaceRatio().add(insurCalculatorVO.getIpBenefitPaidToSA()).multiply(new BigDecimal(0.01))).multiply(yearlyGrossSal)).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP);
					}			
					
					
					recommendedIpCover=QuoteHelper.createMultiplesByFactor(recommendedIpCover,insurCalculatorRuleBO.getIpMultFactor());
					maxRecommendedIpCover=recommendedIpCover;
					/*Check if recommendedCover is greater than max eligible limit or less than min limit. if true than reset to max/min eligible amount*/
					if(insurCalculatorRuleBO!=null && insurCalculatorRuleBO.getIpMaxCover()!=null && recommendedIpCover!=null && insurCalculatorRuleBO.getIpMinCover()!=null){
						BigDecimal eightyFivePerSalary=null;
						if(yearlyGrossSal!=null && insurCalculatorRuleBO.getMaxIPSalaryPerc()!=null){
							eightyFivePerSalary=((insurCalculatorRuleBO.getMaxIPSalaryPerc().multiply(new BigDecimal(0.01))).multiply(yearlyGrossSal)).divide(new BigDecimal(12),0,BigDecimal.ROUND_HALF_UP);
							if(eightyFivePerSalary.compareTo(insurCalculatorRuleBO.getIpMaxCover())>0){
								ipThresholdLimit=insurCalculatorRuleBO.getIpMaxCover();
							}else{
								ipThresholdLimit=eightyFivePerSalary;
							}
							ipThresholdLimit=QuoteHelper.createMultiplesByFactor(ipThresholdLimit,insurCalculatorRuleBO.getIpMultFactor());
						}
						insurCalculatorVO.setIpThresholdLimit(ipThresholdLimit);
						if(ipThresholdLimit!=null && recommendedIpCover.compareTo(ipThresholdLimit)>0){
							recommendedIpCover=ipThresholdLimit.setScale(0);
							insurCalculatorVO.setIpRecommendedCvrExceeded(true);
						}else if(recommendedIpCover.compareTo(insurCalculatorRuleBO.getIpMinCover())<0){
						
							if(maxRecommendedIpCover.compareTo(insurCalculatorRuleBO.getIpMinCover())<0){
								recommendedIpCover=maxRecommendedIpCover;
							}else{
								recommendedIpCover=insurCalculatorRuleBO.getIpMinCover().setScale(0);
							}
	//						recommendedIpCover=BigDecimal.ZERO;
						}
					}
					
					insurCalculatorVO.setRecommendedIpCover(maxRecommendedIpCover);
					/*If the user changes the required cover then while doing the recalulcalation if the recommended is same as required
					 * then use the user entered amount as required cover else update the user enterede amount with 
					 * recommended cover*/
					if(insurCalculatorVO.getRecommendedIpCoverOld()==null){/*SRKR; First time recommended cover and required cover are same if it is with in limts else required cover = max eligble cover*/
						insurCalculatorVO.setRequiredIpCover(recommendedIpCover);
						insurCalculatorVO.setRecommendedIpCoverOld(maxRecommendedIpCover);
					}else if(insurCalculatorVO.getRecommendedIpCoverOld().compareTo(maxRecommendedIpCover)!=0){/*SRKR: Check if old recommeded cover and new recommended cover are different, if different 
					means reset both required and recommended covers*/
						insurCalculatorVO.setRequiredIpCover(recommendedIpCover);
						insurCalculatorVO.setRecommendedIpCoverOld(maxRecommendedIpCover);
//						insurCalculatorVO.setIpRequriedEnterdByUser(false);
//						insurCalculatorVO.setIpReuiredUnitsRounded(false);
					}else{/*SRKR; if only required cover amount is changed then dont change the required cover.*/
						insurCalculatorVO.setRecommendedIpCoverOld(maxRecommendedIpCover);
						if(insurCalculatorVO.isIpRecommendedCvrExceeded() && insurCalculatorVO.getRequiredIpCover().compareTo(recommendedIpCover)<=0){
							insurCalculatorVO.setIpRecommendedCvrExceeded(false);
						}
					}
				}
			}else{
				insurCalculatorVO.setIpNotEligibleDueToAge(true);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return insurCalculatorVO;
	}
	public InsurCalculatorVO calculateRecommendedTPDCover(InsurCalculatorRuleBO insurCalculatorRuleBO,InsurCalculatorVO insurCalculatorVO){
		log.info("calculateRecommendedTPDCover start");
		String METHOD_NAME = "calculateRecommendedTPDCover";
		//LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		BigDecimal recommendedTpdCover=null,maxRecommendedTpdCover=null;;
		BigDecimal superContrAndExpenses=BigDecimal.ZERO;
		BigDecimal debits=BigDecimal.ZERO;
		try{
			insurCalculatorVO.setTpdRecommendedCvrExceeded(false);
			if(insurCalculatorVO.getTtlTpdNpvAmt()!=null && insurCalculatorVO.getTotalSuperAmt()!=null){
				superContrAndExpenses=insurCalculatorVO.getTtlTpdNpvAmt().add(insurCalculatorVO.getTotalSuperAmt());
				if(superContrAndExpenses.compareTo(BigDecimal.ZERO)<0){
					superContrAndExpenses=BigDecimal.ZERO;
				}
			}
			if(insurCalculatorVO.getDebtVal()!=null && insurCalculatorVO.getDebtVal().compareTo(BigDecimal.ZERO)>0){
				debits=insurCalculatorVO.getDebtVal();
			}
			if(insurCalculatorVO.getTtlNursingNpvAmt()!=null){
				recommendedTpdCover=(superContrAndExpenses.add(debits)).add(insurCalculatorVO.getTtlNursingNpvAmt());
				recommendedTpdCover=QuoteHelper.createMultiplesByFactor(recommendedTpdCover,insurCalculatorRuleBO.getTpdMultFactor());
				maxRecommendedTpdCover=recommendedTpdCover;
				/*Check if recommendedCover is greater than max eligible limit or less than min limit. if true than reset to max/min eligible amount*/
				if(insurCalculatorRuleBO!=null && insurCalculatorRuleBO.getDeathMaxCover()!=null && recommendedTpdCover!=null){
					if(recommendedTpdCover.compareTo(insurCalculatorRuleBO.getTpdMaxCover())>0){
						recommendedTpdCover=insurCalculatorRuleBO.getTpdMaxCover().setScale(0);
						insurCalculatorVO.setTpdRecommendedCvrExceeded(true);
					}else if(recommendedTpdCover.compareTo(BigDecimal.ZERO)==0){
						recommendedTpdCover=BigDecimal.ZERO;
					}else if(recommendedTpdCover.compareTo(insurCalculatorRuleBO.getTpdMinCover())<0){
						recommendedTpdCover=insurCalculatorRuleBO.getTpdMinCover().setScale(0);
					}
				}
				insurCalculatorVO.setRecommendedTpdCover(maxRecommendedTpdCover);
				/*If the user changes the required cover then while doing the recalulcalation if the recommended is same as required
				 * then use the user entered amount as required cover else update the user enterede amount with 
				 * recomended cover*/
				if(insurCalculatorVO.getRecommendedTpdCoverOld()==null){
					insurCalculatorVO.setRequiredTpdCover(recommendedTpdCover);
					insurCalculatorVO.setRecommendedTpdCoverOld(maxRecommendedTpdCover);
				}else if(insurCalculatorVO.getRecommendedTpdCoverOld().compareTo(maxRecommendedTpdCover)!=0){
					insurCalculatorVO.setRequiredTpdCover(recommendedTpdCover);
					insurCalculatorVO.setRecommendedTpdCoverOld(maxRecommendedTpdCover);
				}else{
					insurCalculatorVO.setRecommendedTpdCoverOld(maxRecommendedTpdCover);
					if(insurCalculatorVO.isTpdRecommendedCvrExceeded() && insurCalculatorVO.getRequiredTpdCover().compareTo(recommendedTpdCover)<=0){
						insurCalculatorVO.setTpdRecommendedCvrExceeded(false);
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return insurCalculatorVO;
	}
	/**
	 * @param insurCalculatorRuleBO
	 * @param insurCalculatorVO
	 * @return
	 */
	public static InsurCalculatorVO calculateRecommendedDeathCover(InsurCalculatorRuleBO insurCalculatorRuleBO,InsurCalculatorVO insurCalculatorVO){
		log.info("calculateRecommendedDeathCover start");
		String METHOD_NAME = "calculateRecommendedDeathCover";
		BigDecimal recommendedDeathCover=null,maxRecommendedDeathCover=null;
		BigDecimal superContrAndExpenses=BigDecimal.ZERO;
		BigDecimal debits=BigDecimal.ZERO;
		try{
			insurCalculatorVO.setDeathRecommendedLessThanTpd(false);
			insurCalculatorVO.setDeathRecommendedCvrExceeded(false);
			if(insurCalculatorVO.getTtlDeathNpvAmt()!=null && insurCalculatorVO.getTotalSuperAmt()!=null){
				superContrAndExpenses=insurCalculatorVO.getTtlDeathNpvAmt().add(insurCalculatorVO.getTotalSuperAmt());
			}
			if(insurCalculatorVO.getDebtVal()!=null && insurCalculatorVO.getDebtVal().compareTo(BigDecimal.ZERO)>0){
				debits=insurCalculatorVO.getDebtVal();
			}
			if(insurCalculatorVO.getFuneralCost()!=null){
				recommendedDeathCover=(superContrAndExpenses.add(debits)).add(insurCalculatorVO.getFuneralCost());
			}
			/*SRKR: If the person is not living with spouse and dont have any dependents
			 * then recomendedDeathCover=funeralcost+debts*/
			if((insurCalculatorVO.isLivingWithSpouse() || insurCalculatorVO.getKidsList().size()>0) && insurCalculatorVO.getFuneralCost()!=null){
				recommendedDeathCover=(superContrAndExpenses.add(debits)).add(insurCalculatorVO.getFuneralCost());
			}else if(insurCalculatorVO.getFuneralCost()!=null && debits!=null){
				recommendedDeathCover=insurCalculatorVO.getFuneralCost().add(debits);
			}
			
			if(recommendedDeathCover!=null){
				recommendedDeathCover=QuoteHelper.createMultiplesByFactor(recommendedDeathCover,insurCalculatorRuleBO.getTpdMultFactor());
				maxRecommendedDeathCover=recommendedDeathCover;
				/*Check if recommendedCover is greater than max eligible limit or less than min limit. if true than reset to max/min eligible amount*/
				if(insurCalculatorRuleBO!=null && insurCalculatorRuleBO.getDeathMaxCover()!=null && recommendedDeathCover!=null){
					if(recommendedDeathCover.compareTo(insurCalculatorRuleBO.getDeathMaxCover())>0){
						recommendedDeathCover=insurCalculatorRuleBO.getDeathMaxCover().setScale(0);
						insurCalculatorVO.setDeathRecommendedCvrExceeded(true);
					}else if(recommendedDeathCover.compareTo(BigDecimal.ZERO)==0){
						recommendedDeathCover=BigDecimal.ZERO;
					}else if(recommendedDeathCover.compareTo(insurCalculatorRuleBO.getDeathMinCover())<0){
						recommendedDeathCover=insurCalculatorRuleBO.getDeathMinCover().setScale(0);
					}
				}
				insurCalculatorVO.setRecommendedDeathCover(maxRecommendedDeathCover);
				/*If the user changes the required cover then while doing the recalulcalation if the recommended is same as required
				 * then use the user entered amount as required cover else update the user enterede amount with 
				 * recommended cover*/
				if(insurCalculatorVO.getRecommendedDeathCoverOld()==null){
					insurCalculatorVO.setRecommendedDeathCoverOld(maxRecommendedDeathCover);
					insurCalculatorVO.setRequiredDeathCover(recommendedDeathCover);
				}else if(insurCalculatorVO.getRecommendedDeathCoverOld().compareTo(maxRecommendedDeathCover)!=0){
					insurCalculatorVO.setRequiredDeathCover(recommendedDeathCover);
					insurCalculatorVO.setRecommendedDeathCoverOld(maxRecommendedDeathCover);
				}else{
					insurCalculatorVO.setRecommendedDeathCoverOld(maxRecommendedDeathCover);
					if(insurCalculatorVO.isDeathRecommendedCvrExceeded() && insurCalculatorVO.getRequiredDeathCover().compareTo(recommendedDeathCover)<=0){
						insurCalculatorVO.setDeathRecommendedCvrExceeded(false);
					}
				}
				/*SRKR: CR004 The below logic for to display message advising the applicant that the TPD cover cannot be greater than Death. 
				 * The placement of this message is at the top of the screen,  
				 * is to be red in colour and screen should navigate the user to the top the same as error messages. */
				if(insurCalculatorVO.getRequiredTpdCover().compareTo(insurCalculatorVO.getRequiredDeathCover())>0){
					insurCalculatorVO.setRequiredDeathCover(insurCalculatorVO.getRequiredTpdCover());
					insurCalculatorVO.setDeathRecommendedLessThanTpd(true);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return insurCalculatorVO;
	}
	/**
	 * @param currFreq
	 * @param currAmount
	 * @return
	 */
	public BigDecimal convertToYearlyAmount(String currFreq,BigDecimal currAmount) {
		BigDecimal yearlyAmount=BigDecimal.ZERO;
		if(currFreq!=null && currAmount!=null){
			if(currFreq.equalsIgnoreCase(ANNUALLY)){
				yearlyAmount=currAmount;
			}else if(currFreq.equalsIgnoreCase(WEEKLY)){
				yearlyAmount=currAmount.multiply(new BigDecimal(52));
			}else if(currFreq.equalsIgnoreCase(QUARTERLY)){
				yearlyAmount=(currAmount.multiply(new BigDecimal(4)));
			}else if(currFreq.equalsIgnoreCase(MONTHLY)){
				yearlyAmount=(currAmount.multiply(new BigDecimal(12)));
			}else if(currFreq.equalsIgnoreCase(FORTNIGHTLY)){
				yearlyAmount=(currAmount.multiply(new BigDecimal(26)));
			}
		}
		return yearlyAmount;
	}
	public BigDecimal convertToMortageMonthlyAmount(String currFreq,BigDecimal currAmount) {
		BigDecimal monthlyAmount=BigDecimal.ZERO;
		if(currFreq!=null && currAmount!=null){
			if(currFreq.equalsIgnoreCase(ANNUALLY)){
				monthlyAmount=currAmount.divide(new BigDecimal(12));
			}else if(currFreq.equalsIgnoreCase(WEEKLY)){
				monthlyAmount=currAmount.multiply(new BigDecimal(4));
			}else if(currFreq.equalsIgnoreCase(QUARTERLY)){
				monthlyAmount=(currAmount.divide(new BigDecimal(3)));
			}else if(currFreq.equalsIgnoreCase(MONTHLY)){
				monthlyAmount=currAmount;
			}else if(currFreq.equalsIgnoreCase(FORTNIGHTLY)){
				monthlyAmount=(currAmount.multiply(new BigDecimal(2)));
			}
		}
		return monthlyAmount;
	}
}
