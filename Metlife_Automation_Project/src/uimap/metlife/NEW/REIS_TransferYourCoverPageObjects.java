package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class REIS_TransferYourCoverPageObjects extends PageObjectLibrary{
	
	public static final By  TransferYourCoverLink = getObjectIdentifaction("REIS.TYC.Link.id");
	public static final By  FifteenHoursYes = getObjectIdentifaction("REIS.TYC.FifteenHoursYes.id");
	
	public static final By  FifteenHoursNo = getObjectIdentifaction("REIS.TYC.FifteenHoursNo.id");
	public static final By  Industry = getObjectIdentifaction("REIS.TYC.Industry.id");
	public static final By  Occupation = getObjectIdentifaction("REIS.TYC.Occupation.id");
	public static final By  AnnualSalary = getObjectIdentifaction("REIS.TYC.AnnualSalary.id");
	public static final By  PreviousInsurer = getObjectIdentifaction("REIS.TYC.PreviousInsurer.id");
	public static final By  PolicyNumber = getObjectIdentifaction("REIS.TYC.PolicyNumber.id");
	public static final By  SPINNumber = getObjectIdentifaction("REIS.TYC.SPINNumber.id");
	public static final By  PremiumFrequency = getObjectIdentifaction("REIS.TYC.PremiumFrequency.id");
	public static final By  DeathAmount = getObjectIdentifaction("REIS.TYC.DeathAmount.id");
	public static final By  TPDAmount = getObjectIdentifaction("REIS.TYC.TPDAmount.id");
	public static final By  IPAmount = getObjectIdentifaction("REIS.TYC.IPAmount.id");
	public static final By  WaitingPeriod = getObjectIdentifaction("REIS.TYC.WaitingPeriod.id");
	public static final By  BenefitPeriod = getObjectIdentifaction("REIS.TYC.BenefitPeriod.id");
	public static final By  CalculateQuote = getObjectIdentifaction("REIS.TYC.CalculateQuote.xpath");
	public static final By  FetchDeathAmt = getObjectIdentifaction("REIS.TYC.FetchDeathAmt.id");
	public static final By  FetchTPDAmt = getObjectIdentifaction("REIS.TYC.FetchTPDAmt.id");
	public static final By  FetchIPAmt = getObjectIdentifaction("REIS.TYC.FetchIPAmt.id");
	public static final By  ContinueafterCoverdetails = getObjectIdentifaction("REIS.TYC.ContinueafterCoverdetails.xpath");
	public static final By  RestrictedillnessYes = getObjectIdentifaction("REIS.TYC.RestrictedillnessYes.id");
	public static final By  RestrictedillnessNo = getObjectIdentifaction("REIS.TYC.RestrictedillnessNo.id");
	public static final By  PriorClaimYes = getObjectIdentifaction("REIS.TYC.PriorClaimYes.id");
	public static final By  PriorClaimNo = getObjectIdentifaction("REIS.TYC.PriorClaimNo.id");
	public static final By  RestrictedFromWorkYes = getObjectIdentifaction("REIS.TYC.RestrictedFromWorkYes.id");
	public static final By  RestrictedFromWorkNo = getObjectIdentifaction("REIS.TYC.RestrictedFromWorkNo.id");
	public static final By  DiagnosedillnessYes = getObjectIdentifaction("REIS.TYC.DiagnosedillnessYes.id");
	public static final By  DiagnosedillnessNo = getObjectIdentifaction("REIS.TYC.DiagnosedillnessNo.id");
	public static final By  MedicalTreatmentYes = getObjectIdentifaction("REIS.TYC.MedicalTreatmentYes.id");
	public static final By  MedicalTreatmentNo = getObjectIdentifaction("REIS.TYC.MedicalTreatmentNo.id");
	public static final By  DeclinedApplicationYes = getObjectIdentifaction("REIS.TYC.DeclinedApplicationYes.id");
	public static final By  DeclinedApplicationNo = getObjectIdentifaction("REIS.TYC.DeclinedApplicationNo.id");
	public static final By  PremFundLoadingYes = getObjectIdentifaction("REIS.TYC.PremFundLoadingYes.id");
	public static final By  PremLoadDetails = getObjectIdentifaction("REIS.TYC.PremLoadDetails.id");
	public static final By  EnterPremLoading = getObjectIdentifaction("REIS.TYC.EnterPremLoading.id");
	public static final By  PremFundLoadingNo = getObjectIdentifaction("REIS.TYC.PremFundLoadingNo.id");
	public static final By  SubmitHealthPage = getObjectIdentifaction("REIS.TYC.SubmitHealthPage.xpath");

	public static final By  EvidenceofCoverchkbox = getObjectIdentifaction("REIS.TYC.EvidenceofCoverchkbox.id");	
	public static final By  Declarationchkbox = getObjectIdentifaction("REIS.TYC.Declarationchkbox.id");
	public static final By  PrivacyStatement = getObjectIdentifaction("REIS.TYC.PrivacyStatement.id");
	public static final By  GeneralConsent = getObjectIdentifaction("REIS.TYC.GeneralConsent.id");
	public static final By  PreferredContact = getObjectIdentifaction("REIS.TYC.PreferredContact.id");
	public static final By  PreferredTime = getObjectIdentifaction("REIS.TYC.PreferredTime.id");
	public static final By  ContactNumber = getObjectIdentifaction("REIS.TYC.ContactNumber.id");
	public static final By  EmailAddress = getObjectIdentifaction("REIS.TYC.EmailAddress.id");
	public static final By  SubmitDeclaration = getObjectIdentifaction("REIS.TYC.SubmitDeclaration.xpath");

	
}
