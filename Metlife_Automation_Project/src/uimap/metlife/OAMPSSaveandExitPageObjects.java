package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSSaveandExitPageObjects extends PageObjectLibrary{
	public static final By btnSave = getObjectIdentifaction("OAMPS.payment.saveandexit.btn.css");
	public static final By btnConfirm = getObjectIdentifaction("OAMPS.payment.confirm.btn.css");
	public static final By btnFinish= getObjectIdentifaction("OAMPS.payment.finish.btn.css");
}
