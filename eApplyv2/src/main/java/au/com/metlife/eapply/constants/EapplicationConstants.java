package au.com.metlife.eapply.constants;

public interface EapplicationConstants {
	
	
	String AUTHORIZATION = "Authorization";
	String THIRTY_DAYS = "30 Days";
	String FOURTYFIVE_DAYS = "45 Days";
	String SIXTY_DAYS = "60 Days";
	String NINETY_DAYS = "90 Days";
	String TWO_YEARS = "2 Years";
	String FIVE_YEARS = "5 Years";
	String AGE_SIXTYFIVE = "Age 65";
	String AGE_SIXTYSEVEN = "Age 67";
	String DAYS30 = "30DAYS";
	String DAYS45 = "45DAYS";
	String DAYS60 = "60DAYS";
	String DAYS90 = "90DAYS";
	String YEARS2 = "2YEARS";
	String YEARS5 = "5YEARS";
	String AGE65 = "AGE65";
	String AGE67 = "AGE67";
	String FUND_ID = "fund_id";	
	String FIRST_SUPER = "firstSuper";
	String NON_MV = "nonmv";
	String CLIENT_REF_NO = "clientRefNo";
	String HD_INPUTSTRING = "HDInputString";
    String STUB_RESPPAGE_ID = "stubRespPage_id";
    String SESSION_WEBSERV_RESP = "sess_web_serv_resp";
    String SESSION_SECURE_ID = "sess_secureId";
    String EAPPLY_CORPWEBSTUB_BASEPATH = "/eapply/CorpWebStub/";
    String INPUTDATA = "InputData";
    String ENCRYPTDATA = "EncryptData";
    String EMAIL_SMTP_PORT = "EMAIL_SMTP_PORT";
    String EMAIL_SUBJECT = "EMAIL_SUBJECT";
    String UW_DECISION = "UW_DECISION";
    String LASTNAME = "LASTNAME";
    String PDFLINK = "PDFLINK";
    String EMAIL_IND_SUBJECT_NAME = "EMAIL_IND_SUBJECT_NAME";
    String UW_DECISION_BLURB = "UW_DECISION_BLURB";
    String TITLE = "TITLE";
    String FIRSTNAME = "FIRSTNAME";
    String EMAIL_SMTP_HOST = "EMAIL_SMTP_HOST";
    String APPLICATIONNUMBER = "APPLICATIONNUMBER";
    String MEMBER_NO = "MEMBER_NO";
    String ADDRESS = "address";
    String FUNDCODE = "fundCode";
    String APPLICATION_JSON = "application/json";
    String CONTENT_TYPE = "Content-Type";
    String ACCEPT = "Accept";
    String ACCESS_TOKEN = "access_token";
    String GET = "GET";
    String POST = "POST";
    String UTF_EIGHT = "UTF-8";
    String CORPORATE = "corporate";
    
    
    
    
    /*Field that is hard coded in XML */
    String OCCRATING_XML = "<occRating>Standard</occRating>";
	String TYPE_XML = "<type>2</type>";
	String UNITS_XML = "<units>0</units>";
	String AMOUNT_XML = "<amount>0</amount>";
    
    /*Field that is being repeated in LogInfo */
    String INPUT_STRING_INFO = "inputString>> ";
    String BASEURL_INFO = "getB2bbaseurl inside>> ";
    String WEBSER_RET_INFO = "webserviceReturn>> ";
    String STUB_RESP_PAGENAME_INFO = "stubRespPageName>> ";
    String SECURE_ID_INFO = "secureId>> ";
    String MALFORMEDURL_ERR_INFO = "MalformedURL error";
    String PROTOCOL_ERR_INFO = "Protocol error";
    String MALFORMED_URL_INFO = "Input output error";
    String S_DATA="sdata";
    String INPUT_IDENTIFIER="InputIdentifier";
    String INPUT_DATA="InputData";
    String CHANNEL="Channel";
    String GEN_KEY="432rerr32rhWR#432g";
    String EMAIL="email";
    String EVIEW="eview";
    //MTAA Change Starts
    String MTAA_SUPER = "mtaaLogin";
    String FIRS_SUPER = "firsLogin";
    String MTAA = "mtaa";
    String FUNDNAME_MTAA = "MTAA";
    String FUNDNAME_GUIL = "GUIL";
    String GUIL_SUPER = "guilLogin";
    String FUNDNAME_FIRS = "FIRS";
    String GUIL = "guil";
    String OCCRATING_XML_MTAA = "<occRating>General</occRating>";
    String OCCRATING_XML_GUIL = "<occRating>Standard</occRating>";
    String DOMAINNAME = "domainname";
    String DOMAINURL = "domainurl";
    //MTAA Change Ends
}
