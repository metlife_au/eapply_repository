package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIAcceptedPageObjects extends PageObjectLibrary{
		
	public static final By btnfinish = getObjectIdentifaction("ALI.accepted.submit.btn.css");
}
