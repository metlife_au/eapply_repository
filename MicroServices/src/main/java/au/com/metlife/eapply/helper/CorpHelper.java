/**
 * 
 */
package au.com.metlife.eapply.helper;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import au.com.metlife.eapply.business.entity.BrokerDet;
import au.com.metlife.eapply.business.entity.CorpPartner;
import au.com.metlife.eapply.business.entity.DeathCover;
import au.com.metlife.eapply.business.entity.EmployerDet;
import au.com.metlife.eapply.business.entity.FundCategory;
import au.com.metlife.eapply.business.entity.IPCover;
import au.com.metlife.eapply.business.entity.TPDCover;
import au.com.metlife.eapply.common.ResponseLink;
import au.com.metlife.eapply.constants.EtoolkitConstants;
import au.com.metlife.eapply.corporate.business.CorpEntityRequest;
import au.com.metlife.eapply.corporate.business.CorpEntityResponse;
import au.com.metlife.eapply.mapper.CorpServiceMapper;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;

/**
 * @author 599063
 *
 */
public class CorpHelper{
	private CorpHelper(){
		
	}

	public static CorpEntityResponse processFundInformationResponse(CorporateFund corpFund) {
		CorpEntityResponse response = new CorpEntityResponse();

		if (null != corpFund) {
			response.setPartner(CorpServiceMapper.processMemberValues(corpFund));
			response.setLinks(new ResponseLink[0]);
		}

		return response;
	}

	public static CorporateFund processFundInformationRequest(CorpEntityRequest request, String fundCode,
			String userId) {
		
		
		CorporateFund corporateFund = new CorporateFund();
		
		CorpPartner fund = request.getPartner();
		
		/*
		 * 1. Setting general values for corporate fund in CorporateFund entity
		 */
		processGeneralFundInfo(corporateFund, fund, fundCode);
		corporateFund.setStatus(EtoolkitConstants.STATUS_SUBMITTED);
		/*
		 * 2. Setting Employer details for corporate fund in CorporateFund entity
		 */
		processEmployerDetails(corporateFund, fund);
		
		/*
		 * 3. Setting Broker details for corporate fund in CorporateFund entity
		 */
		processBrokerDetails(corporateFund, fund);
		
		/*
		 * 4. Setting Fund Category details for corporate fund in CorporateFundCat entity
		 */
		List<CorporateFundCat> fundCategories = processFundCategoryDetails(corporateFund, fund, userId);
		corporateFund.setCorpFundCategories(fundCategories);
		

		/*
		 * 5. Audit information
		 */
		CorpServiceMapper.getCorpFundAudits(corporateFund, userId);

		return corporateFund;
	}

	
	

	private static List<CorporateFundCat> processFundCategoryDetails(CorporateFund corporateFund, CorpPartner fund, String userId) {
		List<CorporateFundCat> fundCategories = new ArrayList<>();
		FundCategory[] fundInputs = fund.getFundCat();
		
		if(null!=fundInputs){
			List<FundCategory> entityFundCats = Arrays.asList(fundInputs);
			fundCategories =  entityFundCats.stream().map(i -> setFundCategories(i, userId, corporateFund)).collect(Collectors.toList());
		}
		
		
		return fundCategories;
	}

	private static CorporateFundCat setFundCategories(FundCategory fundCat, String userId, CorporateFund corporateFund) {
		CorporateFundCat corpFundCat = new CorporateFundCat();
		CorpServiceMapper.getFundCategoryAudits(corpFundCat,userId);
		corpFundCat.setCorporateFundId(corporateFund);
		processCatGeneralInfo(corpFundCat, fundCat);
		if("Y".equalsIgnoreCase(fundCat.getTpdReqd())) {
		processCatTPDCover(corpFundCat, fundCat);
		}
		if("Y".equalsIgnoreCase(fundCat.getDeathReqd())) {
		processCatDeathCover(corpFundCat, fundCat);
		}
		if("Y".equalsIgnoreCase(fundCat.getIpReqd())) {
		processCatIPCover(corpFundCat, fundCat);
		}
		
		return corpFundCat;
	}

	private static void processCatIPCover(CorporateFundCat corpFundCat, FundCategory fundCat) {
		IPCover ipCover = fundCat.getIpCover();
		
		corpFundCat.setIpMinAge(Integer.valueOf(ipCover.getIpMinAge()));
		corpFundCat.setIpMaxAge(Integer.valueOf(ipCover.getIpMaxAge()));
		corpFundCat.setIpMinSumIns(ipCover.getIpMinSumIns());
		corpFundCat.setIpMaxSumIns(ipCover.getIpMaxSumIns());
		corpFundCat.setIpSumInsType(ipCover.getIpSumInsType());
		corpFundCat.setIpFormula(ipCover.getIpFormula());
		corpFundCat.setIpAal(ipCover.getIpAal());
		corpFundCat.setIpWp(ipCover.getIpWp());
		corpFundCat.setIpBp(ipCover.getIpBp());
		corpFundCat.setIpSciPolicyNum(ipCover.getIpSciPolicyNum());
	}

	private static void processCatDeathCover(CorporateFundCat corpFundCat, FundCategory fundCat) {
		DeathCover deathCover = fundCat.getDeathCover();
		corpFundCat.setDeathMinAge(Integer.valueOf(deathCover.getDeathMinAge()));
		corpFundCat.setDeathMaxAge(Integer.valueOf(deathCover.getDeathMaxAge()));
		corpFundCat.setDeathMinSumIns(deathCover.getDeathMinSumIns());
		corpFundCat.setDeathMaxSumIns(deathCover.getDeathMaxSumIns());
		corpFundCat.setDeathAal(deathCover.getDeathAal());
		
	}

	private static void processCatTPDCover(CorporateFundCat corpFundCat, FundCategory fundCat) {
		TPDCover tpdCover = fundCat.getTpdCover();
		corpFundCat.setTpdMinAge(Integer.valueOf(tpdCover.getTpdMinAge()));
		corpFundCat.setTpdMaxAge(Integer.valueOf(tpdCover.getTpdMaxAge()));
		corpFundCat.setTpdMaxSumIns(tpdCover.getTpdMaxSumIns());
		corpFundCat.setTpdMinSumIns(tpdCover.getTpdMinSumIns());
		corpFundCat.setTpdAal(tpdCover.getTpdAal());
		
	}

	private static void processCatGeneralInfo(CorporateFundCat corpFundCat, FundCategory fundCat) {
		
		
		Date date = new Date();
		corpFundCat.setCategory(Integer.valueOf(fundCat.getCategory()));
		corpFundCat.setDeathRequired(fundCat.getDeathReqd().charAt(0));
		corpFundCat.setTpdRequired(fundCat.getTpdReqd().charAt(0));
		corpFundCat.setIpRequired(fundCat.getIpReqd().charAt(0));
		corpFundCat.setGlPolicyNum(fundCat.getGlPolicyNum());
		corpFundCat.setCreatedDate(new Timestamp(date.getTime()));
		
	}

	/**
	 * Setting general values for corporate fund in CorporateFund entity
	 * @param corporateFund
	 * @param fund
	 * @param fundCode 
	 */
	private static void processGeneralFundInfo(CorporateFund corporateFund, CorpPartner fund, String fundCode) {
		

		Date date = new Date();
		corporateFund.setFundCode(fund.getFundCode());
		corporateFund.setAdminCode(fund.getAdminCode());
		corporateFund.setPartnerName(fund.getPartnerName());
		//corporateFund.setStatus(fund.getStatus());
		corporateFund.setCreatedDate(new Timestamp(date.getTime()));
		
	}

	/**
	 * Setting Employer details for corporate fund in CorporateFund entity
	 * @param corporateFund
	 * @param fund
	 */
	private static void processEmployerDetails(CorporateFund corporateFund, CorpPartner fund) {
		
		EmployerDet emp = fund.getEmployer();
		if(null!=emp) {
		if(!CorporateHelper.isNullOrEmpty(emp.getEmployerCnctNum())) {
			corporateFund.setEmployerContactNum(emp.getEmployerCnctNum());
		}
		if(!CorporateHelper.isNullOrEmpty(emp.getEmployerEmail())) {
			corporateFund.setEmployerEmail(emp.getEmployerEmail());
		}
		if(!CorporateHelper.isNullOrEmpty(emp.getEmployerKeyCnctName())) {
			corporateFund.setEmployerKeyContactNm(emp.getEmployerKeyCnctName());
		}
		}
		
	}
	
	private static void processBrokerDetails(CorporateFund corporateFund, CorpPartner fund) {
		
		BrokerDet broker = fund.getBroker();
		corporateFund.setBrokercontactNum(broker.getBrokerCnctNum()!=null?broker.getBrokerCnctNum():null);
		corporateFund.setBrokerEmail(broker.getBrokerEmail());
		corporateFund.setBrokerName(broker.getBrokerName());
		
	}

	public static CorporateFund processFundInformationRequestForUpdate(CorpEntityRequest request, CorporateFund corpFund,
			String userId) {
		
		CorpPartner fund = request.getPartner();
		String fundCode = fund.getFundCode();
		
		/*
		 * 1. Clear and process general info for partner
		 */
		CorpServiceMapper.clearGeneralFundInfo(corpFund);
		processGeneralFundInfo(corpFund, fund, fundCode);
		corpFund.setStatus(fund.getStatus());
		
		/*
		 * 2. Clear and process Employer details for corporate fund in CorporateFund entity
		 */
		CorpServiceMapper.clearEmployerDetails(corpFund);
		processEmployerDetails(corpFund, fund);
		
		/*
		 * 3. Clear and process Broker details for corporate fund in CorporateFund entity
		 */
		CorpServiceMapper.clearBrokerDetails(corpFund);
		processBrokerDetails(corpFund, fund);
		/*
		 * 4. Clear and process Fund Category details for corporate fund in CorporateFundCat entity
		 */
		List<CorporateFundCat> fundCategories  = corpFund.getCorpFundCategories();
		processFundCategoryDetailsForUpdate(corpFund, fund, fundCategories, userId);
		/*
		 * 5. Clear and process Audit information
		 */
		CorpServiceMapper.getCorpFundAudits(corpFund, userId);
		
		return corpFund;
	}

	private static void processFundCategoryDetailsForUpdate(CorporateFund corpFund, CorpPartner fund,
			List<CorporateFundCat> corporateFundCategories, String userId) {

		FundCategory[] fundCategories = fund.getFundCat();
		List<CorporateFundCat> categoriesToBeRemoved = new ArrayList<>();

		for (CorporateFundCat category : corporateFundCategories) {
			FundCategory newCategory = findFundCategory(category, fundCategories);
			if (null != newCategory) {
				CorpServiceMapper.setFundCategoriesForUpdate(newCategory, category, userId);
			} else {
				categoriesToBeRemoved.add(category);
			}
		}
		// Remove the Categories removed from client
		for (CorporateFundCat category : categoriesToBeRemoved) {
			corporateFundCategories.remove(category);
		}
		// Add new category
		if (null != fundCategories) {
			for (FundCategory fundCat : fundCategories) {
				if (fundCat.getId() == null || fundCat.getId().isEmpty()) {
					CorporateFundCat corpFundCat = setFundCategories(fundCat, userId, corpFund);
					corporateFundCategories.add(corpFundCat);
				}
			}
		}
		corpFund.setCorpFundCategories(corporateFundCategories);
	}

	private static FundCategory findFundCategory(CorporateFundCat category, FundCategory[] fundCategories) {
		
			if(null!=fundCategories){
				for(FundCategory cat: fundCategories){
					if(cat!=null && category.getId().toString().equals(cat.getId())){
						return cat;
					}
				}
			}
		
		return null;
	}

	public static CorpEntityResponse processNewFundDetails(String fundCode, String fundName) {
		CorpEntityResponse response = new CorpEntityResponse();
		CorpPartner corporateFund = new CorpPartner();
		corporateFund.setFundCode(fundCode);
		corporateFund.setPartnerName(fundName);
		response.setPartner(corporateFund);
		return response;
	}
	

}
