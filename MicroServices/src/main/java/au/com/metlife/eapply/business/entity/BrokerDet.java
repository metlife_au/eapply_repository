package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

public class BrokerDet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7310081610116187859L;
	private String brokerName;
	private String brokerEmail;
	private String brokerCnctNum;
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getBrokerEmail() {
		return brokerEmail;
	}
	public void setBrokerEmail(String brokerEmail) {
		this.brokerEmail = brokerEmail;
	}
	public String getBrokerCnctNum() {
		return brokerCnctNum;
	}
	public void setBrokerCnctNum(String brokerCnctNum) {
		this.brokerCnctNum = brokerCnctNum;
	}

	
}
