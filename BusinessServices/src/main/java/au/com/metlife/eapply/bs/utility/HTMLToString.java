package au.com.metlife.eapply.bs.utility;

public class HTMLToString {
    

	
    public static String[] dictionary = { "</P>", "</p>", "<BR>", "<br>",
        "<LI>", "<li>", "</UL>", "</ul>", "<P>", "<p>", "<U>", "<B>", "</U>",
        "</B>", "<u>", "<b>", "</u>", "</b>", "</LI>", "</li>", "<U><STRONG>",
        "<u><strong>", "</STRONG></U>", "</strong></u>", "</STRONG>",
        "</strong", "<STRONG>", "<strong>", "<UL>", "<ul>", "<A", "<a",
        "<span", "<SPAN", "</a>", "</A>", "</span>", "</SPAN>" , "<br />", 
        "</div>", "/DIV", "<p class=\"normal\">", "<ul class=\"list\">", "<P class=\"normal\">",
        "<UL class=\"list\">", "<div class=\"desc\">", "<DIV class=\"desc\">", "<ul class=\"list\">", "<UL class=\"list\">"};
    
    public static String convertHTMLToString(String source) {
    	if(source!=null) {
	        boolean flag = false;
	        	        
	        if (source.contains( "<P>")) {
	            source = source.replaceAll( "<P>", "");
	        }
	        if (source.contains( "<p>")) {
	            source = source.replaceAll( "<p>", "");
	        }
	        if (source.contains( "</P>")) {
	            source = source.replaceAll( "</P>", "\n");
	        }	
	        if (source.contains( "</p>")) {
	            source = source.replaceAll( "</p>", "\n");
	        }
	        if (source.contains("<P style=\"padding:0 0 0 20px;\">")) {
	            source = source.replaceAll("<P style=\"padding:0 0 0 20px;\">", "");
	        }
	        if (source.contains( "<BR><BR>")) {
	            source = source.replaceAll("<BR><BR>", "\n\n");
	        }
	        if (source.contains( "<BR>")) {
	            source = source.replaceAll( "<BR>", "\n");
	        }
	        if (source.contains( "<BR/>")) {
	            source = source.replaceAll( "<BR/>", "");
	        }
	        if (source.contains( "<br/>")) {
	            source = source.replaceAll( "<br/>", "");
	        }
	        if (source.contains( "<br>")) {
	            source = source.replaceAll( "<br>", "\n");
	        }
	        if (source.contains( "<br><br>")) {
	            source = source.replaceAll("<br><br>", "\n\n");
	        }
	        if(source.contains("<span style='font-weight:bold;'>")){
	        	 source = source.replaceAll( "<span style='font-weight:bold;'>", "");
	        }
	        if(source.contains("</span>")){
	        	 source = source.replaceAll( "</span>", "");
	        } 	     
	        if (source.contains( "</SPAN>")) {
	            source = source.replaceAll( "</SPAN>", "");
	        }	        
	        if(source.contains("<style='font-weight:bold;'>")){
	        	 source = source.replaceAll( "<style='font-weight:bold;'>", "");
	        } 

	        if (source.contains( "<SPAN ")) {
	            source = source.replaceAll( "<SPAN style='font-weight:bold;'>", "");
	        }
	        
	        if (source.contains( "<span ")) {
	            source = source.replaceAll( "<span style='font-weight:bold;'>", "");
	        }
	        
	        if (source.contains( "<SPAN ")) {
	            source = source.replaceAll( "<SPAN style='color:blue;'>", "");
	        }
	        
	        if (source.contains( "<span ")) {
	            source = source.replaceAll( "<span style='color:blue;'>", "");
	        }
	        
	        if (source.contains( "<p ")) {
	            source = source.replaceAll( "<p style='padding:0 0 0 20px;line-height:17px;'>", "");
	        }
	        
	        if (source.contains( "<ul ")) {
	            source = source.replaceAll( "<ul style='padding:0 0 0 35px;list-style-type:disc;'>", "");
	        }
	      
	        if (source.contains( "<LI>")) {
	            source = source.replaceAll( "<LI>", "\t-\t");
	        }
	        if (source.contains( "<li>")) {
	            source = source.replaceAll( "<li>", "\t-\t");
	        }	
	        if (source.contains( "</LI>")) {
	            source = source.replace( "</LI>", "\n");
	        }
	        if (source.contains( "</li>")) {
	            source = source.replace( "</li>", "\n");
	        }
	        if (source.contains( "<UL>")) {
	            source = source.replaceAll( "<UL>", "\t");
	        }
	        if (source.contains( "<ul>")) {
	            source = source.replaceAll( "<ul>", "\t");
	        }
	        if (source.contains( "</UL>")) {
	            source = source.replaceAll( "</UL>", "\n\r");
	        }
	        if (source.contains( "</ul>")) {
	            source = source.replaceAll( "</ul>", "\n\r");
	        }       	      	       
	       if (source.contains("<UL style=\"padding:0 0 0 20px;\">")) {
	            source = source.replaceAll("<UL style=\"padding:0 0 0 20px;\">", "");
	        }
	        if (source.contains( "<U>")) {
	            source = source.replaceAll( "<U>", "");
	        }	       
	        if (source.contains( "</U>")) {
	            source = source.replaceAll( "</U>", "");
	        }
	        if (source.contains( "</B>")) {
	            source = source.replaceAll( "</B>", "");
	        }
	        if (source.contains( "<b>")) {
	            source = source.replaceAll( "<b>", "");
	        }
	        if (source.contains( "</b>")) {
	            source = source.replaceAll( "</b>", "");
	        }
	        if (source.contains( "<u>")) {
	            source = source.replaceAll( "<u>", "");
	        }	        
	        if (source.contains( "</u>")) {
	            source = source.replaceAll( "</u>", "");
	        }	     
	        if (source.contains( "<U><STRONG>")) {
	            source = source.replaceAll( "<U><STRONG>", "");
	        }
	        if (source.contains( "<u><strong>")) {
	            source = source.replaceAll( "<u><strong>", "");
	        }
	        if (source.contains( "</STRONG></U>")) {
	            source = source.replaceAll( "</STRONG></U>", "");
	        }
	        if (source.contains( "</strong></u>")) {
	            source = source.replaceAll( "</strong></u>", "");
	        }
	        if (source.contains( "</STRONG>")) {
	            source = source.replaceAll( "</STRONG>", "");
	        }
	        if (source.contains( "</strong")) {
	            source = source.replaceAll( "</strong>", "");
	        }
	        if (source.contains( "<STRONG>")) {
	            source = source.replaceAll( "<STRONG>", "");
	        }
	        if (source.contains( "<strong>")) {
	            source = source.replaceAll( "<strong>", "");
	        }
	        if (source.contains("<ul style=\"list-style-type:disc;padding-left:20px;\">")) {
	            source = source.replaceAll( "<ul style=\"list-style-type:disc;padding-left:20px;\">", "\t");
	        }
	        if (source.contains("<UL style=\"padding:0 0 0 35px;list-style-type:disc\">")) {
	            source = source.replaceAll( "<UL style=\"padding:0 0 0 35px;list-style-type:disc\">", "\t");
	        }
	        
	        if(source.contains("<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc\">")){
	        	 source = source.replaceAll( "<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc\">", "\t");
	        }
	        
	        if(source.contains("<P style=\"padding:0 0 0 20px;line-height:17px;\">")){
	        	 source = source.replaceAll( "<P style=\"padding:0 0 0 20px;line-height:17px;\">", "\t");
	        }
	        if(source.contains("<p style=\"padding-bottom:5px;line-height:20px;\">")){
	        	 source = source.replaceAll( "<p style=\"padding-bottom:5px;line-height:20px;\">", "");
	        }
	        if(source.contains("<ul style=\"padding:0px 0px 10px 25px;list-style-type:disc;line-height:25px \">")){
	        	 source = source.replaceAll( "<ul style=\"padding:0px 0px 10px 25px;list-style-type:disc;line-height:25px \">", "\t");
	        }
	        
	        if(source.contains("<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc;\">")){
	        	 source = source.replaceAll( "<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc;\">", "\t");
	        }
	        if(source.contains("<UL style=\"padding:0 0 0 35px;line-height:15px;list-style-type:disc;\">")){
	        	 source = source.replaceAll( "<UL style=\"padding:0 0 0 35px;line-height:15px;list-style-type:disc;\">", "\t");
	        }
	        if(source.contains("<UL style=\"padding:0 0 0 35px;list-style-type:disc;\">")){
	        	 source = source.replaceAll( "<UL style=\"padding:0 0 0 35px;list-style-type:disc;\">", "\t");
	        }
	        
	        source = source.replaceAll("<A", "<a");
	        source = source.replaceAll("</A>", "</a>");
	        
	     
	        if (source.contains( "<A")) {
	            String tempS = source.substring( source.indexOf( "<A"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<A"), source.indexOf( "<A")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        
	        if (source.contains( "<a")) {
	            
	            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
 
	        if (source.contains( "<a")) {
     
			     String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
			     String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
			             + tempS.indexOf( ">") + 1);
			     source = source.replace( tempA, "");
			     tempA = null;
			     tempS = null;
			 }
 
			 if (source.contains( "<a")) {
			     
			     String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
			     String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
			             + tempS.indexOf( ">") + 1);
			     source = source.replace( tempA, "");
			     tempA = null;
			     tempS = null;
			 }
	        if (source.contains( "<a")) {
	            
	            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        
	        if (source.contains( "<a")) {
	            
	            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        if (source.contains( "<span")) {
	            String tempS = source.substring( source.indexOf( "<span"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<span"), source.indexOf( "<span")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replaceAll( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        if (source.contains( "<SPAN")) {
	            String tempS = source.substring( source.indexOf( "<SPAN"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<SPAN"), source.indexOf( "<SPAN")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replaceAll( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        if (source.contains( "</a>")) {
	            source = source.replaceAll( "</a>", "");
	        }
	        if (source.contains( "</A>")) {
	            source = source.replaceAll( "</A>", "");
	        }  
	        
	       /* for (int i = 0; i < dictionary.length; i++) {
	            
	            if (( null != dictionary[i]) && source.contains( dictionary[i])) {
	                flag = true;
	                break;
	            }
	        }
	        if (flag) {
	            
	            source = convertHTMLToString( source);
	        }*/
    	}
        
        return source;
    }
    
    public static String convertHTMLToStringForInsCalc(String source) {
    	if(source!=null) {
	        if (source.contains( "<br />")) {
	            source = source.replaceAll( "<br />", "\n");
	        }
	        if (source.contains( "<div class=\"desc\">")) {
	            source = source.replaceAll( "<div class=\"desc\">", "");
	        }
	        if (source.contains( "<p class=\"normal\">")) {
	            source = source.replaceAll( "<p class=\"normal\">", "\n");
	        }
	        if (source.contains( "<ul class=\"list\">")) {
	            source = source.replaceAll( "<ul class=\"list\">", "");
	        }
	        if (source.contains( "&#38;")) {
	            source = source.replaceAll( "&#38;", "&");
	        }
	        if (source.contains( "<div>")) {
	            source = source.replaceAll( "<div>", "");
	        }
	        if (source.contains( "</div>")) {
	            source = source.replaceAll( "</div>", "");
	        }
	        if (source.contains( "<P>")) {
	            source = source.replaceAll( "<P>", "");
	        }
	        if (source.contains( "<p>")) {
	            source = source.replaceAll( "<p>", "");
	        }
	        if (source.contains( "</P>")) {
	            source = source.replaceAll( "</P>", "\n");
	        }	
	        if (source.contains( "</p>")) {
	            source = source.replaceAll( "</p>", "\n");
	        }
	        if (source.contains("<P style=\"padding:0 0 0 20px;\">")) {
	            source = source.replaceAll("<P style=\"padding:0 0 0 20px;\">", "");
	        }
	        if (source.contains( "<BR><BR>")) {
	            source = source.replaceAll("<BR><BR>", "\n\n");
	        }
	        if (source.contains( "<BR>")) {
	            source = source.replaceAll( "<BR>", "\n");
	        }
	        if (source.contains( "<BR/>")) {
	            source = source.replaceAll( "<BR/>", "");
	        }
	        if (source.contains( "<br/>")) {
	            source = source.replaceAll( "<br/>", "");
	        }
	        if (source.contains( "<br>")) {
	            source = source.replaceAll( "<br>", "\n");
	        }
	        if (source.contains( "<br><br>")) {
	            source = source.replaceAll("<br><br>", "\n\n");
	        }
	        if(source.contains("<span style='font-weight:bold;'>")){
	        	 source = source.replaceAll( "<span style='font-weight:bold;'>", "");
	        }
	        if(source.contains("</span>")){
	        	 source = source.replaceAll( "</span>", "");
	        } 	     
	        if (source.contains( "</SPAN>")) {
	            source = source.replaceAll( "</SPAN>", "");
	        }	        
	        if(source.contains("<style='font-weight:bold;'>")){
	        	 source = source.replaceAll( "<style='font-weight:bold;'>", "");
	        }         
	      
	        if (source.contains( "<LI>")) {
	            source = source.replaceAll( "<LI>", "\n\t-\t");
	        }
	        if (source.contains( "<li>")) {
	            source = source.replaceAll( "<li>", "\n\t-\t");
	        }	
	        if (source.contains( "</LI>")) {
	            source = source.replace( "</LI>", "");
	        }
	        if (source.contains( "</li>")) {
	            source = source.replace( "</li>", "\n");
	        }
	        if (source.contains( "<UL>")) {
	            source = source.replaceAll( "<UL>", "\t");
	        }
	        if (source.contains( "<ul>")) {
	            source = source.replaceAll( "<ul>", "\t");
	        }
	        if (source.contains( "</UL>")) {
	            source = source.replaceAll( "</UL>", "\n\r");
	        }
	        if (source.contains( "</ul>")) {
	            source = source.replaceAll( "</ul>", "\n\r");
	        }       	      	       
	       if (source.contains("<UL style=\"padding:0 0 0 20px;\">")) {
	            source = source.replaceAll("<UL style=\"padding:0 0 0 20px;\">", "");
	        }
	        if (source.contains( "<U>")) {
	            source = source.replaceAll( "<U>", "");
	        }	       
	        if (source.contains( "</U>")) {
	            source = source.replaceAll( "</U>", "");
	        }
	        if (source.contains( "</B>")) {
	            source = source.replaceAll( "</B>", "");
	        }
	        if (source.contains( "<b>")) {
	            source = source.replaceAll( "<b>", "");
	        }
	        if (source.contains( "</b>")) {
	            source = source.replaceAll( "</b>", "");
	        }
	        if (source.contains( "<u>")) {
	            source = source.replaceAll( "<u>", "");
	        }	        
	        if (source.contains( "</u>")) {
	            source = source.replaceAll( "</u>", "");
	        }	     
	        if (source.contains( "<U><STRONG>")) {
	            source = source.replaceAll( "<U><STRONG>", "");
	        }
	        if (source.contains( "<u><strong>")) {
	            source = source.replaceAll( "<u><strong>", "");
	        }
	        if (source.contains( "</STRONG></U>")) {
	            source = source.replaceAll( "</STRONG></U>", "");
	        }
	        if (source.contains( "</strong></u>")) {
	            source = source.replaceAll( "</strong></u>", "");
	        }
	        if (source.contains( "</STRONG>")) {
	            source = source.replaceAll( "</STRONG>", "");
	        }
	        if (source.contains( "</strong")) {
	            source = source.replaceAll( "</strong>", "");
	        }
	        if (source.contains( "<STRONG>")) {
	            source = source.replaceAll( "<STRONG>", "");
	        }
	        if (source.contains( "<strong>")) {
	            source = source.replaceAll( "<strong>", "");
	        }
	        if (source.contains("<ul style=\"list-style-type:disc;padding-left:20px;\">")) {
	            source = source.replaceAll( "<ul style=\"list-style-type:disc;padding-left:20px;\">", "\t");
	        }
	        if (source.contains("<UL style=\"padding:0 0 0 35px;list-style-type:disc\">")) {
	            source = source.replaceAll( "<UL style=\"padding:0 0 0 35px;list-style-type:disc\">", "\t");
	        }
	        
	        if(source.contains("<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc\">")){
	        	 source = source.replaceAll( "<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc\">", "\t");
	        }
	        
	        source = source.replaceAll("<A", "<a");
	        source = source.replaceAll("</A>", "</a>");
	        
	     
	        if (source.contains( "<A")) {
	            String tempS = source.substring( source.indexOf( "<A"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<A"), source.indexOf( "<A")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        
	        if (source.contains( "<a")) {
	            
	            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
 
	        if (source.contains( "<a")) {
     
			     String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
			     String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
			             + tempS.indexOf( ">") + 1);
			     source = source.replace( tempA, "");
			     tempA = null;
			     tempS = null;
			 }
 
			 if (source.contains( "<a")) {
			     
			     String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
			     String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
			             + tempS.indexOf( ">") + 1);
			     source = source.replace( tempA, "");
			     tempA = null;
			     tempS = null;
			 }
	        if (source.contains( "<a")) {
	            
	            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        
	        if (source.contains( "<a")) {
	            
	            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replace( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        if (source.contains( "<span")) {
	            String tempS = source.substring( source.indexOf( "<span"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<span"), source.indexOf( "<span")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replaceAll( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        if (source.contains( "<SPAN")) {
	            String tempS = source.substring( source.indexOf( "<SPAN"), source.length() - 1);
	            String tempA = source.substring( source.indexOf( "<SPAN"), source.indexOf( "<SPAN")
	                    + tempS.indexOf( ">") + 1);
	            source = source.replaceAll( tempA, "");
	            tempA = null;
	            tempS = null;
	        }
	        if (source.contains( "</a>")) {
	            source = source.replaceAll( "</a>", "");
	        }
	        if (source.contains( "</A>")) {
	            source = source.replaceAll( "</A>", "");
	        }  
	        
	       /* for (int i = 0; i < dictionary.length; i++) {
	            
	            if (( null != dictionary[i]) && source.contains( dictionary[i])) {
	                flag = true;
	                break;
	            }
	        }
	        if (flag) {
	            
	            source = convertHTMLToString( source);
	        }*/       
    	}        
        return source;
    }
}
