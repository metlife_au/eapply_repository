/*package VicsSuper_Objects;

public class LifeEventObject {

}*/
package vicsSuper_Objects;

import org.openqa.selenium.By;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class LifeEventObject extends ReusableLibrary {

	public LifeEventObject(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}


	// ************************************************************************************************************************
	// -----------------------------------------Personal Details
	// ************************************************************************************************************************
	public static By obj_PersonalDetails_dutyOfDisclosure = By.xpath(".//input[@id='dodlabelCheck']/parent::*");
	public static By obj_PersonalDetails_privacyStatement = By.xpath(".//input[@id='privacylabelCheck']/parent::*");
	// ContactDetails
	public static By obj_PersonalDetails_email = By.name("contactDetailsLifeEventEmail");
	public static By obj_PersonalDetails_contactType = By.xpath("//select[@ng-model='preferredContactType']");
	public static By obj_PersonalDetails_phNO = By.name("contactDetailsLifeEventPhone");
	public static By obj_PersonalDetails_Morning = By.xpath("(.//input[@name='contactDetailsLifeEventPrefTime']/parent::*)[1]");
	public static By obj_PersonalDetails_Afternoon = By.xpath("(.//input[@name='contactDetailsLifeEventPrefTime']/parent::*)[2]");
	public static By obj_PersonalDetails_Male = By.xpath("(.//input[@name='gender']/parent::*)[1]");
	public static By obj_PersonalDetails_Female = By.xpath("(.//input[@name='gender']/parent::*)[2]");
	public static By obj_Continue_ContactDetail = By.xpath("//*[@ng-click='lifeEventFormSubmit(contactDetailsLifeEventForm);']");

	// OccupationDetails
	public static By obj_PersonalDetails_FourteenHrsYes = By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*");
	public static By obj_PersonalDetails_FourteenHrsNo = By.xpath("//input[@name='fifteenHrsQuestion']/parent::*");
	public static By obj_PersonalDetails_CitizenYes = By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[1]");
	public static By obj_PersonalDetails_CitizenNo = By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[2]");
	public static By obj_PersonalDetails_AddQuest1aYes = By.xpath("(//input[@name='occRating1a']/parent::*)[1]");
	public static By obj_PersonalDetails_AddQuest1aNo = By.xpath("(//input[@name='occRating1a']/parent::*)[2]");
	public static By obj_PersonalDetails_AddQuest1bYes = By.xpath("(//input[@name='occRating1b']/parent::*)[1]");
	public static By obj_PersonalDetails_AddQuest1bNo = By.xpath("(//input[@name='occRating1b']/parent::*)[2]");
	public static By obj_PersonalDetails_AddQuest2Yes = By.xpath("(//input[@name='occgovrating2']/parent::*)[1]");
	public static By obj_PersonalDetails_AddQuest2No = By.xpath("(//input[@name='occgovrating2']/parent::*)[2]");
	public static By obj_PersonalDetails_AnnualSal = By.id("annualSalCCId");
	public static By obj_Continue_Occupation = By.xpath("//*[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']");

	// LifeEvent
	public static By obj_LifeEvent = By.name("event");
	public static By obj_DateOfEvent = By.name("eventDate");
	public static By obj_DateOfEventHeader = By.xpath("//label[contains(text(),' What is the date of the life event?')]");
	public static By obj_IncLifeEventYes = By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[1]");
	public static By obj_IncLifeEventNo = By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[2]");
	public static By obj_IncLifeEventUnSure = By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[3]");
	public static By obj_Path= By.id("ngf-transferCvrFileUploadAddIdBtn");

	public static By obj_BrowseLifeEvent = By.name("transferCvrFileUploadAddIdBtn");
	public static By obj_AddLifeEvent = By.xpath("//*[@id='transferCvrFileUploadAddId']");
	//public static By obj_Add = By.xpath("//span[contains(text(),'Add')]");
	public static By obj_Continue_LifeEvent = By.xpath("//*[@ng-click='lifeEventFormSubmit(lifeEvent);']");


	// CoverDetails (Indexation Not Handled)
	public static By obj_PersonalDetails_PremiumFrequency = By.name("premFreq");
	public static By obj_PersonalDetails_Fixed = By.xpath("(.//input[@name='coverType']/parent::*)[1]");
	public static By obj_PersonalDetails_Unitised = By.xpath("(.//input[@name='coverType']/parent::*)[2]");
	public static By obj_PersonalDetails_Fixed_DeathUnit0 = By.xpath("(//input[@name='requireCover']/parent::*)[4]");
	public static By obj_PersonalDetails_Fixed_DeathUnit1 = By.xpath("(//input[@name='requireCover']/parent::*)[5]");
	public static By obj_PersonalDetails_Fixed_DeathUnit2 = By.xpath("(//input[@name='requireCover']/parent::*)[6]");
	public static By obj_PersonalDetails_Fixed_TPDUnit0 = By.xpath("(//input[@name='TPDRequireCover']/parent::*)[1]");
	public static By obj_PersonalDetails_Fixed_TPDUnit1 = By.xpath("(//input[@name='TPDRequireCover']/parent::*)[2]");
	public static By obj_PersonalDetails_Fixed_TPDUnit2 = By.xpath("(//input[@name='TPDRequireCover']/parent::*)[3]");

	public static By obj_PersonalDetails_Unit_DeathUnit0 = By.xpath("(//input[@name='requireCover']/parent::*)[1]");
	public static By obj_PersonalDetails_Unit_DeathUnit1 = By.xpath("(//input[@name='requireCover']/parent::*)[2]");
	public static By obj_PersonalDetails_Unit_DeathUnit2 = By.xpath("(//input[@name='requireCover']/parent::*)[3]");
	public static By obj_PersonalDetails_Unit_TPDUnit0 = By.xpath("(//input[@name='TPDRequireCover']/parent::*)[4]");
	public static By obj_PersonalDetails_Unit_TPDUnit1 = By.xpath("(//input[@name='TPDRequireCover']/parent::*)[5]");
	public static By obj_PersonalDetails_Unit_TPDUnit2 = By.xpath("(//input[@name='TPDRequireCover']/parent::*)[6]");


	//*[text()='the member is not a casual or contractor employee ']
		public static By obj_PersonalDetails_DeathAmount = By.xpath("(//*[@id='death']//h4)");
		public static By obj_PersonalDetails_DeathCost = By.xpath("(//*[@id='death']//h4)[2]");
		public static By obj_PersonalDetails_TPDAmount = By.xpath("(//*[@id='tpd']//h4)");
		public static By obj_PersonalDetails_TPDCost = By.xpath("(//*[@id='tpd']//h4)[2]");
		public static By obj_PersonalDetails_TotalCostYearly = By.xpath("//*[text()=' Total Yearly cost : ']/following-sibling::h4");
		public static By obj_PersonalDetails_TotalCostMonthly = By.xpath("//*[text()=' Total Monthly cost : ']/following-sibling::h4");
		public static By obj_PersonalDetails_TotalCostWeekly = By.xpath("//*[text()=' Total Weekly cost : ']/following-sibling::h4");

	//Footer
	public static By obj_PersonalDetails_Save = By.xpath("//*[@ng-click='saveQuoteLifeEvent()']");
	public static By obj_PersonalDetails_Continue = By.xpath("//*[@ng-click='lifeEventFormSubmit(coverCalculatorForm)']");

	public static By obj_SaveReferenceNo = By.xpath("//*[contains(text(), 'Your application reference no')]");
	public static By obj_SaveClose_personalDetails = By.xpath("//button[@ng-click='closeThisDialog()']");
	public static By obj_SaveClose_Confirmation = By.xpath("//button[@ng-click='preCloseCallback()']");
	public static By obj_PersonalDetails_OpenSavedApplication = By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']");
	public static By obj_PersonalDetails_CancelMsg = By.id("decCancelMsg_text");
	public static By obj_PersonalDetails_DecisionContinue = By.xpath("//button[@ng-click='goTo()']");

	// ************************************************************************************************************************
	// -----------------------------------------Confirmation Details
	// ************************************************************************************************************************

	// Personal Details
	public static By obj_Confirmation_FirstName = By.xpath("//*[text()='First name']//following::label");
	public static By obj_Confirmation_LastName = By.xpath("//*[text()='Last name']//following::label");
	public static By obj_Confirmation_Gender = By.xpath("//*[text()='Gender']//following::label");
	public static By obj_Confirmation_ContactType = By.xpath("//*[text()='Preferred contact type']//following::label");
	public static By obj_Confirmation_ContactNo = By.xpath("//*[text()='Preferred contact number ']//following::label");
	public static By obj_Confirmation_Email = By.xpath("//*[text()='Email address']//following::label");
	public static By obj_Confirmation_DOB = By.xpath("//*[text()='Date of birth']//following::label");
	public static By obj_Confirmation_Time = By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label");
	public static By obj_Confirmation_HoursWork = By.xpath("//*[text()='Do you work 14 or more hours per week?']//following::label");
	public static By obj_Confirmation_Citizen = By.xpath("//*[contains(text(),'Are you a citizen or permanent resident of Australia')]//following::label");
	public static By obj_Confirmation_AddQuest1a = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[1]");
	public static By obj_Confirmation_AddQuest1b = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[3]");
	public static By obj_Confirmation_TertiaryQualification = By.xpath("//*[text()='Do you:  ']//following::label");
	public static By obj_Confirmation_AnnualIncome = By.xpath("//*[text()='Annual income']//following::label");

	public static By obj_Confirmation_LifeEvent = By.xpath("//*[text()='Life event']//following::label");
	public static By obj_Confirmation_DateofEvent = By.xpath("//*[text()='Date of event']//following::label");
	public static By obj_Confirmation_IncreaseCover = By.xpath("//*[text()='Have you successfully applied for an increase in cover due to a life event within this calendar year?']//following::label");





	// Cover Details
	public static By obj_Confirmation_Death_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[1]//following::p");
	public static By obj_Confirmation_Death_NewCover = By.xpath("(//*[text()='New Cover'])[1]//following::p");
	public static By obj_Confirmation_Death_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[1]//following::p");
	public static By obj_Confirmation_Death_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[1]//following::p");
	public static By obj_Confirmation_TPD_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[2]//following::p");
	public static By obj_Confirmation_TPD_NewCover = By.xpath("(//*[text()='New Cover'])[2]//following::p");
	public static By obj_Confirmation_TPD_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[2]//following::p");
	public static By obj_Confirmation_TPD_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[2]//following::p");
	public static By obj_Confirmation_IP_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[3]//following::p");
	public static By obj_Confirmation_IP_NewCover = By.xpath("(//*[text()='New Cover'])[3]//following::p");
	public static By obj_Confirmation_IP_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[3]//following::p");
	public static By obj_Confirmation_IP_WaitingPeriod = By.xpath("//*[text()='Waiting period']//following::p");
	public static By obj_Confirmation_IP_BenefitPeriod = By.xpath("//*[text()='Benefit period']//following::p");
	public static By obj_Confirmation_IP_WeeklyCost = By.xpath("//*[text()=' Total Weekly cost']//following::h4");
	public static By obj_Confirmation_TotalWeeklyCost = By.xpath("//*[contains(text(), 'Total Weekly cost')]//following::h4");


	public static By obj_Confirmation_TermsConditionsCheckBox = By.xpath("//*[@id='generalConsentLabel']/span");
	public static By obj_Confirmation_Submit = By.xpath("//*[@ng-click='navigateToDecision()']");
	public static By obj_DecisionPopUP_No = By.xpath("//button[text()='No']");
	public static By obj_DecisionPopUP_Appstatus = By.xpath("//a[@class='noarrow']/h4");
	public static By obj_DecisionPopUP_AppMsg = By.xpath("//*[@id='collapseOne']/div/div/div/p");
	public static By obj_Decision_App_No = By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p");
	public static By obj_Confirmation_Download = By.xpath("//strong[text()='Download ']");
	public static By obj_Confirmation_Save = By.xpath("//*[@ng-click='saveSummary()']");

}
