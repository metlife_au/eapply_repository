package pages.metlife.ECLAIMS;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ScriptHelper;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class Claim_Angular_claimtype_Injury extends MasterPage {
	WebDriverUtil driverUtil = null;

	public Claim_Angular_claimtype_Injury E_injury() {
		Eclaims_injuryflow();
		Eclaims_Summary();

		return new Claim_Angular_claimtype_Injury(scriptHelper);
	}
	
	public Claim_Angular_claimtype_Injury E_injurynegativescenario() {
		Eclaims_injurynegativescenario();

		return new Claim_Angular_claimtype_Injury(scriptHelper);
	}

	public Claim_Angular_claimtype_Injury(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void Eclaims_injuryflow() {
	
		String Creditcard = dataTable.getData("Eclaims_Injury", "Creditcard");
		String Title = dataTable.getData("Eclaims_Injury", "Title");
		String DOB = dataTable.getData("Eclaims_Injury", "DOB");
		String Name = dataTable.getData("Eclaims_Injury", "Name");
		String Surname = dataTable.getData("Eclaims_Injury", "Surname");
		String Address = dataTable.getData("Eclaims_Injury", "Address1");
		String Addresss = dataTable.getData("Eclaims_Injury", "Address2");
		String suburb = dataTable.getData("Eclaims_Injury", "suburb");
		String state = dataTable.getData("Eclaims_Injury", "State");
		String postcode = dataTable.getData("Eclaims_Injury", "Postcode");
		String Relationship = dataTable.getData("Eclaims_Injury", "Relationship");
		String givenTitle = dataTable.getData("Eclaims_Injury", "givenTitle");
		String Givenname = dataTable.getData("Eclaims_Injury", "Givenname");
		String Givensurname = dataTable.getData("Eclaims_Injury", "Givensurname");
		String PreferredPhoneNO = dataTable.getData("Eclaims_Injury", "PreferredPhoneNO");
		String Otherphoneno = dataTable.getData("Eclaims_Injury", "Otherphoneno");
		String Email = dataTable.getData("Eclaims_Injury", "Email");
		String Preferredmode = dataTable.getData("Eclaims_Injury", "Preferredmode");
		String Contacttime = dataTable.getData("Eclaims_Injury", "Contacttime");
		String symptom = dataTable.getData("Eclaims_Injury", "Symptom");
		String dateofinjury = dataTable.getData("Eclaims_Injury", "dateofinjury");

		String Medicalpractitionername = dataTable.getData("Eclaims_Injury", "Medicalpractitionername");
		String Medicalparctionerphn = dataTable.getData("Eclaims_Injury", "Medicalpractitionerphoneno");
		String practitioneradd = dataTable.getData("Eclaims_Injury", "practitioneradd");
		String practitioneraddress = dataTable.getData("Eclaims_Injury", "practitioneraddress");
		String Deathcertificate = dataTable.getData("Eclaims_Injury", "Deathcertificate");
		String Attachpath = dataTable.getData("Eclaims_Injury", "Attachpath");

		String Addresssame = dataTable.getData("Eclaims_Injury", "Addresssame");
		String InsuredDetail = dataTable.getData("Eclaims_Injury", "InsuredDetail");
		String Relationshiptoinsurer = dataTable.getData("Eclaims_Injury", "Relationshiptoinsurer");
		String Relationshipdetails = dataTable.getData("Eclaims_Injury", "Relationshipdetails");
		String insuredDOB = dataTable.getData("Eclaims_Injury", "insuredDOB");
		String Typeofinjury = dataTable.getData("Eclaims_Injury", "Typeofinjury");
		String Lastdayofwork = dataTable.getData("Eclaims_Injury", "Lastdayofwork");
		String Lastdaydate = dataTable.getData("Eclaims_Injury", "Lastdaydate");
		String dateofreturn = dataTable.getData("Eclaims_Injury", "dateofreturn");
		String Occupation = dataTable.getData("Eclaims_Injury", "Occupation");
		String AnotherMedicalpractitioner = dataTable.getData("Eclaims_Injury", "AnotherMedicalpractitioner");
		String Insurancebenefit = dataTable.getData("Eclaims_Injury", "Insurancebenefit");
		String Nameofinsurer = dataTable.getData("Eclaims_Injury", "Nameofinsurer");
		String Claimnumber = dataTable.getData("Eclaims_Injury", "Claimnumber");
		String insurerAddress = dataTable.getData("Eclaims_Injury", "insurerAddress");
		String insurerAddresss = dataTable.getData("Eclaims_Injury", "insurerAddresss");
		String Otherdocument = dataTable.getData("Eclaims_Injury", "Otherdocument");
		String Powerofattornydoc = dataTable.getData("Eclaims_Injury", "Powerofattornydoc");
		String Details = dataTable.getData("Eclaims_Injury", "Details");

		try {

			WebDriverWait wait = new WebDriverWait(driver, 18);

			// ****Click on claim type*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-DISABILITY']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Claim Type:", "Injury/Illness claim is Selected", Status.PASS);

			// ****Enter creditcard number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
			report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",
					Creditcard + " is entered", Status.PASS);

			if (InsuredDetail.equalsIgnoreCase("Yes")) {

				// ****Primary Account Holder*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Primary Account Holder", "Yes is Selected", Status.PASS);

				// ****Select title*****\\

				Select titletype = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
				titletype.selectByVisibleText(Title);
				report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

				// ****Enter DOB*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
						.sendKeys(DOB);
				report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

				// ****Enter Name*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
						.sendKeys(Name);
				report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

				// ****Enter surname*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
						.sendKeys(Surname);
				report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

				// ****Enter Address1*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
						.sendKeys(Address);
				report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

				// ****Enter Address2*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
						.sendKeys(Addresss);
				report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

				// ****Enter Suburb*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
						.sendKeys(suburb);
				report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

				// ****Select State*****\\

				Select Stateselect = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
				Stateselect.selectByVisibleText(state);
				report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
				sleep(500);

				// ****Enter Postal code*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
						.sendKeys(postcode);
				sleep(500);
				report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
				sleep(300);

				// ****Enter Emailaddress*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ213_0")))
						.sendKeys(Email);
				report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

				// ****Enter Peferred phone number*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ211_1")))
						.sendKeys(PreferredPhoneNO);
				sleep(500);
				report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
				sleep(500);

				// ****Enter other phone number*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ210_2")))
						.sendKeys(Otherphoneno);
				sleep(500);
				report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
				sleep(500);

				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,600)", "");
				sleep(500);

				// ****Preferred mode of contact (Phone / Email /
				// Letter)?*****\\

				if (Preferredmode.equalsIgnoreCase("Phone")) {

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*")))
							.click();
					sleep(1000);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

					Select mode = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ417_1")));
					mode.selectByVisibleText(Contacttime);
					report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
							Contacttime + " is selected ", Status.PASS);
					sleep(500);
				}
				if (Preferredmode.equalsIgnoreCase("Email")) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
				}

			} else {

				// ****Primary Account Holder*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Primary Account Holder", "No is Selected", Status.PASS);

				// ****Select title*****\\

				Select titletype = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
				titletype.selectByVisibleText(Title);
				report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

				// ****Enter DOB*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
						.sendKeys(DOB);
				report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

				// ****Enter Name*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
						.sendKeys(Name);
				report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

				// ****Enter surname*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
						.sendKeys(Surname);
				report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

				// ****Enter Address1*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
						.sendKeys(Address);
				report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

				// ****Enter Address2*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
						.sendKeys(Addresss);
				report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

				// ****Enter Suburb*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
						.sendKeys(suburb);
				report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

				// ****Select State*****\\

				Select Stateselect = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
				Stateselect.selectByVisibleText(state);
				report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
				sleep(500);

				// ****Enter Postal code*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
						.sendKeys(postcode);
				sleep(500);
				report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
				sleep(300);

				// *******************************//
				// *****Claimant Details***********
				// *******************************//

				// ***Relationship to insured****//
				if (Relationshiptoinsurer.equalsIgnoreCase("Powerofattorney")) {
					Select insurerselect = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ400_0")));
					insurerselect.selectByVisibleText(Relationshipdetails);
					report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
					sleep(500);

					// ****Select title*****\\

					Select title = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ401_2")));
					title.selectByVisibleText(Title);
					report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

					// ****Enter Given name*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ403_0")))
							.sendKeys(Givenname);
					report.updateTestLog("Enter Given name:", Givenname + " is entered", Status.PASS);
					sleep(500);

					// ****Enter surname*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ404_1")))
							.sendKeys(Givensurname);
					report.updateTestLog("Enter Surname name:", Givensurname + " is entered", Status.PASS);
					sleep(500);

					// ****Enter Relationship to insured DOB******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11058_2")))
							.sendKeys(insuredDOB);
					report.updateTestLog("Enter Relationship to insured DOB :", insuredDOB + " is entered",
							Status.PASS);
					sleep(500);

				}
				if (Relationshiptoinsurer.equalsIgnoreCase("others")) {
					Select insurerselect = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ400_0")));
					insurerselect.selectByVisibleText(Relationshipdetails);
					report.updateTestLog("Select Title:", Title + " is selected ", Status.PASS);
					sleep(500);

					// ****Relationship details*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11054_1")))
							.sendKeys(Details);
					report.updateTestLog("Relationship details :", Details + " is entered", Status.PASS);
					sleep(500);

					// ****Select title*****\\

					Select title = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ401_2")));
					title.selectByVisibleText(Title);
					report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

					// ****Enter Given name*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ403_0")))
							.sendKeys(Givenname);
					report.updateTestLog("Enter Given name:", Givenname + " is entered", Status.PASS);
					sleep(500);

					// ****Enter surname*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ404_1")))
							.sendKeys(Givensurname);
					report.updateTestLog("Enter Surname name:", Givensurname + " is entered", Status.PASS);
					sleep(500);

					// ****Enter Relationship to insured DOB******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11058_2")))
							.click();
					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11058_2")))
							.sendKeys(insuredDOB);
					report.updateTestLog("Enter Relationship to insured DOB :", insuredDOB + " is entered",
							Status.PASS);
					sleep(500);

				}

				// ****Is your address the same as the primary account
				// holder?*****\\
				if (Addresssame.equalsIgnoreCase("Yes")) {

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-Yes']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Address same is selected", "Selected Yes", Status.PASS);
					sleep(500);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-No']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Address same as primary accont hoder", "Selected No", Status.PASS);
					sleep(500);

					// ****Enter Address1*****\\
					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ409_1")))
							.sendKeys(Address);
					report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

					// ****Enter Address2*****\\
					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ410_2")))
							.sendKeys(Addresss);
					report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

					// ****Enter Suburb*****\\
					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ411_0")))
							.sendKeys(suburb);
					report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

					// ****Select State*****\\

					Select Stateprimaryselect = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ412_1")));
					Stateprimaryselect.selectByVisibleText(state);
					report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
					sleep(500);

					// ****Enter Postal code*****\\
					wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ413_2")))
							.sendKeys(postcode);
					sleep(500);
					report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
					sleep(300);
				}

				// ****Enter Emailaddress*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ402_0")))
						.sendKeys(Email);
				report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

				// ****Enter Peferred phone number*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ415_1")))
						.sendKeys(PreferredPhoneNO);
				sleep(500);
				report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
				sleep(500);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,600)", "");
				sleep(500);
				// ****Enter other phone number*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ414_2")))
						.sendKeys(Otherphoneno);
				sleep(500);
				report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
				sleep(500);

				// ****Preferred mode of contact (Phone / Email /
				// Letter)?*****\\

				if (Preferredmode.equalsIgnoreCase("Phone")) {

					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ416-Phone']/parent::*")))
							.click();
					sleep(700);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

					Select mode = new Select(
							driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ215_1")));
					mode.selectByVisibleText(Preferredmode);
					report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
							Preferredmode + " is selected ", Status.PASS);
					sleep(500);
				}
				if (Preferredmode.equalsIgnoreCase("Email")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ416-Email']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ416-Letter']/parent::*")))
							.click();
					sleep(500);
					report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
				}

			}

			// ***************Condition Details***************//

			// ****What is your illness/injury?******\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input-datalist_CQ10001_0")))
					.sendKeys(Typeofinjury);
			sleep(1000);
			report.updateTestLog("What is your illness/injury? :", Typeofinjury + " is entered", Status.PASS);

			// ****When did you first experience symptoms? *******\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ10002_0")))
					.sendKeys(symptom);
			sleep(500);
			report.updateTestLog("When did you first experience symptoms? :", symptom + " is entered", Status.PASS);

			// ****Date illness/injury diagnosed ********\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ10003_1")))
					.sendKeys(dateofinjury);
			sleep(500);
			report.updateTestLog("Date illness/injury diagnosed :", dateofinjury + " is entered", Status.PASS);

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//label[text()='When did you first experience symptoms? ']")))
					.click();
			sleep(500);

			// ****Proceed button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
			sleep(500);
			report.updateTestLog("Proceed Button", "clicked", Status.PASS);

			// ***Additional Details*****\\

			// *****Were you continuously employed for at least 180 days prior
			// to your last day of work? ***//

			if (Lastdayofwork.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Employed for at least 180 days ", "Yes is selected", Status.PASS);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Employed for at least 180 days ", "No is selected", Status.PASS);
			}

			// ****Last day of work*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10031_0")))
					.click();
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10031_0")))
					.sendKeys(Lastdaydate);
			sleep(500);
			report.updateTestLog("Last day of work: ", Lastdaydate + " is entered", Status.PASS);

			// ****Date of return to work (if known) ********\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10032_1")))
					.click();
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10032_1")))
					.sendKeys(dateofreturn);
			sleep(500);
			report.updateTestLog("Date of return to work (if known):", dateofreturn + " is entered", Status.PASS);

			// ****What is your usual occupation? ********\\

			sleep(500);
			wait.until(
					ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input-datalist_CQ702_0")))
					.sendKeys(Occupation);
			sleep(500);
			report.updateTestLog("Occupation:", Occupation + " is entered", Status.PASS);
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("window.scrollBy(0,300)", "");
			sleep(500);

			// *************************************************************//
			// *****************Medical Practitioner Details*****************//
			// *************************************************************//

			// ****Medical practitioner name*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10048_0")))
					.sendKeys(Medicalpractitionername);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner name:", Medicalpractitionername + " is entered",
					Status.PASS);

			JavascriptExecutor jss = (JavascriptExecutor) driver;
			jss.executeScript("window.scrollBy(0,300)", "");
			sleep(500);

			// ****Medical practitioner phone number******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10049_1")))
					.sendKeys(Medicalparctionerphn);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner phone number :", Medicalparctionerphn + " is entered",
					Status.PASS);

			// ****Medical practitioner address 1******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10050_0")))
					.sendKeys(practitioneradd);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner address 1:", practitioneradd + " is entered", Status.PASS);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,800)", "");
			sleep(500);

			// ****Medical practitioner address 2******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10051_1")))
					.sendKeys(practitioneraddress);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner address 2:", practitioneraddress + " is entered",
					Status.PASS);

			if (AnotherMedicalpractitioner.equalsIgnoreCase("Yes")) {

				// ***Are you seeing another medical practitioner for your
				// condition?**//

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ10052-Yes']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Enter Another medical practitioner:", "Yes checkbox Clicked", Status.PASS);

				// ****Medical practitioner name*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("vm.additionalDetailsForm_input_CQ10053_0")))
						.sendKeys(Medicalpractitionername);
				sleep(500);
				report.updateTestLog("Enter Another Medical practitioner name:",
						Medicalpractitionername + " is entered", Status.PASS);

				// ****Medical practitioner phone number******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10054_1")))
						.sendKeys(Medicalparctionerphn);
				sleep(500);
				report.updateTestLog("Enter Another  Medical practitioner phone number :",
						Medicalparctionerphn + " is entered", Status.PASS);

				// ****Medical practitioner address 1******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10055_0")))
						.sendKeys(practitioneradd);
				sleep(500);
				report.updateTestLog("Enter Another  Medical practitioner address 1:", practitioneradd + " is entered",
						Status.PASS);

				// ****Medical practitioner address 2******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10056_1")))
						.sendKeys(practitioneraddress);
				sleep(500);
				report.updateTestLog("Enter Another  Medical practitioner address 2:",
						practitioneraddress + " is entered", Status.PASS);
			} else {
				// ***Are you seeing another medical practitioner for your
				// condition?**//

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ10052-No']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Enter Another medical practitioner :", "No checkbox Clicked", Status.PASS);
			}

			JavascriptExecutor j = (JavascriptExecutor) driver;
			j.executeScript("window.scrollBy(0,500)", "");
			sleep(500);

			// ************Are you in receipt of any other insurance benefits
			// for your condition? *****//

			if (Insurancebenefit.equalsIgnoreCase("Yes")) {

				// ***Are you in receipt of any other insurance benefits for
				// your condition? **//

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ10057-Yes']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Other insurance benefits :", "Yes checkbox Clicked", Status.PASS);

				// ****Name of insurer*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10058_0")))
						.sendKeys(Nameofinsurer);
				sleep(500);
				report.updateTestLog("Enter Name of insurer :", Nameofinsurer + " is entered", Status.PASS);

				// ****Claim number******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10059_1")))
						.sendKeys(Claimnumber);
				sleep(500);
				report.updateTestLog("Enter Claim number :", Claimnumber + " is entered", Status.PASS);

				// ****Insurer address 1******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10060_0")))
						.sendKeys(insurerAddress);
				sleep(500);
				report.updateTestLog("Enter Address 1 :", insurerAddress + " is entered", Status.PASS);

				// ****Insurer address 2******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10061_1")))
						.sendKeys(insurerAddresss);
				sleep(500);
				report.updateTestLog("Enter Address 2 :", insurerAddresss + " is entered", Status.PASS);

			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
				// ***Are you in receipt of any other insurance benefits for
				// your condition? **//

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ10057-No']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Other insurance benefits :", "No checkbox Clicked", Status.PASS);
			}

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,400)", "");
			sleep(500);

			// *************************************//
			// ******Required Documentation********//
			// ************************************//
			// ****Attachpath******\\

			if (Deathcertificate.equalsIgnoreCase("Yes")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_upload_CQ901_1")))
						.sendKeys(Attachpath);
				report.updateTestLog("Document Attachment", "Medical statement is attached", Status.PASS);
				sleep(1500);
				// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']"))).click();
				WebElement add = driver.findElement(By.xpath("//*[@id='attach_action_spanid']"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("attatchrequiredDocument"))).click();
				sleep(1500);
				report.updateTestLog("Attached file button", "Attach Button is Selected", Status.PASS);

			}
			if (Otherdocument.equalsIgnoreCase("Yes")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_upload_CQ909_1")))
						.sendKeys(Attachpath);
				report.updateTestLog("Document Attachment", "Other document is attached", Status.PASS);
				sleep(1300);
				// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']"))).click();
				WebElement add = driver.findElement(By.xpath("//*[@id='attach_action_spanid']"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1000);
				sleep(2000);
				report.updateTestLog("Attached file", "Attach Button is Selected", Status.PASS);

			}
			if (Powerofattornydoc.equalsIgnoreCase("Yes")) {

				wait.until(ExpectedConditions
						.elementToBeClickable(By.id("vm.additionalDetailsForm_upload_powerOfAttorney_1")))
						.sendKeys(Attachpath);
				report.updateTestLog("Document Attachment", "Power of Attorny document is ed", Status.PASS);
				sleep(1300);
				// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']"))).click();
				WebElement add = driver.findElement(By.id("attatchrequiredDocument"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(2000);
				report.updateTestLog("Attached file", "Attach Button is Selected", Status.PASS);

			}

			if (Deathcertificate.equalsIgnoreCase("No") && Otherdocument.equalsIgnoreCase("No")) {

				// ***Agree button**//

				wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
				report.updateTestLog("Required document", "File not attached checkbox is clicked", Status.PASS);
			}

			JavascriptExecutor e = (JavascriptExecutor) driver;
			e.executeScript("window.scrollBy(0,700)", "");
			sleep(500);
			// ****Next button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
			sleep(1000);
			report.updateTestLog("Next Button :", "Selected next button", Status.PASS);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	public void Eclaims_Summary() {

		try {

			String ClaimType = dataTable.getData("Eclaims_Injury", "ClaimType");
			String Creditcard = dataTable.getData("Eclaims_Injury", "Creditcard");
			String Title = dataTable.getData("Eclaims_Injury", "Title");
			String DOB = dataTable.getData("Eclaims_Injury", "DOB");
			String Name = dataTable.getData("Eclaims_Injury", "Name");
			String Surname = dataTable.getData("Eclaims_Injury", "Surname");
			String Address = dataTable.getData("Eclaims_Injury", "Address1");
			String Addresss = dataTable.getData("Eclaims_Injury", "Address2");
			String suburb = dataTable.getData("Eclaims_Injury", "suburb");
			String state = dataTable.getData("Eclaims_Injury", "State");
			String postcode = dataTable.getData("Eclaims_Injury", "Postcode");
			String Relationship = dataTable.getData("Eclaims_Injury", "Relationship");
			String givenTitle = dataTable.getData("Eclaims_Injury", "givenTitle");
			String Givenname = dataTable.getData("Eclaims_Injury", "Givenname");
			String Givensurname = dataTable.getData("Eclaims_Injury", "Givensurname");
			String PreferredPhoneNO = dataTable.getData("Eclaims_Injury", "PreferredPhoneNO");
			String Otherphoneno = dataTable.getData("Eclaims_Injury", "Otherphoneno");
			String Email = dataTable.getData("Eclaims_Injury", "Email");
			String Preferredmode = dataTable.getData("Eclaims_Injury", "Preferredmode");
			String Contacttime = dataTable.getData("Eclaims_Injury", "Contacttime");
			String symptom = dataTable.getData("Eclaims_Injury", "Symptom");
			String dateofinjury = dataTable.getData("Eclaims_Injury", "dateofinjury");
			String Medicalpractitionername = dataTable.getData("Eclaims_Injury", "Medicalpractitionername");
			String Medicalparctionerphn = dataTable.getData("Eclaims_Injury", "Medicalpractitionerphoneno");
			String practitioneradd = dataTable.getData("Eclaims_Injury", "practitioneradd");
			String practitioneraddress = dataTable.getData("Eclaims_Injury", "practitioneraddress");
			String Deathcertificate = dataTable.getData("Eclaims_Injury", "Deathcertificate");
			String Attachpath = dataTable.getData("Eclaims_Injury", "Attachpath");
			String Addresssame = dataTable.getData("Eclaims_Injury", "Addresssame");
			String InsuredDetail = dataTable.getData("Eclaims_Injury", "InsuredDetail");
			String Typeofinjury = dataTable.getData("Eclaims_Injury", "Typeofinjury");
			String Lastdayofwork = dataTable.getData("Eclaims_Injury", "Lastdayofwork");
			String Lastdaydate = dataTable.getData("Eclaims_Injury", "Lastdaydate");
			String dateofreturn = dataTable.getData("Eclaims_Injury", "dateofreturn");
			String Occupation = dataTable.getData("Eclaims_Injury", "Occupation");
			String AnotherMedicalpractitioner = dataTable.getData("Eclaims_Injury", "AnotherMedicalpractitioner");
			String Insurancebenefit = dataTable.getData("Eclaims_Injury", "Insurancebenefit");
			String Nameofinsurer = dataTable.getData("Eclaims_Injury", "Nameofinsurer");
			String Claimnumber = dataTable.getData("Eclaims_Injury", "Claimnumber");
			String insurerAddress = dataTable.getData("Eclaims_Injury", "insurerAddress");
			String insurerAddresss = dataTable.getData("Eclaims_Injury", "insurerAddresss");
			String Otherdocument = dataTable.getData("Eclaims_Injury", "Otherdocument");
			String producttype = dataTable.getData("Eclaims_Injury", "Producttype");
			String Powerofattornydoc = dataTable.getData("Eclaims_Injury", "Powerofattornydoc");

			WebElement product = driver.findElement(By.xpath("//label[text()='Product ']/following-sibling::label"));
			WebElement claimtype = driver
					.findElement(By.xpath("//label[text()='Claim Type ']/following-sibling::label"));
			WebElement creditcard = driver.findElement(By.xpath(
					"//label[text()='Credit card / facility no. (Please enter the last 4 digits) ']/following-sibling::label"));
			WebElement title = driver.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[1]"));
			WebElement Dob = driver
					.findElement(By.xpath("//label[text()='Date of birth (dd/mm/yyyy) ']/following-sibling::label"));
			WebElement GivenName = driver
					.findElement(By.xpath("(//label[text()='Given name ']/following-sibling::label)[1]"));
			WebElement SurName = driver
					.findElement(By.xpath("(//label[text()='Surname ']/following-sibling::label)[1]"));
			WebElement Address1 = driver
					.findElement(By.xpath("(//label[text()='Address1 ']/following-sibling::label)[1]"));
			WebElement Address2 = driver
					.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[1]"));
			WebElement Suburb = driver.findElement(By.xpath("(//label[text()='Suburb ']/following-sibling::label)[1]"));
			WebElement State = driver.findElement(By.xpath("(//label[text()='State ']/following-sibling::label)[1]"));
			WebElement postcodes = driver
					.findElement(By.xpath("(//label[text()='Post Code ']/following-sibling::label)[1]"));

			WebElement Preferredphoneno = driver
					.findElement(By.xpath("//label[text()='Preferred phone no ']/following-sibling::label"));
			WebElement Emailaddress = driver
					.findElement(By.xpath("//label[text()='Email address ']/following-sibling::label"));

			String product_validation = product.getText();
			String claimtype_validation = claimtype.getText();
			String creditcard_validation = creditcard.getText();
			String title_validation = title.getText();
			String Dob_validation = Dob.getText();
			String GivenName_validation = GivenName.getText();
			String SurName_validation = SurName.getText();
			String Address1_validation = Address1.getText();
			String Address2_validation = Address2.getText();
			String Suburb_validation = Suburb.getText();
			String State_validation = State.getText();
			String postcode_validation = postcodes.getText();
			String Preferredphoneno_validation = Preferredphoneno.getText();
			String Emailaddress_validation = Emailaddress.getText();

			WebDriverWait wait = new WebDriverWait(driver, 18);

			// ****Product*****\\
			if (producttype.contains(product_validation)) {

				report.updateTestLog("Product validation:", producttype + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Product validation:", producttype + " is not displayed as expected", Status.FAIL);
			}

			// ****Validate claim type*****\\
			if (ClaimType.contains(claimtype_validation)) {

				report.updateTestLog("Claim Type validation:", ClaimType + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Claim Type validation:", ClaimType + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Enter creditcard number*****\\

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,100)", "");
			sleep(500);

			if (Creditcard.contains(creditcard_validation)) {

				report.updateTestLog("Credit card validation:", Creditcard + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Credit card validation:", Creditcard + " is not displayed as expected",
						Status.FAIL);
			}

			if (InsuredDetail.equalsIgnoreCase("Yes")) {

				// ****Select title*****\\

				if (Title.contains(title_validation)) {

					report.updateTestLog("Title validation:", Title + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Title validation:", Title + " is not displayed as expected", Status.FAIL);
				}

				JavascriptExecutor a = (JavascriptExecutor) driver;
				a.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
				// ****Enter DOB*****\\

				if (DOB.contains(Dob_validation)) {

					report.updateTestLog("DOB validation:", DOB + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("DOB validation:", DOB + " is not displayed as expected", Status.FAIL);
				}

				JavascriptExecutor jj = (JavascriptExecutor) driver;
				jj.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				// ****Enter Name*****\\

				if (Name.contains(GivenName_validation)) {

					report.updateTestLog("Name validation:", Name + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Name validation:", Name + " is not displayed as expected", Status.FAIL);
				}

				// ****Enter surname*****\\

				if (Surname.contains(SurName_validation)) {

					report.updateTestLog("Surname validation:", Surname + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Surname validation:", Surname + " is not displayed as expected", Status.FAIL);
				}
				JavascriptExecutor b = (JavascriptExecutor) driver;
				b.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
				// ****Enter Address1*****\\

				if (Address.contains(Address1_validation)) {

					report.updateTestLog("Address 1 validation:", Address + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Address 1 validation:", Address + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Enter Address2*****\\
				JavascriptExecutor exe = (JavascriptExecutor) driver;
				exe.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				if (Addresss.contains(Address2_validation)) {

					report.updateTestLog("Address 2 validation:", Addresss + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Address 2 validation:", Addresss + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Enter Suburb*****\\

				if (suburb.contains(Suburb_validation)) {

					report.updateTestLog("Suburb validation:", suburb + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Suburb validation:", suburb + " is not displayed as expected", Status.FAIL);
				}
				JavascriptExecutor exs = (JavascriptExecutor) driver;
				exs.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				// ****Select State*****\\

				if (state.contains(State_validation)) {

					report.updateTestLog("State validation:", state + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("State validation:", state + " is not displayed as expected", Status.FAIL);
				}

				// ****Enter Postal code*****\\

				if (postcode.contains(postcode_validation)) {

					report.updateTestLog("Post code validation:", postcode + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Post code validation:", postcode + " is not displayed as expected",
							Status.FAIL);
				}

				JavascriptExecutor jss = (JavascriptExecutor) driver;
				jss.executeScript("window.scrollBy(0,400)", "");
				sleep(500);

				// ****Enter Emailaddress*****\\

				if (Email.contains(Emailaddress_validation)) {

					report.updateTestLog("Email validation:", Email + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Email validation:", Email + " is not displayed as expected", Status.FAIL);
				}

				// ****Enter Peferred phone number*****\\

				if (PreferredPhoneNO.contains(Preferredphoneno_validation)) {

					report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("PreferredPhoneno validation:",
							PreferredPhoneNO + " is not displayed as expected", Status.FAIL);
				}

				JavascriptExecutor c = (JavascriptExecutor) driver;
				c.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
				// ****Enter other phone number*****\\
				/*
				 * WebElement Otherphonenos=driver.findElement(By.
				 * xpath("//label[text()='Other phone no ']/following-sibling::label"
				 * )); String Otherphoneno_validation=Otherphonenos.getText();
				 * if(Otherphoneno.contains(Otherphoneno_validation)){
				 * 
				 * report.updateTestLog("Otherphoneno validation:",
				 * Otherphoneno+" is displayed as Expected", Status.PASS); }
				 * else{ report.updateTestLog("Otherphoneno validation:",
				 * Otherphoneno+" is not displayed as expected", Status.FAIL); }
				 */

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,600)", "");
				sleep(500);

				// ****Preferred mode of contact (Phone / Email /
				// Letter)?*****\\

				if (Preferredmode.equalsIgnoreCase("Phone")) {
					WebElement Modeofcontact = driver.findElement(By.xpath(
							"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
					WebElement timeofcontact = driver.findElement(
							By.xpath("//label[text()='Preferred contact time: AM / PM ']/following-sibling::label"));
					String Modeofcontact_validation = Modeofcontact.getText();
					String timeofcontact_validation = timeofcontact.getText();
					if (Preferredmode.contains(Modeofcontact_validation)) {

						report.updateTestLog("Preferred Mode of Contact validation:",
								Preferredmode + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Preferred Mode of Contact validation:",
								Preferredmode + " is not displayed as expected", Status.FAIL);
					}

					if (Contacttime.contains(timeofcontact_validation)) {

						report.updateTestLog("Time of contact validation:", Contacttime + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Time of contact validation:",
								Contacttime + " is not displayed as expected", Status.FAIL);
					}

				}
				if (Preferredmode.equalsIgnoreCase("Email")) {
					WebElement Modeofcontact = driver.findElement(By.xpath(
							"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
					String Modeofcontact_validation = Modeofcontact.getText();
					if (Preferredmode.contains(Modeofcontact_validation)) {

						report.updateTestLog("Preferred mode of contact validation:",
								Preferredmode + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Preferred mode of contact validation:",
								Preferredmode + " is not displayed as expected", Status.FAIL);
					}

				} else {
					WebElement Modeofcontact = driver.findElement(By.xpath(
							"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
					String Modeofcontact_validation = Modeofcontact.getText();
					if (Preferredmode.contains(Modeofcontact_validation)) {

						report.updateTestLog("Preferredmode of contact validation:",
								Preferredmode + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Preferredmode of contact validation:",
								Preferredmode + " is not displayed as expected", Status.FAIL);
					}

				}
			}
			if (InsuredDetail.equalsIgnoreCase("No")) {
				WebElement Relationships = driver.findElement(
						By.xpath("//label[text()='Relationship to insured person ']/following-sibling::label"));
				WebElement Titles = driver
						.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[2]"));
				String Relationship_validation = Relationships.getText();
				String Title_validation = Titles.getText();
				JavascriptExecutor d = (JavascriptExecutor) driver;
				d.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
				// ****Select title*****\\

				if (Title.contains(Title_validation)) {

					report.updateTestLog("Title validation:", Title + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Title validation:", Title + " is not displayed as expected", Status.FAIL);
				}

				// ****Enter DOB*****\\

				if (DOB.contains(Dob_validation)) {

					report.updateTestLog("DOB validation:", DOB + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("DOB validation:", DOB + " is not displayed as expected", Status.FAIL);
				}

				JavascriptExecutor jj = (JavascriptExecutor) driver;
				jj.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				// ****Enter Name*****\\

				if (Name.contains(GivenName_validation)) {

					report.updateTestLog("Name validation:", Name + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Name validation:", Name + " is not displayed as expected", Status.FAIL);
				}

				// ****Enter surname*****\\

				if (Surname.contains(SurName_validation)) {

					report.updateTestLog("Surname validation:", Surname + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Surname validation:", Surname + " is not displayed as expected", Status.FAIL);
				}
				JavascriptExecutor e = (JavascriptExecutor) driver;
				e.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
				// ****Enter Address1*****\\

				if (Address.contains(Address1_validation)) {

					report.updateTestLog("Address 1 validation:", Address + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Address 1 validation:", Address + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Enter Address2*****\\
				JavascriptExecutor exe = (JavascriptExecutor) driver;
				exe.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				if (Addresss.contains(Address2_validation)) {

					report.updateTestLog("Address 2 validation:", Addresss + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Address 2 validation:", Addresss + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Enter Suburb*****\\

				if (suburb.contains(Suburb_validation)) {

					report.updateTestLog("Suburb validation:", suburb + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Suburb validation:", suburb + " is not displayed as expected", Status.FAIL);
				}
				JavascriptExecutor exs = (JavascriptExecutor) driver;
				exs.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				// ****Select State*****\\

				if (state.contains(State_validation)) {

					report.updateTestLog("State validation:", state + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("State validation:", state + " is not displayed as expected", Status.FAIL);
				}
				JavascriptExecutor f = (JavascriptExecutor) driver;
				f.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				// ****Enter Postal code*****\\

				if (postcode.contains(postcode_validation)) {

					report.updateTestLog("Post code validation:", postcode + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Post code validation:", postcode + " is not displayed as expected",
							Status.FAIL);
				}

				JavascriptExecutor jss = (JavascriptExecutor) driver;
				jss.executeScript("window.scrollBy(0,400)", "");
				sleep(500);

				// *******************************//
				// *****Claimant Details***********
				// *******************************//
				String Relationshiptoinsurer = dataTable.getData("Eclaims_Injury", "Relationshiptoinsurer");
				String Relationshipdetails = dataTable.getData("Eclaims_Injury", "Relationshipdetails");
				WebElement claimantGivenname = driver
						.findElement(By.xpath("(//label[text()='Given name ']/following-sibling::label)[2]"));
				WebElement claimantsurname = driver
						.findElement(By.xpath("(//label[text()='Surname ']/following-sibling::label)[2]"));
				WebElement Adresssame = driver.findElement(By.xpath(
						"//label[text()='Is your address the same as the primary account holder? ']/following-sibling::label"));
				WebElement claimantaddress1 = driver
						.findElement(By.xpath("(//label[text()='Address1 ']/following-sibling::label)[2]"));
				WebElement claimantaddress2 = driver
						.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[2]"));
				WebElement claimantsuburb = driver
						.findElement(By.xpath("(//label[text()='Suburb ']/following-sibling::label)[2]"));
				WebElement claimantstate = driver
						.findElement(By.xpath("(//label[text()='State ']/following-sibling::label)[2]"));
				WebElement claimantpostalcode = driver
						.findElement(By.xpath("(//label[text()='Post Code ']/following-sibling::label)[2]"));

				String claimantGivenname_validation = claimantGivenname.getText();
				String claimantsurname_validation = claimantsurname.getText();
				String Adresssame_validation = Adresssame.getText();
				String claimantaddress1_validation = claimantaddress1.getText();
				String claimantaddress2_validation = claimantaddress2.getText();
				String claimantsuburb_validation = claimantsuburb.getText();
				String claimantstate_validation = claimantstate.getText();
				String claimantpostalcode_validation = claimantpostalcode.getText();

				String insuredDOB = dataTable.getData("Eclaims_Injury", "insuredDOB");
				// ***Relationship to insured****//
				WebElement relationshiptoinsurer = driver.findElement(
						By.xpath("//label[text()='Relationship to insured person ']/following-sibling::label"));
				String relationshiptoinsurer_validation = relationshiptoinsurer.getText();
				if (Relationshiptoinsurer.equalsIgnoreCase("Powerofattorney")) {

					if (Relationshiptoinsurer.contains(relationshiptoinsurer_validation)) {

						report.updateTestLog("Relationship to insurer validation:",
								Relationshiptoinsurer + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Relationship to insurer validation:",
								Relationshiptoinsurer + " is not displayed as expected", Status.FAIL);
					}

					// ****Select title*****\\

					WebElement Claimanttitle = driver
							.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[2]"));
					String Claimanttitle_validation = Claimanttitle.getText();
					if (givenTitle.contains(Claimanttitle_validation)) {

						report.updateTestLog("Claimant Title validation:", Title + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Claimant Title validation:", Title + " is not displayed as expected",
								Status.FAIL);
					}

					// ****Enter Given name*****\\
					if (Givenname.contains(claimantGivenname_validation)) {

						report.updateTestLog("Claimant Given name validation:", Givenname + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Claimant Given name validation:",
								Givenname + " is not displayed as expected", Status.FAIL);
					}

					JavascriptExecutor je = (JavascriptExecutor) driver;
					je.executeScript("window.scrollBy(0,300)", "");
					sleep(500);

					// ****Enter surname*****\\

					if (Givensurname.contains(claimantsurname_validation)) {

						report.updateTestLog("Surname name validation:", Givensurname + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Surname name validation:", Givensurname + " is not displayed as expected",
								Status.FAIL);
					}

				}
				if (Relationshiptoinsurer.contains("others")) {
					/*
					 * if(Relationshiptoinsurer.contains(
					 * relationshiptoinsurer_validation)){
					 * 
					 * report.
					 * updateTestLog("Relationship to insurer validation:",
					 * Relationshiptoinsurer+" is displayed as Expected",
					 * Status.PASS); } else{ report.
					 * updateTestLog("Relationship to insurer validation:",
					 * Relationshiptoinsurer+" is not displayed as expected",
					 * Status.FAIL); }
					 */
					sleep(500);

					// ****Enter Given name*****\\
					if (Givenname.contains(claimantGivenname_validation)) {

						report.updateTestLog("Claimant Given name validation:", Givenname + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Claimant Given name validation:",
								Givenname + " is not displayed as expected", Status.FAIL);
					}

					JavascriptExecutor je = (JavascriptExecutor) driver;
					je.executeScript("window.scrollBy(0,300)", "");
					sleep(500);

					// ****Enter surname*****\\

					if (Givensurname.contains(claimantsurname_validation)) {

						report.updateTestLog("Surname name validation:", Givensurname + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Surname name validation:", Givensurname + " is not displayed as expected",
								Status.FAIL);
					}

				}

				// ****Is your address the same as the primary account
				// holder?*****\\
				if (Addresssame.equalsIgnoreCase("Yes") && Addresssame.equalsIgnoreCase("No")) {

					if (Addresssame.contains(Adresssame_validation)) {

						report.updateTestLog("Address same as primary account holder validation:",
								Addresssame + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Address same as primary account holder validation:",
								Addresssame + " is not displayed as expected", Status.FAIL);
					}

					// ****Enter Address1*****\\

					if (Address.contains(claimantaddress1_validation)) {

						report.updateTestLog("Address1 validation:", Address + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Address1 validation:", Address + " is not displayed as expected",
								Status.FAIL);
					}

					JavascriptExecutor jee = (JavascriptExecutor) driver;
					jee.executeScript("window.scrollBy(0,400)", "");
					sleep(500);
					// ****Enter Address2*****\\

					if (Addresss.contains(claimantaddress2_validation)) {

						report.updateTestLog("Address2 validation:", Addresss + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Address2 validation:", Addresss + " is not displayed as expected",
								Status.FAIL);
					}

					// ****Enter Suburb*****\\

					if (suburb.contains(claimantsuburb_validation)) {

						report.updateTestLog("Suburb validation:", suburb + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Suburb validation:", suburb + " is not displayed as expected",
								Status.FAIL);
					}
					JavascriptExecutor g = (JavascriptExecutor) driver;
					g.executeScript("window.scrollBy(0,100)", "");
					sleep(500);
					// ****Select State*****\\

					if (state.contains(claimantstate_validation)) {

						report.updateTestLog("state validation:", state + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("state validation:", state + " is not displayed as expected", Status.FAIL);
					}

					// ****Enter Postal code*****\\

					if (postcode.contains(claimantpostalcode_validation)) {

						report.updateTestLog("Post code validation:", postcode + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Post code validation:", postcode + " is not displayed as expected",
								Status.FAIL);
					}
				}

				// ****Enter Emailaddress*****\\

				if (Email.contains(Emailaddress_validation)) {

					report.updateTestLog("Email validation:", Email + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Email validation:", Email + " is not displayed as expected", Status.FAIL);
				}

				JavascriptExecutor h = (JavascriptExecutor) driver;
				h.executeScript("window.scrollBy(0,100)", "");
				sleep(500);

				// ****Enter Peferred phone number*****\\

				if (PreferredPhoneNO.contains(Preferredphoneno_validation)) {

					report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("PreferredPhoneno validation:",
							PreferredPhoneNO + " is not displayed as expected", Status.FAIL);
				}

				// ****Enter other phone number*****\\
				/*
				 * WebElement Otherphonenos=driver.findElement(By.
				 * xpath("//label[text()='Other phone no ']/following-sibling::label"
				 * )); String Otherphoneno_validation=Otherphonenos.getText();
				 * if(Otherphoneno.contains(Otherphoneno_validation)){
				 * 
				 * report.updateTestLog("Otherphoneno validation:",
				 * Otherphoneno+" is displayed as Expected", Status.PASS); }
				 * else{ report.updateTestLog("Otherphoneno validation:",
				 * Otherphoneno+" is not displayed as expected", Status.FAIL); }
				 * JavascriptExecutor jsee = (JavascriptExecutor) driver;
				 * jsee.executeScript("window.scrollBy(0,600)", ""); sleep(500);
				 * 
				 */

				// ****Preferred mode of contact (Phone / Email /
				// Letter)?*****\\

				if (Preferredmode.equalsIgnoreCase("Phone")) {
					if (Preferredmode.equalsIgnoreCase("Phone")) {
						WebElement Modeofcontact = driver.findElement(By.xpath(
								"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
						WebElement timeofcontact = driver.findElement(By
								.xpath("//label[text()='Preferred contact time: AM / PM ']/following-sibling::label"));
						String Modeofcontact_validation = Modeofcontact.getText();
						String timeofcontact_validation = timeofcontact.getText();
						if (Preferredmode.contains(Modeofcontact_validation)) {

							report.updateTestLog("Preferred Mode of Contact validation:",
									Preferredmode + " is displayed as Expected", Status.PASS);
						} else {
							report.updateTestLog("Preferred Mode of Contact validation:",
									Preferredmode + " is not displayed as expected", Status.FAIL);
						}

						if (Contacttime.contains(timeofcontact_validation)) {

							report.updateTestLog("Time of contact validation:",
									Contacttime + " is displayed as Expected", Status.PASS);
						} else {
							report.updateTestLog("Time of contact validation:",
									Contacttime + " is not displayed as expected", Status.FAIL);
						}
						JavascriptExecutor i = (JavascriptExecutor) driver;
						i.executeScript("window.scrollBy(0,100)", "");
						sleep(500);
					}
					if (Preferredmode.equalsIgnoreCase("Email")) {
						WebElement Modeofcontact = driver.findElement(By.xpath(
								"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
						String Modeofcontact_validation = Modeofcontact.getText();
						if (Preferredmode.contains(Modeofcontact_validation)) {

							report.updateTestLog("Preferred mode of contact validation:",
									Preferredmode + " is displayed as Expected", Status.PASS);
						} else {
							report.updateTestLog("Preferred mode of contact validation:",
									Preferredmode + " is not displayed as expected", Status.FAIL);
						}

					} else {
						WebElement Modeofcontact = driver.findElement(By.xpath(
								"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
						String Modeofcontact_validation = Modeofcontact.getText();
						if (Preferredmode.contains(Modeofcontact_validation)) {

							report.updateTestLog("Preferredmode of contact validation:",
									Preferredmode + " is displayed as Expected", Status.PASS);
						} else {
							report.updateTestLog("Preferredmode of contact validation:",
									Preferredmode + " is not displayed as expected", Status.FAIL);
						}

					}

				}
			}
			// ***************Condition Details***************//

			JavascriptExecutor j = (JavascriptExecutor) driver;
			j.executeScript("window.scrollBy(0,100)", "");
			sleep(500);
			// ****What is your illness/injury?******\\

			WebElement injury = driver
					.findElement(By.xpath("//label[text()='What is your illness/injury? ']/following-sibling::label"));
			String illness_injury_validation = injury.getText();
			if (Typeofinjury.contains(illness_injury_validation)) {

				report.updateTestLog("Type of illness/injury validation:", Typeofinjury + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Type of illness/injury validation:",
						Typeofinjury + " is not displayed as expected", Status.FAIL);
			}

			// ****When did you first experience symptoms? *******\\

			WebElement Symptoms = driver.findElement(
					By.xpath("//label[text()='When did you first experience symptoms? ']/following-sibling::label"));
			String Symptom_validation = Symptoms.getText();
			if (symptom.contains(Symptom_validation)) {

				report.updateTestLog("When did you first experience symptom validation:",
						symptom + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("When did you first experience symptom validation:",
						symptom + " is not displayed as expected", Status.FAIL);
			}

			// ****Date illness/injury diagnosed ********\\

			WebElement diagoneseddate = driver
					.findElement(By.xpath("//label[text()='Date illness/injury diagnosed ']/following-sibling::label"));
			String Datediagnosed_validation = diagoneseddate.getText();
			if (dateofinjury.contains(Datediagnosed_validation)) {

				report.updateTestLog("illness/injury diagnosed validation:", dateofinjury + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("illness/injury diagnosed validation:",
						dateofinjury + " is not displayed as expected", Status.FAIL);
			}

			// ***Additional Details*****\\

			JavascriptExecutor k = (JavascriptExecutor) driver;
			k.executeScript("window.scrollBy(0,100)", "");
			sleep(500);
			// *****Were you continuously employed for at least 180 days prior
			// to your last day of work? ***//
			WebElement work = driver.findElement(By.xpath(
					"//label[text()='Were you continuously employed for at least 180 days prior to your last day of work? ']/following-sibling::label"));
			String Lastdayofwork_validation = work.getText();
			if (Lastdayofwork.equalsIgnoreCase("Yes")) {

				if (Lastdayofwork.contains(Lastdayofwork_validation)) {

					report.updateTestLog("Last day of work validation:", Lastdayofwork + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Last day of work validation:",
							Lastdayofwork + " is not displayed as expected", Status.FAIL);
				}

			} else {
				if (Lastdayofwork.contains(Lastdayofwork_validation)) {

					report.updateTestLog("Last day of work validation:", Lastdayofwork + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Last day of work validation:",
							Lastdayofwork + " is not displayed as expected", Status.FAIL);
				}
			}

			// ****Last day of work*******\\
			WebElement Lastdaydates = driver
					.findElement(By.xpath("//label[text()='Last day of work ']/following-sibling::label"));
			String Lastdaydate_validation = Lastdaydates.getText();
			if (Lastdaydate.contains(Lastdaydate_validation)) {

				report.updateTestLog("Last day of work date validation:", Lastdaydate + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Last day of work date validation:", Lastdaydate + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Date of return to work (if known) ********\\
			WebElement Dateofreturn = driver.findElement(
					By.xpath("//label[text()='Date of return to work (if known) ']/following-sibling::label"));
			String Dateofreturn_validation = Dateofreturn.getText();
			if (dateofreturn.contains(Dateofreturn_validation)) {

				report.updateTestLog("Date of Return to work validation:", dateofreturn + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Date of Return to work validation:",
						dateofreturn + " is not displayed as expected", Status.FAIL);
			}
			JavascriptExecutor l = (JavascriptExecutor) driver;
			l.executeScript("window.scrollBy(0,100)", "");
			sleep(500);

			// ****What is your usual occupation? ********\\

			WebElement occupation = driver.findElement(
					By.xpath("//label[text()='What is your usual occupation? ']/following-sibling::label"));
			String Occupation_validation = occupation.getText();
			if (Occupation.contains(Occupation_validation)) {

				report.updateTestLog("Occupation validation :", Occupation + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Occupation validation :", Occupation + " is not displayed as expected",
						Status.FAIL);
			}

			// *************************************************************//
			// *****************Medical Practitioner Details*****************//
			// *************************************************************//

			// ****Medical practitioner name*****\\

			WebElement name = driver
					.findElement(By.xpath("//label[text()='Medical practitioner name ']/following-sibling::label"));
			String Medical_validation = name.getText();
			if (Medicalpractitionername.contains(Medical_validation)) {

				report.updateTestLog("Medical practitioner name validation:",
						Medicalpractitionername + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Medical practitioner name validation:",
						Medicalpractitionername + " is not displayed as expected", Status.FAIL);
			}

			// ****Medical practitioner phone number******\\

			WebElement phonenumber = driver.findElement(
					By.xpath("//label[text()='Medical practitioner phone number ']/following-sibling::label"));
			String Medicalpractitionerphone_validation = phonenumber.getText();
			if (Medicalparctionerphn.contains(Medicalpractitionerphone_validation)) {

				report.updateTestLog("Medical practitioner phone number validation:",
						Medicalparctionerphn + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Medical practitioner phone number validation:",
						Medicalparctionerphn + " is not displayed as expected", Status.FAIL);
			}

			// ****Medical practitioner address 1******\\

			WebElement partitioneradd = driver.findElement(
					By.xpath("//label[text()='Medical practitioner address 1 ']/following-sibling::label"));
			String partitioneradd_validation = partitioneradd.getText();
			if (practitioneradd.contains(partitioneradd_validation)) {

				report.updateTestLog("Medical practitioner address 1 validation:",
						practitioneradd + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Medical practitioner address 1 validation:",
						practitioneradd + " is not displayed as expected", Status.FAIL);
			}

			JavascriptExecutor jj = (JavascriptExecutor) driver;
			jj.executeScript("window.scrollBy(0,500)", "");
			sleep(500);

			// ****Medical practitioner address 2******\\

			WebElement partitioneraddress = driver.findElement(
					By.xpath("//label[text()='Medical practitioner address 2 ']/following-sibling::label"));
			String partitioneraddress_validation = partitioneraddress.getText();
			if (practitioneraddress.contains(partitioneraddress_validation)) {

				report.updateTestLog("Medical practitioner address 2 validation:",
						practitioneraddress + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Medical practitioner address 2 validation:",
						practitioneraddress + " is not displayed as expected", Status.FAIL);
			}

			WebElement anotherdoc = driver.findElement(By.xpath(
					"//label[text()='Are you seeing another medical practitioner for your condition? ']/following-sibling::label"));
			String Anotherdoctor_validation = anotherdoc.getText();
			if (AnotherMedicalpractitioner.equalsIgnoreCase("Yes")) {

				// ***Are you seeing another medical practitioner for your
				// condition?**//

				if (AnotherMedicalpractitioner.contains(Anotherdoctor_validation)) {

					report.updateTestLog("Another medical practitioner validation:",
							AnotherMedicalpractitioner + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Another medical practitioner validation:",
							AnotherMedicalpractitioner + " is not displayed as expected", Status.FAIL);
				}

				// ****Medical practitioner name*****\\

				WebElement AnotherPractitionername = driver.findElement(
						By.xpath("//label[text()='Other medical practitioner name ']/following-sibling::label"));
				String AnotherPractitionername_validation = AnotherPractitionername.getText();
				if (Medicalpractitionername.contains(AnotherPractitionername_validation)) {

					report.updateTestLog("Another Medical practitioner name validation:",
							Medicalpractitionername + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Another Medical practitioner name validation:",
							Medicalpractitionername + " is not displayed as expected", Status.FAIL);
				}

				// ****Medical practitioner phone number******\\
				WebElement AnotherPractitionerphoneno = driver.findElement(By
						.xpath("//label[text()='Other medical practitioner phone number ']/following-sibling::label"));
				String AnotherPractitionerphoneno_validation = AnotherPractitionerphoneno.getText();
				if (Medicalparctionerphn.contains(AnotherPractitionerphoneno_validation)) {

					report.updateTestLog("Another  Medical practitioner phone number validation:",
							Medicalparctionerphn + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Another  Medical practitioner phone number validation:",
							Medicalparctionerphn + " is not displayed as expected", Status.FAIL);
				}

				// ****Medical practitioner address 1******\\
				WebElement practitioneraddr = driver.findElement(
						By.xpath("//label[text()='Other medical practitioner address1 ']/following-sibling::label"));
				String practitioneraddr1_validation = practitioneraddr.getText();
				if (practitioneradd.contains(practitioneraddr1_validation)) {

					report.updateTestLog("Another  Medical practitioner address 1 validation:",
							practitioneradd + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Another  Medical practitioner address 1 validation:",
							practitioneradd + " is not displayed as expected", Status.FAIL);
				}

				// ****Medical practitioner address 2******\\
				WebElement practitioneraddrr = driver.findElement(
						By.xpath("//label[text()='Other medical practitioner address2 ']/following-sibling::label"));
				String practitioneraddrr_validation = practitioneraddrr.getText();
				if (practitioneraddress.contains(practitioneraddrr_validation)) {

					report.updateTestLog("Date of Return to work validation:",
							practitioneraddress + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Date of Return to work validation:",
							practitioneraddress + " is not displayed as expected", Status.FAIL);
				}

			} else {
				// ***Are you seeing another medical practitioner for your
				// condition?**//
				if (AnotherMedicalpractitioner.contains(Anotherdoctor_validation)) {

					report.updateTestLog("Another medical practitioner validation:",
							AnotherMedicalpractitioner + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Another medical practitioner validation:",
							AnotherMedicalpractitioner + " is not displayed as expected", Status.FAIL);
				}
			}

			// ************Are you in receipt of any other insurance benefits
			// for your condition? *****//

			WebElement insurerbenefit = driver.findElement(By.xpath(
					"//label[text()='Are you in receipt of any other insurance benefits for your condition? ']/following-sibling::label"));
			String insurerbenefit_validation = insurerbenefit.getText();
			if (Insurancebenefit.equalsIgnoreCase("Yes") && InsuredDetail.equalsIgnoreCase("Yes")) {

				if (Insurancebenefit.contains(insurerbenefit_validation)) {

					report.updateTestLog("Receipt of any other insurance benefits for your condition validation:",
							Insurancebenefit + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Receipt of any other insurance benefits for your condition validation:",
							Insurancebenefit + " is not displayed as expected", Status.FAIL);
				}

				// ****Name of insurer*****\\
				WebElement Medicalpractitionerinsurername = driver
						.findElement(By.xpath("//label[text()='Name of insurer ']/following-sibling::label"));
				String Medicalpractitionerinsurername_validation = Medicalpractitionerinsurername.getText();
				if (Nameofinsurer.contains(Medicalpractitionerinsurername_validation)) {

					report.updateTestLog("Name of insurer validation:", Nameofinsurer + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Name of insurer validation:", Nameofinsurer + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Claim number******\\
				WebElement Claimnumbers = driver
						.findElement(By.xpath("//label[text()='Claim number ']/following-sibling::label"));
				String Claimnumber_validation = Claimnumbers.getText();
				if (Claimnumber.contains(Claimnumber_validation)) {

					report.updateTestLog("Claim number validation:", Claimnumber + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Claim number validation:", Claimnumber + " is not displayed as expected",
							Status.FAIL);
				}

				// ****Insurer address 1******\\
				WebElement insureraddr = driver
						.findElement(By.xpath("//label[text()='Address 1 ']/following-sibling::label"));
				String insureraddr_validation = insureraddr.getText();
				if (insurerAddress.contains(insureraddr_validation)) {

					report.updateTestLog("Insurer address 1 validation :", insurerAddress + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Insurer address 1 validation :",
							insurerAddress + " is not displayed as expected", Status.FAIL);
				}
				sleep(500);
				// ****Insurer address 2******\\
				WebElement insurerAddres = driver
						.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[2]"));
				String insurerAddres_validation = insurerAddres.getText();
				if (insurerAddresss.contains(insurerAddres_validation)) {

					report.updateTestLog("Insurer address 2 validation :",
							insurerAddresss + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Insurer address 2 validation :",
							insurerAddresss + " is not displayed as expected", Status.FAIL);
				}
				if (Insurancebenefit.equalsIgnoreCase("Yes") && InsuredDetail.equalsIgnoreCase("No")) {

					sleep(500);
					// ****Insurer address 2******\\
					WebElement insurerAddresr = driver
							.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[3]"));
					String insurerAddresr_validation = insurerAddresr.getText();
					if (insurerAddresss.contains(insurerAddresr_validation)) {

						report.updateTestLog("Insurer address 2 validation :",
								insurerAddresss + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Insurer address 2 validation :",
								insurerAddresss + " is not displayed as expected", Status.FAIL);
					}
				}
			} else {
				// ***Are you in receipt of any other insurance benefits for
				// your condition? **//

				if (Insurancebenefit.contains(insurerbenefit_validation)) {

					report.updateTestLog(
							"Are you in receipt of any other insurance benefits for your condition validation:",
							Insurancebenefit + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog(
							"Are you in receipt of any other insurance benefits for your condition validation:",
							Insurancebenefit + " is not displayed as expected", Status.FAIL);
				}
			}

			sleep(1000);
			JavascriptExecutor jss = (JavascriptExecutor) driver;
			jss.executeScript("window.scrollBy(0,500)", "");
			sleep(500);

			// *************************************//
			// ******Required Documentation********//
			// ************************************//
			// ****Attachpath******\\

			if (Deathcertificate.equalsIgnoreCase("Yes")) {

				WebElement medical = driver
						.findElement(By.xpath("//table//td[text()='Medical Statement']/./following::td"));
				String Attachment_validation = medical.getText();
				if (Attachpath.contains(Attachment_validation)) {

					report.updateTestLog("Medical statement name validation:", Attachpath + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Medical statement name validation:",
							Attachpath + " is not displayed as expected", Status.FAIL);
				}

			}
			if (Otherdocument.equalsIgnoreCase("Yes")) {
				WebElement otherdoc = driver.findElement(By.xpath("//table//td[text()='Other']/./following::td"));
				String otherdoc_validation = otherdoc.getText();
				if (Attachpath.contains(otherdoc_validation)) {

					report.updateTestLog("Other document name validation:", Attachpath + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Other document name validation:",
							Attachpath + " is not displayed as expected", Status.FAIL);
				}

			}

			if (Powerofattornydoc.equalsIgnoreCase("Yes")) {
				WebElement power = driver
						.findElement(By.xpath("//table//td[text()='Power of Attorney']/./following::td"));
				String power_validation = power.getText();
				if (Attachpath.contains(power_validation)) {

					report.updateTestLog("Power of Attorny document name validation:",
							Attachpath + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Power of Attorny document name validation:",
							Attachpath + " is not displayed as expected", Status.FAIL);
				}

			}

			sleep(1000);
			JavascriptExecutor jsws = (JavascriptExecutor) driver;
			jsws.executeScript("window.scrollBy(0,800)", "");
			sleep(500);

			// ***Agree button**//
			/*
			wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
			sleep(500);
			report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);

*/
			// ***Submit button**//
/*
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
			sleep(500);
			report.updateTestLog("Acknowledgement checkbox", "Checkbox is clicked", Status.PASS);
			*/
/*
			// ******Download the PDF***********\\
			properties = Settings.getInstance();
			String StrBrowseName = properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(2000);
			}
			if (StrBrowseName.equalsIgnoreCase("Firefox")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(1500);
				Robot roboobj = new Robot();
				roboobj.keyPress(KeyEvent.VK_ENTER);
			}

			if (StrBrowseName.equalsIgnoreCase("InternetExplorer")) {
				Robot roboobj = new Robot();
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();

				sleep(1500); 
			}

			report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);
			
			*/

		//	sleep(3000);
			/*
			JavascriptExecutor jf = (JavascriptExecutor) driver;
			jf.executeScript("window.scrollBy(0,900)", "");
			sleep(500);
			*/

			// ***Finish button**//
/*
			wait.until(ExpectedConditions.elementToBeClickable(
					By.xpath("//div[@class='col-xs-6 col-sm-2 right-col']/button[@type='submit']"))).click();
			sleep(1500);
			report.updateTestLog("Finish button:", "Finish button is clicked", Status.PASS);
			sleep(500);
		*/

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

public void Eclaims_injurynegativescenario(){
	try{
	
		String Creditcard = dataTable.getData("Eclaims_Injury", "Creditcard");
		String Title = dataTable.getData("Eclaims_Injury", "Title");
		String DOB = dataTable.getData("Eclaims_Injury", "DOB");
		String Name = dataTable.getData("Eclaims_Injury", "Name");
		String Surname = dataTable.getData("Eclaims_Injury", "Surname");
		String Address = dataTable.getData("Eclaims_Injury", "Address1");
		String Addresss = dataTable.getData("Eclaims_Injury", "Address2");
		String suburb = dataTable.getData("Eclaims_Injury", "suburb");
		String state = dataTable.getData("Eclaims_Injury", "State");
		String postcode = dataTable.getData("Eclaims_Injury", "Postcode");
		String Relationship = dataTable.getData("Eclaims_Injury", "Relationship");
		String givenTitle = dataTable.getData("Eclaims_Injury", "givenTitle");
		String Givenname = dataTable.getData("Eclaims_Injury", "Givenname");
		String Givensurname = dataTable.getData("Eclaims_Injury", "Givensurname");
		String PreferredPhoneNO = dataTable.getData("Eclaims_Injury", "PreferredPhoneNO");
		String Otherphoneno = dataTable.getData("Eclaims_Injury", "Otherphoneno");
		String Email = dataTable.getData("Eclaims_Injury", "Email");
		String Preferredmode = dataTable.getData("Eclaims_Injury", "Preferredmode");
		String Contacttime = dataTable.getData("Eclaims_Injury", "Contacttime");
		String symptom = dataTable.getData("Eclaims_Injury", "Symptom");
		String dateofinjury = dataTable.getData("Eclaims_Injury", "dateofinjury");
		String Medicalpractitionername = dataTable.getData("Eclaims_Injury", "Medicalpractitionername");
		String Medicalparctionerphn = dataTable.getData("Eclaims_Injury", "Medicalpractitionerphoneno");
		String practitioneradd = dataTable.getData("Eclaims_Injury", "practitioneradd");
		String practitioneraddress = dataTable.getData("Eclaims_Injury", "practitioneraddress");
		String Deathcertificate = dataTable.getData("Eclaims_Injury", "Deathcertificate");
		String Attachpath = dataTable.getData("Eclaims_Injury", "Attachpath");

		String Addresssame = dataTable.getData("Eclaims_Injury", "Addresssame");
		String InsuredDetail = dataTable.getData("Eclaims_Injury", "InsuredDetail");
		String Relationshiptoinsurer = dataTable.getData("Eclaims_Injury", "Relationshiptoinsurer");
		String Relationshipdetails = dataTable.getData("Eclaims_Injury", "Relationshipdetails");
		String insuredDOB = dataTable.getData("Eclaims_Injury", "insuredDOB");
		String Typeofinjury = dataTable.getData("Eclaims_Injury", "Typeofinjury");	
		String Details = dataTable.getData("Eclaims_Injury", "Details");
		String Doberror = dataTable.getData("Eclaims_Injury", "Doberror");
		String emailerror = dataTable.getData("Eclaims_Injury", "emailerror");
		String phonenoerror = dataTable.getData("Eclaims_Injury", "phonenoerror");
		String Otherphonenoerror = dataTable.getData("Eclaims_Injury", "Otherphonenoerror");	
		String Postcodeerror = dataTable.getData("Eclaims_Injury", "Postcodeerror");
		String Symptomdateerror = dataTable.getData("Eclaims_Injury", "Symptomdateerror");
		String Diagnosederror = dataTable.getData("Eclaims_Injury", "Diagnosederror");	
		String Greaterthanerror = dataTable.getData("Eclaims_Injury", "Greaterthanerror");
				

		
		WebDriverWait wait = new WebDriverWait(driver, 18);

		// ****Click on claim type*****\\

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-DISABILITY']/parent::*")))
				.click();
		sleep(500);
		report.updateTestLog("Claim Type:", "Injury/Illness claim is Selected", Status.PASS);

		// ****Enter creditcard number*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
		report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",
				Creditcard + " is entered", Status.PASS);

		if (InsuredDetail.equalsIgnoreCase("Yes")) {

			// ****Primary Account Holder*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Primary Account Holder", "Yes is Selected", Status.PASS);

			// ****Select title*****\\

			Select titletype = new Select(
					driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
			titletype.selectByVisibleText(Title);
			report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

			// ****Enter DOB*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
					.sendKeys(DOB);
			report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

			// ****Enter Name*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
					.sendKeys(Name);
			report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

			// ****Enter surname*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
					.sendKeys(Surname);
			report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

			// ****Enter Address1*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
					.sendKeys(Address);
			report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

			// ****Enter Address2*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
					.sendKeys(Addresss);
			report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

			// ****Enter Suburb*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
					.sendKeys(suburb);
			report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

			// ****Select State*****\\

			Select Stateselect = new Select(
					driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
			Stateselect.selectByVisibleText(state);
			report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
			sleep(500);

			// ****Enter Postal code*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
					.sendKeys(postcode);
			sleep(500);
			report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
			sleep(300);

			// ****Enter Emailaddress*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ213_0")))
					.sendKeys(Email);
			report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

			// ****Enter Peferred phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ211_1")))
					.sendKeys(PreferredPhoneNO);
			sleep(500);
			report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
			sleep(500);

			// ****Enter other phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ210_2")))
					.sendKeys(Otherphoneno);
			sleep(500);
			report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
			sleep(500);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,600)", "");
			sleep(500);

			// ****Preferred mode of contact (Phone / Email /
			// Letter)?*****\\

			if (Preferredmode.equalsIgnoreCase("Phone")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*")))
						.click();
				sleep(1000);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

				Select mode = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ215_1")));
				mode.selectByVisibleText(Contacttime);
				report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
						Contacttime + " is selected ", Status.PASS);
				sleep(500);
			}
			if (Preferredmode.equalsIgnoreCase("Email")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			}

		} else {

			// ****Primary Account Holder*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Primary Account Holder", "No is Selected", Status.PASS);

			// ****Select title*****\\

			Select titletype = new Select(
					driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
			titletype.selectByVisibleText(Title);
			report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

			// ****Enter DOB*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
					.sendKeys(DOB);
			report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

			// ****Enter Name*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
					.sendKeys(Name);
			report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

			// ****Enter surname*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
					.sendKeys(Surname);
			report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

			// ****Enter Address1*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
					.sendKeys(Address);
			report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

			// ****Enter Address2*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
					.sendKeys(Addresss);
			report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

			// ****Enter Suburb*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
					.sendKeys(suburb);
			report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

			// ****Select State*****\\

			Select Stateselect = new Select(
					driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
			Stateselect.selectByVisibleText(state);
			report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
			sleep(500);

			// ****Enter Postal code*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
					.sendKeys(postcode);
			sleep(500);
			report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
			sleep(300);

			// *******************************//
			// *****Claimant Details***********
			// *******************************//

			// ***Relationship to insured****//
			if (Relationshiptoinsurer.equalsIgnoreCase("Powerofattorney")) {
				Select insurerselect = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ400_0")));
				insurerselect.selectByVisibleText(Relationshipdetails);
				report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
				sleep(500);

				// ****Select title*****\\

				Select title = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ401_2")));
				title.selectByVisibleText(Title);
				report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

				// ****Enter Given name*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ403_0")))
						.sendKeys(Givenname);
				report.updateTestLog("Enter Given name:", Givenname + " is entered", Status.PASS);
				sleep(500);

				// ****Enter surname*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ404_1")))
						.sendKeys(Givensurname);
				report.updateTestLog("Enter Surname name:", Givensurname + " is entered", Status.PASS);
				sleep(500);

				// ****Enter Relationship to insured DOB******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11058_2")))
						.sendKeys(insuredDOB);
				report.updateTestLog("Enter Relationship to insured DOB :", insuredDOB + " is entered",
						Status.PASS);
				sleep(500);

			}
			if (Relationshiptoinsurer.equalsIgnoreCase("others")) {
				Select insurerselect = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ400_0")));
				insurerselect.selectByVisibleText(Relationshipdetails);
				report.updateTestLog("Select Title:", Title + " is selected ", Status.PASS);
				sleep(500);

				// ****Relationship details*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11054_1")))
						.sendKeys(Details);
				report.updateTestLog("Relationship details :", Details + " is entered", Status.PASS);
				sleep(500);

				// ****Select title*****\\

				Select title = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ401_2")));
				title.selectByVisibleText(Title);
				report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

				// ****Enter Given name*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ403_0")))
						.sendKeys(Givenname);
				report.updateTestLog("Enter Given name:", Givenname + " is entered", Status.PASS);
				sleep(500);

				// ****Enter surname*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ404_1")))
						.sendKeys(Givensurname);
				report.updateTestLog("Enter Surname name:", Givensurname + " is entered", Status.PASS);
				sleep(500);

				// ****Enter Relationship to insured DOB******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11058_2")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ11058_2")))
						.sendKeys(insuredDOB);
				report.updateTestLog("Enter Relationship to insured DOB :", insuredDOB + " is entered",
						Status.PASS);
				sleep(500);

			}

			// ****Is your address the same as the primary account
			// holder?*****\\
			if (Addresssame.equalsIgnoreCase("Yes")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-Yes']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Address same is selected", "Selected Yes", Status.PASS);
				sleep(500);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-No']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Address same as primary accont hoder", "Selected No", Status.PASS);
				sleep(500);

				// ****Enter Address1*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ409_1")))
						.sendKeys(Address);
				report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

				// ****Enter Address2*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ410_2")))
						.sendKeys(Addresss);
				report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

				// ****Enter Suburb*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ411_0")))
						.sendKeys(suburb);
				report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

				// ****Select State*****\\

				Select Stateprimaryselect = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ412_1")));
				Stateprimaryselect.selectByVisibleText(state);
				report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
				sleep(500);

				// ****Enter Postal code*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ413_2")))
						.sendKeys(postcode);
				sleep(500);
				report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
				sleep(300);
			}

			// ****Enter Emailaddress*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ402_0")))
					.sendKeys(Email);
			report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

			// ****Enter Peferred phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ415_1")))
					.sendKeys(PreferredPhoneNO);
			sleep(500);
			report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
			sleep(500);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,600)", "");
			sleep(500);
			// ****Enter other phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ414_2")))
					.sendKeys(Otherphoneno);
			sleep(500);
			report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
			sleep(500);

			// ****Preferred mode of contact (Phone / Email /
			// Letter)?*****\\

			if (Preferredmode.equalsIgnoreCase("Phone")) {

				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ416-Phone']/parent::*")))
						.click();
				sleep(700);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

				Select mode = new Select(
						driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ417_1")));
				mode.selectByVisibleText(Contacttime);
				report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
						Contacttime + " is selected ", Status.PASS);
				sleep(500);
			}
			if (Preferredmode.equalsIgnoreCase("Email")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ416-Email']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ416-Letter']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			}

		}

		// ***************Condition Details***************//

		// ****What is your illness/injury?******\\

		wait.until(
				ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input-datalist_CQ10001_0")))
				.sendKeys(Typeofinjury);
		sleep(1000);
		report.updateTestLog("What is your illness/injury? :", Typeofinjury + " is entered", Status.PASS);

		// ****When did you first experience symptoms? *******\\

		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ10002_0")))
				.sendKeys(symptom);
		sleep(500);
		report.updateTestLog("When did you first experience symptoms? :", symptom + " is entered", Status.PASS);

		// ****Date illness/injury diagnosed ********\\

		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ10003_1")))
				.sendKeys(dateofinjury);
		sleep(500);
		report.updateTestLog("Date illness/injury diagnosed :", dateofinjury + " is entered", Status.PASS);

		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//label[text()='When did you first experience symptoms? ']")))
				.click();
		sleep(500);
		
		

		// ****Proceed button******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
		sleep(500);
		report.updateTestLog("Proceed Button", "clicked", Status.PASS);	
		
	
		WebElement postcodeerrormessage = driver.findElement(
				By.xpath("(//div[@class='ng-scope'])[2]"));
		WebElement Doberrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[1]"));
		WebElement emailerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[2]"));		
		WebElement phonenoerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[3]"));
		WebElement otherphonenoerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[4]"));			
		WebElement Symptomdateerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[5]"));
		WebElement Diagnosederrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[6]"));
	
		String Doberror_validation = Doberrormessage.getText();
		String emailerror_validation = emailerrormessage.getText();
		String phonenoerror_validation = phonenoerrormessage.getText();		
		String otherphonenoerror_validation  = otherphonenoerrormessage.getText();
		String Symptomdate_validation  = Symptomdateerrormessage.getText();
		String Diagnosederror_validation = Diagnosederrormessage.getText();
	//	String Mandatoryerrormessage_validation = Mandatoryerror.getText();
		String postcodeerror_validation  = postcodeerrormessage.getText();
		
		
		
		if (Doberror.contains(Doberror_validation)) {

			report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is not displayed as expected",
					Status.FAIL);
		}
		
		
		if (Postcodeerror.contains(postcodeerror_validation)) {

			report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is not displayed as expected",
					Status.FAIL);
		}
		
		if (emailerror.contains(emailerror_validation)) {

			report.updateTestLog("Email address Error validation:", emailerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Email address Error validation:", emailerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (phonenoerror.contains(phonenoerror_validation)) {

			report.updateTestLog("Phone number Error message validation:", phonenoerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Phone number Error message validation:", phonenoerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (Otherphonenoerror.contains(otherphonenoerror_validation)) {

			report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (Symptomdateerror.contains(Symptomdate_validation)) {

			report.updateTestLog("Future Symptom Date of Death Error message validation:", Symptomdateerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Future Symptom Date of Death Error message validation:", Symptomdateerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (Diagnosederror.contains(Diagnosederror_validation)) {

			report.updateTestLog("Diagnosed Date of Death Error message validation:", Diagnosederror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Diagnosed Date of Death Error message validation:", Diagnosederror + " is not displayed as expected",
					Status.FAIL);
		}
	/*	if (Greaterthanerror.contains(Mandatoryerrormessage_validation)) {

			report.updateTestLog("Injury diagnosed date should be greater than symptoms date validation ", Greaterthanerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Injury diagnosed date should be greater than symptoms date validation ", Greaterthanerror + " is not displayed as expected",
					Status.FAIL);
		}
*/

}catch (Exception e) {
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
}
}
