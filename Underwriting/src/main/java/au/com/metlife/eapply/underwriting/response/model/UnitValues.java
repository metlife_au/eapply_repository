package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UnitValues implements Serializable{
	private String position;
	private String unit;
	private String value;
	
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
