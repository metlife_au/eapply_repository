package pages.metlife;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.MetlifeINGDirPayloadPageObjects;

public class INGDManageInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public INGDManageInsurancePage selectInsuranceCoverOption() {
		manageINGDInsuranceCover();
		return new INGDManageInsurancePage(scriptHelper);
	}
	
	public INGDManageInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void manageINGDInsuranceCover(){
		try {
			
			sleep();
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkChangeyourcover), "click", "Change Insurance - Cover Details");
			//sleep();
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action manageINGDInsuranceCover " + e.getMessage(), Status.FAIL);
		}
	}
}
