package pages.metlife;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import java.util.List;
import supportlibraries.ScriptHelper;
import uimap.metlife.CoverDetailsPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;

public class STFTGetQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTGetQuotePage getQuote() {
		premiumCalculation();
		return new STFTGetQuotePage(scriptHelper);
	}

	public STFTGetQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void premiumCalculation() {

		String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String lastName = dataTable.getData("MetLife_Data", "LastName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCitizen = dataTable.getData("MetLife_Data", "Citizen");
		String strIndustry = dataTable.getData("MetLife_Data", "Industry");
		String stroccupation = dataTable.getData("MetLife_Data", "Occupation");
		String strlifeCoverAmt = dataTable.getData("MetLife_Data", "LifeCoverAmount");
		String strTotalCoverAmt = dataTable.getData("MetLife_Data", "TotalCoverAmount");
		String strPaymentFreq = dataTable.getData("MetLife_Data", "PaymentFrequency");
		String strBrokerClient = dataTable.getData("MetLife_Data", "BrokerClient");
    	String strTCID = dataTable.getData("MetLife_Data", "TC_ID");
    
		try {
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			
			Thread.sleep(500);
			
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtLastName), "SET", "Last Name", lastName);
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtLastName), "TAB", "Last Name");
			Thread.sleep(2000);
			
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.txtDOB), "SET", "Date of birth", getPremiumDOB(Integer.parseInt("-"+dob)));
			Thread.sleep(1000);
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.txtDOB), "TAB", "Date of birth");
			
			Thread.sleep(1000);
			
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
			} else {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdFemale), "clkradio", "Gender -" + gender);
			}
			
			Thread.sleep(1000);
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdSmokedIn12MthsYes), "clkradio",
						"Have you Smoked in 12 months - Yes");
			} else {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdSmokedIn12MthsNo), "clkradio",
						"Have you Smoked in 12 months - No");
			}
			
			Thread.sleep(1000);
			
			if (strCitizen.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdCitizenYes), "Click",
						"Are you Citizen or PR - Yes");

			} else {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdCitizenNo), "Click",
						"Are you Citizen or PR - No");
			}
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.selIndustry), "specialDropdown", "Industry",strIndustry);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.selOccupation), "specialDropdown", "Occupation",stroccupation);		
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.txtLifeCoverAmt), "ENTER", "Cover Amount");
			
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.txtLifeCoverAmt), "SET", "Cover Amount", strlifeCoverAmt);
			Thread.sleep(1000);
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtTotalCoverAmt), "ENTER", "Total Amount");
			Thread.sleep(1000);
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtTotalCoverAmt), "SET", "Total Amount", strTotalCoverAmt);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.txtTotalCoverAmt), "TAB", "Total Amount");
			Thread.sleep(1000);
			tryAction(fluentWaitElement(STFTGetQuotePageObjects.selFrequency), "specialDropdown", "Payment Frequency",strPaymentFreq);
			
			if (strBrokerClient.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdBrokerYes), "clkradio",
						"Are you SteadFast Broker Client - Yes");

			} else {
				tryAction(waitForClickableElement(STFTGetQuotePageObjects.rdBrokerNo), "clkradio",
						"Are you SteadFast Broker Client - No");
			}	
			
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTGetQuotePageObjects.btnCalculateQuote)).click().perform();
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.btnCalculateQuote), "Click", "Apply");
			Thread.sleep(2000);
			
			//getPremiumsFromScreen();
			
		/*				
			String dyn_dcCostAmt1 = driver.findElement(By.xpath("//*[@id='j_id366']/div[1]/div[2]/table/tbody/tr[1]/td[2]/strong")).getText();
			String dyn_dcCostAmt = dyn_dcCostAmt1.substring(1, dyn_dcCostAmt1.length()-14);
			String dyn_tpdCostAmt1 = driver.findElement(By.xpath("//*[@id='j_id366']/div[1]/div[2]/table/tbody/tr[2]/td[2]/strong")).getText();
			String dyn_tpdCostAmt = dyn_tpdCostAmt1.substring(1, dyn_dcCostAmt1.length()-14);
		    System.out.println(dyn_dcCostAmt);
		    System.out.println(dyn_tpdCostAmt);*/
//			if(strTCID.contains("PremiumCalculation")){
//				assertEqualText(dataTable.getData("MetLife_Data", "PolicyFee"),getStringValueFromSessionThreadData("dyn_PFCostAmt").substring(1,getStringValueFromSessionThreadData("dyn_PFCostAmt").indexOf(" per")));
//				if(getStringValueFromSessionThreadData("dyn_TPDCostAmt").contains("per")){
//					assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1,getStringValueFromSessionThreadData("dyn_TPDCostAmt").indexOf(" per")));
//					
//				}
//				else {
//					assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
//					
//				}
//				assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1,getStringValueFromSessionThreadData("dyn_dcCostAmt").indexOf(" per")));;
//				assertEqualText(dataTable.getData("MetLife_Data", "Estmated_Premium"),getStringValueFromSessionThreadData("dyn_ESTCostAmt").substring(1));
//				}
//			
			sleep(1000);
			//Actions actions1 = new Actions(driver);
			//actions1.moveToElement(driver.findElement(STFTGetQuotePageObjects.btnApply)).click().perform();
			tryAction(waitForClickableElement(STFTGetQuotePageObjects.btnApply), "Click", "Apply");
			Thread.sleep(2000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}

	private void getPremiumsFromScreen() {
		// TODO Auto-generated method stub
			
		if (isElementPresent(STFTGetQuotePageObjects.lblDCCostAmt)){
			storeSessionThreadData("dyn_dcCostAmt",fluentWaitElements(STFTGetQuotePageObjects.lblDCCostAmt).getText());
		}
		
		if (isElementPresent(STFTGetQuotePageObjects.lblTPDCostAmt)){
			storeSessionThreadData("dyn_TPDCostAmt",fluentWaitElements(STFTGetQuotePageObjects.lblTPDCostAmt).getText());
		}
		
		if (isElementPresent(STFTGetQuotePageObjects.lblPFCostAmt)){
			storeSessionThreadData("dyn_PFCostAmt",fluentWaitElements(STFTGetQuotePageObjects.lblPFCostAmt).getText());
		}
		
		if (isElementPresent(STFTGetQuotePageObjects.lblEstTotCostAmt)){
			storeSessionThreadData("dyn_ESTCostAmt",fluentWaitElements(STFTGetQuotePageObjects.lblEstTotCostAmt).getText());
		}
		
		System.out.println(getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1,getStringValueFromSessionThreadData("dyn_PFCostAmt").indexOf(" per")));
		
		if(getStringValueFromSessionThreadData("dyn_TPDCostAmt").contains("per")){
			
		System.out.println(getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1,getStringValueFromSessionThreadData("dyn_TPDCostAmt").indexOf(" per")));
				}
		else{
			
			System.out.println(getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
		
		}
			
	    System.out.println(getStringValueFromSessionThreadData("dyn_PFCostAmt").substring(1,getStringValueFromSessionThreadData("dyn_PFCostAmt").indexOf(" per")));
	    System.out.println(getStringValueFromSessionThreadData("dyn_ESTCostAmt").substring(1));
	}
}
