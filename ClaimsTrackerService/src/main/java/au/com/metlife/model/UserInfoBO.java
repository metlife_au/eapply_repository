package au.com.metlife.model;

import java.util.Vector;

public class UserInfoBO {
	
	private String id;	
	private String fundType;	
	private String firstname;
	private String lastname;
	private String title;
	private String gender;
	private java.util.Date dateOfBirth;
	private String  clientRefNumber;
	private java.util.Date  dateJoinedFund;
	private String  additionalInfo;
	private java.util.Vector elodgeDocument;
	private java.lang.String applicationId1;
	
	
	public java.lang.String getApplicationId1() {
		return applicationId1;
	}
	public void setApplicationId1(java.lang.String applicationId1) {
		this.applicationId1 = applicationId1;
	}
	public java.util.Vector getElodgeDocument() {
		return elodgeDocument;
	}
	public void setElodgeDocument(java.util.Vector elodgeDocument) {
		this.elodgeDocument = elodgeDocument;
	}
	public java.util.Date getDateJoinedFund() {
		return dateJoinedFund;
	}
	public void setDateJoinedFund(java.util.Date dateJoinedFund) {
		this.dateJoinedFund = dateJoinedFund;
	}
	public java.util.Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(java.util.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}	
	public String getClientRefNumber() {
		return clientRefNumber;
	}
	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}	
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
