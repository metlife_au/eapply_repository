package au.com.metlife.eapply.bs.constants;

public class MetlifeInstitutionalConstants {
	private MetlifeInstitutionalConstants(){
		
	}
    
	public static final String FUND_ING = "INGD";
	
	public static final String FUND_APSD = "APSD";
	
	public static final String FUND_FIRS = "FIRS";
	
	public static final String FUND_HOST = "HOST";
	
	public static final String FUND_VICS = "VICT";
	
    public static final String APP_NUM = "APPLICATIONNUMBER";
    
    public static final String APP_DECISION = "APP_DECISION";
    
    public static final String TITLE = "TITLE";
    
    public static final String FIRST_NAME = "FIRSTNAME";
    
    public static final String LAST_NAME = "LASTNAME";
    
    public static final String PRODUCTNAME = "PRODUCTNAME";
    
    public static final String PDFLINK = "PDFLINK";
    
    public static final String EMAIL_SMTP_PORT = "EMAIL_SMTP_PORT";
    
    public static final String EMAIL_SMTP_HOST = "EMAIL_SMTP_HOST";
    
    public static final String EMAIL_SMTP_USERID = "EMAIL_SMTP_USERID";
    
    public static final String EMAIL_SMTP_PASS = "EMAIL_SMTP_PASS";
    
    public static final String EMAIL_SUBJECT = "EMAIL_SUBJECT";
    
    public static final String EMAIL_PREFERRED_CONTACT_NUMBER = "EMAIL_PREFERRED_CONTACT_NUMBER";
    
    public static final String EMAIL_OTHER_CONTACT_NUMBER = "EMAIL_OTHER_CONTACT_NUMBER";
    
    public static final String EMAIL_PREFERRED_CONTACT_TIME = "EMAIL_PREFERRED_CONTACT_TIME";
    
    public static final String EMAIL_EMAIL_ADDRESS = "EMAIL_EMAIL_ADDRESS";
    
    public static final String MEMBER_NO = "MEMBER_NO";
    
    public static final String URL_RETRIEVE ="URL_RETRIEVE";
    
    public static final String EMAIL_IND_SUBJECT_NAME = "EMAIL_IND_SUBJECT_NAME";
    
    public static final String INSTITUTIONAL = "INSTITUTIONAL";
    
    public static final String ERROR_CODE = "500";
    
    public static final String RETRIEVEINSTAPPLIST = "retrieveInstitutionalApplicationList";
    
    public static final String RETRIEVEINSTAPP = "retrieveInstitutionalApplication";
    
    public static final String RETRIEVEAPP = "retrieveApplication";
    
    public static final String RETRIEVEAPPLICANT = "retrieveApplicant";
    
    public static final String RETRIEVECONTACTINFO = "retrieveContactInfo";
    
    public static final String RETRIEVECOVERS = "retrieveCovers";
    
    public static final String RETRIEVEFUNDINFO = "retrieveFundInfo";
    
    public static final String RETRIEVEAURADETAILS = "retrieveAuraDetails";
    
    public static final String BEFORERETRIEVE = "BEFORE retrieveApplication";
    
    public static final String AFTERRETRIEVE = "AFTER retrieveApplication";
    
    public static final String RETRIEVEAURA = "retrieveAuraDetails = >";
    
    public static final String RETRIEVEFUND = "retrieveFundInfo = >";
    
    public static final String RETRIEVERULE = "vecOfRuleInfo = >";
    
   /* public static final String REFER_UW = "RUW";*/
    public static final String EMAIL = "Email";
    
    public static final String HOME_PH = "Home Phone";
    
    public static final String WORK_PH = "Work Phone";
    
    public static final String MOBILE_PH = "Mobile";
    
    public static final String COLOR_GRAY = "grey";
    
    public static final String COLOR_GREEN = "green";
    
    public static final String HELVETICA_BOLD = "HELVETICA_BOLD";
    
    public static final String FILE_NOT_FOUND_EXCEPTION = "10020";
    
    public static final String DOCUMENT_EXCEPTION = "10021";
    
    public static final String COLOR_BLACK = "black";
    
    public static final String COLOR_WHITE = "white";
    
    public static final String COLOR_RED = "red";
    
    public static final String COLOR_BLUE = "blue";
    
    public static final String BLANK = "";
    
    public static final String DATE_OF_APPL = "Date of application:";
    
    public static final String PREFF_CON_DTS = "Preferred Contact Details";
    
    public static final String PREFF_CON_METHOD = "Preferred Contact Method:";
    
    public static final String PREFF_CON_TIME = "Preferred Contact Time:";
    
    public static final String MEMBER_DETAILS = "Member Details";
    
    public static final String TITLE_PDF = "Title";
    
    public static final String FIRST_NAME_PDF = "Firstname";
    
    public static final String SURNAME = "Surname";
    
    public static final String DOB = "Date of Birth";
    
    public static final String DJF = "Date Joined Fund";
    
    public static final String GENDER = "Gender";
    
    public static final String MEMBER_NUMBER = "Member number";
    
    public static final String UW_DECISION_BLURB = "UW_DECISION_BLURB";
    
    public static final String UW_DECISION = "UW_DECISION";
    
    public static final String EXCL_LOAD = "EXCL_LOAD";
    
    public static final String PRODUCT_DETAILS = "PRODUCT_DETAILS";
    
    public static final String FREQUENCY = "FREQUENCY";
    
    public static final String INS_APPLICATION_TYPE = "Institutional";
    
    public static final String INDV_APPLICATION_TYPE = "Individual";
    
    public static final String INDV_REPAYMENT_COVER = "Repayment Cover";
    
    public static final String INDV_REPAYMENT_COVER_NO = "181";
    
    public static final String LOAN_PROTECTION_CITI = "PRD027";
    
    public static final String INDV_LOAN_COVER_NO = "180";
    
    public static final String INDV_LOAN_COVER = "Loan Protection";
    
    public static final String DCL_TEXT_DEATH="Based on the information disclosed in your personal statement, we were unable to offer you additional Death. The following disclosures contributed to this decision: ";
    
    public static final String DCL_TEXT_TPD="Based on the information disclosed in your personal statement, we were unable to offer you additional TPD. The following disclosures contributed to this decision: ";
    
    public static final String DCL_TEXT_DEATH_ADD = " Your additional Death cover has been included in the Death only cover component.";
    
	public static final String DCL_TEXT_TRAUMA ="Based on the information disclosed in your personal statement, we were unable to offer you additional Income Protection cover. The following disclosures contributed to this decision: ";

	public static final String DCL_TEXT_IP ="Based on the information disclosed in your personal statement, we were unable to offer you additional Income Protection cover. The following disclosures contributed to this decision: ";
    
    public static final String DCL_TEXT_OVERALL = "Based on the information disclosed in your personal statement, we were unable to offer you additional Insurance cover. The following conditions contributed to this decision: ";    
    
    public static final String DECLARE_PRIVACY_POLICY_OWNER = "<br> <p>I acknowledge and consent to the following declarations below:</p><br> <ul style=\"list-style-type:disc;padding-left:20px;\"> <li>I have read and understood my Duty of Disclosure as explained in the PDS and have not withheld any information material to my application for insurance and understand this duty continues to apply until my application has been accepted;</li> <li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement; and</li> <li>The information provided in this application is true and correct.</li> </ul>";
    
    public static final String DECLARE_PRIVACY_COMPANY = "<br> <p>As representative of the company, I acknowledge and consent to the following declarations below:</p><br> <ul style=\"list-style-type:disc;padding-left:20px;\"> <li>I have read and understand the Product Disclosure Statement (PDS) (including the \"When will a benefit not be paid\" section) and my decision to purchase OAMPS Life is based on the PDS;</li> <li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement;</li> <li>I understand my OAMPS Life policy will not become effective until this application is accepted in writing; and</li> <li>I acknowledge that any direct debit arrangement is governed by the terms of the Direct Debit Request Service Agreement in the PDS and the terms and conditions of my policy.</li> </ul>";
    
    public static final String DECLARE_PRIVACY_SUPER = "<br><p>As representative of the superannuation trustee, I acknowledge and consent to the following declarations below:</p><br><ul style=\"list-style-type:disc;padding-left:20px;\">	<li>I have read and understand the Product Disclosure Statement (PDS) (including the \"When will a benefit not be paid\" section) and my decision to purchase OAMPS Life is based on the PDS;</li>	<li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement;</li><li>I understand my OAMPS Life policy will not become effective until this application is accepted in writing; and</li>	<li>I acknowledge that any direct debit arrangement is governed by the terms of the Direct Debit Request Service Agreement in the PDS and the terms and conditions of my policy.</li></ul>";
    
    public static final String DECLARE_PRIVACY_INDIVIDUAL = "<br><p>I acknowledge and consent to the following declarations below:</p><br><ul style=\"list-style-type:disc;padding-left:20px;\"><li>I have read and understand the Product Disclosure Statement (PDS) (including the \"When will a benefit not be paid\" section) and my decision to purchase OAMPS Life is based on the PDS;</li><li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement;</li><li>I understand my OAMPS Life policy will not become effective until this application is accepted in writing; and</li><li>I acknowledge that any direct debit arrangement is governed by the terms of the Direct Debit Request Service Agreement in the PDS and the terms and conditions of my policy.</li></ul>";
   
    public static final String POLICY_OWNER_COMPANY = "Company";
	public static final String POLICY_OWNER_SUPERANNUATION_TRUSTEE = "Superannuation trustee";
	public static final String POLICY_OWNER_ANOTHER_INDIVIDUAL = "Another individual";
	
	public static final String ALI_SIMPLE_INCOME_PROTECTION="ALI Simple Income Protection";
	
	public static final String ALI_GROUP_SIMPLE_INCOME_PROTECTION="ALI Group's Simple Income Protection";
	
	public static final String FUNERAL_ESTATE_PLAN="Funeral Estate Plan";
	
	public static final String FUNERAL_COVER_PLUS="Funeral Cover Plus";
	
	public static final String METL_FUNERAL_COVER_PLUS="MetLife Funeral Cover Plus";
	
	public static final String PREMIUM_FUNERAL_ESTATE_PLAN="Premium Funeral & Estate Plan";
	
	public static final String RULE_FILE_LOC="RULE_FILE_LOC";
	
	public static final String COVER_NO_219="219";
	
	public static final String COVER_NO_220="220";
	
	public static final String COVER_NO_DEATH_256="256";
	
	public static final String COVER_NO_TPD_257="257";
	
	public static final String ACC_CVRS_STR="$#$ACC_CVRS$#$";
	
	public static final String DCL_CVRS_STR="$#$DCL_CVRS$#$";
	
	public static final String AND_STR=" and ";
	
	public static final String ACC = "ACC";
	
	public static final String DCL = "DCL";
	
	public static final String HTMLEMAILS="HTMLEMAILS";
	
	public static final String GEN_KEY="432rerr32rhWR#432g";
	
	public static final  String DATE_OF_QUOT = "Date of Quotation";
	
	public static final String CONVERT_AND_MAINTAIN_COVER="Convert and maintain cover";
	
	public static final String SWITCH_COVER_TYPE="Switch cover type";
	
	public static final String COVER_OPTION_INCREASE="Increase";
	
	public static final String COVER_OPTION_DECREASE="Decrease";
	
    public static final String DC_CANCEL="DcCancel";
    
    public static final String TPD_CANCEL="TpdCancel";
    
    public static final String IP_CANCEL="IpCancel";
    
    public static final String NO_CHANGE="No change";
    
    public static final String ING_COPY_RIGHT = "<br><P style=\"padding:0 0 0 20px;line-height:17px;\">This information was prepared and sent on behalf of Diversa Trustees Limited ABN 49 006 421 638, AFSL 235153, RSE L0000635, the Trustee of the ING DIRECT Superannuation Fund ABN 13 355 603 448 (Fund) and the issuer of interest in the Fund. ING DIRECT Living Super is a product issued out of the Fund. ING DIRECT, a division of ING BANK (Australia) Limited ABN 24 000 893 292, AFSL 229823, is the Promoter of the Fund. Insurance cover is issued by MetLife Insurance Limited ABN 75 004 274 882 AFSL 238096 to the Trustee.<br></P>";
	
    public static final String KEY1 = "KEY1";
    public static final String KEY2 = "KEY2";
    public static final String P_NAME = "P_NAME";
    public static final String P_LAST_NAME = "P_LAST_NAME";
    public static final String S_LAST_NAME = "S_LAST_NAME";
    public static final String P_NAME1 = "P_NAME1";
    public static final String S_NAME = "S_NAME";
    
    public static final String INPUT_IDENT_ENCR="INPUT_IDENT_ENCR";
    public static final String INPUT_DATA_ENCR="INPUT_DATA_ENCR";
    
    /*Used for sonar fixes*/
    public static final String LOADING = "    Load: (";
    public static final String EXCLUSION = "    Excl: (";
    public static final String UNITS = " Units )";
    public static final String AND = " and ";
    public static final String SPECIALTERMMSG = " with special terms";
    public static final String ERROR_IN_WEBSERVICE = "*** ERROR *** encountered in locating web-service: ";
    public static final String ACCEPT_COVER = "<%ACC_COVER%>";
    public static final String APPLICATIONNUMBER = "<%APPLICATIONNUMBER%>";
    public static final String APPLICATION_DECISION = "<%APP_DECISION%>";
    public static final String COVER = "<%COVER%>";
    public static final String DECLINE_COVER = "<%DCL_COVER%>";
    public static final String DECLINE_REASONS = "<%DCL_REASONS%>";
    public static final String EMAIL_ADDRESS = "<%EMAIL_EMAIL_ADDRESS%>";
    public static final String EMAIL_SUBJECT_NAME = "<%EMAIL_IND_SUBJECT_NAME%>";
    public static final String EMAIL_OTHER_NUMBER = "<%EMAIL_OTHER_CONTACT_NUMBER%>";
    public static final String EMAIL_PREFERRED_NUMBER = "<%EMAIL_PREFERRED_CONTACT_NUMBER%>";
    public static final String EMAIL_PREFERRED_TIME = "<%EMAIL_PREFERRED_CONTACT_TIME%>";
    public static final String FIRSTNAME = "<%FIRSTNAME%>";
    public static final String LASTNAME = "<%LASTNAME%>";
    public static final String MEMBERNO = "<%MEMBER_NO%>";
    public static final String SPECIAL_CONDITION = "<%SPL_COND%>";
    public static final String EMAIL_TITLE = "<%TITLE%>";
    public static final String UWDECISION = "<%UW_DECISION%>";
    public static final String APPLICATION_NUMBER = "APPLICATIONNUMBER";
    public static final String APPDECISION = "APP_DECISION";
    public static final String EMAILSUBJECTNAME = "EMAIL_IND_SUBJECT_NAME";
    public static final String TITLE_NAME = "TITLE";
    public static final String UNDERWRITING_DECISION ="UW_DECISION";
    public static final String EMAILCONTENT = "text/html; charset=utf-8";
    public static final String ACCEPT = "Accept ";
    public static final String ACCEPT_ = "Accept";
    public static final String TPD_AMOUNT = "tpdAmt";
    public static final String IP_AMOUNT = "ipAmt";
    public static final String DEATH_AMOUNT = "deathAmt";
    public static final String COUNTRY = "country";
    public static final String APPNUMBER = "appnumber";
    public static final String PRODUCTS = "</Products>";
    public static final String QUESTIONFILTERS = "</QuestionFilters>";
    public static final String CANCOVER = "CANCOVER";
    public static final String SLASHBRACKET = "\n  ( ";
    public static final String SLASH_BRACKET = "\n ( ";
    public static final String SLASH_N_FIXED = "\n ( Fixed )";
    public static final String SLASH_N_UNITS = "\n ( Units )";
    public static final String LABEL_GENERAL_ACK = "label_genrl_ack_text_cancover";
    public static final String UW_DECISION_CURLY = "UW DECISION>>> {}";
    public static final String EMAIL_BRK_SUBJ_RUW = "email_brk_subj_ruw";
    public static final String EMAIL_BRK_SUBJ_NOCHANGE_ACC = "email_brk_subj_nochange_acc";
    public static final String EMAIL_BRK_SUBJ_MXD_ACC = "email_brk_subj_mxd_acc";
    public static final String EMAIL_BRK_SUBJ_LOAD_EXL_ACC = "email_brk_subj_load_exl_acc";
    public static final String EMAIL_BRK_SUBJ_LOAD_ACC = "email_brk_subj_load_acc";
    public static final String EMAIL_BRK_SUBJ_EXL_ACC = "email_brk_subj_exl_acc";
    public static final String EMAIL_BRK_SUBJ_DCL = "email_brk_subj_dcl";
    public static final String EMAIL_BRK_SUBJ_ACC = "email_brk_subj_acc";
    public static final String EMAIL_BODY_TCOVER_DCL = "email_body_tcover_dcl";
    public static final String EMAIL_BODY_SCOVER_DCL = "email_body_scover_dcl";
    public static final String EMAIL_BODY_CCOVER_DCL = "email_body_ccover_dcl";
    public static final String EMAIL_BODY_RUW = "email_body_ruw";
    public static final String EMAIL_BODY_NEWMEMBER_DCL = "email_body_newmember_dcl";
    public static final String EMAIL_BODY_MXD = "email_body_mxd";
    public static final String EMAIL_BODY_ICOVER_DCL = "email_body_icover_dcl";
    public static final String EMAIL_BODY_DCL = "email_body_dcl";
    public static final String EMAIL_BODY_ACC = "email_body_acc";
    public static final String CLIENT_EMAIL_SUBJ_ACC = "client_email_subj_acc";
    public static final String CLIENT_EMAIL_BODY_MXD = "client_email_body_mxd";
    public static final String CLIENT_EMAIL_BODY_DCL = "client_email_body_dcl";
    public static final String UWCOVER = "UWCOVER";
    public static final String TCOVER = "TCOVER";
    public static final String SCOVER = "SCOVER";
    public static final String REFERRED_TO_UNDERWRITING = "Referred to Underwriting";
    public static final String NCOVER = "NCOVER";
    public static final String MIXED_ACCEPTANCE_AND_DECLINE = "Mixed Acceptance and Decline";
    public static final String INCOME_PROTECTION_COVER = "Income Protection cover - ";
    public static final String ICOVER = "ICOVER";
    public static final String DECLINE = "Decline";
    public static final String CCOVER = "CCOVER";
    public static final String BREAK_LINE = "<br/><br/>";
    public static final String EMAIL_BODY_UWCOVER_DCL = "email_body_uwcover_dcl";
    public static final String TOTAL_PERMANENT_DISABLEMENT = "Total & permanent disablement";
    public static final String CLIENT_EMAIL_SUBJ_RUW = "client_email_subj_ruw";
    public static final String SALARY_CONTINUANCE = "Salary Continuance";
    public static final String INCOME_PROTECTION = "Income Protection";
    public static final String EMAIL_BRK_SUBJ_CANCEL_ACK = "email_brk_subj_cancel_ack";
    public static final String UNIQUEID = "</UniqueID>";
    public static final String VARIABLE = "</Variable>";
    public static final String VARIABLE_NAME = "</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable>";
    public static final String VARIABLE_NAME_OCCUPATION = "</Variable><Variable Name=\"Occupation\">";
    public static final String VARIABLE_NAME_SALARY ="</Variable><Variable Name=\"Salary\">";
    public static final String VARIABLE_NAME_SUMINSUREDTPD = "</Variable><Variable Name=\"SumInsuredTPD\">";
    public static final String VARIABLE_NAME_SUMINSUREDTERM = "</Variable><Variable Name=\"SumInsuredTerm\">";
    public static final String VARIABLE_NAME_SUMINSUREDIP = "</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">";
    public static final String AURACONTROL = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>";
    public static final String UNIQUEID_WAIVED = "\" UniqueId=\"1\" Waived=\"false\">";
    public static final String FINANCIAL_COVERAGE_AMOUNT = "\" FinancialCoverageAmount=\"";
    public static final String COVERAGE_AMOUNT = "\" CoverageAmount=\"";
    public static final String AGEDISTR_COVERAGE_AMOUNT = "\" AgeDistrCoverageAmount=\"";
    public static final String WORK_RATING = "WorkRating";
    public static final String LIFE_EVENT = "LifeEvent";
    public static final String VARIABLENAME_SCHEMENAME = "<Variable Name=\"SchemeName\">";
    public static final String VARIABLENAME_PARTNER = "<Variable Name=\"Partner\">";
    public static final String VARIABLENAME_COUNTRY = "<Variable Name=\"Country\">";
    public static final String UNIQUE_ID = "<UniqueID>";
    public static final String QUESTION_FILTERS = "<QuestionFilters>";
    public static final String PRODUCT_S = "<Products>";
    public static final String PRODUCT_AGE_COVERAGE_AMOUNT = "<Product AgeCoverageAmount=\"";
    public static final String PRESENTATION_OPTIONS = "<PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>";
    public static final String INSURED_INTERVIEW_SUBMITTED = "<Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"";
    public static final String ENGINE_VARIABLES = "<EngineVariables><Variable Name=\"Gender\">Female</Variable><Variable Name=\"Age\">";
    public static final String COMPANY = "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl>";
    public static final String CANCEL = "Cancel";
    public static final String DEATH = "DEATH";
    public static final String DEATH_COVER = "Death cover - ";
    public static final String SALARYCONTINUANCE = "Salary Continuance ";
    public static final String TOTAL_PERMANENT_DISABLEMENT_COVER ="Total & permanent disablement cover - ";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String ADD_SINGLE_CELL_FINISH = "Add single cell finish";
    public static final String ADD_SINGLE_CELL_START = "Add single cell start";
    public static final String ADDITIONAL_DEATHCOVER_DETAILS = "Additional deathCoverDetails>>>> {}";
    public static final String CLIENT = "Client";
    public static final String DC_FIXED = "DcFixed";
    public static final String DC_UNITISED = "DcUnitised";
    public static final String DEATH_CASE = "Death";
    public static final String IP_FIXED = "IpFixed";
    public static final String IP_UNITISED = "IpUnitised";
    public static final String LIFE_EVENT_DOCUMANTES = "LifeEvent Documents";
    public static final String NMCOVER = "NMCOVER";
    public static final String TPDFIXED = "TpdFixed";
    public static final String TPDUNITISED = "TpdUnitised";
    public static final String TRANSFER_DOCUMENTS = "Transfer Documents";
    public static final String WEEKLY = "Weekly";
    public static final String FALSE = "false";
    public static final String PDF_BASE_PATH = "pdfbasepath";
    public static final String FIXED = "Fixed";
    public static final String REFERRED = "Referred";
    public static final String UNITISED = "Unitised";
    public static final String YEARS = "YEARS";
    public static final String GET_ALL_AURA_QUESTIONS_START = "Get all aura questions start";
    public static final String GROUP = "Group";
    public static final String LODGE_MEM_AGE = "lodge_mem_age";
    public static final String REASON_FOR_APPLICATION_ADDITIONAL_COVER = "Reason for application for additional cover:";
    public static final String UNDERWRITING_DETAILS = "Underwriting details";
    public static final String PROPERTY_FILE = "propertyFile";
    public static final String UTF_8 = "UTF-8";
    public static final String CLIENT_DATA_GENERATION_FINISH = "Client data generation finish";
    public static final String CLIENT_DATA_GENERATION_START = "Client data generation start";
    public static final String FUND_PROP = "fundProp";
    public static final String LOGIN_END = "login() .... end";
    public static final String LOGIN_START = "login() .... start";
    public static final String SUCCESS = "success";
    public static final String UL_STYLE = "<ul style=\"list-style-type:disc;padding-left:20px;\">";
    public static final String UL_CLASS = "<ul class=\"list\">";
    public static final String U_STRONG = "<U><STRONG>";
    public static final String U_STRONG_CASE = "<u><strong>";
    public static final String STYLE_FONT = "<style='font-weight:bold;'>";
    public static final String STRONG = "<strong>";
    public static final String STRONG_CASE = "<STRONG>";
    public static final String SPAN = "<span";
    public static final String SPAN_CASH = "<SPAN";
    public static final String SPAN_STYLE = "<span style='font-weight:bold;'>";
	public static final String P_CLASS = "<p class=\"normal\">";
	public static final String DIV_CLASS = "<div class=\"desc\">";
	public static final String BR = "<br><br>";
	public static final String BR_CASE = "<BR><BR>";
	public static final String BR_SINGLE = "<br/>";
	public static final String BR_SINGLE_CASE = "<BR/>";
	public static final String BR_SPACE = "<br />";
	public static final String UL_STYLE_CASE = "<UL style=\"padding:0 0 0 35px;list-style-type:disc\">";
	public static final String UL_STYLE_PADDING = "<UL style=\"padding:0 0 0 20px;\">";
    public static final String UL_STYLE_LINE_HEIGHT = "<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc\">";
    public static final String P_STYLE = "<P style=\"padding:0 0 0 20px;\">";
    public static final String UL = "</UL>";
    public static final String UL_CASE = "</ul>";
    public static final String STRONG_U = "</strong></u>";
    public static final String STRONG_CASE_U = "</STRONG></U>";
    public static final String STRONG_WITHOUT = "</strong";
    public static final String STRONG_CASE_WITH = "</STRONG>";
    public static final String SPAN_WITH = "</span>";
    public static final String SPAN_WITH_SPACE = "</SPAN>";
    public static final String LI = "</LI>";
    public static final String LI_SPACE = "</li>";
    public static final String DIV = "</div>";
    public static final String MAX_THRESHOLD ="10000";
    public static final String CORPORATE_GEN_KEY="fef@42geE^Th4325NJ35htjHKETH4GRF(*&&#";
    public static final String COVERTYPE_TPD = "TPD";//mtaa change
    public static final String COVERTYPE_IP = "IP";
    public static final String FUND_MTAA = "MTAA";
    public static final String TRUE = "true";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String NO = "no";
    public static final String INDEXATION = "Indexation";
    public static final String CLIENT_EMAIL_BODY_FIX_YOUR_COVER = "client_email_body_convcover";
    public static final String EMAIL_BODY_FIX_YOUR_COVER = "email_body_fix_your_cover";
    public static final String EMAIL_SUBJ_FIX_YOUR_COVER = "email_brk_subj_fix_you_cover";
    public static final Double DEATH_LOADING = 50.0;
    public static final String RUW = "RUW";
    public static final String VICSUPER_LOGO = "<%LOGO%>";
    //ING
    public static final String SLASH_N_TAILORED = "\n ( Tailored )";
    public static final String SLASH_N_AUTOMATIC = "\n ( Automatic )";
    public static final String SLASH_N_75PERCENT_SALARY = "\n ( 75% of salary )";
    public static final String SLASH_N_85PERCENT_SALARY = "\n ( 85% of salary )";
    public static final String SLASH_N_90PERCENT_SALARY = "\n ( 90% of salary )";
    public static final String SLASH_N_NOCHANGE = "\n ( No Change )";
    public static final String SLASH_N_CHOOSEOWN = "\n ( Choose Your Own )";
    public static final String TPDTAILORED = "TPDTailored";
    public static final String TPDAUTOMATIC = "TPDAutomatic";
    public static final String DCTAILORED = "DcTailored";
    public static final String DCAUTOMATIC = "DcAutomatic";
    public static final String IPTAILORED = "IpTailored";
    
    public static final String CLIENT_EMAIL_SUBJ_ICOVER_DCL ="client_email_subj_icover_dcl";
    public static final String CLIENT_EMAIL_BODY_ICOVER_DCL ="client_email_body_icover_dcl";
    
    //Added for insurance calculator
    public static final String DATE_OF_PRIN = "Date of Print:";
    public static final String PER_MONTH = " per month";
    public static final String DOLLAR = "$";
    public static final String PERCENTAGE = "%";
    public static final String INSURANCE_YEARS = "Years";
    
}
