const AppConstants = {
  // api: 'https://conduit.productionready.io/api',
  //api: 'http://localhost:8087/',
  appName: 'Insurance Calculator',
  urlFilePath: 'CareSuperUrls.properties',
  logoPath: './static/insuranceCalculator/src/assets/'+fundID+'/images/logo.png',
  calIconPath: './static/insuranceCalculator/src/assets/common/css/images/calendar.png',
  appUrls: {
    "clientDataUrl":"getcustomerdata",
    "quoteUrl":"getIndustryList",
    "occupationUrl":"getOccupationName",
    "newOccupationUrl":"getNewOccupationList",
    "downloadUrl":"download",
    "printQuotePageNew":"printQuotePageNew",
    "calculateUrl":"calculateAll",
    "insuranceCalcUrl":"insuranceCalc",
    "insuranceRuleURL" : "getInsuranceRule"
    }
};

export default AppConstants;
