/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class EViewSearchUnderwritePDFPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public EViewSearchUnderwritePDFPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	

	
	public static final By linkequery = getObjectIdentifaction("eview.search.equery.link.xpath");
	public static final By linkunderpdf = getObjectIdentifaction("eview.search.underpdf.link.xpath");
	public static final By txtFname = getObjectIdentifaction("eview.search.fname.txt.css");
	public static final By txtSname = getObjectIdentifaction("eview.search.sname.txt.css");
	public static final By txtDob = getObjectIdentifaction("eview.search.dob.txt.css");
	public static final By btnSearch = getObjectIdentifaction("eview.search.search.btn.css");
	public static final By iconPdf = getObjectIdentifaction("eview.search.pdf.icon.xpath");
}
