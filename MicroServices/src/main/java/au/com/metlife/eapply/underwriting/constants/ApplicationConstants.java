package au.com.metlife.eapply.underwriting.constants;

public class ApplicationConstants {
	
	private ApplicationConstants(){
		
	}
	public static final String ANSWER_ORIENTATION_L = "LEFT";
	public static final String ANSWER_ORIENTATION_R = "RIGHT";
	public static final String ANSWER_ORIENTATION_B = "BELOW";
	
	 /*Decision Constants - Required to show decisions on the Screen*/
	public static final String RUW = "Referred to Underwriting";
	public static final String ACC = "Accepted";
	public static final String DEC = "Decline";
	
	/*XML Constants - TAGS*/
	public static final String UNICODE_XML = "UTF-8";
	public static final String TAG_DECISION = "Decision";
	public static final String TAG_OVERALL_DECISIONS = "OverallDecisions";
	public static final String TAG_UNDERWRITING_DECISION = "UnderwritingDecision";
	public static final String TAG_UNDERWRITING_DECISIONS = "UnderwritingDecisions";
	public static final String TAG_REASON = "Reason";
	public static final String TAG_CLIENT_DATA = "ClientData";
	public static final String TAG_APPLICATION_VERSION = "ApplicationVersion";
	public static final String TAG_EXCLUSION = "Exclusion";
	public static final String TAG_INSURED = "Insured";
	public static final String TAG_ORIGINAL_RULE_DECISIONS = "OriginalRuleDecisions";
	public static final String TAG_RULE_DECISIONS = "RuleDecisions";
	public static final String TAG_GENDER_AGE_BMI_DECISIONS = "GenderAgeBMIDecisions";
	public static final String TAG_GENDER_AGE_BMI_RANGE_TABLE =  "Gender_Age_Calculated_BMI_Range_Table";
	public static final String TAG_GENDER_AGE_BMI_RANGE_TABLE_BILL_PROTECT ="Gender_Age_Calculated_BMI_Range_Bill_Protect_Table" ;
	public static final String TAG_GENDER_AGE_CALCULATED_BMI_RANGE_NONSMK ="Gender_Age_Calculated_BMI_Rng_NonSmk_MetLife_Table" ;	
	public static final String TAG_AGE_COVERAGE = "AgeCoverage";
	public static final String TAG_INSURED_DECISION = "InsuredDecision";
	public static final String TAG_IMPAIRMENT_DECISION = "ImpairmentDecisions";
	public static final String TAG_IMPAIRMENT = "Impairment";
	public static final String TAG_DEPENDENT_RISK_DECISION = "DependentRiskDecisions";
	public static final String TAG_DEPENDENT_RISK = "DependentRisk";
	public static final String TAG_XML = "XML";
	public static final String TAG_RULES = "Rules";
	public static final String TAG_RULE = "Rule";
	public static final String TAG_LOOK_UPS = "Lookups";
	public static final String TAG_ENGINE_VARS = "EngineVariables";
	public static final String TAG_CONSTANTS = "Constants";
	public static final String TAG_UNDERWRITING_DECISION_PRE = "UnderwritingDecisions_Precedence";
	public static final String TAG_REQUIREMENTS = "Requirements";
	public static final String TAG_SUB_CATEGORIES = "SubCategories";
	public static final String TAG_SUB_CATEGORIES_CLASS = "SubCategoryClass";
	public static final String TAG_IMPAIRMENTS = "Impairments";
	public static final String TAG_RESULT_TYPES = "ResultTypes";
	public static final String TAG_QUESTION_TYPES = "QuestionTypes";
	public static final String TAG_SEVERITIES = "Severities";
	public static final String TAG_HEIGHT_WEIGHT_DECISIONS = "HeightWeightDecisions";
	public static final String TAG_AGE_COV_DECISIONS = "AgeCoverageDecisions";
	public static final String TAG_AGE_BMI_DECISIONS = "AgeBMIDecisions";
	public static final String TAG_COV_AMT_DECISIONS = "CoverageAmountDecisions";
	public static final String TAG_GENDER_MERITAL_DECISIONS = "GenderMaritalDecisions";
	public static final String TAG_AGE_DISTR_CVG_DECISIONS = "AgeDistrCvgDecisions";
	public static final String TAG_DECISION_TYPES = "DecisionTypes";
	public static final String TAG_CHOLESTEROL_DECISIONS = "CholesterolDecisions";
	public static final String TAG_HYPERTENSION_DECISIONS = "HypertensionDecisions";
	public static final String TAG_JUVENILE_HEIGHT_WEIGHT_DECISIONS = "JuvenileHeightWeightDecisions";
	public static final String TAG_PRODUCTS = "Products";
	public static final String TAG_RATING = "Rating";
	public static final String TAG_TOTAL_DEBITS = "TotalDebits";
	public static final String TAG_ANSWER = "Answer";
	public static final String TAG_VALUE = "Value";
	public static final String TAG_INTERNAL = "Internal";
	public static final String TAG_EXTERNAL = "External";
	public static final String TAG_UNSELECTED_ANSWER = "UnselectedAnswer";
	
	/*XML Constants - Attributes*/
	public static final String ATTRIBUTE_PRODUCT = "Product";
	public static final String ATTRIBUTE_DECISION = "Decision";
	public static final String ATTRIBUTE_AURA_VERSION = "Version";
	public static final String ATTRIBUTE_NAME = "Name";
	public static final String ATTRIBUTE_QUESTION_FILTER = "QuestionFilter";
	public static final String ATTRIBUTE_SESSION_ID = "SessionId";
	public static final String ATTRIBUTE_EXTRACT_LANGUAGE = "ExtractLanguage";
	public static final String ATTRIBUTE_INTERVIEW_MODE = "InterviewMode";
	public static final String ATTRIBUTE_SET_CODE = "SetCode";
	public static final String ATTRIBUTE_SUB_SET_CODE = "SubsetCode";
	public static final String ATTRIBUTE_AGE = "Age";
	public static final String ATTRIBUTE_BMI = "BMI";
	public static final String ATTRIBUTE_CHOLESTROL = "Cholesterol";
	public static final String ATTRIBUTE_DIASTOLIC = "Diastolic";
	public static final String ATTRIBUTE_GENDER = "Gender";
	public static final String ATTRIBUTE_HDL_CHOLESTROL = "HDLCholesterol";
	public static final String ATTRIBUTE_HEIGHT = "Height";
	public static final String ATTRIBUTE_INT_SUBMITTED = "InterviewSubmitted";
	public static final String ATTRIBUTE_MERITAL_STATUS = "MaritalStatus";
	public static final String ATTRIBUTE_PERCENT_COMPLETE = "PercentComplete";
	public static final String ATTRIBUTE_SMOKER = "Smoker";
	public static final String ATTRIBUTE_SYSTOLIC = "Systolic";
	public static final String ATTRIBUTE_UNIQUE_ID = "UniqueId";
	public static final String ATTRIBUTE_WAIVED = "Waived";
	public static final String ATTRIBUTE_WEIGHT = "Weight";
	public static final String ATTRIBUTE_BASE = "Base";
	public static final String ATTRIBUTE_CAT = "Cat";
	public static final String ATTRIBUTE_MED = "Med";
	public static final String ATTRIBUTE_RISK_TYPE = "RiskType";
	public static final String ATTRIBUTE_SEV = "Sev";
	public static final String ATTRIBUTE_CODE = "Code";
	public static final String ATTRIBUTE_ID = "Id";
	public static final String ATTRIBUTE_MAT = "Mat";
	public static final String ATTRIBUTE_AGE_COVERAGE_AMT = "AgeCoverageAmount";
	public static final String ATTRIBUTE_AGE_DISTR_COVERAGE_AMT = "AgeDistrCoverageAmount";
	public static final String ATTRIBUTE_COVERAGE_AMT = "CoverageAmount";
	public static final String ATTRIBUTE_FIN_COVERAGE_AMT = "FinancialCoverageAmount";
	public static final String ATTRIBUTE_TYPE = "Type";
	public static final String ATTRIBUTE_ALIAS = "Alias";
	public static final String ATTRIBUTE_EV = "EV";
	public static final String ATTRIBUTE_HT = "Ht";
	public static final String ATTRIBUTE_INSURED_ID = "InsuredId";
	public static final String ATTRIBUTE_NUMBER = "Number";
	public static final String ATTRIBUTE_QUEST_TYPE_ID = "QuestionTypeId";
	public static final String ATTRIBUTE_RESULT_TYPE = "ResultType";
	public static final String ATTRIBUTE_SEQ = "Seq";
	public static final String ATTRIBUTE_VISIBLE = "Visible";
	public static final String ATTRIBUTE_WIDTH = "Width";
	public static final String ATTRIBUTE_POSITION = "Position";
	public static final String ATTRIBUTE_UNIT = "Unit";
	public static final String ATTRIBUTE_IMP_ID = "ImpId";
	public static final String ATTRIBUTE_SUB_CAT_ID = "SubCatId";
	public static final String ATTRIBUTE_APPLICATION_COMPLETE = "ApplicationComplete";
	public static final String ATTRIBUTE_APPLICATION_SUBMITTED = "ApplicationSubmitted";
	
	/* XML Paths*/
	public static final String OUTPUT_XML_PATH = "/AuraIntegration/";
	protected static final String[] SERVICE_URL = {"-lhttps://aura.e2e.au.metlifeasia.com/MetAus/services/AuraService"};
	
	public static final String UTF_8 = "UTF-8";
	public static final String STR_NO = "No";
	public static final String STR_YES = "Yes";
	public static final String CONST_TRUE = "true";
	public static final String EMPTY_STRING = "";
	public static final String RULE_FILE_LOC="RULE_FILE_LOC";
	public static final String PARTNER_CODE_DESC_RULE_NAME ="INDV_Ref_Partner_Mapping.xls";
	public static final String INDV_RULE_NAME ="Aura_QuestionFilter.xls";
	
	public static final String SETCODE = "93";
	public static final String UNIQUEID = "1";
	public static final String WAIVED = "false";
	public static final String BASE_FORMAT = "1";
	public static final String BASE_NUMBERING = "1";
	public static final String BASE_START_AT_NUNBER = "1";
	public static final String DETAIL_NUMBERING = "3";
	public static final String DETAIL_FORMAT = "1";
	public static final String AUTO_POSITION_TOP = "1";
	public static final String ONLY_SHOW_ACTIVE_BRANCH = "0";
	public static final String INTERVIEW_MODE = "1";
	
	public static final String FORCE_BRANCH_COMPLETION = "0";
	public static final String FORCE_OPTIONAL_QUESTIONS = "0";
	public static final String BRANDING = "default";
	public static final String HEIGHT_UNITS = "ft.in,m.cm";
	public static final String WEIGHT_UNITS = "lb,st.lb,kg";
	public static final String SPEED_UNITS = "mph,kmph";
	public static final String DISTANCE_UNITS = "ft,m";
	public static final String DATE_FORMAT = "dd/mm/yyyy";
	
	public static final String POSTBACKTO = "https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp";
	public static final String POSTERRORTO = "https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp";
	public static final String SAVEEXIT_PAGE = "https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp";
	
	public static final String POST_PROCESS_MASTER ="PostProcessingMaster.xls";
	public static final String POST_PROCESS_1 ="PostProcessing1.xls";
	public static final String POST_PROCESS_2 = "PostProcessing2.xls";
	public static final String POST_PROCESS_3 = "PostProcessing3.xls";
	public static final String POST_PROCESS_4 = "PostProcessing4.xls";
	public static final String AURA_QUESTION_FILTER_RULE ="Aura_QuestionFilter.xls";
	public static final String AURA_QUESTION_ID_BY_PROD ="AURA_QUESTION_ID_BY_PROD.xls";
	
	public static final String INS_CALC_KEY = "fef@42geE^Th4325NJ35htjHKETH4GRF(*&&#";
	
	//MTAA Changes
	public static final String DATE_FORMAT_MMYY = "MMyy";
	public static final String DECISION_ACC = "ACC";
	public static final String APPTYPE_PRIMARY = "Primary";
	public static final String FLAG_ZERO = "0";
	public static final String FLAG_ONE = "1";
	public static final String FLAG_Y = "Y";
	public static final String FLAG_N = "N";
}
