/**
 * 
 */
package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

/**
 * @author 199306
 *
 */
public class EapplyOutput implements Serializable{
	
	private String clientPDFLocation;
	
	private boolean appStatus;

	public String getClientPDFLocation() {
		return clientPDFLocation;
	}

	public void setClientPDFLocation(String clientPDFLocation) {
		this.clientPDFLocation = clientPDFLocation;
	}

	public boolean isAppStatus() {
		return appStatus;
	}

	public void setAppStatus(boolean appStatus) {
		this.appStatus = appStatus;
	}
	
	

}
