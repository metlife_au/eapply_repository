<!DOCTYPE html>

<html lang="en" ng-app="claimTrackermain">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <meta name="description" content="">
	    <link rel="icon" id="favIconId" ng-if="fundName" href="../../eapply/static/pages/assets/img/favicon.ico">
		<title ng-bind="$root.title"> </title>

		<link ng-href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

		
		
		<!-- Vendor files concatenated and minified CSS -->
		<link id='vendorImportId'  href="../../eapply/static/pages/dist/css/vendor-imports.min.css" type="text/css" rel="stylesheet">
		
		<!-- SASS files concatenated and minified CSS -->
		<link id='mainMinId' href="../../eapply/static/pages/dist/css/main.min.css" type="text/css" rel="stylesheet">
		
		<!-- user defined css  -->
		<link id ="customId" href="../../eapply/static/pages/dist/css/custom.css" type="text/css" rel="stylesheet">

				
<!-- 		<script src="pages/dist/js/ng-file-upload.js" type="text/javascript"></script> -->
<!-- 		<script src="pages/dist/js/ng-file-upload-shim.js" type="text/javascript"></script> -->

		
	<!--  	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-resource/1.5.8/angular-resource.min.js"></script>-->
		<script src="../../eapply/static/pages/assets/js/angular.min.js"></script>
		<script src="../../eapply/static/pages/assets/js/angular-route.js"></script>
		<script src="../../eapply/static/pages/assets/js/angular-resource.min.js"></script>
		
		<script src="../../eapply/static/pages/assets/js/angular-recaptcha.min.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/jquery-1.11.0.min.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/dist/js/output.min.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/assets/js/script.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimcontroller.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimService.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimDirectives.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/dist/js/moment.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/dist/js/fine-uploader.min.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/dist/js/ng-file-upload.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/dist/js/ng-file-upload-shim.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/assets/js/claimcontroller.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimService.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimDirectives.js" type="text/javascript" ></script>
		<!-- reCaptcha -->
		<!-- 		<script src='https://www.google.com/recaptcha/api.js'></script> -->
		<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
		<script src="../../eapply/static/common/services/google-analytics-service.js"></script>
	</head>

	<body class="login-page" ng-controller='admin'>
	<div class="loader-mask">
						                <div class="loader"></div>	
						                </div>						
	<header class="global_header">
			<div class="metlifeLogo col-sm-12">
				<div class="row">
					<a id="headerLogoLink" href="/" title="Home"><img src="../../eapply/static/pages/assets/img/MetLife.png" alt="Metlife logo" class="desktop-view"></a>
				</div>
			</div>
		</header>
	<!-- end Global header module -->


	<!-- Claims Tracker specific code START -->
		<section class="main" role="main">
			<!-- Login block START -->
			<section class="module login">
				<div class="container">
					<div class="row">
						<div class="col col-sm-12">
							<h1>Claims Tracker Admin</h1>

							<div class="login-wrapper">
								<div class="row">
									<div class="login-container col col-sm-12 col-lg-4">
									<div ng-if="message != ''" class="success-msg">
									<span id="message" ng-show="message" >{{message}}</span>
									</div>
									<div ng-if="errmessage != ''" class="closed-msg">
									<span id="errmessage" ng-show="errmessage" >{{errmessage}}</span>
									</div>
										<h2>Admin</h2>

										<form method="post"  id="form" name="claimAdminForm" novalidate>														
											<div class="form-group">
												<label for="Partner Name">Select Fund Code</label>
												<select  ng-required="true" name="selected_Partner" ng-change="change()"
												         ng-model="partner.selectedPartner" ng-options="name.partnerId as name.PartnerName for name in names">
												         <option value="">Select Fund</option> 
												</select>
												
												<span class="error" ng-if="(claimAdminForm.selected_Partner.$error.required && claimAdminForm.selected_Partner.$touched) 
												|| (claimAdminForm.$submitted && claimAdminForm.selected_Partner.$error.required)">Please select Fund Code</span>
  
												
												
											</div>
											
											<div class="form-group" ng-if="partner.selectedPartner == 'NewPartner'">
												<label for="claim-number">New Fund Code</label>
												<input type="text" class="form-control" ng-required="true" name="partner_Name" ng-model="partner.partnerName" ng-change="change()">
												<span class="error" ng-if="(claimAdminForm.partner_Name.$error.required && claimAdminForm.partner_Name.$touched) 
												|| (claimAdminForm.$submitted && claimAdminForm.partner_Name.$error.required)">Please enter Fund</span>
											</div>
											
											<div class="form-group" ng-if=" partner.selectedPartner && partner.selectedPartner != 'NewPartner'">
												<label for="file-Type">File type</label>												
												<select  ng-required="true" name="selected_fileType" ng-change="change()"
												         ng-model="partner.selectedFileType" ng-options="file.fileId as file.fileName for file in fileNames">
												         <option value="">Select filetype</option> 
												</select>
												
												<span class="error" ng-if="(claimAdminForm.selected_fileType.$error.required && claimAdminForm.selected_fileType.$touched) 
												|| (claimAdminForm.$submitted && claimAdminForm.selected_fileType.$error.required)">Please select fileType</span>
											</div>
											
											<div class="form-group">
												<label for="date-of-birth">upload file</label>
												<input type="file" file-model="myFile"  ng-model="myFile" ng-required="true" name="upload_file" ng-change="change()"/>
												<span class="error" ng-if="(claimAdminForm.upload_file.$error.required && claimAdminForm.upload_file.$touched) 
												|| (claimAdminForm.$submitted && claimAdminForm.upload_file.$error.required)">Please upload file</span>
											</div>
											
											<button type="submit" class="btn btn-primary" ng-click="go(claimAdminForm,partner)">Upload</button>
											
											</br></br>
											
											<div class="form-group" ng-if="partnerUrl != ''">												
												<label for="date-of-birth"><b>{{partner.partnerName}} url :</b></label>
												<span id="partnerUrl" ng-show="partnerUrl" style="color:#a0c323'">{{partnerUrl}}</span>
																						
											</div>
											</br></br></br></br>
											<p>Don't forget that files should be uploaded in zip file format.</p><br />
											<br/>
											<p>
											Note:- 1) Please make sure that folder name and fundname should be same if it is newpartner<br/>
												   2) Images should be in .png format.
												   </p>
										</form>

									</div>

									

								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Login block END -->
		</section>
	<!-- Claims Tracker specific code END -->

		<footer class="region region-footer" role="contentinfo">
			<section id="block-block-9" class="block block-block clearfix">
			<!--Begin privacy/legal Widget -->
				<div class="container">
					<div class="row">
						<div class="col col-lg-12 footer_logo_wrapper">
							<div class="footer_logo_legal">
								<div class="footer_logo_contact col-sm-3 col-md-2">
									<span class="footer_logo_wrapper col-xs-12"><img alt="MetLife" class="footer_metlife_logo col-xs-4 lazy-loaded" data-src="../../eapply/static/pages/assets/MetLife.png" src="../../eapply/static/pages/assets/img/MetLife.png"></span>
								</div>
								<div class="col-sm-9 col-xs-12 col-md-8 footer_legal">
									<p class="legal_footer_links col-xs-12"><a target="_blank" href="https://www.metlife.com.au/privacy-policy">Privacy </a><span class="divider"> • </span><a target="_blank" href="https://www.metlife.com.au/disclaimer">Disclaimer </a><span class="divider"> • </span><a target="_blank" href="https://www.metlife.com.au/legal-notices">Legal Notices</a><span class="hidden-xs"> 2016 © Copyright MetLife Insurance Limited ABN 75 004 274 882</span></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--End privacy/legal widget-->
			</section>
		</footer>
		
	</body>
</html>
