/**
 * 
 */
package au.com.metlife.eapply.bs.exception;

/**
 * @author 199306
 *
 */
public class MetLifeBSRuntimeException extends RuntimeException{
	
	private final String errCode;
	
	private final String errMsg;
	
	public String getErrCode() {
		return errCode;
	}

	public MetLifeBSRuntimeException(String errCode, String errMsg) {
		super();
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}

}
