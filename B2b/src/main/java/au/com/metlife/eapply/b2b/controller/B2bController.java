package au.com.metlife.eapply.b2b.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.metlife.eapply.b2b.model.Eappstring;
import au.com.metlife.eapply.b2b.service.B2bService;
import au.com.metlife.eapply.b2b.utility.B2bServiceHelper;
import au.com.metlife.eapply.b2b.xsd.beans.Policy;
import au.com.metlife.eapply.b2b.xsd.beans.Request;


@RestController
@Component
@PropertySource(value = "application.properties")
public class B2bController {
	
	@Value("${spring.xsdpath}")
	private  String xsdpath;
	
	public String getXsdpath() {
		return xsdpath;
	}

	public void setXsdpath(String xsdpath) {
		this.xsdpath = xsdpath;
	}
	
	 private final B2bService b2bService;

	  @Autowired
	    public B2bController(final B2bService b2bService) {
	        this.b2bService = b2bService;
	    }
	
	
	/* @RequestMapping("/createToken")
	  public Eappstring getTokenId(@RequestParam(value="secureString") String secureString) {		
		 Eappstring eappstring =  b2bService.generateToken(secureString);
	     return eappstring;
		 
	 }
	 
	 @RequestMapping(value = "/postb2bdata11", method = RequestMethod.POST)
	  public Eappstring getTokens(@RequestBody String eappstring) {		
		//b2bService.generateToken(eappstring.getSecurestring());
		 System.out.println("eappstring>>"+eappstring);		 
		 Eappstring eappstring1 =  b2bService.generateToken(eappstring);
	     return eappstring1;
		 
	 }
	 */
	 @RequestMapping(value = "/postb2bdata", method = RequestMethod.POST)
		public  ResponseEntity<String> getTokens(InputStream inputStream) {
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;	
			String string = "";				
			 Eappstring eappstring = null;		
			 String validXML = null;
			 B2bServiceHelper b2bServiceHelper = null;
			 String[] splitParameters= null;
			 String fundData = null;
			 String token = null;
			try {
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					System.out.println("line>>"+line);	
					splitParameters = line.split("##");
					for (int i = 0; i < splitParameters.length; i++) {
						if(i==0){
							fundData= splitParameters[i];
							System.out.println("fundDate>>"+fundData);
						}else{
							token= splitParameters[i];
							System.out.println("token>>"+token);
						}						
					}					
					if(line!=null && line.trim().length()>0){
						b2bServiceHelper = new B2bServiceHelper();
						validXML= b2bServiceHelper.validateXMLString(fundData, getXsdpath()+"CARE_WebService_schema_new.xsd");
						System.out.println("validXML>>"+validXML);
						if("valid".equalsIgnoreCase(validXML)){
							eappstring =  b2bService.generateToken(fundData,token);
						}else{
							eappstring= null;
						}
						
					}
					 
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		    if(eappstring==null){
		        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		    }
		    
		    System.out.println("END--"+new  java.util.Date());
		    return new ResponseEntity<String>(eappstring.getSecureToken(), HttpStatus.OK);
		    

		}
	
	 @RequestMapping("/getcustomerdata")
	  public ResponseEntity<Policy> getClientData(@RequestParam(value="tokenid") String token) {		
		 Request request = null;
		 Eappstring eappstring =  b2bService.retriveClientData(token);
		 if(eappstring!=null){
			 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
		 }	
		 if(eappstring==null){
		        return new ResponseEntity<Policy>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		    }	
	      return new ResponseEntity<Policy>(request.getPolicy(), HttpStatus.OK);
		 
	 }
	
}