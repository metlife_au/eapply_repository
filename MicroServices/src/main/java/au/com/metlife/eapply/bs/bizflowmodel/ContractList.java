package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractList")

@XmlRootElement
public class ContractList {
	
	private String LifeEvent;

	public String getLifeEvent() {
		return LifeEvent;
	}

	public void setLifeEvent(String lifeEvent) {
		LifeEvent = lifeEvent;
	}

	

	
	
}
