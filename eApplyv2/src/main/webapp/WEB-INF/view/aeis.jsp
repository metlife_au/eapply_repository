<!DOCTYPE html>
<html lang="en" ng-app="eapplymain">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../eapply/static/aeis/images/favicon.ico">
<title>Australian Ethical Super</title>

<!-- Bootstrap core CSS -->
<link href="../../eapply/static/aeis/css/bootstrap.min.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/bootstrap-theme.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/bootstrap-select.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/bootstrap.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/docs.min.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/docs.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="../../eapply/static/aeis/css/datepicker.css" />


<!-- Custom styles for this template -->
<link href="../../eapply/static/aeis/css/custom.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/custom-ben.css" rel="stylesheet">
<link href="../../eapply/static/aeis/css/ngDialog.css" rel="stylesheet">
<link rel="stylesheet" href="../../eapply/static/aeis/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="../../eapply/static/aeis/css/ngDialog-theme-plain.css">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../eapply/static/aeis/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var baseUrl = '<%= domainName%>';
</script>


<body class="tooltip-demo">
 
<div ng-view autoscroll="true"></div>
<div id="body-mask" data-loading>
	<div class="body-mask-loader"></div>
</div>
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../../eapply/resources/js/lib/angular.min.js"></script>
<script src="../../eapply/resources/js/lib/angular-idle.min.js"></script>
<script src="../../eapply/static/aeis/js/angular-route.js"></script>
<script src="../../eapply/static/aeis/js/angular-resource.min.js"></script>
<script src="../../eapply/static/aeis/js/jquery.min.js"></script> 
<script src="../../eapply/static/aeis/js/jquery-ui.js"></script>
<script src="../../eapply/static/aeis/js/bootstrap.min.js"></script> 
<script src="../../eapply/static/aeis/js/docs.min.js"></script>
<script src="../../eapply/static/aeis/js/ng-file-upload-shim.js"></script>
<script src="../../eapply/static/aeis/js/ng-file-upload.js"></script>
<script src="../../eapply/static/aeis/js/ngDialog.js"></script>
<script src="../../eapply/static/host/js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="../../eapply/static/aeis/js/angular-sanitize.js"></script>

<script src="../../eapply/static/common/services/sessionTimeoutService.js"></script>
<script src="../../eapply/static/common/services/exceptionHandler.js"></script>
<script src="../../eapply/static/common/services/storageHandler.js"></script>
<script src="../../eapply/static/common/services/restAPI.js"></script>
<script src="../../eapply/static/common/services/appDataModel.js"></script>
<script src="../../eapply/static/common/services/eApplyInterceptor.js"></script>
<!-- CareSuperApp application file -->
<script src="../../eapply/static/aeis/js/app.js"></script>

<!-- <script src="https://assets.adobedtm.com/7bd378b63ae642286f5e272876265df98dfca74c/satelliteLib-9d49fffd8e96ae995aa8e242a15f1acf1428ecc8-staging.js"></script>
 -->

<!-- controllers -->
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/login.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/quote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/aura.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/summary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/retrieveSavedApp.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/header.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/DecisionController.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/quoteCancel.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/controllers/cancelConfirmation.js"></script>


<script type="text/javascript" src="../../eapply/static/aeis/js/moment.js"></script> 
<script type="text/javascript" src="../../eapply/static/aeis/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="../../eapply/static/aeis/js/bootstrap-select.js"></script> 

<!-- CareSuperApp Services -->
<script type="text/javascript" src="../../eapply/static/aeis/js/services/service.js"></script>

<!-- CareSuperApp utilities -->
<script type="text/javascript" src="../../eapply/static/aeis/js/utils/filters.js"></script>
<script type="text/javascript" src="../../eapply/static/aeis/js/utils/directives.js"></script>


<script src="../../eapply/static/aeis/js/custom.js"></script> 
<script src="../../eapply/static/aeis/js/validation.js"></script>
<!--<script type="text/javascript">_satellite.pageBottom();</script> -->

<script>
//awful hack for angular-vertilize...
angular.element.prototype.outerWidth = function() {
    return jQuery(this[0]).outerWidth();
};
angular.element.prototype.height = function() {
    return jQuery(this[0]).height();
};
</script>
<script type="text/javascript" src="../../eapply/static/aeis/js/utils/verticalHeight.js"></script>
</body>
</html>