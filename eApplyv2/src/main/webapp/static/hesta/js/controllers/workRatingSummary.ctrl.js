/*Summary Page Controller Starts*/ 
CareSuperApp.controller('workRatingSummary',['$scope', '$location','$timeout','$window', 'auraInputService','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService',
                         function($scope, $location, $timeout,$window, auraInputService,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;           
    };      	
    
    
    $scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}
     };
     
  // added for session expiry
	   /* $timeout(callAtTimeout, 900000); 
	  	function callAtTimeout() {
	  		$location.path("/sessionTimeOut");
	  	}*/
	  
	  	/* var timer;
	     angular.element($window).bind('mouseover', function(){
	     	timer = $timeout(function(){
	     		sessionStorage.clear();
	     		localStorage.clear();
	     		$location.path("/sessionTimeOut");
	     	}, 900000);
	     }).bind('mouseout', function(){
	     	$timeout.cancel(timer);
	     }); */ 	
     
     var ackCheckWRDD;
     var ackCheckWRPP;
     var ackCheckWRGC;

     
    $scope.workRatingCoverDetails=PersistenceService.getworkRatingCoverDetails();
    console.log($scope.workRatingCoverDetails);
    
    $scope.workRatingAuraDetails=PersistenceService.getWorkRatingAuraDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    
    $scope.workRatingOccDetails =PersistenceService.getWorkRatingCoverOccDetails();
    $scope.auraDisabled = $scope.workRatingCoverDetails.auraDisabled;
    
    $scope.submitWorkRating = function(){
    	ackCheckWRDD = $('#DutyOfDisclosureLabelWR').hasClass('active');
    	ackCheckWRPP = $('#privacyPolicyLabelWR').hasClass('active');
    	ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');
    	
    	
    	if(ackCheckWRDD && ackCheckWRPP && ackCheckWRGC){
    		$scope.WRDDackFlag = false;
    		$scope.WRPPackFlag = false;
    		$scope.WRGCackFlag = false;
    		if($scope.workRatingCoverDetails != null && $scope.workRatingOccDetails != null && $scope.workRatingAuraDetails != null && $scope.personalDetails[0] != null){
    			$scope.workRatingCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.occupationDetails=$scope.workRatingOccDetails;
    			var personalDetails=angular.extend($scope.details,$scope.personalDetails[0]);
        		var temp = angular.extend(personalDetails,$scope.workRatingCoverDetails);   
        		var submitObject = angular.extend(temp,$scope.workRatingAuraDetails);
        		auraResponseService.setResponse(submitObject);
        		submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);

            		if($scope.workRatingAuraDetails.deathDecision!=null &&  $scope.workRatingAuraDetails.tpdDecision!=null){
            			if(($scope.workRatingAuraDetails.deathDecision=="DCL") ||($scope.workRatingAuraDetails.tpdDecision=="DCL")){
            				$scope.workRatingAuraDetails.overallDecision = 'DCL';
    					}else{
    						$scope.workRatingAuraDetails.overallDecision = 'ACC';
    					}
            		} 
            		if($scope.workRatingAuraDetails.overallDecision == 'ACC'){
                		$scope.go('/workRatingAccept');
                	}else if($scope.workRatingAuraDetails.overallDecision == 'DCL'){
                		$scope.go('/workRatingDecline');
                	}  
            	});      		
        	}else{
        		if($scope.workRatingCoverDetails != null && $scope.workRatingOccDetails != null && $scope.personalDetails[0] != null){
        			$scope.workRatingCoverDetails.lastSavedOn = '';
        			$scope.details={};
        			$scope.details.occupationDetails=$scope.workRatingOccDetails;
        			var personalDetails=angular.extend($scope.details,$scope.personalDetails[0]);
            		var submitObject = angular.extend(personalDetails,$scope.workRatingCoverDetails);   
            		//var submitObject = angular.extend(temp,$scope.workRatingAuraDetails);
            		auraResponseService.setResponse(submitObject);
            		submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
                		console.log(response.data);
                		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                		PersistenceService.setNpsUrl(response.data.npsTokenURL);
                		$scope.go('/workRatingMaintain');
                	});
        		}
        		
        	}	
    	}else{
    		//$scope.WRackFlag = true;
    		//$scope.WRDDackFlag = true;
    		//$scope.WRPPackFlag = true;
    		//$scope.WRGCackFlag = true;
    		if(ackCheckWRDD){
        		$scope.WRDDackFlag = false;
        	}else{
        		$scope.WRDDackFlag = true;
        	}
        	
        	if(ackCheckWRPP){
        		$scope.WRPPackFlag = false;
        	}else{
        		$scope.WRPPackFlag = true;
        	}
        	
        	if(ackCheckWRGC){
        		$scope.WRGCackFlag = false;
        	}else{
        		$scope.WRGCackFlag = true;
        	}
        	$scope.scrollToUncheckedElement();
    	}   	
    	
    };
    $scope.scrollToUncheckedElement = function(){
		var elements = [ackCheckWRDD, ackCheckWRPP, ackCheckWRGC];
		var ids = ['DutyOfDisclosureLabelWR', 'privacyPolicyLabelWR', 'generalConsentLabelWR'];
    	for(var k = 0; k < elements.length; k++){
    		if(!elements[k]){
    			$('html, body').animate({
        	        scrollTop: $("#" + ids[k]).offset().top
        	    }, 1000);
    			break;
    		}
    	}
    };
    /*var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveSummaryWorkRating = function() {
    	$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.workRatingCoverDetails != null && $scope.workRatingOccDetails != null && $scope.workRatingAuraDetails != null && $scope.personalDetails[0] != null){
    		$scope.workRatingCoverDetails.lastSavedOn = 'SummaryUpdatePage';
    		$scope.details={};
			$scope.details.workRatingOccDetails = $scope.workRatingOccDetails;
    		var temp = angular.extend($scope.workRatingCoverDetails,$scope.details);
    		var aura = angular.extend(temp,$scope.workRatingAuraDetails);
        	var saveUpdateSummaryObject = angular.extend(aura, $scope.personalDetails[0]);
        	auraResponseService.setResponse(saveUpdateSummaryObject);
	        saveEapply.reqObj().then(function(response) {  
	                console.log(response.data)
	        });
    	}
    };
    
   $scope.saveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};*/
    /*$scope.checkAckStateWR = function(){
    	$timeout(function(){
    		ackCheckWRDD = $('#DutyOfDisclosureLabelWR').hasClass('active');
        	ackCheckWRPP = $('#privacyPolicyLabelWR').hasClass('active');
        	ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');
        	
        	if(ackCheckWRDD){
        		$scope.WRDDackFlag = false;
        	}else{
        		$scope.WRDDackFlag = true;
        	}
        	
        	if(ackCheckWRPP){
        		$scope.WRPPackFlag = false;
        	}else{
        		$scope.WRPPackFlag = true;
        	}
        	
        	if(ackCheckWRGC){
        		$scope.WRGCackFlag = false;
        	}else{
        		$scope.WRGCackFlag = true;
        	}
    	}, 10);
    };*/
    
    $scope.checkAckStateDDWR = function(){
    	$timeout(function(){
    		ackCheckWRDD = $('#DutyOfDisclosureLabelWR').hasClass('active');        	
        	if(ackCheckWRDD){
        		$scope.WRDDackFlag = false;
        	}else{
        		$scope.WRDDackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStatePPWR = function(){
    	$timeout(function(){
    		ackCheckWRPP = $('#privacyPolicyLabelWR').hasClass('active');
        	if(ackCheckWRPP){
        		$scope.WRPPackFlag = false;
        	}else{
        		$scope.WRPPackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStateGCWR = function(){
    	$timeout(function(){
    		ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');  
        	if(ackCheckWRGC){
        		$scope.WRGCackFlag = false;
        	}else{
        		$scope.WRGCackFlag = true;
        	}        	
        	
    	}, 10);
    };
    
    
    }]); 
   /*Summary Page Controller Ends*/