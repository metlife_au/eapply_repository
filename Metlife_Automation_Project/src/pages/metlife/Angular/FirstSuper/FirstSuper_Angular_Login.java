/**
 * 
 */
package pages.metlife.Angular.FirstSuper;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Hemnath
 * 
 */
public class FirstSuper_Angular_Login extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public FirstSuper_Angular_Login(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public FirstSuper_Angular_Login FirstSuper_Login() {
		enterlogindetails();
		return new FirstSuper_Angular_Login(scriptHelper);
	} 
	
	private void enterlogindetails() {
		
		String XML = dataTable.getData("FirstSuper_Login", "XML");
		
		
		try{
			
			
			
			//******Input the XML String******\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				 StringSelection sel = new StringSelection(XML);
				 
	                                                           			   // Copy to clipboard  
				 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
				 
				 //Click
				 driver.findElement(By.id("input_string_id")).click();
				 
				 // Create object of Robot class
				 Robot robot = new Robot();
				 Thread.sleep(1000);
				      
				  // Press Enter
				 robot.keyPress(KeyEvent.VK_ENTER);
				 
				// Release Enter
				 robot.keyRelease(KeyEvent.VK_ENTER);
				 
				  // Press CTRL+V
				 robot.keyPress(KeyEvent.VK_CONTROL);
				 robot.keyPress(KeyEvent.VK_V);
				 
				// Release CTRL+V
				 robot.keyRelease(KeyEvent.VK_CONTROL);
				 robot.keyRelease(KeyEvent.VK_V);
				 Thread.sleep(1000);
				        
				        // Press Enter 
				// robot.keyPress(KeyEvent.VK_ENTER);
				// robot.keyRelease(KeyEvent.VK_ENTER);
				 
				//sleep(1000);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(XML);
				sleep(800);
				report.updateTestLog("XML String", "XML String is entered", Status.PASS);
				
			}
			
			
			//****Proceed to Next page button is Clicked*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'PROCEED TO NEXT PAGE')]"))).click();
			report.updateTestLog("Proceed To Next Page", "Proceed To Next Page button is Clicked", Status.PASS);
			
			
			//****Web response button click*****\\
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Rwd Application')]"))).click();
			report.updateTestLog("RWD Application", "RWD Application button is Clicked", Status.PASS);
			
			sleep(3000);
			report.updateTestLog("HOSTPLUS Login", "Sucessfully Logged into the application", Status.PASS);
			
		
	
	
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
