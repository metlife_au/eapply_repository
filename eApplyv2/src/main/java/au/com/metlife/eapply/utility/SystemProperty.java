/**
* This class is modified on July 07, 2006 by lyju for  AW Replatforming.
*/

package au.com.metlife.eapply.utility;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * This class is used to read the property file and store the parameters
 * Currently in our system, we use one property file. The property object is a public variable which can be accessed
 * directly.
 * This class is a singleton class.
 * MODIFIED BY Lyjue for CAW Replatforming on 07 July, 2006.
 * A new method is added to check whether the requested key is presented or not.
 */
public class SystemProperty{
	private static final Logger log = LoggerFactory.getLogger(SystemProperty.class);
	String CLASS_NAME = "SystemProperty";

	/**Only object of the singleton class */
	private static SystemProperty obj_systemProperty;

	private SecureRandom sRandom ;

	/**Property object for the system property file*/
	private java.util.Properties obj_Properties;

	private java.util.HashMap obj_drools;

	/**Name of the properties file for the batch process*/
	/*private String PROPERTY_FILE = "/com/caw/citiaus/utils/CAWApplication.properties";*/

	/*Production*/
	private String PROPERTY_FILE = "/shared/props/eapply2/eaply2.properties";
	/*E2E
	private String PROPERTY_FILE = "/var/WebSphere/props/CAW/CAWApplication.properties";*/

	/**Code addred by RZ67436 to enable passowrd encryption*/
	private String ENC_PROPERTY_FILE = "/com/caw/citiaus/utils/defaultprops.dat";
	/**Code ends here by RZ67436 to enable passowrd encryption*/

	/*private String EMAIL_PROPERTY_FILE = "CAWEmail.properties";*/


	/**
	 * PropertyFileParameters constructor comment.
	 * This method reads the property file and creates a properties object for it
	 * @param - none
	 * @return - none
	 * @exception - java.lang.Exception
	 */
	private SystemProperty() throws java.lang.Exception
	{
		loadProperties();
	}

	/**
	 * PropertyFileParameters reads from properties file and initializes the project
	 * @param - none
	 * @return - none
	 * @exception - java.lang.Exception
	 */
	public void loadProperties() throws java.lang.Exception
	{
		
		
		obj_Properties = getPropertySet();

		/**Code addred by RZ67436 to enable passowrd encryption*/	

		if(obj_Properties  == null)
		{
			throw(new Exception("System not initialized properly. Terminating process."));
		}

		


	}

	/**
	 * returns the only instance of this class
	 * @param - none
	 * @return - PropertyFileParameters object
	 * @exception - java.lang.Exception
	 */
	public static SystemProperty getInstance() throws Exception
	{
		if(obj_systemProperty ==  null)
		{
			obj_systemProperty = new SystemProperty();


		}

		return obj_systemProperty;
	}

	/**
	 * Gets the specified property
	 * @return java.lang.String
	 * @param arg_property java.lang.String
	 */
	public String getProperty(String arg_property)
	{
		return obj_Properties.getProperty(arg_property);
	}

	/**
	 * Gets the list of keys for all the properties
	 * @return java.lang.String
	 * @param arg_property java.lang.String
	 */
	public java.util.Enumeration getPropertyKeyList()
	{
		return obj_Properties.propertyNames();
	}

	/**
	 * Reads from the property file and returns a properties object
	 * @param - file name
	 * @return - Properties object for that file
	 * @exception -  java.io.FileNotFoundException,java.io.IOException
	 */
	private Properties getPropertySet() throws java.io.FileNotFoundException, java.io.IOException
	{
		  log.info("System property file load starts");
		/* InputStream inputStream = getClass().getResourceAsStream(PROPERTY_FILE);*/
		 /* if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
			  PROPERTY_FILE="c:"+PROPERTY_FILE;
		  }*/
		  /*InputStream inputStream = ClassLoader.getSystemResourceAsStream(PROPERTY_FILE);*/
		  InputStream inputStream =null;


		  if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
			  inputStream=new FileInputStream("c:"+PROPERTY_FILE);
		  }else{
			  inputStream=new FileInputStream(PROPERTY_FILE);
		  }

		log.info("System property file load ends");
	      /*InputStream emailInputStream = ClassLoader.getSystemResourceAsStream(EMAIL_PROPERTY_FILE);*/

		/*if the file does not exist then return null else load the props and return the object.*/
		Properties obj_props = null;

		if (inputStream != null)
		{
			log.info("input stream is not null - good ");
			obj_props = new Properties();

			obj_props.load(inputStream);

			inputStream.close();
		}
		else
		{
			log.info("input stream is  null - this is bad 99999999999");
			
		}

		return obj_props;
	}

	
	




	

}
