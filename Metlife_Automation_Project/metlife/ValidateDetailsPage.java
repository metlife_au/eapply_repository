/**
 * 
 */
package pages.metlife;

import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.DeclarationAndConfirmationPageObjects;

/**
 * @author sampath
 * 
 */
public class ValidateDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public ValidateDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public ValidateDetailsPage validatePWCDecisionPage() {
		validateDecision();
		return new ValidateDetailsPage(scriptHelper);
	} 

	private void validateDecision() {
		
		String strApplicationMessage = dataTable.getData("MetLife_Data", "applicationMessage");
		
		try{
			assertContainsText(fluentWaitElements(DeclarationAndConfirmationPageObjects.finalPWCReferredMsg),strApplicationMessage);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
}
