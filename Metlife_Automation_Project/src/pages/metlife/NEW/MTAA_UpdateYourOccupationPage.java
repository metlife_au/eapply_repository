/**
 * 
 */
package pages.metlife.NEW;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.MTAA_UpdateYourOccupationPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

/**
 * @author Hemnath
 * 
 */
public class MTAA_UpdateYourOccupationPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public MTAA_UpdateYourOccupationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public MTAA_UpdateYourOccupationPage enterMTAAUpdateoccupation() {
		updateoccupation();
		HealthandLifestyle();
		Declaration();
		return new MTAA_UpdateYourOccupationPage(scriptHelper);
	} 
	
	private void updateoccupation() {
		String EmailId = dataTable.getData("MTAA_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("MTAA_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("MTAA_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("MTAA_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("MTAA_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("MTAA_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("MTAA_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("MTAA_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("MTAA_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("MTAA_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("MTAA_UpdateOccupation", "TertiaryQual");
		String HazardousEnv = dataTable.getData("MTAA_UpdateOccupation", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("MTAA_UpdateOccupation", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("MTAA_UpdateOccupation", "AnnualSalary");
		
		
		try{
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.UpdateYourOccupationLink), "Click", "Update Your Occupation Link");
			
			 sleep(1500);
				
				
				if(driver.getPageSource().contains("Cancel Application")){
					quickSwitchWindows();
					sleep(1000);
					driver.findElement(By.xpath("(//span[contains(text(),'Cancel Application')])[1]")).click();
					report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
					sleep(1000);
			
				}
				
			//*****Agree to Duty of disclosure*******\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "selected the Checkbox", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Privacy Statement", "selected the Checkbox", Status.PASS);
			
			//*****Enter the Email Id*****\\
			
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.Email), "SET", "Email Id",EmailId);
			

			//*****Click on the Contact Number Type****\\
			if(!TypeofContact.equalsIgnoreCase("Mobile")){
			
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ContactTypeClick), "Click", "Preferred Type of Contact: "+TypeofContact);
						
			//*****Choose the Type of Contact*****\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();
			
			}
			
			//****Enter the Contact Number****\\
			waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);
			
			
			
			//***Select the Preferred time of Contact*****\\
			if(TimeofContact.equalsIgnoreCase("Morning")){
				
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");
			}
			else{
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.AfterNoonContact), "Click", "Preferred Contact Time is AfterNoon");
			}
			
			//****Contact Details-Continue Button*****\\			
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");
			sleep(1200);
			
			
			//***************************************\\	
			//**********OCCUPATION SECTION***********\\
			//****************************************\\	
				
				//*****Select the 15 Hours Question*******\\
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){
				
				tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.FifteenHoursYes), "Click", "15 Hours a Week-Yes");
				
				}
				else{
				
				tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");
				
				}
				
				//*****Select if Resident of Australia****\\
				if(Citizen.equalsIgnoreCase("Yes")){

					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.CitizenYes), "Click", "Australian Citizen-Yes");
					
					}
				else{
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.CitizenNo), "Click", "Australian Citizen-No");	
					
					}
				
				//*****Click on Industry********\\
				
				tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ClickIndustry), "Click", "Industry");
				sleep(800);
				
				//*****Choose the Industry Type*****\\
				driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();
				sleep(800);
			
				//*****Click On  Occupation*****\\
				
		        tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ClickOccupation), "Click", "Occupation");
		        sleep(800);
		        
		        //****Choose the Occupation Type*****\\
				
				driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();
				sleep(1000);
				
				
				//*****Other Occupation *****\\
				if(OccupationType.equalsIgnoreCase("Other")){
					
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);
					sleep(1000);
				}
				
				
				//*****Extra Questions*****\\
				
				if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){
			String Question=driver.findElement(By.xpath("//*[@id='work_duty_div_id']/div/label")).getText().trim();
			sleep(800);
			
			
			
			if(Question.equalsIgnoreCase("Are your duties entirely undertaken within an office environment?")){
				
			//***Select if your office Duties are Undertaken within an Office Environment?\\
				if(OfficeEnvironment.equalsIgnoreCase("Yes")){
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");
					
				}
				else{
					
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");
					
				}
				
			//*****Do You Hold a Tertiary Qualification******\\
				
				if(TertiaryQual.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");				
				}						
			}
				}
				
				
			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){
			
				
				
			//******Do you work in hazardous environment*****\\
				if(HazardousEnv.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");				
				}
				
			//******Do you spend 20% time working outside office*******\\
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");				
				}						
			}
			
	//*****Extra Questions*****\\
				
				if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){
			String Question=driver.findElement(By.xpath("//*[@id='work_duty_div_id']/div/label")).getText().trim();
			sleep(800);
			
			
			
			if(Question.equalsIgnoreCase("Are your duties entirely undertaken within an office environment?")){
				
			//***Select if your office Duties are Undertaken within an Office Environment?\\
				if(OfficeEnvironment.equalsIgnoreCase("Yes")){
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");
					
				}
				else{
					
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");
					
				}
				
			//*****Do You Hold a Tertiary Qualification******\\
				
				if(TertiaryQual.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");				
				}						
			}
				}
				
				
			//*****What is your annual Salary******
			
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);
			sleep(500);
			
			//***Load the Salary Amount****\\
			waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.LoadSalaryLabel).click();
			sleep(500);
			
			
		//*****Continue button after entering occupation details*******\\
			
			
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");
			sleep(4500);
			

			


		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	private void HealthandLifestyle() {	
		String RestrictedIllness = dataTable.getData("MTAA_UpdateOccupation", "RestrictedIllness");
		String WorkinHazardousEnvironment = dataTable.getData("MTAA_UpdateOccupation", "WorkinHazardousEnvironment");
		String WorkOutsideOfc = dataTable.getData("MTAA_UpdateOccupation", "WorkOutsideOfc");
		try{
			
			
			if(driver.getPageSource().contains("Eligibility Check")){
			
		//*****Have you been restricted in the last 30 days, due to illness or injury*****\\
		if(RestrictedIllness.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.RestrictedillnessYes), "Click", "Restrictedillness-Yes");	
		}
		else{
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.RestrictedillnessNo), "Click", "Restrictedillness-No");	
		}
		
		
		//***Do you perform any work duties of a manual nature or work within a hazardous environment?**\\
		
		if(WorkinHazardousEnvironment.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.HazardousEnvYes), "Click", "Hazardous Environment-Yes");	
		}
		else{
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.HazardousEnvNo), "Click", "Hazardous Environment-No");	
		}
		//*****Do you spend more than 20 percent of your working time****\\
		
		if(WorkOutsideOfc.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.WorkOutsideOfcYes), "Click", "20% Work Outside Ofc-Yes");	
		}
		else{
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.WorkOutsideOfcNo), "Click", "20% Work Outside Ofc-No");	
		}
		
		//*****Click On Continue Button*****\\
		
		tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.ContinueHealthPage), "Click", "Continue After entering Health and Lifestyle Details");	
			
	}
		}
		catch(Exception e)
	{
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}
	
	private void Declaration() {
		try{
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.Declarationcheckbox), "Click", "Declaration Checkbox");
			
			tryAction(waitForClickableElement(MTAA_UpdateYourOccupationPageObjects.DeclarationpageSubmit), "Click", "Submit");
			
			//*******Feedback Pop-up******\\

			WebDriverWait wait=new WebDriverWait(driver,18);	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1000);
			
		String Appstatus=driver.findElement(By.xpath("//h4[@class='panel-title']")).getText();
		report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);
		
	}
	catch(Exception e)
{
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
}
		
	}

