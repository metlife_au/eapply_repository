/**
 * 
 */
package pages.metlife.NEW;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.NSFSLoginPageObjects;

/**
 * @author Hemnath
 * 
 */
public class NSFS_LoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public NSFS_LoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public NSFS_LoginPage enterNSFSlogin() {
		enterlogindetails();
		return new NSFS_LoginPage(scriptHelper);
	} 
	
	private void enterlogindetails() {
		String InputString = dataTable.getData("NSFS_Login", "InputString");
		
	
		try{
			//***Enter the INput String XML*******\\
			
			tryAction(fluentWaitElements(NSFSLoginPageObjects.XMLInput), "SET", "Input XML Strinf",InputString);
			
			
		//****Click on Proceed to Next button******\\
			
			tryAction(waitForClickableElement(NSFSLoginPageObjects.NextButton), "Click", "Next Button");
			sleep(1500);
			
		//*****Click on New Application******\\
			
			tryAction(waitForClickableElement(NSFSLoginPageObjects.NewApplication), "Click", "New Application");
			sleep(2500);
			
			//*****Switch Window and Maximize*****
			
			quickSwitchWindows();
			sleep(2500);
			
			
			report.updateTestLog("NSFS Login", "Sucessfully Logged into the application", Status.PASS);
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
