package au.com.metlife.eapply.bs.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lowagie.text.DocumentException;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.ApplicantInfo;
import au.com.metlife.eapply.bs.model.Auradetail;
import au.com.metlife.eapply.bs.model.ContactDetails;
import au.com.metlife.eapply.bs.model.Contactinfo;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverInfo;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.DocumentInfo;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.pdf.GenerateEapplyPDF;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.constants.EtoolkitConstants;
import au.com.metlife.eapply.corporate.serviceImpl.CorpServiceImpl;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;


/**
 * @author 175373
 *
 */
public class EapplyServiceHelper {
	
	private EapplyServiceHelper() {
		
	}
	
	public static final int ZERO_CONST = 0;
	private static final Logger log = LoggerFactory.getLogger(CorpServiceImpl.class);
	public static Eapplication convertToEapplication(EappInput eappInput,String appStatus) throws ParseException, IOException {
		
		log.info("EapplyServiceHelper.convertToEapplication start....");
		
		Eapplication eapplication = new Eapplication();
		Applicant applicant = new Applicant();
		List<Applicant> applicantList=new ArrayList<>();
		String saveEappInput = null;
		String deathLoading = null;
		String tpdLoading = null;
		String ipLoading = null;
		
		try {
			
			if(eappInput != null) {
				
				//Saving the JSon from UI 
				ObjectMapper objmp = new ObjectMapper();
				saveEappInput = objmp.writeValueAsString(eappInput);
				eapplication.seteAppSaveDataclob(saveEappInput);
				System.out.println("Submit json sent from UI: "+saveEappInput);
				log.info("Submit json sent from UI: "+saveEappInput);
				
				DateFormat dateMonthFormat = new SimpleDateFormat(ApplicationConstants.DATE_FORMAT_MMYY);
				String monthDir = dateMonthFormat.format(new java.util.Date());
				String campaignCode = null;
				
				
				//Eapplication Mapping Stars
				eapplication.setApplicationstatus(appStatus);
				
				
				if(null != eappInput.getAppDecision()){
					eapplication.setAppdecision(eappInput.getAppDecision());
	 			}else{
	 				//convert& maintain scenario- by default all cover are acc
	 				eapplication.setAppdecision(ApplicationConstants.DECISION_ACC);
	 			}
				
				if(eappInput.isAckCheck()){
					eapplication.setGenconsentindicator(ApplicationConstants.CONST_TRUE);						
				}
				if(eappInput.isDodCheck()){
					eapplication.setDisclosureindicator(ApplicationConstants.CONST_TRUE);
				}
				if(eappInput.isPrivacyCheck()){
					eapplication.setPrivacystatindicator(ApplicationConstants.CONST_TRUE);
				}
				if(eappInput.isFulCheck()){
					eapplication.setFulstatindicator(ApplicationConstants.CONST_TRUE);
				}
				
				eapplication.setPartnercode(eappInput.getPartnerCode());
	 			eapplication.setRequesttype(eappInput.getRequestType());
				eapplication.setApplicationtype(MetlifeInstitutionalConstants.INSTITUTIONAL);
				eapplication.setLastsavedon(eappInput.getLastSavedOn());
				
				if(null != eappInput.getClientMatchReason()&& eappInput.getClientMatchReason().length()<3000){
	 				eapplication.setClientmatchreason(eappInput.getClientMatchReason());
	 			}
	 			eapplication.setClientmatch(eappInput.getClientMatch());
				
	
	 			if(null != eappInput.getAuraDisabled() && MetlifeInstitutionalConstants.FALSE.equalsIgnoreCase(eappInput.getAuraDisabled())){
	 				eapplication.setUnderwritingpolicy(ApplicationConstants.STR_YES);
	 			}else if(null != eappInput.getResponseObject() || "AuraPage".equalsIgnoreCase(eappInput.getLastSavedOn())){
	 				eapplication.setUnderwritingpolicy(ApplicationConstants.STR_YES);
	 			}else{
	 				eapplication.setUnderwritingpolicy(ApplicationConstants.STR_NO);
	 			}
				
	 			if(null != eappInput.getApplicatioNumber()){
	 				eapplication.setApplicationumber(eappInput.getApplicatioNumber());
	 				campaignCode = eapplication.getPartnercode() + monthDir +"WG-" + eappInput.getApplicatioNumber();
	 				eapplication.setCampaigncode(campaignCode);
	 			}
	 			
	 			if((null != eappInput.getDeathExclusions() && eappInput.getDeathExclusions().trim().length()>ZERO_CONST)
	 					|| (null != eappInput.getTpdExclusions()&& eappInput.getTpdExclusions().trim().length()>ZERO_CONST)
	 					|| (null != eappInput.getIpExclusions()&& eappInput.getIpExclusions().trim().length()>ZERO_CONST)){
	 				eapplication.setSaindicator(ApplicationConstants.CONST_TRUE);
	 			}else{
	 				eapplication.setSaindicator(MetlifeInstitutionalConstants.FALSE);
	 			}
	 			
	 			 			
	 			if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
	 				for (Iterator iterator = eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
	 					CoverInfo coverInfoObj = (CoverInfo)iterator.next();
	 					if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
	 						if(coverInfoObj.getLoading()!= null && coverInfoObj.getLoading()!= "") {
	 							deathLoading = coverInfoObj.getLoading();
	 						}
	 					}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
	 						if(coverInfoObj.getLoading()!= null && coverInfoObj.getLoading()!= "") {
	 							tpdLoading = coverInfoObj.getLoading();
	 						}
	 					}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
	 						if(coverInfoObj.getLoading()!= null && coverInfoObj.getLoading()!= "") {
	 							ipLoading = coverInfoObj.getLoading();
	 						}
	 					}
	 				}
	 			}	
				if((deathLoading!= null && deathLoading.trim().length()>ZERO_CONST)
	 					|| (tpdLoading!=null && tpdLoading.trim().length()>ZERO_CONST)
	 					|| (ipLoading !=null && ipLoading.trim().length()>ZERO_CONST)){
	 				eapplication.setPaindicator(ApplicationConstants.CONST_TRUE);
	 			}else{
	 				eapplication.setPaindicator(MetlifeInstitutionalConstants.FALSE);
	 			}
	 			
	 			if(null != eappInput.getTotalMonthlyPremium()){
	 				eapplication.setTotalMonthlyPremium(eappInput.getTotalMonthlyPremium());
	 			}
	 			//Eapplication Mapping Ends
	 			
	 			//Applicant Mapping Starts
	 			applicant = getapplicantInfo(applicant, eappInput);
	 			//applicant.setClientrefnumber(eappInput.getClientRefNumber());//Local Change - Needs to be removed
	 			//Applicant Mapping Ends
	 			
	 			//Cover Mapping Stars
	 			List<Cover> coverList = null;
	            if(eappInput.getRequestType()!= null){
	            	if(eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.CCOVER)){
	            		coverList = createChangeCvCovers(eappInput);
	            	}else if(eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.TCOVER)){
	            		coverList = createTransferCvCovers(eappInput);
	            	}else if(eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.ICOVER)){
	            		coverList = createLifeEventCovers(eappInput);
	            	}else if(eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.UWCOVER)){
	            		coverList = createUpdateWorkRatingCvCovers(eappInput);
	            	}else if(eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.CANCOVER)){
	            		coverList = createCancelCvCovers(eappInput);
	            	}else if(eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.NMCOVER)){
	            		coverList = createNonMemberCvCovers(eappInput);
	            	}
	            }
	            
	            //ContactInfo mapping
	            Contactinfo contactinfo=null;
	            contactinfo = getcontactInfo(eappInput);
	            
	            applicant.setCovers(coverList);
	            applicant.setContactinfo(contactinfo);
	            applicantList.add(applicant);
	            eapplication.setApplicant(applicantList);
	            
	            //Aura Detail mapping
	            Auradetail auradetail =new Auradetail();
	            auradetail.setCreatedate(new Timestamp(new Date().getTime()));
	            auradetail.setLastupdatedate(new Timestamp(new Date().getTime()));		           
	            if(eappInput.getResponseObject()!=null){
	            	if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())){
	            		auradetail.setShortformstatus(eappInput.getAuraType());
	            	}else{
	            		auradetail.setShortformstatus("Long");
	            	}
	            	  
	            }
	            eapplication.setAuradetail(auradetail);
	            
	            //Document mapping
	            getDocumentIno(eapplication, eappInput);
	            
			}
		} catch (JsonParseException ex) {
			log.error(EtoolkitConstants.JSON_PARSE_EXCEPTION+ex.getMessage());
		} catch (JsonMappingException ex) {
			log.error(EtoolkitConstants.JSON_MAPPING_EXCEPTION+ex.getMessage());
		} catch (IOException ex) {
			log.error(EtoolkitConstants.IO_EXPECTION+ex.getMessage());
		}
		log.info("EapplyServiceHelper.convertToEapplication End....");
		return eapplication;
	}
	
	public static Applicant getapplicantInfo(Applicant applicant, EappInput eappInput) throws ParseException {
		
		log.info("EapplyServiceHelper.getapplicantInfo start....");
		
		ApplicantInfo applicantObj = null;
		Calendar dob;
		DateFormat df1;
		
			if(eappInput.getApplicant() != null) {
				applicantObj = eappInput.getApplicant();
			
	 			if(null != eappInput.getApplicatioNumber()){
	 				applicant.setQuestionnaireid(eappInput.getApplicatioNumber());
	 			}
	 			applicant.setCreatedate(new Timestamp(new Date().getTime()));
	 			applicant.setLastupdatedate(new Timestamp(new Date().getTime()));
	 			
	 			if(isNotNullandNotEmpty(applicantObj.getTitle())){
	 				applicant.setTitle(applicantObj.getTitle());
	 			}
	 			if(isNotNullandNotEmpty(applicantObj.getFirstName())){
	 				applicant.setFirstname(applicantObj.getFirstName());
	 			}
	 			if(isNotNullandNotEmpty(applicantObj.getLastName())){
	 				applicant.setLastname(applicantObj.getLastName());
	 			}
	 			if(applicantObj.getBirthDate()!=null){
	        		dob = Calendar.getInstance();
	        		df1 = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
	        		dob.setTime(df1.parse(applicantObj.getBirthDate()));
	        		applicant.setBirthdate(dob.getTime());
	        	}
	        	if(applicantObj.getSmoker()!=null){
	        		applicant.setSmoker(applicantObj.getSmoker());
	    			//Added for metflow part
	    			
	        	}
	        	if(applicantObj.getGender()!=null){
	        		applicant.setGender(applicantObj.getGender());
	        	}
	        	applicant.setWorkhours(applicantObj.getWorkHours());
	        	applicant.setIndustrytype(applicantObj.getIndustryType());
	        	applicant.setOccupationcode(applicantObj.getOccupationCode());
	        	applicant.setOccupation(applicantObj.getOccupation());
	 			
	        	if(isNotNullandNotEmpty(applicantObj.getOtherOccupation())){
	       		 applicant.setOtheroccupation(applicantObj.getOtherOccupation()); 
	       	 	}
	        	 
	       	 	applicant.setAnnualsalary(applicantObj.getAnnualSalary());
	 			
	           	if(applicantObj.getAustraliaCitizenship()!=null){
	        	 applicant.setAustraliacitizenship(applicantObj.getAustraliaCitizenship()); 
	        	}
	           	
	           	if(applicantObj.getOwnBusinessQue()!=null){
	           		applicant.setOwnbusinessque(applicantObj.getOwnBusinessQue()); 
	           	}
	        	if(applicantObj.getOwnBusinessYesQue() != null){
	        		applicant.setOwnbusinessyesque(applicantObj.getOwnBusinessYesQue());
	        	}
	        	if(applicantObj.getOwnBusinessNoQue() != null){
	        		applicant.setOwnbusinesnosque(applicantObj.getOwnBusinessNoQue());
	        	}
	       	 	
	        	if(applicantObj.getPermanenTemplQue() != null){
	        		applicant.setPermanentemplque(applicantObj.getPermanenTemplQue() );
	        	}	
	        	if(applicantObj.getThirtyFiveHrsWrkQue()!=null){
	        		applicant.setThirtyfivehrswrkque(applicantObj.getThirtyFiveHrsWrkQue());
	        	}
	        	applicant.setWorkduties(applicantObj.getWorkDuties()); 
	        	applicant.setOccupationduties(applicantObj.getOccupationDuties());
	        	applicant.setSpendtimeoutside(applicantObj.getSpendTimeOutside());
	        	applicant.setTertiaryque(applicantObj.getTertiaryQue());
	        	
	        	 if(isNotNullandNotEmpty(applicantObj.getRelation())){
		            	applicant.setRelation(applicantObj.getRelation());
		            }
	
		          applicant.setApplicanttype(ApplicationConstants.APPTYPE_PRIMARY);
		        
		         if(applicantObj.getEmailId()!=null){
	            	applicant.setEmailid(applicantObj.getEmailId());
	            }
		         applicant.setDatejoinedfund(applicantObj.getDateJoinedFund());
		         applicant.setClientrefnumber(applicantObj.getClientRefNumber());
		         applicant.setMembertype(applicantObj.getMemberType());
		         applicant.setAge(applicantObj.getAge());
	            if(applicantObj.getCustomerReferenceNumber()!=null){
	            	applicant.setCustomerreferencenumber(applicantObj.getCustomerReferenceNumber());
	            }
	            applicant.setContacttype(applicantObj.getContactType());
	            applicant.setContactnumber(applicantObj.getContactNumber());
	            applicant.setOverallcoverdecision(applicantObj.getOverallCoverDecision());
		        
	            if(applicantObj.getIndexationDeath() != null && applicantObj.getIndexationDeath().equalsIgnoreCase(ApplicationConstants.CONST_TRUE)){
	            	applicant.setDeathindexationflag(ApplicationConstants.FLAG_ONE);
	        	}else{
	        		applicant.setDeathindexationflag(ApplicationConstants.FLAG_ZERO);
	        	}
	        	
	        	if(applicantObj.getIndexationTpd()!= null && applicantObj.getIndexationTpd().equalsIgnoreCase(ApplicationConstants.CONST_TRUE)){
	        		applicant.setTpdindexationflag(ApplicationConstants.FLAG_ONE);
	        	}else{
	        		applicant.setTpdindexationflag(ApplicationConstants.FLAG_ZERO);
	        	}
	        	
	        	if(applicantObj.getUnitisedCovers()!= null && applicantObj.getUnitisedCovers().equalsIgnoreCase(ApplicationConstants.CONST_TRUE)){
	        		applicant.setUnitcheckflag(ApplicationConstants.FLAG_ONE);
	        	}else{
	        		applicant.setUnitcheckflag(ApplicationConstants.FLAG_ZERO);
	        	}
	        	
	        	
	        	if(applicantObj.getInsuredSalary() != null && applicantObj.getInsuredSalary().equalsIgnoreCase(ApplicationConstants.CONST_TRUE)){
	        		applicant.setInsuredsalary(ApplicationConstants.FLAG_Y);
	        	}else{
	        		applicant.setInsuredsalary(ApplicationConstants.FLAG_N);
	        	}
		        
	        	
	        	applicant.setPreviousinsurername(applicantObj.getPreviousInsurerName());
	        	applicant.setFundmempolicynumber(applicantObj.getFundMemPolicyNumber());
	        	applicant.setSpinnumber(applicantObj.getSpinNumber());
	        	applicant.setPrevioustpdclaimque(applicantObj.getPreviousTpdClaimQue());
	        	if(applicantObj.getPreviousTerminalIllQue()!=null){
	        		applicant.setPreviousterminalillque(applicantObj.getPreviousTerminalIllQue());  /*for transfer and new member */
	        	}
	        	if(applicantObj.getDocumentEvidence() != null){
	        		applicant.setDocumentevidence(applicantObj.getDocumentEvidence());  /*for transfer and new member */
	        	}
	        	
	        	/*lifeevent mapping */
	        	
	        	if(isNotNullandNotEmpty(applicantObj.getLifeEvent())){
	        		applicant.setLifeevent(applicantObj.getLifeEvent());
	        		
	        	}
	        	if(isNotNullandNotEmpty(applicantObj.getLifeEventDate())){
	        		applicant.setLifeeventdate(applicantObj.getLifeEventDate());
	        	}
	        	if(isNotNullandNotEmpty(applicantObj.getEventAlreadyApplied())){
	        		applicant.setEventalreadyapplied(applicantObj.getEventAlreadyApplied());
	        	}
	        	
	        	/*For Cancel Cover*/
	        	if(isNotNullandNotEmpty(applicantObj.getCancelReason())){
	        		applicant.setCancelreason(applicantObj.getCancelReason());
	        	}
	        	if(isNotNullandNotEmpty(applicantObj.getCancelOtherReason())){
	        		applicant.setCancelotherreason(applicantObj.getCancelOtherReason());
	        	}
 			
			}//applicantobj not null Ends
			log.info("EapplyServiceHelper.getapplicantInfo End....");
			return applicant;
	}
	
	private static List<Cover> createChangeCvCovers(EappInput eappInput) {
		
		log.info("EapplyServiceHelper.createChangeCvCovers start....");
		Cover deathCover = null;
		Cover tpdCover = null;
		Cover ipCover = null;
		List<Cover> coverList=new ArrayList<>();
		
		if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
			for (Iterator iterator = eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
				CoverInfo coverInfoObj = (CoverInfo)iterator.next();
				if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
					deathCover = new Cover();
					
					deathCover.setCreatedate(new Timestamp(new Date().getTime()));
					deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
					deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
					deathCover.setCoverdecision(coverInfoObj.getCoverDecision());
					deathCover.setCoverreason(coverInfoObj.getCoverReason());
					deathCover.setCoverexclusion(coverInfoObj.getCoverExclusion());
					
					if(coverInfoObj.getCoverDecision()==null && !"DCL".equalsIgnoreCase(eappInput.getAppDecision())){
						coverInfoObj.setCoverDecision("ACC");
		 			}
					
					
					if(coverInfoObj.getFixedUnit()!=null) {
						deathCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					
					if(coverInfoObj.getLoading()!=null){
						deathCover.setLoading(new BigDecimal(coverInfoObj.getLoading()));
					}
					
					if(coverInfoObj.getPolicyFee()!= null){
						deathCover.setPolicyfee(new BigDecimal( coverInfoObj.getPolicyFee()));
					}
					deathCover.setOccupationrating(coverInfoObj.getOccupationRating());
					deathCover.setCoveroption(coverInfoObj.getCoverOption());
					deathCover.setCost(""+coverInfoObj.getCost());
					
					if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
						deathCover.setCovercategory(coverInfoObj.getCoverCategory());
						//When unitized set the additional unit - unit value enterd in cover required text box 
						deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit()).intValue()));			
						deathCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}else if (coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)) {
						deathCover.setCovercategory(coverInfoObj.getCoverCategory());
						// When fixed set the additional cover amount - amount value entered in the cover required text box
						deathCover.setAdditionalunit(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue());
						deathCover.setAddunitind(ApplicationConstants.FLAG_ZERO);
					}
					
	else if("INGD".equalsIgnoreCase(eappInput.getPartnerCode()) && (coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DCAUTOMATIC))){
						deathCover.setCovercategory(coverInfoObj.getCoverCategory());
						deathCover.setCoveroption(coverInfoObj.getCoverOption());
						//When unitized set the additional unit - unit value enterd in cover required text box 
						deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));			
						deathCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}else if("INGD".equalsIgnoreCase(eappInput.getPartnerCode()) && (coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DCTAILORED))){
						deathCover.setCovercategory(coverInfoObj.getCoverCategory());
						deathCover.setCoveroption(coverInfoObj.getCoverOption());
						//When unitized set the additional unit - unit value enterd in cover required text box 
						deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));			
						deathCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}
					if(coverInfoObj.getExistingCoverAmount() !=null){
						deathCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					deathCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					deathCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					
					coverList.add(deathCover);
				}
				if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
					tpdCover = new Cover();
					
					
					tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
					tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
					tpdCover.setCovercode(MetlifeInstitutionalConstants.COVERTYPE_TPD);
					tpdCover.setCoverdecision(coverInfoObj.getCoverDecision());
					tpdCover.setCoverreason(coverInfoObj.getCoverReason());
					tpdCover.setCoverexclusion(coverInfoObj.getCoverExclusion());

					if(coverInfoObj.getCoverDecision()==null && !"DCL".equalsIgnoreCase(eappInput.getAppDecision())){
						coverInfoObj.setCoverDecision("ACC");
					}
					
					if(coverInfoObj.getFixedUnit()!=null){
						tpdCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					
					if(coverInfoObj.getLoading()!=null) {
						tpdCover.setLoading(new BigDecimal(coverInfoObj.getLoading()));
					}
					if(coverInfoObj.getPolicyFee() != null){
						tpdCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
					}
					tpdCover.setOccupationrating(coverInfoObj.getOccupationRating());
					tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
					tpdCover.setCoveroption(coverInfoObj.getCoverOption());
					tpdCover.setCost(""+coverInfoObj.getCost());
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						tpdCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
						tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
						tpdCover.setAddunitind("1");
						tpdCover.setAdditionalunit(new BigDecimal(coverInfoObj.getAdditionalUnit()).intValue());
					}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
						tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
						tpdCover.setAddunitind("0");
						tpdCover.setAdditionalunit(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue());
	       }else if("INGD".equalsIgnoreCase(eappInput.getPartnerCode()) && (coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDAUTOMATIC))){
						tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
						tpdCover.setCoveroption(coverInfoObj.getCoverOption());
						//When unitized set the additional unit - unit value enterd in cover required text box 
						tpdCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));			
						tpdCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}else if("INGD".equalsIgnoreCase(eappInput.getPartnerCode()) &&(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDTAILORED))){
						tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
						tpdCover.setCoveroption(coverInfoObj.getCoverOption());
						tpdCover.setAddunitind("0");
						tpdCover.setAdditionalunit(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue());
					}
					
					tpdCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					tpdCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					
					coverList.add(tpdCover);
				}
				if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
					ipCover = new Cover();
					
					
					ipCover.setCreatedate(new Timestamp(new Date().getTime()));
					ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
					ipCover.setCovercode(MetlifeInstitutionalConstants.COVERTYPE_IP);
					ipCover.setCoverdecision(coverInfoObj.getCoverDecision());
					ipCover.setCoverreason(coverInfoObj.getCoverReason());
					ipCover.setCoverexclusion(coverInfoObj.getCoverExclusion());
					
					ipCover.setOccupationrating(coverInfoObj.getOccupationRating());
					ipCover.setCovercategory(coverInfoObj.getCoverCategory());
					ipCover.setCoveroption(coverInfoObj.getCoverOption());
					ipCover.setCost(""+coverInfoObj.getCost());
					ipCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					
					if(coverInfoObj.getCoverDecision()==null && !"DCL".equalsIgnoreCase(eappInput.getAppDecision())){
						coverInfoObj.setCoverDecision("ACC");	
					}
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						ipCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					if(coverInfoObj.getFixedUnit()!=null) {
						ipCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					
					if(coverInfoObj.getLoading()!=null && coverInfoObj.getLoading().trim().length()>ZERO_CONST) {
						ipCover.setLoading(new BigDecimal(coverInfoObj.getLoading()));
					}
					if(coverInfoObj.getPolicyFee()!= null){
						ipCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
					}
					
					if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED) && coverInfoObj.getAdditionalUnit()!= null){
						ipCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());
						ipCover.setAddunitind("1");
					}else if("INGD".equalsIgnoreCase(eappInput.getPartnerCode()) && (coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IPTAILORED))){
						ipCover.setCovercategory(coverInfoObj.getCoverCategory());
						//When unitized set the additional unit - unit value enterd in cover required text box 
						ipCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));			
						ipCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}
					ipCover.setOptionalunit(coverInfoObj.getOptionalUnit());  /*added for 85% salary check-box */
					
						
					if(coverInfoObj.getAdditionalCoverAmount()!= null){
					    ipCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					ipCover.setExistingipwaitingperiod(coverInfoObj.getExistingIpWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverInfoObj.getExistingIpBenefitPeriod());
					
					ipCover.setAdditionalipbenefitperiod(coverInfoObj.getAdditionalIpBenefitPeriod());
					ipCover.setAdditionalipwaitingperiod(coverInfoObj.getAdditionalIpWaitingPeriod());					
					coverList.add(ipCover);
				}
			}	
		}
		log.info("EapplyServiceHelper.createChangeCvCovers End....");
		return coverList;
	}
	
	private static List<Cover> createTransferCvCovers(EappInput eappInput) {
		
		log.info("EapplyServiceHelper.createTransferCvCovers start....");
		Cover deathCover = null;
		Cover tpdCover = null;
		Cover ipCover = null;
		List<Cover> coverList=new ArrayList<>();
		
		
		if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
		
			for (Iterator iterator =  eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
				CoverInfo coverInfoObj = (CoverInfo)iterator.next();
				if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
					//Death Cover
					deathCover = new Cover();
					
					deathCover.setCreatedate(new Timestamp(new Date().getTime()));
					deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
					deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
					deathCover.setCoverdecision(coverInfoObj.getCoverDecision());
					deathCover.setCoverreason(coverInfoObj.getCoverReason());
					if(coverInfoObj.getFixedUnit()!=null) {
						deathCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					deathCover.setOccupationrating(coverInfoObj.getOccupationRating());
					deathCover.setCovercategory(coverInfoObj.getCoverCategory());
					
					deathCover.setCost(""+coverInfoObj.getCost());
					
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						deathCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
													
					deathCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					if(coverInfoObj.getTransferCoverAmount() != null){
						deathCover.setTransfercoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));	
					}
					deathCover.setAdditionalcoveramount(coverInfoObj.getTransferCoverAmount());
					
					if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED) && coverInfoObj.getAdditionalUnit() != null)
					{	
						deathCover.setAdditionalunit(Integer.valueOf(coverInfoObj.getAdditionalUnit()));
						deathCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}
					
					
					if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())){
						if(coverInfoObj.getTransferCoverAmount() != null){
							deathCover.setAdditionalunit(coverInfoObj.getTransferCoverAmount().intValueExact());	
						}
						deathCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						
					}
					
					coverList.add(deathCover);
				}else if (coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
					//TPD Cover
					tpdCover = new Cover();
					tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
					tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
					tpdCover.setCovercode(MetlifeInstitutionalConstants.COVERTYPE_TPD);
					tpdCover.setCoverdecision(coverInfoObj.getCoverDecision());
					tpdCover.setCoverreason(coverInfoObj.getCoverReason());
					
					if(coverInfoObj.getFixedUnit()!=null) {
						tpdCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					
					tpdCover.setOccupationrating(coverInfoObj.getOccupationRating());
					tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
					
					tpdCover.setCost(""+coverInfoObj.getCost());
					
					
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						tpdCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					
					tpdCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					if(coverInfoObj.getTransferCoverAmount() != null){
						tpdCover.setTransfercoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					tpdCover.setAdditionalcoveramount(coverInfoObj.getTransferCoverAmount());
					
					if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED) && coverInfoObj.getAdditionalUnit() != null)
					{	
						tpdCover.setAdditionalunit(Integer.valueOf(coverInfoObj.getAdditionalUnit()));
				tpdCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}
//Added for INGD
					if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())){
						if(coverInfoObj.getTransferCoverAmount() != null){
							//tpdCover.setTransfercoveramount(coverInfoObj.getTransferCoverAmount());	
							tpdCover.setAdditionalunit(coverInfoObj.getTransferCoverAmount().intValueExact());
						}
						tpdCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						
					}
					
					coverList.add(tpdCover);
				}else if (coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
					//IP Cover
					ipCover = new Cover();
					ipCover.setCreatedate(new Timestamp(new Date().getTime()));
					ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
					ipCover.setCovercode(MetlifeInstitutionalConstants.COVERTYPE_IP);
					ipCover.setCoverdecision(coverInfoObj.getCoverDecision());
					ipCover.setCoverreason(coverInfoObj.getCoverReason());
					ipCover.setCovercategory(coverInfoObj.getCoverCategory());
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						ipCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					if(coverInfoObj.getFixedUnit()!=null && coverInfoObj.getFixedUnit().trim()!="") {
						ipCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					ipCover.setOccupationrating(coverInfoObj.getOccupationRating());
					ipCover.setCost(""+coverInfoObj.getCost());
					ipCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					if(coverInfoObj.getTransferCoverAmount()!= null){
						ipCover.setTransfercoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					if(coverInfoObj.getAdditionalCoverAmount()!= null){
						ipCover.setAdditionalcoveramount(coverInfoObj.getTransferCoverAmount());
					}
					
					ipCover.setAdditionalipbenefitperiod(coverInfoObj.getAdditionalIpBenefitPeriod());
					ipCover.setAdditionalipwaitingperiod(coverInfoObj.getAdditionalIpWaitingPeriod());
					ipCover.setTotalipwaitingperiod(coverInfoObj.getTotalIpWaitingPeriod());
					ipCover.setTotalipbenefitperiod(coverInfoObj.getTotalIpBenefitPeriod());
					ipCover.setExistingipwaitingperiod(coverInfoObj.getExistingIpWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverInfoObj.getExistingIpBenefitPeriod());
					
					if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED) && coverInfoObj.getAdditionalUnit() != null)
					{
						ipCover.setAdditionalunit(Integer.valueOf(coverInfoObj.getAdditionalUnit()));
						ipCover.setAddunitind(ApplicationConstants.FLAG_ONE);
					}
					if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())){
						
						if(coverInfoObj.getTransferCoverAmount()!= null){
							ipCover.setAdditionalunit(coverInfoObj.getTransferCoverAmount().intValueExact());
						}
						
					}
				
					coverList.add(ipCover);
				}
			}
			
		}
		log.info("EapplyServiceHelper.createTransferCvCovers End....");
		return coverList;
	}
	
	private static List<Cover> createLifeEventCovers(EappInput eappInput) {
		
		log.info("EapplyServiceHelper.createLifeEventCovers start....");
		Cover deathCover = null;
		Cover tpdCover = null;
		Cover ipCover = null;
		List<Cover> coverList=new ArrayList<>();

		if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
			for (Iterator iterator = eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
				CoverInfo coverInfoObj = (CoverInfo)iterator.next();
				if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
					deathCover = new Cover();
					
					deathCover.setCreatedate(new Timestamp(new Date().getTime()));
					deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
					deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
					deathCover.setCoverdecision(coverInfoObj.getCoverDecision());
					deathCover.setCoverreason(coverInfoObj.getCoverReason());
					
					if(coverInfoObj.getFixedUnit()!=null) {
						deathCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					deathCover.setOccupationrating(coverInfoObj.getOccupationRating());
					deathCover.setCovercategory(coverInfoObj.getCoverCategory());
					deathCover.setCost(coverInfoObj.getCost());
					if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED) 
						&& coverInfoObj.getAdditionalUnit()!= null && coverInfoObj.getAdditionalUnit()!= ""){
						if(coverInfoObj.getFixedUnit()!=null) {
							deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue()+Integer.parseInt(coverInfoObj.getFixedUnit()));
						}
						deathCover.setAdditionalcoveramount((new BigDecimal(coverInfoObj.getAdditionalCoverAmount())));
					}
					if(coverInfoObj.getExistingCoverAmount()!=null){
						deathCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					deathCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					if(coverInfoObj.getCoverCategory() !=null){
						if(coverInfoObj.getCoverCategory() .equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
							deathCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory() .equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
							deathCover.setAddunitind("0");
						}
					}
					
					
				}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
					
					tpdCover = new Cover();
					tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
					tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
					tpdCover.setCovercode("TPD");
					tpdCover.setCoverdecision(coverInfoObj.getCoverDecision());
					tpdCover.setCoverreason(coverInfoObj.getCoverReason());
					if(coverInfoObj.getFixedUnit()!= null && coverInfoObj.getFixedUnit() != "") {
						tpdCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					tpdCover.setOccupationrating(coverInfoObj.getOccupationRating());
					tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
					tpdCover.setCost(coverInfoObj.getCost());
					if(coverInfoObj.getExistingCoverAmount()!=null){
						tpdCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory() .equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)
							&& coverInfoObj.getAdditionalUnit() != null && coverInfoObj.getAdditionalUnit()!= ""){
						if(coverInfoObj.getFixedUnit()!=null) {
							tpdCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue()+Integer.parseInt(coverInfoObj.getFixedUnit()));
						}
						tpdCover.setAdditionalcoveramount((new BigDecimal(coverInfoObj.getAdditionalCoverAmount())));
					}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory() .equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)
							&& coverInfoObj.getAdditionalCoverAmount()!= null && coverInfoObj.getAdditionalCoverAmount() != ""){
						tpdCover.setAdditionalcoveramount((new BigDecimal(coverInfoObj.getAdditionalCoverAmount())));		
					}
					
					tpdCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					if(coverInfoObj.getCoverCategory()!=null){
						if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
							tpdCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
							tpdCover.setAddunitind("0");
						}
					}
					
				}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
					ipCover = new Cover();
					if(coverInfoObj.getExistingCoverAmount()!=null){
						ipCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						
					}		
					if(coverInfoObj.getFixedUnit() != null && coverInfoObj.getFixedUnit() !=""){
						ipCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					
					ipCover.setCreatedate(new Timestamp(new Date().getTime()));
					ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
					ipCover.setCovercode("IP");
					ipCover.setCoverdecision(coverInfoObj.getCoverDecision());
					ipCover.setCoverreason(coverInfoObj.getCoverReason());
					ipCover.setOccupationrating(coverInfoObj.getOccupationRating());
					ipCover.setCovercategory(coverInfoObj.getCoverCategory());
					ipCover.setCost(coverInfoObj.getCost());
					ipCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					
					if( null!= coverInfoObj.getCoverCategory()&& MetlifeInstitutionalConstants.IP_UNITISED.equalsIgnoreCase(coverInfoObj.getCoverCategory())
							&& null!=coverInfoObj.getAdditionalUnit() && coverInfoObj.getAdditionalUnit()!="")
					{
						ipCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());		
					}
					ipCover.setOptionalunit(coverInfoObj.getOptionalUnit());  /*added for 85% salary check-box */
					
					if(coverInfoObj.getCoverCategory()!=null){
						if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
							ipCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
							ipCover.setAddunitind("0");
						}
					}	
					if(coverInfoObj.getAdditionalCoverAmount()!= null){
					    ipCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					
					ipCover.setExistingipwaitingperiod(coverInfoObj.getExistingIpWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverInfoObj.getExistingIpBenefitPeriod());
					
					ipCover.setAdditionalipbenefitperiod(coverInfoObj.getAdditionalIpBenefitPeriod());
					ipCover.setAdditionalipwaitingperiod(coverInfoObj.getAdditionalIpWaitingPeriod());
				}
			}
			
		}
		
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		/*Cover object mapping end*/
		log.info("EapplyServiceHelper.createLifeEventCovers End....");
		return coverList;
	}
	
	
	private static List<Cover> createCancelCvCovers(EappInput eappInput) {
		 log.info("Create Cancel cover start");
			
			
			/*Cover object mapping start*/        	
			
			Cover deathCover=new Cover();
			Cover tpdCover=new Cover();
			Cover ipCover=new Cover();
			List<Cover> coverList=new ArrayList<>();
			
			
			if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
				
				for (Iterator iterator =  eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
					CoverInfo coverInfoObj = (CoverInfo)iterator.next();
					if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
						//Death Cover
						deathCover = new Cover();
						
						deathCover.setCreatedate(new Timestamp(new Date().getTime()));
						deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
						deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
						deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
						deathCover.setCoverdecision("ACC");
						deathCover.setCoverreason(coverInfoObj.getCoverReason());
						deathCover.setCoverexclusion(coverInfoObj.getCoverExclusion());
						
						if(coverInfoObj.getFixedUnit()!=null && coverInfoObj.getFixedUnit() !=""){
							deathCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						}
						if(coverInfoObj.getLoading()!=null){
						 deathCover.setLoading(new BigDecimal((coverInfoObj.getLoading())));
						}
						
						deathCover.setOccupationrating(coverInfoObj.getOccupationRating());
						deathCover.setCost(coverInfoObj.getCost());
						if(coverInfoObj.getExistingCoverAmount()!=null){
							deathCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						}
												
						if(null != coverInfoObj.getFrequencyCostType() && !"".equalsIgnoreCase(coverInfoObj.getFrequencyCostType())){
							deathCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
						}
						if(coverInfoObj.getPolicyFee()!= null) {
							deathCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
						}
						deathCover.setCovercategory(coverInfoObj.getCoverCategory());
						deathCover.setCoveroption(coverInfoObj.getCoverOption());
						
						if(coverInfoObj.getAdditionalCoverAmount()!=null) {
							deathCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						}
						if(coverInfoObj.getAdditionalUnit()!= null) {
							deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());
						}
						//Added for INGD
						if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())&& coverInfoObj.getAdditionalCoverAmount()!=null ){
							deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));
						}
						if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
							deathCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
							deathCover.setAddunitind("0");
						}
											
					}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
						tpdCover = new Cover();
						
						
						tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
						tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
						tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
						tpdCover.setCovercode("TPD");
						tpdCover.setCoverdecision("ACC");
						
						
						tpdCover.setCoverreason(coverInfoObj.getCoverReason());
						tpdCover.setCoverexclusion(coverInfoObj.getCoverExclusion());
						
						if(coverInfoObj.getFixedUnit()!=null && coverInfoObj.getFixedUnit() !=""){
							tpdCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						}
						if(coverInfoObj.getLoading()!=null){
							tpdCover.setLoading(new BigDecimal((coverInfoObj.getLoading())));
						}
						
						tpdCover.setOccupationrating(coverInfoObj.getOccupationRating());
						tpdCover.setCost(coverInfoObj.getCost());
						if(coverInfoObj.getExistingCoverAmount()!=null){
							tpdCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						}
												
						if(null != coverInfoObj.getFrequencyCostType() && !"".equalsIgnoreCase(coverInfoObj.getFrequencyCostType())){
							tpdCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
						}
						if(coverInfoObj.getPolicyFee()!= null) {
							tpdCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
						}
						tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
						tpdCover.setCoveroption(coverInfoObj.getCoverOption());
						if(coverInfoObj.getAdditionalCoverAmount()!=null) {
							tpdCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						}
						if(coverInfoObj.getAdditionalUnit()!= null) {
							tpdCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());
						}
						//Added for INGD
						if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())&& coverInfoObj.getAdditionalCoverAmount()!=null ){
							tpdCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));
						}
						if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
							tpdCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
							tpdCover.setAddunitind("0");
						}
						
					}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
						ipCover = new Cover();
						
						if(coverInfoObj.getExistingCoverAmount()!=null){
							ipCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						}
						ipCover.setCreatedate(new Timestamp(new Date().getTime()));
						ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
						ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
						ipCover.setCovercode("IP");
						ipCover.setCoverdecision("ACC");
						
						
						ipCover.setCoverreason(coverInfoObj.getCoverReason());
						ipCover.setCoverexclusion(coverInfoObj.getCoverExclusion());
						ipCover.setCost(coverInfoObj.getCost());
						if(coverInfoObj.getLoading()!=null){
							ipCover.setLoading(new BigDecimal((coverInfoObj.getLoading())));
						}
						if(null != coverInfoObj.getFrequencyCostType() && !"".equalsIgnoreCase(coverInfoObj.getFrequencyCostType())){
							ipCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
						}
						ipCover.setOccupationrating(coverInfoObj.getOccupationRating());
						if(coverInfoObj.getPolicyFee()!= null) {
							ipCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
						}
						
						ipCover.setCovercategory(coverInfoObj.getCoverCategory());
						ipCover.setCoveroption(coverInfoObj.getCoverOption());

						if(coverInfoObj.getAdditionalCoverAmount()!=null) {
							ipCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						}
						
						if(coverInfoObj.getFixedUnit()!=null && coverInfoObj.getFixedUnit() !=""){
							ipCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						}
						if(coverInfoObj.getAdditionalUnit()!= null) {
							ipCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());
						}
						//Added for INGD
						if("INGD".equalsIgnoreCase(eappInput.getPartnerCode())&& coverInfoObj.getAdditionalCoverAmount()!=null ){
							ipCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalCoverAmount()).intValue()));
						}
						if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
							ipCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory()!= null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
							ipCover.setAddunitind("0");
						}
						
						
					}
				}
			}
			
			coverList.add(deathCover);
			coverList.add(tpdCover);
			coverList.add(ipCover);
			/*Cover object mapping end*/
			log.info("Create cancel cover finish");
			return coverList;
	}
	
	
	private static List<Cover> createUpdateWorkRatingCvCovers(EappInput eappInput) {
		log.info("EapplyServiceHelper.createUpdateWorkRatingCvCovers start....");
		
		Cover deathCover = null;
		Cover tpdCover = null;
		Cover ipCover = null;
		List<Cover> coverList=new ArrayList<>();
				
		if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
			for (Iterator iterator = eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
				CoverInfo coverInfoObj = (CoverInfo)iterator.next();
				if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
					deathCover = new Cover();
					
					deathCover.setCreatedate(new Timestamp(new Date().getTime()));
					deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
					deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
					deathCover.setCoverdecision(coverInfoObj.getCoverDecision());
					deathCover.setCoverreason(coverInfoObj.getCoverReason());
					if(coverInfoObj.getFixedUnit()!= null && coverInfoObj.getFixedUnit() !=""){
						deathCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						deathCover.setAdditionalunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					deathCover.setOccupationrating(coverInfoObj.getOccupationRating());
					deathCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					deathCover.setCovercategory(coverInfoObj.getCoverCategory());
					if(coverInfoObj.getCoverCategory()!=null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
							deathCover.setAddunitind("1");
					}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
							deathCover.setAddunitind("0");
					}
					
					deathCover.setCost(""+coverInfoObj.getCost());
					if(coverInfoObj.getExistingCoverAmount()!=null){
						deathCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					if(coverInfoObj.getAdditionalCoverAmount() != null){
						deathCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					
				}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
					tpdCover = new Cover();
					
					
					tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
					tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
					tpdCover.setCovercode("TPD");
					tpdCover.setCoverdecision(coverInfoObj.getCoverDecision());
					tpdCover.setCoverreason(coverInfoObj.getCoverReason());
					
					if(coverInfoObj.getFixedUnit()!= null && coverInfoObj.getFixedUnit() !=""){
						tpdCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						tpdCover.setAdditionalunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					tpdCover.setOccupationrating(coverInfoObj.getOccupationRating());
					
					tpdCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
					
					if(coverInfoObj.getCoverCategory()!=null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
						tpdCover.setAddunitind("1");
					}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
						tpdCover.setAddunitind("0");
					}
					tpdCover.setCost(""+coverInfoObj.getCost());
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						tpdCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
					}
					if(coverInfoObj.getAdditionalCoverAmount()!=null){
						tpdCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					
				}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
					ipCover = new Cover();
					
					
					ipCover.setCreatedate(new Timestamp(new Date().getTime()));
					ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
					ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
					ipCover.setCovercode("IP");
					ipCover.setCoverdecision(coverInfoObj.getCoverDecision());
					ipCover.setCoverreason(coverInfoObj.getCoverReason());
					ipCover.setOccupationrating(coverInfoObj.getOccupationRating());
					
					ipCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
					ipCover.setCovercategory(coverInfoObj.getCoverCategory());
					
					if(coverInfoObj.getExistingCoverAmount()!=null){
						ipCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						ipCover.setAdditionalcoveramount(coverInfoObj.getExistingCoverAmount());
					}	
						            
					if(coverInfoObj.getFixedUnit()!=null){
						ipCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						ipCover.setAdditionalunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
					}
					
					ipCover.setExistingipwaitingperiod(coverInfoObj.getExistingIpWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverInfoObj.getExistingIpBenefitPeriod());
					ipCover.setAdditionalipbenefitperiod(coverInfoObj.getAdditionalIpBenefitPeriod());
					ipCover.setAdditionalipwaitingperiod(coverInfoObj.getAdditionalIpWaitingPeriod());
					
					if(coverInfoObj.getCoverCategory()!=null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
						ipCover.setAddunitind("1");
					}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
						ipCover.setAddunitind("0");
					}
					
					ipCover.setCost(""+coverInfoObj.getCost());
					if(coverInfoObj.getAdditionalCoverAmount()!= null){
						ipCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
					}
					
				}
			}
		}
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		
		log.info("EapplyServiceHelper.createUpdateWorkRatingCvCovers End....");
		return coverList;
	}
	
	
	private static List<Cover> createNonMemberCvCovers(EappInput eappInput) {
		log.info("EapplyServiceHelper.createNonMemberCvCovers Start....");
						
			Cover deathCover=new Cover();
			Cover tpdCover=new Cover();
			Cover ipCover=new Cover();
			List<Cover> coverList=new ArrayList<>();
			
			if(eappInput.getApplicant() != null && eappInput.getApplicant().getCover()!= null && !eappInput.getApplicant().getCover().isEmpty()){
				for (Iterator iterator = eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
					CoverInfo coverInfoObj = (CoverInfo)iterator.next();
					if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("1")) {
						deathCover = new Cover();
						deathCover.setCreatedate(new Timestamp(new Date().getTime()));
						deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
						deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
						deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
						deathCover.setCoverdecision(coverInfoObj.getCoverDecision());
						deathCover.setCoverreason(coverInfoObj.getCoverReason());
						deathCover.setCoverexclusion(coverInfoObj.getCoverExclusion());

						if( coverInfoObj.getFixedUnit() != null && coverInfoObj.getFixedUnit()!=""){

							deathCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						}
						if(coverInfoObj.getLoading()!=null){
						 deathCover.setLoading(new BigDecimal((coverInfoObj.getLoading())));
						}
						if(coverInfoObj.getPolicyFee() != null){
							deathCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
						}
						deathCover.setOccupationrating(coverInfoObj.getOccupationRating());
						deathCover.setCovercategory(coverInfoObj.getCoverCategory());
						deathCover.setCoveroption(coverInfoObj.getCoverOption());
						deathCover.setCost(""+coverInfoObj.getCost());
						if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
							deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());			
						}else{
							deathCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());
							
						}
						if(coverInfoObj.getExistingCoverAmount()!=null){
							deathCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						}
						deathCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
						if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
							deathCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
							deathCover.setAddunitind("0");
						}
						
						deathCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						
					}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("2")) {
						tpdCover = new Cover();
						
						tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
						tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
						tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
						tpdCover.setCovercode("TPD");
						tpdCover.setCoverdecision(coverInfoObj.getCoverDecision());
						tpdCover.setCoverreason(coverInfoObj.getCoverReason());
						tpdCover.setCoverexclusion(coverInfoObj.getCoverExclusion());

						if( coverInfoObj.getFixedUnit() != null && coverInfoObj.getFixedUnit()!=""){
							tpdCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						}

						if(coverInfoObj.getLoading()!=null)
							tpdCover.setLoading(new BigDecimal((coverInfoObj.getLoading())));
						if(coverInfoObj.getPolicyFee() != null){
							tpdCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
						}
						tpdCover.setOccupationrating(coverInfoObj.getOccupationRating());
						tpdCover.setCovercategory(coverInfoObj.getCoverCategory());
						tpdCover.setCoveroption(coverInfoObj.getCoverOption());
						tpdCover.setCost(""+coverInfoObj.getCost());
						if(coverInfoObj.getExistingCoverAmount()!=null){
							tpdCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						}
						if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
							tpdCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());			
						}
						tpdCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
						if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
							tpdCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
							tpdCover.setAddunitind("0");
						}
						tpdCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						
					}else if(coverInfoObj != null && coverInfoObj.getBenefitType().equalsIgnoreCase("4")) {
						ipCover = new Cover();
						
						if(coverInfoObj.getFixedUnit()!= null && coverInfoObj.getFixedUnit() !=""){
							ipCover.setFixedunit(Integer.valueOf(coverInfoObj.getFixedUnit()));
						}


						ipCover.setCreatedate(new Timestamp(new Date().getTime()));
						ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
						ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
						ipCover.setCovercode("IP");
						ipCover.setCoverdecision(coverInfoObj.getCoverDecision());
						ipCover.setCoverreason(coverInfoObj.getCoverReason());
						ipCover.setCoverexclusion(coverInfoObj.getCoverExclusion());
						if(coverInfoObj.getLoading()!=null && coverInfoObj.getLoading().trim().length()>ZERO_CONST)
							ipCover.setLoading(new BigDecimal((coverInfoObj.getLoading())));
						if(coverInfoObj.getPolicyFee()!= null){
							ipCover.setPolicyfee(new BigDecimal(coverInfoObj.getPolicyFee()));
						}
						ipCover.setOccupationrating(coverInfoObj.getOccupationRating());
						ipCover.setCovercategory(coverInfoObj.getCoverCategory());
						ipCover.setCoveroption(coverInfoObj.getCoverOption());
						ipCover.setCost(""+coverInfoObj.getCost());
						ipCover.setFrequencycosttype(coverInfoObj.getFrequencyCostType());
						if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
							ipCover.setAdditionalunit((new BigDecimal(coverInfoObj.getAdditionalUnit())).intValue());			
						}
						ipCover.setOptionalunit(coverInfoObj.getOptionalUnit());  /*added for 85% salary check-box */
						
						if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
							ipCover.setAddunitind("1");
						}else if(coverInfoObj.getCoverCategory() != null && coverInfoObj.getCoverCategory().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
							ipCover.setAddunitind("0");
						}
						if(coverInfoObj.getExistingCoverAmount() != null){
							ipCover.setExistingcoveramount(coverInfoObj.getExistingCoverAmount());
						}
						
						if(coverInfoObj.getAdditionalCoverAmount()!= null){
						    ipCover.setAdditionalcoveramount(new BigDecimal(coverInfoObj.getAdditionalCoverAmount()));
						}
						ipCover.setAdditionalipbenefitperiod(coverInfoObj.getAdditionalIpBenefitPeriod());
						ipCover.setAdditionalipwaitingperiod(coverInfoObj.getAdditionalIpWaitingPeriod());
					}
				}
			}			
			coverList.add(deathCover);
			coverList.add(tpdCover);
			coverList.add(ipCover);
			/*Cover object mapping end*/
			log.info("EapplyServiceHelper.createNonMemberCvCovers End....");
			return coverList;
	}	
	
	private static Contactinfo getcontactInfo(EappInput eappInput) {
		
		log.info("EapplyServiceHelper.getcontactInfo Start....");
		Contactinfo contactinfo = new Contactinfo();
		ContactDetails contDtlsObj = null;
		if(eappInput.getApplicant().getContactDetails()!=null) {
			contDtlsObj = eappInput.getApplicant().getContactDetails();
			contactinfo.setMobilephone(contDtlsObj.getMobilePhone());
	    	contactinfo.setHomephone(contDtlsObj.getHomePhone());
	    	contactinfo.setWorkphone(contDtlsObj.getWorkPhone());
	    	contactinfo.setPreferedcontactnumber(contDtlsObj.getPreferedContactNumber());
	    	contactinfo.setPreferedcontacttype(contDtlsObj.getPreferedContacType());
	    	contactinfo.setPreferedcontacttime(contDtlsObj.getPreferedContactTime());
	    	contactinfo.setAddressline1(contDtlsObj.getAddressLine1());
	    	contactinfo.setAddressline2(contDtlsObj.getAddressLine2());
	    	contactinfo.setCountry(contDtlsObj.getCountry());
	    	contactinfo.setPostcode(contDtlsObj.getPostCode());
	    	contactinfo.setState(contDtlsObj.getState());
	    	contactinfo.setSuburb(contDtlsObj.getSuburb());
	    	if(isNotNullandNotEmpty(contDtlsObj.getOtherContactType())) {
	    		contactinfo.setOthercontacttype(contDtlsObj.getOtherContactType());
	    	}
	    	if(isNotNullandNotEmpty(contDtlsObj.getOtherContactNumber())) {
	    		contactinfo.setOthercontactnumber(contDtlsObj.getOtherContactNumber());
	    	}
		}
		log.info("EapplyServiceHelper.getcontactInfo End....");
		return contactinfo;
	}
	
	private static Eapplication getDocumentIno(Eapplication eapplication, EappInput eappInput) {
		log.info("EapplyServiceHelper.getDocumentIno Start....");
				
		List<Document> documentList = new ArrayList<>();
			 if(eappInput.getTransferDocuments()!=null && !eappInput.getTransferDocuments().isEmpty()){
				 for(DocumentInfo docInfo:eappInput.getTransferDocuments()){
					 Document transferDocument = new Document();
					 transferDocument.setDocLoc(docInfo.getDocLoc());
					 transferDocument.setDocType(MetlifeInstitutionalConstants.TRANSFER_DOCUMENTS);
					 transferDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
					documentList.add(transferDocument);	 	 	
				 }
				 eapplication.setDocuments(documentList);
			 }
		
			 if(eappInput.getLifeEventDocuments()!=null && !eappInput.getLifeEventDocuments().isEmpty()){
				 for(DocumentInfo docInfo:eappInput.getLifeEventDocuments()){
					 Document transferDocument = new Document();
					 transferDocument.setDocLoc(docInfo.getDocLoc());
					 transferDocument.setDocType(MetlifeInstitutionalConstants.TRANSFER_DOCUMENTS);
					 transferDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
					documentList.add(transferDocument);	 	 	
				 }
				 eapplication.setDocuments(documentList);
			 }
			 log.info("EapplyServiceHelper.getDocumentIno End....");
		return eapplication;
		
	}
	
	 public static boolean isNotNullandNotEmpty(final String strValue) {
	      boolean isNotNullandNotEmpty = false;
	      if (strValue != null && !strValue.trim().isEmpty()) {
	    	  isNotNullandNotEmpty = true;
	      }
	      return isNotNullandNotEmpty;
	    }
	 
	 public static PDFObject callpdfGeneration(PDFObject pdfObject, String  pdfBasePath, EappInput eappInput, Eapplication eapplication, String fileUploadPath)throws DocumentException{
			log.info("EapplyServiceHelper.callpdfGeneration start"); 
			String clientPdfLocation;
			String pdfAttachDir;
			String logoPath;
			String uwPdfLocation;
			 String corpFundId = null;
			 InputStream url;
			 List<String> questionHideList = new ArrayList<>();
			Properties property = new Properties();
			SystemProperty sys;
			sys = SystemProperty.getInstance();
			File file = new File( pdfBasePath);
	        try {
				url = new FileInputStream( file);
			    property.load( url);
			
			/*need to set this*/
	        questionHideList.add("11241");
	        questionHideList.add("11242");
	        questionHideList.add("11243");
	        questionHideList.add("11251");
	        questionHideList.add("11252");        
	     
	         pdfObject.setQuestionListForHide(questionHideList);
		        pdfObject.setLob(MetlifeInstitutionalConstants.INSTITUTIONAL);		 

				pdfObject.setFundId(eapplication.getPartnercode());
				

				pdfObject.setSectionBackground(property.getProperty("ClientSectionColor_" + pdfObject.getFundId()));
				pdfObject.setColorStyle(property.getProperty("ClientColorStyle_" + pdfObject.getFundId()));
				pdfObject.setSectionTextColor(property.getProperty("ClientSectionTextColor_" + pdfObject.getFundId()));
				pdfObject.setBorderColor(pdfObject.getSectionBackground());
				logoPath = property.getProperty("logo_" + pdfObject.getFundId()); 
			    pdfObject.setLogPath(logoPath);	
				pdfObject.setApplicationNumber(""+eappInput.getApplicatioNumber());
				
				clientPdfLocation = getIndvPdfAttachDir(eapplication, MetlifeInstitutionalConstants.INSTITUTIONAL,MetlifeInstitutionalConstants.CLIENT,sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH),property.getProperty("fund_pdf_name"),corpFundId);
				pdfObject.setPdfAttachDir(clientPdfLocation);
				pdfObject.setClientPDFLoc(clientPdfLocation);
				GenerateEapplyPDF generateEapplyPDF = new GenerateEapplyPDF();
				
				pdfAttachDir = generateEapplyPDF.generatePDF(pdfObject,pdfBasePath);
				pdfObject.setFundId(eapplication.getPartnercode());
				pdfObject.setSectionBackground(property.getProperty("UWSectionColor_" + pdfObject.getFundId()));
	    		pdfObject.setColorStyle(property.getProperty("UWColorStyle_" + pdfObject.getFundId()));
	    		pdfObject.setSectionTextColor(property.getProperty("UWSectionTextColor_" + pdfObject.getFundId()));
	    		pdfObject.setBorderColor(pdfObject.getSectionBackground());
	    		pdfObject.setPdfType("UW");
	    		uwPdfLocation = getIndvPdfAttachDir(eapplication, MetlifeInstitutionalConstants.INSTITUTIONAL,"UW",sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH),"",corpFundId);
	    		pdfObject.setPdfAttachDir(uwPdfLocation);		
	    		pdfObject.setUwPDFLoc(uwPdfLocation);
	    		
					generateEapplyPDF.generatePDF(pdfObject, pdfBasePath);
				
				pdfObject.setUwPDFLoc(uwPdfLocation);
	        } catch (FileNotFoundException e) {
				log.error("Error in getting file {}",e);
			} catch (IOException e) {
				log.error("Error in input and output {}",e);
			}  
	        log.info("EapplyServiceHelper.callpdfGeneration End");
			return pdfObject;
		 }
	 
	 private static String getIndvPdfAttachDir(Eapplication eapplication,String lob,String type, String fileUploadPath,String fundPdfName,String corpFundId) {
		 log.info("EapplyServiceHelper.getIndvPdfAttachDir Start");
	        String pdfAttachDir;
	        Applicant applicantDTO;      
	        String baseDirPath =  fileUploadPath;
	        Applicant applicantDTOPrimary = null;
	        String applicantsNames = null;  
	        StringBuilder builder;
	       
	        if (null != eapplication && null != eapplication.getApplicant()) {
	            builder = new StringBuilder();
	            for (int itr = 0; itr < eapplication.getApplicant().size(); itr++) {
	                applicantDTO = eapplication.getApplicant().get( itr);                
	                applicantDTOPrimary = applicantDTO;
	            }          
	            if (null != applicantDTOPrimary) {
	                if (null != applicantDTOPrimary.getTitle())
	                    builder.append(applicantDTOPrimary.getTitle());
	                if (null != applicantDTOPrimary.getFirstname())
	                    builder.append(applicantDTOPrimary.getFirstname());
	                if (null != applicantDTOPrimary.getLastname())
	                    builder.append(applicantDTOPrimary.getLastname());
	            }          
	            applicantsNames = builder.toString();
	            
	            if(null != applicantsNames && applicantsNames.contains(" ")){
	            	applicantsNames = applicantsNames.replaceAll(" ", "");
				}
	          
	        }
	        if (null!=type && type.equalsIgnoreCase( "UW")) {                  
	         
	        		        	
	        	/*Attaching Client email for Fund Administrator mails -- Added by 169682*/
	        	if (null != eapplication && null != eapplication.getPartnercode()) {
	        		if(null!=corpFundId) {
	        		pdfAttachDir = baseDirPath +corpFundId+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        		}
	        		else {
	        			pdfAttachDir = baseDirPath +eapplication.getPartnercode()+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
		                + ".pdf";
		        		}
	        	}else{
	        		pdfAttachDir = baseDirPath +MetlifeInstitutionalConstants.CLIENT+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        	}
	            
	        }else {
	        	if(null!=fundPdfName && !"".equalsIgnoreCase(fundPdfName)){
	        		if(null!=corpFundId) {
		        		pdfAttachDir = baseDirPath +corpFundId+fundPdfName+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
		                + ".pdf";
		        		}
	        		else {
		        	pdfAttachDir = baseDirPath +fundPdfName+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        		}
	        	}else{
	        		pdfAttachDir = baseDirPath +MetlifeInstitutionalConstants.CLIENT+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        	}
	        }
	        log.info("EapplyServiceHelper.getIndvPdfAttachDir End");
	        return pdfAttachDir;
	    }
}
