package au.com.metlife.eapply.b2b.model;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface  EappstringRepository extends JpaRepository<Eappstring, String>{
	public Eappstring findById(Long id);
//	public Eappstring findBySecureToken(String secureToken);
	@Query("SELECT t FROM Eappstring t where t.secureToken = ?1 AND t.createdate > ?2")
	public Eappstring findBySecureToken(String secureToken,Timestamp createdate);
	
	
}
