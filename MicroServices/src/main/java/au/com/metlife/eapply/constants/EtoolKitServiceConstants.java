package au.com.metlife.eapply.constants;

public class EtoolKitServiceConstants {
	private EtoolKitServiceConstants(){
		
	}
	
	public static final String ERR_NOT_FOUND = "404";
	public static final String ERR_BAD_REQUEST = "400";
	public static final String ERR_SERVER_ERROR = "500";
	
	public static final String ERRMSG_MEMBER_NOT_FOUND = "Member not found";
	public static final String ERRMSG_FUND_NOT_FOUND = "Fund not found";
	public static final String ERRMSG_MEMBERS_NOT_FOUND = "Members not found";
	public static final String ERRMSG_SERVER_ERROR = "Server Error";
	
	public static final String ERR_CD_DATABASE_NOT_AVAILABLE = "9501";
	public static final String ERR_CD_DATA_ACCESS = "9503";
	public static final String ERR_CD_BAD_INPUT_DATA = "9400";
	public static final String ERR_CD_UNCATEGORIZED = "9500";
	public static final String ERR_CD_RESOURCE_NOT_FOUND = "9404";

}
