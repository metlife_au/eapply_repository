function TimeoutConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.timeout', {
    url: '/timeout',
    controller: 'TimeoutCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'timeout/timeout.html',
    title: 'Session Timeout',
    resolve: {
      urls: function(CommonAPI, AppConstants) {
        return !CommonAPI.getUrlList() && CommonAPI.fetchApiUrls(AppConstants.urlFilePath);
      }
    }
  });

};

export default TimeoutConfig;
