package pages.metlife;

import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.STFTCCStubPageNewObjects;

public class STFTCCStubPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public STFTCCStubPage enterInputXML() {
		InputXMLDetails();
		return new STFTCCStubPage(scriptHelper);
	}

	public STFTCCStubPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void InputXMLDetails() {
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			String inputXML = dataTable.getData("MetLife_Data", "XML");
			Thread.sleep(1000);
						
			tryAction(fluentWaitElement(STFTCCStubPageNewObjects.txtareainput),"SET", "Input",inputXML);
		
			sleep(4000);
		
			switchToActiveWindow();
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTCCStubPageNewObjects.btnApply)).click().perform();
			Thread.sleep(4000);
			
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
