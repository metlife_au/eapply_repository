package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractAmountItem")


@XmlRootElement
public class ContractAmountItem {	
	
	private String ItemAmount;

	public String getItemAmount() {
		return ItemAmount;
	}

	public void setItemAmount(String itemAmount) {
		ItemAmount = itemAmount;
	}
	

	
	
	
}
