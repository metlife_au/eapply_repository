package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesGetQuotePageObjects extends PageObjectLibrary{
	public static final By btnNewApplication = getObjectIdentifaction("coles.newapplication.button.css");
	
	public static final By txtFirstName = getObjectIdentifaction("coles.getquote.firstname.text.id");
	public static final By txtLastName = getObjectIdentifaction("coles.getquote.lastname.text.id");
	public static final By txtPhoneNumber = getObjectIdentifaction("coles.getquote.phonenumber.text.id");
	public static final By txtDOB = getObjectIdentifaction("coles.getquote.dob.text.css");
	public static final By rdMale = getObjectIdentifaction("coles.getquote.gender.male.radio.id");
	public static final By rdFemale = getObjectIdentifaction("coles.getquote.gender.female.radio.id");
	public static final By rdSmokedIn12MthsYes = getObjectIdentifaction("coles.getquote.smokedin12months.yes.radio.id");
	public static final By rdSmokedIn12MthsNo = getObjectIdentifaction("coles.getquote.smokedin12months.no.radio.id");
	public static final By btnState = getObjectIdentifaction("coles.getquote.state.dropdown.css");
	public static final By linkState = getObjectIdentifaction("coles.getquote.state.nsw.linktext");
	public static final By selCoverAmt = getObjectIdentifaction("coles.getquote.coveramount.dropdown.css");
	public static final By linkAmount2 = getObjectIdentifaction("coles.getquote.state.two.linktext");
	public static final By linkAmount1 = getObjectIdentifaction("coles.getquote.state.one.linktext");
	public static final By selFrequency = getObjectIdentifaction("coles.getquote.frequency.dropdown.css");
	public static final By linkPayFreqFortNightly = getObjectIdentifaction("coles.getquote.paymentfreq.fortnightly.linktext");
	public static final By linkPayFreqMonthly = getObjectIdentifaction("coles.getquote.paymentfreq.monthly.linktext");
	public static final By linkPayFreqYearly = getObjectIdentifaction("coles.getquote.paymentfreq.yearly.linktext");
	public static final By rdFlyBuysYes = getObjectIdentifaction("coles.getquote.flybuys.member.yes.radio.id");
	public static final By rdFlyBuysNo = getObjectIdentifaction("coles.getquote.flybuys.member.no.radio.id");
	public static final By rdFlyBuysJoinYes = getObjectIdentifaction("coles.getquote.flybuys.join.yes.radio.id");
	public static final By rdFlyBuysJoinNo = getObjectIdentifaction("coles.getquote.flybuys.join.no.radio.id");
	public static final By lnkCalculateQuote = getObjectIdentifaction("coles.getquote.calculatequote.link.css");
	public static final By lnkCalculateQuoteId = getObjectIdentifaction("coles.getquote.calculatequote.link.id");
	public static final By lnkApply = getObjectIdentifaction("coles.getquote.apply.link.id");
	public static final By lblPremium = getObjectIdentifaction("coles.stepped.premium.label.css");
	public static final By lblLevelPremium = getObjectIdentifaction("coles.level.premium.label.css");
	public static final By lblLevelPremium1 = getObjectIdentifaction("coles.level1.premium.label.xpath");
	public static final By btnAccCover = getObjectIdentifaction("coles.getquote.addaccover.btn.css");
	public static final By btnAddKid = getObjectIdentifaction("coles.getquote.addkid.btn.css");
	public static final By selNoofKids = getObjectIdentifaction("coles.getquote.noofkids.dropdown.css");
	public static final By linkLevel = getObjectIdentifaction("coles.getquote.selectlevel.link.xpath");
		
}
