package au.com.metlife.eapply.configuration;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CORSFilter implements Filter {
	private static final Logger log = LoggerFactory.getLogger(CORSFilter.class);
	
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		/*response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type");*/
		response.addHeader("X-FRAME_OPTIONS", "SAMEORIGIN");
		/*response.setHeader("SET-COOKIE", "JSESSIONID=" + request.getSession().getId() +"; remember-me");*/
		
		response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me, Authorization");

		chain.doFilter(req, res);		
	}
	/*Intialization for filter*/
	public void init(FilterConfig filterConfig) {}
    /*Destroy*/
	public void destroy() {}

}