package supportlibraries;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PageObjectLoader {
	private static Properties prop = new Properties();
	private static String DEFAULT_PATH = "src/Page_Objects.properties";

	public static void loadProperties() {
		loadProperties(DEFAULT_PATH);
	}

	public static void loadProperties(String propertyFilePath) {
		InputStream input = null;
		try {
			//System.out.println("Loading Page Object Properties from:"+ propertyFilePath);
			if (propertyFilePath != null
					&& propertyFilePath.trim().length() > 0) {
				input = new FileInputStream(propertyFilePath);
			} else {
				input = new FileInputStream(DEFAULT_PATH);
			}
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String getProperty(String key) {
		return prop.getProperty(key);
	}
}
