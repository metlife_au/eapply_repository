package au.com.metlife.eapply.bs.utility;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.mail.SendFailedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.email.MetLifeEmailService;
import au.com.metlife.eapply.bs.email.MetLifeEmailVO;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Auradetail;
import au.com.metlife.eapply.bs.model.Contactinfo;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverJSON;
import au.com.metlife.eapply.bs.model.DeathAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.DocumentJSON;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.model.IpAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.TpdAddnlCoversJSON;
import au.com.metlife.eapply.business.error.EapplyServiceError;
import au.com.metlife.eapply.common.EmptyErrorElement;
import au.com.metlife.eapply.common.ResponseLink;
import au.com.metlife.eapply.common.ResponseMetaData;
import au.com.metlife.eapply.constants.EtoolKitServiceConstants;
import au.com.metlife.eapply.corporate.business.CorpResponse;
import au.com.metlife.eapply.corporate.business.ErrorResponse;
import au.com.metlife.eapply.quote.utility.QuoteConstants;


public class EapplyHelper{
	private EapplyHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(EapplyHelper.class);
	
   /* Package name of this class*/
	public static final String PACKAGE_NAME = "com.metlife.eapply.services";	/*$NON-NLS-1$*/
	/*Name given to this class.*/
	public static final String CLASS_NAME = "EapplyHelper";	/*$NON-NLS-1$*/
	
	/*private static final String APPLICANT_TYPE_CHILD = "child";*/
	
	public static final String APPLICATION_STATUS_SUBMITTED = "Submitted";
	public static final String APPLICATION_STATUS_PENDING = "Pending";
	
	/* private static final String APPLICANT_TYPE_PRIMARY = "primary";*/
	public static final int ZERO_CONST = 0;
     
    public static String getDateExtension() {
    	log.info("Get date extension start");

        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "hhmmddMMyyyy");
        String theDate = formatter.format( currDate);
        log.info("Get date extension finish");
        return theDate;
    }
    
    public static String getTodaysDate() {
    	log.info("Get today date start");
        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( MetlifeInstitutionalConstants.DATE_FORMAT);
        String theDate = formatter.format( currDate);
        log.info("Get today date finish");
        return theDate;
    }
    
    public static String getDateInString(Date dte) {
        log.info("Get date in string start");
        java.util.Date currDate = dte;
        SimpleDateFormat formatter = new SimpleDateFormat( MetlifeInstitutionalConstants.DATE_FORMAT);
        String theDate = formatter.format( currDate);
        log.info("Get date in string finish");
        return theDate;
    }
    
    public static String getDateInHrsString(Date dte) {
        log.info("Get date in hours string start");
        java.util.Date currDate = dte;
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy HH:mm");
        String theDate = formatter.format( currDate);
        log.info("Get date in hours string finish");
        return theDate;
    }
    
  /*  public static String addLoading(Eapplication applicantDTO) {

        Double totalPremium = 0.0;
        BigDecimal loading = BigDecimal.valueOf( 0.0);
        if (null != applicantDTO.get
                && applicantDTO.getLoading().trim().length() > 0) {
            loading = BigDecimal.valueOf( Double.valueOf( applicantDTO.getLoading()));
        }
        BigDecimal premium = BigDecimal.valueOf( 0.0);
        if (null != applicantDTO.getPremium()) {
            premium = BigDecimal.valueOf( applicantDTO.getPremium());
        }
        
        loading = loading.multiply( premium);
        loading = loading.divide( BigDecimal.valueOf( 100));
        premium = loading.add( premium);
        double p = Math.pow( 10, (double) 2);
        totalPremium = Math.round( Double.valueOf( premium.toString()) * p) / p;
        loading = null;
        premium = null;
        return totalPremium.toString();
    }*/
    
    public static String strTotalPremium(double totalPremium) {
        log.info("String total premium start");
        String str = String.valueOf( totalPremium);
        if (str.substring( str.lastIndexOf( '.') + 1, str.length()).length() == 0) {
            str = str + "00";
        } else if (str.substring( str.lastIndexOf( '.') + 1, str.length()).length() == 1) {
            str = str + "0";
        }
        log.info("String total premium finish");
        return str;
    }
    
    public static double round(double num, int places) {
         log.info("Round start");
        double p = Math.pow( 10, (double) places);
        log.info("Round finish");
        return Math.round( num * p) / p;
    }    
    public static String getLoadingAmount(Double premiumAmount,
            String loadingFactor) {
    	log.info("Get loading amount start");
    	Double premAmount = premiumAmount;
        if (null != premAmount && null != loadingFactor) {
            try {
            	premAmount = premAmount
                        - premAmount
                        / ( 1 + ( new BigDecimal( loadingFactor).doubleValue()) / 100);
            } catch (Exception e) {
            	premAmount = 0.0;
            }
        } else {
        	premAmount = 0.0;
        }
        
        if (premAmount <= 0.0) {
            return null;
        }
        log.info("Get loading amount finish");
        return strTotalPremium( round( premAmount, 2));
    }    
       
    /**
     * Formats amount to $#,###,###.00 e.g. $1,000.00
     * 
     * @param amount
     * @return
     */
    public static String formatAmount(String amount) {
        log.info("Format amount start");
        String amt = amount;
        String formattedStr = null;
        NumberFormat formatter = null;
        try {
            formatter = new DecimalFormat( "$#,###,###.00");
            if (amt != null && !amt.trim().equalsIgnoreCase( "")
                    ) {
                if (amt.contains( "$")) {
                	amt = amt.replace( "$", "").trim();
                }
                if (amt.contains( ",")) {
                	amt = amt.replace( ",", "").trim();
                }
                formattedStr = formatter.format( new BigDecimal( amt).doubleValue());
                if (formattedStr != null
                        && ( formattedStr.substring( 1, ( formattedStr.indexOf( '.')))).equals( "")) {
                    formattedStr = "$0."
                            + formattedStr.substring( ( formattedStr.indexOf( '.')) + 1);
                }
            }
        } catch (Exception e) {
        	log.error("Error in format amount: {}",e);
        }
        log.info("Format amount finish");
        return formattedStr;
    }
    
    public static String formatCoverAmount(String amount) {
    	log.info("Format cover amount start");
    	String amt = amount;
        String formattedStr = null;
        NumberFormat formatter = null;
        
        try {
            formatter = new DecimalFormat( "$#,###,###.00");
            
            if (amt != null && !amt.trim().equalsIgnoreCase( "")) {
                
                if (amt.contains( "$")) {
                	amt = amt.replace( "$", "").trim();
                }
                if (amt.contains( ",")) {
                	amt = amt.replace( ",", "").trim();
                }
                
                double coverAmount = new BigDecimal( amt).doubleValue();
                
                if (coverAmount > 0 || coverAmount < 0) 
                {
                    formattedStr = formatter.format( coverAmount);
                    
                    if (formattedStr != null
                            && ( formattedStr.substring( 1, ( formattedStr.indexOf( '.')))).equals( "")) {
                        formattedStr = "$0."
                                + formattedStr.substring( ( formattedStr.indexOf( '.')) + 1);
                    }
                }
                else
                {
                	return "$0";
                }
            }
        } catch (Exception e) {
        	log.error("Error in cover amount: {}",e);
        }
        log.info("Format cover amount finish");
        return formattedStr;
    }
    
    public static String getStrFromBoolean(boolean booleanVal) {
        log.info("Get str from boolean start");
        String retVal = null;
        if (booleanVal) {
            retVal = "Yes";
        } else {
            retVal = "No";
        }
        log.info("Get str from boolean finish");
        return retVal;
    }   
     
    public static Boolean checkQuestionsForHide(String question,
            List questionHideVec) {
    	log.info("Check questions for hide start");
    	String questionText = null;
        Boolean isQuestionToHide = Boolean.FALSE;
        if (null != questionHideVec) {
            for (int itr = 0; itr < questionHideVec.size(); itr++) {
                questionText = (String) questionHideVec.get( itr);
                if (question.contains( questionText)) {
                    isQuestionToHide = Boolean.TRUE;
                }   

            }
        }
        log.info("Check questions for hide finish");
        return isQuestionToHide;
    }    
    public static String replaceDollar(String coverAmount) {
    	log.info("Replace dollar start");

        String amount = "";
        if (null != coverAmount && coverAmount.contains( "$")) {
            amount = coverAmount.replace( "$", "");
        }
        log.info("Replace dollar finish");
        return amount.trim();
    } 
   
    /**
     * Method to check if a String is <i>not null<i> and <i>not ""<i>
     * 
     * @param str
     * @return boolean
     */
    public static boolean isStringNotNullorEmpty(String str) {
    	log.info("string not null check start");
        if (str != null && !"".equalsIgnoreCase( str)) {
            return true;
        }
        log.info("string not null check finish");
        return false;
    }   
    
    
    
    public static Boolean isNoChange(Eapplication applicationDTO){
    	log.info("Is no change start");
    	Applicant applicantDTO = null;
    	Cover coversDTO = null;
    	Boolean noChangeCover = Boolean.FALSE;
    	StringBuilder strBuffer = new StringBuilder();
    	/*if(null!=applicationDTO && null!=applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0){*/
    	if(null!=applicationDTO && null!=applicationDTO.getApplicant() && !(applicationDTO.getApplicant().isEmpty())){
    		for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
    			applicantDTO = (Applicant) iter.next();
    			/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
    			if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
    				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
    						.hasNext();) {
    					coversDTO = (Cover) iterator.next();	
    					if(null!=coversDTO){						
    						if(null!=coversDTO.getCoveroption() && "DECREASE".equalsIgnoreCase(coversDTO.getCoveroption())){
    							strBuffer.append(coversDTO.getCoveroption());
    						}
    						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.CANCEL)){
    							strBuffer.append(MetlifeInstitutionalConstants.CANCEL);							
    						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE) 
    								){
    							strBuffer.append(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE);  							
    							
    						}
    					}
    						
    					
    				}
    			}
    		}
    		if(!strBuffer.toString().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE)){
        		noChangeCover = Boolean.TRUE;
        	}
    	}
    	log.info("Is no change finish");
    	return noChangeCover;
    }
    
public static Boolean isWaitBenIPCoverChange(Cover coversDTO){
		log.info("Is waiting benefit IP cover change start");
		Boolean ipFlagforWaitBenPer = Boolean.FALSE;
		Boolean ipFlagforBenfitBenPer = Boolean.FALSE;
		int exBenPeriod = 0;
		int addBenPeriod = 0;
		int exWaitPeriod = 0;
		int addWaitPeriod = 0;
		
		if(null!= coversDTO.getExistingipbenefitperiod() && !coversDTO.getExistingipbenefitperiod().equalsIgnoreCase(coversDTO.getAdditionalipbenefitperiod())){
			
			if(coversDTO.getExistingipbenefitperiod().contains("Years")){
				exBenPeriod = Integer.parseInt(coversDTO.getExistingipbenefitperiod().substring(0,1).trim());
			}else if(coversDTO.getExistingipbenefitperiod().contains("Age")){
				exBenPeriod = Integer.parseInt(coversDTO.getExistingipbenefitperiod().substring(4, 6).trim());
			}
			if(coversDTO.getAdditionalipbenefitperiod().contains("Years")){
				addBenPeriod = Integer.parseInt(coversDTO.getAdditionalipbenefitperiod().substring(0,1).trim());
			}else if(coversDTO.getAdditionalipbenefitperiod().contains("Age")){
				addBenPeriod = Integer.parseInt(coversDTO.getAdditionalipbenefitperiod().substring(4, 6).trim());
			}
			if(addBenPeriod>exBenPeriod){
				ipFlagforBenfitBenPer = Boolean.TRUE;
			}
		}
		
		if(null!=coversDTO.getExistingipwaitingperiod() && !coversDTO.getExistingipwaitingperiod().equalsIgnoreCase(coversDTO.getAdditionalipwaitingperiod())){
			
			exWaitPeriod = Integer.parseInt(coversDTO.getExistingipwaitingperiod().substring(0, 2).trim());
			addWaitPeriod = Integer.parseInt(coversDTO.getAdditionalipwaitingperiod().substring(0, 2).trim());
			if(addWaitPeriod<exWaitPeriod){
				ipFlagforWaitBenPer = Boolean.TRUE;
			}
		}
		
		if(ipFlagforBenfitBenPer || ipFlagforWaitBenPer){
			log.info("Is waiting benefit IP cover change finish");
			return Boolean.TRUE;
			
		}else{
			log.info("Is waiting benefit IP cover change finish");
			return Boolean.FALSE;
		}
		
	}
    

    public static Date formatDateToString(String dateStr) {
		log.info("Format date to string start");
		Date dte = null;
		try {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		if(null!=dateStr){			
			  java.util.Date parsedDate;
		      parsedDate = dateFormat.parse(dateStr);
			  java.sql.Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			  dte = new Date(timestamp.getTime());
		}	
		} catch (ParseException e) {
			log.error("Error in parse date {}",e);
		}
		log.info("Format date to string finish");
		return dte;		
	}
    
    public static void sendMailForFailedMember(Eapplication applicationDTO,au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject pdfObject,String channel, String memberAction) throws IOException{
    	log.info("Send mail for failed member start");
		MetLifeEmailVO eVO = new MetLifeEmailVO(); 
		 ArrayList mailList = new ArrayList();
		 HashMap placeHolders = new HashMap(); 
		StringBuilder applicantBulder = null;
		String brokerEmailId = null;
		String applicantName = null;
		/*String inputRetrieveString = null;*/
		/*String key = null;*/
		/*String emailSubject = null;*/
		try{
			 if(null!=pdfObject)
		        {
			 if (null != pdfObject.getMemberDetails()) {
		            applicantBulder = new StringBuilder( "");
		            
		                        
		            brokerEmailId = pdfObject.getMemberDetails().getEmailId();
		            if (pdfObject.getMemberDetails().getTitle() != null) {
		            	  eVO.setTitle( pdfObject.getMemberDetails().getTitle());
		                applicantBulder.append( pdfObject.getMemberDetails().getTitle()
		                        + " ");
		            }else{
		            	eVO.setTitle("");
		            }
		            if (pdfObject.getMemberDetails().getMemberFName() != null) {
		            	 eVO.setFirstName( pdfObject.getMemberDetails().getMemberFName() );
		                applicantBulder.append( pdfObject.getMemberDetails().getMemberFName()
		                        + " ");
		            }else{
		            	eVO.setFirstName("");
		            }
		            if (pdfObject.getMemberDetails().getMemberLName() != null) {
		            	 eVO.setLastName( pdfObject.getMemberDetails().getMemberLName());
		                applicantBulder.append( pdfObject.getMemberDetails().getMemberLName());
		            }else{
		            	 eVO.setLastName( "");
		            }
		        }
		        if (null != applicantBulder) {
		            applicantName = applicantBulder.toString();
		        }
		        eVO.setApplicationId( pdfObject.getApplicationNumber());          
		       /* eVO.setFromSender(ConfigurationHelper.getConfigurationValue(applicationDTO.getFundinfo().getFundId(), "SENDER_EMAIL"));      */
		        eVO.setApplicationtype("Institutional"); 		      
		        placeHolders.put( "EMAIL_SMTP_HOST", "smtp.gmail.com");
		        placeHolders.put("EMAIL_SMTP_PORT", 25);		       
		        placeHolders.put( "APPLICATIONNUMBER", applicationDTO.getApplicationumber());
		        placeHolders.put( "TITLE", eVO.getTitle());
		        placeHolders.put( "FIRSTNAME", eVO.getFirstName());
		        placeHolders.put("LASTTNAME", eVO.getLastName());      
		        placeHolders.put( "EMAIL_IND_SUBJECT_NAME", applicantName);
		        placeHolders.put( "EMAIL_SUBJECT", "New Online Application received from - <%TITLE%> <%FIRSTNAME%> <%LASTNAME%>");
		        placeHolders.put( "ACTION", channel);        
		        
		        
		        if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
		        	placeHolders.put( MetlifeInstitutionalConstants.APPDECISION, "decline");
		        }else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
		        	placeHolders.put( MetlifeInstitutionalConstants.APPDECISION, "referred");
		        }else{
		        	placeHolders.put( MetlifeInstitutionalConstants.APPDECISION, "accepted");
		        }		        
		      
		        /*if(!"eApply".equalsIgnoreCase(channel) && null!=memberAction && "Accept".equalsIgnoreCase(memberAction)){
		        	
		        }else if(!"eApply".equalsIgnoreCase(channel) && null!=memberAction && "Decline".equalsIgnoreCase(memberAction)){
		        	
		        }else{
		        	
		        }		*/          
		        eVO.setEmailDataElementMap( placeHolders);        
		        mailList.add(brokerEmailId);
		        eVO.setToRecipents( mailList); 
		        
		        MetLifeEmailService.getInstance().sendMail( eVO, pdfObject.getPdfAttachDir());		        
		       
		}  
		}catch(SendFailedException emailException){
			log.error("Error in send mail fail : {}",emailException.getMessage());
       } 
       catch (Exception e) {
    	   log.error("Error in mail: {}",e);
       }
		log.info("Send mail for failed member finish");
	}
    
	public static String getDCLReasons(Eapplication applicationDTO, List<Cover> coverList,String decision,boolean isDCLreasonFlag){
		log.info("Get DCL reasons start");
		Cover coversDTO = null;
		String productName = null;
		String temp = null;
		StringBuilder productNameBuffer = new StringBuilder();
		  for (int covItr = 0; covItr < coverList.size(); covItr++) {
          	coversDTO =  coverList.get( covItr);            
          	if(null!=coversDTO && decision.equalsIgnoreCase(coversDTO.getCoverdecision()) && !isDCLreasonFlag){
          		productNameBuffer.append(coversDTO.getCovertype());
          		productNameBuffer.append( ",");
          	/*}else if(null!=coversDTO && null!=coversDTO.getCoverAuraDclReason() && !productNameBuffer.toString().contains(coversDTO.getCoverAuraDclReason())*/
        	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.DEATH_COVER);
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT_COVER);
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
          				productNameBuffer.append(MetlifeInstitutionalConstants.SALARYCONTINUANCE);
          			}else{
          				productNameBuffer.append(MetlifeInstitutionalConstants.INCOME_PROTECTION_COVER);
          			}
          		}
          		/*if(null!=coversDTO.getCoverAuraDclReason() && coversDTO.getCoverAuraDclReason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverAuraDclReason().substring(0, coversDTO.getCoverAuraDclReason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverAuraDclReason());
          		}*/ 
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf(',')));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}
          		productNameBuffer.append( "\n");
          	}
         // Commented as per Sonar fix Start(This branch can not be reached because the condition duplicates a previous condition in the same sequence of "if/else if" statements
          	/*else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.DEATH_COVER);
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT_COVER);
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
          				productNameBuffer.append(MetlifeInstitutionalConstants.SALARYCONTINUANCE);
          			}else{
          				productNameBuffer.append(MetlifeInstitutionalConstants.INCOME_PROTECTION_COVER);
          			}
          		}
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(',')));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}          		
          		productNameBuffer.append( "\n");
          		
         	}else if(null!=coversDTO && null!=coversDTO.getCoverOrgDclReason() && !productNameBuffer.toString().contains(coversDTO.getCoverOrgDclReason())
        	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.DEATH_COVER);
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT_COVER);
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
          				productNameBuffer.append(MetlifeInstitutionalConstants.SALARYCONTINUANCE);
          			}else{
          				productNameBuffer.append(MetlifeInstitutionalConstants.INCOME_PROTECTION_COVER);
          			}
          		}
          		if(null!=coversDTO.getCoverOrgDclReason() && coversDTO.getCoverOrgDclReason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverOrgDclReason().substring(0, coversDTO.getCoverOrgDclReason().toString().lastIndexOf(",")));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverOrgDclReason());
          		} 
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(',')));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}
          		productNameBuffer.append( "\n");
          	}*/
		  }
		  if(productNameBuffer.toString().contains(",") && !isDCLreasonFlag){			 
			  productName =productNameBuffer.substring(0, productNameBuffer.toString().lastIndexOf(',')) ;			  
			  if(productName.contains(",")){
				  temp = productName.substring(0, productName.lastIndexOf(','));				  
				  productName = temp +" and "+productName.substring(productName.lastIndexOf(',')+1) +" ";			 
			  
			  }
		  }else{
			  productName =productNameBuffer.toString();
		  }
		  log.info("Get DCL reasons finish");
		  return productName;
	}
    
    
    /*public static boolean determineSavedAppliactionValidatity(Eapplication applicationDTO,String userName, String authtoken,String fundId){
		Date todayDate = new Date();
		Date dob = null;
		Date lastSavedDate = null;	
		int ageAtSaved = 0;
		int currentAge = 0;
		SimpleDateFormat dateformat = null;
		boolean birthDayPassed = false,thrityDaysPassed=false,auraVersionChanged=false,applicationExpired=false;
		long diff =0;
		long diffDays = 0;
    	try{
			if(applicationDTO!=null){
				dateformat = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
				if(applicationDTO.getApplicant()!=null){
					dob = applicationDTO.getApplicant().get(0).getBirthdate();	
					lastSavedDate = applicationDTO.getLastupdatedate();
			    	ageAtSaved = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(lastSavedDate));
			    	currentAge = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(todayDate));
					if(ageAtSaved == currentAge){
						birthDayPassed = false;
					}else{
						birthDayPassed = true;
					}
					
					30 Days Validation Business Rule - secound Rule
					todayDate = dateformat.parse(dateformat.format(todayDate));
					lastSavedDate = dateformat.parse(dateformat.format(lastSavedDate));
					diff = todayDate.getTime() - lastSavedDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);
					if(diffDays > 30){
						thrityDaysPassed = Boolean.TRUE;
					}else{
						thrityDaysPassed = Boolean.FALSE;
					}
				}			
				Third Rule for Aura Verison Check
				if((applicationDTO.getIsNUWFlow()==null ||!applicationDTO.getIsNUWFlow()) && !"coverDtls".equalsIgnoreCase(applicationDTO.getLastSavedOn())){
					String  latestAuraVersion = null;
					LookupClientAgent lCA = (LookupClientAgent) ClientAgentHelper.getClientAgent(LookupClientAgent.CLASS_NAME); 
					List  auraVersionList = lCA.getLookupData(userName, authtoken,EapplicationConstant.AURAVERSION,EapplicationConstant.PARTNERNAME,fundId,Boolean.TRUE,null);
					for(int itr=0; itr<auraVersionList.size(); itr++){
		                latestAuraVersion = ((LookupData)auraVersionList.get(itr)).getCode();
					}
					if(null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails() 
							&& null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion() ){
						if(LogHelper.isDebugLogOn()){
							LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, EapplicationConstant.GETAURAVERSIONOUTSIDE+((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion());
						}
		                if(!latestAuraVersion.equalsIgnoreCase(((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion())){
		                	if(LogHelper.isDebugLogOn()){
		        			LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, EapplicationConstant.GETAURAVERSION+((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion());
		                	}
		                	auraVersionChanged = true;
		                }
					}
				}else{
					auraVersionChanged = false;
				}
                if(birthDayPassed || thrityDaysPassed || auraVersionChanged){
                	applicationExpired=true;
                }else{
                	applicationExpired=false;
                }
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
    	return applicationExpired;
    }*/
    
	public static int getAgeNextBirthDay(String dob, String dateCompare){
		log.info("Get age next birthday start");
		int age = 0;
		Calendar birth = null;
		Calendar today = null;
		Date birthDate = null;
		Date currentDate = null;
		try {
			birth = new GregorianCalendar();
			today = new GregorianCalendar();
			birthDate = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT).parse(dob);
			currentDate = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT).parse(dateCompare);
			if (birthDate != null
					&& currentDate != null) {
				birth.setTime(birthDate);
				today.setTime(currentDate);
				age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
				if (today.get(Calendar.DAY_OF_MONTH) == birth.get(Calendar.DAY_OF_MONTH)
						&& today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)) {
					age = age+1;

				} else if (today.get(Calendar.MONTH) < birth.get(Calendar.MONTH)) {
					/*age = age;*/
					
				} else if (today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)
						&& today.get(Calendar.DAY_OF_MONTH) < birth.get(Calendar.DAY_OF_MONTH)) {

				} else {
					age = age + 1;
				}
			}
		} catch (ParseException e) {
			log.error("Error in get age next birthday: {}",e);
		} 
		log.info("Get age next birthday finish");
		return age;
	}
	
	public static String getDCLProductName(Eapplication applicationDTO,List<Cover> coverList,String decision,boolean isDCLreasonFlag){
		log.info("Get DCL product name start");
		Cover coversDTO = null;
		String productName = null;
		String temp = null;
		StringBuilder productNameBuffer = new StringBuilder();
		  for (int covItr = 0; covItr < coverList.size(); covItr++) {
          	coversDTO = coverList.get( covItr);            
          	if(null!=coversDTO && decision.equalsIgnoreCase(coversDTO.getCoverdecision()) && !isDCLreasonFlag){
          		productNameBuffer.append(coversDTO.getCovertype());
          		productNameBuffer.append( ",");
          	}else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.DEATH_COVER);
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT_COVER);
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
          			if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode()) || "MTAA".equalsIgnoreCase(applicationDTO.getPartnercode())){
          				productNameBuffer.append("<br>Salary Continuance ");
          			}else{
          				productNameBuffer.append("<br>Income Protection cover - ");
          			}
          		}
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf(',')));
          		}else{
          			productNameBuffer.append("<ul><li>"+coversDTO.getCoverreason()+"</ul></li>");
          		}          		
          		productNameBuffer.append( "\n\n");
          		
          	}
         // Commented as per Sonar fix Start(This branch can not be reached because the condition duplicates a previous condition in the same sequence of "if/else if" statements
          	/*else if(null!=coversDTO && null!=coversDTO.getCoverreason() && !productNameBuffer.toString().contains(coversDTO.getCoverreason())
          			&& isDCLreasonFlag){
          		productNameBuffer.append( "\t");
          		if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.DEATH_COVER);
          		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
          			productNameBuffer.append(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT_COVER);
          		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){          			
          			if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
          				productNameBuffer.append(MetlifeInstitutionalConstants.SALARYCONTINUANCE);
          			}else{
          				productNameBuffer.append(MetlifeInstitutionalConstants.INCOME_PROTECTION_COVER);
          			}
          		}
          		if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){		
          			productNameBuffer.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().toString().lastIndexOf(',')));
          		}else{
          			productNameBuffer.append(coversDTO.getCoverreason());
          		}       		
          		productNameBuffer.append( "\n\n");
          	}*/
		  }
		  if(productNameBuffer.toString().contains(",") && !isDCLreasonFlag){			 
			  productName =productNameBuffer.substring(0, productNameBuffer.toString().lastIndexOf(',')) ;			  
			  if(productName.contains(",")){
				  temp = productName.substring(0, productName.lastIndexOf(','));				  
				  productName = temp +" and "+productName.substring(productName.lastIndexOf(',')+1) +" ";			 
			  
			  }
		  }else{
			  productName =productNameBuffer.toString();
		  }
		  log.info("Get DCL product name finish");
		  return productName;
	}
	
	

	
	/**
	 * @param lodgeFundId
	 * @param memberType
	 * @param ruleInfoMap
	 * @return
	 */
	
	
	/**
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str){
		log.info("Is null or empty check start");
		if(str==null){
			return true;
		}else if(str.trim().equalsIgnoreCase("")){
			log.info("Is null or empty check finish");
			return true;
		}else{
			log.info("Is null or empty check finish");
			return false;
		}
		
	}
	
	public static String formatIntAmount(String amount){
		log.info("Format int amount start");
		
		String formattedStr=null;
		NumberFormat formatter=null;
		try{
			formatter = new DecimalFormat("$#,###,###");
			if(amount!=null && !amount.trim().equalsIgnoreCase("")){
				formattedStr=formatter.format(new BigDecimal(amount).intValue());
				
			}
		}catch(Exception e){
			log.error("Error in format int amount: {}",e);
		}
		log.info("Format int amount finish");
		return formattedStr;
	}
	
	  public static boolean determineSavedAppliactionValidatity(Eapplication eapplication,String fundId){
		  log.info("Determine saved application validatity start");
			Date todayDate = new Date();
			Date dob = null;
			Date lastSavedDate = null;	
			int ageAtSaved = 0;
			int currentAge = 0;
			SimpleDateFormat dateformat = null;
			boolean birthDayPassed = false;
			boolean thrityDaysPassed=false;
			boolean applicationExpired=false;
			long diff =0;
			long diffDays = 0;
	    	try{
				if(eapplication!=null){
					dateformat = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
					if(eapplication.getApplicant()!=null){
						dob = eapplication.getApplicant().get(0).getBirthdate();	
						lastSavedDate = eapplication.getLastupdatedate();
				    	ageAtSaved = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(lastSavedDate));
				    	currentAge = getAgeNextBirthDay(dateformat.format(dob), dateformat.format(todayDate));
						if(ageAtSaved == currentAge){
							birthDayPassed = false;
						}else{
							birthDayPassed = true;
						}
						
						/*30 Days Validation Business Rule - secound Rule*/
						todayDate = dateformat.parse(dateformat.format(todayDate));
						lastSavedDate = dateformat.parse(dateformat.format(lastSavedDate));
						diff = todayDate.getTime() - lastSavedDate.getTime();
						diffDays = diff / (24 * 60 * 60 * 1000);
						if(diffDays > 30){
							thrityDaysPassed = Boolean.TRUE;
						}else{
							thrityDaysPassed = Boolean.FALSE;
						}
					}
					

/*					Third Rule for Aura Verison Check
					if((applicationDTO.getIsNUWFlow()==null ||!applicationDTO.getIsNUWFlow()) && !"coverDtls".equalsIgnoreCase(applicationDTO.getLastSavedOn())){
						String  latestAuraVersion = null;
						LookupClientAgent lCA = (LookupClientAgent) ClientAgentHelper.getClientAgent(LookupClientAgent.CLASS_NAME); 
						List  auraVersionList = lCA.getLookupData(userName, authtoken,EapplicationConstant.AURAVERSION,EapplicationConstant.PARTNERNAME,fundId,Boolean.TRUE,null);
     					for(int itr=0; itr<auraVersionList.size(); itr++){
			                latestAuraVersion = ((LookupData)auraVersionList.get(itr)).getCode();
						}
						if(null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails() 
								&& null!=((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion() ){
			                if(!latestAuraVersion.equalsIgnoreCase(((ApplicantDTO)applicationDTO.getApplicant().get(0)).getAuradetails().getAuraVersion())){
			                	auraVersionChanged = true;
			                }
						}
					}else{
						auraVersionChanged = false;
					}
*/					
	                if(birthDayPassed || thrityDaysPassed){
	                	applicationExpired=true;
	                }else{
	                	applicationExpired=false;
	                }
				}
	    	}catch(Exception e){
	    		log.error("Error in saved application validatity : {}",e);
	    	}
	    	log.info("Determine saved application validatity finish");
	    	return applicationExpired;
	    }
	  
	  public static String formEncryptedFilePath(String pdfPath){
			
			//String pdfPath = "/shared/pdf/CareSuper_MstesttestB_1494223789052.pdf";
			
			String fileName = pdfPath.substring(pdfPath.lastIndexOf('/')+1);
			/*String  tempFileUrl= pdfPath.substring(0,pdfPath.lastIndexOf('/')+1);*/
			Date now = new Date();  
			DateFormat df = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);				
			String secretkey = df.format(now);	
			String encryptedPath = encrypt(secretkey, fileName);
			/*System.out.println("encryptedPath>>"+encryptedPath+"fileName>>"+fileName);*/
			log.info("encryptedPath>> {} and fileName>> {}",encryptedPath,fileName);
			return encryptedPath;
		}
		
		
		public static String formDecryptedFilePath(String fileUrl){
			String fileUrlInner = fileUrl;
			//String fileUrl = "https://www.e2e.eapplication.metlife.com.au/ebusiness/download?file_name=61b7afe2d4c6bde18ef673a800f22e243815257a240864653adc1322d3215b3d527b513f6d1c3a14";
			
			 if(null!=fileUrlInner && fileUrlInner.contains("=")){
				 fileUrlInner = fileUrlInner.substring(fileUrlInner.lastIndexOf('=') +1 ); 
	         }  
	    	   //decryption logic to get file name
	         Date now = new Date();  
				DateFormat df = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);				
				String secretkey = df.format(now);			
				fileUrlInner= 	decrypt(secretkey, fileUrlInner); 	
				/*System.out.println("fileUrl>>"+fileUrl);*/
				log.info("fileUrl>> {}",fileUrlInner);
			return fileUrlInner.trim();
		}
		
		public static String decrypt(String key,String hexaEncStrign){
			byte[] msgBuf=StringConverter.hexToByte(hexaEncStrign);
			byte[] keyByte=key.getBytes();
			BlowfishECB bfe = new BlowfishECB(keyByte, 0, keyByte.length);
			bfe.decrypt(msgBuf, 0, msgBuf, 0, msgBuf.length);
			return new String(msgBuf);
		}

		

		public static String  encrypt(String key,String plainString ){

			int nRest;
			int nMsgSize;
			byte[] testKey;
			byte[] tempBuf;
			byte[] msgBuf;
			BlowfishECB bfe;
			testKey=key.getBytes();
			bfe = new BlowfishECB(testKey, 0, testKey.length);

			tempBuf=plainString.getBytes();
			nMsgSize=tempBuf.length;
			nRest = nMsgSize & 7;

			if (nRest != 0)	{
				msgBuf = new byte[(nMsgSize & (~7))+ 8 ];

				System.arraycopy(tempBuf, 0, msgBuf, 0, nMsgSize);

				for (int count = nMsgSize; count < msgBuf.length; count++){
					msgBuf[count] = ' ';
				}
			}
			else{
				msgBuf = new byte[nMsgSize];
				System.arraycopy(tempBuf, 0, msgBuf, 0, nMsgSize);
			}

			bfe.encrypt(msgBuf, 0, msgBuf, 0, msgBuf.length);
			return BinConverter.bytesToHexStr(msgBuf);
		}
		
		public static void setdefaultDecision(Eapplication applicationDTO, EapplyInput eapplyInput){			
			String deathCoverDecision = null;
			StringBuilder overallDecisionBuffer = new StringBuilder();
			for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
				Applicant applicantDTO = (Applicant) iter.next();
				/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
				if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
					for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
							.hasNext();) {
						Cover coversDTO = (Cover) iterator.next();	
						if(null!=coversDTO && null!=coversDTO.getCoveroption()){
							if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
								if("Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption()) || "No change".equalsIgnoreCase(coversDTO.getCoveroption()) || MetlifeInstitutionalConstants.CANCEL.equalsIgnoreCase(coversDTO.getCoveroption())){
									deathCoverDecision = "ACC";
								}else{
									deathCoverDecision = coversDTO.getCoverdecision();
								}
								
							}
							if("Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption()) || "No change".equalsIgnoreCase(coversDTO.getCoveroption()) || MetlifeInstitutionalConstants.CANCEL.equalsIgnoreCase(coversDTO.getCoveroption())){
								coversDTO.setCoverdecision("ACC");
							}
						}
					}
				}
			}
			//require to handle Same as Death Cover
			for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
				Applicant applicantDTO = (Applicant) iter.next();
				/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
				if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
					for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
							.hasNext();) {
						Cover coversDTO = (Cover) iterator.next();	
						if(null!=coversDTO && null!=coversDTO.getCoveroption()){
							if("TPD".equalsIgnoreCase(coversDTO.getCovercode()) && "Same as Death Cover".equalsIgnoreCase(coversDTO.getCoveroption())){
								coversDTO.setCoverdecision(deathCoverDecision);								
								overallDecisionBuffer.append(deathCoverDecision);																
							}else{
								overallDecisionBuffer.append(coversDTO.getCoverdecision());
							}
							
						}
					}
				}
			}
			if("DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
				  applicationDTO.setAppdecision("DCL");
			}else if("RUW".equalsIgnoreCase(eapplyInput.getOverallDecision())){
				applicationDTO.setAppdecision("RUW");
			}
			else if(null!=overallDecisionBuffer && !"".equalsIgnoreCase(overallDecisionBuffer.toString())){   
				  if(overallDecisionBuffer.toString().contains("RUW")){
					  applicationDTO.setAppdecision("RUW");
	              }else if(!overallDecisionBuffer.toString().contains("ACC") && overallDecisionBuffer.toString().contains("DCL")){
	            	  applicationDTO.setAppdecision("DCL");
	              }else if(overallDecisionBuffer.toString().contains("ACC") && overallDecisionBuffer.toString().contains("DCL")){
	            	  applicationDTO.setAppdecision("ACC");
	              }else{
	            	  applicationDTO.setAppdecision("ACC");
	              }
			  }
		}

		public static ErrorResponse getDataForEapplyErrorResponse(String errorCode, String errorMsg, boolean isCollection) {
			ErrorResponse errResponse = new ErrorResponse();
	
			if (isCollection) {
				EmptyErrorElement[] elements = new EmptyErrorElement[0];
				errResponse.setErrElements(elements);
			} else {
				EmptyErrorElement element = new EmptyErrorElement();
				errResponse.setErrElement(element);
			}
	
			ResponseLink[] links = new ResponseLink[0];
			errResponse.setLinks(links);
	
			ResponseMetaData metadata = new ResponseMetaData();
			errResponse.setMetadata(metadata);
	
			EapplyServiceError[] errors = new EapplyServiceError[1];
			EapplyServiceError error = new EapplyServiceError();
			errors[0] = error;
	
			switch (errorCode) {
			case EtoolKitServiceConstants.ERR_CD_RESOURCE_NOT_FOUND:
				error.setHttpCode(HttpStatus.NOT_FOUND);
				break;
			case EtoolKitServiceConstants.ERR_CD_BAD_INPUT_DATA:
				error.setHttpCode(HttpStatus.BAD_REQUEST);
				break;
			default:
				error.setHttpCode(HttpStatus.INTERNAL_SERVER_ERROR);
				break;
			}
	
			/*if (ERR_SERVER_ERROR.equals(error.getHttpCode())) {*/
			if (EtoolKitServiceConstants.ERR_SERVER_ERROR.equalsIgnoreCase(error.getHttpCode().toString())) {
				error.setDescription(EtoolKitServiceConstants.ERRMSG_SERVER_ERROR);
			} else {
				error.setDescription(errorMsg);
			}
	
			error.setCode(errorCode);
			error.setElement("");
			errResponse.setErrors(errors);
			return errResponse;
		}

		public static ResponseEntity<CorpResponse> getErrorResponse(ErrorResponse response, boolean flag) {
			
			HttpStatus errorCode = response.getErrors()[0].getHttpCode();
			response.getErrors()[0].setHttpCode(null);
			if(EtoolKitServiceConstants.ERR_NOT_FOUND.equals(errorCode.toString())){
				if(flag){
					return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
				}
				else {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			}
			else if(EtoolKitServiceConstants.ERR_BAD_REQUEST.equals(errorCode.toString())){
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			else if(EtoolKitServiceConstants.ERR_SERVER_ERROR.equals(errorCode.toString())){
				return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			else{
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
			
		}
		
		public static void setdefaultDecisionNew(Eapplication applicationDTO, EappInput eappInput){			
			String deathCoverDecision = null;
			StringBuilder overallDecisionBuffer = new StringBuilder();
			for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
				Applicant applicantDTO = (Applicant) iter.next();
				
				if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
					for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator.hasNext();) {
						Cover coversDTO = (Cover) iterator.next();	
						if(null!=coversDTO && null!=coversDTO.getCoveroption()){
							if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
								if("Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption()) || "No change".equalsIgnoreCase(coversDTO.getCoveroption()) || MetlifeInstitutionalConstants.CANCEL.equalsIgnoreCase(coversDTO.getCoveroption())){
									deathCoverDecision = "ACC";
								}else{
									deathCoverDecision = coversDTO.getCoverdecision();
								}
								
							}
							if("Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption()) || "No change".equalsIgnoreCase(coversDTO.getCoveroption()) || MetlifeInstitutionalConstants.CANCEL.equalsIgnoreCase(coversDTO.getCoveroption())){
								coversDTO.setCoverdecision("ACC");
							}
							if("TPD".equalsIgnoreCase(coversDTO.getCovercode()) && "Same as Death Cover".equalsIgnoreCase(coversDTO.getCoveroption())){
								coversDTO.setCoverdecision(deathCoverDecision);								
								overallDecisionBuffer.append(deathCoverDecision);																
							}else{
								overallDecisionBuffer.append(coversDTO.getCoverdecision());
							}
						}
					}
				}
				
			}
			
			
			if("DCL".equalsIgnoreCase(eappInput.getAppDecision())){
				  applicationDTO.setAppdecision("DCL");
			}else if("RUW".equalsIgnoreCase(eappInput.getAppDecision())){
				applicationDTO.setAppdecision("RUW");
			}
			else if(null!=overallDecisionBuffer && !"".equalsIgnoreCase(overallDecisionBuffer.toString())){   
				  if(overallDecisionBuffer.toString().contains("RUW")){
					  applicationDTO.setAppdecision("RUW");
	              }else if(!overallDecisionBuffer.toString().contains("ACC") && overallDecisionBuffer.toString().contains("DCL")){
	            	  applicationDTO.setAppdecision("DCL");
	              }else if(overallDecisionBuffer.toString().contains("ACC") && overallDecisionBuffer.toString().contains("DCL")){
	            	  applicationDTO.setAppdecision("ACC");
	              }else{
	            	  applicationDTO.setAppdecision("ACC");
	              }
			  }
		}
		
		private static List<Cover> createCancelCvCovers(EapplyInput eapplyInput) {
			 log.info("Create Cancel cover start");
				String deathExUnit = "0";
				String tpdExUnit = "0";
				String ipExUnit = "0";
				String ipExistingAmt = null;
				if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
					for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
						CoverJSON coverJSON = (CoverJSON) iterator.next();
						if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
							deathExUnit = coverJSON.getUnits();
						}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
							tpdExUnit = coverJSON.getUnits();
						}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
							ipExUnit = coverJSON.getUnits();
							ipExistingAmt=coverJSON.getAmount();
						}
						
					}
				}
				
				/*Cover object mapping start*/        	
				
				Cover deathCover=new Cover();
				Cover tpdCover=new Cover();
				Cover ipCover=new Cover();
				List<Cover> coverList=new ArrayList<>();
				if(eapplyInput.getDeathAmt()!=null){
					eapplyInput.setExistingDeathAmt(eapplyInput.getDeathAmt());
				}
				deathCover.setCreatedate(new Timestamp(new Date().getTime()));
				deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
				deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
				deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
				//deathCover.setCoverdecision(eapplyInput.getDeathDecision());
				deathCover.setCoverdecision("ACC");
				deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
				deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());
				if(deathExUnit!=null && deathExUnit !=""){
					deathCover.setFixedunit(Integer.valueOf(deathExUnit));
				}
				if(eapplyInput.getDeathLoading()!=null){
				 deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
				}
				
				deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
				deathCover.setCost("0");
				if(eapplyInput.getExistingDeathAmt()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
				}
				
				/*For Host Freq type is weekly,monthly  and yearly where as for caresuper it is weekly */
				
				if(null != eapplyInput.getFreqCostType() && !"".equalsIgnoreCase(eapplyInput.getFreqCostType())){
					deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
				}else{
					deathCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);
				}
				
				
				if(null != eapplyInput.getAddnlDeathCoverDetails() && !"".equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().toString())){
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost() != null){
					deathCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost()));
				}
				
				deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
				deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
				deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
				
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
					deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());			
				}else{			
					deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt())).intValue());
				}
				
				
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
					if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
						deathCover.setAddunitind("1");
					}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
						deathCover.setAddunitind("0");
					}
				}
				deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
				
				}
				
				if(eapplyInput.getTpdAmt()!=null){
					eapplyInput.setExistingTpdAmt(eapplyInput.getTpdAmt());
				}
				tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
				tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
				tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
				tpdCover.setCovercode("TPD");
				//tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
				tpdCover.setCoverdecision("ACC");
				tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
				tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());
				if(tpdExUnit!=null && tpdExUnit!=""){
					tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
				}
				if(eapplyInput.getTpdLoading()!=null)
					tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
				tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
				tpdCover.setCost("0");
				if(eapplyInput.getExistingTpdAmt()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
				}
				
				if(null!=eapplyInput.getFreqCostType() && !"".equalsIgnoreCase(eapplyInput.getFreqCostType())){
					tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
				}else{
					tpdCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);
				}
				if(null!=eapplyInput.getAddnlTpdCoverDetails() && !"".equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().toString())){
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost() != null){
					tpdCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost()));
				}
				
				tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
				tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
				tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
				
				
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
					tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());			
				}else{			
					tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt())).intValue());
				}
				
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
					if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
						tpdCover.setAddunitind("1");
					}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
						tpdCover.setAddunitind("0");
					}
				}
				tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
				}
				
				if(eapplyInput.getIpAmt()!=null){
					eapplyInput.setExistingIPAmount(eapplyInput.getIpAmt());
				}
				
				if(ipExistingAmt!=null){
					ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
					eapplyInput.setExistingIPAmount(ipExistingAmt);
				}		            
				if(ipExUnit!=null && ipExUnit!=""){
					ipCover.setFixedunit(Integer.valueOf(ipExUnit));
				}
				ipCover.setCreatedate(new Timestamp(new Date().getTime()));
				ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
				ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
				ipCover.setCovercode("IP");
				//ipCover.setCoverdecision(eapplyInput.getIpDecision());
				ipCover.setCoverdecision("ACC");
				ipCover.setCoverreason(eapplyInput.getIpAuraResons());
				ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
				if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
					ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
				ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
				ipCover.setCost("0");
				if(null!=eapplyInput.getFreqCostType() && !"".equalsIgnoreCase(eapplyInput.getFreqCostType())){
					ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
				}else{
					ipCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);
				}
				ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
				
				if(null!=eapplyInput.getAddnlIpCoverDetails() && !"".equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().toString())){
				if(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()!= null){
					ipCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()));
				}
				
				
				if(null!=eapplyInput && QuoteConstants.PARTNER_SFPS.equalsIgnoreCase(eapplyInput.getPartnerCode())
						&& null!= eapplyInput.getAddnlIpCoverDetails().getIpCoverType() 
						&& QuoteConstants.IP_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()))
				{
					ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue()).intValue()));
					ipCover.setAddunitind("1");
				}
				
				ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
				ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
				ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
				
				if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
					ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue())).intValue());			
				}else{			
					ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt())).intValue());
				}
				
				
				if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
					if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
						ipCover.setAddunitind("1");
					}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
						ipCover.setAddunitind("0");
					}
				}	
				if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
				    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
				}
				ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
				ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());
				}

				if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
					for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
						CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
						if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
							if(coverJSON.getAmount()!=null){
								deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
							}
							
						}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
							if(coverJSON.getAmount()!=null){
								tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
							}
						}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
								&& coverJSON.getAmount()!=null){
								ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
								ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
								ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
						}
						
					}
				}
				coverList.add(deathCover);
				coverList.add(tpdCover);
				coverList.add(ipCover);
				/*Cover object mapping end*/
				log.info("Create cancel cover finish");
				return coverList;
		}
		
		private static List<Cover> createTransferCvCovers(EapplyInput eapplyInput) {
		    log.info("Create transfer cover start");
			String deathExUnit = "0";
			String tpdExUnit = "0";
			String ipExUnit = "0";
			String ipExistingAmt = null;
			if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
				for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
					CoverJSON coverJSON = (CoverJSON) iterator.next();
					if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
						deathExUnit = coverJSON.getUnits();
					}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
						tpdExUnit = coverJSON.getUnits();
					}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
						ipExUnit = coverJSON.getUnits();
						ipExistingAmt=coverJSON.getAmount();
					}
					
				}
			}
			
			/*Cover object mapping start*/        	
			
			Cover deathCover=new Cover();
			Cover tpdCover=new Cover();
			Cover ipCover=new Cover();
			List<Cover> coverList=new ArrayList<>();
			
			deathCover.setCreatedate(new Timestamp(new Date().getTime()));
			deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
			deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
			deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
			deathCover.setCoverdecision(eapplyInput.getDeathDecision());
			deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
			/*deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());*/

			if(deathExUnit!= null && deathExUnit !=""){
				deathCover.setFixedunit(Integer.valueOf(deathExUnit));
			}

			/*if(eapplyInput.getDeathLoading()!=null)
			deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));*/
			deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
			//Changes for Hostplus Transfer cover unitized
			//deathCover.setCovercategory(MetlifeInstitutionalConstants.DC_FIXED);
			if(null!=eapplyInput && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()))
			{
				deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType());
			}
			else
			{
			deathCover.setCovercategory(MetlifeInstitutionalConstants.DC_FIXED);
			}
			
			/*deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());*/
			deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathTransferWeeklyCost());
			if(eapplyInput.getExistingDeathAmt()!=null){
				deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
			}
			else if(null!= eapplyInput && null!= eapplyInput.getTransferDeathExistingAmt())
			{
				deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getTransferDeathExistingAmt()));
				eapplyInput.setExistingDeathAmt(eapplyInput.getTransferDeathExistingAmt());
			}
			else{						
				eapplyInput.setExistingDeathAmt(""+(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt())).subtract(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt()));			
			}
			deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt()!= null){
				deathCover.setTransfercoveramount(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt());	
			}
			/*log.info("Death transfer amt>> {}",eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt());*/
			/*if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType()!=null){
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
					deathCover.setAddunitind("1");
				}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
					deathCover.setAddunitind("0");
				}
			}*/
			deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt()));
			if(eapplyInput.getAddnlDeathCoverDetails()!=null && eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt()!=null){
				eapplyInput.getAddnlDeathCoverDetails().setDeathFixedAmt(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt());
			}		
			/*log.info("Death trans amt>> {}",eapplyInput.getAddnlDeathCoverDetails().getDeathTransferWeeklyCost());
			log.info("Death Addnl amt>> {}",deathCover.getAdditionalcoveramount());
			log.info("existing death>> {}",eapplyInput.getExistingDeathAmt());*/
			
			
			tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
			tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
			tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
			tpdCover.setCovercode("TPD");
			tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
			tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
		    /*tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());*/

			if(tpdExUnit!= null && tpdExUnit !=""){

				tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
			}
			/*if(eapplyInput.getTpdLoading()!=null)
			tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));*/
			tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
			//Changes for Hostplus Transfer cover unitized
			//tpdCover.setCovercategory(MetlifeInstitutionalConstants.TPDFIXED);
			if(null!=eapplyInput && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()))
			{
				tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType());
			}
			else
			{
			tpdCover.setCovercategory(MetlifeInstitutionalConstants.TPDFIXED);
			}
		    /*tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());*/
			tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdTransferWeeklyCost());
			if(eapplyInput.getExistingTpdAmt()!=null){
				tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
			}
			else if(null!= eapplyInput && null!= eapplyInput.getTransferTpdExistingAmt())
			{
				tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getTransferTpdExistingAmt()));
				eapplyInput.setExistingTpdAmt(eapplyInput.getTransferTpdExistingAmt());
			}
			else{						
				eapplyInput.setExistingTpdAmt(""+(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt())).subtract(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt()));			
			}
			tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt() != null){
				tpdCover.setTransfercoveramount(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt());
			}
			log.info("TPD transfer amt>> {}",eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt());
			/*if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()!=null){
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
					tpdCover.setAddunitind("1");
				}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
					tpdCover.setAddunitind("0");
			}
			}*/
			tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt()));
			if(eapplyInput.getAddnlTpdCoverDetails()!=null && eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt()!=null){
				eapplyInput.getAddnlTpdCoverDetails().setTpdFixedAmt(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt());
			}
			if(ipExistingAmt!=null){
				ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
				eapplyInput.setExistingIPAmount(ipExistingAmt);

			}
			if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && null!= ipExistingAmt)
			{
				eapplyInput.setExistingIPUnits(ipExUnit);
			}
			
			if(ipExUnit!=null && ipExUnit!=""){

				ipCover.setFixedunit(Integer.valueOf(ipExUnit));
			}
			ipCover.setCreatedate(new Timestamp(new Date().getTime()));
			ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
			ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
			ipCover.setCovercode("IP");
			ipCover.setCoverdecision(eapplyInput.getIpDecision());
			ipCover.setCoverreason(eapplyInput.getIpAuraResons());
			/*ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
			if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
			ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));*/
			ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
			ipCover.setCovercategory(MetlifeInstitutionalConstants.IP_FIXED);
			
		    /*ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());*/
			ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpTransferWeeklyCost());
			ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
			if(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt()!= null){
				ipCover.setTransfercoveramount(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt());
			}
			log.info("IP transfer amt>> {}",eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt());
			/*if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
				if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
					ipCover.setAddunitind("1");
				}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
					ipCover.setAddunitind("0");
				}
			}*/
			if(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()!= null){
				ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()));
			}
			if(eapplyInput.getAddnlTpdCoverDetails()!=null && eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()!=null){
				eapplyInput.getAddnlIpCoverDetails().setIpFixedAmt(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt());
			}
			ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getAddnlTransferBenefitPeriod());
			ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getAddnlTransferWaitingPeriod());
			ipCover.setTotalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getTotalipwaitingperiod());
			ipCover.setTotalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getTotalipbenefitperiod());
			
			if("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
			{
				ipCover.setCovercategory(MetlifeInstitutionalConstants.IP_UNITISED);
				ipCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlIpCoverDetails().getIpAddnlUnits()));
			}
			
			//eapplyInput.setExistingIPAmount(""+(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt())).subtract(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt()));			
			
			if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
				for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
					CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
					if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
						if(coverJSON.getAmount()!=null){
							deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						}
						
					}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
						if(coverJSON.getAmount()!=null){
							tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						}
					}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
							&& coverJSON.getAmount()!=null){
							ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
							ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
							ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
					}
					
	      }
	}
			//Added for Units inclusion in transfer cover
	if(((QuoteConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType())) 
			 || (QuoteConstants.TPD_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()))) && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()))
			{
				deathCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferUnits()));
				tpdCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferUnits()));
			}
			
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	log.info("Create transfer cover finish");
	 return coverList;
	}
		
		private static List<Cover> createChangeCvCovers(EapplyInput eapplyInput) {
			   log.info("Create change cover start");
				String deathExUnit = "0";
				String tpdExUnit = "0";
				String ipExUnit = "0";
				String ipExistingAmt = null;
				if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
					for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
						CoverJSON coverJSON = (CoverJSON) iterator.next();
						if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
							deathExUnit = coverJSON.getUnits();
						}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
							tpdExUnit = coverJSON.getUnits();
						}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
							ipExUnit = coverJSON.getUnits();
							ipExistingAmt=coverJSON.getAmount();
						}
						
					}
				}
				
				/*Cover object mapping start*/        	
				
				Cover deathCover=new Cover();
				Cover tpdCover=new Cover();
				Cover ipCover=new Cover();
				List<Cover> coverList=new ArrayList<>();
				
				deathCover.setCreatedate(new Timestamp(new Date().getTime()));
				deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
				deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
				deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
				deathCover.setCoverdecision(eapplyInput.getDeathDecision());
				deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
				deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());

				if(deathExUnit != null && deathExUnit!=""){

					deathCover.setFixedunit(Integer.valueOf(deathExUnit));
				}
				if(eapplyInput.getDeathLoading()!=null){
				 deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
				}
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost() != null){
					deathCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost()));
				}
				deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
				deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
				deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
				deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
					deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());			
				}else{	
					deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt())).intValue());
				}
				if(eapplyInput.getExistingDeathAmt()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
				}
				deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
				if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
					if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
						deathCover.setAddunitind("1");
					}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
						deathCover.setAddunitind("0");
					}
				}
				deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
				
				tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
				tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
				tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
				tpdCover.setCovercode("TPD");
				tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
				tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
				tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());

				if(tpdExUnit!=null && tpdExUnit!=""){

					tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
				}
				if(eapplyInput.getTpdLoading()!=null)
					tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost() != null){
					tpdCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost()));
				}
				tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
				tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
				tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
				tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
				if(eapplyInput.getExistingTpdAmt()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
				}
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
					tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());			
				}else{	
					tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt())).intValue());
					}
				tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
				if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
					if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
						tpdCover.setAddunitind("1");
					}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
						tpdCover.setAddunitind("0");
					}
				}
				tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
				if(ipExistingAmt!=null){
					ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
					eapplyInput.setExistingIPAmount(ipExistingAmt);
				}else if(eapplyInput.getExistingIPAmount()!=null){
					ipCover.setExistingcoveramount(new BigDecimal((eapplyInput.getExistingIPAmount())));
					eapplyInput.setExistingIPAmount(eapplyInput.getExistingIPAmount());
				}
				
				if(ipExUnit!= null && ipExUnit !=""){
					ipCover.setFixedunit(Integer.valueOf(ipExUnit));
				}


				ipCover.setCreatedate(new Timestamp(new Date().getTime()));
				ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
				ipCover.setCovertype("Salary Continuance");
				ipCover.setCovercode("IP");
				ipCover.setCoverdecision(eapplyInput.getIpDecision());
				ipCover.setCoverreason(eapplyInput.getIpAuraResons());
				ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
				if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
					ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
				if(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()!= null){
					ipCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()));
				}
				ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
				ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
				ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
				ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
				ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
				if(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
					ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue())).intValue());			
				}else{
					if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!=null) {
					ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt())).intValue());
					}
					
				}
				ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
				
				if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
					if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
						ipCover.setAddunitind("1");
					}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
						ipCover.setAddunitind("0");
					}
				}	
				if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
				    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
				}
				if(null!= eapplyInput.getAddnlIpCoverDetails().getIpAddnlUnits())
				{
					ipCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlIpCoverDetails().getIpAddnlUnits()));
				}
				ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
				ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());

				if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
					for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
						CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
						if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
							if(coverJSON.getAmount()!=null){
								deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
							}
							
						}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
							if(coverJSON.getAmount()!=null){
								tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
							}
						}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
								&& coverJSON.getAmount()!=null){
								ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
								ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
								ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
						}
						
					}
				}
				if("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()))
				{
					tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
					ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
				}
				if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getExistingIPAmount()!=null
						&& eapplyInput.isConvertCheck())
				{
					eapplyInput.setExistingIPUnits(ipExUnit);
					ipCover.setAdditionalunit(Integer.valueOf(ipExUnit));
				}
				coverList.add(deathCover);
				coverList.add(tpdCover);
				coverList.add(ipCover);
				/*Cover object mapping end*/
				log.info("Create change cover finish");
				return coverList;
		}
		
		public static Eapplication convertToEapplication(EapplyInput eapplyInput,String appStatus) throws ParseException {
			log.info("Convert to eapplication start");
			 Eapplication eapplication =new Eapplication();
			 Applicant applicant1=new Applicant();
			 List<Applicant> applicantList=new ArrayList<>();	
			 Calendar dob;
	 		DateFormat df1;
			 if(eapplyInput!=null){
			 			/*Eapplication object mapping start*/
			 			//eapplication.setCreatedate(new Timestamp(new Date().getTime()));
			 			//eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
			 			eapplication.setApplicationstatus(appStatus);
			 			
			 			//temperory fix
			 			if(eapplyInput.getDeathDecision()==null && !"DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
			 				eapplyInput.setDeathDecision("ACC");
			 			}
						if(eapplyInput.getTpdDecision()==null && !"DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
							eapplyInput.setTpdDecision("ACC");
						}
						if(eapplyInput.getIpDecision()==null && !"DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
							eapplyInput.setIpDecision("ACC");	
						}
						if(eapplyInput.isAckCheck()){
							eapplication.setGenconsentindicator("true");						
						}
						if(eapplyInput.isDodCheck()){
							eapplication.setDisclosureindicator("true");
						}
						if(eapplyInput.isPrivacyCheck()){
							eapplication.setPrivacystatindicator("true");
						}
						if(eapplyInput.isFulCheck()){
							eapplication.setFulstatindicator("true");
						}
						/**Vicsuper changes made by purna - starts**/
						if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())){
							if(eapplyInput.isIpDisclaimer()){
								eapplication.setNuwflowind(MetlifeInstitutionalConstants.Y);
							} else {
								eapplication.setNuwflowind(MetlifeInstitutionalConstants.N);
							}						
							if(eapplyInput.getOwnOccuptionTpd()){
								eapplication.setOccupgrade(MetlifeInstitutionalConstants.Y);
							} else {
								eapplication.setOccupgrade(MetlifeInstitutionalConstants.N);
							}
							if(eapplyInput.getOwnOccuptionIp() != null && eapplyInput.getOwnOccuptionIp().equalsIgnoreCase(MetlifeInstitutionalConstants.TRUE)){
								eapplication.setApporgdecision(MetlifeInstitutionalConstants.TRUE);
							} else {
								eapplication.setApporgdecision(MetlifeInstitutionalConstants.FALSE);
							}
						}
						/**Vicsuper changes made by purna - ends**/
						
			 			if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.CANCOVER) && "CARE".equalsIgnoreCase(eapplyInput.getPartnerCode())){
			 				if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			 					for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			 						CoverJSON coverJSON = (CoverJSON) iterator.next();
			 						if(coverJSON.getBenefitType()!=null){
			 							if("1".equalsIgnoreCase(coverJSON.getBenefitType())){
			 								DeathAddnlCoversJSON deathAddnlCoversJSON = new DeathAddnlCoversJSON();
			 								eapplyInput.setExistingDeathAmt(coverJSON.getAmount());
			 								eapplyInput.setDeathOccCategory(coverJSON.getOccRating());
			 								//eapplyInput.setDeathCoverType(deathCoverType);
			 								if("1".equalsIgnoreCase(coverJSON.getType())){		 									
			 									eapplyInput.setExistingDeathUnits(coverJSON.getUnits());
			 									deathAddnlCoversJSON.setDeathCoverType(MetlifeInstitutionalConstants.TPDUNITISED);
			 									deathAddnlCoversJSON.setDeathInputTextValue("0");
			 								}else{
			 									deathAddnlCoversJSON.setDeathCoverType(MetlifeInstitutionalConstants.DC_FIXED);
			 								}
			 								deathAddnlCoversJSON.setDeathFixedAmt("0");
			 								deathAddnlCoversJSON.setDeathCoverName(MetlifeInstitutionalConstants.CANCEL);
			 								deathAddnlCoversJSON.setDeathCoverPremium("0");
			 								eapplyInput.setAddnlDeathCoverDetails(deathAddnlCoversJSON);	
			 								
			 								
			 							}else if("2".equalsIgnoreCase(coverJSON.getBenefitType())){
			 								TpdAddnlCoversJSON tpdAddnlCoversJSON = new TpdAddnlCoversJSON();
			 								eapplyInput.setExistingTpdAmt(coverJSON.getAmount());
			 								eapplyInput.setTpdOccCategory(coverJSON.getOccRating());
			 								if("1".equalsIgnoreCase(coverJSON.getType())){
			 									eapplyInput.setExistingTPDUnits(coverJSON.getUnits());
			 									tpdAddnlCoversJSON.setTpdCoverType(MetlifeInstitutionalConstants.TPDUNITISED);
			 									tpdAddnlCoversJSON.setTpdInputTextValue("0");
			 								}else{
			 									tpdAddnlCoversJSON.setTpdCoverType(MetlifeInstitutionalConstants.TPDFIXED);
			 									
			 								}
			 								tpdAddnlCoversJSON.setTpdFixedAmt("0");
			 								tpdAddnlCoversJSON.setTpdCoverName(MetlifeInstitutionalConstants.CANCEL);
			 								tpdAddnlCoversJSON.setTpdCoverPremium("0");
			 								eapplyInput.setAddnlTpdCoverDetails(tpdAddnlCoversJSON);
			 							}else if("4".equalsIgnoreCase(coverJSON.getBenefitType())){
			 								IpAddnlCoversJSON ipAddnlCoversJSON = new IpAddnlCoversJSON();
			 								eapplyInput.setExistingIPAmount(coverJSON.getAmount());
			 								eapplyInput.setWaitingPeriod(coverJSON.getWaitingPeriod());
			 								eapplyInput.setBenefitPeriod(coverJSON.getBenefitPeriod());
			 								eapplyInput.setIpOccCategory(coverJSON.getOccRating());
			 								if("1".equalsIgnoreCase(coverJSON.getType())){
			 									eapplyInput.setExistingIPUnits(coverJSON.getUnits());
			 									ipAddnlCoversJSON.setIpCoverType(MetlifeInstitutionalConstants.IP_UNITISED);
			 									ipAddnlCoversJSON.setIpInputTextValue("0");
			 								}else{
			 									ipAddnlCoversJSON.setIpCoverType(MetlifeInstitutionalConstants.IP_FIXED);		 									
			 								}
			 								ipAddnlCoversJSON.setBenefitPeriod(coverJSON.getBenefitPeriod());
			 								ipAddnlCoversJSON.setWaitingPeriod(coverJSON.getWaitingPeriod());		 								
			 								ipAddnlCoversJSON.setIpFixedAmt("0");
			 								ipAddnlCoversJSON.setIpCoverName(MetlifeInstitutionalConstants.CANCEL);
			 								ipAddnlCoversJSON.setIpCoverPremium("0");
			 								eapplyInput.setAddnlIpCoverDetails(ipAddnlCoversJSON);
			 							}
			 						}
			 						
									
								}
			 				}
			 				eapplication.setAppdecision("ACC");
			 				eapplyInput.setOverallDecision("ACC");
			 			}else if(eapplyInput.getOverallDecision()==null){
			 				eapplyInput.setOverallDecision("ACC");
			 				//convert& maintain scenario- by default all cover are acc
			 				eapplication.setAppdecision("ACC");
			 			}else{
			 				eapplication.setAppdecision(eapplyInput.getOverallDecision());
			 			}
			 			//temperory fix end
			 			eapplication.setApplicationtype(MetlifeInstitutionalConstants.INSTITUTIONAL);
			 			eapplication.setLastsavedon(eapplyInput.getLastSavedOn());
			 			if(eapplyInput.getClientMatchReason()!= null && eapplyInput.getClientMatchReason().length()<3000){
			 				eapplication.setClientmatchreason(eapplyInput.getClientMatchReason());
			 			}
			 			eapplication.setClientmatch(eapplyInput.getClientMatched());
			 			/*if(eapplyInput.getClientMatched()){
			 				
			 			}else{
			 				eapplication.setClientmatch("No");
			 			}*/		 			
			 			
			 			if(eapplyInput.getAuraDisabled()!= null && eapplyInput.getAuraDisabled().equalsIgnoreCase(MetlifeInstitutionalConstants.FALSE)){
			 				eapplication.setUnderwritingpolicy("Yes");
			 			}else if(eapplyInput.getResponseObject()!=null || "AuraPage".equalsIgnoreCase(eapplyInput.getLastSavedOn())){
			 				eapplication.setUnderwritingpolicy("Yes");
			 			}else{
			 				eapplication.setUnderwritingpolicy("No");
			 			}
			 			if(null!=eapplyInput && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())
			 					&& eapplyInput.isCcoverIndex())
			 			{
			 				eapplication.setUnderwritingpolicy("No");
			 			}
			 			if(eapplyInput.getAppNum()!=null){
			 				eapplication.setApplicationumber(eapplyInput.getAppNum().toString());
			 			}
			 			if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getProductnumber()!=null) {
			 				eapplication.setProductnumber(eapplyInput.getProductnumber());
			 			}
			 			eapplication.setPartnercode(eapplyInput.getPartnerCode());
			 			eapplication.setRequesttype(eapplyInput.getManageType());
			 			DateFormat dateMonthFormat = new SimpleDateFormat("MMyy");
						String monthDir = dateMonthFormat.format(new java.util.Date());
						if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getCorpFundCode()!=null)
						{
							String campaignCode = eapplyInput.getCorpFundCode() + monthDir +"WG-" + eapplyInput.getAppNum();
							eapplication.setCampaigncode(campaignCode);
						}
						else {
							String campaignCode = eapplication.getPartnercode() + monthDir +"WG-" + eapplyInput.getAppNum();
							eapplication.setCampaigncode(campaignCode);
						}
			 			
			 			if((eapplyInput.getDeathExclusions()!=null && eapplyInput.getDeathExclusions().trim().length()>ZERO_CONST)
			 					|| (eapplyInput.getTpdExclusions()!=null && eapplyInput.getTpdExclusions().trim().length()>ZERO_CONST)
			 					|| (eapplyInput.getIpExclusions()!=null && eapplyInput.getIpExclusions().trim().length()>ZERO_CONST)
			 					){
			 				eapplication.setSaindicator("True");
			 			}else{
			 				eapplication.setSaindicator(MetlifeInstitutionalConstants.FALSE);
			 			}
			 			if((eapplyInput.getDeathLoading()!=null && eapplyInput.getDeathLoading().trim().length()>ZERO_CONST)
			 					|| (eapplyInput.getTpdLoading()!=null && eapplyInput.getTpdLoading().trim().length()>ZERO_CONST)
			 					|| (eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
			 					){
			 				eapplication.setPaindicator("True");
			 			}else{
			 				eapplication.setPaindicator(MetlifeInstitutionalConstants.FALSE);
			 			}
			 			if(eapplyInput.getTotalPremium() != null){
			 				eapplication.setTotalMonthlyPremium(eapplyInput.getTotalPremium());
			 			}
			 			/*Eapplication object mapping end*/
			 			
			 			/*Applicant object mapping start*/
			            if(eapplyInput.getAppNum()!=null){
			            	applicant1.setQuestionnaireid(eapplyInput.getAppNum().toString());
			 			}
			            if(eapplyInput.getPersonalDetails()!=null){
			            	applicant1.setCreatedate(new Timestamp(new Date().getTime()));
				 			applicant1.setLastupdatedate(new Timestamp(new Date().getTime()));
				 			if(eapplyInput.getPersonalDetails().getTitle() != null && eapplyInput.getPersonalDetails().getTitle()!=""){
				 				applicant1.setTitle(eapplyInput.getPersonalDetails().getTitle());
				 			}else if(eapplyInput.getTitle() != null && eapplyInput.getTitle() != ""){
				 				applicant1.setTitle(eapplyInput.getTitle());
				 			}
			            	if(eapplyInput.getPersonalDetails().getFirstName() != null && eapplyInput.getPersonalDetails().getFirstName() !=""){
			            		applicant1.setFirstname(eapplyInput.getPersonalDetails().getFirstName());
			            	}else if(eapplyInput.getFirstName() != null && eapplyInput.getFirstName() != ""){
			            		applicant1.setFirstname(eapplyInput.getFirstName());
			            	}
			            	if(eapplyInput.getPersonalDetails().getLastName()!= null && eapplyInput.getPersonalDetails().getLastName() !=""){
			            		applicant1.setLastname(eapplyInput.getPersonalDetails().getLastName());
			            	}else if(eapplyInput.getLastName() != null && eapplyInput.getLastName() !=""){
			            		applicant1.setLastname(eapplyInput.getLastName());
			            	}
			            	
			            	if(eapplyInput.getPersonalDetails().getDateOfBirth()!=null){
			            		dob = Calendar.getInstance();
			            		df1 = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
			            		dob.setTime(df1.parse(eapplyInput.getPersonalDetails().getDateOfBirth()));
			            		applicant1.setBirthdate(dob.getTime());
			            	}else if(eapplyInput.getDob()!= null && eapplyInput.getDob()!=""){
			            		dob = Calendar.getInstance();
			            		df1 = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
			            		dob.setTime(df1.parse(eapplyInput.getDob()));
			            		applicant1.setBirthdate(dob.getTime());
			            	}
			            	if(eapplyInput.getPersonalDetails().getSmoker()!=null){
		            			applicant1.setSmoker(eapplyInput.getPersonalDetails().getSmoker());
		            			//Added for metflow part
		            			
			            	}else if(eapplyInput.getSmoker()!= null && eapplyInput.getPersonalDetails().getSmoker() !=""){
			            		applicant1.setSmoker(eapplyInput.getSmoker());
			            	}
			            	if(eapplyInput.getPersonalDetails().getGender() != null){
			            		applicant1.setGender(eapplyInput.getPersonalDetails().getGender());
			            	}
			            	/*in case of CANCEL cover */
			            	if(eapplyInput.getGender() != null){  
			            		applicant1.setGender(eapplyInput.getGender());
			            	}
			            }
			            
			            if(eapplyInput.getOccupationDetails() != null){
				        	 applicant1.setWorkhours(eapplyInput.getOccupationDetails().getFifteenHr());
				             /**Vicsuper Code changes starts - Made by Purna**/
				        	 //if (MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) 
				        			// && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				        	 	eapplication.setMagazineoffer(eapplyInput.getOccupationDetails().getOccRating1a());
				        	 	eapplication.setAuthorizationindicator(eapplyInput.getOccupationDetails().getOccRating1b());			        		 
				        	 //} else {
				        		 applicant1.setIndustrytype(eapplyInput.getOccupationDetails().getIndustryCode());
					        	 applicant1.setOccupationcode(eapplyInput.getOccupationDetails().getIndustryName());
				        	// }
				        	 /**Vicsuper Code changes ends - Made by Purna**/
				        	 /* applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());*/
			            	 /*if(eapplyInput.getOccupationDetails().getOccupation().equalsIgnoreCase("Other")){
			            		 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOtherOccupation()); 
			            	 }else{
			            		 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());
			            	 }*/
				        	 
				        	 /*169682: Added code for Other occupation*/
				        	 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());
				        	 if(eapplyInput.getOccupationDetails().getOccupation() !=null && eapplyInput.getOccupationDetails().getOccupation().equalsIgnoreCase("Other")){
			            		 applicant1.setOtheroccupation(eapplyInput.getOccupationDetails().getOtherOccupation()); 
			            	 }
				        	 
			            	 applicant1.setAnnualsalary(eapplyInput.getOccupationDetails().getSalary());
			            	 if(eapplyInput.getOccupationDetails().getCitizenQue() != null){
			            		 applicant1.setAustraliacitizenship(eapplyInput.getOccupationDetails().getCitizenQue()); 
			            	 }
				        	 applicant1.setGender(eapplyInput.getOccupationDetails().getGender());
				        	 
				        	 // own business questions for Change and Cancel cover
				        	 if(eapplyInput.getOccupationDetails().getOwnBussinessQues() != null){
				        		 applicant1.setOwnbusinessque(eapplyInput.getOccupationDetails().getOwnBussinessQues()); 
				        	 }
				        	 if(eapplyInput.getOccupationDetails().getOwnBussinessYesQues() != null){
				        		 applicant1.setOwnbusinessyesque(eapplyInput.getOccupationDetails().getOwnBussinessYesQues());
				        	 }
				        	 if(eapplyInput.getOccupationDetails().getOwnBussinessNoQues() != null){
				        		 applicant1.setOwnbusinesnosque(eapplyInput.getOccupationDetails().getOwnBussinessNoQues());
				        	 }
				        	 // for Statewide
				        	 if(eapplyInput.getOccupationDetails().getSmokerQuestion() != null){
				        		 applicant1.setSmoker(eapplyInput.getOccupationDetails().getSmokerQuestion());
				        	 }
				        	 if(eapplyInput.getOccupationDetails().getPermanentlyEmployedQuestion() != null){
				        		 applicant1.setPermanentemplque(eapplyInput.getOccupationDetails().getPermanentlyEmployedQuestion());
				        	 }	
				        	 if(eapplyInput.getOccupationDetails().getHoursPerWeekQuestion() != null){
				        		 applicant1.setThirtyfivehrswrkque(eapplyInput.getOccupationDetails().getHoursPerWeekQuestion());
				        	 }
				        	 
				        	 /* For HOST
			        	     *  Workduties -  Do you work in a hazardous environment and/or perform any duties of a manual nature? hazardousQue
				        	 *  Spendtimeoutside - Do you spend more than 10% of your working time outside of an office environment? managementRoleQue
				        	 *  Occupationduties -  Are your duties entirely undertaken within an office environment? withinOfficeQue
				        	 *  Tertiaryque - Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a
				        	 *  			  government body related to your profession? tertiaryQue
				        	 */
				        	 
			        	     applicant1.setWorkduties(eapplyInput.getOccupationDetails().getHazardousQue()); 
				        	 applicant1.setOccupationduties(eapplyInput.getOccupationDetails().getWithinOfficeQue());
				        	 applicant1.setSpendtimeoutside(eapplyInput.getOccupationDetails().getManagementRoleQue());
				        	 /**Vicsuper Code changes starts - Made by Purna**/
				        	 if ((MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				        			 || MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				        			 || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				        			 || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				        			 || MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplyInput.getManageType())) 
				        			 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				        		 applicant1.setTertiaryque(eapplyInput.getOccupationDetails().getOccRating2());
				        	 } else {
				        		 applicant1.setTertiaryque(eapplyInput.getOccupationDetails().getTertiaryQue());
				        	 }
				        	 /**Vicsuper Code changes ends - Made by Purna**/
			           }
			            if(null != eapplyInput.getRsnFrAdnlCvr() && !"".equalsIgnoreCase(eapplyInput.getRsnFrAdnlCvr())){
			            	applicant1.setRelation(eapplyInput.getRsnFrAdnlCvr());
			            }

			            applicant1.setApplicanttype("Primary");
			           
			            if(eapplyInput.getEmail()!=null){
		            	applicant1.setEmailid(eapplyInput.getEmail());
		            }
			        	applicant1.setDatejoinedfund(eapplyInput.getDateJoined());
			            applicant1.setClientrefnumber(eapplyInput.getClientRefNumber());
			        	applicant1.setMembertype(eapplyInput.getMemberType());
			            applicant1.setAge(Integer.toString(eapplyInput.getAge()));
			            if(eapplyInput.getContactDetails().getFundEmailAddress()!=null){
			            	//applicant1.setEmailid(eapplyInput.getContactDetails().getFundEmailAddress());
			            	applicant1.setCustomerreferencenumber(eapplyInput.getContactDetails().getFundEmailAddress());
			            }
			        	applicant1.setContacttype(eapplyInput.getContactType());
			        	applicant1.setContactnumber(eapplyInput.getContactPhone());
			        	applicant1.setOverallcoverdecision(eapplyInput.getOverallDecision());
			        	if(eapplyInput.getIndexationDeath()!= null && eapplyInput.getIndexationDeath().equalsIgnoreCase("true")){
			        		applicant1.setDeathindexationflag("1");
			        	}else{
			        		applicant1.setDeathindexationflag("0");
			        	}
			        	
			        	if(eapplyInput.getIndexationTpd()!= null && eapplyInput.getIndexationTpd().equalsIgnoreCase("true")){
			        		applicant1.setTpdindexationflag("1");
			        	}else{
			        		applicant1.setTpdindexationflag("0");
			        	}
			        	
			        	if(eapplyInput.getUnitisedCovers()!= null && eapplyInput.getUnitisedCovers().equalsIgnoreCase("true")){
			        		applicant1.setUnitcheckflag("1");
			        	}else{
			        		applicant1.setUnitcheckflag("0");
			        	}
			        	
			        	
			        	if(eapplyInput.getIpcheckbox() != null && eapplyInput.getIpcheckbox().equalsIgnoreCase("true")){
			        		applicant1.setInsuredsalary("Y");
			        	}else{
			        		applicant1.setInsuredsalary("N");
			        	}
		        		
			           /* Transfer Previous cover section fields*/
			        	applicant1.setPreviousinsurername(eapplyInput.getPreviousFundName());
			        	applicant1.setFundmempolicynumber(eapplyInput.getMembershipNumber());
			        	applicant1.setSpinnumber(eapplyInput.getSpinNumber());
			        	applicant1.setPrevioustpdclaimque(eapplyInput.getPreviousTpdClaimQue());
			        	if(eapplyInput != null && eapplyInput.getTerminalIllClaimQue()!= null){
			        		applicant1.setPreviousterminalillque(eapplyInput.getTerminalIllClaimQue());  /*for transfer and new member */
			        	}
			        	if(eapplyInput != null && eapplyInput.getDocumentName() != null){
			        		applicant1.setDocumentevidence(eapplyInput.getDocumentName());  /*for transfer and new member */
			        	}
			        	
			        	/*lifeevent mapping */
			        	
			        	if(null!=eapplyInput && null!=eapplyInput.getEventName() && !"".equalsIgnoreCase(eapplyInput.getEventName())){
			        		applicant1.setLifeevent(eapplyInput.getEventName());
			        		
			        	}
			        	if(null!=eapplyInput && null!=eapplyInput.getEventDate() && !"".equalsIgnoreCase(eapplyInput.getEventDate())){
			        		applicant1.setLifeeventdate(eapplyInput.getEventDate());
			        	}
			        	if(null!=eapplyInput && null!=eapplyInput.getEventAlreadyApplied() && !"".equalsIgnoreCase(eapplyInput.getEventAlreadyApplied())){
			        		applicant1.setEventalreadyapplied(eapplyInput.getEventAlreadyApplied());
			        		
			        	}
			        	/*For Cancel Cover*/
			        	if(null!=eapplyInput && null!=eapplyInput.getCancelreason() && !"".equalsIgnoreCase(eapplyInput.getCancelreason())){
			        		applicant1.setCancelreason(eapplyInput.getCancelreason());
			        	}
			        	if(null!=eapplyInput && null!=eapplyInput.getCancelotherreason() && !"".equalsIgnoreCase(eapplyInput.getCancelotherreason())){
			        		applicant1.setCancelotherreason(eapplyInput.getCancelotherreason());
			        	}
			        		
			        	// For special cover	
			        	if(null!=eapplyInput && null!=eapplyInput.getPreviousTpdBenefit() && eapplyInput.getPreviousTpdBenefit() != ""){
			        		applicant1.setPreviostpdbenefit(eapplyInput.getPreviousTpdBenefit());
			        		
			        	}
			        		
			        	
			        	
			        	/*Applicant object mapping start*/
			        	
			        	List<Cover> coverList = null;
			            if(eapplyInput.getManageType()!= null){
			            	if(eapplyInput.getManageType().equalsIgnoreCase("CCOVER")){
			            		coverList = createChangeCvCovers(eapplyInput);
			            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.TCOVER)){
			            		coverList = createTransferCvCovers(eapplyInput);
			            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.CANCOVER)){
			            		coverList = createCancelCvCovers(eapplyInput);
	 		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.UWCOVER)){
			            		coverList = createUpdateWorkRatingCvCovers(eapplyInput);
			            	}
	 		            }
			            /*Cover object mapping end*/
			            
			            /*Aura detail object mapping start*/
			            Auradetail auradetail =new Auradetail();
			            auradetail.setCreatedate(new Timestamp(new Date().getTime()));
			            auradetail.setLastupdatedate(new Timestamp(new Date().getTime()));		           
			            if(eapplyInput.getResponseObject()!=null){
			            	  auradetail.setShortformstatus("Long");
			            }
			            if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())
			            		&& null!=eapplyInput.getAuraSessionId()) {
			            		auradetail.setAurasessionid(eapplyInput.getAuraSessionId());
			            }
			          
			            /*Aura detail object mapping end*/
			            
			            applicant1.setCovers(coverList);
			            applicantList.add(applicant1);
			            eapplication.setApplicant(applicantList);
			            eapplication.setAuradetail(auradetail);
			            /*eapplication.setFundInfo(fundInfoList);*/
			            
			            if(eapplyInput.getContactDetails()!=null){
			            	Contactinfo contactinfo=new Contactinfo();
			            	contactinfo.setMobilephone(eapplyInput.getContactDetails().getMobilePhone());
			            	contactinfo.setHomephone(eapplyInput.getContactDetails().getHomePhone());
			            	contactinfo.setWorkphone(eapplyInput.getContactDetails().getWorkPhone());
			            	contactinfo.setPreferedcontactnumber(eapplyInput.getContactPhone());
			            	contactinfo.setPreferedcontacttype(eapplyInput.getContactType());
			            	contactinfo.setPreferedcontacttime(eapplyInput.getContactPrefTime());
			            	contactinfo.setAddressline1(eapplyInput.getAddress().getLine1());
			            	contactinfo.setAddressline2(eapplyInput.getAddress().getLine2());
			            	contactinfo.setCountry(eapplyInput.getAddress().getCountry());
			            	contactinfo.setPostcode(eapplyInput.getAddress().getPostCode());
			            	contactinfo.setState(eapplyInput.getAddress().getState());
			            	contactinfo.setSuburb(eapplyInput.getAddress().getSuburb());
			            	contactinfo.setOthercontacttype(eapplyInput.getOtherContactType());
			            	contactinfo.setOthercontactnumber(eapplyInput.getOtherContactPhone());
			            	applicant1.setContactinfo(contactinfo);
			            }
			            
			        
			            /*Adding transfer upload documents*/
			            List<Document> documentList = new ArrayList<>();
		 	 			 if(eapplyInput.getTransferDocuments()!=null && eapplyInput.getTransferDocuments().size()>ZERO_CONST){
		 	 				 for(DocumentJSON documentJSON:eapplyInput.getTransferDocuments()){
		 	 					 Document transferDocument = new Document();
		 	 					 transferDocument.setDocLoc(documentJSON.getFileLocations());
		 	 					 transferDocument.setDocType(MetlifeInstitutionalConstants.TRANSFER_DOCUMENTS);
		 	 					 transferDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
		 	 					documentList.add(transferDocument);	 	 					
		 	 					
		 	 				 }
		 	 				eapplication.setDocuments(documentList);
		 	 			 }
		 	 			
		 	 			 /*Adding life event upload documents*/
		 	 			 if(eapplyInput.getLifeEventDocuments()!=null && eapplyInput.getLifeEventDocuments().size()>ZERO_CONST){
		 	 				 for(DocumentJSON documentJSON:eapplyInput.getLifeEventDocuments()){
		 	 					 Document lifeEventDocument = new Document();
		 	 					lifeEventDocument.setDocLoc(documentJSON.getFileLocations());
		 	 					//lifeEventDocument.setDocType(MetlifeInstitutionalConstants.LIFE_EVENT_DOCUMANTES);
		 	 					if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())){
		 	 						lifeEventDocument.setDocType(eapplyInput.getEventName());
		 	 					} else {
		 	 						lifeEventDocument.setDocType(MetlifeInstitutionalConstants.LIFE_EVENT_DOCUMANTES);
		 	 					}		 	 					
		 	 					lifeEventDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
		 	 					documentList.add(lifeEventDocument);	 	 
		 	 				 }
		 	 				eapplication.setDocuments(documentList);
		 	 			 } 
		 	 			eapplication.setPolicynumber(eapplyInput.getPolicyNumber());
		 	 			
			 		}
		           
		          
			return eapplication;
		}
		private static List<Cover> createUpdateWorkRatingCvCovers(EapplyInput eapplyInput) {
			log.info("Create update workrating start");
			String deathExUnit = "0";
			String tpdExUnit = "0";
			String ipExUnit = "0";
			String ipExistingAmt = null;
			if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
				for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
					CoverJSON coverJSON = (CoverJSON) iterator.next();
					if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
						deathExUnit = coverJSON.getUnits();
					}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
						tpdExUnit = coverJSON.getUnits();
					}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
						ipExUnit = coverJSON.getUnits();
						ipExistingAmt=coverJSON.getAmount();
					}
					
				}
			}
			
			/*Cover object mapping start*/        	
			
			Cover deathCover=new Cover();
			Cover tpdCover=new Cover();
			Cover ipCover=new Cover();
			List<Cover> coverList=new ArrayList<>();
			if(eapplyInput.getDeathAmt()!=null){
				eapplyInput.setExistingDeathAmt(eapplyInput.getDeathAmt());
				if(eapplyInput.getAddnlDeathCoverDetails()!=null){
					eapplyInput.getAddnlDeathCoverDetails().setDeathFixedAmt(eapplyInput.getDeathNewAmt());
				}		
			}
			deathCover.setCreatedate(new Timestamp(new Date().getTime()));
			deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
			deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
			deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
			deathCover.setCoverdecision(eapplyInput.getDeathDecision());
			deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
			if(deathExUnit!= null && deathExUnit !=""){
				deathCover.setFixedunit(Integer.valueOf(deathExUnit));
				deathCover.setAdditionalunit(Integer.valueOf(deathExUnit));
			}
			deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
			/*deathCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);*/
		    /*deathCover.setAdditionalcoveramount(BigDecimal.ZERO);*/
			deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
			deathCover.setCovercategory(eapplyInput.getDeathCoverType());
			if(eapplyInput.getDeathCoverType()!=null){
				if(eapplyInput.getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
					deathCover.setAddunitind("1");
				}else if(eapplyInput.getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
					deathCover.setAddunitind("0");
				}
			}
			deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
			if(eapplyInput.getExistingDeathAmt()!=null){
				deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
			}
			if(eapplyInput.getDeathNewAmt() != null){
				deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getDeathNewAmt()));
			}
			
			if(eapplyInput.getTpdNewAmt()!=null){
				eapplyInput.setExistingTpdAmt(eapplyInput.getTpdNewAmt());
				if(eapplyInput.getAddnlTpdCoverDetails()!=null){
					eapplyInput.getAddnlTpdCoverDetails().setTpdFixedAmt(eapplyInput.getTpdNewAmt());
				}		
			}	
			tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
			tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
			tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
			tpdCover.setCovercode("TPD");
			tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
			tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());

			if(tpdExUnit!=null && tpdExUnit!=""){
				tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
				tpdCover.setAdditionalunit(Integer.valueOf(tpdExUnit));
			}
			
			tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
			/*tpdCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);*/
		    /*tpdCover.setAdditionalcoveramount(BigDecimal.ZERO);*/
			tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
			tpdCover.setCovercategory(eapplyInput.getTpdCoverType());
			if(eapplyInput.getTpdCoverType()!=null){
				if(eapplyInput.getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
					tpdCover.setAddunitind("1");
				}else if(eapplyInput.getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
					tpdCover.setAddunitind("0");
				}
			}
			tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
			if(eapplyInput.getExistingTpdAmt()!=null){
				tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
			}
			if(eapplyInput.getTpdNewAmt()!=null){
				tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getTpdNewAmt()));
			}
			if(ipExistingAmt!=null){
				ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
				eapplyInput.setExistingIPAmount(ipExistingAmt);
				ipCover.setAdditionalcoveramount(new BigDecimal (eapplyInput.getExistingIPAmount()));
			}	
				            
			if(ipExUnit!=null && ipExUnit!=""){
				ipCover.setFixedunit(Integer.valueOf(ipExUnit));
				ipCover.setAdditionalunit(Integer.valueOf(ipExUnit));
			}
			
			if(eapplyInput.getIpNewAmt()!=null){
				eapplyInput.setExistingIPAmount(eapplyInput.getIpNewAmt());
				if(eapplyInput.getAddnlIpCoverDetails()!=null){
					eapplyInput.getAddnlIpCoverDetails().setIpFixedAmt(eapplyInput.getIpNewAmt());
				}		
			}
			
			ipCover.setCreatedate(new Timestamp(new Date().getTime()));
			ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
			ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
			ipCover.setCovercode("IP");
			ipCover.setCoverdecision(eapplyInput.getIpDecision());
			ipCover.setCoverreason(eapplyInput.getIpAuraResons());
			ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
			/*ipCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);*/
		    /*ipCover.setAdditionalcoveramount(BigDecimal.ZERO);*/
			ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
			ipCover.setCovercategory(eapplyInput.getIpCoverType());
			if(eapplyInput.getIpCoverType()!=null){
				if(eapplyInput.getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
					ipCover.setAddunitind("1");
				}else if(eapplyInput.getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
					ipCover.setAddunitind("0");
				}
			}
			ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
			if(eapplyInput.getIpNewAmt()!= null){
				ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getIpNewAmt()));
			}	
			
			if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
				for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
					CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
					if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
						if(coverJSON.getAmount()!=null){
							deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						}
						
					}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
						if(coverJSON.getAmount()!=null){
							tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						}
					}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
							&& coverJSON.getAmount()!=null){
							ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
							ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
							ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
							eapplyInput.getAddnlIpCoverDetails().setBenefitPeriod(coverJSON.getBenefitPeriod());
							ipCover.setAdditionalipbenefitperiod(coverJSON.getBenefitPeriod());
							ipCover.setAdditionalipwaitingperiod(coverJSON.getWaitingPeriod());
					}
					
				}
			}
			coverList.add(deathCover);
			coverList.add(tpdCover);
			coverList.add(ipCover);
			/*Cover object mapping end*/
			log.info("Create update workrating finish");
			return coverList;
		}
    
}

