package pages.metlife.Angular.HOSTPLUS;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_InsuranceCost_back extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_InsuranceCost_back InsuranceCost_HOSTPLUS() {
		InsuranceCost();
		return new HOSTPLUS_Angular_InsuranceCost_back(scriptHelper);
	}
	public HOSTPLUS_Angular_InsuranceCost_back(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void InsuranceCost() {

		String dateofbirth = dataTable.getData("InsuranceCost_Data", "DOB");
		String Gender = dataTable.getData("InsuranceCost_Data", "Gender");
		String FifteenPlusQuestion=dataTable.getData("InsuranceCost_Data", "Morethan_15HoursWork");
		String workindustry=dataTable.getData("InsuranceCost_Data", "WorkIndustry");
		String occupationtype=dataTable.getData("InsuranceCost_Data", "OccupationType");
		String OtherOccupation=dataTable.getData("InsuranceCost_Data", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("InsuranceCost_Data", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("InsuranceCost_Data", "TertiaryQual");
		String HazardousEnv = dataTable.getData("InsuranceCost_Data", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("InsuranceCost_Data", "OutsideOfcPercent");
		String annualsalary=dataTable.getData("InsuranceCost_Data", "AnnualSalary");
		String costfrequncy=dataTable.getData("InsuranceCost_Data", "CostFrequency");
		String costtype=dataTable.getData("InsuranceCost_Data", "CostType");
		String deathamount=dataTable.getData("InsuranceCost_Data", "DeathAmount");
		String DeathYN=dataTable.getData("InsuranceCost_Data", "DeathYN");
		String TPDYN=dataTable.getData("InsuranceCost_Data", "TPDYN");
		String IPYN=dataTable.getData("InsuranceCost_Data", "IPYN");
		String tpdamount=dataTable.getData("InsuranceCost_Data", "TPDAmount");
		String deathunit=dataTable.getData("InsuranceCost_Data", "DeathUnit");
		String tpdunit=dataTable.getData("InsuranceCost_Data", "TPDUnit");
		String waitingperiod=dataTable.getData("InsuranceCost_Data", "WaitingPeriod");
		String benefitperiod=dataTable.getData("InsuranceCost_Data", "BenefitPeriod");
		String coverageunit=dataTable.getData("InsuranceCost_Data", "CoverageUnit");
		String expecteddeathcover=dataTable.getData("InsuranceCost_Data", "ExpectedDeathCover");
		String expectedtpdcover=dataTable.getData("InsuranceCost_Data", "ExpectedTPDCover");
		String expectedipcover=dataTable.getData("InsuranceCost_Data", "ExpectedIpCover");



		try{

			//****Click on Transfer Your Cover Link*****\\
			waitForPageTitle("testing", 8);

			WebDriverWait wait=new WebDriverWait(driver,18);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,3500)", "");
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#collapseThree > div > div:nth-child(3) > div > h4"))).click();
			report.updateTestLog("Insurance Cost", "Clicked on Insurance Cost Link", Status.PASS);
			waitForPageTitle("testing", 4);
			sleep(1500);
		//	 driver.close();
   //     	 switchToActiveWindow();
			if(driver.getPageSource().contains("You have previously saved application(s).")){
				if(driver.findElement(By.xpath("//button[@ng-click='confirm()']")).isDisplayed()){
					sleep(500);
					quickSwitchWindows();
					sleep(1000);
					driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
					report.updateTestLog("Saved App Pop-up", "Continue the saved app", Status.PASS);
					sleep(3500);

				}
				}
	/*		 sleep(500);
	         ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			 driver.switchTo().window(tabs.get(0));
		     driver.close();
			 driver.switchTo().window(tabs.get(1));
			 sleep(2000);		*/
			 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			 driver.switchTo().window(tabs2.get(1));
			 driver.close();
			// driver.switchTo().window(tabs2.get(0));

			 ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			 System.out.println("No. of tabs: " + tabs.size());
			 driver.switchTo().window(tabs.get(0));
			 driver.switchTo().window(tabs.get(1));
			 sleep(3000);
/*
			 sleep(1500);
			 ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			 driver.switchTo().window(tabs.get(0));
			 driver.close();
			 driver.switchTo().window(tabs.get(1));
			 sleep(2000);
			 */

			//****Enter the DOB*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("dobId"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dobId"))).sendKeys(dateofbirth);
			report.updateTestLog("Date Of Birth", dateofbirth+" is entered", Status.PASS);

			//****Select the Gender*****\\

			if(Gender.equalsIgnoreCase("Male")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("gender_male_id__xc_"))).click();
				report.updateTestLog("Gender", "Male is Selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("gender_female_id__xc_"))).click();
				report.updateTestLog("Gender", "Female is Selected", Status.PASS);
			}

		   //*****Do you work 15 or more hours per week?******\\

			if(FifteenPlusQuestion.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhours_yes_id__xc_"))).click();
				report.updateTestLog("Do you work 15 or more hours a week?", "Yes is Selected", Status.PASS);
			}

			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhours_no_id__xc_"))).click();
				report.updateTestLog("Do you work 15 or more hours a week?", "No is Selected", Status.PASS);
			}

			//*****What Industry do you work in*****\\

			sleep(500);
			driver.findElement(By.xpath("(//span[contains(text(),'Please select')])[1]")).click();
			sleep(800);
			driver.findElement(By.xpath("//span[contains(text(),'"+workindustry+"')]")).click();
			sleep(500);
			report.updateTestLog("Industry", workindustry+" is Selected", Status.PASS);
			sleep(1000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,400)", "");
			sleep(500);

           //*****What is your occupation?*****\\

			driver.findElement(By.xpath("(//*[@id='occuption_div_id']/div/label)[1]")).click();
			sleep(1200);
			driver.findElement(By.xpath("(//span[contains(text(),'Please select')])[2]")).click();
			sleep(800);
			driver.findElement(By.xpath("//span[contains(text(),'"+occupationtype+"')]")).click();
			sleep(500);
			report.updateTestLog("Occupation", occupationtype+" is Selected", Status.PASS);
			sleep(1000);




			//*****Other Occupation *****\\

			if(occupationtype.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupationId"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}
			sleep(5000);

			//*****Extra Questions*****\\

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_"))).click();
		    	sleep(300);
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_"))).click();
			sleep(300);
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

		}
			sleep(800);
		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					sleep(300);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				sleep(300);
		}
		}



		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



		//******Do you work in hazardous environment*****\\

		if(HazardousEnv.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.id("askMannual_yes_id__xc_"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.id("askMannual_no_id__xc_"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			sleep(300);
		}
		sleep(800);
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_yes_id__xc_"))).click();
			report.updateTestLog("10% work outside Ofice", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_no_id__xc_"))).click();
			report.updateTestLog("10% work outside Ofice", "Selected No", Status.PASS);
			sleep(300);
		}

		if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


    		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_"))).click();
		    	sleep(300);
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_"))).click();
			sleep(300);
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

		}
			sleep(800);
		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					sleep(300);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				sleep(300);
		}
					}
		}

          sleep(800);







        //*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.id("custAnualSal"))).sendKeys(annualsalary);
		    sleep(500);
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+annualsalary, Status.PASS);

		//*****Display the cost of my insurance cover as****\\

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='premFreq_div_id']/div[2]/span[1]/div/button/span[1]"))).click();
		    sleep(1500);
		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+costfrequncy+"')]"))).click();
		    sleep(500);
		    report.updateTestLog("Insurance Cost Frequency", costfrequncy+" is Selected", Status.PASS);


		  //*****Select the Cost Type*****\\
		    if(DeathYN.equalsIgnoreCase("Yes")){
		    if(costtype.equalsIgnoreCase("Fixed")){

		    	  wait.until(ExpectedConditions.elementToBeClickable(By.id("deathfxd__xc_"))).click();
		    	  sleep(2000);
				  report.updateTestLog("Cost Type", costtype+" is Selected", Status.PASS);
				  sleep(500);

				  //****Enter the Death Amount*****\\

				  wait.until(ExpectedConditions.elementToBeClickable(By.id("dcAddnlCvrIpTxt"))).sendKeys(deathamount);
		    	  sleep(2000);
		    	  driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[1]")).click();
		    	  sleep(2000);
				  report.updateTestLog("Death Cover Amount", deathamount+" is entered", Status.PASS);
				  sleep(500);

               //****Enter the TPD Amount*****\\


				  wait.until(ExpectedConditions.elementToBeClickable(By.id("dc_tpdAddnlCvrIpTxt"))).sendKeys(tpdamount);
		    	  sleep(2000);
		    	  driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[2]")).click();
		    	  sleep(2000);
				  report.updateTestLog("TPD Cover Amount", tpdamount+" is entered", Status.PASS);
				  sleep(200);


		    }

		    else{
		    	sleep(500);
		    	  wait.until(ExpectedConditions.elementToBeClickable(By.id("deathunit__xc_"))).click();
		    	  sleep(2000);
				   report.updateTestLog("Cost Type", costtype+" is Selected", Status.PASS);
				   sleep(500);

					  //****Enter the Death Units*****\\

					  wait.until(ExpectedConditions.elementToBeClickable(By.id("dcAddnlCvrIpTxt"))).sendKeys(deathunit);
			    	  sleep(2000);
			    	  driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[1]")).click();
			    	  sleep(2000);
					  report.updateTestLog("Death Cover Units", deathunit+" units is entered", Status.PASS);
					  sleep(200);

	               //****Enter the TPD Units*****\\

					  sleep(500);
					  wait.until(ExpectedConditions.elementToBeClickable(By.id("dc_tpdAddnlCvrIpTxt"))).sendKeys(tpdunit);
			    	  sleep(2000);
			    	  driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[2]")).click();
			    	  sleep(2000);
					  report.updateTestLog("TPD Cover Units", tpdunit+" units is entered", Status.PASS);
					  sleep(200);

		    }
		    }



		    //*****Enter the IP Cover Details*******\
		    sleep(800);
		  if(IPYN.equalsIgnoreCase("Yes")){

		    //******Select the Waiting Period******\\
		    sleep(1000);
		    driver.findElement(By.xpath("(//button[@data-id='ipWaitingPeriodLst']/span)[1]")).click();
		    sleep(800);
		    driver.findElement(By.xpath("//span[contains(text(),'"+waitingperiod+"')]")).click();
		    sleep(500);
		    report.updateTestLog("IP Waiting Period",waitingperiod+"  is Selected", Status.PASS);

		    //*****Select the Benefit Period******\\
		    sleep(800);
		    driver.findElement(By.xpath("(//button[@data-id='ipBenefitPeriodLst']/span)[1]")).click();
		    sleep(800);
		    driver.findElement(By.xpath("//span[contains(text(),'"+benefitperiod+"')]")).click();
		    sleep(500);
		    report.updateTestLog("IP Waiting Period",benefitperiod+"  is Selected", Status.PASS);

			JavascriptExecutor a = (JavascriptExecutor) driver;
			a.executeScript("window.scrollBy(0,350)", "");
			sleep(500);

		    //*****Select the IP Cover Amount Required*****\\

	//	    wait.until(ExpectedConditions.elementToBeClickable(By.id("ipRequredcvrId"))).click();
		    sleep(1000);
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("ipRequredcvrId"))).click();
		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='ipRequredcvrId']"))).sendKeys(coverageunit);

		    sleep(2000);
		    driver.findElement(By.xpath("(//h5[contains(text(),'Cover amount')])[3]")).click();
		    sleep(2000);
		    report.updateTestLog("IP Cover Amount",coverageunit+"  is entered", Status.PASS);
		  }
		    sleep(1000);

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='agreecheckbox__xc_c']/span"))).click();
		    sleep(400);
		    report.updateTestLog("Acknowledgment checkbox ","Acknowledgment checkbox  is Clicked", Status.PASS);
            sleep(3000);

		    //******Click on Calculate Quote*******\\

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calcQuoteId']/span"))).click();
		    sleep(400);
		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
            sleep(3000);
/*
          String Totalcost=driver.findElement(By.xpath("(//h4[@class='coverval']/span)[2]")).getText();
          if(Totalcost.equalsIgnoreCase("$0.00")){
        	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calcQuoteId']/span"))).click();
  		    sleep(400);
  		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
           sleep(3000);
          }

          if(Totalcost.equalsIgnoreCase("$0.00")){
        	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calcQuoteId']/span"))).click();
  		    sleep(400);
  		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
           sleep(3000);
          }

 */
			///****Fetch the Death cover and TPD cover amounts****\\\



				sleep(500);

				if(DeathYN.equalsIgnoreCase("Yes")){
	            WebElement DeathCover=driver.findElement(By.xpath("(//h4[@class='coverval'])[2]"));
	            DeathCover.click();
	            sleep(500);
				String DeathCoveramount=DeathCover.getText();


				if(DeathCoveramount.equalsIgnoreCase(expecteddeathcover)){

			//		System.out.println("Death cover is as expected ");
			//		System.out.println("Death cover amount displayed "+DeathCoveramount);
			//		System.out.println("Death cover amount expected "+expecteddeathcover);
					report.updateTestLog("Death Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expecteddeathcover+" Actual Premium: "+DeathCoveramount, Status.PASS);
				}
				else{

//					System.out.println("Death cover is not as expected ");
//					System.out.println("Death cover amount displayed "+DeathCoveramount);
//					System.out.println("Death cover amount expected "+expecteddeathcover);
					report.updateTestLog("Death Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expecteddeathcover+" Actual Premium: "+DeathCoveramount, Status.FAIL);
				}
				}

				sleep(1000);
				if(TPDYN.equalsIgnoreCase("Yes")){

				WebElement TPDCover=driver.findElement(By.xpath("//span[@id='tpdCost']"));
				TPDCover.click();
				sleep(500);
				String TPDCoveramount=TPDCover.getText();
				if(TPDCoveramount.equalsIgnoreCase(expectedtpdcover)){
//					System.out.println("TPD cover is as expected ");
//					System.out.println("TPD cover amount displayed "+TPDCoveramount);
//					System.out.println("TPD cover amount expected "+expectedtpdcover);
					report.updateTestLog("TPD Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedtpdcover+" Actual Premium: "+TPDCoveramount, Status.PASS);

				}
				else{
//					System.out.println("TPD cover is not as expected ");
//					System.out.println("TPD cover amount displayed "+TPDCoveramount);
//					System.out.println("TPD cover amount expected "+expectedtpdcover);
					report.updateTestLog("TPD Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedtpdcover+" Actual Premium: "+TPDCoveramount, Status.FAIL);
				}
				}
				 sleep(1000);
				if(IPYN.equalsIgnoreCase("Yes")){
				WebElement IPCover=driver.findElement(By.xpath("(//h4[@class='coverval'])[6]"));
				IPCover.click();
				sleep(500);
				String IpCoverAmount=IPCover.getText();

				if(IpCoverAmount.equalsIgnoreCase(expectedipcover)){
//					System.out.println("Ip cover is as expected ");
//					System.out.println("Ip cover amount displayed "+IpCoverAmount);
//					System.out.println("Ip cover amount expected "+expectedipcover);
					report.updateTestLog("IP Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedipcover+" Actual Premium: "+IpCoverAmount, Status.PASS);

				}
				else{
//					System.out.println("Ip cover is not as expected ");
//					System.out.println("Ip cover amount displayed "+IpCoverAmount);
//					System.out.println("Ip cover amount expected "+expectedipcover);
					report.updateTestLog("IP Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedipcover+" Actual Premium: "+IpCoverAmount, Status.FAIL);
				}
				}
          sleep(1000);


			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}


}
