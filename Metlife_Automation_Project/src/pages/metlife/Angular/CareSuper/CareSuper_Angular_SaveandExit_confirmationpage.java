package pages.metlife.Angular.CareSuper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class CareSuper_Angular_SaveandExit_confirmationpage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public CareSuper_Angular_SaveandExit_confirmationpage SaveandExit_Confirmationpage_CareSuper() {
		ChangeYourCover();
		HealthandLifeStyle();
		confirmation();
		return new CareSuper_Angular_SaveandExit_confirmationpage(scriptHelper);
	}



	public CareSuper_Angular_SaveandExit_confirmationpage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ChangeYourCover() {

		String EmailId = dataTable.getData("CareSuper_SaveandExit", "EmailId");
		String TypeofContact = dataTable.getData("CareSuper_SaveandExit", "TypeofContact");
		String ContactNumber = dataTable.getData("CareSuper_SaveandExit", "ContactNumber");
		String TimeofContact = dataTable.getData("CareSuper_SaveandExit", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("CareSuper_SaveandExit", "FifteenHoursWork");
		String IndustryType = dataTable.getData("CareSuper_SaveandExit", "IndustryType");
		String OccupationType = dataTable.getData("CareSuper_SaveandExit", "OccupationType");
		String OtherOccupation = dataTable.getData("CareSuper_SaveandExit", "OtherOccupation");
		String WithinOffice=dataTable.getData("CareSuper_SaveandExit", "WithinOffice");
		String TertiaryQual = dataTable.getData("CareSuper_SaveandExit", "TertiaryQual");
		String ManagementRole = dataTable.getData("CareSuper_SaveandExit", "ManagementRole");
		String AnnualSalary = dataTable.getData("CareSuper_SaveandExit", "AnnualSalary");
		String DeathYN=dataTable.getData("CareSuper_SaveandExit", "DeathYN");
		String AmountType=dataTable.getData("CareSuper_SaveandExit", "AmountType");
		String DeathAction=dataTable.getData("CareSuper_SaveandExit", "DeathAction");
		String indexfixedcover5percent=dataTable.getData("CareSuper_SaveandExit", "indexfixedcover5percent");
		String DeathAmount=dataTable.getData("CareSuper_SaveandExit", "DeathAmount");
		String DeathUnits=dataTable.getData("CareSuper_SaveandExit", "DeathUnits");
		String TPDYN=dataTable.getData("CareSuper_SaveandExit", "TPDYN");
		String TPDAction=dataTable.getData("CareSuper_SaveandExit", "TPDAction");
		String TPDAmount=dataTable.getData("CareSuper_SaveandExit", "TPDAmount");
		String TPDUnits=dataTable.getData("CareSuper_SaveandExit", "TPDUnits");
		String IPYN=dataTable.getData("CareSuper_SaveandExit", "IPYN");
		String IPAction=dataTable.getData("CareSuper_SaveandExit", "IPAction");
		String Insure85Percent=dataTable.getData("CareSuper_SaveandExit", "Insure85Percent");
		String WaitingPeriod=dataTable.getData("CareSuper_SaveandExit", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("CareSuper_SaveandExit", "BenefitPeriod");
		String IPAmount=dataTable.getData("CareSuper_SaveandExit", "IPAmount");



		try{




			//****Click on Change Your Insurance Cover Link*****\\
			sleep(200);
			JavascriptExecutor a = (JavascriptExecutor)driver;
			a.executeScript("window.scrollBy(0,750)", "");
			Thread.sleep(2000);
	//		sleep(1500);
			WebDriverWait waiting=new WebDriverWait(driver,30);
			waiting.until(ExpectedConditions.elementToBeClickable(By.id("changeCoverId"))).click();
			sleep(3000);
			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[text()='Cancel']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(1000);

			}
			WebDriverWait wait=new WebDriverWait(driver,18);

			//*****Agree to Duty of disclosure*******\\
			 JavascriptExecutor b = (JavascriptExecutor)driver;
				b.executeScript("window.scrollBy(0,700)", "");
				Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			sleep(400);


           //*****Agree to Privacy Statement*******\\
			JavascriptExecutor c = (JavascriptExecutor)driver;
			c.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			sleep(400);

			//*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


		//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath("(//label/input[@value='Morning (9am - 12pm)']/./following::span)[1]")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath("(//label/input[@value='Afternoon (12pm - 6pm)']/./following::span)[1]")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}



			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);


				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\
			JavascriptExecutor d = (JavascriptExecutor)driver;
			d.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(2000);

			//*****Select the 15 Hours Question*******\\

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='fifteenHrsQuestion']/./following::span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='fifteenHrsQuestion']/./following::span)[2]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}



		//*****Industry********\\

	    sleep(500);
	    Select Industry=new Select(driver.findElement(By.name("industry")));

	    Industry.selectByVisibleText(IndustryType);
	    sleep(1000);
	    WebElement inds =Industry.getFirstSelectedOption();
	    System.out.println(inds);
	    sleep(800);
		report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
		sleep(500);

		//*****Occupation*****\\

		Select Occupation=new Select(driver.findElement(By.name("occupation")));
		Occupation.selectByVisibleText(OccupationType);
	    sleep(800);
		report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
		sleep(500);
		JavascriptExecutor e = (JavascriptExecutor)driver;
		e.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(2000);

		//*****Other Occupation *****\\

		if(OccupationType.equalsIgnoreCase("Other")){

			wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
			report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
			sleep(1000);
		}


        //********************************************************\\
		//***************Extra Questions**************************\\
		//*********************************************************\\


	    //***Are the duties of your occupation limited to professional, managerial, administrative, clerical,
		//secretarial or similar 'white collar' tasks which do not involve manual work and are undertaken
		//entirely within an office environment (excluding travel time from one office environment to
		//another)? ***\\

		if(WithinOffice.equalsIgnoreCase("Yes")){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='withinOfficeQuestion']/./following::span)[1]"))).click();
		report.updateTestLog("Work within Office", "Yes is Selected", Status.PASS);
		sleep(500);

		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='withinOfficeQuestion']/./following::span)[2]"))).click();
			report.updateTestLog("Work within Office", "No is Selected", Status.PASS);
			sleep(500);
		}

		//****Do you hold a tertiary qualification or are you a member of a professional institute or
		//registered as a practising member of your profession by a government body?

		if(TertiaryQual.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='tertiaryQuestion']/./following::span)[1]"))).click();
			report.updateTestLog("Tertiary Qualification", "Yes is Selected", Status.PASS);
			sleep(500);
		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='tertiaryQuestion']/./following::span)[2]"))).click();
			report.updateTestLog("Tertiary Qualification", "No is Selected", Status.PASS);
			sleep(500);
		}

		//**********Are you in a management role? ***************\\

		if(ManagementRole.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='managementRole']/./following::span)[1]"))).click();
			report.updateTestLog("Mangement Role", "Yes is Selected", Status.PASS);
			sleep(500);
		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='managementRole']/./following::span)[1]"))).click();
			report.updateTestLog("Mangement Role", "No is Selected", Status.PASS);
			sleep(500);
		}


	//*****What is your annual Salary******


	    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
	    sleep(500);
	    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

	    JavascriptExecutor f = (JavascriptExecutor)driver;
		f.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(2000);

	//*****Click on Continue*******\\

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


//**************************************************************************************************************\\
//****************************COVER CALCULATOR*****************************************************************\\
//*************************************************************************************************************\\




	//*******Death Cover Section******\\

	if(DeathYN.equalsIgnoreCase("Yes")){

		Select deathaction=new Select(driver.findElement(By.name("coverName")));
		deathaction.selectByVisibleText(DeathAction);
		sleep(300);
		report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
		sleep(600);

		if(AmountType.equalsIgnoreCase("Convert")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='coverType']/./following::span)[1]"))).click();
			sleep(800);
			report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

		}

		if(DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){
//		if(DeathYN.equalsIgnoreCase("Yes")&& DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){

		if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert")){
			if(indexfixedcover5percent.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@id='changecvr_index-dth']/./following::span)[1]"))).click();
				sleep(800);
			    report.updateTestLog("Index 5% of Death Cover","is Selected", Status.PASS);
			}
			sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);
				sleep(800);
				report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
				sleep(800);


		}
		else{
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='death']/div[2]/div[6]/div[2]/h4"))).click();

			sleep(200);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathUnits);
			sleep(800);
			report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
			sleep(800);
		}
	}
	}
	JavascriptExecutor w = (JavascriptExecutor)driver;
	w.executeScript("window.scrollBy(0,600)", "");
	Thread.sleep(2000);
	//*******TPD Cover Section******\\
    sleep(800);


	if(TPDYN.equalsIgnoreCase("Yes"))
	{

		Select tpdaction=new Select(driver.findElement(By.name("tpdCoverName")));
		tpdaction.selectByVisibleText(TPDAction);
		sleep(500);
		report.updateTestLog("TPD Cover action", TPDAction+" is selected on TPD cover", Status.PASS);
		sleep(800);



		if(TPDAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Decrease your cover"))
		{

			if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert"))
			{
				if(indexfixedcover5percent.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@id='changecvr_index-dth']/./following::span)[7]"))).click();
					sleep(800);
				    report.updateTestLog("Index 5% of Death Cover","is Selected", Status.PASS);
				}
				sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDAmount);
				sleep(800);
				report.updateTestLog("TPD Cover Amount", TPDAmount+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
				sleep(800);
			}

			else
			{

				wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(TPDUnits);
				sleep(800);
				report.updateTestLog("TPD Cover Amount", TPDUnits+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
				sleep(800);
			}

		}


	}

	JavascriptExecutor v = (JavascriptExecutor)driver;
	v.executeScript("window.scrollBy(0,600)", "");
	Thread.sleep(2000);


                 //*******IP Cover section********\\
sleep(500);
if(IPYN.equalsIgnoreCase("Yes")){

	Select ipaction=new Select(driver.findElement(By.name("ipCoverName")));
	ipaction.selectByVisibleText(IPAction);
	sleep(500);
	report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);

	//*****Insure 85% of My Salary******\\

	if(!IPAction.equalsIgnoreCase("No change")){
		if(Insure85Percent.equalsIgnoreCase("Yes")){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ipsalarychecklabel']/span"))).click();
		report.updateTestLog("Insure 90%", "Insure 85% of My Salary is Selected", Status.PASS);
		sleep(500);
		}
	}
	sleep(500);
	if(!IPAction.equalsIgnoreCase("No change")){

		Select waitingperiod=new Select(driver.findElement(By.xpath("//*[@ng-model='waitingPeriodAddnl']")));
		waitingperiod.selectByVisibleText(WaitingPeriod);
		sleep(300);
		report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
		sleep(500);


		Select benefitperiod=new Select(driver.findElement(By.xpath("//*[@ng-model='benefitPeriodAddnl']")));
		benefitperiod.selectByVisibleText(BenefitPeriod);
		sleep(300);
		report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
		sleep(500);
	}
	sleep(500);
	if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")){

		if(!Insure85Percent.equalsIgnoreCase("Yes"))
		{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@name='IPRequireCover'])[2]"))).sendKeys(IPAmount);
			sleep(800);
			report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
			sleep(800);

		}
		sleep(500);
////		if(IPAction.equalsIgnoreCase("Decrease your cover")){
//
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(IPAmount);
//				sleep(800);
//				report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
//				sleep(800);

			}

	}

JavascriptExecutor n = (JavascriptExecutor)driver;
n.executeScript("window.scrollBy(0,700)", "");
Thread.sleep(2000);

//*****Click on Calculate Quote********\\

sleep(500);
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
sleep(3000);

  //*****Check the terms label*****\\

driver.findElement(By.xpath("//*[@id='termsLabel']/span")).click();
report.updateTestLog("Terms Label", "Checkbox is Selected", Status.PASS);
sleep(500);





//******Click on Continue button******\\


driver.findElement(By.xpath("(//button[contains(text(),'Continue')])[3]")).click();
report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);
sleep(3000);

//*****Handling the pop-up*******\\

	 if(DeathAction.equalsIgnoreCase("Decrease your cover")||DeathAction.equalsIgnoreCase("Cancel your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")||TPDAction.equalsIgnoreCase("Cancel your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){
		quickSwitchWindows();
		sleep(250);

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='checkForAura()']"))).click();
		report.updateTestLog("Pop-Up", "Pop-up continue button is Clicked", Status.PASS);
		sleep(1500);
	 }


}catch(Exception e){
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
}



	public void HealthandLifeStyle() {

		String DeathAction=dataTable.getData("CareSuper_SaveandExit", "DeathAction");


		String TPDAction=dataTable.getData("CareSuper_SaveandExit", "TPDAction");

		String IPAction=dataTable.getData("CareSuper_SaveandExit", "IPAction");
		String Height=dataTable.getData("CareSuper_SaveandExit", "Height");
		String HeightType=dataTable.getData("CareSuper_SaveandExit", "HeightType");
		String Weight=dataTable.getData("CareSuper_SaveandExit", "Weight");
		String WeightType=dataTable.getData("CareSuper_SaveandExit", "WeightType");
		String Smoker=dataTable.getData("CareSuper_SaveandExit", "Smoker");
		String Gender = dataTable.getData("CareSuper_SaveandExit", "Gender");
		String Pregnant = dataTable.getData("CareSuper_SaveandExit", "Pregnant");

		String Exclusion_3Years_Question_for_TPDandIP=dataTable.getData("CareSuper_SaveandExit", "Exclusion_3Years_Question_for_TPDandIP");
		String Loading_3Years_Questions_for_Death50_TPD50_IP100=dataTable.getData("CareSuper_SaveandExit", "Loading_3Years_Questions_for_Death50_TPD50_IP100");
		String Loading_3Years_Questions_for_Death100_TPD100_IP150=dataTable.getData("CareSuper_SaveandExit", "Loading_3Years_Questions_for_Death100_TPD100_IP150");


		String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore=dataTable.getData("CareSuper_SaveandExit", "Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");

		String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP=dataTable.getData("CareSuper_SaveandExit", "Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
		String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP=dataTable.getData("CareSuper_SaveandExit", "Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");


		String UsualDoctororMedicalCentre=dataTable.getData("CareSuper_SaveandExit", "UsualDoctororMedicalCentre");
		String UsualDoctor_Name=dataTable.getData("CareSuper_SaveandExit", "UsualDoctor_Name");
		String UsualDoctor_ContactNumber=dataTable.getData("CareSuper_SaveandExit", "UsualDoctor_ContactNumber");
		String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50=dataTable.getData("CareSuper_SaveandExit", "Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
		String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP=dataTable.getData("CareSuper_SaveandExit", "Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
		String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP=dataTable.getData("CareSuper_SaveandExit", "Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
		String Drugs_Last5Years=dataTable.getData("CareSuper_SaveandExit", "Drugs_Last5Years");
		String Alcohol_DrinksPerDay=dataTable.getData("CareSuper_SaveandExit", "Alcohol_DrinksPerDay");
		String Alcohol_Professionaladvice=dataTable.getData("CareSuper_SaveandExit", "Alcohol_Professionaladvice");
		String HIVInfected=dataTable.getData("CareSuper_SaveandExit", "HIVInfected");
		String PriorApplication=dataTable.getData("CareSuper_SaveandExit", "PriorApplication");
		String PreviousClaims=dataTable.getData("CareSuper_SaveandExit", "PreviousClaims");
		String PreviousClaims_PaidBenefit_terminalillness=dataTable.getData("CareSuper_SaveandExit", "PreviousClaims_PaidBenefit_terminalillness");
		String CurrentPolicies=dataTable.getData("CareSuper_SaveandExit", "CurrentPolicies");



		try{
			if(DeathAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Increase your cover")||
					IPAction.equalsIgnoreCase("Increase your cover"))
			{

			WebDriverWait wait=new WebDriverWait(driver,18);

			//******Enter the Height*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='heightval']"))).sendKeys(Height);
			sleep(200);
			report.updateTestLog("Height", Height+" is Entered", Status.PASS);
			sleep(250);

			//******Choose Height Type******\\

			Select heighttype=new Select(driver.findElement(By.xpath("//*[@ng-model='heightDropDown']")));
			heighttype.selectByVisibleText(HeightType);
			sleep(250);
			report.updateTestLog("Height Type", HeightType+" is Selected", Status.PASS);
			sleep(500);

			//******Enter the Weight*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='weightVal']"))).sendKeys(Weight);
			sleep(200);
			report.updateTestLog("Weight",Weight+" is Entered", Status.PASS);
			sleep(250);

			//******Choose Weight Type******\\

			Select weighttype=new Select(driver.findElement(By.xpath("//*[@ng-model='weightDropDown']")));
			weighttype.selectByVisibleText(WeightType);
			sleep(250);
			report.updateTestLog("Weight Type", WeightType+" is Selected", Status.PASS);
			sleep(500);

			//******Have you smoked in the past 12 months?*******\\

			if(Smoker.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(500);
				report.updateTestLog("Smoker Question", "Yes is Selected", Status.PASS);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(500);
				report.updateTestLog("Smoker Question", "No is Selected", Status.PASS);
			}

			sleep(800);

			if(Gender.equalsIgnoreCase("Female")){

				//******Are you currently pregnant?*******\\

			if(Pregnant.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Are you currently pregnant?", "Yes is Selected", Status.PASS);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Are you currently pregnant?", "No is Selected", Status.PASS);
			}
			sleep(800);
			}

			//**********************************************************************************************************************\\
			//*******In the last 3 years have you suffered from, been diagnosed with or sought medical advice or treatment for*******\\
			//***********************************************************************************************************************\\

			if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Headaches or migraines')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Last 3 Years Question", "Headaches or migraines is Selected", Status.PASS);
				sleep(800);

			//******Are you currently under investigations or contemplating investigations for your headaches?*****\\

					driver.findElement(By.xpath(" //div/label[text()='Are you currently under investigations or contemplating investigations for your headaches?']/../following-sibling::div/div/div[2]/div/div/label/span")).click();
					sleep(800);
					report.updateTestLog("Headache or Migraines_Under Current Investigations", "No is Selected", Status.PASS);
					sleep(200);

				//******How would you describe your headaches?******\\

					Select headaches=new Select(driver.findElement(By.xpath("//*[@ng-model='selectedName']")));
					headaches.selectByVisibleText("Recurring or severe episodes");
					sleep(600);
					report.updateTestLog("Type of Headache", "Recurring or severe episodes is Selected", Status.PASS);
					sleep(800);

				//******Select the Headache Type*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']"))).click();
					sleep(800);

				//***Have your headaches been fully investigated with all underlying causes excluded?****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Have your headaches been fully investigated with all underlying causes excluded?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
					sleep(500);
					report.updateTestLog("Is headache fully investigated?","Yes is Selected", Status.PASS);
					sleep(800);

				//*****How many headaches do you suffer in a week?******\\

					Select headachesno=new Select(driver.findElement(By.xpath("(//*[@ng-model='selectedName'])[2]")));
					headachesno.selectByVisibleText("3 episodes or less");
					sleep(250);
					report.updateTestLog("Number of Headaches","3 episodes or less is Selected", Status.PASS);
					sleep(800);

					driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
					sleep(800);



				//*******Is this easily controlled with over the counter medication?******\\


					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is this easily controlled with over the counter medication?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
					sleep(250);
					report.updateTestLog("Can  your headache be controlled over Medication", "Yes is Selected", Status.PASS);
					sleep(800);

			}

			if(Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")||Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Lung or breathing conditions')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Last 3 Years Question", "Lung or breathing conditions is Selected", Status.PASS);
				sleep(800);

				//****What was Your Diagnosis*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Asthma')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Lung or breathing conditions", "Asthma is Selected", Status.PASS);
				sleep(800);

				//****Is your condition?******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Moderate')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Lung or breathing conditions", "Moderate Asthma is Selected", Status.PASS);
				sleep(2000);

				if(Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){

					//****Is your Asthma worsened by your condition****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
					sleep(250);
					report.updateTestLog("Lung or breathing conditions", "Asthma is worsened by current occupation", Status.PASS);
					sleep(800);

				}
				else{
                    //****Is your Asthma worsened by your condition****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
					sleep(250);
					report.updateTestLog("Lung or breathing conditions", "Asthma is not worsened by current occupation", Status.PASS);
					sleep(800);
				}


			}

			if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")){
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[1]"))).click();
				sleep(250);
				report.updateTestLog("Last 3 Years Question?","None of the above is Selected", Status.PASS);
				sleep(800);
			}
			sleep(1000);

			//********************************************************************************\\
			//*****************************5 Years Question***********************************\\
			//********************************************************************************\\

			if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'High cholesterol')]]/span"))).click();
			sleep(250);
			report.updateTestLog("Last 5 Years Question", "High Cholesterol is Selected", Status.PASS);
			sleep(800);

		  //******How is your high Cholesterol being treated??******\\

			Select cholesteroltreatment=new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
			cholesteroltreatment.selectByVisibleText("Medication prescribed by your doctor");
			sleep(250);
			report.updateTestLog("High Cholesterol","Treated by medication prescribed by your doctor", Status.PASS);
			sleep(800);

		  //******Select the Treatment Type*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']"))).click();
				sleep(800);

		  //******When did you last have a reading taken?*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'12 months ago or less')]]/span"))).click();
				sleep(250);
				report.updateTestLog("High Cholesterol","Last reading taken 12 months ago or less", Status.PASS);
				sleep(800);

			//****How did your doctor describe your most recent cholesterol reading?*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Elevated')]]/span"))).click();
				sleep(250);
				report.updateTestLog("High Cholesterol","Last reading was described by doctor as Elevated", Status.PASS);
				sleep(1000);

		   //****Do you recall your last reading?***\\


				driver.findElement(By.xpath("//div/label[text()='Do you recall your last reading?']/../following-sibling::div/div/div[1]/div/div/label/span")).click();
				sleep(800);
				report.updateTestLog("High Cholesterol","Do you remember the last Reading- Yes", Status.PASS);
				sleep(1000);

				//*****What was your most recent cholesterol reading taken by your doctor?****\\

				driver.findElement(By.xpath("(//*[@ng-model='rangeText'])[1]")).sendKeys("7.1");
				sleep(400);
				report.updateTestLog("High Cholesterol","Last reading is 7.1 mmol/L", Status.PASS);
				sleep(800);

				driver.findElement(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]")).click();
				sleep(1000);

				//*****Have you been advised that your triglycerides are elevated?****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been advised that your triglycerides are elevated? ']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(250);
				report.updateTestLog("High Cholesterol","Tryglycerides is not Elevated", Status.PASS);
				sleep(800);


			}
			if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")){
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[2]"))).click();
				sleep(250);
				report.updateTestLog("Last 5 Years Question","None of the above is Selected", Status.PASS);
				sleep(800);
			}
			sleep(800);

		//****************************************************************************************************************************************\\
		//**********Have you ever suffered from, been diagnosed with or sought medical advice or treatment for (Please tick all that apply):*******\\
        //*****************************************************************************************************************************************\\

			if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Bone, joint or limb conditions')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Prior Treatment or Diagnosis Question","Bone,Joint or limb conditions is Selected", Status.PASS);
				sleep(800);

				//*******What was your diagnosis?********\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Amputation/missing or deformed limb')]]/span"))).click();
				sleep(250);
				report.updateTestLog("What was your diagnosis?","Amputation/missing or deformed limb is Selected", Status.PASS);
				sleep(800);

				//*******Which limbs were amputated/missing or deformed?*****\\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Arm(s)')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Which limbs were amputated/missing or deformed?"," Arm(s) is Selected", Status.PASS);
				sleep(1200);

				//****was it?****\\
				/*
				Select arm=new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
				arm.selectByVisibleText("The left arm");
				sleep(500);
				report.updateTestLog("Which Limb?","The left arm is Selected", Status.PASS);
				sleep(1000);
				*/
				driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
				sleep(800);

			}

			if(Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Thyroid conditions')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Prior Treatment or Diagnosis Question","Thyroid Conditions is Selected", Status.PASS);
				sleep(800);

				//*****What was the specific condition*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Hyperthyroidism (Overactive)')]]/span"))).click();
				sleep(250);
				report.updateTestLog("Thyroid Conditions"," Hyperthyroidism (Overactive) is Selected", Status.PASS);
				sleep(800);

				//****Has your condition been fully investigated?******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Has your condition been fully investigated?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(250);
				report.updateTestLog("Thyroid Conditions","Condition has been fully investigated is Selected", Status.PASS);
				sleep(800);

				//****Is your condition Fully Controlled*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is your condition fully controlled?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(250);
				report.updateTestLog("Thyroid Conditions","Condition is not fully controlled is Selected", Status.PASS);
				sleep(800);


			}


			if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")&&Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("No")){
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[3]"))).click();
				sleep(250);
				report.updateTestLog("Prior Treatment or Diagnosis?","None of the above is Selected", Status.PASS);
				sleep(800);
			}
			sleep(500);



			//******Do you have a usual doctor or medical centre you regularly visit?******\\

			if(UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div/div/div/label/span)[1]"))).click();
				sleep(800);
				report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","Yes is Selected", Status.PASS);

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']"))).sendKeys(UsualDoctor_Name);
				sleep(800);
				report.updateTestLog("Usual Doctor Name",UsualDoctor_Name+" is Entered", Status.PASS);
				sleep(300);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']"))).click();
				sleep(4000);


				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeText']"))).sendKeys(UsualDoctor_ContactNumber);
				sleep(4000);
				report.updateTestLog("Usual Doctor Contact NUmber",UsualDoctor_ContactNumber+" is Entered", Status.PASS);
				sleep(4000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeText,x)']"))).click();
				sleep(4000);

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Add another Doctor?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(4000);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(4000);
				report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","No is Selected", Status.PASS);
			}
			 JavascriptExecutor j = (JavascriptExecutor)driver;
				j.executeScript("window.scrollBy(0,700)", "");
				Thread.sleep(1000);
				sleep(4000);
			//******Click on Continue button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[1]"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(7000);

			//******************************************************************************************************\\
			//*********************************Family History*********************************************************\\
			//******************************************************************************************************\\

			//****Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions***\\

			if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(1000);
				report.updateTestLog("Family History Question","Yes is Selected", Status.PASS);

				//***Select the health condition****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'Diabetes')]]/span)[2]"))).click();
				sleep(1000);
				report.updateTestLog("Health condition diagnosed"," Diabetes is Selected", Status.PASS);
				sleep(1000);

				//***How many family members were affected?****\\

				driver.findElement(By.xpath("//div/label[text()='How many family members were affected?']/../following-sibling::div/div[1]/input")).sendKeys("3");
				sleep(800);
				report.updateTestLog("Number of Family members affected","3 is Entered", Status.PASS);
				sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]"))).click();
				sleep(800);

				//***were two or more family members diagnosed over age 19****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Were two or more family members diagnosed over the age of 19?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Number of affected Family members over age 19","Two or more is Selected", Status.PASS);
				sleep(500);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Family History Question","No is Selected", Status.PASS);
				sleep(500);
			}
			sleep(1000);

			//******Click on Continue button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[2]"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(1200);

			//********************************************************************************************************\\
			//******************************Lifestyle Questions********************************************************\\
			//*********************************************************************************************************\\

			//*******Do you have firm plans to travel or reside in another country*******\\
			if(Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Plans to travel outside the country","Yes is Selected", Status.PASS);
				sleep(800);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Plans to travel outside the country","No is Selected", Status.PASS);
				sleep(800);
			}


			//******Do you regularly engage in or intend to engage in any of the following hazardous activities*****\\

			if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Water sports')]]/span"))).click();
				sleep(800);
				report.updateTestLog("Do you engage in any Hazardous activities","Water sports is Selected", Status.PASS);
				sleep(200);



				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Underwater diving')]]/span"))).click();
				sleep(800);
				report.updateTestLog("Hazardous activity Type","Underwater diving is Selected", Status.PASS);
				sleep(200);

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'More than 40 metres')]]/span"))).click();
				sleep(800);
				report.updateTestLog("Dept of diving ","More than 40 metres is Selected", Status.PASS);
				sleep(500);
			}

			if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")){
				sleep(1000);
				if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
					driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[5]")).click();
				}
				else{
					driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[4]")).click();
				}


				sleep(800);
				report.updateTestLog("Do you engage in any Hazardous activities","None of the above is Selected", Status.PASS);
				sleep(500);
			}






			//*****Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter)
			//or have you exceeded the recommended dosage for any medication?

			if(Drugs_Last5Years.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Have you sed any drugs in the last 5 years","Yes is Selected", Status.PASS);
				sleep(200);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Have you used any drugs in the last 5 years","No is Selected", Status.PASS);
				sleep(200);
			}


			//*****How many alcoholic drinks you have in a day?*****\\
			sleep(800);
			driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).click();
			sleep(250);
			driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).sendKeys(Alcohol_DrinksPerDay);
			sleep(800);
			report.updateTestLog("How many alcoholic drinks per day","1 is Selected", Status.PASS);
			sleep(900);
			driver.findElement(By.xpath("//button[text()='Enter']")).click();
			sleep(900);

			//****Have you ever been advised by a health professional to reduce your alcohol consumption?****\\

			if(Alcohol_Professionaladvice.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(1600);
				report.updateTestLog("Advised to reduce alcohol consumption","Yes is Selected", Status.PASS);
				sleep(200);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(1600);
				report.updateTestLog("Advised to reduce alcohol consumption","No is Selected", Status.PASS);
				sleep(200);
			}


			//******Are you infected with HIV (Human Immunodeficiency Virus******\\



			if(HIVInfected.equalsIgnoreCase("Yes")){
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
		//		sleep(500);
		//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(500);
				report.updateTestLog("Are you infected with HIV","Yes is Selected", Status.PASS);
				sleep(800);

			}
			else{
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(500);
	//chrome		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
		//		sleep(500);
				report.updateTestLog("Are you infected with HIV","No is Selected", Status.PASS);
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been referred for or waiting on an HIV test result and/or are taking preventative medication?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(1000);
			}

			sleep(1000);
		//chrome	sleep(10000);

			//******Click on Continue button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[3]"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
		///chrome-	sleep(8000);
			sleep(1000);

			//******************************************************************************************************\\
			//****************************General Questions******************************************************\\
			//*******************************************************************************************************\\
			WebDriverWait waiting=new WebDriverWait(driver,40);
			waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
			sleep(4000);
	//chrome
		//	waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();

			report.updateTestLog("Do you presently suffer from any condition?","No is Selected", Status.PASS);
			//chrome - sleep(9000);
			sleep(1000);
			//******Click on Continue button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[4]"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(2000);

			//***********************************************************************************************\\
			//***************************INSURANCE DETAILS****************************************************\\
			//**************************************************************************************************\\

			//*****Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or
			//accepted with a loading or exclusion or any other special conditions or terms?

			if(PriorApplication.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[1]/div/div[2]/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","Yes is Selected", Status.PASS);

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				//	chromesleep(6000);
			}

			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","No is Selected", Status.PASS);
			}
			sleep(800);
		//chrome	sleep(6000);


			//****Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers�
			//Compensation, or any other form of compensation due to illness or injury?
			sleep(200);
			if(PreviousClaims.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Have you made a prior claim?","Yes is Selected", Status.PASS);

				if(PreviousClaims_PaidBenefit_terminalillness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
					sleep(800);
				}

			}
			else{
				sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
				sleep(800);
				report.updateTestLog("Have you made a prior claim?","No is Selected", Status.PASS);
			}


			if(driver.getPageSource().contains("Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?")){

				//*****Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?
				 if(CurrentPolicies.equalsIgnoreCase("yes")){
					 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-19:0']"))).click();
						sleep(800);
						report.updateTestLog("Do you currently have any insurance policies?","Yes is Selected", Status.PASS);

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-19_0_0:0']"))).click();
						sleep(800);

						wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-19_0_0_0_0"))).sendKeys("90000");
						sleep(800);

						wait.until(ExpectedConditions.elementToBeClickable(By.id("ran219_0_0_0_0-Insurance_Details-Insurance_Details-19"))).click();
						sleep(800);
				 }
				 else{
					 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
						sleep(800);
						report.updateTestLog("Do you currently have any insurance policies?","No is Selected", Status.PASS);
				 }

			}
			sleep(1000);




			//****Click on Continue****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedToNext()']"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
		//chrome-	sleep(90000);
			sleep(6500);

			}
		}
		catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void confirmation() {


		WebDriverWait wait=new WebDriverWait(driver,20);

			try{
				JavascriptExecutor w = (JavascriptExecutor)driver;
				w.executeScript("window.scrollBy(0,3000)", "");
				Thread.sleep(2000);
			//	if(driver.getPageSource().contains("By clicking on the tick box below, I declare that:")){

			//****Loading consent*****\\


				//****Click on Continue****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()=' Save & exit'])[1]"))).click();
				report.updateTestLog("Save And Exit","Button is Clicked", Status.PASS);
				Thread.sleep(2000);

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Finish & Close Window '])[1]"))).click();
				Thread.sleep(2000);
				report.updateTestLog("Finish and Close Button","Button is Clicked", Status.PASS);

			//******Agree to general Consent*******\\



		//	}

				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
	}
}