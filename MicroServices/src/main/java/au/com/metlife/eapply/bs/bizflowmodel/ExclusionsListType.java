package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExclusionsListType")

@XmlRootElement
public class ExclusionsListType {	
	
	private String ExclusionNameCode;
	private String ExclusionTerm;
	public String getExclusionNameCode() {
		return ExclusionNameCode;
	}
	public void setExclusionNameCode(String exclusionNameCode) {
		ExclusionNameCode = exclusionNameCode;
	}
	public String getExclusionTerm() {
		return ExclusionTerm;
	}
	public void setExclusionTerm(String exclusionTerm) {
		ExclusionTerm = exclusionTerm;
	}
	

	
	
	
}
