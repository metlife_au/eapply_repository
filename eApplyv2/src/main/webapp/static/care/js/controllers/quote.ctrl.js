/* Change Cover Controller,Progressive and Mandatory validations Starts  */
CareSuperApp.controller('quote',['$scope','$rootScope', '$routeParams', '$location','$http', '$timeout', '$window', 'QuoteService', 'OccupationService','persoanlDetailService',
    'deathCoverService','tpdCoverService','ipCoverService', 'CalculateService', 'MaxLimitService','ConvertService',
    'auraInputService', 'CalculateDeathService', 'CalculateTPDService', 'PersistenceService','ngDialog','auraResponseService','saveEapply','RetrieveAppDetailsService','urlService','$filter','APP_CONSTANTS',
    function($scope,$rootScope, $routeParams, $location,$http, $timeout, $window, QuoteService, OccupationService, persoanlDetailService, deathCoverService, tpdCoverService,
              ipCoverService, CalculateService, MaxLimitService,ConvertService,auraInputService, CalculateDeathService,
              CalculateTPDService, PersistenceService,ngDialog,auraResponseService,saveEapply,RetrieveAppDetailsService,urlService,$filter, APP_CONSTANTS){
	/* Code for appD starts */
	  var pageTracker = null;
	  if(ADRUM) {
	    pageTracker = new ADRUM.events.VPageView();
	    pageTracker.start();
	  }

	  $scope.$on('$destroy', function() {
	    pageTracker.end();
	    ADRUM.report(pageTracker);
	  });
	  /* Code for appD ends */       
	
	  		$rootScope.$broadcast('enablepointer');
            $scope.phoneNumbr = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
            $scope.emailFormat = APP_CONSTANTS.emailFormat;
            $scope.regex = /[0-9]{1,3}/;
            $scope.isDCCoverRequiredDisabled = true;
            $scope.isDCCoverTypeDisabled = true;
            $scope.isTPDCoverRequiredDisabled = true;
            $scope.isTPDCoverTypeDisabled = true;
            $scope.isTPDCoverNameDisabled = false;
            $scope.isWaitingPeriodDisabled = true;
            $scope.isBenefitPeriodDisabled = true;
            $scope.isIPCoverRequiredDisabled = true;
            $scope.isIPCoverNameDisabled = true;
            $scope.isIpSalaryCheckboxDisabled = true;
            $scope.ipWarningFlag = false;
            $scope.coltwo = false;
            $scope.colthree = false;
            $scope.ackFlag = false;
            $scope.modalShown = false;
          $scope.dcCoverAmount = 0.00;
            $scope.dcWeeklyCost = 0.00;
            $scope.tpdCoverAmount = 0.00;
            $scope.tpdWeeklyCost = 0.00;
            $scope.ipCoverAmount = 0.00;
            $scope.ipWeeklyCost = 0.00;
            $scope.totalWeeklyCost = 0.00;
            $scope.invalidSalAmount = false;
            $scope.dcIncreaseFlag = false;
            $scope.tpdIncreaseFlag = false;
            $scope.ipIncreaseFlag = false;
            $scope.auraDisabled = false;
            $scope.disclaimerFlag = true;
            $scope.otherOccupationObj = {'otherOccupation': ''};
            $scope.urlList = urlService.getUrlList();
            $scope.contactTypeOptions = ["Home", "Work", "Mobile"];
            $scope.preferredContactType = '';
            $scope.isDCIndexationDisabled = true;
            $scope.isTPDIndexationDisabled = true;
            $scope.ipunitsOnchange = null;
            $scope.ipIncreaseFlagfinal = false;
            /*Error Flags*/
            $scope.dodFlagErr = null;
            $scope.privacyFlagErr = null;
            $scope.unitDisplay = false;
            $scope.DeathCoverOptionsOne = [{
            key: 'option1',
            coverOptionName: 'Increase your cover',
            showAlways: true
          }, {
            key: 'option2',
            coverOptionName: 'Decrease your cover'
          },
          {
            key: 'option3',
            coverOptionName: 'Cancel your cover'
          },
          {
            key: 'option4',
            coverOptionName: 'Convert and maintain cover'
          },
          {
            key: 'option5',
            coverOptionName: 'No change',
            showAlways: true
          }];
           
          $scope.TPDCoverOptionsOne = [{
            key: 'option1',
            coverOptionName: 'Increase your cover',
            showAlways: true
          }, {
            key: 'option2',
            coverOptionName: 'Decrease your cover'
          },
          {
            key: 'option3',
            coverOptionName: 'Cancel your cover'
          },
          {
            key: 'option4',
            coverOptionName: 'Convert and maintain cover'
          },
          {
            key: 'option5',
            coverOptionName: 'No change',
            showAlways: true
          },
          {
            key: 'option6',
            coverOptionName: 'Same as Death Cover'
          }];
         
          $scope.IPCoverOptionsOne = [{
            key: 'option1',
            coverOptionName: 'Increase your cover',
            showAlways: true
          }, {
            key: 'option2',
            coverOptionName: 'Decrease your cover'
          },
          {
            key: 'option3',
            coverOptionName: 'Cancel your cover'
          },
         
          {
            key: 'option5',
            coverOptionName: 'No change',
            showAlways: true
          },
          {
            key: 'option6',
            coverOptionName: 'Change Waiting and Benefit Period'
          }
          ];
         
          $scope.coverOptionsTwo = [{
            key: 'option1',
            coverOptionName: 'Increase your cover',
            showAlways: true
          },
          {
            key: 'option2',
            coverOptionName: 'No change',
            showAlways: true
          }
          ];
          $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
          $scope.benefitPeriodOptions = ['2 Years', '5 Years'];
          $scope.indexation= {
                  death: false,
                  disable: false
          };
         
            var dynamicFlag = false;
            var fetchAppnum = true;
            var appNum;
            var ackCheck;
            var DCMaxAmount, TPDMaxAmount;
            var FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime'];
            var deathAmount, TPDAmount, IPAmount;
            var ipUnchanged = true;
            var mode3Flag = false;
            var unitisedDeathInitialFlag = false;
            var unitisedTpdInitialFlag = false;
            var tpdUnitFlag = true, tpdUnitSuccess = false;
            var maxTPDUnits;
            var disableIpCover = false;
            var OccupationFormFields, OccupationOtherFormFields;
            $scope.noconvert = true;
            // added for session expiry
           
           
            $scope.deathCoverDetails = deathCoverService.getDeathCover();
            $scope.tpdCoverDetails = tpdCoverService.getTpdCover();
            $scope.ipCoverDetails = ipCoverService.getIpCover();
           
            if($scope.deathCoverDetails.indexation == '1' && $scope.tpdCoverDetails.indexation == '1'){
                  $scope.indexation.death = $scope.indexation.disable = true;
                  $("#changecvr_index-dth").parent().addClass('active');
            $("#changeCvr-indextpd-disable").parent().addClass('active');
            } else{
                  $scope.indexation.death = $scope.indexation.disable = false;
                  $("#changecvr_index-dth").parent().removeClass('active');
            $("#changeCvr-indextpd-disable").parent().removeClass('active');
            }
           
            if($scope.deathCoverDetails && $scope.deathCoverDetails.occRating && $scope.deathCoverDetails.occRating != ''){
                  $scope.deathOccupationCategory = $scope.deathCoverDetails.occRating;
                  $scope.existingDeathOccupationCategory = $scope.deathCoverDetails.occRating;
            } else{
                  $scope.deathOccupationCategory = 'General';
                  $scope.existingDeathOccupationCategory = 'General';
            }
            if($scope.tpdCoverDetails && $scope.tpdCoverDetails.occRating && $scope.tpdCoverDetails.occRating != ''){
                  $scope.tpdOccupationCategory = $scope.tpdCoverDetails.occRating;
                  $scope.existingTpdOccupationCategory = $scope.tpdCoverDetails.occRating;
            } else{
                  $scope.tpdOccupationCategory = 'General';
                  $scope.existingTpdOccupationCategory = 'General';
            }
            if($scope.ipCoverDetails && $scope.ipCoverDetails.occRating && $scope.ipCoverDetails.occRating != ''){
                  $scope.ipOccupationCategory = $scope.ipCoverDetails.occRating;
                  $scope.existingIpOccupationCategory = $scope.ipCoverDetails.occRating;
            } else{
                  $scope.ipOccupationCategory = 'General';
                  $scope.existingIpOccupationCategory = 'General';
            }
           
            if ($scope.deathCoverDetails && $scope.deathCoverDetails.type) {
                  if($scope.deathCoverDetails.type == "1"){
                        $scope.coverType = "DcUnitised";
                        $scope.exDcCoverType = "DcUnitised";
                        showhide('nodollar1','dollar1');
                        showhide('nodollar','dollar');
                  } else if($scope.deathCoverDetails.type == "2"){
                        $scope.coverType = "DcFixed";
                        $scope.exDcCoverType = "DcFixed";
                  }
            }
 
            if ($scope.tpdCoverDetails && $scope.tpdCoverDetails.type) {
                  if($scope.tpdCoverDetails.type == "1"){
                        $scope.tpdCoverType = "TPDUnitised";
                        $scope.exTpdCoverType = "TPDUnitised";
                        showhide('nodollar1','dollar1');
                        showhide('nodollar','dollar');
                  } else if($scope.tpdCoverDetails.type == "2"){
                        $scope.tpdCoverType = "TPDFixed";
                        $scope.exTpdCoverType = "TPDFixed";
                  }
            }
           
            if ($scope.ipCoverDetails && $scope.ipCoverDetails.type) {
              showhide('dollar3','nodollar3');
            }
            if($scope.ipCoverDetails && $scope.ipCoverDetails.waitingPeriod && $scope.ipCoverDetails.waitingPeriod != ''){
                  $scope.waitingPeriodAddnl = $scope.ipCoverDetails.waitingPeriod;
            } else {
                  $scope.waitingPeriodAddnl = '30 Days';
            }
           
            if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitPeriod && $scope.ipCoverDetails.benefitPeriod != ''){
                  $scope.benefitPeriodAddnl = $scope.ipCoverDetails.benefitPeriod;
            } else{
                  $scope.benefitPeriodAddnl = '2 Years';
            }
 
            var CoverCalculatorFormFields =['coverName','coverType','requireCover','tpdCoverName','tpdCoverType','TPDRequireCover'];
            var inputDetails = persoanlDetailService.getMemberDetails();
            $scope.personalDetails = inputDetails[0].personalDetails;
           
            if(inputDetails[0] && inputDetails[0].contactDetails.emailAddress){
                  $scope.email = inputDetails[0].contactDetails.emailAddress;
            }
            if(inputDetails[0] && inputDetails[0].contactDetails.fundEmailAddress){
                  $scope.fundemail = inputDetails[0].contactDetails.fundEmailAddress;
            }
            if(inputDetails[0] && inputDetails[0].contactDetails.prefContactTime){
                  if(inputDetails[0].contactDetails.prefContactTime == "1"){
                        $scope.time= "Morning (9am - 12pm)";
                  }else{
                        $scope.time= "Afternoon (12pm - 6pm)";
                  }
            }
         
            if(inputDetails[0] && inputDetails[0].contactDetails.prefContact){
                  if(inputDetails[0].contactDetails.prefContact == "1"){
                        $scope.preferredContactType= "Mobile";
                        $scope.changeCvrPhone = inputDetails[0].contactDetails.mobilePhone;
                  }else if(inputDetails[0].contactDetails.prefContact == "2"){
                        $scope.preferredContactType= "Home";
                        $scope.changeCvrPhone = inputDetails[0].contactDetails.homePhone;
                  }else if(inputDetails[0].contactDetails.prefContact == "3"){
                        $scope.preferredContactType= "Work";
                        $scope.changeCvrPhone = inputDetails[0].contactDetails.workPhone;
                  }
         }
 
            $scope.setCoverCalculatorOptions = function(){
            $scope.DeathCoverOptions = $scope.configDeathCoverOptions();
                  $scope.TPDCoverOptions = $scope.configTpdOptions();
                  $scope.IPCoverOptions = $scope.configIpCoverOptions();
            };
 
            $scope.configDeathCoverOptions = function() {
                  var array = [];
                  if($scope.coverType == "DcUnitised") {
                        array = angular.copy($scope.DeathCoverOptionsOne);
                        $scope.requireCover = $routeParams.mode == 1 ? $scope.deathCoverDetails.units : $scope.requireCover;
                  } else if($scope.coverType == "DcFixed") {
                        array = angular.copy($scope.DeathCoverOptionsOne);
                        array.splice(3, 1);
                        $scope.requireCover = $routeParams.mode == 1 ? parseInt($scope.deathCoverDetails.amount) : $scope.requireCover;
                  }
 
                  if($scope.deathCoverDetails && $scope.deathCoverDetails.amount == 0) {
                        var tempArray = [];
                        angular.forEach(array, function (cover) {
                        if(cover.showAlways) {
                          tempArray.push(cover);
                        }
                    });
                    //$scope.tpdCoverName = 'No change';
                    array = tempArray;
                  }
                  $scope.coverName = $scope.coverName || 'No change';
                  return array;
            };
 
            $scope.configTpdOptions = function() {
                  var array = [];
                  if ($scope.coverName != 'Convert and maintain cover' && $scope.TPDRequireCover != 0) {
                array = angular.copy($scope.TPDCoverOptionsOne);
                $scope.tpdCoverName = $scope.tpdCoverName == 'Convert and maintain cover' ? 'No change' : $scope.tpdCoverName;
            } else if($scope.coverName == 'Convert and maintain cover') {
                  array.push({
                        key: 'option4',
                        coverOptionName: 'Convert and maintain cover'
                  });
                  $scope.tpdCoverName = 'Convert and maintain cover';
                  $scope.isTPDCoverNameDisabled = true;
            } else {
                  angular.forEach($scope.TPDCoverOptionsOne, function (cover) {
                        if(cover.showAlways) {
                          array.push(cover);
                        }
                    });
                    $scope.tpdCoverName = 'No change';
            }
            $scope.tpdCoverName = $scope.tpdCoverName || 'No change';
 
            if($scope.tpdCoverType == "TPDUnitised") {
                  if($scope.tpdCoverName == 'No change') {
                        $scope.TPDRequireCover =  $routeParams.mode == 1 ? parseInt($scope.tpdCoverDetails.units) : $scope.TPDRequireCover;
                  }
            } else if($scope.tpdCoverType == "TPDFixed") {
                  if($scope.tpdCoverName == 'No change') {
                        $scope.TPDRequireCover =  $routeParams.mode == 1 ? parseInt($scope.tpdCoverDetails.amount): $scope.TPDRequireCover;
                  }
                  array.splice(3, 1);
            }
            //$scope.checkTPDOption();
              return array;
            };
 
           
 
            $scope.configIpCoverOptions = function() {
                  var array = [];
                  if (parseInt($scope.ipCoverDetails.amount) != 0) {
                array = $scope.IPCoverOptionsOne;
            } else {
                  angular.forEach($scope.IPCoverOptionsOne, function (cover) {
                        if(cover.showAlways) {
                          array.push(cover);
                        }
                    });
                    //$scope.ipCoverName = 'No change';
            }
            $scope.ipCoverName = $routeParams.mode != 1 && $scope.ipCoverName != 'Cancel your cover' ? $scope.ipCoverName || 'No change' : 'No change';
            /*$scope.IPRequireCover =  $routeParams.mode == 1 ? parseInt($scope.ipCoverDetails.units) : $scope.IPRequireCover;*/
            $scope.IPRequireCover =  $routeParams.mode == 1 ? parseInt($scope.ipCoverDetails.amount) : $scope.IPRequireCover;
            $scope.IPRequireCover = $scope.IPRequireCover || 0;
            // if($scope.ipCoverDetails.type == "1") {
            //    $scope.IPRequireCover =  $routeParams.mode == 1 ? parseInt($scope.ipCoverDetails.units) : $scope.IPRequireCover;
            // } else if($scope.ipCoverDetails.type == "2") {
            //    $scope.IPRequireCover =  $routeParams.mode == 1 ? parseInt($scope.ipCoverDetails.amount) : $scope.IPRequireCover;
            // }
            return array;
            };
 
            $scope.changePrefContactType = function(){
                  if($scope.preferredContactType == "Home"){
                        $scope.changeCvrPhone = inputDetails[0].contactDetails.homePhone;
                  }else if($scope.preferredContactType == "Work"){
                        $scope.changeCvrPhone = inputDetails[0].contactDetails.workPhone;
                  }else if($scope.preferredContactType == "Mobile"){
                        $scope.changeCvrPhone = inputDetails[0].contactDetails.mobilePhone;
                  }
            };
           
           
            while($scope.personalDetails){
                  if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
                        $scope.genderFlag =  false;
                        $scope.gender = '';
                        OccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzQuestion',*/'industry','occupation',
                                              'withinOfficeQuestion','tertiaryQuestion','managementRole','gender','annualSalary'];
                      OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation', 'otherOccupation',
                                                       'withinOfficeQuestion','tertiaryQuestion','managementRole','gender','annualSalary'];
                  } else{
                        $scope.genderFlag =  true;
                        $scope.gender = $scope.personalDetails.gender;
                        OccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzQuestion',*/'industry','occupation','withinOfficeQuestion',
                                              'tertiaryQuestion','managementRole','annualSalary'];
                      OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation', 'otherOccupation', 'withinOfficeQuestion',
                                                       'tertiaryQuestion','managementRole','annualSalary'];
                  }
                  break;
            }
            var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
            $scope.ageLimit=anb;
            if($scope.ageLimit > 69){
                  $('#deathsection').removeClass('active');
                  $('#tpdsection').removeClass('active');
                  $('#ipsection').removeClass('active');
                  $("#death").css("display", "none");
                  $("#tpd").css("display", "none");
                  $("#sc").css("display", "none");
                  $scope.isDeathDisabled = true;
                $scope.isTPDDisabled = true;
                $scope.isIPDisabled = true;
            }else if($scope.ageLimit > 64){
                  $('#tpdsection').removeClass('active');
                  $('#ipsection').removeClass('active');
                  $("#tpd").css("display", "none");
                  $("#sc").css("display", "none");
                  $scope.isTPDDisabled = true;
              $scope.isIPDisabled = true;
            }
           
            QuoteService.getList($scope.urlList.quoteUrl,"CARE").then(function(res){
                  $scope.IndustryOptions = res.data;
            }, function(err){
                  console.info("Error while fetching industry list " + JSON.stringify(err));
            });
           
            MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"CARE",inputDetails[0].memberType,"CCOVER").then(function(res){
                  var limits = res.data;
            DCMaxAmount = limits[0].deathMaxAmount;
            TPDMaxAmount = limits[0].tpdMaxAmount;
            //IPMaxAmount = limits[0].ipMaxAmount;
            ipCostMultiplier = limits[0].ipUnitCostMulitiplier;
            }, function(error){
                  console.info('Something went wrong while fetching limits ' + error);
            });
           
            // Added to fix defect 169682: Fixed is not displayed, if cover is passed in
            // fixed
            $scope.isEmpty = function(value){
                  return ((value == "" || value == null) || value == "0");
            };
 
            $scope.checkLimits = function(){
                  if(parseInt($scope.requireCover) > parseInt(DCMaxAmount)){
                        $scope.deathMaxFlag = true;
                  }
                  $scope.calculateOnChange();
            };
           
          $scope.getOccupations = function(){
            if(!$scope.industry){
                  $scope.industry = '';
            }
            OccupationService.getOccupationList($scope.urlList.occupationUrl, "CARE", $scope.industry).then(function(res){
                  $scope.OccupationList = res.data;
                  $scope.occupation = "";
                  $scope.calculateOnChange();
            }, function(err){
                  console.info("Error while fetching occupations " + JSON.stringify(err));
            });
          };
         
          /*$scope.getOtherOccupationAS = function(entered) {
                return $http.get('./occupation.json').then(function(response) {
                  $scope.occupationList=[];
             if(response.data.Other) {
                    for (var key in response.data.Other) {
                          var obj={};
                          obj.id=key;
                           obj.name=response.data.Other[key];
                        $scope.occupationList.push(obj.name);
                          
                    }
                  }
              return $filter('filter')($scope.occupationList, entered);
                }, function(err){
                  console.info("Error while fetching occupations " + JSON.stringify(err));
                });
               
              };*/
         
        //Death, TPD and IP Validations
          $scope.deathErrorFlag = false;
          $scope.tpdErrorFlag = false;
          $scope.ipErrorFlag = false;
          $scope.deathErrorMsg ="";
          $scope.tpdErrorMsg ="";
          $scope.ipErrorMsg ="";
                      
                      
      $scope.validateDeathTpdIpAmounts = function(){
            if($scope.coverType == "DcFixed"){
                  if($scope.requireCover > TPDMaxAmount){
                        $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                              return obj.coverOptionName != 'Same as Death Cover';
                        });
                  } else{
                        var tpdOption = $scope.TPDCoverOptionsOne.some(function(obj){
                              return obj.coverOptionName == 'Same as Death Cover';
                        });
                        if(!tpdOption){
                              $scope.TPDCoverOptionsOne.push({'key':'option6', 'coverOptionName':'Same as Death Cover'});
                        }
                  }
            }
            /*Death validations Starts*/
          if($scope.coverName == 'Increase your cover'){
              if($scope.coverType == "DcFixed"){
                  if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.amount) ){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="You cannot reduce your Death Only cover. Please re-enter your cover amount.";
                  }else if(parseInt($scope.deathCoverDetails.amount) == parseInt($scope.requireCover)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Existing and additional cover amounts are same.";
                  }else if(parseInt($scope.requireCover) > parseInt(DCMaxAmount)){
                           $scope.deathErrorFlag = true;
                           $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is  " + DCMaxAmount + ". Please re-enter your cover.";
                       }else if(parseInt($scope.requireCover) > parseInt($scope.deathCoverDetails.amount)){
                             $scope.deathErrorFlag = false;
                           $scope.deathErrorMsg="";
                       }
               }else if($scope.coverType == "DcUnitised"){
                  if(deathAmount < parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Your converted amount is " +deathAmount+ ".You cannot reduce your Death Only cover. Please re-enter your cover amount";
                  }else if(deathAmount == parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Existing and additional cover amounts are same.";
                  }else if(deathAmount > parseInt(DCMaxAmount)){
                        $scope.deathErrorFlag = true;
                           $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxAmount + ". Please re-enter your cover.";
                  }else if(deathAmount > parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                  }
             }
          }else if($scope.coverName == 'Decrease your cover'){
            if($scope.coverType == "DcFixed"){
                  if(parseInt($scope.requireCover) > parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Please enter your Death cover less than to your existing cover.";
                  }else if(parseInt($scope.deathCoverDetails.amount) == parseInt($scope.requireCover)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Existing and additional cover amounts are same.";
                  }else if(parseInt($scope.requireCover) < 10000){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Your death cover is less than eligibility limit, minimum allowed for this product is " + 10000 + ". Please re-enter your cover.";
                  }else if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                    }
              }else if($scope.coverType == "DcUnitised"){
                  if(deathAmount > parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Your converted amount is " +deathAmount+ ".Please enter your Death cover less than to your existing cover.";
                  }else if(deathAmount == parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Existing and additional cover amounts are same.";
                  }else if(parseInt($scope.requireCover) < 1){
                        $scope.deathErrorFlag = true;
                        $scope.deathErrorMsg ="Your death cover is less than eligibility limit, minimum allowed for this product is " + 1 + ". Please re-enter your cover.";
                  }else if(deathAmount > parseInt(DCMaxAmount)){
                        $scope.deathErrorFlag = true;
                           $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxAmount + ". Please re-enter your cover.";
                  }else if(deathAmount < parseInt($scope.deathCoverDetails.amount)){
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                  }
              }
         }
      /*Death validations Ends*/
           
       /*TPD validations Starts*/
          if($scope.tpdCoverName == 'Increase your cover'){
              if($scope.tpdCoverType == "TPDFixed"){
                  if($scope.TPDRequireCover != null && $scope.requireCover == null ){
                        $scope.tpdErrorFlag = true;
                              $scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
               }
                  else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.amount) ){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
                  }else if(parseInt($scope.tpdCoverDetails.amount) == parseInt($scope.TPDRequireCover)){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
                  }else if(parseInt($scope.TPDRequireCover) > parseInt(TPDMaxAmount)){
                           $scope.tpdErrorFlag = true;
                           $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
                       }/*else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
                           $scope.tpdErrorFlag = true;
                           $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
                       }*/else if(parseInt($scope.TPDRequireCover) > parseInt($scope.tpdCoverDetails.amount)){
                             $scope.tpdErrorFlag = false; 
                            $scope.tpdErrorMsg="";
                       }
               }else if($scope.tpdCoverType == "TPDUnitised"){
                      if($scope.TPDRequireCover != null && $scope.requireCover == null ){
                              $scope.tpdErrorFlag = true;
                                    $scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
                     }
                      else if(TPDAmount < parseInt($scope.tpdCoverDetails.amount)){
                              $scope.tpdErrorFlag = true;
                              $scope.tpdErrorMsg ="Your converted amount is " +TPDAmount+ ".You cannot reduce your TPD Only cover. Please re-enter your cover amount";
                        }else if(TPDAmount == parseInt($scope.tpdCoverDetails.amount)){
                              $scope.tpdErrorFlag = true;
                              $scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
                        }else if(TPDAmount > TPDMaxAmount){
                                 $scope.tpdErrorFlag = true;
                                 $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
                             }/*else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
                                 $scope.tpdErrorFlag = true;
                                 $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
                             }*/else if(TPDAmount > parseInt($scope.tpdCoverDetails.amount)){
                              $scope.tpdErrorFlag = false;
                              $scope.tpdErrorMsg ="";
                        }
             }
          }else if($scope.tpdCoverName == 'Decrease your cover'){
            if($scope.tpdCoverType == "TPDFixed"){
                  if($scope.TPDRequireCover != null && $scope.requireCover == null ){
                        $scope.tpdErrorFlag = true;
                              $scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
                }else if(parseInt($scope.TPDRequireCover) < 10000){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Your TPD cover is less than eligibility limit, minimum allowed for this product is " + 10000 + ". Please re-enter your cover.";
                  }else if(parseInt($scope.TPDRequireCover) > parseInt($scope.tpdCoverDetails.amount)){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Please enter your TPD cover less than to your existing cover.";
                  }else if(parseInt($scope.tpdCoverDetails.amount) == parseInt($scope.TPDRequireCover)){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
                  }/*else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
                           $scope.tpdErrorFlag = true;
                           $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
                       }*/else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.amount)){
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                    }
              }else if($scope.tpdCoverType == "TPDUnitised"){
                  if($scope.TPDRequireCover != null && $scope.requireCover == null ){
                        $scope.tpdErrorFlag = true;
                              $scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
              }else if(parseInt($scope.TPDRequireCover) < 1){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Your TPD cover is less than eligibility limit, minimum allowed for this product is " + 1 + ". Please re-enter your cover.";
                  }else if(TPDAmount > parseInt($scope.tpdCoverDetails.amount)){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Your converted amount is " +TPDAmount+ ".Please enter your TPD cover less than to your existing cover.";
                  }else if(TPDAmount == parseInt($scope.tpdCoverDetails.amount)){
                        $scope.tpdErrorFlag = true;
                        $scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
                  }else if(TPDAmount > TPDMaxAmount){
                           $scope.tpdErrorFlag = true;
                           $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
                       }/*else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
                           $scope.tpdErrorFlag = true;
                           $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
                       }*/else if(TPDAmount < parseInt($scope.tpdCoverDetails.amount)){
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                  }
              }
         }
      /*TPD validations Ends*/
               
   /*IP validations Starts*/
    if($scope.ipCoverName == 'Increase your cover'){
      //if(parseInt($scope.IPRequireCover) < parseInt($scope.ipCoverDetails.units)){
      if(parseInt($scope.ipConvertedAmt) < parseInt($scope.ipCoverDetails.amount)){
            $scope.ipErrorFlag = true;
            $scope.ipErrorMsg="You cannot reduce your IP Only cover. Please re-enter your cover amount";
      /*}else if(parseInt($scope.IPRequireCover) == parseInt($scope.ipCoverDetails.units)){*/
      }else if(parseInt($scope.ipConvertedAmt) == parseInt($scope.ipCoverDetails.amount)){
            $scope.ipErrorFlag = true;
            $scope.ipErrorMsg="Existing and additional cover amounts are same.";
      }else if(parseInt($scope.ipConvertedAmt/ipCostMultiplier) > IPAmount){
            $scope.IPRequireCover = IPAmount;
               $scope.ipErrorFlag = false;
               $scope.ipErrorMsg="";
               $scope.ipWarningFlag = true;
      /*}else if(parseInt($scope.IPRequireCover) > parseInt($scope.ipCoverDetails.units)){*/
      }else if(parseInt($scope.ipConvertedAmt) > parseInt($scope.ipCoverDetails.amount)){
            $scope.ipErrorFlag = false;
            $scope.ipErrorMsg="";
      }
    }else if($scope.ipCoverName == 'Decrease your cover'){
      /*if(parseInt($scope.IPRequireCover) > $scope.ipCoverDetails.units){*/
      if(parseInt($scope.ipConvertedAmt) > $scope.ipCoverDetails.amount){
            $scope.ipErrorFlag = true;
            $scope.ipErrorMsg="Please enter your IP cover less than to your existing cover.";
      /*}else if(parseInt($scope.IPRequireCover) == $scope.ipCoverDetails.units){*/
      }else if(parseInt($scope.ipConvertedAmt) == $scope.ipCoverDetails.amount){
            $scope.ipErrorFlag = true;
            $scope.ipErrorMsg="Existing and additional cover amounts are same.";
      /*}else if(parseInt($scope.IPRequireCover) < parseInt($scope.ipCoverDetails.units)){*/
      }else if(parseInt($scope.ipConvertedAmt/ipCostMultiplier) > IPAmount)
            {
            $scope.IPRequireCover = IPAmount;
               $scope.ipErrorFlag = false;
               $scope.ipErrorMsg="";
               $scope.ipWarningFlag = true;
            }
      else if(parseInt($scope.ipConvertedAmt) < parseInt($scope.ipCoverDetails.amount)){
            $scope.ipErrorFlag = false;
            $scope.ipErrorMsg="";
      }
    }
    /*IP validations Ends*/
            $scope.calculateOnChange();
};
         
                
          $scope.navigateToLandingPage = function (){
            /*if(window.confirm('Are you sure you want to navigate to Home Page?')){
                  $location.path("/landing");
            }*/
            ngDialog.openConfirm({
                  template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
                  plain: true,
                  className: 'ngdialog-theme-plain custom-width'
              }).then(function(){
                  $location.path("/landing");
              }, function(e){
                  if(e=='oncancel'){
                        return false;
                  }
              });
          }
           
          $scope.syncRadios = function(val){
            if($scope.coverName == 'No change') {
                  $scope.requireCover = val == 'Unitised' ? $scope.deathCoverDetails.units : $scope.deathCoverDetails.amount;
                  //$scope.checkDeathOption();
            }else{
            	$scope.requireCover = 0;
            }
 
            if($scope.tpdCoverName == 'No change') {
                  $scope.TPDRequireCover = val == 'Unitised' ? $scope.tpdCoverDetails.units : $scope.tpdCoverDetails.amount;
                  //$scope.checkTPDOption();
            }else{
            	$scope.TPDRequireCover = 0;
            }
 
            /*$scope.requireCover = $scope.requireCover || 0;
            $scope.TPDRequireCover = $scope.TPDRequireCover || 0;*/
           
            var radios = $('label[radio-sync]');
            var data = $('input[data-sync]');
                  data.filter('[data-sync="' + val + '"]').attr('checked','checked');
                  if(val == 'Fixed'){
                        $scope.deathErrorFlag = false;
                     $scope.deathErrorMsg="";
                     $scope.tpdErrorFlag = false;
                  $scope.tpdErrorMsg ="";
                        showhide('dollar1','nodollar1');
                        showhide('dollar','nodollar');
                        $scope.coverType = 'DcFixed';
                        $scope.tpdCoverType = 'TPDFixed';
                  } else if(val == 'Unitised'){
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg="";
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                        showhide('nodollar1','dollar1');
                        showhide('nodollar','dollar');
                        $scope.coverType = 'DcUnitised';
                        $scope.tpdCoverType = 'TPDUnitised';
                  }
                  radios.removeClass('active');
                  radios.filter('[radio-sync="' + val + '"]').addClass('active');
                  /* Commenting existing code to fix dropdown - start*/
                  // if($scope.coverType != $scope.exDcCoverType){
                  //    var found = false;
                  //    for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
                  //        if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'Convert and maintain cover') {
                  //            found = true;
                  //            break;
                  //        }
                  //    }
                  //    if(!found){
                  //          $scope.TPDCoverOptionsOne.push({key: 'option4',coverOptionName: 'Convert and maintain cover'});
                  //    }
                  //    $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                  //          return obj.coverOptionName !== 'No change';
                  //    });
                  // } else{
                  //    var nochangeExists = false;
                  //    for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
                  //        if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'No change') {
                  //          nochangeExists = true;
                  //            break;
                  //        }
                  //    }
                  //    if(!nochangeExists){
                  //          $scope.TPDCoverOptionsOne.push({key: 'option4',coverOptionName: 'No change'});
                  //    }
                  //    $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                  //          return obj.coverOptionName !== 'Convert and maintain cover';
                  //    });
                  // }
                  /* Commenting existing code to fix dropdown - end*/
                  $scope.calculateOnChange();
          };
         
          $scope.convertDeathUnitsToAmount = function(){
            if($scope.requireCover && $scope.requireCover != ''){
                  if($scope.tpdCoverName == 'Same as Death Cover')
                        $scope.TPDRequireCover = $scope.requireCover;
                  unitisedDeathInitialFlag = true;
            }
            $scope.getNewOccupation();
            var deathReqObject = {
                        "age": anb,
                        "fundCode": "CARE",
                        "gender": $scope.gender,
                        "deathOccCategory": $scope.deathOccupationCategory,
                        "smoker": false,
                        "deathUnits": parseInt($scope.requireCover),
                        "deathUnitsCost": null,
                        "premiumFrequency": "Weekly",
                        "memberType": null,
                        "manageType": "CCOVER",
                        "deathCoverType": $scope.coverType
                  };
            if(unitisedDeathInitialFlag){
                  CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
                        deathAmount = res.data[0].coverAmount;
                        $scope.validateDeathTpdIpAmounts();
                  }, function(err){
                        console.info('Error while fetching death amount ' + err);
                  });
            }
           
            if(tpdUnitFlag){
                  var unitTPDReqObj = {
                              "age": anb,
                              "fundCode": "CARE",
                              "gender": $scope.gender,
                              "tpdOccCategory": $scope.tpdOccupationCategory,
                              "smoker": false,
                              "tpdUnits": 1,
                              "tpdUnitsCost": null,
                              "premiumFrequency": "Weekly",
                              "memberType": null,
                              "manageType": "CCOVER",
                              "tpdCoverType": "TPDUnitised",
                        };
                      CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,unitTPDReqObj).then(function(res){
                        tpdUnitFlag = false;
                        tpdUnitSuccess = true;
                        var TPDUnitAmount = res.data[0].coverAmount;
                        maxTPDUnits = Math.floor(TPDMaxAmount/TPDUnitAmount);
                  }, function(err){
                        console.info('Error while fetching TPD amount ' + err);
                  });
            }
           
            if(tpdUnitSuccess){
                  if($scope.requireCover > maxTPDUnits){
                        $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                              return obj.coverOptionName != 'Same as Death Cover';
                        });
                  } else{
                        var optionExists = $scope.TPDCoverOptionsOne.some(function(obj){
                              return obj.coverOptionName == 'Same as Death Cover';
                        });
                        if(!optionExists){
                              $scope.TPDCoverOptionsOne.push({'key':'option6', 'coverOptionName':'Same as Death Cover'});
                        }
                  }
            }
          };
 
          $scope.convertTPDUnitsToAmount = function(){
            if($scope.TPDRequireCover && $scope.TPDRequireCover != ''){
                  unitisedTpdInitialFlag = true;
            }
            $scope.getNewOccupation();
            var TPDReqObject = {
                        "age": anb,
                        "fundCode": "CARE",
                        "gender": $scope.gender,
                        "tpdOccCategory": $scope.tpdOccupationCategory,
                        "smoker": false,
                        "tpdUnits": parseInt($scope.TPDRequireCover),
                        "tpdUnitsCost": null,
                        "premiumFrequency": "Weekly",
                        "memberType": null,
                        "manageType": "CCOVER",
                        "tpdCoverType": $scope.tpdCoverType,
                  };
                CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,TPDReqObject).then(function(res){
                  TPDAmount = res.data[0].coverAmount;
                  if(unitisedTpdInitialFlag){
                        $scope.validateDeathTpdIpAmounts();
                  }
            }, function(err){
                  console.info('Error while fetching TPD amount ' + err);
            });
          };
         
          $scope.checkIpCover = function(){
            $scope.ipWarningFlag = false;
                  if(parseInt($scope.annualSalary) <= 423530){
                        IPAmount  = Math.ceil((0.85 * ($scope.annualSalary/12)) / 425);
                  } else if(parseInt($scope.annualSalary) > 423530 && parseInt($scope.annualSalary) <= 623530){
                        IPAmount = Math.ceil((Math.round(((parseInt($scope.annualSalary) - 423530)/12)*0.6) + 30000)/425);
                  } else if(parseInt($scope.annualSalary) > 623530){
                        IPAmount = 95;
                  }
                  $scope.unitToFixedIp();
                  $scope.validateDeathTpdIpAmounts();
          };
         
          $scope.setIndexation = function ($event) {
            $event.stopPropagation();
            $event.preventDefault();
            $scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
            if(!$scope.indexation.death) {
                  $("#changecvr_index-dth").parent().removeClass('active');
                  $("#changeCvr-indextpd-disable").parent().removeClass('active');
            }
          };
         
          $scope.deathDecOrCancelFlag=false;
          $scope.tpdDecOrCancelFlag=false;
          $scope.ipDecOrCancelFlag=false;
         
            $scope.checkDeathOption = function () {
                  $scope.isDCIndexationDisabled = false;
                  if($scope.coverName == 'Cancel your cover'){
                        $scope.dcIncreaseFlag = false;
                        $scope.tpdIncreaseFlag = false;
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                        $scope.deathDecOrCancelFlag=true;
                        /*$scope.tpdDecOrCancelFlag=true;*/
                        $scope.isDCCoverRequiredDisabled = true;
                        $scope.isDCCoverTypeDisabled = true;
                        //$scope.isTPDCoverRequiredDisabled = true;
                        //$scope.isTPDCoverTypeDisabled = true;
                        //$scope.isTPDCoverNameDisabled = true;
                              $scope.requireCover = 0;
                              //$scope.TPDRequireCover = 0;
                        //$scope.tpdCoverName = 'Cancel your cover';
                        // if(!mode3Flag){
                        //    $scope.requireCover = '';
                        //    $scope.TPDRequireCover = '';
                        // }
                        $scope.isDCIndexationDisabled = true;
                  } else if($scope.coverName == 'Increase your cover'){
                        $scope.dcIncreaseFlag = true;
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                        $scope.deathDecOrCancelFlag=false;
                        $scope.isDCCoverRequiredDisabled = false;
                        $scope.isDCCoverTypeDisabled = $scope.deathCoverDetails.type == 2 ? true : false;
                        if($scope.isTPDCoverRequiredDisabled != true){
                              $scope.isTPDCoverRequiredDisabled = false;
                        }
                        if($scope.isTPDCoverTypeDisabled != true){
                              $scope.isTPDCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.requireCover = 0;
                        // if(!mode3Flag){
                        //    $scope.requireCover = '';
                        // }
                  }else if( $scope.coverName == 'Decrease your cover'){
                        $scope.dcIncreaseFlag = false;
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                        $scope.deathDecOrCancelFlag=true;
                        $scope.isDCCoverRequiredDisabled = false;
                        $scope.isDCCoverTypeDisabled = $scope.deathCoverDetails.type == 2 ? true : false;
                        if($scope.isTPDCoverRequiredDisabled != true){
                              $scope.isTPDCoverRequiredDisabled = false;
                        }
                        if($scope.isTPDCoverTypeDisabled != true){
                              $scope.isTPDCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.requireCover = 0;
                        // if(!mode3Flag){
                        //    $scope.requireCover = '';
                        // }
 
                  } else if($scope.coverName == 'No change'){
                        $scope.dcIncreaseFlag = false;
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                        $scope.deathDecOrCancelFlag=false;
                        $scope.isDCCoverRequiredDisabled = true;
                        $scope.isDCCoverTypeDisabled = true;
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.isTPDCoverTypeDisabled = true;
                        if($scope.isTPDCoverRequiredDisabled != true){
                              $scope.isTPDCoverRequiredDisabled = false;
                        }
                        //$scope.coverType = $scope.exDcCoverType;
                        //$scope.tpdCoverType = $scope.exTpdCoverType;
                        $scope.coverType = $scope.exDcCoverType;
                       $scope.tpdCoverType = $scope.exTpdCoverType;
                        // if(!mode3Flag){
                        //    $scope.TPDRequireCover = "";
                        //    $scope.coverType = $scope.exDcCoverType;
                        // }
                        if($scope.coverType == "DcUnitised"){
                              $scope.requireCover = parseInt($scope.deathCoverDetails.units);
                              showhide('nodollar1','dollar1');
                       showhide('nodollar','dollar');
                        } else if($scope.coverType == "DcFixed"){
                              $scope.requireCover = parseInt($scope.deathCoverDetails.amount);
                              showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
                        }
                        if($scope.tpdCoverType == "TPDUnitised"){
                            $scope.TPDRequireCover = parseInt($scope.tpdCoverDetails.units);
                            showhide('nodollar1','dollar1');
                    showhide('nodollar','dollar');
                      } else if($scope.tpdCoverType == "TPDFixed"){
                            $scope.TPDRequireCover = parseInt($scope.tpdCoverDetails.amount);
                            showhide('dollar1','nodollar1');
                  showhide('dollar','nodollar');
                      }
                        $scope.isDCIndexationDisabled = true;
                  }else if($scope.coverName == 'Convert and maintain cover'){
                        $scope.dcIncreaseFlag = false;
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg ="";
                if($scope.deathCoverDetails.type == "1"){
                    $scope.coverType = "DcFixed";
                    $scope.tpdCoverType = "TPDFixed";
                    showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
                      } else if($scope.deathCoverDetails.type == "2"){
                    $scope.coverType = "DcUnitised";
                    $scope.tpdCoverType = 'TPDUnitised';
                    showhide('nodollar1','dollar1');
                    showhide('nodollar','dollar');
                      }
                $scope.deathDecOrCancelFlag=false;
                      $scope.isDCCoverRequiredDisabled = true;
                      $scope.isDCCoverTypeDisabled = true;
                      $scope.isTPDCoverRequiredDisabled = true;
                      $scope.isTPDCoverTypeDisabled = true;
                      // if(!mode3Flag){
                      //      $scope.requireCover = '';
                      // }
                      $scope.isTPDCoverNameDisabled = false;
                      /* Commenting existing code to fix dropdown - start*/
                  //    if($scope.coverType != $scope.exDcCoverType){
                        //    var found = false;
                        //    $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                        //          return obj.coverOptionName !== 'No change';
                        //    });
                        //    for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
                        //        if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'Convert and maintain cover') {
                        //            found = true;
                        //            break;
                        //        }
                        //    }
                        //    if(!found){
                        //          $scope.TPDCoverOptionsOne.push({key: 'option4',coverOptionName: 'Convert and maintain cover'});
                        //    }
                        // } else{
                        //    $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                        //          return obj.coverOptionName !== 'Convert and maintain cover';
                        //    });
                        // }
 
                        /* Commenting existing code to fix dropdown - ends*/
//                    $scope.TPDRequireCover = '';
 
                        $scope.convertDeathCover();
                  $scope.convertTPDCover();
                  $scope.isDCIndexationDisabled = true;
                  } else if($scope.coverName == undefined || $scope.coverName == ''){
                        $scope.deathErrorFlag = false;
                        $scope.deathErrorMsg = "";
                        $scope.deathDecOrCancelFlag = false;
                        $scope.tpdDecOrCancelFlag = false;
                        $scope.isDCCoverRequiredDisabled = true;
                        $scope.isDCCoverTypeDisabled = true;
                        $scope.isTPDCoverRequiredDisabled = true;
                        $scope.isTPDCoverTypeDisabled = true;
                        $scope.isTPDCoverNameDisabled = true;
                        $scope.requireCover = '';
                        $scope.TPDRequireCover = '';
                        $scope.dcIncreaseFlag = false;
                  }
                 
                  /* Commenting existing code to fix dropdown - start*/
 
                  // if($scope.tpdCoverName == "Convert and maintain cover"){
                  //    var nochangeFound = false;
                  //    $scope.tpdCoverName = "";
                  //    $scope.TPDRequireCover = "";
                  //    $scope.isTPDCoverNameDisabled = false;
                  //    $scope.isTPDCoverTypeDisabled = true;
                  //    $scope.isTPDCoverRequiredDisabled = true;
                       
                  //    $scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
                  //          return obj.coverOptionName !== 'Convert and maintain cover';
                  //    });
                       
                  //    if($scope.coverType == $scope.exDcCoverType){
                  //          for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
                  //              if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'No change') {
                  //                nochangeFound = true;
                  //                  break;
                  //              }
                  //          }
                  //          if(!nochangeFound){
                  //                $scope.TPDCoverOptionsOne.push({key: 'option5',coverOptionName: 'No change'});
                  //          }
                  //    }
                  // }
 
                  /* Commenting existing code to fix dropdown - end*/
                 
            //    $scope.decOrCancelCovers();
                  if($scope.tpdCoverName == 'Same as Death Cover') {
                  $scope.TPDRequireCover = $scope.requireCover;
                  $scope.isDCIndexationDisabled = true;
                  }
                  $scope.TPDCoverOptions = $scope.configTpdOptions();
                  $scope.calculateOnChange();
        }
           
            $scope.checkTPDOption = function () {
                  $scope.isTPDIndexationDisabled = false;
                  if($scope.tpdCoverName == 'Cancel your cover'){
                        $scope.tpdIncreaseFlag = false;
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                        $scope.tpdDecOrCancelFlag=true;
                        if($scope.isDCCoverRequiredDisabled != true){
                              $scope.isDCCoverRequiredDisabled = false;
                        }
                        if($scope.isDCCoverTypeDisabled != true){
                              $scope.isDCCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverRequiredDisabled = true;
                        $scope.isTPDCoverTypeDisabled = true;
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.TPDRequireCover = 0;
                        // if(!mode3Flag){
                        //    $scope.TPDRequireCover = '';
                        // }
                        $scope.isTPDIndexationDisabled = true;
                  } else if($scope.tpdCoverName == 'Increase your cover'){
                        $scope.tpdIncreaseFlag = true;
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                        $scope.tpdDecOrCancelFlag=false;
                        if($scope.isDCCoverRequiredDisabled != true){
                              $scope.isDCCoverRequiredDisabled = false;
                        }
                        if($scope.isDCCoverTypeDisabled != true){
                              $scope.isDCCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverRequiredDisabled = false;
                        $scope.isTPDCoverTypeDisabled = $scope.tpdCoverDetails.type == '2' ? true : false;
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.TPDRequireCover = 0;
                        // if(!mode3Flag){
                        //    $scope.TPDRequireCover = '';
                        // }
                  }else if($scope.tpdCoverName == 'Decrease your cover'){
                        $scope.tpdIncreaseFlag = false;
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                        $scope.tpdDecOrCancelFlag=true;
                        if($scope.isDCCoverRequiredDisabled != true){
                              $scope.isDCCoverRequiredDisabled = false;
                        }
                        if($scope.isDCCoverTypeDisabled != true){
                              $scope.isDCCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverRequiredDisabled = false;
                        $scope.isTPDCoverTypeDisabled = $scope.tpdCoverDetails.type == '2' ? true : false;
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.TPDRequireCover = 0;
                        // if(!mode3Flag){
                        //    $scope.TPDRequireCover = '';
                        // }
                  } else if($scope.tpdCoverName == 'No change'){
                        $scope.tpdIncreaseFlag = false;
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                        $scope.tpdDecOrCancelFlag=false;
                        if($scope.isDCCoverRequiredDisabled != true){
                              $scope.isDCCoverRequiredDisabled = false;
                        }
                        if($scope.isDCCoverTypeDisabled != true){
                              $scope.isDCCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverRequiredDisabled = true;
                        $scope.isTPDCoverTypeDisabled = true;
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.coverType = $scope.exDcCoverType;
                        $scope.tpdCoverType = $scope.exTpdCoverType;
                        // if(!mode3Flag){
                        //    $scope.tpdCoverType = $scope.exTpdCoverType;
                        // }
                        if($scope.tpdCoverType == "TPDUnitised"){
                              $scope.TPDRequireCover = parseInt($scope.tpdCoverDetails.units);
                              showhide('nodollar1','dollar1');
                      showhide('nodollar','dollar');
                        } else if($scope.tpdCoverType == "TPDFixed"){
                              $scope.TPDRequireCover = parseInt($scope.tpdCoverDetails.amount);
                              showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
                        }
                      if($scope.coverType == "DcUnitised"){
                            $scope.requireCover = parseInt($scope.deathCoverDetails.units);
                            showhide('nodollar1','dollar1');
                     showhide('nodollar','dollar');
                      } else if($scope.coverType == "DcFixed"){
                            $scope.requireCover = parseInt($scope.deathCoverDetails.amount);
                            showhide('dollar1','nodollar1');
                  showhide('dollar','nodollar');
                      }
                        $scope.isTPDIndexationDisabled = true;
                  } else if($scope.tpdCoverName == 'Same as Death Cover'){
                        $scope.tpdIncreaseFlag = false;
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                        $scope.tpdDecOrCancelFlag=false;
                        if($scope.isDCCoverRequiredDisabled != true){
                              $scope.isDCCoverRequiredDisabled = false;
                        }
                        if($scope.isDCCoverTypeDisabled != true){
                              $scope.isDCCoverTypeDisabled = false;
                        }
                        $scope.isTPDCoverRequiredDisabled = true;
                        $scope.isTPDCoverTypeDisabled = true;
                        $scope.isTPDCoverNameDisabled = false;
                        $scope.TPDRequireCover = $scope.requireCover;
                        // if(!mode3Flag){
                        //    $scope.TPDRequireCover = $scope.requireCover;
                        // }
                        $scope.isTPDIndexationDisabled = true;
                  }else if($scope.tpdCoverName == "Convert and maintain cover"){
                        $scope.tpdIncreaseFlag = false;
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg ="";
                if($scope.tpdCoverDetails.type == "1"){
                    $scope.coverType = "DcFixed";
                    $scope.tpdCoverType = "TPDFixed";
                    showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
                      } else if($scope.tpdCoverDetails.type == "2"){
                    $scope.coverType = "DcUnitised";
                    $scope.tpdCoverType = 'TPDUnitised';
                    showhide('nodollar1','dollar1');
                    showhide('nodollar','dollar');
                      }
                      $scope.coverName = 'Convert and maintain cover';
                      $scope.isTPDCoverNameDisabled = true;
                if($scope.coverName == "Convert and maintain cover"){
                  $scope.isDCCoverRequiredDisabled = true;
                }
                $scope.tpdDecOrCancelFlag=false;
                      $scope.isDCCoverTypeDisabled = true;
                      $scope.isTPDCoverRequiredDisabled = true;
                      $scope.isTPDCoverTypeDisabled = true;
                      // if(!mode3Flag){
                      //      $scope.TPDRequireCover = '';
                      // }
                      $scope.convertTPDCover();
                      $scope.convertDeathCover();
                      $scope.isTPDIndexationDisabled = true;
                  } else if($scope.tpdCoverName == undefined || $scope.tpdCoverName == ''){
                        $scope.tpdErrorFlag = false;
                        $scope.tpdErrorMsg = "";
                        $scope.tpdDecOrCancelFlag = false;
                        $scope.isDCCoverRequiredDisabled = true;
                        $scope.isDCCoverTypeDisabled = true;
                        $scope.isTPDCoverRequiredDisabled = true;
                        $scope.isTPDCoverTypeDisabled = true;
                        $scope.isTPDCoverNameDisabled = true;
                        $scope.TPDRequireCover = '';
                        $scope.tpdIncreaseFlag = false;
                  }
            //    $scope.decOrCancelCovers();
                  //$scope.TPDCoverOptions = $scope.configTpdOptions();
                  $scope.calculateOnChange();
        }
                                               
            $scope.checkIPOption = function (salflag) {
                  $scope.noconvert = false;
                  $scope.unitDisplay = false;
                  $scope.ipIncreaseFlagfinal = false;
                  if($scope.ipCoverName == 'Cancel your cover'){
                        $scope.ipIncreaseFlag = false;
                        $scope.ipErrorFlag = false;
                        $scope.ipErrorMsg="";
                        $scope.ipDecOrCancelFlag=true;
                        $scope.isWaitingPeriodDisabled = true;
                        $scope.isBenefitPeriodDisabled = true;
                        $scope.IPRequireCover = 0;
                        // if(!mode3Flag){
                        //    $scope.IPRequireCover = "";
                        // }
                        $scope.isIpSalaryCheckboxDisabled = true;
                        $scope.isIPCoverRequiredDisabled = true;
                        $scope.isIPCoverNameDisabled = false;
                        $scope.ipWarningFlag = false;
                        $scope.noconvert = true;
                        $scope.unitDisplay = true;
                        showhide('nodollar3','dollar3');
                        $('#ipsalarycheck').removeAttr('checked');
                        $('#ipsalarychecklabel').removeClass('active');
                        ipCheckboxState = false;
                  } else if($scope.ipCoverName == 'Increase your cover'){
                        $scope.ipIncreaseFlag = true;
                        $scope.ipIncreaseFlagfinal = true;
                        $scope.ipErrorFlag = false;
                        $scope.ipErrorMsg="";
                        $scope.ipDecOrCancelFlag=false;
                        $scope.isWaitingPeriodDisabled = false;
                        $scope.isBenefitPeriodDisabled = false;
                        $scope.isIPCoverRequiredDisabled = false;
                        $scope.isIpSalaryCheckboxDisabled = false;
                        $scope.isIPCoverNameDisabled = false;
                        $scope.ipWarningFlag = false;
                        $scope.IPRequireCover = 0;
                        $scope.unitDisplay = true;
                        showhide('nodollar3','dollar3');
                        // if(!mode3Flag){
                        //    $scope.IPRequireCover = '';
                        // }
                  } else if($scope.ipCoverName == 'Decrease your cover'){
                        $scope.ipIncreaseFlag = false;
                        $scope.ipErrorFlag = false;
                        $scope.ipErrorMsg="";
                        $scope.ipDecOrCancelFlag=true;
                        $scope.isWaitingPeriodDisabled = false;
                        $scope.isBenefitPeriodDisabled = false;
                        $scope.isIPCoverRequiredDisabled = false;
                        $scope.isIpSalaryCheckboxDisabled = true;
                        $scope.isIPCoverNameDisabled = false;
                        $scope.ipWarningFlag = false;
                        if (!salflag){
                        	$scope.IPRequireCover = 0;
                        }
                        /*$scope.IPRequireCover = 0;*/
                        $scope.unitDisplay = true;
                        showhide('nodollar3','dollar3');
                        // if(!mode3Flag){
                        //    $scope.IPRequireCover = '';
                        // }
                        $('#ipsalarycheck').removeAttr('checked');
                        $('#ipsalarychecklabel').removeClass('active');
                        ipCheckboxState = false;
                  } else if($scope.ipCoverName == 'No change'){
                        $scope.ipIncreaseFlag = false;
                        $scope.ipErrorFlag = false;
                        $scope.ipErrorMsg="";
                        $scope.ipDecOrCancelFlag=false;
                        $scope.isWaitingPeriodDisabled = true;
                        $scope.isBenefitPeriodDisabled = true;
                        /*$scope.IPRequireCover = $scope.ipCoverDetails.units;*/
                        $scope.IPRequireCover = $scope.ipCoverDetails.amount;
                        $scope.noconvert = true;
                        // if(!mode3Flag){
                        //    $scope.IPRequireCover = $scope.ipCoverDetails.units;
                        // }
                        // Checking for waiting period
                        if($scope.ipCoverDetails && $scope.ipCoverDetails.waitingPeriod && $scope.ipCoverDetails.waitingPeriod != ''){
                            $scope.waitingPeriodAddnl = $scope.ipCoverDetails.waitingPeriod;
                      } else {
                            $scope.waitingPeriodAddnl = '30 Days';
                      }
                        $scope.isIPCoverRequiredDisabled = true;
                        $scope.isIpSalaryCheckboxDisabled = true;
                        $scope.isIPCoverNameDisabled = false;
                        $scope.ipWarningFlag = false;
                        showhide('dollar3','nodollar3');
                        $('#ipsalarycheck').removeAttr('checked');
                        $('#ipsalarychecklabel').removeClass('active');
                        ipCheckboxState = false;
                  } else if($scope.ipCoverName == 'Change Waiting and Benefit Period'){
                        $scope.ipErrorFlag = false;
                        $scope.ipErrorMsg="";
                        $scope.ipDecOrCancelFlag=false;
                        $scope.isWaitingPeriodDisabled = false;
                        $scope.isBenefitPeriodDisabled = false;
                        $scope.IPRequireCover =  Math.ceil(parseInt($scope.ipCoverDetails.amount)/ipCostMultiplier);
                        // if(!mode3Flag){
                        //    $scope.IPRequireCover = $scope.ipCoverDetails.units;
                        // }
                        $scope.isIPCoverRequiredDisabled = true;
                        $scope.isIpSalaryCheckboxDisabled = true;
                        $scope.isIPCoverNameDisabled = false;
                        $scope.ipWarningFlag = false;
                        $scope.unitDisplay = true;
                        showhide('nodollar3','dollar3');
                        $('#ipsalarycheck').removeAttr('checked');
                        $('#ipsalarychecklabel').removeClass('active');
                        ipCheckboxState = false;
                  } else if($scope.ipCoverName == undefined || $scope.ipCoverName == ''){
                        $scope.ipIncreaseFlag = false;
                        $scope.isIPCoverNameDisabled = false;
                        $scope.isWaitingPeriodDisabled = true;
                        $scope.isBenefitPeriodDisabled = true;
                        $scope.isIPCoverRequiredDisabled = true;
                        $scope.isIpSalaryCheckboxDisabled = true;
                        $scope.IPRequireCover = 0;
                        $scope.noconvert = true;
                        showhide('dollar3','nodollar3');
                        $('#ipsalarycheck').removeAttr('checked');
                        $('#ipsalarychecklabel').removeClass('active');
                        ipCheckboxState = false;
                  }
                  ipUnchanged = false;
                  if(mode3Flag){
                        mode3Flag = false;
                        dynamicFlag = true;
                  }
            //    $scope.decOrCancelCovers();
                  $scope.calculateOnChange();
                  //$scope.$apply();
        }
           
            $scope.isBenifitPeriodIncresed = function() {
                  if(($scope.ipCoverDetails.benefitPeriod == "") && ($scope.benefitPeriodAddnl == '2 Years' || $scope.benefitPeriodAddnl == '5 Years')) {
                        return true;
                  } else if($scope.ipCoverDetails.benefitPeriod == '2 Years' && $scope.benefitPeriodAddnl == '5 Years') {
                        return true;
                  } else {
                        return false;
                  }
            }
           
            $scope.isWaitingPeriodIncresed = function() {
                  if(($scope.ipCoverDetails.waitingPeriod == "") && ($scope.waitingPeriodAddnl == '90 Days' || $scope.waitingPeriodAddnl == '60 Days' || $scope.waitingPeriodAddnl == '30 Days')) {
                        return true;
                  } else if($scope.ipCoverDetails.waitingPeriod == '90 Days' && ($scope.waitingPeriodAddnl == '60 Days' || $scope.waitingPeriodAddnl == '30 Days')) {
                        return true;
                  } else if($scope.ipCoverDetails.waitingPeriod == '60 Days' && $scope.waitingPeriodAddnl == '30 Days') {
                        return true;
                  } else {
                        return false;
                  }
            }
           
            $scope.checkBenefitPeriod = function(){
                  if($scope.ipIncreaseFlagfinal)
                        {
                        $scope.ipIncreaseFlag = true;
                        }else if($scope.ipCoverDetails.waitingPeriod == $scope.waitingPeriodAddnl && $scope.ipCoverDetails.benefitPeriod == $scope.benefitPeriodAddnl){
                        $scope.ipIncreaseFlag = false;
                        $scope.disclaimerFlag = true;
                  } else if($scope.isBenifitPeriodIncresed() || $scope.isWaitingPeriodIncresed()){
                        $scope.ipIncreaseFlag = true;
                        $scope.disclaimerFlag = true;
                  } else{
                        $scope.ipIncreaseFlag = false;
                        $scope.disclaimerFlag = false;
                  }
                  $scope.calculateOnChange();
            };
                 
          // added for Decrease or Cancel cover popup "on continue" after calculate quote
               $scope.showDecreaseOrCancelPopUp = function (val){
                        if(val == null || val == "" || val == " "){
                              hideTips();
                        }else{
                         ackCheck = $('#termsLabel').hasClass('active');
                        if(ackCheck){
                               $scope.ackFlag = false;
                                     document.getElementById('mymodalDecCancel').style.display = 'block';
                                     document.getElementById('mymodalDecCancelFade').style.display = 'block';
                                     document.getElementById('decCancelMsg_text').innerHTML=val;
                        }else{
                              $scope.ackFlag = true;
                        }
                       
                        }
                  //    $scope.saveDataForPersistence();
                  }
                $scope.hideTips = function  (){
                        if(document.getElementById('help_div')){
                              document.getElementById('help_div').style.display = "none";
                        }                                  
                  }
                 
            // added to check which covers are selected as Decrease/Cancel
                  $scope.decCancelCover="";
                  $scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
                  /*$scope.decOrCancelCovers = function(){
                        //Coomented this section as a part of Latest BRD requirement -- S73124 BRD V2.1
                        if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
                              $scope.decCancelCover = "Death, TPD & IP";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }else if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true){
                              $scope.decCancelCover = "Death & TPD";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }else if($scope.deathDecOrCancelFlag && $scope.ipDecOrCancelFlag){
                              $scope.decCancelCover = "Death & IP";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }else if($scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
                              $scope.decCancelCover = "TPD & IP";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }else if($scope.deathDecOrCancelFlag == true){
                              $scope.decCancelCover = "Death";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }else if($scope.tpdDecOrCancelFlag == true){
                              $scope.decCancelCover = "TPD";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }else if($scope.ipDecOrCancelFlag == true){
                              $scope.decCancelCover = "IP";
                              $scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
                        }
                        $scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
                  };*/
                  $scope.checkForAura= function(){
                        $scope.checkAuraDisable();
                        if($scope.auraDisabled == true){
                              $scope.continueToNextPage('/summary/1');
                        }else{
                              $scope.goToAura();
                        }
               }
                 
            $scope.checkAuraDisable = function(){
                  if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true && (!$scope.ipIncreaseFlag)){
                        $scope.auraDisabled = true;
                  }else{
                        $scope.auraDisabled = false;
                  }
            }
                 
            $scope.checkAnnualSalary = function(){
                  if((parseInt($scope.annualSalary) < 16000 || $scope.fifteenHrsQuestion == 'No') || anb > 64){
                        $scope.ipCoverName = 'No change';
                        $scope.isIPCoverNameDisabled = true;
                        $scope.isWaitingPeriodDisabled = true;
                        $scope.isBenefitPeriodDisabled = true;
                        $scope.IPRequireCover = $scope.ipCoverDetails.amount;
                        $scope.isIPCoverRequiredDisabled = true;
                        $scope.isIpSalaryCheckboxDisabled = true;
                        $('#ipsalarycheck').removeAttr('checked');
                        $('#ipsalarychecklabel').removeClass('active');
                        showhide('dollar3','nodollar3');
                        $scope.noconvert = true;
                        $scope.unitDisplay = false;
                        ipUnchanged = true;
                  } else {
                        // if($scope.ipCoverName == 'No change'){
                        //    $scope.ipCoverName = '';
                        // }
                        if(disableIpCover){
                              $scope.isIPCoverRequiredDisabled = true;
                        } else{
                              $scope.isIPCoverRequiredDisabled = false;
                        }
                        $scope.isIPCoverNameDisabled = false;
                        $scope.isWaitingPeriodDisabled = false;
                        $scope.isBenefitPeriodDisabled = false;
                        //$scope.isIPCoverRequiredDisabled = false;
                        $scope.isIpSalaryCheckboxDisabled = false;
                       
                        if(ipUnchanged){
                              $scope.isIPCoverNameDisabled = false;
                              $scope.isWaitingPeriodDisabled = true;
                              $scope.isBenefitPeriodDisabled = true;
                              $scope.isIPCoverRequiredDisabled = true;
                              $scope.isIpSalaryCheckboxDisabled = true;
                        }
                  }
                  //$scope.setCategory();
                  $scope.checkIpCover();
                  $scope.calculateOnChange();
                  $scope.getNewOccupation();
            };
                       
          $scope.go = function ( path ) {
              $location.path( path );
            };
           
            $scope.toggleIPCondition = function(check){
                  if(parseInt($scope.annualSalary) == 0){
                        $scope.invalidSalAmount = true;
                  } else{
                        $scope.invalidSalAmount = false;
                  }
                  $scope.ipWarningFlag = false;
                  $scope.ipErrorFlag = false;
                  $timeout(function(){
                        ipCheckboxState = $('#ipsalarycheck').prop('checked');
                        if(ipCheckboxState == true){
                              if(parseInt($scope.annualSalary) <= 423530){
                                    $scope.IPRequireCover  = Math.ceil((0.85 * ($scope.annualSalary/12)) / 425);
                              } else if(parseInt($scope.annualSalary) > 423530 && parseInt($scope.annualSalary) <= 623530){
                                    $scope.IPRequireCover = Math.ceil((Math.round(((parseInt($scope.annualSalary) - 423530)/12)*0.6) + 30000)/425);
                              } else if(parseInt($scope.annualSalary) > 623530){
                                    $scope.IPRequireCover = 95;
                              }
                              $scope.isIPCoverRequiredDisabled = true;
                              disableIpCover = true;
                        } else if(ipCheckboxState == false && check == false){
                              $scope.IPRequireCover = 0;
                              disableIpCover = false;
                        }
                        
                        $scope.checkAnnualSalary();
                        
                        if (check && !($scope.ipCoverName == 'Increase your cover')){
                        	$scope.checkIPOption(true);
                        }
                        
                        $scope.calculateOnChange();
                  //    $scope.checkMaxAge();
                  }, 10);
                  // $scope.checkAnnualSalary();
            };
           
            $scope.continueToNextPage = function(path){
                  $scope.saveDataForPersistence();
                  $location.path(path);
            };
                 
            $scope.getNewOccupation = function(){
                  if(($scope.coverName && $scope.coverName.toLowerCase() == 'increase your cover') ||
                              ($scope.tpdCoverName && $scope.tpdCoverName.toLowerCase() == 'increase your cover') ||
                              ($scope.ipCoverName && $scope.ipCoverName.toLowerCase() == 'increase your cover')){
                        if($scope.withinOfficeQuestion == "Yes"){
                              $scope.deathOccupationCategory = "Office";
                              if($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes"){
                                    if(parseInt($scope.annualSalary) > 100000){
                                          $scope.deathOccupationCategory = "Professional";
                                    }
                              } else if($scope.tertiaryQuestion == "No" && $scope.managementRole == "No"){
                                    $scope.deathOccupationCategory = "Office";
                              }
                        } else if($scope.withinOfficeQuestion == "No"){
                              $scope.deathOccupationCategory = "General";
                        }
                       
                        if($scope.withinOfficeQuestion == "Yes"){
                              $scope.tpdOccupationCategory = "Office";
                              if($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes"){
                                    if(parseInt($scope.annualSalary) > 100000){
                                          $scope.tpdOccupationCategory = "Professional";
                                    }
                              } else if($scope.tertiaryQuestion == "No" && $scope.managementRole == "No"){
                                    $scope.tpdOccupationCategory = "Office";
                              }
                        } else if($scope.withinOfficeQuestion == "No"){
                              $scope.tpdOccupationCategory = "General";
                        }
                       
                        if($scope.withinOfficeQuestion == "Yes"){
                              $scope.ipOccupationCategory = "Office";
                              if($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes"){
                                    if(parseInt($scope.annualSalary) > 100000){
                                          $scope.ipOccupationCategory = "Professional";
                                    }
                              } else if($scope.tertiaryQuestion == "No" && $scope.managementRole == "No"){
                                    $scope.ipOccupationCategory = "Office";
                              }
                        } else if($scope.withinOfficeQuestion == "No"){
                              $scope.ipOccupationCategory = "General";
                        }
                  } else /*if((($scope.coverName && $scope.coverName.toLowerCase() == 'decrease your cover') &&
                              ($scope.tpdCoverName && $scope.tpdCoverName.toLowerCase() == 'decrease your cover') &&
                              ($scope.ipCoverName && $scope.ipCoverName.toLowerCase() == 'decrease your cover')) ||
                              ($scope.coverName && $scope.coverName.toLowerCase() == 'decrease your cover' &&
                                          ($scope.tpdCoverName == undefined || $scope.tpdCoverName == '') &&
                                          ($scope.ipCoverName == undefined || $scope.ipCoverName == '')) ||
                                          (($scope.coverName && $scope.coverName.toLowerCase() == 'decrease your cover') &&
                                                      ($scope.tpdCoverName && $scope.tpdCoverName.toLowerCase() == 'decrease your cover') &&
                                                      ($scope.ipCoverName == undefined || $scope.ipCoverName == '')) ||
                                                      (($scope.ipCoverName && $scope.ipCoverName.toLowerCase() == 'decrease your cover') &&
                                                                  ($scope.coverName == undefined || $scope.coverName == '' || $scope.coverName.toLowerCase() =='cancel your cover' || $scope.coverName.toLowerCase() =='no change') &&
                                                                  ($scope.tpdCoverName == undefined || $scope.tpdCoverName == '' || $scope.tpdCoverName.toLowerCase() =='cancel your cover' || $scope.tpdCoverName.toLowerCase() =='no change')))*/{
                        if(/*$scope.existingDeathOccupationCategory != "Professional" &&*/ $scope.withinOfficeQuestion == "Yes"){
                              $scope.deathOccupationCategory = "Office";
                              if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
                                    $scope.deathOccupationCategory = "Professional";
                              }
                        } else if(/*$scope.existingDeathOccupationCategory != "Professional" &&*/ $scope.withinOfficeQuestion == "No"){
                              $scope.deathOccupationCategory = $scope.existingDeathOccupationCategory;
                        }
                       
                        if(/*$scope.existingTpdOccupationCategory != "Professional" &&*/ $scope.withinOfficeQuestion == "Yes"){
                              $scope.tpdOccupationCategory = "Office";
                              if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
                                    $scope.tpdOccupationCategory = "Professional";
                              }
                        } else if(/*$scope.existingTpdOccupationCategory != "Professional" &&*/ $scope.withinOfficeQuestion == "No"){
                              $scope.tpdOccupationCategory = $scope.existingTpdOccupationCategory;
                        }
                       
                        if(/*$scope.existingIpOccupationCategory != "Professional" &&*/ $scope.withinOfficeQuestion == "Yes"){
                              $scope.ipOccupationCategory = "Office";
                              if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
                                    $scope.ipOccupationCategory = "Professional";
                              }
                        } else if(/*$scope.existingIpOccupationCategory != "Professional" &&*/ $scope.withinOfficeQuestion == "No"){
                              $scope.ipOccupationCategory = $scope.existingIpOccupationCategory;
                        }
                  }
                 
                  //$scope.calculateOnChange();
            };
                 
            /*$scope.setCategory = function(){
                  var deathCat, tpdCat, ipCat;
                 
                  if(occupationCategory){
                        deathCat = occupationCategory[0].deathfixedcategeory;
                        tpdCat = occupationCategory[0].tpdfixedcategeory;
                        ipCat = occupationCategory[0].ipfixedcategeory;
                       
                        $scope.deathOccupationCategory = occupationCategory[0].deathfixedcategeory;
                        $scope.tpdOccupationCategory = occupationCategory[0].tpdfixedcategeory;
                        $scope.ipOccupationCategory = occupationCategory[0].ipfixedcategeory;
                  }
                  if(deathCat != "Professional" && $scope.withinOfficeQuestion == "Yes"){
                        $scope.deathOccupationCategory = "Office";
                        if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
                              $scope.deathOccupationCategory = "Professional";
                        }
                  }
                  if(tpdCat != "Professional" && $scope.withinOfficeQuestion == "Yes"){
                        $scope.tpdOccupationCategory = "Office";
                        if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
                              $scope.tpdOccupationCategory = "Professional";
                        }
                  }
                  if(ipCat != "Professional" && $scope.withinOfficeQuestion == "Yes"){
                        $scope.ipOccupationCategory = "Office";
                        if(parseInt($scope.annualSalary) > 100000 && ($scope.tertiaryQuestion == "Yes" || $scope.managementRole == "Yes")){
                              $scope.ipOccupationCategory = "Professional";
                        }
                  }
                  $scope.calculateOnChange();
            };*/
                 
            // $scope.toggleTwo = function() {
         //      $scope.coltwo = !$scope.coltwo;
         //      $("a[data-target='#collapseTwo']").click();
         //  };
                 
            // $scope.toggleThree = function() {
         //      $scope.colthree = !$scope.colthree;
         //      $("a[data-target='#collapseThree']").click()
         //  };
 
         /* Check if your is allowed to proceed to the next accordion */
            // TBC
            // Need to revisit, need better implementation
            $scope.isCollapsible = function(targetEle, event) {
                  if( targetEle == 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
                        event.stopPropagation();
                        return false;
                  } else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
                        event.stopPropagation();
                        return false;
                  }  else if( targetEle == 'collapseTwo' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
                        event.stopPropagation();
                        return false;
                  }  else if( targetEle == 'collapseThree' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
                        event.stopPropagation();
                        return false;
                  }
            }
 
            $scope.toggleTwo = function(checkFlag) {
              $scope.coltwo = checkFlag;
              if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
                  return false;
              $("a[data-target='#collapseTwo']").click(); /* Can be improved */
          };
                 
            $scope.toggleThree = function(checkFlag) {
              $scope.colthree = checkFlag;
              if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
                  return false;
              $("a[data-target='#collapseThree']").click(); /* Can be improved */
          };
         
          $scope.privacyCol = false;
          var dodCheck;
          var privacyCheck;
         
          /* TBC */
          $scope.togglePrivacy = function(checkFlag) {
              $scope.privacyCol = checkFlag;
              if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
                  return false;
              $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
          };
 
          $scope.toggleContact = function(checkFlag) {
              $scope.contactCol = checkFlag;
              if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
                  return false;
              $("a[data-target='#collapseOne']").click(); /* Can be improved */
             
          };
          $scope.checkDodState = function() {
            $timeout(function() {
                  $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
                  if($('#dodLabel').hasClass('active')) {
                        $scope.togglePrivacy(true);
                  } else {
                        $scope.togglePrivacy(false);
                        $scope.toggleContact(false);
                        $scope.toggleTwo(false);
                        $scope.toggleThree(false);
                  }
            }, 1);     
          };
         
          $scope.checkPrivacyState  = function() {
            $timeout(function() {
                  $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
                  if($('#privacyLabel').hasClass('active')) {
                        $scope.toggleContact(true);
                  } else {
                        $scope.toggleContact(false);
                        $scope.toggleTwo(false);
                        $scope.toggleThree(false);
                  }
            }, 1);
          };
                  
          $scope.calculate = function(){
            // if($scope.requireCover == undefined || $scope.requireCover == ""){
            //    $scope.requireCover = 0;
            // }
            // if($scope.TPDRequireCover == undefined || $scope.TPDRequireCover == ""){
            //    $scope.TPDRequireCover = 0;
            // }
            // if($scope.IPRequireCover == undefined || $scope.IPRequireCover == ""){
            //    $scope.IPRequireCover = 0;
            // }
            $scope.ipunitsOnchange = null;
            if(!($scope.ipCoverName && $scope.ipCoverName == 'No change'))
                  {
                  $scope.ipunitsOnchange = $scope.IPRequireCover;
                  }
           
            $scope.ipConvertedAmt = $scope.IPRequireCover;
            if(!$scope.noconvert)
                  {
                  $scope.unitToFixedIp();
                  }
           
            $scope.getNewOccupation();
            var ruleModel = {
                        "age": anb,
                        "fundCode": "CARE",
                        "gender": $scope.gender,
                        "deathOccCategory": $scope.deathOccupationCategory,
                        "tpdOccCategory": $scope.tpdOccupationCategory,
                        "ipOccCategory": $scope.ipOccupationCategory,
                        "smoker": false,
                        "deathUnits": parseInt($scope.requireCover || 0),
                        "deathFixedAmount": parseInt($scope.requireCover  || 0),
                        "deathFixedCost": null,
                        "deathUnitsCost": null,
                        "tpdUnits": parseInt($scope.TPDRequireCover  || 0),
                        "tpdFixedAmount": parseInt($scope.TPDRequireCover  || 0),
                        "tpdFixedCost": null,
                        "tpdUnitsCost": null,
                        "ipUnits": $scope.ipunitsOnchange,
                        "ipFixedAmount": parseInt($scope.ipConvertedAmt  || 0),
                        "ipFixedCost": null,
                        "ipUnitsCost": null,
                        "premiumFrequency": "Weekly",
                        "memberType": null,
                        "manageType": "CCOVER",
                        "deathCoverType": $scope.coverType,
                        "tpdCoverType": $scope.tpdCoverType,
                        "ipCoverType": "IpFixed",
                        "ipWaitingPeriod": $scope.waitingPeriodAddnl,
                        "ipBenefitPeriod": $scope.benefitPeriodAddnl,
                        "deathExistingAmount":parseInt($scope.deathCoverDetails.amount || 0),
                        "tpdExistingAmount":parseInt($scope.tpdCoverDetails.amount || 0)
                  };
                CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
                  var premium = res.data;
                  dynamicFlag = true;
                  for(var i = 0; i < premium.length; i++){
                        if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
                              $scope.dcCoverAmount = premium[i].coverAmount;
                              $scope.dcWeeklyCost = premium[i].cost;
                        } else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
                              $scope.tpdCoverAmount = premium[i].coverAmount;
                              $scope.tpdWeeklyCost = premium[i].cost;
                        } else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
                              $scope.ipCoverAmount = premium[i].coverAmount;
                              $scope.ipWeeklyCost = premium[i].cost;
                        }
                  }
                  $scope.totalWeeklyCost = $scope.dcWeeklyCost+ $scope.tpdWeeklyCost+$scope.ipWeeklyCost;
                  if(fetchAppnum){
                        fetchAppnum = false;
                        appNum = PersistenceService.getAppNumber();
                  }
            }, function(err){
                  console.info("Something went wrong while calculating..." + JSON.stringify(err));
            });
        };
       
          $scope.convertDeathCover = function(){
            $scope.getNewOccupation();
            var ruleModel = {
                        "age": anb,
                        "fundCode": "CARE",
                        "gender": $scope.gender,
                        "deathOccCategory": $scope.deathOccupationCategory,
                        "tpdOccCategory": $scope.tpdOccupationCategory,
                        "ipOccCategory": $scope.ipOccupationCategory,
                        "smoker": false,
                        "premiumFrequency": "Weekly",
                        "manageType": "CCOVER",
                        "exDeathCoverType": $scope.exDcCoverType,
                        "exTpdCoverType":null,
                        "deathExistingAmount":$scope.deathCoverDetails.amount,
                        "tpdExistingAmount":null
                  };
                ConvertService.convert($scope.urlList.convertCoverUrl,ruleModel).then(function(res){
            //ConvertService.convert({}, ruleModel, function(res){
                  var premium = res.data;
                  for(var i = 0; i < premium.length; i++){
                        $scope.requireCover = premium[i].convertedAmount;
                  }
            }, function(err){
                  console.info("Error while converting death amount " + JSON.stringify(err));
            });
        };
          $scope.convertTPDCover = function(){
            $scope.getNewOccupation();
            var ruleModel = {
                        "age": anb,
                        "fundCode": "CARE",
                        "gender": $scope.gender,
                        "deathOccCategory": $scope.deathOccupationCategory,
                        "tpdOccCategory": $scope.tpdOccupationCategory,
                        "ipOccCategory": $scope.ipOccupationCategory,
                        "smoker": false,
                        "premiumFrequency": "Weekly",
                        "manageType": "CCOVER",
                        "exDeathCoverType": null,
                        "exTpdCoverType":$scope.exTpdCoverType,
                        "deathExistingAmount":null,
                        "tpdExistingAmount":$scope.tpdCoverDetails.amount
                  };
                ConvertService.convert($scope.urlList.convertCoverUrl,ruleModel).then(function(res){
            //ConvertService.convert({}, ruleModel, function(res){
                  var premium = res.data;
                  for(var i = 0; i < premium.length; i++){
                        $scope.TPDRequireCover = premium[i].convertedAmount;
                  }
            }, function(err){
                  console.info("Error while converting TPD amount " + JSON.stringify(err));
            });
        };        
                    
       $scope.calculateOnChange = function(){
         if(dynamicFlag && !$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag ){
               $scope.calculate();
         }else{
               return false;
        }
       };
            
       // Progressive validation
          $scope.checkPreviousMandatoryFields  = function (elementName,formName){
            var formFields;
            if(formName == 'formOne'){
                  formFields = FormOneFields;
            } else if(formName == 'occupationForm'){
                  if($scope.occupation != undefined && $scope.occupation == 'Other'){
                        formFields = OccupationOtherFormFields;
                  } else{
                        formFields = OccupationFormFields;
                  }
            } else if(formName == 'coverCalculatorForm'){
                  formFields = CoverCalculatorFormFields;
            }
            var inx = formFields.indexOf(elementName);
            if(inx > 0){
              for(var i = 0; i < inx ; i++){
                $scope[formName][formFields[i]].$touched = true;
              }
            }
          };
             
         // validate fields "on continue"
          $scope.formOneSubmit =  function (form){
            if(form.$name == 'coverCalculatorForm') {
                  $scope.checkIpCover();
                  if($scope.exTpdCoverType == "TPDUnitised")
                        $scope.convertTPDUnitsToAmount();
 
                  if($scope.exDcCoverType == "DcUnitised")
                        $scope.convertDeathUnitsToAmount();
 
                  if($scope.exTpdCoverType == "TPDFixed" && $scope.exDcCoverType == "DcFixed")
                        $scope.validateDeathTpdIpAmounts();
            }
            if(!form.$valid){
              form.$submitted=true;
              if(form.$name == 'formOne'){
                          $scope.toggleTwo(false);
                          $scope.toggleThree(false);
                    }else if(form.$name == 'occupationForm'){
                          $scope.toggleThree(false);
                    }
              } else{
                        if(form.$name == 'formOne'){
                              $scope.toggleTwo(true);
                        }else if(form.$name == 'occupationForm'){
                              $scope.toggleThree(true);
                              $scope.setCoverCalculatorOptions();
                        }else if(form.$name == 'coverCalculatorForm'){
                             if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag){
                                   $scope.calculate();
                        }
                    }
             }
          };
         
          $scope.checkAckState = function(){
            $timeout(function(){
                  ackCheck = $('#termsLabel').hasClass('active');
                  if(ackCheck){
                        $scope.ackFlag = false;
                  } else{
                        $scope.ackFlag = true;
                  }
            }, 10);
          };
         
          $scope.showHelp = function(msg){
            $scope.modalShown = !$scope.modalShown;
            $scope.tipMsg = msg;
          };
                      
          $scope.saveDataForPersistence = function(){
           
            var coverObj = {};
            var coverStateObj = {};
            var changeCoverOccObj = {};
            var deathAddnlCoverObj={};
            var tpdAddnlCoverObj={};
            var ipAddnlCoverObj={};
            var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
                  return $scope.industry == obj.key;
            });
           
                       
                        coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
                  coverObj['dob'] = $scope.personalDetails.dateOfBirth;
                  coverObj['email'] = $scope.email;
                  changeCoverOccObj['gender'] = $scope.gender;
                  coverObj['contactType'] = $scope.preferredContactType;
                  coverObj['contactPhone'] = $scope.changeCvrPhone;
                  coverObj['contactPrefTime'] = $scope.time;
                 
                 
                  changeCoverOccObj['fifteenHr'] = $scope.fifteenHrsQuestion;
                  /*changeCoverOccObj['citizenQue'] = $scope.areyouperCitzQuestion;*/
                  changeCoverOccObj['industryName'] = selectedIndustry[0].value;
                  changeCoverOccObj['industryCode'] = selectedIndustry[0].key;
                  changeCoverOccObj['occupation'] = $scope.occupation;
                  changeCoverOccObj['withinOfficeQue']= $scope.withinOfficeQuestion;
                  changeCoverOccObj['tertiaryQue']= $scope.tertiaryQuestion;
                  changeCoverOccObj['managementRoleQue']= $scope.managementRole;
                  changeCoverOccObj['salary'] = $scope.annualSalary;
                  changeCoverOccObj['otherOccupation'] = $scope.otherOccupationObj.otherOccupation;
                 
                  coverObj['existingDeathAmt'] = parseFloat($scope.deathCoverDetails.amount);
                  coverObj['existingDeathUnits'] = $scope.deathCoverDetails.units;
                  coverObj['deathOccCategory'] = $scope.deathOccupationCategory;
                  deathAddnlCoverObj['deathCoverName'] = $scope.coverName;
                  deathAddnlCoverObj['deathCoverType'] = $scope.coverType;
                  deathAddnlCoverObj['deathFixedAmt'] = parseFloat($scope.dcCoverAmount);
                  deathAddnlCoverObj['deathInputTextValue'] = parseFloat($scope.requireCover);
                  deathAddnlCoverObj['deathCoverPremium'] = parseFloat($scope.dcWeeklyCost);
                     
                      coverObj['existingTpdAmt'] = parseFloat($scope.tpdCoverDetails.amount);
                      coverObj['existingTPDUnits'] = $scope.tpdCoverDetails.units;
                      coverObj['tpdOccCategory'] = $scope.tpdOccupationCategory;
                      tpdAddnlCoverObj['tpdCoverName'] = $scope.tpdCoverName;
                      tpdAddnlCoverObj['tpdCoverType'] = $scope.tpdCoverType;
                      tpdAddnlCoverObj['tpdFixedAmt'] = parseFloat($scope.tpdCoverAmount);
                      tpdAddnlCoverObj['tpdInputTextValue'] = parseFloat($scope.TPDRequireCover);
                      tpdAddnlCoverObj['tpdCoverPremium'] = parseFloat($scope.tpdWeeklyCost);
                     
                      /*coverObj['existingIPUnits'] = $scope.ipCoverDetails.units || 0;*/
                      coverObj['existingIPAmount'] = parseFloat($scope.ipCoverDetails.amount || 0);
                      coverObj['ipOccCategory'] = $scope.ipOccupationCategory;
                      coverObj['ipcheckbox'] = ipCheckboxState;
                      coverObj['ipSalaryPercent'] = 85;
                      ipAddnlCoverObj['ipCoverName'] = $scope.ipCoverName;
                      ipAddnlCoverObj['ipCoverType'] = 'IpUnitised';
                      ipAddnlCoverObj['waitingPeriod'] = $scope.waitingPeriodAddnl;
                      ipAddnlCoverObj['benefitPeriod'] = $scope.benefitPeriodAddnl;
                      ipAddnlCoverObj['ipFixedAmt'] = parseFloat($scope.ipCoverAmount);
                      if($scope.noconvert)
                        {
                        ipAddnlCoverObj['ipInputTextValue'] = parseFloat($scope.IPRequireCover);
                        if((parseInt($scope.IPRequireCover)%ipCostMultiplier)==0)
                              {
                              ipAddnlCoverObj['ipAddnlUnits'] = parseInt($scope.IPRequireCover) / ipCostMultiplier;
                              }
                        }
                      else
                        {
                        ipAddnlCoverObj['ipInputTextValue'] = parseFloat($scope.IPRequireCover);
                        ipAddnlCoverObj['ipAddnlUnits'] = parseFloat($scope.IPRequireCover);
                        }
                     
                      ipAddnlCoverObj['ipCoverPremium'] = parseFloat($scope.ipWeeklyCost);
                     
                      coverObj['totalPremium'] = parseFloat($scope.totalWeeklyCost);
                      coverObj['appNum'] = parseInt(appNum);
                      coverObj['auraDisabled'] = $scope.auraDisabled;
                      coverObj['ackCheck'] = ackCheck;
                      coverObj['dodCheck'] = $('#dodLabel').hasClass('active');
                      coverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');
                      coverObj['age'] = anb;
                      coverObj['manageType'] = 'CCOVER';
                      coverObj['partnerCode'] = 'CARE';
                      coverObj['freqCostType'] = 'Weekly';
                      coverObj['existingIPWaitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
                      coverObj['existingIPBenefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;
                        coverObj['lastSavedOn'] = 'Quotepage';
                      
                        
                      coverStateObj['isDCCoverRequiredDisabled'] = $scope.isDCCoverRequiredDisabled;
                      coverStateObj['isDCCoverTypeDisabled'] = $scope.isDCCoverTypeDisabled;
                      coverStateObj['isTPDCoverRequiredDisabled'] = $scope.isTPDCoverRequiredDisabled;
                      coverStateObj['isTPDCoverTypeDisabled'] = $scope.isTPDCoverTypeDisabled;
                      coverStateObj['isTPDCoverNameDisabled'] = $scope.isTPDCoverNameDisabled;
                      coverStateObj['isWaitingPeriodDisabled'] = $scope.isWaitingPeriodDisabled;
                      coverStateObj['isBenefitPeriodDisabled'] = $scope.isBenefitPeriodDisabled;
                      coverStateObj['isIPCoverRequiredDisabled'] = $scope.isIPCoverRequiredDisabled;
                      coverStateObj['isIPCoverNameDisabled'] = $scope.isIPCoverNameDisabled;
                      coverStateObj['isIpSalaryCheckboxDisabled'] = $scope.isIpSalaryCheckboxDisabled;
                      coverObj['indexationDeath'] = $scope.indexation.death;
                      coverObj['indexationTpd'] = $scope.indexation.disable;
                      coverStateObj['tpdDropdown'] = $scope.TPDCoverOptionsOne;
                      coverStateObj['dynamicFlag'] = dynamicFlag;
                      coverStateObj['deathDecOrCancelFlag'] = $scope.deathDecOrCancelFlag;
                      coverStateObj['tpdDecOrCancelFlag'] = $scope.tpdDecOrCancelFlag;
                      coverStateObj['ipDecOrCancelFlag'] = $scope.ipDecOrCancelFlag;
                      coverStateObj['dcIncreaseFlag'] = $scope.dcIncreaseFlag;
                      coverStateObj['tpdIncreaseFlag'] = $scope.tpdIncreaseFlag;
                      coverStateObj['ipIncreaseFlag'] = $scope.ipIncreaseFlag ;
                 
                  PersistenceService.setChangeCoverDetails(coverObj);
                  PersistenceService.setChangeCoverStateDetails(coverStateObj);
                  PersistenceService.setChangeCoverOccDetails(changeCoverOccObj);
                  PersistenceService.setDeathAddnlCoverDetails(deathAddnlCoverObj);
                  PersistenceService.setTpdAddnlCoverDetails(tpdAddnlCoverObj);
                  PersistenceService.setIpAddnlCoverDetails(ipAddnlCoverObj);
                             
          };
         /*$scope.showSaveOrExitPopUp = function (){
            var saveText = '<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>';
                  if(saveText == null || saveText == "" || saveText == " "){
                        hideSaveExitTips();
                  }else{
                   document.getElementById('mymodalSaveExit').style.display = 'block';
                   document.getElementById('mymodalSaveExitFade').style.display = 'block';
                   document.getElementById('saveExitMsg_text').innerHTML = saveText;                     
                  }
                  //$scope.saveQuote();
            }
         
          $scope.hideSaveExitTips = function  (){
                  if(document.getElementById('help_div')){
                        document.getElementById('help_div').style.display = "none";
                  }                                  
            }*/
          $scope.saveQuote =function(){
            $scope.checkAuraDisable();
           // $scope.showSaveOrExitPopUp();
            $scope.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR>');
            $scope.saveDataForPersistence();
            var quoteObject =  PersistenceService.getChangeCoverDetails();
            var changeCoverOccDetails = PersistenceService.getChangeCoverOccDetails();
            var deathAdditionalDetails = PersistenceService.getDeathAddnlCoverDetails();
            var tpdAdditionalDetails = PersistenceService.getTpdAddnlCoverDetails();
            var ipAdditionalDetails = PersistenceService.getIpAddnlCoverDetails();
          var personalInfo = persoanlDetailService.getMemberDetails();
          if(quoteObject != null && changeCoverOccDetails != null && deathAdditionalDetails != null && tpdAdditionalDetails != null && ipAdditionalDetails != null && personalInfo[0] != null ){
            var details={};
                  details.addnlDeathCoverDetails = deathAdditionalDetails;
                  details.addnlTpdCoverDetails = tpdAdditionalDetails;
                  details.addnlIpCoverDetails = ipAdditionalDetails;
                  details.occupationDetails = changeCoverOccDetails;
                  var temp = angular.extend(details,quoteObject);
                var saveQuoteObject = angular.extend(temp, personalInfo[0]);
            auraResponseService.setResponse(saveQuoteObject)
            $rootScope.$broadcast('disablepointer');
              saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) { 
                      console.log(response.data)
              });
          }
          };
         
       $scope.quoteSaveAndExitPopUp = function (hhText) {
                 
                  var dialog1 = ngDialog.open({
                              template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
                              className: 'ngdialog-theme-plain custom-width',
                              preCloseCallback: function(value) {
                                     var url = "/landing"
                                     $location.path( url );
                                     return true
                              },
                              plain: true
                  });
                  dialog1.closePromise.then(function (data) {
                        console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
                  });
            };
           
          $scope.goToAura =function(){
            $scope.validateDeathTpdIpAmounts();
            $scope.saveDataForPersistence();
            if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
                  if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag){
                        $rootScope.$broadcast('disablepointer');
                        if($scope.dcIncreaseFlag == true || $scope.tpdIncreaseFlag == true || $scope.ipIncreaseFlag ==true){
                              $scope.auraDisabled = false;
                              $timeout(function(){
                                          ackCheck = $('#termsLabel').hasClass('active');
                                          if(ackCheck){
                                                $scope.ackFlag = false;
                                          $scope.continueToNextPage('/aura/1');
                                          }else{
                                                $scope.ackFlag = true;
                                                $rootScope.$broadcast('enablepointer');
                                          }
                              }, 10);
                        } else{
                              $scope.auraDisabled = true;
                              $timeout(function(){
                                          ackCheck = $('#termsLabel').hasClass('active');
                                          if(ackCheck){
                                                $scope.ackFlag = false;
                                                $scope.continueToNextPage('/summary/1');
                                          }else{
                                                $scope.ackFlag = true;
                                                $rootScope.$broadcast('enablepointer');
                                          }
                              }, 10);
                        }
                  }else{
                        return false;
                  }
            }
          };
         
          /*$scope.clickToOpen = function (hhText) {
           
                  var dialog = ngDialog.open({
                        template: '<p>'+hhText+'</p>' +
                              '<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',
                              template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="">Close</button></div></div>',
                              className: 'ngdialog-theme-plain',
                              plain: true
                  });
                  dialog.closePromise.then(function (data) {
                        console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
                  });
            };*/
           
         
          if($routeParams.mode == 2 || $routeParams.mode == 3){
            var existingDetails = PersistenceService.getChangeCoverDetails();
            var stateDetails = PersistenceService.getChangeCoverStateDetails();
            var occupationDetails = PersistenceService.getChangeCoverOccDetails();
            var deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
            var tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
            var ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
            if(!existingDetails || !stateDetails || !occupationDetails || !deathAddnlDetails || !tpdAddnlDetails || !ipAddnlDetails) {
                  $location.path("/quote/1");
                  return false;
            }
            $scope.email = existingDetails.email;
            $scope.preferredContactType=existingDetails.contactType;
            $scope.changeCvrPhone = existingDetails.contactPhone;
            $scope.time = existingDetails.contactPrefTime;
            $scope.auraDisabled=existingDetails.auraDisabled;
           
            //Occupation Details
            $scope.fifteenHrsQuestion = occupationDetails.fifteenHr;
            /*$scope.areyouperCitzQuestion = occupationDetails.citizenQue;*/
            $scope.industry = occupationDetails.industryCode;
            //$scope.occupation = occupationDetails.occupation;
            $scope.withinOfficeQuestion = occupationDetails.withinOfficeQue;
            $scope.tertiaryQuestion = occupationDetails.tertiaryQue;
            $scope.managementRole = occupationDetails.managementRoleQue;
            $scope.gender = occupationDetails.gender;
            $scope.annualSalary = occupationDetails.salary;
            $scope.otherOccupationObj.otherOccupation = occupationDetails.otherOccupation;
           
            $scope.deathCoverDetails.amount = existingDetails.existingDeathAmt;
            $scope.deathCoverDetails.units = existingDetails.existingDeathUnits;
            $scope.deathOccupationCategory = existingDetails.deathOccCategory;
           
            $scope.coverName = deathAddnlDetails.deathCoverName;
            $scope.coverType = deathAddnlDetails.deathCoverType;
            $scope.dcCoverAmount = deathAddnlDetails.deathFixedAmt;
            $scope.requireCover = deathAddnlDetails.deathInputTextValue;
            $scope.dcWeeklyCost = deathAddnlDetails.deathCoverPremium;
           
            $scope.tpdCoverDetails.amount = existingDetails.existingTpdAmt;
            $scope.tpdCoverDetails.units = existingDetails.existingTPDUnits;
            $scope.tpdOccupationCategory = existingDetails.tpdOccCategory;
           
            $scope.tpdCoverName = tpdAddnlDetails.tpdCoverName;
            $scope.tpdCoverType = tpdAddnlDetails.tpdCoverType;
            $scope.tpdCoverAmount = tpdAddnlDetails.tpdFixedAmt;
            $scope.TPDRequireCover = tpdAddnlDetails.tpdInputTextValue;
            $scope.tpdWeeklyCost = tpdAddnlDetails.tpdCoverPremium;
           
            //$scope.ipCoverDetails.units = existingDetails.existingIPUnits;
            $scope.ipCoverDetails.amount = existingDetails.existingIPAmount;
            $scope.ipOccupationCategory = existingDetails.ipOccCategory;
            ipCheckboxState = existingDetails.ipcheckbox;
           
            /*$scope.ipCoverDetails.benefitPeriod = $scope.existingbenefitPeriod;
            $scope.ipCoverDetails.waitingPeriod =$scope.existingWaitingPeriod;*/
             
            $scope.ipCoverName = ipAddnlDetails.ipCoverName;
            $scope.waitingPeriodAddnl = ipAddnlDetails.waitingPeriod;
            $scope.benefitPeriodAddnl = ipAddnlDetails.benefitPeriod;
            $scope.ipCoverAmount = ipAddnlDetails.ipFixedAmt;
           
            if($scope.ipCoverName && $scope.ipCoverName == 'No change')
                  {
                  $scope.IPRequireCover = ipAddnlDetails.ipFixedAmt || 0;
                  showhide('dollar3','nodollar3');
                    $scope.unitDisplay = false;
                    $scope.noconvert = true;
                  }
            else
                  {
                    /*$scope.IPRequireCover = ipAddnlDetails.ipAddnlUnits || 0;
                  $scope.ipConvertedAmt = ipAddnlDetails.ipInputTextValue || 0;*/
                  $scope.IPRequireCover = ipAddnlDetails.ipInputTextValue || 0;
                  $scope.ipConvertedAmt = ipAddnlDetails.ipFixedAmt;
                  showhide('nodollar3','dollar3');
                $scope.unitDisplay = true;
                $scope.noconvert = false;
                ipUnchanged = false;
                  }
            $scope.ipWeeklyCost = ipAddnlDetails.ipCoverPremium;
           
            $scope.totalWeeklyCost = existingDetails.totalPremium;
            appNum = existingDetails.appNum;
            ackCheck = existingDetails.ackCheck;
            dodCheck = existingDetails.dodCheck;
            privacyCheck = existingDetails.privacyCheck;
           
           
            if(parseInt(ipAddnlDetails.ipFixedAmt) > parseInt($scope.ipCoverDetails.amount))
                  {
                  $scope.ipIncreaseFlagfinal = true;
                  }
           
            $scope.isDCCoverRequiredDisabled = stateDetails.isDCCoverRequiredDisabled;
            $scope.isDCCoverTypeDisabled = stateDetails.isDCCoverTypeDisabled;
            $scope.isTPDCoverRequiredDisabled = stateDetails.isTPDCoverRequiredDisabled;
            $scope.isTPDCoverTypeDisabled = stateDetails.isTPDCoverTypeDisabled;
            $scope.isTPDCoverNameDisabled = stateDetails.isTPDCoverNameDisabled;
            $scope.isWaitingPeriodDisabled = stateDetails.isWaitingPeriodDisabled;
            $scope.isBenefitPeriodDisabled = stateDetails.isBenefitPeriodDisabled;
            $scope.isIPCoverRequiredDisabled = stateDetails.isIPCoverRequiredDisabled;
            $scope.isIPCoverNameDisabled = stateDetails.isIPCoverNameDisabled;
            $scope.isIpSalaryCheckboxDisabled = stateDetails.isIpSalaryCheckboxDisabled;
            $scope.indexation.death = existingDetails.indexationDeath;
            $scope.indexation.disable = existingDetails.indexationTpd;
            $scope.tpdDropdown = stateDetails.TPDCoverOptionsOne;
            dynamicFlag = stateDetails.dynamicFlag;
            $scope.deathDecOrCancelFlag = stateDetails.deathDecOrCancelFlag;
            $scope.tpdDecOrCancelFlag = stateDetails.tpdDecOrCancelFlag;
            $scope.ipDecOrCancelFlag = stateDetails.ipDecOrCancelFlag;
            $scope.dcIncreaseFlag = stateDetails.dcIncreaseFlag;
            $scope.tpdIncreaseFlag = stateDetails.tpdIncreaseFlag;
            $scope.ipIncreaseFlag = stateDetails.ipIncreaseFlag;
           
            if($scope.coverType == "DcUnitised"){
                  showhide('nodollar1','dollar1');
                        showhide('nodollar','dollar');
            } else if($scope.coverType == "DcFixed"){
                  showhide('dollar1','nodollar1');
                  showhide('dollar','nodollar');
            }
           
            OccupationService.getOccupationList($scope.urlList.occupationUrl, "CARE", $scope.industry).then(function(res){
                  $scope.OccupationList = res.data;
                  var temp = $scope.OccupationList.filter(function(obj){
                        return obj.occupationName == occupationDetails.occupation;
                  });
                  $scope.occupation = temp[0].occupationName;
            }, function(err){
                  console.info("Error while fetching occupations " + JSON.stringify(err));
            });
           
            if(ipCheckboxState){
                  $('#ipsalarycheck').parent().addClass('active');
            }
            if(ackCheck){
                $timeout(function(){
                        $('#termsLabel').addClass('active');
                           });
                         }
            if($scope.indexation.death){
                  $('#changecvr_index-dth').parent().addClass('active');
            }
           
            if($scope.indexation.disable){
                  $('#changeCvr-indextpd-disable').parent().addClass('active');
            }
 
//          if(dodCheck){
//              $timeout(function(){
//                      $('#dodLabel').addClass('active');
//                   });
//                }
//          if(privacyCheck){
//              $timeout(function(){
//                      $('#privacyLabel').addClass('active');
//                   });
//                }
           
            $('#dodLabel').addClass('active');       
                  $('#privacyLabel').addClass('active');
 
 
                  $scope.isDCIndexationDisabled = true;
                  $scope.isTPDIndexationDisabled = true;
 
                  if($scope.coverName == 'Increase your cover' || $scope.coverName == 'Decrease your cover')
                        $scope.isDCIndexationDisabled = false;
 
                  if($scope.tpdCoverName == 'Increase your cover' || $scope.tpdCoverName == 'Decrease your cover')                   
                        $scope.isTPDIndexationDisabled = false;
                 
                  $scope.setCoverCalculatorOptions();
            $scope.toggleTwo(true);
            $scope.toggleThree(true);
            $scope.togglePrivacy(true);
                  $scope.toggleContact(true);
          }
         
          if($routeParams.mode == 4){
            mode3Flag = true;
            var num = PersistenceService.getAppNumToBeRetrieved();
           
                RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
            //RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
                  var appDetails = res.data[0];
                  $scope.email = appDetails.email;
                  $scope.preferredContactType = appDetails.contactType;
                  $scope.changeCvrPhone = appDetails.contactPhone;
                  $scope.time = appDetails.contactPrefTime;
                 
                  $scope.fifteenHrsQuestion = appDetails.occupationDetails.fifteenHr;
                  /*$scope.areyouperCitzQuestion = appDetails.occupationDetails.citizenQue;*/
                  $scope.industry = appDetails.occupationDetails.industryCode;
                  $scope.withinOfficeQuestion = appDetails.occupationDetails.withinOfficeQue;
                  $scope.tertiaryQuestion = appDetails.occupationDetails.tertiaryQue;
                  $scope.managementRole = appDetails.occupationDetails.managementRoleQue;
                  $scope.gender = appDetails.occupationDetails.gender;
                  $scope.annualSalary = appDetails.occupationDetails.salary;
                  $scope.otherOccupationObj.otherOccupation = appDetails.occupationDetails.otherOccupation;
                  $scope.auraDisabled = appDetails.auraDisabled;
                  if(appDetails.auraDisabled && appDetails.auraDisabled == "true"){
                        $scope.auraDisabled = true;
                  } else{
                        $scope.auraDisabled = false;
                  }
                 
                  $scope.deathCoverDetails.amount = appDetails.existingDeathAmt;
                  $scope.deathCoverDetails.units = appDetails.existingDeathUnits;
                  $scope.deathOccupationCategory = appDetails.deathOccCategory;
                 
                  $scope.coverName = appDetails.addnlDeathCoverDetails.deathCoverName;
                  $scope.coverType = appDetails.addnlDeathCoverDetails.deathCoverType;
                  $scope.dcCoverAmount = appDetails.addnlDeathCoverDetails.deathFixedAmt;
                  $scope.requireCover = appDetails.addnlDeathCoverDetails.deathInputTextValue;
                  $scope.dcWeeklyCost = appDetails.addnlDeathCoverDetails.deathCoverPremium;
                 
                  $scope.tpdCoverDetails.amount = appDetails.existingTpdAmt;
                  $scope.tpdCoverDetails.units = appDetails.existingTPDUnits;
                  $scope.tpdOccupationCategory = appDetails.tpdOccCategory;
                 
                  $scope.tpdCoverName = appDetails.addnlTpdCoverDetails.tpdCoverName;
                  $scope.tpdCoverType = appDetails.addnlTpdCoverDetails.tpdCoverType;
                  $scope.tpdCoverAmount = appDetails.addnlTpdCoverDetails.tpdFixedAmt;
                  $scope.TPDRequireCover = appDetails.addnlTpdCoverDetails.tpdInputTextValue;
                  $scope.tpdWeeklyCost = appDetails.addnlTpdCoverDetails.tpdCoverPremium;
                 
                  //$scope.ipCoverDetails.units = appDetails.existingIPUnits;
                  $scope.ipCoverDetails.amount = appDetails.existingIPAmount;
                  $scope.ipOccupationCategory = appDetails.ipOccCategory;
                  ipCheckboxState = appDetails.ipcheckbox;
                  if(appDetails.indexationDeath && appDetails.indexationDeath == "true"){
                        $scope.indexation.death = true;
                  } else{
                        $scope.indexation.death = false;
                  }
                  if(appDetails.indexationTpd && appDetails.indexationTpd == "true"){
                        $scope.indexation.disable = true;
                  } else{
                        $scope.indexation.disable = false;
                  }
                 
                  $scope.ipCoverName = appDetails.addnlIpCoverDetails.ipCoverName;
                  $scope.waitingPeriodAddnl = appDetails.addnlIpCoverDetails.waitingPeriod;
                  $scope.benefitPeriodAddnl = appDetails.addnlIpCoverDetails.benefitPeriod;
                  $scope.ipCoverAmount = appDetails.addnlIpCoverDetails.ipFixedAmt;
                  if($scope.ipCoverName && $scope.ipCoverName == 'No change')
                  {
                $scope.IPRequireCover = appDetails.addnlIpCoverDetails.ipFixedAmt || 0;
                  $scope.noconvert = true;
                  showhide('dollar3','nodollar3');
                    $scope.unitDisplay = false;
                  }
            else
                  {
                  /*$scope.IPRequireCover = appDetails.addnlIpCoverDetails.ipAddnlUnits||0;
                  $scope.ipConvertedAmt = appDetails.addnlIpCoverDetails.ipInputTextValue || 0;*/
                  $scope.IPRequireCover = appDetails.addnlIpCoverDetails.ipInputTextValue || 0;
                  $scope.ipConvertedAmt = appDetails.addnlIpCoverDetails.ipFixedAmt;
                  showhide('nodollar3','dollar3');
                $scope.unitDisplay = true;
                $scope.noconvert = false;
                ipUnchanged = false;
                  }
                 
                 
                  //$scope.IPRequireCover = appDetails.addnlIpCoverDetails.ipInputTextValue;
                  $scope.ipWeeklyCost = appDetails.addnlIpCoverDetails.ipCoverPremium;
                 
                  $scope.totalWeeklyCost = appDetails.totalPremium;
                  appNum = appDetails.appNum;
                  ackCheck = appDetails.ackCheck;
                  dodCheck = appDetails.dodCheck;
                  privacyCheck = appDetails.privacyCheck;
 
//                $scope.dcIncreaseFlag = appDetails.addnlDeathCoverDetails.deathCoverName == "Increase your cover";
//                      $scope.tpdIncreaseFlag = appDetails.addnlTpdCoverDetails.tpdCoverName == "Increase your cover";
//                      $scope.ipIncreaseFlag = appDetails.addnlIpCoverDetails.ipCoverName == "Increase your cover";
                 
                 
 
                 
                  if($scope.coverType == "DcUnitised"){
                        showhide('nodollar1','dollar1');
                              showhide('nodollar','dollar');
                  } else if($scope.coverType == "DcFixed"){
                        showhide('dollar1','nodollar1');
                        showhide('dollar','nodollar');
                  }
                 
                  OccupationService.getOccupationList($scope.urlList.occupationUrl, "CARE", $scope.industry).then(function(res){
                        $scope.OccupationList = res.data;
                        var temp = $scope.OccupationList.filter(function(obj){
                              return obj.occupationName == appDetails.occupationDetails.occupation;
                        });
                        $scope.occupation = temp[0].occupationName;
                  }, function(err){
                        console.info("Error while fetching occcupation list " + JSON.stringify(err));
                  });
                 
                  // $scope.checkDeathOption();
                  // $scope.checkTPDOption();
                  // $scope.checkIPOption();
                 
                  if(ipCheckboxState){
                        $('#ipsalarycheck').parent().addClass('active');
                        $scope.isIPCoverRequiredDisabled = true;
                  }
                  if($scope.indexation.death){
                        $('#changecvr_index-dth').parent().addClass('active');
                  }
                 
                  if($scope.indexation.disable){
                        $('#changeCvr-indextpd-disable').parent().addClass('active');
                  }
                  if(ackCheck){
                      $timeout(function(){
                              $('#termsLabel').addClass('active');
                     });
                   }
                 
                  $('#dodLabel').addClass('active');       
                  $('#privacyLabel').addClass('active');
 
                  $scope.isDCIndexationDisabled = true;
                  $scope.isTPDIndexationDisabled = true;
 
                  if($scope.coverName == 'Increase your cover' || $scope.coverName == 'Decrease your cover')
                        $scope.isDCIndexationDisabled = false;
 
                  if($scope.tpdCoverName == 'Increase your cover' || $scope.tpdCoverName == 'Decrease your cover')                   
                              $scope.isTPDIndexationDisabled = false;
 
                  $scope.toggleTwo(true);
                  $scope.toggleThree(true);
                  $scope.togglePrivacy(true);
                  $scope.toggleContact(true);
                                   
                  $scope.setCoverCalculatorOptions();
                 
            }, function(err){
                  console.info("Error fetching the saved app details " + err);
           });
          }
 
$scope.unitToFixedIp = function()
            {
                  $scope.ipConvertedAmt = parseInt($scope.IPRequireCover) * ipCostMultiplier;
            }
         
          
  }]);
 
 /* Change Cover Controller,Progressive and Mandatory validations Ends */