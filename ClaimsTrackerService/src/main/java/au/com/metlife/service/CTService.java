/**
 * 
 */
package au.com.metlife.service;

import java.util.List;

import au.com.metlife.dto.DocumentDto;
import au.com.metlife.model.Document;

/**
 * @author kp
 *
 */
public interface CTService {
	
	Document generateToken(String input, String outToken);
	Document retriveClientData(String token);
	void saveDocumentList(List<DocumentDto> documentDto);
	List<Document> retriveDocList(String ClaimNo);
}
