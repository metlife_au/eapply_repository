package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSGetQuotePageObjects extends PageObjectLibrary{
		
	public static final By txtFirstName = getObjectIdentifaction("OAMPS.yourquote.firstname.text.id");
//	public static final By txtLastName = getObjectIdentifaction("stft.getquote.lastname.text.id");
	public static final By txtDOB = getObjectIdentifaction("OAMPS.yourquote.dob.text.id");
	public static final By rdMale = getObjectIdentifaction("OAMPS.yourquote.gender.radio.css");
	public static final By rdFemale = getObjectIdentifaction("OAMPS.yourquote.gender.female.radio.css");
//	public static final By rdFemale = getObjectIdentifaction("stft.getquote.gender.female.radio.id");
//	public static final By rdSmokedIn12MthsYes = getObjectIdentifaction("stft.getquote.smokedin12months.yes.radio.id");
	public static final By rdSmokedIn12MthsNo = getObjectIdentifaction("OAMPS.yourquote.smoker.radio.css");
	public static final By rdCitizenYes = getObjectIdentifaction("OAMPS.yourquote.citizen.radio.css");
//	public static final By rdCitizenNo = getObjectIdentifaction("stft.getquote.citizen.no.radio.id");
	public static final By selIndustry = getObjectIdentifaction("OAMPS.yourquote.industry.radio.css");
	public static final By selOccupation = getObjectIdentifaction("OAMPS.yourquote.occupation.radio.css");
	
	public static final By selLifeInsurance = getObjectIdentifaction("OAMPS.yourquote.coverlevel.drpdown.css");	
	public static final By selDisability = getObjectIdentifaction("OAMPS.yourquote.disabilitybenefit.drpdown.css");	
	public static final By btnCalculateQuote = getObjectIdentifaction("OAMPS.yourquote.calculatequote.btn.css");
	public static final By btnApply = getObjectIdentifaction("OAMPS.yourquote.apply.btn.css");
}
