package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.SAXException;

import au.com.metlife.eapply.constants.EapplicationConstants;
import au.com.metlife.eapply.utility.XSDHelper;

public class XSDValidationTest {
	
	@Test
	public void shouldValidateMTAA() throws Exception {
		//given
		XSDHelper xsdHelper = new XSDHelper();
		String inputString = "<request><adminPartnerID>IFSP</adminPartnerID><transRefGUID>73bbfa52-797d-43bd-85db-05f596d209e7</transRefGUID><transType>2</transType><transDate>24/11/2017</transDate><transTime>10:19:09</transTime><fundID>MTAA</fundID><fundEmail>Multifund_insurance@aas.com.au</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>MTAA</partnerID><applicant><dateJoined>18/10/2017</dateJoined><workCommencementDate>18/10/2017</workCommencementDate><firstContributionDate>30/11/2017</firstContributionDate><clientRefNumber>312658452</clientRefNumber><memberType>Employer Sponsored</memberType><applicantRole>1</applicantRole><personalDetails><title>MRS</title><firstName>Tina</firstName><lastName>Smith</lastName><dateOfBirth>19/12/1975</dateOfBirth><gender>Female</gender><priority>2</priority><smoker>2</smoker><applicantSubType /></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><amount>3000.00</amount>			<units>12</units><loading /><exclusions>Y</exclusions><waitingPeriod>90DAYS</waitingPeriod><benefitPeriod>5YEARS</benefitPeriod><indexation>2</indexation></cover></existingCovers><address><addressType>2</addressType><line1>2 Park St</line1><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>AUSTRALIA</country></address><contactDetails><emailAddress>pkotian@metlife.com</emailAddress><mobilePhone>0412345678</mobilePhone></contactDetails></applicant></policy></request>";
		if(inputString.contains(EapplicationConstants.DAYS30)){			
			inputString = inputString.replace(EapplicationConstants.DAYS30, EapplicationConstants.THIRTY_DAYS);
		}else if(inputString.contains(EapplicationConstants.DAYS45)){
			inputString = inputString.replace(EapplicationConstants.DAYS45, EapplicationConstants.FOURTYFIVE_DAYS);
		}else if(inputString.contains(EapplicationConstants.DAYS60)){
			inputString = inputString.replace(EapplicationConstants.DAYS60, EapplicationConstants.SIXTY_DAYS);
		}else if(inputString.contains(EapplicationConstants.DAYS90)){
			inputString = inputString.replace(EapplicationConstants.DAYS90, EapplicationConstants.NINETY_DAYS);
		}
		if(inputString.contains(EapplicationConstants.YEARS2)){
			inputString = inputString.replace(EapplicationConstants.YEARS2, EapplicationConstants.TWO_YEARS);
		}else if(inputString.contains(EapplicationConstants.YEARS5)){
			inputString = inputString.replace(EapplicationConstants.YEARS5, EapplicationConstants.FIVE_YEARS);
		}else if(inputString.contains(EapplicationConstants.AGE65)){
			inputString = inputString.replace(EapplicationConstants.AGE65,EapplicationConstants. AGE_SIXTYFIVE);
		}else if(inputString.contains(EapplicationConstants.AGE67)){
			inputString = inputString.replace(EapplicationConstants.AGE67, EapplicationConstants.AGE_SIXTYSEVEN);
		}
		//when	
		String output = xsdHelper.validateXMLString(inputString, new ClassPathResource("MTAA_WebService_schema_new.xsd").getFile().getPath());
		assertThat(output,is("valid"));
	}
	
	@Test
    public void shouldSuccessfullyValidateInstance() throws IOException, SAXException {		
		String inputString = "<request><adminPartnerID>IFSP</adminPartnerID><transRefGUID>73bbfa52-797d-43bd-85db-05f596d209e7</transRefGUID><transType>2</transType><transDate>24/11/2017</transDate><transTime>10:19:09</transTime><fundID>MTAA</fundID><fundEmail>Multifund_insurance@aas.com.au</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>MTAA</partnerID><applicant><dateJoined>18/10/2017</dateJoined><workCommencementDate>18/10/2017</workCommencementDate><firstContributionDate>30/11/2017</firstContributionDate><clientRefNumber>312658452</clientRefNumber><memberType>Employer Sponsored</memberType><applicantRole>1</applicantRole><personalDetails><title>MRS</title><firstName>Tina</firstName><lastName>Smith</lastName><dateOfBirth>19/12/1975</dateOfBirth><gender>Female</gender><priority>2</priority><smoker>2</smoker><applicantSubType /></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><amount>3000.00</amount>			<units>12</units><loading /><exclusions>Y</exclusions><waitingPeriod>90DAYS</waitingPeriod><benefitPeriod>5YEARS</benefitPeriod><indexation>2</indexation></cover></existingCovers><address><addressType>2</addressType><line1>2 Park St</line1><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>AUSTRALIA</country></address><contactDetails><emailAddress>pkotian@metlife.com</emailAddress><mobilePhone>0412345678</mobilePhone></contactDetails></applicant></policy></request>";
		if(inputString.contains(EapplicationConstants.DAYS30)){			
			inputString = inputString.replace(EapplicationConstants.DAYS30, EapplicationConstants.THIRTY_DAYS);
		}else if(inputString.contains(EapplicationConstants.DAYS45)){
			inputString = inputString.replace(EapplicationConstants.DAYS45, EapplicationConstants.FOURTYFIVE_DAYS);
		}else if(inputString.contains(EapplicationConstants.DAYS60)){
			inputString = inputString.replace(EapplicationConstants.DAYS60, EapplicationConstants.SIXTY_DAYS);
		}else if(inputString.contains(EapplicationConstants.DAYS90)){
			inputString = inputString.replace(EapplicationConstants.DAYS90, EapplicationConstants.NINETY_DAYS);
		}
		if(inputString.contains(EapplicationConstants.YEARS2)){
			inputString = inputString.replace(EapplicationConstants.YEARS2, EapplicationConstants.TWO_YEARS);
		}else if(inputString.contains(EapplicationConstants.YEARS5)){
			inputString = inputString.replace(EapplicationConstants.YEARS5, EapplicationConstants.FIVE_YEARS);
		}else if(inputString.contains(EapplicationConstants.AGE65)){
			inputString = inputString.replace(EapplicationConstants.AGE65,EapplicationConstants. AGE_SIXTYFIVE);
		}else if(inputString.contains(EapplicationConstants.AGE67)){
			inputString = inputString.replace(EapplicationConstants.AGE67, EapplicationConstants.AGE_SIXTYSEVEN);
		}
		SchemaFactory factory =  SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");	          
	    File schemaLocation = new ClassPathResource("MTAA_WebService_schema_new.xsd").getFile();
	    Schema schema = factory.newSchema(schemaLocation);
	    Validator  validator = schema.newValidator();
	
	    StringReader reader = new StringReader(inputString);
	      if(reader!=null){
		      Source source = new StreamSource(reader);
		      validator.validate(source);
		   }      

    }

}
