package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.OAMPSGetQuotePageObjects;
import uimap.metlife.YBRIPGetQuotePageObjects;
import uimap.metlife.STFTGetQuotePageObjects;

public class YBRIPGetQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRIPGetQuotePage getQuote() {
		premiumCalculation();
		return new YBRIPGetQuotePage(scriptHelper);
	}

	public YBRIPGetQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void premiumCalculation() {

		String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCitizen = dataTable.getData("MetLife_Data", "Citizen");
		String strIndustry = dataTable.getData("MetLife_Data", "Industry");
		String stroccupation = dataTable.getData("MetLife_Data", "Occupation");
		String strannualSalary = dataTable.getData("MetLife_Data", "AnnualSalary");


		try {
			
			Thread.sleep(2000);
			driver.manage().window().maximize();			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.txtDOB), "SET", "Date of birth", dob);
			Thread.sleep(1000);
			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.txtDOB), "TAB", "Date of birth");
			
			Thread.sleep(1000);
			
/*			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(ALIGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
			} */
			
			tryAction(gender.equalsIgnoreCase("Male")?waitForClickableElement(YBRIPGetQuotePageObjects.rdMale):waitForClickableElement(YBRIPGetQuotePageObjects.rdFemale),"clkradio","Gender -" + gender);
			sleep();
			
			Thread.sleep(1000);
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.rdSmokedIn12MthsNo), "clkradio",
						"Have you Smoked in 12 months - Yes");
			} else {
				tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.rdSmokedIn12MthsNo), "clkradio",
						"Have you Smoked in 12 months - No");
			}
			
			Thread.sleep(1000);
			
			if (strCitizen.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.rdCitizenYes), "Click",
						"Are you Citizen or PR - Yes");

			} else {
				tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.rdCitizenYes), "Click",
						"Are you Citizen or PR - No");
			}
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.selIndustry), "DropDownSelect", "Industry",strIndustry);
			Thread.sleep(2000);
			
			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.selOccupation), "DropDownSelect", "Occupation",stroccupation);		
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.txtAnnualsalary), "SET", "Annual Salary",strannualSalary);
			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.txtAnnualsalary), "TAB", "Annual Salary");
			
			Thread.sleep(1000);
			tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.selemploymentStaus), "clkradio", "Emplyment Type");
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(YBRIPGetQuotePageObjects.rdConsultant), "clkradio", "Consultant ?");
			Thread.sleep(1000);
			

			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.btnCalculateQuote), "Click", "Calculate Quote");
			Thread.sleep(10000);

			tryAction(fluentWaitElement(YBRIPGetQuotePageObjects.btnApply), "Click", "Apply");
			
			Thread.sleep(10000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
