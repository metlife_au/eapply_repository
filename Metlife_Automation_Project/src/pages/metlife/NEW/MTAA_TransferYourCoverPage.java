package pages.metlife.NEW;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.MTAA_TransferYourCoverPageObjects;

public class MTAA_TransferYourCoverPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public MTAA_TransferYourCoverPage mtaatransfercover() {
		TransferCoverMTAA();
		healthandlifestyledetails();
		confirmation();
		return new MTAA_TransferYourCoverPage(scriptHelper);
	}
	
	public MTAA_TransferYourCoverPage mtaatransfercoverNegative() {
		TransferCoverMTAANegative();
		return new MTAA_TransferYourCoverPage(scriptHelper);
	}

	public MTAA_TransferYourCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void TransferCoverMTAA() {

		String EmailId = dataTable.getData("MTAA_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("MTAA_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("MTAA_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("MTAA_TransferCover", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("MTAA_TransferCover", "FifteenHoursWork");
		String Citizen = dataTable.getData("MTAA_TransferCover", "Citizen");
		String IndustryType = dataTable.getData("MTAA_TransferCover", "IndustryType");
		String OccupationType = dataTable.getData("MTAA_TransferCover", "OccupationType");
		String OtherOccupation = dataTable.getData("MTAA_TransferCover", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("MTAA_TransferCover", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("MTAA_TransferCover", "TertiaryQual");
		String HazardousEnv = dataTable.getData("MTAA_TransferCover", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("MTAA_TransferCover", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("MTAA_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("MTAA_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("MTAA_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("MTAA_TransferCover", "FormerFundNo");
		String DocumentaryEvidence = dataTable.getData("MTAA_TransferCover", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("MTAA_TransferCover", "AttachPath");
		String CostType = dataTable.getData("MTAA_TransferCover", "CostType");
		String DeathYN = dataTable.getData("MTAA_TransferCover", "Death");
		String TPDYN = dataTable.getData("MTAA_TransferCover", "TPD");
		String IPYN = dataTable.getData("MTAA_TransferCover", "IP");
		String DeathAmount = dataTable.getData("MTAA_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("MTAA_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("MTAA_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("MTAA_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("MTAA_TransferCover", "BenefitPeriod");
		

		
		
		
		try{
			
			
			
			//****Click on Transfer Your Cover Link*****\\
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TransferYourCoverLink), "Click", "Transfer Your Cover Link");
		

			//*****Agree to Duty of disclosure*******\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "selected the Checkbox", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Privacy Statement", "selected the Checkbox", Status.PASS);

		

			//*****Enter the Email Id*****\\
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.EmailId), "SET", "Email Id",EmailId);
			

			//*****Click on the Contact Number Type****\\
			if(!TypeofContact.equalsIgnoreCase("Mobile")){
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactTypeClick), "Click", "Preferred Type of Contact: "+TypeofContact);
						
			//*****Choose the Type of Contact*****\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();
			
			}
			
			//****Enter the Contact Number****\\
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);
			
			
			
			//***Select the Preferred time of Contact*****\\
			if(TimeofContact.equalsIgnoreCase("Morning")){
				
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");
			}
			else{
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.AfterNoonContact), "Click", "Preferred Contact Time is AfterNoon");
			}
			
			//****Contact Details-Continue Button*****\\			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");
			sleep(1200);
			
			
			//***************************************\\	
			//**********OCCUPATION SECTION***********\\
			//****************************************\\	
				
				//*****Select the 15 Hours Question*******\\
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.FifteenHoursYes), "Click", "15 Hours a Week-Yes");
				
				}
				else{
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");
				
				}
				
				//*****Select if Resident of Australia****\\
				if(Citizen.equalsIgnoreCase("Yes")){
		
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CitizenYes), "Click", "Australian Citizen-Yes");
					
					}
				else{
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CitizenNo), "Click", "Australian Citizen-No");	
					
					}
				
				//*****Click on Industry********\\
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ClickIndustry), "Click", "Industry");
				sleep(800);
				
				//*****Choose the Industry Type*****\\
				driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();
				sleep(800);
			
				//*****Click On  Occupation*****\\
				
	            tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ClickOccupation), "Click", "Occupation");
	            sleep(800);
	            
	            //****Choose the Occupation Type*****\\
				
				driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();
				sleep(1000);
				
				
				//*****Other Occupation *****\\
				if(OccupationType.equalsIgnoreCase("Other")){
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);
					sleep(1000);
				}
				
				
				//*****Extra Questions*****\\
				
				if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){
			String Question=driver.findElement(By.xpath("//*[@id='work_duty_div_id']/div/label")).getText().trim();
			sleep(800);
			
			
			
			if(Question.equalsIgnoreCase("Are your duties entirely undertaken within an office environment?")){
				
			//***Select if your office Duties are Undertaken within an Office Environment?\\
				if(OfficeEnvironment.equalsIgnoreCase("Yes")){
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");
					
				}
				else{
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");
					
				}
				
			//*****Do You Hold a Tertiary Qualification******\\
				
				if(TertiaryQual.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");				
				}						
			}
				}
				
				
			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){
			
				
				
			//******Do you work in hazardous environment*****\\
				if(HazardousEnv.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");				
				}
				
			//******Do you spend 20% time working outside office*******\\
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){				
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");				
				}
				else{				
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");				
				}						
			}
			
				
			//*****What is your annual Salary******
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);
			sleep(500);
			
			//***Load the Salary Amount****\\
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadSalaryLabel).click();
			sleep(500);
			
			
		//*****Continue button after entering occupation details*******\\
			
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");
			sleep(1000);
			
       //*****Continue button after entering occupation details*******\\
			
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");
			sleep(1000);
			
		//*************************************************************\\	
		//************PREVIOUS COVER SECTION****************************\\
		//**************************************************************\\
		
			//*****Name of previous insurer*****\\	
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.PreviousInsurer), "SET", "Previous Insurer Name",PreviousInsurer);	
		
		
		//*****please enter your fund member of insurance policy number*****\\		
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.PolicyNumber), "SET", "Member Number of Policy Number",PolicyNo);
		
		
		//please enter your former fund USI number*****		
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.USINumber), "SET", "Former Fund USI Number",FormerFundNo);
		
		
		//*****8Do You want to Attach documentary evidence****\\
		if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
			
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceYes).click();
			sleep(500);
			
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceNo).click();
			sleep(500);
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceYes), "Click", "Documentary Evidence-Yes");
			sleep(1000);
			
			
			//****Attach the file******		
			driver.findElement(By.id("transferCvrFileUploadAttach")).sendKeys(AttachPath);
			sleep(1500);
			
			//*****Let the attachment load*****\\		
			driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
			sleep(1000);
			
			//*****Add the Attachment*****\\
			
			WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
			sleep(1500);
			
			
			
		}
		else{
			
			//****No Attachment *****\\
			
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceYes).click();
			sleep(500);
			
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceNo).click();
			sleep(500);
			
			//Transfer check box
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TransferChkbox), "Click", "Transfer Check Box");
			
		}
		
		
		//******Click on Cost of  Insurance as*******\\
		
		if(!CostType.equalsIgnoreCase("Monthly")){
		
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CostofInsurance), "Click", "Select the Cost Of Insurance");
		sleep(800);
		
		//*****Choose the cost of Insurance Type******\\
		
		driver.findElement(By.xpath("//span[contains(text(),'"+CostType+"')]")).click();
		sleep(800);
		
		
		}
		
		//****Click on Continue Button******\\
		
		driver.findElement(By.xpath("//*[@id='previousContinueId']/span")).click();
		report.updateTestLog("Continue", "Click on Continue button after entering Previos coverd etails", Status.PASS);
		sleep(1000);
		
		//****Click on Continue Button******\\
		
		properties = Settings.getInstance();	
		String StrBrowseName=properties.getProperty("currentBrowser");
		System.out.println(StrBrowseName);
	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
	  			driver.findElement(By.xpath("//*[@id='previousContinueId']/span")).click();
	  			report.updateTestLog("Continue", "Click on Continue button after entering Previos coverd etails", Status.PASS);
	  			sleep(1000);	
	  		}
		
				
		
		
		
		
		//*************************************************\\
		//**************TRANSFER COVER SECTION***************\\
		//**************************************************\\
		
		//*****Death transfer amount******
		
		if(DeathYN.equalsIgnoreCase("Yes")){
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.DeathAmount), "SET", "Death transfer amount",DeathAmount);	
		sleep(500);
		waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadDeathAmount).click();
		}
		
		//*****TPD transfer amount******
		if(TPDYN.equalsIgnoreCase("Yes")){
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TPDAmount), "SET", "TPD transfer amount",TPDAmount);	
		sleep(500);
		waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadTPDAmount).click();
		}
		//IP transfer amount*****\\
		sleep(800);
		if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.IPAmount), "SET", "IP transfer amount",IPAmount);	
		sleep(500);
		waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadIPAmount).click();
		

		//*****Select The Waiting Period********\\
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.Waitingperiod), "Click", "Waiting Period");
		sleep(800);	
		driver.findElement(By.xpath("//span[contains(text(),'"+WaitingPeriod+"')]")).click();
		
		
		
		//*****Select The Benefit  Period********\\
		
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.benefitperiod), "Click", "Benefit Period");
		sleep(800);		
		driver.findElement(By.xpath("//span[contains(text(),'"+BenefitPeriod+"')]")).click();
		}
			
		//*****Scroll to the Bottom of the Page
		JavascriptExecutor jse = (JavascriptExecutor) driver;
	    jse.executeScript("window.scrollBy(0,250)", "");
		
		
		//*****Click on Calculate Quote******\\
		sleep(1000);
		tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");
		sleep(2000);
		
		if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright hidden-xs']/h4")).getText().equalsIgnoreCase("$0.00")){
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");
			sleep(4000);	
		}
		
		if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright hidden-xs']/h4")).getText().equalsIgnoreCase("$0.00")){
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");
			sleep(4000);	
		}
		
		if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright hidden-xs']/h4")).getText().equalsIgnoreCase("$0.00")){
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");
			sleep(4000);	
		}
		
		//********************************************************************************************\\
		//************Capturing the premiums displayed************************************************\\
		//********************************************************************************************\\
		
		//*******Capturing the Death Cost********\\
		WebElement Death=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[1]"));
		Death.click();
		sleep(300);
		jse.executeScript("window.scrollBy(0,-150)", "");
		sleep(500);
		String DeathCost=Death.getText();	
		report.updateTestLog("Death Cover amount", CostType+" Cost is "+DeathCost, Status.SCREENSHOT);
		sleep(500);
		
		//*******Capturing the TPD Cost********\\
		WebElement tpd=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[2]"));
		tpd.click();
		sleep(500);
		String TPDCost=tpd.getText();
		report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);
		
		//*******Capturing the IP Cost********\\
		WebElement ip=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[3]"));
		ip.click();
		sleep(500);
		String IPCost=ip.getText();
		
		report.updateTestLog("IP Cover amount", CostType+" Cost is "+IPCost, Status.SCREENSHOT);
		
		//*****Scroll to the Bottom of the Page
		
	    jse.executeScript("window.scrollBy(0,250)", "");
	    
	    //******Click on Continue******\\
	    tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContinueAfterTYC), "Click", "Continue After entering Transfer Cover Details");
	    sleep(3000);


	    

			report.updateTestLog("Personal Details Page", "Sucessfully automated personal details section", Status.PASS);
		
			
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void healthandlifestyledetails() {
		
		String RestrictedIllness = dataTable.getData("MTAA_TransferCover", "RestrictedIllness");
		String DiagnosedIllness = dataTable.getData("MTAA_TransferCover", "RestrictedIllness");
		String LodgeTPDClaim = dataTable.getData("MTAA_TransferCover", "LodgeTPDClaim");
		String Lessthan65 = dataTable.getData("MTAA_TransferCover", "Lessthan65");
		String Fundpremloading = dataTable.getData("MTAA_TransferCover", "Fundpremloading");
			
		WebDriverWait wait=new WebDriverWait(driver,20);
			
			try{
				
			//*******Are you restricted due to illness or injury*********\\
				
				if(RestrictedIllness.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-1:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(800);
				
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-1:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(800);
					
				}
				
				
			//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy 
		    //to less than 3 years?
				
				if(DiagnosedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-2:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
					sleep(800);
				}
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-2:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
					sleep(800);
				}
				
				
				//*****Have you ever been paid, or are you eligible to be paid, or have you lodged a claim for a 
				//TPD benefit from MTAA Super, another superannuation fund or life insurance policy?
				
				if(LodgeTPDClaim.equalsIgnoreCase("Yes")){				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-3:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior TPD Benefit-Yes", Status.PASS);
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-3:0']"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior TPD Benefit-No", Status.PASS);
					sleep(800);
				}
		
				//******Are you aged less than 65 years?*****\\
				
				if(Lessthan65.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-4:0']"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Age less then 65 Years-Yes", Status.PASS);
					sleep(600);
				}
				else{				
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-4:0']"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Age less then 65 Years-No", Status.PASS);
					sleep(600);
				}
				
				//*******Is your cover under the previous fund or individual insurer subject to any premium loadings 
				//and/or exclusions, including but not limited to pre-existing conditions exclusions, or restrictions in 
				//regards to medical or other conditions?
				
				if(Fundpremloading.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-5:0']"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-Yes", Status.PASS);
					sleep(600);
				}
				else{				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-5:0']"))).click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-No", Status.PASS);
					sleep(600);
				}
				
			//******Click on Continue Button********\\
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='applyBtnId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(3500);
			    									
				
				
				
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
					


		private void confirmation() {
			
			
			
			WebDriverWait wait=new WebDriverWait(driver,20);	
				
				try{
					if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){
				
					
					
				//******Agree to general Consent*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(500);
					
				//Load the element before clicking submit
					
					driver.findElement(By.xpath("//li[contains(text(),'The answers to the questions in this application are true and correct, and I have not deliberately withheld any information material to the proposed insurance.  ')]")).click();
					sleep(500);
					
				//******Click on Submit*******\\
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
					report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
					sleep(2000);			
					
				}
				
				//*******Feedback Pop-up******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
				report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
				sleep(1000);
				
				if(driver.getPageSource().contains("Application number")){
				
				
				//*****Fetching the Application Status*****\\
				
				String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
				sleep(500);
				
				//*****Fetching the Application Number******\\
				
				String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
				sleep(1000);
				
				
				
				//******Download the PDF***********\\
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
				//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
				sleep(1500);
				properties = Settings.getInstance();	
				String StrBrowseName=properties.getProperty("currentBrowser");
				System.out.println(StrBrowseName);
			  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
			  			Robot roboobj=new Robot();
			  			roboobj.keyPress(KeyEvent.VK_ENTER);	
			  		}
				
				report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
				
				sleep(1000);
			
				 
				  report.updateTestLog("Confirmation Page", "Application No: "+App,Status.PASS);
				
					
				 report.updateTestLog("Confirmation Page", "Application Status: "+Appstatus,Status.PASS);	
				}

				else{
					 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
				}
				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
			}
			

		public void TransferCoverMTAANegative() {

			String EmailId = dataTable.getData("MTAA_TransferCover", "EmailId");
			String TypeofContact = dataTable.getData("MTAA_TransferCover", "TypeofContact");
			String ContactNumber = dataTable.getData("MTAA_TransferCover", "ContactNumber");
			String TimeofContact = dataTable.getData("MTAA_TransferCover", "TimeofContact");
			String FifteenHoursWork = dataTable.getData("MTAA_TransferCover", "FifteenHoursWork");
			String Citizen = dataTable.getData("MTAA_TransferCover", "Citizen");
			String IndustryType = dataTable.getData("MTAA_TransferCover", "IndustryType");
			String OccupationType = dataTable.getData("MTAA_TransferCover", "OccupationType");
			String OtherOccupation = dataTable.getData("MTAA_TransferCover", "OtherOccupation");
			String OfficeEnvironment = dataTable.getData("MTAA_TransferCover", "OfficeEnvironment");
			String TertiaryQual = dataTable.getData("MTAA_TransferCover", "TertiaryQual");
			String HazardousEnv = dataTable.getData("MTAA_TransferCover", "HazardousEnv");
			String OutsideOfcPercent = dataTable.getData("MTAA_TransferCover", "OutsideOfcPercent");
			String AnnualSalary = dataTable.getData("MTAA_TransferCover", "AnnualSalary");
			String PreviousInsurer = dataTable.getData("MTAA_TransferCover", "PreviousInsurer");
			String PolicyNo = dataTable.getData("MTAA_TransferCover", "PolicyNo");
			String FormerFundNo = dataTable.getData("MTAA_TransferCover", "FormerFundNo");
			String DocumentaryEvidence = dataTable.getData("MTAA_TransferCover", "DocumentaryEvidence");
			String AttachPath = dataTable.getData("MTAA_TransferCover", "AttachPath");
			String CostType = dataTable.getData("MTAA_TransferCover", "CostType");
			String DeathYN = dataTable.getData("MTAA_TransferCover", "Death");
			String TPDYN = dataTable.getData("MTAA_TransferCover", "TPD");
			
			String DeathAmount = dataTable.getData("MTAA_TransferCover", "DeathAmount");
			String TPDAmount = dataTable.getData("MTAA_TransferCover", "TPDAmount");
			String TPDAmtGreater = dataTable.getData("MTAA_TransferCover", "TPDAmtGreater");
			

			
			
			
			try{
				
				
				
				//****Click on Transfer Your Cover Link*****\\
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TransferYourCoverLink), "Click", "Transfer Your Cover Link");
			

				//*****Agree to Duty of disclosure*******\\
				WebDriverWait wait=new WebDriverWait(driver,18);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
				report.updateTestLog("Duty of Disclosure", "selected the Checkbox", Status.PASS);

				
	           //*****Agree to Privacy Statement*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
				report.updateTestLog("Privacy Statement", "selected the Checkbox", Status.PASS);

			

				//*****Enter the Email Id*****\\
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.EmailId), "SET", "Email Id",EmailId);
				

				//*****Click on the Contact Number Type****\\
				if(!TypeofContact.equalsIgnoreCase("Mobile")){
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactTypeClick), "Click", "Preferred Type of Contact: "+TypeofContact);
							
				//*****Choose the Type of Contact*****\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();
				
				}
				
				//****Enter the Contact Number****\\
				waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);
				
				
				
				//***Select the Preferred time of Contact*****\\
				if(TimeofContact.equalsIgnoreCase("Morning")){
					
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");
				}
				else{
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.AfterNoonContact), "Click", "Preferred Contact Time is AfterNoon");
				}
				
				//****Contact Details-Continue Button*****\\			
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");
				sleep(1200);
				
				
				//***************************************\\	
				//**********OCCUPATION SECTION***********\\
				//****************************************\\	
					
					//*****Select the 15 Hours Question*******\\
					if(FifteenHoursWork.equalsIgnoreCase("Yes")){
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.FifteenHoursYes), "Click", "15 Hours a Week-Yes");
					
					}
					else{
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");
					
					}
					
					//*****Select if Resident of Australia****\\
					if(Citizen.equalsIgnoreCase("Yes")){
			
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CitizenYes), "Click", "Australian Citizen-Yes");
						
						}
					else{
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CitizenNo), "Click", "Australian Citizen-No");	
						
						}
					
					//*****Click on Industry********\\
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ClickIndustry), "Click", "Industry");
					sleep(800);
					
					//*****Choose the Industry Type*****\\
					driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();
					sleep(800);
				
					//*****Click On  Occupation*****\\
					
		            tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ClickOccupation), "Click", "Occupation");
		            sleep(800);
		            
		            //****Choose the Occupation Type*****\\
					
					driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();
					sleep(1000);
					
					
					//*****Other Occupation *****\\
					if(OccupationType.equalsIgnoreCase("Other")){
						
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);
						sleep(1000);
					}
					
					
					//*****Extra Questions*****\\
					
					if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){
				String Question=driver.findElement(By.xpath("//*[@id='work_duty_div_id']/div/label")).getText().trim();
				sleep(800);
				
				
				
				if(Question.equalsIgnoreCase("Are your duties entirely undertaken within an office environment?")){
					
				//***Select if your office Duties are Undertaken within an Office Environment?\\
					if(OfficeEnvironment.equalsIgnoreCase("Yes")){
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");
						
					}
					else{
						
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");
						
					}
					
				//*****Do You Hold a Tertiary Qualification******\\
					
					if(TertiaryQual.equalsIgnoreCase("Yes")){				
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");				
					}
					else{				
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");
						sleep(1000);
					}						
				}
					}
					
					
				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){
				
					
					
				//******Do you work in hazardous environment*****\\
					if(HazardousEnv.equalsIgnoreCase("Yes")){				
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");				
					}
					else{				
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");				
					}
					
				//******Do you spend 20% time working outside office*******\\
					if(OutsideOfcPercent.equalsIgnoreCase("Yes")){				
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");				
					}
					else{				
						tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");				
					}						
				}
				
					
				//*****What is your annual Salary******
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);
				sleep(500);
				
				//***Load the Salary Amount****\\
				waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadSalaryLabel).click();
				sleep(500);
				
				
				
				//*****Continue button after entering occupation details*******\\
					
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");
					sleep(1000);
					
		       //*****Continue button after entering occupation details*******\\
					
					
					tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");
					sleep(1000);
				
			//*************************************************************\\	
			//************PREVIOUS COVER SECTION****************************\\
			//**************************************************************\\
			
				//*****Name of previous insurer*****\\	
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.PreviousInsurer), "SET", "Previous Insurer Name",PreviousInsurer);	
			
			
			//*****please enter your fund member of insurance policy number*****\\		
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.PolicyNumber), "SET", "Member Number of Policy Number",PolicyNo);
			
			
			//please enter your former fund USI number*****		
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.USINumber), "SET", "Former Fund USI Number",FormerFundNo);
			
			
			//*****8Do You want to Attach documentary evidence****\\
			if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
				
				waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceYes).click();
				sleep(500);
				
				waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceNo).click();
				sleep(500);
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceYes), "Click", "Documentary Evidence-Yes");
				sleep(1000);
				
				
				//****Attach the file******		
				driver.findElement(By.id("transferCvrFileUploadAttach")).sendKeys(AttachPath);
				sleep(1500);
				
				//*****Let the attachment load*****\\		
				driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
				sleep(1000);
				
				//*****Add the Attachment*****\\
				
				WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1500);
				
				
				
			}
			else{
				
				//****No Attachment *****\\
				
				waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceYes).click();
				sleep(500);
				
				waitForClickableElement(MTAA_TransferYourCoverPageObjects.DocumentEvidenceNo).click();
				sleep(500);
				
				//Transfer check box
				
				tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TransferChkbox), "Click", "Transfer Check Box");
				
			}
			
			
			//******Click on Cost of  Insurance as*******\\
			
			if(!CostType.equalsIgnoreCase("Monthly")){
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.CostofInsurance), "Click", "Select the Cost Of Insurance");
			sleep(800);
			
			//*****Choose the cost of Insurance Type******\\
			
			driver.findElement(By.xpath("//span[contains(text(),'"+CostType+"')]")).click();
			
			
			}
			
			//****Click on Continue Button******\\
			
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.ContinueAfterPreviousCover), "Click", "Continue After entering Previous Cover Details");
			sleep(1000);
			
			
			
			
			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\
			
			//*****Death transfer amount******
			
			if(DeathYN.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.DeathAmount), "SET", "Death transfer amount",DeathAmount);	
			sleep(500);
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadDeathAmount).click();
			}
			
			//*****TPD transfer amount******
			if(TPDYN.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(MTAA_TransferYourCoverPageObjects.TPDAmount), "SET", "TPD transfer amount",TPDAmount);	
			sleep(500);
			waitForClickableElement(MTAA_TransferYourCoverPageObjects.LoadTPDAmount).click();
			}
			sleep(800);
			String TPDError=driver.findElement(By.id("ErrorTransfrTpdAdditional")).getText();
			
			if(TPDError.equalsIgnoreCase(TPDAmtGreater)){
				report.updateTestLog("TPD Amount Greater than Death", "Error Message: "+TPDAmtGreater, Status.FAIL);
			}
			else{
				report.updateTestLog("TPD Amount Greater than Death", "Error message is not displayed", Status.PASS);
			}
				
				
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}



}
