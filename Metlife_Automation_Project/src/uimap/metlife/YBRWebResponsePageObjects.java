package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRWebResponsePageObjects extends PageObjectLibrary{
		
		public static final By btnNewapp = getObjectIdentifaction("ybr.webresponse.new.btn.css");

}
