package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the APPLICANT database table.
 * 
 */
@Entity
@Table(name = "TBL_A_APPLICANT", schema="EAPPDB")
public class Applicant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_A_APPLICANT_SEQ")
	@SequenceGenerator(name="TBL_A_APPLICANT_SEQ",sequenceName = "EAPPDB.TBL_A_APPLICANT_SEQ", initialValue=1, allocationSize=100)
	private long id;

	@Column(length=5)
	private String activeaalstatus;

	@Column(length=5)
	private String activeempstatus;


	@Column(length=10)
	private String annualsalary;

	@Column(length=20)
	private String appcreatestate;

	@Column(length=30)
	private String applicanttype;

	@Column(length=1)
	private String applicantupdate;

	@Column(length=20)
	private String applicationtype;

	@Column(length=5)
	private String australiacitizenship;

	private Timestamp birthdate;

	@Column(length=500)
	private String clientpdfloc;

	@Column(length=50)
	private String clientrefnumber;

	@Column(length=10)
	private String contactnumber;

	@Column(length=6)
	private String contacttype;

	private Timestamp createdate;

	@Column(length=1)
	private String currentlyemployed;

	@Column(length=50)
	private String customerreferencenumber;

	@Column(length=10)
	private String datejoinedfund;

	@Column(nullable=false, length=47)
	private long eapplicationid;

	@Column(length=5)
	private String elitememberindicator;

	@Column(length=50)
	private String emailid;

	@Column(length=1)
	private String exclusionindicator;

	@Column(length=50)
	private String firstname;

	@Column(length=6)
	private String gender;

	@Column(length=4)
	private String heighttype;
	
	@Column(length=2)
	private String age;

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Column(precision=8, scale=2)
	private BigDecimal heightvalue;

	@Column(length=100)
	private String industrytype;

	@Column(length=5)
	private String insuredsalary;

	@Column(length=50)
	private String lastname;

	private Timestamp lastupdatedate;

	@Column(length=1)
	private String loadingindicator;

	@Column(length=5)
	private String longtermipoption;

	@Column(length=50)
	private String membertype;

	@Column(length=5)
	private String nominatedadditionalcover;

	@Column(length=5000)
	private String occupation;

	@Column(length=500)
	private String occupationcode;

	@Column(length=5)
	private String occupationduties;

	@Column(length=5)
	private String overallcoverdecision;

	@Column(length=1)
	private String partnershipstatus;

	@Column(length=50)
	private String permanentemptype;

	@Column(precision=8, scale=2)
	private BigDecimal premium;

	@Column(nullable=false, length=20)
	private String questionnaireid;

	@Column(length=50)
	private String relation;

	@Column(length=5)
	private String residentialstatus;

	@Column(length=1)
	private String salaryindicator;

	@Column(length=1)
	private String salaryinsured;

	@Column(length=5)
	private String smoker;

	@Column(length=5)
	private String spendtimeoutside;

	@Column(length=5)
	private String spousememberindicator;

	@Column(precision=20, scale=2)
	private BigDecimal summation;

	@Column(length=50)
	private String surname;

	@Column(length=10)
	private String title;

	@Column(length=1)
	private String transfercover;

	@Column(length=500)
	private String uwpdfloc;

	@Column(length=5)
	private String weighttype;

	@Column(precision=8, scale=2)
	private BigDecimal weightvalue;

	@Column(length=5)
	private String workduties;

	@Column(length=5)
	private String workhours;
	
	@Column(length=50)
	private String percentcomplete;
	
	public String getPercentcomplete() {
		return percentcomplete;
	}

	@Column(length=50)
	private String previousinsurername;
	
	@Column(length=50)
	private String fundmempolicynumber;
	
	@Column(length=100)
	private String spinnumber;
	
	@Column(length=6)
	private String previoustpdclaimque;
	
	@Column(length=6)
	private String previousterminalillque;
	
	@Column(length=6)
	private String documentevidence;
	
	@Column(length=1)
	private String deathindexationflag;
	
	@Column(length=1)
	private String tpdindexationflag;


	public String getDeathindexationflag() {
		return deathindexationflag;
	}

	public void setDeathindexationflag(String deathindexationflag) {
		this.deathindexationflag = deathindexationflag;
	}

	public String getTpdindexationflag() {
		return tpdindexationflag;
	}

	public void setTpdindexationflag(String tpdindexationflag) {
		this.tpdindexationflag = tpdindexationflag;
	}

	public String getPrevioustpdclaimque() {
		return previoustpdclaimque;
	}

	public void setPrevioustpdclaimque(String previoustpdclaimque) {
		this.previoustpdclaimque = previoustpdclaimque;
	}

	public String getPreviousterminalillque() {
		return previousterminalillque;
	}

	public void setPreviousterminalillque(String previousterminalillque) {
		this.previousterminalillque = previousterminalillque;
	}
	
	public String getPreviousinsurername() {
		return previousinsurername;
	}

	public void setPreviousinsurername(String previousinsurername) {
		this.previousinsurername = previousinsurername;
	}

	public String getFundmempolicynumber() {
		return fundmempolicynumber;
	}

	public void setFundmempolicynumber(String fundmempolicynumber) {
		this.fundmempolicynumber = fundmempolicynumber;
	}

	public String getSpinnumber() {
		return spinnumber;
	}

	public void setSpinnumber(String spinnumber) {
		this.spinnumber = spinnumber;
	}

	public String getDocumentevidence() {
		return documentevidence;
	}

	public void setDocumentevidence(String documentevidence) {
		this.documentevidence = documentevidence;
	}

	public void setPercentcomplete(String percentcomplete) {
		this.percentcomplete = percentcomplete;
	}

	public String getAuraform() {
		return auraform;
	}

	public void setAuraform(String auraform) {
		this.auraform = auraform;
	}

	public String getAuraversion() {
		return auraversion;
	}

	public void setAuraversion(String auraversion) {
		this.auraversion = auraversion;
	}

	@Column(length=20)
	private String auraform;
	
	@Column(length=50)
	private String auraversion;
	
    public Contactinfo getContactinfo() {
		return contactinfo;
	}

	public void setContactinfo(Contactinfo contactinfo) {
		this.contactinfo = contactinfo;
	}


    @OneToOne (cascade=CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name="CONTACTINFO_ID", unique= true, nullable=true, insertable=true, updatable=true)
    private Contactinfo contactinfo;
	
	public List<Cover> getCovers() {
		return covers;
	}

	public void setCovers(List<Cover> covers) {
		this.covers = covers;
	}

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "applicantid")
	private List<Cover> covers;

	public Applicant() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActiveaalstatus() {
		return this.activeaalstatus;
	}

	public void setActiveaalstatus(String activeaalstatus) {
		this.activeaalstatus = activeaalstatus;
	}

	public String getActiveempstatus() {
		return this.activeempstatus;
	}

	public void setActiveempstatus(String activeempstatus) {
		this.activeempstatus = activeempstatus;
	}

	public String getAnnualsalary() {
		return this.annualsalary;
	}

	public void setAnnualsalary(String annualsalary) {
		this.annualsalary = annualsalary;
	}

	public String getAppcreatestate() {
		return this.appcreatestate;
	}

	public void setAppcreatestate(String appcreatestate) {
		this.appcreatestate = appcreatestate;
	}

	public String getApplicanttype() {
		return this.applicanttype;
	}

	public void setApplicanttype(String applicanttype) {
		this.applicanttype = applicanttype;
	}

	public String getApplicantupdate() {
		return this.applicantupdate;
	}

	public void setApplicantupdate(String applicantupdate) {
		this.applicantupdate = applicantupdate;
	}

	public String getApplicationtype() {
		return this.applicationtype;
	}

	public void setApplicationtype(String applicationtype) {
		this.applicationtype = applicationtype;
	}

	public String getAustraliacitizenship() {
		return this.australiacitizenship;
	}

	public void setAustraliacitizenship(String australiacitizenship) {
		this.australiacitizenship = australiacitizenship;
	}

	public Timestamp getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Timestamp birthdate) {
		this.birthdate = birthdate;
	}

	public String getClientpdfloc() {
		return this.clientpdfloc;
	}

	public void setClientpdfloc(String clientpdfloc) {
		this.clientpdfloc = clientpdfloc;
	}

	public String getClientrefnumber() {
		return this.clientrefnumber;
	}

	public void setClientrefnumber(String clientrefnumber) {
		this.clientrefnumber = clientrefnumber;
	}

	public String getContactnumber() {
		return this.contactnumber;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	public String getContacttype() {
		return this.contacttype;
	}

	public void setContacttype(String contacttype) {
		this.contacttype = contacttype;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getCurrentlyemployed() {
		return this.currentlyemployed;
	}

	public void setCurrentlyemployed(String currentlyemployed) {
		this.currentlyemployed = currentlyemployed;
	}

	public String getCustomerreferencenumber() {
		return this.customerreferencenumber;
	}

	public void setCustomerreferencenumber(String customerreferencenumber) {
		this.customerreferencenumber = customerreferencenumber;
	}

	public String getDatejoinedfund() {
		return this.datejoinedfund;
	}

	public void setDatejoinedfund(String datejoinedfund) {
		this.datejoinedfund = datejoinedfund;
	}

	public long getEapplicationid() {
		return this.eapplicationid;
	}

	public void setEapplicationid(long eapplicationid) {
		this.eapplicationid = eapplicationid;
	}

	public String getElitememberindicator() {
		return this.elitememberindicator;
	}

	public void setElitememberindicator(String elitememberindicator) {
		this.elitememberindicator = elitememberindicator;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getExclusionindicator() {
		return this.exclusionindicator;
	}

	public void setExclusionindicator(String exclusionindicator) {
		this.exclusionindicator = exclusionindicator;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHeighttype() {
		return this.heighttype;
	}

	public void setHeighttype(String heighttype) {
		this.heighttype = heighttype;
	}

	public BigDecimal getHeightvalue() {
		return this.heightvalue;
	}

	public void setHeightvalue(BigDecimal heightvalue) {
		this.heightvalue = heightvalue;
	}

	public String getIndustrytype() {
		return this.industrytype;
	}

	public void setIndustrytype(String industrytype) {
		this.industrytype = industrytype;
	}

	public String getInsuredsalary() {
		return this.insuredsalary;
	}

	public void setInsuredsalary(String insuredsalary) {
		this.insuredsalary = insuredsalary;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Timestamp getLastupdatedate() {
		return this.lastupdatedate;
	}

	public void setLastupdatedate(Timestamp lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	public String getLoadingindicator() {
		return this.loadingindicator;
	}

	public void setLoadingindicator(String loadingindicator) {
		this.loadingindicator = loadingindicator;
	}

	public String getLongtermipoption() {
		return this.longtermipoption;
	}

	public void setLongtermipoption(String longtermipoption) {
		this.longtermipoption = longtermipoption;
	}

	public String getMembertype() {
		return this.membertype;
	}

	public void setMembertype(String membertype) {
		this.membertype = membertype;
	}

	public String getNominatedadditionalcover() {
		return this.nominatedadditionalcover;
	}

	public void setNominatedadditionalcover(String nominatedadditionalcover) {
		this.nominatedadditionalcover = nominatedadditionalcover;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getOccupationcode() {
		return this.occupationcode;
	}

	public void setOccupationcode(String occupationcode) {
		this.occupationcode = occupationcode;
	}

	public String getOccupationduties() {
		return this.occupationduties;
	}

	public void setOccupationduties(String occupationduties) {
		this.occupationduties = occupationduties;
	}

	public String getOverallcoverdecision() {
		return this.overallcoverdecision;
	}

	public void setOverallcoverdecision(String overallcoverdecision) {
		this.overallcoverdecision = overallcoverdecision;
	}

	public String getPartnershipstatus() {
		return this.partnershipstatus;
	}

	public void setPartnershipstatus(String partnershipstatus) {
		this.partnershipstatus = partnershipstatus;
	}

	public String getPermanentemptype() {
		return this.permanentemptype;
	}

	public void setPermanentemptype(String permanentemptype) {
		this.permanentemptype = permanentemptype;
	}

	public BigDecimal getPremium() {
		return this.premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	public String getQuestionnaireid() {
		return this.questionnaireid;
	}

	public void setQuestionnaireid(String questionnaireid) {
		this.questionnaireid = questionnaireid;
	}

	public String getRelation() {
		return this.relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getResidentialstatus() {
		return this.residentialstatus;
	}

	public void setResidentialstatus(String residentialstatus) {
		this.residentialstatus = residentialstatus;
	}

	public String getSalaryindicator() {
		return this.salaryindicator;
	}

	public void setSalaryindicator(String salaryindicator) {
		this.salaryindicator = salaryindicator;
	}

	public String getSalaryinsured() {
		return this.salaryinsured;
	}

	public void setSalaryinsured(String salaryinsured) {
		this.salaryinsured = salaryinsured;
	}

	public String getSmoker() {
		return this.smoker;
	}

	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}

	public String getSpendtimeoutside() {
		return this.spendtimeoutside;
	}

	public void setSpendtimeoutside(String spendtimeoutside) {
		this.spendtimeoutside = spendtimeoutside;
	}

	public String getSpousememberindicator() {
		return this.spousememberindicator;
	}

	public void setSpousememberindicator(String spousememberindicator) {
		this.spousememberindicator = spousememberindicator;
	}

	public BigDecimal getSummation() {
		return this.summation;
	}

	public void setSummation(BigDecimal summation) {
		this.summation = summation;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTransfercover() {
		return this.transfercover;
	}

	public void setTransfercover(String transfercover) {
		this.transfercover = transfercover;
	}

	public String getUwpdfloc() {
		return this.uwpdfloc;
	}

	public void setUwpdfloc(String uwpdfloc) {
		this.uwpdfloc = uwpdfloc;
	}

	public String getWeighttype() {
		return this.weighttype;
	}

	public void setWeighttype(String weighttype) {
		this.weighttype = weighttype;
	}

	public BigDecimal getWeightvalue() {
		return this.weightvalue;
	}

	public void setWeightvalue(BigDecimal weightvalue) {
		this.weightvalue = weightvalue;
	}

	public String getWorkduties() {
		return this.workduties;
	}

	public void setWorkduties(String workduties) {
		this.workduties = workduties;
	}

	public String getWorkhours() {
		return this.workhours;
	}

	public void setWorkhours(String workhours) {
		this.workhours = workhours;
	}

}