function coverConfig($stateProvider, $httpProvider) {
  'ngInject';
  $httpProvider.defaults.withCredentials = true;
  $stateProvider
  .state('app.cover', {
    url: '/',
    controller: 'CoverCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'cover/cover.html',
    resolve: {
      urls: function(CommonAPI, AppConstants) {
        return !CommonAPI.getUrlList() && CommonAPI.fetchApiUrls(AppConstants.urlFilePath);
      }
    }
  });

};

export default coverConfig;
