(function(angular){
	'use strict';
    /* global channel, moment */
	angular
		.module('MtaaApp')
		.controller('quotetransfer', quotetransfer);
	quotetransfer.$inject=['$scope','$rootScope','$stateParams','$location','$http','$timeout','$window','persoanlDetailService','MaxLimitService','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'TransferCalculateService', 'NewOccupationService','auraInputService','PersistenceService','ngDialog','auraResponseService','urlService','appData','mtaaUIConstants' ,'$q','$filter', 'saveEapply','Upload','tokenNumService','printQuotePage','DownloadPDFService'];
	function quotetransfer($scope,$rootScope,$stateParams,$location,$http,$timeout,$window,persoanlDetailService,MaxLimitService,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService, TransferCalculateService, NewOccupationService, auraInputService,PersistenceService,ngDialog,auraResponseService, urlService,appData,mtaaUIConstants,$q, $filter, saveEapply, Upload, tokenNumService, printQuotePage, DownloadPDFService){
		
		var vm = this;
		vm.TPD = {
				  'requestType':'TCOVER',
				  'partnerCode':'MTAA',
				  'lastSavedOn':'transferpage',
				  'clientRefNumber':null,           //new
				  'totalMonthlyPremium':null, 
				  'applicatioNumber':null,    //new
				  'ackCheck':false,
				  'privacyCheck':false,
				  'dodCheck':false,
				  'fulCheck':false,
				  'appDecision':'',               //hardcode
				  'clientMatchReason':'',            //new
				  'clientMatch':'',				//new
				  'auraDisabled':true,
				  'responseObject':null,               //new
				  'deathExclusions':'',              //new
				  'tpdExclusions':'',				 //new
				  'ipExclusions':'',                 //new
				  'applicant': {
				    'firstName':'',
				    'lastName':'',
				    'birthDate':'',
				    'emailId':'',
				    'contactType':'',
				    'contactNumber':'',
				    'smoker':'',
				    'title':'',
				    'age':'',
				    'memberType':'',
				    'gender':'',
				    'customerReferenceNumber':'',
				    'clientRefNumber':null,
				    'australiaCitizenship':'',
				    'dateJoinedFund':'',
				    'workHours':'',
				    'industryType':'',
				    'occupationCode':'',
				    'occupation':'',
				    'spendTimeOutside':null,
				    'annualSalary':null,
				    'occupationDuties':null,
				    'otherOccupation':'',
				    'workDuties':null,
				    'ownBusinessQue':'',
				    'ownBusinessYesQue':null,
				    'ownBusinessNoQue':'',
				    'tertiaryQue':null,
				    'permanenTemplQue':'',
				    'thirtyFiveHrsWrkQue':'',
				    'relation':'',
				    'overallCoverDecision':'',
				    'previousInsurerName':'',
				    'fundMemPolicyNumber':'',
				    'spinNumber':'',
				    'previousTpdClaimQue':'',
				    'previousTerminalIllQue':'',
				    'documentEvidence':'',
				    'contactDetails':{
				    	'preferedContactTime':'',
						'mobilePhone': '',
						'workPhone': '',
						'homePhone': '',
						'preferedContactNumber':'',
						'preferedContacType':'',
						'country':'Australia',
						'addressLine1':'',
						'addressLine2':'',
						'postCode':'',
						'state':'',
						'suburb':'',
						'otherContactType':''
						//'otherContactNumber':''
						},
				    'cover':[
				      {
				        'benefitType':'1',
				        'coverDecision':'',
				        'coverReason':'',
				        'existingCoverAmount':null,
				        'fixedUnit':'0',
				        'transferCoverAmount':'',
				        'occupationRating':'',
				        'cost':null,
				        'coverCategory':'',
				        'additionalCoverAmount':0,
				        'frequencyCostType':'Monthly',
				        'optionalUnit':'',
				      },
				      {
				        'benefitType':'2',
				        'coverDecision':'',
				        'coverReason':'',
				        'existingCoverAmount':'',
				        'fixedUnit':'0',
				        'transferCoverAmount':'',
				        'occupationRating':'',
				        'cost':null,
				        'coverCategory':'',
				        'tpdInputTextValue':'',
				        'additionalCoverAmount':0,
				        'frequencyCostType':'Monthly',
				        'optionalUnit':'',
				      },
				      {
				        'benefitType':'4',
				        'coverDecision':'',
				        'coverReason':'',
				        'existingCoverAmount':'',
				        'transferCoverAmount':'',
				        'ipInputTextValue':'',
				        'optionalUnit':'',
				        'existingIpBenefitPeriod':'',
				        'existingIpWaitingPeriod':'',
				        'fixedUnit':'',
				        'occupationRating':'',
				        'cost':0,
				        'coverCategory':'',
				        'additionalCoverAmount':0,
				        'additionalUnit':null,
				        'additionalIpBenefitPeriod':'',
				        'additionalIpWaitingPeriod':'',
				        'totalIpWaitingPeriod':'',
				        'totalIpBenefitPeriod':'',
				        'frequencyCostType':'Monthly'
				      }]
				  },
				  'transferDocuments':{}
				};
		
		vm.constants = {
				image: mtaaUIConstants.path.images,
				phoneNumber: mtaaUIConstants.onboardDetails.phoneNumber,
				emailFormat: mtaaUIConstants.onboardDetails.emailFormat,
				postCodeFormat: mtaaUIConstants.onboardDetails.postCodeFormat,
				contactTypeOption: mtaaUIConstants.onboardDetails.contactTypeOption,
				titleOption: mtaaUIConstants.onboardDetails.titleOption,
				addressTypeOption: mtaaUIConstants.onboardDetails.addressTypeOption,
				stateOptions: mtaaUIConstants.onboardDetails.stateOptions
		};
		
		
		vm.modelOptions = {updateOn: 'blur'};
		//var annualSalForTransUpgradeVal;
		var autoCalculate = false;
		var fetchAppnum = true;
		var appNum;
		vm.TPD.totalMonthlyPremium = 0;
		var deathTransDBCategory, tpdTransDBCategory, ipTransDBCategory;
		vm.showWithinOfficeTransferQuestion = false;
		vm.showTertiaryTransferQuestion = false;
		vm.showHazardousTransferQuestion = false;
		vm.showOutsideOffice = false;
		vm.isDeathDisabled = false;
		vm.isTPDDisabled = false;
		vm.isIPDisabled = false;
		vm.isIPCoverRequiredDisabled = false;
		vm.fileNotUploadedError = false;
		
		
		vm.init = function() {

			var defer = $q.defer();
			vm.urlList = urlService.getUrlList();
			
			vm.premiumFrequencyOptions = ['Monthly', 'Yearly', 'Weekly'];
			vm.waitingPeriodTransOptions = ['14 Days', '30 Days', '45 Days', '60 Days', '90 Days', 'Not Listed'];
		    vm.benefitPeriodTransOptions = ['2 Years', '5 Years', 'Age 60', 'Age 65', 'Not Listed'];

		    vm.waitingPeriodTransAdlnOptions = ['30 Days', '60 Days', '90 Days'];
		    vm.benefitPeriodTransAdlnOptions = ['2 Years', '5 Years','Age 65'];
		    
			vm.inputDetails = persoanlDetailService.getMemberDetails();
			vm.userAge = parseInt(moment().diff(moment(vm.inputDetails.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
			vm.ageLimit=vm.userAge;
			
			//setting value to JSON
			vm.TPD.applicant.memberType = vm.inputDetails.memberType;
			vm.TPD.applicant.firstName = vm.inputDetails.personalDetails.firstName;
			vm.TPD.applicant.lastName = vm.inputDetails.personalDetails.lastName;
			vm.TPD.applicant.gender = vm.inputDetails.personalDetails.gender;
			vm.TPD.applicant.birthDate =  vm.inputDetails.personalDetails.dateOfBirth;
			vm.TPD.applicant.dateJoinedFund = vm.inputDetails.dateJoined;
			vm.TPD.applicant.emailId = vm.inputDetails.contactDetails.emailAddress;
			
			//address
			vm.TPD.applicant.contactDetails.preferedContactTime = vm.inputDetails.contactDetails.prefContactTime;
			vm.TPD.applicant.contactDetails.preferedContacType = vm.inputDetails.contactDetails.prefContact;
			
			//documentAddress 
			vm.TPD.applicant.documentAddress = 'Postal address\n\nMTAA Super\nLocked Bag 5134\nParramatta\nNSW 2124';
			
			vm.TPD.applicant.contactDetails.addressLine1 = vm.inputDetails.address.line1;
			vm.TPD.applicant.contactDetails.addressLine2 = vm.inputDetails.address.line2;
			vm.TPD.applicant.contactDetails.postCode = vm.inputDetails.address.postCode;
			vm.TPD.applicant.contactDetails.country = vm.inputDetails.address.country;
			vm.TPD.applicant.contactDetails.state = vm.inputDetails.address.state;
			vm.TPD.applicant.contactDetails.suburb = vm.inputDetails.address.suburb;
			
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
			vm.TPD.applicant.customerReferenceNumber  = vm.inputDetails.contactDetails.fundEmailAddress;
			
				
			vm.TPD.clientRefNumber = vm.inputDetails.clientRefNumber;
			vm.TPD.applicant.clientRefNumber = parseInt(vm.inputDetails.clientRefNumber);
			
			 vm.deathCoverTransDetails = deathCoverService.getDeathCover();
			 vm.tpdCoverTransDetails = tpdCoverService.getTpdCover();
			 vm.ipCoverTransDetails = ipCoverService.getIpCover();
			
			if(vm.deathCoverTransDetails.amount){
				vm.TPD.applicant.cover[0].existingCoverAmount = vm.deathCoverTransDetails.amount;
		    }
			if(vm.deathCoverTransDetails.units){
				vm.TPD.applicant.cover[0].fixedUnit = vm.deathCoverTransDetails.units;
		    }
			if(vm.tpdCoverTransDetails.amount){
				vm.TPD.applicant.cover[1].existingCoverAmount = vm.tpdCoverTransDetails.amount;
		    }
			if(vm.tpdCoverTransDetails.units){
				vm.TPD.applicant.cover[1].fixedUnit = vm.tpdCoverTransDetails.units;
		    }
			if(vm.ipCoverTransDetails.amount){
				vm.TPD.applicant.cover[2].existingCoverAmount = vm.ipCoverTransDetails.amount;
				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].totalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].totalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
		    }
			if(vm.ipCoverTransDetails.units){
				vm.TPD.applicant.cover[2].fixedUnit = vm.ipCoverTransDetails.units;
			}
			
			QuoteService.getList(vm.urlList.quoteUrl, 'MTAA').then(function(res) {
				vm.IndustryOptions = res.data;
				},function(err) {
					console.log('Error while getting industry options '+ JSON.stringify(err));
			});
			
			/*if (vm.deathCoverTransDetails.type == '1' && vm.tpdCoverTransDetails.type == '1') {
				vm.unitRoundMsg = 'The total cover amount has been rounded up to the nearest number of units. ';
				vm.roundingIndDeath = false;
				vm.roundingIndTPD = false;
			}*/
			
			MaxLimitService.getMaxLimits(vm.urlList.maxLimitUrl, 'MTAA',vm.inputDetails.memberType, 'TCOVER').then(function(res) {
				var limits = res.data;
				vm.annualSalForTransUpgradeVal = limits[0].annualSalForUpgradeVal;
				defer.resolve(res);
				},function(error) {
					console.log('Something went wrong while fetching limits '+ error);
					defer.reject(error);
		   });
			return defer.promise;
		};
		
		vm.init().then(function() {
			
			if($stateParams.mode === '3' || $stateParams.mode === '2'){
				autoCalculate = true;
				vm.TPD = PersistenceService.getTransferCoverDetails();
				
				vm.TPD.applicant.industryType = vm.TPD.applicant.industryType === '' ? null : vm.TPD.applicant.industryType;
	            OccupationService.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.TPD.applicant.industryType).then(function(res) {
	                vm.OccupationList = res.data;
	            }, function(err) {
	                console.info('Error while fetching occupations ' + JSON.stringify(err));
	            });
	            
	            vm.getTransCategoryFromDB();
				
				$timeout(function() {
					$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('show');
					$('#OccupationSection').collapse('show');
					$('#PreviousCoverSection').collapse('show');
					$('#CoverSection').collapse('show');
					
					if(vm.TPD.applicant.documentEvidence === 'No'){
		            	vm.TPD.ackCheck = true;
		            	$('#acknowledgeDocAdressCheck').addClass('active');
		            }else{
		            	vm.TPD.ackCheck = false;
		            	$('#acknowledgeDocAdressCheck').removeClass('active');
		            }
					
					//console.log("Open the panel");
					
				}, 2000);
				
				vm.TPD.totalMonthlyPremium = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost) + parseFloat(vm.TPD.applicant.cover[2].cost);
				vm.files = vm.TPD.transferDocuments;
	            vm.uploadedFiles = vm.files;
	            PersistenceService.setUploadedFileDetails(vm.files);
				
			}else{
				
				$('#collapseprivacy').collapse('hide');
				$('#PersonalDetailsSection').collapse('hide');
				$('#OccupationSection').collapse('hide');
				$('#PreviousCoverSection').collapse('hide');
				$('#CoverSection').collapse('hide');
			}
			
			if(vm.deathCoverTransDetails && vm.deathCoverTransDetails.benefitType && vm.deathCoverTransDetails.benefitType === '1'){
				if(vm.ageLimit <= 11 || vm.ageLimit > 70){
					$('#deathsection').removeClass('active');
					$('#death').css('display', 'none');
					vm.isDeathDisabled = true;
				}
			}
			if(vm.tpdCoverTransDetails && vm.tpdCoverTransDetails.benefitType && vm.tpdCoverTransDetails.benefitType === '2'){
				if(vm.ageLimit <= 11 || vm.ageLimit > 65){
					$('#tpdsection').removeClass('active');
					$('#tpd').css('display', 'none');
					vm.isTPDDisabled = true;
				}
			}
			if(vm.ipCoverTransDetails && vm.ipCoverTransDetails.benefitType && vm.ipCoverTransDetails.benefitType === '4'){
				if(vm.ageLimit < 15 || vm.ageLimit > 65){
					$('#ipsection').removeClass('active');
					$('#sc').css('display', 'none');
					vm.isIPDisabled = true;
				}
			}
			
		});
		
		vm.changePrefContactType = function(){
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
		};
		
		vm.checkDodState = function() {
			vm.TPD.dodCheck = !vm.TPD.dodCheck;
        };

        vm.checkPrivacyState = function() {
        	vm.TPD.privacyCheck =  !vm.TPD.privacyCheck;
        };
        
        vm.getOccupations = function(){
	    	vm.TPD.applicant.occupation = '';
            vm.TPD.applicant.otherOccupation = '';
            vm.TPD.applicant.industryType = vm.TPD.applicant.industryType === '' ? null : vm.TPD.applicant.industryType;
            OccupationService.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.TPD.applicant.industryType).then(function(res) {
                vm.OccupationList = res.data;
                vm.TPD.applicant.occupation = '';
            }, function(err) {
                console.info('Error while fetching occupations ' + JSON.stringify(err));
            });
	    };
	    
	    vm.autoCalculate = function(){
	    	
	    	vm.TPD.applicant.cover[1].frequencyCostType = vm.TPD.applicant.cover[0].frequencyCostType;
    		vm.TPD.applicant.cover[2].frequencyCostType = vm.TPD.applicant.cover[0].frequencyCostType;

	    	if(autoCalculate){
	        	vm.calculateTransfer();
	        }
	    	
	    };
	    
	    vm.calculateTransfer = function(){
        	
        	vm.lowerThanExitingFlag = false;
        	
        	if(vm.TPD.applicant.cover[0].additionalCoverAmount !== ''){
        		vm.TPD.applicant.cover[0].additionalCoverAmount = parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount);
        	}
        	
        	if(vm.TPD.applicant.cover[1].additionalCoverAmount !== ''){
        		vm.TPD.applicant.cover[1].additionalCoverAmount = parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount);
        	}
        	
        	if(vm.TPD.applicant.cover[2].additionalCoverAmount !== ''){
        		vm.TPD.applicant.cover[2].additionalCoverAmount = parseInt(vm.TPD.applicant.cover[2].additionalCoverAmount);
        	}
        	
        	if((vm.deathCoverTransDetails && vm.deathCoverTransDetails.amount && typeof(vm.deathCoverTransDetails.amount) === undefined) || vm.deathCoverTransDetails.amount === ''){
        		vm.deathCoverTransDetails.amount = 0;
        	}
        	
        	if((vm.tpdCoverTransDetails && vm.tpdCoverTransDetails.amount && typeof(vm.tpdCoverTransDetails.amount) === undefined) || vm.tpdCoverTransDetails.amount === ''){
        		vm.tpdCoverTransDetails.amount = 0;
        	}
        	
        	if((vm.ipCoverTransDetails && vm.ipCoverTransDetails.amount && typeof(vm.ipCoverTransDetails.amount) === undefined) || vm.ipCoverTransDetails.amount === ''){
        		vm.ipCoverTransDetails.amount = 0;
        	}
        	
        	//vm.calculateForUnits();
        	
        	if(vm.deathCoverTransDetails.type === '1' && vm.tpdCoverTransDetails.type === '1'){
        		vm.calculateForUnits();
        	}else{   
        		vm.calculateForFixed();
        	}
   };
   
   vm.validateAll = function(){
	   
	   if(!vm.isDeathDisabled){
		   vm.validateDeathMaxAmount();
	   }
	   
	   if(!vm.isTPDDisabled){
		   vm.validateTpdMaxAmount();
	   }
	   
	   /*if(!vm.isIPDisabled){
		   vm.validateIpMaxAmount(true);
	   }*/
  };
  
  vm.validateDeathMaxAmount = function(){
      
	  vm.maxDeathErrorFlag = false;
	  vm.maxDeathCoverErrorFlag = false;
	  
      if(parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount) > 1000000) {
    	  vm.maxDeathErrorFlag = true;
      }else if(parseInt(vm.TPD.applicant.cover[0].transferCoverAmount) > 5000000) {
          vm.maxDeathCoverErrorFlag = true;
      }
    
  };
     
     vm.validateTpdMaxAmount = function(){
    	 
    	 $timeout(function() {
    		 vm.maxTpdErrorFlag = false;
    		 vm.maxTpdCoverErrorFlag = false;
    		 vm.TPDcoverAmtErrFlag = false;
   		  
   		  if(parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount) > 0 && parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount) <= 0){
   			  vm.TPDcoverAmtErrFlag = true;
   		  }else if(parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount) > 1000000){
   			  vm.maxTpdErrorFlag = true;
   		  }else if(parseInt(vm.TPD.applicant.cover[1].transferCoverAmount) > 3000000) {
   	          vm.maxTpdCoverErrorFlag = true;
   	      }
      });
  };
  
  vm.calculateForFixed = function(){
	  var ipcoverAmount = parseInt(vm.TPD.applicant.cover[2].additionalCoverAmount) + parseInt(vm.TPD.applicant.cover[2].existingCoverAmount);
	  
	  var ruleModel = {
			  'age': vm.userAge,
			  'fundCode': 'MTAA',
			  'gender': vm.TPD.applicant.gender,
			  'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
			  'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
			  'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
			  'manageType': 'TCOVER',
			  'deathCoverType': 'DcFixed',
			  'tpdCoverType': 'TpdFixed',
			  'ipCoverType': 'IpFixed',
			  'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
			  'ipWaitingPeriod': vm.TPD.applicant.cover[2].totalIpWaitingPeriod,
			  'ipBenefitPeriod': vm.TPD.applicant.cover[2].totalIpBenefitPeriod,
			  'deathTransferAmount': parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount),
			  'tpdTransferAmount': parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount),
			  'ipTransferAmount': parseInt(ipcoverAmount),
			  'deathExistingAmount': parseInt(vm.TPD.applicant.cover[0].existingCoverAmount),
			  'tpdExistingAmount': parseInt(vm.TPD.applicant.cover[1].existingCoverAmount),
			  'ipExistingAmount': parseInt(vm.TPD.applicant.cover[2].existingCoverAmount)	 
	  };
	  
	  console.log(JSON.stringify(ruleModel));
	   
	   TransferCalculateService.calculate(ruleModel,vm.urlList.transferCalculateUrl).then(function(res){
		   
		   var premium = res.data;
		   autoCalculate = true;
		   
		   for(var i = 0; i < premium.length; i++){
			   if(premium[i].coverType === 'DcFixed'){
				   vm.TPD.applicant.cover[0].transferCoverAmount = premium[i].coverAmount;
				   vm.TPD.applicant.cover[0].cost = premium[i].cost || 0;
				   vm.TPD.applicant.cover[0].coverCategory = 'DcFixed';
	        		
			   }else if(premium[i].coverType === 'TpdFixed'){
				   vm.TPD.applicant.cover[1].transferCoverAmount = premium[i].coverAmount;
				   vm.TPD.applicant.cover[1].cost = premium[i].cost || 0;
				   vm.TPD.applicant.cover[1].coverCategory = 'TPDFixed';
	        		
			   }else if(premium[i].coverType === 'IpFixed'){
				   vm.TPD.applicant.cover[2].transferCoverAmount = premium[i].coverAmount;
				   vm.TPD.applicant.cover[2].cost = premium[i].cost || 0;
				   vm.TPD.applicant.cover[2].coverCategory = 'IpFixed';
			   }
			}
		   
		   vm.TPD.totalMonthlyPremium = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost) + parseFloat(vm.TPD.applicant.cover[2].cost);
		   if(parseInt(vm.TPD.applicant.cover[2].transferCoverAmount) < parseInt(vm.ipCoverTransDetails.amount)){
			   vm.lowerThanExitingFlag = true;
		   }
		   
		   if(fetchAppnum){
			   fetchAppnum = false;
			   appNum = PersistenceService.getAppNumber();
			   vm.TPD.applicatioNumber = appNum;
		   }
		   
		   vm.validateAll();
		   console.log(vm.TPD.applicant.cover[0].transferCoverAmount, vm.TPD.applicant.cover[1].transferCoverAmount, vm.TPD.applicant.cover[2].transferCoverAmount);
		   console.log(vm.TPD.applicant.cover[0].cost, vm.TPD.applicant.cover[1].cost, vm.TPD.applicant.cover[2].cost);
	   
	   }, function(err){
		console.info('Something went wrong while calculating...' + JSON.stringify(err));
	});
	  
	  
	  };
  
  vm.calculateForUnits = function() {
	   
	   var deathtotal = parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount) + parseInt(vm.deathCoverTransDetails.amount);
	   var tpdtotal = parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount) + parseInt(vm.tpdCoverTransDetails.amount);
	   var ipcoverAmount = parseInt(vm.TPD.applicant.cover[2].additionalCoverAmount) + parseInt(vm.TPD.applicant.cover[2].existingCoverAmount);
	   
	   vm.roundingIndDeath = false;
	   vm.roundingIndTPD = false;
	   
	   var ruleModel = {
			   'age': vm.userAge,
			   'fundCode': 'MTAA',
			   'gender': vm.TPD.applicant.gender,
			   'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
			   'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
			   'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
			   'manageType': 'TCOVER',
			   'deathCoverType': 'DcUnitised',
			   'tpdCoverType': 'TPDUnitised',
			   'ipCoverType': 'IpUnitised',
			   'deathUnits': 1,
			   'tpdUnits': 1,
			   'ipUnits': 1,
			   'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
			   'ipWaitingPeriod': vm.TPD.applicant.cover[2].totalIpWaitingPeriod,
			   'ipBenefitPeriod': vm.TPD.applicant.cover[2].totalIpBenefitPeriod,
			   'deathFixedAmount': parseInt(deathtotal),
			   'tpdFixedAmount': parseInt(tpdtotal),
			   'ipTransferAmount': parseInt(ipcoverAmount),
			   'ipExistingAmount': parseInt(vm.ipCoverTransDetails.amount)
			   
	   };
	   
	   console.log(JSON.stringify(ruleModel));
	   
	   TransferCalculateService.calculate(ruleModel,vm.urlList.transferCalculateUrl).then(function(res){
		   
		   var coverAmtPerUnit = 0;
		   var premium = res.data;
		   
		   for(var i = 0; i < premium.length; i++){
			   
			   if(premium[i].coverType === 'DcUnitised'){
				   
				   coverAmtPerUnit = premium[i].coverAmount || 0;
				   if(coverAmtPerUnit > 0){
					   vm.TPD.applicant.cover[0].additionalUnit = Math.floor(deathtotal/coverAmtPerUnit);
					   
					   if(vm.TPD.applicant.cover[0].additionalUnit > 0 && (deathtotal%coverAmtPerUnit !== 0)){
						   vm.TPD.applicant.cover[0].additionalUnit = vm.TPD.applicant.cover[0].additionalUnit + 1;
						   vm.roundingIndDeath = true;
					  }else{
					  	 vm.TPD.applicant.cover[0].additionalUnit = 1;
					  }
				  }else{
					  vm.TPD.applicant.cover[0].additionalUnit = 0;
				  }
			
			   }else if(premium[i].coverType === 'TPDUnitised'){
				   coverAmtPerUnit = premium[i].coverAmount || 0;
				   
				   if(coverAmtPerUnit > 0){
					   vm.TPD.applicant.cover[1].additionalUnit = Math.floor(tpdtotal/coverAmtPerUnit);
					   
					   if(vm.TPD.applicant.cover[1].additionalUnit > 0 && (tpdtotal % coverAmtPerUnit !== 0)){
						   vm.TPD.applicant.cover[1].additionalUnit = vm.TPD.applicant.cover[1].additionalUnit + 1;
						   vm.roundingIndTPD = true;
	        			}else{
						  	 vm.TPD.applicant.cover[1].additionalUnit = 1;
						  }
	        		}else{
	        			vm.TPD.applicant.cover[1].additionalUnit = 0;
	        		}
			  }else if(premium[i].coverType === 'IpUnitised'){
				   coverAmtPerUnit = premium[i].coverAmount || 0;
				   
				   if(coverAmtPerUnit > 0){
					   vm.TPD.applicant.cover[2].additionalUnit = Math.ceil(ipcoverAmount/250);
	        		}else{
	        			vm.TPD.applicant.cover[2].additionalUnit = 0;
	        		}
			  }
			   
			   //vm.calculateUnits(); 
	      }
		   vm.calculateUnits(); 
	}, function(err){
		console.info('Something went wrong while calculating...' + JSON.stringify(err));
	});
};

vm.calculateUnits = function() {
	   
	   var deathtotal = parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount) + parseInt(vm.deathCoverTransDetails.amount);
	   var tpdtotal = parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount) + parseInt(vm.tpdCoverTransDetails.amount);
	   var ipcoverAmount = parseInt(vm.TPD.applicant.cover[2].additionalCoverAmount) + parseInt(vm.TPD.applicant.cover[2].existingCoverAmount);
	   
	   var ruleModel = {
        'age': vm.userAge,
        'fundCode': 'MTAA',
        'gender': vm.TPD.applicant.gender,
        'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
        'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
        'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
        'manageType': 'TCOVER',
        'deathUnits': vm.TPD.applicant.cover[0].additionalUnit,
        'deathFixedAmount': parseInt(deathtotal),
        'tpdUnits': vm.TPD.applicant.cover[1].additionalUnit,
        'tpdFixedAmount': parseInt(tpdtotal),
        'deathCoverType': 'DcUnitised',
        'tpdCoverType': 'TPDUnitised',
        'ipCoverType': 'IpUnitised',
        'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
        'ipWaitingPeriod': vm.TPD.applicant.cover[2].totalIpWaitingPeriod,
        'ipBenefitPeriod': vm.TPD.applicant.cover[2].totalIpBenefitPeriod,
        'ipTransferAmount': parseInt(ipcoverAmount),
        'ipExistingAmount': parseInt(vm.ipCoverTransDetails.amount)
      };
	   
	   console.log(JSON.stringify(ruleModel));
	   
	   TransferCalculateService.calculate(ruleModel,vm.urlList.transferCalculateUrl).then(function(res){
		   
		   var premium = res.data;
		   autoCalculate = true;
		   
		   //console.log(JSON.stringify(res.data));
		   
		   for(var i = 0; i < premium.length; i++){
			   if(premium[i].coverType === 'DcUnitised'){
				   vm.TPD.applicant.cover[0].transferCoverAmount = premium[i].coverAmount;
				   vm.TPD.applicant.cover[0].cost = premium[i].cost || 0;
				   vm.TPD.applicant.cover[0].coverCategory = 'DcUnitised';
			   }else if(premium[i].coverType === 'TPDUnitised'){
				   vm.TPD.applicant.cover[1].transferCoverAmount = premium[i].coverAmount;
				   vm.TPD.applicant.cover[1].cost = premium[i].cost || 0;
				   vm.TPD.applicant.cover[1].coverCategory = 'TPDUnitised';
			   }else if(premium[i].coverType === 'IpUnitised'){
				   vm.TPD.applicant.cover[2].transferCoverAmount = premium[i].coverAmount;
				   vm.TPD.applicant.cover[2].cost = premium[i].cost || 0;
				   vm.TPD.applicant.cover[2].coverCategory = 'IpUnitised';
			   }
			}
		   
		   vm.TPD.totalMonthlyPremium = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost) + parseFloat(vm.TPD.applicant.cover[2].cost);
		   if(parseInt(vm.TPD.applicant.cover[2].transferCoverAmount) < parseInt(vm.ipCoverTransDetails.amount)){
			   vm.lowerThanExitingFlag = true;
		   }
		   
		   if(fetchAppnum){
			   fetchAppnum = false;
			   appNum = PersistenceService.getAppNumber();
			   vm.TPD.applicatioNumber = appNum;
		   }
		   
		   vm.validateAll();
		   console.log(vm.TPD.applicant.cover[0].transferCoverAmount, vm.TPD.applicant.cover[1].transferCoverAmount, vm.TPD.applicant.cover[2].transferCoverAmount);
		   console.log(vm.TPD.applicant.cover[0].cost, vm.TPD.applicant.cover[1].cost, vm.TPD.applicant.cover[2].cost);
	   }, function(err){
		   console.log('Error while calculating transfer premium ' + JSON.stringify(err));
	  });
	   
	   
};

	vm.invalidSalAmount = false;
	vm.checkValidSalary = function(){
		
		vm.validateIpMaxAmount(true);
		vm.renderOccupationQuestions();
		
		$timeout(function() {
			vm.invalidSalAmount = false;
			if(parseInt(vm.TPD.applicant.annualSalary) === 0){
				vm.invalidSalAmount = true;
			}
		},100);
	};
	
	 vm.validateIpMaxAmount = function(checkAuto){
   	  	 var ipMaxAmount = 10000;
   	  	 var ipMaxCoverAmount = 25000;
   	  	 var maxAllowedCoverAmount;
         vm.maxIpErrorFlag = false;
         
         if(vm.TPD.applicant.cover[2].additionalCoverAmount > ipMaxAmount){
        	 vm.maxIpErrorFlag = true;
        	 vm.TPD.applicant.cover[2].additionalCoverAmount = ipMaxAmount;
         }
         
         var ninetyPercentOfSal = parseInt(0.85 * (parseInt(vm.TPD.applicant.annualSalary)/12));
         var tempTotalCover = parseInt(vm.TPD.applicant.cover[2].additionalCoverAmount) + parseInt(vm.TPD.applicant.cover[2].existingCoverAmount);
         
         if(parseInt(ipMaxCoverAmount) > parseInt(ninetyPercentOfSal)){
        	 maxAllowedCoverAmount = ninetyPercentOfSal;
         }else{
        	 maxAllowedCoverAmount = ipMaxCoverAmount;
         }
         
         if(parseInt(vm.TPD.applicant.cover[2].existingCoverAmount) > maxAllowedCoverAmount){
        	 vm.maxIpErrorFlag = true;
        	 vm.TPD.applicant.cover[2].additionalCoverAmount = 0; 
         }else if(tempTotalCover > maxAllowedCoverAmount){
        	 vm.maxIpErrorFlag = true;
        	 var finalAmount = parseInt(maxAllowedCoverAmount) - parseInt(vm.TPD.applicant.cover[2].existingCoverAmount);
        	 if(finalAmount > 0){
        		 vm.TPD.applicant.cover[2].additionalCoverAmount = finalAmount;
        	 }else{
              	 vm.TPD.applicant.cover[2].additionalCoverAmount = 0;
        	 }
         }
         
         if(!checkAuto){
       	  	vm.autoCalculate();
          }
       };

    vm.changeWaitingPeriod = function() {
        if((vm.TPD.applicant.cover[2].additionalIpWaitingPeriod === '14 Days') || (vm.TPD.applicant.cover[2].additionalIpWaitingPeriod === '30 Days')){
       	 vm.TPD.applicant.cover[2].totalIpWaitingPeriod = '30 Days';
        }else  if((vm.TPD.applicant.cover[2].additionalIpWaitingPeriod === '45 Days') || (vm.TPD.applicant.cover[2].additionalIpWaitingPeriod === '60 Days')){
       	 vm.TPD.applicant.cover[2].totalIpWaitingPeriod = '60 Days';
        }else if(vm.TPD.applicant.cover[2].additionalIpWaitingPeriod === '90 Days'){
       	 vm.TPD.applicant.cover[2].totalIpWaitingPeriod = '90 Days';
        } else if(vm.TPD.applicant.cover[2].additionalIpWaitingPeriod === 'Not Listed'){
       	 vm.TPD.applicant.cover[2].totalIpWaitingPeriod = '90 Days';
        }
        vm.autoCalculate();
         };
	
	vm.changeBenefitPeriod = function() {
        $timeout(function(){
      	  if((vm.TPD.applicant.cover[2].additionalIpBenefitPeriod === '2 Years')){
      		vm.TPD.applicant.cover[2].totalIpBenefitPeriod = '2 Years';
          } else if(vm.TPD.applicant.cover[2].additionalIpBenefitPeriod === '5 Years'){
        	  vm.TPD.applicant.cover[2].totalIpBenefitPeriod = '5 Years';
          } else if(vm.TPD.applicant.cover[2].additionalIpBenefitPeriod === 'Not Listed'){
        	  vm.TPD.applicant.cover[2].totalIpBenefitPeriod = '2 Years';
          } else if(vm.TPD.applicant.cover[2].additionalIpBenefitPeriod === 'Age 65'){
        	  vm.TPD.applicant.cover[2].totalIpBenefitPeriod = 'Age 65';
          } else if(vm.TPD.applicant.cover[2].additionalIpBenefitPeriod === 'Age 60'){
        	  vm.TPD.applicant.cover[2].totalIpBenefitPeriod = '2 Years';
          }
          vm.autoCalculate();
        }, 10);
      };
      
      vm.checkForFileUpload = function(){
    	  if(vm.files.length > 0){
    		  vm.fileNotUploadedError = false;
    	  }else{
    		  if(vm.TPD.applicant.documentEvidence === 'Yes'){
    			  vm.fileNotUploadedError = true;            
    		  }else{
    			  vm.fileNotUploadedError = false;
    		  }
    	  }  
      };
      
      vm.CoverDetailsTransferFormSubmit =  function (){
    	  
    	  vm.checkForFileUpload();
    	  
    	  if(!vm.fileNotUploadedError && !vm.invalidSalAmount && !vm.maxDeathErrorFlag && !vm.maxDeathCoverErrorFlag && !vm.maxTpdErrorFlag && !vm.maxTpdCoverErrorFlag && !vm.TPDcoverAmtErrFlag){
    		  vm.calculateTransfer();
    	  }
        };
	
	vm.goToAura = function(){
		
		vm.checkForFileUpload();
		 
		 if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && vm.previousCoverForm.$valid && vm.TranscoverCalculatorForm.$valid){
			 
			 if(!vm.fileNotUploadedError && !vm.invalidSalAmount && !vm.maxDeathErrorFlag && !vm.maxDeathCoverErrorFlag && !vm.maxTpdErrorFlag && !vm.maxTpdCoverErrorFlag && !vm.TPDcoverAmtErrFlag){
				 $timeout(function(){
					 vm.saveDataForPersistence().then(function() {
						 $location.path('/auratransfer/1');
					 }, function(err) {});
				}, 10);
	
	        }else{
	        	return false;
	        }
	    }
	  };
	  
	  vm.getTransCategoryFromDB = function(fromOccupation){
		  
		  if(vm.otherOccupationObj){
			  vm.otherOccupationObj.transferotherOccupation = '';
		  }
		  
		  if(vm.TPD.applicant.occupation !== undefined){
			  
			  var occName = vm.TPD.applicant.industryType + ':' + vm.TPD.applicant.occupation;
			  
			  NewOccupationService.getOccupation(vm.urlList.newOccupationUrl, 'MTAA', occName).then(function(res){
				  deathTransDBCategory = res.data[0].deathfixedcategeory;
		          tpdTransDBCategory = res.data[0].tpdfixedcategeory;
		          ipTransDBCategory = res.data[0].ipfixedcategeory;
		          vm.renderOccupationQuestions();
		      }, function(err){
		    	  console.info('Error while getting transfer category from DB ' + JSON.stringify(err));
		        });
		   } 
	  };
	  
	  vm.toggleIPCondition = function(){
	    	console.log('no clicked');
	    	if (vm.TPD.applicant.workHours === 'No') {
	    		vm.additionalIpWaitingPeriodDisabled = true;
	    		vm.additionalIpBenefitPeriodDisabled = true;
	    		vm.isIPCoverRequiredDisabled = true;
	    	}else{
	    		if (vm.TPD.applicant.workHours === 'Yes') {
	    		vm.additionalIpWaitingPeriodDisabled = false;
	    		vm.additionalIpBenefitPeriodDisabled = false;
	    		vm.isIPCoverRequiredDisabled = false; 
	    		}
	    	}
	    };
	    
	  vm.renderOccupationQuestions = function(){
		  
		  //var occupationDetailsTransferFormFields;
		  /*vm.TPD.applicant.cover[0].occupationRating = vm.deathCoverTransDetails.occRating === '' ? 'General' : vm.deathCoverTransDetails.occRating;
	      vm.TPD.applicant.cover[1].occupationRating = vm.tpdCoverTransDetails.occRating === '' ? vm.deathCoverTransDetails.occRating : vm.tpdCoverTransDetails.occRating;
	      vm.TPD.applicant.cover[2].occupationRating = vm.ipCoverTransDetails.occRating === '' ? vm.deathCoverTransDetails.occRating : vm.ipCoverTransDetails.occRating;*/
		  
		  if (vm.TPD.applicant.workHours === 'No') {
	    		vm.additionalIpWaitingPeriodDisabled = true;
	    		vm.additionalIpBenefitPeriodDisabled = true;
	    		vm.isIPCoverRequiredDisabled = true;
	    	}else{
	    		if (vm.TPD.applicant.workHours === 'Yes') {
	    		vm.additionalIpWaitingPeriodDisabled = false;
	    		vm.additionalIpBenefitPeriodDisabled = false;
	    		vm.isIPCoverRequiredDisabled = false;
	    		}
	    	}
		  
		  if(vm.OccupationList){
			  var selectedOcc = vm.OccupationList.filter(function(obj){
				  return obj.occupationName === vm.TPD.applicant.occupation;
	          });
	          var selectedOccObj = selectedOcc[0];
	          vm.occupUpgradeNotEligible = false;
	          if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
	              vm.showWithinOfficeTransferQuestion = true;
	        	  vm.showTertiaryTransferQuestion = true;
	              vm.showHazardousTransferQuestion = false;
	              vm.showOutsideOffice = false;
	              
	              vm.TPD.applicant.spendTimeOutside = '';
	              vm.TPD.applicant.workDuties = '';
	              
	          
	          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'true'){
	              vm.showWithinOfficeTransferQuestion = false;
	              vm.showTertiaryTransferQuestion = false;
	              vm.showHazardousTransferQuestion = true;
	              vm.showOutsideOffice = true;
	              
	              vm.TPD.applicant.occupationDuties = '';
	              vm.TPD.applicant.tertiaryQue = '';
	              
	          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'true'){	        	  
	              vm.showTertiaryTransferQuestion = false;
	              vm.showHazardousTransferQuestion = true;
	              vm.showOutsideOffice = true;
	              
	              vm.TPD.applicant.tertiaryQue = '';
	              
	          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
	        	  vm.showWithinOfficeTransferQuestion = false;
	              vm.showTertiaryTransferQuestion = false;
	              vm.showHazardousTransferQuestion = false;
	              vm.showOutsideOffice = false;
	              
	              vm.TPD.applicant.occupationDuties = '';
	              vm.TPD.applicant.tertiaryQue = '';
	              vm.TPD.applicant.workDuties = '';
	              vm.TPD.applicant.spendTimeOutside = '';
	          }
	      }else{
	    	  vm.showWithinOfficeTransferQuestion = false;
	          vm.showTertiaryTransferQuestion = false;
	          vm.showHazardousTransferQuestion = false;
	          vm.showOutsideOffice = false;
	          
	          vm.TPD.applicant.occupationDuties = '';
              vm.TPD.applicant.tertiaryQue = '';
              vm.TPD.applicant.workDuties = '';
              vm.TPD.applicant.spendTimeOutside = '';
	      }
		  
		  if(vm.TPD.applicant.workDuties === 'No' && vm.TPD.applicant.spendTimeOutside === 'No'){
			  vm.showWithinOfficeTransferQuestion = true;
			  vm.showTertiaryTransferQuestion = true;
		  }
		  vm.nonManualFlag = false;
		  vm.professionalFlag = false;	
		  
		  if(vm.TPD.applicant.workDuties === 'No' && vm.TPD.applicant.spendTimeOutside === 'No'){
			  vm.TPD.applicant.cover[0].occupationRating = 'Non Manual';
	          vm.TPD.applicant.cover[1].occupationRating = 'Non Manual';
	          vm.TPD.applicant.cover[2].occupationRating = 'Non Manual';
	          vm.nonManualFlag = true;
	      }
		  
		  if(parseInt(vm.TPD.applicant.annualSalary) > 120000 && vm.TPD.applicant.occupationDuties === 'Yes' && vm.TPD.applicant.tertiaryQue=== 'Yes'){
			  vm.TPD.applicant.cover[0].occupationRating = 'Professional';
	          vm.TPD.applicant.cover[1].occupationRating = 'Professional';
	          vm.TPD.applicant.cover[2].occupationRating = 'Professional';
	          vm.professionalFlag = true;
		  }
		  
		  if(!vm.nonManualFlag && !vm.professionalFlag){
			  vm.TPD.applicant.cover[0].occupationRating = 'General';
	          vm.TPD.applicant.cover[1].occupationRating = 'General';
	          vm.TPD.applicant.cover[2].occupationRating = 'General';
		  }
		  
	      vm.checkFifteenHourQuestion();
	      vm.autoCalculate();
	    };
	    
	    vm.checkFifteenHourQuestion = function(){
	    	
	    	if(vm.fifteenHrsTransferQuestion === 'No'){
				vm.isIPCoverRequiredDisabled = true;
				vm.TransIPRequireCover = '';
				vm.maxIpErrorFlag = false;
			}else{
				vm.isIPCoverRequiredDisabled = false;
			}       
	    };
	    
	    vm.saveDataForPersistence = function(){
            var defer = $q.defer();
            /*PersistenceService.setTransferCoverDetails(vm.TPD);
            defer.resolve();*/
            
            var selectedIndustry = vm.IndustryOptions.filter(function(obj){
                return vm.TPD.applicant.industryType === obj.key;
            });
            vm.TPD.applicant.occupationCode = selectedIndustry[0].value;
            
             vm.submitFiles().then(function(res) {
            	vm.TPD.transferDocuments = PersistenceService.getUploadedFileDetails();
            	console.log(vm.TPD.transferDocuments);
            	PersistenceService.setTransferCoverDetails(vm.TPD);
            	defer.resolve(res);
             }, function(err) {
            	 defer.reject(err);
            });
            
            return defer.promise;
          };
	    
	    vm.saveQuoteTransfer = function() {
	    	
	    	vm.checkForFileUpload();
	    	
	    	vm.saveDataForPersistence().then(function() {
	    		
	    		var quoteTransferObject =  PersistenceService.getTransferCoverDetails();	
	    		
	    		if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && vm.previousCoverForm.$valid && vm.TranscoverCalculatorForm.$valid && quoteTransferObject != null && !vm.fileNotUploadedError && !vm.invalidSalAmount && !vm.maxDeathErrorFlag && !vm.maxDeathCoverErrorFlag && !vm.maxTpdErrorFlag && !vm.maxTpdCoverErrorFlag && !vm.TPDcoverAmtErrFlag) {
	                  
	                  var saveQuoteTransferObject = quoteTransferObject;
	                  saveQuoteTransferObject.lastSavedOn = 'transferpage';
	                  auraResponseService.setResponse(saveQuoteTransferObject);
	                  
	                  console.log(JSON.stringify(saveQuoteTransferObject));
	                  
	                  saveEapply.reqObj(vm.urlList.saveEapplyUrlNew).then(function(response) {
	                    console.log(response.data);
	                    vm.transferQuoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
	                  },function(err) {
	                    console.log('Something went wrong while saving...'+JSON.stringify(err));
	                  });
	              }
	              
	    	});
	    };
	    
	    vm.isUnitized = function(value){
	    	return (value === '1');
	    };
	    
	    vm.transferQuoteSaveAndExitPopUp = function (hhText) {


	          var dialog1 = ngDialog.open({
	                template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
	              className: 'ngdialog-theme-plain custom-width',
	              preCloseCallback: function(value) {
	                     var url = '/landing';
	                     $location.path( url );
	                     return true;
	              },
	              plain: true
	          });
	          dialog1.closePromise.then(function (data) {
	            console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
	          });
	        
	    };
	    
	    vm.files = [];
	    vm.selectedFile = null;
	    vm.uploadFiles = function(files, errFiles) {

	    	vm.fileSizeErrFlag = false;
	    	vm.fileFormatError = errFiles.length > 0 ? true : false;
	    	vm.selectedFile =  files[0];
	    	
	    };
	    vm.addFilesToStack = function () {
		      var fileSize = (vm.selectedFile.size / 1048576).toFixed(3);
		      if(fileSize > 10) {
		    	  vm.fileSizeErrFlag=true;
		    	  vm.fileSizeErrorMsg ='File size should not be more than 10MB';
		    	  vm.selectedFile = null;
		        return;
		      }else{
		    	  vm.fileSizeErrFlag=false;
		      }
		      if(!vm.files){
		    	  vm.files = [];
		      }		    	 
		      vm.files.push(vm.selectedFile);
		      PersistenceService.setUploadedFileDetails(vm.files);
		      vm.fileNotUploadedError = false;
		      vm.selectedFile = null;
		    
	    };

	    vm.removeFile = function(index) {
	    	vm.files.splice(index, 1);
	    	PersistenceService.setUploadedFileDetails(vm.files);
	    	if(vm.files.length < 1) {
	    		vm.fileNotUploadedError = true;
	    	}
	  
	    };
	    
	    vm.submitFiles = function () {
	    	
	    	vm.uploadedFiles = vm.uploadedFiles || [];
	    	var defer = $q.defer();
	        
	    	if(!vm.files){
	          vm.files = [];
	        }
	    	
	        if(!vm.files.length) {
	          defer.resolve({});
	        }
	        
	        var upload;
	        var numOfFiles = vm.files.length;
	        angular.forEach(vm.files, function(file, index) {
	        	if(Upload.isFile(file)) {
	        		upload = Upload.http({
	        			url: vm.urlList.fileUploadUrlNew,
	        			headers : {
	        				'Content-Type': file.name,
	        				'Authorization':tokenNumService.getTokenId()
	        			},
	        			data: file
	        		});
	        		
	        		upload.then(function(res){
	        			numOfFiles--;
	        			vm.uploadedFiles[index] = res.data;
	        			if(numOfFiles === 0){
	        				PersistenceService.setUploadedFileDetails(vm.uploadedFiles);
	        				defer.resolve(res);
	        			}
	        		}, function(err){
	        			console.log('Error uploading the file ' + err);
	        			defer.reject(err);
	        		});
	        	}else{
	        		numOfFiles--;
	        		if(numOfFiles === 0) {
	        			PersistenceService.setUploadedFileDetails(vm.uploadedFiles);
	        			defer.resolve({});
	        		}
	        	}
	        });
	        return defer.promise;
	      
	    };
	    
	    vm.generatePDF = function(){ 
	    	
	    	vm.saveDataForPersistence().then(function() {
	    	
		    	var quoteTransferObject =  PersistenceService.getTransferCoverDetails();
		    	
		    	if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && vm.previousCoverForm.$valid && vm.TranscoverCalculatorForm.$valid && quoteTransferObject != null && !vm.fileNotUploadedError && !vm.invalidSalAmount && !vm.maxDeathErrorFlag && !vm.maxDeathCoverErrorFlag && !vm.maxTpdErrorFlag && !vm.maxTpdCoverErrorFlag && !vm.TPDcoverAmtErrFlag) {
		    		
		    		auraResponseService.setResponse(quoteTransferObject);
		    		printQuotePage.reqObj(vm.urlList.printQuotePageNew).then(function(response){
		    			PersistenceService.setPDFLocation(response.data.clientPDFLocation);
		    			vm.downloadPDF();
		    		},function(err){
		    			console.log('Something went wrong while saving...'+JSON.stringify(err));
		    		});	              
		    	}
		    	
	    	});
	    };
	    
	    vm.downloadPDF = function(){
	    	var pdfLocation =null;
	    	var filename = null;
	    	var a = null;
	    	pdfLocation = PersistenceService.getPDFLocation();
	    	console.log(pdfLocation+'pdfLocation');
	    	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	    	a = document.createElement('a');
	    	document.body.appendChild(a);
	    	DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
	    		
	    		if (navigator.appVersion.toString().indexOf('.NET') > 0) {
	    			window.navigator.msSaveBlob(res.data.response,filename);
	    		}else{
	    			var fileURL = URL.createObjectURL(res.data.response);
	    			a.href = fileURL;
	    			a.download = filename;
	    			a.click();
	    		}
	    	}, function(err){
	    		console.log('Error downloading the PDF ' + err);
	    	});
	    };
	    
	    vm.coverDetailsTransferFormFields = ['contactEmail', 'contactType', 'contactPhone', 'contactPrefTime', 'gender'];
	    vm.occupationDetailsTransferFormFields = ['fifteenHrsQuestion','ownBusinessQue','ownBusinessYesQue','ownBusinessNoQue','areyouperCitzQuestion','industry','occupation','otherOccupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
	    vm.previousCoverFormFields = ['previousFundName','membershipNumber','documentName'];
	    vm.TranscoverCalculatorFormFields = ['TransDeathRequireCover','TransTPDRequireCover','TransIPRequireCover'];
	    
	    vm.checkPreviousMandatoryFields  = function (elementName,formName){
	    	
	    	var transferFormFields = [];
	    	
	    	if(formName === 'coverDetailsTransferForm'){
	    		transferFormFields = vm.coverDetailsTransferFormFields;
	    	}else if(formName === 'occupationDetailsTransferForm'){
	    		transferFormFields = vm.occupationDetailsTransferFormFields;
	        }else if(formName === 'previousCoverForm'){	        	
	        	transferFormFields = vm.previousCoverFormFields;	        	
	        }else if(formName === 'TranscoverCalculatorForm'){
	    		transferFormFields = vm.TranscoverCalculatorFormFields;
	    	}
	    	
	        var inx = transferFormFields.indexOf(elementName);	        
	        
	        if(inx > 0){
	        	for(var i = 0; i < inx ; i++){
	        		 if (vm[formName][transferFormFields[i]]){
	        			 vm[formName][transferFormFields[i]].$touched = true;
	        		 }                        
	        	}
	        }
	      };
	    
	    vm.isCollapsible = function(targetEle, event) {
	    	
	    	var formdutyValid = vm.formduty.$valid;
	    	var transferPrivacyPolicyFormValid = vm.transferPrivacyPolicyForm.$valid;
	    	var coverDetailsTransferFormValid = vm.coverDetailsTransferForm.$valid;
	    	var occupationDetailsTransferFormValid = vm.occupationDetailsTransferForm.$valid;
	    	var previousCoverFormValid = vm.previousCoverForm.$valid;
	    	var stopPropagation = false;
	    	vm.checkForFileUpload();
	    	
	    	if(targetEle === 'collapseprivacy' && !formdutyValid){
	    		vm.formduty.$submitted = true;
	    		stopPropagation = true;
	        }else if(targetEle === 'PersonalDetailsSection' && (!formdutyValid || !transferPrivacyPolicyFormValid)) {
	        	vm.transferPrivacyPolicyForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'OccupationSection' && (!formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid)) {
	        	vm.coverDetailsTransferForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'PreviousCoverSection' && (vm.invalidSalAmount || !formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid || !occupationDetailsTransferFormValid)){
	        	vm.occupationDetailsTransferForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'CoverSection' && (vm.fileNotUploadedError || vm.invalidSalAmount || !formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid || !occupationDetailsTransferFormValid || !previousCoverFormValid)){
	        	vm.previousCoverForm.$submitted=true;
	        	stopPropagation = true;
	        }
	        
	        if(stopPropagation){
	        	event.stopPropagation();
	        	return false;
	        }
	        
	    };
	    
	    vm.clickToOpen = function (hhText) {
	    	
	    	var templateContent = '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4>';
	    	templateContent += '<div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p>';
	    	templateContent += '<div id="tips_text">'+hhText+'</div><p></p></div></div></div>';
	    	templateContent += '<div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12">';
	    	templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>';
	    	
	    	

			var dialog = ngDialog.open({
				template: templateContent,
				className: 'ngdialog-theme-plain',
				plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		  
	  };
			
			
		//watchers for form valid and invalid
        
        $scope.$watch('vm.TPD.dodCheck' , function(newVal, oldVal) {
        	$timeout(function() {
        		if(vm.TPD.dodCheck) {
        			$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
					$('#PreviousCoverSection').collapse('hide');
					$('#CoverSection').collapse('hide');
	        	}else{
	        		$('#collapseprivacy').collapse('hide');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
					$('#PreviousCoverSection').collapse('hide');
					$('#CoverSection').collapse('hide');
	        	}
        	},10);
        });
        
        $scope.$watch('vm.TPD.privacyCheck', function(newVal, oldVal) {
        	$timeout(function() {
	        	if(vm.TPD.privacyCheck) {
	        		$('#PersonalDetailsSection').collapse('show');
	        		$('#OccupationSection').collapse('hide');
	        		$('#PreviousCoverSection').collapse('hide');
	        		$('#CoverSection').collapse('hide');
	        	}else{
	        		$('#PersonalDetailsSection').collapse('hide');
	        		$('#OccupationSection').collapse('hide');
	        		$('#PreviousCoverSection').collapse('hide');
	        		$('#CoverSection').collapse('hide');
	        	}
        	},10);
       });
        
        $scope.$watch('vm.coverDetailsTransferForm.$valid', function(newVal, oldVal) {
      	  if(newVal) {
      		  $('#OccupationSection').collapse('show');
      		  $('#PreviousCoverSection').collapse('hide');
      		  $('#CoverSection').collapse('hide');
      	  }
        });
        
        $scope.$watch('vm.coverDetailsTransferForm.$invalid', function(newVal, oldVal) {
      	  if(newVal) {
      		  $('#OccupationSection').collapse('hide');
      		  $('#PreviousCoverSection').collapse('hide');
      		  $('#CoverSection').collapse('hide'); 
      	  }
        });
        
       $scope.$watch('vm.occupationDetailsTransferForm.$valid', function(newVal, oldVal) {
    	   $timeout(function() {
	      	  if(newVal && !vm.invalidSalAmount) {
	      		  $('#PreviousCoverSection').collapse('show');
	      		  $('#CoverSection').collapse('hide');
	      	  }
    	   },100);
        });
        
        $scope.$watch('vm.occupationDetailsTransferForm.$invalid', function(newVal, oldVal) {
      	  if(newVal) {
      		  $('#PreviousCoverSection').collapse('hide');
      		  $('#CoverSection').collapse('hide'); 
      	  }
        });
        
        $scope.$watch('vm.invalidSalAmount', function(newVal, oldVal) {
        	if(newVal) {
        		$timeout(function() {
	        			 $('#PreviousCoverSection').collapse('hide');
	        			 $('#CoverSection').collapse('hide');
        		},100);
        	 }else if(vm.occupationDetailsTransferForm.$valid){
        		 $('#PreviousCoverSection').collapse('show');
    			 $('#CoverSection').collapse('hide');
        	 }
        });
        
        vm.showCoverSection = function(documentEvidence, formValid){
        	
        	var showCoversection = false; 
        	
        	if(!formValid){
        		showCoversection = false; 
        	}else{
        		if(documentEvidence === 'Yes') {
            		vm.checkForFileUpload();
    	      		$timeout(function() {
    	      			if(vm.fileNotUploadedError){
    		      			showCoversection = false;
    		      		}else{
    		      			showCoversection = true;
    		      		}
    	      		},10);    	      		
            	}else{
            		showCoversection = true;      	  
            	}
        	}
        	
        	$timeout(function() {
	        	if(showCoversection){
		      		$('#CoverSection').collapse('show');   
		      	}else{
		      		$('#CoverSection').collapse('hide'); 
		      	}
        	},200); 
        };
        
        /*$scope.$watch('vm.TPD.applicant.documentEvidence', function(newVal, oldVal) {
        	vm.showCoverSection(newVal, vm.previousCoverForm.$valid);        	
        });*/
        
        $scope.$watch('vm.previousCoverForm.$valid', function(newVal, oldVal) {
        	if(newVal){
        		vm.showCoverSection(vm.TPD.applicant.documentEvidence, newVal);
        	}        	
        });
        $scope.$watch('vm.fileNotUploadedError', function(newVal, oldVal) {
        	vm.showCoverSection(vm.TPD.applicant.documentEvidence, vm.previousCoverForm.$valid);
        });
        
        $scope.$watch('vm.previousCoverForm.$invalid', function(newVal, oldVal) {
      	  if(newVal) {
      		  $('#CoverSection').collapse('hide'); 
      	  }
        });
		
	}
})(angular);

 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */
