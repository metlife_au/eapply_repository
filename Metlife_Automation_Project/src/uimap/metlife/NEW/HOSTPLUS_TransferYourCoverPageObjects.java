package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class HOSTPLUS_TransferYourCoverPageObjects extends PageObjectLibrary{
	
	public static final By  TransferYourCoverLink = getObjectIdentifaction("HOSTPLUS.TYC.Link.xpath");
	public static final By  EmailId = getObjectIdentifaction("HOSTPLUS.TYC.Email.id");
	public static final By  ContactTypeClick = getObjectIdentifaction("HOSTPLUS.TYC.ContactTypeClick.xpath");
	public static final By  ContactNumber = getObjectIdentifaction("HOSTPLUS.TYC.ContactNumber.id");
	public static final By  MorningContact = getObjectIdentifaction("HOSTPLUS.TYC.MorningContact.id");
	public static final By  AfterNoonContact = getObjectIdentifaction("HOSTPLUS.TYC.AfterNoonContact.id");
	public static final By  ContactDetailsContinue = getObjectIdentifaction("HOSTPLUS.TYC.ContactDetailsContinue.xpath");
	public static final By  FifteenHoursYes = getObjectIdentifaction("HOSTPLUS.TYC.FifteenHoursYes.id");
	public static final By  FifteenHoursNo = getObjectIdentifaction("HOSTPLUS.TYC.FifteenHoursNo.id");
	public static final By  CitizenYes = getObjectIdentifaction("HOSTPLUS.TYC.CitizenYes.id");
	public static final By  CitizenNo = getObjectIdentifaction("HOSTPLUS.TYC.CitizenNo.id");
	public static final By  ClickIndustry = getObjectIdentifaction("HOSTPLUS.TYC.ClickIndustry.xpath");
	public static final By  ClickOccupation = getObjectIdentifaction("HOSTPLUS.TYC.ClickOccupation.xpath");
	public static final By  OtherOccupation = getObjectIdentifaction("HOSTPLUS.TYC.OtherOccupation.id");
	public static final By  OfficeEnvironmentYes = getObjectIdentifaction("HOSTPLUS.TYC.OfficeEnvironmentYes.id");
	public static final By  OfficeEnvironmentNo = getObjectIdentifaction("HOSTPLUS.TYC.OfficeEnvironmentNo.id");
	public static final By  TertiaryQualYes = getObjectIdentifaction("HOSTPLUS.TYC.TertiaryQualYes.id");
	public static final By  TertiaryQualNo = getObjectIdentifaction("HOSTPLUS.TYC.TertiaryQualNo.id");
	public static final By  HazardousEncYes = getObjectIdentifaction("HOSTPLUS.TYC.HazardousEncYes.id");
	public static final By  HazardousEncNo = getObjectIdentifaction("HOSTPLUS.TYC.HazardousEncNo.id");
	public static final By  OutsideOfcPercentYes = getObjectIdentifaction("HOSTPLUS.TYC.OutsideOfcPercentYes.id");
	public static final By  OutsideOfcPercentNo = getObjectIdentifaction("HOSTPLUS.TYC.OutsideOfcPercentNo.id");
	public static final By  AnnualSalary = getObjectIdentifaction("HOSTPLUS.TYC.AnnualSalary.id");
	public static final By  LoadSalaryLabel = getObjectIdentifaction("HOSTPLUS.TYC.LoadSalaryLabel.xpath");
	public static final By  ContinueOccupationDetails = getObjectIdentifaction("HOSTPLUS.TYC.ContinueOccupationDetails.xpath");
	
	public static final By PreviousInsurer = getObjectIdentifaction("HOSTPLUS.TYC.PreviousInsurer.id");
	public static final By PolicyNumber = getObjectIdentifaction("HOSTPLUS.TYC.PolicyNumber.id");
	public static final By USINumber = getObjectIdentifaction("HOSTPLUS.TYC.USINumber.id");
	public static final By DocumentEvidenceYes = getObjectIdentifaction("HOSTPLUS.TYC.DocumentEvidenceYes.id");
	public static final By DocumentEvidenceNo = getObjectIdentifaction("HOSTPLUS.TYC.DocumentEvidenceNo.id");
	
	
	public static final By TransferChkbox = getObjectIdentifaction("HOSTPLUS.TYC.TransferChkbox.id");
	public static final By CostofInsurance = getObjectIdentifaction("HOSTPLUS.TYC.CostofInsurance.xpath");
	public static final By ContinueAfterPreviousCover = getObjectIdentifaction("HOSTPLUS.TYC.ContinueAfterPreviousCover.xpath");
	public static final By DeathAmount = getObjectIdentifaction("HOSTPLUS.TYC.DeathAmount.id");
	public static final By LoadDeathAmount = getObjectIdentifaction("HOSTPLUS.TYC.LoadDeathAmount.xpath");
	public static final By TPDAmount = getObjectIdentifaction("HOSTPLUS.TYC.TPDAmount.id");
	public static final By LoadTPDAmount = getObjectIdentifaction("HOSTPLUS.TYC.LoadTPDAmount.xpath");
	public static final By IPAmount = getObjectIdentifaction("HOSTPLUS.TYC.IPAmount.xpath");
	public static final By LoadIPAmount = getObjectIdentifaction("HOSTPLUS.TYC.LoadIPAmount.xpath");
	public static final By Waitingperiod = getObjectIdentifaction("HOSTPLUS.TYC.Waitingperiod.xpath");
	public static final By benefitperiod = getObjectIdentifaction("HOSTPLUS.TYC.benefitperiod.xpath");
	public static final By CalculateQuote = getObjectIdentifaction("HOSTPLUS.TYC.CalculateQuote.xpath");
	
	public static final By ContinueAfterTYC = getObjectIdentifaction("HOSTPLUS.TYC.ContinueAfterTYC.xpath");
//	
//	public static final By RestrictedillnessYes = getObjectIdentifaction("HOSTPLUS.TYC.RestrictedillnessYes.xpath");
//	public static final By RestrictedillnessNo = getObjectIdentifaction("HOSTPLUS.TYC.RestrictedillnessNo.xpath");
//	public static final By DiagnosedillnessYes = getObjectIdentifaction("HOSTPLUS.TYC.DiagnosedillnessYes.xpath");
//	public static final By DiagnosedillnessNo = getObjectIdentifaction("HOSTPLUS.TYC.DiagnosedillnessNo.xpath");
//	public static final By TPDClaimYes = getObjectIdentifaction("HOSTPLUS.TYC.TPDClaimYes.xpath");
//	public static final By TPDClaimNo = getObjectIdentifaction("HOSTPLUS.TYC.TPDClaimNo.xpath");
//	public static final By Age65Yes = getObjectIdentifaction("HOSTPLUS.TYC.65AgeYes.xpath");
//	public static final By Age65No = getObjectIdentifaction("HOSTPLUS.TYC.65AgeNo.xpath");
//	public static final By FundpremloadingYes = getObjectIdentifaction("HOSTPLUS.TYC.FundpremloadingYes.xpath");
//	public static final By FundpremloadingNo = getObjectIdentifaction("HOSTPLUS.TYC.FundpremloadingNo.xpath");
//	public static final By ContinueAfterHealthSection = getObjectIdentifaction("HOSTPLUS.TYC.ContinueAfterHealthSection.xpath");

	//public static final By DutyofDisclosure = getObjectIdentifaction("HOSTPLUS.TYCover.DutyofDisclosure.xpath");
	//public static final By PrivacyStatement = getObjectIdentifaction("HOSTPLUS.TYCover.PrivacyStatement.xpath");
	//public static final By GeneralConsent = getObjectIdentifaction("HOSTPLUS.TYCover.GeneralConsent.xpath");
	//public static final By Submit = getObjectIdentifaction("HOSTPLUS.TYCover.Submit.xpath");
	//public static final By FeedbackNo = getObjectIdentifaction("HOSTPLUS.TYCover.FeedbackNo.xpath");


}
