(function(angular){
	'use strict';
    /* global channel, moment */
	angular
	.module('guildApp')
	.controller('auratransfer', auratransfer);
	auratransfer.$inject=['$scope','$rootScope','$timeout','$location','$window','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','RetrieveAppDetailsService','ngDialog','urlService','saveEapply','submitEapply'];
	function auratransfer($scope,$rootScope,$timeout,$location,$window,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,RetrieveAppDetailsService,ngDialog,urlService,saveEapply,submitEapply){
		
		var vm=this;		
		vm.urlList = urlService.getUrlList();
		vm.svdRtrv =false;
		vm.go = function (path) {
			
			if(vm.svdRtrv && path ==='/quotetransfer/2'){
				path = '/quotetransfer/3';
			}
			
	    	$timeout(function(){
	    		$location.path(path);
	    	}, 10);
	  	};
	  	
	  	vm.navigateToLandingPage = function (){
	  		
	  		var templateContent = '<div class="ngdialog-content"><div class="modal-body">';
	  		templateContent += '<div class="row rowcustom"><div class="col-sm-8">';
	  		templateContent += '<p> Are you sure you want to navigate to Home Page?</p>';
	  		templateContent += '</div> </div></div></br>';
	  		templateContent += '<div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter">';
	  		templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button>';
	  		templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
	  		templateContent += '</div></div>';
	  		
	    	ngDialog.openConfirm({
	            template:templateContent,
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
	    };
	    
	    var init = function(){
	    	
	    	var savedData =  PersistenceService.getTransferCoverDetails();
	    	
	  		auraInputService.setFund('GUIL');
	  	  	auraInputService.setMode('TransferCover');
	  	  	//setting values from quote page
	  	    auraInputService.setAge(moment().diff(moment(savedData.applicant.birthDate, 'DD-MM-YYYY'), 'years')) ;
	  	  	auraInputService.setName(savedData.applicant.firstName+' '+savedData.applicant.lastName);
	  	  	auraInputService.setAppnumber(parseInt(savedData.applicatioNumber));
	  	  	auraInputService.setGender(savedData.applicant.gender);	  	  	
	  	  	auraInputService.setCountry('Australia');	  	  	
	  	  	auraInputService.setFifteenHr(savedData.applicant.workHours);
	  	  	auraInputService.setDeathAmt(1);
	  	  	auraInputService.setTpdAmt(1);
	  	  	auraInputService.setIpAmt(1);
	  	  	
	  	  	auraInputService.setWaitingPeriod(savedData.applicant.cover[2].additionalIpWaitingPeriod);
		  	auraInputService.setBenefitPeriod(savedData.applicant.cover[2].additionalIpBenefitPeriod) ;
	  	  	auraInputService.setIndustryOcc(savedData.applicant.industryType+':'+savedData.applicant.occupation);
	  	  	auraInputService.setSalary(savedData.applicant.annualSalary);
	  	  	auraInputService.setClientname('metaus');
	  	  	auraInputService.setLastName(savedData.applicant.lastName);
	  	  	auraInputService.setFirstName(savedData.applicant.firstName);
	  	  	auraInputService.setDob(savedData.applicant.birthDate);
	  	  	auraInputService.setExistingTerm(false);
	  	    auraInputService.setMemberType('INDUSTRY OCCUPATION');
	  	  	/*if($scope.personalDetails.memberType=='Personal'){
	  	  		auraInputService.setMemberType('INDUSTRY OCCUPATION')
	  	  	}else{
	  	  		auraInputService.setMemberType('None')
	  	  	}*/
	  	    console.log(JSON.stringify(auraInputService));
	  	    
	  		 getAuraTransferData.requestObj(vm.urlList.auraTransferDataUrl).then(function(response) {
	  	  		vm.auraResponseDataList = response.data.questions;
	  	  		angular.forEach(vm.auraResponseDataList, function(Object) {
	  				vm.sectionname = Object.questionAlias.substring(3);

	  				});
	  	  	});
	  	};
	  	init();
	    
	    vm.updateRadio = function (answerValue, questionObj){

	    	
	    	questionObj.arrAns[0]=answerValue;
	    	vm.auraRes={'questionID':questionObj.questionId,'auraAnswers':questionObj.arrAns};
	    	auraResponseService.setResponse(vm.auraRes);
	    	
	    	auraPostfactory.reqObj(vm.urlList.auraPostUrl).then(function(response) {
	    		
	    		vm.selectedIndex = vm.auraResponseDataList.indexOf(questionObj);
	    		vm.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data);
	    		vm.auraResponseDataList[vm.selectedIndex]=response.data;
	    		
	  	  		angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
	  	  			if(vm.selectedIndex>selectedIndex && !Object.questionComplete){
	  	  				vm.auraResponseDataList[selectedIndex].error=true;
	  	  			}else if(Object.questionComplete){
	  	  				vm.auraResponseDataList[selectedIndex].error=false;
	  	  			}

	  	  		});

	      	}, function () {
	      		//console.log('failed');
	      	});
	    };
	    
	    vm.updateQuestionList = function(questionId,changedQuestion, questionObj){
	    	
	    	angular.forEach(vm.auraResponseDataList, function (Object,index1) {
	    		if(Object.questionId === changedQuestion.questionId){
	    			vm.auraResponseDataList[index1]= changedQuestion;
  				}
  			});  	
	    };
	    
	    vm.transferAuraSaveAndExitPopUp = function (hhText) {
	    	
	    	var templateContent = '<div class="ngdialog-content"><div class="modal-body">';
	    	templateContent += '<h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4>';
	    	templateContent += '<div class="row rowcustom"><div class="col-sm-12"><p class="aligncenter">  </p>';
	    	templateContent += '<div id="tips_text">'+hhText+'</div>  <p></p>  </div></div></div>';
	    	templateContent += '<div class="ngdialog-buttons aligncenter">';
	    	templateContent += '<button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button>';
	    	templateContent += '</div></div>';
	    	

			var dialog1 = ngDialog.open({
				    template: templateContent,
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = '/landing';
					       $location.path( url );
					       return true;
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
		
		vm.saveAuraTransfer = function() {
			
			var appNum = PersistenceService.getAppNumber();
			var saveAuraTransferObject =  PersistenceService.getTransferCoverDetails();
	    	
	    	if(saveAuraTransferObject !== null){
	    		
	    		saveAuraTransferObject.lastSavedOn = 'AuraTransferPage';
	        	auraResponseService.setResponse(saveAuraTransferObject);
	        	
	        	console.log(JSON.stringify(saveAuraTransferObject));	        	
	        	saveEapply.reqObj(vm.urlList.saveEapplyUrlNew).then(function(response) {
		                console.log(response.data);
		                vm.transferAuraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
		        });
	    	}
	  	};
	    
	    vm.proceedNext = function() {
	    	
	    	vm.keepGoing= true;
	  		
	    	angular.forEach(vm.auraResponseDataList, function (obj, selectedIndex) {
	  			if(obj.questionComplete && obj.branchComplete) {
	  				vm.auraResponseDataList[selectedIndex].error=false;
	  			} else {
	          vm.auraResponseDataList[selectedIndex].error=true;
	          vm.keepGoing= false;
	  			}
	  		});
	    	
	  		if(vm.keepGoing){
	  			
	  			submitAura.requestObj(vm.urlList.submitAuraUrl).then(function(response) {
	  				PersistenceService.setTransCoverAuraDetails(response.data.responseObject);
	  				vm.auraResponses = response.data;
			  		if(vm.auraResponses.overallDecision !==  'DCL'){
							//console.log(response.data);
			  			
			  			var savedObject =  PersistenceService.getTransferCoverDetails();
			  			savedObject.applicant.cover[0].coverDecision = vm.auraResponses.deathDecision;
			  			savedObject.applicant.cover[1].coverDecision = vm.auraResponses.tpdDecision;
			  			savedObject.applicant.cover[2].coverDecision = vm.auraResponses.ipDecision;
						PersistenceService.setTransferCoverDetails(savedObject);
						
						vm.go('/transferSummary/1');
						
			  		}else if(vm.auraResponses.overallDecision === 'DCL'){
			  			var quoteTransferObject =  PersistenceService.getTransferCoverDetails();
			  			quoteTransferObject.responseObject = vm.auraResponses.responseObject;
			  			quoteTransferObject.appDecision = 'DCL';
			  			if(vm.auraResponses != null && quoteTransferObject != null){
			  				auraResponseService.setResponse(quoteTransferObject);
			      			submitEapply.reqObj(vm.urlList.submitEapplyUrlNew).then(function(response) {
			              		console.log(response.data);
			              		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
			              		PersistenceService.setNpsUrl(response.data.npsTokenURL);
		                  		vm.go('/transferDecision/decline');
			              	}, function(err){
	  		            		console.log('Error while submitting transfer cover ' + err);
	  		            	});
			              }
			  		}
	  	  		});
	  		}
	  	};
	    
	}
})(angular);

 /* Transfer Aura Controller,Progressive and Mandatory validations Ends  */
