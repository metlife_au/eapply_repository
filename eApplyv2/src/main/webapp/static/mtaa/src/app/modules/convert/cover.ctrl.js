(function(angular){
	'use strict';
    /* global channel, moment, Date */
	angular
		.module('MtaaApp')
		.controller('quoteConvert', quoteConvert);
	quoteConvert.$inject=['$scope','$rootScope','$stateParams','$location','$http','$timeout','$window','urlService','persoanlDetailService','PersistenceService','mtaaUIConstants','$q','QuoteService','OccupationService','NewOccupationService','deathCoverService','tpdCoverService','ipCoverService','Upload','saveEapply','tokenNumService', 'auraResponseService','CalculateService','ngDialog','DownloadPDFService','printQuotePage'];
	function quoteConvert($scope, $rootScope, $stateParams, $location, $http, $timeout, $window, urlService, persoanlDetailService, PersistenceService, mtaaUIConstants, $q, QuoteService, OccupationService, NewOccupationService, deathCoverService, tpdCoverService, ipCoverService, Upload, saveEapply, tokenNumService, auraResponseService, CalculateService, ngDialog, DownloadPDFService, printQuotePage){
		var vm = this;
		var fetchAppnum = true;
		var appNum;
		vm.calculateCover = false;
		vm.TPD = {
				'requestType':'CCOVER',
				'partnerCode':'MTAA',
				'lastSavedOn':'lifeeventpage',
				'applicant': {
					'cover':[{},{},{}],
					'contactDetails':{}
				}
		};
		
		vm.constants = {
				image: mtaaUIConstants.path.images,
				phoneNumber: mtaaUIConstants.onboardDetails.phoneNumber,
				emailFormat: mtaaUIConstants.onboardDetails.emailFormat,
				postCodeFormat: mtaaUIConstants.onboardDetails.postCodeFormat,
				contactTypeOption: mtaaUIConstants.onboardDetails.contactTypeOption,
				titleOption: mtaaUIConstants.onboardDetails.titleOption,
				addressTypeOption: mtaaUIConstants.onboardDetails.addressTypeOption,
				stateOptions: mtaaUIConstants.onboardDetails.stateOptions
		};
		
		vm.init = function(){
			
			var defer = $q.defer();
			vm.urlList = urlService.getUrlList();
			vm.premiumFrequencyOptions = ['Monthly', 'Yearly', 'Weekly'];
			vm.inputDetails = persoanlDetailService.getMemberDetails();
			vm.userAge = parseInt(moment().diff(moment(vm.inputDetails.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
			vm.ageLimit=vm.userAge;
			
			//setting value to JSON
			vm.TPD.applicant.memberType = vm.inputDetails.memberType;
			vm.TPD.applicant.firstName = vm.inputDetails.personalDetails.firstName;
			vm.TPD.applicant.lastName = vm.inputDetails.personalDetails.lastName;
			vm.TPD.applicant.gender = vm.inputDetails.personalDetails.gender;
			vm.TPD.applicant.birthDate =  vm.inputDetails.personalDetails.dateOfBirth;
			vm.TPD.applicant.dateJoinedFund = vm.inputDetails.dateJoined;
			vm.TPD.applicant.emailId = vm.inputDetails.contactDetails.emailAddress;
			
			//address
			vm.TPD.applicant.contactDetails.preferedContactTime = vm.inputDetails.contactDetails.prefContactTime;
			vm.TPD.applicant.contactDetails.preferedContacType = vm.inputDetails.contactDetails.prefContact;
			
			vm.TPD.applicant.contactDetails.addressLine1 = vm.inputDetails.address.line1;
			vm.TPD.applicant.contactDetails.addressLine2 = vm.inputDetails.address.line2;
			vm.TPD.applicant.contactDetails.postCode = vm.inputDetails.address.postCode;
			vm.TPD.applicant.contactDetails.country = vm.inputDetails.address.country;
			vm.TPD.applicant.contactDetails.state = vm.inputDetails.address.state;
			vm.TPD.applicant.contactDetails.suburb = vm.inputDetails.address.suburb;
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
			
			vm.TPD.clientRefNumber = vm.inputDetails.clientRefNumber;
			vm.TPD.applicant.clientRefNumber = parseInt(vm.inputDetails.clientRefNumber);
			vm.doj = vm.inputDetails.dateJoined;
			vm.TPD.applicant.customerReferenceNumber = vm.inputDetails.contactDetails.fundEmailAddress;
			
			vm.deathCoverTransDetails = deathCoverService.getDeathCover();
			vm.tpdCoverTransDetails = tpdCoverService.getTpdCover();
			vm.ipCoverTransDetails = ipCoverService.getIpCover();
			
			if(vm.deathCoverTransDetails.amount){
				vm.TPD.applicant.cover[0].existingCoverAmount = vm.deathCoverTransDetails.amount;
		    }
			if(vm.deathCoverTransDetails.units){
				vm.TPD.applicant.cover[0].fixedUnit = vm.deathCoverTransDetails.units;
		    }
			if(vm.tpdCoverTransDetails.amount){
				vm.TPD.applicant.cover[1].existingCoverAmount = vm.tpdCoverTransDetails.amount;
		    }
			if(vm.tpdCoverTransDetails.units){
				vm.TPD.applicant.cover[1].fixedUnit = vm.tpdCoverTransDetails.units;
		    }
			if(vm.ipCoverTransDetails.amount){
				vm.TPD.applicant.cover[2].existingCoverAmount = vm.ipCoverTransDetails.amount;
				vm.TPD.applicant.cover[2].fixedUnit = vm.ipCoverTransDetails.units;
				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].totalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].totalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
		    }
			
			//benefit type
			vm.TPD.applicant.cover[0].benefitType = vm.deathCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[1].benefitType = vm.tpdCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[2].benefitType = vm.ipCoverTransDetails.benefitType;
			
			vm.TPD.applicant.cover[0].frequencyCostType = 'Weekly';
			vm.TPD.applicant.cover[1].frequencyCostType = 'Weekly';
			vm.TPD.applicant.cover[2].frequencyCostType = 'Weekly';
			
			//coverDecision - set to ACC since no AURA
			vm.TPD.applicant.cover[0].coverDecision = 'ACC';
			vm.TPD.applicant.cover[1].coverDecision = 'ACC';
			vm.TPD.applicant.cover[2].coverDecision = 'ACC';
			
			QuoteService.getList(vm.urlList.quoteUrl, 'MTAA').then(function(res) {
				vm.IndustryOptions = res.data;
				defer.resolve(res);
			},function(err) {
				console.log('Error while getting industry options '+ JSON.stringify(err));
				defer.reject(err);
			});	
			
			return defer.promise;
		};
		
		vm.init();
		
		vm.init().then(function() {
			
			if($stateParams.mode === '3' || $stateParams.mode === '2'){
				vm.TPD = PersistenceService.getConvertMaintainDetails();
				
				vm.TPD.applicant.industryType = vm.TPD.applicant.industryType === '' ? null : vm.TPD.applicant.industryType;
	            OccupationService.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.TPD.applicant.industryType).then(function(res) {
	                vm.OccupationList = res.data;
	            }, function(err) {
	                console.info('Error while fetching occupations ' + JSON.stringify(err));
	            });
	            
	            $timeout(function() {
					
	            	$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('show');
					$('#OccupationSection').collapse('show');
					$('#ConvertCoverSection').collapse('show');
					
				}, 1000);
				
			}else{
				$('#collapseprivacy').collapse('hide');
				$('#PersonalDetailsSection').collapse('hide');
				$('#OccupationSection').collapse('hide');
				$('#ConvertCoverSection').collapse('hide');
			}
			
		});
		
		vm.changePrefContactType = function(){
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
		};
		
		vm.getOccupations = function(){
	    	vm.TPD.applicant.occupation = '';
            vm.TPD.applicant.otherOccupation = '';
            vm.TPD.applicant.industryType = vm.TPD.applicant.industryType === '' ? null : vm.TPD.applicant.industryType;
            OccupationService.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.TPD.applicant.industryType).then(function(res) {
                vm.OccupationList = res.data;
                vm.TPD.applicant.occupation = '';
            }, function(err) {
                console.info('Error while fetching occupations ' + JSON.stringify(err));
            });
	    };
	    
	    vm.invalidSalAmount = false;
		vm.checkValidSalary = function(){			
			vm.renderOccupationQuestions();
			
			$timeout(function() {
				vm.invalidSalAmount = false;
				if(parseInt(vm.TPD.applicant.annualSalary) === 0){
					vm.invalidSalAmount = true;
				}
			},100);
		};
	    
	    vm.getTransCategoryFromDB = function(fromOccupation){
			  
			  if(vm.otherOccupationObj){
				  vm.otherOccupationObj.transferotherOccupation = '';
			  }
			  
			  if(vm.TPD.applicant.occupation !== undefined){
				  vm.renderOccupationQuestions();
			  } 
		  };
		
		vm.renderOccupationQuestions = function(){
			  
			  //var occupationDetailsTransferFormFields;
			/*vm.TPD.applicant.cover[0].occupationRating = vm.deathCoverTransDetails.occRating === '' ? 'General' : vm.deathCoverTransDetails.occRating;
			vm.TPD.applicant.cover[1].occupationRating = vm.tpdCoverTransDetails.occRating === '' ? vm.deathCoverTransDetails.occRating : vm.tpdCoverTransDetails.occRating;
			vm.TPD.applicant.cover[2].occupationRating = vm.ipCoverTransDetails.occRating === '' ? vm.deathCoverTransDetails.occRating : vm.ipCoverTransDetails.occRating;*/
			  
			  if(vm.OccupationList){
				  var selectedOcc = vm.OccupationList.filter(function(obj){
					  return obj.occupationName === vm.TPD.applicant.occupation;
		          });
		          var selectedOccObj = selectedOcc[0];
		          vm.occupUpgradeNotEligible = false;
		          if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
		              vm.showWithinOfficeTransferQuestion = true;
		        	  vm.showTertiaryTransferQuestion = true;
		              vm.showHazardousTransferQuestion = false;
		              vm.showOutsideOffice = false;
		              
		              vm.TPD.applicant.spendTimeOutside = '';
		              vm.TPD.applicant.workDuties = '';
		          
		          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'true'){
		              vm.showWithinOfficeTransferQuestion = false;
		              vm.showTertiaryTransferQuestion = false;
		              vm.showHazardousTransferQuestion = true;
		              vm.showOutsideOffice = true;
		              
		              vm.TPD.applicant.occupationDuties = '';
		              vm.TPD.applicant.tertiaryQue = '';
		              
		          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'true'){	        	  
		              vm.showTertiaryTransferQuestion = false;
		              vm.showHazardousTransferQuestion = true;
		              vm.showOutsideOffice = true;
		              
		              vm.TPD.applicant.tertiaryQue = '';
		              
		          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
		        	  vm.showWithinOfficeTransferQuestion = false;
		              vm.showTertiaryTransferQuestion = false;
		              vm.showHazardousTransferQuestion = false;
		              vm.showOutsideOffice = false;
		              
		              vm.TPD.applicant.occupationDuties = '';
		              vm.TPD.applicant.tertiaryQue = '';
		              vm.TPD.applicant.workDuties = '';
		              vm.TPD.applicant.spendTimeOutside = '';
		          }
		      }else{
		    	  vm.showWithinOfficeTransferQuestion = false;
		          vm.showTertiaryTransferQuestion = false;
		          vm.showHazardousTransferQuestion = false;
		          vm.showOutsideOffice = false;
		          
		          vm.TPD.applicant.occupationDuties = '';
	              vm.TPD.applicant.tertiaryQue = '';
	              vm.TPD.applicant.workDuties = '';
	              vm.TPD.applicant.spendTimeOutside = '';
		      }
			  
			  vm.nonManualFlag = false;
			  vm.professionalFlag = false;
			  
			  if(vm.TPD.applicant.workDuties === 'No' && vm.TPD.applicant.spendTimeOutside === 'No'){
				  vm.TPD.applicant.cover[0].occupationRating = 'Non Manual';
		          vm.TPD.applicant.cover[1].occupationRating = 'Non Manual';
		          vm.TPD.applicant.cover[2].occupationRating = 'Non Manual';
		          vm.nonManualFlag = true;
		      }
			  
			  if(parseInt(vm.TPD.applicant.annualSalary) > 120000 && vm.TPD.applicant.occupationDuties === 'Yes' && vm.TPD.applicant.tertiaryQue=== 'Yes'){
				  vm.TPD.applicant.cover[0].occupationRating = 'Professional';
		          vm.TPD.applicant.cover[1].occupationRating = 'Professional';
		          vm.TPD.applicant.cover[2].occupationRating = 'Professional';
		          vm.professionalFlag = true;
			  }
			  
			  if(!vm.nonManualFlag && !vm.professionalFlag){
				  vm.TPD.applicant.cover[0].occupationRating = 'General';
		          vm.TPD.applicant.cover[1].occupationRating = 'General';
		          vm.TPD.applicant.cover[2].occupationRating = 'General';
			  }
			  
			  vm.autoCalculate();
			  
		    };
		
		vm.checkDodState = function() {
			vm.TPD.dodCheck = !vm.TPD.dodCheck;
        };

        vm.checkPrivacyState = function() {
        	vm.TPD.privacyCheck =  !vm.TPD.privacyCheck;
        };
        
        vm.fetchAppnum = function(){
        	
        	if(fetchAppnum){
        		fetchAppnum = false;
        		appNum = PersistenceService.getAppNumber();
        		vm.TPD.applicatioNumber = appNum;
        	}        	
        };
        
        vm.autoCalculate = function(){
        	
        	if(vm.calculateCover){
        		
        		vm.TPD.applicant.cover[1].frequencyCostType = vm.TPD.applicant.cover[0].frequencyCostType;
        		vm.TPD.applicant.cover[2].frequencyCostType = vm.TPD.applicant.cover[0].frequencyCostType;
        		
        		var deathCoverRounded = vm.TPD.applicant.cover[0].existingCoverAmount;
        		var tpdCoverRounded = vm.TPD.applicant.cover[1].existingCoverAmount;
        		
        		if(((parseFloat(vm.TPD.applicant.cover[0].existingCoverAmount))%1000)>0){
    	    		
    	    		deathCoverRounded = Math.ceil(Math.round((parseFloat(vm.TPD.applicant.cover[0].existingCoverAmount)) + (1000 - ((parseFloat(vm.TPD.applicant.cover[0].existingCoverAmount))%1000))));
    					
    			}
    	    	if(((parseFloat(vm.TPD.applicant.cover[1].existingCoverAmount))%1000)>0){
    	    		
    	    		tpdCoverRounded = Math.ceil(Math.round((parseFloat(vm.TPD.applicant.cover[1].existingCoverAmount)) + (1000 - ((parseFloat(vm.TPD.applicant.cover[1].existingCoverAmount))%1000))));
    					
    			}
        		
        		var ruleModel = {
        				'age': vm.userAge,
    	        		'fundCode': 'MTAA',
    	        		'gender': vm.TPD.applicant.gender,
    	        		'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
    	        		'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
    	        		'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
    	        		'deathUnits': null,
    	        		'deathFixedAmount': parseInt(deathCoverRounded),
    	        		'deathFixedCost': null,
    	        		'deathUnitsCost': null,
    	        		'tpdUnits': null,
    	        		'tpdFixedAmount': parseInt(tpdCoverRounded),
    	        		'tpdFixedCost': null,
    	        		'tpdUnitsCost': null,
    	        		'ipUnits': null,
    	        		'ipFixedAmount': vm.TPD.applicant.cover[2].existingCoverAmount,
    	        		'ipFixedCost': null,
    	        		'ipUnitsCost': null,
    	        		'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
    	        		'memberType': null,
    	        		'manageType': 'CCOVER',
    	        		'deathCoverType': 'DcFixed',
    	        		'tpdCoverType': 'TpdFixed',
    	        		'ipCoverType': 'IpFixed',
    	        		'ipWaitingPeriod': vm.TPD.applicant.cover[2].totalIpWaitingPeriod,
    	        		'ipBenefitPeriod': vm.TPD.applicant.cover[2].totalIpBenefitPeriod
    	        	};
        		
        		CalculateService.calculate(ruleModel,vm.urlList.calculateUrl).then(function(res){
        			var premium = res.data;
    	    		
        			for(var i = 0; i < premium.length; i++){
    	    			if(premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised'){
    	    				vm.TPD.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
    	    				vm.TPD.applicant.cover[0].cost = premium[i].cost;
    	    			} else if(premium[i].coverType === 'TpdFixed' || premium[i].coverType === 'TPDUnitised'){
    	    				vm.TPD.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
    	    				vm.TPD.applicant.cover[1].cost = premium[i].cost;
    	    			} else if(premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised'){
    	    				vm.TPD.applicant.cover[2].additionalCoverAmount = premium[i].coverAmount;
    	    				vm.TPD.applicant.cover[2].cost = premium[i].cost;
            			}
            		}
    	    		// to-do
        			vm.premiumDeathTPD = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost);
    	    		vm.TPD.totalMonthlyPremium = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost) + parseFloat(vm.TPD.applicant.cover[2].cost);
    	    		
    	    		console.log(vm.TPD.applicant.cover[0].additionalCoverAmount, vm.TPD.applicant.cover[1].additionalCoverAmount, vm.TPD.applicant.cover[2].additionalCoverAmount);
    	    		console.log(vm.TPD.applicant.cover[0].cost, vm.TPD.applicant.cover[1].cost, vm.TPD.applicant.cover[2].cost);
    	    	}, function(err){
    	    		console.info('Something went wrong while calculating...' + JSON.stringify(err));
    	    	});
        	}
        };
        
        vm.saveDataForPersistence = function(){
        	
        	vm.fetchAppnum();
        	
            var defer = $q.defer();
            
            var selectedIndustry = vm.IndustryOptions.filter(function(obj){
                return vm.TPD.applicant.industryType === obj.key;
            });
            
            vm.TPD.applicant.occupationCode = selectedIndustry[0].value;            
            
            PersistenceService.setConvertMaintainDetails(vm.TPD);
            defer.resolve();
            return defer.promise;
        };
        
        vm.continueToNextPage = function(){
        	
        	if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && !vm.invalidSalAmount){
        		
        		$timeout(function(){
					 vm.saveDataForPersistence().then(function() {
						$location.path('/summaryConvert/1');
					 }, function(err) {});
				}, 1000);
        		
        	}else{
        		return false;
        	}
        };
	    
	    vm.coverDetailsTransferFormFields = ['contactEmail', 'contactType', 'contactPhone', 'contactPrefTime', 'gender'];
	    vm.occupationDetailsTransferFormFields = ['fifteenHrsQuestion','ownBusinessQue','ownBusinessYesQue','ownBusinessNoQue','areyouperCitzQuestion','industry','occupation','otherOccupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary','premTransFreq'];
	    vm.lifeEventFormFields = [];
	    
        vm.checkPreviousMandatoryFields  = function (elementName,formName){
        	
        	var transferFormFields = [];
	    	
	    	if(formName === 'coverDetailsTransferForm'){
	    		transferFormFields = vm.coverDetailsTransferFormFields;
	    	}else if(formName === 'occupationDetailsTransferForm'){
	    		transferFormFields = vm.occupationDetailsTransferFormFields;
	        }else if(formName === 'lifeEvent'){	        	
	        	transferFormFields = vm.lifeEventFormFields;	        	
	        }
	    	
	        var inx = transferFormFields.indexOf(elementName);	        
	        
	        if(inx > 0){
	        	for(var i = 0; i < inx ; i++){
	        		 if (vm[formName][transferFormFields[i]]){
	        			 vm[formName][transferFormFields[i]].$touched = true;
	        		 }                        
	        	}
	        }
	      
        };
        
        
        vm.isCollapsible = function(targetEle, event) {
	    	
	    	var formdutyValid = vm.formduty.$valid;
	    	var transferPrivacyPolicyFormValid = vm.transferPrivacyPolicyForm.$valid;
	    	var coverDetailsTransferFormValid = vm.coverDetailsTransferForm.$valid;
	    	var occupationDetailsTransferFormValid = vm.occupationDetailsTransferForm.$valid;
	    	var stopPropagation = false;
	    	
	    	
	    	if(targetEle === 'collapseprivacy' && !formdutyValid){
	    		vm.formduty.$submitted = true;
	    		stopPropagation = true;
	        }else if(targetEle === 'PersonalDetailsSection' && (!formdutyValid || !transferPrivacyPolicyFormValid)) {
	        	vm.transferPrivacyPolicyForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'OccupationSection' && (!formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid)) {
	        	vm.coverDetailsTransferForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'ConvertCoverSection' && (vm.invalidSalAmount || !formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid || !occupationDetailsTransferFormValid)){
	        	vm.occupationDetailsTransferForm.$submitted = true;
	        	stopPropagation = true;
	        }
	        
	        if(stopPropagation){
	        	event.stopPropagation();
	        	return false;
	        }
	        
	    };
	    
	    vm.clickToOpen = function (hhText) {
	    	
	    	var templateContent = '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4>';
	    	templateContent += '<div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p>';
	    	templateContent += '<div id="tips_text">'+hhText+'</div><p></p></div></div></div>';
	    	templateContent += '<div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12">';
	    	templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>';
	    	
	    	

			var dialog = ngDialog.open({
				template: templateContent,
				className: 'ngdialog-theme-plain',
				plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		  
	  };
		
		//watchers for form valid and invalid
        
        $scope.$watch('vm.TPD.dodCheck' , function(newVal, oldVal) {
        	$timeout(function() {
	        	if(vm.TPD.dodCheck) {
		        	$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
					$('#ConvertCoverSection').collapse('hide');
	        	}else{
	        		$('#collapseprivacy').collapse('hide');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
					$('#ConvertCoverSection').collapse('hide');
	        	}
        	},10);
        });
        
        $scope.$watch('vm.TPD.privacyCheck', function(newVal, oldVal) {
        	$timeout(function() {
	        	if(vm.TPD.privacyCheck) {
	        		$('#PersonalDetailsSection').collapse('show');
	        		$('#OccupationSection').collapse('hide');
	        		$('#ConvertCoverSection').collapse('hide');
	        	}else{
	        		$('#PersonalDetailsSection').collapse('hide');
	        		$('#OccupationSection').collapse('hide');
	        		$('#ConvertCoverSection').collapse('hide');
	        	}
        	},10);
       });
        
        $scope.$watch('vm.coverDetailsTransferForm.$valid', function(newVal, oldVal) {
        	  if(newVal) {
        		  $('#OccupationSection').collapse('show');
        		  $('#ConvertCoverSection').collapse('hide');
        	  }
          });
          
          $scope.$watch('vm.coverDetailsTransferForm.$invalid', function(newVal, oldVal) {
        	  if(newVal) {
        		  $('#OccupationSection').collapse('hide');
        		  $('#ConvertCoverSection').collapse('hide');        		  
        	  }
          });
          
          $scope.$watch('vm.occupationDetailsTransferForm.$valid', function(newVal, oldVal) {
       	   $timeout(function() {
   	      	  if(newVal && !vm.invalidSalAmount) {
   	      		  vm.calculateCover = true;
   	      		  $('#ConvertCoverSection').collapse('show');   	      		  
   	      	  }
       	   },100);
           });
           
           $scope.$watch('vm.occupationDetailsTransferForm.$invalid', function(newVal, oldVal) {
         	  if(newVal) {
         		 vm.calculateCover = false;
         		  $('#ConvertCoverSection').collapse('hide');
         	  }
           });
           
           $scope.$watch('vm.invalidSalAmount', function(newVal, oldVal) {
           	if(newVal) {
           		$timeout(function() {
           			$('#ConvertCoverSection').collapse('hide');
           		},100);
           	 }else if(vm.occupationDetailsTransferForm.$valid){
           		 $('#ConvertCoverSection').collapse('show');
           	 }
           });		
	}
})(angular);

 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */
