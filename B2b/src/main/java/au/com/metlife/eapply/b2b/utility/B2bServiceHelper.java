package au.com.metlife.eapply.b2b.utility;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import au.com.metlife.eapply.b2b.xsd.beans.Request;

public class B2bServiceHelper {
	
	public static final String XMLSCHEMA="http://www.w3.org/2001/XMLSchema"; 


	public  static Request unMarshallingMInput(String inputXML){
		final String METHOD_NAME = "unMarshallingMInput";
		
		Request input=null;
		try {
			JAXBContext jc = JAXBContext.newInstance (new Class[] {Request.class});            
            Unmarshaller u = jc.createUnmarshaller ();
      
             input = (Request) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8")));
       } catch (Exception e) {
          e.printStackTrace();
       }
       return input;
	}
	
	/**
	 * @param xmlContent
	 * @param xsdFileFullPath
	 * @return
	 */
	public  String validateXMLString(String xmlContent,String xsdpath) throws Exception{
		
		final String METHOD_NAME = "validateXMLString";
		
		String validationMesage="valid";
		//xsdname = "CARE_WebService_schema_new.xsd";
        // 5. Check the document
		Validator validator=null;
        try {


    		SchemaFactory factory =
                SchemaFactory.newInstance(XMLSCHEMA);

            // 2. Compile the schema.
            // Here the schema is loaded from a java.io.File, but you could use
            // a java.net.URL or a javax.xml.transform.Source instead.
    	/*	B2bServiceHelper b2bServiceHelper = new B2bServiceHelper();
    		System.out.println("xsdpath>>"+b2bServiceHelper.xsdpath);*/
    		System.out.println("xsdpath>>"+xsdpath);
            File schemaLocation = new File(xsdpath);
            Schema schema = factory.newSchema(schemaLocation);

            // 3. Get a validator from the schema.
             validator = schema.newValidator();

            validator.setErrorHandler(new XMLErrorHandler());

            String errors=null;

            // 4. Parse the document you want to check.
          //  Source source = new StreamSource(xmlFilePath);
           // Source source = new StreamSource(xmlFilePath);
            StringReader reader = new StringReader(xmlContent);
              if(reader!=null){
              Source source = new StreamSource(reader);
              validator.validate(source);


              errors=((XMLErrorHandler)validator.getErrorHandler()).getErrors();
              
              if(errors!=null  && errors.trim().length()>1){
            	  validationMesage="invalid -{\n"+errors+"\n}";
              }

             }
        }
        catch (Exception ex) {        	
            validationMesage="invalid -{\n 400: Bad xml data passed.";
        }

		return validationMesage;

	}

}
