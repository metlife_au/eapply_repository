function SavedappsConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.savedapps', {
    url: '/savedapps',
    controller: 'savedAppsCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'savedapps/savedapps.html',
    title: 'Saved Application',
    resolve: {
      urls: function(CommonAPI, AppConstants) {
        return !CommonAPI.getUrlList() && CommonAPI.fetchApiUrls(AppConstants.urlFilePath);
      }
    }
  });

};

export default SavedappsConfig;
