export default class EapplyData {
  constructor(AppConstants, $http, $state, $q, ngDialog, CommonAPI) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;
    this._ngDialog = ngDialog;
    this._CommonAPI = CommonAPI;
    this.urlList = this._CommonAPI.getUrlList();
    this.exDthCover = null;
    this.exTpdCover = null;
    this.exIpCover = null;
    this.authToken = this._CommonAPI.getAuthToken();    
    this.eApplyData = {
			'partnerCode':'',
			'requestType': 'CCOVER',
			'calculatorFlag':'premium',
			'quickQuoteRender':'true',
			'applicant': {
				'cover':[{},{},{}],
				'contactDetails':{}
			}
	};
  }

/*
* Application client data will be fetched (fetchClientData) and stored (setClientData) here 
* to reference (getClientData) through out the application
*/
  setClientData(data) {
    this.clientData = data;
  }

  getClientData() {
    return this.clientData || null;
  }

  setApplicantRawXmlData(data) {
    this.applicantRawXmlData = data;
    this.convertRawXmlCoversToApplicationCovers(data.existingCovers.cover);
    this.convertRawXmlToApplicationData();
  }

  getApplicantRawXmlData() {
    return this.applicantRawXmlData;
  }

  setApplicantData(data) {
    this.applicantData = data;
  }

  getApplicantData() {
    return this.applicantData;
  }

/*
* Application cover data will be stored and referenced throught the app here
*  
*/

convertRawXmlToApplicationData() {
  
  this.eApplyData.applicant.age = this.applicantRawXmlData.age;
  this.eApplyData.applicant.dateJoinedFund = this.applicantRawXmlData.dateJoined;
  this.eApplyData.clientRefNumber = this.applicantRawXmlData.clientRefNumber;
  this.eApplyData.applicant.clientRefNumber = this.applicantRawXmlData.clientRefNumber;
  this.eApplyData.applicant.memberType = this.applicantRawXmlData.memberType;
  
  //personalDetails
  this.eApplyData.applicant.firstName = this.applicantRawXmlData.personalDetails.firstName;
  this.eApplyData.applicant.lastName = this.applicantRawXmlData.personalDetails.lastName;
  this.eApplyData.applicant.gender = this.applicantRawXmlData.personalDetails.gender;
  this.eApplyData.applicant.birthDate =  this.applicantRawXmlData.personalDetails.dateOfBirth;
  this.eApplyData.applicant.title =  this.applicantRawXmlData.personalDetails.title;

  //contactDetails
  this.eApplyData.applicant.emailId = this.applicantRawXmlData.contactDetails.emailAddress;
  this.eApplyData.applicant.contactDetails.preferedContactTime = this.applicantRawXmlData.contactDetails.prefContactTime;
  this.eApplyData.applicant.contactDetails.preferedContacType = this.applicantRawXmlData.contactDetails.prefContact;
  this.eApplyData.applicant.customerReferenceNumber  = this.applicantRawXmlData.contactDetails.fundEmailAddress;
  this.eApplyData.applicant.contactDetails.homePhone  = this.applicantRawXmlData.contactDetails.homePhone;
  this.eApplyData.applicant.contactDetails.mobilePhone  = this.applicantRawXmlData.contactDetails.mobilePhone;
  this.eApplyData.applicant.contactDetails.workPhone  = this.applicantRawXmlData.contactDetails.workPhone;
  
  this.eApplyData.applicant.contactDetails.addressLine1 = this.applicantRawXmlData.address.line1;
  this.eApplyData.applicant.contactDetails.addressLine2 = this.applicantRawXmlData.address.line2;
  this.eApplyData.applicant.contactDetails.postCode = this.applicantRawXmlData.address.postCode;
  this.eApplyData.applicant.contactDetails.country = this.applicantRawXmlData.address.country;
  this.eApplyData.applicant.contactDetails.state = this.applicantRawXmlData.address.state;
  this.eApplyData.applicant.contactDetails.suburb = this.applicantRawXmlData.address.suburb; 
  
  this.setApplicantData(this.eApplyData);
}

convertRawXmlCoversToApplicationCovers(covers) {
  let tempExistingCovers = covers;
  let tempExDthCover = null;
  let tempExTpdCover = null;
  let tempExIpCover = null;
  angular.forEach(tempExistingCovers, function (value, key) {
  //for (let value of tempExistingCovers) {
    //this.cleanObj(value);
    if(value.benefitType === '1') {
      //let tempExDthCover = angular.extend({}, this._EapplyData.defaultCovers[0], value);
      tempExDthCover = {
        'benefitType':'1',
        'existingCoverAmount': 0,
        'fixedUnit':value.units,
        'occupationRating': value.occRating,
        'cost':0,
        'coverCategory': value.type === '2' ? 'DcFixed' : 'DcUnitised',
        'existingCoverCategory': value.type === '2' ? 'DcFixed' : 'DcAutomatic',
        'additionalCoverAmount':'',
        'frequencyCostType':'Monthly',
        'optionalUnit':'',        
        'coverStartDate': value.coverStartDate
      };
      
    } else if(value.benefitType === '2') {
      tempExTpdCover = {
        'benefitType':'2',
        'existingCoverAmount': 0,
        'fixedUnit':value.units,
        'occupationRating': value.occRating,
        'cost':0,
        'coverCategory': value.type === '2' ? 'TPDFixed' : 'TPDUnitised',
        'existingCoverCategory': value.type === '2' ? 'TPDFixed' : 'TPDAutomatic',
        'additionalCoverAmount':'',
        'frequencyCostType':'Monthly',
        'optionalUnit':'',
        'coverStartDate': value.coverStartDate
      };
      //let tempExTpdCover = angular.extend({}, this._EapplyData.defaultCovers[1], value);
      
    } else if(value.benefitType === '4') {
      tempExIpCover = {
        'benefitType':'4',
        'existingCoverAmount': 0,
        'optionalUnit':'',
        'existingIpBenefitPeriod': value.benefitPeriod || '2 Years',
        'existingIpWaitingPeriod': value.waitingPeriod || '90 Days',
        'fixedUnit':value.units,
        'occupationRating':value.occRating,
        'cost':0,
        'coverCategory': value.type === '2' ? 'IpFixed' : 'IpUnitised',
        'existingCoverCategory': value.type === '2' ? 'IpFixed' : 'IpUnitised',
        'additionalCoverAmount':'',
        'additionalUnit':null,
        'additionalIpBenefitPeriod': value.benefitPeriod ? value.benefitPeriod: '2 Years',
        'additionalIpWaitingPeriod': value.waitingPeriod ? value.waitingPeriod: '90 Days',
        'totalIpWaitingPeriod': value.waitingPeriod || '90 Days',
        'totalIpBenefitPeriod': value.benefitPeriod || '2 Years',
        'frequencyCostType':'Monthly',
        'coverStartDate': value.coverStartDate
      };
      //let tempExIpCover = angular.extend({}, this._EapplyData.defaultCovers[2], value);
      
    }
  });
  this.setExistingDeathCover(tempExDthCover);
  this.setExistingTpdCover(tempExTpdCover);
  this.setExistingIpCover(tempExIpCover);
}
/*
* Application cover data will be stored and referenced throught the app here
*  
*/ 

  fetchCoversData() {
    return this.defaultCovers;
  }

  setExistingDeathCover(cover) {
    this.exDthCover = cover;
  }

  setExistingTpdCover(cover) {
    this.exTpdCover = cover;
  }

  setExistingIpCover(cover) {
    this.exIpCover = cover;
  }
  getExistingDeathCover() {
    return this.exDthCover || this.defaultCovers[0];
  }

  getExistingTpdCover() {
    return this.exTpdCover || this.defaultCovers[1];
  }

  getExistingIpCover() {
    return this.exIpCover || this.defaultCovers[2];
  }
/*
* Application data will be manipulated here 
* and referenced through out the application
*/
  
  setPdfLocation(data) {
	  this.pdfLocation = data;  
  }
  
  getPdfLocation() {
	  return this.pdfLocation || null;  
  }  

  changeState(path) {
    this._$state.go(path);
  }

    // Utility to clean undefined, null and empty keys from Object
    cleanObj(value) {
      Object.keys(value).forEach((key) => (value[key] == null || value[key] === '') && delete value[key]);
    }
}
