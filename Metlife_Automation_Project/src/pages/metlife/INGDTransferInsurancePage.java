package pages.metlife;


import java.util.List;

import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.CoverDetailsPageObjects;
import uimap.metlife.MetlifeINGDirPayloadPageObjects;

public class INGDTransferInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public INGDTransferInsurancePage enterINGDPayload() {
		TransferINGDInsurance();
		return new INGDTransferInsurancePage(scriptHelper);
	}

	public INGDTransferInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void TransferINGDInsurance() {
				
		try {
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkTransferCover), "Click", "Proceed To Transfer Cover Page");
			sleep(2000);
			//quickSwitchWindows();
			String work15hrs=dataTable.getData("MetLife_Data", "Work 15 hours and more");
			String industry=dataTable.getData("MetLife_Data", "Industry");
			String currentOccupation=dataTable.getData("MetLife_Data", "currentOccupation");
			String annualSalary=dataTable.getData("MetLife_Data", "Annual Salary");
					
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"TAB","Annual Salary");
			sleep(1000);
			String strDeathCover = dataTable.getData("MetLife_Data", "DeathCover");
			String strTPDCover = dataTable.getData("MetLife_Data", "TPDCover");
			String strIPCover = dataTable.getData("MetLife_Data", "change.ipaddlcover");
			String StrWP = dataTable.getData("MetLife_Data", "change.waitngperiod");
			String StrBP = dataTable.getData("MetLife_Data", "change.benefitperiod");
			sleep(1000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.txtTranDeathunits), "SET", "Death Cover",strDeathCover);
			sleep(1000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.txtTranTPDunits), "SET", "TPD Cover",strTPDCover);
			sleep(1000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.txtTranIPunits), "SET", "IP Cover",strIPCover);
			sleep(1000);
			tryAction(fluentWaitElements(MetlifeINGDirPayloadPageObjects.selWaitPeriod), "DropDownSelect", "Waiting Period",StrWP);
			sleep(1000);
			tryAction(fluentWaitElements(MetlifeINGDirPayloadPageObjects.selBenefitPeriod ), "DropDownSelect", "Benefit Period",StrBP);
			sleep(2000);
			tryAction(fluentWaitElements(MetlifeINGDirPayloadPageObjects.selBenefitPeriod),"TAB","Benefit Period");
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCalquote), "Click", "Calculate Quote");
			sleep(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCancelContinue), "Click", "Contiune to Transfer the cover");
			
			List<WebElement> StrHQ = null;
			StrHQ =	fluentWaitListElements(MetlifeINGDirPayloadPageObjects.rdTranHealthQuest);
			
			tryAction(fluentWaitElement(StrHQ.get(1)),"Click","Are you restricted, due to injury or illness, from carrying out the identifiable duties?  - No");
			tryAction(fluentWaitElement(StrHQ.get(3)),"Click","Have you been paid, or are you eligible to be paid, or have you lodged a claim for a TPD benefit ?");
			tryAction(fluentWaitElement(StrHQ.get(5)),"Click","Have you been diagnosed with an illness that reduces your life expectancy?");
			tryAction(fluentWaitElement(StrHQ.get(7)),"Click","Is your cover under the former insurer subject to any premium loadings and/or exclusions?");
			sleep(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCancelContinue), "Click", "Contiune to Transfer the cover");
			} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}
}
