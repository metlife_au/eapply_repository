/**
 *
 */
package pages.metlife.Angular.HOSTPLUS;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Yuvaraj
 *
 */
public class MetflowIntegration_CaseSearch extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public MetflowIntegration_CaseSearch(ScriptHelper scriptHelper) {
		super(scriptHelper);

	}

	public MetflowIntegration_CaseSearch MetFlowCaseSearch() {
		ChangeYourCover();
		HealthandLifeStyle();
		confirmation();

	//	casesearch();
     //	validatedetails();
		return new MetflowIntegration_CaseSearch(scriptHelper);
	}


	public void ChangeYourCover() {

		String EmailId = dataTable.getData("Eapply_Data", "EmailId");
		String TypeofContact = dataTable.getData("Eapply_Data", "TypeofContact");
		String ContactNumber = dataTable.getData("Eapply_Data", "ContactNumber");
		String TimeofContact = dataTable.getData("Eapply_Data", "TimeofContact");
		String Gender = dataTable.getData("Eapply_Data", "Gender");
		String FifteenHoursWork = dataTable.getData("Eapply_Data", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("Eapply_Data", "RegularIncome");
		String Perform_WithoutRestriction = dataTable.getData("Eapply_Data", "Perform_WithoutRestriction");
		String Work_WithoutLimitation = dataTable.getData("Eapply_Data", "Work_WithoutLimitation");
		String Citizen = dataTable.getData("Eapply_Data", "Citizen");
		String IndustryType = dataTable.getData("Eapply_Data", "IndustryType");
		String OccupationType = dataTable.getData("Eapply_Data", "OccupationType");
		String OtherOccupation = dataTable.getData("Eapply_Data", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("Eapply_Data", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("Eapply_Data", "TertiaryQual");
		String HazardousEnv = dataTable.getData("Eapply_Data", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("Eapply_Data", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("Eapply_Data", "AnnualSalary");
		String CostType=dataTable.getData("Eapply_Data", "CostType");
		String DeathYN=dataTable.getData("Eapply_Data", "DeathYN");
		String AmountType=dataTable.getData("Eapply_Data", "AmountType");
		String DeathAction=dataTable.getData("Eapply_Data", "DeathAction");
		String DeathAmount=dataTable.getData("Eapply_Data", "DeathAmount");
		String DeathUnits=dataTable.getData("Eapply_Data", "DeathUnits");
		String TPDYN=dataTable.getData("Eapply_Data", "TPDYN");
		String TPDAction=dataTable.getData("Eapply_Data", "TPDAction");
		String TPDAmount=dataTable.getData("Eapply_Data", "TPDAmount");
		String TPDUnits=dataTable.getData("Eapply_Data", "TPDUnits");
		String IPYN=dataTable.getData("Eapply_Data", "IPYN");
		String IPAction=dataTable.getData("Eapply_Data", "IPAction");
		String Insure90Percent=dataTable.getData("Eapply_Data", "Insure90Percent");
		String WaitingPeriod=dataTable.getData("Eapply_Data", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("Eapply_Data", "BenefitPeriod");
		String IPAmount=dataTable.getData("Eapply_Data", "IPAmount");


		try{



			//****Click on Change Your Insurance Cover Link*****\\
		//	waitForPageTitle("testing", 17);
//			waitForPageTitle("testing", 8);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Change your insurance cover']"))).click();

			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				//		if(driver.findElement(By.xpath("//button[@ng-click='confirm()']")).isDisplayed()){
							sleep(500);
							quickSwitchWindows();
							sleep(1000);
							driver.findElement(By.xpath("//*[@id='ngdialog1']/div[2]/div[1]/div[3]/div/button[1]")).click();
							report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
							sleep(2000);

						}

				//*****Agree to Duty of disclosure*******\\
				waitForPageTitle("testing", 1);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);



	           //*****Agree to Privacy Statement*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
				sleep(200);

				//*****Enter the Email Id*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
				report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
				sleep(500);

				//*****Click on the Contact Number Type****\\

				Select Contact=new Select(driver.findElement(By.name("contactType")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


			//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

				//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]")).click();

					sleep(400);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]")).click();
					sleep(400);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}


				//***********Select the Gender******************\\

				if(Gender.equalsIgnoreCase("Male")){

					driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}

				//****Contact Details-Continue Button*****\\
			/*
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(formOne);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);	*/

				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\

					//*****Select the 15 Hours Question*******\\

				if(FifteenHoursWork.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion']/parent::*"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

			//***Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*****\\

				if(RegularIncome.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
					sleep(300);

					//*****Perform duty without restriction due to illness or injury?*****\\

					if(Perform_WithoutRestriction.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected Yes", Status.PASS);
						sleep(300);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected No", Status.PASS);
						sleep(300);
					}
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
					sleep(300);

	                   //*****Work Without Limitations *****\\

					if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Work Without Limitations", "Selected Yes", Status.PASS);
						sleep(300);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Work Without Limitations", "Selected No", Status.PASS);
						sleep(300);
					}

				}

				//*****Resident of Australia****\\

			/*	if(Citizen.equalsIgnoreCase("Yes")){
					driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

					}
				else{
					driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

					}*/

				//*****Industry********\\
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("industry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(500);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();


				//*****Occupation*****\\

				Select Occupation=new Select(driver.findElement(By.name("occupation")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(500);


				//*****Other Occupation *****\\

				if(OccupationType.equalsIgnoreCase("Other")){

					wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);
				}


				//*****Extra Questions*****\\

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
					sleep(1000);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
				sleep(1000);
			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
						sleep(1000);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					sleep(1000);
			}
			}



			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



			//******Do you work in hazardous environment*****\\

			if(HazardousEnv.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[1]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				sleep(1000);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				sleep(1000);
			}

			//******Do you spend more than 10% of your working time outside of an office environment?*******\\

			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				sleep(1000);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				sleep(1000);
			}
			 sleep(800);

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    	 if(OfficeEnvironment.equalsIgnoreCase("Yes")){

					    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
							sleep(1000);

					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
						sleep(1000);

					}

					      //*****Do You Hold a Tertiary Qualification******\\

					        if(TertiaryQual.equalsIgnoreCase("Yes")){

					        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
								sleep(1000);
					}
					   else{

						   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
							sleep(1000);
					}
					}

		}

	      sleep(800);


		//*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
		    sleep(500);

		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //label[text()='What is your annual salary?']"))).click();
		    sleep(500);
		    sleep(300);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,400)", "");
			sleep(500);

		//*****Click on Continue*******\\
	/*
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(occupationForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		*/


	//**************************************************************************************************************\\
	//****************************COVER CALCULATOR*****************************************************************\\
	//*************************************************************************************************************\\

					//******Click on Cost of  Insurance as*******\\
		sleep(1000);
		Select COSTTYPE=new Select(driver.findElement(By.name("premFreq")));
		COSTTYPE.selectByVisibleText(CostType);
		sleep(250);
		report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
		sleep(500);



					//*******Death Cover Section******\\

		//restricted 3 digits
		/*sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
		sleep(500);	*/

					if(DeathYN.equalsIgnoreCase("Yes")){

				/*		Select deathaction=new Select(driver.findElement(By.name("coverName")));
						deathaction.selectByVisibleText(DeathAction);
						sleep(300);
						report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
						sleep(600);
						*/

	               if(AmountType.equalsIgnoreCase("unit")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
							sleep(800);
							report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

						}
						if(AmountType.equalsIgnoreCase("Convert")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
							sleep(800);
							report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

						}


			//		if(DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){

						if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert")){
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);
							sleep(800);
							report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
							waitForPageTitle("testing", 3);


						}
						else{
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathUnits);
							sleep(800);
							report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
							sleep(800);
							waitForPageTitle("testing", 3);
						}
					}
				//	}

					//*******TPD Cover Section******\\
				    sleep(800);


					if(TPDYN.equalsIgnoreCase("Yes"))
					{

				/*		Select tpdaction=new Select(driver.findElement(By.name("tpdCoverName")));
						tpdaction.selectByVisibleText(TPDAction);
						sleep(500);
						report.updateTestLog("TPD Cover action", TPDAction+" is selected on TPD cover", Status.PASS);
						sleep(800);	*/

				//		if(TPDAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")){

							if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert"))
							{
								sleep(250);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								sleep(500);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDAmount);
								sleep(800);
								report.updateTestLog("TPD Cover Amount", TPDAmount+" is entered", Status.PASS);
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
								sleep(800);
								waitForPageTitle("testing", 3);
							}

							else
							{
								sleep(250);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								sleep(500);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(TPDUnits);
								sleep(800);
								report.updateTestLog("TPD Cover Amount", TPDUnits+" is entered", Status.PASS);
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
								sleep(800);
								waitForPageTitle("testing", 3);
							}

						}
				//	}




				                 //*******IP Cover section********\\
				sleep(500);
				if(IPYN.equalsIgnoreCase("Yes")){
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("window.scrollBy(0,400)", "");
					sleep(500);
			/*
					Select ipaction=new Select(driver.findElement(By.name("ipCoverName")));
					ipaction.selectByVisibleText(IPAction);
					sleep(500);
					report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);*/

					//*****Insure 90% of My Salary******\\

					if(!IPAction.equalsIgnoreCase("No change")){
						if(Insure90Percent.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='ipsalarycheck']/parent::*"))).click();
						report.updateTestLog("Insure 90%", "Insure 90% of My Salary is Selected", Status.PASS);
						sleep(500);
						waitForPageTitle("testing", 1);
						}
					}
					sleep(500);
					if(!IPAction.equalsIgnoreCase("No change")){

						Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.waitingPeriod']")));
						waitingperiod.selectByVisibleText(WaitingPeriod);
						sleep(300);
						report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
						sleep(500);


						Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.benefitPeriod']")));
						benefitperiod.selectByVisibleText(BenefitPeriod);
						sleep(300);
						report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
						sleep(2000);
					}
					sleep(500);
					if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){

						if(!Insure90Percent.equalsIgnoreCase("Yes"))
						{
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(IPAmount);
							sleep(500);
							report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
							sleep(800);
							waitForPageTitle("testing", 1);
						}

					}
				}
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,700)", "");
				sleep(500);


				//*****Click on Calculate Quote********\\

		/*		sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
				report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
				sleep(4000);*/




				//******Click on Continue button******\\

			    //jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			//	sleep(300);


			//	 driver.findElement(By.xpath("(//button[contains(text(),'Continue')])[3]")).click();

				if(DeathYN.equalsIgnoreCase("Yes")||TPDYN.equalsIgnoreCase("Yes")||IPYN.equalsIgnoreCase("Yes")||DeathAction.equalsIgnoreCase("No Change")&&TPDAction.equalsIgnoreCase("No Change")&&IPAction.equalsIgnoreCase("No Change")){
					driver.findElement(By.xpath("//button[@ng-click='goToAura()']")).click();
					report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
					sleep(500);

				}



				//*****Handling the pop-up*******\\

					 if(DeathAction.equalsIgnoreCase("Decrease your cover")||DeathAction.equalsIgnoreCase("Cancel your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")||TPDAction.equalsIgnoreCase("Cancel your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){
//						 driver.findElement(By.xpath("//*[@ng-click='showDecreaseOrCancelPopUp(decCancelMsg)']")).click();
//							report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
//							sleep(500);

//						quickSwitchWindows();
//						sleep(1000);

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='goTo()']"))).click();
						report.updateTestLog("Pop-Up", "Pop-up continue button is Clicked", Status.PASS);
						sleep(300);
//						driver.findElement(By.xpath("//button[@ng-click='goToAura()']")).click();
//						report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
						sleep(1000);


					 }
		//				else{
//						 driver.findElement(By.xpath("//*[@ng-click='goToAura()']")).click();
//						report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
//						sleep(100);
//					 }
				sleep(1000);
				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
			}

		public void HealthandLifeStyle() {

			String Height=dataTable.getData("Eapply_Data", "Height");
			String HeightType=dataTable.getData("Eapply_Data", "HeightType");
			String Weight=dataTable.getData("Eapply_Data", "Weight");
			String WeightType=dataTable.getData("Eapply_Data", "WeightType");
			String Smoker=dataTable.getData("Eapply_Data", "Smoker");
			String Gender = dataTable.getData("Eapply_Data", "Gender");
			String Pregnant = dataTable.getData("Eapply_Data", "Pregnant");

			String Exclusion_3Years_Question_for_TPDandIP=dataTable.getData("Eapply_Data", "Exclusion_3Years_Question_for_TPDandIP");
			String Loading_3Years_Questions_for_Death50_TPD50_IP100=dataTable.getData("Eapply_Data", "Loading_3Years_Questions_for_Death50_TPD50_IP100");
			String Loading_3Years_Questions_for_Death100_TPD100_IP150=dataTable.getData("Eapply_Data", "Loading_3Years_Questions_for_Death100_TPD100_IP150");


			String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore=dataTable.getData("Eapply_Data", "Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");

			String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP=dataTable.getData("Eapply_Data", "Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
			String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP=dataTable.getData("Eapply_Data", "Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");


			String UsualDoctororMedicalCentre=dataTable.getData("Eapply_Data", "UsualDoctororMedicalCentre");
			String UsualDoctor_Name=dataTable.getData("Eapply_Data", "UsualDoctor_Name");
			String UsualDoctor_ContactNumber=dataTable.getData("Eapply_Data", "UsualDoctor_ContactNumber");
			String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50=dataTable.getData("Eapply_Data", "Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
			String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP=dataTable.getData("Eapply_Data", "Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
			String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP=dataTable.getData("Eapply_Data", "Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
			String Drugs_Last5Years=dataTable.getData("Eapply_Data", "Drugs_Last5Years");
			String Alcohol_DrinksPerDay=dataTable.getData("Eapply_Data", "Alcohol_DrinksPerDay");
			String Alcohol_Professionaladvice=dataTable.getData("Eapply_Data", "Alcohol_Professionaladvice");
			String HIVInfected=dataTable.getData("Eapply_Data", "HIVInfected");
			String PriorApplication=dataTable.getData("Eapply_Data", "PriorApplication");
			String PreviousClaims=dataTable.getData("Eapply_Data", "PreviousClaims");
			String PreviousClaims_PaidBenefit_terminalillness=dataTable.getData("Eapply_Data", "PreviousClaims_PaidBenefit_terminalillness");
			String CurrentPolicies=dataTable.getData("Eapply_Data", "CurrentPolicies");
			String DeathAction=dataTable.getData("Eapply_Data", "DeathAction");
			String TPDAction=dataTable.getData("Eapply_Data", "TPDAction");
			String IPAction=dataTable.getData("Eapply_Data", "IPAction");



			try{
				if(DeathAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Increase your cover"))
			{
				WebDriverWait waiting=new WebDriverWait(driver,20);

				//******Enter the Height*******\\


				waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='heightval']"))).sendKeys(Height);
				sleep(200);
				report.updateTestLog("Height", Height+" is Entered", Status.PASS);
				sleep(250);

				WebDriverWait wait=new WebDriverWait(driver,18);
				//******Choose Height Type******\\

				Select heighttype=new Select(driver.findElement(By.xpath("//*[@ng-model='heightDropDown']")));
				heighttype.selectByVisibleText(HeightType);
				sleep(250);
				report.updateTestLog("Height Type", HeightType+" is Selected", Status.PASS);
				sleep(500);

				//******Enter the Weight*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='weightVal']"))).sendKeys(Weight);
				sleep(200);
				report.updateTestLog("Weight",Weight+" is Entered", Status.PASS);
				sleep(250);

				//******Choose Weight Type******\\

				Select weighttype=new Select(driver.findElement(By.xpath("//*[@ng-model='weightDropDown']")));
				weighttype.selectByVisibleText(WeightType);
				sleep(250);
				report.updateTestLog("Weight Type", WeightType+" is Selected", Status.PASS);
				sleep(500);

				//******Have you smoked in the past 12 months?*******\\

				if(Smoker.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Smoker Question", "Yes is Selected", Status.PASS);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Smoker Question", "No is Selected", Status.PASS);
				}
				sleep(800);

				if(Gender.equalsIgnoreCase("Female")){

					//******Are you currently pregnant?*******\\

				if(Pregnant.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Are you currently pregnant?", "Yes is Selected", Status.PASS);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Are you currently pregnant?", "No is Selected", Status.PASS);
				}
				sleep(800);
				}

				//**********************************************************************************************************************\\
				//*******In the last 3 years have you suffered from, been diagnosed with or sought medical advice or treatment for*******\\
				//***********************************************************************************************************************\\

				if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Headaches or migraines')]]/span"))).click();
					sleep(250);
					report.updateTestLog("Last 3 Years Question", "Headaches or migraines is Selected", Status.PASS);
					sleep(800);

				//******Are you currently under investigations or contemplating investigations for your headaches?*****\\

						driver.findElement(By.xpath(" //div/label[text()='Are you currently under investigations or contemplating investigations for your headaches?']/../following-sibling::div/div/div[2]/div/div/label")).click();
						sleep(500);
						report.updateTestLog("Headache or Migraines_Under Current Investigations", "No is Selected", Status.PASS);
						sleep(200);

					//******How would you describe your headaches?******\\

						Select headaches=new Select(driver.findElement(By.xpath("//*[@ng-model='selectedName']")));
						headaches.selectByVisibleText("Recurring or severe episodes");
						sleep(500);
						report.updateTestLog("Type of Headache", "Recurring or severe episodes is Selected", Status.PASS);
						sleep(800);

					//******Select the Headache Type*******\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']"))).click();
						sleep(500);

					//***Have your headaches been fully investigated with all underlying causes excluded?****\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Have your headaches been fully investigated with all underlying causes excluded?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
						sleep(500);
						report.updateTestLog("Is headache fully investigated?","Yes is Selected", Status.PASS);
						sleep(800);

					//*****How many headaches do you suffer in a week?******\\

						Select headachesno=new Select(driver.findElement(By.xpath("(//*[@ng-model='selectedName'])[2]")));
						headachesno.selectByVisibleText("3 episodes or less");
						sleep(500);
						report.updateTestLog("Number of Headaches","3 episodes or less is Selected", Status.PASS);
						sleep(500);

						driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
						sleep(800);



					//*******Is this easily controlled with over the counter medication?******\\


						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is this easily controlled with over the counter medication?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
						sleep(500);
						report.updateTestLog("Can  your headache be controlled over Medication", "Yes is Selected", Status.PASS);
						sleep(500);

				}

				if(Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")||Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Lung or breathing conditions')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Last 3 Years Question", "Lung or breathing conditions is Selected", Status.PASS);
					sleep(500);

					//****What was Your Diagnosis*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Asthma')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Lung or breathing conditions", "Asthma is Selected", Status.PASS);
					sleep(1000);

					//****Is your condition?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Moderate  ']"))).click();
					sleep(500);
					report.updateTestLog("Lung or breathing conditions", "Moderate Asthma is Selected", Status.PASS);
					sleep(500);




					if(Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){

						//****Is your Asthma worsened by your condition****\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
						sleep(500);
						report.updateTestLog("Lung or breathing conditions", "Asthma is worsened by current occupation", Status.PASS);
						sleep(500);

					}
					else{
	                    //****Is your Asthma worsened by your condition****\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
						sleep(500);
						report.updateTestLog("Lung or breathing conditions", "Asthma is not worsened by current occupation", Status.PASS);
						sleep(500);
					}


				}

				if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Last 3 Years Question?","None of the above is Selected", Status.PASS);
					sleep(500);
				}
				sleep(1000);

				//********************************************************************************\\
				//*****************************5 Years Question***********************************\\
				//********************************************************************************\\

				if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'High cholesterol')]]/span"))).click();
				sleep(500);
				report.updateTestLog("Last 5 Years Question", "High Cholesterol is Selected", Status.PASS);
				sleep(500);

			  //******How is your high Cholesterol being treated??******\\

				Select cholesteroltreatment=new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
				cholesteroltreatment.selectByVisibleText("Medication prescribed by your doctor");
				sleep(500);
				report.updateTestLog("High Cholesterol","Treated by medication prescribed by your doctor", Status.PASS);
				sleep(500);

			  //******Select the Treatment Type*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']"))).click();
					sleep(500);

			  //******When did you last have a reading taken?*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'12 months ago or less')]]"))).click();
					sleep(500);
					report.updateTestLog("High Cholesterol","Last reading taken 12 months ago or less", Status.PASS);
					sleep(500);

				//****How did your doctor describe your most recent cholesterol reading?*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Elevated  ']"))).click();
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Elevated  ']"))).click();
					report.updateTestLog("High Cholesterol","Last reading was described by doctor as Elevated", Status.PASS);
					sleep(500);

			   //****Do you recall your last reading?***\\


					driver.findElement(By.xpath("//div/label[text()='Do you recall your last reading?']/../following-sibling::div/div/div[1]/div/div/label")).click();
					sleep(500);
					report.updateTestLog("High Cholesterol","Do you remember the last Reading- Yes", Status.PASS);
					sleep(500);

					//*****What was your most recent cholesterol reading taken by your doctor?****\\

					driver.findElement(By.xpath("(//*[@ng-model='rangeText'])[1]")).sendKeys("7.1");
					sleep(500);
					report.updateTestLog("High Cholesterol","Last reading is 7.1 mmol/L", Status.PASS);
					sleep(500);

					driver.findElement(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]")).click();
					sleep(1000);

					//*****Have you been advised that your triglycerides are elevated?****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been advised that your triglycerides are elevated? ']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("High Cholesterol","Tryglycerides is not Elevated", Status.PASS);
					sleep(800);


				}
				if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[2]"))).click();
					sleep(1000);
					report.updateTestLog("Last 5 Years Question","None of the above is Selected", Status.PASS);
					sleep(800);
				}
				sleep(800);

			//****************************************************************************************************************************************\\
			//**********Have you ever suffered from, been diagnosed with or sought medical advice or treatment for (Please tick all that apply):*******\\
	        //*****************************************************************************************************************************************\\

				if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Bone, joint or limb conditions')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Prior Treatment or Diagnosis Question","Bone,Joint or limb conditions is Selected", Status.PASS);
					sleep(1000);

					//*******What was your diagnosis?********\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Amputation/missing or deformed limb')]]/span"))).click();
					sleep(500);
					report.updateTestLog("What was your diagnosis?","Amputation/missing or deformed limb is Selected", Status.PASS);
					sleep(1000);

					//*******Which limbs were amputated/missing or deformed?*****\\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Arm(s)')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Which limbs were amputated/missing or deformed?"," Arm(s) is Selected", Status.PASS);
					sleep(1200);

					//****was it?****\\

					Select arm=new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
					arm.selectByVisibleText("The left arm");
					sleep(500);
					report.updateTestLog("Which Limb?","The left arm is Selected", Status.PASS);
					sleep(500);

					driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
					sleep(1000);

				}

				if(Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Thyroid conditions')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Prior Treatment or Diagnosis Question","Thyroid Conditions is Selected", Status.PASS);
					sleep(800);

					//*****What was the specific condition*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Hyperthyroidism (Overactive)')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Thyroid Conditions"," Hyperthyroidism (Overactive) is Selected", Status.PASS);
					sleep(800);

					//****Has your condition been fully investigated?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Has your condition been fully investigated?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Thyroid Conditions","Condition has been fully investigated is Selected", Status.PASS);
					sleep(800);

					//****Is your condition Fully Controlled*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is your condition fully controlled?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Thyroid Conditions","Condition is not fully controlled is Selected", Status.PASS);
					sleep(1000);


				}


				if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")&&Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("No")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[3]"))).click();
					sleep(500);
					report.updateTestLog("Prior Treatment or Diagnosis?","None of the above is Selected", Status.PASS);
					sleep(2000);
				}
				sleep(500);



				//******Do you have a usual doctor or medical centre you regularly visit?******\\

				if(UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div/div/div/label)[1]"))).click();
					sleep(1500);
					report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","Yes is Selected", Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']"))).sendKeys(UsualDoctor_Name);
					sleep(1500);
					report.updateTestLog("Usual Doctor Name",UsualDoctor_Name+" is Entered", Status.PASS);
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']"))).click();
					sleep(1500);


					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeText']"))).sendKeys(UsualDoctor_ContactNumber);
					sleep(1500);
					report.updateTestLog("Usual Doctor Contact NUmber",UsualDoctor_ContactNumber+" is Entered", Status.PASS);
					sleep(1000);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeText,x)']"))).click();
					sleep(1500);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Add another Doctor?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1500);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1500);
					report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","No is Selected", Status.PASS);
				}


				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[1]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(6500);

				//******************************************************************************************************\\
				//*********************************Family History*********************************************************\\
				//******************************************************************************************************\\

				//****Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions***\\

				if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(2600);
					report.updateTestLog("Family History Question","Yes is Selected", Status.PASS);

					//***Select the health condition****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'Diabetes')]]/span)[2]"))).click();
					sleep(2600);
					report.updateTestLog("Health condition diagnosed"," Diabetes is Selected", Status.PASS);
					sleep(500);

					//***How many family members were affected?****\\

					driver.findElement(By.xpath("//div/label[text()='How many family members were affected?']/../following-sibling::div/div[1]/input")).sendKeys("3");
					sleep(1000);
					report.updateTestLog("Number of Family members affected","3 is Entered", Status.PASS);
					sleep(400);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]"))).click();
					sleep(500);

					//***were two or more family members diagnosed over age 19****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Were two or more family members diagnosed over the age of 19?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(2600);
					report.updateTestLog("Number of affected Family members over age 19","Two or more is Selected", Status.PASS);
					sleep(1000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(2600);
					report.updateTestLog("Family History Question","No is Selected", Status.PASS);
					sleep(500);
				}


				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[2]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(2600);

				//********************************************************************************************************\\
				//******************************Lifestyle Questions********************************************************\\
				//*********************************************************************************************************\\

				//*******Do you have firm plans to travel or reside in another country*******\\
				if(Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Plans to travel outside the country","Yes is Selected", Status.PASS);
					sleep(500);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Plans to travel outside the country","No is Selected", Status.PASS);
					sleep(1000);
				}


				//******Do you regularly engage in or intend to engage in any of the following hazardous activities*****\\

				if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Water sports')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Do you engage in any Hazardous activities","Water sports is Selected", Status.PASS);
					sleep(1000);



					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Underwater diving')]]/span"))).click();
					sleep(3100);
					report.updateTestLog("Hazardous activity Type","Underwater diving is Selected", Status.PASS);
					sleep(2000);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'More than 40 metres')]]"))).click();
					sleep(3100);
					report.updateTestLog("Dept of diving ","More than 40 metres is Selected", Status.PASS);
					sleep(2000);
				}

				if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")){
					sleep(1000);
					if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
						driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[5]")).click();
						sleep(2000);
					}
					else{
						driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[4]")).click();
						sleep(2000);
					}


					sleep(500);
					report.updateTestLog("Do you engage in any Hazardous activities","None of the above is Selected", Status.PASS);
					sleep(500);
				}






				//*****Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter)
				//or have you exceeded the recommended dosage for any medication?

				if(Drugs_Last5Years.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Have you sed any drugs in the last 5 years","Yes is Selected", Status.PASS);
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Have you used any drugs in the last 5 years","No is Selected", Status.PASS);
					sleep(800);
				}


				//*****How many alcoholic drinks you have in a day?*****\\
				sleep(500);
				driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).click();
				sleep(500);
				driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).sendKeys(Alcohol_DrinksPerDay);
				sleep(500);
				report.updateTestLog("How many alcoholic drinks per day","1 is Selected", Status.PASS);
				sleep(500);
				driver.findElement(By.xpath("//button[text()='Enter']")).click();
				sleep(500);

				//****Have you ever been advised by a health professional to reduce your alcohol consumption?****\\

				if(Alcohol_Professionaladvice.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Advised to reduce alcohol consumption","Yes is Selected", Status.PASS);
					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Advised to reduce alcohol consumption","No is Selected", Status.PASS);
					sleep(1000);
				}


				//******Are you infected with HIV (Human Immunodeficiency Virus******\\

				if(HIVInfected.equalsIgnoreCase("Yes")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Are you infected with HIV","Yes is Selected", Status.PASS);
					sleep(500);

				}
				else{
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(4000);
					report.updateTestLog("Are you infected with HIV","No is Selected", Status.PASS);
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been referred for or waiting on an HIV test result and/or are taking preventative medication?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
				}



				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[3]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(1000);

				//******************************************************************************************************\\
				//****************************General Questions******************************************************\\
				//*******************************************************************************************************\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
				sleep(1000);
				report.updateTestLog("Do you presently suffer from any condition?","No is Selected", Status.PASS);

				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[4]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(3600);

				//***********************************************************************************************\\
				//***************************INSURANCE DETAILS****************************************************\\
				//**************************************************************************************************\\

				//*****Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or
				//accepted with a loading or exclusion or any other special conditions or terms?

				if(PriorApplication.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[1]/div/div[2]/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","Yes is Selected", Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
				}

				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep();
					report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","No is Selected", Status.PASS);
				}




				//****Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers�
				//Compensation, or any other form of compensation due to illness or injury?
				sleep(200);
				if(PreviousClaims.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Have you made a prior claim?","Yes is Selected", Status.PASS);

					if(PreviousClaims_PaidBenefit_terminalillness.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
						sleep(500);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
						sleep(500);
					}

				}
				else{
					sleep(400);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Have you made a prior claim?","No is Selected", Status.PASS);
				}


				if(driver.getPageSource().contains("Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?")){

					//*****Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?
					 if(CurrentPolicies.equalsIgnoreCase("yes")){
						 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-19:0']"))).click();
							sleep(500);
							report.updateTestLog("Do you currently have any insurance policies?","Yes is Selected", Status.PASS);

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-19_0_0:0']"))).click();
							sleep(1500);

							wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-19_0_0_0_0"))).sendKeys("90000");
							sleep(1500);

							wait.until(ExpectedConditions.elementToBeClickable(By.id("ran219_0_0_0_0-Insurance_Details-Insurance_Details-19"))).click();
							sleep(1500);
					 }
					 else{
						 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
							sleep(1500);
							report.updateTestLog("Do you currently have any insurance policies?","No is Selected", Status.PASS);
					 }

				}




						//****insurance detail click on Continue****\\

				/*		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@ng-click='collapseUncollapse($index,section.sectionName)'])[5]"))).click();
						report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
						sleep(500);

						  sleep(300);
							JavascriptExecutor jse = (JavascriptExecutor) driver;
							jse.executeScript("window.scrollBy(0,500)", "");
							sleep(500);*/

		        //****Treatment question****\\

				/*	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@class='ipradio']/parent::*)[11]"))).click();
					report.updateTestLog("Treatment Question","No button is Clicked", Status.PASS);
					sleep(1000);*/

					//****Click on Continue****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedToNext()']"))).click();
					report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
					sleep(4000);

		//		sleep(20000);
//				waitForPageTitle("testing", 8);

				}
			}
			catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

		public void confirmation() {


			WebDriverWait wait=new WebDriverWait(driver,20);

				try{

					if(driver.getPageSource().contains("I acknowledge I have read and understood the above exclusion(s) and/or loading(s) which apply to this cover.")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@id='lodadingExclusionLabel']/span"))).click();
						report.updateTestLog("Exclusion(s) and/or loading(s) acknowledgement checkbox: ", "Checkbox is clicked", Status.PASS);

					}

					if(driver.getPageSource().contains("I acknowledge and consent to the above.")){

				//****Loading consent*****\\
				sleep(1000);
				if(driver.getPageSource().contains("We wish to note that in considering the information disclosed in your personal statement")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='loading_chckbox_id__xc_c']/span"))).click();
					report.updateTestLog("Loading Consent", "selected the Checkbox", Status.PASS);
					sleep(500);
				}

				 WebElement Firstname=driver.findElement(By.xpath("(//strong[text()='First name']/following::label)[1]"));
				 String Firstname_validation=Firstname.getText();
			//	 System.out.println("Firstname:-->"+Firstname_validation);
				 WebElement LastNameapp=driver.findElement(By.xpath("(//strong[text()='Last name']/following::label)[1]"));
				 String LastName_validation=LastNameapp.getText();
			//	 System.out.println("LastName:-->"+LastName_validation);
				 WebElement Genders=driver.findElement(By.xpath("(//strong[text()='Gender']/following::label)[1]"));
				 String Genders_validation=Genders.getText();
			//	 System.out.println("Genders:-->"+Genders_validation);
				 WebElement contacttype=driver.findElement(By.xpath("(//strong[text()='Preferred contact type']/following::label)[1]"));
				 String contacttype_validation=contacttype.getText();
			//	 System.out.println("contacttype:-->"+contacttype_validation);
				 sleep(500);
				 WebElement contactnumber=driver.findElement(By.xpath("(//strong[text()='Preferred contact number ']/following::label)[1]"));
				 String contactnumber_validation=contactnumber.getText();
			//	 System.out.println("contactnumber:-->"+contactnumber_validation);
				 WebElement Email=driver.findElement(By.xpath("(//strong[text()='Email address']/following::label)[1]"));
				 String Email_validation=Email.getText();
			//	 System.out.println("Email:-->"+Email_validation);
				 WebElement DOB=driver.findElement(By.xpath("(//strong[text()='Date of birth']/following::label)[1]"));
				 String DOB_validation=DOB.getText();
			//	 System.out.println("DOB:-->"+DOB_validation);
				 WebElement Timeofcontact=driver.findElement(By.xpath("(//strong[text()='What time of day do you prefer to be contacted?']/following::label)[1]"));
				 String Timeofcontact_validation=Timeofcontact.getText();
		//		 System.out.println("Timeofcontact"+Timeofcontact_validation);
				 sleep(500);
				 WebElement DeathExisitngcover=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[1]"));
				 String DeathExisitngamt_validation=DeathExisitngcover.getText();

				 StringBuilder deathamt  = new StringBuilder(DeathExisitngamt_validation);
				 StringBuilder deathamountvalue= deathamt.append(".00");
				 sleep(1000);
				 String deathAmnt = deathamountvalue.toString();
				 String DeathExisitngcover_validation = deathAmnt.replace("$"," $ ");
		//		 System.out.println("DeathExisitngcover" +DeathExisitngcover_validation);
				 WebElement TPDExisitngcover=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[5]"));
				 String TPDExisitngamt_validation=TPDExisitngcover.getText();

				 sleep(500);

				 StringBuilder Tpdamt  = new StringBuilder(TPDExisitngamt_validation);
				 StringBuilder Tpdamtamountvalue= Tpdamt.append(".00");
				 sleep(1000);
				 String TpdAmnt = Tpdamtamountvalue.toString();
				 String TPDExisitngcover_validation = TpdAmnt.replace("$"," $ ");
		//		 System.out.println("TPDExisitngcover" +TPDExisitngcover_validation);

				 WebElement IPNExisitngcover=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[9]"));
				 String IPExisitngamt_validation=IPNExisitngcover.getText();



				 StringBuilder IPExistingamt  = new StringBuilder(IPExisitngamt_validation);
				 StringBuilder IPExistingvalue= IPExistingamt.append(".00");
				 sleep(1000);
				 String IPExistingAmt = IPExistingvalue.toString();
				 String IPExisitngcover_validation = IPExistingAmt.replace("$"," $ ");
		//		 System.out.println("IPNExisitngcover" +IPExisitngcover_validation);


				 WebElement DeathNewcover=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[2]"));
				 String DeathNewcoverAmt_validation=DeathNewcover.getText();


				 StringBuilder DeathNewAmt  = new StringBuilder(DeathNewcoverAmt_validation);
				 StringBuilder DeathNewAmtvalue= DeathNewAmt.append(".00");
				 sleep(1000);
				 String deatham = DeathNewAmtvalue.toString();
				 String DeathNewcover_validation = deatham.replace("$"," $ ");
		//		 System.out.println("DeathNewcover" +DeathNewcover_validation);
				 sleep(500);


				 WebElement TPDNewcover=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[6]"));
				 String TPDNewcoverAmt_validation=TPDNewcover.getText();
				 StringBuilder TPDNewcoverAmt  = new StringBuilder(TPDNewcoverAmt_validation);
				 StringBuilder TPDNewcoverAmtvalue= TPDNewcoverAmt.append(".00");
				 sleep(1000);
				 String TPDNewcoverAmnt = TPDNewcoverAmtvalue.toString();
				 String TPDNewcover_validation = TPDNewcoverAmnt.replace("$"," $ ");
				 sleep(500);
		//		 System.out.println("TPDNewcover"  +TPDNewcover_validation);

				 WebElement IPNewcover=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[10]"));
				 String IPNewcoverAmt_validation=IPNewcover.getText();
				 StringBuilder IPNewcoverAmt  = new StringBuilder(IPNewcoverAmt_validation);
				 StringBuilder IPNewcoverAmtvalue= IPNewcoverAmt.append(".00");
				 sleep(1000);
				 String IPNewcoverAmount = IPNewcoverAmtvalue.toString();
				 String IPNewcover_validation = IPNewcoverAmount.replace("$"," $ ");
		//		 System.out.println("IPNewcover"+IPNewcover_validation);

				 WebElement Waitingperiod=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[12]"));
				 String Waitingperiod_validation=Waitingperiod.getText();
	//			 System.out.println("Waitingperiod"+Waitingperiod_validation);
				 WebElement Benefitperiod=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[13]"));
				 String Benefitperiod_validation=Benefitperiod.getText();
	//			 System.out.println("Benefitperiod"+Benefitperiod_validation);
				 sleep(500);

				 WebElement DeathWeeklycost=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[4]"));
				 String DeathWeeklycostAmt_validation=DeathWeeklycost.getText();
				// StringBuilder DeathWeeklycostAmt  = new StringBuilder(DeathWeeklycostAmt_validation);
			//	 StringBuilder DeathWeeklycostAmtvalue= DeathWeeklycostAmt.append(".00");
				 sleep(1000);
			//	 String DeathWeeklycostAmount = DeathWeeklycostAmt_validation.toString();
				 String DeathWeeklycost_validation = DeathWeeklycostAmt_validation.replace("$","$ ");
	//			 System.out.println("DeathWeeklycost" +DeathWeeklycost_validation);


				 WebElement TPDWeeklycost=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[8]"));
				 String TPDWeeklycostAmt_validation=TPDWeeklycost.getText();
				 StringBuilder TPDWeeklycostAmt  = new StringBuilder(TPDWeeklycostAmt_validation);
			//	 StringBuilder TPDWeeklycostAmtvalue= TPDWeeklycostAmt.append(".00");
				 sleep(1000);
				 String TPDWeeklycostAmount = TPDWeeklycostAmt.toString();
				 String TPDWeeklycost_validation = TPDWeeklycostAmount.replace("$"," $ ");
				 sleep(500);
	//			 System.out.println("TPDWeeklycost"  +TPDWeeklycost_validation);

				 WebElement IPWeeklycost=driver.findElement(By.xpath("(//p[@class='color3 ng-binding'])[14]"));
				 String IPWeeklycostAmt_validation=IPWeeklycost.getText();
				 StringBuilder IPWeeklycostAmt  = new StringBuilder(IPWeeklycostAmt_validation);
				// StringBuilder IpWeeklycostAmtvalue= IPWeeklycostAmt.append(".00");
				 sleep(1000);
				 String IPWeeklycostAmount = IPWeeklycostAmt.toString();
				 String IPWeeklycost_validation = IPWeeklycostAmount.replace("$"," $ ");
				 sleep(500);
	//			 System.out.println("IPWeeklycost" +IPWeeklycost_validation);

				 sleep(500);
				 WebElement TotalWeeklycost=driver.findElement(By.xpath("//h4[@class='coverval ng-binding']"));
				 String TotalWeeklycost_validation=TotalWeeklycost.getText();
	//			 System.out.println("TotalWeeklycost"+TotalWeeklycost_validation);
				//******Agree to general Consent*******\\


				    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span"))).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(500);




				//******Click on Submit*******\\
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
					report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
					sleep(7500);




					//*******Feedback popup******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
					report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
					sleep(2000);

					//*****Fetching the Application Status*****\\

					String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
					sleep(500);
					report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);
					sleep(600);

					String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
					sleep(1000);
				//	System.out.println("Application #---->"+App);
					sleep(1000);
					report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);





					//Eveiw application

					try
					{

						String fundname = dataTable.getData("Eapply_Data", "fundname");
						String FirstName = dataTable.getData("Eapply_Data", "FirstName");
						String LastName = dataTable.getData("Eapply_Data", "LastName");

						driver.get("https://www.e2e.eservice.metlife.com.au/wps/portal");
						report.updateTestLog("Eveiw application:", "Eveiw application URL lauched successfully", Status.PASS);
						sleep(1000);
						driver.findElement(By.name("MLPAuLoginPortletFormID")).sendKeys("deepam");
						sleep(500);
						report.updateTestLog("Eview Username", "Username Entered", Status.PASS);
						driver.findElement(By.name("MLPAuLoginPortletFormPassword")).sendKeys("Metlife321");
						sleep(500);
						report.updateTestLog("Eview Password", "Password Entered", Status.PASS);
						driver.findElement(By.className("submit")).click();
						sleep(500);
						report.updateTestLog("Eview Login", "Submit button Clicked", Status.PASS);
						Actions actions = new Actions(driver);
						WebElement equery = driver.findElement(By.xpath("//strong[text()='eQuery']"));
						actions.moveToElement(equery);
						sleep(500);
						WebElement underwritingPDF = driver.findElement(By.xpath("//ul[@class='fleft']/li/a[text()='Underwriting PDFs']"));
						actions.moveToElement(underwritingPDF);
						actions.click().build().perform();
						report.updateTestLog("Underwriting PDF:", "Underwriting PDF selected", Status.PASS);
						sleep(1000);
						Select fund=new Select(driver.findElement(By.name("fundId")));
						fund.selectByVisibleText(fundname);
						report.updateTestLog("Fund Name:", "Fund: "+fundname+" is Selected", Status.PASS);
						sleep(500);
						driver.findElement(By.id("ns_Z7_B0SGQO48A8RD30AKB94J6J10G0_fName")).sendKeys(Firstname_validation);
						sleep(1000);
						report.updateTestLog("Firstname:",  Firstname_validation+" is entered", Status.PASS);
						driver.findElement(By.id("ns_Z7_B0SGQO48A8RD30AKB94J6J10G0_sName")).sendKeys(LastName_validation);
						sleep(1000);
						report.updateTestLog("LastName:",  LastName_validation+" is entered", Status.PASS);
						driver.findElement(By.xpath("//button[@class='submit']/span")).click();
						sleep(2000);
						report.updateTestLog("Search Button:","Search button is Clicked", Status.PASS);


						 StringBuilder Campaign  = new StringBuilder("HOST1217WG-");
						 StringBuilder Campaigncode= Campaign.append(App);
						 sleep(1000);
						 String Campaignc = Campaigncode.toString();
					//	System.out.println("Campaign-->" +Campaignc);
						String CampaignApp = driver.findElement(By.xpath("//td[text()='"+Campaignc+"']")).getText();
					//	System.out.println("Camp-->" +CampaignApp);
						sleep(1000);
						if(CampaignApp.contains(Campaignc)){
							report.updateTestLog("PDF Verification", "Underwriting/Client PDF's is generated in Eview", Status.PASS);
							sleep(1000);
						}


					}catch(Exception e){
						report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
					}



						//Metflow integration


					driver.get("http://ausydbf1u01.alico.corp/bizflow/index.jsp");

					report.updateTestLog("Metflow Application", "Invoked the Metflow Application",Status.PASS);
					sleep(1000);
					String strLoginName = dataTable.getData("Eapply_Data", "Username");
					String strLoginPassword = dataTable.getData("Eapply_Data", "Password");
					driver.get("http://ausydbf1u01.alico.corp/bizflow/index.jsp");
					Thread.sleep(1000);
					WebElement fram =driver.findElement(By.id("fraMain"));
					driver.switchTo().frame(fram);
					driver.findElement(By.xpath("//*[@class='loginId']")).sendKeys(strLoginName);
					report.updateTestLog("Metflow Login", "Metflow Username is entered Successfully",Status.PASS);
					driver.findElement(By.xpath("//*[@class='password']")).sendKeys(strLoginPassword);
					report.updateTestLog("Metflow Password", "Metflow Password is entered Successfully",Status.PASS);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					driver.findElement(By.xpath("//*[@class='login-button']")).click();
					driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
					try{
						   driver.switchTo().alert().accept();
						   sleep(3000);
						 //  System.out.println("Alert Handled");
						  }catch(Exception e){
						//   System.out.println("");
						  }
					Thread.sleep(3000);
					report.updateTestLog("Metflow Login", "Application Logged in successfully",Status.PASS);





	//	String AppNo = dataTable.getData("Eapply_Data", "AppNo");
		/*HOSTPLUS_Angular_ChangeYourInsuranceCover obj= new HOSTPLUS_Angular_ChangeYourInsuranceCover(scriptHelper);
		String App = obj.confirmation();*/
	/*	System.out.println("App variable inside MetFlow Integration --->"+App);*/

		switchToMainFrame();
		WebElement workarea=driver.findElement(By.id("workAreaFrame"));
		driver.switchTo().frame(workarea);

		//***Click on Case search Tab*****\\
		//WebDriverWait wait=new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='topmenu']//span[contains(text(),'Case Search')]"))).click();
		report.updateTestLog("Case Search", "Case Search Tab is clicked", Status.PASS);
		sleep(5000);

		//****Loop Through Frames****\\
		WebElement MLA=driver.findElement(By.name("MLA_CaseSearch"));
		driver.switchTo().frame(MLA);

		WebElement Bizcoverframe=driver.findElement(By.id("frame1000214"));
		driver.switchTo().frame(Bizcoverframe);


		//Entering the Application No\\App
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("CaseSearchExternalReferenceNo"))).sendKeys(App);
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("CaseSearchExternalReferenceNo"))).sendKeys(AppNo);
		report.updateTestLog("Application Number",App+" is entered is the search field", Status.PASS);


		//Clicking on Search button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("CaseSearchButton"))).click();
		report.updateTestLog("Case Search", "Case search button is clicked", Status.PASS);
		sleep(4000);

		//Validating the presence of Application\\
		WebElement Result=driver.findElement(By.xpath("//*[@id='CaseSearchResultHolderTable_body']//td"));
		if(Result.getText().equalsIgnoreCase("No Data Found")){

			wait.until(ExpectedConditions.elementToBeClickable(By.id("CaseSearchButton"))).click();
			sleep(80000);
		//	System.out.println("clicked");
			wait.until(ExpectedConditions.elementToBeClickable(By.id("CaseSearchButton"))).click();
			sleep(80000);
	//		System.out.println("clicked");
			wait.until(ExpectedConditions.elementToBeClickable(By.id("CaseSearchButton"))).click();
			sleep(20000);
	//		System.out.println("clicked");
		/*	wait.until(ExpectedConditions.elementToBeClickable(By.id("CaseSearchButton"))).click();
			sleep(80000);*/
			/*wait.until(ExpectedConditions.elementToBeClickable(By.id("CaseSearchButton"))).click();
			sleep(1000);
			System.out.println("clicked");*/

		}

			WebElement Case=driver.findElement(By.xpath("//*[@id='CaseSearchResultHolderTable_body']//td[2]//span"));
			String CaseId=Case.getText();
			report.updateTestLog("Case ID", CaseId+" is the Case ID", Status.PASS);

			WebElement status=driver.findElement(By.xpath("(//*[contains(@id,'content_result')]/td[7]/span)[1]"));
			String CaseStatus=status.getText();
			report.updateTestLog("Case Status", CaseStatus+" is the Case staus", Status.PASS);

			sleep(2000);
			Case.click();
			sleep(2000);
			try{
				   driver.switchTo().alert().accept();
				   sleep(3000);
			//	   System.out.println("Alert Handled");
				  }catch(Exception e){
			//	   System.out.println("");
				  }
			report.updateTestLog("View Details", "CaseID is Selected", Status.PASS);
			sleep(3000);
			quickSwitchWindows();
			sleep(3000);



//	}

/*	catch(Exception e){
		report.updateTestLog("Requirement Status", "Requirement Status is not updated yet", Status.SCREENSHOT);
		}
}*/

//	public void validatedetails() {
		String ExpLOB = dataTable.getData("Eapply_Data", "LOB");
		String ExpAppType = dataTable.getData("Eapply_Data", "ApplicationType");
		String ExpTitle = dataTable.getData("Eapply_Data", "Title");
		String ExpFN = dataTable.getData("Eapply_Data", "FirstName");
		String ExpLN = dataTable.getData("Eapply_Data", "LastName");
		String ExpDOB = dataTable.getData("Eapply_Data", "DOB");
		String ExpGender = dataTable.getData("Eapply_Data", "Gender");
		String ExpNationality = dataTable.getData("Eapply_Data", "Nationality");
		String ExpAddressType = dataTable.getData("Eapply_Data", "AddressType");
		String ExpAddress = dataTable.getData("Eapply_Data", "Address");
		String ExpCity = dataTable.getData("Eapply_Data", "City");
		String ExpState = dataTable.getData("Eapply_Data", "State");
		String ExpPostCode = dataTable.getData("Eapply_Data", "PostCode");
		String ExpCountry = dataTable.getData("Eapply_Data", "Country");
		String ExpContactNumber = dataTable.getData("Eapply_Data", "ContactNumber");
		String ExpEmail = dataTable.getData("Eapply_Data", "Email");
		String ExpContactTime = dataTable.getData("Eapply_Data", "ContactTime");
		String ExpAgeNextBday = dataTable.getData("Eapply_Data", "AgeNextBday");
		String Expheight = dataTable.getData("Eapply_Data", "height");
		String Expweight = dataTable.getData("Eapply_Data", "weight");
		String ExpBMI = dataTable.getData("Eapply_Data", "BMI");
		String ExpMemberID = dataTable.getData("Eapply_Data", "MemberId");
		String SupportDocName = dataTable.getData("Eapply_Data", "SupportDocName");
		String ExpJoinDate = dataTable.getData("Eapply_Data", "FundJoinDate");
		String ExpSmoker = dataTable.getData("Eapply_Data", "ExpSmoker");
		String ExpExisWorkScale = dataTable.getData("Eapply_Data", "ExisWorkScale");
		String ExpNewWorkScale = dataTable.getData("Eapply_Data", "NewWorkScale");
		String ExpPremiumFreq = dataTable.getData("Eapply_Data", "PremiumFreq");
		String ExpFundName = dataTable.getData("Eapply_Data", "FundName");
		String CoverType = dataTable.getData("Eapply_Data", "CoverType");
		String ValidateDeathCover = dataTable.getData("Eapply_Data", "ValidateDeathCover");
		String ValidateTPDCover = dataTable.getData("Eapply_Data", "ValidateTPDCover");
		String ValidateIPCover = dataTable.getData("Eapply_Data", "ValidateIPCover");

		String DeathCover = dataTable.getData("Eapply_Data", "DeathCover");
		String TraumaCover = dataTable.getData("Eapply_Data", "TraumaCover");
		String TPDCover = dataTable.getData("Eapply_Data", "TPDCover");
		String IPCover = dataTable.getData("Eapply_Data", "IPCover");
		String ExpDeathCoverAmt = dataTable.getData("Eapply_Data", "ExistingDeathCover");
		String ExpDeathAddCoverAmt = dataTable.getData("Eapply_Data", "AdditionalDeathCover");
		String ExpDeathTotalCoverAmt = dataTable.getData("Eapply_Data", "TotaloDeathCover");
		String ExpDeathCoverUnits = dataTable.getData("Eapply_Data", "ExistingDeathUnits");
		String ExpDeathAddCoverUnits = dataTable.getData("Eapply_Data", "AdditionalDeathUnits");
		String ExpDeathTotalCoverUnits = dataTable.getData("Eapply_Data", "TotalDeathUnits");
		String ExpDeathPremium = dataTable.getData("Eapply_Data", "DeathPremium");
		String ExpTPDCoverAmt = dataTable.getData("Eapply_Data", "ExistingTPDCover");
		String ExpTPDAddCoverAmt = dataTable.getData("Eapply_Data", "AdditionalTPDCover");
		String ExpTPDTotalCoverAmt = dataTable.getData("Eapply_Data", "TotalTPDCover");
		String ExpTPDCoverUnits = dataTable.getData("Eapply_Data", "ExistingTPDUnits");
		String ExpTPDAddCoverUnits = dataTable.getData("Eapply_Data", "AdditionalTPDUnits");
		String ExpTPDTotalCoverUnits = dataTable.getData("Eapply_Data", "TotalTPDUnits");
		String ExpTPDPremium = dataTable.getData("Eapply_Data", "TPDPremium");
		String ExpIPCoverAmt = dataTable.getData("Eapply_Data", "ExistingIPCover");
		String ExpIPAddCoverAmt = dataTable.getData("Eapply_Data", "AdditionalIPCover");
		String ExpIPTotalCoverAmt = dataTable.getData("Eapply_Data", "TotalIPCover");
		String ExpIPPremium = dataTable.getData("Eapply_Data", "IPPremium");
		String ExpWaitingPeriod = dataTable.getData("Eapply_Data", "IPWaitingPeriod");
		String ExpBenefitPeriod = dataTable.getData("Eapply_Data", "IPBenefitPeriod");

//		try {

			// ***Validate Line of Business*****\\

			WebElement LOB = driver.findElement(By.id("csLOBID"));
			LOB.click();
			String ActualLOB = LOB.getText();

			if (ActualLOB.equalsIgnoreCase(ExpLOB)) {
				report.updateTestLog("LOB", ActualLOB + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("LOB", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate the Application Category*****\\

			WebElement ApplicationType = driver.findElement(By.id("csApplicationTypeID"));
			ApplicationType.click();
			String ActualAppType = ApplicationType.getText();

			if (ActualAppType.contains(ExpAppType)) {
				report.updateTestLog("Application Type", ActualAppType + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Application Type", "is not displayed as expected", Status.FAIL);
			}

			sleep(250);
			// *************************************************************\\
			// *****MEMBER DETAILS SECTION***********************************\\
			// **************************************************************\\

			// ******Validate Title********\\

			WebElement Title = driver.findElement(By.id("csTitle"));
			Title.click();
			String ActualTitle = Title.getText();

			if (ActualTitle.equalsIgnoreCase(ExpTitle)) {
				report.updateTestLog("Title", ActualTitle + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Title", "is not displayed as expected", Status.FAIL);
			}

			// ******Validate FirstName******\\

			WebElement FirstName = driver.findElement(By.id("csFirstName"));
			FirstName.click();
			String ActualFN = FirstName.getText();

			if (ActualFN.equalsIgnoreCase(Firstname_validation)) {
				report.updateTestLog("First Name", ActualFN + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("First Name", "is not displayed as expected", Status.FAIL);
			}



			// *****Validate LastName******\\

			WebElement LastName = driver.findElement(By.id("csLastName"));
			LastName.click();
			String ActualLN = LastName.getText();

			if (ActualLN.equalsIgnoreCase(LastName_validation)) {
				report.updateTestLog("Last Name", ActualLN + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Last Name", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate DOB**********\\

			WebElement Dateofbirth = driver.findElement(By.id("csDOB"));
			Dateofbirth.click();
			String ActualDOB = Dateofbirth.getText();
			if (ActualDOB.equalsIgnoreCase(DOB_validation)) {
				report.updateTestLog("Date of Birth", ActualDOB + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Date of Birth", "is not displayed as expected", Status.FAIL);
			}

			// ******Validate Gender******\\

			WebElement Gender = driver.findElement(By.id("csGender"));
			Gender.click();
			String ActualGender = Gender.getText();

			if (ActualGender.equalsIgnoreCase(Genders_validation)) {
				report.updateTestLog("Gender", ActualGender + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Gender", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Nationality*****\\

			WebElement Nationality = driver.findElement(By.id("csNationalityID"));
			Nationality.click();
			String ActualNationality = Nationality.getText();

			if (ActualNationality.equalsIgnoreCase(ExpNationality)) {
				report.updateTestLog("Nationality", ActualNationality + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Nationality", "is not displayed as expected", Status.FAIL);
			}

			// ******Validate Address Type****\\

			WebElement AddressType = driver.findElement(By.id("csAddressType"));
			AddressType.click();
			String ActualAddressType = AddressType.getText();

			if (ActualAddressType.equalsIgnoreCase(ExpAddressType)) {
				report.updateTestLog("Address Type", ActualAddressType + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Address Type", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Address*****\\

			WebElement AddressLine = driver.findElement(By.id("csAddress1"));
			AddressLine.click();
			String ActualAddress = AddressLine.getText();

			if (ActualAddress.equalsIgnoreCase(ExpAddress)) {
				report.updateTestLog("Address", ActualAddress + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Address", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate City*****\\

			WebElement City = driver.findElement(By.id("csSuburb"));
			City.click();
			String ActualCity = City.getText();

			if (ActualCity.equalsIgnoreCase(ExpCity)) {
				report.updateTestLog("City", ActualCity + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("City", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate State*****\\

			WebElement State = driver.findElement(By.id("csState"));
			State.click();
			String ActualState = State.getText();

			if (ActualState.equalsIgnoreCase(ExpState)) {
				report.updateTestLog("State", ActualState + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("State", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate PostCode*****\\

			WebElement PostCode = driver.findElement(By.id("csPostCode"));
			PostCode.click();
			String ActualPostCode = PostCode.getText();

			if (ActualPostCode.equalsIgnoreCase(ExpPostCode)) {
				report.updateTestLog("Post Code", ActualPostCode + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Post Code", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Country*****\\

			WebElement Country = driver.findElement(By.id("csCountryID"));
			Country.click();
			String ActualCountry = Country.getText();

			if (ActualCountry.equalsIgnoreCase(ExpCountry)) {
				report.updateTestLog("Country", ActualCountry + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Country", "is not displayed as expected", Status.FAIL);
			}


			// *****Validate Contact Number*****\\

			WebElement ContactNumber = driver.findElement(By.id("csContactNo"));
			ContactNumber.click();
			String ActualNumber = ContactNumber.getText();

			if (ActualNumber.equalsIgnoreCase(contactnumber_validation)) {
				report.updateTestLog("Contact Number", ActualNumber + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Contact Number", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Email Address*****\\

			WebElement EmailAddress = driver.findElement(By.id("csEmailAddress"));
			EmailAddress.click();
			String ActualEmail = EmailAddress.getText();

			if (ActualEmail.equalsIgnoreCase(Email_validation)) {
				report.updateTestLog("Email Address", ActualEmail + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Email Address", "is not displayed as expected", Status.FAIL);
			}

			// ****Validate preferred Contact time*****\\

			WebElement ContactTime = driver.findElement(By.id("csPreferredContactTime"));
			ContactTime.click();
			String ActualContactTime = ContactTime.getText();
			//Timeofcontact_validation
			if (ActualContactTime.contains(ExpContactTime)) {
				report.updateTestLog("Contact Time", ActualContactTime + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Contact Time", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Date of birth under member details sub
			// section****\\

			WebElement DateofBirth = driver.findElement(By.id("csGDOB"));
			DateofBirth.click();
			String ActualDateofBirth = DateofBirth.getText();

			if (ActualDateofBirth.equalsIgnoreCase(DOB_validation)) {
				report.updateTestLog("Date of Birth", ActualDateofBirth + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Date of Birth", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Age Next Birthday******\\

			WebElement AgeNextBday = driver.findElement(By.id("csGAgeNextBirthday"));
			AgeNextBday.click();
			String ActualAgeNextBday = AgeNextBday.getText();

			if (ActualAgeNextBday.equalsIgnoreCase(ExpAgeNextBday)) {
				report.updateTestLog("Age Next Birthday", ActualAgeNextBday + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Age Next Birthday", "is not displayed as expected", Status.FAIL);
			}

			/*
			 * //******Validate Height*******\\
			 *
			 * WebElement height=driver.findElement(By.id("csGHeight"));
			 * height.click(); String Actualheight=height.getText();
			 *
			 * if(Actualheight.equalsIgnoreCase(Expheight)){
			 * report.updateTestLog("Height",
			 * Actualheight+" is displayed as expected", Status.PASS); } else{
			 * report.updateTestLog("Height", "is not displayed as expected",
			 * Status.FAIL); }
			 *
			 * //******Validate Weight********\\
			 *
			 * WebElement weight=driver.findElement(By.id("csGWeight"));
			 * weight.click(); String Actualweight=weight.getText();
			 *
			 * if(Actualweight.equalsIgnoreCase(Expweight)){
			 * report.updateTestLog("Weight",
			 * Actualweight+" is displayed as expected", Status.PASS); } else{
			 * report.updateTestLog("Weight", "is not displayed as expected",
			 * Status.FAIL); }
			 *
			 * //*******Validate BMI********\\
			 *
			 * WebElement BMI=driver.findElement(By.id("csGBMI")); BMI.click();
			 * String ActualBMI=BMI.getText();
			 *
			 * if(ActualBMI.equalsIgnoreCase(ExpBMI)){
			 * report.updateTestLog("BMI",
			 * ActualBMI+" is displayed as expected", Status.PASS); } else{
			 * report.updateTestLog("BMI", "is not displayed as expected",
			 * Status.FAIL); }
			 *
			 *
			 *
			 */

			// ****Validate Gender in member details sub section******\\

			WebElement Mgender = driver.findElement(By.id("csGGender"));
			Mgender.click();
			String ActualMgender = Mgender.getText();

			if (ActualMgender.equalsIgnoreCase(ExpGender)) {
				report.updateTestLog("Gender", ActualMgender + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Gender", "is not displayed as expected", Status.FAIL);
			}

			// *****Validate Member ID*******\\

			/*WebElement MemberID = driver.findElement(By.id("csGExternalMemberID"));
			MemberID.click();
			String ActualMemberID = MemberID.getText();

			if (ActualMemberID.equalsIgnoreCase(ExpMemberID)) {
				report.updateTestLog("MemberID", ActualMemberID + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("MemberID", "is not displayed as expected", Status.FAIL);
			}*/

			// ****Validate Fund joined date******\\

			WebElement JoinedDate = driver.findElement(By.id("csGFundJoinDate"));
			JoinedDate.click();
			String ActualJoinDate = JoinedDate.getText();

			if (ActualJoinDate.equalsIgnoreCase(ExpJoinDate)) {
				report.updateTestLog("Fund Joined Date", ActualJoinDate + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Fund Joined Date", "is not displayed as expected", Status.FAIL);
			}

			// ****Validate Smoker flag****\\

			WebElement Smoker = driver.findElement(By.id("csGSmokeFlag"));
			Smoker.click();
			String ActualSmoker = Smoker.getText();
		/*	System.out.println("smoke--->" +ActualSmoker);
			System.out.println("Expsmoke--->" +ExpSmoker);*/
			if (ActualSmoker.equalsIgnoreCase(ExpSmoker)) {
				report.updateTestLog("Smoker", ActualSmoker + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Smoker", "is not displayed as expected", Status.FAIL);
			}

			// ****Validate existing work scale******\

			WebElement Existingworkscale = driver.findElement(By.id("csGExistingScaleID"));
			Existingworkscale.click();
			String ActualExisWorkScale = Existingworkscale.getText();

			if (ActualExisWorkScale.equalsIgnoreCase(ExpExisWorkScale)) {
				report.updateTestLog("Exiting Work Scale", ActualExisWorkScale + " is displayed as expected",
						Status.PASS);
			} else {
				report.updateTestLog("Exiting Work Scale", "is not displayed as expected", Status.FAIL);
			}

			// ****Validate New Work scale*****\\

			WebElement NewWorkScale = driver.findElement(By.id("csGScaleSeq"));
			NewWorkScale.click();
			String ActualNewWorkScale = NewWorkScale.getText();

			if (ActualNewWorkScale.equalsIgnoreCase(ExpNewWorkScale)) {
				report.updateTestLog("New Work Scale", ActualNewWorkScale + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("New Work Scale", "is not displayed as expected", Status.FAIL);
			}

			// ****Validate premium frequency*****\\

			WebElement PremiumFreq = driver.findElement(By.id("csGPremiumFrequencyID"));
			PremiumFreq.click();
			String ActualPremiumFreq = PremiumFreq.getText();

			if (ActualPremiumFreq.equalsIgnoreCase(ExpPremiumFreq)) {
				report.updateTestLog("Premium Frequency", ActualPremiumFreq + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Premium Frequency", "is not displayed as expected", Status.FAIL);
			}

			// ***************************************************************************************\\
			// ***************COVER DETAILS****************************************************\\
			// *******************************************************************************\\


			// *****Validate the Fund Name*****

			WebElement FundName = driver.findElement(By.id("csFundID"));
			FundName.click();
			String ActualFundName = FundName.getText();

			if (ActualFundName.equalsIgnoreCase(ExpFundName)) {
				report.updateTestLog("Fund Name", ActualFundName + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Fund Name", "is not displayed as expected", Status.FAIL);
			}

			// *****Death Cover****\\

			WebElement Death = driver.findElement(By.id("csDTHCheck"));
			Death.click();
			if (Death.isSelected() && DeathCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("Death Cover", "Death Cover is displayed as expected", Status.PASS);
			}

			else if (!Death.isSelected() && DeathCover.equalsIgnoreCase("No")) {
				report.updateTestLog("Death Cover", "Death Cover is not available for this application as expected",
						Status.PASS);
			}

			else if (Death.isSelected() && DeathCover.equalsIgnoreCase("No")) {
				report.updateTestLog("Death Cover", "Insured doesn't have a Death Cover yet it is displayed",
						Status.FAIL);
			}

			else if (!Death.isSelected() && DeathCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("Death Cover", "Insured has a Death Cover yet it is not displayed", Status.FAIL);
			}

			// *****Trauma Cover*****\\

			WebElement Trauma = driver.findElement(By.id("csTRACheck"));
			Trauma.click();
			if (Trauma.isSelected() && TraumaCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("Trauma Cover", "Trauma Cover is displayed as expected", Status.PASS);
			}

			else if (!Trauma.isSelected() && TraumaCover.equalsIgnoreCase("No")) {
				report.updateTestLog("Trauma Cover", "Trauma Cover is not available for this application as expected",
						Status.PASS);
			}

			else if (Trauma.isSelected() && TraumaCover.equalsIgnoreCase("No")) {
				report.updateTestLog("Trauma Cover", "Insured doesn't have a Trauma Cover yet it is displayed",
						Status.FAIL);
			}

			else if (!Trauma.isSelected() && TraumaCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("Trauma Cover", "Insured has a Trauma Cover yet it is not displayed", Status.FAIL);
			}

			// ******TPD Cover************\\

			WebElement TPD = driver.findElement(By.id("csTPDCheck"));
			TPD.click();

			if (TPD.isSelected() && TPDCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("TPD Cover", "TPD Cover is displayed as expected", Status.PASS);
			}

			else if (!TPD.isSelected() && TPDCover.equalsIgnoreCase("No")) {
				report.updateTestLog("TPD Cover", "TPD Cover is not available for this application as expected",
						Status.PASS);
			}

			else if (TPD.isSelected() && TPDCover.equalsIgnoreCase("No")) {
				report.updateTestLog("TPD Cover", "Insured doesn't have a TPD Cover yet it is displayed", Status.FAIL);
			}

			else if (!TPD.isSelected() && TPDCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("TPD Cover", "Insured has a TPD Cover yet it is not displayed", Status.FAIL);
			}

			// ******IP Cover**********\\

			WebElement IP = driver.findElement(By.id("csIPCheck"));
			IP.click();

			if (IP.isSelected() && IPCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("IP Cover", "IP Cover is displayed as expected", Status.PASS);
			}

			else if (!IP.isSelected() && IPCover.equalsIgnoreCase("No")) {
				report.updateTestLog("IP Cover", "IP Cover is not available for this application as expected",
						Status.PASS);
			}

			else if (IP.isSelected() && IPCover.equalsIgnoreCase("No")) {
				report.updateTestLog("IP Cover", "Insured doesn't have an IP Cover yet it is displayed", Status.FAIL);
			}

			else if (!IP.isSelected() && IPCover.equalsIgnoreCase("Yes")) {
				report.updateTestLog("IP Cover", "Insured has an IP Cover yet it is not displayed", Status.FAIL);
			}
			sleep(250);

			// *********************************************************************\\
			// ********************COVER Amount Details******************************\\
			// *********************************************************************\\

			// ****Validate the existing Death Cover Amount******\\
			if (ValidateDeathCover.equalsIgnoreCase("Yes")) {

				WebElement ActualDeathCovAmt = driver.findElement(By.id("csDTHExistingAmount"));
				ActualDeathCovAmt.click();
				String ActualDeathCoverAmt = ActualDeathCovAmt.getText();
			/*	System.out.println("ActualDeathCoverAmt==>" +ActualDeathCoverAmt);
				System.out.println("DeathExisitngcover_validation==>" +DeathExisitngcover_validation);*/
				if (ActualDeathCoverAmt.equalsIgnoreCase(DeathExisitngcover_validation)) {
					report.updateTestLog("Existing Death Cover", ActualDeathCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Existing Death Cover", " is not displayed as expected", Status.FAIL);
				}

				// *****Validate the additional death cover amount******\\ --DeathNewcover_validation

				WebElement ActualDeathAddCovAmt = driver.findElement(By.id("csDTHAdditionalAmount"));
				ActualDeathAddCovAmt.click();
				String ActualDeathAddCoverAmt = ActualDeathAddCovAmt.getText();
				System.out.println("ActualDeathAddCoverAmt==>"+ActualDeathAddCoverAmt);
				System.out.println("DeathNewcover_validation==>"+ExpDeathAddCoverAmt);
				if (ActualDeathAddCoverAmt.equalsIgnoreCase(ExpDeathAddCoverAmt)) {
					report.updateTestLog("Additional Death Cover", ActualDeathAddCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Additional Death Cover", " is not displayed as expected", Status.FAIL);
				}

				// *****Validate the total death cover amount******\\

				WebElement ActualDeathTotalCovAmt = driver.findElement(By.id("csDTHTotalAmount"));
				ActualDeathTotalCovAmt.click();
				String ActualDeathTotalCoverAmt = ActualDeathTotalCovAmt.getText();

				if (ActualDeathTotalCoverAmt.equalsIgnoreCase(DeathNewcover_validation)) {
					report.updateTestLog("Total Death Cover", ActualDeathTotalCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Total Death Cover", " is not displayed as expected", Status.FAIL);
				}

				if (CoverType.equalsIgnoreCase("Unitised")) {

					// ****Validate the existing Death Cover Units******\\

					WebElement ActualDeathCovUnits = driver.findElement(By.id("csDTHExistingUnit"));
					ActualDeathCovUnits.click();
					String ActualDeathCoverUnits = ActualDeathCovUnits.getText();

					if (ActualDeathCoverUnits.equalsIgnoreCase(ExpDeathCoverUnits)) {
						report.updateTestLog("Existing Death units",
								ActualDeathCoverUnits + " is displayed as expected", Status.PASS);
					} else {
						report.updateTestLog("Existing Death units", " is not displayed as expected", Status.FAIL);
					}

					// ****Validate the additional death cover units*****\\

					WebElement ActualDeathAddCovUnits = driver.findElement(By.id("csDTHAdditionalUnit"));
					ActualDeathAddCovUnits.click();
					String ActualDeathAddCoverUnits = ActualDeathAddCovUnits.getText();

					if (ActualDeathAddCoverUnits.equalsIgnoreCase(ExpDeathAddCoverUnits)) {
						report.updateTestLog("Additional Death units",
								ActualDeathAddCoverUnits + " is displayed as expected", Status.PASS);
					} else {
						report.updateTestLog("Additional Death units", " is not displayed as expected", Status.FAIL);
					}

					// *****Validate the Total Death Cover units*****\\

					WebElement ActualDeathTotalCovUnits = driver.findElement(By.id("csDTHTotalUnit"));
					ActualDeathTotalCovUnits.click();
					String ActualDeathTotalCoverUnits = ActualDeathTotalCovUnits.getText();

					if (ActualDeathTotalCoverUnits.equalsIgnoreCase(ExpDeathTotalCoverUnits)) {
						report.updateTestLog("Total Death units",
								ActualDeathTotalCoverUnits + " is displayed as expected", Status.PASS);
					} else {
						report.updateTestLog("Total Death units", " is not displayed as expected", Status.FAIL);
					}
				}

				// ******Validate Death Premium Amount*****\\
				sleep(200);
				WebElement ActualDeathPrem = driver.findElement(By.id("csDTHTotalPremium"));
				ActualDeathPrem.click();
				String ActualDeathPremium = ActualDeathPrem.getText().trim().trim();;
					//
			/*	System.out.println("ActualDeathPremium-->" +ActualDeathPremium);
				System.out.println("DeathWeeklycost_validation-->" +DeathWeeklycost_validation);*/
				if (ActualDeathPremium.equalsIgnoreCase(DeathWeeklycost_validation)) {
					report.updateTestLog("Death Cover Premium", ActualDeathPremium + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Death Cover premium", " is not displayed as expected", Status.FAIL);
				}

			}




			if (ValidateTPDCover.equalsIgnoreCase("Yes")) {

				// ****Validate the existing TPD Cover Amount******\\

				WebElement ActualTPDCovAmt = driver.findElement(By.id("csTPDExistingAmount"));
				ActualTPDCovAmt.click();
				String ActualTPDCoverAmt = ActualTPDCovAmt.getText();
			/*	System.out.println("ActualTPDCoverAmt-->" +ActualTPDCoverAmt);
				System.out.println("TPDExisitngcover_validation-->" +TPDExisitngcover_validation);*/
				if (ActualTPDCoverAmt.equalsIgnoreCase(TPDExisitngcover_validation)) {
					report.updateTestLog("Existing TPD Cover", ActualTPDCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Existing TPD Cover", " is not displayed as expected", Status.FAIL);
				}

				// *****Validate the additional TPD cover amount******\\

				WebElement ActualTPDAddCovAmt = driver.findElement(By.id("csTPDAdditionalAmount"));
				ActualTPDAddCovAmt.click();
				String ActualTPDAddCoverAmt = ActualTPDAddCovAmt.getText();
				/*System.out.println("ActualTPDAddCoverAmt-->" +ActualTPDAddCoverAmt);
				System.out.println("TPDExisitngcover_validation-->" +TPDExisitngcover_validation);*/
				if (ActualTPDAddCoverAmt.equalsIgnoreCase(ExpTPDAddCoverAmt)) {
					report.updateTestLog("Additional TPD Cover", ActualTPDAddCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Additional TPD Cover", " is not displayed as expected", Status.FAIL);
				}

				// *****Validate the total TPD cover amount******\\

				WebElement ActualTPDTotalCovAmt = driver.findElement(By.id("csTPDTotalAmount"));
				ActualTPDTotalCovAmt.click();
				String ActualTPDTotalCoverAmt = ActualTPDTotalCovAmt.getText();

				if (ActualTPDTotalCoverAmt.equalsIgnoreCase(ExpTPDTotalCoverAmt)) {
					report.updateTestLog("Total TPD Cover", ActualTPDTotalCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Total TPD Cover", " is not displayed as expected", Status.FAIL);
				}

				if (CoverType.equalsIgnoreCase("Unitised")) {

					// ****Validate the existing TPD Cover Units******\\

					WebElement ActualTPDCovUnits = driver.findElement(By.id("csTPDExistingUnit"));
					ActualTPDCovUnits.click();
					String ActualTPDCoverUnits = ActualTPDCovUnits.getText();

					if (ActualTPDCoverUnits.equalsIgnoreCase(ExpTPDCoverUnits)) {
						report.updateTestLog("Existing TPD units", ActualTPDCoverUnits + " is displayed as expected",
								Status.PASS);
					} else {
						report.updateTestLog("Existing TPD units", " is not displayed as expected", Status.FAIL);
					}

					// ****Validate the additional TPD cover units*****\\

					WebElement ActualTPDAddCovUnits = driver.findElement(By.id("csTPDAdditionalUnit"));
					ActualTPDAddCovUnits.click();
					String ActualTPDAddCoverUnits = ActualTPDAddCovUnits.getText();

					if (ActualTPDAddCoverUnits.equalsIgnoreCase(ExpTPDAddCoverUnits)) {
						report.updateTestLog("Additional TPD units",
								ActualTPDAddCoverUnits + " is displayed as expected", Status.PASS);
					} else {
						report.updateTestLog("Additional TPD units", " is not displayed as expected", Status.FAIL);
					}

					// *****Validate the Total TPD Cover units*****\\

					WebElement ActualTPDTotalCovUnits = driver.findElement(By.id("csTPDTotalUnit"));
					ActualTPDTotalCovUnits.click();
					String ActualTPDTotalCoverUnits = ActualTPDTotalCovUnits.getText();

					if (ActualTPDTotalCoverUnits.equalsIgnoreCase(ExpTPDTotalCoverUnits)) {
						report.updateTestLog("Total TPD units", ActualTPDTotalCoverUnits + " is displayed as expected",
								Status.PASS);
					} else {
						report.updateTestLog("Total TPD units", " is not displayed as expected", Status.FAIL);
					}
				}

				// ******Validate TPD Premium Amount*****\\

				WebElement ActualTPDPrem = driver.findElement(By.id("csTPDTotalPremium"));
				ActualTPDPrem.click();
				String ActualTPDPremium = ActualTPDPrem.getText();

				if (ActualTPDPremium.equalsIgnoreCase(TPDWeeklycost_validation)) {
					report.updateTestLog("TPD Cover Premium", ActualTPDPremium + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("TPD Cover premium", " is not displayed as expected", Status.FAIL);
				}
			}

			// *****Validate existing IP cover amount******\\

			WebElement ActualIPCovAmt = driver.findElement(By.id("csIPExistingAmount"));
			ActualIPCovAmt.click();
			String ActualIPCoverAmt = ActualIPCovAmt.getText();
			/*System.out.println("ActualIPCoverAmt-->" +ActualIPCoverAmt);
			System.out.println("IPExisitngcover_validation-->" +IPExisitngcover_validation);*/
			if (ActualIPCoverAmt.equalsIgnoreCase(ExpIPCoverAmt)) {
				report.updateTestLog("Existing IP Cover", ActualIPCoverAmt + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("Existing IP Cover", " is not displayed as expected", Status.FAIL);
			}

			if (ValidateIPCover.equalsIgnoreCase("Yes")) {
				// ****Validate additional IP cover amount*****\\

				WebElement ActualIPAddCovAmt = driver.findElement(By.id("csIPAdditionalAmount"));
				ActualIPAddCovAmt.click();
				String ActualIPAddCoverAmt = ActualIPAddCovAmt.getText().trim();
	/*			System.out.println("ActualIPAddCoverAmt-->" +ActualIPAddCoverAmt);
				System.out.println("ActualIPAddCoverAmt-->" +ActualIPAddCoverAmt);*/

				if (ActualIPAddCoverAmt.equalsIgnoreCase(ExpIPAddCoverAmt)) {
					report.updateTestLog("Additional IP Cover", ActualIPAddCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Additional IP Cover", " is not displayed as expected", Status.FAIL);
				}

				// ****Validate total IP cover amount*****\\

				WebElement ActualIPTotalCovAmt = driver.findElement(By.id("csIPTotalAmount"));
				ActualIPTotalCovAmt.click();
				String ActualIPTotalCoverAmt = ActualIPTotalCovAmt.getText();

				if (ActualIPTotalCoverAmt.equalsIgnoreCase(ExpIPTotalCoverAmt)) {
					report.updateTestLog("Total IP Cover", ActualIPTotalCoverAmt + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("Total IP Cover", " is not displayed as expected", Status.FAIL);
				}

				// ****Validate IP Premium amount*****\\

				WebElement ActualIPPrem = driver.findElement(By.id("csIPTotalPremium"));
				ActualIPPrem.click();
				String ActualIPPremium = ActualIPPrem.getText();
			/*	System.out.println("ActualIPPremium-->" +ActualIPPremium);
				System.out.println("ActualIPPremium-->" +ActualIPPremium);*/
				if (ActualIPPremium.equalsIgnoreCase(ExpIPPremium)) {
					report.updateTestLog("IP Cover Premium", ActualIPPremium + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("IP Cover Premium", " is not displayed as expected", Status.FAIL);
				}

				// ******Validate Waiting Period*****\\

				WebElement ActualWaitPeriod = driver.findElement(By.id("csIPAdditionalWaitPeriod"));
				ActualWaitPeriod.click();
				String ActualWaitingPeriod = ActualWaitPeriod.getText().trim();

				if (ActualWaitingPeriod.equalsIgnoreCase(Waitingperiod_validation)) {
					report.updateTestLog("IP Waiting Period", ActualWaitingPeriod + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("IP Waiting Period", " is not displayed as expected", Status.FAIL);
				}

				// *****Validate Benefit Period*****\\

				WebElement ActualBenePeriod = driver.findElement(By.id("csIPAdditionalBenefitPeriod"));
				ActualBenePeriod.click();
				String ActualBenefitPeriod = ActualBenePeriod.getText().trim();

				if (ActualBenefitPeriod.equalsIgnoreCase(Benefitperiod_validation)) {
					report.updateTestLog("IP Benefit Period", ActualBenefitPeriod + " is displayed as expected",
							Status.PASS);
				} else {
					report.updateTestLog("IP Benefit Period", " is not displayed as expected", Status.FAIL);
				}

			}

			// *******************************************************************************\\
			// *******************************Decisions****************************************\\
			// *******************************************************************************\\

			WebElement EApplyStatus = driver.findElement(By.id("csEApplyStatus"));
			EApplyStatus.click();
			String ActualEapplyStatus = EApplyStatus.getText();
			report.updateTestLog("EApply Status", ActualEapplyStatus + " is displayed", Status.PASS);

			WebElement DeathDc = driver.findElement(By.id("insured0csCoverDecisionDTHSelect"));
			DeathDc.click();
			String ActualDeathDc = DeathDc.getText();
			report.updateTestLog("Death Cover Decision", ActualDeathDc + " is displayed", Status.PASS);

			WebElement DeathValue = driver.findElement(By.id("insured0csCoverDecisionDTHAmount"));
			DeathValue.click();
			String ActualDeathValue = DeathValue.getText();
			if (ExpDeathTotalCoverAmt.equalsIgnoreCase(ActualDeathValue)) {
				report.updateTestLog("Total Death Cover", ExpDeathTotalCoverAmt + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("Total Death Cover", " is not displayed as Expected", Status.FAIL);
			}

			WebElement TPDDc = driver.findElement(By.id("insured0csCoverDecisionTPDSelect"));
			TPDDc.click();
			String ActualTPDDc = TPDDc.getText();
			report.updateTestLog("TPD Cover Decision", ActualTPDDc + " is displayed", Status.PASS);

			WebElement TPDValue = driver.findElement(By.id("insured0csCoverDecisionTPDAmount"));
			TPDValue.click();
			String ActualTPDValue = TPDValue.getText();

			if (ExpTPDTotalCoverAmt.equalsIgnoreCase(ActualTPDValue)) {
				report.updateTestLog("Total TPD Cover", ExpTPDTotalCoverAmt + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Total TPD Cover", " is not displayed as Expected", Status.FAIL);
			}

			WebElement IPDc = driver.findElement(By.id("insured0csCoverDecisionIPSelect"));
			IPDc.click();
			String ActualIPDc = IPDc.getText();
			report.updateTestLog("IP Cover Decision", ActualIPDc + " is displayed", Status.PASS);

			WebElement IPValue = driver.findElement(By.id("insured0csCoverDecisionIPAmount"));
			IPValue.click();
			String ActualIPValue = IPValue.getText();
			if (ExpIPTotalCoverAmt.equalsIgnoreCase(ActualIPValue)) {
				report.updateTestLog("Total IP Cover", ExpIPTotalCoverAmt + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Total IP Cover", " is not displayed as Expected", Status.FAIL);
			}

			WebElement IPWaitPeriod = driver.findElement(By.id("insured0csCoverDecisionIPWaitPeriod"));
			IPWaitPeriod.click();
			String AtcualIPWaitingPeriod = IPWaitPeriod.getText();

			if (ExpWaitingPeriod.equalsIgnoreCase(AtcualIPWaitingPeriod)) {
				report.updateTestLog("IP Waiting Period", ExpWaitingPeriod + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("IP Waiting Period", " is not displayed as expected", Status.FAIL);
			}

			WebElement IPBenefitPeriod = driver.findElement(By.id("insured0csCoverDecisionIPBenefitPeriod"));
			IPBenefitPeriod.click();
			String ActualIPBenefitPeriod = IPBenefitPeriod.getText();

			if (ExpBenefitPeriod.equalsIgnoreCase(ActualIPBenefitPeriod)) {
				report.updateTestLog("IP Benefit Period", ExpBenefitPeriod + " is displayed as expected", Status.PASS);
			} else {
				report.updateTestLog("IP Benefit Period", " is not displayed as expected", Status.FAIL);
			}

			// **************************************************************************\\
			// *********************DOCUMENT DETAILS**********************************\\
			// *************************************************************************\\
			// **************************************************************************\\
			driver.findElement(By.id("csDocumentDetails_label")).click();

			List<WebElement> Documents = driver.findElements(By.xpath("//tr[contains(@id, 'csDocumentRepeat')]"));

			int DocRowsSize = Documents.size();

			for (int i = 1; i <= DocRowsSize; i++) {

				String DocName = driver
						.findElement(By.xpath("(//tr[contains(@id, 'csDocumentRepeat')]/td[2]/span)[" + i + "]"))
						.getText();
				String FormType = driver
						.findElement(By.xpath("(//tr[contains(@id, 'csDocumentRepeat')]/td[4]/span)[" + i + "]"))
						.getText();
				driver.findElement(By.xpath("(//tr[contains(@id, 'csDocumentRepeat')]/td[1]/span)[" + i + "]")).click();

				if (DocName.contains(".pdf") && FormType.equalsIgnoreCase("Application Form")) {
					report.updateTestLog("PDF File", "PDF of the application form is attached", Status.PASS);

				}

				if (DocName.contains(SupportDocName)) {
					report.updateTestLog("Supporting Document", DocName + " is the supporting document attached",
							Status.PASS);

				}

			}

					}
				}

		catch (Exception e) {
			report.updateTestLog("Exception", "Exception handled", Status.SCREENSHOT);
		}
	}

}






