/**
 * 
 */
package au.com.metlife.eapply.bs.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;


/**
 * @author akishore
 *
 */
@Transactional
public interface TblOccMappingRuledataDao extends CrudRepository<TblOccMappingRuledata, String> {
	
	//public TblOccMappingRuledata findById(String fundCode,String induCode);	
	
	public List<TblOccMappingRuledata> findByFundCode(String fundCode);	
	
}
