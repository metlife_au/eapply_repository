/* Work Rating Controller,Progressive and Mandatory validations Starts  */  
CareSuperApp.controller('quoteoccupdate',['$scope','$routeParams', '$location','$timeout','$window','QuoteService','OccupationService','NewOccupationService','auraInputService','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','ngDialog','auraResponseService','urlService',
                                          function($scope,$routeParams, $location,$timeout,$window,QuoteService,OccupationService,NewOccupationService,auraInputService,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService ,ngDialog,auraResponseService,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.validnumberregEx = '^0[1-8][0-9]{8}';
    //$scope.validAnnualSalaryregEx = '/^[0-9]*$/';
    $scope.phoneNumbrUpdate = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
    $scope.emailFormatUpdate = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{2,}$/;
    QuoteService.getList($scope.urlList.quoteUrl,"METL").then(function(res){
    	$scope.IndustryOptions = res.data;
    }, function(err){
    	console.log("Error while fetching industry options " + JSON.stringify(err));
    });
    
    $scope.getOccupations = function(){
    	if(!$scope.workRatingIndustry){
    		$scope.workRatingIndustry = '';
    	}
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"CARE",$scope.workRatingIndustry).then(function(res){
    		$scope.OccupationList = res.data;
    	}, function(err){
    		console.log("Error while fetching occupation options " + JSON.stringify(err));
    	});
    };
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);    
  	  $location.path( path );
  	};
  	
 // added for session expiry
   /* $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  	/*var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/
    
	$scope.coltwo = false;
  	$scope.toggleTwo = function() {
        $scope.coltwo = !$scope.coltwo; 
        $("a[data-target='#collapseTwo']").click();
    };
    
    $scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
    
    
    $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
    $scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
	$scope.ipCoverTransDetails = ipCoverService.getIpCover();
	var fetchAppnum = true;
	var appNum;
	var inputDetails = persoanlDetailService.getMemberDetails();
	$scope.personalDetails = inputDetails[0].personalDetails;
	$scope.otherOccupationObj = {'workRatingotherOccupation': ''};
	
	if(inputDetails[0] && inputDetails[0].contactDetails.emailAddress){
		$scope.email = inputDetails[0].contactDetails.emailAddress;
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContactTime){
		if(inputDetails[0].contactDetails.prefContactTime == "1"){
			$scope.time= "Morning (9am - 12pm)";
		}else{
			$scope.time= "Afternoon (12pm - 6pm)";
		}
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContact){
		if(inputDetails[0].contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.updatePhone = inputDetails[0].contactDetails.mobilePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home";
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.updatePhone = inputDetails[0].contactDetails.homePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work";
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.updatePhone = inputDetails[0].contactDetails.workPhone;
		}
   }
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home"){
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.updatePhone = inputDetails[0].contactDetails.homePhone;
		}else if($scope.preferredContactType == "Work"){
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.updatePhone = inputDetails[0].contactDetails.workPhone;
		}else if($scope.preferredContactType == "Mobile"){
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.updatePhone = inputDetails[0].contactDetails.mobilePhone;
		}
	}
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    
   //Progressive validation
    var workRatingFormFields = ['workRatingEmail', 'wrkRatingPrefContactNo','workRatingPrefTime'];
    
    while($scope.personalDetails){
		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.genderFlag =  false;
			$scope.gender = '';
			 var workRatingOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','gender','workRatingAnnualSal'];
		     var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','gender','workRatingAnnualSal'];
			    
		} else{
			$scope.genderFlag =  true;
			$scope.gender = $scope.personalDetails.gender;
			 var workRatingOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
		     var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
			    
		}
		break;
	}
    
   /* var workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
    var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];*/
      $scope.checkPreviousMandatoryFieldsFrWorkRating  = function (workRatingElementName,workRatingFormName){
      	var WorkRatingformFields;
      	if(workRatingFormName == 'workRatingContactForm'){
      		WorkRatingformFields = workRatingFormFields;
    	}else if(workRatingFormName == 'workRatingOccupForm'){
     		//WorkRatingformFields = workRatingOccupationFormFields;
    		if($scope.workRatingoccupation != undefined && $scope.workRatingoccupation == 'Other'){
    			WorkRatingformFields = workRatingOthrOccupationFormFields;
    		} else{
    			WorkRatingformFields = workRatingOccupationFormFields;
    		}
    	}
        var inx = WorkRatingformFields.indexOf(workRatingElementName);
        //console.log(workRatingElementName, inx);
        if(inx > 0){
          for(var i = 0; i < inx ; i++){
            $scope[workRatingFormName][WorkRatingformFields[i]].$touched = true;
          }
        }
      };
      
      $scope.updateAckFlag = false;
 	 var updateAckCheck;
 	  
 	  $scope.checkUpdateAckState = function(){
 	    	$timeout(function(){
 	    		updateAckCheck = $('#ackLabel').hasClass('active');
 	    		if(updateAckCheck){
 	    			$scope.updateAckFlag = false;
 	    		} else{
 	    			$scope.updateAckFlag = true;
 	    		}
 	    	}, 10);
 	    };
   // Validate fields "on continue"
 	   appNum = PersistenceService.getAppNumber();
     $scope.workRatingFormSubmit =  function (form){
        if(!form.$valid){
      	//  alert("invalid>>"+$scope["workRatingContactForm"].$invalid);
      	  form.$submitted=true; 
  	    }else{
  		  if(form.$name == 'workRatingContactForm'){
      	    $scope.toggleTwo();
  		  }else if(form.$name == 'workRatingOccupForm'){
  			if(fetchAppnum){
    			fetchAppnum = false;
    			appNum = PersistenceService.getAppNumber();
    		}
  			    $scope.goToAura();  
  		     //	$scope.continueWorkRatingPage();
   		  }
       }
             // console.log("Form Validation");
     };
     
     $scope.invalidSalAmount = false;
	    $scope.checkValidSalary = function(){
	    	if(parseInt($scope.workRatingAnnualSal) == 0){
	  			$scope.invalidSalAmount = true;
	  		} else{
	  			$scope.invalidSalAmount = false;
	  		}
	    };
	 
     $scope.auraDisabled = false;
     $scope.continueWorkRatingPage = function(){
       	 var occupationName = $scope.workRatingIndustry + ":" + $scope.workRatingoccupation;
       	NewOccupationService.getOccupation($scope.urlList.newOccupationUrl,"METL",occupationName).then(function(result){   
       	 //NewOccupationService.getOccupation({fundId:"CARE", occName:occupationName},function(result){
	     			var occupationCategory = result.data;
	     			$scope.deathOccupationCategory = occupationCategory[0].deathfixedcategeory;
	     			$scope.deathOccupationCategory = occupationCategory[0].tpdfixedcategeory;
	     			$scope.deathOccupationCategory = occupationCategory[0].ipfixedcategeory;
	      			var deathUpgrade = false;
	      			var tpdUpgrade = false;
	      			var ipUpgrade = false;
	      			
	     			if($scope.deathOccupationCategory != "Professional" && $scope.workRatingWithinOffcQue == "Yes"){
	     				$scope.deathOccupationCategory = "Office";
	     				deathUpgrade = true;
	      				if(parseInt($scope.workRatingAnnualSal) > 100000 && ($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes")){
	      					$scope.deathOccupationCategory = "Professional";
	      				}
	      			}
	      			if($scope.tpdOccupationCategory != "Professional" && $scope.workRatingWithinOffcQue == "Yes"){
	      				$scope.tpdOccupationCategory = "Office";
	      				tpdUpgrade = true;
	      				if(parseInt($scope.workRatingAnnualSal) > 100000 && ($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes")){
	      					$scope.tpdOccupationCategory = "Professional";
	      				}
	      			}
	      			if($scope.ipOccupationCategory != "Professional" && $scope.workRatingWithinOffcQue == "Yes"){
	      				$scope.ipOccupationCategory = "Office";
	      				ipUpgrade = true;
	      				if(parseInt($scope.workRatingAnnualSal) > 100000 && ($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes")){
	      					$scope.ipOccupationCategory = "Professional";
	      				}
	      			}
	      			
	      			if(deathUpgrade || tpdUpgrade || ipUpgrade){         			
	          			/*$scope.personalDetails = persoanlDetailService.getMemberDetails()[0].personalDetails;
	          	  		auraInputService.setName($scope.personalDetails.firstName+" "+$scope.personalDetails.lastName)
	          	  		auraInputService.setIndustryOcc($scope.workRatingIndustry + ":" + $scope.workRatingoccupation)
	          	  		auraInputService.setWaitingPeriod(ipCoverService.getIpCover().waitingPeriod);
	          	  		auraInputService.setBenefitPeriod(ipCoverService.getIpCover().benefitPeriod);
	          	  		auraInputService.setGender($scope.personalDetails.gender);	  		
	          	  		auraInputService.setAge( moment().diff($scope.personalDetails.dateOfBirth, 'years')) ; 		
	          	  		auraInputService.setCountry(persoanlDetailService.getMemberDetails()[0].address.country);
	          	  		auraInputService.setDeathAmt(Math.floor(deathCoverService.getDeathCover().amount))
	          	  		auraInputService.setTpdAmt(Math.floor(tpdCoverService.getTpdCover().amount))
	          	  		auraInputService.setIpAmt(Math.floor(ipCoverService.getIpCover().amount)) 	
	          	  		auraInputService.setFifteenHr("")
	          	  		auraInputService.setSalary($scope.workRatingAnnualSal)	*/	 
	      				$scope.auraDisabled = false;
	      				$scope.saveDataForPersistence();
	          	  		$location.path('/auraocc/1');
	          		}else{
	          			$scope.auraDisabled = true;
	          			$scope.saveDataForPersistence();
	          			$location.path('/workRatingSummary');
	          		}
        		}, function(e){
        			console.log("Error while getting new occupation " + JSON.stringify(e));
        		});
	};
	
	 
	$scope.saveDataForPersistence = function(){
		$scope.personalDetails = persoanlDetailService.getMemberDetails()[0].personalDetails;
    	var coverObj = {};
    	var occObj={};
    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
    		return $scope.workRatingIndustry == obj.key;
    	});
    	
    	coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
    	coverObj['email'] = $scope.email;
    	occObj['gender'] = $scope.gender;
    	coverObj['contactType']=$scope.preferredContactType;
    	coverObj['contactPhone'] = $scope.updatePhone;
    	coverObj['contactPrefTime'] = $scope.time;
    	
    	coverObj['deathOccCategory'] = $scope.deathOccupationCategory;
    	coverObj['tpdOccCategory'] = $scope.tpdOccupationCategory;
    	coverObj['ipOccCategory'] = $scope.ipOccupationCategory;
    	coverObj['auraDisabled'] = $scope.auraDisabled;
    	
    	occObj['fifteenHr'] = $scope.fifteenHrsQuestion;
    	occObj['industryName'] = selectedIndustry[0].value;
    	occObj['industryCode'] = selectedIndustry[0].key;
    	occObj['occupation'] = $scope.workRatingoccupation;
    	occObj['withinOfficeQue']= $scope.workRatingWithinOffcQue;
    	occObj['tertiaryQue']= $scope.workRatingTertQue;
    	occObj['managementRoleQue']= $scope.workRatingManagementRole;
    	occObj['salary'] = $scope.workRatingAnnualSal;
    	occObj['otherOccupation'] = $scope.otherOccupationObj.workRatingotherOccupation;
    	/*occObj['citizenQue'] = $scope.areyouperCitzWrkUpdateQuestion;*/
    	
    	coverObj['deathAmt'] = $scope.deathCoverTransDetails.amount;
    	coverObj['tpdAmt'] = $scope.tpdCoverTransDetails.amount;
    	coverObj['ipAmt'] = $scope.ipCoverTransDetails.amount;
    	coverObj['waitingPeriod'] = ipCoverService.getIpCover().waitingPeriod;
    	coverObj['benefitPeriod'] = ipCoverService.getIpCover().benefitPeriod;
    	coverObj['appNum'] = appNum;
    	coverObj['ackCheckbox'] = updateAckCheck;
    	coverObj['lastSavedOn'] = 'QuoteUpdatePage';
    	coverObj['age'] = anb;
        coverObj['manageType'] = 'UWCOVER';
        coverObj['partnerCode'] = 'METL';
    	
    	PersistenceService.setworkRatingCoverDetails(coverObj);
    	PersistenceService.setWorkRatingCoverOccDetails(occObj);
    	
	       
       
    };
    
    $scope.goToAura = function (){
    //	$scope.saveDataForPersistence();
    	if(this.workRatingContactForm.$valid && this.workRatingOccupForm.$valid){
    	$timeout(function(){
			updateAckCheck = $('#ackLabel').hasClass('active');
		   if(updateAckCheck){
				$scope.updateAckFlag = false;
		    	$scope.continueWorkRatingPage();
		   }else{
		  	   $scope.updateAckFlag = true;
		   }
  	    }, 10);
    }
    };
    
    /*$scope.saveWorkRating = function (){
    	$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	$scope.saveDataForPersistence();
		$scope.workRatingObject =  PersistenceService.getworkRatingCoverDetails();
		$scope.workRatingOccDetails = PersistenceService.getWorkRatingCoverOccDetails();
	    $scope.personalDetails = persoanlDetailService.getMemberDetails();
	    if($scope.workRatingObject != null && $scope.workRatingOccDetails != null && $scope.personalDetails[0] != null){
	    	$scope.details={};
			$scope.details.workRatingOccDetails=$scope.workRatingOccDetails;
			var temp = angular.extend($scope.details,$scope.workRatingObject)
    	    var saveWorkRatingObject = angular.extend(temp,$scope.personalDetails[0]);
	    	auraResponseService.setResponse(saveWorkRatingObject)
	        saveEapply.reqObj().then(function(response) {  
	                console.log(response.data)
	        });
	    }
    };
    
    $scope.saveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};*/
  $scope.clickToOpen = function (hhText) {
      	
		var dialog = ngDialog.open({
			/*template: '<p>'+hhText+'</p>' +
				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
				template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hint</h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="">Close</button></div></div>',
				className: 'ngdialog-theme-plain',
				plain: true
		});
		dialog.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
	
    if($routeParams.mode == 2){
    	var existingWorkRatingDetails = PersistenceService.getworkRatingCoverDetails();
    	var workRatingOccDetails=PersistenceService.getWorkRatingCoverOccDetails();
    	
    	
    	$scope.email = existingWorkRatingDetails.email;
    	$scope.preferredContactType=existingWorkRatingDetails.contactType;
    	$scope.updatePhone = existingWorkRatingDetails.contactPhone;
    	$scope.time = existingWorkRatingDetails.contactPrefTime;
    	$scope.auraDisabled = existingWorkRatingDetails.auraDisabled;
    	
    	$scope.fifteenHrsQuestion = workRatingOccDetails.fifteenHr;
    	/*$scope.areyouperCitzWrkUpdateQuestion = workRatingOccDetails.citizenQue;*/
    	$scope.workRatingIndustry = workRatingOccDetails.industryCode;
    	//$scope.workRatingoccupation = workRatingOccDetails.occupation;
    	$scope.workRatingWithinOffcQue = workRatingOccDetails.withinOfficeQue;
    	$scope.workRatingTertQue = workRatingOccDetails.tertiaryQue;
    	$scope.workRatingManagementRole = workRatingOccDetails.managementRoleQue;
    	$scope.gender = workRatingOccDetails.gender;
    	$scope.workRatingAnnualSal = workRatingOccDetails.salary;
    	$scope.otherOccupationObj.workRatingotherOccupation = workRatingOccDetails.otherOccupation;
    	
    	appNum = existingWorkRatingDetails.appNum;
    	updateAckCheck = existingWorkRatingDetails.ackCheckbox;
    	
    	if(updateAckCheck){
    	    $timeout(function(){
    			$('#ackLabel').addClass('active');
    			   });
    			 }
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"METL",$scope.workRatingIndustry).then(function(res){
    	//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.workRatingIndustry}, function(occupationList){
        	$scope.OccupationList = res.data;
        	var temp = $scope.OccupationList.filter(function(obj){
        		return obj.occupationName == workRatingOccDetails.occupation;
        	});
        	$scope.workRatingoccupation = temp[0].occupationName;
        }, function(err){
        	console.log("Error while fetching occupation options " + JSON.stringify(err));
        });
    	
    	$scope.toggleTwo();
    }
    
}]); 

/* Work Rating Controller,Progressive and Mandatory validations Ends  */
CareSuperApp.directive('phoneOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
            
            $(element).on("keyup", function() {
            	var TempVal=$(this).val().replace(" ","");
             	var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            	if( !regex.test(TempVal)) {
            		// element.controller('ngModel').$setValidity('required', false);
            		// element.controller('ngModel').$touched = true;
            	}
            	
            })
        }
    };
});
  