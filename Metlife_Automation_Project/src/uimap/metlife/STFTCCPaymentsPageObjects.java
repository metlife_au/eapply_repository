package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTCCPaymentsPageObjects extends PageObjectLibrary{
		
	public static final By rddebit = getObjectIdentifaction("stft.cc.payment.debit.radio.css");
	//public static final By rdcredit = getObjectIdentifaction("stft.payment.credit.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("stft.cc.payment.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("stft.cc.payment.accountnumber.text.id");
	public static final By txtbsb = getObjectIdentifaction("stft.cc.payment.bsb.text.id");	
	public static final By chkacknowledge = getObjectIdentifaction("stft.cc.payment.authorise.checkbox.id");
	public static final By btnsubmit = getObjectIdentifaction("stft.cc.payment.next.button.css");
}
