package pages.metlife.Angular.Statewide;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class STATEWIDE_SaveAndExitPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public STATEWIDE_SaveAndExitPage SaveandExit_STATEWIDE() {
		SaveandExit();		
		
		
		return new STATEWIDE_SaveAndExitPage(scriptHelper);
	}

	public STATEWIDE_SaveAndExitPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void SaveandExit() {

		String EmailId = dataTable.getData("STATEWIDE_SaveAndExit", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_SaveAndExit", "TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_SaveAndExit", "ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_SaveAndExit", "TimeofContact");
		String Gender = dataTable.getData("STATEWIDE_SaveAndExit", "Gender");
		String Smokerquestion = dataTable.getData("STATEWIDE_SaveAndExit", "Smoker");
		String FifteenHoursWork = dataTable.getData("STATEWIDE_SaveAndExit", "FifteenHoursWork");
//		String RegularIncome = dataTable.getData("STATEWIDE_SaveAndExit", "RegularIncome");
//		String Perform_WithoutRestriction = dataTable.getData("STATEWIDE_SaveAndExit", "Perform_WithoutRestriction");
//		String WorkLimitation = dataTable.getData("STATEWIDE_SaveAndExit", "WorkLimitation");
		String Citizen = dataTable.getData("STATEWIDE_SaveAndExit", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_SaveAndExit", "IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_SaveAndExit", "OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_SaveAndExit", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_SaveAndExit", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("STATEWIDE_SaveAndExit", "TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_SaveAndExit", "HazardousEnv");		
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_SaveAndExit", "OutsideOfcPercent");		
		String AnnualSalary = dataTable.getData("STATEWIDE_SaveAndExit", "AnnualSalary");
		String WorkLimitation = dataTable.getData("STATEWIDE_SaveAndExit", "WorkLimitation");
		String Permanentlyempyd = dataTable.getData("STATEWIDE_SaveAndExit", "Permanentlyempyd");	
		String CostType=dataTable.getData("STATEWIDE_SaveAndExit", "CostType");			
	//	String WorkLimitation=dataTable.getData("STATEWIDE_SaveAndExit", "WorkLimitation");	
		try{
			
		
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,450)", "");
			
			Thread.sleep(2000);
			//****Click on Change Your Insurance Cover Link*****\\
			//h4[contains(text(),'Change your insurance cover')]
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Change your insurance']"))).click();
			
			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);
		
			Thread.sleep(2000);
			
			
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);
			
			//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);
							
			
		//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
			
			//***Select the Preferred time of Contact*****\\			
		
			if(TimeofContact.equalsIgnoreCase("Morning")){			
						
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}		
			
			
			//***********Select the Gender******************\\	
			
			if(Gender.equalsIgnoreCase("Male")){			
						
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}	
						
			//****Contact Details-Continue Button*****\\			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);			
						
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
				
			
			
			//*****Select the Smoker Question*******\\		
			
			if(Smokerquestion.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[1]/div[2]/div/div[1]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);							
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[1]/div[2]/div/div[2]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[2]/div[2]/div/div[1]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);							
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[2]/div[2]/div/div[2]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			
			//*****Select the industry type*******\\	
			
			Select Industry=new Select(driver.findElement(By.xpath("//select[@ng-model='industry']")));		
			Industry.selectByVisibleText(IndustryType);			
			report.updateTestLog("Industry Type", "Preferred Industry Type: "+IndustryType+" is Selected", Status.PASS);
			sleep(250);
				
			//*****Select the occupation type*******\\	
			
			Select Occupation=new Select(driver.findElement(By.xpath("//select[@ng-model='occupation']")));		
			Occupation.selectByVisibleText(OccupationType);			
			report.updateTestLog("Occupation Type", "Preferred Industry Type: "+OccupationType+" is Selected", Status.PASS);
			sleep(250);	
		
			//*****If other occupation type*******\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
				
				wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}								
		    
			

			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Do you work wholly within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[5]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[5]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[6]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[6]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}							
				
		    
		      //*****What is your annual Salary******							
		    	
		    	
		        wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary);
		        sleep(500);		
		        report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);	
		    
		    //yuvi
		    
			if(WorkLimitation.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[1]/label/span"))).click();
				sleep(250);
				report.updateTestLog("35 hours questions", "Selected Yes", Status.PASS);
					
				}	
			else{	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[2]/label/span"))).click();
				sleep(250);
				report.updateTestLog("35 hours questions", "Selected No", Status.PASS);
					
				}
		  
		  
		  
		  //*****Permanently employed***\\	
			
			if(WorkLimitation.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[1]/label/span"))).click();
				sleep(250);
				report.updateTestLog("Permanently emplyd question", "Selected Yes", Status.PASS);
					
				}	
			else{	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[2]/label/span"))).click();
				sleep(250);
				report.updateTestLog("Permanently emplyd question", "Selected No", Status.PASS);
					
				}
		  
		  
		//*****Resident of Australia****\\	
			
				if(Citizen.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
						
					}	
				else{	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
						
					}
				
				
				
		    }
		    //yuvi
									
									
									
		// if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
									
		    else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){			
									
		//******Do you work in hazardous environment*****\\		
			
		if(HazardousEnv.equalsIgnoreCase("Yes")){							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[5]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[5]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}							
									
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\	

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[6]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[1]/div[6]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}	

		//*****What is your annual Salary******							


		wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary);
		sleep(500);		
		report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);	

		//yuvi

			if(WorkLimitation.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[1]/label/span"))).click();
				sleep(250);
				report.updateTestLog("35 hours questions", "Selected Yes", Status.PASS);
					
				}	
			else{	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[2]/label/span"))).click();
				sleep(250);
				report.updateTestLog("35 hours questions", "Selected No", Status.PASS);
					
				}

		//*****Permanently employed***\\	
			
			if(WorkLimitation.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[1]/label/span"))).click();
				sleep(250);
				report.updateTestLog("Permanently emplyd question", "Selected Yes", Status.PASS);
					
				}	
			else{	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[2]/label/span"))).click();
				sleep(250);
				report.updateTestLog("Permanently emplyd question", "Selected No", Status.PASS);
					
				}

		//*****Resident of Australia****\\	
			
				if(Citizen.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
						
					}	
				else{	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
						
					}
				}		
				
		    else{
			//*****What is your annual Salary******							
		     

			wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary);
			sleep(500);		
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);	
			
		//35 hours question
			sleep(250);
				if(WorkLimitation.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);                                                 
					report.updateTestLog("35 hours questions", "Selected Yes", Status.PASS);
						
					}	
				else{	                                                         
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("35 hours questions", "Selected No", Status.PASS);
						
					}
			  
			  
			  
			  //*****Permanently employed***\\	
				sleep(250);
				if(WorkLimitation.equalsIgnoreCase("Yes")){		         
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[7]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Permanently emplyd question", "Selected Yes", Status.PASS);
						
					}	
				else{	 														
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[7]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Permanently emplyd question", "Selected No", Status.PASS);
						
					}
			  
			  
			//*****Resident of Australia****\\	
				sleep(250);
				
					if(Citizen.equalsIgnoreCase("Yes")){		                     
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[1]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[7]/div[2]/div/div[2]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}

		    }
		  sleep(800);
			
		
	//*****Click on Continue*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);	

  

			//******Click on Cost of  Insurance as*******\\		
			sleep(1000);			
			Select COSTTYPE=new Select(driver.findElement(By.name("premFreq")));		
			COSTTYPE.selectByVisibleText(CostType);	
			sleep(250);
			report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
			sleep(800);  
					  
						
										
			
						//*****Click on Calculate Quote********\\
						
						sleep(500);	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
						report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
						sleep(3000);
						

						if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4")).getText().equalsIgnoreCase("$0.00")){
							
							driver.findElement(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']")).click();
							sleep(5500);
						}
						

			            if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4")).getText().equalsIgnoreCase("$0.00")){
							
							driver.findElement(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']")).click();
							sleep(5500);
						}						
							
			
			            
			          //*****Click on SaveandExit button********\\    
			    
			        
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@class='btn btn-primary w100p ng-scope'])[2]"))).click();  
			            
			            
			    Thread.sleep(2000);      
			    
			    
			  //****Fetch the Application No******\\
				
				quickSwitchWindows();
				sleep(800);
				String AppNo=driver.findElement(By.xpath("//*[@id='tips_text']/strong[2]")).getText();
				report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);
				
				//******Finish and Close Window******\\
				
				sleep(500);
				driver.findElement(By.xpath("//button[@ng-dialog='secondDialogId']")).click();
				report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
				sleep(1500);			    
			    
			    
			
				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
		}				
						
						
}	



