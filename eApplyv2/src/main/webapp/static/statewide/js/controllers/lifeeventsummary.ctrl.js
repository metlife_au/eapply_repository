/*Summary Page Controller Starts*/
StateWideApp.controller('lifeeventsummary',['$scope','$rootScope', '$location','$timeout','$routeParams','$window','auraInputService','getAuraTransferData','submitAura','PersistenceService', 'submitEapply','auraResponseService','ngDialog', 'persoanlDetailService','saveEapply','RetrieveAppDetailsService','CalculateDeathService','CalculateTPDService','CalculateIPService','urlService',
                         function($scope,$rootScope, $location, $timeout, $routeParams, $window, auraInputService,getAuraTransferData,submitAura,PersistenceService,submitEapply,auraResponseService,ngDialog, persoanlDetailService,saveEapply, RetrieveAppDetailsService, CalculateDeathService,CalculateTPDService,CalculateIPService,urlService){

	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    $scope.errorOccured = false;
    $scope.eventList = [{
    	"cde": "MARR",
    	"desc": "Marriage",
    	"docDesc": "A marriage that is recognised as valid under the Marriage Act 1961(Cth)."
    },
    {
    	"cde": "BRTH",
    	"desc": "Birth or adoption of a child",
    	"docDesc": "Adopting or becoming the natural parent of a child."
    },
    {
    	"cde": "FRST",
    	"desc": "Obtaining a new mortgage or increasing an existing mortgage",
    	"docDesc": "Obtaining either a new mortgage or increasing an existing mortgage on your residence."
    },
    {
    	"cde": "DIVO",
    	"desc": "Divorce",
    	"docDesc": "Divorcing from a spouse."
    },
    {
    	"cde": "DSPO",
    	"desc": "Death of a spouse",
    	"docDesc": "Death of a spouse."
    },
    {
    	"cde": "CUGA",
    	"desc": "Completion of an undergraduate degree",
    	"docDesc": "Completing an undergraduate degree at an Australian University."
    },
    {
    	"cde": "DCSS",
    	"desc": "Dependent child starts secondary school",
    	"docDesc": "A dependent child starting secondary school."
    },
    {
    	"cde": "BCFM",
    	"desc": "Becoming a carer of an immediate family member",
    	"docDesc": "Becoming a carer of an immediate family member for the first time and being financially responsible for such care and/or are physically providing such care."
    },
    {
    	"cde": "NBLO",
    	"desc": "Obtaining a new business loan/increasing an existing business loan",
    	"docDesc": "Obtaining either a new business loan in excess of $100,000 or increasing an existing business loan by at least $100,000 (excluding re-draw and refinancing) on your business."
    }];
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	var decidedMode;
    	if($routeParams.mode == 3 || $scope.lifeEventCoverDetails.svRt){
    		decidedMode = 3
    	} else if($routeParams.mode == 1 || $routeParams.mode == 2){
    		decidedMode = 2;
    	}
    	$timeout(function(){
    		$location.path(path + decidedMode);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;
    };
    $window.scrollTo(0, 0);


    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
     };

     var ackCheckCCGC;



    $scope.lifeEventCoverDetails=PersistenceService.getlifeEventCoverDetails();
    $scope.lifeEventCoverOccDetails = PersistenceService.getLifeEventCoverOccDetails();
    $scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();
    $scope.auraDetails = PersistenceService.getLifeEventCoverAuraDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
	if($scope.lifeEventCoverDetails!= null && $scope.lifeEventCoverDetails.auraDisabled){
		$scope.auraDisabled = $scope.lifeEventCoverDetails.auraDisabled;
	}



    $scope.navigateToDecision = function(){
	    ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
    	if(ackCheckCCGC){
    		$scope.CCGCackFlag = false;
    		if($scope.lifeEventCoverDetails != null && $scope.lifeEventCoverOccDetails != null && $scope.personalDetails != null){
    			$rootScope.$broadcast('disablepointer');
    			$scope.lifeEventCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.occupationDetails = $scope.lifeEventCoverOccDetails;
    			$scope.details.lifeEventDocuments = $scope.uploadedFileDetails;

    			var temp = angular.extend($scope.details,$scope.lifeEventCoverDetails);
    			var submitObject = null;
    			if($scope.auraDetails != null){
    				var aura = angular.extend(temp,$scope.auraDetails);
    				submitObject = angular.extend(aura, $scope.personalDetails);
    			}else{
    				submitObject = angular.extend(temp, $scope.personalDetails);
    			}


    			auraResponseService.setResponse(submitObject);
    			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
            		if($scope.auraDetails!=null){
            			if($scope.auraDetails.overallDecision == 'ACC'){
                    			$location.path('/lifeeventaccept');
                    	} else if($scope.auraDetails.overallDecision == 'DCL'){
                    		$location.path('/lifeeventdecline');
                    	} else if($scope.auraDetails.overallDecision == 'RUW'){
                    		$location.path('/lifeeventunderwriting');
                    	}
            		}else{
            			$location.path('/lifeeventaccept');
            		}

            	}, function(err){
            		$scope.errorOccured = true;
            		$window.scrollTo(0, 0);
            		$rootScope.$broadcast('enablepointer');
            	});
            }
    	} else{
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
    	}
    };


    var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveSummary = function(){
    	$scope.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.lifeEventCoverDetails != null && $scope.lifeEventCoverOccDetails != null &&  $scope.personalDetails != null){
    		$scope.lifeEventCoverDetails.lastSavedOn = 'SummaryLifeEventPage';
    		$scope.details={};
			$scope.details.occupationDetails = $scope.lifeEventCoverOccDetails;
			$scope.details.lifeEventDocuments = $scope.uploadedFileDetails;
    		var temp = angular.extend( $scope.details,$scope.lifeEventCoverDetails);
    		var saveSummaryObject = null;
    		if($scope.auraDetails!=null){
    			var aura = angular.extend(temp,$scope.auraDetails);
    			saveSummaryObject = angular.extend(aura, $scope.personalDetails);
    		}else{
    			saveSummaryObject = angular.extend(temp, $scope.personalDetails);
    		}

        	auraResponseService.setResponse(saveSummaryObject);
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                console.log(response.data)
	        });
    	}

    };

    $scope.summarySaveAndExitPopUp = function (hhText) {
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

    $scope.checkAckStateGC = function(){
    	$timeout(function(){
    		ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
    	}, 10);
    };

    if($routeParams.mode == 3){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
    		var result = res.data[0];
    		var coverDet = {},
    			occDet = {},
    		    auraLoadingDet ={};

    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		coverDet.contactType = result.contactType;
    		coverDet.deathAmt = result.existingDeathAmt;
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.tpdAmt = result.existingTpdAmt;
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.ipAmt = result.existingIPAmount;
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium = result.totalPremium;
    		coverDet.deathCoverPremium = result.deathCoverPremium;
    		coverDet.tpdCoverPremium = result.tpdCoverPremium;
    		coverDet.ipCoverPremium = result.ipCoverPremium;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.eventAlreadyApplied = result.eventAlreadyApplied;
    		coverDet.eventDate = result.eventDate;
    		coverDet.eventName = result.eventName;
    		coverDet.deathNewAmt = result.deathNewAmt;
    		coverDet.tpdNewAmt = result.tpdNewAmt;
    		coverDet.ipNewAmt = parseFloat(result.ipNewAmt);
    		coverDet.waitingPeriod = result.waitingPeriod;
    		coverDet.benefitPeriod = result.benefitPeriod;
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.manageType = result.manageType;
    		coverDet.partnerCode = result.partnerCode;
    		coverDet.ipLifeCoverType = 'IpFixed';
    		coverDet.appNum = result.appNum;
    		coverDet.documentName = (result.lifeEventDocuments && result.lifeEventDocuments.length > 0) ? 'Yes' : 'No';
    		$scope.uploadedFileDetails = coverDet.documentName === 'Yes' ? result.lifeEventDocuments : [];	

    		if(result.auraDisabled && result.auraDisabled =="true"){
    			$scope.auraDisabled = true;
    		}else{
    			$scope.auraDisabled = false;
    		}
    		var tempEvent = $scope.eventList.filter(function(obj){
	    		return obj.cde == result.eventName;
	    	});
    		coverDet.eventDesc = tempEvent[0].desc;
    	  //coverDet.auraDisabled = result.auraDisabled;
    		$scope.lifeEventCoverDetails = coverDet;

	    	/*$scope.event = tempEvent[0];*/
    		occDet.gender = result.personalDetails.gender;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
	    	occDet.ownBussinessQues = result.occupationDetails.ownBussinessQues;
	    	occDet.ownBussinessYesQues = result.occupationDetails.ownBussinessYesQues;
	    	occDet.ownBussinessNoQues = result.occupationDetails.ownBussinessNoQues;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		occDet.hazardousQue = result.occupationDetails.hazardousQue;
    		$scope.lifeEventCoverOccDetails = occDet;
    		$scope.svdRtrv = true;
    		
    		PersistenceService.setlifeEventCoverDetails(coverDet);
	    	PersistenceService.setLifeEventCoverOccDetails(occDet);
	    	$scope.getAuradetails();
    	}, function(err){
    		console.log("Encountered an error while fetchimg the app details " + err);
    	});
    }
    
    $scope.getAuradetails = function()
    {
    	auraInputService.setFund('SFPS');
      	auraInputService.setMode('LifeEvent');
      	auraInputService.setName($scope.lifeEventCoverDetails.name);
      	auraInputService.setAge(moment().diff(moment($scope.lifeEventCoverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
      	auraInputService.setAppnumber($scope.lifeEventCoverDetails.appNum);
      	auraInputService.setDeathAmt(parseInt($scope.lifeEventCoverDetails.deathAmt));
      	auraInputService.setTpdAmt(parseInt($scope.lifeEventCoverDetails.tpdAmt));
      	auraInputService.setIpAmt(parseInt($scope.lifeEventCoverDetails.ipAmt));
      	auraInputService.setWaitingPeriod($scope.lifeEventCoverDetails.waitingPeriod);
      	auraInputService.setBenefitPeriod($scope.lifeEventCoverDetails.benefitPeriod);
      	if($scope.lifeEventCoverOccDetails && $scope.lifeEventCoverOccDetails.gender ){
      		auraInputService.setGender($scope.lifeEventCoverOccDetails.gender);
      	}
      	auraInputService.setIndustryOcc($scope.lifeEventCoverOccDetails.industryCode+":"+$scope.lifeEventCoverOccDetails.occupation);
      	auraInputService.setCountry('Australia');
      	auraInputService.setSalary('150000');
      	auraInputService.setFifteenHr('Yes');
      	auraInputService.setClientname('metaus')
      	auraInputService.setLastName($scope.lifeEventCoverDetails.lastName)
      	auraInputService.setFirstName($scope.lifeEventCoverDetails.firstName)
      	auraInputService.setDob($scope.lifeEventCoverDetails.dob)
      	auraInputService.setExistingTerm(false);
      	if($scope.personalDetails.memberType=="Personal"){
      		auraInputService.setMemberType("INDUSTRY OCCUPATION")
      	}else{
      		auraInputService.setMemberType("None")
      	}
    	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
      		$scope.auraResponseDataList = response.data.questions;
      		angular.forEach($scope.auraResponseDataList, function(Object) {
    			$scope.sectionname = Object.questionAlias.substring(3);

    			});
      	});
    	 
    	 submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
    		 $scope.auraDetails = response.data;
    	 });
    	 
    };

    }]);
   /*Summary Page Controller Ends*/
