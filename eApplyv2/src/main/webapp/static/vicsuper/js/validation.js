
function extractNumber(obj, decimalPlaces, allowNegative)
{
      var temp = obj.value;
      
      // avoid changing things if already formatted correctly
      var reg0Str = '[0-9]*';
      if (decimalPlaces > 0) {
            reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
      } else if (decimalPlaces < 0) {
            reg0Str += '\\.?[0-9]*';
      }
      reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
      reg0Str = reg0Str + '$';
      var reg0 = new RegExp(reg0Str);
      if (reg0.test(temp)) return true;

      // first replace all non numbers
      var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
      var reg1 = new RegExp(reg1Str, 'g');
      temp = temp.replace(reg1, '');

      if (allowNegative) {
            // replace extra negative
            var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
            var reg2 = /-/g;
            temp = temp.replace(reg2, '');
            if (hasNegative) temp = '-' + temp;
      }
      
      if (decimalPlaces != 0) {
            var reg3 = /\./g;
            var reg3Array = reg3.exec(temp);
            if (reg3Array != null) {
                  // keep only first occurrence of .
                  //  and the number of places specified by decimalPlaces or the entire string if 

decimalPlaces < 0
                  var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                  reg3Right = reg3Right.replace(reg3, '');
                  reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                  temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
            }
      }
      
      obj.value = temp;
}
function blockNonNumbers(obj, e, allowDecimal)
{
      var key;
      var isCtrl = false;
      var keychar;
      var reg;
            
      if(window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
      }
      else if(e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
      }
      
      if (isNaN(key)) return true;
      
      keychar = String.fromCharCode(key);
      
      // check for backspace or delete, or if Ctrl was pressed
      if (key == 8 || isCtrl)
      {
            return true;
      }

      reg = /\d/;
      //var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
      var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
      
      //return isFirstN || isFirstD || reg.test(keychar);
}



//for numeric validation

 function isNumeric(target){

 	if (null != document.getElementById(target)) {
	 	var v=document.getElementById(target).value;
	 	var regExp= /^[0-9]*$/;
	 					 	
	 	if (v.length > 0){
				
			if ((v.charAt(v.length-1)).match(regExp)) {					 			
				return true;
	 		}else {
		 		document.getElementById(target).value=v.substring(0,v.length-1);
		 		return false;
	 		}
	 	}
 	}	 
 	
 }
 
 
//for numeric validation not allow starting 0 in transfer cover

 function isNumericTransfer(target){

 	if (null != document.getElementById(target)) {
	 	var v=document.getElementById(target).value;
	 	var regExp= /^[0-9]*$/;
	 					 	
	 	if (v.length > 0){
				
	 		if(v.length == 1 && v === '0')
	 			{
	 			document.getElementById(target).value=null;
	 			return false;
	 			}
	 		
			if ((v.charAt(v.length-1)).match(regExp)) {					 			
				return true;
	 		}else {
		 		document.getElementById(target).value=v.substring(0,v.length-1);
		 		return false;
	 		}
	 	}
 	}	 
 	
 }
 
  //for Salary validation

 function isDefault(target){

 	var v=document.getElementById(target).value;

 					 	
 	if (v=="dd/mm/yyyy"){
	
	 		document.getElementById(target).value="";
	 	
 	}	 
 	
 }
 
 //for Salary validation

 function isSalary(target){

 	var v=document.getElementById(target).value;
 	var regExp= /^[0-9]*$/;
 					 	
 	if (v.length > 0){
			
		if ((v.charAt(v.length-1)).match(regExp)) {					 			
			return true;
 		}else {
	 		document.getElementById(target).value=v.substring(0,v.length-1);
	 		return false;
 		}
 	}	 
 	
 }
 //for character validation
 function isCharacter(target){

 	var v=document.getElementById(target).value;
 	var regExp= /^[a-z A-Z'-]*$/;
 					 	
/* 	if (v.length > 0){
			
 		if ((v.charAt(v.length-1)).match(regExp)) {					 			
			return true;
 		}else {
	 		document.getElementById(target).value=v.substring(0,v.length-1);
	 		return false;
 		}
 	}
 */	
 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			document.getElementById(target).value=str1+str2;
 	
 		return false;
 		}
 	}	 	
 }
 
 
 function isObject( what ) { return (typeof what == 'object'); } 
 
 
 
 function isCharacterAlpha(target){
	var regExp= /^[a-z A-Z-]*$/;
   if(target != null && !isObject(target)){
	 	var v=document.getElementById(target).value;
	 	for(var k=0;k<v.length;k++){
	 		if (!(v.charAt(k)).match(regExp)) {
	 			str1=v.substring(0, k);
	 			str2=v.substring(k+1);
	 			document.getElementById(target).value=str1+str2;
		 		return false;
		 	}
	 	}
 	}
	
 	if(target != null && isObject(target)) {
 		var v=target.value;
 		for(var k=0;k<v.length;k++){
	 		if (!(v.charAt(k)).match(regExp)) {
	 			str1=v.substring(0, k);
	 			str2=v.substring(k+1);
	 			target.value=str1+str2;
		 		return false;
		 	}
 	}
 	}
 	
 	
 					 	
/* 	if (v.length > 0){
			
 		if ((v.charAt(v.length-1)).match(regExp)) {					 			
			return true;
 		}else {
	 		document.getElementById(target).value=v.substring(0,v.length-1);
	 		return false;
 		}
 	}
 */	
 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			document.getElementById(target).value=str1+str2;
 		return false;
 		}
 	}	 	
 }
 
 //for checking 0,',blank space in numeric, character fields
 function isEmpty(target){
 	var v=document.getElementById(target).value;
 	var regExp1=  /^[ ]*$/;					 					 	
 	var regExp2=  /^[0]*$/;	
 	var regExp3=  /^[']*$/;	
 	var regExp4=  /^['0 ]*$/;	
 		 				 	
 	if (v.length > 0){								 			 		
 		if (v.match(regExp1)){
 				v='';					 				
 		}else if (v.match(regExp2)){
 				v='';					 				
 		}else if (v.match(regExp3)){
 				v='';
 		}else if (v.match(regExp4)){
 				v='';
 		}
 		document.getElementById(target).value=v;
 		return true;
 	}
 }
 
 //event if the user press enter
 function pressedEnter(e,page){ 		
	var keycode;
	
   	if(window.event) // IE
	{
	  keycode = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
	  keycode = e.which;
	}		
 	if(keycode==13){
 		if (page == 'uw'){ 			
 			eventFire('UWSearchRsltBtn','click');		
		}else if (page == 'cl'){
			eventFire('CLSearchRsltBtn','click');
		}
	} 		
 }	
 	
//cross browser click event handler
function eventFire(el, etype){
	var flag=false;
	if(window.event){
	if (navigator.userAgent.indexOf('Safari') != -1) {
		var theEvent = document.createEvent("MouseEvent");
		theEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		var element = document.getElementById(el);
		element.dispatchEvent(theEvent);
	} 
		//document.getElementById(el).click();
		flag=true;
		window.document.all[el].click();
	}else
	if(flag==false) {
		var theEvent = document.createEvent("MouseEvent");
		theEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		var element = document.getElementById(el);
		element.dispatchEvent(theEvent);
	}
}
 	
 
function isNumberFun(target){
 	var v=document.getElementById(target).value;
 	var regExp= /^[0-9]*$/;

 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			document.getElementById(target).value=str1+str2;
 	
 		return false;
 		}
 	}
 }
 	
function isAlphaNumeric(target){
 	var v=document.getElementById(target).value;
 	var regExp= /^[0-9 a-z A-Z]*$/;

 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			document.getElementById(target).value=str1+str2;
 	
 		return false;
 		}
 	}
 } 	
 
 
function isDateFormat(target){

if(target != null && !isObject(target)){
	var v=document.getElementById(target).value;
 	var regExp= /^[0-9/]*$/;
// 	var regExp=/^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/;
 	for(var k=0;k<v.length;k++){ 	
 		if (!(v.charAt(k)).match(regExp)) { 	
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			document.getElementById(target).value=str1+str2;
 	
 		return false;
 		} 	
 	} 	
}
if(target != null && isObject(target)){
	var v=target.value;
 	var regExp= /^[0-9/]*$/;
// 	var regExp=/^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/;
 	for(var k=0;k<v.length;k++){ 	
 		if (!(v.charAt(k)).match(regExp)) { 	
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			target.value=str1+str2;
 	
 		return false;
 		} 	
 	} 	
}
 	
 } 	
 
 //for adding the values in a drop down
 function addOption(selectbox, value, text ) { 
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
 } 
 
 //for removing the values from a drop down
 function removeOption(selectbox) { 
  	selectbox.length=0;
 }
 
 //for changing the height, weight, date, season units in Personal Stmt UI
 function changeUnits(target1,target2,unit,flag,target3){							
	
	var comp1=target1;
	var comp2=target2;
	var comp3=unit;	
	var comp4=target3;	
	//alert (target1+"/"+target2+"/"+unit+"/"+flag+"/"+target3); 	
	if (unit=='Feet'){
		document.getElementById(comp1).style.display="block";
		if (document.getElementById(comp1).style.display == 'none')
		{							 
			document.getElementById(comp1).style.display = '';
		}
		//document.getElementById(comp2).style.visibility="visible";
		if(document.getElementById(comp2+'_table')){
			document.getElementById(comp2+'_table').style.display="block";
			if (document.getElementById(comp2+'_table').style.display == 'none')
			{							 
				document.getElementById(comp2+'_table').style.display = '';
			}
		}
		if(document.getElementById(comp2+'_spr')){
			document.getElementById(comp2+'_spr').style.display="block";
			if (document.getElementById(comp2+'_spr').style.display == 'none')
			{							 
				document.getElementById(comp2+'_spr').style.display = '';
			}
			
		}
		if(document.getElementById(comp4) ){
			if(flag=="false" ){
				//document.getElementById(comp4).value="";
			}
		}
			
		document.getElementById(comp1).value = "in";
		document.getElementById(comp2).disabled=false;
		document.getElementById(comp1).disabled=true;
		//document.getElementById(comp2).focus();
	}else if (unit=='Metres'){
		if(document.getElementById(comp4) ){
			if(flag=="false" ){
				document.getElementById(comp4).value="";
			}
		}
		document.getElementById(comp1).value = "cm";
		document.getElementById(comp2).disabled=false;
		document.getElementById(comp1).disabled=true;
		//document.getElementById(comp2).focus();
	}else if (unit=='cm'){
	if(document.getElementById(comp4) ){
			if(flag=="false" ){
				//document.getElementById(comp4).value="";
			}
		}
		document.getElementById(comp1).value = "";
		document.getElementById(comp2).value = "";
		document.getElementById(comp1).style.display="none";
		//document.getElementById(comp2).style.visibility="hidden";
		if(document.getElementById(comp2+'_table')){
			document.getElementById(comp2+'_table').style.display="none";
		}
		if(document.getElementById(comp2+'_spr')){
			document.getElementById(comp2+'_spr').style.display="none";
		}
		document.getElementById(comp1).disabled=true;
		//document.getElementById(comp2).focus();
	}else if (unit=='Stones'){
		if(document.getElementById(comp4) ){
			if(flag=="false" ){
				//document.getElementById(comp4).value="";
			}
		}
		document.getElementById(comp1).style.display="block";
		if (document.getElementById(comp1).style.display == 'none')
		{							 
			document.getElementById(comp1).style.display = '';
		}
		//document.getElementById(comp2).style.visibility="visible";
		if(document.getElementById(comp2+'_table')){
			document.getElementById(comp2+'_table').style.display="block";
			if (document.getElementById(comp2+'_table').style.display == 'none')
			{							 
				document.getElementById(comp2+'_table').style.display = '';
			}
		}
		if(document.getElementById(comp2+'_spr')){
			document.getElementById(comp2+'_spr').style.display="block";
			if (document.getElementById(comp2+'_spr').style.display == 'none')
			{							 
				document.getElementById(comp2+'_spr').style.display = '';
			}
		}
		document.getElementById(comp1).value = "lbs"; 
		document.getElementById(comp2).disabled=false;
		document.getElementById(comp1).disabled=true;
		//document.getElementById(comp2).focus();
	}else if (unit=='Kilograms'){ 
		if(document.getElementById(comp4) ){
			if(flag=="false" ){
				//document.getElementById(comp4).value="";
			}
		}
		document.getElementById(comp1).style.display="none";
		//document.getElementById(comp2).style.visibility="hidden";
		if(document.getElementById(comp2+'_table')){
			document.getElementById(comp2+'_table').style.display="none";
		}
		if(document.getElementById(comp2+'_spr')){
			document.getElementById(comp2+'_spr').style.display="none";
		}
		document.getElementById(comp1).value = "";
		document.getElementById(comp2).value = "";
		document.getElementById(comp2).disabled=true;
		document.getElementById(comp1).disabled=true;
	}else if (unit=='Pounds'){
		if(document.getElementById(comp4) ){
			if(flag=="false" ){
				//document.getElementById(comp4).value="";
			}
		}
		document.getElementById(comp1).style.display="none";
		//document.getElementById(comp2).style.visibility="hidden";
		if(document.getElementById(comp2+'_table')){
			document.getElementById(comp2+'_table').style.display="none";
		}
		if(document.getElementById(comp2+'_spr')){
			document.getElementById(comp2+'_spr').style.display="none";
		}
		document.getElementById(comp1).value = "";
		document.getElementById(comp2).value = "";
		document.getElementById(comp2).disabled=true;	
		document.getElementById(comp1).disabled=true;					
	}	
	if ((unit=='Exact Date') || (unit=='Number Of Months') || (unit=='Relative Age')){		
		document.getElementById(comp1).disabled=false;			
		if (unit=='Exact Date') 							
			document.getElementById(comp2).style.visibility="visible";
		else
			document.getElementById(comp2).style.visibility="hidden";
		removeOption(document.getElementById(comp1));
		addOption(document.getElementById(comp1),"None", "None");										
		document.getElementById(comp1).focus();
	} else if (unit =='season'){		
		document.getElementById(comp1).disabled=false;
		document.getElementById(comp2).style.visibility="hidden"; 
		removeOption(document.getElementById(comp1));					 
		var arrSeason = new Array("Spring","Summer","Autumn","Winter");							
		for(var itr=0;itr<arrSeason.length;itr++){
		addOption(document.getElementById(comp1),arrSeason[itr], arrSeason[itr]);
		}
		document.getElementById(comp1).focus();										
	}else if (unit =='month'){			
		document.getElementById(comp1).disabled=false;	
		document.getElementById(comp2).style.visibility="hidden";
		removeOption(document.getElementById(comp1));				 												
		for(var itr=1;itr<=12;itr++){
			addOption(document.getElementById(comp1),itr, itr);
		}
		document.getElementById(comp1).focus();

	}
	poupulateDefaultHeightWeight();
} 

//for displaying the entity question details in Personal Stmt UI
function showEntity(target1,target2,target3,unit,entValues){
						
	var comp1=target1;
	var comp2=target2;
	var comp3=target3;
	var ent=new Array();
	//alert (comp1+"/"+comp2+"/"+comp3+"/"+unit+"/"+entValues);
	if ((entValues.indexOf(unit)) > -1){
			
		entValues=entValues.substring(entValues.search(unit));		
		ent=entValues.split("<>",3); 		
		if (ent[1] !=null){
			  if (ent[2] != null){			  		
				document.getElementById(comp1).value=ent[0];
				document.getElementById(comp2).value=ent[1]; 
			 	document.getElementById(comp3).value=ent[2];
			  }else {			  			
				document.getElementById(comp1).value="";
				document.getElementById(comp2).value="";
	 			document.getElementById(comp3).value="";	
			  }
		}else {				
			document.getElementById(comp1).value="";
			document.getElementById(comp2).value="";
	 		document.getElementById(comp3).value="";	
		}			 	
	} else if ((entValues.indexOf(unit)) == -1){ 			
		document.getElementById(comp1).value="";
		document.getElementById(comp2).value="";
	 	document.getElementById(comp3).value="";			
	}	 
	document.getElementById(comp1).focus(); 
} 	

//for setting the question pointer in Personal Stmt UI
function qsPointer(idList) 
{ 
	//for disabling the weight & height components on form load
	var comp,comp1,comp2,unit;
	comp1=''; comp2=''; unit='';
	if ((idList.indexOf("wq2-")) > -1){
		comp=idList.substring(idList.search("wq2-"));				
		comp=comp.split("<>",1); 	
		unit = document.getElementById(comp).value;
		//alert("unit wq2:"+unit);
		if ((idList.indexOf("wq3-")) > -1){
			comp1=idList.substring(idList.search("wq3-"));				
			comp1=comp1.split("<>",1); 	
			//alert("comp wq3:"+comp1);
		}
		if ((idList.indexOf("wq4-")) > -1){
			comp2=idList.substring(idList.search("wq4-"));				
			comp2=comp2.split("<>",1); 	
			//alert("comp wq4:"+comp2);
		}
		if (unit != ''){
			if (comp1 != ''){
				if (comp2 != ''){
					changeUnits(comp2,comp1,unit);
				}		
			}
		}
	}
	comp1=''; comp2=''; unit='';
	if ((idList.indexOf("hq2-")) > -1){
		comp=idList.substring(idList.search("hq2-"));				
		comp=comp.split("<>",1); 		
		unit = document.getElementById(comp).value;
		//alert("unit hq2:"+unit);
		if ((idList.indexOf("hq3-")) > -1){
			comp1=idList.substring(idList.search("hq3-"));				
			comp1=comp1.split("<>",1); 	
			//alert("comp hq3:"+comp1);
		}
		if ((idList.indexOf("hq4-")) > -1){
			comp2=idList.substring(idList.search("hq4-"));				
			comp2=comp2.split("<>",1); 	
			//alert("comp hq4:"+comp2);
		}
		if (unit != ''){
			if (comp1 != ''){
				if (comp2 != ''){
					changeUnits(comp2,comp1,unit);
				}		
			}
		}
	}
	var comp,comp1,comp2,unit;
	comp1=''; comp2=''; unit='';
	if ((idList.indexOf("dwcq1-")) > -1){
		comp=idList.substring(idList.search("dwcq1-"));				
		comp=comp.split("<>",1); 	
		unit = document.getElementById(comp).value;
		//alert("unit dwcq1:"+unit);
		if ((idList.indexOf("dwcq2-")) > -1){
			comp2=idList.substring(idList.search("dwcq2-"));				
			comp2=comp2.split("<>",1); 	
			//alert("comp dwcq2:"+comp2);
		}
		if ((idList.indexOf("dwcq4-")) > -1){
			comp1=idList.substring(idList.search("dwcq4-"));				
			comp1=comp1.split("<>",1); 	
			//alert("comp dwcq4:"+comp1);
		}
		if (unit != ''){
			if (comp1 != ''){
				if (comp2 != ''){
					changeUnits(comp2,comp1,unit);
				}		
			}
		}
	}
	// Hieght & Weight Default value selected - Atul
	//document.getElementById('hq2-2').options[1].selected=true;
	//document.getElementById('wq2-3').options[1].selected=true;
}   
//for initializing the form components
function initialize() 
{   
	var idList=document.getElementById('compId').value;		
	document.getElementById('compId').style.visibility='hidden';	
	//document.getElementById('inputXML').style.visibility='hidden';	
	qsPointer(idList);  						
}
function confirmNoneOfTheAbove(el){
		var userDecision = confirm("Clicking on 'None of the above' will clear your medical disclosures in the above section.\n Do you wish to proceed?")
		if(userDecision){
			return true;
		}
		else{
			return false;
		}
	}
//for setting the controls disabled
function disableAll() 
{	
	for (var i=0;i<document.forms[0].elements.length;i++){		
		document.forms[0].elements[i].disabled=true;
	}
	return true; 
}
//for setting the controls enabled
function enableAll() 
{		
	for (var i=0;i<document.forms[0].elements.length;i++){	
		document.forms[0].elements[i].disabled=false;
	}
	return true;
} 

//for cursor change
var tempId;
function registerStateChangeListener(buttonids){

      TrPage.getInstance().getRequestQueue().addStateChangeListener(changeCursor);					
}

function changeCursor(state)
{
	
     if (state == TrRequestQueue.STATE_BUSY) {
     	disableAll();
        document.body.style.cursor = "wait";        
        //alert("Changed cursor to hour glass!");
     } else {     				
     	enableAll();     	     	
        document.body.style.cursor = "default";
        //alert("Changed cursor to default!");
     }
}

function  registerStateChangeListenerIndividual(buttonids){
	   tempId=buttonids;
      TrPage.getInstance().getRequestQueue().addStateChangeListener(changeCursorIndividual);					
}

function changeCursorIndividual(state)
{

     if (state == TrRequestQueue.STATE_BUSY) {
     	disableIndividualAll();
        document.body.style.cursor = "wait";        
     } else {     				
     	enableIndividualAll();     	     	
        document.body.style.cursor = "default";
        TrPage.getInstance().getRequestQueue().removeStateChangeListener(changeCursorIndividual);		
     }
}

//for setting the controls disabled
/*function disableIndividualAll() 
{	
	for (var i=0;i<document.forms[0].elements.length;i++){		
		document.forms[0].elements[i].disabled=true;
	}
	return false; 
}

//for setting the controls enabled
function enableIndividualAll() 
{	
	for (var i=0;i<document.forms[0].elements.length;i++){	
		document.forms[0].elements[i].disabled=false;
	}
	return false;
} */

var disabledElements;
function disableIndividualAll() 
{	
	disabledElements = new Array(document.forms[0].elements.length)
	for (var i=0;i<document.forms[0].elements.length;i++){		
		if (document.forms[0].elements[i].disabled) {
			disabledElements[i] = document.forms[0].elements[i];
		}
		document.forms[0].elements[i].disabled=true;
	}
	return false; 
}

//for setting the controls enabled
function enableIndividualAll() 
{	
	for (var i=0;i<document.forms[0].elements.length;i++){	
		document.forms[0].elements[i].disabled=false;
		for (var j=0;j<disabledElements.length;j++) {
			if (isObject(disabledElements[j])) {
				if (document.forms[0].elements[i].id == disabledElements[j].id) {
					document.forms[0].elements[i].disabled=true;
				}
			} 
		}
	}
	return false;
} 

function OnTop(){
     if(document.getElementById("scrollTop").value == "scroll"){
       var vv =document.getElementById("scrollTop").value ;
       scrollTo(0,0);
     }
}

var Individual = {
	validateEmailAddress: function (target, errDsplId) {
		var retval;
		if(!Individual.validateEmail(target)){
			document.getElementById(errDsplId).innerHTML = "&#160;Email should be in valid Format";
			retval = false;
		}else{
			document.getElementById(errDsplId).innerHTML = "";
			retval = true;
		}
		return retval;
	},
	validateEmail: function (target){
		var emailID=document.getElementById(target).value;
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		var retrval = emailPattern.test(emailID);
		return retrval;
	},
	validatePasscode: function (target, errDsplId) {
		var retval;
		var passcode=document.getElementById(target).value;
		if(passcode==null || passcode.trim()=='') {
			document.getElementById(errDsplId).innerHTML = "&#160;Password should be in valid Format";
			retval = false;
		}else{
			document.getElementById(errDsplId).innerHTML = "";
			retval = true;
		}
		return retval;
	}
}
var disableCvrAmtForJoint=false;
function  registerStateChangeListenerAutoQuote(disableCvrAmtForJoint){
	this.disableCvrAmtForJoint=disableCvrAmtForJoint;
	TrPage.getInstance().getRequestQueue().addStateChangeListener(changeCursorAutoQuote);					
}
function changeCursorAutoQuote(state){
     if (state == TrRequestQueue.STATE_BUSY) {
     	disableAutoQuoteAll();
        document.body.style.cursor = "wait";        
     } else {     				
     	enableAutoQuoteAll();     	     	
        document.body.style.cursor = "default";
        TrPage.getInstance().getRequestQueue().removeStateChangeListener(changeCursorAutoQuote);		
     }
}
function disableAutoQuoteAll(){	
	for (var i=0;i<document.forms[0].elements.length;i++){		
		document.forms[0].elements[i].disabled=true;
	}
	return false; 
}
function enableAutoQuoteAll(){	
	for (var i=0;i<document.forms[0].elements.length;i++){	
		document.forms[0].elements[i].disabled=false;
	}
	//SRKR.agent email id is always disabled, so while enabling disable it back.
	if(document.getElementById('eamilidAgent')!=null){
		document.getElementById('eamilidAgent').disabled=true;
	}
	//SRKR. Cover amount selection in join is always disabled for DRLN Accidental plan.
	if(document.getElementById('coverR')!=null){
		if(this.disableCvrAmtForJoint==true || this.disableCvrAmtForJoint=="true"){
			document.getElementById("coverR").disabled=true;
		}
	}
	return false;
}

function validateFlyBuys(flybuyNumber){							
	//document.getElementById("demo").innerHTML = Date();
	var isValid =false ; 
	var number = flybuyNumber;
	number= number.replace(" ", "");
	
	if(number!=null && number.length==10){
	
		var firstNum = (number.charAt(0));
		if((firstNum>=2) && (firstNum<=9)){
			var flybuynumber = "600894"+number;
			
			
           	var evenpositionvaluesum = 0;
			var oddpositionvalsum = 0;
			for (var i = 0; i < flybuynumber.length-1; i++) {
			
					var k = i + 1;			
					if (k % 2 == 0) {
					/**1.	Sum the digits in the even numbered positions from left to right. **/				
					var evenPositionValue = flybuynumber.charAt(k - 1);
					evenpositionvaluesum = parseInt(evenpositionvaluesum)+ parseInt(evenPositionValue);					
					}else {
					/** 2.	Multiply each digit in the odd numbered positions (from left to right) by the 														number 2.  If any results are 2 digits, sum the digits into one.  Sum the digits 												from each multiplication into a final result.**/				
					var oddPositionValue = flybuynumber.charAt(k - 1);
					var multiplyValue = parseInt(oddPositionValue) * 2;
					var oddPositionValuetointegerValue = 0;
					if (multiplyValue > 9) {
					oddPositionValuetointegerValue=(parseInt(parseInt(multiplyValue)%10))+ (parseInt(parseInt													(multiplyValue)/10));
					oddpositionvalsum = parseInt(oddpositionvalsum)+ parseInt(oddPositionValuetointegerValue);
					} else {
					oddpositionvalsum = parseInt(oddpositionvalsum) + parseInt(multiplyValue);						
					}				
					}
				}
			//document.getElementById("evensum").innerHTML = evenpositionvaluesum;
			//document.getElementById("oddsum").innerHTML = oddpositionvalsum;
			/** 3. Add the final results of steps 1 and 2. **/
			var finalresult=parseInt(evenpositionvaluesum)+parseInt(oddpositionvalsum);
			//document.getElementById("checkResult").innerHTML=finalresult;
						
			/** 4.	Take the last digit of the result of step 3 and subtract from 10 to give the check digit.  If 												the result of step 3 is a multiple of 10, the check digit will be zero. **/	
				var checkdigit = 0;
				if(parseInt(finalresult % 10)== 0){
				checkdigit = 0;
			}else{
			
				checkdigit = 10 - parseInt(finalresult % 10);
				
			}
			
			//document.getElementById("checkdigit").innerHTML=checkdigit;

			
                   
			 if(flybuynumber!=null && (parseInt(flybuynumber%10))==parseInt(checkdigit)){
			
			            
                                  isValid=true;
							
							
                            }else{
                           
                                  isValid=false;
							//document.getElementById("validation").innerHTML=isValid;
                            }
		}else{
			isValid = false;
			
		}	
		
		
	}else{
			isValid = false;
			
		}	
	            //alert(isValid);
				//document.getElementById("validation").innerHTML=isValid;
 return isValid;
}

function CommaFormatted(id) {			
	var delimiter = ",";  
	var num = document.getElementById(id).value;
	var parts = num.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		document.getElementById(id).value = parts.join(".");
}
function checkMinMaxAmount(cvramntID,minAmt,maxAmt) {						
		var cvrAmnt = document.getElementById(cvramntID).value;

	if (cvrAmnt != null && cvrAmnt != '') {							
		if ((minAmt <= cvrAmnt) && (cvrAmnt <= maxAmt)) {									
			return true;
		} else {																				
			return false;
		}
	}else {									
		return false;
							
	}						
}  	

function getAgeNextBirthDay(date) {

	if (null != date && date != '' && isNaN(date)) {	
		var age = 0;
	
	 	var today = new Date();
	 	var UserDate=document.getElementById("datepicker").value;
	 	var elem = UserDate.split('/');  
        day = elem[0];  
        month = elem[1];  
        year = elem[2];
	 	
	 	//var birth = new Date(year,month,day);
	 	var birth = new Date(month+'/'+day+'/'+year);
	 	
	    var nowyear = today.getFullYear();
	    var nowmonth = today.getMonth()+1;
	    var nowday = today.getDate();
	    
	    var birthyear = birth.getFullYear();
	    var birthmonth = birth.getMonth()+1;
	    var birthday = birth.getDate();
	    
	    age = nowyear - birthyear;
	    var age_month = nowmonth - birthmonth;
	    var age_day = nowday - birthday;
	    
	    if (age_day == 0 && age_month == 0) {
	    	age = parseInt(age)+1;
	    } else if (age_month > 0) {
	    	age = parseInt(age)+1;
	    } else if (age_month == 0 && age_day > 0) {
	    	age = parseInt(age)+1;
	    }
	    //alert('age :'+age);    
		return age;
	}
	
}	

function validateAge(date,minAge,maxAge) {	
		
	var age = getAgeNextBirthDay(date);				
	if (!(age >= parseInt(minAge) && age <= parseInt(maxAge))) {		
		document.getElementById("datepicker").value="";
		minAge = minAge-1;
		maxAge = maxAge-1;
		document.getElementById("dobErrTxtID").innerHTML = 'The person to be insured must be between '+minAge+' and '+maxAge+' years to apply for this product'; 
		showErrMsgsoncalc('dob_div_id','tick2');
		dateOfBirthValErr=true;
		dateOfBirthFilled=false;		
	}	
	
}
function isIntegerFun(obj){
 	var v=obj.value;
	var regExp= /^[0-9]*$/;

 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);
 			obj.value=str1+str2;
 			return false;
 		}
 	}
 }
 		 function addComma(obj){
						var nStr=obj.value;
						
						  nStr += '';
					    x = nStr.split('.');
					    x1 = x[0];
					    x2 = x.length > 1 ? '.' + x[1] : '';
					    var rgx = /(\d+)(\d{3})/;
					    while (rgx.test(x1)) {
					        x1 = x1.replace(rgx, '$1' + ',' + '$2');
					    }
					   
					    obj.value=x1+x2;
					}	
					
					function removeComma(obj){					
						if (obj.value.indexOf(',') > -1){
							if(obj!=null){
								if(obj.value!=""){
								var cursorPos = doGetCaretPosition(document.getElementById(obj.id));
								//alert('cursorPos'+cursorPos);
								//var valLen = obj.value.length;
								var commaCount = 0;
								
								for(var i=0;cursorPos > i;i++){
								if(obj.value[i] == ","){
								commaCount = commaCount+1;
								//alert('commaCount'+commaCount);
								}
								}			
									obj.value=obj.value.replace(/,/g, "");
									setCursorPosition(document.getElementById(obj.id),cursorPos-commaCount);
								}
							}
						}		
					}			
					
				function doGetCaretPosition (ctrl) {
					var CaretPos = 0;
					// IE Support
					if (document.selection) {
						ctrl.focus ();
						var Sel = document.selection.createRange ();
						Sel.moveStart ('character', -ctrl.value.length);
						CaretPos = Sel.text.length;
					}
					// Firefox support
					else if (ctrl.selectionStart || ctrl.selectionStart == '0')
					CaretPos = ctrl.selectionStart;
					return (CaretPos);
			 }
			 function setCursorPosition(ctrl, pos){
					if(ctrl.setSelectionRange){
						ctrl.focus();
						ctrl.setSelectionRange(pos,pos);
					}
					else if (ctrl.createTextRange) {
						var range = ctrl.createTextRange();
						range.collapse(true);
						range.moveEnd('character', pos);
						range.moveStart('character', pos);
						range.select();
					}
			    }
