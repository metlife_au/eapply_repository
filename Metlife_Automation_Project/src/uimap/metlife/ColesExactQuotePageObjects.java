package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesExactQuotePageObjects extends PageObjectLibrary {
	public static final By lnkNext = getObjectIdentifaction("coles.exactquote.next.link.id");
	public static final By chkAcknowledge = getObjectIdentifaction("coles.exactquote.acknowledge.chk.id");
	public static final By textLoading = getObjectIdentifaction("coles.exactquote.loading.css");
	public static final By hidTermLoadingId = getObjectIdentifaction("coles.exactquote.termLoadingId.id");
	public static final By hidTpdloadingId = getObjectIdentifaction("coles.exactquote.tpdloadingId.id");
	public static final By hidExclusionId = getObjectIdentifaction("coles.exactquote.exclusionId.id");
	public static final By txtFlybuysNumber = getObjectIdentifaction("coles.yourhealthb.flybuys.number.txt.css");
	
	
}
