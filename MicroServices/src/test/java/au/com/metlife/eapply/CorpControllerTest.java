package au.com.metlife.eapply;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.corporate.business.CorpEntityRequest;
import au.com.metlife.eapply.corporate.business.DashboardEmailInput;
import au.com.metlife.eapply.corporate.controller.CorpController;
import au.com.metlife.eapply.repositories.CorporateFundRepo;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.utils.TestUtils;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CorpControllerTest extends TestUtils {

	@InjectMocks
	private CorpController controller;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private  CorporateFundRepo corpFundRepo;

	/*
	 * Created testData to store the string json value from createTestData()
	 */	
	String testData = null;
	
	/*
	 * Created fileName to store the  json file name and pass it to createTestData()
	 */
	String fileName =null;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	
	
	
	/*
	 * This test case is to check the saveCorporateFund() gives expected
	 * reponse when we send valid json data as @requestbody from testSave.json file
	 * and valid userid & fundCode parameter
	 */

	@Test
	public void saveCorporateFundTest() throws Exception {
	    fileName = "testSave.json";
		testData = createTestData(fileName);
		mockMvc.perform(post("/corporate/fund").param("userId", "5").param("fundCode", "S1173")
				.contentType(MediaType.APPLICATION_JSON).content(testData).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.partner").exists())
				.andExpect(jsonPath("$.partner.id").exists())

				.andExpect(jsonPath("$.partner.status").exists())
				.andExpect(jsonPath("$.partner.status").value("Submitted"))
				.andExpect(jsonPath("$.partner.fundCode").exists())
				.andExpect(jsonPath("$.partner.fundCode").value("S1173"))
				.andExpect(jsonPath("$.partner.partnerName").exists())
				.andExpect(jsonPath("$.partner.partnerName").value("Unit Testing Co. Proprietary Limited"))
				.andExpect(jsonPath("$.partner.adminCode").exists())
				.andExpect(jsonPath("$.partner.adminCode").value("S1173"))

				.andExpect(jsonPath("$.partner.broker").exists())
				.andExpect(jsonPath("$.partner.broker.brokerName").exists())
				.andExpect(jsonPath("$.partner.broker.brokerName").value("Michael Giansiracusa"))
				.andExpect(jsonPath("$.partner.broker.brokerEmail").exists())
				.andExpect(jsonPath("$.partner.broker.brokerEmail").value("michaelg@whitbread.com.au"))
				.andExpect(jsonPath("$.partner.broker.brokerCnctNum").exists())
				.andExpect(jsonPath("$.partner.broker.brokerCnctNum").value("386460237"))

				.andExpect(jsonPath("$.partner.employer").exists())
				.andExpect(jsonPath("$.partner.employer.employerCnctNum").exists())
				.andExpect(jsonPath("$.partner.employer.employerCnctNum").value("392457000"))
				.andExpect(jsonPath("$.partner.employer.employerEmail").exists())
				.andExpect(jsonPath("$.partner.employer.employerEmail").value("bill.sant@scalzofoods.com.au"))
				.andExpect(jsonPath("$.partner.employer.employerKeyCnctName").exists())
				.andExpect(jsonPath("$.partner.employer.employerKeyCnctName").value("William Sant"));
	}
	

	/*
	 * This test case is to check the getFundsInformation() gives expected
	 * reponse when we send valid fundCode & fundName
	 */	
	@Test
	public void getCorporateFundTest() throws Exception {

		
			mockMvc.perform(
					MockMvcRequestBuilders.get("/corporate/fund").param("fundName", "Unit Testing Co. Proprietary Limited").param("fundCode", "S1171")
							.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk())
					;

		

	}
	
	/*
	 * This test case is to check the updateCorporateFund() gives expected
	 * reponse when we send valid json data as @requestbody from testUpdate.json file
	 * and valid userid & fundCode parameter
	 */

	
	@Test
	public void updateCorporateFundTest() throws Exception {
	    fileName = "testUpdate.json";
		testData = createTestData(fileName);
		mockMvc.perform(put("/corporate/fund").param("userId", "5").param("fundCode", "S1171")
				.contentType(MediaType.APPLICATION_JSON).content(testData).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}
	
	/*
	 * This test case is to check the updateCorporateFund() gives expected
	 * reponse when we send invalid json data as @requestbody from testUpdate.json file
	 * and invalid userid & fundCode parameter
	 */

	@Test
	public void updateCorporateFundInvalidRequestTest() throws Exception {
		mockMvc.perform(put("/corporate/fund").param("userId", "5").param("fundCode", "1234567")
				.contentType(MediaType.APPLICATION_JSON).content("").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	/*
	 * This test case is to check the updateCorporateFund() gives expected
	 * reponse when we send valid json data as @requestbody from testUpdate.json file
	 * and invalid userid & fundCode parameter
	 */

	@Test
	public void updateCorporateFundInvalidDataTest() throws Exception {
		fileName = "testInvalidUpdate.json";
		testData = createTestData(fileName);
		mockMvc.perform(put("/corporate/fund").param("userId", "5").param("fundCode", "1234567")
				.contentType(MediaType.APPLICATION_JSON).content(testData).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.partner").doesNotExist());
	}
	
	
	/*
	 * This test case is to check the getFundsInformation() gives expected
	 * reponse when we send invalid fundCode & fundName
	 */

	@Test
	public void getCorporateFundInvalidTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/corporate/fund?fundCode=12345&fundName=123456&userId=123456")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.partner.fundCode").value("12345"))
				.andExpect(jsonPath("$.partner.partnerName").value("123456"));
	}
	
	/*
	 * to clear the saved data from database 
	 */
	
	 @Test
	 public void truncateTestData() throws Exception {
			String fileName = "testSave.json";
			CorpEntityRequest CorpData = createTestDataObject(fileName);
			CorporateFund corpFund = corpFundRepo.findByFundCode(CorpData.getPartner().getFundCode());
			corpFundRepo.delete(corpFund);
		}
	
	 /*
		 * This test case is to check the getFundsInformationwithCategory() gives expected
		 * reponse when  valid fundCode & Category is passed
		 */	
		@Test
		public void getCorporateFundCategoryTest() throws Exception {

			
				mockMvc.perform(
						MockMvcRequestBuilders.get("/corporate/fund/cover").param("fundCode", "A145").param("category", "1")
								.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk()).andExpect(jsonPath("$.partner").exists())
						.andExpect(jsonPath("$.partner.id").exists())
						.andExpect(jsonPath("$.partner.status").exists())
						.andExpect(jsonPath("$.partner.status").value("Submitted"))
						.andExpect(jsonPath("$.partner.fundCode").exists())
						.andExpect(jsonPath("$.partner.fundCode").value("A145"))
						.andExpect(jsonPath("$.partner.partnerName").exists())
						.andExpect(jsonPath("$.partner.adminCode").exists())

						.andExpect(jsonPath("$.partner.broker").exists())
						.andExpect(jsonPath("$.partner.broker.brokerName").exists())
						.andExpect(jsonPath("$.partner.broker.brokerEmail").exists())
						.andExpect(jsonPath("$.partner.broker.brokerCnctNum").exists())

						.andExpect(jsonPath("$.partner.employer").exists())
						.andExpect(jsonPath("$.partner.employer.employerCnctNum").exists())
						.andExpect(jsonPath("$.partner.employer.employerEmail").exists())
						.andExpect(jsonPath("$.partner.employer.employerKeyCnctName").exists())
						;

			

		}
		
		 /*
		 * This test case is to check the getmemberDetails on Corporate Email Login gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test
		public void getCorporateMemberDetailsEmailTest() throws Exception {

			
					mockMvc.perform(
						MockMvcRequestBuilders.get("/corporate/login").param("inputData", "19e40e289dc3dd23fdee0e8df20490b4").param("inputIdentifier", "fc5e1bcbf5c8572de2c20fe3df61dc53d3e1cba14c1b4815ff1ba3f1bd96d982ad776f8ba0c47901").param("sdata", "b10917500c87ded8ed66a45672310d77f77002e5ecf054f9d0c52a008cf11ef42dc87fa94f9d60de").param("c", "")
								.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andReturn()
				
						;
		}
		
		
		
		 /*
		 * This test case is to check the getmemberDetails on Corporate Eview Login gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test
		public void getCorporateMemberDetailsEviewTest() throws Exception {

			
			MvcResult result=	mockMvc.perform(
						MockMvcRequestBuilders.get("/corporate/login").param("inputData", "").param("inputIdentifier", "").param("sdata", "").param("c", "b9a2f9a313bc00bb")
								.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andReturn()
				
						;
				String details=result.getResponse().getContentAsString();
				assertEquals(details, details);
		}
		
		/*
		 * This test case is to check the DashboardFundDetails gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test
		public void getCorporateDashboardMemberDetailsTest() throws Exception {

			
				mockMvc.perform(
						MockMvcRequestBuilders.get("/corporate/memberdetails").param("fundCode", "A145").param("status", "")
						.param("firstName", "").param("lastName", "").param("applicationNumber", "").param("fromDate", "").param("toDate", "")
								.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());
				
				mockMvc.perform(
						MockMvcRequestBuilders.get("/corporate/memberdetails").param("fundCode", "A145").param("status", "Pending")
						.param("firstName", "").param("lastName", "").param("applicationNumber", "").param("fromDate", "").param("toDate", "")
								.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());

				mockMvc.perform(
						MockMvcRequestBuilders.get("/corporate/memberdetails").param("fundCode", "A145").param("status", "Submitted")
						.param("firstName", "").param("lastName", "").param("applicationNumber", "").param("fromDate", "").param("toDate", "")
								.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());

		}
		
		/*
		 * This test case is to update the Application Status gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test
		public void CorporateDashboardUpdateStatusTest() throws Exception {
			fileName = "testcorpUpdate.json";
			JSONParser parser = new JSONParser();
			Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
			JSONObject jsonObject = (JSONObject) object;
			System.out.println(jsonObject.toString());
				mockMvc.perform(
						MockMvcRequestBuilders.put("/corporate/update")
						.contentType(MediaType.APPLICATION_JSON).content(jsonObject.toString()).accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk());
				
				
		}
		
		/*
		 * This test case is to Resend the Application through Dashboard gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test(expected = NestedServletException.class)
		public void CorporateResendEmailTest() throws Exception {
			fileName = "corpResendEmail.json";
			String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
			ObjectMapper mapper = new ObjectMapper();
			File file = new ClassPathResource(fileName).getFile();
			DashboardEmailInput emailInput = mapper.readValue(file, DashboardEmailInput.class);
			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(emailInput);
				mockMvc.perform(
						MockMvcRequestBuilders.post("/corporate/email").header("Authorization", authId)
						.contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk());
				
				
		}
		
		 /*
		 * This test case is to check the decrypted fund code for onboard,dashboard and eview screen gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test
		public void getDecryptedFundCode() throws Exception {
		    fileName = "testSave.json";
			testData = createTestData(fileName);
			mockMvc.perform(MockMvcRequestBuilders.post("/corporate/decryptFundCode").param("fundcode", "b9a2f9a313bc00bb")
					.contentType(MediaType.APPLICATION_JSON).content(testData).accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						;
		}
		
		
		 /*
		 * This test case is to expire the application gives expected
		 * reponse when  valid parameters are passed
		 */	
		@Test
		public void expireCorporateApplicationTest() throws Exception {
			mockMvc.perform(MockMvcRequestBuilders.post("/corporate/expireApplication").param("applicationNumber", "1").param("fundCode", "A145")
					.accept(MediaType.APPLICATION_JSON))
						.andDo(print())
						.andExpect(status().isOk())
						;
		}
}
