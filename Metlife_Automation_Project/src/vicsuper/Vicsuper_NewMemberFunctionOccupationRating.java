package vicsuper;

/*public class Vics {

}
*/

import java.awt.Robot;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class Vicsuper_NewMemberFunctionOccupationRating extends MasterPage {
	WebDriverUtil driverUtil = null;
	//JavascriptExecutor jse = (JavascriptExecutor)driver.getWebDriver() ;
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	WebDriverWait wait = new WebDriverWait(driver, 20);
	public Vicsuper_NewMemberFunctionOccupationRating(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}


	public Vicsuper_NewMemberFunctionOccupationRating NewMemberFunctionOccupationRating() {

		NewMemberFunction();
		HealthandLifeStyle();
		confirmation();

		return new Vicsuper_NewMemberFunctionOccupationRating(scriptHelper);
	}



/*	public Vicsuper_NewMemberFunctionOccupationRating NewMember_vicsuper() {
	String ConfirmationPage = dataTable.getData("NewMemFunOccupationRating", "ConfirmationPage");
	System.out.println(ConfirmationPage);
		NewMemberFunction();
		HealthandLifeStyle();
		if (ConfirmationPage.equalsIgnoreCase("Yes")) {
			confirmation();
		}
		else {
			report.updateTestLog("Confirmation Page", "Confirmation Page is not displayed",
					Status.SCREENSHOT);
		}

		feedbackPopup();

		return new Vicsuper_NewMemberFunctionOccupationRating(scriptHelper);
	}
	*/


	public void NewMemberFunction() {
		System.out.println("Change");
		String EmailId = dataTable.getData("NewMemFunOccupationRating", "EmailId");
		String TypeofContact = dataTable.getData("NewMemFunOccupationRating", "TypeofContact");
		String ContactNumber = dataTable.getData("NewMemFunOccupationRating", "ContactNumber");
		String TimeofContact = dataTable.getData("NewMemFunOccupationRating", "TimeofContact");
		String Gender = dataTable.getData("NewMemFunOccupationRating", "Gender");
		System.out.println(".....................");
		String FourteenHoursWork = dataTable.getData("NewMemFunOccupationRating", "FourteenHoursWork");
		String Citizen = dataTable.getData("NewMemFunOccupationRating", "Citizen");
		String EducationalInstitution = dataTable.getData("NewMemFunOccupationRating", "EducationalInstitution");
		String EducationalDuties = dataTable.getData("NewMemFunOccupationRating", "EducationalDuties");
		String TertiaryQual = dataTable.getData("NewMemFunOccupationRating", "TertiaryQual");
		String AnnualSalary = dataTable.getData("NewMemFunOccupationRating", "AnnualSalary");
		/*String CostType = dataTable.getData("NewMemFunOccupationRating", "CostType");
		String DeathYN = dataTable.getData("NewMemFunOccupationRating", "DeathYN");
		String DeathUnits = dataTable.getData("NewMemFunOccupationRating", "DeathUnits");
		String TPDYN = dataTable.getData("NewMemFunOccupationRating", "TPDYN");
		String TPDUnits = dataTable.getData("NewMemFunOccupationRating", "TPDUnits");
		String IPYN = dataTable.getData("NewMemFunOccupationRating", "IPYN");
		String IPUnits = dataTable.getData("NewMemFunOccupationRating", "IPUnits");
		String Insure85Percent = dataTable.getData("NewMemFunOccupationRating", "Insure85Percent");
		String WaitingPeriod = dataTable.getData("NewMemFunOccupationRating", "WaitingPeriod");*/
		String BenefitPeriod = dataTable.getData("NewMemFunOccupationRating", "BenefitPeriod");

		String ExistingDeath = dataTable.getData("NewMemFunOccupationRating", "ExistingDeath");
		String ExistingTPD = dataTable.getData("NewMemFunOccupationRating", "ExistingTPD");
		String ExistingIP = dataTable.getData("NewMemFunOccupationRating", "ExistingIP");

		String XML = dataTable.getData("VicSuper_Login", "XML");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;

		String occRating1;
		String occRating2;
		String occRating3;



		String[] value;

		//To get firstName
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if ( !(occRating1.equals(ExistingDeath))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death",
					Status.FAIL);
		}
		if ( !(occRating2.equals(ExistingTPD))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD",
					Status.FAIL);
		}
		if ( !(occRating3.equals(ExistingIP))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP",
					Status.FAIL);
		}

		try {

			Robot roboobj = new Robot();


			// ****Click on Change Your Insurance Cover Link*****\\
			sleep(500);




				WebElement splOff = driver.findElement(By.xpath("//h4[text()='Special offer for new VicSuper EmployeeSaver members']"));
				jse.executeScript("arguments[0].scrollIntoView(true);", splOff);
				 sleep(500);

				report.updateTestLog("SPECIAL OFFER FOR NEW MEMBERS", "Special offer for new members is present",
						Status.PASS);

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Special offer for new VicSuper EmployeeSaver members']")))
				.click();

		report.updateTestLog("SPECIAL OFFER FOR NEW MEMBERS", "Special offer for new members link is clicked",
				Status.PASS);




			sleep(5000);

// -----------------------------------------------------Personal Details Page------------------------------------------------------------------------//


			// *****Agree to Duty of disclosure*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*")))
					.click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			// *****Agree to Privacy Statement*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*")))
					.click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			sleep(200);

// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
			sleep(500);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(By.xpath("//*[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId")))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {
				driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]")).click();
				sleep(400);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			} else {
				driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]")).click();
				sleep(400);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			}

			/*
			 * // ***********Select the Gender******************\\
			 *
			 * if (Gender.equalsIgnoreCase("Male")) {
			 * driver.findElement(By.xpath(".//input[@value='Male']/parent::*"))
			 * .click(); sleep(250); report.updateTestLog("Gender",
			 * "Gender selected is: " + Gender, Status.PASS); } else {
			 * driver.findElement(By.xpath(".//input[@value='Female']/parent::*"
			 * )).click(); sleep(250); report.updateTestLog("Gender",
			 * "Gender selected is: " + Gender, Status.PASS); }
			 *
			 */

			//*****Click on Continue*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
			sleep(3000);

// -----------------------------------------------------Occupation------------------------------------------------------------------------//

			// *****Select the 14 Hours Question*******\\

			if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@name='fifteenHrsQuestion ']/parent::*"))).click(); // Dhipika
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@name='fifteenHrsQuestion']/parent::*"))).click(); // Dhipika
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
			}

			// *****Resident of Australia****\\

			if (Citizen.equalsIgnoreCase("Yes")) {
				driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
			} else {
				driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
			}

			/*
			// *****Industry********\\

			sleep(500);
			Select Industry = new Select(driver.findElement(By.name("industry")));
			Industry.selectByVisibleText(IndustryType);
			sleep(500);
			report.updateTestLog("Industry", "Industry Selected is: " + IndustryType, Status.PASS);
			sleep(1500);

			// *****Occupation*****\\

			Select Occupation = new Select(driver.findElement(By.name("occupation")));
			Occupation.selectByVisibleText(OccupationType);
			sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: " + OccupationType, Status.PASS);
			sleep(1000);
*/

			// *****Are the duties of your regular occupation limited to
			// either:*****\\

			if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

				// ***Select duties which are undertaken within an Office
				// Environment?\\
				if (EducationalInstitution.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]")))
							.click(); // Dhipika
					report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]")))
							.click(); // Dhipika
					report.updateTestLog("Educational institution", "Selected No", Status.PASS);
					sleep(1000);
				}

				// *****Educational duties performed within a school or other
				// educational institution******\\
				if (EducationalDuties.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]")))
							.click(); // Dhipika
					report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]")))
							.click(); // Dhipika
					report.updateTestLog("Educational duties", "Selected No", Status.PASS);
					sleep(1000);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

			}

			// Do you hold a tertiary qualification or work in a management role
			if (driver.getPageSource().contains("Do you:")) {

				// *****Do You Hold a Tertiary Qualification******\\
				if (TertiaryQual.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[1]"))).click(); // Dhipika
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[2]"))).click(); // Dhipika
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
					sleep(1000);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Do you hold a tertiary qualification or work in a management role is not present",
						Status.FAIL);
			}

			// *****What is your annual Salary******

			wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary); // Dhipika
			sleep(1000);
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);

			//*****Click on Continue*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
			sleep(3000);



// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//

		/*
			// ******Click on Cost of Insurance as*******\\
			sleep(1000);
			Select COSTTYPE = new Select(driver.findElement(By.name("premFreq")));
			COSTTYPE.selectByVisibleText(CostType);
			report.updateTestLog("Cost of Insurance", "Cost frequency selected is: " + CostType, Status.PASS);
			sleep(100);

			// *******Death Cover Section******\\
			if (DeathYN.equalsIgnoreCase("Yes")) {

				// To Select - Extra cover required
				if (DeathUnits.equalsIgnoreCase("0")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[1]"))).click(); // Dhipika
					report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
					sleep(1000);



				}

				else if (DeathUnits.equalsIgnoreCase("1")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[2]"))).click(); // Dhipika
					report.updateTestLog("Death Extra Cover", "Death Extra Cover - 1 Unit is selected", Status.PASS);
					sleep(1000);

				} else if (DeathUnits.equalsIgnoreCase("2")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[3]"))).click(); // Dhipika
					report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
					sleep(1000);

				} else {
					report.updateTestLog("Data Table", "Invalid input for DeathUnits in Data Sheet. Please enter 0/1/2",
							Status.FAIL);
				}

			}

			else if (DeathYN.equalsIgnoreCase("No")) {

			}
			else {
				report.updateTestLog("Data Table", "Invalid input for DeathYN in Data Sheet", Status.FAIL);
			}
			// *******TPD Cover Section******\\
			sleep(800);

			if (TPDYN.equalsIgnoreCase("Yes")) {

				// To Select - Extra cover required
				if (TPDUnits.equalsIgnoreCase("0")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[1]"))).click(); // Dhipika
					report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
					sleep(1000);

				}

				else if (TPDUnits.equalsIgnoreCase("1")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[2]"))).click(); // Dhipika
					report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 1 Unit is selected", Status.PASS);
					sleep(1000);

				} else if (TPDUnits.equalsIgnoreCase("2")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[3]"))).click(); // Dhipika
					report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 2 Unit is selected", Status.PASS);
					sleep(1000);

				} else {
					report.updateTestLog("Data Table", "Invalid input for TPDUnits in Data Sheet. Please enter 0/1/2",
							Status.FAIL);
				}

				if ((DeathYN.equalsIgnoreCase("Yes"))) {
					Integer DU = Integer.parseInt(DeathUnits);
					Integer TPDU = Integer.parseInt(TPDUnits);

					if (TPDU>DU) {


					if (driver.getPageSource().contains("Your TPD cover amount should not be greater than Death cover amount.")) {
						report.updateTestLog("TPD Validation",
								"Error Message 'Your TPD cover amount should not be greater than Death cover amount.' is displayed",
								Status.PASS);

						// To Select None value for Death
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[3]")))
								.click(); // Dhipika
						report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
						sleep(1000);

					} else {
						report.updateTestLog("TPD Validation",
								"Error Message 'Your TPD cover amount should not be greater than Death cover amount.' is not displayed",
								Status.FAIL);
					}

					}

				}
				else if ((DeathYN.equalsIgnoreCase("No"))) {

					if (driver.getPageSource().contains("You cannot apply for TPD cover without Death Cover.")) {
						report.updateTestLog("TPD Validation",
								"Error Message 'You cannot apply for TPD cover without Death Cover.' is displayed",
								Status.PASS);

						// To Select None value for Death
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[3]")))
								.click(); // Dhipika
						report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
						sleep(1000);

					} else {
						report.updateTestLog("TPD Validation",
								"Error Message 'You cannot apply for TPD cover without Death Cover.' is not displayed",
								Status.FAIL);
					}
				}
			}
			else if (TPDYN.equalsIgnoreCase("No")) {

			}
			else {
				report.updateTestLog("Data Table", "Invalid input for TPDYN in Data Sheet", Status.FAIL);
			}


			// *******IP Cover section********\\
			sleep(500);
			if (IPYN.equalsIgnoreCase("Yes")) {

				// *****Insure 85% of My Salary******\\

				if (Insure85Percent.equalsIgnoreCase("Yes")) {

					WebElement noUnitIP = driver
							.findElement(By.xpath("(//input[@name='IPRequireCover']/parent::*)[1]"));
					WebElement oneUnitIP = driver
							.findElement(By.xpath("(//input[@name='IPRequireCover']/parent::*)[2]/parent::*"));
					WebElement twoUnitIP = driver
							.findElement(By.xpath("(//input[@name='IPRequireCover']/parent::*)[3]/parent::*"));

					String oneUnitIPatt = oneUnitIP.getAttribute("disabled");
					String twoUnitIPatt = twoUnitIP.getAttribute("disabled");
					System.out.println("ATT:"+oneUnitIPatt);
					System.out.println("ATT:"+twoUnitIPatt);

					// To vlaidate none unit
					if (noUnitIP.isEnabled()) {
						report.updateTestLog("IP Extra Cover",
								"IP Unit 'None' is enabled when IP amount is greater than 85% of the annual salary.",
								Status.PASS);
					} else {
						report.updateTestLog("IP Extra Cover",
								"IP Unit 'None' is not enabled when IP amount is greater than 85% of the annual salary.",
								Status.FAIL);
					}
					// To vlaidate 1 unit
					if (oneUnitIPatt.equalsIgnoreCase("true")) {

						report.updateTestLog("IP Extra Cover",
								"IP Unit '1 Unit' is not enabled when IP amount is greater than 85% of the annual salary.",
								Status.PASS);
					} else {
						report.updateTestLog("IP Extra Cover",
								"IP Unit '1 Unit' is enabled when IP amount is greater than 85% of the annual salary.",
								Status.FAIL);
					}
					// To vlaidate 2 unit
					if (twoUnitIPatt.equalsIgnoreCase("true")) {
						report.updateTestLog("IP Extra Cover",
								"IP Unit '2 Unit' is not enabled when IP amount is greater than 85% of the annual salary.",
								Status.PASS);
					} else {
						report.updateTestLog("IP Extra Cover",
								"IP Unit '2 Unit' is enabled when IP amount is greater than 85% of the annual salary.",
								Status.FAIL);
					}

				}

					Select waitingperiod = new Select(
							driver.findElement(By.xpath("//select[@ng-model='waitingPeriodAddnl']")));
					waitingperiod.selectByVisibleText(WaitingPeriod);
					report.updateTestLog("Waiting Period", "Waiting Period of: " + WaitingPeriod + " is Selected",
							Status.PASS);
					sleep(2000);

					Select benefitperiod = new Select(
							driver.findElement(By.xpath("//select[@ng-model='benefitPeriodAddnl']")));
					benefitperiod.selectByVisibleText(BenefitPeriod);
					report.updateTestLog("Benefit Period", "Benefit Period: " + BenefitPeriod + " is Selected",
							Status.PASS);
					sleep(2000);


					if (BenefitPeriod.equalsIgnoreCase("5 Years")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("//*[text()='The member is not a casual or contractor employee']"))).click();
						report.updateTestLog("Casual or contractor employee", "The member is not a casual or contractor employee is selected", Status.PASS);
						sleep(3000);

					}

				// To Select - Extra cover required
				if (IPUnits.equalsIgnoreCase("0")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[1]"))).click(); // Dhipika
					report.updateTestLog("IP Extra Cover", "IP Extra Cover - None is selected", Status.PASS);
					sleep(1000);

				}

				else if (IPUnits.equalsIgnoreCase("1")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[2]"))).click(); // Dhipika
					report.updateTestLog("IP Extra Cover", "IP Extra Cover - 1 Unit is selected", Status.PASS);
					sleep(1000);

				} else if (IPUnits.equalsIgnoreCase("2")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[3]"))).click(); // Dhipika
					report.updateTestLog("IP Extra Cover", "IP Extra Cover - 2 Unit is selected", Status.PASS);
					sleep(1000);

				} else {
					report.updateTestLog("Data Table", "Invalid input for IPUnits in Data Sheet. Please enter 0/1/2",
							Status.FAIL);
				}

			}
			else if (IPYN.equalsIgnoreCase("No")) {

			}
			else {
				report.updateTestLog("Data Table", "Invalid input for IPYN in Data Sheet", Status.FAIL);
			}

			if ( !(BenefitPeriod.equalsIgnoreCase("2 Years")) ) {
				//sleep(2000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@ng-model='ownOccuptionIp']/parent::*)[1]")));
				if (driver.getPageSource().contains("Apply Own Occupation rating?")) {
					if (ExistingIP.equalsIgnoreCase("Own Occupation")) {
						WebElement objMorning = driver.findElement(By.xpath("(.//input[@ng-model='ownOccuptionIp']/parent::*)[1]"));
						 fieldValidation("WebButton",objMorning,"Yes","Own Occupation");
					}

					else {
						WebElement objMorning = driver.findElement(By.xpath("(.//input[@ng-model='ownOccuptionIp']/parent::*)[2]"));
						 fieldValidation("WebButton",objMorning,"No","Own Occupation");
					}

					sleep(2000);
					}
				else {
					report.updateTestLog("Own Occupation", "Own Occupation Question is not displayed", Status.FAIL);
				}
			}*/

			//To select Death , TPD, IP - no
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[1]"))).click(); // Dhipika
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
			sleep(2000);

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[1]"))).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
			sleep(3500);

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[1]"))).click(); // Dhipika
			report.updateTestLog("IP Extra Cover", "IP Extra Cover - None is selected", Status.PASS);
			sleep(2000);

			//To click on calculate
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@ng-click='formOneSubmit(coverCalculatorForm);']")))
					.click();
			sleep(2000);
			report.updateTestLog("Calculate", "Calculate button is clicked", Status.PASS);

			//To click on continue
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@ng-click='goToAura()']")))
					.click();
			report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);

			sleep(1000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}

	public void HealthandLifeStyle() {
		sleep(3000);
		String Restricted_Ilness = dataTable.getData("NewMemFunOccupationRating", "Restricted_Ilness");
		String PreviousClaim = dataTable.getData("NewMemFunOccupationRating", "PreviousClaim");
		String Restricted_Work = dataTable.getData("NewMemFunOccupationRating", "Restricted_Work");
		String Diagnosed_Illness = dataTable.getData("NewMemFunOccupationRating", "Diagnosed_Illness");
		String MedicalTreatment = dataTable.getData("NewMemFunOccupationRating", "MedicalTreatment");
		String Previous_Insurance = dataTable.getData("NewMemFunOccupationRating", "Previous_Insurance");

		try {

			// *****Select the Restricted_Ilness Question*******\\

			if (Restricted_Ilness.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you restricted, due to illness or injury from carrying out any of the identifiable duties of your current and normal occupation on a full time basis (even if you are not currently working on a full time basis). Full time basis is considered to be at least 35 hours per week.']/../following-sibling::div/div/div[1]/label)[1]")))
						.click();
				sleep(2000);
				report.updateTestLog("Restricted due to ilness Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you restricted, due to illness or injury from carrying out any of the identifiable duties of your current and normal occupation on a full time basis (even if you are not currently working on a full time basis). Full time basis is considered to be at least 35 hours per week.']/../following-sibling::div/div/div[1]/label)[2]")))
						.click();
				sleep(2000);
				report.updateTestLog("Restricted due to ilness Question", "Selected No", Status.PASS);
			}

			// *****Select the PreviousClaim Question*******\\

			if (PreviousClaim.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Are you contemplating or have you ever made a claim')]/../following-sibling::div/div/div[1]/label)[1]")))
						.click();
				sleep(1000);
				report.updateTestLog("Previous Claim Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Are you contemplating or have you ever made a claim')]/../following-sibling::div/div/div[1]/label)[2]")))
						.click();
				sleep(1000);
				report.updateTestLog("Previous Claim Question", "Selected No", Status.PASS);
			}

			// *****Select the Restricted_Work Question*******\\

			if (Restricted_Work.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you been restricted from work or unable to perform any of your regular duties for more than ten consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?']/../following-sibling::div/div/div[1]/label)[1]")))
						.click();
				sleep(1000);
				report.updateTestLog("Restricted Work Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you been restricted from work or unable to perform any of your regular duties for more than ten consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?']/../following-sibling::div/div/div[1]/label)[2]")))
						.click();
				sleep(1000);
				report.updateTestLog("Restricted Work Question", "Selected No", Status.PASS);
			}

			// *****Select the Diagnosed_Illness Question*******\\

			if (Diagnosed_Illness.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Have you been diagnosed with an illness')]/../following-sibling::div/div/div[1]/label)[1]")))
						.click();
				sleep(1000);
				report.updateTestLog("Diagnosed with illness Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Have you been diagnosed with an illness')]/../following-sibling::div/div/div[1]/label)[2]")))
						.click();
				sleep(1000);
				report.updateTestLog("Diagnosed with illness Question", "Selected No", Status.PASS);
			}

			// *****Select the MedicalTreatment Question*******\\

			if (MedicalTreatment.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you currently contemplating any medical treatment or advice for any illness or injury for which you have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?']/../following-sibling::div/div/div[1]/label)[1]")))
						.click();
				sleep(1000);
				report.updateTestLog("Medical Treatment Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you currently contemplating any medical treatment or advice for any illness or injury for which you have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?']/../following-sibling::div/div/div[1]/label)[2]")))
						.click();
				sleep(1000);
				report.updateTestLog("Medical Treatment Question", "Selected No", Status.PASS);
			}

			jse.executeScript("window.scrollTo(0, document.body.scrollHeight);");


			// *****Select the Previous_Insurance Question*******\\

			if (Previous_Insurance.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you had an application for Life, TPD, Trauma, Income Protection insurance declined, deferred or accepted with exclusions or loadings by an insurer?']/../following-sibling::div/div/div[1]/label)[1]")))
						.click();
				sleep(1000);
				report.updateTestLog("Previous Insurance Question", "Selected Yes", Status.PASS);
			} else {
				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you had an application for Life, TPD, Trauma, Income Protection insurance declined, deferred or accepted with exclusions or loadings by an insurer?']/../following-sibling::div/div/div[1]/label)[2]")))
						.click();
				sleep(1000);
				report.updateTestLog("Previous Insurance Question", "Selected No", Status.PASS);
			}

			//To click on Continue
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@ng-click='proceedNext()']")))
					.click();
			report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);


			sleep(1000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void confirmation() {

			String OutcomeForDeath = dataTable.getData("NewMemFunOccupationRating", "OutcomeForDeath");
			String OutcomeForTPD = dataTable.getData("NewMemFunOccupationRating", "OutcomeForTPD");
			String OutcomeForIP = dataTable.getData("NewMemFunOccupationRating", "OutcomeForIP");
			//String OutcomeForIP_OwnOccupationNo = dataTable.getData("ChangecoverOccupationRating", "OutcomeForIP_OwnOccupationNo");
			//String OwnOccupation = dataTable.getData("ChangecoverOccupationRating", "OwnOccupation");

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()='Occupational Category '])[1]//following::p")));
WebElement OccupationalCategory_Death = driver
					.findElement(By.xpath("(//*[text()='Occupational Category '])[1]//following::p"));
			jse.executeScript("arguments[0].scrollIntoView(true);", OccupationalCategory_Death);
			fieldValidation("WebElement", OccupationalCategory_Death, OutcomeForDeath, "OccupationalCategory - Death");

			WebElement OccupationalCategory_tpd = driver
					.findElement(By.xpath("(//*[text()='Occupational Category '])[2]//following::p"));
			fieldValidation("WebElement", OccupationalCategory_tpd, OutcomeForTPD, "OccupationalCategory - TPD");


			WebElement OccupationalCategory_ip = driver
					.findElement(By.xpath("(//*[text()='Occupational Category '])[3]//following::p"));
			fieldValidation("WebElement", OccupationalCategory_ip, OutcomeForIP, "OccupationalCategory - IP");

			return;


	}







}
