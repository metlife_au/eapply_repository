/**
 * 
 */
package pages.metlife.NEW;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.INGD_TransferYourCoverPageObjects;

/**
 * @author Hemnath
 * 
 */
public class INGD_TransferYourCoverPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public INGD_TransferYourCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public INGD_TransferYourCoverPage enterINGDtransfercover() {
		transfercoverpersonaldetails();
		HealthandLifestyle();
		DeclarationandConfirmation();
		
		
		return new INGD_TransferYourCoverPage(scriptHelper);
	} 
	
	
	private void transfercoverpersonaldetails() {
		
		String FifteenHoursWork = dataTable.getData("INGD_TransferYourCover", "FifteenHoursWork");
		String IndustryType = dataTable.getData("INGD_TransferYourCover", "IndustryType");
		String OccupationType = dataTable.getData("INGD_TransferYourCover", "OccupationType");
		String AnnualSalary = dataTable.getData("INGD_TransferYourCover", "AnnualSalary");
		String DeathAmount = dataTable.getData("INGD_TransferYourCover", "DeathAmount");
		String TPDAmount = dataTable.getData("INGD_TransferYourCover", "TPDAmount");
		String IPAmount = dataTable.getData("INGD_TransferYourCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("INGD_TransferYourCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("INGD_TransferYourCover", "BenefitPeriod");
	
		
		
		
		try{
			
			driver.manage().window().maximize();
			
			//****Click on Transfer Your Cover Link*****\\
			
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.TransferYourCoverLink), "Click", "Transfer Your Cover Link");
			
			//*****Select the 15 Hours Question*******\\
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){
			
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.FifteenHoursYes), "Click", "15 Hours a Week-Yes");
			
			}
			else{
			
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");
			
			}
			

			//*****Click on Industry********\\
			
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.Industry), "DropDownSelect", "Industry",IndustryType);
			sleep(800);
			
		
			//*****Click On  Occupation*****\\
			
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.Occupation), "DropDownSelect", "Occupation",OccupationType);
        
          
            //*****Annual Salary*****\\
			sleep(500);
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);
        
          
			//****Enter the Death Cover amount*****\\
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.DeathAmount), "SET", "Transfer Death Amount",DeathAmount);
            
            //*****Enter the TPD Cover amount*****\\
            
          
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.TPDAmount), "SET", "Transfer TPD Amount",TPDAmount);
            
           
            //******Enter the IP Cover amount******
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.IPAmount), "SET", "Transfer IP Amount",IPAmount);
            
           
            //*****Waiting period******\\
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.WaitingPeriod), "DropDownSelect", "Waiting Period",WaitingPeriod);
            
           
            //******Benefit period******\\
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.BenefitPeriod), "DropDownSelect", "Benefit Period",BenefitPeriod);
            
           
            //*****Calculate Quote******\\
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");
            
         
		
            //****Fetching the Death Cost*****\\
            
            String DeathPremium=waitForClickableElement(INGD_TransferYourCoverPageObjects.FetchDeathAmt).getText();
            report.updateTestLog("Death Cost", "Estimated Death prmeium is: "+DeathPremium , Status.SCREENSHOT);
            
          
            //*****Fetching the TPD cost******\\
            
            String TPDPremium=waitForClickableElement(INGD_TransferYourCoverPageObjects.FetchTPDAmt).getText();
            report.updateTestLog("Death Cost", "Estimated TPD prmeium is: "+TPDPremium , Status.SCREENSHOT);
            
           
            //****Fetching the IP cost*****\\
            
            String IPPremium=waitForClickableElement(INGD_TransferYourCoverPageObjects.FetchIPAmt).getText();
            report.updateTestLog("Death Cost", "Estimated IP prmeium is: "+IPPremium , Status.SCREENSHOT);
            
           
            //*****Transfer Cover Check box****\\
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.TransferCheckbox), "Click", "Transfer Cover Checkbox");
            sleep(500);
           
            //*****Continue Button*****
            
            tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.ContinueafterCoverdetails), "Click", "Continue");
            sleep(2000);
			
	
			report.updateTestLog("Cover Details Page", "Sucessfully automated cover details section", Status.PASS);

			
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
	
private void HealthandLifestyle() {
		
	String Restrictedillness = dataTable.getData("INGD_TransferYourCover", "Restrictedillness");
	String LodgeTPDClaim = dataTable.getData("INGD_TransferYourCover", "LodgeTPDClaim");
	String Diagnosedillness = dataTable.getData("INGD_TransferYourCover", "Diagnosedillness");
	String PremFundLoading = dataTable.getData("INGD_TransferYourCover", "Diagnosedillness");
		
		
		
		try{
			
			//******Are you restricted, due to injury or illness, from carrying out the identifiable duties 
			//of your current and normal occupation on a full-time basis (even if you are not currently working
			//on a full-time basis)? Full-time basis is considered to be at least 30 hours per week.
			
			if(Restrictedillness.equalsIgnoreCase("Yes")){
			
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.RestrictedillnessYes), "Click", "Restrictedillness-Yes");	
			}
			else{
				
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.RestrictedillnessNo), "Click", "Restrictedillness-No");	
			}
			
			//*****Have you been paid, or are you eligible to be paid, or have you lodged a claim for a TPD benefit from 
			//any super fund (including ING DIRECT Living Super) or life insurance policy?
			
			if(LodgeTPDClaim.equalsIgnoreCase("Yes")){
				
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.LodgeTPDClaimYes), "Click", "LodgeTPDClaim-Yes");	
				}
			else{
					
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.LodgeTPDClaimNo), "Click", "LodgeTPDClaim-No");	
				}
			
			
			
			//******Have you been diagnosed with an illness that reduces your life expectancy to less than 12 months
			//from the date of this application?
			
			if(Diagnosedillness.equalsIgnoreCase("Yes")){
				
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.DiagnosedillnessYes), "Click", "Diagnosedillness-Yes");	
					}
				else{
						
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.DiagnosedillnessNo), "Click", "Diagnosedillness-No");	
					}
			
			
			//*****Is your cover under the former insurer subject to any premium loadings and/or exclusions, including but not 
			//limited to pre-existing condition exclusions, or restrictions in regards to medical or other conditions?
			

			if(PremFundLoading.equalsIgnoreCase("Yes")){
				
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.PremFundLoadingYes), "Click", "PremFundLoading-Yes");	
					}
				else{
						
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.PremFundLoadingNo), "Click", "PremFundLoading-No");	
					}	
			
			//******Click on Submit Button******\\
			
			tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.SubmitHealthPage), "Click", "Submit-Health Page");	
			
			
			report.updateTestLog("Health and lifestyle Page", "Sucessfully automated Health and Lifestyle section", Status.PASS);

			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

private void DeclarationandConfirmation() {
	
	String Restrictedillness = dataTable.getData("INGD_TransferYourCover", "Restrictedillness");
	String LodgeTPDClaim = dataTable.getData("INGD_TransferYourCover", "LodgeTPDClaim");
	String Diagnosedillness = dataTable.getData("INGD_TransferYourCover", "Diagnosedillness");
	String PremFundLoading = dataTable.getData("INGD_TransferYourCover", "PremFundLoading");
	String PreferredContact = dataTable.getData("INGD_TransferYourCover", "PreferredContact");
	String PreferredTime = dataTable.getData("INGD_TransferYourCover", "PreferredTime");
	String ContactNumber = dataTable.getData("INGD_TransferYourCover", "ContactNumber");
		
		
		
		try{

if(Restrictedillness.equalsIgnoreCase("No")&&LodgeTPDClaim.equalsIgnoreCase("No")&&Diagnosedillness.equalsIgnoreCase("No")&&PremFundLoading.equalsIgnoreCase("No")){
	
	//*****Evidence of existing cover*****\\
	
	tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.EvidenceofCoverchkbox), "Click", "Evidence of Cover");	
	
	//******Declaration Checkbox*******\\
	
	tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.Declarationchkbox), "Click", "Declaration checkbox");	
	
	
	//******Preferred Contact*****\\
	
	tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.PreferredContact), "DropDownSelect", "Preferred Contact",PreferredContact);	
	
	//*****Preferred Time*****\\
	
	tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.PreferredTime), "DropDownSelect", "Preferred Time",PreferredTime);	
	
	//********Contact Number******\\
	
	waitForClickableElement(INGD_TransferYourCoverPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));
	tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);	
	
	//*****Submit*****\\

	tryAction(waitForClickableElement(INGD_TransferYourCoverPageObjects.SubmitDeclaration), "Click", "Submit");	
	sleep(5000);
}
		
//*******Feedback Pop-up******\\
WebDriverWait wait=new WebDriverWait(driver,18);
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
sleep(1000);

if(driver.getPageSource().contains("REFERRED TO ADMINISTRATOR")){
	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//font[@color='#800080']"))).click();
	 report.updateTestLog("PDF", "PDF File Downloaded",Status.SCREENSHOT);
	sleep(1000);
}
else{
	 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
}
	

		
		
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}
}

