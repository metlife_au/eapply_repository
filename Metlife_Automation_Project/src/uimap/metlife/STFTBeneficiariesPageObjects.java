package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTBeneficiariesPageObjects extends PageObjectLibrary{
		
	public static final By btnnominate = getObjectIdentifaction("stft.nominate.button.css");
	public static final By linkTitle = getObjectIdentifaction("stft.nominate.title.dropdown.css");
	public static final By linktitleMr = getObjectIdentifaction("stft.nominate.title.mr.linktext");
	public static final By txtFirstName = getObjectIdentifaction("stft.nominate.firstname.text.id");
	public static final By txtLastName = getObjectIdentifaction("stft.nominate.lastname.text.id");
	public static final By txtDOB = getObjectIdentifaction("stft.nominate.dob.text.css");
	public static final By selRelation = getObjectIdentifaction("stft.nominate.relation.dropdown.css");
	public static final By txtPercentage = getObjectIdentifaction("stft.nominate.percentage.text.id");
	public static final By rdAddressYes = getObjectIdentifaction("stft.nominate.address.yes.radio.css");
	public static final By btnsubmit = getObjectIdentifaction("stft.nominate.submit.btn.css");
	public static final By btnfinish = getObjectIdentifaction("stft.nominate.finish.btn.css");

}
