package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesCCGetQuotePageObjects extends PageObjectLibrary{
	
	public static final By selCoverAmt = getObjectIdentifaction("coles.cc.getquote.coveramount.dropdown.css");
	public static final By rdFlyBuysYes = getObjectIdentifaction("coles.cc.getquote.flybuys.member.yes.radio.css");
	public static final By rdFlyBuysNo = getObjectIdentifaction("coles.cc.getquote.flybuys.member.no.radio.css");
	public static final By rdFlyBuysJoinYes = getObjectIdentifaction("coles.cc.getquote.flybuys.join.yes.radio.css");
	public static final By rdFlyBuysJoinNo = getObjectIdentifaction("coles.cc.getquote.flybuys.join.no.radio.css");
	public static final By lnkCalculateQuote = getObjectIdentifaction("coles.cc.getquote.calculatequote.link.css");
	public static final By lnkApply = getObjectIdentifaction("coles.cc.getquote.apply.link.css");
	public static final By txtColesLname = getObjectIdentifaction("colescc.getquote.lastname.text.id");
}
