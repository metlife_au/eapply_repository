package au.com.metlife.eapply.corporate.business;

import java.io.Serializable;
import java.util.Arrays;

import au.com.metlife.eapply.business.entity.CorpPartner;
import au.com.metlife.eapply.business.entity.CorpMember;
import au.com.metlife.eapply.common.ResponseLink;

public class CorpEntityResponse extends CorpResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2434170489332664553L;
	
	private CorpPartner partner;
	private CorpMember member;
	private ResponseLink[] links;
	public CorpPartner getPartner() {
		return partner;
	}
	public void setPartner(CorpPartner partner) {
		this.partner = partner;
	}
	public CorpMember getMember() {
		return member;
	}
	public void setMember(CorpMember member) {
		this.member = member;
	}
	public ResponseLink[] getLinks() {
		return links;
	}
	public void setLinks(ResponseLink[] links) {
		this.links = links;
	}
	@Override
	public String toString() {
		return "CorpEntityResponse [partner=" + partner + ", member=" + member + ", links=" + Arrays.toString(links)
				+ "]";
	}
	
	
	
	

}
