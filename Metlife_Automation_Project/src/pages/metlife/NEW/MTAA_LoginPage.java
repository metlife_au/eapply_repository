/**
 * 
 */
package pages.metlife.NEW;

import supportlibraries.ScriptHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

/**
 * @author Hemnath
 * 
 */
public class MTAA_LoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public MTAA_LoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public MTAA_LoginPage enterMTAAlogin() {
		enterlogindetails();
		return new MTAA_LoginPage(scriptHelper);
	} 
	
	private void enterlogindetails() {
		
		String MemberNo = dataTable.getData("MTAA_Login", "MemberNo");
		String FirstName = dataTable.getData("MTAA_Login", "FirstName");
		String SurName = dataTable.getData("MTAA_Login", "SurName");
		String DOB = dataTable.getData("MTAA_Login", "DOB");
		
		try{
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("memberId"))).sendKeys(MemberNo);
			report.updateTestLog("Member No", "Member Number: "+MemberNo+" is entered", Status.PASS);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("firstnameId"))).sendKeys(FirstName);
			report.updateTestLog("First Name", "First Name: "+FirstName+" is entered", Status.PASS);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("surnameId"))).sendKeys(SurName);
			report.updateTestLog("SurName", "SurName: "+SurName+" is entered", Status.PASS);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dobId"))).sendKeys(DOB);
			report.updateTestLog("Date of Birth", "Date of Birth: "+DOB+" is entered", Status.PASS);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("loginID"))).click();
			report.updateTestLog("Login", "Login button is clicked", Status.PASS);
		
			sleep(3000);
			report.updateTestLog("MTAA Login", "Sucessfully Logged into the application", Status.PASS);
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
