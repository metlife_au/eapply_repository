package au.com.metlife.claimtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;


@Controller
@SpringBootApplication
@EnableAutoConfiguration
public class ClaimsTrackerClientApplication extends SpringBootServletInitializer{

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

            return application.sources(ClaimsTrackerClientApplication.class);

    }


	public static void main(String[] args) {
		SpringApplication.run(ClaimsTrackerClientApplication.class, args);
	}
}
