package pages.metlife.NEW;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_ChangeYourInsuranceCoverPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_ChangeYourInsuranceCoverPage HOSTPLUSChangeYourInsuranceCover() {
		ChangeYourCover();
		HealthandLifeStyle();
		confirmation();
		
		return new HOSTPLUS_ChangeYourInsuranceCoverPage(scriptHelper);
	}

	public HOSTPLUS_ChangeYourInsuranceCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ChangeYourCover() {

		String EmailId = dataTable.getData("HOSTPLUS_ChangeInsurance", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_ChangeInsurance", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_ChangeInsurance", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_ChangeInsurance", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_ChangeInsurance", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_ChangeInsurance", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_ChangeInsurance", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_ChangeInsurance", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_ChangeInsurance", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_ChangeInsurance", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_ChangeInsurance", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_ChangeInsurance", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_ChangeInsurance", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_ChangeInsurance", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_ChangeInsurance", "AnnualSalary");
		String CostType=dataTable.getData("HOSTPLUS_ChangeInsurance", "CostType");
		String DeathYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathYN");
		String AmountType=dataTable.getData("HOSTPLUS_ChangeInsurance", "AmountType");
		String DeathAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAction");
		String DeathAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAmount");
		String DeathUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathUnits");
		String TPDYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDYN");
		String TPDAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAction");
		String TPDAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAmount");
		String TPDUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDUnits");
		String IPYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPYN");
		String IPAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAction");
		String Insure90Percent=dataTable.getData("HOSTPLUS_ChangeInsurance", "Insure90Percent");
		String WaitingPeriod=dataTable.getData("HOSTPLUS_ChangeInsurance", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("HOSTPLUS_ChangeInsurance", "BenefitPeriod");
		String IPAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAmount");
		String IPUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPUnits");
		String ExpectedDeathcover=dataTable.getData("HOSTPLUS_ChangeInsurance", "ExpectedDeathcover");
		String ExpectedTPDcover=dataTable.getData("HOSTPLUS_ChangeInsurance", "ExpectedTPDcover");
		String ExpectedIPcover=dataTable.getData("HOSTPLUS_ChangeInsurance", "ExpectedIPcover");
		
		try{
			
			
			
			//****Click on Change Your Insurance Cover Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Change your insurance cover')]"))).click();
			
			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\	
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(200);

			//*****Click on the Contact Number Type****\\			
				
				if(!TypeofContact.equalsIgnoreCase("Mobile")){			
							
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Mobile')]"))).click();
				sleep(300);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();			
				report.updateTestLog("Contact Type", "Preferred contact: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
				}
				
				
				//****Enter the Contact Number****\\			
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact NUmber", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mrngRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("aftrNoonRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				//***********Select the Gender******************\\	
				
				if(Gender.equalsIgnoreCase("Male")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mem_gender_male_id__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mem_gender_female_id__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}	
							
				//****Contact Details-Continue Button*****\\			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueContDetlID']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				sleep(1200);			
							
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
					//*****Select the 15 Hours Question*******\\		
					
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhr_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhr_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
					
					//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
					
					if(RegularIncome.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
							
					}		
					else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
							
					}	
					
					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_yesId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_noId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_yesId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_noId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					
					//*****Resident of Australia****\\	
					
					if(Citizen.equalsIgnoreCase("Yes")){		
						driver.findElement(By.id("valCitizenAus_yes_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						driver.findElement(By.id("valCitizenAus_no_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}	
							
					//*****Industry********\\		
				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='industryId']/span)[1]"))).click();
					sleep(800);				
					driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();		
					sleep(800);		
					report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),' What is your occupation?')]"))).click();
					sleep(500);
					
					//*****Occupation*****\\		
					
					driver.findElement(By.xpath("(//button[@data-id='occupationId']/span)[1]")).click();
					//wait.until(ExpectedConditions.elementToBeClickable(By.id("(//button[@data-id='occupationId']/span)[2]"))).click();	
		            sleep(800);							
					driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();		
					sleep(800);	
					report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
					sleep(500);
							
							
					//*****Other Occupation *****\\	
					
					if(OccupationType.equalsIgnoreCase("Other")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupationId"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);	
					}		
					
					
					//*****Extra Questions*****\\							
					
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
				   
				    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
											
				}							
				else{							

					wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
											
				}							
											
				      //*****Do You Hold a Tertiary Qualification******\\							
											
				        if(TertiaryQual.equalsIgnoreCase("Yes")){							
				       
				        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}							
				   else{							
				       
					   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}							
				}							
											
											
											
				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
											
											
											
				//******Do you work in hazardous environment*****\\		
					
				if(HazardousEnv.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_yes_id__xc_r"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_no_id__xc_"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}							
											
				//******Do you spend 20% time working outside office*******\\	
				
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_yes_id__xc_r"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_no_id__xc_"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}							
				}							
							
				    
                  sleep(800);
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				    	//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
					   
					    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
												
					}							
					else{							

						wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
												
					}							
												
					      //*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){							
					       
					        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}						
				}		
				    
											
				//*****What is your annual Salary******							
											
				
				    wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalId"))).sendKeys(AnnualSalary);
				    sleep(500);		
				    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
											
				//***Load the Salary Amount****\\	
				    				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Insert gross salary (i.e.your salary before tax)')]"))).click();
				sleep(500);	
				
				//*****Click on Continue*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occuContinueId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				
				//******Click on Cost of  Insurance as*******\\		
				
				if(!CostType.equalsIgnoreCase("Weekly")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Weekly')])[1]"))).click();
				sleep(800);
				driver.findElement(By.xpath("//span[contains(text(),'"+CostType+"')]")).click();
				sleep(500);
				report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
							
				}
				
				//*******Death Cover Section******\\
				
				if(DeathYN.equalsIgnoreCase("Yes")){
		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='deathSelectCoverId']/span)[1]"))).click();	
					sleep(800);	
					driver.findElement(By.xpath("//*[@id='deathSelCvr_div_id']/span/div/div/ul/li/a/span[contains(text(),'"+DeathAction+"')]")).click();
					sleep(500);
					report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
					
				}
				
				if(AmountType.equalsIgnoreCase("Fixed")){
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),'Fixed')])[1]"))).click();				
					sleep(800);
					report.updateTestLog("Cost Type", "Fixed cost type is Selected", Status.PASS);
				}

				else{
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),'Unitised')])[1]"))).click();					
					sleep(800);
					report.updateTestLog("Cost Type", "Unitised cost type is selected", Status.PASS);
				}
				if(DeathYN.equalsIgnoreCase("Yes")&& DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){
				
					if(AmountType.equalsIgnoreCase("Fixed")){
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("dcAddnlCvrTxt"))).sendKeys(DeathAmount);				
						sleep(800);	
						report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
						sleep(800);
						

					}
					else{
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("dcAddnlCvrTxt"))).sendKeys(DeathUnits);				
						sleep(800);	
						report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
						sleep(800);
					}
				}
			
				//*******TPD Cover Section******\\
			sleep(800);	
			if(!DeathAction.equalsIgnoreCase("Cancel�your cover"))
			{
				
				if(TPDYN.equalsIgnoreCase("Yes"))
				{
					
					driver.findElement(By.xpath("(//button[@data-id='tpdSelectCoverId']/span)[1]")).click();
					sleep(800);	
					
				
					driver.findElement(By.xpath("//*[@id='tpdSelCvr_div_id']/span/span/div/div/ul/li/a/span[contains(text(),'"+TPDAction+"')]")).click();	
					
					
					
					sleep(800);
					report.updateTestLog("TPD Cover action", TPDAction+" is selected on TPD cover", Status.PASS);
					sleep(800);	
					
					if(TPDAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Decrease your cover"))
					{					
						
						if(AmountType.equalsIgnoreCase("Fixed"))
						{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdAddnlCvrTxt"))).sendKeys(TPDAmount);				
							sleep(800);	
							report.updateTestLog("TPD Cover Amount", TPDAmount+" is entered", Status.PASS);		
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
							sleep(800);
						}
						
						else
						{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdAddnlCvrTxt"))).sendKeys(TPDUnits);				
							sleep(800);	
							report.updateTestLog("TPD Cover Amount", TPDUnits+" is entered", Status.PASS);		
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
							sleep(800);
						}	
						
					}
				}			
				
			}
			
			
			                 //*******IP Cover section********\\
			sleep(500);	
			if(IPYN.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='lifeStageCvrId']/span)[1]"))).click();
				sleep(800);	
				driver.findElement(By.xpath("//*[@id='lifeStagCvr_div_id']/span/div/div/ul/li/a/span[contains(text(),'"+IPAction+"')]")).click();
				sleep(800);
				report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);
				
				//*****Insure 90% of My Salary******\\
				
				if(!IPAction.equalsIgnoreCase("Cancel your cover")&&!IPAction.equalsIgnoreCase("Change waiting/benefit period")&&!IPAction.equalsIgnoreCase("Decrease your cover")&&!IPAction.equalsIgnoreCase("No change")){
					if(Insure90Percent.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='insureSalchckboxLbl']/span"))).click();
					report.updateTestLog("Insure 90%", "Insure 90% of My Salary is Selected", Status.PASS);
					sleep(500);
					}
				}
				sleep(500);	
				if(!IPAction.equalsIgnoreCase("Cancel your cover")&&!IPAction.equalsIgnoreCase("No change")){
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='waitingPerFreqId']/span)[1]"))).click();					
					sleep(800);	
					driver.findElement(By.xpath("//span[contains(text(),'"+WaitingPeriod+"')]")).click();
					sleep(800);
					report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
					
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='benefitPerFreqId']/span)[1]"))).click();
					sleep(800);	
					driver.findElement(By.xpath("//span[contains(text(),'"+BenefitPeriod+"')]")).click();
					sleep(800);
					report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
					
				}
				sleep(500);	
				if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")){
					
					if(AmountType.equalsIgnoreCase("Fixed")&&!Insure90Percent.equalsIgnoreCase("Yes"))
					{
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("ipRequredcvrTxt"))).sendKeys(IPAmount);				
						sleep(800);	
						report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),'Waiting Period')])[1]"))).click();
						sleep(800);
					}
					
					else if(AmountType.equalsIgnoreCase("Unitised")&&!Insure90Percent.equalsIgnoreCase("Yes"))
					{
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("ipRequredcvrTxt"))).sendKeys(IPUnits);				
						sleep(800);	
						report.updateTestLog("IP Cover Amount", IPUnits+" is entered", Status.PASS);		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),'Waiting Period')])[1]"))).click();
						sleep(800);
					}	
				}
			}
			

			//*****Click on Calculate Quote********\\
			
			sleep(500);	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueCovrCalcId']/span"))).click();
			report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
			sleep(3000);
			

			if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
				
				driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
				sleep(5500);
			}
			

            if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
				
				driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
				sleep(5500);
			}
           if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
	
	            driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
	            sleep(5500);
                }
           
           if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
        		
	            driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
	            sleep(5500);
               }
			
			//********************************************************************************************\\
					//************Capturing the premiums displayed************************************************\\
					//********************************************************************************************\\
					
					//*******Capturing the Death Cost********\\
					WebElement Death=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[1]"));
					Death.click();
					sleep(300);
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,-200)", "");
					sleep(500);
					String DeathCost=Death.getText();	

					if(DeathCost.equalsIgnoreCase(ExpectedDeathcover)){
						
						report.updateTestLog("Death Cover", CostType+" cost "+DeathCost+" is displayed as Expected", Status.PASS);
					}
					else{
						
						report.updateTestLog("Death Cover", CostType+" cost "+DeathCost+" is not as Expected", Status.FAIL);
					}
					
					
					
					//*******Capturing the TPD Cost********\\
					
					sleep(500);
					WebElement tpd=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[2]"));
					tpd.click();
					sleep(250);
					String TPDCost=tpd.getText();
					if(TPDCost.equalsIgnoreCase(ExpectedTPDcover)){
					
						report.updateTestLog("TPD Cover amount", CostType+" cost "+TPDCost+" is displayed as Expected", Status.PASS);
					}
					else{
						
						report.updateTestLog("TPD Cover amount", CostType+" cost "+TPDCost+" is not as Expected", Status.FAIL);
					}
					
					
					//*******Capturing the IP Cost********\\
					
					sleep(250);
					WebElement ip=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[3]"));
					ip.click();
					sleep(500);
					String IPCost=ip.getText();
					if(IPCost.equalsIgnoreCase(ExpectedIPcover)){
						
						report.updateTestLog("IP Cover amount", CostType+" cost "+IPCost+" is displayed as Expected", Status.PASS);
					}
					else{
						
						report.updateTestLog("IP Cover amount", CostType+" cost "+IPCost+" is not as Expected", Status.FAIL);
					}
					
			sleep(1500);
			
			//******Click on Continue button******\\
			
		    jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			sleep(300);
			driver.findElement(By.xpath("(//span[contains(text(),'Continue')])[4]")).click();
			report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
			sleep(1500);

			//*****Handling the pop-up*******\\
				
				 if(DeathAction.equalsIgnoreCase("Decrease your cover")||DeathAction.equalsIgnoreCase("Cancel your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")||TPDAction.equalsIgnoreCase("Cancel your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){
					quickSwitchWindows();
					sleep(250);
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Continue')]"))).click();
					report.updateTestLog("Pop-Up", "Pop-up continue button is Clicked", Status.PASS);
					sleep(500);
				 }
			 
			sleep(8000);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
	public void HealthandLifeStyle() {

		String Height=dataTable.getData("HOSTPLUS_ChangeInsurance", "Height");
		String HeightType=dataTable.getData("HOSTPLUS_ChangeInsurance", "HeightType");
		String Weight=dataTable.getData("HOSTPLUS_ChangeInsurance", "Weight");
		String WeightType=dataTable.getData("HOSTPLUS_ChangeInsurance", "WeightType");
		String Smoker=dataTable.getData("HOSTPLUS_ChangeInsurance", "Smoker");
		
		String Exclusion_3Years_Question_for_TPDandIP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Exclusion_3Years_Question_for_TPDandIP");
		String Loading_3Years_Questions_for_Death50_TPD50_IP100=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_3Years_Questions_for_Death50_TPD50_IP100");
		String Loading_3Years_Questions_for_Death100_TPD100_IP150=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_3Years_Questions_for_Death100_TPD100_IP150");
		
		
		String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");
		
		String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
		String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");
		
		
		String UsualDoctororMedicalCentre=dataTable.getData("HOSTPLUS_ChangeInsurance", "UsualDoctororMedicalCentre");
		String UsualDoctor_Name=dataTable.getData("HOSTPLUS_ChangeInsurance", "UsualDoctor_Name");
		String UsualDoctor_ContactNumber=dataTable.getData("HOSTPLUS_ChangeInsurance", "UsualDoctor_ContactNumber");
		String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
		String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
		String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
		String Drugs_Last5Years=dataTable.getData("HOSTPLUS_ChangeInsurance", "Drugs_Last5Years");
		String Alcohol_DrinksPerDay=dataTable.getData("HOSTPLUS_ChangeInsurance", "Alcohol_DrinksPerDay");
		String Alcohol_Professionaladvice=dataTable.getData("HOSTPLUS_ChangeInsurance", "Alcohol_Professionaladvice");
		String HIVInfected=dataTable.getData("HOSTPLUS_ChangeInsurance", "HIVInfected");
		String PriorApplication=dataTable.getData("HOSTPLUS_ChangeInsurance", "PriorApplication");
		String PreviousClaims=dataTable.getData("HOSTPLUS_ChangeInsurance", "PreviousClaims");
		String PreviousClaims_PaidBenefit_terminalillness=dataTable.getData("HOSTPLUS_ChangeInsurance", "PreviousClaims_PaidBenefit_terminalillness");
		String CurrentPolicies=dataTable.getData("HOSTPLUS_ChangeInsurance", "CurrentPolicies");
		
		
		String DeathAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAction");
		
		String TPDAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAction");
		
		String IPAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAction");
		
		
		try{
			
			//if(driver.getPageSource().contains("Health Questions")){
			if(DeathAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Increase your cover")){
				
			WebDriverWait wait=new WebDriverWait(driver,20);
			
			//******Enter the Height*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("hq1-2"))).sendKeys(Height);
			report.updateTestLog("Height", "Height of: "+Height+" is Entered", Status.PASS);
			
			//******Choose Height Type******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'cm')])[1]"))).click();
			sleep(500);
			driver.findElement(By.xpath("//span[contains(text(),'"+HeightType+"')]")).click();
			sleep(250);
			report.updateTestLog("Height Type", HeightType+" is Selected", Status.PASS);
			
			//******Enter the Weight*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wq1-3"))).sendKeys(Weight);
			report.updateTestLog("Weight", "Weight of: "+Weight+" is Entered", Status.PASS);
			
			//******Choose Weight Type******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Kilograms')])[1]"))).click();
			sleep(500);
			driver.findElement(By.xpath("//span[contains(text(),'"+WeightType+"')]")).click();
			sleep(250);
			report.updateTestLog("Weight Type", WeightType+" is Selected", Status.PASS);
			sleep(500);
			
			//******Have you smoked in the past 12 months?*******\\
			
			if(Smoker.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-4:0']"))).click();
				sleep(1000);
				report.updateTestLog("Smoker Question", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-4:0']"))).click();
				sleep(1000);
				report.updateTestLog("Smoker Question", "No is Selected", Status.PASS);
			}
			sleep(800);
			
			
			//**********************************************************************************************************************\\
			//*******In the last 3 years have you suffered from, been diagnosed with or sought medical advice or treatment for*******\\
			//***********************************************************************************************************************\\
			
			if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-5:0']"))).click();
				sleep(250);
				report.updateTestLog("Last 3 Years Question", "Headaches or migraines is Selected", Status.PASS);
				sleep(800);
				
			//******Are you currently under investigations or contemplating investigations for your headaches?*****\\
			
					driver.findElement(By.xpath("//label[@for='radio1-1-5_0_0:0']")).click();
					sleep(800);
					report.updateTestLog("Headache or Migraines_Under Current Investigations", "No is Selected", Status.PASS);
					sleep(200);
					
					//******How would you describe your headaches?******\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Nothing selected')]"))).click();
					sleep(600);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Recurring or severe episodes')]"))).click();
					sleep(600);
					report.updateTestLog("Type of Headache", "Recurring or severe episodes is Selected", Status.PASS);
					sleep(800);
					
					//******Select the Headache Type*******\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.id("cbo5_0_0_1_02-Health_Questions-Health_Questions-5"))).click();		
					sleep(800);
									
					//***Have your headaches been fully investigated with all underlying causes excluded?****\\
						
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-5_0_0_1_0_1_0:0']"))).click();
							sleep(500);
							report.updateTestLog("Is headache fully investigated?","Yes is Selected", Status.PASS);
							sleep(800);
							
							//*****How many headaches do you suffer in a week?******\\
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Nothing selected')]"))).click();
							sleep(800);
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'3 episodes or less')]"))).click();
							sleep(250);
							report.updateTestLog("Number of Headaches","3 episodes or less is Selected", Status.PASS);
							sleep(800);
							
							driver.findElement(By.id("cbo5_0_0_1_0_1_0_0_02-Health_Questions-Health_Questions-5")).click();
							sleep(800);
							
						
								
								//*******Is this easily controlled with over the counter medication?******\\
								
								
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-5_0_0_1_0_1_0_0_0_0_0:0']"))).click();
									sleep(250);
									report.updateTestLog("Can  your headache be controlled over Medication", "Yes is Selected", Status.PASS);
									sleep(800);
							
			}
			
			if(Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")||Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-1-5:0']"))).click();
				sleep(250);
				report.updateTestLog("Last 3 Years Question", "Lung or breathing conditions is Selected", Status.PASS);
				sleep(800);
				
				//****What was Your Diagnosis*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-5_1_0:0']"))).click();
				sleep(250);
				report.updateTestLog("Lung or breathing conditions", "Asthma is Selected", Status.PASS);
				sleep(800);
				
				//****Is your condition?******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-5_1_0_0_0:0']"))).click();
				sleep(250);
				report.updateTestLog("Lung or breathing conditions", "Moderate Asthma is Selected", Status.PASS);
				sleep(800);
				
				if(Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){
					
					//****Is your Asthma worsened by your condition****\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-5_1_0_0_1:0']"))).click();
					sleep(250);
					report.updateTestLog("Lung or breathing conditions", "Asthma is worsened by current occupation", Status.PASS);
					sleep(800);
					
				}
				else{
                    //****Is your Asthma worsened by your condition****\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-5_1_0_0_1:0']"))).click();
					sleep(250);
					report.updateTestLog("Lung or breathing conditions", "Asthma is not worsened by current occupation", Status.PASS);
					sleep(800);
				}
				
				
			}
				
			if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-8-5:0']"))).click();	
				sleep(250);
				report.updateTestLog("Last 3 Years Question?","None of the above is Selected", Status.PASS);
				sleep(800);	
			}
			sleep(1000);
			
			//********************************************************************************\\
			//*****************************5 Years Question***********************************\\
			//********************************************************************************\\
			
			if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")){
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-1-6:0']"))).click();	
			sleep(250);
			report.updateTestLog("Last 5 Years Question", "High Cholesterol is Selected", Status.PASS);
			sleep(800);
			
		  //******How is your high Cholesterol being treated??******\\
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Nothing selected')]"))).click();
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Medication prescribed by your doctor')]"))).click();
				sleep(250);
				report.updateTestLog("High Cholesterol","Treated by medication prescribed by your doctor", Status.PASS);
				sleep(800);
				
		  //******Select the Treatment Type*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("cbo6_1_02-Health_Questions-Health_Questions-6"))).click();		
				sleep(800);
				
		  //******When did you last have a reading taken?*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-6_1_0_1_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("High Cholesterol","Last reading taken 12 months ago or less", Status.PASS);
				sleep(800);
				
			//****How did your doctor describe your most recent cholesterol reading?*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-6_1_0_1_0_0_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("High Cholesterol","Last reading was described by doctor as Elevated", Status.PASS);
				sleep(800);
				
		   //****Do you recall your last reading?***\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-6_1_0_1_0_0_0_1_0:0']"))).click();	
				sleep(800);
				
				//*****What was your most recent cholesterol reading taken by your doctor?****\\
				
				driver.findElement(By.id("ran1-6_1_0_1_0_0_0_1_0_0_0")).sendKeys("7.1");
				sleep(250);
				report.updateTestLog("High Cholesterol","Last reading is 7.1 mmol/L", Status.PASS);
				sleep(800);
				
				driver.findElement(By.id("ran26_1_0_1_0_0_0_1_0_0_0-Health_Questions-Health_Questions-6")).click();
				sleep(800);
				
				//*****Have you been advised that your triglycerides are elevated?****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-6_1_1:0']"))).click();	
				sleep(250);
				report.updateTestLog("High Cholesterol","Tryglycerides is not Elevated", Status.PASS);
				sleep(800);
				
			
			}
			if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-3-6:0']"))).click();	
				sleep(250);
				report.updateTestLog("Last 5 Years Question","None of the above is Selected", Status.PASS);
				sleep(800);	
			}
			sleep(800);
		
		//****************************************************************************************************************************************\\
		//**********Have you ever suffered from, been diagnosed with or sought medical advice or treatment for (Please tick all that apply):*******\\
        //*****************************************************************************************************************************************\\
			
			if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-2-7:0']"))).click();	
				sleep(250);
				report.updateTestLog("Prior Treatment or Diagnosis Question","Bone,Joint or limb conditions is Selected", Status.PASS);
				sleep(800);	
				
				//*******What was your diagnosis?********\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-7_2_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("What was your diagnosis?","Amputation/missing or deformed limb is Selected", Status.PASS);
				sleep(800);	
				
				//*******Which limbs were amputated/missing or deformed?*****\\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-7_2_0_0_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("Which limbs were amputated/missing or deformed?"," Arm(s) is Selected", Status.PASS);
				sleep(800);	
				
				//****was it?****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Nothing selected')]"))).click();
				sleep(500);
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'The left arm')]"))).click();
				sleep(250);
				report.updateTestLog("Which Limb?","The left arm is Selected", Status.PASS);
				sleep(500);
				
				driver.findElement(By.id("cbo7_2_0_0_0_0_02-Health_Questions-Health_Questions-7")).click();
				sleep(500);
		
			}
			
			if(Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-9-7:0']"))).click();	
				sleep(250);
				report.updateTestLog("Prior Treatment or Diagnosis Question","Thyroid Conditions is Selected", Status.PASS);
				sleep(800);	
				
				//*****What was the specific condition*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-1-7_9_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("Thyroid Conditions"," Hyperthyroidism (Overactive) is Selected", Status.PASS);
				sleep(800);	
				
				//****Has your condition been fully investigated?******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-7_9_0_1_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("Thyroid Conditions","Condition has been fully investigated is Selected", Status.PASS);
				sleep(800);	
				
				//****Is your condition Fully Controlled*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-7_9_0_1_0_0_0:0']"))).click();	
				sleep(250);
				report.updateTestLog("Thyroid Conditions","Condition is not fully controlled is Selected", Status.PASS);
				sleep(800);	
				
				
			}
			
			
			if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")&&Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("No")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-19-7:0']"))).click();	
				sleep(250);
				report.updateTestLog("Prior Treatment or Diagnosis?","None of the above is Selected", Status.PASS);
				sleep(800);	
			}
			sleep(500);
			
			
			
			//******Do you have a usual doctor or medical centre you regularly visit?******\\
			
			if(UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-8:0']"))).click();
				sleep(800);
				report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","Yes is Selected", Status.PASS);
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("txtArea1-8_0_1"))).sendKeys(UsualDoctor_Name);
				sleep(800);
				report.updateTestLog("Usual Doctor Name",UsualDoctor_Name+" is Entered", Status.PASS);
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("txtArea28_0_1-Health_Questions-Health_Questions-8"))).click();
				sleep(800);
				
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("txt1-8_0_2"))).sendKeys(UsualDoctor_ContactNumber);
				sleep(800);
				report.updateTestLog("Usual Doctor Contact NUmber",UsualDoctor_ContactNumber+" is Entered", Status.PASS);
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("txt28_0_2-Health_Questions-Health_Questions-8"))).click();
				sleep(800);
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-8_0_3:0']"))).click();
				sleep(1000);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-8:0']"))).click();
				sleep(800);
				report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","No is Selected", Status.PASS);
			}
			
			
			//******Click on Continue button*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("Continue-Health_Questions-Health_Questions-6"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(800);
			
			//******************************************************************************************************\\
			//*********************************Family History*********************************************************\\
			//******************************************************************************************************\\
			
			//****Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions***\\
			
			if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-9:0']"))).click();
				sleep(800);
				report.updateTestLog("Family History Question","Yes is Selected", Status.PASS);
				
				//***Select the health condition****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-3-9_0_0:0']"))).click();
				sleep(800);
				report.updateTestLog("Health condition diagnosed"," Diabetes is Selected", Status.PASS);
				
				//***How many family members were affected?****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-9_0_0_3_0"))).sendKeys("3");
				sleep(800);
				report.updateTestLog("Number of Family members affected","3 is Entered", Status.PASS);		
				wait.until(ExpectedConditions.elementToBeClickable(By.id("ran29_0_0_3_0-Family_History-Family_History-9"))).click();
				sleep(800);
				
				//***were two or more family memebers diagnosed over age 19****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-9_0_0_3_0_0_0:0']"))).click();
				sleep(800);
				report.updateTestLog("Number of affected Family members over age 19","Two or more is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-9:0']"))).click();
				sleep(800);
				report.updateTestLog("Family History Question","No is Selected", Status.PASS);	
			}
			
			
			//******Click on Continue button*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("Continue-Family_History-Family_History-0"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(1200);
			
			//********************************************************************************************************\\
			//******************************Lifestyle Questions********************************************************\\
			//*********************************************************************************************************\\
			
			//*******Do you have firm plans to travel or reside in another country*******\\
			if(Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-10:0']"))).click();
				sleep(800);
				report.updateTestLog("Plans to travel outside the country","Yes is Selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-10:0']"))).click();
				sleep(800);
				report.updateTestLog("Plans to travel outside the country","No is Selected", Status.PASS);
			}
						
			
			//******Do you regularly engage in or intend to engage in any of the following hazardous activities*****\\
			
			if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-11:0']"))).click();
				sleep(800);
				report.updateTestLog("Do you engage in any Hazardous activities","Water sports is Selected", Status.PASS);
				
				
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-11_0_0:0']"))).click();
				sleep(800);
				report.updateTestLog("Hazardous activity Type","Underwater diving is Selected", Status.PASS);
					
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-11_0_0_0_0:0']"))).click();
				sleep(800);
				report.updateTestLog("Dept of diving ","More than 40 metres is Selected", Status.PASS);	
					
			}
			
			if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-10-11:0']"))).click();
				sleep(800);
				report.updateTestLog("Do you engage in any Hazardous activities","None of the above is Selected", Status.PASS);	
			}
			
					
				
			
			
			
			//*****Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) 
			//or have you exceeded the recommended dosage for any medication?
			
			if(Drugs_Last5Years.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-12:0']"))).click();
				sleep(800);
				report.updateTestLog("Have you used any drugs in the last 5 years","Yes is Selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-12:0']"))).click();
				sleep(800);
				report.updateTestLog("Have you used any drugs in the last 5 years","No is Selected", Status.PASS);
			}
			
			
			//*****How many alcoholic drinks you have in a day?*****\\
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-13"))).sendKeys(Alcohol_DrinksPerDay);
			sleep(800);
			report.updateTestLog("How many alcoholic drinks per day","1 is Selected", Status.PASS);
			sleep(200);
			driver.findElement(By.id("ran213-Lifestyle_Questions-Lifestyle_Questions-13")).click();
			sleep(800);
			
			//****Have you ever been advised by a health professional to reduce your alcohol consumption?****\\
			
			if(Alcohol_Professionaladvice.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-14:0']"))).click();
				sleep(800);
				report.updateTestLog("Advised to reduce alcohol consumption","Yes is Selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-14:0']"))).click();
				sleep(800);
				report.updateTestLog("Advised to reduce alcohol consumption","No is Selected", Status.PASS);
			}
			
			
			//******Are you infected with HIV (Human Immunodeficiency Virus******\\
			
			if(HIVInfected.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-15:0']"))).click();
				sleep(500);
				report.updateTestLog("Are you infected with HIV","Yes is Selected", Status.PASS);
				sleep(800);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-15:0']"))).click();
				sleep(500);
				report.updateTestLog("Are you infected with HIV","No is Selected", Status.PASS);
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-15_1_0:0']"))).click();
				sleep(800);
			}
			
			
			
			//******Click on Continue button*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("Continue-Lifestyle_Questions-Lifestyle_Questions-5"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(1200);
			
			//******************************************************************************************************\\
			//****************************General Questions******************************************************\\
			//*******************************************************************************************************\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-16:0']"))).click();
			sleep(800);
			report.updateTestLog("Do you presently suffer from any condition?","No is Selected", Status.PASS);
			
			//******Click on Continue button*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("Continue-General_Questions-General_Questions-0"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(1200);
			
			//***********************************************************************************************\\
			//***************************INSURANCE DETAILS****************************************************\\
			//**************************************************************************************************\\
			
			//*****Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or
			//accepted with a loading or exclusion or any other special conditions or terms?
			
			if(PriorApplication.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-17:0']"))).click();
				sleep(800);
				report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","Yes is Selected", Status.PASS);	
               
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-17_0_0:0']"))).click();
				sleep(800);
			}
			
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-17:0']"))).click();
				sleep(800);
				report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","No is Selected", Status.PASS);
			}
			
			
			
			
			//****Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers�
			//Compensation, or any other form of compensation due to illness or injury?
			sleep(200);
			if(PreviousClaims.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-18:0']"))).click();
				sleep(800);
				report.updateTestLog("Have you made a prior claim?","Yes is Selected", Status.PASS);
				
				if(PreviousClaims_PaidBenefit_terminalillness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-18_0_0:0']"))).click();
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-18_0_0:0']"))).click();
					sleep(800);
				}
				
			}
			else{
				sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-18:0']"))).click();
				sleep(800);
				report.updateTestLog("Have you made a prior claim?","No is Selected", Status.PASS);
			}
			
			
			if(driver.getPageSource().contains("Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?")){
				
				//*****Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?
				 if(CurrentPolicies.equalsIgnoreCase("yes")){
					 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-19:0']"))).click();
						sleep(800);
						report.updateTestLog("Do you currently have any insurance policies?","Yes is Selected", Status.PASS);
						
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-19_0_0:0']"))).click();
						sleep(800);
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-19_0_0_0_0"))).sendKeys("90000");
						sleep(800);
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("ran219_0_0_0_0-Insurance_Details-Insurance_Details-19"))).click();
						sleep(800);
				 }
				 else{
					 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-19:0']"))).click();
						sleep(800);
						report.updateTestLog("Do you currently have any insurance policies?","No is Selected", Status.PASS);
				 }
				
			}
			
			

			
			//****Click on Continue****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='applyBtnId']/span"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(800);
			
			sleep(1000);
			
			
			
			
			
			
			
			
			
			}			
		}
		catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void confirmation() {
		
		WebDriverWait wait=new WebDriverWait(driver,20);	
			
			try{
				if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){
					
			//****Loading consent*****\\
			sleep(1000);		
			if(driver.getPageSource().contains("We wish to note that in considering the information disclosed in your personal statement")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='loading_chckbox_id__xc_c']/span"))).click();
				report.updateTestLog("Loading Consent", "selected the Checkbox", Status.PASS);
				sleep(500);
			}
				

				
			//******Agree to general Consent*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(500);
				
				
			//******Click on Submit*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
				report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
				sleep(1000);			
				
			}
			
			//*******Feedback popup******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1000);
			
			if(driver.getPageSource().contains("Application number")){
				
			
			//*****Fetching the Application Status*****\\
			
			String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(500);
			
			//*****Fetching the Application Number******\\
			
			String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
			sleep(1000);
			
			
			
			//******Download the PDF***********\\
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
				sleep(1500);
	  		}
			
			
			//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
			
			
			
		  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		  			sleep(1500);
		  			Robot roboobj=new Robot();
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  		}
		  		
		  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		  			Robot roboobj=new Robot();
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		  				
		  			sleep(4000);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  			
		  		}
			
			report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
			
			sleep(1000);
		
			 
			  report.updateTestLog("Confirmation Page", "Application No: "+App,Status.PASS);
			
				
			 report.updateTestLog("Confirmation Page", "Application Status: "+Appstatus,Status.PASS);	
			}

			else{
				 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
			}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
	}

		
		
	
	
	}





