<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style> html{display : none ; } </style>
<script> if( self == top ) { document.documentElement.style.display = 'block' ; } else { document.write(''); }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MTAA Super</title>
<link rel="shortcut icon" href="static/mtaa/src/app/assets/images/favicon.ico">
<link rel="stylesheet" href="https://www.e2e.eapplication.metlife.com.au/eApplication/xAdvisorWeb/CMS/Group/HOST/css/eservicestyle.css" media="screen"  />

<script type="text/javascript">
	function formSubmit(){
		var errorStatus = false;
		if(document.getElementById('input_string_id').value == "" || document.getElementById('input_string_id').value == null ){
			document.getElementById('errorMessage1').innerHTML = "Input String is a mandatory field";
			errorStatus = true;
		}else{
		document.getElementById('HDInputString').value=document.getElementById('input_string_id').value;
		}
		if(!errorStatus){
			document.instform.submit(); 
		}
	}
function openLegalNoticenew(value)
{
//alert(value);
var sUrl = '/eApplication/faces/xAdvisorWeb/bundles/institutional/login/LegalNotice.jsp?fundID=NSFS';
window.open(sUrl,"Window1","menubar=no,width=1000,height=1200,toolbar=no,resizable=yes,scrollbars=yes");

//alert(value);
// write to window
//Window1.document.writeln(value); 
}
</script>
</head>

<body>
<div class="outerWrap"> 
  <!-- Top navigation container starts -->
  <div class="topNavCont">
    <div class="cornerTopLeft"></div>
    <div class="topNavContent">
      <ul class="topNav">
        <li class="cornerLeft"></li>
        <li> <a href="https://mtaasuper.com.au/about-us" target="_new" title="About us"> About us </a> </li>
        <li>|</li>
        <li> <a href="http://www.mtaasuper.com.au/contact-us" target="_new" title="Contact us"> Contact us </a> </li>
        <li class="cornerRight"></li>
      </ul>
    </div>
    <div class="cornerTopRight"></div>
    <!-- Top navigation container ends --> 
  </div>
  
  <!-- Main Container starts -->
  <div class="mainCont"> 
    <!-- Logo container starts -->
    <div class="logoCont">
      <div class="logoLeft"> <a href="#"> <img src="static/mtaa/src/app/assets/images/logo.png" alt="" title="MTAA" /></a> </div>
      <div class="logoRight none"> </div>
    </div>
    <!-- Logo container ends --> 
    
    <!-- Content container starts -->
    <div class="contentCont">

      <div class="leftCol">
        <div class="titleCont">
          <h1 class="title">Login</h1>
        </div>
        <h2 class="subTitle">Please enter the following information.</h2>
        <div class="blueLine"></div>
       <form name="instform" id="instform"  action="/eapply/mtaaWebStub">
       <input type="hidden" name=HDInputString id="HDInputString" value=""/>
	   <input type="hidden" name="emailID" id="emailID" value="ravi.reddy@cognizant.com"/>
	   <input type="hidden" name="request_type" id="request_type" value="newBusiness"/>
	   <input type="hidden" name="fund_id" id="fund_id" value="MTAA"/>
	   <input type="hidden" name="stubRespPage_id" id="stubRespPage_id" value="mtaaStubResp.jsp"/>
          <!-- Forms started -->
         			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td valign="top" style="padding-left:5px; padding-right:5px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						style="height: 5px">
						<tr> 
							<td>
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tbody>
									
										<tr class="td_alt_white">
											<td width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><label class="label">Input String</label></td>
											<td><div id="errorMessage1" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:red;font-size:13px;"> </div></td>
										</tr>
										<tr>
											<td colspan="2" width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><textarea class="medium" rows="5" cols="100" id="input_string_id" name="inputString"></textarea></td>
										</tr>
										<tr>
											<td>
											<textarea rows="5" cols="100" ><request><adminPartnerID>IFSP</adminPartnerID><transRefGUID>73bbfa52-797d-43bd-85db-05f596d209e7</transRefGUID><transType>2</transType><transDate>24/11/2017</transDate><transTime>10:19:09</transTime><fundID>MTAA</fundID><fundEmail>Multifund_insurance@aas.com.au</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>MTAA</partnerID><applicant><dateJoined>18/10/2017</dateJoined><workCommencementDate>18/10/2017</workCommencementDate><firstContributionDate>30/11/2017</firstContributionDate><clientRefNumber>312658452</clientRefNumber><memberType>Employer Sponsored</memberType><applicantRole>1</applicantRole><personalDetails><title>MRS</title><firstName>Tina</firstName><lastName>Smith</lastName><dateOfBirth>19/12/1975</dateOfBirth><gender>Female</gender><priority>2</priority><smoker>2</smoker><applicantSubType /></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><amount>3000.00</amount>			<units>12</units><loading /><exclusions>Y</exclusions><waitingPeriod>90DAYS</waitingPeriod><benefitPeriod>5YEARS</benefitPeriod><indexation>2</indexation></cover></existingCovers><address><addressType>2</addressType><line1>2 Park St</line1><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>AUSTRALIA</country></address><contactDetails><emailAddress>test@test.com</emailAddress></contactDetails></applicant></policy></request>
											</textarea>
											</td>
										</tr>								
									</tbody>
								</table>
								<br />

							</td>
						</tr>

						

					</table> 
					</td>
				</tr>
			</table>
          <div class="btnHolder fright">
             <a href="#" class="submit" onclick="formSubmit();"><span>PROCEED TO NEXT PAGE</span></a>
          </div>
          <!-- Forms ends -->
        </form>

      </div>
     <div class="rightCol"> 
        <!-- Rounded edge box starts -->        
        <div class="boxCont">
          <div class="popupCont">
          	<div class="popupContRight">
                <h2 class="subHead">Customer service</h2>
                <div class="greyWhite"></div>
                <span class="icoPhone"></span>
                <h2 class="tel">1300 362 415</h2>
            </div>
          </div>
          <div class="bottomCont">
            <div></div>
          </div>
        </div>
        <!-- Rounded edge box ends --> 
      </div>
      <!-- Content container ends --> 
    </div>
    <!-- Main Container ends --> 
      <div class="footerCont">
	    <ul class="footer">
	      <li><a href="https://mtaasuper.com.au/privacy-policy" target="_new" title="Privacy policy">Privacy policy</a></li>
	      <li>|</li>
	     <li><a href="https://mtaasuper.com.au/general-advice-warning" class="" target="_new" >Legal Notice</a></li>
	    </ul>
	    <p class="footeTxt">Copyright &copy 2018 MTAA Super.</p>
	  </div>
  </div>

  
  <!-- Bottom container starts -->
  <div class="footerCurve">
    <div class="cornerBottomLeft"></div>
    <div class="BottomMid">&nbsp;</div>
    <div class="cornerBottomRight"></div>
    <!-- Bottom container ends --> 
  </div>
</div>
</body>
</html>
