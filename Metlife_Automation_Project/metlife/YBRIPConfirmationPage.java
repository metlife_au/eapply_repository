package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.YBRIPConfirmationsPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.OAMPSHealthPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

import javax.swing.JOptionPane;

public class YBRIPConfirmationPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRIPConfirmationPage yourConfirmationEnter() {
		yourConfirmationDetails();
		return new YBRIPConfirmationPage(scriptHelper);
	}

	public YBRIPConfirmationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourConfirmationDetails() {

		String lastname = dataTable.getData("MetLife_Data", "LastName");
		String title = dataTable.getData("MetLife_Data", "Title");
		String contact = dataTable.getData("MetLife_Data", "ContactNumber");
		String contactOther = dataTable.getData("MetLife_Data", "ContactOther");
		String email = dataTable.getData("MetLife_Data", "Email");
		String address1 = dataTable.getData("MetLife_Data", "Address1");
		String address2 = dataTable.getData("MetLife_Data", "Address2");
		String suburb = dataTable.getData("MetLife_Data", "Suburb");
		String state = dataTable.getData("MetLife_Data", "State");
		String postcode = dataTable.getData("MetLife_Data", "Postcode");
	
		try {
			Thread.sleep(2000);
			driver.manage().window().maximize();			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			//	JOptionPane.showMessageDialog(null,"ALERT MESSAGE","TITLE",JOptionPane.WARNING_MESSAGE);			
			tryAction(fluentWaitElement(YBRIPConfirmationsPageObjects.linkTitle), "DropDownSelect", "Title", title);
			Thread.sleep(1000);
////			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtlastName), "SET", "Last Name", lastname);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtlastName), "TAB", "Last Name");
			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtContact), "SET", "Contact", contact);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtContact), "TAB", "Contact");
			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtcontactOther), "SET", "Contact", contactOther);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtcontactOther), "TAB", "Contact");
			
//	
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtemail), "SET", "Email", email);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtemail), "TAB", "Email");
//			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtemailconfirm), "SET", "Conafirm Email", email);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtemailconfirm), "TAB", "Confirm Email");
//			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtaddress1), "SET", "Address 1", address1);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtaddress1), "TAB", "Address 1");
//			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtaddress2), "SET", "Address 2", address2	);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtaddress2), "TAB", "Address 2");
//			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtSuburb), "SET", "Suburb", suburb);
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.txtSuburb), "TAB", "Suburb");
//						
			Thread.sleep(1000);
//			
			tryAction(fluentWaitElement(YBRIPConfirmationsPageObjects.drpState), "DropDownSelect", "State", state);
			Thread.sleep(1000);
//			
			tryAction(fluentWaitElement(YBRIPConfirmationsPageObjects.txtPostcode), "SET", "Post Code", postcode);
			tryAction(fluentWaitElement(YBRIPConfirmationsPageObjects.txtPostcode), "TAB", "Post Code");
//			
			tryAction(waitForClickableElement(YBRIPConfirmationsPageObjects.chkacknowledge), "Click", "Acknowledge");
//			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPConfirmationsPageObjects.btnNext), "Click", "Next");
			Thread.sleep(4000);
			
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
