/* Change Cover Controller,Progressive and Mandatory validations Starts  */
HostApp.controller('quotespecial',['$scope','$rootScope', '$routeParams', '$location','$http', '$timeout', '$window', 'QuoteService', 'OccupationService','persoanlDetailService',
    'deathCoverService','tpdCoverService','ipCoverService', 'CalculateService', 'MaxLimitService','ConvertService',
    'auraInputService', 'CalculateDeathService', 'CalculateTPDService', 'PersistenceService','ngDialog','auraResponseService','propertyServiceSpecial','saveEapply','RetrieveAppDetailsService','urlService','DownloadPDFService','printQuotePage','NewOccupationService','submitEapply','$filter','APP_CONSTANTS',
    function($scope,$rootScope, $routeParams, $location,$http, $timeout, $window, QuoteService, OccupationService, persoanlDetailService, deathCoverService, tpdCoverService,
  		  ipCoverService, CalculateService, MaxLimitService,ConvertService,auraInputService, CalculateDeathService,
  		  CalculateTPDService, PersistenceService,ngDialog,auraResponseService,propertyServiceSpecial,saveEapply,RetrieveAppDetailsService,urlService,DownloadPDFService,printQuotePage,NewOccupationService,submitEapply,$filter, APP_CONSTANTS){

			/* Code for appD starts */
			var pageTracker = null;
			if(ADRUM) {
				pageTracker = new ADRUM.events.VPageView();
				pageTracker.start();
			}
		
			$scope.$on('$destroy', function() {
				pageTracker.end();
				ADRUM.report(pageTracker);
			});
			/* Code for appD ends */
		$scope.phoneNumbr = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
		$scope.emailFormat = APP_CONSTANTS.emailFormat;
		$scope.regex = /[0-9]{1,3}/;
		$scope.isDCCoverRequiredDisabled = true;
		$scope.isDCCoverTypeDisabled = true;
		$scope.isTPDCoverRequiredDisabled = true;
		$scope.isTPDCoverTypeDisabled = true;
		$scope.isTPDCoverNameDisabled = true;
		$scope.isWaitingPeriodDisabled = false;
		$scope.isBenefitPeriodDisabled = false;
		$scope.isIPCoverRequiredDisabled = false;
		$scope.isIPCoverNameDisabled = true;
		$scope.isIpSalaryCheckboxDisabled = false;
		$scope.ipWarningFlag = false;
		$scope.coltwo = false;
		$scope.colthree = false;
		$scope.ackFlag = false;
		$scope.modalShown = false;
	    $scope.dcCoverAmount = 0.00;
		$scope.dcWeeklyCost = 0.00;
		$scope.tpdCoverAmount = 0.00;
		$scope.tpdWeeklyCost = 0.00;
		$scope.ipCoverAmount = 0.00;
		$scope.ipWeeklyCost = 0.00;
		$scope.totalWeeklyCost = 0.00;
		$scope.invalidSalAmount = false;
		$scope.dcIncreaseFlag = false;
		$scope.tpdIncreaseFlag = false;
		$scope.ipIncreaseFlag = false;
		$scope.auraDisabled = false;
		$scope.disclaimerFlag = true;
		$scope.otherOccupationObj = {'otherOccupation': ''};
		$scope.showWithinOfficeQuestion = false;
	    $scope.showTertiaryQuestion = false;
	    $scope.showHazardousQuestion = false;
	    $scope.showOutsideOfficeQuestion = false;
		$scope.urlList = urlService.getUrlList();
	    $scope.deathErrorFlag = false;
	    $scope.tpdErrorFlag = false;
	    $scope.ipErrorFlag = false;
	    $scope.deathErrorMsg ="";
	    $scope.tpdErrorMsg ="";
	    $scope.ipErrorMsg ="";
	    $scope.premFreq = 'Weekly';
	    $rootScope.$broadcast('enablepointer');
	    /*Error Flags*/
		$scope.dodFlagErr = null;
		$scope.privacyFlagErr = null;
		$scope.requireCover = 0;
		$scope.TPDRequireCover = 0;
		$scope.existingIPAmount = 0;
		$scope.coverTypeTouch = false;
		/*$scope.prevOtherOcc = null;*/
		$scope.DeathCoverOptionsOne = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    }, {
	    	key: 'option2',
	    	coverOptionName: 'Decrease your cover'
	    },
	    {
	    	key: 'option3',
	    	coverOptionName: 'Cancel your cover'
	    },
	    {
	    	key: 'option4',
	    	coverOptionName: 'Convert and maintain cover'
	    },
	    {
	    	key: 'option5',
	    	coverOptionName: 'No change'
	    }];

	    $scope.TPDCoverOptionsOne = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    }, {
	    	key: 'option2',
	    	coverOptionName: 'Decrease your cover'
	    },
	    {
	    	key: 'option3',
	    	coverOptionName: 'Cancel your cover'
	    },
	    {
	    	key: 'option5',
	    	coverOptionName: 'No change'
	    },
	    {
	    	key: 'option6',
	    	coverOptionName: 'Same as Death Cover'
	    }];

	    $scope.IPCoverOptionsOne = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    }, {
	    	key: 'option2',
	    	coverOptionName: 'Decrease your cover'
	    },
	    {
	    	key: 'option3',
	    	coverOptionName: 'Cancel your cover'
	    },

	    {
	    	key: 'option5',
	    	coverOptionName: 'No change'
	    },
	    {
	    	key: 'option6',
	    	coverOptionName: 'Change Waiting and Benifit Period'
	    }
	    ];

	    $scope.coverOptionsTwo = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    },
	    {
	    	key: 'option2',
	    	coverOptionName: 'No change'
	    }
	    ];
	    $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
	    $scope.benefitPeriodOptions = ['2 Years'];
	    $scope.premiumFrequencyOptions = ['Monthly', 'Yearly', 'Weekly'];
	    $scope.contactTypeOptions = ["Home", "Work", "Mobile"];
	    var dynamicFlag = false;
		var fetchAppnum = true;
		var appNum;
		var ackCheck;
		var DCMaxAmount = 500000, TPDMaxAmount = 500000,IPMaxAmount = 4000;
		var DCMinUnits = 8, TPDMinUnits = 8;
		var FormOneFields;
		var deathAmount, TPDAmount, IPAmount;
		var ipUnchanged = true;
		var mode3Flag = false;
		var unitisedDeathInitialFlag = false;
		var unitisedTpdInitialFlag = false;
		var tpdUnitFlag = true, tpdUnitSuccess = false;
		var maxTPDUnits;
		var disableIpCover = false;
		var OccupationFormFields, OccupationOtherFormFields;
		var annualSalForUpgradeVal;
		var deathDBCategory, tpdDBCategory, ipDBCategory;
		$scope.preferredContactType = '';
		// added for session expiry


		$scope.deathCoverDetails = deathCoverService.getDeathCover();
		$scope.tpdCoverDetails = tpdCoverService.getTpdCover();
		$scope.ipCoverDetails = ipCoverService.getIpCover();

		$scope.deathOccupationCategory = $scope.deathCoverDetails.occRating;
		$scope.tpdOccupationCategory = $scope.tpdCoverDetails.occRating;
		$scope.ipOccupationCategory = $scope.ipCoverDetails.occRating;

		if($scope.deathCoverDetails && $scope.deathCoverDetails.occRating && $scope.deathCoverDetails.occRating != ''){
  			$scope.deathOccupationCategory = $scope.deathCoverDetails.occRating;
  		} else{
  			$scope.deathOccupationCategory = 'General';
  		}
  		if($scope.tpdCoverDetails && $scope.tpdCoverDetails.occRating && $scope.tpdCoverDetails.occRating != ''){
  			$scope.tpdOccupationCategory = $scope.tpdCoverDetails.occRating;
  		} else{
  			$scope.tpdOccupationCategory = 'General';
  		}
  		if($scope.ipCoverDetails && $scope.ipCoverDetails.occRating && $scope.ipCoverDetails.occRating != ''){
  			$scope.ipOccupationCategory = $scope.ipCoverDetails.occRating;
  		} else{
  			$scope.ipOccupationCategory = 'General';
  		}

		if ($scope.deathCoverDetails && $scope.deathCoverDetails.type) {
			if($scope.deathCoverDetails.type == "1"){
				$scope.coverType = "DcUnitised";
				$scope.exDcCoverType = "DcUnitised";
				showhide('nodollar1','dollar1');
				showhide('nodollar','dollar');
			} else if($scope.deathCoverDetails.type == "2"){
				$scope.coverType = "DcFixed";
				$scope.exDcCoverType = "DcFixed";
			}
		}

		if ($scope.tpdCoverDetails && $scope.tpdCoverDetails.type) {
			if($scope.tpdCoverDetails.type == "1"){
				$scope.tpdCoverType = "TPDUnitised";
				$scope.exTpdCoverType = "TPDUnitised";
				showhide('nodollar1','dollar1');
				showhide('nodollar','dollar');
			} else if($scope.tpdCoverDetails.type == "2"){
				$scope.tpdCoverType = "TPDFixed";
				$scope.exTpdCoverType = "TPDFixed";
			}
		}
		/*if($scope.ipCoverDetails && $scope.ipCoverDetails.type){
			if($scope.ipCoverDetails.type==2){
				 $scope.IPRequireCover = $scope.ipCoverDetails.amount;
			}else if($scope.ipCoverDetails.type==2){
				$scope.IPRequireCover = $scope.ipCoverDetails.units;
			}

		}*/

		if($scope.ipCoverDetails && $scope.ipCoverDetails.waitingPeriod && $scope.ipCoverDetails.waitingPeriod != ''){
			$scope.waitingPeriodAddnl = $scope.ipCoverDetails.waitingPeriod;
		} else {
			$scope.waitingPeriodAddnl = '90 Days';
		}

		if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitPeriod && $scope.ipCoverDetails.benefitPeriod != ''){
			$scope.benefitPeriodAddnl = $scope.ipCoverDetails.benefitPeriod;
		} else{
			$scope.benefitPeriodAddnl = '2 Years';
		}



		var inputDetails = persoanlDetailService.getMemberDetails();
		$scope.personalDetails = inputDetails.personalDetails;
		var age = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
		var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
		$scope.ageLimit=anb;
		var DCMaxUnits,TPDMaxUnits;
		if(anb >= 24){
			DCMaxUnits = 20, TPDMaxUnits = 20;
		}else if (anb < 24){
			DCMaxUnits = 16, TPDMaxUnits = 16;
		}


		if(inputDetails && inputDetails.contactDetails.emailAddress){
			$scope.email = inputDetails.contactDetails.emailAddress;
		}
		if(inputDetails && inputDetails.contactDetails.prefContactTime){
			if(inputDetails.contactDetails.prefContactTime == "1"){
				$scope.time= "Morning (9am - 12pm)";
			}else{
				$scope.time= "Afternoon (12pm - 6pm)";
			}
		}
		if(inputDetails.contactDetails.prefContact == null || inputDetails.contactDetails.prefContact == "")
			{
			inputDetails.contactDetails.prefContact=1;
			}


		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.gender ='';
		}else{
			$scope.gender = $scope.personalDetails.gender;
		}


		if(inputDetails && inputDetails.contactDetails.prefContact){
			if(inputDetails.contactDetails.prefContact == "1"){
				$scope.preferredContactType= "Mobile";
				$scope.changeCvrPhone = inputDetails.contactDetails.mobilePhone;
			}else if(inputDetails.contactDetails.prefContact == "2"){
				$scope.preferredContactType= "Home";
				$scope.changeCvrPhone = inputDetails.contactDetails.homePhone;
			}else if(inputDetails.contactDetails.prefContact == "3"){
				$scope.preferredContactType= "Work";
				$scope.changeCvrPhone = inputDetails.contactDetails.workPhone;
			}
	   }
		$scope.changePrefContactType = function(){
			if($scope.preferredContactType == "Home"){
				$scope.changeCvrPhone = inputDetails.contactDetails.homePhone;
			} else if($scope.preferredContactType == "Work"){
				$scope.changeCvrPhone = inputDetails.contactDetails.workPhone;
			} else if($scope.preferredContactType == "Mobile"){
				$scope.changeCvrPhone = inputDetails.contactDetails.mobilePhone;
			} else {
				$scope.changeCvrPhone = '';
			}
		}

		/*while($scope.personalDetails){
			if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
				$scope.genderFlag =  false;
				$scope.gender = '';*/
				FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime','gender'];
			/*}else{
				$scope.genderFlag =  true;
				$scope.gender = $scope.personalDetails.gender;
				FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime'];
				}
			  break;
		}*/
		OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
	    OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];


		if($scope.ageLimit > 69){
			$('#deathsection').removeClass('active');
			$('#tpdsection').removeClass('active');
			$('#ipsection').removeClass('active');
			$("#death").css("display", "none");
			$("#tpd").css("display", "none");
			$("#sc").css("display", "none");
			$scope.isDeathDisabled = true;
		    $scope.isTPDDisabled = true;
		    $scope.isIPDisabled = true;
		}else if($scope.ageLimit > 64){
			$('#tpdsection').removeClass('active');
			$('#ipsection').removeClass('active');
			$("#tpd").css("display", "none");
			$("#sc").css("display", "none");
			$scope.isTPDDisabled = true;
	        $scope.isIPDisabled = true;
		}

		QuoteService.getList($scope.urlList.quoteUrl,"HOST").then(function(res){
			$scope.IndustryOptions = res.data;
		}, function(err){
			console.info("Error while fetching industry list " + JSON.stringify(err));
		});

		MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"HOST",inputDetails.memberType,"SCOVER").then(function(res){
			var limits = res.data;
			annualSalForUpgradeVal = limits[0].annualSalForUpgradeVal;
	    	/*DCMaxAmount = limits[0].deathMaxAmount;
	    	TPDMaxAmount = limits[0].tpdMaxAmount;
	    	IPMaxAmount = limits[0].ipMaxAmount;*/
		}, function(error){
			console.info('Something went wrong while fetching limits ' + error);
		});

		// Added to fix defect 169682: Fixed is not displayed, if cover is passed in
		// fixed
		$scope.isEmpty = function(value){
			return ((value == "" || value == null) || value == "0");s
		};

		$scope.checkLimits = function(){
			if(parseInt($scope.requireCover) > parseInt(DCMaxAmount)){
				$scope.deathMaxFlag = true;
			}
			$scope.calculateOnChange();
		};

	    $scope.getOccupations = function(){
	    	if($scope.otherOccupationObj)
	    		$scope.otherOccupationObj.otherOccupation = '';
	    	if(!$scope.industry){
	    		$scope.industry = '';
	    	}
	    	OccupationService.getOccupationList($scope.urlList.occupationUrl, "HOST", $scope.industry).then(function(res){
	    		$scope.OccupationList = res.data;
	    		$scope.occupation = "";
//	    		 $scope.getCategoryFromDB();
	        	$scope.calculateOnChange();
	    	}, function(err){
	    		console.info("Error while fetching occupations " + JSON.stringify(err));
	    	});
	    };
	    
	    /*$scope.getOtherOccupationAS = function(entered) {

		    return $http.get('./occupation.json').then(function(response) {
		      $scope.occupationList=[];
	        if(response.data.Other) {
		        for (var key in response.data.Other) {
		              var obj={};
		              obj.id=key;
		               obj.name=response.data.Other[key];
	                 //if(obj.name.indexOf(entered) > -1) {
	                  $scope.occupationList.push(obj.name);
	                 //}
		               
		        }
		      }
	        return $filter('filter')($scope.occupationList, entered);
		    }, function(err){
		      console.info("Error while fetching occupations " + JSON.stringify(err));
		    });
		    
		  };*/

	    $scope.checkOwnBusinessQuestion = function(){
	    	if($scope.ownBussinessQuestion == 'Yes'){
		    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
			    OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
		    } else if($scope.ownBussinessQuestion == 'No'){
		    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
			    OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
		    }
	    };

	  //Death, TPD and IP Validations


	$scope.validateDeathTpdIpAmounts = function(){
		
	
		if($scope.requireCover === ''){
//			$scope.TPDRequireCover = $scope.requireCover;
			if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
				$scope.TPDRequireCover = $scope.requireCover;
				}
				if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'Yes'){
					$scope.TPDRequireCover = 0;
					}
			if($scope.coverType == "DcFixed"){
				if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.amount)){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.requireCover) > DCMaxAmount){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "Your Death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxAmount + ". Please re-enter your cover.";
				}else{
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					if(!isNaN($scope.requireCover)&& ($scope.requireCover!=0)){
						var Deathreqcover=	1000*(Math.ceil(Math.abs($scope.requireCover/1000)));
						}
						if(Deathreqcover!=0 && !isNaN(Deathreqcover)){
							$scope.requireCover=Deathreqcover;
						}
				}
			}else if($scope.coverType == "DcUnitised"){
				if(parseInt($scope.requireCover) < DCMinUnits){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "Minimum amount allowed for this product is " + DCMinUnits + ". Please re-enter your cover.";
				} else if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.units)){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.requireCover) > DCMaxUnits){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "Your Death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxUnits + ". Please re-enter your cover.";
				}else{
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
				}
				
		
			}
			if($scope.tpdCoverType == "TPDFixed"){
				if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
				if($scope.TPDRequireCover != null && ($scope.requireCover == null || $scope.requireCover == "")){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot apply for TPD cover without Death Cover." ;
				}else if((parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)) ){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
				}else if(parseInt($scope.TPDRequireCover) < parseInt($scope.requireCover)){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover amount should not be lesser than Death cover amount." ;
				}
				else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.amount)){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.TPDRequireCover) > TPDMaxAmount){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
				}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
				}
			}else{
//				$scope.TPDRequireCover = 0;
			}
			}
				else if($scope.tpdCoverType == "TPDUnitised"){
					if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
				if($scope.TPDRequireCover != null && ($scope.requireCover == null || $scope.requireCover == "")){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot apply for TPD cover without Death Cover." ;
				}else if(parseInt($scope.TPDRequireCover) < TPDMinUnits){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Minimum amount allowed for this product is " + TPDMinUnits + ". Please re-enter your cover.";
				}
				else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
				}else if(parseInt($scope.TPDRequireCover) < parseInt($scope.requireCover) ){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover amount should not be lesser than Death cover amount." ;
				}
				else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.units)){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.TPDRequireCover) > TPDMaxUnits){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxUnits + ". Please re-enter your cover.";
				}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
				}
			}else{
//				$scope.TPDRequireCover = 0;
			}
				}
		}else if($scope.coverTypeTouch == true){
			$scope.tpdErrorFlag = false;
			$scope.tpdErrorMsg = "";
			$scope.TPDRequireCover = $scope.requireCover;
			if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
			$scope.TPDRequireCover = $scope.requireCover;
			}
			if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'Yes'){
				$scope.TPDRequireCover = 0;
				}
			if($scope.coverType == "DcFixed"){
				if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.amount)){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.requireCover) > DCMaxAmount){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "Your Death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxAmount + ". Please re-enter your cover.";
				}else{
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					if(!isNaN($scope.requireCover)&& ($scope.requireCover!=0)){
						var Deathreqcover=	1000*(Math.ceil(Math.abs($scope.requireCover/1000)));
						}
						if(Deathreqcover!=0 && !isNaN(Deathreqcover)){
							$scope.requireCover=Deathreqcover;
							if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
								$scope.TPDRequireCover = $scope.requireCover;		
							}
						}
				}
			}else if($scope.coverType == "DcUnitised"){
				if(parseInt($scope.requireCover) < DCMinUnits){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "Minimum amount allowed for this product is " + DCMinUnits + ". Please re-enter your cover.";
				} else if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.units)){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.requireCover) > DCMaxUnits){
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "Your Death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxUnits + ". Please re-enter your cover.";
				}else{
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
						$scope.TPDRequireCover = $scope.requireCover;		
					}
	
				}
				
		
			}
			if($scope.tpdCoverType == "TPDFixed"){
				if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
				if($scope.TPDRequireCover != null && ($scope.requireCover == null || $scope.requireCover == "")){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot apply for TPD cover without Death Cover." ;
				}else if((parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)) ){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
				}else if(parseInt($scope.TPDRequireCover) < parseInt($scope.requireCover)){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover amount should not be lesser than Death cover amount." ;
				}
				else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.amount)){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
				}else if(parseInt($scope.TPDRequireCover) > TPDMaxAmount){
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "Your TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
				}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
				}
			}else{
//				$scope.TPDRequireCover = 0;
			}
			}
			else if($scope.tpdCoverType == "TPDUnitised"){
				if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
			if($scope.TPDRequireCover != null && ($scope.requireCover == null || $scope.requireCover == "")){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "You cannot apply for TPD cover without Death Cover." ;
			}else if(parseInt($scope.TPDRequireCover) < TPDMinUnits){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Minimum amount allowed for this product is " + TPDMinUnits + ". Please re-enter your cover.";
			}
			else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
			}else if(parseInt($scope.TPDRequireCover) < parseInt($scope.requireCover) ){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Your TPD cover amount should not be lesser than Death cover amount." ;
			}
			else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.units)){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
			}else if(parseInt($scope.TPDRequireCover) > TPDMaxUnits){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Your TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxUnits + ". Please re-enter your cover.";
			}else{
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg = "";
			}
		}else{
//			$scope.TPDRequireCover = 0;
		}
			}
		}
		
		if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
			if(parseInt($scope.IPRequireCover) < parseInt($scope.ipCoverDetails.amount)){
				$scope.ipErrorFlag = true;
				$scope.ipErrorMsg = "You cannot reduce your IP Only cover. Please re-enter your cover amount.";
			}
			else{
				$scope.ipErrorFlag = false;
				$scope.ipErrorMsg = "";
			}
//			Added to load IpCover Value on multiples of 500 upto
			if(!$scope.ipcheckbox && $scope.IPRequireCover!=0){
			var ipninety =	$scope.checkIpCoverwithSalary();
			//if(ipninety>IPMaxAmount){
			if(ipninety == 0){
				IPMaxAmount = 4000;
			}else{
				IPMaxAmount = ipninety;
				ipninety = 0;
			}
			
			if(!isNaN($scope.IPRequireCover)&& ($scope.IPRequireCover!=0)){
				$scope.ipWarningFlag = false;
				var IPreqcover=	500*(Math.ceil(Math.abs($scope.IPRequireCover/500)));
			}
			if(IPreqcover!=0 && !isNaN(IPreqcover)){
				 if($scope.IPRequireCover > IPMaxAmount) {
			        	$scope.IPRequireCover = IPMaxAmount;
			        	$scope.ipWarningFlag = true;
			        }
				 else{
					 if($scope.ipWarningFlag){
							$scope.ipWarningFlag = false; 
					 }
					 $scope.IPRequireCover=IPreqcover;
				 }
			}
			
			
			/*}
			else{
				$scope.IPRequireCover = ipninety;
	        	$scope.ipWarningFlag = true;
			}*/
			}
		}
	    $scope.customDigest();
	      $timeout(function(){
	        $scope.calculateOnChange();
	      });
	};
	
	
	 $scope.checkIpCoverwithSalary = function() {
		 var ipninety = 0;
		 if(parseInt($scope.annualSalary) <= 1000000){
				ipninety =   500*(Math.ceil(Math.abs(parseFloat((0.90 * ($scope.annualSalary/12)).toFixed(2))/500)));
				if(ipninety > 4000){
					ipninety = 4000;
				}
		      }
    
    	return ipninety;
	  };
	  
	  
	  $scope.insureNinetyPercentIp = function() {
		    IPMaxAmount = 4000;
		    $timeout(function() {
		      $scope.ipcheckbox = $('#ipsalarycheck').prop('checked');
		      if($scope.ipcheckbox == true) {
		        $scope.IPRequireCover  =   500*(Math.ceil(Math.abs(parseFloat((0.90 * ($scope.annualSalary/12)).toFixed(2))/500)));
		        if($scope.IPRequireCover > IPMaxAmount) {
		        	$scope.IPRequireCover = IPMaxAmount;
		        	$scope.ipWarningFlag = true;
		        	if($scope.ipErrorFlag){
		        	  	$scope.ipErrorFlag = false;
		        	}
		      
		        } else if($scope.IPRequireCover < parseInt($scope.ipCoverDetails.amount)) {
		            $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);
		        } else {
		          $scope.ipWarningFlag = false;
		        }
		        $scope.isIPCoverRequiredDisabled = true;
		      } else {
		    	  $scope.IPRequireCover = parseInt($scope.ipCoverDetails.amount);;
		        $scope.isIPCoverRequiredDisabled = false;
		        $scope.ipWarningFlag = false;
		        $scope.isWaitingPeriodDisabled = false;
				$scope.isBenefitPeriodDisabled = false;
		      }
		      $scope.validateDeathTpdIpAmounts();
		    });
		  };
	    $scope.navigateToLandingPage = function (){
	    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
	    		$location.path("/landing");
	    	}*/
	    	ngDialog.openConfirm({
	            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path("/landing");
	        }, function(e){
	        	if(e=='oncancel'){
	        		return false;
	        	}
	        });
	    }

	    $scope.syncRadios = function(val){
	    	$scope.requireCover = "";
	    	$scope.TPDRequireCover = "";
	    	var radios = $('label[radio-sync]');
	    	var data = $('input[data-sync]');
			data.filter('[data-sync="' + val + '"]').attr('checked','checked');
			if(val == 'Fixed'){
				$scope.deathErrorFlag = false;
	 	  	    $scope.deathErrorMsg="";
	 	  	    $scope.tpdErrorFlag = false;
	    		$scope.tpdErrorMsg ="";
				showhide('dollar1','nodollar1');
				showhide('dollar','nodollar');
				$scope.coverType = 'DcFixed';
				$scope.tpdCoverType = 'TPDFixed';
			} else if(val == 'Unitised'){
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg="";
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				showhide('nodollar1','dollar1');
				showhide('nodollar','dollar');
				$scope.coverType = 'DcUnitised';
				$scope.tpdCoverType = 'TPDUnitised';
			}
			radios.removeClass('active');
			radios.filter('[radio-sync="' + val + '"]').addClass('active');
			/*if($scope.coverType != $scope.exDcCoverType){
				var found = false;
				for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
				    if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'Convert and maintain cover') {
				        found = true;
				        break;
				    }
				}
				if(!found){
					$scope.TPDCoverOptionsOne.push({key: 'option4',coverOptionName: 'Convert and maintain cover'});
				}
				$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
					return obj.coverOptionName !== 'No change';
				});
			} else{
				var nochangeExists = false;
				for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
				    if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'No change') {
				    	nochangeExists = true;
				        break;
				    }
				}
				if(!nochangeExists){
					$scope.TPDCoverOptionsOne.push({key: 'option4',coverOptionName: 'No change'});
				}
				$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
					return obj.coverOptionName !== 'Convert and maintain cover';
				});
			}*/
			 $scope.getCategoryFromDB();
			$scope.calculateOnChange();
	    };

	    $scope.convertDeathUnitsToAmount = function(){
	    	if($scope.requireCover && $scope.requireCover != ''){
	    		unitisedDeathInitialFlag = true;
    		}
	    	//$scope.getNewOccupation();
	    	$scope.renderOccupationQuestions();
	    	var deathReqObject = {
	        		"age": anb,
	        		"fundCode": "HOST",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"smoker": false,
	        		"deathUnits": parseInt($scope.requireCover),
	        		"deathUnitsCost": null,
	        		"premiumFrequency": "Weekly",
	        		"memberType": null,
	        		"manageType": "SCOVER",
	        		"deathCoverType": $scope.coverType
	        	};
	    	if(unitisedDeathInitialFlag){
	    		CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
	    			deathAmount = res.data[0].coverAmount;
	    			$scope.validateDeathTpdIpAmounts();
	    		}, function(err){
	    			console.info('Error while fetching death amount ' + err);
	    		});
	    	}

	    	if(tpdUnitFlag){
		    	var unitTPDReqObj = {
		        		"age": anb,
		        		"fundCode": "HOST",
		        		"gender": $scope.gender,
		        		"tpdOccCategory": $scope.tpdOccupationCategory,
		        		"smoker": false,
		        		"tpdUnits": 1,
		        		"tpdUnitsCost": null,
		        		"premiumFrequency": "Weekly",
		        		"memberType": null,
		        		"manageType": "SCOVER",
		        		"tpdCoverType": "TPDUnitised",
		        	};
		    	CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,unitTPDReqObj).then(function(res){
		    		tpdUnitFlag = false;
		    		tpdUnitSuccess = true;
		    		var TPDUnitAmount = res.data[0].coverAmount;
		    		maxTPDUnits = Math.floor(TPDMaxAmount/TPDUnitAmount);
		    	}, function(err){
		    		console.info('Error while fetching TPD amount ' + err);
		    	});
	    	}

	    	/*if(tpdUnitSuccess){
	    		if($scope.requireCover > maxTPDUnits){
	    			$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
	    				return obj.coverOptionName != 'Same as Death Cover';
	    			});
	    		} else{
	    			var optionExists = $scope.TPDCoverOptionsOne.some(function(obj){
	    				return obj.coverOptionName == 'Same as Death Cover';
	    			});
	    			if(!optionExists){
	    				$scope.TPDCoverOptionsOne.push({'key':'option6', 'coverOptionName':'Same as Death Cover'});
	    			}
	    		}
	    	}*/
	    };

	    $scope.convertTPDUnitsToAmount = function(){
	    	if($scope.TPDRequireCover && $scope.TPDRequireCover != ''){
	    		unitisedTpdInitialFlag = true;
	    	}
	    	//$scope.getNewOccupation();
	    	$scope.renderOccupationQuestions();
	    	var TPDReqObject = {
	        		"age": anb,
	        		"fundCode": "HOST",
	        		"gender": $scope.gender,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"smoker": false,
	        		"tpdUnits": parseInt($scope.TPDRequireCover),
	        		"tpdUnitsCost": null,
	        		"premiumFrequency": "Weekly",
	        		"memberType": null,
	        		"manageType": "SCOVER",
	        		"tpdCoverType": $scope.tpdCoverType,
	        	};
	    	CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,TPDReqObject).then(function(res){
	    		TPDAmount = res.data[0].coverAmount;
	    		if(unitisedTpdInitialFlag){
	    			$scope.validateDeathTpdIpAmounts();
	    		}
	    	}, function(err){
	    		console.info('Error while fetching TPD amount ' + err);
	    	});
	    };


	    $scope.indexation= {
	    		death: false,
	    		disable: false
	    }
	    $scope.setIndexation = function ($event) {
	    	$event.stopPropagation();
	    	$event.preventDefault();
	    	$scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
	    	if(!$scope.indexation.death) {
	    		$("#changecvr_index-dth").parent().removeClass('active');
	    		$("#changeCvr-indextpd-disable").parent().removeClass('active');
	    	}
	    }

	    $scope.deathDecOrCancelFlag=false;
	    $scope.tpdDecOrCancelFlag=false;
	    $scope.ipDecOrCancelFlag=false;



		$scope.checkBenefitPeriod = function(){
			if($scope.ipCoverDetails.waitingPeriod == $scope.waitingPeriodAddnl && $scope.ipCoverDetails.benefitPeriod == $scope.benefitPeriodAddnl){
				$scope.ipIncreaseFlag = false;
				$scope.disclaimerFlag = true;
			} else if(($scope.ipCoverDetails.benefitPeriod == '2 Years') ||
					($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.waitingPeriodAddnl == '60 Days') ||
					($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.waitingPeriodAddnl == '30 Days') ||
					($scope.ipCoverDetails.waitingPeriod == '60 Days' && $scope.waitingPeriodAddnl == '30 Days')){
				$scope.ipIncreaseFlag = true;
				$scope.disclaimerFlag = true;
			} else{
				$scope.ipIncreaseFlag = false;
				$scope.disclaimerFlag = false;
			}
			$scope.calculateOnChange();
		};

	    // added for Decrease or Cancel cover popup "on continue" after calculate quote
		   $scope.showDecreaseOrCancelPopUp = function (val){
		   		if(val == null || val == "" || val == " "){
		   			hideTips();
		   		}else{
		   		 ackCheck = $('#termsLabel').hasClass('active');
		    		if(ackCheck){
		    			 $scope.ackFlag = false;
				   		 document.getElementById('mymodalDecCancel').style.display = 'block';
				   		 document.getElementById('mymodalDecCancelFade').style.display = 'block';
				   		 document.getElementById('decCancelMsg_text').innerHTML=val;
		    		}else{
		    			$scope.ackFlag = true;
		    		}

		   		}
		   	//	$scope.saveDataForPersistence();
		   	}
		    $scope.hideTips = function  (){
		   		if(document.getElementById('help_div')){
		   			document.getElementById('help_div').style.display = "none";
		   		}
		   	}

		 // added to check which covers are selected as Decrease/Cancel
			$scope.decCancelCover="";
			$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
			/*$scope.decOrCancelCovers = function(){
				//Coomented this section as a part of Latest BRD requirement -- S73124 BRD V2.1
				if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
					$scope.decCancelCover = "Death, TPD & IP";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}else if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true){
					$scope.decCancelCover = "Death & TPD";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}else if($scope.deathDecOrCancelFlag && $scope.ipDecOrCancelFlag){
					$scope.decCancelCover = "Death & IP";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}else if($scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
					$scope.decCancelCover = "TPD & IP";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}else if($scope.deathDecOrCancelFlag == true){
					$scope.decCancelCover = "Death";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}else if($scope.tpdDecOrCancelFlag == true){
					$scope.decCancelCover = "TPD";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}else if($scope.ipDecOrCancelFlag == true){
					$scope.decCancelCover = "IP";
					$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
				}
				$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
			};*/
			/*$scope.checkForAura= function(){
				$scope.checkAuraDisable();
				if($scope.auraDisabled == true){
					$scope.continueToNextPage('/summary/1');
				}else{
					$scope.goToAura();
				}
		   }*/


		$scope.checkAnnualSalary = function(){
			 if(parseInt($scope.annualSalary) == 0){
			      $scope.invalidSalAmount = true;
			    } else{
			      $scope.invalidSalAmount = false;
			    }
			    if(!$scope.invalidSalAmount)
			      $scope.renderOccupationQuestions();
			    
			    if($scope.ipcheckbox ) {
			      $scope.insureNinetyPercentIp();      
			    }
			//$scope.setCategory();
			//$scope.getNewOccupation();
		};

	    $scope.go = function ( path ) {
	  	  $location.path( path );
	  	};

	  	$scope.continueToNextPage = function(path){
	  		$scope.saveDataForPersistence();
	  		$location.path(path);
	  	};

		/* Check if your is allowed to proceed to the next accordion */
	  	// TBC
	  	// Need to revisit, need better implementation
	  	$scope.isCollapsible = function(targetEle, event) {
	  		if( targetEle == 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
	  			if($('#dodLabel').is(':visible'))
	  				$scope.dodFlagErr = true;
	  			event.stopPropagation();
	  			return false;
	  		} else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
	  			if($('#privacyLabel').is(':visible'))
	  				$scope.privacyFlagErr = true;
	  			event.stopPropagation();
	  			return false;
	  		}  else if( targetEle == 'collapseTwo' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
	  			if($("#collapseOne form").is(':visible'))
	  				$scope.formOneSubmit($scope.formOne);
	  			event.stopPropagation();
	  			return false;
	  		}  else if( targetEle == 'collapseThree' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
	  			if($("#collapseTwo form").is(':visible'))
	  				$scope.formOneSubmit($scope.occupationForm);
	  			event.stopPropagation();
	  			return false;
	  		}
	  	}

	  	$scope.toggleTwo = function(checkFlag) {
	        $scope.coltwo = checkFlag;
	        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
	    };

	  	$scope.toggleThree = function(checkFlag) {
	        $scope.colthree = checkFlag;
	        if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseThree']").click(); /* Can be improved */
	    };

	    $scope.privacyCol = false;
	    var dodCheck;
	    var privacyCheck;
	    var dodVal = 0;
	    var privacyVal = 0;
	    var contactVal = 0;
	    var occupationVal = 0;
	    var coverVal = 0;

	    /* TBC */
	    $scope.togglePrivacy = function(checkFlag) {
	        $scope.privacyCol = checkFlag;
	        if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
	    };

	    $scope.toggleContact = function(checkFlag) {
	        $scope.contactCol = checkFlag;
	        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseOne']").click(); /* Can be improved */

	    };
	    $scope.checkDodState = function() {
	    	$timeout(function() {
	    		$scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
	    		if($('#dodLabel').hasClass('active')) {
	    			$scope.togglePrivacy(true);
	    		} else {
	    			$scope.togglePrivacy(false);
	    			$scope.toggleContact(false);
	    			$scope.toggleTwo(false);
	    			$scope.toggleThree(false);
	    		}
	    	}, 1);
	    };

	    $scope.checkPrivacyState  = function() {
	    	$timeout(function() {
	    		$scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
	    		if($('#privacyLabel').hasClass('active')) {
	    			$scope.toggleContact(true);
	    		} else {
	    			$scope.toggleContact(false);
	    			$scope.toggleTwo(false);
	    			$scope.toggleThree(false);
	    		}
	    	}, 1);
	    };

	   //  $scope.togglePrivacy = function(flag) {
	   //      $scope.privacyCol = flag;
	   //      $("a[data-target='#collapseprivacy']").click();

	   //  };

	   //  $scope.toggleContact = function(flag) {
	   //      $scope.contactCol = flag;
	   //      $("a[data-target='#collapseOne']").click();

	   //  };

	  	// $scope.toggleTwo = function(flag) {
	   //      $scope.coltwo = flag;
	   //      occupationVal++;
	   //      $("a[data-target='#collapseTwo']").click();
	   //  };

	  	// $scope.toggleThree = function(flag) {
	   //      $scope.colthree = flag;
	   //      coverVal++
	   //      $("a[data-target='#collapseThree']").click()
	   //  };

	   //  $scope.checkDodState = function(){
	   //  	$timeout(function(){
	   //  		dodCheck = $('#dodLabel').hasClass('active');
	   //  		if(dodCheck){
	   //  			$scope.dodFlagErr = false;
	   //  			privacyVal++;
	   //  			if(privacyVal > 0){
	   //  				$scope.togglePrivacy(true);
	   //  			} else{
	   //  				$scope.togglePrivacy(false);
	   //  			}
	   //  			if(contactVal > 0){
	   //  				$scope.toggleContact(true);
	   //  			} else{
	   //  				$scope.toggleContact(false);
	   //  			}
	   //  			if(occupationVal > 0){
	   //  				$scope.toggleTwo(true);
	   //  			}else{
	   //  				$scope.toggleTwo(false);
	   //  			}
	   //  			if(coverVal > 0){
	   //  				$scope.toggleThree(true);
	   //  			}else{
	   //  				$scope.toggleThree(false);
	   //  			}
	   //  		} else{
	   //  			$scope.dodFlagErr = true;
	   //  			$scope.togglePrivacy(false);
	   //  			$scope.toggleContact(false);
	   //  			$scope.toggleTwo(false);
	   //  			$scope.toggleThree(false);
	   //  		}
	   //  	}, 1);
	   //  };

	   //  $scope.checkPrivacyState  = function(){
		  //   if(dodCheck){
	   //  		$timeout(function(){
		  //   		privacyCheck = $('#privacyLabel').hasClass('active');
		  //   		if(privacyCheck){
		  //   			$scope.privacyFlagErr = false;
			 //    		contactVal++;
		  //   			if(contactVal > 0){
		  //   				$scope.toggleContact(true);
		  //   			} else{
		  //   				$scope.toggleContact(false);
		  //   			}
		  //   			if(occupationVal > 0){
		  //   				$scope.toggleTwo(true);
		  //   			}else{
		  //   				$scope.toggleTwo(false);
		  //   			}
		  //   			if(coverVal > 0){
		  //   				$scope.toggleThree(true);
		  //   			}else{
		  //   				$scope.toggleThree(false);
		  //   			}
		  //   		} else{
		  //   			$scope.privacyFlagErr = true;
		  //   			$scope.toggleContact(false);
		  //   			$scope.toggleTwo(false);
		  //   			$scope.toggleThree(false);
		  //   		}
		  //   	}, 1);
		  //   } else{
		  //   	$scope.dodFlagErr = true;
		  //   	$scope.togglePrivacy(false);
				// $scope.toggleContact(false);
				// scope.toggleTwo(false);
    // 			$scope.toggleThree(false);
		  //   }
	   //  };

	    $scope.calculate = function(){
	    	if($scope.requireCover == undefined || $scope.requireCover == ""){
	    		$scope.requireCover = 0;
	    	}
	    	if($scope.TPDRequireCover == undefined || $scope.TPDRequireCover == ""){
	    		$scope.TPDRequireCover = 0;
	    	}
	    	if($scope.IPRequireCover == undefined || $scope.IPRequireCover == ""){
	    		$scope.IPRequireCover = 0;
	    	}
	    	//$scope.getNewOccupation();
//	    	$scope.validateDeathTpdIpAmounts();
	    	$scope.ipRoundOFF();
	    	$scope.defaultingtoexistingcover();
	    	var ruleModel = {
	        		"age": anb,
	        		"fundCode": "HOST",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"ipOccCategory": $scope.ipOccupationCategory,
	        		"smoker": false,
	        		"deathUnits": parseInt($scope.requireCover),
	        		"deathFixedAmount": parseInt($scope.requireCover),
	        		"deathFixedCost": null,
	        		"deathUnitsCost": null,
	        		"tpdUnits": parseInt($scope.TPDRequireCover),
	        		"tpdFixedAmount": parseInt($scope.TPDRequireCover),
	        		"tpdFixedCost": null,
	        		"tpdUnitsCost": null,
	        		"ipUnits": null,
	        		"ipFixedAmount": parseInt($scope.IPRequireCover),
	        		"ipFixedCost": null,
	        		"ipUnitsCost": null,
	        		"premiumFrequency": $scope.premFreq,
	        		"memberType": null,
	        		"manageType": "SCOVER",
	        		"deathCoverType": $scope.coverType,
	        		"tpdCoverType": $scope.tpdCoverType,
	        		"ipCoverType": "IpFixed",
	        		"ipWaitingPeriod": $scope.waitingPeriodAddnl,
	        		"ipBenefitPeriod": $scope.benefitPeriodAddnl
	        	};
	    	CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
	    		var premium = res.data;
	    		dynamicFlag = true;
	    		for(var i = 0; i < premium.length; i++){
	    			if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
	    				$scope.dcCoverAmount = premium[i].coverAmount;
	    				$scope.dcWeeklyCost = premium[i].cost;
	    			} else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
	    				$scope.tpdCoverAmount = premium[i].coverAmount;
	    				$scope.tpdWeeklyCost = premium[i].cost;
	    			} else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
        				$scope.ipCoverAmount = premium[i].coverAmount;
        				$scope.ipWeeklyCost = premium[i].cost;
        			}
        		}
	    		$scope.totalWeeklyCost = parseFloat($scope.dcWeeklyCost)+ parseFloat($scope.tpdWeeklyCost)+parseFloat($scope.ipWeeklyCost);
        		if(fetchAppnum){
        			fetchAppnum = false;
        			appNum = PersistenceService.getAppNumber();
        		}
	    	}, function(err){
	    		console.info("Something went wrong while calculating..." + JSON.stringify(err));
	    	});
        };

        $scope.defaultingtoexistingcover = function(){
	    	$scope.defaultingtoexistingcoverstatus= false;
	    if($scope.requireCover==0 && $scope.totalWeeklyCost==0 && $scope.previousTpdBenefitQue=='No' &&  $scope.previousTerminalillnessQue=='No' ){
	    	$scope.requireCover=parseInt($scope.deathCoverDetails.amount);
	    	$scope.requireCover=parseInt($scope.deathCoverDetails.units);
	    	$scope.TPDRequireCover=	parseInt($scope.tpdCoverDetails.amount);
	    	$scope.TPDRequireCover=parseInt($scope.tpdCoverDetails.units);
	    	$scope.defaultingtoexistingcoverstatus =true;
	    }
        };
	    $scope.convertDeathCover = function(){
	    	//$scope.getNewOccupation();
	    	$scope.renderOccupationQuestions();
	    	var ruleModel = {
	        		"age": anb,
	        		"fundCode": "HOST",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"ipOccCategory": $scope.ipOccupationCategory,
	        		"smoker": false,
	        		"premiumFrequency": $scope.premFreq,
	        		"manageType": "SCOVER",
	        		"exDeathCoverType": $scope.exDcCoverType,
	        		"exTpdCoverType":null,
	        		"deathExistingAmount":$scope.deathCoverDetails.amount,
	        		"tpdExistingAmount":null
	        	};
	    	ConvertService.convert($scope.urlList.convertCoverUrl,ruleModel).then(function(res){
	    	//ConvertService.convert({}, ruleModel, function(res){
	    		var premium = res.data;
	    		for(var i = 0; i < premium.length; i++){
	    			$scope.requireCover = premium[i].convertedAmount;
        		}
        	}, function(err){
        		console.info("Error while converting death amount " + JSON.stringify(err));
        	});
        };
	    $scope.convertTPDCover = function(){
	    	//$scope.getNewOccupation();
	    	$scope.renderOccupationQuestions();
	    	var ruleModel = {
	        		"age": anb,
	        		"fundCode": "HOST",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"ipOccCategory": $scope.ipOccupationCategory,
	        		"smoker": false,
	        		"premiumFrequency": $scope.premFreq,
	        		"manageType": "SCOVER",
	        		"exDeathCoverType": null,
	        		"exTpdCoverType":$scope.exTpdCoverType,
	        		"deathExistingAmount":null,
	        		"tpdExistingAmount":$scope.tpdCoverDetails.amount
	        	};
	    	ConvertService.convert($scope.urlList.convertCoverUrl,ruleModel).then(function(res){
	    	//ConvertService.convert({}, ruleModel, function(res){
	    		var premium = res.data;
	    		for(var i = 0; i < premium.length; i++){
	    			$scope.TPDRequireCover = premium[i].convertedAmount;
        		}
        	}, function(err){
        		console.info("Error while converting TPD amount " + JSON.stringify(err));
        	});
        };
        $scope.customDigest = function() {
        	  if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
        	      $scope.$digest();
        	  }
        	}
    
       $scope.calculateOnChange = function(){
    	    if(dynamicFlag && !$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag ){
    	    	$scope.calculate();
    	   } 
       };
       $scope.calculate15hours = function(){
    	   if($scope.fifteenHrsQuestion == 'No'){
			   $scope.ipCoverName = 'No change';
				$scope.isIPCoverNameDisabled = true;
				$scope.isWaitingPeriodDisabled = true;
			    $scope.ipWarning = true;
				$scope.waitingPeriodAddnl = '30 Days';
				$scope.isBenefitPeriodDisabled = true;
				$scope.IPRequireCover = $scope.ipCoverDetails.amount;
				$scope.isIPCoverRequiredDisabled = true;
				$scope.isIpSalaryCheckboxDisabled = true;
				$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
				$scope.ipWarningFlag = false;
			}
		   else if ($scope.fifteenHrsQuestion == 'Yes'){
			   $scope.ipCoverName = 'No change';
			   $scope.IPRequireCover = $scope.ipCoverDetails.amount;
  				$scope.isIPCoverNameDisabled = false;
  				$scope.isWaitingPeriodDisabled = false;
  				$scope.isBenefitPeriodDisabled = false;
  			    $scope.ipWarning = false;
  				$scope.isIPCoverRequiredDisabled = false;
  				$scope.isIpSalaryCheckboxDisabled = false;
  				if($scope.occupation)
				{
					$scope.isOccElegibleForIP();
				}
		   }
    	   $scope.getCategoryFromDB();
    	   $scope.renderOccupationQuestions();
//    	   $scope.withinOfficeQue = null;
//    	    $scope.tertiaryQue = null;
//    	    $scope.hazardousQue = null;
//    	    $scope.outsideOffice = null;
    	
       }
       
       
       $scope.isOccElegibleForIP = function(){
      		var occ = $scope.occupation;
      		if(occ=="Other"){
      			occ = $scope.otherOccupationObj.otherOccupation; 
      		}
      		var occSearch = true;
      		propertyServiceSpecial.getOccList().then(function(response){
      			 $scope.decOccList = response.data;
      			 angular.forEach($scope.decOccList,function(value,key){
      				  if(occSearch){
       		    		  if(key.toLowerCase() === occ.toLowerCase()){
       		    			  if(value.toLowerCase() == 'ip'){
       		    				  	$scope.ipCoverName = 'No change';
       		    					$scope.isIPCoverNameDisabled = true;
       		    					$scope.isWaitingPeriodDisabled = true;
       		    				    $scope.ipWarningOcc = true;
       		    					$scope.waitingPeriodAddnl = '30 Days';
       		    					$scope.isBenefitPeriodDisabled = true;
       		    					$scope.IPRequireCover = $scope.ipCoverDetails.amount;
       		    					$scope.isIPCoverRequiredDisabled = true;
       		    					$scope.isIpSalaryCheckboxDisabled = true;
       		    					$('#ipsalarycheck').removeAttr('checked');
       		    					$('#ipsalarychecklabel').removeClass('active');
       		    					$scope.ipWarningFlag = false;
       			    			    occSearch = false;
       		    			  }
       		    		  }else if($scope.fifteenHrsQuestion == 'Yes'){
       		    			  	$scope.ipCoverName = 'No change';
       		    			  	$scope.IPRequireCover = $scope.ipCoverDetails.amount;
       		     				$scope.isIPCoverNameDisabled = false;
       		     				$scope.isWaitingPeriodDisabled = false;
       		     				$scope.isBenefitPeriodDisabled = false;
       		     			    $scope.ipWarningOcc = false;
       		     				$scope.isIPCoverRequiredDisabled = false;
       		     				$scope.isIpSalaryCheckboxDisabled = false;
       				      }
       				  }
      			 });
      		});
      	  };       

       // Progressive validation
	    $scope.checkPreviousMandatoryFields  = function (elementName,formName){
	    	var formFields,CoverCalculatorFormFields;
	    	if(formName == 'formOne'){
	    		formFields = FormOneFields;
	    	} else if(formName == 'occupationForm'){
	    		if($scope.occupation != undefined && $scope.occupation == 'Other'){
	    			formFields = OccupationOtherFormFields;
	    		} else{
	    			formFields = OccupationFormFields;
	    		}
	    	} else if(formName == 'coverCalculatorForm'){
	    		if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'Yes'){
	    			CoverCalculatorFormFields =['previousTpdBenefitQue','previousTerminalillnessQue','coverType','requireCover'];
	    			$scope.calculateweeklycost();
	    		}
	    		else	if($scope.previousTpdBenefitQue != undefined && $scope.previousTpdBenefitQue == 'No'){
	    			CoverCalculatorFormFields =['previousTpdBenefitQue','previousTerminalillnessQue','coverType','requireCover'];
	    			$scope.calculateweeklycost();
	    		}
	    		else if($scope.previousTerminalillnessQue != undefined && $scope.previousTerminalillnessQue == 'Yes'){
	    			 CoverCalculatorFormFields =['previousTpdBenefitQue','previousTerminalillnessQue','coverType','requireCover'];
	    			 $scope.calculateweeklycost();
	    		}
	    		
	    		else{
	    			CoverCalculatorFormFields = ['previousTpdBenefitQue','previousTerminalillnessQue','coverType','requireCover','tpdCoverType','TPDRequireCover','IPRequireCover'];
	    		}
	    		formFields = CoverCalculatorFormFields;
	    	}
	      var inx = formFields.indexOf(elementName);
	      if(inx > 0){
	        for(var i = 0; i < inx ; i++){
	          $scope[formName][formFields[i]].$touched = true;
	        }
	      }
	    };

	    $scope.calculateweeklycost = function(){
	    	if( $scope.totalWeeklyCost != 0 && $scope.previousTpdBenefitQue == 'Yes'){
	    	  	$scope.requireCover = '';
	    		$scope.IPRequireCover ='';
	    	$scope.TPDRequireCover = 0;
	    	$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
				$scope.ipWarningFlag = false;
				$scope.ipcheckbox=false;
	    		  $scope.calculate();
	    	}
	    	else if( $scope.totalWeeklyCost != 0 && $scope.previousTpdBenefitQue == 'No' && $scope.dcWeeklyCost== $scope.totalWeeklyCost && $scope.previousTerminalillnessQue == 'No'){
	    		$scope.requireCover = '';
	    	  	$scope.dcWeeklyCost='';
	    	  	$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
				$scope.ipcheckbox=false;
				$scope.ipWarningFlag = false;
	    		  $scope.calculate();
	    	}
	    	else if($scope.totalWeeklyCost!=0 && $scope.previousTerminalillnessQue == 'Yes'){
	    	  	$scope.requireCover = 0;
		    	$scope.TPDRequireCover = 0;
		    	$scope.IPRequireCover=0;
		    	$('#ipsalarycheck').removeAttr('checked');
   				$('#ipsalarychecklabel').removeClass('active');
   				$scope.ipWarningFlag = false;
	    		  $scope.calculate();
	    	}
	    };
	    
	    
	    $scope.getCategoryFromDB = function(fromSelect){
	    	
		    	/*if(fromSelect && $scope.occupation!=="Other"){
		    		$scope.otherOccupationObj.otherOccupation = '';
				}*/
		    	/*if( $scope.prevOtherOcc !== $scope.otherOccupationObj.otherOccupation){*/
		    	if($scope.occupation != undefined /*|| ($scope.industry && $scope.otherOccupationObj.otherOccupation != undefined)*/){
		    		 if(fromSelect) {
		    		        $scope.withinOfficeQuestion = null;
		    		        $scope.tertiaryQuestion = null;
		    		        $scope.hazardousQuestion = null;
		    		        $scope.outsideOffice = null;                                                     
		    		      }
			    	var occName = $scope.industry + ":" + $scope.occupation;
		    		/*if($scope.occupation != undefined && ($scope.otherOccupationObj.otherOccupation == null && $scope.otherOccupationObj.otherOccupation == '')){
		      		  var occName = $scope.industry + ":" + $scope.occupation;
		            }else if ($scope.otherOccupationObj.otherOccupation != undefined){
		            	if(!($scope.otherOccupationObj.otherOccupation == ''))
		            		{
		            		$scope.prevOtherOcc = $scope.otherOccupationObj.otherOccupation;
		            		}
		          	  	if(($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.otherOccupation))!== undefined){
		            		var occName = $scope.industry + ":" + $scope.otherOccupationObj.otherOccupation;
		            	}else{
		            		var occName = $scope.industry + ":" + $scope.occupation;
		            	}
		          	  
		            }*/
			    	NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "HOST", occName).then(function(res){
			    		  if($scope.coverType == "DcFixed"){
			    			  deathDBCategory = res.data[0].deathfixedcategeory;
			    	        }else if($scope.coverType == "DcUnitised"){
			    	        	deathDBCategory = res.data[0].deathunitcategeory;
			    	        }
			    	        if($scope.tpdCoverType == "TPDFixed"){
			    	        	tpdDBCategory = res.data[0].tpdfixedcategeory;
			    	        }else if($scope.tpdCoverType == "TPDUnitised"){
			    	        	tpdDBCategory = res.data[0].tpdunitcategeory;
			    	        }    		
			    		
			    		ipDBCategory = res.data[0].ipfixedcategeory;
			    	       $scope.renderOccupationQuestions();
			    	       if ($scope.fifteenHrsQuestion == 'Yes'){
				    	       $scope.isOccElegibleForIP();
				    	       }
			    	}, function(err){
			    		console.info("Error while getting category from DB " + JSON.stringify(err));
			    	});
		    	}
	    	
	    	/*}*/
	    };

	    $scope.renderOccupationQuestions = function(){
	    	/*if($scope.fifteenHrsQuestion == 'Yes'){*/
	    		if($scope.occupation/* || $scope.otherOccupationObj.otherOccupation*/){
		  			var selectedOcc = $scope.OccupationList.filter(function(obj){
			  			return obj.occupationName == $scope.occupation;
		  				/*if($scope.occupation && $scope.otherOccupationObj.otherOccupation == ''){
		                    return obj.occupationName == $scope.occupation;
			           	}else if($scope.otherOccupationObj.otherOccupation && (($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.otherOccupation))!== undefined)){
			           		return obj.occupationName == $scope.otherOccupationObj.otherOccupation;
			           	}else{
			           		return obj.occupationName == $scope.occupation;
			           	}*/
			  		});
			  		var selectedOccObj = selectedOcc[0];
			  		if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
			  			$scope.showWithinOfficeQuestion = true;
			  			/*$scope.showWithinOfficeQuestion = false;*/
			  		    $scope.showTertiaryQuestion = true;
			  		    $scope.showHazardousQuestion = false;
			  		    $scope.showOutsideOfficeQuestion = false;
			  		    /*$scope.showOutsideOfficeQuestion = true;*/
			  		    if($scope.ownBussinessQuestion == 'Yes'){
					    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
					    } else if($scope.ownBussinessQuestion == 'No'){
					    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
					    }
			  		    /*if($scope.withinOfficeQuestion == 'No' && $scope.tertiaryQuestion == 'Yes' && $scope.annualSalary && parseFloat($scope.annualSalary) >= parseFloat(annualSalForUpgradeVal)){*/
			  		  if($scope.withinOfficeQuestion == 'Yes' && $scope.tertiaryQuestion == 'Yes' && $scope.annualSalary && parseFloat($scope.annualSalary) >= parseFloat(annualSalForUpgradeVal)){
			  		    	$scope.deathOccupationCategory = 'Professional';
		  		    		$scope.tpdOccupationCategory = 'Professional';
		  		    		$scope.ipOccupationCategory = 'Professional';
		  		    	} else{
		  		    		$scope.deathOccupationCategory = deathDBCategory;
		  		    		$scope.tpdOccupationCategory = tpdDBCategory;
		  		    		$scope.ipOccupationCategory = ipDBCategory;
		  		    	}
			  		}
			  			
			  		else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  			$scope.showWithinOfficeQuestion = false;
			  		    $scope.showTertiaryQuestion = false;
			  		    $scope.showHazardousQuestion = true;
			  		    $scope.showOutsideOfficeQuestion = true;
			  		    if($scope.ownBussinessQuestion == 'Yes'){
					    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
					    } else if($scope.ownBussinessQuestion == 'No'){
					    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
					    }
			  		    /*if($scope.hazardousQuestion == 'No' && $scope.outsideOffice == 'No'){*/
			  		  if($scope.hazardousQuestion == 'No' && $scope.outsideOffice == 'Yes'){
			  		    	$scope.showWithinOfficeQuestion = true;
			  	  		    $scope.showTertiaryQuestion = true;
				  	  		if($scope.ownBussinessQuestion == 'Yes'){
						    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
						    } else if($scope.ownBussinessQuestion == 'No'){
						    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
						    }

					  	  	$scope.deathOccupationCategory = 'White Collar';
		  		    		$scope.tpdOccupationCategory = 'White Collar';
		  		    		$scope.ipOccupationCategory = 'White Collar';
		  		    		/*if($scope.withinOfficeQuestion == 'No' && $scope.tertiaryQuestion == 'Yes' && $scope.annualSalary && parseFloat($scope.annualSalary) >= parseFloat(annualSalForUpgradeVal)){*/
		  		    		if($scope.withinOfficeQuestion == 'Yes' && $scope.tertiaryQuestion == 'Yes' && $scope.annualSalary && parseFloat($scope.annualSalary) >= parseFloat(annualSalForUpgradeVal)){
		  		    			$scope.deathOccupationCategory = 'Professional';
			  		    		$scope.tpdOccupationCategory = 'Professional';
			  		    		$scope.ipOccupationCategory = 'Professional';
			  		    	} else{
			  		    		$scope.deathOccupationCategory = 'White Collar';
			  		    		$scope.tpdOccupationCategory = 'White Collar';
			  		    		$scope.ipOccupationCategory = 'White Collar';
			  		    	}
			  		    } else{
			  		    	$scope.showWithinOfficeQuestion = false;
			  	  		    $scope.showTertiaryQuestion = false;
				  	  		if($scope.ownBussinessQuestion == 'Yes'){
						    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
						    } else if($scope.ownBussinessQuestion == 'No'){
						    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
						    }

					  	  	$scope.deathOccupationCategory = deathDBCategory;
		  		    		$scope.tpdOccupationCategory = tpdDBCategory;
		  		    		$scope.ipOccupationCategory = ipDBCategory;
			  		    }
			  		} else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  			$scope.showWithinOfficeQuestion = false;
			  		    $scope.showTertiaryQuestion = false;
			  		    $scope.showHazardousQuestion = true;
			  		    $scope.showOutsideOfficeQuestion = true;
			  		    if($scope.ownBussinessQuestion == 'Yes'){
					    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
					    } else if($scope.ownBussinessQuestion == 'No'){
					    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
					    }

				  		  /*if($scope.hazardousQuestion == 'No' && $scope.outsideOffice == 'No'){*/
			  		      if($scope.hazardousQuestion == 'No' && $scope.outsideOffice == 'Yes'){
					  			$scope.deathOccupationCategory = 'White Collar';
			  		    		$scope.tpdOccupationCategory = 'White Collar';
			  		    		$scope.ipOccupationCategory = 'White Collar';
				  		  } else{
					  			$scope.deathOccupationCategory = deathDBCategory;
			  		    		$scope.tpdOccupationCategory = tpdDBCategory;
			  		    		$scope.ipOccupationCategory = ipDBCategory;
				  		  }
			  		} else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
			  			$scope.showWithinOfficeQuestion = false;
			  		    $scope.showTertiaryQuestion = false;
			  		    $scope.showHazardousQuestion = false;
			  		    $scope.showOutsideOfficeQuestion = false;
			  		    OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];

			  		    $scope.deathOccupationCategory = deathDBCategory;
	  		    		$scope.tpdOccupationCategory = tpdDBCategory;
	  		    		$scope.ipOccupationCategory = ipDBCategory;
			  		}
		  		}
		  	/*}*/ else{
		  		$scope.showWithinOfficeQuestion = false;
	  		    $scope.showTertiaryQuestion = false;
	  		    $scope.showHazardousQuestion = false;
	  		    $scope.showOutsideOfficeQuestion = false;
	  		    OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];

	  		    if(deathDBCategory == 'White Collar'){
		  		    $scope.deathOccupationCategory = 'Standard';
	  		    } else if(deathDBCategory == 'Professional'){
	  		    	$scope.deathOccupationCategory = 'Professional';
	  		    } else{
	  		    	$scope.deathOccupationCategory = 'Standard';
	  		    }

	  		    if(tpdDBCategory == 'White Collar'){
		  		    $scope.tpdOccupationCategory = 'Standard';
	  		    } else if(tpdDBCategory == 'Professional'){
	  		    	$scope.tpdOccupationCategory = 'Professional';
	  		    } else{
	  		    	$scope.tpdOccupationCategory = 'Standard';
	  		    }

	  		  if(ipDBCategory == 'White Collar'){
		  		    $scope.ipOccupationCategory = 'Standard';
	  		    } else if(ipDBCategory == 'Professional'){
	  		    	$scope.ipOccupationCategory = 'Professional';
	  		    } else{
	  		    	$scope.ipOccupationCategory = 'Standard';
	  		    }
		  	}
		  	 $scope.customDigest();
		     var _this = this;
		     $timeout(function(){
		       if(_this.formOne.$valid && _this.occupationForm.$valid && _this.coverCalculatorForm.$valid){
		         $scope.calculate();
		       }
		     });
	  	};

	   // validate fields "on continue"
	    $scope.formOneSubmit =  function (form){
	    	if(!form.$valid){
	    	  form.$submitted=true;
	    	  if(form.$name == 'formOne'){
				  $scope.toggleTwo(false);
				  $scope.toggleThree(false);
			  }else if(form.$name == 'occupationForm'){
				  $scope.toggleThree(false);
			  }
		  } else{
			  if(form.$name == 'formOne'){
				  $scope.toggleTwo(true);
			  }else if(form.$name == 'occupationForm'){
				  $scope.toggleThree(true);
			  }else if(form.$name == 'coverCalculatorForm'){
				  if (!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag){
//					  if($scope.IPRequireCover!=0 && $scope.requireCover==0){
//						  $scope.requireCover=$scope.deathCoverDetails.amount;
//						  $scope.TPDRequireCover=$scope.tpdCoverDetails.amount;
//					  }
					  $scope.calculate();
				 }
			  }
	       }
	    };

	    
	    $scope.checkAckState = function(){
	    	$timeout(function(){
	    		ackCheck = $('#termsLabel').hasClass('active');
	    		if(ackCheck){
	    			$scope.ackFlag = false;
	    		} else{
	    			$scope.ackFlag = true;
	    		}
	    	}, 10);
	    };

	    $scope.showHelp = function(msg){
	    	$scope.modalShown = !$scope.modalShown;
	    	$scope.tipMsg = msg;
	    };

	    $scope.clickToOpen = function (hhText) {

	  		var dialog = ngDialog.open({
	  			/*template: '<p>'+hhText+'</p>' +
	  				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
	  			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" style="width: 100%;color: #000 !important;font-weight: normal !important; font-size: 10pt !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Close</button></div><div class="col-sm-4"></div></div></div>',
	  				className: 'ngdialog-theme-plain',
	  				plain: true
	  		});
	  		dialog.closePromise.then(function (data) {
	  			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
	  		});
	  	};

	    $scope.saveDataForPersistence = function(){

	    	var coverObj = {};
	    	var coverStateObj = {};
	    	var specialCoverOccObj = {};
	    	var deathAddnlCoverObj={};
	    	var tpdAddnlCoverObj={};
	    	var ipAddnlCoverObj={};
	    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
	    		return $scope.industry == obj.key;
	    	});


	    			coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
	    			coverObj['firstName'] = $scope.personalDetails.firstName;
	    			coverObj['lastName'] = $scope.personalDetails.lastName;
	    	    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
	    	    	coverObj['email'] = $scope.email;
	    	    	coverObj['contactType'] = $scope.preferredContactType;
	    	    	coverObj['contactPhone'] = $scope.changeCvrPhone;
	    	    	coverObj['contactPrefTime'] = $scope.time;


	    	    	specialCoverOccObj['gender'] = $scope.gender;
	    	    	specialCoverOccObj['fifteenHr'] = $scope.fifteenHrsQuestion;
	    	    	specialCoverOccObj['citizenQue'] = $scope.areyouperCitzQuestion;
	    	    	specialCoverOccObj['industryName'] = selectedIndustry[0].value;
	    	    	specialCoverOccObj['industryCode'] = selectedIndustry[0].key;
	    	    	specialCoverOccObj['occupation'] = $scope.occupation;
	    	    	
	    	    	if(!$scope.showWithinOfficeQuestion){
	    	    		$scope.withinOfficeQuestion = null;
	    	    	}
	    	    	
	    	    	if(!$scope.showTertiaryQuestion){
	    	    		$scope.tertiaryQuestion = null;
	    	    	}
	    	    	
	    	    	if(!$scope.showHazardousQuestion){
	    	    		$scope.hazardousQuestion = null;
	    	    	}
	    	    	
	    	    	if(!$scope.showOutsideOfficeQuestion){
	    	    		$scope.outsideOffice = null;
	    	    	}
	    	    	
	    	    	/*if( !($scope.showWithinOfficeQuestion && $scope.showTertiaryQuestion)){
	    	    		$scope.withinOfficeQuestion = null;
	    		        $scope.tertiaryQuestion = null;
	              	}
	                if(!$scope.showHazardousQuestion && !$scope.showOutsideOfficeQuestion){
	                	$scope.hazardousQuestion = null;
	    		        $scope.outsideOffice = null;
	              	}*/
	    	    	
	    	    	specialCoverOccObj['withinOfficeQue']= $scope.withinOfficeQuestion;
	    	    	specialCoverOccObj['tertiaryQue']= $scope.tertiaryQuestion;
	    	    	specialCoverOccObj['managementRoleQue']= $scope.outsideOffice;
	    	    	specialCoverOccObj['hazardousQue']= $scope.hazardousQuestion;
	    	    	specialCoverOccObj['ownBussinessQues']= $scope.ownBussinessQuestion;
	    	    	specialCoverOccObj['ownBussinessYesQues']= $scope.ownBussinessYesQuestion;
	    	    	specialCoverOccObj['ownBussinessNoQues']= $scope.ownBussinessNoQuestion;
	    	    	specialCoverOccObj['salary'] = $scope.annualSalary;
	    	    	specialCoverOccObj['otherOccupation'] = $scope.otherOccupationObj.otherOccupation;

	    	    	coverObj['existingDeathAmt'] = parseFloat($scope.deathCoverDetails.amount);
	    	    	coverObj['existingDeathUnits'] = parseInt($scope.deathCoverDetails.units);
	    	    	coverObj['deathOccCategory'] = $scope.deathOccupationCategory;
	    	    	coverObj['previousTpdBenefit'] = $scope.previousTpdBenefitQue;
	    	    	coverObj['previousTerminalillness'] = $scope.previousTerminalillnessQue;
	    	      	coverObj['terminalIllClaimQue'] = $scope.previousTerminalillnessQue;
	    	    	deathAddnlCoverObj['deathCoverType'] = $scope.coverType;
	    	    	deathAddnlCoverObj['deathFixedAmt'] = parseFloat($scope.dcCoverAmount);
	    	    	deathAddnlCoverObj['deathInputTextValue'] = parseFloat($scope.requireCover);
	    	    	deathAddnlCoverObj['deathCoverPremium'] = parseFloat($scope.dcWeeklyCost);
	    	    	deathAddnlCoverObj['coverTypeTouch']=$scope.coverTypeTouch;
	                coverObj['existingTpdAmt'] = parseFloat($scope.tpdCoverDetails.amount);
	                coverObj['existingTPDUnits'] = parseInt($scope.tpdCoverDetails.units);
	                coverObj['tpdOccCategory'] = $scope.tpdOccupationCategory;
	                tpdAddnlCoverObj['tpdCoverType'] = $scope.tpdCoverType;
	                tpdAddnlCoverObj['tpdFixedAmt'] = parseFloat($scope.tpdCoverAmount);
	                tpdAddnlCoverObj['tpdInputTextValue'] = parseFloat($scope.TPDRequireCover);
	                tpdAddnlCoverObj['tpdCoverPremium'] = $scope.tpdWeeklyCost;

	                coverObj['existingIPUnits'] = parseInt($scope.ipCoverDetails.units);
	                coverObj['existingIpAmount'] = parseFloat($scope.ipCoverDetails.amount);
	                coverObj['ipOccCategory'] = $scope.ipOccupationCategory;
	                ipAddnlCoverObj['ipCoverType'] = 'IpFixed';
	                ipAddnlCoverObj['waitingPeriod'] = $scope.waitingPeriodAddnl;
	                ipAddnlCoverObj['benefitPeriod'] = $scope.benefitPeriodAddnl;
	                ipAddnlCoverObj['ipFixedAmt'] = parseFloat($scope.ipCoverAmount);
	                ipAddnlCoverObj['ipInputTextValue'] = parseFloat($scope.IPRequireCover);
	                ipAddnlCoverObj['ipCoverPremium'] = parseFloat($scope.ipWeeklyCost);

	                coverObj['totalPremium'] = parseFloat($scope.totalWeeklyCost);
	                coverObj['appNum'] = parseInt(appNum);
	                coverObj['auraDisabled'] = $scope.auraDisabled;
	                coverObj['ackCheck'] = ackCheck;
	                coverObj['dodCheck'] = $('#dodLabel').hasClass('active');
	                coverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');
	                coverObj['age'] = anb;
	                coverObj['manageType'] = 'SCOVER';
	                coverObj['partnerCode'] = 'HOST';
	                coverObj['freqCostType'] = $scope.premFreq;
	                coverObj['ipcheckbox']= $scope.ipcheckbox;
	                coverObj['existingIPWaitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
	                coverObj['existingIPBenefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;
		            coverObj['lastSavedOn'] = 'SpecialQuotepage';

	                coverStateObj['dynamicFlag'] = dynamicFlag;
	                coverStateObj['showWithinOfficeQuestion'] = $scope.showWithinOfficeQuestion;
	                coverStateObj['showTertiaryQuestion'] = $scope.showTertiaryQuestion;
	                coverStateObj['showHazardousQuestion'] = $scope.showHazardousQuestion;
	                coverStateObj['showOutsideOfficeQuestion'] = $scope.showOutsideOfficeQuestion;

	    	    	PersistenceService.setChangeCoverDetails(coverObj);
	    	    	PersistenceService.setChangeCoverStateDetails(coverStateObj);
	    	    	PersistenceService.setChangeCoverOccDetails(specialCoverOccObj);
	    	    	PersistenceService.setDeathAddnlCoverDetails(deathAddnlCoverObj);
	    	    	PersistenceService.setTpdAddnlCoverDetails(tpdAddnlCoverObj);
	    	    	PersistenceService.setIpAddnlCoverDetails(ipAddnlCoverObj);

	    };
	   /*$scope.showSaveOrExitPopUp = function (){
	    	var saveText = '<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>';
	   		if(saveText == null || saveText == "" || saveText == " "){
	   			hideSaveExitTips();
	   		}else{
	   		 document.getElementById('mymodalSaveExit').style.display = 'block';
	   		 document.getElementById('mymodalSaveExitFade').style.display = 'block';
	   		 document.getElementById('saveExitMsg_text').innerHTML = saveText;
	   		}
	   		//$scope.saveQuote();
	   	}

	    $scope.hideSaveExitTips = function  (){
	   		if(document.getElementById('help_div')){
	   			document.getElementById('help_div').style.display = "none";
	   		}
	   	}*/
	    $scope.saveQuote =function(){
	     // $scope.showSaveOrExitPopUp();
	    	$scope.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR>');
	    	$scope.saveDataForPersistence();
    		var quoteObject =  PersistenceService.getChangeCoverDetails();
    		var changeCoverOccDetails = PersistenceService.getChangeCoverOccDetails();
    		var deathAdditionalDetails = PersistenceService.getDeathAddnlCoverDetails();
    		var tpdAdditionalDetails = PersistenceService.getTpdAddnlCoverDetails();
    		var ipAdditionalDetails = PersistenceService.getIpAddnlCoverDetails();
    	    var personalInfo = persoanlDetailService.getMemberDetails();
    	    if(quoteObject != null && changeCoverOccDetails != null && deathAdditionalDetails != null && tpdAdditionalDetails != null && ipAdditionalDetails != null && personalInfo != null ){
    	    	var details={};
        		details.addnlDeathCoverDetails = deathAdditionalDetails;
    			details.addnlTpdCoverDetails = tpdAdditionalDetails;
    			details.addnlIpCoverDetails = ipAdditionalDetails;
    			details.occupationDetails = changeCoverOccDetails;
    			var temp = angular.extend(details,quoteObject);
        	    var saveQuoteObject = angular.extend(temp, personalInfo);
    	    	auraResponseService.setResponse(saveQuoteObject)
    	    	$rootScope.$broadcast('disablepointer');
    	    	//console.log(JSON.stringify(saveQuoteObject));
    	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
    	                console.log(response.data)
    	        });
    	    }
	    };

       $scope.quoteSaveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
					template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};

	    $scope.goToAura =function(){
	        this.formOne.$submitted = true;
	        this.occupationForm.$submitted = true;
	        this.coverCalculatorForm.$submitted = true;
	            $scope.validateDeathTpdIpAmounts();
	    	if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
		    	if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag){
		    		$timeout(function(){
			    			/*navigate to Aura*/
						    	$scope.continueToNextPage('/auraspecialoffer/1');
					    	/*navigate to Aura*/
				    	}, 10);
			    	}
	    	}else{
	    		return false;
	    	}
	    };
	    $scope.goToSummarydecline =function(){
	    	$scope.saveDataForPersistence();
      			$scope.details={};
      			 var status = {};
      			 var ques ={};
      			$scope.personalDetails = persoanlDetailService.getMemberDetails();
      			if($scope.previousTerminalillnessQue != undefined && $scope.previousTerminalillnessQue == 'Yes'){
      				status.deathDecision='DCL';
         			status.ipDecision='DCL';
         			status.tpdDecision='DCL';
         			
      			}
      			ques.previousTpdClaimQue=$scope.previousTpdBenefitQue;
      			ques.terminalIllClaimQue=$scope.previousTerminalillnessQue;
      			ques.overallDecision='DCL';
      			ques.appNum=PersistenceService.getAppNumber();
      			ques.deathAuraResons='Previously received, applied for, are eligible, or in the process of applying for a Terminal Illness benefit  from any source';
      			ques.tpdAuraResons='Previously received, applied for, are eligible, or in the process of applying for a Terminal Illness benefit  from any source';
      			ques.ipAuraResons='Previously received, applied for, are eligible, or in the process of applying for a Terminal Illness benefit from any source';
      		  	$scope.coverDetails = PersistenceService.getChangeCoverDetails();
      			$scope.details.occupationDetails =PersistenceService.getChangeCoverOccDetails();
      			$scope.details.addnlDeathCoverDetails=PersistenceService.getDeathAddnlCoverDetails();
      			$scope.details.addnlDeathCoverDetails.deathInputTextValue= 	0;
      			$scope.details.addnlDeathCoverDetails.deathFixedAmt=	0;
      			$scope.details.addnlTpdCoverDetails=PersistenceService.getTpdAddnlCoverDetails();
      			$scope.details.addnlTpdCoverDetails.tpdInputTextValue=	0;
      			$scope.details.addnlTpdCoverDetails.tpdFixedAmt=	0;
      			$scope.details.addnlIpCoverDetails=PersistenceService.getIpAddnlCoverDetails();
      			$scope.details.addnlIpCoverDetails.ipInputTextValue=0;
      			$scope.details.transferDocuments = PersistenceService.getUploadedFileDetails();
	    		if($scope.coverDetails != null && $scope.details.addnlDeathCoverDetails != null && $scope.details.addnlTpdCoverDetails != null && $scope.details.addnlIpCoverDetails != null &&
	    				$scope.details.occupationDetails != null && $scope.personalDetails != null){
		      			var coverObject = angular.extend($scope.details,$scope.coverDetails);
		      			var auraObject = angular.extend(coverObject, status);
		      			var submitObject = angular.extend(auraObject,$scope.personalDetails);
		      			var decision=angular.extend(submitObject,ques);
		      			auraResponseService.setResponse(decision);
		      			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
		              		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
		              		PersistenceService.setNpsUrl(response.data.npsTokenURL);
	                  		$scope.go('/specialofferdecline');
		              	}, function(err){
			            		console.log('Error while submitting Special cover ' + err);
			            	});
		              }
	    	else{
	    		return false;
	    	}
	    };


	    if($routeParams.mode == 2){
	    	var existingDetails = PersistenceService.getChangeCoverDetails();
	    	var stateDetails = PersistenceService.getChangeCoverStateDetails();
	    	var occupationDetails = PersistenceService.getChangeCoverOccDetails();
	    	var deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	    	var tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
	    	var ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();

	    	$scope.email = existingDetails.email;
	    	$scope.preferredContactType=existingDetails.contactType;
	    	$scope.changeCvrPhone = existingDetails.contactPhone;
	    	$scope.time = existingDetails.contactPrefTime;
	    	$scope.auraDisabled=existingDetails.auraDisabled;
	    	$scope.previousTpdBenefitQue = existingDetails.previousTpdBenefit;
	    	$scope.previousTerminalillnessQue = existingDetails.previousTerminalillness;
	    	$scope.coverTypeTouch=deathAddnlDetails.coverTypeTouch;
	    	//Occupation Details
	    	$scope.fifteenHrsQuestion = occupationDetails.fifteenHr;
	    	$scope.areyouperCitzQuestion = occupationDetails.citizenQue;
	    	$scope.industry = occupationDetails.industryCode;
//	    	$scope.occupation = occupationDetails.occupation;
	    	$scope.ownBussinessQuestion = occupationDetails.ownBussinessQues;
	    	$scope.ownBussinessYesQuestion = occupationDetails.ownBussinessYesQues;
	    	$scope.ownBussinessNoQuestion = occupationDetails.ownBussinessNoQues;
	    	$scope.withinOfficeQuestion = occupationDetails.withinOfficeQue;
	    	$scope.tertiaryQuestion = occupationDetails.tertiaryQue;
	    	$scope.outsideOffice = occupationDetails.managementRoleQue;
	    	$scope.hazardousQuestion = occupationDetails.hazardousQue;
	    	$scope.gender = occupationDetails.gender;
	    	$scope.premFreq = existingDetails.freqCostType;
	    	$scope.annualSalary = occupationDetails.salary;
	    	$scope.otherOccupationObj.otherOccupation = occupationDetails.otherOccupation;

	    	$scope.deathCoverDetails.amount = existingDetails.existingDeathAmt;
	    	$scope.deathCoverDetails.units = existingDetails.existingDeathUnits;
	    	$scope.deathOccupationCategory = existingDetails.deathOccCategory;

	    	$scope.coverName = deathAddnlDetails.deathCoverName;
	    	$scope.coverType = deathAddnlDetails.deathCoverType;
	    	$scope.dcCoverAmount = deathAddnlDetails.deathFixedAmt;
	    	$scope.requireCover = deathAddnlDetails.deathInputTextValue;
	    	$scope.dcWeeklyCost = deathAddnlDetails.deathCoverPremium;

	    	$scope.tpdCoverDetails.amount = existingDetails.existingTpdAmt;
	    	$scope.tpdCoverDetails.units = existingDetails.existingTPDUnits;
	    	$scope.tpdOccupationCategory = existingDetails.tpdOccCategory;

	    	$scope.tpdCoverName = tpdAddnlDetails.tpdCoverName;
	    	$scope.tpdCoverType = tpdAddnlDetails.tpdCoverType;
	    	$scope.tpdCoverAmount = tpdAddnlDetails.tpdFixedAmt;
	    	$scope.TPDRequireCover = tpdAddnlDetails.tpdInputTextValue;
	    	$scope.tpdWeeklyCost = tpdAddnlDetails.tpdCoverPremium;

	    	$scope.ipCoverDetails.units = existingDetails.existingIPUnits;
	    	$scope.ipOccupationCategory = existingDetails.ipOccCategory;
	    	if(existingDetails.ipcheckbox){
	    		$('#ipsalarycheck').prop('checked','true');
	    		$('#ipsalarycheck').parent().addClass("active");
	    	      $scope.isIPCoverRequiredDisabled = true;
	    	      $scope.insureNinetyPercentIp();
	    	}
	    	if($scope.fifteenHrsQuestion =='No'){
	    		 $scope.calculate15hours();
	    	}
	   
	    	$scope.ipCoverName = ipAddnlDetails.ipCoverName;
	    	$scope.waitingPeriodAddnl = ipAddnlDetails.waitingPeriod;
	    	$scope.benefitPeriodAddnl = ipAddnlDetails.benefitPeriod;
	    	$scope.ipCoverAmount = ipAddnlDetails.ipFixedAmt;
	    	$scope.IPRequireCover = ipAddnlDetails.ipInputTextValue;
	    	$scope.ipWeeklyCost = ipAddnlDetails.ipCoverPremium;
	    	$scope.totalWeeklyCost = existingDetails.totalPremium;
	    	appNum = existingDetails.appNum;
	    	ackCheck = existingDetails.ackCheck;
	    	dodCheck = existingDetails.dodCheck;
	    	privacyCheck = existingDetails.privacyCheck;
	    	dynamicFlag = stateDetails.dynamicFlag;
	    	$scope.showWithinOfficeQuestion = stateDetails.showWithinOfficeQuestion;
	    	$scope.showTertiaryQuestion = stateDetails.showTertiaryQuestion;
	    	$scope.showHazardousQuestion = stateDetails.showHazardousQuestion;
	    	$scope.showOutsideOfficeQuestion = stateDetails.showOutsideOfficeQuestion;
	    	//$scope.renderOccupationQuestions();
	     	$scope.validateDeathTpdIpAmounts();
	 
	    	if($scope.coverType == "DcUnitised"){
	    		showhide('nodollar1','dollar1');
				showhide('nodollar','dollar');
	    	} else if($scope.coverType == "DcFixed"){
	    		showhide('dollar1','nodollar1');
	    		showhide('dollar','nodollar');
	    	}

	    	OccupationService.getOccupationList($scope.urlList.occupationUrl, "HOST", $scope.industry).then(function(res){
	    		$scope.OccupationList = res.data;
	    		
	    		var temp = $scope.OccupationList.filter(function(obj){
	        		return obj.occupationName == occupationDetails.occupation;
	        	});
	        	$scope.occupation = temp[0].occupationName;
	        	 $scope.getCategoryFromDB();
	        	$scope.renderOccupationQuestions();
	    	}, function(err){
	    		console.info("Error while fetching occupations " + JSON.stringify(err));
	    	});
	    	$('#dodLabel').addClass('active');
			$('#privacyLabel').addClass('active');

	    	$scope.toggleTwo(true);
	    	$scope.toggleThree(true);
	    	$scope.togglePrivacy(true);
			$scope.toggleContact(true);
	    }

	    if($routeParams.mode == 3){
	    	mode3Flag = true;
	    	var num = PersistenceService.getAppNumToBeRetrieved();

	    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
	    	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
	    		var appDetails = res.data[0];
	    		$scope.email = appDetails.email;
		    	$scope.preferredContactType = appDetails.contactType;
		    	$scope.changeCvrPhone = appDetails.contactPhone;
		    	$scope.time = appDetails.contactPrefTime;

		    	$scope.fifteenHrsQuestion = appDetails.occupationDetails.fifteenHr;
		    	$scope.areyouperCitzQuestion = appDetails.occupationDetails.citizenQue;
		    	$scope.industry = appDetails.occupationDetails.industryCode;
		    	$scope.ownBussinessQuestion = appDetails.occupationDetails.ownBussinessQues;
		    	$scope.ownBussinessYesQuestion = appDetails.occupationDetails.ownBussinessYesQues;
		    	$scope.ownBussinessNoQuestion = appDetails.occupationDetails.ownBussinessNoQues;
		    	$scope.withinOfficeQuestion = appDetails.occupationDetails.withinOfficeQue;
		    	$scope.tertiaryQuestion = appDetails.occupationDetails.tertiaryQue;
		    	$scope.outsideOffice = appDetails.occupationDetails.managementRoleQue;
		    	$scope.hazardousQuestion = appDetails.occupationDetails.hazardousQue;
		    	$scope.gender = appDetails.occupationDetails.gender;
		    	$scope.annualSalary = appDetails.occupationDetails.salary;
		    	$scope.otherOccupationObj.otherOccupation = appDetails.occupationDetails.otherOccupation;
		    	$scope.auraDisabled = appDetails.auraDisabled;
		    	if(appDetails.auraDisabled && appDetails.auraDisabled == "true"){
		    		$scope.auraDisabled = true;
		    	} else{
		    		$scope.auraDisabled = false;
		    	}

		    	$scope.deathCoverDetails.amount = appDetails.existingDeathAmt;
		    	$scope.deathCoverDetails.units = appDetails.existingDeathUnits;
		    	$scope.deathOccupationCategory = appDetails.deathOccCategory;
		    	$scope.previousTpdBenefitQue = appDetails.previousTpdBenefit;
		    	$scope.previousTerminalillnessQue = appDetails.terminalIllClaimQue;
		    	$scope.coverName = appDetails.addnlDeathCoverDetails.deathCoverName;
		    	$scope.coverType = appDetails.addnlDeathCoverDetails.deathCoverType;
		    	$scope.dcCoverAmount = appDetails.addnlDeathCoverDetails.deathFixedAmt;
		    	$scope.requireCover = appDetails.addnlDeathCoverDetails.deathInputTextValue;
		    	$scope.dcWeeklyCost = appDetails.addnlDeathCoverDetails.deathCoverPremium;

		    	$scope.tpdCoverDetails.amount = appDetails.existingTpdAmt;
		    	$scope.tpdCoverDetails.units = appDetails.existingTPDUnits;
		    	$scope.tpdOccupationCategory = appDetails.tpdOccCategory;

		    	$scope.tpdCoverName = appDetails.addnlTpdCoverDetails.tpdCoverName;
		    	$scope.tpdCoverType = appDetails.addnlTpdCoverDetails.tpdCoverType;
		    	$scope.tpdCoverAmount = appDetails.addnlTpdCoverDetails.tpdFixedAmt;
		    	$scope.TPDRequireCover = appDetails.addnlTpdCoverDetails.tpdInputTextValue;
		    	$scope.tpdWeeklyCost = appDetails.addnlTpdCoverDetails.tpdCoverPremium;

		    	$scope.ipCoverDetails.units = appDetails.existingIPUnits;
		    	$scope.ipOccupationCategory = appDetails.ipOccCategory;
		    	ipCheckboxState = appDetails.ipcheckbox;
		    	if(appDetails.indexationDeath && appDetails.indexationDeath == "true"){
		    		$scope.indexation.death = true;
		    	} else{
		    		$scope.indexation.death = false;
		    	}
		    	if(appDetails.indexationTpd && appDetails.indexationTpd == "true"){
		    		$scope.indexation.disable = true;
		    	} else{
		    		$scope.indexation.disable = false;
		    	}

		    	$scope.ipCoverName = appDetails.addnlIpCoverDetails.ipCoverName;
		    	$scope.waitingPeriodAddnl = appDetails.addnlIpCoverDetails.waitingPeriod;
		    	$scope.benefitPeriodAddnl = appDetails.addnlIpCoverDetails.benefitPeriod;
		    	$scope.ipCoverAmount = appDetails.addnlIpCoverDetails.ipFixedAmt;
		    	$scope.IPRequireCover = appDetails.addnlIpCoverDetails.ipInputTextValue;
		    	$scope.ipWeeklyCost = appDetails.addnlIpCoverDetails.ipCoverPremium;

		    	$scope.totalWeeklyCost = appDetails.totalPremium;
		    	appNum = appDetails.appNum;
		    	$scope.deathErrorFlag = false;
		    	$scope.tpdErrorFlag = false;
		    	$scope.ipErrorFlag = false;
		    	dynamicFlag = true;
		    	
		    	if($scope.coverType == "DcUnitised"){
		    		showhide('nodollar1','dollar1');
					showhide('nodollar','dollar');
		    	} else if($scope.coverType == "DcFixed"){
		    		showhide('dollar1','nodollar1');
		    		showhide('dollar','nodollar');
		    	}

		    	OccupationService.getOccupationList($scope.urlList.occupationUrl, "HOST", $scope.industry).then(function(res){
		    		$scope.OccupationList = res.data;
		    		var temp = $scope.OccupationList.filter(function(obj){
		        		return obj.occupationName == appDetails.occupationDetails.occupation;
		        	});
		        	$scope.occupation = temp[0].occupationName;
		        	//$scope.renderOccupationQuestions();
		    	}, function(err){
		    		console.info("Error while fetching occcupation list " + JSON.stringify(err));
		    	});

    			$('#dodLabel').addClass('active');
    			$('#privacyLabel').addClass('active');

		    	$scope.toggleTwo(true);
		    	$scope.toggleThree(true);
		    	$scope.togglePrivacy(true);
    			$scope.toggleContact(true);

	    	}, function(err){
	    		console.info("Error fetching the saved app details " + err);
	    	});
	    }


	    $scope.generatePDF = function(){

	    	$scope.saveDataForPersistence();
	    	var CCOccDetails = PersistenceService.getChangeCoverOccDetails();
    		var deathAddnlInfo = PersistenceService.getDeathAddnlCoverDetails();
    		var tpdAddnlInfo = PersistenceService.getTpdAddnlCoverDetails();
    		var ipAddnlInfo = PersistenceService.getIpAddnlCoverDetails();
    	    var personalInformation = persoanlDetailService.getMemberDetails();
    	    var quoteObj =  PersistenceService.getChangeCoverDetails();
    	    if(quoteObj.existingIpUnits== undefined){
    	    	quoteObj.existingIpUnits=0;
    	    	quoteObj.existingIpAmount=0;
    	    }
    	    if(quoteObj != null && CCOccDetails != null && deathAddnlInfo != null && tpdAddnlInfo != null && ipAddnlInfo != null && personalInformation != null ){
    	    	var info={};
    	    	info.addnlDeathCoverDetails = deathAddnlInfo;
    	    	info.addnlTpdCoverDetails = tpdAddnlInfo;
    	    	info.addnlIpCoverDetails = ipAddnlInfo;
    	    	info.occupationDetails = CCOccDetails;
    	    	info.appNum=PersistenceService.getAppNumber();
    			var temp = angular.extend(info,quoteObj);
    			var printObject = angular.extend(temp,personalInformation);
    			auraResponseService.setResponse(printObject);
    			printQuotePage.reqObj($scope.urlList.printQuotePage).then(function(response) {
    				PersistenceService.setPDFLocation(response.data.clientPDFLocation);
        		$scope.downloadPDF();
	    	}, function(err){
	    		console.info("Something went wrong while generating pdf..." + JSON.stringify(err));
	    	});
    	    }
	    };
	    
		
    	    $scope.downloadPDF = function(){
    	    var pdfLocation =null;
    	    var filename = null;
    	   	var a = null;
	    	pdfLocation = PersistenceService.getPDFLocation();
	    	console.log(pdfLocation+"pdfLocation");
	    	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  		a = document.createElement("a");
	  	    document.body.appendChild(a);
	  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
	  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
	  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
	   	           window.navigator.msSaveBlob(res.data.response,filename);
	   	       }else{
	  	        var fileURL = URL.createObjectURL(res.data.response);
	  	        a.href = fileURL;
	  	        a.download = filename;
	  	        a.click();
	   	       }
	  		}, function(err){
	  			console.log("Error downloading the PDF " + err);
	  		});
	  	};
	  	$scope.coverRestrictMax = function(){
	  		var maxIPRequireCover = 4000;
	  		var ipninety =	$scope.checkIpCoverwithSalary();

	  		if(ipninety == 0){
	  			maxIPRequireCover = 4000;
			}else{
				maxIPRequireCover = ipninety;
				ipninety = 0;
			}
	  		if($scope.IPRequireCover > maxIPRequireCover){
	  			$scope.ipWarningFlag = true;
	  			$scope.IPRequireCover = maxIPRequireCover;
	  		}
	  	};
	  	$scope.ipRoundOFF= function(){
					$scope.IPRequireCover=	500*(Math.ceil(Math.abs($scope.IPRequireCover/500)));
	  	};
  }]);

 /* Change Cover Controller,Progressive and Mandatory validations Ends */
