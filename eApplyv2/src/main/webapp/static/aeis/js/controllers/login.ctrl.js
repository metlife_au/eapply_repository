/*Login Page Controller Starts*/
AeisApp.controller('login',['$scope', '$rootScope','$routeParams', '$location','$window', 'fetchClientDataSvc', 'fetchTokenNumSvc','fetchDeathCoverSvc','fetchTpdCoverSvc',
                                 'fetchIpCoverSvc','fetchNewMemberOfferSvc', 'fetchPersoanlDetailSvc', '$http', '$window','appNumberService',
                                 'auraInputService', 'PersistenceService','ngDialog', 'CheckSavedAppService', 'CancelSavedAppService','$timeout', 'fetchUrlSvc','urls','SpecialCvrRenderService','fetchAppNumberSvc', 'CheckUWDuplicateAppService','RetrieveAppDetailsService', 'appData', 'extendAppModel', '$q', 'fetchIndustryList',
                                 function($scope, $rootScope, $routeParams, $location,$window, fetchClientDataSvc, fetchTokenNumSvc, fetchDeathCoverSvc, fetchTpdCoverSvc,
                                		 fetchIpCoverSvc, fetchNewMemberOfferSvc, fetchPersoanlDetailSvc, $http, $window,appNumberService,auraInputService,
                                		 PersistenceService, ngDialog, CheckSavedAppService, CancelSavedAppService,$timeout,fetchUrlSvc,urls,SpecialCvrRenderService, fetchAppNumberSvc, CheckUWDuplicateAppService,RetrieveAppDetailsService, appData, extendAppModel, $q, fetchIndustryList) {
	var clientRefNum;
  var age;
  var deathUnits = 0,tpdUnits = 0,memberType;
  $scope.renderView = false;
  $scope.isEmpty = function(value) {
		return ((value == "" || value == null) || value == "0");
	};

  $scope.init = function() {
	  var defer = $q.defer();
  	$scope.urlList = fetchUrlSvc.getUrlList();
    fetchTokenNumSvc.setTokenId(inputData);
    fetchIndustryList.getObj($scope.urlList.quoteUrl);
    $scope.deathCover = {
      amount: "0.0",
      benefitPeriod: "",
      benefitType: "1",
      coverStartDate: null,
      coverUnit: null,
      description: null,
      exclusions: "",
      indexation: null,
      loading: "0.0",
      occRating: "General",
      type: "2",
      units: "0",
      waitingPeriod:""
    };
    $scope.tpdCover = {
      amount: "0.0",
      benefitPeriod: "",
      benefitType: "2",
      coverStartDate: null,
      coverUnit: null,
      description: null,
      exclusions: "",
      indexation: null,
      loading: "0.0",
      occRating: "General",
      type: "2",
      units: "0",
      waitingPeriod:""
    };
    $scope.ipCover = {
      amount: "0.0",
      benefitPeriod: "",
      benefitType: "4",
      coverStartDate: null,
      coverUnit: null,
      description: null,
      exclusions: "",
      indexation: null,
      loading: "0.0",
      occRating: "General",
      type: "2",
      units: "0",
      waitingPeriod:""
    };
    
  
    fetchClientDataSvc.requestObj($scope.urlList.clientDataUrl).then(function(response) {
      var applicantlist = null;
      $scope.partnerRequest = response.data;
      $scope.calcEncrpytURL = $scope.partnerRequest.calcEncrpytURL;
      applicantlist = $scope.partnerRequest.applicant[0];
      memberType = applicantlist.memberType;
      $scope.clientRefNum = applicantlist.clientRefNumber || 0;
      $scope.personalDetails = applicantlist.personalDetails;
      $scope.coverList = applicantlist.existingCovers.cover;
      $scope.fName= applicantlist.personalDetails.firstName;
      $scope.lName= applicantlist.personalDetails.lastName;
      $scope.maxAge = moment().diff(moment(applicantlist.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years') + 1;
      dob=applicantlist.personalDetails.dateOfBirth;
      $scope.newMemberOffer = false;
      if(moment().diff(moment(applicantlist.welcomeLetterDate, 'DD-MM-YYYY'), 'days') +1<91 && moment(moment(moment(applicantlist.welcomeLetterDate,'DD-MM-YYYY')).format('MM-DD-YYYY')).isAfter('01-31-2017', 'day') && $scope.maxAge<61 && (applicantlist.memberType=='Industry' || applicantlist.memberType=='Corporate')){
        $scope.newMemberOffer = true;
      }
      fetchNewMemberOfferSvc.setNewMemberOfferFlag($scope.newMemberOffer);
      angular.forEach($scope.coverList, function(value, key) {
         if(value.benefitType =='1'){
           $scope.deathCover = value;
           $scope.deathCover.amount = parseInt($scope.deathCover.amount);
           deathUnits = value.units;
         } else if(value.benefitType =='2'){
           $scope.tpdCover = value;
           tpdUnits = value.units;
           $scope.tpdCover.amount = parseInt($scope.tpdCover.amount);
         }else if(value.benefitType =='4'){
           $scope.ipCover = value;
           $scope.ipCover.amount = parseInt($scope.ipCover.amount);
         }
      });
      fetchDeathCoverSvc.setDeathCover($scope.deathCover);
      fetchTpdCoverSvc.setTpdCover($scope.tpdCover);
      fetchIpCoverSvc.setIpCover( $scope.ipCover);
      
      if($scope.tpdCover.type  == "2" && $scope.deathCover.type  == "1") {
      	$scope.tpdCover.type  = $scope.deathCover.type;
      }
      
      switch( $scope.ipCover.waitingPeriod.toLowerCase().trim().substr(0,2)){
		
		case "30":
			 $scope.ipCover.waitingPeriod = "30 Days";
			break;
		case "60":
			 $scope.ipCover.waitingPeriod = "60 Days";
			break;
		case "90":
			 $scope.ipCover.waitingPeriod = "90 Days";
			break;
		default :
			 $scope.ipCover.waitingPeriod = "30 Days";
		}
		switch( $scope.ipCover.benefitPeriod.toLowerCase().trim().substr(0,1)){
		
		case "2":
			 $scope.ipCover.benefitPeriod = "2 Years";
			break;
		case "a":
			 $scope.ipCover.benefitPeriod = "Age 65";
			break;
		case "5":
			 $scope.ipCover.benefitPeriod = "5 Years";
			break;
		default :
			 $scope.ipCover.benefitPeriod = "2 Years";
		}
      
      
      $scope.customDigest();
      if($scope.deathCover.type=='1' && $scope.tpdCover.type=='1' &&  ($scope.intvalue($scope.deathCover.units) || $scope.intvalue($scope.tpdCover.units))){
        $scope.convertMaintainDisable=true;
      }else{
        $scope.convertMaintainDisable=false;
      }
      if($scope.deathCover.occRating=='White Collar' && $scope.tpdCover.occRating=='White Collar'  && $scope.ipCover.occRating=='White Collar'){
        $scope.updateOccDisable = true;
      }else{
        $scope.updateOccDisable = false;
      }

      if($scope.deathCover.type=='2' && $scope.tpdCover.type=='2' &&  ($scope.intvalue($scope.deathCover.amount) || $scope.intvalue($scope.tpdCover.amount))){
        $scope.cancelEnable=true;
      }else if($scope.deathCover.type=='1' && $scope.tpdCover.type=='1' &&  ($scope.intvalue($scope.deathCover.units) || $scope.intvalue($scope.tpdCover.units))){
        $scope.cancelEnable=true;
      }else{
        $scope.cancelEnable=false;
      }
      
      if(!$scope.intvalue($scope.deathCover.amount) && !$scope.intvalue($scope.tpdCover.amount) && !$scope.intvalue($scope.ipCover.amount)){
    	  $scope.lifeEventDisable = true;
      }else{
    	  $scope.lifeEventDisable = false;
      }
      $scope.appContactDetails = applicantlist.contactDetails || {};
      $scope.appContactDetails.homePhone = $scope.appContactDetails.homePhone ? $scope.appContactDetails.homePhone.replace(/[^0-9]/g,'') : $scope.appContactDetails.homePhone;
      $scope.appContactDetails.mobilePhone = $scope.appContactDetails.mobilePhone ? $scope.appContactDetails.mobilePhone.replace(/[^0-9]/g,'') : $scope.appContactDetails.mobilePhone;
      $scope.appContactDetails.workPhone = $scope.appContactDetails.workPhone ? $scope.appContactDetails.workPhone.replace(/[^0-9]/g,'') : $scope.appContactDetails.workPhone;
      angular.extend(applicantlist.contactDetails, $scope.appContactDetails);
      fetchPersoanlDetailSvc.setMemberDetails(applicantlist);
      // to render special offer tile
//      SpecialCvrRenderService.requestObj($scope.urlList.specialCvrEligUrl, "AEIS", $scope.maxAge, deathUnits,tpdUnits).then(function(res){
//          defer.resolve(res);
//    	  $scope.check=res.data;
//        if($scope.newMemberOffer == false && $scope.check == true && (memberType.toLowerCase()=='industry' || memberType.toLowerCase()=='executive')){
//            $scope.renderSpecialCover = true;
//          }else{
//            $scope.renderSpecialCover = false;
//          }
//      }, function(err){
//        console.info("Error while fetching default units " + JSON.stringify(err));
//        defer.reject(err);
//      });
    });
    $scope.renderView = true;
    return defer.promise;
  }

  $scope.init();
  
  $scope.customDigest = function() {
    if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
        $scope.$digest();
    }
  }



    //$location.search( 'InputData', null );



    $scope.go = function (path, generateAppnum, num) {
    	if(generateAppnum) {
    		//generate app number
    		appNumberService.requestObj($scope.urlList.appNumUrl).then(function(response){
        		fetchAppNumberSvc.setAppNumber(response.data);
        		$location.path(path);
        	 });
    	} else {
    		if(num)
    			fetchAppNumberSvc.setAppNumber(num);
    		$location.path(path);
    	}
  	};
  	$scope.tokenid = fetchTokenNumSvc.getTokenId();
    $scope.openWindow = function(url) {
        $window.open(url);
    }

    // added for new member popup on landing page
    $scope.showNewMemberPopUp = function(val){
   		if(val == null || val == "" || val == " "){
   			hideTips();
   		}else{
   		 document.getElementById('mymodalNewMem').style.display = 'block';
   		 document.getElementById('mymodalNewMemFade').style.display = 'block';
   		 document.getElementById('newMember_text').innerHTML=val;
   		}
   	}
    $scope.hideTips =function(){
   		if(document.getElementById('help_div')){
   			document.getElementById('help_div').style.display = "none";
   		}
   	};

   	$scope.intvalue = function(amount) {
   	  return parseInt(amount) > 0 ? true : false;
   	}

   	$scope.showSavedAppPopup = function(manageType){
   		CheckSavedAppService.checkSavedApps($scope.urlList.savedAppUrl, "AEIS", $scope.clientRefNum, manageType).then(function(res){
   		//CheckSavedAppService.checkSavedApps({fundCode:"AEIS", clientRefNo:clientRefNum, manageType:manageType}, function(res){
   			//$scope.apps = res;
   			$scope.apps = res.data;
   			if($scope.apps.length > 0){
	   			var newScope = $scope.$new();
	   			for(var i = 0; i < $scope.apps.length; i++){
	   				var tempDate = new Date($scope.apps[i].createdDate);
	   				$scope.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');

	   				if($scope.apps[i].requestType == "CCOVER"){
	   					$scope.apps[i].requestType = "Change Cover";
	   				} else if($scope.apps[i].requestType == "TCOVER"){
	   					$scope.apps[i].requestType = "Transfer Cover";
	   				} else if($scope.apps[i].requestType == "UWCOVER"){
	   					$scope.apps[i].requestType = "Update Work Rating";
	   				} else if($scope.apps[i].requestType == "NCOVER"){
	   					$scope.apps[i].requestType = "New Member Cover";
	   				} else if($scope.apps[i].requestType == "ICOVER"){
	   					$scope.apps[i].requestType = "Life Event Cover";
	   				} else if($scope.apps[i].requestType == "SCOVER"){
	   					$scope.apps[i].requestType = "Special Cover";
	   				}else if($scope.apps[i].requestType == "CANCOVER"){
	   					$scope.apps[i].requestType = "Cancel Cover";
	   				}
	   			}
	   			// Created a new scope for the modal popup as the parent controller variables are not accessible to the modal
	   			newScope.apps = $scope.apps;

	   			ngDialog.openConfirm({
	   	            template:'<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">You have previously saved application(s).</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-12"><p> Please continue with the saved application or cancel saved application to start a new application. </p> </div> </div><!-- Row ends --> <div class="row visible-xs"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th class="coverbg"><h5 class=" ">Application no.</h5></th><th class="coverbg     "><h5 class=" ">Date saved</h5></th><th class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div></div> <div class="row hidden-xs"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th width="20%" class="coverbg"><h5 class=" ">Application no.</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Date saved</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div></div>',
	   	            plain: true,
	   	            className: 'ngdialog-theme-plain custom-width',
	   	            scope: newScope
	   	        }).then(function (value) {
                fetchAppNumberSvc.setAppNumber($scope.apps[0].applicationNumber);
              	PersistenceService.setAppNumToBeRetrieved($scope.apps[0].applicationNumber);
                PersistenceService.setAppNumber($scope.apps[0].applicationNumber);
                RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,$scope.apps[0].applicationNumber).then(function(res){
          				console.log(res);
          				appData.setAppData(extendAppModel.extendObj(res.data[0]));
                 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
             	     	case "quotepage":
             	     		$scope.go('/quote');
             	     		break;
             	     	case "aurapage":
             	     		$scope.go('/aura');
             	     		break;
             	     	case "summarypage":
             	     		$scope.go('/summary');
             	     		break;
             	     	case "transferpage":
             	     		$scope.go('/quotetransfer/3');
             	     		break;
             	     	case "auratransferpage":
             	     		$scope.go('/auratransfer/3');
             	     		break;
             	     	case "summarytransferpage":
             	     		$scope.go('/transferSummary/3');
             	     		break;
             	     	case "quoteupdatepage":
             	     		$scope.go('/quoteoccchange/3');
             	     		break;
             	     	case "auraupdatepage":
             	     		$scope.go('/auraocc/3');
             	     		break;
             	     	case "summaryupdatepage":
             	     		$scope.go('/workRatingSummary/3');
             	     		break;
             	     	case "lifeeventpage":
             	     		$scope.go('/lifeevent/3');
             	     		break;
             	     	case "auralifeeventpage":
             	     		$scope.go('/auralifeevent/3');
             	     		break;
             	     	case "summarylifeeventpage":
             	     		$scope.go('/lifeeventsummary/3');
             	     		break;
             	     	case "specialquotepage":
             	     		$scope.go('/quotespecial/3');
             	     		break;
             	     	case "specialcoveraurapage":
             	     		$scope.go('/auraspecial/3');
             	     		break;
             	     	case "specialsummarypage":
             	     		$scope.go('/specialoffersummary/3');
             	     		break;
             	     	case "quotecancelpage":
             	     		$scope.go('/quotecancel/3');
             	     		break;
             	     	case "auracancelpage":
             	     		$scope.go('/auracancel/3');
             	     		break;
             	     	case "summarycancelpage":
             	     		$scope.go('/cancelConfirmation/3');
             	     		break;
             	     	default:
             	     		break;
                 	}
          			});
                // rsistenceService.setAppNumToBeRetrieved($scope.apps[0].applicationNumber);
                // $scope.go('/quote/3');
                // (manageType.toUpperCase() == "CCOVER"){
	   	        // 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
                // e "quotepage":
                // cope.go('/quote', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "aurapage":
                // cope.go('/aura', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "summarypage":
                // cope.go('/summary', false, $scope.apps[0].applicationNumber);
                // eak;
                // ault:
                // eak;
                //
                // lse if(manageType.toUpperCase() == "TCOVER"){
	   	        // 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
                // e "transferpage":
                // cope.go('/quotetransfer/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "auratransferpage":
                // cope.go('/auratransfer/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "summarytransferpage":
                // cope.go('/transferSummary/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // ault:
                // eak;
                //
                // lse if(manageType.toUpperCase() == "UWCOVER"){
	   	        // 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
                // e "quoteupdatepage":
                // cope.go('/quoteoccchange/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "auraupdatepage":
                // cope.go('/auraocc/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "summaryupdatepage":
                // cope.go('/workRatingSummary/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // ault:
                // eak;
                //
                // lse if(manageType.toUpperCase() == "ICOVER"){
	   	        // 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
                // e "lifeeventpage":
                // cope.go('/lifeevent/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "auralifeeventpage":
                // cope.go('/auralifeevent/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "summarylifeeventpage":
                // cope.go('/lifeeventsummary/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // ault:
                // eak;
                //
                // else if(manageType.toUpperCase() == "SCOVER"){
	   	        // 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
                // e "specialquotepage":
                // cope.go('/quotespecial/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "specialcoveraurapage":
                // cope.go('/auraspecial/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "specialsummarypage":
                // cope.go('/specialoffersummary/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // ault:
                // eak;
                //
                // lse if(manageType.toUpperCase() == "CANCOVER"){
	   	        // 		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
                // e "quotecancelpage":
                // cope.go('/quotecancel/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "auracancelpage":
                // cope.go('/auracancel/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // e "summarycancelpage":
                // cope.go('/cancelConfirmation/3', false, $scope.apps[0].applicationNumber);
                // eak;
                // ault:
                // eak;
                //
                //
	   	        }, function (value) {
	   	        	if(value == 'oncancel'){
	   	        		CancelSavedAppService.cancelSavedApps($scope.urlList.cancelAppUrl,$scope.apps[0].applicationNumber).then(function(result){
	   	        		//CancelSavedAppService.cancelSavedApps({applicationNumber:$scope.apps[0].applicationNumber}, {}, function(result){
		   	        		if($scope.newMemberOffer){
		   	        			$scope.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
		   	        		} else{
                      appData.setAppData(extendAppModel.extendObj());
			   	        		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
			   	        			case "quotepage":
			   	        			case "aurapage":
			   	        			case "summarypage":
			   	        				$scope.go('/quote', true);
			   	        				break;
			   	        			case "transferpage":
			   	        			case "auratransferpage":
			   	        			case "summarytransferpage":
			   	        				$scope.go('/quotetransfer/1', true);
			   	        				break;
			   	        			case "quoteupdatepage":
			   	        			case "auraupdatepage":
			   	        			case "summaryupdatepage":
			   	        				$scope.go('/quoteoccchange/1', true);
			   	        				break;
			   	        			case "lifeeventpage":
			   	        			case "auralifeeventpage":
			   	        			case "summarylifeeventpage":
			   	        				$scope.go('/lifeevent/1', true);
			   	        		    	break;
			   	        			case "specialquotepage":
			   	        			case "specialcoveraurapage":
			   	        			case "specialsummarypage":
			   	        				$scope.go('/quotespecial/1', true);
			   	        				break;
			   	        			case "specialquotepage":
			   	        			/*case "specialcoveraurapage":
			   	        			case "specialsummarypage":*/
			   	        				$scope.go('/quotespecial/1', true);
			   	        				break;
			   	        			default:
			   	        				break;
			   	        		}
		   	        		}
		   	        	}, function(err){
		   	        		console.log("Error while cancelling the saved app " + err);
		   	        	});
	   	        	}
	   	        });
   			} else {
   				if($scope.newMemberOffer){
   					$scope.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
   				} else{
            appData.setAppData(extendAppModel.extendObj());
	   				switch(manageType.toUpperCase()){
	       			case "CCOVER":
	       				$scope.checkDupApplication("CCOVER");
//	       				$scope.go('/quote', true);
	       				break;
	       			case "TCOVER":
	       				$scope.go('/quotetransfer/1', true);
	       				break;
	       			case "UWCOVER":
	       				$scope.go('/quoteoccchange/1', true);
	       				break;
	       			case "ICOVER":
	       				$scope.go('/lifeevent/1', true);
	       				break;
	       			case "SCOVER":
	       				$scope.go('/quotespecial/1', true);
	       				break;
	       			case "CANCOVER":
	       				$scope.go('/quotecancel/1', true);
	       				break;
	       			default:
	       				break;
	   				}
   				}
   			}
   		}, function(err){
   			console.log("Error while fetching saved apps " + err);
   		});
   	};

    $scope.clickToOpen = function (hhText) {

        var dialog = ngDialog.open({
          /*template: '<p>'+hhText+'</p>' +
            '<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
            template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" style="width: 100%;font-weight: normal !important; font-size: 18px !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Close</button></div><div class="col-sm-4"></div></div></div>',
            className: 'ngdialog-theme-plain',
            plain: true
        });
        dialog.closePromise.then(function (data) {
          console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
        });
      };
   	/* Change for Duplicate Application UW message*/
      $scope.checkDupApplication=function(manageTypeCC){
  		CheckUWDuplicateAppService.checkDuplicateApps($scope.urlList.dupAppUrl,"AEIS",$scope.clientRefNum,manageTypeCC,dob,$scope.fName,$scope.lName).then(function(res){
  				console.log(res.data);
  				$scope.check=res.data;
  				if($scope.check)
  					{
  					ngDialog.openConfirm({
  			            //template:'<div class="ngdialog-contentdup"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">YOU HAVE PREVIOUSLY SUBMITTED APPLICATION(S)</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-15"><p> Thank you for commencing your application, we note that you have recently lodged an application for additional insurance in the last 24 hours that was referred to our underwriting team, please note that your application is currently under assessment and we will be in contact with you as soon as possible with an update.</p> <p>If you would like to initiate a different application to the one that is pending, please continue with this application.</p> </div></div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary mr10px pr20px avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button></div></div></div>',
  						template:'<div class="ngdialog-contentdup"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">YOU HAVE PREVIOUSLY SUBMITTED APPLICATION(S)</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-15"><p> Thank you for commencing your application, we note that you have recently lodged an application for additional insurance in the last 24 hours that was referred to our underwriting team, please note that your application is currently under assessment and we will be in contact with you as soon as possible with an update.</p></div></div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary mr10px pr20px avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Ok</button><!--<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button>--></div></div></div>',
  			            plain: true,
  			            className: 'ngdialog-theme-plain custom-width'
  			        }).then(function(){
  			        	$scope.go('/quote', true);
  			        }, function(e){
  			        	if(e=='oncancel'){
  			        		return false;
  			        	}
  			        });
  					}
  				else
  					{
  					$scope.go('/quote', true);
  					}

  			}, function(err){
  				console.log("Error " + err);
  			});

  	}; 


 }]);
 /*Login Page Controller Ends*/
