package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTConfirmationPageObjects extends PageObjectLibrary{
		
	public static final By rdowner = getObjectIdentifaction("stft.confirmation.owner.yes.radio.css");
	public static final By linkTitle = getObjectIdentifaction("stft.confirmation.title.dropdown.css");
	
	public static final By linktitleMr = getObjectIdentifaction("stft.confirmation.title.mr.linktext");
	public static final By txtContact = getObjectIdentifaction("stft.confirmation.contact.preferred.text.id");
	public static final By txtcontactOther = getObjectIdentifaction("stft.confirmation.contact.other.text.id");
	public static final By txtemail = getObjectIdentifaction("stft.confirmation.email.text.id");
	public static final By txtemailconfirm = getObjectIdentifaction("stft.confirmation.email.confirm.text.id");
	public static final By txtunit = getObjectIdentifaction("stft.confirmation.unit.text.id");
	public static final By txtstreetNum = getObjectIdentifaction("stft.confirmation.streetnumber.text.id");
	public static final By txtstreetName = getObjectIdentifaction("stft.confirmation.streetname.text.id");
	public static final By txtSuburb = getObjectIdentifaction("stft.confirmation.suburb.text.id");
	public static final By btnState = getObjectIdentifaction("stft.confirmation.state.dropdown.css");
	public static final By linkState = getObjectIdentifaction("stft.confirmation.state.nsw.linktext");
	
	public static final By txtPostcode = getObjectIdentifaction("stft.confirmation.postcode.text.id");
	public static final By chkacknowledge = getObjectIdentifaction("stft.confirmation.acknowledge.checkbox.id");
	public static final By btnNext = getObjectIdentifaction("stft.confirmation.next.button.css");
}
