package au.com.metlife.eapply.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class XSDHelper {
	private static final Logger log = LoggerFactory.getLogger(XSDHelper.class);
	
	public static final String XMLSCHEMA="http://www.w3.org/2001/XMLSchema"; 


	public  static Request unMarshallingMInput(String inputXML){
		
		Request input=null;
		try {
			JAXBContext jc = JAXBContext.newInstance (new Class[] {Request.class});            
            Unmarshaller u = jc.createUnmarshaller ();
      
             input = (Request) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8")));
       } catch (Exception e) {
          log.info("Error in xsd"+e);
       }
       return input;
	}
	
	/**
	 * @param xmlContent
	 * @param xsdFileFullPath
	 * @return
	 */
	public  String validateXMLString(String xmlContent,String xsdpath) throws Exception{		
		String validationMesage="valid";
		Validator validator=null;
        try {


    		SchemaFactory factory =
                SchemaFactory.newInstance(XMLSCHEMA);

    		log.info("xsdpath>>"+xsdpath);
            File schemaLocation = new File(xsdpath);
            Schema schema = factory.newSchema(schemaLocation);

             validator = schema.newValidator();

            validator.setErrorHandler(new XMLErrorHandler());

            String errors=null;

            StringReader reader = new StringReader(xmlContent);
              if(reader!=null){
              Source source = new StreamSource(reader);
              validator.validate(source);


              errors=((XMLErrorHandler)validator.getErrorHandler()).getErrors();
              
              if(errors!=null  && errors.trim().length()>1){
            	  validationMesage="invalid -{\n"+errors+"\n}";
              }

             }
        }
        catch (Exception ex) {        	
            validationMesage="invalid -{\n 400: Bad xml data passed.";
        }

		return validationMesage;

	}
	
	public String getResponseXML(Response input) throws Exception {
    	final String METHOD_NAME = "getResponseXML";
		String response = null;
		
			JAXBContext context = JAXBContext.newInstance(Response.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(JAXB_FORMATTED_OUTPUT, true);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			marshaller.marshal(input, byteArrayOutputStream);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
			
			if(null != byteArrayInputStream) {
				  int size = byteArrayInputStream.available();
				    char[] theChars = new char[size];
				    byte[] bytes    = new byte[size];

				    byteArrayInputStream.read(bytes, 0, size);
				    for (int i = 0; i < size;) {
				        theChars[i] = (char)(bytes[i++]&0xff);
				    }
				    response = new String(theChars);
			}					
		
		
		return response;
	}

}
