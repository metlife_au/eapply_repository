package au.com.metlife.eapply.underwriting.utils;

import java.util.Iterator;

import au.com.metlife.eapply.underwriting.response.model.Decision;

public class AuraProcessDecisionHelper {
	
	public static String getReasons(Decision decision){
		 String [] reasons = null;
		 StringBuffer declineReasons = null;
		 reasons = decision.getReasons();
			declineReasons = new StringBuffer(); 
			if(null!=reasons && reasons.length>0){
				for(int resonItr=0; resonItr<reasons.length;resonItr++){
					if(reasons[resonItr]!=null){                       									
						declineReasons.append(reasons[resonItr]);
						declineReasons.append(", ");	                                      								
					}
				}
			}
			if(declineReasons!=null)
				return declineReasons.toString();
			else
				return null;
	}
	
	
	public static String getAuraDclReason(Decision decision){
		StringBuffer declineOrgReasons = null;
		if(decision.getOrgReason()!=null && decision.getOrgReason().size()>0){
			declineOrgReasons = new StringBuffer(); 
			for (Iterator iter = decision.getOrgReason().iterator(); iter
			.hasNext();) {
				String element = (String) iter.next();
				if(null!=element){
					if("BMI_Term".equalsIgnoreCase(element) || "BMI_TPD".equalsIgnoreCase(element) || "BMI_IP".equalsIgnoreCase(element)){
						declineOrgReasons.append("Height/weight ratio");
					}else{
						declineOrgReasons.append(element);
					}	                                                        					
					declineOrgReasons.append("##");
				}
		
			}
			//coversDTO.setCoverAuraDclReason(declineOrgReasons.toString());
		}
		if(declineOrgReasons!=null)
			return declineOrgReasons.toString();
		else
			return null;
	}
	
	public static String getExclusions(Decision decision){
		String exclusionValue = null;
		if (null != decision.getListExclusion() && decision.getListExclusion().size() > 0) {
			for (int itrExclusion = 0; itrExclusion < decision.getListExclusion().size(); itrExclusion++) {
				 if (itrExclusion == 0){
                     exclusionValue = ""+decision.getListExclusion().get(itrExclusion).getText()+"<br><br>"; 
                    
    			 }                        
                 else{
                     if(decision.getListExclusion().size() == itrExclusion){
                             exclusionValue = exclusionValue + "\n"+decision.getListExclusion().get(itrExclusion).getText(); 
                     }else{
                             exclusionValue = exclusionValue + "\n"+decision.getListExclusion().get(itrExclusion).getText()+"<br><br>";
                     }	    	                                                                    
                 }  
    			
			}
			
		}
		return exclusionValue;
	}

}
