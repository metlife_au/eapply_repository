package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sender")

@XmlRootElement
public class Sender {
	
	private String RoleCode;
	private OrganizationReferences OrganizationReferences ;
	private Contact Contact;
	
	public Contact getContact() {
		return Contact;
	}
	public void setContact(Contact contact) {
		Contact = contact;
	}	
	public String getRoleCode() {
		return RoleCode;
	}
	public void setRoleCode(String roleCode) {
		RoleCode = roleCode;
	}
	public OrganizationReferences getOrganizationReferences() {
		return OrganizationReferences;
	}
	public void setOrganizationReferences(
			OrganizationReferences organizationReferences) {
		OrganizationReferences = organizationReferences;
	}
	
	
	
}
