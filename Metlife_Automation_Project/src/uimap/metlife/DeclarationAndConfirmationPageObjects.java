/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;
import supportlibraries.PageObjectLibrary;

/**
 * @author sampath
 *
 */
public class DeclarationAndConfirmationPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public DeclarationAndConfirmationPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	
		
	public static final By chkDutyOfDeclaration = getObjectIdentifaction("confirmationDe.DutyOfDec.css");
	public static final By chkExclusionsOfDeclaration = getObjectIdentifaction("confirmationDe.exclusions.chkbox.css");
	public static final By chkLoadingsOfDeclaration = getObjectIdentifaction("confirmationDe.loadings.chkbox.css");
	public static final By chkPrivacyStatement =	getObjectIdentifaction("confirmationDe.privacyStat.css");
	public static final By chkGeneralConsent = getObjectIdentifaction("confirmationDe.general.css");
	public static final By chkIngdGeneralConsent = getObjectIdentifaction("confirmationDe.general.ingd.css");
	public static final By lstPreferredContactMethod =	getObjectIdentifaction("confirmationDe.preferredContact.id");
	public static final By lstPreferredContactTime = getObjectIdentifaction("confirmationDe.preferredTime.id");
	public static final By txtDetails = getObjectIdentifaction("confirmationDe.contactDetails.css");
	public static final By txtEmailAddress = getObjectIdentifaction("confirmationDe.emailaddress.id");
	public static final By btnContinue = getObjectIdentifaction("confirmationDe.Continue.css");
	public static final By btnPwcContinue = getObjectIdentifaction("pwc.confirmationDe.Continue.css");
	public static final By hidDeathLoad = getObjectIdentifaction("metlife.loadings.hidden.death_loading.id");
	public static final By hidTPDLoad = getObjectIdentifaction("metlife.loadings.hidden.tpd_loading.id");
	public static final By hidIPLoad = getObjectIdentifaction("metlife.loadings.hidden.ip_loading.id");
	public static final By hidDeathExclu = getObjectIdentifaction("metlife.loadings.hidden.death_exclusion.id");
	public static final By hidTPDExclu = getObjectIdentifaction("metlife.loadings.hidden.tpd_exclusion.id");
	public static final By hidIPExclu = getObjectIdentifaction("metlife.loadings.hidden.ip_exclusion.id");
	public static final By finalSuccessMsg = getObjectIdentifaction("metlife.application.success.css");
	public static final By finalPWCReferredMsg = getObjectIdentifaction("pwc.confirmation.message.label.css");
	public static final By chkEvidence=getObjectIdentifaction("confirmationDe.transfer.evidence.css");
	public static final By txtDesMsg=getObjectIdentifaction("decsion.transfer.decisionmessage.css");
	
	
	
}
