class QuoteAuraCtrl {
  constructor(CommonAPI, EapplyData, AuraAPI, AppConstants, $scope, $state, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._AuraAPI = AuraAPI;
    this._ngDialog = ngDialog;
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList(); 
    this.applicantData = this._EapplyData.getApplicantData();
    this.changeCoverData = this._EapplyData.getChangeCoverData();
    
    this.serachText = '';
    this.heightDropdownOpt = ['cm', 'Feet'];
    this.heightDropDown = this.heightDropdownOpt[0];
    this.heightOptions = this.heightOptions === undefined ? 'm.cm' : this.heightOptions;

    this.weightDropdownOpt = ['Kilograms', 'Pound', 'Stones'];
    this.weightDropDown = this.weightDropdownOpt[0];
    this.weighOptions = this.weighOptions === undefined ? 'kg' : this.weighOptions;
    this.weightMaxLen = 3;
    
    this.auraSessionID = this._CommonAPI.getAppNumber();
    
    if(this.changeCoverData.auraType == 'short'){
    	this.auraSessionID = this.auraSessionID + "1";
    }else{
    	this.auraSessionID = this.auraSessionID + "2";
    }
    
    if(this.changeCoverData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.changeCoverData.applicant.cover[0].coverCategory === 'DcTailored'){
		  this.deathAmount = this.changeCoverData.applicant.cover[0].additionalCoverAmount;
		  this.tpdAmount = this.changeCoverData.applicant.cover[1].additionalCoverAmount;		 
	 }else{
		 this.deathAmount = parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
		 this.tpdAmount = parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
	 }
    this.ipAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount);
    
    
    //fix for aura IP unchanged
    if(this.ipAmount == 0){
    	 if(this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod != this.changeCoverData.applicant.cover[2].existingIpBenefitPeriod || this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod != this.changeCoverData.applicant.cover[2].existingIpWaitingPeriod){
    	    	this.ipAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount);
    	 }
    }else if(this.ipAmount < 0){
    	this.ipAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount);
    }
    
    this.auraData = {
    		'fund': 'INGD',
    		'mode': 'change',
    		'age': parseInt(this.changeCoverData.applicant.age),
    		'name': this.changeCoverData.applicant.firstName + ' ' + this.changeCoverData.applicant.lastName,
    		'appnumber': parseInt(this.auraSessionID),
    		'gender': this.changeCoverData.applicant.gender,
    		'country': "Australia",
    		'fifteenHr': this.changeCoverData.applicant.workHours,
    		'deathAmt': this.deathAmount,
    		'tpdAmt': this.tpdAmount,
    		'ipAmt': this.ipAmount,
    		'waitingPeriod': this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod,
    	    'benefitPeriod': this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod == "Age 67" ? "Age 65" :this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod,
    	    'industryOcc': this.changeCoverData.applicant.industryType+':'+this.changeCoverData.applicant.occupation,
    	    'salary':  this.changeCoverData.applicant.annualSalary.toString(),
    	    'clientname': 'metaus',
    	    'lastName': this.changeCoverData.applicant.lastName,
    	    'firstName': this.changeCoverData.applicant.firstName,
    	    'dob': this.changeCoverData.applicant.birthDate,
    	    'existingTerm': false,
    	    'memberType': "None",
    	    'auraType':this.changeCoverData.auraType,
    	    };
    
    console.log(JSON.stringify(this.auraData));
   
    this._AuraAPI.fetchAuraQuestions(this.urlList.auraTransferDataUrl, this.auraData).then(
      (res) => {
        this.auraResponseDataList = res.sections;
        this.updateSectionStatus();
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );
  }
  
  updateSectionStatus(){
	  const _this = this;
	  angular.forEach(_this.auraResponseDataList, function (Object, index) {
		  _this.questionAlias = Object.sectionName;
		  _this.questionlist = Object.questions;
			
			if (_this.questionAlias === 'QG=Health Questions') {
				_this.auraResponseDataList[index].sectionStatus = true;
			} else {
				var keepGoing = true;
				angular.forEach(_this.questionlist, function (Object, index1) {
					if (Object.questionId === '4') {
						Object.branchComplete = false;
					}
					if (keepGoing && Object.branchComplete) {
						_this.auraResponseDataList[index].sectionStatus = true;
						keepGoing = false;
					}
				});
			}
		});
		this.updateHeightWeightValue();
  }
  
  setHeightWeight() {
		this.heightval = this.heightval || null;
		this.weightVal = this.weightVal || null;
		if (this.heightval !== null || this.weightVal !== null) {
			this.submitHeightWeightQuestion(2);
		}
	};
	
	inchValue(value) {
		this.heightin = value;
	}
	
	meterValue(value, answer, questionObj) {
		if (!angular.isUndefined(value)) {
			this.heightinMeter = value;
			this.heightval = value;
			questionObj.error = false;
		} else {
			questionObj.error = true;
		}
	}

	heighOptions(value) {
		if (value === 'cm') {
			this.heightOptions = 'm.cm';
		} else {
			this.heightval = '';
			this.heightOptions = 'ft.in';
		}
	}
	
	weightValue(value, answer, questionObj) {

		if (!angular.isUndefined(value)) {
			this.weightVal = value;
			questionObj.error = false;
		} else if (!angular.isUndefined(value) && value === '') {
			questionObj.error = true;
		}

	}
	
	weightOptions(value, answer) {
		if (value === 'Kilograms') {
			this.weighOptions = 'kg';
			this.weightDropDown = 'Kilograms';
			this.weightMaxLen = 3;
		} else if (value === 'Pound') {
			this.weighOptions = 'lb';
			this.weightDropDown = 'Pound';
			this.weightMaxLen = 3;
		} else if (value === 'Stones') {
			this.weighOptions = 'st.lb';
			this.weightDropDown = 'Stones';
			this.weightMaxLen = 100;
		}
	}

	lbsValue(value) {
		this.lbsval = value;
	}
	
  updateHeightWeightValue() {
	  const _this = this;
	  
	  angular.forEach(_this.auraResponseDataList, function (Object, index) {
		  _this.questionAlias = Object.sectionName;
		  _this.questionlist = Object.questions;
			if (_this.questionAlias === 'QG=Health Questions') {
				_this.healthQuestion = _this.auraResponseDataList[index].questions;
				angular.forEach(_this.healthQuestion, function (question) {
					if (question.classType === 'HeightQuestion') {
						if (question.answerTest.indexOf('Metres') !== -1) {

							_this.heightval = parseInt(question.answerTest.split(',')[0].split('Metres')[0]) * 100 + parseInt(question.answerTest.split(',')[1].split('cm')[0]);
							_this.heightDropDown = 'cm';
							_this.heightOptions = 'm.cm';
						}
						if (question.answerTest.indexOf('Feet') !== -1) {
							_this.heightDropDown = 'Feet';
							_this.heightval = question.answerTest.substring(0, question.answerTest.indexOf('Feet') - 1);
							_this.inches = question.answerTest.substring(question.answerTest.indexOf(',') + 2, question.answerTest.indexOf('in') - 1);
							_this.heightOptions = 'ft.in';
						}

					} else if (question.classType === 'WeightQuestion') {
						if (question.answerTest.indexOf('Kilograms') !== -1) {
							_this.weightDropDown = 'Kilograms';
							_this.weightVal = question.answerTest.substring(0, question.answerTest.indexOf('Kilograms') - 1);
							_this.weighOptions = 'kg';
						}
						if (question.answerTest.indexOf('Pound') !== -1) {
							_this.weightDropDown = 'Pound';
							_this.weightVal = question.answerTest.substring(0, question.answerTest.indexOf('Pound') - 1);
							_this.weighOptions = 'lb';
						}
						if (question.answerTest.indexOf('Stones') !== -1) {
							_this.weightDropDown = 'Stones';
							_this.weightVal = question.answerTest.substring(0, question.answerTest.indexOf('Stones') - 1);
							_this.weighOptions = 'st.lb';
							_this.lbsval = question.answerTest.substring(question.answerTest.indexOf(',') + 2, question.answerTest.indexOf('lbs') - 1);
						}
					}
				});
			}
		});	
  }

updateRadio(answerValue, questionObj) {
  console.log(this.auraResponseDataList);
  questionObj.arrAns[0] = answerValue;
  let auraRes = {
    'questionID': questionObj.questionId,
    'auraAnswers': questionObj.arrAns
  };
  this._AuraAPI.submitAuraQuestions(this.urlList.auraPostUrl, auraRes).then(
    (response) => {
      const _this = this;
      _this.selectedIndex = this.auraResponseDataList.indexOf(questionObj);
      this.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data);
      _this.auraResponseDataList[this.selectedIndex] = response.data;
      angular.forEach(this.auraResponseDataList, function (obj, selectedIndex) {
        if(_this.selectedIndex > selectedIndex && !obj.questionComplete) {
          //branch complete false for previous questions
          _this.auraResponseDataList[selectedIndex].error = true;
        } else if(obj.questionComplete){
          _this.auraResponseDataList[selectedIndex].error = false;
        }
      });
    },
    (err) => {
      this.errors = err.data.errors;
    }
  );
  }

	updateFreeText(answerValue, questionObj) {
		
		const _this = this;
		
		questionObj.arrAns = [];
		questionObj.arrAns[0] = answerValue;
		questionObj.arrAns[1] = 'accept';
		
		let auraRes = {
			    'questionID': questionObj.questionId,
			    'auraAnswers': questionObj.arrAns
		};
		
		_this._AuraAPI.submitAuraQuestions(_this.urlList.auraPostUrl, auraRes).then(
		    (response) => {
		    	_this.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
		    },
		    (err) => {
		    	_this.errors = err.data.errors;
		     }
		);	
	}
	
	searchValSubmit(answerValue, questionObj, parentQuestionObj) {
		
		const _this = this;
		
		questionObj.arrAns = [];
		questionObj.arrAns[0] = questionObj.answerValue.answerValue;
		
		let auraRes = {
			    'questionID': parentQuestionObj.questionId,
			    'auraAnswers': questionObj.arrAns
		};
		
		_this._AuraAPI.submitAuraQuestions(_this.urlList.auraPostUrl, auraRes).then(
			    (response) => {
			    	_this.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
			    },
			    (err) => {
			    	_this.errors = err.data.errors;
			     }
			);
	};

	collapseUncollapse (index, sectionName) {
		
		const _this = this;
		
		if (sectionName === 'QG=Health Questions' && this.changeCoverData.auraType=='long') {
			_this.submitHeightWeightQuestion(index);
		} else {
			var keepGoing = true;
			angular.forEach(_this.auraResponseDataList[index].questions, function (Object, index1) {
				_this.setprogressiveError(_this.auraResponseDataList[index].questions[index1]);
				if (keepGoing && !Object.branchComplete) {
					keepGoing = false;
				}
	
			});
			if (keepGoing) {
				index = index + 1;
				if (_this.auraResponseDataList[index].sectionStatus) {
					index = index + 1;
				}
				_this.auraResponseDataList[index].sectionStatus = true;
	
			}
		}
	}
	
	isCollapsible(event, sectionStatus) {

		if (sectionStatus === false) {
			event.stopPropagation();
			return false;
		}

	}

	submitHeightWeightQuestion(sectionIndex) {
		
		 const _this = this;
		
		angular.forEach(_this.auraResponseDataList, function (Object, index) {
			angular.forEach(Object.questions, function (Object) {
				var serviceCallRequired = false;
				var questionObj = Object;
				if (Object.classType === 'HeightQuestion') {
					questionObj = Object;
					if (_this.heightOptions === 'm.cm') {
						questionObj.arrAns = [];
						questionObj.arrAns[0] = _this.heightOptions;
						questionObj.arrAns[1] = '0';
						questionObj.arrAns[2] = _this.heightval;
					} else if (_this.heightOptions === 'ft.in') {
						questionObj.arrAns = [];
						questionObj.arrAns[0] = _this.heightOptions;
						questionObj.arrAns[1] = _this.heightval;
						questionObj.arrAns[2] = _this.heightin;
					}
	
					serviceCallRequired = true;
				} else if (Object.classType === 'WeightQuestion') {
					questionObj = Object;
					if (_this.weightDropDown === 'Stones') {
						questionObj.arrAns = [];
						questionObj.arrAns[0] = 'st.lb';
						questionObj.arrAns[1] = _this.weightVal;
						questionObj.arrAns[2] = _this.lbsval;
					} else if (_this.weightDropDown === 'Kilograms') {
						questionObj.arrAns = [];
						questionObj.arrAns[0] = 'kg';
						questionObj.arrAns[1] = _this.weightVal;
					} else if (_this.weightDropDown === 'Pound') {
						questionObj.arrAns = [];
						questionObj.arrAns[0] = 'lb';
						questionObj.arrAns[1] = _this.weightVal;
					}
	
					serviceCallRequired = true;
				}
				if (serviceCallRequired && questionObj.arrAns.length > 1) {
					
					let auraRes = {
						    'questionID': questionObj.questionId,
						    'auraAnswers': questionObj.arrAns
					};
					
					_this._AuraAPI.submitAuraQuestions(_this.urlList.auraPostUrl, auraRes).then(
						    (response) => {
						    	_this.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
						    	_this.updateHeightWeightValue();
						    	var keepGoing = true;
						    	angular.forEach(_this.auraResponseDataList[index].questions, function (Object, index1) {
						    		_this.setprogressiveError(_this.auraResponseDataList[sectionIndex].questions[index1]);
									if (Object.questionId === '4') {
										Object.branchComplete = true;
									}
									if (keepGoing && !Object.branchComplete) {
										keepGoing = false;
									}			
								});
								if (keepGoing) {
									index = index + 1;
									_this.auraResponseDataList[index].sectionStatus = true;
								}
						    },
						    (err) => {
						    	_this.errors = err.data.errors;
						     }
						    );
				} else {
					angular.forEach(_this.auraResponseDataList[sectionIndex].questions, function (Object, index1) {
						_this.setprogressiveError(_this.auraResponseDataList[sectionIndex].questions[index1]);
					});
	
				}
	
			});
		});
	
	}

  updateQuestionList(questionId, changedQuestion, questionObj) {
	  
	  const _this = this;
	  
	  angular.forEach(_this.auraResponseDataList, function (Object, index) {
		  _this.questionlist = Object.questions;
		  
		  angular.forEach(_this.questionlist, function (Obj, index1) {
				if (Obj && changedQuestion) {
					if (Obj.questionId === changedQuestion.questionId) {
						_this.auraResponseDataList[index].questions[index1] = changedQuestion;
						_this.progressive(questionId, changedQuestion, questionObj);
					}
				}
			});
		});
	}
  
  progressive(questionid, changedQuestion, questionObj) {
	  
	  const _this = this;
	  
	  angular.forEach(_this.auraResponseDataList, function (Object, selectedIndex) {
		  if (changedQuestion.questionAlias === Object.sectionName) {
			  _this.questionIndex = _this.auraResponseDataList[selectedIndex].questions.indexOf(changedQuestion);
			  angular.forEach(_this.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
				  if (index <= _this.questionIndex) {
					  if (index === _this.questionIndex){
						  var keepGoing = true;
						  _this.subprogressive(_this.auraResponseDataList[selectedIndex].questions[index], questionid, keepGoing, questionObj);
						}else{
							_this.setprogressiveError(_this.auraResponseDataList[selectedIndex].questions[index]);
						}
					 }
				});
			}

		});
	}
  
  setprogressiveError(baseQuestionObj) {
	  
	  const _this = this;
	  
	  if(baseQuestionObj && baseQuestionObj.questionComplete) {
			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
					if (subauraQustion.questionComplete) {
						_this.setprogressiveError(subauraQustion);
					} else {
						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error = true;
						_this.keepGoing = false;
					}
				});

			});
		} else if (baseQuestionObj && baseQuestionObj.classType === 'HeightQuestion' && (!angular.isUndefined(this.heightval) && this.heightval.length > 0)) {
			baseQuestionObj.error = false;
		} else if (baseQuestionObj && baseQuestionObj.classType === 'WeightQuestion' && (!angular.isUndefined(this.weightVal) && this.weightVal.length > 0)) {
			baseQuestionObj.error = false;
		} else if (baseQuestionObj) {
			baseQuestionObj.error = true;
		}
  }
  
  subprogressive(baseQuestionObj, questionid, keepGoing, updatedQuestionObj) {
	  
	  const _this = this;
	  
	  if (baseQuestionObj && baseQuestionObj.questionId < questionid){
		  	angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
		  		angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
		  			
		  			if (!subauraQustion.questionComplete && subauraQustion.questionId < questionid) {
						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error = true;
					} else if (subauraQustion.questionComplete && subauraQustion.questionId < questionid) {
						_this.subprogressive(subauraQustion, questionid, keepGoing, updatedQuestionObj);
					} else if (subauraQustion.questionComplete && subauraQustion.questionId === questionid) {
						var arrayAnswer = null;
						if (subauraQustion.answerTest.indexOf(',') !== -1) {
							arrayAnswer = subauraQustion.answerTest.split(',');
						} else if (!angular.isUndefined(subauraQustion.answerTest)) {
							arrayAnswer = [];
							arrayAnswer[0] = subauraQustion.answerTest;
						}
						angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].auraAnswer, function (answer, answerIndex) {
							if (arrayAnswer[arrayAnswer.length - 1].indexOf(answer.answerValue) !== -1) {
								_this.checkboxProgressive(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex], answerIndex);
							}
						});
					}
				});
			});
		} else if (baseQuestionObj && baseQuestionObj.questionId === questionid && baseQuestionObj.answerTest === updatedQuestionObj.answerTest) {
			var arrayAnswer = null;
			if (baseQuestionObj.answerTest.indexOf(',') !== -1) {
				arrayAnswer = baseQuestionObj.answerTest.split(',');
			} else if (!angular.isUndefined(baseQuestionObj.answerTest)) {
				arrayAnswer = [];
				arrayAnswer[0] = baseQuestionObj.answerTest;
			}
			angular.forEach(baseQuestionObj.auraAnswer, function (answer, answerIndex) {
				if (arrayAnswer[arrayAnswer.length - 1].indexOf(answer.answerValue) !== -1) {
					_this.checkboxProgressive(baseQuestionObj, answerIndex);
				}
			});
		}

	}
	
	checkboxProgressive(baseQuestionObj, answerIndex) {
		
		const _this = this;
		
		if (baseQuestionObj) {
			angular.forEach(baseQuestionObj.auraAnswer, function (answer, index) {
				if (index < answerIndex) {
					angular.forEach(baseQuestionObj.auraAnswer[index].childQuestions, function (subauraQustion, subSelectedIndex) {
						_this.setprogressiveError(subauraQustion);
					});
				}
			});
		}
	};
  
  visible(idshow) {
		if(document.getElementById(idshow) != null){
			document.getElementById(idshow).style.visibility = 'visible';
		}
	}
  
  updateCheckBox(answerValue, questionObj, index, $event) {
		if((questionObj.answerTest !== '') && (questionObj.answerTest.indexOf('None of the above') < 0) && (answerValue.indexOf('None of the above') > -1)) {
			var savePopupTemplate = '';
			savePopupTemplate += '<div class="ngdialog-content">';
			savePopupTemplate += '<div class="modal-body">';
			savePopupTemplate += '<div class="row  rowcustom ">';
			savePopupTemplate += '<div class="col-sm-12">';
			savePopupTemplate += '<div id="tips_text">';
			savePopupTemplate += '<p>Clicking on "None of the above" will clear your medical disclosures in the above section.</p>';
			savePopupTemplate += '<p class="mt-2">Do you wish to proceed?</p>';
			savePopupTemplate += '</div>';
			savePopupTemplate += '</div>';
			savePopupTemplate += '</div>';
			savePopupTemplate += '</div>';
			savePopupTemplate += '<div class="ngdialog-buttons text-center">';
			savePopupTemplate += '<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
			savePopupTemplate += '<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button>';
			savePopupTemplate += '</div>';
			savePopupTemplate += '</div>';
			
			this._ngDialog.openConfirm({
				template: savePopupTemplate,
				plain: true,
				className: 'ngdialog-theme-plain custom-width'
			}).then(
					() => { 
						this.submitQuestionOnCheckBoxClick(answerValue, questionObj, index);
					}, 
	    	        (event) => {
	      	          if(event === 'oncancel') {
	      	        	angular.element($event.currentTarget).removeClass('active');
	      	        	return false;
	      	          }
	      	        }
			);
		} else {
			this.submitQuestionOnCheckBoxClick(answerValue, questionObj, index);
		}
	};
	
	submitQuestionOnCheckBoxClick(answerValue, questionObj, index) {
		if (questionObj.arrAns.length === 0) {
			var answerArray = questionObj.answerTest.split(',');
			if (answerArray.length > 1) {
				questionObj.arrAns = answerArray;
			} else {
				questionObj.arrAns[0] = questionObj.answerTest;
			}
		}


		var addtoArray = true;
		if (answerValue.indexOf('None of the above') !== -1) {
			questionObj.arrAns = [];
			questionObj.arrAns[0] = answerValue;
		} else {
			for (var i = 0; i < questionObj.arrAns.length; i++) {
				//to remove white space
				questionObj.arrAns[i] = $.trim(questionObj.arrAns[i]);
				if (questionObj.arrAns[i] === answerValue) {
					questionObj.arrAns.splice(i, 1);
					addtoArray = false;
				}
			}
			if (addtoArray === true) {
				questionObj.arrAns.splice(index, 0, answerValue);
			}
		}
		//removing none of the above, if there are other answers
		if (questionObj.arrAns.length > 1) {
			for (var j = 0; j < questionObj.arrAns.length; j++) {
				if (questionObj.arrAns[j].indexOf('None of the above') !== -1) {
					questionObj.arrAns.splice(j, 1);
				}
			}
		}
		
		
		let auraRes = {
			    'questionID': questionObj.questionId,
			    'auraAnswers': questionObj.arrAns
		};
			  this._AuraAPI.submitAuraQuestions(this.urlList.auraPostUrl, auraRes).then(
			    (response) => {
			    	this.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
			    },
			    (err) => {
			        this.errors = err.data.errors;
			     }
			    );
	};

  save() {
	  this.changeCoverData.lastSavedOn = 'aurapage';
    console.log(JSON.stringify(this.changeCoverData));
    this._CommonAPI.saveEapply(this.urlList.saveEapplyUrlNew, this.changeCoverData).then(
      (res) => {
        console.log(res);
        this.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+this.changeCoverData.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );
	  
  }
  
  clickToOpen(hhText) {
	  this._ngDialog.open({
	      template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
	      className: 'ngdialog-theme-plain',
	      showClose: false,
	      plain: true
	    });
  }
  
saveAndExitPopUp(hhText) {
	  
	  this._ngDialog.openConfirm({
		  template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Finish &amp; Close Window </button></div></div>',
		  plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
      }).then(
      () => {
        this._$state.go('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }

  previous() {
	  this._$state.go('app.quote');
  }
  
  formatErrorMsg(){
	  
	  const _this = this;
	  
	  for(var v = 0; v < _this.erroneousSections.length; v++) {
		  _this.erroneousSections[v] = _this.erroneousSections[v].replace('QG=', '');
	  }	
  }
  
  checkIncompleteQuestion(baseQuestionObj, keepGoing) {
	  const _this = this;
	  
	  if(baseQuestionObj.questionComplete && keepGoing) {
		  angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
			  angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
				  if(subauraQustion.questionComplete){
					  _this.setprogressiveError(subauraQustion);
				  }else if(!subauraQustion.questionComplete){
					  _this.keepGoing = false;
				  }
			  });
		  });
		} else if (baseQuestionObj.classType === 'HeightQuestion' && (!angular.isUndefined(_this.heightval) && _this.heightval.length > 0)) {
			_this.keepGoing = true;
		} else if (baseQuestionObj.classType === 'WeightQuestion' && (!angular.isUndefined(_this.weightVal) && _this.weightVal.length > 0)) {
			_this.keepGoing = true;
		} else {
			_this.keepGoing = false;
		}
  }

  continue() {
	  
	  const _this = this;
	  
	  _this.changeCoverData.lastSavedOn = '';
	  _this.erroneousSections = [];
	  
	  angular.forEach(_this.auraResponseDataList, function (Object, selectedIndex) {
		  _this.keepGoing = true;
		  angular.forEach(_this.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
			  if(_this.keepGoing) {
				  _this.checkIncompleteQuestion(_this.auraResponseDataList[selectedIndex].questions[index], _this.keepGoing);			
			  }
		  });
		  
		  if(!_this.keepGoing){
			  _this.erroneousSections.push(Object.sectionName);
		  }
	  });
	  
	  _this.isAuraErroneous = _this.erroneousSections.length > 0;
	  
		if (!_this.isAuraErroneous) {
			_this.submitAura();
		}
		
		if (_this.erroneousSections.length) {
			_this.formatErrorMsg();
		}	
  }
  
  proceedToSummary(){
	  
	  	const _this = this;
		let clientData = {
			    'firstName': _this.changeCoverData.applicant.firstName,
			    'surName': _this.changeCoverData.applicant.lastName,
			    'dob':_this.changeCoverData.applicant.birthDate
		};
		
		_this._AuraAPI.clientMatchSvc(_this.urlList.clientMatchUrl,clientData).then(
				
				(clientMatchResponse) => {
					
					if(clientMatchResponse.data.matchFound) {
						
						_this.clientmatchreason = '';
						_this.auraResponses.clientMatched = clientMatchResponse.data.matchFound;
						_this.auraResponses.overallDecision = 'RUW';
						_this.changeCoverData.appDecision ='RUW';
						_this.information = clientMatchResponse.data.information;
						
						angular.forEach(_this.information, function (infoObj, index){
							
							if(index === _this.information.length - 1){
								_this.clientmatchreason = _this.clientmatchreason + infoObj.system + ' client match : ' + infoObj.key + ', ';
								_this.clientmatchreason = _this.clientmatchreason.substring(0, _this.clientmatchreason.lastIndexOf(','));
							} else {
								_this.clientmatchreason = _this.clientmatchreason + infoObj.system + ' client match : ' + infoObj.key + ', ';
							}
						});
						
						_this.auraResponses.clientMatchReason = _this.clientmatchreason;
						_this.auraResponses.specialTerm = false;	        					
					}
					
					if(_this.auraResponses.overallDecision === 'ACC'){
						_this.changeCoverData.appDecision ='ACC';
					}
					_this.changeCoverData.appDecision = _this.auraResponses.overallDecision;
					_this.changeCoverData.applicant.overallCoverDecision = _this.auraResponses.overallDecision;
					_this.changeCoverData.applicant.cover[0].coverReason = _this.auraResponses.deathAuraResons;
					_this.changeCoverData.applicant.cover[1].coverReason = _this.auraResponses.tpdAuraResons;
					_this.changeCoverData.applicant.cover[2].coverReason = _this.auraResponses.ipAuraResons;
					_this.changeCoverData.applicant.cover[0].coverDecision = _this.auraResponses.deathDecision;
					_this.changeCoverData.applicant.cover[1].coverDecision = _this.auraResponses.tpdDecision;
					_this.changeCoverData.applicant.cover[2].coverDecision = _this.auraResponses.ipDecision;
					_this.changeCoverData.applicant.cover[0].coverExclusion = _this.auraResponses.deathExclusions;
					_this.changeCoverData.applicant.cover[1].coverExclusion = _this.auraResponses.tpdExclusions;
					_this.changeCoverData.applicant.cover[2].coverExclusion = _this.auraResponses.ipExclusions;
					_this.changeCoverData.applicant.cover[0].loading = _this.auraResponses.deathLoading;
					_this.changeCoverData.applicant.cover[1].loading = _this.auraResponses.tpdLoading;
					_this.changeCoverData.applicant.cover[2].loading = _this.auraResponses.ipLoading;
					
					_this._EapplyData.setChangeCoverData(_this.changeCoverData);			
					_this.setHeightWeight();			
					_this._$state.go('app.quotesummary');
	              
				},
				(err) => {
					console.log('Error while submitting Change cover ' + err);
				}
			);	
  }
  
  submitApplication(){
	  const _this = this;
	  
	  _this.changeCoverData.appDecision ='DCL';
	  _this.changeCoverData.applicant.overallCoverDecision ='DCL';
	  _this.changeCoverData.applicant.cover[0].coverReason = _this.auraResponses.deathAuraResons;
	  _this.changeCoverData.applicant.cover[1].coverReason = _this.auraResponses.tpdAuraResons;
	  _this.changeCoverData.applicant.cover[2].coverReason = _this.auraResponses.ipAuraResons;
	  _this.changeCoverData.applicant.cover[0].coverDecision = _this.auraResponses.deathDecision;
	  _this.changeCoverData.applicant.cover[1].coverDecision = _this.auraResponses.tpdDecision;
	  _this.changeCoverData.applicant.cover[2].coverDecision = _this.auraResponses.ipDecision;
	  _this.changeCoverData.applicant.cover[0].coverExclusion = _this.auraResponses.deathExclusions;
	  _this.changeCoverData.applicant.cover[1].coverExclusion = _this.auraResponses.tpdExclusions;
	  _this.changeCoverData.applicant.cover[2].coverExclusion = _this.auraResponses.ipExclusions;
	  _this.changeCoverData.applicant.cover[0].loading = _this.auraResponses.deathLoading;
	  _this.changeCoverData.applicant.cover[1].loading = _this.auraResponses.tpdLoading;
	  _this.changeCoverData.applicant.cover[2].loading = _this.auraResponses.ipLoading;
	  
	  _this._EapplyData.setChangeCoverData(_this.changeCoverData);
	  
	  _this._CommonAPI.submitEapply(_this.urlList.submitEapplyUrlNew, _this.changeCoverData).then(
              (res) => {
            	  _this._EapplyData.setPdfLocation(res.clientPDFLocation);
            	  _this._EapplyData.setNpsUrl(res.npsTokenURL);
            	  _this._$state.go('app.quotedecision');
              }, 
              (err) => {
                console.log('Error while submitting transfer cover ' + err);
              }
       );	  
  }
  
  submitAura(){
	  
	  const _this = this;
	  _this._AuraAPI.submitAuraSession(_this.urlList.submitAuraUrl, _this.auraData).then(
		        (response) => {
		        	_this.auraResponses = response.data;
		        	_this.changeCoverData.responseObject = _this.auraResponses.responseObject;
		        	if(response.status === 200) {		        		
		        		if(_this.auraResponses.overallDecision !== 'DCL') {
		        			this.proceedToSummary();
		        		}else if(_this.auraResponses.overallDecision === 'DCL') {
		        			this.submitApplication();
		        		}
					}		        	
		        }
	   );
  }
}

export default QuoteAuraCtrl;
