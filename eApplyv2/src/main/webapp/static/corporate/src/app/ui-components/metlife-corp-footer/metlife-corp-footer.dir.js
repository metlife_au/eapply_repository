(function (angular) {
  'use strict';

  angular
    .module('Metlife.corp.uicomponents')
    .directive('metlifeCorpFooter', metlifeCorpFooter);

  function metlifeCorpFooter(APP_CONSTANTS, $document) {
    var directive = {
      restrict: 'EA',
      templateUrl: APP_CONSTANTS.path.uiComponents + '/metlife-corp-footer/metlife-corp-footer.tmpl.html',
      link: link,
      controller: footerCtrl,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
    function link(scope, element, attrs, vm) {
      
    }
  }

  footerCtrl.$inject = ['$scope', 'APP_CONSTANTS'];

  function footerCtrl($scope, APP_CONSTANTS) {
    var vm = this;
    vm.footerNav = {
      privacyUrl: APP_CONSTANTS.navUrls.privacyUrl,
      aboutUsUrl: APP_CONSTANTS.navUrls.about,
      legalNoticeUrl:APP_CONSTANTS.navUrls.legalNoticeUrl
    };
  }
})(angular);