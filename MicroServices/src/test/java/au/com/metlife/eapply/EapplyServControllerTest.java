package au.com.metlife.eapply;


import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rgatp.aura.wsapi.Question;

import au.com.metlife.eapply.bs.controller.EapplyServicesController;
import au.com.metlife.eapply.bs.serviceimpl.EapplyServiceImpl;
import au.com.metlife.eapply.underwriting.model.AuraSession;
import au.com.metlife.eapply.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EapplyServControllerTest extends TestUtils{
	
	@InjectMocks
	private EapplyServicesController eapplyServicesController;
	
	 @InjectMocks
	 private EapplyServiceImpl service;
	 
	@Autowired
	 private AuraSession auraSession;
	
	private MockMvc mockMvc;
	
	/*
	 * Created testData to store the string json value from createTestData()
	 */	
	String testData = null;
	
	/*
	 * Created fileName to store the  json file name and pass it to createTestData()
	 */
	String fileName =null;


	@Autowired
	private WebApplicationContext context;
	
	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	
	@Test
	public void retriveApplication() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "1515504414745")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void retriveApplicationCCover() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "1518097272634")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void retriveApplicationTCover() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "15154217136213")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void retriveApplicationUWCover() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "15154785333373")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void retriveApplicationCANCover() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "15154901400573")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void retriveApplicationSCover() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "15154994033133")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void retriveApplicationICover() throws Exception {
		mockMvc.perform(get("/retieveApplication").param("applicationNumber", "15155044147453")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	
	// checkSavedApplications for Host
	
	@Test
	public void getcheckSavedApplicationsHostTest() throws Exception {

		
			mockMvc.perform(
					get("/checkSavedApplications?fundCode=HOST&clientRefNo=7&manageType=CCOVER").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
		   ;

	
}
	
	// checkSavedApplications for AEIS
	@Test
	public void getcheckSavedApplicationsAEISTest() throws Exception {

		
			mockMvc.perform(
					get("/checkSavedApplications?fundCode=AEIS&clientRefNo=AEIS7&manageType=CCOVER").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
		    ;

	
}
	
	// checkSavedApplications for Care Super
	
	@Test
	public void getcheckSavedApplicationsCARETest() throws Exception {

		
			mockMvc.perform(
					get("/checkSavedApplications?fundCode=CARE&clientRefNo=7&manageType=CCOVER").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
		    ;

	
}

	// checkSavedApplications for invalid fundcode
	@Test
	public void getcheckSavedApplicationsCORPORATETest() throws Exception {

		
			mockMvc.perform(
					get("/checkSavedApplications?fundCode=CORPORATE&clientRefNo=5756776&manageType=CCOVER").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
		    ;

	
}
	
	// checkSavedApplications for valid fundcode and invalid clientref no:
	@Test
	public void getcheckSavedApplicationsInvalidClientRefTest() throws Exception {

		mockMvc.perform(
					get("/checkSavedApplications?fundCode=HOST&clientRefNo=345&manageType=CCOVER").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
		    ;

	
}
	
	// checkSavedApplications for Invalid manageType no:
	@Test
	public void getcheckSavedApplicationsInvalidmanageTypeTest() throws Exception {

		mockMvc.perform(
					get("/checkSavedApplications?fundCode=HOST&clientRefNo=7&manageType=56456cd").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
		    ;

	
}
	
	//for /getUniqueNumber
	


	@Test
	public void getUniqueNumberHOST() throws Exception {
	
	
		mockMvc.perform(
				get("/getUniqueNumber").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		;
}

	
	// /printQuotePage for HOST
		@Test
		public void printQuotePageHOST() throws Exception {
			String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE56UTNNVGt5TkN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNE5UYzBNakF6Tmkwek1qRTBMVFEzTm1JdFlUQXpNUzFpTUdNMU1HRm1ZVFU1TVRNaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5yblYzVjV0U19CeFJ4RzVnRnVSdWpfZEJhckpoVFdRaEJQcDVlNXoyRk0w";
			fileName = "printQuotePageHost.json";
			String testEapplyInputData = createEapplyInputTestData(fileName);
			mockMvc.perform(post("/printQuotePage").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON).content(testEapplyInputData))
			.andExpect(status().isOk())
			;
		}
	
	// /printQuotePage for AEIS
	@Test
	public void printQuotePageAEIS() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE56UTNOekV5TXl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaVlUVTJObVJoTmkxak9ETXhMVFJqWTJVdE9HRTFNUzFrWXpRNFpXSTJZV0kxTm1JaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5vbkhfX25hc2cta09zSml2ZDF0WUlwMWRuOWRqRmNKb1JxT3ZmcFp3d1I0";
		fileName = "printQuotePageAEIS.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/printQuotePage").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON).content(testEapplyInputData))
		.andExpect(status().isOk())
		;
	}

	// /printQuotePage for Invalid authId
	@Test
	public void printQuotePageInvalidFundCode() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STdvffg";
		fileName = "printQuotePageCorpPartnerCode.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/printQuotePage").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON).content(testEapplyInputData))
		.andExpect(status().isOk())
		;
	}

	// /printQuotePage for null Body
	@Test
	public void printQuotePageNullBody() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE56UTNOekV5TXl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaVlUVTJObVJoTmkxak9ETXhMVFJqWTJVdE9HRTFNUzFrWXpRNFpXSTJZV0kxTm1JaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5vbkhfX25hc2cta09zSml2ZDF0WUlwMWRuOWRqRmNKb1JxT3ZmcFp3d1I0";
		fileName = "QuoteControllercalculateAllNull.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/printQuotePage").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON).content(testEapplyInputData))
		.andExpect(status().isOk())
		;
	}

	// /printQuotePage for null AuthId
	@Test
	public void printQuotePageNullAuthId() throws Exception {
		String authId = "";
		fileName = "printQuotePageHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/printQuotePage").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON).content(testEapplyInputData))
		.andExpect(status().isOk())
		;
	}
	
	@Test
	public void underwritting() throws Exception {	
		String underwrittingInput = "{\"auraID\":\"\",\"questionID\":5,\"auraAnswers\":[\"YES\"]}";
		mockMvc.perform(post("/underwriting").contentType(MediaType.APPLICATION_JSON).content(underwrittingInput))
		.andExpect(status().isNoContent())
		;
	}
	
	@Test
	public void clientMatch() throws Exception {
		String underwrittingInput = "{\"firstName\":\"HOST\",\"lastName\":\"HOSTB\",\"dob\":\"01/11/1958\"}";
		mockMvc.perform(post("/clientMatch").contentType(MediaType.APPLICATION_JSON).content(underwrittingInput))
		.andExpect(status().isOk()).andReturn().getResponse()
		.equals(true);
	}
	
	
	
	// for /download for HOST
	/*@Test
	public void getdownloadHOST() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/download?file_name=2c46ecca5c38b0d4f9053d21378e1aa737d394130b1102f8444d622eb9b1a886342a49baa13315ac"))
		.andExpect(status().isOk())
		;
	}*/

	// for /download for AEIS
	/*@Test
	public void getdownloadAEIS() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/download?file_name=8ffba3f2b948d75b530772d8861cb1542f9b71f1f164e2e7d0564a14d37cc47df586602a90bacfe82c06a63db96268f80c855fc0282f193eec68192c1b0f083c"))
		.andExpect(status().isOk())
		;
	}*/

	// for /download for CARE
	/*@Test
	public void getdownloadCARE() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/download?file_name=1dbb2bcc1ae252bdb508eabcdc2d9f449af79aa04a86fabdc46dde7bade3eacb8adf5ddca726fc3b"))
		.andExpect(status().isOk())
		;
	}*/
	

	//check24hrs valid fundCode for HOST
		@Test
		public void check24hrs() throws Exception {


				mockMvc.perform(
						MockMvcRequestBuilders.get("/check24hrs?fundCode=HOST&clientRefNo=7&manageType=CCOVER").accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						;
}
		
		
		
		


	
}
