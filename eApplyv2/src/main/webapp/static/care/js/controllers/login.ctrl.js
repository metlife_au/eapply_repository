/*Login Page Controller Starts*/  
CareSuperApp.controller('login',['$scope','$rootScope','$routeParams', '$location','$window', 'getclientData', 'tokenNumService','deathCoverService','tpdCoverService',
                                 'ipCoverService','newMemberOfferService', 'persoanlDetailService', '$http', '$window','appNumberService',
                                 'auraInputService', 'PersistenceService','ngDialog', 'CheckSavedAppService', 'CancelSavedAppService','$timeout','urlService','urls', 'submitAura','clientMatch','RetrieveAppDetailsService', '$q',
                                 function($scope,$rootScope, $routeParams, $location,$window, getclientData, tokenNumService, deathCoverService, tpdCoverService,
                                		 ipCoverService, newMemberOfferService, persoanlDetailService, $http, $window,appNumberService,auraInputService,
                                		 PersistenceService, ngDialog, CheckSavedAppService, CancelSavedAppService,$timeout,urlService,urls,submitAura,clientMatch,RetrieveAppDetailsService, $q){
	
	var client_data;
  var applicant_data;
	
	$rootScope.$broadcast('enablepointer');
	//$rootScope.urlList = urls.data;
	urlService.setUrlList(urls.data);
	$scope.urlList = urlService.getUrlList();
	//Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
  	$scope.isEmpty = function(value){
  		return ((value == "" || value == null) || value == "0");
  	};
  	 $scope.deathCover = {
  		      amount: "0.0",
  		      benefitPeriod: "",
  		      benefitType: "1",
  		      coverStartDate: null,
  		      coverUnit: null,
  		      description: null,
  		      exclusions: "",
  		      indexation: null,
  		      loading: "0.0",
  		      occRating: "General",
  		      type: "2",
  		      units: "0",
  		      waitingPeriod:""
  		    };
  		    $scope.tpdCover = {
  		      amount: "0.0",
  		      benefitPeriod: "",
  		      benefitType: "2",
  		      coverStartDate: null,
  		      coverUnit: null,
  		      description: null,
  		      exclusions: "",
  		      indexation: null,
  		      loading: "0.0",
  		      occRating: "General",
  		      type: "2",
  		      units: "0",
  		      waitingPeriod:""
  		    };
  		    $scope.ipCover = {
  		      amount: "0.0",
  		      benefitPeriod: "",
  		      benefitType: "4",
  		      coverStartDate: null,
  		      coverUnit: null,
  		      description: null,
  		      exclusions: "",
  		      indexation: null,
  		      loading: "0.0",
  		      occRating: "General",
  		      type: "2",
  		      units: "0",
  		      waitingPeriod:""
  		    };
 // added for session expiry
 //  	SessionTimeoutService.stopWatch();
 //  	SessionTimeoutService.setup();
	// $scope.$on('$destroy', function() {
	// 	SessionTimeoutService.stopWatch();
	// 	if(!$scope.$$phase){
	// 		$scope.$apply();
	// 	}
	// });
	
     
	$scope.message = 'Look! I am an about page.';
	var clientRefNum;
    tokenNumService.setTokenId(inputData)     
    //$location.search( 'InputData', null );
      getclientData.requestObj($scope.urlList.clientDataUrl).then(function(response) {    	
    	$scope.partnerRequest = response.data;
    	$scope.calcEncrpytURL = $scope.partnerRequest.calcEncrpytURL;
    	$scope.applicantlist = $scope.partnerRequest.applicant;
    	client_data = $scope.partnerRequest;
      applicant_data = client_data.applicant[0];
    	
    	if($scope.applicantlist[0] && $scope.applicantlist[0].clientRefNumber){
    		clientRefNum = $scope.applicantlist[0].clientRefNumber;
    	}
    	angular.forEach($scope.applicantlist, function (applicant) {        		
    		$scope.personalDetails = applicant.personalDetails          	
    		$scope.coverList = applicant.existingCovers;         		
    		var age = moment().diff(moment(applicant.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years') + 1;  
    		$scope.maxAge = age - 1;
    		$scope.newMemberOffer = false;
        applicantData = {
          clientRefNumber: applicant_data.clientRefNumber,
          gender: applicant_data.personalDetails.gender,
          partnerID: client_data.partnerID,
          age: $scope.maxAge,
          //dateOfBirth: applicant_data.personalDetails.dateOfBirth
        };
    		/*var date = moment().diff(moment(applicant.welcomeLetterDate, 'DD-MM-YYYY'), 'days')+1;
    		console.log("date"+date);*/
    		if(moment().diff(moment(applicant.welcomeLetterDate, 'DD-MM-YYYY'), 'days') +1<91 && moment(moment(moment(applicant.welcomeLetterDate,'DD-MM-YYYY')).format('MM-DD-YYYY')).isAfter('01-31-2017', 'day') && age<61 && (applicant.memberType=='Industry' || applicant.memberType=='Corporate')){
    			$scope.newMemberOffer = true;
    		}else{
    			$scope.newMemberOffer = false;
    		}
    		newMemberOfferService.setNewMemberOfferFlag($scope.newMemberOffer);        		
    		angular.forEach($scope.coverList, function (cover) {            		
        		angular.forEach(cover, function(value, key) {
        			$scope.benefitType = value.benefitType;            			
        			 if($scope.benefitType =='1'){
        				 $scope.deathCover = value;
        				 /*deathCoverService.setDeathCover($scope.deathCover)   */         				          				 
        			 } else if($scope.benefitType =='2'){
        				 $scope.tpdCover = value;     
        				 /*tpdCoverService.setTpdCover($scope.tpdCover)  */          				 
        			 }else  if($scope.benefitType =='4'){
        				 $scope.ipCover = value;   
        				 /*ipCoverService.setIpCover( $scope.ipCover)     */       				
        			 }
        			});
        		deathCoverService.setDeathCover($scope.deathCover); 
				 tpdCoverService.setTpdCover($scope.tpdCover);  
				 ipCoverService.setIpCover( $scope.ipCover);
        		
        	});
    		$scope.appContactDetails = applicant.contactDetails || {};
    	      $scope.appContactDetails.homePhone = $scope.appContactDetails.homePhone ? $scope.appContactDetails.homePhone.replace(/[^0-9]/g,'') : $scope.appContactDetails.homePhone;
    	      $scope.appContactDetails.mobilePhone = $scope.appContactDetails.mobilePhone ? $scope.appContactDetails.mobilePhone.replace(/[^0-9]/g,'') : $scope.appContactDetails.mobilePhone;
    	      $scope.appContactDetails.workPhone = $scope.appContactDetails.workPhone ? $scope.appContactDetails.workPhone.replace(/[^0-9]/g,'') : $scope.appContactDetails.workPhone;
    	      angular.extend(applicant.contactDetails, $scope.appContactDetails);
    		persoanlDetailService.setMemberDetails($scope.applicantlist);
    	});
    });
    $scope.go = function (path, generateAppnum, num) {
    	if(generateAppnum){
    		//generate app number
    		appNumberService.requestObj($scope.urlList.appNumUrl).then(function(response){
        		PersistenceService.setAppNumber(response.data);
            applicantData = {
              clientRefNumber: applicant_data.clientRefNumber,
              gender: applicant_data.personalDetails.gender,
              partnerID: client_data.partnerID,
              age: $scope.maxAge,
              //dateOfBirth: applicant_data.personalDetails.dateOfBirth,
              app_number: response.data
            }
        		$location.path(path);
        	 });
    	} else{
        if(num){
          applicantData = {
            clientRefNumber: applicant_data.clientRefNumber,
            gender: applicant_data.personalDetails.gender,
            partnerID: client_data.partnerID,
            age: $scope.maxAge,
            //dateOfBirth: applicant_data.personalDetails.dateOfBirth,
            app_number: num
          };
        }
    		$location.path(path);
    	}
  	};
  	$scope.tokenid = tokenNumService.getTokenId();
    $scope.openWindow = function(url) {
        $window.open(url);
    }
    
    // added for new member popup on landing page
    $scope.showNewMemberPopUp = function(val){	
   		if(val == null || val == "" || val == " "){
   			hideTips();
   		}else{
   		 document.getElementById('mymodalNewMem').style.display = 'block';
   		 document.getElementById('mymodalNewMemFade').style.display = 'block';
   		 document.getElementById('newMember_text').innerHTML=val;	
   		}
   	}
    $scope.hideTips =function(){
   		if(document.getElementById('help_div')){
   			document.getElementById('help_div').style.display = "none";
   		}						
   	};
   	
   	$scope.intvalue = function(amount) {
   	  return parseInt(amount) > 0 ? true : false;
   	}
   	
   	$scope.showSavedAppPopup = function(manageType){
   		CheckSavedAppService.checkSavedApps($scope.urlList.savedAppUrl,"CARE",clientRefNum,manageType).then(function(res){
   		//CheckSavedAppService.checkSavedApps({fundCode:"CARE", clientRefNo:clientRefNum, manageType:manageType}, function(res){
   			//$scope.apps = res;
   			$scope.apps = res.data;
   			if($scope.apps.length > 0){
	   			var newScope = $scope.$new();
	   			for(var i = 0; i < $scope.apps.length; i++){
	   				var tempDate = new Date($scope.apps[i].createdDate);
	   				$scope.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');
	   				
	   				if($scope.apps[i].requestType == "CCOVER"){
	   					$scope.apps[i].requestType = "Change Cover";
	   				} else if($scope.apps[i].requestType == "TCOVER"){
	   					$scope.apps[i].requestType = "Transfer Cover";
	   				} else if($scope.apps[i].requestType == "UWCOVER"){
	   					$scope.apps[i].requestType = "Update Work Rating";
	   				} else if($scope.apps[i].requestType == "NCOVER"){
	   					$scope.apps[i].requestType = "New Member Cover";
	   				}
	   			}
	   			// Created a new scope for the modal popup as the parent controller variables are not accessible to the modal
	   			newScope.apps = $scope.apps;
	   			
	   			ngDialog.openConfirm({
	   	            template:'<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">You have previously saved application(s).</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-12"><p> Please continue with the saved application or cancel saved application to start a new application. </p> </div> </div><!-- Row ends --> <div class="row visible-xs"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th width="20%" class="coverbg"><h5 class=" ">Application no.</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Date saved</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div></div> <div class="row hidden-xs"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th width="20%" class="coverbg"><h5 class=" ">Application no.</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Date saved</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div></div>',
	   	            plain: true,
	   	            className: 'ngdialog-theme-plain custom-width',
	   	            scope: newScope
	   	        }).then(function (value) {
	   	        	PersistenceService.setAppNumToBeRetrieved($scope.apps[0].applicationNumber);
	   	        	//$scope.go('/quote/3');
//	   	        	switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
//		   	     	case "quotepage":
//		   	     		$scope.go('/quote/3', false, $scope.apps[0].applicationNumber);
//		   	     		break;
//		   	     	case "aurapage":
//		   	     		$scope.go('/aura/3', false, $scope.apps[0].applicationNumber);
//		   	     		break;
//		   	     	case "summarypage":
//		   	     		$scope.go('/summary/3', false, $scope.apps[0].applicationNumber);
//		   	     		break;
//		   	     	default:
//		   	     		break;
//		   	     	}
	   	        	$scope.goToSavedApp($scope.apps[0]);
	   	        }, function (value) {
	   	        	if(value == 'oncancel'){
	   	        		CancelSavedAppService.cancelSavedApps($scope.urlList.cancelAppUrl,$scope.apps[0].applicationNumber).then(function(result){
	   	        		//CancelSavedAppService.cancelSavedApps({applicationNumber:$scope.apps[0].applicationNumber}, {}, function(result){
		   	        		if($scope.newMemberOffer){
		   	        			$scope.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
		   	        		} else{
			   	        		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
			   	        			case "quotepage":
			   	        			case "aurapage":
			   	        			case "summarypage":
			   	        				$scope.go('/quote/1', true);
			   	        				break;
			   	        			default:
			   	        				break;
			   	        		}
		   	        		}
		   	        	}, function(err){
		   	        		console.log("Error while cancelling the saved app " + err);
		   	        	});
	   	        	}
	   	        });
   			} else{
   				if($scope.newMemberOffer){
   					$scope.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
   				} else{
	   				switch(manageType.toUpperCase()){
	       			case "CCOVER":
	       				$scope.go('/quote/1', true);
	       				break;
	       			default:
	       				break;
	   				}
   				}
   			}
   		}, function(err){
   			console.log("Error while fetching saved apps " + err);
   		});
   	};
   	
   	$scope.setpersistenceservice = function(appnumber){
    	var defer = $q.defer();
    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,appnumber).then(function(res){
        	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
        		var result = res.data[0];
        		var coverDet = {},
        			coverStateObj = {},
        			occDet = {},
        			deathAddDet = {},
        			tpdAddDet = {},
        			ipAddDet = {};
        		
        		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
        		if(result.personalDetails.dateOfBirth)
    			{
    			coverDet.dob  = moment(result.personalDetails.dateOfBirth).format('DD/MM/YYYY');
    			}
        		else
    			{
    			coverDet.dob  = result.dob;
    			}
        		coverDet.email = result.email;
        		coverDet.contactPhone = result.contactPhone;
        		coverDet.contactType = result.contactType;        		
        		coverDet.contactPrefTime = result.contactPrefTime;
        		occDet.gender = result.personalDetails.gender;
        		coverDet.existingDeathAmt = parseFloat(result.existingDeathAmt);
        		coverDet.existingDeathUnits= parseFloat(result.existingDeathUnits);   
        		coverDet.existingTPDUnits= parseFloat(result.existingTPDUnits);
        		coverDet.deathOccCategory = result.deathOccCategory;
        		coverDet.existingTpdAmt =  parseFloat(result.existingTpdAmt);
        		coverDet.tpdOccCategory = result.tpdOccCategory;
        		coverDet.existingIPAmt =  parseFloat(result.existingIPAmount);
        		coverDet.existingIPAmount = parseFloat(result.existingIPAmount);
        		coverDet.existingIPUnits = result.existingIPUnits;
        		coverDet.totalPremium =  parseFloat(result.totalPremium);
        		coverDet.ipOccCategory = result.ipOccCategory;
        		coverDet.appNum = result.appNum;
        		//need to change
        		coverDet.manageType = result.manageType;
        		coverDet.partnerCode ="CARE";
        		//need to change
        		coverDet.freqCostType = result.freqCostType;
        		coverDet.age = result.age;
        		if(result.auraDisabled && result.auraDisabled =="true"){
        			$scope.auraDisabled = true;
        		}else{
        			$scope.auraDisabled = false;
        		}
        		
        		coverDet.ipcheckbox = result.ipcheckbox == "true";
        		//coverDet.auraDisabled= result.auraDisabled;
        		if(result.auraDisabled && result.auraDisabled =="true"){
        			coverDet.auraDisabled = true;
        		}else{
        			coverDet.auraDisabled = false;
        		}
        		//coverDet.ipcheckbox = result.ipSalaryPercent == 85;
        		coverDet.ipSalaryPercent=result.ipSalaryPercent;
        		
        		$scope.changeCoverDetails = coverDet;
        		
        		occDet.fifteenHr = result.occupationDetails.fifteenHr;
        		occDet.citizenQue = result.occupationDetails.citizenQue;
        		occDet.industryName = result.occupationDetails.industryName;
        		occDet.occupation = result.occupationDetails.occupation;
        		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
        		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
        		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
        		occDet.industryCode = result.occupationDetails.industryCode;
        		occDet.salary = result.occupationDetails.salary;
        		$scope.changeCoverOccDetails = occDet;
        		
        		deathAddDet.deathFixedAmt =  parseFloat(result.addnlDeathCoverDetails.deathFixedAmt);
        		deathAddDet.deathCoverPremium =  parseFloat(result.addnlDeathCoverDetails.deathCoverPremium);
        		deathAddDet.deathLoadingCost =  parseFloat(result.addnlDeathCoverDetails.deathLoadingCost);
        		deathAddDet.deathCoverName = result.addnlDeathCoverDetails.deathCoverName;
        		
        		deathAddDet.deathCoverType = result.addnlDeathCoverDetails.deathCoverType;
        		deathAddDet.deathInputTextValue = result.addnlDeathCoverDetails.deathInputTextValue;
        		
        		$scope.deathAddnlDetails = deathAddDet;
        		
        		
        		//tpdAddDet.tpdCoverName = result.addnlTpdCoverDetails.tpdCoverName;
        		tpdAddDet.tpdCoverType = result.addnlTpdCoverDetails.tpdCoverType;        		
        		tpdAddDet.tpdInputTextValue = result.addnlTpdCoverDetails.tpdInputTextValue;      		
        		
        		tpdAddDet.tpdFixedAmt =  parseFloat(result.addnlTpdCoverDetails.tpdFixedAmt);
        		tpdAddDet.tpdCoverPremium =  parseFloat(result.addnlTpdCoverDetails.tpdCoverPremium);
        		tpdAddDet.tpdLoadingCost =  parseFloat(result.addnlTpdCoverDetails.tpdLoadingCost);
        		tpdAddDet.tpdCoverName = result.addnlTpdCoverDetails.tpdCoverName;
        		$scope.tpdAddnlDetails = tpdAddDet;
        		
        		ipAddDet.ipFixedAmt =  parseFloat(result.addnlIpCoverDetails.ipFixedAmt);
        		ipAddDet.waitingPeriod = result.addnlIpCoverDetails.waitingPeriod;
        		ipAddDet.benefitPeriod = result.addnlIpCoverDetails.benefitPeriod;
        		
        		$scope.benefitPeriodAddnl = result.addnlIpCoverDetails.benefitPeriod;
        		$scope.waitingPeriodAddnl = result.addnlIpCoverDetails.waitingPeriod;
        		
        		ipAddDet.ipCoverType = result.addnlIpCoverDetails.ipCoverType;
        		ipAddDet.ipCoverPremium =  parseFloat(result.addnlIpCoverDetails.ipCoverPremium);
        		ipAddDet.ipLoadingCost =  parseFloat(result.addnlIpCoverDetails.ipLoadingCost);
        		ipAddDet.ipCoverName = result.addnlIpCoverDetails.ipCoverName;
        		
        		if(ipAddDet.ipCoverName == 'No change')
	    		{
        			ipAddDet.ipInputTextValue= result.addnlIpCoverDetails.ipFixedAmt;
        			if((parseInt(result.addnlIpCoverDetails.ipInputTextValue) != parseInt(coverDet.existingIPAmt)))
        			{
        				ipAddDet.ipAddnlUnits = result.addnlIpCoverDetails.ipInputTextValue;
        			}
	    		}
        		else
        			{
        			ipAddDet.ipInputTextValue= result.addnlIpCoverDetails.ipInputTextValue;
        			ipAddDet.ipAddnlUnits = result.addnlIpCoverDetails.ipInputTextValue;
        			}
        		
        		//ipAddDet.ipInputTextValue= result.addnlIpCoverDetails.ipInputTextValue;
        		$scope.existingbenefitPeriod = result.existingIPBenefitPeriod;
        		$scope.existingWaitingPeriod = result.existingIPWaitingPeriod;
        		$scope.ipIncreaseFlag = false;
        		        		
        		$scope.isBenifitPeriodIncresed = function() {
        			if(($scope.existingbenefitPeriod == "") && ($scope.benefitPeriodAddnl == '2 Years' || $scope.benefitPeriodAddnl == '5 Years')) {
        				return true;
        			} else if($scope.existingbenefitPeriod == '2 Years' && $scope.benefitPeriodAddnl == '5 Years') {
        				return true;
        			} else {
        				return false;
        			}
        		}
        		
        		$scope.isWaitingPeriodIncresed = function() {
        			if(($scope.existingWaitingPeriod == "") && ($scope.waitingPeriodAddnl == '90 Days' || $scope.waitingPeriodAddnl == '60 Days' || $scope.waitingPeriodAddnl == '30 Days')) {
        				return true;
        			} else if($scope.existingWaitingPeriod == '90 Days' && ($scope.waitingPeriodAddnl == '60 Days' || $scope.waitingPeriodAddnl == '30 Days')) {
        				return true;
        			} else if($scope.existingWaitingPeriod == '60 Days' && $scope.waitingPeriodAddnl == '30 Days') {
        				return true;
        			} else {
        				return false;
        			}
        		}
        		
        		$scope.checkBenefitPeriod = function(){
        			if(parseInt(ipAddDet.ipFixedAmt) > parseInt(coverDet.existingIPAmt))
        				{
        					return true;
        				}else if($scope.existingWaitingPeriod == $scope.waitingPeriodAddnl && $scope.existingbenefitPeriod == $scope.benefitPeriodAddnl){
        					return false;        				
        				} else if($scope.isBenifitPeriodIncresed() || $scope.isWaitingPeriodIncresed()){
        					return true;       				
        				} else{
        					return false;        				
        				}        			
        		};        		
        		
        		
        		if(deathAddDet.deathCoverName == 'Cancel your cover' ){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isDCCoverRequiredDisabled= true;
        			/*$scope.isTPDCoverRequiredDisabled = true;*/
        		}else if(deathAddDet.deathCoverName == 'Decrease your cover'){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isDCCoverRequiredDisabled= false;
        			/*$scope.isTPDCoverRequiredDisabled = false;*/
        		}else if(deathAddDet.deathCoverName =='Increase your cover'){
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isDCCoverRequiredDisabled= false;
        			/*$scope.isTPDCoverRequiredDisabled = false;*/
        		}else{
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isDCCoverRequiredDisabled= true;
        			/*$scope.isTPDCoverRequiredDisabled = true;*/
        		}
        		
        		if(tpdAddDet.tpdCoverName == 'Cancel your cover' ){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isTPDCoverRequiredDisabled = true;
        		}else if(tpdAddDet.tpdCoverName == 'Decrease your cover'){
        			$scope.deathDecOrCancelFlag = true;
        			$scope.isTPDCoverRequiredDisabled = false;
        		}else if(tpdAddDet.tpdCoverName =='Increase your cover'){
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isTPDCoverRequiredDisabled = false;
        		}else{
        			$scope.deathDecOrCancelFlag = false;
        			$scope.isTPDCoverRequiredDisabled = true;
        		}
        		
        		
        		if(tpdAddDet.tpdCoverName == 'Cancel your cover' || tpdAddDet.tpdCoverName == 'Decrease your cover'){
        			$scope.tpdDecOrCancelFlag = true;
        			$scope.isTPDCoverNameDisabled = false;
        		}else if($scope.tpdCoverName == "Convert and maintain cover"){
        			$scope.isTPDCoverNameDisabled = true;
        		}else{
        			$scope.tpdDecOrCancelFlag = false;
        			$scope.isTPDCoverNameDisabled = false;
        		}
        		
        		
        		if(ipAddDet.ipCoverName == 'Increase your cover'){
    				$scope.ipIncreaseFlag = true;    
    				$scope.ipDecOrCancelFlag=false;
    				var anb = parseInt(moment().diff(moment($scope.changeCoverDetails.dob, 'DD-MM-YYYY'), 'years')) + 1;
    				if((parseInt($scope.changeCoverOccDetails.salary) < 16000 || $scope.changeCoverOccDetails.fifteenHr == 'No') || anb > 64){
    					$scope.isIPCoverNameDisabled=true;
    					$scope.isIpSalaryCheckboxDisabled = true;
    					$scope.isWaitingPeriodDisabled = true;
    					$scope.isBenefitPeriodDisabled = true;
    				}else{
    					$scope.isIPCoverNameDisabled=false;
    					$scope.isIpSalaryCheckboxDisabled = false;
    					$scope.isWaitingPeriodDisabled = false;
        				$scope.isBenefitPeriodDisabled = false;
    				}
    				if(result.ipSalaryPercent!=85){
    					$scope.isIPCoverRequiredDisabled= false;
    				}
    				
        		}else if(ipAddDet.ipCoverName == 'Decrease your cover' ){
        			if((parseInt($scope.changeCoverOccDetails.salary) < 16000 || $scope.changeCoverOccDetails.fifteenHr == 'No') || anb > 64){
        				$scope.isWaitingPeriodDisabled = true;
        				$scope.isBenefitPeriodDisabled = true;
        				$scope.isIpSalaryCheckboxDisabled = true;
        				$scope.isIPCoverNameDisabled=true;
        			}else{
        				$scope.isWaitingPeriodDisabled = false;
        				$scope.isBenefitPeriodDisabled = false;
        				$scope.isIPCoverNameDisabled=false;
        				$scope.isIpSalaryCheckboxDisabled = false;
        			}
        			if(result.ipSalaryPercent!=85){
        				$scope.isIPCoverRequiredDisabled= false;
        			}        			
        			$scope.ipDecOrCancelFlag=true;
        			$scope.isIPCoverNameDisabled=false;
        		}else if(ipAddDet.ipCoverName == 'Cancel your cover'){
        			$scope.ipDecOrCancelFlag=true;
        			$scope.isIPCoverNameDisabled=false;
        		}else{
        			$scope.isIpSalaryCheckboxDisabled = true;
        			$scope.ipIncreaseFlag = false; 
        			$scope.isIPCoverRequiredDisabled = true;
        			$scope.ipDecOrCancelFlag=false;
        			$scope.isIPCoverNameDisabled=false;
        		}
        		
        		
        		$scope.ipAddnlDetails = ipAddDet;
        		
        		$scope.details={};
    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
        		
    			var temp = angular.extend($scope.details,$scope.changeCoverOccDetails);
    			var submitObject = null;
    		
    			submitObject = angular.extend(temp, $scope.personalDetails);			
    			 
    			//state object
    			$scope.dcIncreaseFlag = $scope.details.addnlDeathCoverDetails.deathCoverName == "Increase your cover";
				$scope.tpdIncreaseFlag = $scope.details.addnlTpdCoverDetails.tpdCoverName == "Increase your cover";
				$scope.ipIncreaseFlag = $scope.details.addnlIpCoverDetails.ipCoverName == "Increase your cover"?true:$scope.checkBenefitPeriod();
				coverDet['indexationDeath'] = result.indexationDeath  == 'true';
				coverDet['indexationTpd'] = result.indexationTpd == 'true';
				coverStateObj['isIPCoverNameDisabled'] = $scope.isIPCoverNameDisabled;
				 coverStateObj['isIpSalaryCheckboxDisabled'] = $scope.isIpSalaryCheckboxDisabled;
				 coverStateObj['isIPCoverRequiredDisabled'] = $scope.isIPCoverRequiredDisabled;
				 coverStateObj['isWaitingPeriodDisabled'] = $scope.isWaitingPeriodDisabled;
	             coverStateObj['isBenefitPeriodDisabled'] = $scope.isBenefitPeriodDisabled;
	             coverStateObj['ipDecOrCancelFlag'] = $scope.ipDecOrCancelFlag;
	             coverStateObj['tpdDecOrCancelFlag'] = $scope.tpdDecOrCancelFlag;
	             coverStateObj['deathDecOrCancelFlag'] = $scope.deathDecOrCancelFlag;
	             coverStateObj['isDCCoverRequiredDisabled'] = $scope.isDCCoverRequiredDisabled;
	             coverStateObj['isTPDCoverRequiredDisabled'] = $scope.isTPDCoverRequiredDisabled;
	             coverStateObj['dynamicFlag'] = coverDet.totalPremium != 0;
	             
	             angular.forEach(persoanlDetailService.getMemberDetails(), function (applicant) {           		        	
	            		$scope.coverList = applicant.existingCovers;
	            		angular.forEach($scope.coverList, function (cover) {            		
	                		angular.forEach(cover, function(value, key) {
	                			$scope.benefitType = value.benefitType;            			
	                			 if($scope.benefitType =='1'){
	                				 $scope.deathCover = value;
	                				 $scope.isDCCoverTypeDisabled = $scope.deathCover.type == 2 ? true : false;
	                			 } else if($scope.benefitType =='2'){
	                				 $scope.tpdCover = value;     
	                				 $scope.isTPDCoverTypeDisabled = $scope.tpdCover.type == 2 ? true : false;
	                			 }else  if($scope.benefitType =='4'){
	                				 $scope.ipCover = value;   
	                			 }
	                			});
	                		
	                	});
	                });
	                
	                
	             	
                coverStateObj['isDCCoverTypeDisabled'] = $scope.isDCCoverTypeDisabled;                
                coverStateObj['isTPDCoverTypeDisabled'] = $scope.isTPDCoverTypeDisabled;
                coverStateObj['isTPDCoverNameDisabled'] = $scope.isTPDCoverNameDisabled;
                                                       
           
                
                coverStateObj['dcIncreaseFlag'] = $scope.dcIncreaseFlag;
                coverStateObj['tpdIncreaseFlag'] = $scope.tpdIncreaseFlag;
                coverStateObj['ipIncreaseFlag'] = $scope.ipIncreaseFlag ;				
				
                
                
    			PersistenceService.setChangeCoverDetails($scope.changeCoverDetails);
    	    	PersistenceService.setChangeCoverStateDetails(coverStateObj);
    	    	PersistenceService.setChangeCoverOccDetails($scope.changeCoverOccDetails);
    	    	PersistenceService.setDeathAddnlCoverDetails($scope.deathAddnlDetails);
    	    	PersistenceService.setTpdAddnlCoverDetails($scope.tpdAddnlDetails);
    	    	PersistenceService.setIpAddnlCoverDetails($scope.ipAddnlDetails);
    			
    	    	defer.resolve(res);
        	}, function(err){
        		console.log("Encountered an error while fetchimg the app details " + err);
        		defer.reject(err);
        	});
    	return defer.promise;
    }
	
	
    $scope.goToSavedApp = function(app){
    	PersistenceService.setAppNumToBeRetrieved(app.applicationNumber);
    	PersistenceService.setAppNumber(app.applicationNumber);
    	
    	$scope.setpersistenceservice(app.applicationNumber).then(function() {
    		if(app.auraCallRequired && app.lastSavedOnPage.toLowerCase()=='summarypage'){
    			auraInputService.setFund('CARE');		  		
			  	auraInputService.setAppnumber(parseInt(app.applicationNumber));
				auraInputService.setClientname('metaus');				
			  	auraInputService.setLastName($scope.personalDetails.lastName)
			  	auraInputService.setFirstName($scope.personalDetails.firstName)
			  	auraInputService.setDob($scope.personalDetails.dateOfBirth)
			  	$rootScope.$broadcast('disablepointer');
	  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {         			      				
	  				$scope.auraResponses = response.data    
	  				if($scope.auraResponses.overallDecision!='DCL'){
	  					clientMatch.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
	  						if(clientMatchResponse.data.matchFound){
	  							$scope.clientmatchreason = '';
	  							$scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
	  							$scope.auraResponses.overallDecision='RUW'
	  								$scope.information = clientMatchResponse.data.information
	  								angular.forEach($scope.information, function (infoObj,index) {      									
	  									if(index==$scope.information.length-1){
	  										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
	  										$scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))      										
	  									}else{
	  										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "	
	  									}     																	
	  									
	  								})
	  							$scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
	  							$scope.auraResponses.specialTerm = false;
	  						} 						
	      					PersistenceService.setChangeCoverAuraDetails($scope.auraResponses);          					
	      					switch(app.lastSavedOnPage.toLowerCase()){
	      	  		    	case "quotepage":
	      	  		    		$scope.go('/quote/3');
	      	  		    		break;
	      	  		    	case "aurapage":
	      	  		    		$scope.go('/aura/3');
	      	  		    		break;
	      	  		    	case "summarypage":
	      	  		    		$scope.go('/summary/3');
	      	  		    		break;
	      	  		    	default:
	      	  		    		break;
	      	  		    	}
	      				});
	  				}  				
	  			
	     		 });
    		}else{
    			switch(app.lastSavedOnPage.toLowerCase()){
  		    	case "quotepage":
  		    		$scope.go('/quote/3');
  		    		break;
  		    	case "aurapage":
  		    		$scope.go('/aura/3');
  		    		break;
  		    	case "summarypage":
  		    		$scope.go('/summary/3');
  		    		break;
  		    	default:
  		    		break;
  		    }
    		}
  		}, function() {
  			console.log('error');
  		})
    	
    	
    };
 }]); 
 /*Login Page Controller Ends*/