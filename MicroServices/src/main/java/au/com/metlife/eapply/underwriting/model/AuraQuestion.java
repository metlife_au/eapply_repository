package au.com.metlife.eapply.underwriting.model;

import java.io.Serializable;
import java.util.List;


public class AuraQuestion implements Serializable{


private String classType;	
private String	questionAlias;
private int questionId;
private String questionText;
private String helpText;
private int indent;
public int getIndent() {
	return indent;
}
public void setIndent(int indent) {
	this.indent = indent;
}
private boolean isQuestionComplete;
private boolean isBaseQuestion;
private boolean isBranchComplete;
private String answerTest;
private String validationMsg;
public String getValidationMsg() {
	return validationMsg;
}
public void setValidationMsg(String validationMsg) {
	this.validationMsg = validationMsg;
}
private List<AuraAnswer> auraAnswer;
private boolean isVisible;
public boolean isVisible() {
	return isVisible;
}
public void setVisible(boolean isVisible) {
	this.isVisible = isVisible;
}
private List<String> arrAns;
/*answered values by the applications
to handle progressive validation*/
private boolean isError;
public boolean isError() {
	return isError;
}
public void setError(boolean isError) {
	this.isError = isError;
}
/*temperory*/
private AuraQuestion changedQuestion;



public AuraQuestion getChangedQuestion() {
	return changedQuestion;
}
public void setChangedQuestion(AuraQuestion changedQuestion) {
	this.changedQuestion = changedQuestion;
}
/*private AuraQuestionnaire auraQuestionnare;*/

/*public AuraQuestionnaire getAuraQuestionnare() {
	return auraQuestionnare;
}
public void setAuraQuestionnare(AuraQuestionnaire auraQuestionnare) {
	this.auraQuestionnare = auraQuestionnare;
}*/
public List<String> getArrAns() {
	return arrAns;
}
public void setArrAns(List<String> arrAns) {
	this.arrAns = arrAns;
}
public String getAnswerTest() {
	return answerTest;
}
public void setAnswerTest(String answerTest) {
	this.answerTest = answerTest;
}



public List<AuraAnswer> getAuraAnswer() {
	return auraAnswer;
}
public void setAuraAnswer(List<AuraAnswer> auraAnswer) {
	this.auraAnswer = auraAnswer;
}


public String getClassType() {
	return classType;
}
public void setClassType(String classType) {
	this.classType = classType;
}
public String getQuestionAlias() {
	return questionAlias;
}
public void setQuestionAlias(String questionAlias) {
	this.questionAlias = questionAlias;
}
public int getQuestionId() {
	return questionId;
}
public void setQuestionId(int questionId) {
	this.questionId = questionId;
}
public String getQuestionText() {
	return questionText;
}
public void setQuestionText(String questionText) {
	this.questionText = questionText;
}
public String getHelpText() {
	return helpText;
}
public void setHelpText(String helpText) {
	this.helpText = helpText;
}
public boolean isQuestionComplete() {
	return isQuestionComplete;
}
public void setQuestionComplete(boolean isQuestionComplete) {
	this.isQuestionComplete = isQuestionComplete;
}
public boolean isBaseQuestion() {
	return isBaseQuestion;
}
public void setBaseQuestion(boolean isBaseQuestion) {
	this.isBaseQuestion = isBaseQuestion;
}
public boolean isBranchComplete() {
	return isBranchComplete;
}
public void setBranchComplete(boolean isBranchComplete) {
	this.isBranchComplete = isBranchComplete;
}

}
