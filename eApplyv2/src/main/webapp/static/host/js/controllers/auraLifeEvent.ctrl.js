/*aura transfer controller*/
HostApp.controller('auraLifeEvent',['$scope','$rootScope','$timeout', '$location','$window','$routeParams','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','RetrieveAppDetailsService','ngDialog','urlService','saveEapply','submitEapply',
                                     function($scope,$rootScope,$timeout, $location,$window,$routeParams,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,RetrieveAppDetailsService,ngDialog,urlService,saveEapply,submitEapply){

	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
	
	$scope.eventList = [{
    	"cde": "MARR",
    	"desc": "Marriage",
    	"docDesc": "A marriage that is recognised as valid under the Marriage Act 1961(Cth)."
    },
    {
    	"cde": "BRTH",
    	"desc": "Birth or adoption of a child",
    	"docDesc": "Adopting or becoming the natural parent of a child."
    },
    {
    	"cde": "FRST",
    	"desc": "Obtaining a new mortgage or increasing an existing mortgage",
    	"docDesc": "Obtaining either a new mortgage or increasing an existing mortgage on your residence."
    },
    {
    	"cde": "DIVO",
    	"desc": "Divorce",
    	"docDesc": "Divorcing from a spouse."
    },
    {
    	"cde": "DSPO",
    	"desc": "Death of a spouse",
    	"docDesc": "Death of a spouse."
    },
    {
    	"cde": "CUGA",
    	"desc": "Completion of an undergraduate degree",
    	"docDesc": "Completing an undergraduate degree at an Australian University."
    },
    {
    	"cde": "DCSS",
    	"desc": "Dependent child starts secondary school",
    	"docDesc": "A dependent child starting secondary school."
    },
    {
    	"cde": "BCFM",
    	"desc": "Becoming a carer of an immediate family member",
    	"docDesc": "Becoming a carer of an immediate family member for the first time and being financially responsible for such care and/or are physically providing such care."
    },
    {
    	"cde": "NBLO",
    	"desc": "Obtaining a new business loan/increasing an existing business loan",
    	"docDesc": "Obtaining either a new business loan in excess of $100,000 or increasing an existing business loan by at least $100,000 (excluding re-draw and refinancing) on your business."
    }];
	
	
    $scope.go = function ( path ) {
    	
    	if($routeParams.mode == 3 && path=='/lifeevent/2')
    	{
    		path = "/lifeevent/3";
    	}
    	
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};

  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    }

  	$scope.coverDetails=PersistenceService.getlifeEventCoverDetails();
  	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.lifeEventOccDetails =PersistenceService.getLifeEventCoverOccDetails();
  	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();
    console.log($scope.coverDetails);
    
    var init = function()
  	{
    	auraInputService.setFund('HOST');
      	auraInputService.setMode('LifeEvent');
      	//setting values from quote page
      //setting deafult vaues for testing
      	auraInputService.setName($scope.coverDetails.name);
      	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
      //auraInputService.setAge(moment().diff($scope.coverDetails.dob, 'years'));
      	//below values has to be set from quote page, as of now taking from existing insurance
      	auraInputService.setAppnumber($scope.coverDetails.appNum);
      	auraInputService.setDeathAmt(parseInt($scope.coverDetails.deathAmt));
      	auraInputService.setTpdAmt(parseInt($scope.coverDetails.tpdAmt));
      	auraInputService.setIpAmt(parseInt($scope.coverDetails.ipAmt));
      	auraInputService.setWaitingPeriod($scope.coverDetails.waitingPeriod);
      	auraInputService.setBenefitPeriod($scope.coverDetails.benefitPeriod);
      	if($scope.lifeEventOccDetails && $scope.lifeEventOccDetails.gender ){
      		auraInputService.setGender($scope.lifeEventOccDetails.gender);
      	}else if($scope.personalDetails && $scope.personalDetails.gender){
      		auraInputService.setGender($scope.personalDetails.gender);
      	}
      //auraInputService.setGender($scope.coverDetails.gender);
      	auraInputService.setIndustryOcc($scope.lifeEventOccDetails.industryCode+":"+$scope.lifeEventOccDetails.occupation);
      	auraInputService.setCountry('Australia');
      	auraInputService.setSalary('150000');
      	auraInputService.setFifteenHr('Yes');
      	auraInputService.setClientname('metaus')
      	auraInputService.setLastName($scope.personalDetails.personalDetails.lastName)
      	auraInputService.setFirstName($scope.personalDetails.personalDetails.firstName)
      	auraInputService.setDob($scope.personalDetails.personalDetails.dateOfBirth)
      	auraInputService.setExistingTerm(false);
      	if($scope.personalDetails.memberType=="Personal"){
      		auraInputService.setMemberType("INDUSTRY OCCUPATION")
      	}else{
      		auraInputService.setMemberType("None")
      	}
      	/*auraInputService.setDeathAmt(200000)
      	auraInputService.setTpdAmt(200000)
      	auraInputService.setIpAmt(2000)
      	auraInputService.setWaitingPeriod('30 Days')
      	auraInputService.setBenefitPeriod('2 Years')
      	auraInputService.setGender($scope.coverDetails.gender)
      	auraInputService.setIndustryOcc($scope.coverDetails.industryCode+":"+$scope.coverDetails.occupation)
      	if($scope.transferOccDetails.citizenQue=='Yes'){
      		auraInputService.setCountry('Australia')
      	}else{
      		auraInputService.setCountry($scope.transferOccDetails.citizenQue)
      	}
      	auraInputService.setSalary($scope.transferOccDetails.salary)
      	auraInputService.setFifteenHr($scope.transferOccDetails.fifteenHr)
      	auraInputService.setName($scope.coverDetails.name)*/
      	///////////////

      	 //setting deafult vaues for testing
    	/*auraInputService.setName('HOST')
      	auraInputService.setAge(50)
      	//below values has to be set from quote page, as of now taking from existing insurance
      	auraInputService.setAppnumber(14782223482354338)
      	auraInputService.setDeathAmt(200000)
      	auraInputService.setTpdAmt(200000)
      	auraInputService.setIpAmt(2000)
      	auraInputService.setWaitingPeriod('30 Days')
      	auraInputService.setBenefitPeriod('2 Years')
      	auraInputService.setGender('Male')
      	auraInputService.setIndustryOcc('026:Transfer')
      	auraInputService.setCountry('Australia')
      	auraInputService.setSalary('100000')
      	auraInputService.setFifteenHr('Yes')*/
      	 //end deafult vaues for testing
    	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
      		$scope.auraResponseDataList = response.data.questions;
      		angular.forEach($scope.auraResponseDataList, function(Object) {
    			$scope.sectionname = Object.questionAlias.substring(3);

    			});
      	});

    	
  	}
    if($routeParams.mode == 3 && !$scope.coverDetails ){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    		var result = res.data[0];
    		var coverDet = {}, occDet ={}, deathAddDet = {},tpdAddDet={}, ipAddDet={}, personalDt = {};
    		
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactType = result.contactType;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		coverDet.country = result.address.country;
    		coverDet.eventName=result.eventName;
    		//coverDet.eventDesc=result.eventDesc;
    		coverDet.eventDate=result.eventDate;
    		coverDet.eventAlreadyApplied=result.eventAlreadyApplied;
    		coverDet.documentName=result.documentName;
    		coverDet.documentAddress=result.documentAddress;
    		coverDet.deathOccCategory=result.deathOccCategory;
    		coverDet.tpdOccCategory=result.tpdOccCategory;
    		coverDet.ipOccCategory=result.ipOccCategory;
    		coverDet.deathAmt=result.existingDeathAmt;
    		coverDet.tpdAmt=result.existingTpdAmt?parseInt(result.existingTpdAmt):0;
    		coverDet.ipAmt=result.ipAmt?parseInt(result.ipAmt):0;
    		coverDet.deathNewAmt=result.deathNewAmt;
    		coverDet.tpdNewAmt=result.tpdNewAmt;
    		coverDet.ipNewAmt=result.ipNewAmt;
    		coverDet.waitingPeriod=result.waitingPeriod;
    		coverDet.benefitPeriod=result.benefitPeriod;
    		coverDet.appNum=result.appNum;
    		coverDet.dodCheck=result.dodCheck;
    		coverDet.privacyCheck=result.privacyCheck;
    		coverDet.age=result.age;
    		coverDet.totalPremium=result.totalPremium;
    		coverDet.deathCoverPremium=result.deathCoverPremium;
    		coverDet.tpdCoverPremium=result.tpdCoverPremium;
    		coverDet.ipCoverPremium=result.ipCoverPremium;
    		coverDet.deathLifeCoverType=result.deathLifeCoverType;
    		coverDet.tpdLifeCoverType=result.tpdLifeCoverType;
    		coverDet.ipLifeCoverType=result.ipLifeCoverType;
    		coverDet.freqCostType=result.freqCostType;
    		coverDet.deathLifeUnits=result.deathLifeUnits;
    		coverDet.tpdLifeUnits=result.tpdLifeUnits;
    		coverDet.manageType = "ICOVER";
    		coverDet.partnerCode ="HOST";
    		coverDet.documentName = (result.lifeEventDocuments && result.lifeEventDocuments.length > 0) ? 'Yes' : 'No';
    		coverDet.documentName === 'Yes' ? PersistenceService.setUploadedFileDetails(result.lifeEventDocuments) : [];
    		
    		var tempEvt = $scope.eventList.filter(function(obj){
	    		return obj.cde == result.eventName;
	    	});
    		coverDet.eventDesc = tempEvt[0].desc;
    		
    		
    		occDet.gender = result.personalDetails.gender;
    		occDet.ownBussinessQues=result.occupationDetails.ownBussinessQues;
    		occDet.ownBussinessYesQues=result.occupationDetails.ownBussinessYesQues;
    		occDet.ownBussinessNoQues=result.occupationDetails.ownBussinessNoQues;
    		occDet.citizenQue=result.occupationDetails.citizenQue;
    		occDet.industryName=result.occupationDetails.industryName;
    		occDet.industryCode=result.occupationDetails.industryCode;
    		occDet.occupation=result.occupationDetails.occupation;
    		occDet.managementRoleQue=result.occupationDetails.managementRoleQue;
    		occDet.hazardousQue=result.occupationDetails.hazardousQue;
    		occDet.otherOccupation=result.occupationDetails.otherOccupation;
    		
    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.salary = result.occupationDetails.salary;
    		
    		$scope.svdRtrv = true;
    		coverDet['svRt'] = $scope.svdRtrv;
    		$scope.coverDetails=coverDet;
    		$scope.lifeEventOccDetails =occDet;
    		
    		PersistenceService.setlifeEventCoverDetails($scope.coverDetails);
	    	PersistenceService.setLifeEventCoverOccDetails($scope.lifeEventOccDetails);
    		
    	  	init();
    		
    	}, function(err){
    		console.error("Something went wrong while retrieving the details " + JSON.stringify(err));
    	});
	}
	else
		{
		init();
		}
    
 // 	console.log(auraInputService);
  	
  	 $scope.updateRadio = function (answerValue, questionObj){
  		console.log($scope.auraResponseDataList)

  		questionObj.arrAns[0]=answerValue;
  		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
  		  auraResponseService.setResponse($scope.auraRes)
  		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {
  			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
  			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
  	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
  	  				//branch complete false for previous questions
  	  				$scope.auraResponseDataList[selectedIndex].error=true;
  	  			}else if(Object.questionComplete){
  	  				$scope.auraResponseDataList[selectedIndex].error=false;
  	  			}

  	  		});

      	}, function () {
      		//console.log('failed');
      	});
  	 };

  	$scope.proceedNext = function(){
  		$scope.keepGoing= true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error = true;
  				$scope.keepGoing= false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error = false;
  				//$scope.go('/transferSummary');
  			}
  		});
  		if($scope.keepGoing){
  			$rootScope.$broadcast('disablepointer');
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
  				PersistenceService.setLifeEventCoverAuraDetails(response.data);
  				$scope.auraResponses = response.data;
  				if($scope.auraResponses.overallDecision!='DCL'){
					//console.log(response.data);
  					$scope.go('/lifeeventsummary/1');
	  		}else if($scope.auraResponses.overallDecision =='DCL'){
	  			if($scope.coverDetails != null && $scope.lifeEventOccDetails != null && $scope.personalDetails != null){
	  				$scope.coverDetails.lastSavedOn = '';
	    			$scope.details={};
	    			$scope.details.occupationDetails = $scope.lifeEventOccDetails;
	    			$scope.details.lifeEventDocuments = $scope.uploadedFileDetails;
	      			var coverObject = angular.extend($scope.details,$scope.coverDetails);
	              	var auraObject = angular.extend(coverObject,$scope.auraResponses);
	      			var submitObject = angular.extend(auraObject,$scope.personalDetails);
	      			auraResponseService.setResponse(submitObject);
	      			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
	              		console.log(response.data);
	              		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
	              		PersistenceService.setNpsUrl(response.data.npsTokenURL);
                  		$scope.go('/lifeeventdecline');
	              	}, function(err){
		            		console.log('Error while submitting transfer cover ' + err);
		            	});
	              }
	  		}
  				//console.log(response.data);

  	  		});
  		}
  	};

  	$scope.lifeEventAuraSaveAndExitPopUp = function (hhText) {

		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

 	var appNum;
    appNum = PersistenceService.getAppNumber();
  	$scope.saveAuraLifeEvent = function() {
  		$scope.lifeEventAuraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
  		if($scope.coverDetails != null && $scope.lifeEventOccDetails != null && $scope.personalDetails != null){
    		$scope.coverDetails.lastSavedOn = 'AuraLifeEventPage';
    		//$scope.auraDetails
    		var details = {};
	    	details.occupationDetails = $scope.lifeEventOccDetails;
	    	details.lifeEventDocuments = $scope.uploadedFileDetails;
    		var temp = angular.extend($scope.coverDetails,details);
        	var saveAuraLifeEventObject = angular.extend(temp, $scope.personalDetails);
        	auraResponseService.setResponse(saveAuraLifeEventObject)
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                console.log(response.data)
	        });
    	}
  	};

  	$scope.collapseOne = true;
  	$scope.toggleOne = function() {
        $scope.collapseOne = !$scope.collapseOne;
    };
  	$scope.collapseTwo = false;
  	$scope.toggleTwo = function() {
        $scope.collapseTwo = !$scope.collapseTwo;
    };
    $scope.collapseThree = false;
  	$scope.toggleThree = function() {
        $scope.collapseThree = !$scope.collapseThree;
    };
    $scope.collapseFour = false;
  	$scope.toggleFour = function() {
        $scope.collapseFour = !$scope.collapseFour;
    };
    $scope.collapseFive = false;
  	$scope.toggleFive = function() {
        $scope.collapseFive = !$scope.collapseFive;
    };
}]);
////
