/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class MetFlowLoginPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public MetFlowLoginPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	

	public static final By frameMainFrame = getObjectIdentifaction("metflow.login.main.frame.name.id");
	public static final By txtLogin = getObjectIdentifaction("metflow.login.login.name.txt.css");
	public static final By txtPassword = getObjectIdentifaction("metflow.login.login.password.txt.css");
	public static final By btnSubmit = getObjectIdentifaction("metflow.login.submit.btn.css");
	public static final By frameDashboard = getObjectIdentifaction("metflow.memberarea.frame.name.id");
	public static final By linkCaseSearch = getObjectIdentifaction("metflow.memberarea.casesearch.link.css");
	
}
