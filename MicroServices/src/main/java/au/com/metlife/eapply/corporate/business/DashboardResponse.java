package au.com.metlife.eapply.corporate.business;

import java.util.Date;

public class DashboardResponse {
private String fundCode;
private String date;
private String status;
private String firstName;
private String lastName;
private String email;
private String applicationNumber;
public String getFundCode() {
	return fundCode;
}
public void setFundCode(String fundCode) {
	this.fundCode = fundCode;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getApplicationNumber() {
	return applicationNumber;
}
public void setApplicationNumber(String applicationNumber) {
	this.applicationNumber = applicationNumber;
}

public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
}
