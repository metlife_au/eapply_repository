/**
 * 
 */
package au.com.metlife.eapply.repositories.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * CREATE TABLE "EAPPDB"."TBL_MEMBERFILEUPLOAD" (
		"ID" BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 30000000000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 NO CYCLE NO CACHE NO ORDER ), 
		"FIRST_NAME" VARCHAR(50) NOT NULL, 
		"SURNAME" VARCHAR(50) NOT NULL, 
		"EMAILADDRESS" VARCHAR(50) NOT NULL, 
		"DATEOFBIRTH" VARCHAR(10) NOT NULL, 
		"GENDER" VARCHAR(10), 
		"ANNUAL_SALARY" DOUBLE, 
		"EMPLOYEE_NUMBER" VARCHAR(50), 
		"SMOKER_STATUS" VARCHAR(1), 
		"MEMBERSHIP_CATEGORY" VARCHAR(10) NOT NULL, 
		"BROKEREMAIL" VARCHAR(50), 
		"TOTAL_DEATH_COVER" DOUBLE, 
		"TOTAL_TPD_COVER" DOUBLE, 
		"TOTAL_IP_COVER" DOUBLE, 
		"IP_WAITING_PERIOD" VARCHAR(50), 
		"IP_BENEFIT_PERIOD" VARCHAR(50), 
		"ELIGIBLE_DEATH_COVER" DOUBLE, 
		"ELIGIBLE_TPD_COVER" DOUBLE, 
		"ELIGIBLE_IP_COVER" DOUBLE, 
		"ELIGIBLE_IP_COVER_WAITING_PERIOD" VARCHAR(50), 
		"ELIGIBLE_IP_BENEFITPERIOD" VARCHAR(50), 
		"FUND_ID" VARCHAR(50), 
		"APPLICATIONNUMBER" VARCHAR(50), 
		"STATUS" VARCHAR(20), 
		"CREATEDDATE" TIMESTAMP NOT NULL, 
		"URL" VARCHAR(2000),	 
		"SOURCE" VARCHAR(100), 
		"FILESTATUS" VARCHAR(100), 
		"DEATH_OCCUPATION_CATEGORY" VARCHAR(100), 
		"TPD_OCCUPATION_CATEGORY" VARCHAR(100), 
		"IP_OCCUPATION_CATEGORY" VARCHAR(100), 
		"OCCUPATION" VARCHAR(100), 
		"INDUSTRY" VARCHAR(100), 
		"CAMPAIGN_REF" VARCHAR(100)
	)
	ORGANIZE BY ROW
	DATA CAPTURE NONE 
	IN "EAPPIDX"
	COMPRESS NO;
 * @author 599063
 *
 */
@Entity
@Table(name= "TBL_MEMBERFILEUPLOAD", schema = "EAPPDB" )
public class MemberFileUpload {

	@Id
	@Column(name="ID")
	@SequenceGenerator(name="MEMBER_FILE_UPLOAD_1", sequenceName="MEMBER_FILE_UPLOAD_SEQ_NUM", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MEMBER_FILE_UPLOAD_1")
	private long id;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="SURNAME")
	private String surname;
	
	@Column(name="EMAILADDRESS")
	private String email;
	
	@Column(name="DATEOFBIRTH")
	private String dob;
	
	@Column(name="GENDER")
	private String gender;
	
	@Column(name="ANNUAL_SALARY")
	private Double annualSalary;
	
	@Column(name="EMPLOYEE_NUMBER")
	private String employeeNumber;
	
	@Column(name="SMOKER_STATUS")
	private Character smokerStatus;
	
	@Column(name="MEMBERSHIP_CATEGORY")
	private String membershipCategory;
	
	@Column(name="BROKEREMAIL")
	private String brokerEmail;
	
	@Column(name="TOTAL_DEATH_COVER")
	private Double totalDeathCover;
	
	@Column(name="TOTAL_TPD_COVER")
	private Double totalTpdCover;
	
	
	@Column(name="TOTAL_IP_COVER")
	private Double totalIpCover;
	
	@Column(name="IP_WAITING_PERIOD")
	private String ipWaitingPeriod;
	
	@Column(name="IP_BENEFIT_PERIOD")
	private String ipBenefitPeriod;
	
	@Column(name="ELIGIBLE_DEATH_COVER")
	private Double eligibleDeathCover;
	
	@Column(name="ELIGIBLE_TPD_COVER")
	private Double eligibleTpdCover;
	
	@Column(name="ELIGIBLE_IP_COVER")
	private Double eligibleIpCover;
	
	@Column(name="ELIGIBLE_IP_COVER_WAITING_PERIOD")
	private String eligibleIpCoverWaitingPeriod;
	
	@Column(name="ELIGIBLE_IP_BENEFITPERIOD")
	private String eligibleIpBenefitPeriod;
	
	@Column(name="FUND_ID")
	private String fundId;
	
	@Column(name="APPLICATIONNUMBER")
	private String applicationNumber;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="CREATEDDATE")
	private Timestamp createdDate;
	
	@Column(name="URL")
	private String url;
	
	@Column(name="SOURCE")
	private String source;
	
	@Column(name="FILESTATUS")
	private String fileStatus;
	
	@Column(name="DEATH_OCCUPATION_CATEGORY")
	private String deathOccupationCategory;
	
	@Column(name="TPD_OCCUPATION_CATEGORY")
	private String tpdOccupationCategory;
	
	@Column(name="IP_OCCUPATION_CATEGORY")
	private String ipOccupationCategory;
	
	@Column(name="OCCUPATION")
	private String occupation;
	
	@Column(name="INDUSTRY")
	private String industry;
	
	@Column(name="CAMPAIGN_REF")
	private String campaignRef;
	
	

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}






	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public Double getAnnualSalary() {
		return annualSalary;
	}



	public void setAnnualSalary(Double annualSalary) {
		this.annualSalary = annualSalary;
	}



	public String getEmployeeNumber() {
		return employeeNumber;
	}



	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}



	public Character getSmokerStatus() {
		return smokerStatus;
	}



	public void setSmokerStatus(Character smokerStatus) {
		this.smokerStatus = smokerStatus;
	}



	public String getMembershipCategory() {
		return membershipCategory;
	}



	public void setMembershipCategory(String membershipCategory) {
		this.membershipCategory = membershipCategory;
	}



	public String getBrokerEmail() {
		return brokerEmail;
	}



	public void setBrokerEmail(String brokerEmail) {
		this.brokerEmail = brokerEmail;
	}



	public Double getTotalDeathCover() {
		return totalDeathCover;
	}



	public void setTotalDeathCover(Double totalDeathCover) {
		this.totalDeathCover = totalDeathCover;
	}



	public Double getTotalTpdCover() {
		return totalTpdCover;
	}



	public void setTotalTpdCover(Double totalTpdCover) {
		this.totalTpdCover = totalTpdCover;
	}



	public Double getTotalIpCover() {
		return totalIpCover;
	}



	public void setTotalIpCover(Double totalIpCover) {
		this.totalIpCover = totalIpCover;
	}



	public String getIpWaitingPeriod() {
		return ipWaitingPeriod;
	}



	public void setIpWaitingPeriod(String ipWaitingPeriod) {
		this.ipWaitingPeriod = ipWaitingPeriod;
	}



	public String getIpBenefitPeriod() {
		return ipBenefitPeriod;
	}



	public void setIpBenefitPeriod(String ipBenefitPeriod) {
		this.ipBenefitPeriod = ipBenefitPeriod;
	}



	public Double getEligibleDeathCover() {
		return eligibleDeathCover;
	}



	public void setEligibleDeathCover(Double eligibleDeathCover) {
		this.eligibleDeathCover = eligibleDeathCover;
	}



	public Double getEligibleTpdCover() {
		return eligibleTpdCover;
	}



	public void setEligibleTpdCover(Double eligibleTpdCover) {
		this.eligibleTpdCover = eligibleTpdCover;
	}



	public Double getEligibleIpCover() {
		return eligibleIpCover;
	}



	public void setEligibleIpCover(Double eligibleIpCover) {
		this.eligibleIpCover = eligibleIpCover;
	}



	public String getEligibleIpCoverWaitingPeriod() {
		return eligibleIpCoverWaitingPeriod;
	}



	public void setEligibleIpCoverWaitingPeriod(String eligibleIpCoverWaitingPeriod) {
		this.eligibleIpCoverWaitingPeriod = eligibleIpCoverWaitingPeriod;
	}



	public String getEligibleIpBenefitPeriod() {
		return eligibleIpBenefitPeriod;
	}



	public void setEligibleIpBenefitPeriod(String eligibleIpBenefitPeriod) {
		this.eligibleIpBenefitPeriod = eligibleIpBenefitPeriod;
	}



	public String getFundId() {
		return fundId;
	}



	public void setFundId(String fundId) {
		this.fundId = fundId;
	}



	public String getApplicationNumber() {
		return applicationNumber;
	}



	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public Timestamp getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	public String getSource() {
		return source;
	}



	public void setSource(String source) {
		this.source = source;
	}



	public String getFileStatus() {
		return fileStatus;
	}



	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}



	public String getDeathOccupationCategory() {
		return deathOccupationCategory;
	}



	public void setDeathOccupationCategory(String deathOccupationCategory) {
		this.deathOccupationCategory = deathOccupationCategory;
	}



	public String getTpdOccupationCategory() {
		return tpdOccupationCategory;
	}



	public void setTpdOccupationCategory(String tpdOccupationCategory) {
		this.tpdOccupationCategory = tpdOccupationCategory;
	}



	public String getIpOccupationCategory() {
		return ipOccupationCategory;
	}



	public void setIpOccupationCategory(String ipOccupationCategory) {
		this.ipOccupationCategory = ipOccupationCategory;
	}



	public String getOccupation() {
		return occupation;
	}



	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}



	public String getIndustry() {
		return industry;
	}



	public void setIndustry(String industry) {
		this.industry = industry;
	}



	public String getCampaignRef() {
		return campaignRef;
	}



	public void setCampaignRef(String campaignRef) {
		this.campaignRef = campaignRef;
	}


	public String getDob() {
		return dob;
	}



	public void setDob(String dob) {
		this.dob = dob;
	}



	@Override
 public String toString() {
	return "id=" + id + ", firstName=" + firstName + ", surname=" + surname + ", email=" + email
			+ ", dob=" + dob + ", gender=" + gender + ", annualSalary=" + annualSalary + ", employeeNumber="
			+ employeeNumber + ", smokerStatus=" + smokerStatus + ", membershipCategory=" + membershipCategory
			+ ", brokerEmail=" + brokerEmail + ", totalDeathCover=" + totalDeathCover + ", totalTpdCover="
			+ totalTpdCover + ", totalIpCover=" + totalIpCover + ", ipWaitingPeriod=" + ipWaitingPeriod
			+ ", ipBenefitPeriod=" + ipBenefitPeriod + ", eligibleDeathCover=" + eligibleDeathCover
			+ ", eligibleTpdCover=" + eligibleTpdCover + ", eligibleIpCover=" + eligibleIpCover
			+ ", eligibleIpCoverWaitingPeriod=" + eligibleIpCoverWaitingPeriod + ", eligibleIpBenefitPeriod="
			+ eligibleIpBenefitPeriod + ", fundId=" + fundId + ", applicationNumber=" + applicationNumber
			+ ", status=" + status + ", createdDate=" + createdDate + ", url=" + url + ", source=" + source
			+ ", fileStatus=" + fileStatus + ", deathOccupationCategory=" + deathOccupationCategory
				+ ", tpdOccupationCategory=" + tpdOccupationCategory + ", ipOccupationCategory=" + ipOccupationCategory
			+ ", occupation=" + occupation + ", industry=" + industry + ", campaignRef=" + campaignRef;
	
	
	
		
	
}
	
	
	
	
	
	
}
