package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressRoleList")

@XmlRootElement
public class AddressRoleList {	
	
	private AddressRole AddressRole;

	public AddressRole getAddressRole() {
		return AddressRole;
	}

	public void setAddressRole(AddressRole addressRole) {
		AddressRole = addressRole;
	}

	
	
	
}
