function TransferConfig($stateProvider, $httpProvider) {
  'ngInject';
  $httpProvider.defaults.withCredentials = true;
  $stateProvider
    .state('app.transfercover', {
      url: '/transfercover',
      controller: 'TransferCoverCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'transfer/transfer.cover.html',
      title: 'Transfer Cover',
      resolve: {
        industryList: function(CommonAPI) {
          let urlList = CommonAPI.getUrlList();
          return !CommonAPI.getIndustryList() && CommonAPI.fetchIndustryList(urlList.quoteUrl).then(
            (res) => {
              CommonAPI.setIndustryList(res);
            },
            (err) => {
              console.log(err.data.errors);
            });
        }
      }
    })

    .state('app.transferaura', {
      url:'/transferaura',
      controller: 'TransferAuraCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'transfer/transfer.aura.html',
      title: 'Transfer Aura'
    })

    .state('app.transfersummary', {
      url:'/transfersummary',
      controller: 'TransferSummaryCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'transfer/transfer.summary.html',
      title: 'Transfer Summary'
    })

    .state('app.transferdecision', {
      url:'/transferdecision',
      controller: 'TransferDecisionCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'transfer/transfer.decision.html',
      title: 'Transfer Decision'
    });
}

export default TransferConfig;
