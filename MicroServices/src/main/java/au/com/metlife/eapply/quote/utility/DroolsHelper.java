package au.com.metlife.eapply.quote.utility;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.calculator.model.InsurCalculatorRuleBO;
import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;

public class DroolsHelper {
	private DroolsHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(DroolsHelper.class);
	
	public static CoverDetailsRuleBO invokeUC16BusinessRules(CoverDetailsRuleBO coverDetailsRuleBO,String ruleFileLocation){
		log.info("----------getting called invokeUC16BusinessRules  start------- ");
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		String ruleFileLoc = ruleFileLocation;
		try {
			ruleFileLoc=ruleFileLoc+coverDetailsRuleBO.getRuleFileName();
			if(coverDetailsRuleBO.getKbase()!=null){
				kbase=coverDetailsRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLoc);
				ksession = kbase.newStatelessKnowledgeSession();
			}

				ksession.execute(coverDetailsRuleBO);
		} catch (Exception e) {
			log.error("Error in invokeUC16BusinessRules: {} " , e);
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		log.info("----------getting called invokeUC16BusinessRules  finish------- ");
		return coverDetailsRuleBO;

	}
	
	public static InstProductRuleBO invokeInstProductBusinessRule(InstProductRuleBO instProductRuleBO, String ruleFileLocation){
		log.info("Invoke InstProduct business rule start");
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		String ruleFileLoc = ruleFileLocation;
		try {
			ruleFileLoc=ruleFileLoc+instProductRuleBO.getRuleFileName();
			if(instProductRuleBO.getKbase()!=null){
				kbase=instProductRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLoc);
				ksession = kbase.newStatelessKnowledgeSession();
			}
				ksession.execute(instProductRuleBO);
		} catch (Exception e) {
			log.error("Error in Invoke InstProduct business rule: {} ",e);
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		log.info("Invoke InstProduct business rule finish");
		return instProductRuleBO;

	}
	
	public static KnowledgeBase getRuleSession(String ruleFileLoc,String ruleFileName){
		log.info("Get rule session start");
		KnowledgeBase kbase=null;
		String ruleFileLocation = ruleFileLoc;
		try {
			ruleFileLocation=ruleFileLocation+ruleFileName;
			kbase = readKnowledgeBase(ruleFileLocation);
		} catch (Exception e) {
			log.error("Error in get rule session : {}",e);
		}
		log.info("Get rule session finish");
		return kbase;
	}

	/**
	 * This method will read the rules from the sample.xls file.
	 * Each row in the excel sheet will represent as a business rule.
	 *
	 * @return KnowledgeBase object
	 * @throws Exception
	 */

	private static KnowledgeBase readKnowledgeBase(String ruleFileLoc) {
		log.info("Read knowledge base start");
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		config.setInputType(DecisionTableInputType.XLS);
		kbuilder.add(ResourceFactory.newFileResource(ruleFileLoc), ResourceType.DTABLE, config);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		/*if (errors.size() > 0) {*/
		if (!(errors.isEmpty())) {
			for (KnowledgeBuilderError error: errors) {
				log.error("Error in knowledge base {}",error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		log.info("Read knowledge base finish");
		return kbase;
	}
	public static InsurCalculatorRuleBO invokeInsurCalRue(InsurCalculatorRuleBO insurCalculatorRuleBO,String ruleFileLocation){
		log.info("----------getting called invokeUC16BusinessRules  start------- ");
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		String ruleFileLoc = ruleFileLocation;
		try {
			ruleFileLoc=ruleFileLoc+insurCalculatorRuleBO.getRuleFileName();
			if(insurCalculatorRuleBO.getKbase()!=null){
				kbase=insurCalculatorRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLoc);
				ksession = kbase.newStatelessKnowledgeSession();
			}

				ksession.execute(insurCalculatorRuleBO);
		} catch (Exception e) {
			log.error("Error in invokeUC16BusinessRules: {} " , e);
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		log.info("----------getting called invokeUC16BusinessRules  finish------- ");
		return insurCalculatorRuleBO;

	}

}
