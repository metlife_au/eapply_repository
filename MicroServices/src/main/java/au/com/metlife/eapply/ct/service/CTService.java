/**
 * 
 */
package au.com.metlife.eapply.ct.service;

import java.util.List;

import au.com.metlife.eapply.ct.dto.DocumentDto;
import au.com.metlife.eapply.ct.model.DocumentBO;

/**
 * @author kp
 *
 */
public interface CTService {
	
	DocumentBO generateToken(String input, String outToken);
	DocumentBO retriveClientData(String token);
	void saveDocumentList(List<DocumentDto> documentDto);
	List<DocumentBO> retriveDocList(String ClaimNo);	
}
