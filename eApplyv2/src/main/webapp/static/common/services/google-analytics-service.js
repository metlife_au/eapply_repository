angular.module('googleAnalytics', [])
  .factory('gaTracker', [
    '$window', '$location', 
    function ($window, $location) {

      var googleAnalyticsTracker = {};

      /**
       * Set the page to the current location path
       * and then send a pageview to log path change.
       */
      googleAnalyticsTracker.sendPageview = function(USER_ID) {
        if (window.ga) {
          //$window.ga('set', 'page', $location.path());
          //$window.ga('send', 'pageview');
          window.ga('set', 'userId', USER_ID); // Set the user ID using signed-in user_id.
          window.ga('send', 'pageview', {  'page': $location.path(),  'title': $location.path()} );
        }
      }
      return googleAnalyticsTracker;
    }
  ])