package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.YBRIPSaveandExitPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.OAMPSDeclinePageObjects;
import uimap.metlife.OAMPSSaveandExitPageObjects;
import uimap.metlife.OAMPSSummaryPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTSummaryPageObjects;

public class YBRIPSaveandExitPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRIPSaveandExitPage SaveandExitDetails() {
		SaveDetails();
		return new YBRIPSaveandExitPage(scriptHelper);
	}

	public YBRIPSaveandExitPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void SaveDetails() {
			
		try {
			
			tryAction(fluentWaitElement(YBRIPSaveandExitPageObjects.btnSave),"Click", "Finish");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPSaveandExitPageObjects.btnConfirm),"Click", "Finish");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(YBRIPSaveandExitPageObjects.btnFinish),"Click", "Finish");
			Thread.sleep(1000);
			
			switchToActiveWindow();
			driver.manage().window().maximize();
				
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
