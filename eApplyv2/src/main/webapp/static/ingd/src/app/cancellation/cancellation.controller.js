class CancellationCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, $timeout, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._$timeout = $timeout;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._ngDialog = ngDialog;
    this.applicantData = {};
    this.cancelSubmitData = {};
    this.exDthCover = {};
    this.exTpdCover = {};
    this.exIpCover = {};
	this.contactOpts = AppConstants.transfer.contactOpts;
	this.emailPattern = AppConstants.regEx.emailFormat;
	this.mobilePattern = AppConstants.regEx.phoneNumber;
    this.PrefTimeOpts = [
		'Morning (9am \u002d 12pm)',
		'Afternoon (12pm \u002d 6pm)'];
	
    $timeout(function(){    	
    	//watches    	
    	$scope.$watch('memberDetails.$valid', function(newVal, oldVal) {
    		if(newVal){
    			$('#cancel-details').collapse('show');
    		}    		  		
    	});    	
    	
    	$scope.$watch('memberDetails.$invalid', function(newVal, oldVal) {    		
    		if(newVal){
    			$('#cancel-details').collapse('hide');
    		}    		
		});
    	
    }); 
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
   
    this.applicantData = this._EapplyData.getApplicantData() || this._EapplyData.exitOnError();
    this.applicantData.applicant.cover = [];
    this.applicantData.applicant.cover[0] = this._EapplyData.getExistingDeathCover();
    this.applicantData.applicant.cover[1] = this._EapplyData.getExistingTpdCover();
    this.applicantData.applicant.cover[2] = this._EapplyData.getExistingIpCover();
    
    this.cancelSubmitData = this._EapplyData.getCancelData() ? angular.extend({}, this.cancelSubmitData, this._EapplyData.getCancelData()) : angular.copy(this.applicantData);
    this._EapplyData.populateData(this.cancelSubmitData);
    
    
    this.cancelSubmitData.applicant.cover[0].existingCoverAmount = this.cancelSubmitData.applicant.cover[0].existingCoverAmount ? parseInt(this.cancelSubmitData.applicant.cover[0].existingCoverAmount) : '0';
    this.cancelSubmitData.applicant.cover[1].existingCoverAmount = this.cancelSubmitData.applicant.cover[1].existingCoverAmount ? parseInt(this.cancelSubmitData.applicant.cover[1].existingCoverAmount) : '0';
    this.cancelSubmitData.applicant.cover[2].existingCoverAmount = this.cancelSubmitData.applicant.cover[2].existingCoverAmount ? parseInt(this.cancelSubmitData.applicant.cover[2].existingCoverAmount) : '0';
    
    this.cancelSubmitData.requestType = 'CANCOVER';  
    this.cancelSubmitData.appDecision = 'ACC';
    
	}
	
  changePrefContactType() {
	    if (this.cancelSubmitData.applicant.contactDetails.preferedContacType == 'Home') {
	      this.cancelSubmitData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.homePhone;
	    } else if (this.cancelSubmitData.applicant.contactDetails.preferedContacType == 'Work') {
	      this.cancelSubmitData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.workPhone;
	    } else if (this.cancelSubmitData.applicant.contactDetails.preferedContacType == 'Mobile') {
	      this.cancelSubmitData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.mobilePhone;
	    } else {
	      this.cancelSubmitData.applicant.contactDetails.preferedContactNumber = '';
	    }	  
  }
  
  
  isCollapsable(element, event){
	  
		var memberDetailsValid = this._$scope.memberDetails.$valid;
	  	var stopPropagation = false;
	  	
	  	if(element === 'cancel-details' && !memberDetailsValid){
	    	this._$scope.memberDetails.$submitted=true;
	    	stopPropagation = true;
	    }
	    
	    if(stopPropagation){
	    	event.stopPropagation();
	    	return false;
	    }
  }
	
	checkPreviousMandatoryFields(formName, elementName){

	  	
		var memberDetailsFormFields = ['contactEmail','contactType','contactPhone','contactPrefTime','gender'];
		var currentFormFields = [];
	  	
	  	if(formName === 'memberDetails'){
	  		currentFormFields = memberDetailsFormFields;
	  	}
	  	
	  	var inx = currentFormFields.indexOf(elementName);	        
	      
	      if(inx > 0){
	      	for(var i = 0; i < inx ; i++){
	      		 if (this._$scope[formName][currentFormFields[i]]){
	      			this._$scope[formName][currentFormFields[i]].$touched = true;
	      		 }                        
	      	}
	      }	    
  
	}
	 
	
	// Helpful hints show/hide using this method
  openHH(hhText) {
    this._ngDialog.open({
      template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
      className: 'ngdialog-theme-plain',
      showClose: false,
      plain: true
    });
	}
	
	continue() {
    this._EapplyData.setCancelData(this.cancelSubmitData);
    this._$state.go('app.cancelAcknowledge');
  }
}

export default CancellationCtrl;