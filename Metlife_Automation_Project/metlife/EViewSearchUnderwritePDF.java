/**
 * 
 */
package pages.metlife;

import java.awt.Robot;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.EViewSearchUnderwritePDFPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;


/**
 * @author sampath
 * 
 */
public class EViewSearchUnderwritePDF extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public EViewSearchUnderwritePDF(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public EViewSearchUnderwritePDF SearchforUnderwritePDF() {
		SearchUnderwritePDF();
		return new EViewSearchUnderwritePDF(scriptHelper);
	} 
	
	private void SearchUnderwritePDF() {
		

		try{
			String StrfirstName=dataTable.getData("MetLife_Data", "Fname");
			String StrsurName=dataTable.getData("MetLife_Data", "Sname");
			String dtDOB=dataTable.getData("MetLife_Data", "Dob");
			 Robot robot = new Robot();
			  robot.mouseMove(0,0);
			sleep(5000);
			WebElement element = driver.findElement(EViewSearchUnderwritePDFPageObjects.linkequery);
			WebElement subelement = driver.findElement(EViewSearchUnderwritePDFPageObjects.linkunderpdf);
			Actions action = new Actions(driver);
			action.moveToElement(element).moveToElement(subelement).click().perform();
			sleep(3000);
			tryAction(waitForClickableElement(EViewSearchUnderwritePDFPageObjects.txtFname), "SET", "First Name", StrfirstName);
			tryAction(waitForClickableElement(EViewSearchUnderwritePDFPageObjects.txtSname), "SET", "First Name", StrsurName);
			tryAction(waitForClickableElement(EViewSearchUnderwritePDFPageObjects.txtDob), "SET", "First Name", dtDOB);
			tryAction(waitForClickableElement(EViewSearchUnderwritePDFPageObjects.btnSearch), "Click", "Search Button");
			sleep(2000);
			tryAction(waitForClickableElement(EViewSearchUnderwritePDFPageObjects.iconPdf), "Click", "PDF Icon");
			sleep(10000);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
