// Auth service will be placed here
angular.module('MtaaApp').service('inputDataService',function(){
	var _self= this;
	_self.setCoverDetails= function (temp) {
        _self.CoverDetails= temp;
	};
	_self.getCoverDetails= function () {
		return _self.CoverDetails;
	};
});

angular.module('MtaaApp').factory('fetchCategoryDetailsService',['$http','$q',function($http,$q){
	var object={};
	object.getCategoryDetails= function(url,fundCode,category){
		var defer= $q.defer();
		$http.get(url,{
			headers:{
				'Content-Type': 'application/json'
			},
			params:{
				'fundCode': fundCode,
				'category': category
			}
		}).then(function(res){
			object= res.data;
			defer.resolve(res);
		},function(err){
			console.log('Something went wrong file fecthing category details...'+JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return object;
}]); 

angular.module('MtaaApp').service('calculateFUL', ['$http', '$q','fetchTokenNumSvc', function($http, $q,fetchTokenNumSvc) {
	var factory = {};
	factory.calculate = function(url, data){
		var defer = $q.defer();
		$http.post(url,data,{
			 headers : {
                 'Content-Type': 'application/json',
                 'Authorization':fetchTokenNumSvc.getTokenId()
             }
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while calculation FUL..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return factory;
}]);


angular.module('MtaaApp').factory('printPageService',['$http','$q','fetchTokenNumSvc',function($http,$q,fetchTokenNumSvc){
	var object={};
	object.getPrintObject= function(url,data,token){
		var defer= $q.defer();
		$http.post(url,data,{
			headers: {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
		}).then(function(res){
			object= res.data;
			defer.resolve(res);
		},function(err){
			console.log('Something went wrong file fecthing category details...'+JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return object;
}]); 
angular.module('MtaaApp').factory('DownloadPDFService',['$http','$q', function($http,$q){
    var object = {};
    object.download = function(url,fileName){
        var defer = $q.defer();
        $http.get(url,{ 		
			headers: {
				'Content-Type': 'application/json'
			}, params:{
	            'file_name': fileName
			},responseType: 'arraybuffer',
			transformResponse: function (data) {
	            var pdf;
	            if (data) {
	                pdf = new Blob([data], {
	                    type: 'application/pdf'
	                });
	            }
	            return {
	                response: pdf
	            };
        }}).then(function(res){
        	object = res.data;
            defer.resolve(res);
        }, function(err){
            console.log('Error while getting pdf data' + JSON.stringify(err));
            defer.reject(err);
        });
        return defer.promise;
    };
    return object;
}]);
angular.module('MtaaApp').factory('CheckSavedAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.checkSavedApps = function(url,fundCode,clientRefNo,manageType){
        var defer = $q.defer();
        $http.get(url, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
                'fundCode': fundCode,
                'clientRefNo': clientRefNo,
                'manageType':manageType
        	}}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        	}, function(err){
                console.log('Error while getting saved apps' + JSON.stringify(err));
                defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);

angular.module('MtaaApp').factory('CancelSavedAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.cancelSavedApps = function(url,applicationNumber){
        var defer = $q.defer();
        $http.post(url, {}, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        }, params:{
                'applicationNumber': applicationNumber
        }}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        }, function(err){
                console.log('Error while cancelling app' + JSON.stringify(err));
                defer.reject(err);
        });
        return defer.promise;
    };
    return factory;
}]);

angular.module('MtaaApp').service('PersistenceService', function($sessionstorage, fetchAppNumberSvc){
	var thisObject = this;
	thisObject.uploadedFileList = [];
	
	thisObject.setAppNumber = function(appnum){
	    $sessionstorage.setItem('UniqueAppNum',JSON.stringify(appnum));
	};
	
	thisObject.getAppNumber = function(){
	    return fetchAppNumberSvc.getAppNumber();
	};
	
	thisObject.setAppNumToBeRetrieved = function(num){
		$sessionstorage.setItem('AppNumToBeRetrieved',JSON.stringify(num));
	};
	
	thisObject.getAppNumToBeRetrieved = function(){
	    return JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved')) === null ? null : JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved'));
	};
    thisObject.setPDFLocation = function(location){
        $sessionstorage.setItem('PDFLocation',JSON.stringify(location));
};
    thisObject.getPDFLocation = function(){
        try {
            if($sessionstorage.getItem('PDFLocation') === 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse($sessionstorage.getItem('PDFLocation'));
            }
        } catch(Error) {
            throw Error;
        }
    };
    thisObject.setNpsUrl = function(url){
        $sessionstorage.setItem('NpsLink',JSON.stringify(url));
};

thisObject.getNpsUrl = function(){
try {
if($sessionstorage.getItem('NpsLink') === 'undefined') {
throw {message: 'Not found'};
} else {
return JSON.parse($sessionstorage.getItem('NpsLink'));
}
} catch(Error) {
throw Error;
}
};

});

angular.module('MtaaApp').factory('$fakeStorage', [
 function(){
     function FakeStorage() {}
     FakeStorage.prototype.setItem = function (key, value) {
         this[key] = value;
     };
     FakeStorage.prototype.getItem = function (key) {
         return typeof this[key] === 'undefined' ? null : this[key];
         };
         FakeStorage.prototype.removeItem = function (key) {
             this[key] = undefined;
         };
         FakeStorage.prototype.clear = function(){
             for (var key in this) {
                 if( this.hasOwnProperty(key) )
                 {
                     this.removeItem(key);
                 }
             }
         };
         FakeStorage.prototype.key = function(index){
             return Object.keys(this)[index];
         };
         return new FakeStorage();
     }
 ]);

angular.module('MtaaApp').factory('$sessionstorage', ['$window', '$fakeStorage', function($window, $fakeStorage){
     function isStorageSupported(storageName)
     {
         var testKey = 'test',
             storage = $window[storageName];
         try
         {
             storage.setItem(testKey, '1');
             storage.removeItem(testKey);
             return true;
         }
         catch (error)
         {
             return false;
         }
     }
     var storage = isStorageSupported('sessionStorage') ? $window.sessionStorage : $fakeStorage;
         return {
             set: function(key, value) {
                 storage.setItem(key, value);
             },
             get: function(key, defaultValue) {
                 return storage.getItem(key) || defaultValue;
             },
             setItem: function(key, value) {
                 storage.setItem(key, value);
             },
             getItem: function(key) {
                 return storage.getItem(key);
             },
             remove: function(key){
                 storage.removeItem(key);
             },
             clear: function() {
                 storage.clear();
             },
             key: function(index){
                 storage.key(index);
             }
         };
     }
 ]);
angular.module('MtaaApp').factory('RetrieveAppDetailsService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.retrieveAppDetails = function(url,applicationNumber){
        var defer = $q.defer();
        $http.get(url, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
                'applicationNumber': applicationNumber
        	}}).then(function(res){
                        factory = res.data;
                        defer.resolve(res);
        	},function(err){
                        console.log('Error while retrieving application ' + JSON.stringify(err));
                        defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);
/* Change for Duplicate Application UW message */
angular.module('MtaaApp').factory('CheckUWDuplicateAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.checkDuplicateApps= function(url,fundCode,clientRefNo,manageTypeCC,dob,firstName,lastName){
        var defer = $q.defer();
        $http.post(url, {}, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
            	'fundCode': fundCode,
                'clientRefNo': clientRefNo,
                'manageTypeCC':manageTypeCC,
                'dob': dob,
                'firstName':firstName,
                'lastName': lastName
        	}}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        	},function(err){
                console.log('Error in UW duplicate application ' + JSON.stringify(err));
                defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);
