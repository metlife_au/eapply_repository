package au.com.metlife.eapply.underwriting.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.underwriting.model.AuraDecision;
import au.com.metlife.eapply.underwriting.model.AuraQuestion;
import au.com.metlife.eapply.underwriting.model.AuraQuestionnaire;
import au.com.metlife.eapply.underwriting.model.AuraResponse;
import au.com.metlife.eapply.underwriting.model.Quote;
import au.com.metlife.eapply.underwriting.service.UnderwritingService;


@RestController
public class UnderwritingController {
	private static final Logger log = LoggerFactory.getLogger(UnderwritingController.class);


	@Autowired 
	UnderwritingService  uwService;
	
	
    @RequestMapping("/uw")
    public Quote greeting(@RequestParam(value="name", defaultValue="World") String name) {
    	Quote q=new Quote("Hello");
    	/*QuoteService qs=new QuoteServiceImpl();*/
        return  uwService.getQuote(q);
    }
    
    
    
    @RequestMapping(value = "/initiate", method = RequestMethod.POST)
    public ResponseEntity <AuraQuestionnaire> listXML(InputStream inputStream,HttpServletRequest request) throws IOException, JSONException {	
    	log.info("Listing XML start");
    	/*String inputXml = null;*/
    	/* Calendar cal = Calendar.getInstance();*/
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
     	 String deathQfilter = null;
    	 String tpdQfilter = null;
    	 String ipQfilter = null;    	 
    	 String deathProduct = null;
    	 String tpdProduct = null;
    	 String ipProduct = null;    	 
    	 Integer addBenPeriod = 0;
    	 Integer addWaitPeriod = 0;  	 
    	 StringBuilder questionFilter = new StringBuilder();
    	 StringBuilder products = new StringBuilder();
    	String line = null;    	
    	 String string = "";	
    	 String benefitPeriod = null;
    	 Integer deathAmt = null;
    	 Integer tpdAmt = null;
    	 String waitingPeriod = null;
    	 Integer ipAmt = null;
    	 String mode = null;
    	 String fund = null;
    	 String age = null;
    	 String name = null;
    	 String xml = null;
    	 Long appnumber = null;
    	 String occupation =null;
    	 String country =null;
    	 String gender = null;   
    	 String salary = null;
    	 String fiftennHr = null;
    	 String specialTerm = "No";
    	 String haveOtherCover = "No";
    	 String occScheme = null;
    	 String smoker = null;
    	 
    	 //Added for ING
    	 String auraType = null;
    	while ((line = in.readLine()) != null) { 
    		string += line + "\n";
    		log.info("line>> {}",line);	
    		JSONObject json = new JSONObject(string);        
    		name = (String)json.get("name");
        	 /*age =  (Integer)json.get("age")+"";*/
        	 if(json.get("age")!=null && !(json.get("age").toString()).equalsIgnoreCase("null")){
        		 age =  (Integer)json.get("age")+"";
        	 }
        	 if(json.get(MetlifeInstitutionalConstants.DEATH_AMOUNT)!=null && !(json.get(MetlifeInstitutionalConstants.DEATH_AMOUNT).toString()).equalsIgnoreCase("null")){
        		 deathAmt =  (Integer)json.get(MetlifeInstitutionalConstants.DEATH_AMOUNT);
        	 }
        	 if(json.get(MetlifeInstitutionalConstants.TPD_AMOUNT)!=null && !(json.get(MetlifeInstitutionalConstants.TPD_AMOUNT).toString()).equalsIgnoreCase("null")){
        		 tpdAmt =  (Integer)json.get(MetlifeInstitutionalConstants.TPD_AMOUNT);
        	 }
        	 if(json.get(MetlifeInstitutionalConstants.IP_AMOUNT)!=null && !(json.get(MetlifeInstitutionalConstants.IP_AMOUNT).toString()).equalsIgnoreCase("null")){
        		 ipAmt =  (Integer)json.get(MetlifeInstitutionalConstants.IP_AMOUNT);
        	 }
        	 waitingPeriod = (String)json.get("waitingPeriod");
        	 benefitPeriod =  (String)json.get("benefitPeriod");
        	 mode =  (String)json.get("mode");
        	 fund =  (String)json.get("fund");
        		 if(json.get(MetlifeInstitutionalConstants.APPNUMBER)!=null && !(json.get(MetlifeInstitutionalConstants.APPNUMBER).toString()).equalsIgnoreCase("null")){
        			 appnumber = (Long)json.get(MetlifeInstitutionalConstants.APPNUMBER);
        		 } 
        		 /*appnumber = (Long)json.get(MetlifeInstitutionalConstants.APPNUMBER);*/
        	 if(json.get("gender") != null){
        		 gender = (String)json.get("gender"); 
        	 }
        	 if(json.has("smoker")){
        		 smoker= (String)json.get("smoker");
        	 }else{
        		 smoker= "No";
        	 }
        	 if("INGD".equalsIgnoreCase(fund)){
        	 if(json.get("auraType") != null){
        		auraType = (String)json.getString("auraType"); 
        	 }
        	 }
        	 occupation =  (String)json.get("industryOcc");
        	 if(null!=occupation && occupation.contains("&")){
                 occupation = occupation.replace("&", "&amp;");
                 }
        	 if(json.has(MetlifeInstitutionalConstants.COUNTRY) && !json.isNull(MetlifeInstitutionalConstants.COUNTRY) && "Australia".equalsIgnoreCase((String)json.get(MetlifeInstitutionalConstants.COUNTRY))){
        		 country = "Yes";
        		 country = MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE ;
        	 }else if(json.has(MetlifeInstitutionalConstants.COUNTRY) && !json.isNull(MetlifeInstitutionalConstants.COUNTRY) && !"Australia".equalsIgnoreCase((String)json.get(MetlifeInstitutionalConstants.COUNTRY))){
        		 country = "No";
        		 country = MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE ;
        	 }  else{
        		 country = "<Variable Name=\"Country\"/>" ;
        	 }
        	 salary = (String)json.get("salary");
        	 if(json.get("fifteenHr")!=null){
        		 fiftennHr =  (String)json.get("fifteenHr");
        	 }
        	 if(json.get("existingTerm")!=null
        			 && ((Boolean) json.get("existingTerm"))){ 
        			 specialTerm = "Yes";
        	 }
        	 if(json.get("memberType")!=null){        		 
        		 occScheme = (String)json.get("memberType");
        	 }
        	 if("INGD".equalsIgnoreCase(fund)){
        		 occScheme =  (String)json.get("industryOcc");
            	 if(null!=occScheme && occScheme.contains("&")){
            		 occScheme = occScheme.replace("&", "&amp;");
                     }
            	 }
        	
    	} 
    	
    	if( (null!=deathAmt && deathAmt > 1500000 )
				|| (null!=tpdAmt && tpdAmt > 1500000 )
				|| (null!=ipAmt && ipAmt > 8000 )){
    		haveOtherCover = "Yes";
		}
    	
  
    	 String auraID= appnumber+"_"+fund;/*"test11-"+new java.util.Date().getTime();  */ 
  
    	 
    	 
    	 if(deathAmt!=null  && (deathAmt>0 || (MetlifeInstitutionalConstants.WORK_RATING.equalsIgnoreCase(mode) || MetlifeInstitutionalConstants.LIFE_EVENT.equalsIgnoreCase(mode)))){    		 
    		 deathQfilter = "<QuestionFilter>Term-Group</QuestionFilter>";
    		 questionFilter.append(deathQfilter);
    		 deathProduct = MetlifeInstitutionalConstants.PRODUCT_AGE_COVERAGE_AMOUNT+deathAmt+MetlifeInstitutionalConstants.AGEDISTR_COVERAGE_AMOUNT+deathAmt+MetlifeInstitutionalConstants.COVERAGE_AMOUNT+deathAmt+MetlifeInstitutionalConstants.FINANCIAL_COVERAGE_AMOUNT+deathAmt+"\">Term-Group</Product>";
    		 products.append(deathProduct);
    	 }else if(MetlifeInstitutionalConstants.WORK_RATING.equalsIgnoreCase(mode) || MetlifeInstitutionalConstants.LIFE_EVENT.equalsIgnoreCase(mode)){
    		 
    	 }
    	 if(tpdAmt!=null && (tpdAmt>0 || (MetlifeInstitutionalConstants.WORK_RATING.equalsIgnoreCase(mode) || MetlifeInstitutionalConstants.LIFE_EVENT.equalsIgnoreCase(mode)))){
    		 tpdQfilter = "<QuestionFilter>TPD-Group</QuestionFilter>";
    		 questionFilter.append(tpdQfilter);
    		 tpdProduct = MetlifeInstitutionalConstants.PRODUCT_AGE_COVERAGE_AMOUNT+tpdAmt+MetlifeInstitutionalConstants.AGEDISTR_COVERAGE_AMOUNT+tpdAmt+MetlifeInstitutionalConstants.COVERAGE_AMOUNT+tpdAmt+MetlifeInstitutionalConstants.FINANCIAL_COVERAGE_AMOUNT+tpdAmt+"\">TPD-Group</Product>";
    		 products.append(tpdProduct);
    	 }
    	 if(ipAmt!=null && ipAmt>0 && benefitPeriod!=null && benefitPeriod.trim().length()>0 && waitingPeriod!=null){
    		 
    		 if(benefitPeriod!=null && benefitPeriod != "" && benefitPeriod.contains("Years")){
 				addBenPeriod = Integer.parseInt(benefitPeriod.substring(0,1).trim());
    		 }else if(benefitPeriod!=null && benefitPeriod != "" && benefitPeriod.contains("Age")){
 				addBenPeriod = Integer.parseInt(benefitPeriod.substring(4, 6).trim());
    		 }     	 
     	 
    		if(waitingPeriod!=null && waitingPeriod != "" &&  waitingPeriod.contains("820")){
 				addWaitPeriod = Integer.parseInt(waitingPeriod.substring(0, 3).trim());
 			}else if(waitingPeriod!=null && waitingPeriod != ""  &&  waitingPeriod.contains("Days")){
 				addWaitPeriod = Integer.parseInt(waitingPeriod.substring(0, 2).trim());
 			}
    		
    		if(addWaitPeriod!=null && addBenPeriod!=null ){
    			String ipProductName = "IP W"+addWaitPeriod+ " B"+addBenPeriod+"-Group";
       		 ipQfilter = "<QuestionFilter>"+ipProductName+"</QuestionFilter>";
       		 questionFilter.append(ipQfilter);
       		 ipProduct = MetlifeInstitutionalConstants.PRODUCT_AGE_COVERAGE_AMOUNT+ipAmt+MetlifeInstitutionalConstants.AGEDISTR_COVERAGE_AMOUNT+ipAmt+MetlifeInstitutionalConstants.COVERAGE_AMOUNT+ipAmt+MetlifeInstitutionalConstants.FINANCIAL_COVERAGE_AMOUNT+ipAmt+"\">"+ipProductName+"</Product>";
       		 products.append(ipProduct);
    		}
    		 
    	 }else if(MetlifeInstitutionalConstants.WORK_RATING.equalsIgnoreCase(mode) || MetlifeInstitutionalConstants.LIFE_EVENT.equalsIgnoreCase(mode)){
    		 ipQfilter = "<QuestionFilter>IP W30 B2-Group</QuestionFilter>";
       		 questionFilter.append(ipQfilter);
       		 ipProduct = MetlifeInstitutionalConstants.PRODUCT_AGE_COVERAGE_AMOUNT+ipAmt+MetlifeInstitutionalConstants.AGEDISTR_COVERAGE_AMOUNT+ipAmt+MetlifeInstitutionalConstants.COVERAGE_AMOUNT+ipAmt+MetlifeInstitutionalConstants.FINANCIAL_COVERAGE_AMOUNT+ipAmt+"\">IP W30 B2-Group</Product>";
       		 products.append(ipProduct);
    	 }
    	 
    	if(mode!=null && mode.length()>0){
    		switch (mode) {
    		case "TransferCover":
    			 xml = MetlifeInstitutionalConstants.AURACONTROL
    		 	    		+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
    		 	    		+ MetlifeInstitutionalConstants.COMPANY
    		 	    		+ MetlifeInstitutionalConstants.QUESTION_FILTERS
    		 	    		+questionFilter.toString() 	    	
    		 	    		+MetlifeInstitutionalConstants.QUESTIONFILTERS
    		 	    		+ MetlifeInstitutionalConstants.PRESENTATION_OPTIONS
    		 	    		+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
    		 	    		+ MetlifeInstitutionalConstants.PRODUCT_S
    		 	    		+products.toString() 	    		
    		 	    		+ MetlifeInstitutionalConstants.PRODUCTS
    		 	    		+ MetlifeInstitutionalConstants.ENGINE_VARIABLES+age+"</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE
    		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE_NAME_SALARY+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+MetlifeInstitutionalConstants.VARIABLE
    		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_PARTNER+fund+MetlifeInstitutionalConstants.VARIABLE_NAME
    		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_SCHEMENAME+fund+"</Variable><Variable Name=\"FormLength\">TransferCover</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			break;
    		case MetlifeInstitutionalConstants.WORK_RATING:
    			xml = MetlifeInstitutionalConstants.AURACONTROL
		 	    		+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
		 	    		+ MetlifeInstitutionalConstants.COMPANY
		 	    		+ MetlifeInstitutionalConstants.QUESTION_FILTERS
		 	    		+questionFilter.toString() 	    	
		 	    		+MetlifeInstitutionalConstants.QUESTIONFILTERS
		 	    		+ MetlifeInstitutionalConstants.PRESENTATION_OPTIONS
		 	    		+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
		 	    		+ MetlifeInstitutionalConstants.PRODUCT_S
		 	    		+products.toString() 	    		
		 	    		+ MetlifeInstitutionalConstants.PRODUCTS
		 	    		+ MetlifeInstitutionalConstants.ENGINE_VARIABLES+age+MetlifeInstitutionalConstants.VARIABLE_NAME_OCCUPATION+occupation+"</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE_NAME_SALARY+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_PARTNER+fund+MetlifeInstitutionalConstants.VARIABLE_NAME
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_SCHEMENAME+fund+"</Variable><Variable Name=\"FormLength\">WorkRating</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			break;
    		case MetlifeInstitutionalConstants.LIFE_EVENT:
    			xml = MetlifeInstitutionalConstants.AURACONTROL
		 	    		+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
		 	    		+ MetlifeInstitutionalConstants.COMPANY
		 	    		+ MetlifeInstitutionalConstants.QUESTION_FILTERS
		 	    		+questionFilter.toString() 	    	
		 	    		+MetlifeInstitutionalConstants.QUESTIONFILTERS
		 	    		+ MetlifeInstitutionalConstants.PRESENTATION_OPTIONS
		 	    		+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
		 	    		+ MetlifeInstitutionalConstants.PRODUCT_S
		 	    		+products.toString() 	    		
		 	    		+ MetlifeInstitutionalConstants.PRODUCTS
		 	    		+ MetlifeInstitutionalConstants.ENGINE_VARIABLES+age+MetlifeInstitutionalConstants.VARIABLE_NAME_OCCUPATION+occupation+"</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE_NAME_SALARY+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_PARTNER+fund+MetlifeInstitutionalConstants.VARIABLE_NAME
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_SCHEMENAME+fund+"</Variable><Variable Name=\"FormLength\">LifeEvent</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			break;
    		case "SpecialOffer":
    			xml = MetlifeInstitutionalConstants.AURACONTROL
		 	    		+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
		 	    		+ MetlifeInstitutionalConstants.COMPANY
		 	    		+ MetlifeInstitutionalConstants.QUESTION_FILTERS
		 	    		+questionFilter.toString() 	    	
		 	    		+MetlifeInstitutionalConstants.QUESTIONFILTERS
		 	    		+ MetlifeInstitutionalConstants.PRESENTATION_OPTIONS
		 	    		+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
		 	    		+ MetlifeInstitutionalConstants.PRODUCT_S
		 	    		+products.toString() 	    		
		 	    		+ MetlifeInstitutionalConstants.PRODUCTS
		 	    		+ MetlifeInstitutionalConstants.ENGINE_VARIABLES+age+"</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE_NAME_SALARY+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_PARTNER+fund+MetlifeInstitutionalConstants.VARIABLE_NAME
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_SCHEMENAME+fund+"</Variable><Variable Name=\"FormLength\">SpecialOffer</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			break;
    		case MetlifeInstitutionalConstants.INDEXATION://Added for Vicsuper project - purna 
    			xml = MetlifeInstitutionalConstants.AURACONTROL
		 	    		+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
		 	    		+ MetlifeInstitutionalConstants.COMPANY
		 	    		+ MetlifeInstitutionalConstants.QUESTION_FILTERS
		 	    		+questionFilter.toString() 	    	
		 	    		+MetlifeInstitutionalConstants.QUESTIONFILTERS
		 	    		+ MetlifeInstitutionalConstants.PRESENTATION_OPTIONS
		 	    		+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
		 	    		+ MetlifeInstitutionalConstants.PRODUCT_S
		 	    		+products.toString() 	    		
		 	    		+ MetlifeInstitutionalConstants.PRODUCTS
		 	    		+ MetlifeInstitutionalConstants.ENGINE_VARIABLES+age+"</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_COUNTRY+country+MetlifeInstitutionalConstants.VARIABLE_NAME_SALARY+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+MetlifeInstitutionalConstants.VARIABLE
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_PARTNER+fund+MetlifeInstitutionalConstants.VARIABLE_NAME
		 	    		+ MetlifeInstitutionalConstants.VARIABLENAME_SCHEMENAME+fund+"</Variable><Variable Name=\"FormLength\">Indexation</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			break;
    		default:
    			if("AEIS".equalsIgnoreCase(fund) || "SFPS".equalsIgnoreCase(fund)){
    				xml= MetlifeInstitutionalConstants.AURACONTROL
        	    			+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
        	    			+ MetlifeInstitutionalConstants.COMPANY
        	    			+ MetlifeInstitutionalConstants.QUESTION_FILTERS
        	    			+questionFilter.toString()
        	    			+ "</QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
        	    					+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
        	    							+ MetlifeInstitutionalConstants.PRODUCT_S
        	    							+ products.toString() 	
        	    							+ "</Products><EngineVariables><Variable Name=\"Gender\">"+gender+"</Variable><Variable Name=\"Age\">"+age+MetlifeInstitutionalConstants.VARIABLE_NAME_OCCUPATION+occupation+MetlifeInstitutionalConstants.VARIABLE
        	    									+ "<Variable Name=\"SpecialTerms\">"+specialTerm+MetlifeInstitutionalConstants.VARIABLE
        	    									+ "<Variable Name=\"Smoker\">"+smoker+MetlifeInstitutionalConstants.VARIABLE
        	    									+ "<Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE+country+"<Variable Name=\"Salary\">"+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+"</Variable><Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable><Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">Long</Variable>"
        	    											+ "<Variable Name=\"OccupationScheme\">"+occScheme+MetlifeInstitutionalConstants.VARIABLE
        	    											+ "<Variable Name=\"HaveOtherCover\">"+haveOtherCover+"</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			}else{
    				
    				if("short".equalsIgnoreCase(auraType)){
    					xml= MetlifeInstitutionalConstants.AURACONTROL
            	    			+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
            	    			+ MetlifeInstitutionalConstants.COMPANY
            	    			+ MetlifeInstitutionalConstants.QUESTION_FILTERS
            	    			+questionFilter.toString()
            	    			+ "</QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
            	    					+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
            	    							+ MetlifeInstitutionalConstants.PRODUCT_S
            	    							+ products.toString() 	
            	    							+ "</Products><EngineVariables><Variable Name=\"Gender\">"+gender+"</Variable><Variable Name=\"Age\">"+age+MetlifeInstitutionalConstants.VARIABLE_NAME_OCCUPATION+occupation+MetlifeInstitutionalConstants.VARIABLE
            	    									+ "<Variable Name=\"SpecialTerms\">"+specialTerm+MetlifeInstitutionalConstants.VARIABLE        	    								
            	    									+ "<Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE+country+"<Variable Name=\"Salary\">"+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+"</Variable><Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable><Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">short</Variable>"
            	    											+ "<Variable Name=\"OccupationScheme\">"+occScheme+MetlifeInstitutionalConstants.VARIABLE
            	    											+ "<Variable Name=\"HaveOtherCover\">"+haveOtherCover+"</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    				}else{
    					xml= MetlifeInstitutionalConstants.AURACONTROL
            	    			+ MetlifeInstitutionalConstants.UNIQUE_ID+auraID+MetlifeInstitutionalConstants.UNIQUEID
            	    			+ MetlifeInstitutionalConstants.COMPANY
            	    			+ MetlifeInstitutionalConstants.QUESTION_FILTERS
            	    			+questionFilter.toString()
            	    			+ "</QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
            	    					+ MetlifeInstitutionalConstants.INSURED_INTERVIEW_SUBMITTED+name+MetlifeInstitutionalConstants.UNIQUEID_WAIVED
            	    							+ MetlifeInstitutionalConstants.PRODUCT_S
            	    							+ products.toString() 	
            	    							+ "</Products><EngineVariables><Variable Name=\"Gender\">"+gender+"</Variable><Variable Name=\"Age\">"+age+MetlifeInstitutionalConstants.VARIABLE_NAME_OCCUPATION+occupation+MetlifeInstitutionalConstants.VARIABLE
            	    									+ "<Variable Name=\"SpecialTerms\">"+specialTerm+MetlifeInstitutionalConstants.VARIABLE        	    								
            	    									+ "<Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+MetlifeInstitutionalConstants.VARIABLE+country+"<Variable Name=\"Salary\">"+salary+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTERM+deathAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDTPD+tpdAmt+MetlifeInstitutionalConstants.VARIABLE_NAME_SUMINSUREDIP+ipAmt+"</Variable><Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable><Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">Long</Variable>"
            	    											+ "<Variable Name=\"OccupationScheme\">"+occScheme+MetlifeInstitutionalConstants.VARIABLE
            	    											+ "<Variable Name=\"HaveOtherCover\">"+haveOtherCover+"</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    				}
    				
    			}
    			
    			
    			break;
    		}
    	}    
    	AuraQuestionnaire aq =uwService.getAllAuraQuestions(xml);
        if(aq==null){
            return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);/*You many decide to return HttpStatus.NOT_FOUND*/
                    
        }
        
      
        log.info("Listing XML finish");
        return new ResponseEntity<>(aq, HttpStatus.OK);
    	   
    	
    }
    

@RequestMapping(value = "/underwriting", method = RequestMethod.POST)
public  ResponseEntity <AuraQuestion> updateUnderwriting(@RequestBody AuraResponse res,    UriComponentsBuilder ucBuilder, HttpServletRequest request) {
	log.info("Update underwriting start");

	AuraQuestion q=uwService.updateAuraQuestion(res);
	
    if(q==null){
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
    }
    log.info("q.getAnswerTest()>> {}",q.getAnswerTest());
    log.info("Update underwriting start" ,new  java.util.Date());
    return new ResponseEntity<>(q, HttpStatus.OK);
    

}
@RequestMapping(value = "/submitAura", method = RequestMethod.POST)
public ResponseEntity <AuraDecision> submitAuraSession(InputStream inputStream) throws IOException, JSONException {	
	log.info("Submit aura session start");
	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
	String line = null;
	 String string = "";	
	 JSONObject json = null;
	 Long auraSessionId = null;
	 String clientname = null;
	 AuraDecision responseObject = null;
	 String fund = null;
	/* String firstName = null;*/
	/* String surName = null;*/
	/* String dob = null;*/
	while ((line = in.readLine()) != null) { 
		string += line + "\n";
		json = new JSONObject(string);        
		clientname = (String)json.get("clientname");
		fund = (String)json.get("fund");
		auraSessionId = (Long)json.get(MetlifeInstitutionalConstants.APPNUMBER);
		/*if(json.get("firstName")!=null)
			firstName = (String)json.get("firstName");
		if(json.get("lastName")!=null)
			surName =(String)json.get("lastName");
		if(json.get("dob")!=null)
			dob =(String)json.get("dob");*/
	}
	if(clientname!=null && clientname.length()>0 && auraSessionId!=null){
		responseObject = uwService.submitAuraSession(clientname, auraSessionId+"_"+fund,fund);
		/*client match call to GL/SCI*/
		/*String ca=  oDSSearch(firstName, surName, dob);
		if(null != ca && (ca.contains("GL") || ca.contains("SCI"))){
			responseObject.setClientMatched(true);
			
			if(null != responseObject.getClientMatchReason()){
				ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
				responseObject.setClientMatchReason(responseObject.getClientMatchReason()+" AND "+ca);
			}else{
				ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
				responseObject.setClientMatchReason(ca);
			}
			
		}*/
	}	
	
	ObjectMapper obj = new ObjectMapper();
	System.out.println("josn >>>..."+obj.writeValueAsString(responseObject));
	if(responseObject==null){
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
    }
	log.info("Submit aura session finish");
	 return new ResponseEntity<>(responseObject, HttpStatus.OK);
}

/*public static String oDSSearch(String firstName, String surName, Date dob ) {
	
	  Checking the GL/SCI system for a client match>>
	 
	
	String oDSSearchWebServiceURL = "https://www.e2e.equery.metlife.com.au/CAW2/services/EAPPSearch";ConfigurationHelper.getConfigurationValue("OdsSearch", "weserviceURL").trim(); //"https://www.e2e.eapplication.metlife.com.au/CAW2/services/EServicing?wsdl";//System.getProperty("policySearchWebServiceURL"); //http://localhost:9080/CAW2/services/EServicing?wsdl
	
	EAPPSearchStub  stub1=null; 
	String ca= null;
	boolean status = false; 
	try {
		
		stub1 = new EAPPSearchStub(ServiceClientInstance.getInstance(),oDSSearchWebServiceURL);		 
		ServiceClient client = stub1._getServiceClient();		
		client.engageModule("rampart");
			
		Options options = client.getOptions();
		options.setTimeOutInMilliSeconds(100000);
		options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, getOutflowConfiguration("internal01"));
		PWCBClientHandler myCallback = new PWCBClientHandler();
		myCallback.setUTUsername("internal01");
		myCallback.setUTPassword("metlife980");
		options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
		client.setOptions(options);
		
		EAPPSearchStub.SearchExistingMember req = new EAPPSearchStub.SearchExistingMember();
		SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String dateString = formatter.format(dob);

        if(null == surName){
        	surName = "";
        }
        
		req.setInputXml("<Root><message>" +
    			"<Filters>" +
    			"<Filter>" +
    			"<FilterName>firstname</FilterName>" +
    			"<FilterValue>"+firstName+"</FilterValue>" +
    			"</Filter>" +
    			"<Filter>" +
    			"<FilterName>surname</FilterName>" +
    			"<FilterValue>"+surName+"</FilterValue>" +
    			"</Filter>" +
    			"<Filter>" +
    			"<FilterName>dob</FilterName>" +
    			"<FilterValue>"+dateString+"</FilterValue>" +
    			"</Filter>" +
    			"</Filters>" +
    			"</message></Root>");
		
		
        log.info("For GLSCI Search Last Name :{}",req.getInputXml());
        
        
        
        EAPPSearchStub.SearchExistingMemberResponse res = stub1.searchExistingMember(req);
		log.info(res.get_return());
        ca = res.get_return();


		
	}catch (AxisFault e) {			
		e.printStackTrace();
	}catch (Exception e) {			
		e.printStackTrace();
	}
	
	return ca;
	
}*/

/*private static Parameter getOutflowConfiguration(String username) {
	 final String METHOD_NAME = "getOutflowConfiguration";	//$NON-NLS-1$
       OutflowConfiguration ofc = new OutflowConfiguration();
       ofc.setActionItems("UsernameToken Timestamp");
       ofc.setPasswordType("PasswordText");
       ofc.setUser(username);	       
       return ofc.getProperty();
   }*/
}
