/* Change Cover Controller,Progressive and Mandatory validations Starts  */
AeisApp.controller('quote',['$scope', '$rootScope', '$routeParams', '$location','$timeout','$http', '$window', 'ngDialog', 'fetchUrlSvc', 'fetchPersoanlDetailSvc', 'fetchQuoteSvc', 'fetchOccupationSvc', 'fetchDeathCoverSvc', 'fetchTpdCoverSvc', 'fetchIpCoverSvc', 'MaxLimitService', 'NewOccupationService', 'CalculateService', '$q', 'appData', 'fetchAppNumberSvc', 'saveEapplyData', 'printPageSvc', 'DownloadPDFSvc', 'auraRespSvc', 'CalculateTPDService', 'propertyService','$filter','APP_CONSTANTS',
function($scope, $rootScope, $routeParams, $location, $timeout,$http, $window, ngDialog, fetchUrlSvc, fetchPersoanlDetailSvc, fetchQuoteSvc, fetchOccupationSvc, fetchDeathCoverSvc, fetchTpdCoverSvc, fetchIpCoverSvc, MaxLimitService, NewOccupationService, CalculateService, $q, appData, fetchAppNumberSvc, saveEapplyData, printPageSvc, DownloadPDFSvc, auraRespSvc, CalculateTPDService, propertyService,$filter,APP_CONSTANTS) {


  $scope.quotePageDetails = {
    manageType: 'CCOVER',
    partnerCode: 'AEIS',
    lastSavedOn: 'Quotepage',
    auraDisabled: true,
    email: null,
    contactType: null,
    contactPhone: null,
    contactPrefTime: null,
    dob: null,
    age: null,
    occupationDetails: {
      fifteenHr: null,
      citizenQue: null,
      industryCode: null,
      ownBussinessQues: null,
      ownBussinessYesQues: null,
      ownBussinessNoQues: null,
      withinOfficeQue: null,
      tertiaryQue: null,
      managementRoleQue: null,
      hazardousQue: null,
      gender: null,
      salary: null,
      otherOccupation: null,
      occupation: null
    },
    auraDisabled: null,
    existingDeathAmt: '0',
    existingDeathUnits: '0',
    deathOccCategory: null,
    freqCostType: null,
    ipIncreaseFlag: null,
    addnlDeathCoverDetails: {
      deathCoverName: null,
      deathCoverType: null,
      deathFixedAmt: null,
      deathInputTextValue: null,
      deathCoverPremium: null
    },
    existingTpdAmt: '0',
    existingTPDUnits: '0',
    tpdOccCategory: null,
    addnlTpdCoverDetails: {
      tpdCoverName: null,
      tpdCoverType: null,
      tpdFixedAmt: null,
      tpdInputTextValue: null,
      tpdCoverPremium: null
    },
    existingIPAmount: '0',
    existingIPUnits: null,
    ipOccCategory: null,
    ipcheckbox: null,
    indexationDeath: false,
    indexationTpd: false,
    addnlIpCoverDetails: {
      ipCoverName: null,
      ipCoverType: 'IpFixed',
      ipFixedAmt: null,
      ipInputTextValue: null,
      ipCoverPremium: null,
      waitingPeriod: null,
      benefitPeriod: null
    },
    totalPremium: 0,
    appNum: null,
    ackCheck: false,
    dodCheck: false,
    privacyCheck: false
  };
  $scope.modelOptions = {updateOn: 'blur'};
	$scope.phoneNumbr = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
	$scope.emailFormat = APP_CONSTANTS.emailFormat;
	$scope.regex = /[0-9]{1,3}/;
  $scope.deathCvrRegex = /^\d{1,8}(,\d{1,8}){0,9}?$/;
	$scope.isDCCoverRequiredDisabled = false;
	$scope.isDCCoverTypeDisabled = false;
	$scope.isTPDCoverRequiredDisabled = false;
	$scope.isTPDCoverTypeDisabled = false;

	$scope.isWaitingPeriodDisabled = false;
	$scope.isBenefitPeriodDisabled = false;
	$scope.isIPCoverRequiredDisabled = false;

	$scope.isIpSalaryCheckboxDisabled = false;
	$scope.ipWarningFlag = false;
	$scope.ackFlag = false;
	$scope.modalShown = false;
	$scope.invalidSalAmount = false;
	 $scope.salMaxlimit= false;
	$scope.dcIncreaseFlag = false;
	$scope.tpdIncreaseFlag = false;
	$scope.quotePageDetails.ipIncreaseFlag = false;
	$scope.quotePageDetails.auraDisabled = false;
	$scope.disclaimerFlag = true;
  $scope.IndustryOptions = null;
  $scope.OccupationList = null;

	$scope.showWithinOfficeQuestion = false;
  $scope.showTertiaryQuestion = false;
  $scope.showHazardousQuestion = false;
  $scope.showOutsideOfficeQuestion = false;
	$scope.urlList = fetchUrlSvc.getUrlList();
	$scope.quotePageDetails.freqCostType = 'Monthly';
	$scope.ipWarning = false;
	$scope.quotePageDetails.ipcheckbox = false;
	$scope.quotePageDetails.eligibleFrStIp = false;

	/*$scope.prevOtherOcc = null;*/

	/*Error Flags*/
	$scope.dodFlagErr = null;
	$scope.privacyFlagErr = null;
  $scope.deathErrorFlag = false;
  $scope.tpdErrorFlag = false;
  $scope.ipErrorFlag = false;
  $scope.eligibleFrStIpFlag = false;

  $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
  $scope.benefitPeriodOptions = ['2 Years','5 Years', 'Age 65'];
  $scope.premiumFrequencyOptions = ['Monthly'];
  $scope.contactTypeOptions = [{
    text: 'Home',
    value: '2'
  }, {
    text: 'Work',
    value: '3'
  }, {
    text: 'Mobile',
    value: '1'
  }];

  $scope.FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime','gender'];
  $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
  $scope.OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
  $scope.CoverCalculatorFormFields =['coverName','coverType','requireCover','tpdCoverName','tpdCoverType','TPDRequireCover'];

  $scope.personalDetails = {};
  $scope.contactDetails = {};
  $scope.isEmpty = function(value){
    return ((value == "" || value == null) || value == "0");
  };
  $scope.init = function() {
    var defer = $q.defer();
    $scope.deathCoverDetails = fetchDeathCoverSvc.getDeathCover();
		$scope.tpdCoverDetails = fetchTpdCoverSvc.getTpdCover();
		$scope.ipCoverDetails = fetchIpCoverSvc.getIpCover();

		$scope.quotePageDetails.deathOccCategory = $scope.deathCoverDetails.occRating || 'General';
		$scope.quotePageDetails.tpdOccCategory = $scope.tpdCoverDetails.occRating || 'General';
		$scope.quotePageDetails.ipOccCategory = $scope.ipCoverDetails.occRating || 'General';
		
		/*switch($scope.ipCoverDetails.waitingPeriod.toLowerCase().trim()){
		
		case "30days":
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "30 Days";
			$scope.ipCoverDetails.waitingPeriod = "30 Days";
			break;
		case "60days":
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "60 Days";
			$scope.ipCoverDetails.waitingPeriod = "60 Days";
			break;
		case "90days":
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "90 Days";
			$scope.ipCoverDetails.waitingPeriod = "90 Days";
			break;
		default :
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "90 Days";
			$scope.ipCoverDetails.waitingPeriod = "90 Days";
		}*/
		switch($scope.ipCoverDetails.waitingPeriod.toLowerCase().trim().substr(0,2)){
		
		case "30":
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "30 Days";
			$scope.ipCoverDetails.waitingPeriod = "30 Days";
			break;
		case "60":
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "60 Days";
			$scope.ipCoverDetails.waitingPeriod = "60 Days";
			break;
		case "90":
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "90 Days";
			$scope.ipCoverDetails.waitingPeriod = "90 Days";
			break;
		default :
			$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = "30 Days";
			$scope.ipCoverDetails.waitingPeriod = "30 Days";
		}
		switch($scope.ipCoverDetails.benefitPeriod.toLowerCase().trim().substr(0,1)){
		
		case "2":
			$scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod = "2 Years";
			$scope.ipCoverDetails.benefitPeriod = "2 Years";
			break;
		case "a":
			$scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod = "Age 65";
			$scope.ipCoverDetails.benefitPeriod = "Age 65";
			break;
		case "5":
			$scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod = "5 Years";
			$scope.ipCoverDetails.benefitPeriod = "5 Years";
			break;
		default :
			$scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod = "2 Years";
			$scope.ipCoverDetails.benefitPeriod = "2 Years";
		}

	//$scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.waitingPeriod) > -1 ? $scope.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';;
    //$scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod = $scope.benefitPeriodOptions.indexOf($scope.ipCoverDetails.benefitPeriod) > -1 ? $scope.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';

    if($scope.deathCoverDetails.type == "1") {
      $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType = "DcUnitised";
      $scope.quotePageDetails.exDcCoverType = "DcUnitised";
      $scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue = $scope.deathCoverDetails.units;
      $scope.quotePageDetails.existingDeathAmt = $scope.deathCoverDetails.amount || 0;
      $scope.quotePageDetails.existingDeathUnits = $scope.deathCoverDetails.units || 0;
      $timeout(function() {
        showhide('nodollar1','dollar1');
        showhide('nodollar','dollar');
      });
      
    } else if($scope.deathCoverDetails.type == "2") {
      $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType = "DcFixed";
      $scope.quotePageDetails.exDcCoverType = "DcFixed";
      $scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue = parseInt($scope.deathCoverDetails.amount);
      $scope.quotePageDetails.existingDeathAmt = parseInt($scope.deathCoverDetails.amount) || 0;
      $scope.isDCCoverTypeDisabled = true;
    }

    if($scope.tpdCoverDetails.type == "1") {
      $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType = "TPDUnitised";
      $scope.quotePageDetails.exTpdCoverType = "TPDUnitised";
      $scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdCoverDetails.units;
      $scope.quotePageDetails.existingTpdAmt = $scope.tpdCoverDetails.amount || 0;
      $scope.quotePageDetails.existingTPDUnits = $scope.tpdCoverDetails.units || 0;
      $timeout(function() {
        showhide('nodollar1','dollar1');
        showhide('nodollar','dollar');
      });
      
    } else if($scope.tpdCoverDetails.type == "2") {
      $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType = "TPDFixed";
      $scope.quotePageDetails.exTpdCoverType = "TPDFixed";
      $scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue = parseInt($scope.tpdCoverDetails.amount);
      $scope.quotePageDetails.existingTpdAmt = parseInt($scope.tpdCoverDetails.amount) || 0;
      $scope.isTPDCoverTypeDisabled = true;
    }
    
    if($scope.quotePageDetails.existingDeathAmt == 0 && $scope.deathCoverDetails.type  == "2") {
    	$scope.isDCCoverTypeDisabled = false;
    }
    
    $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = $scope.ipCoverDetails.amount;
    $scope.quotePageDetails.existingIPAmount = parseInt($scope.ipCoverDetails.amount) || 0;
    $scope.quotePageDetails.existingIPUnits = $scope.ipCoverDetails.units;
    $scope.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

    $scope.personalDetails = $scope.inputDetails.personalDetails || {};
    $scope.contactDetails = $scope.inputDetails.contactDetails || {};

    if($scope.contactDetails.prefContact == "1") {
			$scope.quotePageDetails.contactPhone = $scope.contactDetails.mobilePhone;
		} else if($scope.contactDetails.prefContact == "2") {
			$scope.quotePageDetails.contactPhone = $scope.contactDetails.homePhone;
		} else if($scope.contactDetails.prefContact == "3") {
			$scope.quotePageDetails.contactPhone = $scope.contactDetails.workPhone;
		} else {
      $scope.quotePageDetails.contactPhone = '';
    }

    fetchQuoteSvc.getList($scope.urlList.quoteUrl,"AEIS").then(function(res){
			$scope.IndustryOptions = res.data;
		}, function(err){
			console.info("Error while fetching industry list " + JSON.stringify(err));
		});

    $scope.quotePageDetails.age = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

    if($scope.deathCoverDetails && $scope.deathCoverDetails.benefitType && $scope.deathCoverDetails.benefitType == 1 ){
			
    	if(parseInt($scope.deathCoverDetails.amount) == 0 && ($scope.deathCoverDetails.units == "" || $scope.deathCoverDetails.units == null  ||  $scope.deathCoverDetails.units == 0)){
    		if($scope.quotePageDetails.age < 16 || $scope.quotePageDetails.age >= 65){
				$('#deathsection').removeClass('active');
				$("#death").css("display", "none");
				$scope.quotePageDetails.isDeathDisabled = true;
				 $scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue = 0;
			}
    	}else{
    		if($scope.quotePageDetails.age < 16 || $scope.quotePageDetails.age > 70){
				$('#deathsection').removeClass('active');
				$("#death").css("display", "none");
				$scope.quotePageDetails.isDeathDisabled = true;
				 $scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue = 0;
			}
    	}
	}
		if($scope.tpdCoverDetails && $scope.tpdCoverDetails.benefitType && $scope.tpdCoverDetails.benefitType == 2 ){
			
			if(parseInt($scope.deathCoverDetails.amount) == 0 && ($scope.deathCoverDetails.units == "" || $scope.deathCoverDetails.units == null  ||  $scope.deathCoverDetails.units == 0)){
				if($scope.quotePageDetails.age < 16 || $scope.quotePageDetails.age >= 65){
					$('#tpdsection').removeClass('active');
					$("#tpd").css("display", "none");
					$scope.quotePageDetails.isTPDDisabled = true;
					 $scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue = 0;
					
					 $('#ipsection').removeClass('active');
						$("#sc").css("display", "none");
						$scope.quotePageDetails.isIPDisabled = true;
						$scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = 0;
				}
			}else{
					if($scope.quotePageDetails.age < 16 || $scope.quotePageDetails.age > 70){
						$('#tpdsection').removeClass('active');			
						$("#tpd").css("display", "none");
						$scope.quotePageDetails.isTPDDisabled = true;
						$scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue = 0;
					}
				}
		}
		if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitType && $scope.ipCoverDetails.benefitType == 4 ){
			if($scope.quotePageDetails.age < 16 || $scope.quotePageDetails.age > 65){
				$('#ipsection').removeClass('active');
				$("#sc").css("display", "none");
				$scope.quotePageDetails.isIPDisabled = true;
				   $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = 0;
			}
		}
//		  if($scope.quotePageDetails && $scope.quotePageDetails.contactPrefTime){
//			    if($scope.quotePageDetails.contactPrefTime == "1"){
//			    	$scope.quotePageDetails.contactPrefTime= "Morning (9am - 12pm)";
//			    }else{
//			    	$scope.quotePageDetails.contactPrefTime= "Afternoon (12pm - 6pm)";
//			    }
//			  }
		MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl, "AEIS", $scope.inputDetails.memberType, "CCOVER").then(function(res){
			var limits = res.data;
			$scope.annualSalForUpgradeVal = limits[0].annualSalForUpgradeVal;
    	$scope.DCMaxAmount = limits[0].deathMaxAmount;
    	//$scope.DCMaxAmount = "99999999";
    	$scope.TPDMaxAmount = limits[0].tpdMaxAmount;
    	$scope.IPMaxAmount = limits[0].ipMaxAmount;
      defer.resolve(res);
		}, function(error){
			console.info('Something went wrong while fetching limits ' + error);
      defer.reject(err);
		});
    return defer.promise;
  };

  $scope.init().then(function() {
    angular.extend($scope.quotePageDetails, appData.getAppData());
    //$scope.quotePageDetails.dob = moment($scope.quotePageDetails.dob).format('DD-MM-YYYY');
    $scope.quotePageDetails.dob = moment($scope.quotePageDetails.dob, "DD/MM/YYYY").format('DD/MM/YYYY');
    if($scope.quotePageDetails.occupationDetails.fifteenHr == 'No') {
      $scope.disableIpCover();
    }
    if($scope.quotePageDetails.occupationDetails.industryCode) {
      fetchOccupationSvc.getOccupationList($scope.urlList.occupationUrl, "AEIS", $scope.quotePageDetails.occupationDetails.industryCode).then(function(res){
        $scope.OccupationList = res.data;
        $scope.getCategoryFromDB();
      }, function(err){
        console.info("Error while fetching occupations " + JSON.stringify(err));
      });
    }
    $scope.quotePageDetails.ipcheckbox = ($scope.quotePageDetails.ipcheckbox == 'true' || $scope.quotePageDetails.ipcheckbox == true) ? true : false;
    if($scope.quotePageDetails.ipcheckbox) {
      $scope.insureNinetyPercentIp();
    	$('#ipsalarycheck').parent().addClass('active');
		  $('#ipsalarycheck').attr('checked','checked');
    }
    $scope.$watch('quotePageDetails.dodCheck', function(newVal, oldVal) {
      $scope.togglePrivacy(newVal);
      $scope.toggleContact(newVal && $scope.quotePageDetails.privacyCheck);
      $scope.toggleOccupation(newVal && $scope.formOne.$valid && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
      $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.occupationForm.$valid && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
    });

    $scope.$watch('quotePageDetails.privacyCheck', function(newVal, oldVal) {
      $scope.toggleContact(newVal);
      $scope.toggleOccupation(newVal && $scope.formOne.$valid && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
      $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.occupationForm.$valid && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
    });

    $scope.$watch('formOne.$valid', function(newVal, oldVal) {
       $scope.toggleOccupation(newVal && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
       $scope.toggleCoverCalc(newVal && $scope.occupationForm.$valid && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
    });
    
    $scope.$watch('quotePageDetails.occupationDetails.salary', function(newVal, oldVal) {
    	if($scope.occupationForm.$valid && !$scope.invalidSalAmount && !$scope.salMaxlimit){
    		$scope.toggleCoverCalc(true);
    		$scope.maxTPDUnitscheck(false);
    	}
    });

    $scope.$watch('occupationForm.$valid', function(newVal, oldVal) {
       if($scope.invalidSalAmount || $scope.salMaxlimit){
    	   newVal = false;
       }
       $scope.maxTPDUnitscheck(false);
       $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.quotePageDetails.dodCheck && $scope.quotePageDetails.privacyCheck);
    });
    
    $scope.$watch('coverCalculatorForm.$valid', function(newVal, oldVal) {
    	$scope.maxTPDUnitscheck(false);
    });
    $scope.validateDeathTpdIpAmounts();
    if($scope.quotePageDetails.occupationDetails.smoker == undefined && $scope.quotePageDetails.occupationDetails.smokerQuestion == undefined){
	    if($scope.inputDetails.personalDetails.smoker == "1"){
	   	   $scope.quotePageDetails.occupationDetails.smoker = "Yes";
	     }else{
	       $scope.quotePageDetails.occupationDetails.smoker = "No";
	     }
    }else if($scope.quotePageDetails.occupationDetails.smoker == undefined){
    	if($scope.quotePageDetails.occupationDetails.smokerQuestion == "1"){
 	   	   $scope.quotePageDetails.occupationDetails.smoker = "Yes";
 	     }else{
 	       $scope.quotePageDetails.occupationDetails.smoker = "No";
 	     }
    }
    
    //$scope.toggleIPCondition();
  }, function() {

  });
  $scope.preparePageModel = function() {
    //var tempData = appData.getQuoteData();
  }
  $scope.changePrefContactType = function() {
    if($scope.quotePageDetails.contactType == "1") {
			$scope.quotePageDetails.contactPhone = $scope.contactDetails.mobilePhone;
		} else if($scope.quotePageDetails.contactType == "2") {
			$scope.quotePageDetails.contactPhone = $scope.contactDetails.homePhone;
		} else if($scope.quotePageDetails.contactType == "3") {
			$scope.quotePageDetails.contactPhone = $scope.contactDetails.workPhone;
		} else {
      $scope.quotePageDetails.contactPhone = '';
    }
  };

  $scope.isCollapsible = function(targetEle, event) {
    var dodLabelCheck = $('#dodLabel').hasClass('active');
    var privacyLabelCheck = $('#privacyLabel').hasClass('active');
    if( targetEle == 'collapseprivacy' && !dodLabelCheck) {
      if($('#dodLabel').is(':visible'))
        $scope.dodFlagErr = true;
      event.stopPropagation();
      return false;
    } else if( targetEle == 'collapseOne' && (!dodLabelCheck || !privacyLabelCheck)) {
      if($('#privacyLabel').is(':visible'))
        $scope.privacyFlagErr = true;
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseTwo' && (!dodLabelCheck || !privacyLabelCheck || $("#collapseOne form").hasClass('ng-invalid'))) {
      if($("#collapseOne form").is(':visible'))
        $scope.onFormContinue($scope.formOne);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseThree' && (!dodLabelCheck || !privacyLabelCheck || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid')) || $scope.invalidSalAmount || $scope.salMaxlimit) {
      if($("#collapseTwo form").is(':visible'))
        $scope.onFormContinue($scope.occupationForm);
      event.stopPropagation();
      return false;
    }
  }

  $scope.toggleOccupation = function(checkFlag) {
      if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseTwo']").click(); /* Can be improved */
  };

  $scope.toggleCoverCalc = function(checkFlag) {
      if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseThree']").click(); /* Can be improved */
  };

  $scope.togglePrivacy = function(checkFlag) {
      if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
  };

  $scope.toggleContact = function(checkFlag) {
      if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseOne']").click(); /* Can be improved */

  };
  $scope.checkDodState = function() {
    $timeout(function() {
      var dodLabelCheck = $('#dodLabel').hasClass('active');
      $scope.quotePageDetails.dodCheck = dodLabelCheck;
      $scope.dodFlagErr = !dodLabelCheck;
      if(dodLabelCheck) {
        $scope.togglePrivacy(true);
      } else {
        $scope.togglePrivacy(false);
        $scope.toggleContact(false);
        $scope.toggleOccupation(false);
        $scope.toggleCoverCalc(false);
      }
    }, 1);
  };

  $scope.checkPrivacyState  = function() {
    $timeout(function() {
      var privacyLabelCheck = $('#privacyLabel').hasClass('active');
      $scope.quotePageDetails.privacyCheck = privacyLabelCheck;
      $scope.privacyFlagErr = !privacyLabelCheck;
      if(privacyLabelCheck) {
        $scope.toggleContact(true);
      } else {
        $scope.toggleContact(false);
        $scope.toggleOccupation(false);
        $scope.toggleCoverCalc(false);
      }
    }, 1);
  };
  
  $scope.clickToOpen = function (hhText) {

		var dialog = ngDialog.open({
			template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
			className: 'ngdialog-theme-plain',
			plain: true
		});
		dialog.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	  
  };
	
  $scope.checkPreviousMandatoryFields  = function (elementName,formName) {
    var formFields;
    if(formName == 'formOne') {
      formFields = $scope.FormOneFields;
    } else if(formName == 'occupationForm') {
      if($scope.quotePageDetails.occupationDetails.occupation != undefined && $scope.quotePageDetails.occupationDetails.occupation == 'Other') {
        formFields = $scope.OccupationOtherFormFields;
      } else {
        formFields = $scope.OccupationFormFields;
      }
    } else if(formName == 'coverCalculatorForm') {
      formFields = $scope.CoverCalculatorFormFields;
    }
    var inx = formFields.indexOf(elementName);
    if(inx > 0){
      for(var i = 0; i < inx ; i++) {
        if($scope[formName][formFields[i]])
          $scope[formName][formFields[i]].$touched = true;
      }
    }
  };

  $scope.disableIpCover = function() {
    $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = 'No change';
    $scope.quotePageDetails.ipcheckbox = false;
    $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);
    $scope.isIPCoverNameDisabled = true;
    $scope.isWaitingPeriodDisabled = true;
    $scope.isBenefitPeriodDisabled = true;
    $scope.isIPCoverRequiredDisabled = true;
    $scope.isIpSalaryCheckboxDisabled = true;
    $scope.ipWarning = true;
    $('#ipsalarycheck').removeAttr('checked');
    $('#ipsalarychecklabel').removeClass('active');
  }
  $scope.toggleIPCondition = function() {
    if($scope.quotePageDetails.isIPDisabled)
      return false;
    if($scope.quotePageDetails.occupationDetails.fifteenHr == 'No') {
      $scope.disableIpCover();
    } else {
      $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = '';
      $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);
      $scope.isIPCoverNameDisabled = false;
      $scope.isWaitingPeriodDisabled = false;
      $scope.isBenefitPeriodDisabled = false;
      $scope.isIPCoverRequiredDisabled = false;
      $scope.isIpSalaryCheckboxDisabled = false;
      $scope.ipWarning = false;
    }
    $scope.renderOccupationQuestions();
  };
  
  $scope.toggleSmokeCondition = function() {
	  
	  if($scope.quotePageDetails.occupationDetails.smoker == 'No' || $scope.quotePageDetails.occupationDetails.smoker == 'Yes') {
		  $scope.renderOccupationQuestions();
	  }
	    
  };
  
  $scope.checkAnnualSalary = function() {
	   $scope.salMaxlimit = false;
	  
	  if(parseInt($scope.quotePageDetails.occupationDetails.salary) == 0){
		  $scope.invalidSalAmount = true;
		  $scope.toggleCoverCalc(false);
	  }else if(parseInt($scope.quotePageDetails.occupationDetails.salary) > 9999999){
		  $scope.salMaxlimit=true;
		  $scope.toggleCoverCalc(false);
	  }else{
		  $scope.invalidSalAmount = false;
	  }
	  
	  if(!$scope.invalidSalAmount && !$scope.salMaxlimit)
		  $scope.renderOccupationQuestions();
	  
	  if($scope.quotePageDetails.ipcheckbox) {
		  $scope.insureNinetyPercentIp();
	  }
 };

  $scope.insureNinetyPercentIp = function() {
    $scope.ipWarningFlag = false;
    $timeout(function() {
      $scope.quotePageDetails.ipcheckbox = $('#ipsalarycheck').prop('checked');
      if($scope.quotePageDetails.ipcheckbox == true) {
        $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue  = Math.floor(parseFloat((0.85 * ($scope.quotePageDetails.occupationDetails.salary/12)).toFixed(2)));
        if($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue > $scope.IPMaxAmount) {
          $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = $scope.IPMaxAmount;
          $scope.ipWarningFlag = true;
        } else if($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue < parseInt($scope.ipCoverDetails.amount)) {
            $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);
        } else {
          $scope.ipWarningFlag = false;
        }
        $scope.isIPCoverRequiredDisabled = true;
      } else {
        $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);
        $scope.isIPCoverRequiredDisabled = false;
        $scope.ipWarningFlag = false;
      }
      $scope.validateDeathTpdIpAmounts();
    });
  };

  $scope.checkOwnBusinessQuestion = function() {
    $scope.quotePageDetails.occupationDetails.ownBussinessYesQues = null;
    $scope.quotePageDetails.occupationDetails.ownBussinessNoQues = null;
    if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'Yes'){
      $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
      $scope.OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
    } else if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'No'){
      $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
      $scope.OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
    }
  };

  $scope.getOccupations = function() {
    $scope.quotePageDetails.occupationDetails.occupation = "";
    $scope.quotePageDetails.occupationDetails.otherOccupation = '';
    $scope.quotePageDetails.occupationDetails.industryCode = $scope.quotePageDetails.occupationDetails.industryCode == '' ? null : $scope.quotePageDetails.occupationDetails.industryCode;
    fetchOccupationSvc.getOccupationList($scope.urlList.occupationUrl, "AEIS", $scope.quotePageDetails.occupationDetails.industryCode).then(function(res){
      $scope.OccupationList = res.data;
    }, function(err){
      console.info("Error while fetching occupations " + JSON.stringify(err));
    });
  };
  
  /*$scope.getOtherOccupationAS = function(entered) {
	  return $http.get('./occupation.json').then(function(response) {
	  $scope.occupationList=[];
      if(response.data.Other) {
	        for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	              obj.name=response.data.Other[key];
                $scope.occupationList.push(obj.name);	               
	        }
	      }
      return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });	    
	  };*/

  $scope.syncRadios = function(val) {
    var radios = $('label[radio-sync]');
    var data = $('input[data-sync]');
    data.filter('[data-sync="' + val + '"]').attr('checked','checked');
    if(val == 'Fixed') {
      $scope.deathErrorFlag = false;
      $scope.quotePageDetails.deathErrorMsg="";
      $scope.tpdErrorFlag = false;
      $scope.quotePageDetails.tpdErrorMsg ="";
      showhide('dollar1','nodollar1');
      showhide('dollar','nodollar');
      $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType = 'DcFixed';
      $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType = 'TPDFixed';
    } else if(val == 'Unitised') {
      $scope.deathErrorFlag = false;
      $scope.quotePageDetails.deathErrorMsg="";
      $scope.tpdErrorFlag = false;
      $scope.quotePageDetails.tpdErrorMsg ="";
      showhide('nodollar1','dollar1');
      showhide('nodollar','dollar');
      $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType = 'DcUnitised';
      $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType = 'TPDUnitised';
    }
    radios.removeClass('active');
    radios.filter('[radio-sync="' + val + '"]').addClass('active');
    $scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue = null;
    $scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue = null;
    $scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue = val == 'Unitised' ? $scope.deathCoverDetails.units : $scope.deathCoverDetails.amount;
    $scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue = val == 'Unitised' ? $scope.tpdCoverDetails.units : $scope.tpdCoverDetails.amount;
    $scope.validateDeathTpdIpAmounts();
  };

  $scope.navigateToLandingPage = function() {
    ngDialog.openConfirm({
        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
    }).then(function(){
      $location.path("/landing");
    }, function(e){
      if(e=='oncancel'){
        return false;
      }
    });
  };

  $scope.getCategoryFromDB = function(fromSelect) {
	  /*if(fromSelect && $scope.quotePageDetails.occupationDetails.occupation!=="Other"){
		  $scope.quotePageDetails.occupationDetails.otherOccupation = '';
	  }*/
	  $scope.quotePageDetails.occupationDetails.otherOccupation = '';
	  /*if( $scope.prevOtherOcc !== $scope.QPD.occupationDetails.otherOccupation){	*/	    	  
		    if($scope.quotePageDetails.occupationDetails.occupation != undefined/* || $scope.quotePageDetails.occupationDetails.otherOccupation != undefined*/) {
		      if(fromSelect) {
		        $scope.quotePageDetails.occupationDetails.withinOfficeQue = null;
		        $scope.quotePageDetails.occupationDetails.tertiaryQue = null;
		        $scope.quotePageDetails.occupationDetails.hazardousQue = null;
		        $scope.quotePageDetails.occupationDetails.managementRoleQue = null;                                                     
		      }
		      var occName = $scope.quotePageDetails.occupationDetails.industryCode + ":" + $scope.quotePageDetails.occupationDetails.occupation;
		      /*if($scope.quotePageDetails.occupationDetails.occupation != undefined && ($scope.quotePageDetails.occupationDetails.otherOccupation == null || $scope.quotePageDetails.occupationDetails.otherOccupation == '')){
		    	  var occName = $scope.quotePageDetails.occupationDetails.industryCode + ":" + $scope.quotePageDetails.occupationDetails.occupation;
		      }else if ($scope.quotePageDetails.occupationDetails.otherOccupation != undefined){
		    	  if(($scope.OccupationList.find(o => o.occupationName === $scope.quotePageDetails.occupationDetails.otherOccupation))!== undefined){
		    		  var occName = $scope.quotePageDetails.occupationDetails.industryCode + ":" + $scope.quotePageDetails.occupationDetails.otherOccupation;
	    		  }else{
	    			  var occName = $scope.quotePageDetails.occupationDetails.industryCode + ":" + $scope.quotePageDetails.occupationDetails.occupation;
	    		  }		    	  
		      } */     
		      
		      NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "AEIS", occName).then(function(res){
		        if($scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType == "DcFixed"){
		          $scope.quotePageDetails.deathDBCategory = res.data[0].deathfixedcategeory;
		        }else if($scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType == "DcUnitised"){
		          $scope.quotePageDetails.deathDBCategory = res.data[0].deathunitcategeory;
		        }
		        if($scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType == "TPDFixed"){
		          $scope.quotePageDetails.tpdDBCategory = res.data[0].tpdfixedcategeory;
		        }else if($scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"){
		          $scope.quotePageDetails.tpdDBCategory = res.data[0].tpdunitcategeory;
		        }
		        $scope.quotePageDetails.ipDBCategory = res.data[0].ipfixedcategeory;
		        $scope.eligibleForStIp();
		        $scope.renderOccupationQuestions();
		      }, function(err){
		        console.info("Error while getting category from DB " + JSON.stringify(err));
		      });
		    }
	  /*	}*/
  };

  $scope.calculateOnChange = function(){
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
      $scope.calculate();
    }
  };

  $scope.renderOccupationQuestions = function() {
    
      if($scope.quotePageDetails.occupationDetails.occupation/* || $scope.quotePageDetails.occupationDetails.otherOccupation*/){
        var selectedOcc = $scope.OccupationList.filter(function(obj){
          return obj.occupationName == $scope.quotePageDetails.occupationDetails.occupation;        	
        	/*if($scope.quotePageDetails.occupationDetails.occupation && $scope.quotePageDetails.occupationDetails.otherOccupation == ''){
        		return obj.occupationName == $scope.quotePageDetails.occupationDetails.occupation;
	       	}else if($scope.quotePageDetails.occupationDetails.otherOccupation && (($scope.OccupationList.find(o => o.occupationName === $scope.quotePageDetails.occupationDetails.otherOccupation))!== undefined)){
	       		return obj.occupationName == $scope.quotePageDetails.occupationDetails.otherOccupation;
	       	}else{
	       		return obj.occupationName == $scope.quotePageDetails.occupationDetails.occupation;
	       	}*/
        	
        });
        var selectedOccObj = selectedOcc[0];

        if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
          $scope.showWithinOfficeQuestion = true;
          $scope.showTertiaryQuestion = true;
          $scope.showHazardousQuestion = false;
          $scope.showOutsideOfficeQuestion = false;
          if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'Yes'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
          } else if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'No'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
          }

          if($scope.quotePageDetails.occupationDetails.withinOfficeQue == 'Yes' && $scope.quotePageDetails.occupationDetails.tertiaryQue == 'Yes' && $scope.quotePageDetails.occupationDetails.salary && parseFloat($scope.quotePageDetails.occupationDetails.salary) >= parseFloat($scope.annualSalForUpgradeVal)){
            $scope.quotePageDetails.deathOccCategory = 'Professional';
            $scope.quotePageDetails.tpdOccCategory = 'Professional';
            $scope.quotePageDetails.ipOccCategory = 'Professional';
          } else{
            $scope.quotePageDetails.deathOccCategory = $scope.quotePageDetails.deathDBCategory;
            $scope.quotePageDetails.tpdOccCategory = $scope.quotePageDetails.tpdDBCategory;
            $scope.quotePageDetails.ipOccCategory = $scope.quotePageDetails.ipDBCategory;
          }
        } else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
          $scope.showWithinOfficeQuestion = false;
            $scope.showTertiaryQuestion = false;
            $scope.showHazardousQuestion = true;
            $scope.showOutsideOfficeQuestion = true;
            if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'Yes'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
          } else if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'No'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
          }
            if($scope.quotePageDetails.occupationDetails.hazardousQue == 'No' && $scope.quotePageDetails.occupationDetails.managementRoleQue == 'No'){
              $scope.showWithinOfficeQuestion = true;
                $scope.showTertiaryQuestion = true;
              if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'Yes'){
              $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
            } else if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'No'){
              $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
            }

              $scope.quotePageDetails.deathOccCategory = 'White Collar';
              $scope.quotePageDetails.tpdOccCategory = 'White Collar';
              $scope.quotePageDetails.ipOccCategory = 'White Collar';
              if($scope.quotePageDetails.occupationDetails.withinOfficeQue == 'Yes' && $scope.quotePageDetails.occupationDetails.tertiaryQue == 'Yes' && $scope.quotePageDetails.occupationDetails.salary && parseFloat($scope.quotePageDetails.occupationDetails.salary) >= parseFloat($scope.annualSalForUpgradeVal)){
                $scope.quotePageDetails.deathOccCategory = 'Professional';
                $scope.quotePageDetails.tpdOccCategory = 'Professional';
                $scope.quotePageDetails.ipOccCategory = 'Professional';
              } else{
                $scope.quotePageDetails.deathOccCategory = 'White Collar';
                $scope.quotePageDetails.tpdOccCategory = 'White Collar';
                $scope.quotePageDetails.ipOccCategory = 'White Collar';
              }
            } else{
              $scope.showWithinOfficeQuestion = false;
                $scope.showTertiaryQuestion = false;
              if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'Yes'){
              $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
            } else if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'No'){
              $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
            }

              $scope.quotePageDetails.deathOccCategory = $scope.quotePageDetails.deathDBCategory;
              $scope.quotePageDetails.tpdOccCategory = $scope.quotePageDetails.tpdDBCategory;
              $scope.quotePageDetails.ipOccCategory = $scope.quotePageDetails.ipDBCategory;
            }
        } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
          $scope.showWithinOfficeQuestion = false;
            $scope.showTertiaryQuestion = false;
            $scope.showHazardousQuestion = true;
            $scope.showOutsideOfficeQuestion = true;
            if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'Yes'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
          } else if($scope.quotePageDetails.occupationDetails.ownBussinessQues == 'No'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
          }

            if($scope.quotePageDetails.occupationDetails.hazardousQue == 'No' && $scope.quotePageDetails.occupationDetails.managementRoleQue == 'No'){
              $scope.quotePageDetails.deathOccCategory = 'White Collar';
                $scope.quotePageDetails.tpdOccCategory = 'White Collar';
                $scope.quotePageDetails.ipOccCategory = 'White Collar';
            } else{
              $scope.quotePageDetails.deathOccCategory = $scope.quotePageDetails.deathDBCategory;
                $scope.quotePageDetails.tpdOccCategory = $scope.quotePageDetails.tpdDBCategory;
                $scope.quotePageDetails.ipOccCategory = $scope.quotePageDetails.ipDBCategory;
            }
        } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
          $scope.showWithinOfficeQuestion = false;
            $scope.showTertiaryQuestion = false;
            $scope.showHazardousQuestion = false;
            $scope.showOutsideOfficeQuestion = false;
          $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];

            $scope.quotePageDetails.deathOccCategory = $scope.quotePageDetails.deathDBCategory;
            $scope.quotePageDetails.tpdOccCategory = $scope.quotePageDetails.tpdDBCategory;
            $scope.quotePageDetails.ipOccCategory = $scope.quotePageDetails.ipDBCategory;
        }
      }
     else {
      $scope.showWithinOfficeQuestion = false;
        $scope.showTertiaryQuestion = false;
        $scope.showHazardousQuestion = false;
        $scope.showOutsideOfficeQuestion = false;
        $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];

        if($scope.quotePageDetails.deathDBCategory == 'White Collar'){
          $scope.quotePageDetails.deathOccCategory = 'Standard';
        } else if($scope.quotePageDetails.deathDBCategory == 'Professional'){
          $scope.quotePageDetails.deathOccCategory = 'Professional';
        } else{
          $scope.quotePageDetails.deathOccCategory = 'Standard';
        }

        if($scope.quotePageDetails.tpdDBCategory == 'White Collar'){
          $scope.quotePageDetails.tpdOccCategory = 'Standard';
        } else if($scope.quotePageDetails.tpdDBCategory == 'Professional'){
          $scope.quotePageDetails.tpdOccCategory = 'Professional';
        } else{
          $scope.quotePageDetails.tpdOccCategory = 'Standard';
        }

      if($scope.quotePageDetails.ipDBCategory == 'White Collar'){
          $scope.quotePageDetails.ipOccCategory = 'Standard';
        } else if($scope.quotePageDetails.ipDBCategory == 'Professional'){
          $scope.quotePageDetails.ipOccCategory = 'Professional';
        } else{
          $scope.quotePageDetails.ipOccCategory = 'Standard';
        }
    }
    $scope.customDigest();
    var _this = this;
    $timeout(function(){
      if(_this.formOne.$valid && _this.occupationForm.$valid && _this.coverCalculatorForm.$valid){
        $scope.calculate();
      }
    });
  
  };

  // validate fields "on continue"
  $scope.onFormContinue =  function (form) {
   if(!form.$valid) {
     form.$submitted=true;
     if(form.$name == 'formOne'){
       $scope.toggleOccupation(false);
       $scope.toggleCoverCalc(false);
     } else if(form.$name == 'occupationForm'){
       $scope.toggleCoverCalc(false);
     }
   } else {
    if(form.$name == 'formOne') {
      $scope.toggleOccupation(true);
    } else if(form.$name == 'occupationForm') {
    	if($scope.invalidSalAmount || $scope.salMaxlimit){
			$scope.toggleCoverCalc(false);
    	}else{
      		$scope.toggleCoverCalc(true);
    	}
    } else if(form.$name == 'coverCalculatorForm') {
        if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag){
          $scope.calculate();
        }
      }
    }
  };

  $scope.calculate = function() {
	  var smoker = true;
	  if($scope.quotePageDetails.occupationDetails.smoker == "No"){
		  smoker = false;
	  }
	  var ruleModel = {
      "age": $scope.quotePageDetails.age,
      "fundCode": "AEIS",
      "gender": $scope.quotePageDetails.occupationDetails.gender,
      "deathOccCategory": $scope.quotePageDetails.deathOccCategory,
      "tpdOccCategory": $scope.quotePageDetails.tpdOccCategory,
      "ipOccCategory": $scope.quotePageDetails.ipOccCategory,
      "smoker": smoker,
      "deathUnits": parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue),
      "deathFixedAmount": parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue),
      "deathFixedCost": null,
      "deathUnitsCost": null,
      "tpdUnits": parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue),
      "tpdFixedAmount": parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue),
      "tpdFixedCost": null,
      "tpdUnitsCost": null,
      "ipUnits": null,
      "ipFixedAmount": parseInt($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue),
      "ipFixedCost": null,
      "ipUnitsCost": null,
      "premiumFrequency": $scope.quotePageDetails.freqCostType,
      "memberType": $scope.inputDetails.memberType,
      "manageType": "CCOVER",
      "deathCoverType": $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType,
      "tpdCoverType": $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType,
      "ipCoverType": "IpFixed",
      "ipWaitingPeriod": $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod,
      "ipBenefitPeriod": $scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod
    };
    CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res) {
      var premium = res.data;
      $scope.dynamicFlag = true;
      for(var i = 0; i < premium.length; i++){
        if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
          $scope.quotePageDetails.addnlDeathCoverDetails.deathFixedAmt = premium[i].coverAmount || 0;
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdFixedAmt = premium[i].coverAmount || 0;
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
          $scope.quotePageDetails.addnlIpCoverDetails.ipFixedAmt = premium[i].coverAmount || 0;
          $scope.quotePageDetails.addnlIpCoverDetails.ipCoverPremium = premium[i].cost || 0;
        }
      }
      $scope.quotePageDetails.totalPremium = parseFloat($scope.quotePageDetails.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.quotePageDetails.addnlIpCoverDetails.ipCoverPremium);
    }, function(err){
      console.info("Something went wrong while calculating..." + JSON.stringify(err));
    });
  };

  $scope.checkBenefitPeriod = function() {
	  
	  if($scope.ipCoverDetails.waitingPeriod== $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod && $scope.ipCoverDetails.benefitPeriod == $scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod){
		  $scope.quotePageDetails.ipIncreaseFlag = false;
	      $scope.disclaimerFlag = true;
	  } else if(($scope.ipCoverDetails.benefitPeriod == '2 Years' && $scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod == 'Age 65') ||
	        ($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod == '60 Days') ||
	        ($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod == '30 Days') ||
	        ($scope.ipCoverDetails.waitingPeriod == '60 Days' && $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod == '30 Days')){
	      $scope.quotePageDetails.ipIncreaseFlag = true;
	      $scope.disclaimerFlag = true;
	      $scope.quotePageDetails.auraDisabled = false;
	    }else if(($scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod  == '5 Years') ||
	            ($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod == '60 Days') ||
	            ($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod == '30 Days') ||
	            ($scope.ipCoverDetails.waitingPeriod == '60 Days' && $scope.quotePageDetails.addnlIpCoverDetails.waitingPeriod == '30 Days')){
	          $scope.quotePageDetails.ipIncreaseFlag = true;
	          $scope.disclaimerFlag = true;
	          $scope.quotePageDetails.auraDisabled = false;
	    }else{
	      $scope.quotePageDetails.ipIncreaseFlag = false;
	      $scope.disclaimerFlag = false;
	    }
	  
	  $scope.eligibleForStIp();
	  $scope.validateDeathTpdIpAmounts();
  };
  
  $scope.eligibleForStIp = function(){
		occ = $scope.quotePageDetails.occupationDetails.occupation;
		var occSearch = true;
		 propertyService.getOccList().then(function(response){
			 $scope.decOccList = response.data;
			 angular.forEach($scope.decOccList,function(value,key){
				  if(occSearch){
		    		  if(key.toLowerCase() === occ.toLowerCase()){
		    			  if(value.toLowerCase() == 'ip'){
		    				  $scope.quotePageDetails.eligibleFrStIp = true;
			    			  occSearch = false;
		    			  }
		    		  }else{
		    			  $scope.quotePageDetails.eligibleFrStIp = false;
				      }
				  }
			 });
	    	  if($scope.quotePageDetails.eligibleFrStIp && $scope.quotePageDetails.addnlIpCoverDetails.benefitPeriod == 'Age 65'){
	    		  $scope.eligibleFrStIpFlag = true;
	    	  }else{
	    		  $scope.eligibleFrStIpFlag = false;
	    	  }
		});
	  };

  $scope.checkIpCover = function() {
    //$scope.ipWarningFlag = false;
      if(parseInt($scope.quotePageDetails.occupationDetails.salary) <= 1000000){
        $scope.IPAmount  = Math.floor((0.85 * ($scope.quotePageDetails.occupationDetails.salary/12)));
      } else if(parseInt($scope.quotePageDetails.occupationDetails.salary) > 1000000){
        $scope.IPAmount = Math.floor((Math.round(((parseInt($scope.quotePageDetails.occupationDetails.salary))/12)*0.6) ));
      }
      $scope.IPAmount = $scope.IPMaxAmount < $scope.IPAmount ? $scope.IPMaxAmount : $scope.IPAmount;
      $scope.IPAmount = parseInt($scope.ipCoverDetails.amount) > $scope.IPAmount ? parseInt($scope.ipCoverDetails.amount) : $scope.IPAmount;
  };
  
  $scope.maxTPDUnitscheck=function(toAura) {
	  
	  if($scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"){
		  var unitTPDReqObj = {
	      		"age": $scope.quotePageDetails.age,
	      		"fundCode": "AEIS",
	      		 "gender": $scope.quotePageDetails.occupationDetails.gender,
	      	     "tpdOccCategory": $scope.quotePageDetails.tpdOccCategory,
	      		"smoker": false,
	      		"tpdUnits": 1,
	      		"tpdUnitsCost": null,
	      		"premiumFrequency": "Monthly",
	      		"memberType": $scope.inputDetails.memberType,
	      		"manageType": "CCOVER",
	      		"tpdCoverType": "TPDUnitised",
	      	};
		  
		  CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,unitTPDReqObj).then(function(res){
			  
			  tpdUnitFlag = false;
			  tpdUnitSuccess = true;
			  var TPDUnitAmount = res.data[0].coverAmount;
			  var TPDMaxAmount= $scope.TPDMaxAmount;
			  $scope.maxTPDUnits = Math.floor(TPDMaxAmount/TPDUnitAmount);
			  
			  if($scope.coverCalculatorForm.$valid && parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.maxTPDUnits)){
	    	      $scope.tpdErrorFlag = true;
	    	      $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + $scope.maxTPDUnits + ". Please re-enter your cover.";
	    	   }
			  
			  /*if( $scope.quotePageDetails.addnlTpdCoverDetails.tpdFixedAmt >$scope.TPDMaxAmount){
				  $scope.tpdErrorFlag = true;
				  $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + $scope.TPDMaxAmount + ". Please re-enter your cover.";
			  }*/
			  
			  if(toAura){
				  $scope.goToAura();
			  }
	  		  
	  		}, function(err){
	  		console.info('Error while fetching TPD amount ' + err);
	  		});	
	  }else{
		  if(toAura){
			  $scope.goToAura();
		  }
	  }
	 };
 
	 $scope.goToAura = function(){ 
		  if($scope.formOne.$valid && $scope.occupationForm.$valid && $scope.coverCalculatorForm.$valid) {		    	
		    	if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag) {
		    		if($scope.deathDecOrCancelFlag || $scope.tpdDecOrCancelFlag || $scope.ipDecOrCancelFlag){
		    			$scope.decOrCancelCovers();
		    		}else{
		    			$scope.goTo();
		    		}
		    	}
		    }
	}

  $scope.validateDeathTpdIpAmounts = function() {
    $scope.quotePageDetails.auraDisabled = true;
    $scope.deathErrorFlag = false;
    $scope.tpdErrorFlag = false;
    $scope.ipErrorFlag = false;
    $scope.deathDecOrCancelFlag = false;
    $scope.tpdDecOrCancelFlag = false;
    $scope.ipDecOrCancelFlag = false;
    $scope.ipWarningFlag = false;
    $scope.checkIpCover();
    // error check
    if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.DCMaxAmount)){
      $scope.deathErrorFlag = true;
      $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is  " + $scope.DCMaxAmount + ". Please re-enter your cover.";
    }
    if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue)){
      $scope.tpdErrorFlag = true;
      $scope.tpdErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
    } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.TPDMaxAmount)){
      $scope.tpdErrorFlag = true;
      $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + $scope.TPDMaxAmount + ". Please re-enter your cover.";
    }
    if(parseInt($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue) > $scope.IPAmount) {
      $scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue = $scope.IPAmount;
      $scope.ipErrorFlag = false;
      $scope.ipErrorMsg="";
      $scope.ipWarningFlag = true;
    }
    //cover name check
    if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag) {
      // Death cover
      if($scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType == "DcFixed") {
        if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) < parseInt($scope.deathCoverDetails.amount) ) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
          $scope.deathDecOrCancelFlag = true;
        } else if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.deathCoverDetails.amount) ) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
          $scope.quotePageDetails.auraDisabled = false;
        } else if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) == parseInt($scope.deathCoverDetails.amount) ) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'No change';
        } else if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathCoverDetails.amount != 0)) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
          $scope.deathDecOrCancelFlag = true;
        }
      } else if($scope.quotePageDetails.addnlDeathCoverDetails.deathCoverType == "DcUnitised") {
        if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) < parseInt($scope.deathCoverDetails.units) ) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
          $scope.deathDecOrCancelFlag = true;
        } else if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.deathCoverDetails.units) ) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
          $scope.quotePageDetails.auraDisabled = false;
        } else if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) == parseInt($scope.deathCoverDetails.units) ) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'No change';
        } else if(parseInt($scope.quotePageDetails.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathCoverDetails.units != 0)) {
          $scope.quotePageDetails.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
          $scope.deathDecOrCancelFlag = true;
        }
      }

      // Tpd cover
      if($scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType == "TPDFixed") {
        if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) < parseInt($scope.tpdCoverDetails.amount) ) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
          $scope.tpdDecOrCancelFlag = true;
        } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.tpdCoverDetails.amount) ) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
          $scope.quotePageDetails.auraDisabled = false;
        } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) == parseInt($scope.tpdCoverDetails.amount) ) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'No change';
        } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) == 0 && parseInt($scope.tpdCoverDetails.amount != 0)) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
          $scope.tpdDecOrCancelFlag = true;
        }
      } else if($scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
        if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) < parseInt($scope.tpdCoverDetails.units) ) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
          $scope.tpdDecOrCancelFlag = true;
        } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.tpdCoverDetails.units) ) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
          $scope.quotePageDetails.auraDisabled = false;
        } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) == parseInt($scope.tpdCoverDetails.units) ) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'No change';
        } else if(parseInt($scope.quotePageDetails.addnlTpdCoverDetails.tpdInputTextValue) == 0 && parseInt($scope.tpdCoverDetails.units != 0)) {
          $scope.quotePageDetails.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
          $scope.tpdDecOrCancelFlag = true;
        }
      }

      // Ip cover
      if(parseInt($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue) < parseInt($scope.ipCoverDetails.amount) ) {
        $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = 'Decrease your cover';
        $scope.ipDecOrCancelFlag = true;
      } else if(parseInt($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.ipCoverDetails.amount) ) {
        $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = 'Increase your cover';
        $scope.quotePageDetails.auraDisabled = false;
      } else if(parseInt($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue) == parseInt($scope.ipCoverDetails.amount) ) {
        $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = 'No change';
      } else if(parseInt($scope.quotePageDetails.addnlIpCoverDetails.ipInputTextValue) == 0 && parseInt($scope.ipCoverDetails.amount != 0)) {
        $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = 'Cancel your cover';
        $scope.ipDecOrCancelFlag = true;
      }
      
      if($scope.quotePageDetails.ipIncreaseFlag) {
    	  $scope.quotePageDetails.addnlIpCoverDetails.ipCoverName = 'Increase your cover';
          $scope.quotePageDetails.auraDisabled = false;
        }
      // ends here
      $scope.customDigest();
      $timeout(function(){
        $scope.calculateOnChange();
      });
    }
 };

$scope.customDigest = function() {
  if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
      $scope.$digest();
  }
}

  $scope.quoteSaveAndExitPopUp = function (hhText) {
   var dialog1 = ngDialog.open({
       template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
       className: 'ngdialog-theme-plain custom-width',
       preCloseCallback: function(value) {
        var url = "/landing"
        $location.path( url );
        return true
       },
       plain: true
   });
   dialog1.closePromise.then(function (data) {
     console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
   });
  };

  $scope.saveQuote = function() {
    $scope.quotePageDetails.lastSavedOn = 'Quotepage';
    $rootScope.$broadcast('disablepointer');
    var selectedIndustry = $scope.IndustryOptions.filter(function(obj) {
      return $scope.quotePageDetails.occupationDetails.industryCode == obj.key;
    });
    
    if($scope.quotePageDetails.occupationDetails.smoker == 'Yes') {
    	$scope.quotePageDetails.occupationDetails.smokerQuestion = 1;
	}else{
		$scope.quotePageDetails.occupationDetails.smokerQuestion = 2;
	}
    $scope.quotePageDetails.occupationDetails.industryName = selectedIndustry[0].value;
    $scope.quotePageDetails.industryName = selectedIndustry[0].value;
    $scope.quotePageDetails.industryCode = selectedIndustry[0].key;
    $scope.quotePageDetails.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
    $scope.validateDeathTpdIpAmounts();
    $scope.quotePageDetails = angular.extend($scope.quotePageDetails, $scope.inputDetails);
    saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, $scope.quotePageDetails).then(function(response) {
      console.log(response.data);
      $scope.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+$scope.quotePageDetails.appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR>');
    });
  };
  $scope.validateUnits =function() {
	  if( $scope.quotePageDetails.addnlTpdCoverDetails.tpdFixedAmt >$scope.TPDMaxAmount){
		  $scope.tpdErrorFlag = true;
		  $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + $scope.TPDMaxAmount + ". Please re-enter your cover.";
		  return false;
	  }
	  else{
		  return true;
	  }
     
	  };
    
	  $scope.validateAndProceed = function() {
	    this.formOne.$submitted = true;
	    this.occupationForm.$submitted = true;
	    this.coverCalculatorForm.$submitted = true;
	    $scope.validateDeathTpdIpAmounts();
	    $scope.maxTPDUnitscheck(true);
	  };

  $scope.goTo = function() {
    if(!$scope.quotePageDetails.auraDisabled) {
      $timeout(function() {
        $scope.continueToNextPage('/aura');
      }, 10);
    } else {
      $timeout(function() {
        $scope.continueToNextPage('/summary');
      }, 10);
    }
  }
  $scope.continueToNextPage = function(path) {
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
      var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
        return $scope.quotePageDetails.occupationDetails.industryCode == obj.key;
      });
      $scope.quotePageDetails.occupationDetails.industryName = selectedIndustry[0].value;
      $scope.quotePageDetails.industryName = selectedIndustry[0].value;
      $scope.quotePageDetails.industryCode = selectedIndustry[0].key;
      $scope.quotePageDetails.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      appData.setAppData($scope.quotePageDetails);
      $location.path(path);
    }
  };

  $scope.generatePDF = function(){
    // var CCOccDetails = PersistenceService.getChangeCoverOccDetails();
    // var deathAddnlInfo = PersistenceService.getDeathAddnlCoverDetails();
    // var tpdAddnlInfo = PersistenceService.getTpdAddnlCoverDetails();
    // var ipAddnlInfo = PersistenceService.getIpAddnlCoverDetails();
    //   var personalInformation = persoanlDetailService.getMemberDetails();
    //   var quoteObj =  PersistenceService.getChangeCoverDetails();
    //   if(quoteObj != null && CCOccDetails != null && deathAddnlInfo != null && tpdAddnlInfo != null && ipAddnlInfo != null && personalInformation[0] != null ){
    //     var info={};
    //     info.addnlDeathCoverDetails = deathAddnlInfo;
    //     info.addnlTpdCoverDetails = tpdAddnlInfo;
    //     info.addnlIpCoverDetails = ipAddnlInfo;
    //     info.occupationDetails = CCOccDetails;
    //   var temp = angular.extend(info,quoteObj);
    //   var printObject = angular.extend(temp, personalInformation[0]);
    //   auraResponseService.setResponse(printObject);
      $rootScope.$broadcast('disablepointer');
    var selectedIndustry = $scope.IndustryOptions.filter(function(obj) {
      return $scope.quotePageDetails.occupationDetails.industryCode == obj.key;
    });
    $scope.quotePageDetails.occupationDetails.industryName = selectedIndustry[0].value;
    $scope.quotePageDetails.industryName = selectedIndustry[0].value;
    $scope.quotePageDetails.smoker=$scope.quotePageDetails.occupationDetails.smoker;
    $scope.quotePageDetails.industryCode = selectedIndustry[0].key;
    $scope.quotePageDetails.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
    //$scope.validateDeathTpdIpAmounts();
    $scope.quotePageDetails = angular.extend($scope.quotePageDetails, $scope.inputDetails);
    auraRespSvc.setResponse($scope.quotePageDetails);
      printPageSvc.reqObj($scope.urlList.printQuotePage).then(function(response) {
        appData.setPDFLocation(response.data.clientPDFLocation);
        $scope.downloadPDF();
    }, function(err){
      $rootScope.$broadcast('enablepointer');
      console.info("Something went wrong while generating pdf..." + JSON.stringify(err));
    });
  }
  
  $scope.downloadPDF = function(){
          var pdfLocation =null;
          var filename = null;
          var a = null;
        pdfLocation = appData.getPDFLocation();
        console.log(pdfLocation+"pdfLocation");
        filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
        a = document.createElement("a");
          document.body.appendChild(a);
          DownloadPDFSvc.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
          //DownloadPDFService.download({file_name: pdfLocation}, function(res){
          if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
                 window.navigator.msSaveBlob(res.data.response,filename);
             }else{
              var fileURL = URL.createObjectURL(res.data.response);
              a.href = fileURL;
              a.download = filename;
              a.click();
             }
             $rootScope.$broadcast('enablepointer');
        }, function(err){
          console.log("Error downloading the PDF " + err);
          $rootScope.$broadcast('enablepointer');
        });
      };
      
      // added for Decrease or Cancel cover popup "on continue" after calculate quote
		$scope.showDecreaseOrCancelPopUp = function (val){	
		   if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
			   if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag){
				   if(val == null || val == "" || val == " "){
			   			hideTips();
			   		}else{
			   		 /*ackCheck = $('#termsLabel').hasClass('active');
			    		if(ackCheck){*/
			    			 $scope.ackFlag = false;
					   		 document.getElementById('mymodalDecCancel').style.display = 'block';
					   		 document.getElementById('mymodalDecCancelFade').style.display = 'block';
					   		 document.getElementById('decCancelMsg_text').innerHTML=val;	
			    		/*}else{
			    			$scope.ackFlag = true;
			    		}*/
			    		
			   		} 
			   }else{
				   return false;
			   }
		   }
	   	//	$scope.saveDataForPersistence();
	   	}
	    $scope.hideTips = function  (){
	   		if(document.getElementById('help_div')){
	   			document.getElementById('help_div').style.display = "none";
	   		}						
	   	}
      
  	$scope.decOrCancelCovers = function(){
  		if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death, TPD & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death & TPD";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag && $scope.ipDecOrCancelFlag){
  			$scope.decCancelCover = "Death & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "TPD & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.tpdDecOrCancelFlag == true){
  			$scope.decCancelCover = "TPD";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else{
  			$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
  		}					
  		/*$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";*/
  		$scope.showDecreaseOrCancelPopUp($scope.decCancelMsg);
  	};
      

}]);
