rem SET JAVALIBS=C:\Javalibs
rem C:
rem cd C:\WorkSpace\Eclipse\CRAFT
rem java -cp ".;.\supportlibraries\*;%JAVALIBS%\poi-3.10.1\poi-3.10.1-20140818.jar;%JAVALIBS%\poi-3.10.1\poi-ooxml-3.10.1-20140818.jar;%JAVALIBS%\poi-3.10.1\poi-ooxml-schemas-3.10.1-20140818.jar;%JAVALIBS%\poi-3.10.1\ooxml-lib\*;%JAVALIBS%\Selenium\*" allocator.Allocator

SET curr = chdir
echo %curr%
cd %curr%
SET JAVALIBS=%cd%\lib
cd %cd%\src
java -cp ".;%JAVALIBS%\*;%cd%\..\bin" allocator.Allocator
