/* Angular JS services
@ created  Purnachandra k
*/
claimApp.factory('claimFactory', ['$http', function($http) {

    //var urlBase = 'http://10.173.60.69:8084/claims/retrieveClaimTestDetails';
	//var urlBase = 'https://www.eapplication.metlife.com.au/services/ClaimServices/';
	var captcheUrl = 'http://localhost:8087//login';
	//prod urls
	//var updateFunds = 'http://localhost:8087/updateFunds/';
	//var retrieveFunds = 'http://localhost:8087/retrieveFunds';
//	var uploadUrl = "https://www.eapplication.metlife.com.au/ebusiness/singleSave";
	//e2e urls
	var updateFunds = 'https://www.e2e.eapplication.metlife.com.au/ebusiness/updateFunds/';
	var retrieveFunds = 'https://www.e2e.eapplication.metlife.com.au/ebusiness/retrieveFunds';	
	
    var factObj = {};   
    var claimData = {};
    var noOutStanding = true;
    var noCompleted = true;
    var claimId = '';
    var partnerName='';
    var claimStatus = '';
    var uniqueId='';
    var activitiesData ='';
    var labelProperties = '';
    var disableContcHdr = true;
    var ipAddr;
    var loginAttempts = 0;
    
    var captcheConfig = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
              //  'Access-Control-Allow-Origin':'http://localhost:8087'
            }
        }  
    var headerConfig = {
            headers : {
                'Content-Type': 'application/json',
               // 'Access-Control-Allow-Origin':'http://10.173.60.69:8080',
               // 'withCredentials':true
            }
        }  
    
    factObj.getClaim = function (user,url,fundid) { 
    	if ('CareSuper' == fundid){
    		fundid = 'CARE';
  		  }
    	 var data = { claimId: user.claimNo,
    	 	   surName: user.lName,
    		   dob: user.dob,
    		   fundID:fundid};    	
    	 //data = JSON.parse(data);
       return $http.post(url,data,headerConfig);
    };
    
    factObj.getClaimActivities = function (uniqueId,url) {  
   	// var data = { claimId: uniqueId};    	
   	 //data = JSON.parse(data);
      return $http.get(url+uniqueId);
   };
    
    factObj.getClaimData = function(){    
    	return JSON.parse(sessionStorage.getItem('claimData'));
        //return claimData;
    };
    
    factObj.setClaimData = function(data){
    	claimData = data;
    	sessionStorage.setItem('claimData',JSON.stringify(claimData));
       // return claimData;
    };    
    
    factObj.getLabelProperties = function(){    	
    	  return JSON.parse(sessionStorage.getItem('labelProperties'));
     //   return labelProperties;
    };
    
    factObj.setLabelProperties = function(data){
    	labelProperties = data;
    	sessionStorage.setItem('labelProperties',JSON.stringify(labelProperties));
       // return labelProperties;
    };
    factObj.getActivitiesData = function(){    	
       // return activitiesData;
        return JSON.parse(sessionStorage.getItem('activitiesData'));
    };
    
    factObj.setActivitiesData = function(data){
    	activitiesData = data;
    	sessionStorage.setItem('activitiesData',JSON.stringify(activitiesData));
        //return activitiesData;
    };
    factObj.filterClaimData = function(claimActivities){
    	noOutStanding = true;
    	noCompleted = true;
    	//claimActivities = JSON.parse(claimActivities);
    	if (claimActivities != null){
		    for(var i=0; i < claimActivities.length; i++){
		        var activity =  claimActivities[i];
		        if (activity.name == 'Information Requested'   && activity.requirementReceviedDate == '') {           		
		        		noOutStanding = false;
		        		//alert('hii1')	        		
	        		break;           		
		        }	       
		    }
		    for(var i=0; i < claimActivities.length; i++){
		        var activity =  claimActivities[i];
		        if (activity.name == 'Information Requested'   && activity.requirementReceviedDate != '') {           		
		        		noCompleted = false;
		        		//alert('hii2')
	        		
	        		break;           		
		        }	       
		    }
    	}
    	//alert('1 :'+noOutStanding);
    	sessionStorage.setItem('noOutStanding',JSON.stringify(noOutStanding));
    	sessionStorage.setItem('noCompleted',JSON.stringify(noCompleted));
    };
    
    factObj.getNoOutStanding = function() {   
    	//alert('2 :'+JSON.parse(sessionStorage.getItem('noOutStanding')));
    	 return JSON.parse(sessionStorage.getItem('noOutStanding'));
    	//return noOutStanding;
    };
    factObj.getNoCompleted = function() {  
    	return JSON.parse(sessionStorage.getItem('noCompleted'));
    	//return noCompleted;
    };
    
    factObj.verifyCaptche = function (data,captcheUrl) {  
    	
        return $http.post(captcheUrl, data,captcheConfig);
     };
     
     factObj.getClaimId = function() { 
    	 return JSON.parse(sessionStorage.getItem('claimId'));
    	// sessionStorage.getItem('claimId',JSON.stringify(claimId;
     };
     factObj.setClaimId = function(data){
    	 claimId = data;
    	 sessionStorage.setItem('claimId',JSON.stringify(claimId));
         //return claimId;
     }; 
     
     factObj.getClaimStatus = function() {   
    	 return JSON.parse(sessionStorage.getItem('claimStatus'));
      	//return claimStatus;
      };
      factObj.setClaimStatus = function(data){
    	  claimStatus = data;
    	  if (data.toUpperCase() === "Open".toUpperCase() || data.toUpperCase() === "Admitted".toUpperCase()
    			  || data.toUpperCase() === "Pending".toUpperCase() || data.toUpperCase() === "Re-open".toUpperCase() || 
    			  data.toUpperCase() === "Reopened".toUpperCase() || data.toUpperCase() === "Reopen".toUpperCase()  || data.toUpperCase() === "New".toUpperCase()) {
    		  claimStatus = 'Under Assessment'; 
    	  } else if(data.toUpperCase() === "Closed".toUpperCase() || data.toUpperCase() === "Paid".toUpperCase()
    			  || data.toUpperCase() === "Declined".toUpperCase()) {
    		  claimStatus = 'Decision Complete';
    	  }else if(data.toUpperCase() === "Not Proceeded With".toUpperCase() || data.toUpperCase() === "NPW".toUpperCase()) {
    		  claimStatus = 'Inactive'; 
    	  }
    	  sessionStorage.setItem('claimStatus',JSON.stringify(claimStatus));
         // return claimStatus;
      }; 
      
      factObj.setUserIp =function(data){
    	  sessionStorage.setItem('ipAddr',JSON.stringify(data));
    	  ipAddr = data;
      };
      
      factObj.setLoginAttempts =function(data){
    	  sessionStorage.setItem('loginAttempts',JSON.stringify(data));
    	  loginAttempts = data;
      };
      factObj.getLoginAttempts =function(){
    	  if (loginAttempts == undefined) {
    		  loginAttempts = JSON.parse(sessionStorage.getItem('loginAttempts'));
    	  }
    	  return loginAttempts;
      };
      factObj.getUserIp = function () {  
//    	  $.get("https://ipinfo.io", function(response) {
//    		  ipAddr = response.ip;
//  			$('input[name="azbyxc"]').val(response.ip);
//  			alert($('input[name="azbyxc"]').val());
//  	    }, "jsonp");
    	  if (ipAddr == undefined) {
    		  ipAddr = JSON.parse(sessionStorage.getItem('ipAddr'));
    	  }
    	  return ipAddr;
     };
     
		factObj.retrieveLoginAttempts = function (ipAddress) { 			
			 var loginAttempts = { ipAddress: ipAddress};
					     	
		    	 //data = JSON.parse(data);
		       return $http.post(this.getLabelProperties().retriveIpAddr,loginAttempts);
			
			
		};
		
		factObj.updateFunds = function (fund) { 			
			
					     	
		    	 //data = JSON.parse(data);
		       return $http.post( updateFunds +fund);
			
			
		};
		
		factObj.retrieveFunds = function () { 			
			
					     	
		    	 //data = JSON.parse(data);
		       return $http.post(retrieveFunds);
			
			
		};
		
		factObj.manageLoginAttempts = function (ipAddress,attempts) { 
			var ipLocked = 'N';
			var maxAttempts;
			attempts = parseInt(attempts);
			maxAttempts = this.getLabelProperties().maxAttempts;			
			//alert(attempts);
			attempts++;
			//alert('attempts'+(attempts == 5));
			if (attempts == parseInt(maxAttempts)) {
				ipLocked = 'Y';
			}
			
			 var loginAttempts = { ipAddress: ipAddress,
					 attempts:attempts,
					 ipLocked:ipLocked};
					     	
		    	 //data = JSON.parse(data);
		       return $http.post(this.getLabelProperties().mangeLoginAttempts,loginAttempts,headerConfig);
			
			
		};
		
		factObj.manageHistory = function (status,activityName) {
			var historyresponse;			
			 var historyAttempts = { claimNum:this.getClaimId(),
					 fundName:this.getPartnerName(),
					 status:status,
					 activityName:activityName};
					     	
		    	 //data = JSON.parse(data);
			 historyresponse = $http.post(this.getLabelProperties().mangeCTHistory,historyAttempts,headerConfig);
			 if (historyresponse != undefined) { 
				 historyresponse.success(function(data, status, headers, config) {       				 
      		    }),
      		  historyresponse.error(function(data, status, headers, config) {		       				 
      		    })
			 }
			
		};
      
      factObj.getPartnerName = function() {    	
       	return JSON.parse(sessionStorage.getItem('partnerName'))
       };
       factObj.setPartnerName = function(data){
    	   partnerName = data;
    	   sessionStorage.setItem('partnerName',JSON.stringify(partnerName));
          // return partnerName;
       };
       factObj.getUniqueId = function(){    	
           return uniqueId;
       };
       
       factObj.setUniqueId = function(data){
    	   uniqueId = data;
           return uniqueId;
       };       
       
       factObj.getDisableContcHdr = function(){
    	   if (JSON.parse(sessionStorage.getItem('disableContcHdr')) == null) {
    		   return disableContcHdr;
    	   } else {
    		   return JSON.parse(sessionStorage.getItem('disableContcHdr'));
    	   }
           //return disableContcHdr;
       };
       
       factObj.setDisableContcHdr = function(data){    	   
    	   disableContcHdr = data;
    	   sessionStorage.setItem('disableContcHdr',JSON.stringify(disableContcHdr));
          // return disableContcHdr;
       };
    
    return factObj;
}]);

claimApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file,partnerName,newPartner,filepath){
    	//var uploadUrl = "http://localhost:8087/singleSave";
    	var uploadUrl  = "https://www.e2e.eapplication.metlife.com.au/ebusiness/singleSave";
        var fd = new FormData();
        fd.append('file', file);
        fd.append('partnerName', partnerName);
        fd.append('newPartner', newPartner);
        fd.append('filepath', filepath);
//    	 var data = { file: file,
//    			 partnerName: partnerName,
//    			 newPartner:newPartner,
//    			 filepath:filepath}; 
        return $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
       
    }
}]);

//claimApp.factory('propertyService',['$http','$q', function($http,$q){
//	var configObject = {};
//	configObject.getProperties = function(){
//		var defer = $q.defer();		
//		$http.get('./claimTracker_labels.properties').then(function (response) {
//	       	var configData = response.data;
//	       	configData = configData.split('\n');
//	       	var arr = [];
//	       	var key;
//	       	var newArr = [];       	
//	       	
//	       	//var obj = {};
//	       	for(var i = 0; i < configData.length; i++){
//	       		var temp = configData[i].split('=');
//	       		newArr = newArr.concat(temp);
//	       	}
//	       	for(var i = 0; i < newArr.length; i++){
//	       		if(i % 2 == 0){
//	       			key = newArr[i];
//	       		} else if(i % 2 == 1){
//	       			var value = newArr[i];
//	       			configObject[key] = value;
//	       		}
//	       	}
//	       	defer.resolve(response);
//	     }, function(err){
//	    	 console.log("Error fetching properties " + JSON.stringify(err));
//	    	 defer.reject(response);
//	     });
//		return defer.promise;		
//	}
//	return configObject;
//}]);

claimApp.factory('propertyService',['$http','$q','$location', function($http,$q,$location){
	var propFileName= './ClaimTrackerProp.properties';
	 var headerConfig = {
	            headers : {
	                'Content-Type': 'application/json',
	                //'Access-Control-Allow-Origin':'http://ausydetkd03:8084'
	            }
	        }  
	// alert($location.host());
	 if ($location.host() == 'www.eapplication.metlife.com.au'){
		 propFileName = './ClaimTrackerProp_prod.properties';
	 } else if ($location.host() == 'www.e2e.eapplication.metlife.com.au'){
		 propFileName = './ClaimTrackerProp_e2e.properties';
	 }
	    
	var configObject = {};
	configObject.getProperties = function(){
		var defer = $q.defer();	
	  $http.get(propFileName).then(function (response) {
		  configObject = response.data;
		  defer.resolve(response);
		 // return response.data;
	  },function(err){
	    	 console.log("Error fetching properties " + JSON.stringify(err));
	    	 defer.reject(response);
	 });
	  return defer.promise;
	} 
	return configObject; 
}]);
claimApp.factory('ipAddressService',['$http','$q', function($http,$q){
	 var headerConfig = {
	            headers : {
	                'Content-Type': 'text/html',	             
	                'withCredentials':false
	            }
	        }  
	    
	var configObject = {};
	configObject.getIpAddress = function(){
		var defer = $q.defer();	
	  $http.get('http://ipinfo.io',headerConfig).then(function (response) {
		  configObject = response.ip;
		  defer.resolve(response);
		 // return response.data;
	  },function(err){
	    	 console.log("Error fetching properties " + JSON.stringify(err));
	    	 defer.reject(response);
	 });
	  return defer.promise;
	} 
	return configObject; 
}]);
claimApp.factory('SessionTimeoutService', function($rootScope, $location,claimFactory){
	var timeoutID;
	function startTimer() {
	    timeoutID = window.setTimeout(goInactive, 600000);
	}
	 
	function resetTimer(e) {
	    window.clearTimeout(timeoutID);
	    goActive();
	}
	 
	function goInactive() {
	  claimFactory.setDisableContcHdr(true);
   	 claimFactory.manageHistory('logout is succesful','logout'); 
   	$location.path("/ct/"+claimFactory.getPartnerName());
   	 sessionStorage.clear();		
        //$scope.$apply();
		$rootScope.$apply();
	}
	 
	function goActive() {   
	    startTimer();
	}
	return {
		setup: function(){
			window.addEventListener("mousemove", resetTimer, false);
			window.addEventListener("click", resetTimer, false);
			window.addEventListener("mouseup", resetTimer, false);
			window.addEventListener("mousedown", resetTimer, false);
			window.addEventListener("keydown", resetTimer, false);
			window.addEventListener("keypress", resetTimer, false);
			window.addEventListener("keyup", resetTimer, false);
			window.addEventListener("submit", resetTimer, false);
			window.addEventListener("change", resetTimer, false);
			window.addEventListener("mouseenter", resetTimer, false);
			window.addEventListener("scroll", resetTimer, false);
			window.addEventListener("resize", resetTimer, false);
			window.addEventListener("dblclick", resetTimer, false);
		    startTimer();
		},
		stopWatch: function(){
			window.clearTimeout(timeoutID);
			//$rootScope.$apply();
		},
		removeListeners: function(){
			window.removeEventListener("mousemove", resetTimer, false);
			window.removeEventListener("click", resetTimer, false);
			window.removeEventListener("mouseup", resetTimer, false);
			window.removeEventListener("mousedown", resetTimer, false);
			window.removeEventListener("keydown", resetTimer, false);
			window.removeEventListener("keypress", resetTimer, false);
			window.removeEventListener("keyup", resetTimer, false);
			window.removeEventListener("submit", resetTimer, false);
			window.removeEventListener("change", resetTimer, false);
			window.removeEventListener("mouseenter", resetTimer, false);
			window.removeEventListener("scroll", resetTimer, false);
			window.removeEventListener("resize", resetTimer, false);
			window.removeEventListener("dblclick", resetTimer, false);
			window.clearTimeout(timeoutID);
		}
	};
});
