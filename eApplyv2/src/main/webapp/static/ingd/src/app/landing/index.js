import angular from 'angular';

// Create the module where our functionality can attach to
let landingModule = angular.module('app.landing', []);

// Include our UI-Router config settings
import LandingConfig from './landing.config';
landingModule.config(LandingConfig);


// Controllers
import LandingCtrl from './landing.controller';
landingModule.controller('LandingCtrl', LandingCtrl);


export default landingModule;
