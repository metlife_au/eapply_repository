package au.com.metlife.eapply.bs.controller;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.metlife.eapply.bs.model.RefOccmapping;
import au.com.metlife.eapply.bs.service.RefOccmappingService;

@RestController
@Component
/*@PropertySource(value = "application.properties")*/
public class RefOccmappingController {
	
	
	private final RefOccmappingService refOccmappingService;
	
	 @Autowired
	    public RefOccmappingController(final RefOccmappingService refOccmappingService) {
	        this.refOccmappingService = refOccmappingService;
	    }
	 
	 @RequestMapping("/getNewOccupationList")
	  public ResponseEntity<List<RefOccmapping>> getNewOccupationList(@RequestParam(value="fundId") String fundId ,@RequestParam(value="occName") String occName) {		
		System.out.println(">>>fundId"+fundId+">>>>indcc--"+occName);
		List<RefOccmapping> refOccmapping =null;
		if(fundId!=null && occName!=null){
			refOccmapping =  refOccmappingService.getNewOccupationName(fundId, occName);
		}
		
	    if(refOccmapping==null){
	        return new ResponseEntity<List<RefOccmapping>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<RefOccmapping>>(refOccmapping, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/getUniqueNumber")
	  public ResponseEntity<String> getUniqueNumber() {		
		Calendar cal = Calendar.getInstance(); 
		System.out.println("inside app gen>>");
	    return new ResponseEntity<String>(cal.getTimeInMillis()+"", HttpStatus.OK);
		 
	 }
	 
	 

}
