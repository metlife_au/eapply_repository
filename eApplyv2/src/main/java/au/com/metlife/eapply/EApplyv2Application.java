package au.com.metlife.eapply;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.constants.EapplicationConstants;
import au.com.metlife.eapply.email.MetLifeEmailService;
import au.com.metlife.eapply.email.MetLifeEmailVO;
import au.com.metlife.eapply.utility.AESencrp;
import au.com.metlife.eapply.utility.EapplyHelper;
import au.com.metlife.eapply.utility.RSAUtil;
import au.com.metlife.eapply.utility.Response;
import au.com.metlife.eapply.utility.Status;
import au.com.metlife.eapply.utility.XSDHelper;

@Controller
@SpringBootApplication
@EnableAutoConfiguration
public class EApplyv2Application extends SpringBootServletInitializer implements EapplicationConstants{
	private static final Logger log = LoggerFactory.getLogger(EApplyv2Application.class);
	 
	
	@Value("${spring.b2bbaseurl}")
	private  String b2bbaseurl;	
	
	@Value("${spring.xsdpath}")
	private  String xsdPath;
	
	
	@Value("${spring.memberServiceUrl}")
	private  String memberServiceUrl;
	
	@Value("${spring.servicecontext}")
	private String domainUrl;
	@Value("${spring.domainame}")
	private String domainName;
	
	
	@Value("${spring.ssologinurl}")
	private String ssologinurl;
	
	@Value("${spring.ssologouturl}")
	private String ssologouturl;
	
		
	@Value("${spring.vicshomeurl}")
	private String vicshomeurl;
	
	public String getMemberServiceUrl() {
		return memberServiceUrl;
	}

	public void setMemberServiceUrl(String memberServiceUrl) {
		this.memberServiceUrl = memberServiceUrl;
	}

	public String getXsdPath() {
		return xsdPath;
	}

	public void setXsdPath(String xsdPath) {
		this.xsdPath = xsdPath;
	}

	public String getB2bbaseurl() {
		return b2bbaseurl;
	}

	public void setB2bbaseurl(String b2bbaseurl) {
		this.b2bbaseurl = b2bbaseurl;
	}

	public String getDomainUrl() {
		return domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@RequestMapping("/claims")
    public String claims(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        return "main";
    }
	
	@RequestMapping("/index1")
    public String index(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        return "index1";
    }
	
	@RequestMapping("/admin")
    public String uploadFile(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        return "upload";
    }
	
	@RequestMapping("/claimstracker/mtaa")
    public void claimsRedirect(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
        model.addAttribute("name", name);
        String url= "/eapply/claims#/ct/MTAA";
        response.sendRedirect(url);
        //return "main";
    }

	@RequestMapping("/claimstracker/CareSuper")
    public void claimsRedirectCare(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
        model.addAttribute("name", name);
        String url= "/eapply/claims#/ct/CareSuper";
        response.sendRedirect(url);
        //return "main";
    }


	@RequestMapping("/hello")
    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        return "eapplymain";
    }
	@RequestMapping("/care")
    public String careSuper(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside Care>>");
        return "careSuperStub";
    }
	@RequestMapping("/firstSuper")
    public String firstSuper(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside firstsuper>>");
        return "firstSuperStub";
    }
	@RequestMapping("/statewide")
    public String stateWide(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside statewide>>");
        return "stateWideStub";
    }
	
	@RequestMapping("/host")
    public String hostPlus(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside host>>");
        return "hostStub";
    }
	
	//@RequestMapping("/ticv")
	@RequestMapping("/vict")
    public String vicsuper(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside vics>>");
        return "vicSuperStub";
    }
	
	@RequestMapping("/sessionexpired")
    public String sessionExpiry(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name,
    		HttpServletResponse response) {
        model.addAttribute("name", name);
        log.info("inside host>>");
        response.addHeader("vicsHomeUrl", getVicshomeurl());
        return "sessionexpired";
    }
	
	@RequestMapping("/landing")
    public String landing(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside vics>>");
        return "fromsubmit";
    }
	
	@RequestMapping("/ingd")
    public String ingd(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside ingd>>");
        return "ingdStub";
    }

	@RequestMapping("/aeis")
    public String aeis(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside AEIS module>>");
        log.info("inside host>>"+name);
        return "aeisStub";
    } 
    @RequestMapping("/hesta")
    public String hestaSuper(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside11>>");
        return "hestaSuperStub";
    }
    @RequestMapping("/corp")
    public String corporate(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside11>>");
        return "corporateLogin";
    }
    @RequestMapping("/mtaa")
    public String mtaaSuper(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside Care>>");
        return "mtaaStub";
    }	
    @RequestMapping("/guil")
    public String guildSuper(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        log.info("inside Care>>");
        return "guildStub";
    }
    @RequestMapping(value = "/EapplyServices", method = RequestMethod.POST)
	public  String generateHostResponse(HttpServletRequest request,@RequestBody String HDInputString, @RequestBody String fund_id) {
    	System.out.println("HDInputString>>"+HDInputString);
    	System.out.println("fund_id>>"+fund_id);
    	String validXML = null;
    	String secureId = null;
    	XSDHelper xsdHelper = new XSDHelper();
    	Status status = new Status();
    	Response response = new Response();
    	try {
	    	if(HDInputString!=null){
	    		if(HDInputString.contains(DAYS30)){			
	    			HDInputString = HDInputString.replace(DAYS30, THIRTY_DAYS);
				}else if(HDInputString.contains(DAYS45)){
					HDInputString = HDInputString.replace(DAYS45, FOURTYFIVE_DAYS);
				}else if(HDInputString.contains(DAYS60)){
					HDInputString = HDInputString.replace(DAYS60, SIXTY_DAYS);
				}else if(HDInputString.contains(DAYS90)){
					HDInputString = HDInputString.replace(DAYS90, NINETY_DAYS);
				}
				if(HDInputString.contains(YEARS2)){
					HDInputString = HDInputString.replace(YEARS2, TWO_YEARS);
				}else if(HDInputString.contains(YEARS5)){
					HDInputString = HDInputString.replace(YEARS5, FIVE_YEARS);
				}else if(HDInputString.contains(AGE65)){
					HDInputString = HDInputString.replace(AGE65, AGE_SIXTYFIVE);
				}else if(HDInputString.contains(AGE67)){
					HDInputString = HDInputString.replace(AGE67, AGE_SIXTYSEVEN);
				}				
				validXML= xsdHelper.validateXMLString(HDInputString, getXsdPath()+fund_id+"_WebService_schema_new.xsd");
				if("valid".equalsIgnoreCase(validXML)){
					secureId = EapplyHelper.callB2bService(HDInputString,getB2bbaseurl());
					if(secureId==null){
					 	status.setCode("400");
			            status.setDescription("The input object contains missing or malformed elements, or the server is not able to process the request.");
			            response.setStatus(status);
					}else{
						status.setSecureID(fund_id.toLowerCase()+secureId);	              	
						status.setCode("200");
		        		status.setAppType("newBusiness");
		        		response.setStatus(status);
		        		
					}					
					
				}else{
					status.setCode("400");
		            status.setDescription("The input object contains missing or malformed elements, or the server is not able to process the request.");
		            response.setStatus(status);
				}
	    	}else{
	    		status.setCode("400");
	            status.setDescription("The input object contains missing or malformed elements, or the server is not able to process the request.");
	            response.setStatus(status);
	    	}
	    	secureId= xsdHelper.getResponseXML(response);
    	} catch (Exception e) {
			// TODO Auto-generated catch block
    		e.printStackTrace();
    		status.setCode("400");
            status.setDescription("The input object contains missing or malformed elements, or the server is not able to process the request.");
            response.setStatus(status);
		}
    	return secureId;
    }
    
    
    @RequestMapping(value = "/aeisb2bservice", method = RequestMethod.POST,consumes = { "application/xml;charset=UTF-8", "text/xml;charset=UTF-8","application/x-www-form-urlencoded" })
   	public  ResponseEntity <String> generateAeisServiceToken(HttpServletRequest request,@RequestBody String inputString) {
   		String secureId = null;
   		String validXML = null;
   		XSDHelper xsdHelper = new XSDHelper();
   		String aesKey = null;	
   		String fundid = null;
   		System.out.println("inputString aeis>>"+inputString);	
   		inputString = inputString.substring(inputString.indexOf("<request>"));
   		System.out.println("inputString after whitespace removal11>>"+inputString);	
   		try {
   			/*Specific to AEIS*/
  			SecureRandom sRandom = new SecureRandom();
			aesKey=  new BigInteger(80, sRandom).toString(32);
			inputString = AESencrp.encrypt(inputString, aesKey);   			
			String encryptedAesToken = RSAUtil.encryptData(aesKey,"AEIS");		
   			String authHeader = "Basic "+encryptedAesToken;//request.getHeader(AUTHORIZATION);
   			System.out.println("authHeader>>"+authHeader);	
   			//Specific to AEIS ends
   			
   			fundid = request.getHeader("fundid");
   			System.out.println("fundid>>"+fundid);
   			//decrypt key
   			aesKey = RSAUtil.decryptData(authHeader.substring(6),fundid);
   			System.out.println("aesKey>>"+aesKey);
   			/*decrypy string	*/
   			inputString = AESencrp.decrypt(inputString, aesKey);
   			//System.out.println("decrypt inputString>>"+inputString);	
   			fundid = request.getHeader("fundid");
   			System.out.println("fundid>>"+fundid);
   			if("HOST".equalsIgnoreCase(fundid)){
   				/*String age = null;*/			
   					
   				String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
   				
   				StringBuffer daa = new StringBuffer();
   				daa.append("<ageCover>");
   				daa.append(age);
   				daa.append("</ageCover>");
   				
   				inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
   			}
   			if(inputString.contains(DAYS30)){			
   				inputString = inputString.replace(DAYS30, THIRTY_DAYS);
   			}else if(inputString.contains(DAYS45)){
   				inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
   			}else if(inputString.contains(DAYS60)){
   				inputString = inputString.replace(DAYS60, SIXTY_DAYS);
   			}else if(inputString.contains(DAYS90)){
   				inputString = inputString.replace(DAYS90, NINETY_DAYS);
   			}
   			if(inputString.contains(YEARS2)){
   				inputString = inputString.replace(YEARS2, TWO_YEARS);
   			}else if(inputString.contains(YEARS5)){
   				inputString = inputString.replace(YEARS5, FIVE_YEARS);
   			}else if(inputString.contains(AGE65)){
   				inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
   			}else if(inputString.contains(AGE67)){
   				inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
   			}
   			System.out.println(INPUT_STRING_INFO+inputString);
   			if(inputString!=null){				
   				validXML= xsdHelper.validateXMLString(inputString, getXsdPath()+fundid+"_WebService_schema_new.xsd");
   				System.out.println("validXML>>"+validXML);
   				if("valid".equalsIgnoreCase(validXML)){
   					secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
   					if(secureId==null){
   						return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
   					}
   				}else{
   					return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
   				}
   			}else{				
   				return new ResponseEntity<String>("EAPP0002",HttpStatus.FORBIDDEN);
   			}			
   			
   		} catch (Exception e) {
   			log.error("Error in call b2b service"+e);
   			return new ResponseEntity<String>("EAPP0003",HttpStatus.UNAUTHORIZED);
   		}
   		
   		return new ResponseEntity<String>(secureId, HttpStatus.OK);
   	}
    
    
    @RequestMapping(value = "/integrationb2bservice", method = RequestMethod.POST)
	public  ResponseEntity <String> integrationServiceToken(HttpServletRequest request,@RequestBody String inputString) {
		String secureId = null;
		String validXML = null;
		XSDHelper xsdHelper = new XSDHelper();
		String aesKey = null;	
		String fundid = null;
		System.out.println("inputString>>"+inputString);	
		inputString= inputString.substring(1);
		System.out.println("inputString after substring>>"+inputString);	
		try {
			String authHeader = request.getHeader(AUTHORIZATION);
			System.out.println("authHeader>>"+authHeader);	
			fundid = request.getHeader("fundid");	
			System.out.println("fundid>>"+fundid);
			/*decrypt key*/
			aesKey = RSAUtil.decryptData(authHeader.substring(6), fundid);
			System.out.println("aesKey>>"+aesKey);
			/*decrypy string	*/
			//inputString = AESencrp.decrypt(inputString, aesKey);
			inputString =AESencrp.aeisdecrypt(inputString, aesKey);
			System.out.println("raw inputString>>"+inputString);
			if("HOST".equalsIgnoreCase(fundid)){
				/*String age = null;*/			
					
				String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
				
				StringBuffer daa = new StringBuffer();
				daa.append("<ageCover>");
				daa.append(age);
				daa.append("</ageCover>");
				
				inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
			}
			
			if(inputString.contains(DAYS30)){			
				inputString = inputString.replace(DAYS30, THIRTY_DAYS);
			}else if(inputString.contains(DAYS45)){
				inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
			}else if(inputString.contains(DAYS60)){
				inputString = inputString.replace(DAYS60, SIXTY_DAYS);
			}else if(inputString.contains(DAYS90)){
				inputString = inputString.replace(DAYS90, NINETY_DAYS);
			}
			if(inputString.contains(YEARS2)){
				inputString = inputString.replace(YEARS2, TWO_YEARS);
			}else if(inputString.contains(YEARS5)){
				inputString = inputString.replace(YEARS5, FIVE_YEARS);
			}else if(inputString.contains(AGE65)){
				inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
			}else if(inputString.contains(AGE67)){
				inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
			}
			log.info(INPUT_STRING_INFO+inputString);
			if(inputString!=null){				
				validXML= xsdHelper.validateXMLString(inputString, getXsdPath()+fundid+"_WebService_schema_new.xsd");
				log.info("validXML>>"+validXML);
				if("valid".equalsIgnoreCase(validXML)){
					secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
					if(secureId==null){
						return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
					}
				}else{
					return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
				}
			}else{				
				return new ResponseEntity<String>("EAPP0002",HttpStatus.FORBIDDEN);
			}			
			
		} catch (Exception e) {
			log.error("Error in call b2b service"+e);
			return new ResponseEntity<String>("EAPP0003",HttpStatus.UNAUTHORIZED);
		}
		
		return new ResponseEntity<String>(secureId, HttpStatus.OK);
	}
    
    
    @RequestMapping(value = "/genericb2bservice", method = RequestMethod.POST)
	public  ResponseEntity <String> generateServiceToken(HttpServletRequest request,@RequestBody String inputString) {
		String secureId = null;
		String validXML = null;
		XSDHelper xsdHelper = new XSDHelper();
		String aesKey = null;	
		String fundid = null;
		System.out.println("incoming inputString>>"+inputString);		
		try {
			String authHeader = request.getHeader(AUTHORIZATION);
			System.out.println("authHeader>>"+authHeader);	
			fundid = request.getHeader("fundid");		
			System.out.println("fundid>>"+fundid);
			/*decrypt key*/
			aesKey = RSAUtil.decryptData(authHeader.substring(6),fundid);
			System.out.println("aesKey>>"+aesKey);
			/*decrypy string	*/
			inputString = AESencrp.decrypt(inputString, aesKey);
			System.out.println("inputString after decrypt>>"+inputString);
			if("HOST".equalsIgnoreCase(fundid)){
				/*String age = null;*/			
					
				String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
				
				StringBuffer daa = new StringBuffer();
				daa.append("<ageCover>");
				daa.append(age);
				daa.append("</ageCover>");
				
				inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
			}
			
			if(inputString.contains(DAYS30)){			
				inputString = inputString.replace(DAYS30, THIRTY_DAYS);
			}else if(inputString.contains(DAYS45)){
				inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
			}else if(inputString.contains(DAYS60)){
				inputString = inputString.replace(DAYS60, SIXTY_DAYS);
			}else if(inputString.contains(DAYS90)){
				inputString = inputString.replace(DAYS90, NINETY_DAYS);
			}
			if(inputString.contains(YEARS2)){
				inputString = inputString.replace(YEARS2, TWO_YEARS);
			}else if(inputString.contains(YEARS5)){
				inputString = inputString.replace(YEARS5, FIVE_YEARS);
			}else if(inputString.contains(AGE65)){
				inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
			}else if(inputString.contains(AGE67)){
				inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
			}
			System.out.println(INPUT_STRING_INFO+inputString);
			if(inputString!=null){				
				validXML= xsdHelper.validateXMLString(inputString, getXsdPath()+fundid+"_WebService_schema_new.xsd");
				System.out.println("validXML>>"+validXML);
				if("valid".equalsIgnoreCase(validXML)){
					secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
					if(secureId==null){
						return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
					}
				}else{
					return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
				}
			}else{				
				return new ResponseEntity<String>("EAPP0002",HttpStatus.FORBIDDEN);
			}			
			
		} catch (Exception e) {
			log.error("Error in call b2b service"+e);
			return new ResponseEntity<String>("EAPP0003",HttpStatus.UNAUTHORIZED);
		}
		
		return new ResponseEntity<String>(secureId, HttpStatus.OK);
	}
    
	@RequestMapping(value = "/b2bservice", method = RequestMethod.POST)
	public  ResponseEntity <String> callB2b(HttpServletRequest request,@RequestBody String inputString) {
		String secureId = null;
		String validXML = null;
		XSDHelper xsdHelper = new XSDHelper();
		String aesKey = null;	
		String fundid = null;
		try {
			String authHeader = request.getHeader(AUTHORIZATION);
			log.info("authHeader>>"+authHeader);	
			fundid = request.getHeader("fundid");
			if(fundid==null){
				fundid= "CARE";
			}
			/*decrypt key*/
			aesKey = RSAUtil.decryptData(authHeader.substring(6),fundid);
			log.info("aesKey>>"+aesKey);
			/*decrypy string	*/
			inputString = AESencrp.decrypt(inputString, aesKey);
			if(inputString.contains(DAYS30)){			
				inputString = inputString.replace(DAYS30, THIRTY_DAYS);
			}else if(inputString.contains(DAYS45)){
				inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
			}else if(inputString.contains(DAYS60)){
				inputString = inputString.replace(DAYS60, SIXTY_DAYS);
			}else if(inputString.contains(DAYS90)){
				inputString = inputString.replace(DAYS90, NINETY_DAYS);
			}
			if(inputString.contains(YEARS2)){
				inputString = inputString.replace(YEARS2, TWO_YEARS);
			}else if(inputString.contains(YEARS5)){
				inputString = inputString.replace(YEARS5, FIVE_YEARS);
			}else if(inputString.contains(AGE65)){
				inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
			}else if(inputString.contains(AGE67)){
				inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
			}
			log.info(INPUT_STRING_INFO+inputString);
			if(inputString!=null){
				if(inputString.contains("CARE")){
					fundid = "CARE";
				}else if(inputString.contains("SFPS")){
					fundid = "SFPS";
				}else if(inputString.contains("FIRS")){
					fundid = "FIRS";
				}else if(inputString.contains("HOST")){
					fundid = "HOST";
				}else if(inputString.contains("AEIS")){
					fundid = "AEIS";
				}
				validXML= xsdHelper.validateXMLString(inputString, getXsdPath()+fundid+"_WebService_schema_new.xsd");
				log.info("validXML>>"+validXML);
				if("valid".equalsIgnoreCase(validXML)){
					secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
					if(secureId==null){
						return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
					}
				}else{
					return new ResponseEntity<String>("EAPP0001",HttpStatus.BAD_REQUEST);
				}
			}else{				
				return new ResponseEntity<String>("EAPP0002",HttpStatus.FORBIDDEN);
			}			
			
		} catch (Exception e) {
			log.error("Error in call b2b service"+e);
			return new ResponseEntity<String>("EAPP0003",HttpStatus.UNAUTHORIZED);
		}
		
		return new ResponseEntity<String>(secureId, HttpStatus.OK);
	}

	@RequestMapping("/CorpWebStub")
    public String careSuperWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        /*Commented for Time being since this Variable is not being used in the current Method*/
        /*String fundId =  request.getParameter(FUND_ID);*/
       
          
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
		
        
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating response for stub..... finish"+url);
        return "careSuperStubResp";
        
    }
	@RequestMapping("/hestWebStub")
    public String hestaSuperWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating response for stub..... finish"+url);
          return "hestaSuperStubResp";
    }
	
	@RequestMapping("/firsWebStub")
    public String firstSuperWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating firs response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating firs response for stub..... finish "+url);
        return "firstSuperStubResp";
        
    }
	
	@RequestMapping("/sfpsWebStub")
    public String statewideSuperWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating sfps response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating sfps response for stub..... finish "+url);
        return "stateWideStubResp"; 
        
    }
	
	@RequestMapping("/hostWebStub")
    public String hostPlusWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating host response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;        
   
        String fundId =  request.getParameter(FUND_ID);
          
        if("HOST".equalsIgnoreCase(fundId)){
			/*String age = null;*/			
				
			String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
			
			StringBuffer daa = new StringBuffer();
			daa.append("<ageCover>");
			daa.append(age);
			daa.append("</ageCover>");
			
			inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
		}
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
        
        
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating host response for stub..... finish "+url);
        return "hostStubResp"; 
        
    }
	
	@RequestMapping("/ingdWebStub")
    public String ingdWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating host response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;        
   
        String fundId =  request.getParameter(FUND_ID);
          
        if("INGD".equalsIgnoreCase(fundId)){
			/*String age = null;*/			
				
			String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
			
			StringBuffer daa = new StringBuffer();
			daa.append("<ageCover>");
			daa.append(age);
			daa.append("</ageCover>");
			
			inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
		}
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
        
        
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating ingd response for stub..... finish "+url);
        return "ingdStubResp"; 
        
    }
	
	@RequestMapping("/vicsuperWebStub")
    public String vicsuperWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating host response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;        
   
        String fundId =  request.getParameter(FUND_ID);
          
        if("VICT".equalsIgnoreCase(fundId)){
			/*String age = null;*/			
				
			String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
			
			StringBuffer daa = new StringBuffer();
			daa.append("<ageCover>");
			daa.append(age);
			daa.append("</ageCover>");
			
			inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
		}
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
        
        
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating host response for stub..... finish "+url);
        return "vicSuperStubResp"; 
        
    }
	
	
	@RequestMapping("/manageinsurance")
    public void manageinsurance(HttpServletRequest request,HttpServletResponse response) throws IOException {
		 HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            session = request.getSession();
	        }
        response.sendRedirect(ssologinurl);
        //return "main";
    }
	
	@RequestMapping(value="/manageinsurance",method = RequestMethod.POST)
    public String postSAMLData(HttpServletRequest request,@RequestParam(value="encryptedString") String encryptedString,@RequestParam(value="authHeader") String authHeader,HttpServletResponse response) throws Exception{
        
		log.info("Generating host response for stub..... start");
		
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        
        
     System.out.println("<authHeader>"+authHeader);
     
     System.out.println("authHeader.substring(6)" + authHeader.substring(6));
        
        String inputString = null;
       
      //decrypt key
			String aesKey = RSAUtil.decryptData(authHeader.substring(6),"VICT");
			
			System.out.println("after getting the aesKey>>>"+aesKey);
			//System.out.println("aesKey>>"+aesKey);
			/*decrypy string	*/
			
			//pucb2cmvd5345uhh
			inputString = AESencrp.decrypt(encryptedString, aesKey);
         System.out.println("inputString >> "+inputString);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        
        
        
        String output = null;
        String url;
        String secureId;   
		URL obj;
		try {
			obj = new URL(getB2bbaseurl() + "getauthToken");
			//aesKey = new String(Base64.getEncoder().encode(("Bearer "+aesKey).getBytes()));	
			//input = input.trim()+"##"+token;
			//log.info("input>>"+input);		
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			
			
			httpCon.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
			httpCon.setRequestProperty(ACCEPT, APPLICATION_JSON);
			//httpCon.setRequestProperty(ACCESS_TOKEN, aesKey);
			//httpCon.setRequestProperty(AUTHORIZATION, token);						
			httpCon.setRequestMethod(POST);
			httpCon.connect();
			OutputStream os = httpCon.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, UTF_EIGHT);
			
			System.out.println("Before making a call to insert >>"+aesKey);
			osw.write(aesKey);
			osw.flush();
			osw.close();
			os.close();
			int responseCode = httpCon.getResponseCode();
			log.info("Response Code <what the fish purna> : " + responseCode);
//			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
//			String inputLine;
//			StringBuffer responseBuff = new StringBuffer();
//
//			while ((inputLine = in.readLine()) != null) {
//				log.info(inputLine);
//				responseBuff.append(inputLine);
//			}
//			output = responseBuff.toString();
//			in.close();
			
			 
			if (responseCode == 400){				
				 response.sendRedirect(getSsologinurl());
				 response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
					response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
					response.setDateHeader("Expires", 0); // Proxies.
			     
			} else {			
				
				String age = ""+EapplyHelper.getAgeNextBirthDay(inputString.substring(inputString.indexOf("<dateOfBirth>")+13, inputString.indexOf("</dateOfBirth>")));		
				
				StringBuffer daa = new StringBuffer();
				daa.append("<ageCover>");
				daa.append(age);
				daa.append("</ageCover>");
				
				inputString = inputString.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
			//}
	        
	        if(inputString.contains(DAYS30)){			
				inputString = inputString.replace(DAYS30, THIRTY_DAYS);
			}else if(inputString.contains(DAYS45)){
				inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
			}else if(inputString.contains(DAYS60)){
				inputString = inputString.replace(DAYS60, SIXTY_DAYS);
			}else if(inputString.contains(DAYS90)){
				inputString = inputString.replace(DAYS90, NINETY_DAYS);
			}
			if(inputString.contains(YEARS2)){
				inputString = inputString.replace(YEARS2, TWO_YEARS);
			}else if(inputString.contains(YEARS5)){
				inputString = inputString.replace(YEARS5, FIVE_YEARS);
			}else if(inputString.contains(AGE65)){
				inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
			}else if(inputString.contains(AGE67)){
				inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
			}
	        
	        
	       //  url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
	        log.info(BASEURL_INFO+getB2bbaseurl());
	        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
	       // EapplyHelper.callB2bDataService(inputString, getB2bbaseurl(), token);
	        log.info(WEBSER_RET_INFO+secureId); 
	        if(session != null){
	        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
	             session.setAttribute(SESSION_SECURE_ID, secureId);
	        }
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	        response.addHeader("ssoLoginUrl", getSsologinurl());
	        response.addHeader("ssoLogoutUrl", getSsologouturl());
	        response.addHeader("vicsHomeUrl", getVicshomeurl());
	        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	    	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	    	response.setDateHeader("Expires", 0); // Proxies.
			}

		} catch (MalformedURLException e) {
			log.error(MALFORMEDURL_ERR_INFO+e);
		} catch (ProtocolException e) {
			log.error(PROTOCOL_ERR_INFO+e);
		} catch (IOException e) {
			log.error(MALFORMED_URL_INFO+e);
		}
        
        
             
   
       // String fundId =  "VICT";
          
       // if("VICT".equalsIgnoreCase(fundId)){
			/*String age = null;*/			
				
			
    	log.info("Generating vics secureId..... finish");
      return "vicSuper"; 
        
    }
	
	
	@RequestMapping("/guildWebStub")
    public String guildPlusWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating guild response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
        
   
        /*String fundId =  request.getParameter(FUND_ID);*/
        log.info(INPUT_STRING_INFO+inputString);  
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
        
        
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating guild response for stub..... finish "+url);
        return "guildStubResp"; 
        
    }	


	@RequestMapping("/premCalcReq")
    public String premCalc(HttpServletRequest request,@RequestParam(value="encryptedString") String encryptedString,HttpServletResponse response) throws Exception{
        
		log.info("Generating host response for stub..... start");
		
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        
        
     
        response.addHeader(INPUTDATA, encryptedString);
        response.addHeader(DOMAINURL, getDomainUrl());
        response.addHeader(DOMAINNAME, getDomainName());
    	log.info("Generating vics secureId..... finish");
      return "premCalc"; 
        
    }
	
	
	@RequestMapping(value = "/apply", method = RequestMethod.POST)
	  public String eApplyCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {	 
			String value = request.getHeader("inputdata");
			log.info("value>>"+value);
	        response.addHeader(INPUTDATA, value);
	      
	      return "index";
	}
	@RequestMapping("/mtaaWebStub")
    public String mtaaPlusWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating aeis response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
        
   
        /*String fundId =  request.getParameter(FUND_ID);*/
        log.info(INPUT_STRING_INFO+inputString);  
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
        
        
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating aeis response for stub..... finish "+url);
        return "mtaaStubResp"; 
        
    }
	@RequestMapping("/aeisWebStub")
    public String aeisPlusWebServiceCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws Exception{
        
		log.info("Generating aeis response for stub..... start");
		model.addAttribute("name", name);     
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        String inputString = request.getParameter(HD_INPUTSTRING);
        String stubRespPageName = request.getParameter(STUB_RESPPAGE_ID);
        log.info(STUB_RESP_PAGENAME_INFO+stubRespPageName);
        log.info(INPUT_STRING_INFO+inputString);
        String url;
        String secureId;
        
   
        /*String fundId =  request.getParameter(FUND_ID);*/
        log.info(INPUT_STRING_INFO+inputString);  
        
        if(inputString.contains(DAYS30)){			
			inputString = inputString.replace(DAYS30, THIRTY_DAYS);
		}else if(inputString.contains(DAYS45)){
			inputString = inputString.replace(DAYS45, FOURTYFIVE_DAYS);
		}else if(inputString.contains(DAYS60)){
			inputString = inputString.replace(DAYS60, SIXTY_DAYS);
		}else if(inputString.contains(DAYS90)){
			inputString = inputString.replace(DAYS90, NINETY_DAYS);
		}
		if(inputString.contains(YEARS2)){
			inputString = inputString.replace(YEARS2, TWO_YEARS);
		}else if(inputString.contains(YEARS5)){
			inputString = inputString.replace(YEARS5, FIVE_YEARS);
		}else if(inputString.contains(AGE65)){
			inputString = inputString.replace(AGE65, AGE_SIXTYFIVE);
		}else if(inputString.contains(AGE67)){
			inputString = inputString.replace(AGE67, AGE_SIXTYSEVEN);
		}
        
        
         url=EAPPLY_CORPWEBSTUB_BASEPATH+stubRespPageName;
        log.info(BASEURL_INFO+getB2bbaseurl());
        secureId = EapplyHelper.callB2bService(inputString,getB2bbaseurl());
        log.info(WEBSER_RET_INFO+secureId); 
        if(session != null){
        	 session.setAttribute(SESSION_WEBSERV_RESP, secureId);
             session.setAttribute(SESSION_SECURE_ID, secureId);
        }
        log.info("Generating aeis response for stub..... finish "+url);
        return "aeisStubResp"; 
        
    }
	
	@RequestMapping(value="/b2bgenericresponse", method = RequestMethod.POST)
	  public String b2bgenericresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        String fundid = request.getParameter("fundid");
	        if("FIRS".equalsIgnoreCase(fundid)){
	        	fundid = "firstSuper";
	        }else if("SFPS".equalsIgnoreCase(fundid)){
	        	fundid = "statewide";
	        }else if("AEIS".equalsIgnoreCase(fundid)){
	        	fundid = "aeis";
	        }else if("MTAA".equalsIgnoreCase(fundid)){
	        	fundid = "mtaa";
	        }else if("GUIL".equalsIgnoreCase(fundid)){
	        	fundid = "guil";
	        }
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating secureId..... finish");
	      return fundid;
	}
	
	@RequestMapping(value="/b2bresponse", method = RequestMethod.POST)
	  public String b2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating secureId..... finish");
	      return "index";
	}
	
	@RequestMapping(value="/firsab2bresponse", method = RequestMethod.POST)
	  public String firsB2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating firs secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating firs secureId..... finish");
	      return FIRST_SUPER;
	}
	
	@RequestMapping(value="/sfpsab2bresponse", method = RequestMethod.POST)
	  public String sfpsB2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating sfps secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating sfps secureId..... finish");
	      return "statewide";
	}
	
	@RequestMapping(value="/hostab2bresponse", method = RequestMethod.POST)
	  public String hostB2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating host secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating host secureId..... finish");
	      return "host";
	}
	
	@RequestMapping(value="/ingdab2bresponse", method = RequestMethod.POST)
	  public String ingdB2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating host secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating ingd secureId..... finish");
	      return "ingd";
	}
	
	@RequestMapping(value="/vicsuperb2bresponse", method = RequestMethod.POST)
	  public String vicsuperb2bresponse(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating host secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	        response.addHeader("vicsHomeUrl", getVicshomeurl());
	        response.addHeader("ssoLoginUrl", getSsologinurl());
	        response.addHeader("ssoLogoutUrl", getSsologouturl());
	        response.addHeader("Cache-Control", "no-cache");
	        response.addHeader("Pragma", "no-cache");
	        response.addHeader("Expires", "0");
	    	log.info("Generating host secureId..... finish");
	      return "vicSuper";
	}
	
	@RequestMapping(value="/hostab2bredirectione", method = RequestMethod.GET)
	  public String hostB2bredirection(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating host secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        String fundid = request.getParameter("fundid");
	        log.info(SECURE_ID_INFO+secureId); 	
	        secureId=  EapplyHelper.updateb2bhoststring(getB2bbaseurl(), secureId,fundid);
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating host secureId..... finish");
	      return "host";
	}
	
	@RequestMapping(value="/sfpsb2bredirection", method = RequestMethod.GET)
	  public String sfpsB2bredirection(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating host secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        String fundid = request.getParameter("fundid");
	        log.info(SECURE_ID_INFO+secureId); 	
	        secureId=  EapplyHelper.updateb2bhoststring(getB2bbaseurl(), secureId,fundid);
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating host secureId..... finish");
	      return "statewide";
	}

	@RequestMapping(value="/ingdab2bredirectione", method = RequestMethod.GET)
	  public String ingdB2bredirection(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating ingd secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        String fundid = request.getParameter("fundid");
	        log.info(SECURE_ID_INFO+secureId); 	
	        secureId=  EapplyHelper.updateb2bhoststring(getB2bbaseurl(), secureId,fundid);
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating ingd secureId..... finish");
	      return "ingd";
	}
	
	@RequestMapping(value="/aeisab2bresponse", method = RequestMethod.POST)
	  public String aeisB2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating aeis secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating aeis secureId..... finish");
	      return "aeis";
	}

	@RequestMapping(value="/hestab2bresponse", method = RequestMethod.POST)
	  public String hestaB2bresponseCall(HttpServletRequest request,HttpServletResponse response,Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) throws IOException {
		log.info("Generating secureId..... start");
		HttpSession session = request.getSession();
	        if (session != null && !session.isNew()) {
	            session.invalidate();
	            /*session = request.getSession();*/
	        }
	        String secureId = request.getParameter(SESSION_SECURE_ID);
	        log.info(SECURE_ID_INFO+secureId); 	
	    
	        response.addHeader(INPUTDATA, secureId);
	        response.addHeader(DOMAINURL, getDomainUrl());
	        response.addHeader(DOMAINNAME, getDomainName());
	    	log.info("Generating secureId..... finish");
	      return "hesta";
	}
	
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public  ResponseEntity <Boolean> sendEmail(InputStream inputStream) {
		
		log.info("Generating email..... start");
		Boolean emailStatus = Boolean.FALSE;
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String string = "";	
		JSONObject json = null;
		JSONObject placeholderjson = null;
		String line = null;
		MetLifeEmailVO req = new MetLifeEmailVO();
		HashMap<String, String> placeholder = new HashMap<String, String>();
		try {
			while ((line = in.readLine()) != null) { 
				string += line + "\n";
				log.info("clientname>>"+string);
				json = new JSONObject(string);  
				if(!json.isNull("emailDataElementMap")){
					placeholderjson= (JSONObject)json.get("emailDataElementMap");
					if(!placeholderjson.isNull(EMAIL_SMTP_PORT)){
						placeholder.put( EMAIL_SMTP_PORT, (String)placeholderjson.get(EMAIL_SMTP_PORT));
					}
					if(!placeholderjson.isNull(EMAIL_SUBJECT)){
						placeholder.put( EMAIL_SUBJECT, (String)placeholderjson.get(EMAIL_SUBJECT));
					}
					if(!placeholderjson.isNull(UW_DECISION)){
						placeholder.put( UW_DECISION, (String)placeholderjson.get(UW_DECISION));
					}
					if(!placeholderjson.isNull(LASTNAME)){
						placeholder.put( LASTNAME, (String)placeholderjson.get(LASTNAME));
					}
					if(!placeholderjson.isNull(PDFLINK)){
						placeholder.put( PDFLINK, (String)placeholderjson.get(PDFLINK));
						req.setAttachedPdf((String)placeholderjson.get(PDFLINK));
					}
					
					if(!placeholderjson.isNull(EMAIL_IND_SUBJECT_NAME)){
						placeholder.put( EMAIL_IND_SUBJECT_NAME, (String)placeholderjson.get(EMAIL_IND_SUBJECT_NAME));
					}
					if(!placeholderjson.isNull(UW_DECISION_BLURB)){
						placeholder.put( UW_DECISION_BLURB, (String)placeholderjson.get(UW_DECISION_BLURB));
					}
					if(!placeholderjson.isNull(TITLE)){
						placeholder.put( TITLE, (String)placeholderjson.get(TITLE));
					}
					if(!placeholderjson.isNull(FIRSTNAME)){
						placeholder.put( FIRSTNAME, (String)placeholderjson.get(FIRSTNAME));
					}
					if(!placeholderjson.isNull(EMAIL_SMTP_HOST)){
						placeholder.put( EMAIL_SMTP_HOST, (String)placeholderjson.get(EMAIL_SMTP_HOST));
					}
					
					if(!placeholderjson.isNull(APPLICATIONNUMBER)){
						placeholder.put( APPLICATIONNUMBER, (String)placeholderjson.get(APPLICATIONNUMBER));
					}
					if(!placeholderjson.isNull(MEMBER_NO)){
						placeholder.put( MEMBER_NO, (String)placeholderjson.get(MEMBER_NO));
					}
					req.setEmailDataElementMap(placeholder);
					
				}	
				if(!json.isNull("toRecipents")){
					ArrayList<String> toReciepentList = new ArrayList<String>();		
					JSONArray recipentArray = (JSONArray)json.get("toRecipents");
					JSONObject jsonObj;
					for (int i = 0; i < recipentArray.length(); i++) {
						jsonObj = (JSONObject)recipentArray.get(i);
						if(!jsonObj.isNull(ADDRESS)){
							toReciepentList.add((String)jsonObj.get(ADDRESS));		
						}						
					}
					req.setToRecipents(toReciepentList);
					
				}
				
				if(!json.isNull("fromSender")){
					 JSONObject fromSenderjson = (JSONObject)json.get("fromSender");
					 req.setFromSender((String)fromSenderjson.get(ADDRESS));
					
				}
				if(!json.isNull("emailTemplateId")){
					req.setEmailTemplateId((String)json.get("emailTemplateId"));
				}
				if(!json.isNull("title")){
					req.setTitle((String)json.get("title"));
				}
				if(!json.isNull("firstName")){
					req.setFirstName((String)json.get("firstName"));
				}
				if(!json.isNull("lastName")){
					req.setLastName((String)json.get("lastName"));
				}
				if(!json.isNull("applicationId")){
					req.setApplicationId((String)json.get("applicationId"));
				}
				if(!json.isNull("applicationtype")){
					req.setApplicationtype((String)json.get("applicationtype"));
				}
				if(!json.isNull(FUNDCODE)){
					log.info("funcode>>"+json.get(FUNDCODE));
					req.setFundCode((String)json.get(FUNDCODE));
					
				}			
				if(!json.isNull("ccReciepent")){
					req.setCcReciepent((String)json.get("ccReciepent"));
				}
				if(!json.isNull("bccReciepent")){
					req.setBccReciepent((String)json.get("bccReciepent"));
				}		
				req.setHtmlEmails(Boolean.TRUE);
				req.setIsAgentEmailReq(Boolean.FALSE);
				
			}		 
			 
			emailStatus= MetLifeEmailService.getInstance().sendMail(req, req.getAttachedPdf());
		} catch (Exception e) {
			log.error("Error in mail"+e);
			return new ResponseEntity<Boolean>(HttpStatus.NO_CONTENT);
		}
		log.info("Generating email..... finish");
		return new ResponseEntity<Boolean>(emailStatus, HttpStatus.OK);
	}
	
	
	@RequestMapping("/genLand")
	public String memberLogin(Model model,
			@RequestParam(value = "lodge_fund_id", required = true, defaultValue = "fund") String fund,
			@RequestParam(value = "lodge_fund_email", required = true, defaultValue = "email") String fundEmail,
			@RequestParam(value = "input_identifier", required = true, defaultValue = "identifier") String input_identifier,
			@RequestParam(value = NON_MV, required = false, defaultValue = NON_MV) String nonmv,
			HttpServletRequest request,HttpServletResponse response) {
		model.addAttribute("fundName", fund);
		model.addAttribute("fundEmail", fundEmail);
		model.addAttribute("inputIdentifier", input_identifier);
		model.addAttribute(NON_MV, nonmv);
		String output = null;
		HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        if(null!= session){
        	session.setAttribute("lodge_fund_id", fund);
        	session.setAttribute("lodge_fund_email", fundEmail);
        	session.setAttribute("input_identifier", input_identifier);
        }
		log.info("inside memberlogin function>>");
		if (null != fund && fund.equalsIgnoreCase("FIRS")) {
			return "firsLogin";
		}
		//Guild - Migration Starts
		else if (null != fund && fund.equalsIgnoreCase("GUIL")){
			output = EapplicationConstants.GUIL_SUPER;
		}	
		else {
			output =  "";
		}
		return output;
		//Guild - Migration Ends
	}
	
	
	@RequestMapping("/commonLanding")
		public String firsAppService(HttpServletRequest request,HttpServletResponse response) throws Exception {
		log.info("Start of  firsAppService function>>");
		String fund = request.getParameter(FUND_ID);
		String fundEmail = request.getParameter("emailID");
		String input_identifier = request.getParameter("InputIdentifier");
		String clientNumber = request.getParameter("memberId");
		String dob = request.getParameter("dobId");
		String nonmv = request.getParameter(NON_MV);
		String firstName = request.getParameter("firstnameId");
		String surName = request.getParameter("surnameId");
		String redirect = null;
		String savedAppRedirect = request.getParameter("retrieve");
		String output = EapplyHelper.callMemberSearchService(clientNumber, dob, fund, getMemberServiceUrl(),input_identifier,fundEmail);
		String savedAppsCheck = EapplyHelper.checkSavedAppNonMem(clientNumber,getB2bbaseurl(),fund);
		response.addHeader(DOMAINNAME, getDomainName());
			/*String secureId = EapplyHelper.callB2bService(output,getB2bbaseurl());
			response.addHeader(INPUTDATA, secureId);
			response.addHeader(AUTHORIZATION, new String(Base64.getEncoder().encode(("Bearer "+secureId).getBytes())));
			response.addHeader(NON_MV, "savedApp");
			return FIRST_SUPER; */
		 if(output!=null){
			log.info("output>>"+output);
			System.out.println("Member Serach url Output in firsAppService for "+ fund+" is: "+ output);
			String secureId = EapplyHelper.callB2bService(output,getB2bbaseurl());
			response.addHeader(INPUTDATA, secureId);
			if("yes".equalsIgnoreCase(savedAppRedirect) && "yes".equalsIgnoreCase(savedAppsCheck))
			{
				response.addHeader(NON_MV, "savedApp");
				response.addHeader(CLIENT_REF_NO, clientNumber);
			}
			response.addHeader(AUTHORIZATION, new String(Base64.getEncoder().encode(("Bearer "+secureId).getBytes())));
			//GUILD Migration - Change Starts
			if(null != fund && FUNDNAME_FIRS.equalsIgnoreCase(fund)) {
				redirect = FIRST_SUPER;
			}else if(null != fund && FUNDNAME_GUIL.equalsIgnoreCase(fund)) {
				redirect = GUIL;
			}
			//return FIRST_SUPER;
			//GUILD Change Ends
		}else{
				if(null!=nonmv && "yes".equalsIgnoreCase(nonmv)){
					String nonMemberData = EapplyHelper.formNonMemberXML(clientNumber,dob,fund,input_identifier,firstName,surName);
					String secureIdNvm = EapplyHelper.callB2bService(nonMemberData,getB2bbaseurl());
					if("yes".equalsIgnoreCase(savedAppRedirect) && "yes".equalsIgnoreCase(savedAppsCheck))
					{
						response.addHeader(NON_MV, "savedApp");
						response.addHeader(CLIENT_REF_NO, clientNumber);
					}
					else
					{
					response.addHeader(NON_MV, "newApp");
					}
					response.addHeader(INPUTDATA, secureIdNvm);
					response.addHeader(AUTHORIZATION, new String(Base64.getEncoder().encode(("Bearer "+secureIdNvm).getBytes())));
					//Guild - changes starts
					if(null != fund && EapplicationConstants.FUNDNAME_FIRS.equalsIgnoreCase(fund)) {
						redirect =  FIRST_SUPER;
					}else if(null != fund && EapplicationConstants.FUNDNAME_GUIL.equalsIgnoreCase(fund)) {
						redirect =  GUIL;
					}
					return redirect; 
				}else{
					response.addHeader("Status", "loginFailed");
					//MTAA Change Starts
					if(null != fund && EapplicationConstants.FUNDNAME_FIRS.equalsIgnoreCase(fund)) {
						redirect =  EapplicationConstants.FIRS_SUPER;
					}else if(null != fund && EapplicationConstants.FUNDNAME_GUIL.equalsIgnoreCase(fund)) {
						redirect =  EapplicationConstants.GUIL_SUPER;
					}
					//return "firsLogin";
					//Guild Change Ends
				}
		}	
		 return redirect;
		  
    }
	
	@RequestMapping("/corporate")
	public String corporateService(HttpServletRequest request,HttpServletResponse response) throws Exception {
	log.info("Start of  corporateService function>>");
	String sData = request.getParameter(S_DATA);
	String InputIdentifier = request.getParameter(INPUT_IDENTIFIER);
    String InputData= request.getParameter(INPUT_DATA);
    String Channel = request.getParameter(CHANNEL);
    String c = request.getParameter("c");
    RestTemplate rest = new RestTemplate();
	MultiValueMap<String, String> vars = new LinkedMultiValueMap<String, String>();
	String transactionUrl = b2bbaseurl+"/corporate/login";
    if(!EapplyHelper.isNullOrEmpty(Channel)) {
    	vars.add("inputData", InputData);
    	vars.add("inputIdentifier", InputIdentifier);
    	vars.add("sdata", sData);
    	vars.add("c", "");
    	UriComponentsBuilder builder = UriComponentsBuilder
    		    .fromUriString(transactionUrl).queryParams(vars);
    	String fileuploadJson = rest.getForObject(builder.toUriString(), String.class, vars);
    	response.addHeader(INPUTDATA, fileuploadJson);
    	response.addHeader("Channel", Channel);
	}
 else {
	 if(!EapplyHelper.isNullOrEmpty(c)) {
		 vars.add("inputData", "");
	    	vars.add("inputIdentifier", "");
	    	vars.add("sdata", "");
	    	vars.add("c", c);
	    	UriComponentsBuilder builder = UriComponentsBuilder
	    		    .fromUriString(transactionUrl).queryParams(vars);
	    	String fileuploadJson = rest.getForObject(builder.toUriString(), String.class, vars);
	    	 response.addHeader(INPUTDATA, fileuploadJson);
	    	
	 }
	 if(EapplyHelper.isNullOrEmpty(c) && !EapplyHelper.isNullOrEmpty(sData)) {
		 	vars.add("inputData", InputData);
	    	vars.add("inputIdentifier", InputIdentifier);
	    	vars.add("sdata",sData);
	    	vars.add("c","");
	    	UriComponentsBuilder builder = UriComponentsBuilder
	    		    .fromUriString(transactionUrl).queryParams(vars);
	    	String fileuploadJson = rest.getForObject(builder.toUriString(), String.class, vars);
	    	 response.addHeader(INPUTDATA, fileuploadJson);
	    	 response.addHeader("Channel", "eviewsave");
	 }
	 response.addHeader("Channel", EVIEW);
	 }
    String fundId= "Corporate";
    String token=EapplyHelper.callB2bService(fundId,getB2bbaseurl());
	long millisStart = Calendar.getInstance().getTimeInMillis();
	System.out.println("---------------->"+millisStart);
    		response.addHeader("token", token);
    		response.addHeader("auraId", String.valueOf(millisStart));
    		response.addHeader(DOMAINURL, getDomainUrl());
            response.addHeader(DOMAINNAME, getDomainName());
		return CORPORATE; 
	  
}
	//Added for insurance calculator
	@RequestMapping("/insCostReq")
    public String insCost(HttpServletRequest request,@RequestParam(value="encryptedString") String encryptedString,HttpServletResponse response) throws Exception{
        
		log.info("Generating insCost..... start");
		
        HttpSession session = request.getSession();
        if (session != null && !session.isNew()) {
            session.invalidate();
            session = request.getSession();
        }
        
        
     
        response.addHeader(INPUTDATA, encryptedString);
        response.addHeader(DOMAINURL, getDomainUrl());
        response.addHeader(DOMAINNAME, getDomainName());
    	log.info("Generating insCost secureId..... finish");
      return "insuranceCost"; 
        
    }
	
	@RequestMapping("/premiumCalculator")
	public String premiumCalculator(HttpServletRequest request,HttpServletResponse response, @RequestParam(value="encryptedString") String encryptedString) throws Exception {
	log.info("Start of  Premium Calculator function>>");
	String sData = encryptedString;
    RestTemplate rest = new RestTemplate();
    MultiValueMap<String, String> vars = new LinkedMultiValueMap<String, String>();
	vars.add("token",sData);
	String transactionUrl = b2bbaseurl+"getInsurancecustomerdata";
    	UriComponentsBuilder builder = UriComponentsBuilder
    		    .fromUriString(transactionUrl).queryParams(vars);
    	String fileuploadJson = rest.getForObject(builder.toUriString(), String.class, vars);
    	response.addHeader(INPUTDATA, fileuploadJson);
    	response.addHeader(ENCRYPTDATA, encryptedString);
 
	long millisStart = Calendar.getInstance().getTimeInMillis();
	System.out.println("---------------->"+millisStart);
    		response.addHeader("auraId", String.valueOf(millisStart));
    		response.addHeader(DOMAINURL, getDomainUrl());
            response.addHeader(DOMAINNAME, getDomainName());
		return "premiumCalculator"; 
	  
}
	
	@RequestMapping("/insuranceCalculator")
	public String insuranceCalculator(HttpServletRequest request,HttpServletResponse response, @RequestParam(value="encryptedString") String encryptedString) throws Exception {
	log.info("Start of  insurance Calculator function>>");
	String sData = encryptedString;
    RestTemplate rest = new RestTemplate();
    MultiValueMap<String, String> vars = new LinkedMultiValueMap<String, String>();
	vars.add("token",sData);
	String transactionUrl = b2bbaseurl+"getInsurancecustomerdata";
    	UriComponentsBuilder builder = UriComponentsBuilder
    		    .fromUriString(transactionUrl).queryParams(vars);
    	String fileuploadJson = rest.getForObject(builder.toUriString(), String.class, vars);
    	response.addHeader(INPUTDATA, fileuploadJson);
    	response.addHeader(ENCRYPTDATA, encryptedString);
 
	long millisStart = Calendar.getInstance().getTimeInMillis();
	System.out.println("---------------->"+millisStart);
    		response.addHeader("auraId", String.valueOf(millisStart));
    		response.addHeader(DOMAINURL, getDomainUrl());
            response.addHeader(DOMAINNAME, getDomainName());
		return "insuranceCalculator"; 
	  
}
	
	
	public String getSsologinurl() {
		return ssologinurl;
	}

	public void setSsologinurl(String ssologinurl) {
		this.ssologinurl = ssologinurl;
	}

	public String getVicshomeurl() {
		return vicshomeurl;
	}

	public void setVicshomeurl(String vicshomeurl) {
		this.vicshomeurl = vicshomeurl;
	}

	public String getSsologouturl() {
		return ssologouturl;
	}

	public void setSsologouturl(String ssologouturl) {
		this.ssologouturl = ssologouturl;
	}

	public static void main(String[] args) {
		SpringApplication.run(EApplyv2Application.class, args);
	}	

}
