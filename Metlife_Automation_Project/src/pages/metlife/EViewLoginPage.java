/**
 * 
 */
package pages.metlife;

import java.util.Properties;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.EViewLoginPageObjects;


/**
 * @author sampath
 * 
 */
public class EViewLoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public EViewLoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public EViewLoginPage LoginToeView() {
		enterLoginDetails();
		return new EViewLoginPage(scriptHelper);
	} 
	
	private void enterLoginDetails() {
		
		String strUserID = dataTable.getData("MetLife_Data", "UserId");
		String strLoginPassword = dataTable.getData("MetLife_Data", "Password");
		
		try{
			tryAction(waitForClickableElement(EViewLoginPageObjects.txtUserID),"SET","Login User Name is ",strUserID);
			driver.findElement((EViewLoginPageObjects.txtPassword)).sendKeys(strLoginPassword);
			report.updateTestLog("Password", "Password Entry", Status.PASS);
			tryAction(waitForClickableElement(EViewLoginPageObjects.btnLogin),"Click","Login");
			sleep();
		//	switchToMainFrame();
			}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
