<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="eapplymain" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="../../eapply/static/firstsuper/images/favicon.ico">
<title>First Super</title>

<!-- Bootstrap core CSS -->
<link href="../../eapply/static/firstsuper/css/bootstrap.min.css" rel="stylesheet">
<link href="../../eapply/static/firstsuper/css/bootstrap-select.css" rel="stylesheet">
<link href="../../eapply/static/firstsuper/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="../../eapply/static/firstsuper/css/datepicker.css" />


<!-- Custom styles for this template -->
<link href="../../eapply/static/firstsuper/css/custom.css" rel="stylesheet">
<link href="../../eapply/static/firstsuper/css/custom-ben.css" rel="stylesheet">
<link href="../../eapply/static/firstsuper/css/ngDialog.css" rel="stylesheet">
<link rel="stylesheet" href="../../eapply/static/firstsuper/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="../../eapply/static/firstsuper/css/ngDialog-theme-plain.css">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../eapply/static/firstsuper/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.../../eapply/static/firstsuper/js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<%
String headerData = response.getHeader("InputData");
String nonMvStatus = response.getHeader("nonmv");
String clntRefNo = response.getHeader("clientRefNo");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var memberStatus =  '<%=nonMvStatus%>';
var clintRef = '<%=clntRefNo%>';
var baseUrl = '<%= domainName%>';
</script>


<body class="tooltip-demo">

<div ng-view autoscroll="true"></div>
<div id="body-mask" data-loading>
	<div class="body-mask-loader"></div>
</div>
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../../eapply/resources/js/lib/angular.min.js"></script>
<script src="../../eapply/resources/js/lib/angular-idle.min.js"></script>
<script src="../../eapply/static/firstsuper/js/angular-route.js"></script>
<script src="../../eapply/static/firstsuper/js/angular-resource.min.js"></script>
<script src="../../eapply/static/firstsuper/js/jquery.min.js"></script> 
<!--  <script src="../../eapply/static/firstsuper/js/jquery-ui.js"></script> -->
<script src="../../eapply/static/firstsuper/js/bootstrap.min.js"></script> 
<script src="../../eapply/static/firstsuper/js/docs.min.js"></script>
<script src="../../eapply/static/firstsuper/js/ng-file-upload-shim.js"></script>
<script src="../../eapply/static/firstsuper/js/ng-file-upload.js"></script>
<script src="../../eapply/static/firstsuper/js/ngDialog.js"></script>

<script src="../../eapply/static/common/services/sessionTimeoutService.js"></script>
<script src="../../eapply/static/common/services/exceptionHandler.js"></script>
<script src="../../eapply/static/common/services/storageHandler.js"></script>
<script src="../../eapply/static/common/services/restAPI.js"></script>
<script src="../../eapply/static/common/services/appDataModel.js"></script>
<script src="../../eapply/static/common/services/eApplyInterceptor.js"></script>
<!-- CareSuperApp application file -->
<script src="../../eapply/static/firstsuper/js/app.js"></script>

<!-- <script src="https://assets.adobedtm.com/7bd378b63ae642286f5e272876265df98dfca74c/satelliteLib-9d49fffd8e96ae995aa8e242a15f1acf1428ecc8-staging.js"></script>
 -->

<!-- CareSuperApp controllers -->
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/aura.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/auraOcc.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/auraSpecialOffer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/auraTransfer.ctrl.js"></script>
<!-- <script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/auraCancel.ctrl.js"></script> -->
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/decision.ctrl.js"></script>
<!-- <script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/landing.ctrl.js"></script> -->
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/login.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/newMemberQuote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/quote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/quoteCancel.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/quoteOccUpdate.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/quoteTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/retrieveSavedApp.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/summary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/newMemberSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/workRatingSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/transferSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/DecisionController.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/cancelConfirmation.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/timeOutController.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/lifeEvent.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/quoteSpecial.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/specialOfferSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/lifeeventsummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/auraLifeEvent.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/quoteConvert.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/convertSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/header.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/nonmember.quote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/auraNonMember.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/controllers/nonMembersummary.ctrl.js"></script>

<script type="text/javascript" src="../../eapply/static/firstsuper/js/moment.js"></script> 
<script type="text/javascript" src="../../eapply/static/firstsuper/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="../../eapply/static/firstsuper/js/bootstrap-select.js"></script> 

<!-- CareSuperApp Services -->
<script type="text/javascript" src="../../eapply/static/firstsuper/js/services/service.js"></script>

<!-- CareSuperApp utilities -->
<script type="text/javascript" src="../../eapply/static/firstsuper/js/utils/filters.js"></script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/utils/directives.js"></script>


<script src="../../eapply/static/firstsuper/js/custom.js"></script> 
<script src="../../eapply/static/firstsuper/js/validation.js"></script>
<!--<script type="text/javascript">_satellite.pageBottom();</script> -->

<script>
//awful hack for angular-vertilize...
angular.element.prototype.outerWidth = function() {
    return jQuery(this[0]).outerWidth();
};
angular.element.prototype.height = function() {
    return jQuery(this[0]).height();
};
</script>
<script type="text/javascript" src="../../eapply/static/firstsuper/js/utils/verticalHeight.js"></script>
</body>
</html>