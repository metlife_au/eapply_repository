/*aura transfer controller*/    
FirstSuperApp.controller('auraLifeEvent',['$scope','$rootScope','$timeout', '$location','$window','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','ngDialog','urlService','saveEapply','submitEapply',
                                     function($scope,$rootScope,$timeout, $location,$window,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,ngDialog,urlService,saveEapply,submitEapply){
	
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {   
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    }
  	
  	$scope.coverDetails=PersistenceService.getlifeEventCoverDetails();
  	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.lifeEventOccDetails =PersistenceService.getLifeEventCoverOccDetails();
  	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();
    console.log($scope.coverDetails); 	
    	
 // 	console.log(auraInputService); 	
  	auraInputService.setFund('FIRS');
  	auraInputService.setMode('LifeEvent'); 	
  	//setting values from quote page
  //setting deafult vaues for testing
  	auraInputService.setName($scope.coverDetails.name);
  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ; 	
  //auraInputService.setAge(moment().diff($scope.coverDetails.dob, 'years'));
  	//below values has to be set from quote page, as of now taking from existing insurance
  	auraInputService.setAppnumber($scope.coverDetails.appNum);
  	auraInputService.setDeathAmt(parseInt($scope.coverDetails.deathAmt));
  	auraInputService.setTpdAmt(parseInt($scope.coverDetails.tpdAmt));
  	auraInputService.setIpAmt(parseInt($scope.coverDetails.ipAmt));
  	auraInputService.setWaitingPeriod($scope.coverDetails.waitingPeriod);
  	auraInputService.setBenefitPeriod($scope.coverDetails.benefitPeriod); 
  	if($scope.lifeEventOccDetails && $scope.lifeEventOccDetails.gender ){
  		auraInputService.setGender($scope.lifeEventOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
  //auraInputService.setGender($scope.coverDetails.gender);
  	auraInputService.setIndustryOcc($scope.lifeEventOccDetails.industryCode+":"+$scope.lifeEventOccDetails.occupation);
  	auraInputService.setCountry('Australia');
  	auraInputService.setSalary('150000');
  	auraInputService.setFifteenHr('Yes');
  	auraInputService.setClientname('metaus')
  	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
  	auraInputService.setExistingTerm(false);
  	if($scope.personalDetails[0].memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	}
  	/*auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')
  	auraInputService.setGender($scope.coverDetails.gender)
  	auraInputService.setIndustryOcc($scope.coverDetails.industryCode+":"+$scope.coverDetails.occupation)
  	if($scope.transferOccDetails.citizenQue=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.transferOccDetails.citizenQue)
  	}  	
  	auraInputService.setSalary($scope.transferOccDetails.salary)
  	auraInputService.setFifteenHr($scope.transferOccDetails.fifteenHr) 
  	auraInputService.setName($scope.coverDetails.name)*/
  	///////////////   	
  	
  	 //setting deafult vaues for testing
	/*auraInputService.setName('FIRS')
  	auraInputService.setAge(50)
  	//below values has to be set from quote page, as of now taking from existing insurance
  	auraInputService.setAppnumber(14782223482354338)
  	auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')   
  	auraInputService.setGender('Male')
  	auraInputService.setIndustryOcc('026:Transfer')
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary('100000')
  	auraInputService.setFifteenHr('Yes')*/
  	 //end deafult vaues for testing
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
  		$scope.auraResponseDataList = response.data.questions;  		
  		angular.forEach($scope.auraResponseDataList, function(Object) {
			$scope.sectionname = Object.questionAlias.substring(3);            			
			 
			});
  	});
  	
  	 $scope.updateRadio = function (answerValue, questionObj){   
  		console.log($scope.auraResponseDataList) 	
  		
  		questionObj.arrAns[0]=answerValue;
  		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
  		  auraResponseService.setResponse($scope.auraRes)  		  
  		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {  			 
  			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
  			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
  	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 	  			
  	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
  	  				//branch complete false for previous questions  	  				
  	  				$scope.auraResponseDataList[selectedIndex].error=true;  	  				
  	  			}else if(Object.questionComplete){
  	  				$scope.auraResponseDataList[selectedIndex].error=false;
  	  			}
  	  			
  	  		});
  			  
      	}, function () {
      		//console.log('failed');
      	});
  	 };
  	 
  	$scope.proceedNext = function(){
  		$scope.keepGoing= true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error = true; 
  				$scope.keepGoing= false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error = false;
  				//$scope.go('/transferSummary');
  			}
  		});
  		if($scope.keepGoing){
  			$rootScope.$broadcast('disablepointer');
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) { 
  				PersistenceService.setLifeEventCoverAuraDetails(response.data);
  				$scope.auraResponses = response.data;
  				if($scope.auraResponses.overallDecision!='DCL'){
					//console.log(response.data);
  					$scope.go('/lifeeventsummary/1');
	  		}else if($scope.auraResponses.overallDecision =='DCL'){
	  			if($scope.coverDetails != null && $scope.lifeEventOccDetails != null && $scope.personalDetails[0] != null){
	  				$scope.coverDetails.lastSavedOn = '';
	    			$scope.details={};
	    			$scope.details.occupationDetails = $scope.lifeEventOccDetails;
	    			$scope.details.lifeEventDocuments = $scope.uploadedFileDetails;
	      			var coverObject = angular.extend($scope.details,$scope.coverDetails);
	              	var auraObject = angular.extend(coverObject,$scope.auraResponses);
	      			var submitObject = angular.extend(auraObject,$scope.personalDetails[0]);
	      			auraResponseService.setResponse(submitObject);
	      			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
	              		console.log(response.data);
	              		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
	              		PersistenceService.setNpsUrl(response.data.npsTokenURL);
                  		$scope.go('/lifeeventdecline');
	              	}, function(err){
		            		console.log('Error while submitting transfer cover ' + err);
		            	});
	              }
	  		}
  				//console.log(response.data);
  			
  	  		});
  		}
  	};
  	
  	$scope.lifeEventAuraSaveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
	
 	var appNum;
    appNum = PersistenceService.getAppNumber();
  	$scope.saveAuraLifeEvent = function() {
  		$scope.lifeEventAuraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
  		if($scope.coverDetails != null && $scope.lifeEventOccDetails != null && $scope.personalDetails[0] != null){
    		$scope.coverDetails.lastSavedOn = 'AuraLifeEventPage';
    		//$scope.auraDetails
    		var details = {};
	    	details.occupationDetails = $scope.lifeEventOccDetails;
    		var temp = angular.extend($scope.coverDetails,details);
        	var saveAuraLifeEventObject = angular.extend(temp, $scope.personalDetails[0]);
        	auraResponseService.setResponse(saveAuraLifeEventObject)
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {  
	                console.log(response.data)
	        });
    	}
  	};
  	
  	$scope.collapseOne = true;
  	$scope.toggleOne = function() {
        $scope.collapseOne = !$scope.collapseOne;          
    };
  	$scope.collapseTwo = false;
  	$scope.toggleTwo = function() {
        $scope.collapseTwo = !$scope.collapseTwo;            
    };
    $scope.collapseThree = false;
  	$scope.toggleThree = function() {
        $scope.collapseThree = !$scope.collapseThree;            
    };
    $scope.collapseFour = false;
  	$scope.toggleFour = function() {
        $scope.collapseFour = !$scope.collapseFour;            
    };
    $scope.collapseFive = false;
  	$scope.toggleFive = function() {
        $scope.collapseFive = !$scope.collapseFive;            
    };
}]); 
////