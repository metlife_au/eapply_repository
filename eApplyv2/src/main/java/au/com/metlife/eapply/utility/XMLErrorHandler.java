package au.com.metlife.eapply.utility;

import java.util.HashMap;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLErrorHandler implements  ErrorHandler {
	private static final Logger log = LoggerFactory.getLogger(XMLErrorHandler.class);
	
	private String errors = "";
	
	public static final int ZERO_CONST = 0;

	 public String getErrors() {
	        return errors;
	    }

	 public void setErrors(String errors) {
	        this.errors = errors;
	    }
	 
	 public static final HashMap<String, String> elementsNameMap=new HashMap<String, String>();

		static{

			elementsNameMap.put("lodge_fund_email","1001");			

			elementsNameMap.put("userid","1002");		
			
			elementsNameMap.put("user_password","1003");

			elementsNameMap.put("lodge_fund_id","1004");

			elementsNameMap.put("lodge_mem_fname","1005");

			elementsNameMap.put("lodge_mem_lname","1006");

			elementsNameMap.put("lodge_mem_dob","1007");

			elementsNameMap.put("lodge_mem_gender","1008");

			elementsNameMap.put("lodge_cl_ref_no","1009");

			elementsNameMap.put("lodge_e_default_unit_death","1010");

			elementsNameMap.put("lodge_e_default_unit_tpd","1011");

			elementsNameMap.put("lodge_e_default_unit_ip","1012");

			elementsNameMap.put("lodge_e_default_fixed_death","1013");

			elementsNameMap.put("lodge_e_default_fixed_tpd","1014");

			elementsNameMap.put("lodge_e_opt_unit_death_amount","1015");

			elementsNameMap.put("lodge_e_opt_unit_tpd_amount","1016");

			elementsNameMap.put("lodge_e_opt_unit_ip_amount","1017");

			elementsNameMap.put("lodge_e_opt_fixed_death_amount","1018");

			elementsNameMap.put("lodge_e_opt_fixed_tpd_amount","1019");

			elementsNameMap.put("lodge_e_opt_fixed_ip_amount","1020");

			elementsNameMap.put("address_line_1","1021");

			elementsNameMap.put("country","1022");

			elementsNameMap.put("lodge_e_unit_rating_category","1023");

			elementsNameMap.put("lodge_e_ip_rating_category","1024");

			elementsNameMap.put("lodge_e_fixed_rating_category","1025");

			elementsNameMap.put("lodge_e_wait_period","1026");

			elementsNameMap.put("lodge_e_ben_period","1027");

			elementsNameMap.put("lodge_member_type","1028");

			elementsNameMap.put("priority_client","1029");

			elementsNameMap.put("lodge_date_joined","1030");
			
			elementsNameMap.put("xml_body","1031");
			
			elementsNameMap.put("lodge_mem_age","1032");

		}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		 errors +=  transformToString(exception);
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		 errors +=  transformToString(exception);
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		 errors +=  transformToString(exception);
	}
	
	private final String transformToString(SAXParseException exception){
		log.info("Transform to string start");
		String strMessage="";
		
		String strField=getStrField(exception.getMessage());

		if(strField!=null){
						
			if(strField.indexOf('{')<ZERO_CONST){
				String age=null;
				if("lodge_mem_age".equalsIgnoreCase(strField)) {
					/*SRKR:20/11/2013 Updated to show error message with curent age
					  instead of Age next birthday*/
					age=getStrValue(exception.getMessage());
					if(age!=null && age.length()>ZERO_CONST){
						age=""+(new Integer(age).intValue()-1);
					}
					strMessage = getStrFieldNumeric(strField)+":Your age "+age+" is not allowed for the additional cover to apply for."+"$#$";
				}else {
					strMessage = errorCodeNameMap.get(strField)+":"+getStrFieldNumeric(strField)+":The data passed for ["+strField+"] is not in expected format, Possibility of the error might be value of this field exceeded the allowable length or values is not in."+"$#$";	
				}
			}else{
			
				strMessage= errorCodeNameMap.get(strField.substring(1, strField.length()-1))+":"+"Fund authentication failed, missing parameters are "+strField+"$#$";
			}
		}
		log.info("Transform to string finish");
		return strMessage;
	}
	
	/**
	 * @param strField
	 * @return
	 */
	private final String getStrField(String strMessage){

		String strField=null;

		if(strMessage.indexOf("of element '")>ZERO_CONST){

			strField=strMessage.split("of element '")[1].split("'")[0];			

		}
		
		 if(strMessage.indexOf("One of '")>ZERO_CONST){
			
			strField=strMessage.split("One of '")[1].split("'")[0];
			
		}
		return strField;
	}
	
	private final String getStrFieldNumeric(String strField){
		

		String strFieldRet=strField;

			if(elementsNameMap.containsKey(strField)){
				strFieldRet=elementsNameMap.get(strField).toString();
			}
		return strFieldRet;
	}



	private final String getStrValue(String strMessage){
		

		String strValue="";

		if(strMessage.indexOf("value '")>ZERO_CONST){
			strValue=strMessage.split("value '")[1].split("'")[0];
		}
		return strValue;
	}
	
	public static final HashMap<String, String> errorCodeNameMap = new HashMap<String, String>();

	static{

		errorCodeNameMap.put("lineOfBusiness","1001");
		errorCodeNameMap.put("partnerID","1001");
		errorCodeNameMap.put("adminPartnerID","1001");
		errorCodeNameMap.put("transRefGUID","1001");
		errorCodeNameMap.put("transType","1001");
		errorCodeNameMap.put("transDate","1001");
		errorCodeNameMap.put("transTime","1001");
		errorCodeNameMap.put("fundID","1001");
		errorCodeNameMap.put("fundEmail","1001");
		errorCodeNameMap.put("product","1001");
		errorCodeNameMap.put("campaign_code","1001");
		errorCodeNameMap.put("tracking_code","1001");
		errorCodeNameMap.put("promotion_code","1001");
		errorCodeNameMap.put("policyNumber","1001");
			
		
		errorCodeNameMap.put("memberType","1002");
		errorCodeNameMap.put("memberDivision","1002");
		errorCodeNameMap.put("applicantRole","1002");
		errorCodeNameMap.put("clientRefNumber","1002");
		errorCodeNameMap.put("dateJoined","1002");
		errorCodeNameMap.put("segmentCode","1002");

		errorCodeNameMap.put("title","1002");
		errorCodeNameMap.put("firstName","1002");
		errorCodeNameMap.put("lastName","1002");
		errorCodeNameMap.put("dateOfBirth","1002");
		errorCodeNameMap.put("ageCover","1002");
		errorCodeNameMap.put("gender","1002");
		errorCodeNameMap.put("applicantSubType","1002");
		errorCodeNameMap.put("priority","1002");
		errorCodeNameMap.put("smoker","1002");

		errorCodeNameMap.put("benefitType","1003");
		errorCodeNameMap.put("type","1003");
		errorCodeNameMap.put("units","1003");
		errorCodeNameMap.put("amount","1003");
		errorCodeNameMap.put("loading","1003");
		errorCodeNameMap.put("exclusions","1003");
		errorCodeNameMap.put("occRating","1003");
		errorCodeNameMap.put("waitingPeriod","1003");
		errorCodeNameMap.put("benefitPeriod","1003");
		
		errorCodeNameMap.put("emailAddress","1004");
		errorCodeNameMap.put("mobilePhone","1004");
		errorCodeNameMap.put("homePhone","1004");
		errorCodeNameMap.put("workPhone","1004");
		errorCodeNameMap.put("prefContact","1004");
		errorCodeNameMap.put("prefContactTime","1004");		
		
		errorCodeNameMap.put("addressType","1005");
		errorCodeNameMap.put("line1","1005");
		errorCodeNameMap.put("line2","1005");
		errorCodeNameMap.put("suburb","1005");
		errorCodeNameMap.put("state","1005");
		errorCodeNameMap.put("postCode","1005");
		errorCodeNameMap.put("country","1005");
		
		errorCodeNameMap.put("xml_body","1031");
		
		errorCodeNameMap.put("lodge_mem_age","1032");


		
	}
}
