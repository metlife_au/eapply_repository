package supportlibraries;

import java.util.Date;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.testng.ITestContext;
import org.testng.SkipException;

import com.cognizant.framework.FrameworkParameters;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Util;
import com.cognizant.framework.selenium.ResultSummaryManager;
import com.cognizant.framework.selenium.SeleniumTestParameters;

/**
 * Abstract base class for all the test cases to be automated
 * @author Cognizant
 */
public abstract class JunitTestCase
{
	/**
	 * The {@link SeleniumTestParameters} object to be used to specify the test parameters
	 */
	protected SeleniumTestParameters testParameters;
	/**
	 * The {@link DriverScript} object to be used to execute the required test case
	 */
	protected DriverScript driverScript;

	protected static ResultSummaryManager resultSummaryManager = new ResultSummaryManager();
	protected static FrameworkParameters frameworkParameters = FrameworkParameters.getInstance();
	protected Date startTime, endTime;
	protected static SilkIntegrator silkIntegrator;
	protected static Properties properties;
	protected static ScriptHelper scriptHelper;
	
	/**
	 * Function to do the required set-up activities before executing the overall test suite in TestNG
	 * @param testContext The TestNG {@link ITestContext} of the current test suite 
	 */
	@BeforeClass
	public static void suiteSetup()
	{
		resultSummaryManager.setRelativePath();
		//silkIntegrator = new SilkIntegrator();
		frameworkParameters.setRelativePath(CraftUtility.getRelativePath());
		properties = Settings.getInstance();
		CraftUtility.setDriversToRelative(properties);

		/*if (properties.getProperty("ExternalisePageObjects").equalsIgnoreCase("true"))
		{
			PageObjectLoader.loadProperties(CraftUtility.getRelativePath() + "/"
				+ properties.getProperty("pageObjectsFolderFile"));
		}*/
		
		if(properties.getProperty("ExternalisePageObjects").equalsIgnoreCase("true")){
			PageObjectLoader.loadProperties(CraftUtility.getRelativePath()+"/src/uimap/Page_Objects.properties");
		}

		if(System.getProperty("CraftRunConfiguration")!=null && System.getProperty("CraftRunConfiguration").trim().length()>0){
			resultSummaryManager.initializeTestBatch(System.getProperty("CraftRunConfiguration"));
		}else{
			resultSummaryManager.initializeTestBatch(properties.getProperty("RunConfiguration"));
		}
		
		int nThreads = Integer.parseInt(properties.getProperty("NumberOfThreads"));

		resultSummaryManager.initializeSummaryReport(nThreads);
		resultSummaryManager.setupErrorLog();
	}

	/**
	 * Function to do the required set-up activities before executing each Craft JUNIT test case
	 */
	@Before
	public void testMethodSetup()
	{
		if (frameworkParameters.getStopExecution())
		{
			suiteTearDown();
			throw new SkipException("Aborting all subsequent tests!");
		}
		else
		{
			startTime = Util.getCurrentTime();
		}
	}
	
	/**
	 * Function to do the required wrap-up activities after executing each test case in TestNG
	 */
	@After
	public void testMethodTearDown()
	{
		String testStatus = driverScript.getTestStatus();
		scriptHelper=driverScript.getScriptHelper();
		endTime = Util.getCurrentTime();
		String executionTime = Util.getTimeDifference(startTime, endTime);
		resultSummaryManager.updateResultSummary(testParameters.getCurrentScenario(), testParameters.getCurrentTestcase(),
			testParameters.getCurrentTestDescription(), executionTime, testStatus);
	}

	/**
	 * Function to do the required wrap-up activities after executing the overall test suite in TestNG
	 */
	@AfterClass
	public static void suiteTearDown()
	{
		resultSummaryManager.wrapUp(true);
		if (properties.getProperty("enableCraftSilkIntegration").equalsIgnoreCase("true"))
		{
			//silkIntegrator.setScriptHelper(scriptHelper);
			//silkIntegrator.uploadTestCaseResults();
		}
	}
}