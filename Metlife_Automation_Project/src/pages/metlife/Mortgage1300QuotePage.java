package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.Mortgage1300QuoteObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class Mortgage1300QuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public Mortgage1300QuotePage getQuote() {
		quoteCalculation();
		return new Mortgage1300QuotePage(scriptHelper);
	}

	public Mortgage1300QuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void quoteCalculation() {
		String poicyType = dataTable.getData("MetLife_Data", "PolicyType");
		String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String firstNameL = dataTable.getData("MetLife_Data", "FirstName");
		String firstNameR = dataTable.getData("MetLife_Data", "FirstNameApp1");
		
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String dobL = dataTable.getData("MetLife_Data", "DateOfBirth");
		String dobR = dataTable.getData("MetLife_Data", "DateOfBirthApp1");
		
		
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String genderL = dataTable.getData("MetLife_Data", "Gender");
		String genderR = dataTable.getData("MetLife_Data", "GenderApp1");
		
		
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strSmokeL = dataTable.getData("MetLife_Data", "personal.smoked");
		String strSmokeR = dataTable.getData("MetLife_Data", "personal.smokedApp1");
		
		String last12Months = dataTable.getData("MetLife_Data", "LoanLast12Months");	
		String last12MonthsL = dataTable.getData("MetLife_Data", "LoanLast12Months");
		String last12MonthsR = dataTable.getData("MetLife_Data", "LoanLast12MonthsApp1");
	//	String strCitizen = dataTable.getData("MetLife_Data", "Citizen");
		
		String strtypeofLoan = dataTable.getData("MetLife_Data", "TypeOfLoan");
						
		String strLoanAmt = dataTable.getData("MetLife_Data", "LoanAmount");
		String strRepaymentAmt = dataTable.getData("MetLife_Data", "RepaymentAmount");
		String strLoanPercentage = dataTable.getData("MetLife_Data", "Percentage");
		String strLoanPercentageJoint = dataTable.getData("MetLife_Data", "PercentageJoint");
	
		try {
			driver.manage().window().maximize();			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			Thread.sleep(2000);
			
		if (poicyType.equalsIgnoreCase("Individual")) {
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtfirstname), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtfirstname), "TAB", "First Name");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtdob), "SET", "DOB", dob);
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtdob), "TAB", "DOB");
			
			Thread.sleep(1000);
			
			
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdGender), "clkradio", "Gender -" + gender);
			} else {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdGender), "clkradio", "Gender -" + gender);
			}
			
			Thread.sleep(1000);
			
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdnoTobacco), "clkradio",
						"Have you Smoked in 12 months - Yes");
			} else {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdnoTobacco), "clkradio",
						"Have you Smoked in 12 months - No");
			}
			
			Thread.sleep(1000);
			
			if (last12Months.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesloan), "clkradio",
						"Has the loan commenced in last 12 months - Yes");
			} else {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesloan), "clkradio",
						"Has the loan commenced in last 12 months - Yes");
			}
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesCitizen), "Click",
						"Are you Citizen or PR - Yes");	
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.drpLoan), "DropDownSelect", "Loan Type",strtypeofLoan);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.txtloanAmount), "SET", "Loan Amount",strLoanAmt);		
			Thread.sleep(1000);			
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.txtloanAmount), "TAB", "Loan Amount");		
			Thread.sleep(1000);		
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.txtrepaymentAmount), "SET", "Repaymnet Amount", strRepaymentAmt);
			Thread.sleep(1000);
					
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtrepaymentAmount), "TAB", "Repaymnet Amount");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.chkloanCover), "Click", "Cover Option");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.drploanCover), "DropDownSelect", "Loan Cover",strLoanPercentage);
			Thread.sleep(1000);
		} else if (poicyType.equalsIgnoreCase("Joint")) {
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdpolicyType), "clkradio",
					"Poilcy Type");
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtfirstlname), "SET", "First Name Applicant 1", firstNameL);
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtfirstlname), "TAB", "First Name Applicant 1");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtfirstrname), "SET", "First Name Applicant 2", firstNameR);
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtfirstrname), "TAB", "First Name Applicant 2");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtdobl), "SET", "DOB", dobL);
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtdobl), "TAB", "DOB");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtdobr), "SET", "DOB", dobR);
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtdobr), "TAB", "DOB");
			
			Thread.sleep(1000);
			
			
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdGenderl), "clkradio", "Gender -" + genderL);
				Thread.sleep(500);
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdGenderr), "clkradio", "Gender -" + genderR);
			} else {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdGenderl), "clkradio", "Gender -" + gender);
				Thread.sleep(500);
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdGenderr), "clkradio", "Gender -" + gender);
			}
			
			Thread.sleep(1000);
			
			if (strSmokeL.equalsIgnoreCase("YES")||strSmokeR.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdnoTobaccol), "clkradio",
						"Have you Smoked in 12 months - Yes");
				Thread.sleep(200);
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdnoTobaccor), "clkradio",
						"Have you Smoked in 12 months - Yes");
				
			} else {
				
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdnoTobaccol), "clkradio",
						"Have you Smoked in 12 months - Yes");
				Thread.sleep(200);
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdnoTobaccor), "clkradio",
						"Have you Smoked in 12 months - Yes");				
			}
			
			Thread.sleep(1000);
			
			if (last12MonthsL.equalsIgnoreCase("YES")||last12MonthsR.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesloanl), "clkradio",
						"Has the loan commenced in last 12 months - Yes");
				
				Thread.sleep(200);
				
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesloanr), "clkradio",
						"Has the loan commenced in last 12 months - Yes");
				
			} else {
				
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesloanl), "clkradio",
						"Has the loan commenced in last 12 months - Yes");
				
				Thread.sleep(200);
				
				tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesloanr), "clkradio",
						"Has the loan commenced in last 12 months - Yes");
			}
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesCitizenl), "Click",
						"Are you Citizen or PR - Yes");	
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.rdyesCitizenr), "Click",
					"Are you Citizen or PR - Yes");	
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.drpLoanjoint), "DropDownSelect", "Loan Type",strtypeofLoan);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.txtloanAmountjoint), "SET", "Loan Amount",strLoanAmt);		
			Thread.sleep(1000);			
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.txtloanAmountjoint), "TAB", "Loan Amount");		
			Thread.sleep(1000);		
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.txtrepaymentAmountjoint), "SET", "Repaymnet Amount", strRepaymentAmt);
			Thread.sleep(1000);
					
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.txtrepaymentAmountjoint), "TAB", "Repaymnet Amount");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.chkloanCoverl), "Click", "Cover Option Applicant 1");
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.drploanCoverl), "DropDownSelect", "Loan Cover Applicant 1",strLoanPercentage);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.chkloanCoverr), "Click", "Cover Option Applicant 2");
			Thread.sleep(1000);			
		
			tryAction(fluentWaitElement(Mortgage1300QuoteObjects.drploanCoverr), "DropDownSelect", "Loan Cover Applicant 2",strLoanPercentageJoint);
			Thread.sleep(1000);
						
		}
			
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(Mortgage1300QuoteObjects.btnCalculateQuote)).click().perform();
			tryAction(waitForClickableElement(Mortgage1300QuoteObjects.btnCalculateQuote), "Click", "Apply");
			Thread.sleep(10000);

			sleep();
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(driver.findElement(Mortgage1300QuoteObjects.btnApply)).click().perform();
			Thread.sleep(10000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
