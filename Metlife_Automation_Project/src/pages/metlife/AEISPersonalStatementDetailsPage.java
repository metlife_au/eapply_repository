/**
 * 
 */
package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ScriptHelper;
import uimap.metlife.PersonalStatementPageObjects;

import com.cognizant.framework.Status;

/**
 * @author sampath
 * 
 */
public class AEISPersonalStatementDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public AEISPersonalStatementDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public AEISPersonalStatementDetailsPage enterAEISPersonalStatementDetails() {
		fillInAEISPersonalStatementDetails();
		return new AEISPersonalStatementDetailsPage(scriptHelper);
	} 
	
	private void fillInInsuranceDetails() {
		
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strReceivedClaim = dataTable.getData("MetLife_Data", "personal.receivedClaim");
		
		try{
			List<WebElement> radInsuranceDetailsNo = null;
			radInsuranceDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsNo);
			List<WebElement> radInsuranceDetailsYes = null;
			radInsuranceDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsYes);
			tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(0)):fluentWaitElement(radInsuranceDetailsNo.get(0)),"Click","Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined? - "+strInsuTPD);
			tryAction(strReceivedClaim.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(1)):fluentWaitElement(radInsuranceDetailsNo.get(1)),"Click","Are you contemplating or have you ever made a claim for or received sickness? - "+strReceivedClaim);
			if(radInsuranceDetailsYes.size()==3)
				tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(2)):fluentWaitElement(radInsuranceDetailsNo.get(2)),"Click","Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund? - " +strInsuTPD);
				
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInLifeStyleDetails() {
		
		
		try{
			
			
			String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
			String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
			String strAlcoholConsu = dataTable.getData("MetLife_Data", "personal.alcohlConsumption");
			String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
			String strAbtHIV = dataTable.getData("MetLife_Data", "personal.HIV");
			
			List<WebElement> radLifeStyleDetailsNo = null;
			radLifeStyleDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsNo);
			List<WebElement> radLifeStyleDetailsYes = null;
			radLifeStyleDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsYes);
	
			tryAction(strTravelPlan.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(0)):fluentWaitElement(radLifeStyleDetailsNo.get(0)),"Click","Do you have firm plans to travel or reside in another country other than NZ?");
			tryAction(strDrugs.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(1)):fluentWaitElement(radLifeStyleDetailsNo.get(1)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you? - "+strDrugs);
			tryAction(strAlcoholConsu.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(2)):fluentWaitElement(radLifeStyleDetailsNo.get(2)),"Click","Have you ever been advised by a health professional to reduce your alcohol consumption? - "+strAlcoholConsu);
			sleep(1000);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkSportsNota),"Click","Do you regularly engage in or intend to engage in any of the following hazardous activities?");
			System.out.println("hazardous NO");
			sleep(1000);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtAlcoholicDrinksConsumption),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			sleep(1000);
			System.out.println("alcoholic NO");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnAlcoholicEnter),"CLICK","Enter");
			System.out.println("alcoholic Enter");
			tryAction(strAbtHIV.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(3)):fluentWaitElement(radLifeStyleDetailsNo.get(3)),"Click","Do you have HIV (Human Immunodeficiency Virus) that causes AIDS? - "+strAbtHIV);
			if(strAbtHIV.equalsIgnoreCase("no")){
				System.out.println("HIV NO");
			    String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetailsYes.get(3).getAttribute("name")+"_1_0'][value='No']";
				//Click On No for HIV
				tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
				driver.findElement(By.id("radio1-15_1_0:1")).click();
			}
			
						
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}

	private void fillInAEISPersonalStatementDetails() {
		
		String strCitizen = dataTable.getData("MetLife_Data", "personal.citizen");
		String strHeight = dataTable.getData("MetLife_Data", "personal.height");
		String strHeightUnits = dataTable.getData("MetLife_Data", "personal.heightunits");
		String strWeight = dataTable.getData("MetLife_Data", "personal.weight");
		String strWeightUnits = dataTable.getData("MetLife_Data", "personal.weightunits");
		String strSmoked = dataTable.getData("MetLife_Data", "personal.smoked");
		String strLungDese = dataTable.getData("MetLife_Data", "personal.lung");
		String strLungAsthma = dataTable.getData("MetLife_Data", "personal.asthma");
		String strLungCondition = dataTable.getData("MetLife_Data", "personal.condition");
		String strPregnent = dataTable.getData("MetLife_Data", "personal.prebnent");
		String strRegulatVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
		String strFamHist = dataTable.getData("MetLife_Data", "personal.famHist");
		String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strAlcohol = dataTable.getData("MetLife_Data", "personal.alcohol");
		String strAlcoholConsu = dataTable.getData("MetLife_Data", "personal.alcohlConsumption");
		String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
		String strHIV = dataTable.getData("MetLife_Data", "personal.HIV");
		String strDisclosed = dataTable.getData("MetLife_Data", "personal.disclosed");
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strReceivedClaim = dataTable.getData("MetLife_Data", "personal.receivedClaim");
		String strGender = dataTable.getData("MetLife_Data", "applicant2_personalDetails2_gender");
		String strRegVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
		String str3YrDiagnosis = dataTable.getData("MetLife_Data", "last3YearsDiagnosied");
		String[] array3YrDiagnosis = str3YrDiagnosis.split(",");
		String strAsthmaWorsen = dataTable.getData("MetLife_Data", "personal.Asthamaworesen");
		
		try{
			tryAction(strCitizen.equalsIgnoreCase("yes")?waitForClickableElement(PersonalStatementPageObjects.radCitizenPRYes):waitForClickableElement(PersonalStatementPageObjects.radCitizenPRNo),"Click","Are you a citizen or permanent resident of Australia? "+strCitizen);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtHeight),"SET","Height",strHeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstHeightUnits),"DropDownSelect","Height Units",strHeightUnits);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.txtWeight),"SET","Weight",strWeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstWeightUnits),"DropDownSelect","Weight Units",strWeightUnits);
			
			if(strGender.equalsIgnoreCase("female")){
				List<WebElement> radHealthDetailsNo = null;
				radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
				List<WebElement> radHealthDetailsYes = null;
				radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsYes);
				//tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
				tryAction(strPregnent.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Are you currently pregnant? - "+strPregnent);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}else{
				List<WebElement> radHealthDetailsNo = null;
				radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
				List<WebElement> radHealthDetailsYes = null;
				radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsYes);
				//tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}
			for(int i=0;i<array3YrDiagnosis.length;i++){
				System.out.println("array3YrDiagnosis[i] : "+array3YrDiagnosis[i]);
				if(array3YrDiagnosis[i].equalsIgnoreCase("Lung")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
					tryAction(fluentWaitElements(PersonalStatementPageObjects.chkAsthma),"chkCheck","Asthma");
					tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition");
					String strNextId = null;
					strNextId = getAttribute(waitForClickableElement(PersonalStatementPageObjects.radAsthmaMild), "name");
					String strRadioId =null;
					strRadioId = org.apache.commons.lang.StringUtils.substring(strNextId, 0, 15)+"1";
					System.out.println("strRadioId is :: "+strRadioId);
					tryAction(strAsthmaWorsen.equalsIgnoreCase("yes")?waitForClickableElement(getObject("input[type='radio'][name='"+strRadioId+"'][value='Yes']")):waitForClickableElement(getObject("input[type='radio'][name='"+strRadioId+"'][value='No']")),"Click","Is your asthma worsened by your occupation? "+strAsthmaWorsen );
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("none")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.notaLungSTFt),"chkCheck","Selected Values is None Of the Above ");
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("copd")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxCOPD),"chkCheck","Selected Values is Emphysema/COPD ");
				}
			}
			
			
			
			
			
			
			
/*			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
			tryAction(fluentWaitElements(PersonalStatementPageObjects.chkAsthma),"chkCheck","Asthma");
			tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radAsthmaWorseNo),"Click","Is your asthma worsened by your occupation?");*/
			
			
			
			
			
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBPCholNota),"Click","In the last 5 years have you suffered from");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBrainBoneNota),"Click","Have you ever suffered from, been diagnosed with or sought medical advice or treatment for?");
			if(strFamHist.equalsIgnoreCase("yes")){
				tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistYes),"Click","Family History - Yes");
			}else if(strFamHist.equalsIgnoreCase("no")){
				tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistNo),"Click","Family History - No");
			}else{
				tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistUnknown),"Click","Family History - Unknown");
			}		
			
			fillInLifeStyleDetails();


			//General Details
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radSufferFromAnyConditionNo),"CLICK","do you presently suffer from any condition?");
			fillInInsuranceDetails();
			sleep();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinuePersonalStatement),"Click","Calculate Quote");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
}
