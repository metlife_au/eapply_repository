package pages.metlife.Angular.HOSTPLUS;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_TransferYourCover extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_TransferYourCover TransferCover_HOSTPLUS() {
		TransferCoverHOSTPLUS();
		healthandlifestyledetails();
		confirmation();

		return new HOSTPLUS_Angular_TransferYourCover(scriptHelper);
	}

	public HOSTPLUS_Angular_TransferYourCover TransferCover_HOSTPLUS_Negative() {
		TransferCoverHOSTPLUS_Negative();

		return new HOSTPLUS_Angular_TransferYourCover(scriptHelper);
	}

	public HOSTPLUS_Angular_TransferYourCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

public void TransferCoverHOSTPLUS() {

		String EmailId = dataTable.getData("HOSTPLUS_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_TransferCover", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_TransferCover", "FifteenHoursWork");
		String Citizen = dataTable.getData("HOSTPLUS_TransferCover", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_TransferCover", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_TransferCover", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_TransferCover", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_TransferCover", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("HOSTPLUS_TransferCover", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_TransferCover", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_TransferCover", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("HOSTPLUS_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("HOSTPLUS_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("HOSTPLUS_TransferCover", "FormerFundNo");
		String DocumentaryEvidence = dataTable.getData("HOSTPLUS_TransferCover", "DocumentaryEvidence");
		String AttachmentSizeLimitTest	= dataTable.getData("HOSTPLUS_TransferCover", "AttachmentSizeLimitTest");
		String AttachmentTestPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachmentTestPath");
		String OverSizeMessage = dataTable.getData("HOSTPLUS_TransferCover", "OverSizeMessage");
		String AttachPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachPath");
		String CostType = dataTable.getData("HOSTPLUS_TransferCover", "CostType");
		String DeathYN = dataTable.getData("HOSTPLUS_TransferCover", "Death");
		String TPDYN = dataTable.getData("HOSTPLUS_TransferCover", "TPD");
		String IPYN = dataTable.getData("HOSTPLUS_TransferCover", "IP");
		String DeathAmount = dataTable.getData("HOSTPLUS_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("HOSTPLUS_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("HOSTPLUS_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("HOSTPLUS_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("HOSTPLUS_TransferCover", "BenefitPeriod");

		try{




			//****Click on Transfer Your Cover Link*****\\
			waitForPageTitle("testing", 10);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Transfer your cover']"))).click();
			report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);
			sleep(6000);


				sleep(3000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
			report.updateTestLog("Upload Document Evidence", "Upload Document Evidence continue button clicked", Status.PASS);


			if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(2000);

			}




			//*****Agree to Duty of disclosure*******\\
			waitForPageTitle("testing", 6);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
			sleep(400);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);




            //*****Agree to Privacy Statement*******\\


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

			sleep(200);

            //*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


			//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);



				//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}

				//***********Select the Gender******************\\

				if(Gender.equalsIgnoreCase("Male")){

					driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}

				//****Contact Details-Continue Button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);

				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\

					//*****Select the 15 Hours Question*******\\


				if(FifteenHoursWork.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

				//*****Resident of Australia****\\

				/*if(Citizen.equalsIgnoreCase("Yes")){
					driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

					}
				else{
					driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

					}*/

				//*****Industry********\\
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(800);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
				sleep(500);

				//*****Occupation*****\\

				Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(500);


				//*****Other Occupation *****\\

				if(OccupationType.equalsIgnoreCase("Other")){

					wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);
				}


				//*****Extra Questions*****\\

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			}
			}



			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



			//******Do you work in hazardous environment*****\\

			if(HazardousEnv.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[1]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			}

			//******Do you spend more than 10% of your working time outside of an office environment?*******\\

			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
			}


	          sleep(800);

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			        if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}
				}

		}
			//*****What is your annual Salary******


			    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
			    sleep(500);
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);



			//*****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


			//******************************************\\
			//**********PREVIOUS COVER SECTION**********\\
			//*******************************************\\

			//*****What is the name of your previous fund or insurer?*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
			sleep(250);
			report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

            //*****Please enter your fund member or insurance policy number*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
			sleep(250);
			report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

            //*****Please enter your former fund SPIN number (if known)*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
			sleep(250);
			report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);



			//*****Choose the cost of Insurance Type******\\

			Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
			CostofInsurance.selectByVisibleText(CostType);
			sleep(400);
			report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);


			//****Click on Continue Button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
			sleep(500);
			report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);


			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\

			//*****Death transfer amount******

			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);

			}

			//*****TPD transfer amount******

			if(TPDYN.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

			}

			//****IP transfer amount*****\\



			if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){


			//*****Select The Waiting Period********\\

				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);



			//*****Select The Benefit  Period********\\

				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);


			//****Enter the IP amount*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);

			}

			//****Equivalent units of Hostplus checkbox*****\\
		/*
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='equivalentUnit']/span"))).click();
			report.updateTestLog("Equivalent units of HostPlus scale", "Checkbox is selected", Status.PASS);*/

			//*****Click on calculate Quote*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
			sleep(3000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);




			//********************************************************************************************\\
			//************Capturing the premiums displayed************************************************\\
			//********************************************************************************************\\

			//*******Capturing the Death Cost********\\

			WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
			Death.click();
			sleep(300);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-150)", "");
			sleep(500);
			String DeathCost=Death.getText();
			report.updateTestLog("Death Cover amount", CostType+" Cost is "+DeathCost, Status.SCREENSHOT);
			sleep(500);

			//*******Capturing the TPD Cost********\\

			WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
			tpd.click();
			sleep(500);
			String TPDCost=tpd.getText();
			report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);

			//*******Capturing the IP Cost********\\

			WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
			ip.click();
			sleep(500);
			String IPCost=ip.getText();

			report.updateTestLog("IP Cover amount", CostType+" Cost is "+IPCost, Status.SCREENSHOT);

			//*****Scroll to the Bottom of the Page

		    jse.executeScript("window.scrollBy(0,250)", "");

		  //******Click on Continue******\\

		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[2]"))).click();
		 sleep(4500);
		 report.updateTestLog("Continue", "Continue button is selected", Status.PASS);





			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void healthandlifestyledetails() {

		String RestrictedIllness = dataTable.getData("HOSTPLUS_TransferCover", "RestrictedIllness");
		String PriorClaim = dataTable.getData("HOSTPLUS_TransferCover", "PriorClaim");
		String RestrictedFromWork = dataTable.getData("HOSTPLUS_TransferCover", "RestrictedFromWork");
		String DiagnosedIllness = dataTable.getData("HOSTPLUS_TransferCover", "DiagnosedIllness");
		String MedicalTreatment = dataTable.getData("HOSTPLUS_TransferCover", "MedicalTreatment");
		String DeclinedApplication = dataTable.getData("HOSTPLUS_TransferCover", "DeclinedApplication");
		String Fundpremloading = dataTable.getData("HOSTPLUS_TransferCover", "Fundpremloading");
		String Exclusiontext = dataTable.getData("HOSTPLUS_TransferCover", "Exclusion");
		WebDriverWait wait=new WebDriverWait(driver,20);

			try{

				if(driver.getPageSource().contains("Eligibility Check")){

			//*******Are you restricted due to illness or injury*********\\

				if(RestrictedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(2000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(2000);

				}

            //*******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits,
				//Workers� Compensation or any other form of compensation due to illness or injury?*********\\

				if(PriorClaim.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-Yes", Status.PASS);
					sleep(2000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-No", Status.PASS);
					sleep(2000);

				}

                //*******Have you been restricted from work or unable to perform any of your regular duties for more than
				//seven consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?*********\\

				if(RestrictedFromWork.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-Yes", Status.PASS);
					sleep(2000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-No", Status.PASS);
					sleep(2000);

				}


			//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy
		    //to less than 3 years?

				if(DiagnosedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
					sleep(2000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[4] "))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
					sleep(2000);
				}


				//*****Are you currently contemplating any medical treatment or advice for any illness or injury for which you
				//have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?


				if(MedicalTreatment.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
					sleep(2000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
					sleep(2000);
				}

				//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\

				if(DeclinedApplication.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[6]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Declined Application-Yes", Status.PASS);
					sleep(2000);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[6]"))).click();
					sleep(1000);
					report.updateTestLog("Health and Lifestyle Page", "Declined Application-No", Status.PASS);
					sleep(2000);
				}

				//*******Is your cover under the previous fund or individual insurer subject to any premium loadings
				//and/or exclusions, including but not limited to pre-existing conditions exclusions, or restrictions in
				//regards to medical or other conditions?

				if(Fundpremloading.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[7]"))).click();
					sleep(2000);
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-Yes", Status.PASS);
					sleep(500);
					  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']"))).sendKeys(Exclusiontext);
					    sleep(1500);
					    driver.findElement(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']")).click();

					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[7]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-No", Status.PASS);
					sleep(2000);
				}

			//******Click on Continue Button********\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(3500);



				}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void confirmation() {

	WebDriverWait wait=new WebDriverWait(driver,20);
	String AttachPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachPath");
	String DocumentaryEvidence = dataTable.getData("HOSTPLUS_TransferCover", "DocumentaryEvidence");
	String AttachmentSizeLimitTest	= dataTable.getData("HOSTPLUS_TransferCover", "AttachmentSizeLimitTest");
	String AttachmentTestPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachmentTestPath");
	String OverSizeMessage = dataTable.getData("HOSTPLUS_TransferCover", "OverSizeMessage");
		try{
			if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){

				//*****Do You want to Attach documentary evidence****\\


							driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
							report.updateTestLog("Attach Document", "Document Attached", Status.PASS);
							sleep(500);


						//*****Let the attachment load*****\\
						driver.findElement(By.xpath("//p[@class='color2']")).click();
						sleep(1000);

						//*****Add the Attachment*****\\

						WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
						sleep(1500);





		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='option2']/parent::*"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);




		//******Click on Submit*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitTransferCover()']"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(1000);

		}

		//*******Feedback Pop-up******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1500);






		//*****Fetching the Application Status*****\\

		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);
		report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);

		if(!Appstatus.equalsIgnoreCase("TRANSFER OF COVER")){
		//String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
		//System.out.println(Justification);
		sleep(400);

		//*****Fetching the Application Number******\\

		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);



		//******Download the PDF***********\\
		/*properties = Settings.getInstance();
		String StrBrowseName=properties.getProperty("currentBrowser");
		if (StrBrowseName.equalsIgnoreCase("Chrome")){*/
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			sleep(1500);
  		/*}


		//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();



	  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
	  			sleep(2500);
	  			Robot roboobj=new Robot();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  		}

	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
	  			Robot roboobj=new Robot();
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();

	  			sleep(4000);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_ENTER);

	  		}*/

		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);

		sleep(1000);


		  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);



		}


		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}


public void TransferCoverHOSTPLUS_Negative() {

	String EmailId = dataTable.getData("HOSTPLUS_TransferCover", "EmailId");
	String TypeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TypeofContact");
	String ContactNumber = dataTable.getData("HOSTPLUS_TransferCover", "ContactNumber");
	String TimeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TimeofContact");
	String Gender = dataTable.getData("HOSTPLUS_TransferCover", "Gender");
	String FifteenHoursWork = dataTable.getData("HOSTPLUS_TransferCover", "FifteenHoursWork");
	String Citizen = dataTable.getData("HOSTPLUS_TransferCover", "Citizen");
	String IndustryType = dataTable.getData("HOSTPLUS_TransferCover", "IndustryType");
	String OccupationType = dataTable.getData("HOSTPLUS_TransferCover", "OccupationType");
	String OtherOccupation = dataTable.getData("HOSTPLUS_TransferCover", "OtherOccupation");
	String OfficeEnvironment = dataTable.getData("HOSTPLUS_TransferCover", "OfficeEnvironment");
    String TertiaryQual = dataTable.getData("HOSTPLUS_TransferCover", "TertiaryQual");
	String HazardousEnv = dataTable.getData("HOSTPLUS_TransferCover", "HazardousEnv");
	String OutsideOfcPercent = dataTable.getData("HOSTPLUS_TransferCover", "OutsideOfcPercent");
	String AnnualSalary = dataTable.getData("HOSTPLUS_TransferCover", "AnnualSalary");
	String PreviousInsurer = dataTable.getData("HOSTPLUS_TransferCover", "PreviousInsurer");
	String PolicyNo = dataTable.getData("HOSTPLUS_TransferCover", "PolicyNo");
	String FormerFundNo = dataTable.getData("HOSTPLUS_TransferCover", "FormerFundNo");	String CostType = dataTable.getData("HOSTPLUS_TransferCover", "CostType");
	String DeathYN = dataTable.getData("HOSTPLUS_TransferCover", "Death");
	String TPDYN = dataTable.getData("HOSTPLUS_TransferCover", "TPD");
	String DeathAmount = dataTable.getData("HOSTPLUS_TransferCover", "DeathAmount");
	String TPDAmount = dataTable.getData("HOSTPLUS_TransferCover", "TPDAmount");
	String TPDAmtGreater = dataTable.getData("HOSTPLUS_TransferCover", "TPDAmtGreater");

	String Deathlimiterror = dataTable.getData("HOSTPLUS_TransferCover", "Deathlimiterror");
	String TPDlimiterror = dataTable.getData("HOSTPLUS_TransferCover", "TPDlimiterror");
	String IPlimiterror = dataTable.getData("HOSTPLUS_TransferCover", "IPlimiterror");
	String WaitingPeriod = dataTable.getData("HOSTPLUS_TransferCover", "WaitingPeriod");
	String BenefitPeriod = dataTable.getData("HOSTPLUS_TransferCover", "BenefitPeriod");
	String IPYN = dataTable.getData("HOSTPLUS_TransferCover", "IP");
	String IPAmount = dataTable.getData("HOSTPLUS_TransferCover", "IPAmount");

	try{
		//****Click on Transfer Your Cover Link*****\\
		waitForPageTitle("testing", 10);
		WebDriverWait waiting=new WebDriverWait(driver,25);
		//****Click on Transfer Your Cover Link*****\\
		WebDriverWait wait=new WebDriverWait(driver,18);
		waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Transfer your cover']"))).click();
		report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);
		sleep(4000);
		if(driver.getPageSource().contains("The supported file formats are *.pdf, *.txt, *.doc, *.docx,* .xls, *.xlsx, *.tif, *.png, *.jpg and *.jpeg.")){
			sleep(500);
			quickSwitchWindows();
			sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
		report.updateTestLog("Upload Document Evidence", "Upload Document Evidence continue button clicked", Status.PASS);
		}

		if(driver.getPageSource().contains("You have previously saved application(s).")){
			sleep(500);
			quickSwitchWindows();
			sleep(1000);
			driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
			report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
			sleep(2000);

		}

		//*****Agree to Duty of disclosure*******\\
		waitForPageTitle("testing", 6);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
		sleep(600);
		report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);




        //*****Agree to Privacy Statement*******\\


		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
		sleep(250);
		report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

		sleep(200);

        //*****Enter the Email Id*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
		sleep(250);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
		report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
		sleep(500);

		//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


		//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);



			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}

			//***********Select the Gender******************\\

			if(Gender.equalsIgnoreCase("Male")){

				driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}

			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);

			//***************************************\\
			//**********OCCUPATION SECTION***********\\
			//****************************************\\

				//*****Select the 15 Hours Question*******\\


			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}

			//*****Resident of Australia****\\

			/*if(Citizen.equalsIgnoreCase("Yes")){
				driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

				}
			else{
				driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

				}*/

			//*****Industry********\\
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
		//	sleep(500);

			//*****Occupation*****\\

			Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);


			//*****Other Occupation *****\\

			if(OccupationType.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}


			//*****Extra Questions*****\\

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

		}

		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}
		}



		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



		//******Do you work in hazardous environment*****\\

		if(HazardousEnv.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[1]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}

		//******Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?*******\\

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}


          sleep(800);

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		        if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			}
			}

	}
		//*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
		    waitForPageTitle("testing", 3);


		//*****Click on Continue*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


		//******************************************\\
		//**********PREVIOUS COVER SECTION**********\\
		//*******************************************\\

		//*****What is the name of your previous fund or insurer?*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
		sleep(250);
		report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

        //*****Please enter your fund member or insurance policy number*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
		sleep(250);
		report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

        //*****Please enter your former fund SPIN number (if known)*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
		sleep(250);
		report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

		//*****Do You want to Attach documentary evidence****\\



			//****Attach the file******
		//*****Do You want to Attach documentary evidence****\\



		//*****Choose the cost of Insurance Type******\\

		Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
		CostofInsurance.selectByVisibleText(CostType);
		sleep(400);
		report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);


		//****Click on Continue Button******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
		sleep(500);
		report.updateTestLog("Continue Button", "Continue Button is selected", Status.PASS);


		//*************************************************\\
		//**************TRANSFER COVER SECTION***************\\
		//**************************************************\\


		//*****Death transfer amount******

		if(!DeathYN.equalsIgnoreCase("Yes")&&TPDYN.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
			sleep(500);
			report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);


		//*****TPD transfer amount******

			wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
			sleep(500);
			report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);


		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[text()='Cover amount'])[2]"))).click();
		sleep(1500);


			//****Enter the IP amount*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);

//*****Click on calculate Quote*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
			sleep(3000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);
			String TPDError=driver.findElement(By.xpath("//*[@class='error ng-binding ng-scope']")).getText();

			if(TPDError.equalsIgnoreCase(TPDAmtGreater)){
				report.updateTestLog("TPD Error", "Expected Error Message: "+TPDAmtGreater, Status.PASS);
			}
			else{
				report.updateTestLog("TPD Error", "Error message is not displayed", Status.FAIL);
			}
	//		}
		}
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}

}
