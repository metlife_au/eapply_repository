(function(angular){
	'use strict';
    /* global channel, moment, Date */
	angular
		.module('MtaaApp')
		.controller('quoteCancel', quoteCancel);
	quoteCancel.$inject=['$scope','$rootScope','$stateParams','$location','$http','$timeout','$window','urlService','ngDialog','persoanlDetailService','deathCoverService', 'tpdCoverService', 'ipCoverService','PersistenceService','mtaaUIConstants'];
	function quoteCancel($scope, $rootScope, $stateParams, $location, $http, $timeout, $window, urlService, ngDialog, persoanlDetailService, deathCoverService, tpdCoverService, ipCoverService, PersistenceService,mtaaUIConstants){
		
		var vm = this;
		var fetchAppnum = true;
		var appNum;
		vm.TPD = {
				'requestType':'CANCOVER',
				'partnerCode':'MTAA',
				'lastSavedOn':'cancelcoverpage',
				'applicant': {
					'cover':[{},{},{}],
					'contactDetails':{}
				}
		};
		vm.TPD.coverCancellation = {
				death : true,
				tpd : true,
				incomeProtection : true				
		};
		
		vm.showCover = {
				death : true,
				tpd : true,
				incomeProtection : true				
		};
		
		vm.constants = {
				image: mtaaUIConstants.path.images,
				phoneNumber: mtaaUIConstants.onboardDetails.phoneNumber,
				emailFormat: mtaaUIConstants.onboardDetails.emailFormat,
				postCodeFormat: mtaaUIConstants.onboardDetails.postCodeFormat,
				contactTypeOption: mtaaUIConstants.onboardDetails.contactTypeOption,
				titleOption: mtaaUIConstants.onboardDetails.titleOption,
				addressTypeOption: mtaaUIConstants.onboardDetails.addressTypeOption,
				stateOptions: mtaaUIConstants.onboardDetails.stateOptions
		};
		
		vm.init = function(){
			
			vm.urlList = urlService.getUrlList();
			vm.inputDetails = persoanlDetailService.getMemberDetails();
			vm.userAge = parseInt(moment().diff(moment(vm.inputDetails.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
			vm.ageLimit=vm.userAge;
			
			//setting value to JSON
			vm.TPD.applicant.memberType = vm.inputDetails.memberType;
			vm.TPD.applicant.firstName = vm.inputDetails.personalDetails.firstName;
			vm.TPD.applicant.lastName = vm.inputDetails.personalDetails.lastName;
			//vm.TPD.applicant.gender = vm.inputDetails.personalDetails.gender;
			vm.TPD.applicant.birthDate =  vm.inputDetails.personalDetails.dateOfBirth;
			vm.TPD.applicant.dateJoinedFund = vm.inputDetails.dateJoined;
			vm.TPD.applicant.emailId = vm.inputDetails.contactDetails.emailAddress;
			vm.TPD.applicant.age = vm.userAge;
			
			//address
			vm.TPD.applicant.contactDetails.preferedContactTime = vm.inputDetails.contactDetails.prefContactTime;
			vm.TPD.applicant.contactDetails.preferedContacType = vm.inputDetails.contactDetails.prefContact;
			
			vm.TPD.applicant.contactDetails.addressLine1 = vm.inputDetails.address.line1;
			vm.TPD.applicant.contactDetails.addressLine2 = vm.inputDetails.address.line2;
			vm.TPD.applicant.contactDetails.postCode = vm.inputDetails.address.postCode;
			vm.TPD.applicant.contactDetails.country = vm.inputDetails.address.country;
			vm.TPD.applicant.contactDetails.state = vm.inputDetails.address.state;
			vm.TPD.applicant.contactDetails.suburb = vm.inputDetails.address.suburb;
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
			
			vm.TPD.clientRefNumber = vm.inputDetails.clientRefNumber;
			vm.TPD.applicant.clientRefNumber = parseInt(vm.inputDetails.clientRefNumber);
			vm.doj = vm.inputDetails.dateJoined;
			vm.TPD.applicant.customerReferenceNumber = vm.inputDetails.contactDetails.fundEmailAddress;	
			 
			vm.deathCoverTransDetails = deathCoverService.getDeathCover();
			vm.tpdCoverTransDetails = tpdCoverService.getTpdCover();
			vm.ipCoverTransDetails = ipCoverService.getIpCover();
			
			vm.TPD.applicant.cover[0].occupationRating = vm.deathCoverTransDetails.occRating;
			vm.TPD.applicant.cover[1].occupationRating = vm.tpdCoverTransDetails.occRating;
			vm.TPD.applicant.cover[2].occupationRating = vm.ipCoverTransDetails.occRating;
			
			vm.TPD.applicant.cover[0].frequencyCostType = 'Weekly';
			vm.TPD.applicant.cover[1].frequencyCostType = 'Weekly';
			vm.TPD.applicant.cover[2].frequencyCostType = 'Weekly';
			
			if(vm.deathCoverTransDetails.type === '1'){
				vm.TPD.applicant.cover[0].coverCategory = 'DcUnitised';
			}else{
				vm.TPD.applicant.cover[0].coverCategory = 'DcFixed';
			}
			
			if(vm.tpdCoverTransDetails.type === '1'){
				vm.TPD.applicant.cover[1].coverCategory = 'TPDUnitised';
			}else{
				vm.TPD.applicant.cover[1].coverCategory = 'TPDFixed';
			}
			
			if(vm.ipCoverTransDetails.type === '1'){
				vm.TPD.applicant.cover[2].coverCategory = 'IpUnitised';
			}else{
				vm.TPD.applicant.cover[2].coverCategory = 'IpFixed';
			}
			
			//benefit type
			vm.TPD.applicant.cover[0].benefitType = vm.deathCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[1].benefitType = vm.tpdCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[2].benefitType = vm.ipCoverTransDetails.benefitType;
			
			if(vm.deathCoverTransDetails.amount){
				vm.TPD.applicant.cover[0].existingCoverAmount = vm.deathCoverTransDetails.amount;
		    }
			if(vm.deathCoverTransDetails.units){
				vm.TPD.applicant.cover[0].fixedUnit = vm.deathCoverTransDetails.units;
		    }
			if(vm.tpdCoverTransDetails.amount){
				vm.TPD.applicant.cover[1].existingCoverAmount = vm.tpdCoverTransDetails.amount;
		    }
			if(vm.tpdCoverTransDetails.units){
				vm.TPD.applicant.cover[1].fixedUnit = vm.tpdCoverTransDetails.units;
		    }
			if(vm.ipCoverTransDetails.amount){
				vm.TPD.applicant.cover[2].existingCoverAmount = vm.ipCoverTransDetails.amount;
				vm.TPD.applicant.cover[2].fixedUnit = vm.ipCoverTransDetails.units;
				
				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].totalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].totalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
			}
			
			//hide cover if no existing amount
			
			if(parseInt(vm.TPD.applicant.cover[0].existingCoverAmount) === 0){
				vm.TPD.coverCancellation.death = false;
				vm.showCover.death = false;
			}
			
			if(parseInt(vm.TPD.applicant.cover[1].existingCoverAmount) === 0){
				vm.TPD.coverCancellation.tpd = false;
				vm.showCover.tpd = false;
			}
			
			if(parseInt(vm.TPD.applicant.cover[2].existingCoverAmount) === 0){
				vm.TPD.coverCancellation.incomeProtection = false;
				vm.showCover.incomeProtection = false;
			}		
			
			
			//benefit type
			vm.TPD.applicant.cover[0].benefitType = vm.deathCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[1].benefitType = vm.tpdCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[2].benefitType = vm.ipCoverTransDetails.benefitType;
			
			
			if($stateParams.mode === '2'){
				
				vm.TPD = PersistenceService.getCancelCoverDetails();
				
				$timeout(function() {
					$('#coverCancellationSection').collapse('show');
					
					if(vm.TPD.coverCancellation.death){
						$('#deathLable').addClass('active');
					}else{
						$('#deathLable').removeClass('active');
					}
					
					if(vm.TPD.coverCancellation.tpd){
						$('#tpdLable').addClass('active');
					}else{
						$('#tpdLable').removeClass('active');
					}
					
					if(vm.TPD.coverCancellation.incomeProtection){
						$('#ipLable').addClass('active');
					}else{
						$('#ipLable').removeClass('active');
					}
					
				},100);
				
			}else{
				$timeout(function() {
					$('#coverCancellationSection').collapse('hide');
					$('#deathLable').addClass('active');
					$('#tpdLable').addClass('active');
					$('#ipLable').addClass('active');
				},100);
			}
			
			
		};
		
		vm.init();
		
		vm.go = function (path){
			$location.path( path );
	  	};
	  	
	  	vm.changePrefContactType = function(){
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
		};
	  	
	  	vm.checkCoverCancellation = function(cover){
	  		
	  		if(cover === 'death'){
	  			vm.TPD.coverCancellation.death = !vm.TPD.coverCancellation.death;
	  		}else if(cover === 'tpd'){
	  			vm.TPD.coverCancellation.tpd = !vm.TPD.coverCancellation.tpd;
	  		}else if(cover === 'incomeProtection'){
	  			vm.TPD.coverCancellation.incomeProtection = !vm.TPD.coverCancellation.incomeProtection;
	  		}
	  		
	  		$timeout(function() {
		  		if(vm.TPD.coverCancellation.death === true){
		  			vm.TPD.coverCancellation.tpd = true;
		  			$('#tpdLable').addClass('active');
		  		}
	  		},10);
	  	};
	  	
	  	vm.fetchAppnum = function(){
        	
        	if(fetchAppnum){
        		fetchAppnum = false;
        		appNum = PersistenceService.getAppNumber();
        		vm.TPD.applicatioNumber = appNum;
        	}        	
        };
	  	
	  	vm.submitForm = function(){
	  		
	  		vm.fetchAppnum();
	  		
	  		if(vm.coverCancellationForm.$valid && vm.coverDetailsTransferForm.$valid){
	  			PersistenceService.setCancelCoverDetails(vm.TPD);
		  		vm.go('/cancelSummary');
	  		}	  		
	  	};
	  	
	  	vm.clickToOpen = function (hhText) {
	    	
	    	var templateContent = '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4>';
	    	templateContent += '<div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p>';
	    	templateContent += '<div id="tips_text">'+hhText+'</div><p></p></div></div></div>';
	    	templateContent += '<div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12">';
	    	templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>';
	    	
	    	

			var dialog = ngDialog.open({
				template: templateContent,
				className: 'ngdialog-theme-plain',
				plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		  
	  };
	  	
	  	 vm.isCollapsible = function(targetEle, event) {
	  		 
	  		 var coverDetailsTransferFormValid = vm.coverDetailsTransferForm.$valid;
	  		 var stopPropagation = false;
	  		 
	  		 if(targetEle === 'coverCancellationSection' && !coverDetailsTransferFormValid){
	  			 vm.coverDetailsTransferForm.$submitted = true;
	  			 stopPropagation = true;	 
	  		 }
	  		 
	  		 if(stopPropagation){
	  			 event.stopPropagation();
	  			 return false;	  			 
	  		 }
		       
	  	 };
	  	
	  //watchers for form valid and invalid
	  	
	  	$scope.$watch('vm.coverDetailsTransferForm.$valid', function(newVal, oldVal) {
      	  if(newVal) {
      		  $('#coverCancellationSection').collapse('show');
      		  
      	  }
        });
        
        $scope.$watch('vm.coverDetailsTransferForm.$invalid', function(newVal, oldVal) {
      	  if(newVal) {
      		  $('#coverCancellationSection').collapse('hide');    		  
      	  }
        });
		
	}
})(angular);

 /* Cancel Cover Controller,Progressive and Mandatory validations Ends  */
