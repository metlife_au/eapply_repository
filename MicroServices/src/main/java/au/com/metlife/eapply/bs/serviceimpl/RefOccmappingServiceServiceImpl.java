package au.com.metlife.eapply.bs.serviceimpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.bs.model.RefOccmapping;
import au.com.metlife.eapply.bs.model.RefOccmappingRepository;
import au.com.metlife.eapply.bs.service.RefOccmappingService;

@Service
public class RefOccmappingServiceServiceImpl implements RefOccmappingService {
	private static final Logger log = LoggerFactory.getLogger(RefOccmappingServiceServiceImpl.class);

	 private final RefOccmappingRepository repository;

	 @Autowired
	    public RefOccmappingServiceServiceImpl(final RefOccmappingRepository repository) {
	        this.repository = repository;
	    }
	 
	
	public RefOccmappingRepository getRepository() {
		return repository;
	}

	
	
	
   @Override
	public List<RefOccmapping> getNewOccupationName(String fundId, String occName) {
	   log.info("Get new occupation name start");
	   List<RefOccmapping> refOccmapping=repository.findByFundIdAndOccName(fundId, occName);
	   log.info("Get new occupation name finish");
	   return refOccmapping;
	}
}
