/*package vicsuper;

public class UpdateOccupation {

}*/
package vicsuper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import vicsSuper_Objects.LandingPageObject;
import vicsSuper_Objects.UpdateOccupationObject;

public class UpdateOccupation extends MasterPage {
	WebDriverWait wait = new WebDriverWait(driver, 20);
	WebDriverUtil driverUtil = null;
//	JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	public UpdateOccupation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	String XML;
	String SenarioType;
	String DataSheet;
	String occRating1;
	String occRating2;
	String occRating3;

	String SecondPage = null;

	// Actions action = new Actions(driver.getWebDriver());

	// UpdateOccupationObject PerDet= new UpdateOccupationObject(scriptHelper);

	public UpdateOccupation UpdateYourOccupation_vicsuper(String DataSheetName, String Type) {

		SenarioType = Type;
		// PerDet.UpdateOccupationObjectInitialization(AppPage);
		XML = dataTable.getData("VicSuper_Login", "XML");
		DataSheet = DataSheetName;
		if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			occupationRatingInputValidation();
		}  else {
			report.updateTestLog("Error Type", "Invalid Type value-" + SenarioType, Status.FAIL);
		}
		PersonalDeatils();
		return new UpdateOccupation(scriptHelper);

	}

	public void PersonalDeatils() {
		String DutyOfDisclosure = dataTable.getData(DataSheet, "DutyOfDisclosure");
		String PrivacyStatement = dataTable.getData(DataSheet, "PrivacyStatement");
		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
		String Citizen = dataTable.getData(DataSheet, "Citizen");
		String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
		String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
		String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
		String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");

		try {

			sleep(500);

			wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.updateOccupation)).click();
			sleep(4000);
			// Popup Validation
			/*
			 * if (driver.findElement(By.xpath("//button[text()='Continue']")).
			 * isDisplayed() == true) {
			 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
			 * "//button[text()='Continue']"))).click(); sleep(5000); }
			 */
			sleep(5000);

			// -----------------------------------------------------PersonalDetails------------------------------------------------------------------------//
			// *****Agree to Duty of disclosure*******\\

			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_dutyOfDisclosure));


			String[] ArrayHeaderDD;
			int flag = 1;

			String part2;
			String[] value;
			String HeaderDD = null;

			String[] parts = DutyOfDisclosure.split("---");
			String DDTxt = driver.findElement(By.xpath(".//*[@name='formduty']//p[2]/strong")).getText();
			 if (DDTxt.equalsIgnoreCase(parts[0])) {
				}
				else {
					flag =0;
					report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
				}

			parts = parts[1].split("\\n\\n");

			int j=3;
			for (int a = 0; a < 9; a++) {

					if ( !(a==3) ) {
						HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//p)["+j+"]")).getText();
						j++;

						 if (HeaderDD.equalsIgnoreCase(parts[a])) {
							}
							else {
								flag =0;
								report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}


					}
				else {
					String[] parts1 = parts[a].split("\\n");
					int k=1;
					for (int b = 0; b < 5; b++) {

						System.out.println("K="+k);
						if ( !(b==4) ) {
							 HeaderDD = driver.findElement(By.xpath(".//*[@name='formduty']//ul/li["+k+"]")).getText();


						}
						else {
							 HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//strong)[2]")).getText();

						}
						 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

						 }
							else {
								flag =0;
								report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}

						k++;

					}

				}


			}



					if (flag==0) {
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is not as expected", Status.FAIL);
					}
					else {
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is present as expected", Status.PASS);

					}


			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_dutyOfDisclosure))
					.click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			// *****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_privacyStatement));



			parts = PrivacyStatement.split("---");
			DDTxt = driver.findElement(By.xpath(".//*[@name='cancelprivacyPolicyForm']//p[2]/strong")).getText();
			 if (DDTxt.equalsIgnoreCase(parts[0])) {

				}
				else {
					flag =0;
					report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
				}

			parts = parts[1].split("\\n\\n\\n");
			 j=3;
				for (int a = 0; a < 2; a++) {

			HeaderDD = driver.findElement(By.xpath("(.//*[@name='cancelprivacyPolicyForm']//p)["+j+"]")).getText();

			 if (HeaderDD.equalsIgnoreCase(parts[a])) {

				}
				else {
					flag =0;
					report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
				}
			j++;
			}

				if (flag==0) {
					report.updateTestLog("Privacy statement", "Privacy statement - Statement is not as expected", Status.FAIL);

				}
				else {
					report.updateTestLog("Privacy statement", "Privacy statement - Statement is present as expected", Status.PASS);
				}


			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_privacyStatement))
					.click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_email)).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_email))
					.sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
			sleep(300);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(UpdateOccupationObject.obj_PersonalDetails_contactType));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_phNO))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_phNO))
					.sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {
				wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_Morning))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			} else if (TimeofContact.equalsIgnoreCase("Afternoon")) {
				wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_Afternoon))
						.click();
				sleep(500);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for TimeofContact", Status.FAIL);
			}

			/*
			 * //***********Select the Gender******************\\
			 *
			 * if (Gender.equalsIgnoreCase("Male")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * UpdateOccupationObject.obj_PersonalDetails_Morning)).click();
			 * sleep(250); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS); } else if
			 * (Gender.equalsIgnoreCase("Female")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * UpdateOccupationObject.obj_PersonalDetails_Afternoon)).click();
			 * sleep(500); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS);
			 *
			 * } else { report.updateTestLog("DataTable",
			 * "Invalid value for TimeofContact", Status.FAIL); }
			 */

			// ****Contact Details-Continue Button*****\\

						wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_Continue_ContactDetail)).click();
						report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
						sleep(1200);

			// -----------------------------------------------------Occupation------------------------------------------------------------------------//

			// *****Select the 14 Hours Question*******\\

			if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_FourteenHrsYes))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
			} else if (FourteenHoursWork.equalsIgnoreCase("No")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_FourteenHrsNo))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for FourteenHoursWork", Status.FAIL);
			}

			// *****Resident of Australia****\\
			if (Citizen.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_CitizenYes))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
			} else if (Citizen.equalsIgnoreCase("No")) {
				wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_CitizenNo))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
			} else {
				report.updateTestLog("DataTable", "Invalid value for Citizen", Status.FAIL);
			}



			// *****Are the duties of your regular occupation limited to
			// either:*****\\

			if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

				// ***Select duties which are undertaken within an Office
				// Environment?\\
				if (EducationalInstitution.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AddQuest1aYes)).click();
					report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalInstitution.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AddQuest1aNo)).click();
					report.updateTestLog("Educational institution", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalInstitution", Status.FAIL);
				}

				// *****Educational duties performed within a school or other
				// educational institution******\\
				if (EducationalDuties.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AddQuest1bYes)).click();
					report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalDuties.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AddQuest1bNo)).click();
					report.updateTestLog("Educational duties", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalDuties", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

			}

			// Do you hold a tertiary qualification or work in a management role
			if (driver.getPageSource().contains("Do you:")) {

				// *****Do You Hold a Tertiary Qualification******\\
				if (TertiaryQual.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AddQuest2Yes)).click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (TertiaryQual.equalsIgnoreCase("No")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AddQuest2No))
							.click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for TertiaryQual", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Do you hold a tertiary qualification or work in a management role is not present",
						Status.FAIL);
			}

			// *****What is your annual Salary******

			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AnnualSal))
					.clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AnnualSal))
					.sendKeys(AnnualSalary);
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_AnnualSal))
					.sendKeys(Keys.ENTER);
			sleep(1000);
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);

			// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//
			if (SenarioType.equalsIgnoreCase("OccupationRating")) {
				// *****Click on Continue*******\\
				wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_PersonalDetails_Continue)).click();
				sleep(4000);
				report.updateTestLog("PersonalDetails - Continue", "PersonalDetails - Continue button is clicked",
						Status.PASS);
				confirmation();
			}

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}



	private void confirmation() {
		sleep(3000);
		System.out.println("confirmation");
		String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
		String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");
		String Confirmation_DetailsValidations = dataTable.getData(DataSheet, "Confirmation_DetailsValidations");
		String GeneralConsent = dataTable.getData(DataSheet, "GeneralConsent");
		System.out.println(Confirmation_DetailsValidations);
		if (Confirmation_DetailsValidations.equalsIgnoreCase("Yes")
				|| Confirmation_DetailsValidations.equalsIgnoreCase("Occupation")) {
			detailsValidation();
		}


			try {


					// ******Agree to general Consent*******\\

				String part2;
				String[] value;
				String HeaderDD = null;
				int flag=1;
						wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_Confirmation_TermsConditionsCheckBox));
						String[] parts = GeneralConsent.split("\\n\\n");



						 int j=1;
						for (int a = 0; a < 4; a++) {

							if ( !(a==3) ) {
								HeaderDD = driver.findElement(By.xpath("(.//*[@id='collapse4']/div/div/div/p)["+j+"]")).getText();
								j++;

								 if (HeaderDD.equalsIgnoreCase(parts[a])) {

									}
									else {
										flag =0;
										report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
									}

							}
							else {
								System.out.println("<><><><><><>");

								String[] parts1 = parts[a].split("\\n");
								System.out.println("1)"+parts1[0]);
								System.out.println("2)"+parts1[1]);
								System.out.println("3)"+parts1[2]);
								System.out.println("4)"+parts1[3]);
								System.out.println("5)"+parts1[4]);
								System.out.println("6)"+parts1[5]);
								System.out.println("7)"+parts1[6]);
								System.out.println("8)"+parts1[7]);
								System.out.println("9)"+parts1[8]);
								System.out.println("10)"+parts1[9]);


								int k=1;
								for (int b = 0; b < 10; b++) {


										 HeaderDD = driver.findElement(By.xpath(".//*[@id='collapse4']/div/div/div/ul/li["+k+"]")).getText();

									 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

									 }
										else {
											flag =0;
											report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
										}

									k++;

								}

							}
						}

						if (flag==0) {
							report.updateTestLog("General Consent", "General Consent - Statement is not as expected", Status.FAIL);

						}
						else {
							report.updateTestLog("General Consent", "General Consent - Statement is present as expected", Status.PASS);
						}

						sleep(2000);


					wait.until(ExpectedConditions
							.elementToBeClickable(UpdateOccupationObject.obj_Confirmation_TermsConditionsCheckBox)).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(3000);

					wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_Confirmation_Submit))
					.click();
					report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
					sleep(6500);


					// *******Feedback popup******\\

					wait.until(ExpectedConditions.elementToBeClickable(UpdateOccupationObject.obj_DecisionPopUP_No)).click();
					report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
					sleep(2500);



					String Appstatus = driver.findElement(UpdateOccupationObject.obj_DecisionPopUP_Appstatus).getText();
					sleep(500);


					if (Appstatus.equalsIgnoreCase(DecisionHeader)) {
						report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
					}


			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}

	}

	public void detailsValidation() {


			String Gender = dataTable.getData(DataSheet, "Gender");
			String EmailId = dataTable.getData(DataSheet, "EmailId");
			String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
			String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
			String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
			String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
			String Citizen = dataTable.getData(DataSheet, "Citizen");
			String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
			String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
			String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
			String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");


			// String DeathAmount = dataTable.getData(DataSheet,
			// "DeathAmount");



			AnnualSalary = "$" + AnnualSalary;


			String XML = dataTable.getData("VicSuper_Login", "XML");

			String[] part;
			String part2;
			String[] value;

			// To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1];
			value = part2.split("</dateOfBirth>");
			String dob = value[0];


		/*	// First Name
			WebElement objFName = driver.findElement(UpdateOccupationObject.obj_Confirmation_FirstName);
			fieldValidation("WebElement", objFName,  VicSuper__Login.firstName, "First Name");

			// Last Name
			WebElement objLName = driver.findElement(UpdateOccupationObject.obj_Confirmation_LastName);
			fieldValidation("WebElement", objLName, VicSuper__Login.lastName, "Last Name");*/

			// Gender
			WebElement ObjGender = driver.findElement(UpdateOccupationObject.obj_Confirmation_Gender);
			fieldValidation("WebElement", ObjGender, Gender, "Genderquestion");

			// Phone Number type
			WebElement objcontactType = driver.findElement(UpdateOccupationObject.obj_Confirmation_ContactType);
			fieldValidation("WebElement", objcontactType, TypeofContact, "contactType");

			// PhoneNumber
			WebElement objContactNo = driver.findElement(UpdateOccupationObject.obj_Confirmation_ContactNo);
			fieldValidation("WebElement", objContactNo, ContactNumber, "ContactNo");

			// Email
			WebElement objcontactEmail = driver.findElement(UpdateOccupationObject.obj_Confirmation_Email);
			fieldValidation("WebElement", objcontactEmail, EmailId, "Email");
			js.executeScript("window.scrollBy(0,500)", "");
			// DOB
			WebElement objDOB = driver.findElement(UpdateOccupationObject.obj_Confirmation_DOB);
			fieldValidation("WebElement", objDOB, dob, "DOB");

			// Morning Afternoon
			WebElement objTime = driver.findElement(UpdateOccupationObject.obj_Confirmation_Time);
			String eletext = objTime.getText();
			System.out.println(eletext);
			System.out.println(TimeofContact);
			if (eletext.contains(TimeofContact)) {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is displayed",
						Status.PASS);

			} else {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is not displayed",
						Status.FAIL);

			}

			// Do you work more than 14 hours per week?

			WebElement ObjWorkHRS = driver.findElement(UpdateOccupationObject.obj_Confirmation_HoursWork);
			fieldValidation("WebElement", ObjWorkHRS, FourteenHoursWork, "FourteenHoursWork");

			// Are you a citizen or permanent resident of Australia? Yes No

			WebElement objCitizen = driver.findElement(UpdateOccupationObject.obj_Confirmation_Citizen);
			fieldValidation("WebElement", objCitizen, Citizen, "Citizen");

			// office Duties are Undertaken within an Office Environment?
			WebElement eduIns = driver.findElement(UpdateOccupationObject.obj_Confirmation_AddQuest1a);
			fieldValidation("WebElement", eduIns, EducationalInstitution,
					"Are the duties of your regular occupation limited to Question1");

			WebElement eduDut = driver.findElement(UpdateOccupationObject.obj_Confirmation_AddQuest1b);
			fieldValidation("WebElement", eduDut, EducationalDuties,
					"Are the duties of your regular occupation limited to Question2");

			// TertiaryQualification / ManagementRole

			WebElement objQual = driver.findElement(UpdateOccupationObject.obj_Confirmation_TertiaryQualification);
			fieldValidation("WebElement", objQual, TertiaryQual, "TertiaryQualification / ManagementRole");

			// Current annual salary?
			WebElement objannualSalCCId = driver.findElement(UpdateOccupationObject.obj_Confirmation_AnnualIncome);
			fieldValidation("Amount Validation", objannualSalCCId, AnnualSalary, "AnnualSalary");

			js.executeScript("window.scrollBy(0,500)", "");

	}

	public void occupationRatingInputValidation() {
		System.out.println("starttttttttt-" + DataSheet);
		String ExistingDeath = dataTable.getData(DataSheet, "ExistingDeath");
		String ExistingTPD = dataTable.getData(DataSheet, "ExistingTPD");
		String ExistingIP = dataTable.getData(DataSheet, "ExistingIP");
		System.out.println("enddddddddddddd");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;

		String[] value;

		// To get firstName
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(ExistingDeath))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + ExistingDeath + ".", Status.FAIL);
		}
		if (!(occRating2.equals(ExistingTPD))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + ExistingTPD + ".", Status.FAIL);
		}
		if (!(occRating3.equals(ExistingIP))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + ExistingIP + ".", Status.FAIL);
		}
		System.out.println("///valend");
	}


}
