package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.CoverDetailsPageObjects;
import uimap.metlife.OAMPSGetQuotePageObjects;
import uimap.metlife.STFTGetQuotePageObjects;

public class OAMPSGetQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public OAMPSGetQuotePage getQuote() {
		premiumCalculation();
		return new OAMPSGetQuotePage(scriptHelper);
	}

	public OAMPSGetQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void premiumCalculation() {

		String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCitizen = dataTable.getData("MetLife_Data", "Citizen");
		String strIndustry = dataTable.getData("MetLife_Data", "Industry");
		String stroccupation = dataTable.getData("MetLife_Data", "Occupation");
		String strlifeCoverAmt = dataTable.getData("MetLife_Data", "LifeCoverAmount");
		String strTotalCoverAmt = dataTable.getData("MetLife_Data", "TotalCoverAmount");

		try {
			
			Thread.sleep(2000);
			driver.manage().window().maximize();			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.txtDOB), "SET", "Date of birth", dob);
			Thread.sleep(1000);
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.txtDOB), "TAB", "Date of birth");
			
			Thread.sleep(1000);
			
			tryAction(gender.equalsIgnoreCase("Male")?waitForClickableElement(OAMPSGetQuotePageObjects.rdMale):waitForClickableElement(OAMPSGetQuotePageObjects.rdFemale),"clkradio","Gender -" + gender);
			sleep();
			
			/*if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
			} */
			
			Thread.sleep(1000);
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.rdSmokedIn12MthsNo), "clkradio",
						"Have you Smoked in 12 months - Yes");
			} else {
				tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.rdSmokedIn12MthsNo), "clkradio",
						"Have you Smoked in 12 months - No");
			}
			
			Thread.sleep(1000);
			
			if (strCitizen.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.rdCitizenYes), "Click",
						"Are you Citizen or PR - Yes");

			} else {
				tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.rdCitizenYes), "Click",
						"Are you Citizen or PR - No");
			}
			
			Thread.sleep(1000);
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.selIndustry), "DropDownSelect", "Industry",strIndustry);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.selOccupation), "DropDownSelect", "Occupation",stroccupation);		
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.selLifeInsurance), "DropDownSelect", "Cover Amount",strlifeCoverAmt);
			
			Thread.sleep(1000);
			tryAction(waitForClickableElement(OAMPSGetQuotePageObjects.selDisability), "DropDownSelect", "Disability Amount", strTotalCoverAmt);
			Thread.sleep(1000);

//			Actions actions = new Actions(driver);
//			actions.moveToElement(driver.findElement(STFTGetQuotePageObjects.btnCalculateQuote)).click().perform();
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.btnCalculateQuote), "Click", "Calculate Quote");
			Thread.sleep(10000);

//			sleep();
//			Actions actions1 = new Actions(driver);
//			actions1.moveToElement(driver.findElement(STFTGetQuotePageObjects.btnApply)).click().perform();
			tryAction(fluentWaitElement(OAMPSGetQuotePageObjects.btnApply), "Click", "Apply");
			
			Thread.sleep(10000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
