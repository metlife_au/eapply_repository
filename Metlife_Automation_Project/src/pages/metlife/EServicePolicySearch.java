/**
 * 
 */
package pages.metlife;

import java.util.Properties;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.EServiceLoginPageObjects;
import uimap.metlife.EServiceSearchPolicy;
import uimap.metlife.EViewLoginPageObjects;


/**
 * @author sampath
 * 
 */
public class EServicePolicySearch extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public EServicePolicySearch(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public EServicePolicySearch PolicySerach() {
		PolicySerachDet();
		return new EServicePolicySearch(scriptHelper);
	} 
	
	private void PolicySerachDet() {
		
		String strPolNum = dataTable.getData("MetLife_Data", "PolicyNum");
	try{
			tryAction(waitForClickableElement(EServiceSearchPolicy.linkPolSearch),"Click","Policy Link");
			tryAction(waitForClickableElement(EServiceSearchPolicy.txtPolnum),"SET","Policy Number",strPolNum);
			tryAction(waitForClickableElement(EServiceSearchPolicy.btnPolSearch),"Click","Login");
			sleep(1000);
			tryAction(waitForClickableElement(EServiceSearchPolicy.linkSearResult),"Click","Click on Policy");
			sleep();
		//	switchToMainFrame();
			}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
