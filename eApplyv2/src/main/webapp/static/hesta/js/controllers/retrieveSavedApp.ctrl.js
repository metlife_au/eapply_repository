/*Retrieve Page controller Starts*/
CareSuperApp.controller('retrievesavedapplication',['$scope', '$location', '$timeout','$window','PersistenceService', 'persoanlDetailService', 'RetrieveAppService','urlService',  function($scope, $location, $timeout,$window,PersistenceService, persoanlDetailService, RetrieveAppService,urlService){
	$scope.message = 'Look! I am an about page.';
	
	$scope.urlList = urlService.getUrlList();
	var memberDetails = persoanlDetailService.getMemberDetails();
	var refNo = memberDetails[0].clientRefNumber;
	RetrieveAppService.retrieveApp($scope.urlList.retrieveSavedAppUrl,"METL",refNo).then(function(res){
	//RetrieveAppService.retrieveApp({fundCode: "CARE", clientRefNo: refNo}, function(res){
		$scope.apps = res.data;
		for(var i = 0; i < $scope.apps.length; i++){
			$scope.apps[i].hyperlink = false;
			
			var tempDate = new Date($scope.apps[i].createdDate);
			$scope.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');
			
			if($scope.apps[i].requestType == "CCOVER"){
				$scope.apps[i].requestType = "Change Cover";
			} else if($scope.apps[i].requestType == "TCOVER"){
				$scope.apps[i].requestType = "Transfer Cover";
			} else if($scope.apps[i].requestType == "UWCOVER"){
				$scope.apps[i].requestType = "Update Work Rating";
			} else if($scope.apps[i].requestType == "NCOVER"){
				$scope.apps[i].requestType = "New Member Cover";
			} else if($scope.apps[i].requestType == "CANCOVER"){
				$scope.apps[i].requestType = "Cancel Cover";
			}
			
			if($scope.apps[i].applicationStatus.toLowerCase() == "pending"){
				$scope.apps[i].hyperlink = true;
			}
		}
	}, function(err){
		console.log("Error while fetching the apps " + err);
	});
	
	$scope.go = function (path) {
		$timeout(function(){
			$location.path(path);
		}, 10);
	};
	$scope.navigateToLandingPage = function (){
		if(window.confirm('Are you sure you want to navigate to Home Page?')){
			$location.path("/landing");
        }
    };
    
 // added for session expiry
  /*  $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
    
   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/
    
    $scope.goToSavedApp = function(app){
    	PersistenceService.setAppNumToBeRetrieved(app.applicationNumber);
    	switch(app.lastSavedOnPage.toLowerCase()){
    	case "quotepage":
    		$scope.go('/quote/3');
    		break;
    	case "aurapage":
    		$scope.go('/aura/3');
    		break;
    	case "summarypage":
    		$scope.go('/summary/3');
    		break;
    	default:
    		break;
    	}
    };
}]);
/*Retrieve Page controller Ends*/