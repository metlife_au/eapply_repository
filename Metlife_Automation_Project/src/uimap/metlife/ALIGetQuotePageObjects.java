package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIGetQuotePageObjects extends PageObjectLibrary{
		
	public static final By txtFirstName = getObjectIdentifaction("ALI.yourquote.firstname.text.id");
//	public static final By txtLastName = getObjectIdentifaction("stft.getquote.lastname.text.id");
	public static final By txtDOB = getObjectIdentifaction("ALI.yourquote.dob.text.id");
	public static final By rdMale = getObjectIdentifaction("ALI.yourquote.gender.radio.css");
	public static final By rdFemale = getObjectIdentifaction("ALI.yourquote.gender.Female.radio.css");
//	public static final By rdSmokedIn12MthsYes = getObjectIdentifaction("stft.getquote.smokedin12months.yes.radio.id");
	public static final By rdSmokedIn12MthsNo = getObjectIdentifaction("ALI.yourquote.smoker.radio.css");
	public static final By rdCitizenYes = getObjectIdentifaction("ALI.yourquote.citizen.radio.css");
//	public static final By rdCitizenNo = getObjectIdentifaction("stft.getquote.citizen.no.radio.id");
	public static final By selIndustry = getObjectIdentifaction("ALI.yourquote.industry.dropdown.css");
	public static final By selOccupation = getObjectIdentifaction("ALI.yourquote.occupation.dropdown.css");
	
	public static final By txtAnnualsalary = getObjectIdentifaction("ALI.yourquote.annualsalary.text.id");	
	public static final By selemploymentStaus= getObjectIdentifaction("ALI.yourquote.employmentstatus.radio.css");	
	
	public static final By rdConsultant= getObjectIdentifaction("ALI.yourquote.consultant.radio.css");
	public static final By btnCalculateQuote = getObjectIdentifaction("ALI.yourquote.calculatequote.btn.css");
	public static final By btnApply = getObjectIdentifaction("ALI.yourquote.apply.btn.css");
}
