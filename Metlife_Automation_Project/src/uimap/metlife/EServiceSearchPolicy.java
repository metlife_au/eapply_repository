/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class EServiceSearchPolicy extends PageObjectLibrary {

	/**
	 * 
	 */
	public EServiceSearchPolicy() {
		// TODO Auto-generated constructor stub
		
	}	

	public static final By linkPolSearch = getObjectIdentifaction("eService.polsearch.searchpol.linktext");
	public static final By txtPolnum = getObjectIdentifaction("eService.polsearch.polnum.txt.css");
	public static final By btnPolSearch= getObjectIdentifaction("eService.polsearch.search.btn.css");
	public static final By linkSearResult= getObjectIdentifaction("eService.polsearch.searchresult.link.css");
}
