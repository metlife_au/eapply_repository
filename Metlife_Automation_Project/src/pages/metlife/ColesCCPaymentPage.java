package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCGetQuotePageObjects;
import uimap.metlife.ColesCCPaymentPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.PersonalStatementPageObjects;

import java.io.StringReader;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesCCPaymentPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesCCPaymentPage enterColesCCPayment() {
		fillColesCCPayment();
		return new ColesCCPaymentPage(scriptHelper);
	}

	public ColesCCPaymentPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillColesCCPayment() {
		
		String AccountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String AccountName = dataTable.getData("MetLife_Data", "AccountName");
		String bsbcode = dataTable.getData("MetLife_Data","BSBCode");
		String bsbnumber = dataTable.getData("MetLife_Data", "BSBNumber");
		String StrPaymentType= dataTable.getData("MetLife_Data", "PaymentType");
		String StrCardType= dataTable.getData("MetLife_Data", "CardType");
		String StrCCNumber= dataTable.getData("MetLife_Data", "CardNumber");
		String StrCCHname= dataTable.getData("MetLife_Data", "CardName");
		String StrCCExpMonth= dataTable.getData("MetLife_Data", "ExpiryMonth");
		String StrCCExpYear= dataTable.getData("MetLife_Data", "ExpiryYear");
		String cYSTest = dataTable.getData("MetLife_Data", "CYSTest");
		try {		
			
			switchToActiveWindow();
			driver.manage().window().maximize();
			if (StrPaymentType.equalsIgnoreCase("Credit"))
			{
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.rdCreditCard),"Click", "Credit Card Account");	
			
				if (StrCardType.equalsIgnoreCase("Visa")){
					tryAction(waitForClickableElement(ColesCCPaymentPageObjects.rdVisaCard),"Click", "Visa Card Account");	
				}
				
				else
				{
					tryAction(waitForClickableElement(ColesCCPaymentPageObjects.rdMasterCard),"Click", "Master Card Account");
				}
				sleep(2000);
				tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtCCNumber),"SET", "Credit Card Number", StrCCNumber);
				tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtCCHoldername),"SET", "Credit Holder Name", StrCCHname);
				tryAction(fluentWaitElement(ColesCCPaymentPageObjects.SelCCExpMonth), "DropDownSelect", "Card Exp Month", StrCCExpMonth);
				tryAction(fluentWaitElement(ColesCCPaymentPageObjects.SelCCExpYear), "DropDownSelect", "Card Exp Month", StrCCExpYear);
			}
			else
			{
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.rdBankAccount),
					"Click", "Bank Account");
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtAccountName),"SET", "Account Name", AccountName);
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtAccountName),"TAB", "Account Name");
			
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtAccountNumber),
					"SET", "Account Number", AccountNumber);
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtAccountNumber),
					"TAB", "Account Number");
			
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtBsbCode),
					"SET", "BSB Code", bsbcode);
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtBsbCode),
					"TAB", "BSB Code");
			
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtBsbNumber),
					"SET", "BSB Number", bsbnumber);
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.txtBsbNumber),
					"TAB", "BSB Number");
			}
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.chkAuthorise),
					"Click", "Authorise Payment");
			sleep(2000);
			tryAction(waitForClickableElement(ColesCCPaymentPageObjects.btnSubmit),
					"Click", "Submit");
			
			Thread.sleep(3000);
			
			List<WebElement> txtAreaOutput = null;
			txtAreaOutput = fluentWaitListElements(ColesCCPaymentPageObjects.txtAreaOutput);
			String StrOutput=fluentWaitElement(txtAreaOutput.get(1)).getText();
				//XML Parsing and reading
					DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource src = new InputSource();
					src.setCharacterStream(new StringReader(StrOutput));
					Document doc = builder.parse(src);
					String appNumber= doc.getElementsByTagName("insuredID").item(0).getTextContent();
					//System.out.println(appNumber);
					dataTable.putData("MetLife_Data","AppNumber", appNumber);
					driver.manage().window().maximize();
					if (cYSTest.equalsIgnoreCase("Yes")) {	
						driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
						driver.manage().window().maximize();
					}		
							
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
