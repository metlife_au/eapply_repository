package pages.metlife.NEW;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_CancelYourCoverPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_CancelYourCoverPage HOSTPLUSCancelYourCoverCover() {
		CancelYourCover();		
		confirmation();
		
		return new HOSTPLUS_CancelYourCoverPage(scriptHelper);
	}

	public HOSTPLUS_CancelYourCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void CancelYourCover() {

		String EmailId = dataTable.getData("HOSTPLUS_CancelCover", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_CancelCover", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_CancelCover", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_CancelCover", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_CancelCover", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_CancelCover", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_CancelCover", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_CancelCover", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_CancelCover", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_CancelCover", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_CancelCover", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_CancelCover", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_CancelCover", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_CancelCover", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_CancelCover", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_CancelCover", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_CancelCover", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_CancelCover", "AnnualSalary");
		
		
		try{
			
			
			
			//****Click on Cancel Your Cover Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Cancel your cover')]"))).click();
			
			report.updateTestLog("Cancel Your Cover", "Cancel Your Cover Link is Clicked", Status.PASS);
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(200);

			//*****Click on the Contact Number Type****\\			
				
				if(!TypeofContact.equalsIgnoreCase("Mobile")){			
							
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Mobile')]"))).click();
				sleep(300);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();			
				report.updateTestLog("Contact Type", "Preferred contact: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
				}
				
				
				//****Enter the Contact Number****\\			
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact NUmber", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mrngRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("aftrNoonRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				//***********Select the Gender******************\\	
				
				if(Gender.equalsIgnoreCase("Male")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mem_gender_male_id__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mem_gender_female_id__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}	
							
				//****Contact Details-Continue Button*****\\			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueContDetlID']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				sleep(1200);			
							
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
					//*****Select the 15 Hours Question*******\\		
					
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhr_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhr_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
					
					//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
					
					if(RegularIncome.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
							
					}		
					else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
							
					}	
					
					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_yesId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_noId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_yesId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_noId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					
					//*****Resident of Australia****\\	
					
					if(Citizen.equalsIgnoreCase("Yes")){		
						driver.findElement(By.id("valCitizenAus_yes_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						driver.findElement(By.id("valCitizenAus_no_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}	
							
					//*****Industry********\\		
				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='industryId']/span)[1]"))).click();
					sleep(800);				
					driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();		
					sleep(800);		
					report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),' What is your occupation?')]"))).click();
					sleep(500);
					
					//*****Occupation*****\\		
					
					driver.findElement(By.xpath("(//button[@data-id='occupationId']/span)[1]")).click();
					//wait.until(ExpectedConditions.elementToBeClickable(By.id("(//button[@data-id='occupationId']/span)[2]"))).click();	
		            sleep(800);							
					driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();		
					sleep(800);	
					report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
					sleep(500);
							
							
					//*****Other Occupation *****\\	
					
					if(OccupationType.equalsIgnoreCase("Other")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupationId"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);	
					}		
					
					
					//*****Extra Questions*****\\							
					
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
				   
				    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
											
				}							
				else{							

					wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
											
				}							
											
				      //*****Do You Hold a Tertiary Qualification******\\							
											
				        if(TertiaryQual.equalsIgnoreCase("Yes")){							
				       
				        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}							
				   else{							
				       
					   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}							
				}							
											
											
											
				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
											
											
											
				//******Do you work in hazardous environment*****\\		
					
				if(HazardousEnv.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_yes_id__xc_r"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_no_id__xc_"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}							
											
				//******Do you spend 20% time working outside office*******\\	
				
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_yes_id__xc_r"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_no_id__xc_"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}							
				}							
							
				    
                  sleep(800);
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				    	//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
					   
					    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
												
					}							
					else{							

						wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
												
					}							
												
					      //*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){							
					       
					        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}						
				}		
				    
											
				//*****What is your annual Salary******							
											
				
				    wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalId"))).sendKeys(AnnualSalary);
				    sleep(500);		
				    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
											
				//***Load the Salary Amount****\\	
				    				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Insert gross salary (i.e.your salary before tax)')]"))).click();
				sleep(500);	
				
				
				
				//*****Click on Continue*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occuContinueId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				
				
			sleep(1500);
			
			//****Selecting cancel Your cover*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='deathSelectCoverId']/span)[1]"))).click();	
			sleep(800);	
			driver.findElement(By.xpath("//*[@id='deathSelCvr_div_id']/span/div/div/ul/li/a/span[contains(text(),'Cancel your cover')]")).click();
			sleep(800);
			report.updateTestLog("Death Cover action", " is selected on Death cover", Status.PASS);
			
			
			//***Click on Calculate Quote*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueCovrCalcId']/span"))).click();
			report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
			sleep(4000);
			
			if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
				driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
				sleep(4000);
			}
			
			if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
				driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
				sleep(4000);
			}
			if(driver.findElement(By.xpath("(//div/h4[@class='coverval'])[8]")).getText().equalsIgnoreCase("$0.00")){
				driver.findElement(By.xpath("//*[@id='continueCovrCalcId']/span")).click();
				sleep(4000);
			}
			
			//******Click on Continue button******\\
			
		   
			
			driver.findElement(By.xpath("(//span[contains(text(),'Continue')])[4]")).click();
			report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);
			sleep(2500);

          //****handling pop-up****\\
			
			quickSwitchWindows();
			sleep(250);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Continue')]"))).click();
			report.updateTestLog("Pop-Up", "Pop-up continue button is Clicked", Status.PASS);
			sleep(4500);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	private void confirmation() {
		
		WebDriverWait wait=new WebDriverWait(driver,20);	
			
			try{
				if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){			
				

				
			//******Agree to general Consent*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(500);
				
				
			//******Click on Submit*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
				report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
				sleep(1000);			
				
			}
			
			//*******Feedback popup******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1000);
			
			if(driver.getPageSource().contains("Application number")){
				
			
			//*****Fetching the Application Status*****\\
			
			String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(500);
			
			//*****Fetching the Application Number******\\
			
			String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
			sleep(1000);
			
			
			
			//******Download the PDF***********\\
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
				sleep(1500);
	  		}
			
			
			//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
			
			
			
		  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		  			sleep(1500);
		  			Robot roboobj=new Robot();
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  		}
		  		
		  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		  			Robot roboobj=new Robot();
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		  				
		  			sleep(4000);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  			
		  		}
			
			report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
			
			sleep(1000);
		
			 
			  report.updateTestLog("Confirmation Page", "Application No: "+App,Status.PASS);
			
				
			 report.updateTestLog("Confirmation Page", "Application Status: "+Appstatus,Status.PASS);	
			}

			else{
				 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
			}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
	}

		
		
	
	
	}





