package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationPreferences")

@XmlRootElement
public class CommunicationPreferences {	
	
	private ContactPeriod ContactPeriod;

	public ContactPeriod getContactPeriod() {
		return ContactPeriod;
	}

	public void setContactPeriod(ContactPeriod contactPeriod) {
		ContactPeriod = contactPeriod;
	}
	
	
	
}
