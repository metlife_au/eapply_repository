package au.com.metlife.eapply.bs.service;

import java.io.InputStream;
import java.util.List;

import org.springframework.web.util.UriComponentsBuilder;

import au.com.metlife.eapply.bs.model.AcceptedAppStatus;
import au.com.metlife.eapply.bs.model.DocumentInfo;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyOutput;

public interface EapplyService {
	
	Eapplication submitEapplication(Eapplication eapplication);

	Boolean clientMatch(String firstName, String lastName, String dob);
	
	Boolean checkAppSubmittedInLast24Hrs(String fundCode, String clientRefNo,String manageType);

	Eapplication retrieveApplication(String applicationNumber);
	
	Eapplication expireApplication(String applicationNumber);
	
	List<Eapplication> retrieveSavedApplications(String fundCode, String clientRefNo,String manageType);
	
	EappHistory createHistory(EappHistory eappHistory);
	
	Boolean checkDupUWApplication(String fundCode, String clientRefNo,String manageType, String dob, String firstName, String lastName);

	Boolean deleteDocument(long eapplicationid);
	
	AcceptedAppStatus checkAccApplicationStatus(String fundCode, String clientRefNo,String manageType, String dob, String firstName, String lastName);	
	
	EapplyOutput submitEapplyNew(String authId, EappInput eappInput, UriComponentsBuilder ucBuilder);//MTAA Change
	
	EapplyOutput printQuotePageNew(String authId, EappInput eappInput, UriComponentsBuilder ucBuilder);//MTAA Change
	
	DocumentInfo uploadFileNew(String contentType,InputStream inputStream);//MTAA Change
	
}
