package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.datatype.DatatypeConfigurationException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetails;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.utility.EapplyHelper;
import au.com.metlife.eapply.webservices.EapplicationtRequestMapper;

public class EapplicationrequestMapperTest {

	
	@Test
	public void shouldGenerateStatewideRequest() throws FileNotFoundException, IOException, ParseException, java.text.ParseException, DatatypeConfigurationException  {
		//Given
		String fileName = "eapplySubmitSfps.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
		Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 applicationDTO.setAppdecision("ACC");
		 }		 
		//when
		 UpdateInsuranceDetails updateInsuranceDetails= eapplicationrequestMapper.doMapForSFPSFynSyn(applicationDTO, eapplyInput);
		 assertThat(updateInsuranceDetails.getEndUser(), is(eapplyInput.getClientRefNumber()));
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsuranceBenefitDetailsInput());		 
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput());		 
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput().getValue().getMedicalHistoriesInput());
		 assertThat(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput().getValue().getPLlPolicyNumber(), is(new Long(eapplyInput.getPolicyNumber())));
		 assertThat(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsuranceBenefitDetailsInput().size(),is(5));
		 
	}
	
	@Test
	public void shouldGenerateStatewideRequestTransfer() throws FileNotFoundException, IOException, ParseException, java.text.ParseException, DatatypeConfigurationException  {
		//Given
		String fileName = "SfpsTransferAccept.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
		Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 applicationDTO.setAppdecision("ACC");
		 }		 
		//when
		 UpdateInsuranceDetails updateInsuranceDetails= eapplicationrequestMapper.doMapForSFPSFynSynTransfer(applicationDTO, eapplyInput);
		 assertThat(updateInsuranceDetails.getEndUser(), is(eapplyInput.getClientRefNumber()));
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsuranceBenefitDetailsInput());		 
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput());		 
		 assertThat(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput().getValue().getPLlPolicyNumber(), is(new Long(eapplyInput.getPolicyNumber())));
		 assertThat(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsuranceBenefitDetailsInput().size(),is(3));
		 
	}
	
	@Test
	public void shouldGenerateStatewideRequestCancel() throws FileNotFoundException, IOException, ParseException, java.text.ParseException, DatatypeConfigurationException  {
		//Given
		String fileName = "sfpsCancellation_deathTpd_fixed.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
		Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 applicationDTO.setAppdecision("ACC");
		 }		 
		//when
		 UpdateInsuranceDetails updateInsuranceDetails= eapplicationrequestMapper.doMapForSFPSFynSynCancellation(applicationDTO, eapplyInput);
		 assertThat(updateInsuranceDetails.getEndUser(), is(eapplyInput.getClientRefNumber()));
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsuranceBenefitDetailsInput());		 
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput());		 
		 assertNotNull(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput().getValue().getMedicalHistoriesInput());
		 assertThat(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsurancePolicyDetailsInput().getValue().getPLlPolicyNumber(), is(new Long(eapplyInput.getPolicyNumber())));
		 assertThat(updateInsuranceDetails.getUpdateInsuranceDetailsInput().getInsuranceBenefitDetailsInput().size(),is(5));
		 
	}	
	
}
