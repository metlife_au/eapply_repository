package au.com.metlife.eapply.bs.constants;

public class EapplicationPdfConstants {
	private EapplicationPdfConstants(){}
	
	/*PDF Type*/   
	public static final String UNDER_WRITTING = "UW";
     /*PDF Type Label*/
	public static final String UNDER_WRITTING_LABEL = "##UW##";
    /*Partner Types*/
	public static final String PARTNER_MILLI = "MILLI";
    
	public static final String PARTNER_OAMPS = "OAMPS";
	public static final String PARTNER_ALI = "ALI";
	public static final String PARTNER_YBR = "YBR";
	public static final String PARTNER_STFT = "STFT";
    /*Applicant TYPE*/
	public static final String APPLICANT_TYPE_PRIMARY = "primary";
    
    /*Line of Business*/
	public static final String LOB_INDIVIDUAL = "Individual";
	public static final String LOB_INSTITUTIONAL = "Institutional";
    
    /*Colors*/
	public static final String COLOR_PURPLE = "purple";
    
    /*Cover Codes*/
	public static final String COVER_CODE_KIDS = "202";
    
	public static final String COVER_CODE_DEATH = "199";
    
	public static final String COVER_CODE_DOWN_SALE = "201";
    
    /*Decision*/
	public static final String DECISION_DCL = "DCL";
    
	public static final String APPLICANT_CHILD = "child";
	public static final String APPLICANT_SECONDARY= "secondary";
    
	public static final String APPLICANT_PRIMARY = "primary";
    
	public static final String DECISION_RUW = "RUW";
    
    /*Aura Question Type*/
	public static final String QG_DEFAULT = "";
    
	public static final String QG_KIDS_COVER = "QG=Kids Cover";
    
	public static final String QG_KIDS_COVER_OPTION = "QG=Kids cover option";
    
	public static final String QG_GENERAL_QUESTIONS = "QG=General Questions";
    
	public static final String QG_PERSONAL_DETAILS = "QG=Personal Details";
    
	public static final String QG_ELIGIBILITY_CHECK = "QG=Eligibility Check";
    
	public static final String QG_YOUR_PERSONAL_DETAILS = "100000";
    
	public static final String QG_TRANSFER_DETAILS = "100009";
    
	public static final String QG_WORK_RATING_DETAILS = "100011";
    
	public static final String QG_LIFE_EVENT_DETAILS = "100010";
    
	public static final String QG_OCCUPATION_DETAILS = "QG=Occupation Details";
    
	public static final String QG_YOUR_OCCUPATION_DETAILS = "100001";
    
	public static final String QG_FAMILY_HISTORY = "QG=Family History";
    
	public static final String QG_YOUR_FAMILY_HISTORY = "100004";
    
	public static final String QG_EMPLOYMENT_DETAILS = "QG=Your employment";
    
	public static final String QG_YOUR_EMPLOYMENT_DETAILS = "100001";
    
	public static final String QG_HEALTH_QUESTIONS = "QG=Health Questions";
    
	public static final String QG_YOUR_HEALTH_QUESTIONS = "100002";
    
	public static final String QG_TRANSFER_QUESTIONS = "QG=Transfer Questions";
    
	public static final String QG_YOUR_MEDICAL_QUESTIONS = "100006";
    
	public static final String QG_LIFESTYLE_QUESTIONS = "QG=Lifestyle Questions";
    
	public static final String QG_YOUR_LIFESTYLE_QUESTIONS = "100005";
    
	public static final String QG_INSURANCE_DETAILS = "QG=Insurance Details";
    
	public static final String QG_INSURANCE_HISTORY = "QG=Insurance History";
    
	public static final String QG_YOUR_INSURANCE_HISTORY = "100007";
    
	public static final String QG_PREGNANCY = "QG=Pregnancy";    
    
	public static final String QG_YOUR_PREGNANCY = "100003";   
    
    /*Labels*/
	public static final String FAMILY_DISCOUNT_LABEL = "Family Discount:";
    
	public static final String MULTICOVER_DISCOUNT_LABEL = "Multicover Discount:";
    
	public static final String SPECIAL_CONDITION_SECTION_LABEL = "Special Condition(s):";
    
    /*Messages*/
	public static final String COVER_DECLINE_TEXT = "Sorry, this cover has been declined based on your answers to the health and lifestyle questionnaire.";
    
	public static final String FAMILY_DISCOUNT_TEXT = "Family discount applied.";
    
	public static final String MEMBER_DISCOUNT_TEXT = "Member discount applied.";
    
	public static final String APPLIED_LOADING_CONTACT_TEXT = "These loadings take into account your health and lifestyle. Please call us on 1300 392 679 if you'd like to know more.";
    
	public static final String APPLIED_EXCLUSION_CONTACT_TEXT = "If you'd like to know more about this, Please call us on 1300 392 679.";
    
	public static final String APPLIED_TEXT = " applied";

	public static final String MULTI_COVER_DISCOUNT_TEXT = " multi-cover discount of $";
    
	public static final String TEXT_EXCLUSION_LABEL = "";
    
	public static final String TEXT_LOADING_LABEL = "";
    
	public static final String STR_YOUR = "Your ";
	public static final String LOADING_AMOUNT_TXT = " premium includes a loading of $";
    
    
	public static final String PERCENTAGE_LOADING1 = "Loadings of ";
    
	public static final String PERCENTAGE_LOADING2 = "apply to total cover :";
    
    /*Frequency Type*/
	public static final String FREQUENCY_TYPE_DEFAULT = "Monthly";
    
   /* Discount Types*/
	public static final String DISCOUNT_FAMILY = "Family";
	public static final String DISCOUNT_MULTICOVER = "Multicover";
  //cover Name
	public static final String DEATH = "Death";
	public static final String TPD = "TPD";
	public static final String IP = "Salary Continuance";
}
