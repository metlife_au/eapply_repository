class TransferCoverCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog, $timeout, $q) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._ngDialog = ngDialog;
    this._$timeout = $timeout;
    this.applicantData = {};
    this.transferData = {};
    this.exDthCover = {};
    this.exTpdCover = {};
    this.exIpCover = {};
    this.contactOpts = AppConstants.transfer.contactOpts;
    this.waitingOpts = ['14 Days', '30 Days', '45 Days', '60 Days', '90 Days', '4 Weeks', '8 Weeks', '13 Weeks', 'Not Listed'];
    this.benefitOpts = ['2 Years', '5 Years', 'Age 65', 'Age 67', 'Not Listed'];
    this.PrefTimeOpts = [
		'Morning (9am \u002d 12pm)',
		'Afternoon (12pm \u002d 6pm)'];
    this.modelOptions = {updateOn: "blur"};
    this.emailPattern = AppConstants.regEx.emailFormat;
    this.mobilePattern = AppConstants.regEx.phoneNumber;
    
    
    $timeout(function(){
    	
    	//watches
    	
    	$scope.$watchGroup(['formduty.$valid', 'privacyForm.$valid', 'memberDetails.$valid', 'occupationForm.$valid'], function(newVal, oldVal) {
    		
    		if($scope.formduty.$valid && $scope.privacyForm.$valid && $scope.memberDetails.$valid && $scope.occupationForm.$valid){
    			$('#privacy-statement').collapse('show');
    			$('#contact-details').collapse('show');
    			$('#occupation-details').collapse('show');
    			$('#cover-details').collapse('show'); 
    		}else if($scope.formduty.$valid && $scope.privacyForm.$valid && $scope.memberDetails.$valid){
    			$('#privacy-statement').collapse('show');
    			$('#contact-details').collapse('show');
    			$('#occupation-details').collapse('show');
    		}else if($scope.formduty.$valid && $scope.privacyForm.$valid){
    			$('#privacy-statement').collapse('show');
    			$('#contact-details').collapse('show');
    		}else if($scope.formduty.$valid){
    			$('#privacy-statement').collapse('show');
    		}
    		
    	});
    	
    	$scope.$watch('formduty.$invalid', function(newVal, oldVal) {    		
    		$('#privacy-statement').collapse('hide');
			$('#contact-details').collapse('hide');
			$('#occupation-details').collapse('hide');
			$('#cover-details').collapse('hide'); 
		});
    	
    	$scope.$watch('privacyForm.$invalid', function(newVal, oldVal) {    		
    		$('#contact-details').collapse('hide');
			$('#occupation-details').collapse('hide');
			$('#cover-details').collapse('hide');
		});
    	
    	$scope.$watch('memberDetails.$invalid', function(newVal, oldVal) {    		
    		$('#occupation-details').collapse('hide');
			$('#cover-details').collapse('hide');
		});
    	
    	$scope.$watch('occupationForm.$invalid', function(newVal, oldVal) {
    		$('#cover-details').collapse('hide');  		
    	});
    	
    	$scope.$watch('occupationForm.annualSalary.$invalid', function(newVal, oldVal) {
    		if(newVal){$('#cover-details').collapse('hide');}  		
    	});
    	
    }); 
  }

  init() {

	    this.urlList = this._CommonAPI.getUrlList();
	    this.IndustryOptions = this._CommonAPI.getIndustryList();
	    
	    this.applicantData = this._EapplyData.getApplicantData() || this._EapplyData.exitOnError();
	    this.applicantData.applicant.cover = [];
	    this.applicantData.applicant.cover[0] = this._EapplyData.getExistingDeathCover();
	    this.applicantData.applicant.cover[1] = this._EapplyData.getExistingTpdCover();
	    this.applicantData.applicant.cover[2] = this._EapplyData.getExistingIpCover();
	    
	    this.transferData = this._EapplyData.getTransferData() ? angular.extend({}, this.transferData, this._EapplyData.getTransferData()) : angular.copy(this.applicantData);
	    
	    this._EapplyData.populateData(this.transferData);
	    
	    console.log(JSON.stringify(this.transferData));
	    
	    if(this._EapplyData.getTransferData()){
	    	
	    	this._CommonAPI.fetchOccupationList(this.urlList.occupationUrl, this.transferData.applicant.industryType).then(
	    			(res) => {
	      		        this.occupationList = res;
	      		      },
	      		      (err) => {
	      		        this.errors = err.data.errors;
	      		      }
	        );
	    	
	    }else{
	    	this.transferData.applicant.cover[0].additionalCoverAmount = '';
		    this.transferData.applicant.cover[1].additionalCoverAmount = '';
		    this.transferData.applicant.cover[2].additionalCoverAmount = '';
	    }
	    
	    this.transferData.applicant.cover[2].occupationRating = this.transferData.applicant.cover[2].occupationRating ? this.transferData.applicant.cover[2].occupationRating : this.transferData.applicant.cover[0].occupationRating;
	    
	    this.transferData.requestType = 'TCOVER';   
	    
	  
  }

  changePrefContactType() {
	    if (this.transferData.applicant.contactDetails.preferedContacType == 'Home') {
	      this.transferData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.homePhone;
	    } else if (this.transferData.applicant.contactDetails.preferedContacType == 'Work') {
	      this.transferData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.workPhone;
	    } else if (this.transferData.applicant.contactDetails.preferedContacType == 'Mobile') {
	      this.transferData.applicant.contactDetails.preferedContactNumber = this.applicantData.applicant.contactDetails.mobilePhone;
	    } else {
	      this.transferData.applicant.contactDetails.preferedContactNumber = '';
	    }	  
}

  getOccupations() {
	  this.transferData.applicant.occupation = '';
	  this.transferData.applicant.otherOccupation = '';
	  this.transferData.applicant.industryType = this.transferData.applicant.industryType === '' ? null : this.transferData.applicant.industryType;
    this._CommonAPI.fetchOccupationList(this.urlList.occupationUrl, this.transferData.applicant.industryType).then(
      (res) => {
        this.occupationList = res;
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );
  }
  
getOccupationRating(){
	  
	  if(this.transferData.applicant.occupation !== undefined){
		  
		  var occName = this.transferData.applicant.industryType + ':' + this.transferData.applicant.occupation;
		  
		  this._CommonAPI.getOccupationRating(this.urlList.newOccupationUrl, 'INGD', occName).then(
				  (res) => {
					  this.transferData.applicant.cover[0].occupationRating = res[0].deathfixedcategeory;
					  this.transferData.applicant.cover[1].occupationRating = res[0].tpdfixedcategeory;
					  this.transferData.applicant.cover[2].occupationRating = res[0].ipfixedcategeory;
					  this.calculate();
				  },
			      (err) => {
			          this.errors = err.data.errors;
			        }
		  );
	   } 
  }
  
  print() {
	  
	  if(!this.minIPErrorFlag  && !this.maxIPErrorFlag && !this.maxTPDErrorFlag && !this.minTPDErrorFlag && !this.maxDeathErrorFlag && !this.minDeathErrorFlag){
		  this._CommonAPI.printEapply(this.urlList.printQuotePageNew, this.transferData).then(
			      (res) => {
			    	  this.downloadPDF(res.clientPDFLocation);
			    	  console.log(res);
			      },
			      (err) => {
			        this.errors = err.data.errors;
			      }
		   );
	  }
  }
  
  downloadPDF(pdfLocation){
	  	var filename = null;
	  	var a = null;
	  	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  	a = document.createElement('a');
	  	document.body.appendChild(a);
	  	
	  	this._CommonAPI.downloadPDF(this.urlList.downloadUrl, pdfLocation).then(
				  (res) => {
					  if(navigator.appVersion.toString().indexOf('.NET') > 0) {
						  window.navigator.msSaveBlob(res.response,filename);
					  }else{
						  var fileURL = URL.createObjectURL(res.response);
						  a.href = fileURL;
						  a.download = filename;
						  a.click();
					  }
				  }, 
	            (err) => {
	              console.log('Error while Downloading the PDF' + err);
	            }
		  );
	  	
	  }
  
  continue(){
	  if(!this.minIPErrorFlag  && !this.maxIPErrorFlag && !this.maxTPDErrorFlag && !this.minTPDErrorFlag && !this.maxDeathErrorFlag && !this.minDeathErrorFlag){
		  this.setOccupation();
		  this._EapplyData.setTransferData(this.transferData);
		  this._$state.go('app.transferaura');
	  }
  }
  
  save() {
	  
	  if(!this.minIPErrorFlag  && !this.maxIPErrorFlag && !this.maxTPDErrorFlag && !this.minTPDErrorFlag && !this.maxDeathErrorFlag && !this.minDeathErrorFlag){
	  
		  this.transferData.lastSavedOn = 'transfercover';
		  this.setOccupation();
		  
		  console.log(JSON.stringify(this.transferData));
		  
		  this._CommonAPI.saveEapply(this.urlList.saveEapplyUrlNew, this.transferData).then(
				  (res) => {
					  console.log(res);
					  this.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+this.transferData.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
				  },
				  (err) => {
					  this.errors = err.data.errors;
		  }); 
	  }
  }
  
  setOccupation(){
	  
	  var industryType = this.transferData.applicant.industryType;
	  
	  var selectedIndustry = this.IndustryOptions.filter(function(obj){
          return  industryType === obj.key;
      });
	  
	  this.transferData.applicant.occupationCode = selectedIndustry[0].value;
      
  }
  
 revertIpSection(){
	  
	  /*if(this.transferData.applicant.workHours == 'No'){
		  
		  this.transferData.applicant.cover[2].additionalCoverAmount = 0;
		  this.transferData.applicant.cover[2].additionalIpBenefitPeriod = this.transferData.applicant.cover[2].existingIpBenefitPeriod;
		  this.transferData.applicant.cover[2].additionalIpWaitingPeriod = this.transferData.applicant.cover[2].existingIpWaitingPeriod;
		  this.changeWaitingPeriod();
		  this.changeBenefitPeriod();
	  }*/
  }
  
  changeWaitingPeriod() {	  
	  
	  switch(this.transferData.applicant.cover[2].additionalIpWaitingPeriod) {
	  	case "14 Days":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '30 Days'
	  		break;
	  	case "30 Days":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '30 Days'
	  		break;
	  	case "45 Days":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '60 Days'
	  		break;
	  	case "60 Days":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '60 Days'
	  		break;
	  	case "90 Days":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '90 Days'
	  		break;
	  	case "4 Weeks":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '30 Days'
	  		break;
	  	case "8 Weeks":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '60 Days'
	  		break;
	  	case "13 Weeks":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '90 Days'
	  		break;
	  	case "Not Listed":
	  		this.transferData.applicant.cover[2].totalIpWaitingPeriod = '90 Days'
	  		break;	  		
	  	default:
	  		break;
      }
	  
	  this.calculate();
  }
  
  changeBenefitPeriod() {
	  
	  switch(this.transferData.applicant.cover[2].additionalIpBenefitPeriod) {
	  	case "2 Years":
	  		this.transferData.applicant.cover[2].totalIpBenefitPeriod = '2 Years'
	  		break;
	  	case "5 Years":
	  		this.transferData.applicant.cover[2].totalIpBenefitPeriod = '2 Years'
	  		break;
	  	case "Age 65":
	  		this.transferData.applicant.cover[2].totalIpBenefitPeriod = 'Age 67'
	  		break;
	  	case "Age 67":
	  		this.transferData.applicant.cover[2].totalIpBenefitPeriod = 'Age 67'
	  		break;
	  	case "Not Listed":
	  		this.transferData.applicant.cover[2].totalIpBenefitPeriod = '2 Years'
	  		break;
	  	default:
	  		break;
	  }
	  
	  this.calculate();
  
	}
  
  checkValidSalary(){
	  
	  this.salaryErrorFlag = false;
	  this.minSalaryErrorFlag = false;
	  this.maxSalaryErrorFlag = false;
	  
	  if(parseInt(this.transferData.applicant.annualSalary) > 0 && parseInt(this.transferData.applicant.annualSalary) <=500000){
		  this.transferData.applicant.annualSalary = parseInt(this.transferData.applicant.annualSalary);
		  this.validateIPAmount();		  
	  }else if(parseInt(this.transferData.applicant.annualSalary) > 500000){
		  this.transferData.applicant.annualSalary = parseInt(this.transferData.applicant.annualSalary);
		  this.maxSalaryErrorFlag = true;
		  this.salaryErrorFlag = true;
		  this._$timeout(function(){
			  $('#cover-details').collapse('hide'); 
		  }, 400);
	  }else{
		  //this.transferData.applicant.annualSalary = ''; 
		  this.salaryErrorFlag = true;
		  this.minSalaryErrorFlag = true;
		  this._$timeout(function(){
			  $('#cover-details').collapse('hide'); 
		  }, 400);
	  }	  
  }
  
  validateDeathAmount(){
      
	  this.deathMinLimit = 10000;
	  this.deathMaxLimit = 2000000;
	  
	  this.minDeathErrorFlag = false;
	  this.maxDeathErrorFlag = false;
	  
	  var deathTotal;
	  
	  if(this.transferData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.transferData.applicant.cover[1].existingCoverCategory === 'TPDAutomatic'){
		  deathTotal = parseInt(this.transferData.applicant.cover[0].additionalCoverAmount);
	  }else{
		  deathTotal = parseInt(this.transferData.applicant.cover[0].existingCoverAmount) + parseInt(this.transferData.applicant.cover[0].additionalCoverAmount);
	  }
	  
	  if(deathTotal < this.deathMinLimit){
		  this.minDeathErrorFlag = true;
	  }
	  
	  if(deathTotal > this.deathMaxLimit){
		  this.maxDeathErrorFlag = true;
	  }
	  
	  if(!this.minDeathErrorFlag && !this.maxDeathErrorFlag){
		  this.calculate();
	  }    
  }
  
  validateTPDAmount(){
      
	  this.TPDMinLimit = 10000;
	  this.TPDMaxLimit = 2000000;
	  
	  this.minTPDErrorFlag = false;
	  this.maxTPDErrorFlag = false;	
	  
	  var TPDTotal;
	  
	  if(this.transferData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.transferData.applicant.cover[1].existingCoverCategory === 'TPDAutomatic'){
		  TPDTotal = parseInt(this.transferData.applicant.cover[1].additionalCoverAmount);
	  }else{
		  TPDTotal = parseInt(this.transferData.applicant.cover[1].existingCoverAmount) + parseInt(this.transferData.applicant.cover[1].additionalCoverAmount);
	  }	
	  
	  if(TPDTotal < this.TPDMinLimit){
		  this.minTPDErrorFlag = true;
	  }
	  
	  if(TPDTotal > this.TPDMaxLimit){
		  this.maxTPDErrorFlag = true;
	  }
	  
	  if(!this.minTPDErrorFlag && !this.maxTPDErrorFlag){
		  this.calculate();
	  }    
  }
  
  validateIPAmount(){
	  
	  var IPTotal;
	  this.IPMinLimit = 1000;
	  this.IPMaxLimit = 20000;
	  
	  this.minIPErrorFlag = false;
	  this.maxIPErrorFlag = false;
	  
	  this.insuredMaxIPFlag = false;
	  
	  var calculatedAmount = Math.round((0.85 * (this.transferData.applicant.annualSalary / 12)));
	  
	  IPTotal = parseInt(this.transferData.applicant.cover[2].existingCoverAmount) + parseInt(this.transferData.applicant.cover[2].additionalCoverAmount);
	  
	  if(calculatedAmount < IPTotal){
		  this.transferData.applicant.cover[2].additionalCoverAmount = calculatedAmount;
		  this.insuredMaxIPFlag = true;
	  }
	  
	  IPTotal = parseInt(this.transferData.applicant.cover[2].existingCoverAmount) + parseInt(this.transferData.applicant.cover[2].additionalCoverAmount);
	  
	  if(IPTotal < this.IPMinLimit){
		  this.minIPErrorFlag = true;
	  }
	  
	  if(IPTotal > this.IPMaxLimit){
		  this.maxIPErrorFlag = true;
	  }
	  
	  if(!this.minIPErrorFlag && !this.maxIPErrorFlag){
		  this.calculate();
	  }  
  }
  
  calculate(){
	  
	  var deathTotal,tpdTotal;
	  
	  if(this.transferData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.transferData.applicant.cover[1].existingCoverCategory === 'TPDAutomatic'){
		  deathTotal = parseInt(this.transferData.applicant.cover[0].additionalCoverAmount);
		  this.transferData.applicant.cover[0].coverCategory = 'DcTailored';
	  }else{
		  deathTotal = parseInt(this.transferData.applicant.cover[0].existingCoverAmount) + parseInt(this.transferData.applicant.cover[0].additionalCoverAmount); 
	  }
	  
	  if(this.transferData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.transferData.applicant.cover[1].existingCoverCategory === 'TPDAutomatic'){
		  tpdTotal = parseInt(this.transferData.applicant.cover[1].additionalCoverAmount);
		  this.transferData.applicant.cover[1].coverCategory = 'TPDTailored';
	  }else{
		  tpdTotal = parseInt(this.transferData.applicant.cover[1].existingCoverAmount) + parseInt(this.transferData.applicant.cover[1].additionalCoverAmount);
	  }	  
	  
	  var ipTotal = parseInt(this.transferData.applicant.cover[2].existingCoverAmount) + parseInt(this.transferData.applicant.cover[2].additionalCoverAmount);
	  
	  const calculateJSON = {  
			   'age': this.transferData.applicant.age,
			   'fundCode': 'INGD',
			   'gender': this.transferData.applicant.gender,
			   'deathOccCategory': this.transferData.applicant.cover[0].occupationRating,
			   'tpdOccCategory': this.transferData.applicant.cover[1].occupationRating,
			   'ipOccCategory': this.transferData.applicant.cover[2].occupationRating,
			   'smoker': false,
			   'deathFixedAmount': deathTotal,
			   'tpdFixedAmount': tpdTotal,
			   'annualSalary': this.transferData.applicant.annualSalary,
			   'ipFixedAmount': ipTotal,
			   'premiumFrequency': this.transferData.applicant.cover[0].frequencyCostType,
			   'deathCoverType': this.transferData.applicant.cover[0].coverCategory,
			   'tpdCoverType': this.transferData.applicant.cover[1].coverCategory,
			   'ipCoverType': this.transferData.applicant.cover[2].coverCategory,
			   'manageType': 'TCOVER',
			   'ipBenefitPeriod': this.transferData.applicant.cover[2].totalIpBenefitPeriod,
			   'ipWaitingPeriod': this.transferData.applicant.cover[2].totalIpWaitingPeriod,
			   'stateCode': this.transferData.applicant.contactDetails.state,
			   'deathExistingAmount': this.transferData.applicant.cover[0].existingCoverAmount,			   
			   'tpdExistingAmount': this.transferData.applicant.cover[1].existingCoverAmount,
			   'ipExistingAmount': this.transferData.applicant.cover[2].existingCoverAmount,
			   'deathCoverStartDate': this.applicantData.applicant.cover[0].coverStartDate,	
			   'tpdCoverStartDate': this.applicantData.applicant.cover[1].coverStartDate,	
			   'ipCoverStartDate': this.applicantData.applicant.cover[2].coverStartDate,	
			}; 
	  
	  
    console.log(JSON.stringify(calculateJSON));
    
    this._CommonAPI.calculateINGAll(this.urlList.calculateINGDAll, calculateJSON).then(
      (res) => {
    	  
    	  var premium = res;
    	  
    	  
    	  for(var i = 0; i < premium.length; i++){
			   if(premium[i].coverType === 'DcTailored' || premium[i].coverType === 'DcAutomatic'){
				   this.transferData.applicant.cover[0].transferCoverAmount = premium[i].coverAmount;
				   this.transferData.applicant.cover[0].cost = premium[i].cost || 0;
			   }else if(premium[i].coverType === 'TPDTailored' || premium[i].coverType === 'TPDAutomatic'){
				   this.transferData.applicant.cover[1].transferCoverAmount = premium[i].coverAmount;
				   this.transferData.applicant.cover[1].cost = premium[i].cost || 0;
			   }else if(premium[i].coverType === 'IpTailored'){
				   this.transferData.applicant.cover[2].transferCoverAmount = premium[i].coverAmount||0.00;
				   this.transferData.applicant.cover[2].cost = premium[i].cost || 0;
			   }
			}
    	  
    	  if(!this._CommonAPI.getAppNumber()){
    		  this._CommonAPI.generateAppNumber(this.urlList.appNumUrl).then(
    				  (res) => {
    					  this.transferData.applicatioNumber = this._CommonAPI.getAppNumber();
    			  },
    			  (err) => {
    	              this.errors = err.data.errors;
    	            }
    		  );
    	  }else{
    		  this.transferData.applicatioNumber = this._CommonAPI.getAppNumber();
    	  }
    	  
    	  this.transferData.totalMonthlyPremium = parseFloat(this.transferData.applicant.cover[0].cost) + parseFloat(this.transferData.applicant.cover[1].cost) + parseFloat(this.transferData.applicant.cover[2].cost);
		   
    	  console.log(this.transferData.applicant.cover[0].additionalCoverAmount, this.transferData.applicant.cover[1].additionalCoverAmount, this.transferData.applicant.cover[2].additionalCoverAmount);
		  console.log(this.transferData.applicant.cover[0].cost, this.transferData.applicant.cover[1].cost, this.transferData.applicant.cover[2].cost);
        
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );

  
	  
  }
  
  saveAndExitPopUp(hhText) {
	  
	  this._ngDialog.openConfirm({
		  template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Finish &amp; Close Window </button></div></div>',
		  plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
      }).then(
      () => {
        this._$state.go('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  
  }
  
  isCollapsable(element, event){
	  
		var formdutyValid = this._$scope.formduty.$valid;
	  	var privacyFormValid = this._$scope.privacyForm.$valid;
	  	var memberDetailsValid = this._$scope.memberDetails.$valid;
	  	var occupationFormValid = this._$scope.occupationForm.$valid;
	  	/*var salaryField = this._$scope.occupationForm.annualSalary.$valid;*/
	  	var stopPropagation = false;
	  	
	  	if(element === 'privacy' && !formdutyValid){
	  		this._$scope.formduty.$submitted = true;
			stopPropagation = true;
	    }else if(element === 'memberDetailSection' && (!formdutyValid || !privacyFormValid)) {
	    	this._$scope.privacyForm.$submitted = true;
	    	stopPropagation = true;
	    }else if(element === 'OccupationSection' && (!formdutyValid || !privacyFormValid || !memberDetailsValid)) {
	    	this._$scope.memberDetails.$submitted = true;
	    	stopPropagation = true;
	    }else if(element === 'CoverSection' && (this.salaryErrorFlag || !formdutyValid || !privacyFormValid || !memberDetailsValid || !occupationFormValid)){
	    	this._$scope.occupationForm.$submitted=true;
	    	stopPropagation = true;
	    }
	    
	    if(stopPropagation){
	    	event.stopPropagation();
	    	return false;
	    }	  
  }
  
  checkPreviousMandatoryFields(formName, elementName){
	  	
		var memberDetailsFormFields = ['contactEmail','contactType','contactPhone','contactPrefTime','gender'];
		var occupationFormFields = ['fifteenHrsWork','industry','occupation','otherOccupation','annualSalary'];
		var coverCalculatorFormFields = ['deathRequireCover','TPDrequireCover','ipWaitingPeriod','ipBenefitPeriod','ipRequireCover'];
	  	var currentFormFields = [];
	  	
	  	if(formName === 'memberDetails'){
	  		currentFormFields = memberDetailsFormFields;
	  	}else if(formName === 'occupationForm'){
	  		currentFormFields = occupationFormFields;
	    }else if(formName === 'coverCalculatorForm'){
	  		currentFormFields = coverCalculatorFormFields;
	  	}
	  	
	      var inx = currentFormFields.indexOf(elementName);	        
	      
	      if(inx > 0){
	      	for(var i = 0; i < inx ; i++){
	      		 if (this._$scope[formName][currentFormFields[i]]){
	      			this._$scope[formName][currentFormFields[i]].$touched = true;
	      		 }                        
	      	}
	      }	    
  }
  
  clickToOpen(hhText) {
	  this._ngDialog.open({
	      template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
	      className: 'ngdialog-theme-plain',
	      showClose: false,
	      plain: true
	    });
  }
  
}

export default TransferCoverCtrl;
