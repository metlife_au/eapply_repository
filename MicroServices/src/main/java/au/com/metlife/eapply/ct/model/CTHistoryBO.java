package au.com.metlife.eapply.ct.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "Tbl_Claims_Tracker_History", schema="EAPPDB")
public class CTHistoryBO {
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private long id;	  
	  private String claimNum;
	  private String fundName;
	  private String status;
	  private String activityName;	 
	  private Date accessedDate;
	  
	  public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getClaimNum() {
		return claimNum;
	}
	public void setClaimNum(String claimNum) {
		this.claimNum = claimNum;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	 public String getActivityName() {
			return activityName;
		}
		public void setActivityName(String activityName) {
			this.activityName = activityName;
		}
		public Date getAccessedDate() {
			return accessedDate;
		}
		public void setAccessedDate(Date accessedDate) {
			this.accessedDate = accessedDate;
		}
	  
	
}
