package pages.metlife.Angular.FirstSuper;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_SaveAndExitPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public FirstSuper_Angular_SaveAndExitPage SaveandExit_FirstSuper() {
		SaveandExit();		
		
		
		return new FirstSuper_Angular_SaveAndExitPage(scriptHelper);
	}

	public FirstSuper_Angular_SaveAndExitPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void SaveandExit() {

		String EmailId = dataTable.getData("FirstSuper_SaveAndExit", "EmailId");
		String TypeofContact = dataTable.getData("FirstSuper_SaveAndExit", "TypeofContact");
		String ContactNumber = dataTable.getData("FirstSuper_SaveAndExit", "ContactNumber");
		String TimeofContact = dataTable.getData("FirstSuper_SaveAndExit", "TimeofContact");		
		String FifteenHoursWork = dataTable.getData("FirstSuper_SaveAndExit", "FifteenHoursWork");
		String Citizen = dataTable.getData("FirstSuper_SaveAndExit", "Citizen");		
		String IndustryType = dataTable.getData("FirstSuper_SaveAndExit", "IndustryType");
		String OccupationType = dataTable.getData("FirstSuper_SaveAndExit", "OccupationType");
		String OtherOccupation = dataTable.getData("FirstSuper_SaveAndExit", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("FirstSuper_SaveAndExit", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("FirstSuper_SaveAndExit", "TertiaryQual");
		String Eightypercenttimeinoffice = dataTable.getData("FirstSuper_SaveAndExit", "Eightypercenttimeinoffice");		
		String AnnualSalary = dataTable.getData("FirstSuper_SaveAndExit", "AnnualSalary");
		String CostType=dataTable.getData("FirstSuper_SaveAndExit", "CostType");
		String DeathYN=dataTable.getData("FirstSuper_SaveAndExit", "DeathYN");
		String AmountType=dataTable.getData("FirstSuper_SaveAndExit", "AmountType");
		String DeathAction=dataTable.getData("FirstSuper_SaveAndExit", "DeathAction");
		String DeathAmount=dataTable.getData("FirstSuper_SaveAndExit", "DeathAmount");
		String DeathUnits=dataTable.getData("FirstSuper_SaveAndExit", "DeathUnits");
		
		try{
						

			
			//****Click on Change Your Insurance Cover Link*****\\
			sleep(3000);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Change your insurance cover')]"))).click();
			
			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);
			
			if(driver.getPageSource().contains("You have previously saved application(s).")){
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[text()='Cancel']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(1000);
		
			}
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

		
			
			
			sleep(200);
			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			sleep(200);
			//*****Enter the Email Id*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);
			
			//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);
							
			
		//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
			
			//***Select the Preferred time of Contact*****\\			
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
						
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}	
			
			
						
			//****Contact Details-Continue Button*****\\			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);			
						
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
			
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week? ']/../following-sibling::div/div/div[1]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week? ']/../following-sibling::div/div/div[2]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			
			
			//Added yuvi
				//*****Are You citizen of Australia*******\\		
			
			if(Citizen.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you a citizen or permanent resident of Australia?']/../following-sibling::div/div/div[1]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you a citizen or permanent resident of Australia?']/../following-sibling::div/div/div[2]/label/span"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			
			
		//*****Industry********\\	
		
	    sleep(500);
	    Select Industry=new Select(driver.findElement(By.name("industry")));
	    Industry.selectByVisibleText(IndustryType);
	    sleep(800);
		report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
		sleep(500);
		
		//*****Occupation*****\\		
		
		Select Occupation=new Select(driver.findElement(By.name("occupation")));
		Occupation.selectByVisibleText(OccupationType);
	    sleep(800);
		report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
		sleep(500);
				
				
		//*****Other Occupation *****\\	
		
		if(OccupationType.equalsIgnoreCase("Other")){		
				
			wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
			report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
			sleep(1000);	
		}		
		
		

		//*****Extra Questions*****\\							
		
	    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
	    
								
	//***Select if your office Duties are Undertaken within an Office Environment?\\	
	    	
	    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
	    	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(8) > div > div:nth-child(2) > div > div.col-sm-3.pl0 > label > span"))).click();
	    //	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
								
	}							
	else{							
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(8) > div > div:nth-child(2) > div > div.col-sm-6.pl0xs > label > span"))).click();
	//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
		report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
								
	}							
								
	      //*****Do You Hold a Tertiary Qualification******\\							
								
	        if(TertiaryQual.equalsIgnoreCase("Yes")){							
	        	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(9) > div > div:nth-child(2) > div > div.col-sm-3.pl0 > label > span"))).click();
	       // 	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
	}							
	   else{							
	       
		   wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(9) > div > div:nth-child(2) > div > div.col-sm-6.pl0xs > label > span"))).click();
		   //   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
			report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		
	}		
	        sleep(500);
	        
	}							
								
								
								
	else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
								
		//******Do you spend at least 80% of all working time in an office*****\\
		if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[1]/label/span"))).click();
			report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[2]/label/span"))).click();
			report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
		}
		sleep(500);						
		 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){										

			 //***Select if your office Duties are Undertaken within an Office Environment?\\	

if(OfficeEnvironment.equalsIgnoreCase("Yes")){							

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
			
}							
    else{							

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
				
 }	

//added Yuvi
					
//*****Do You Hold a Tertiary Qualification******\\							
					
if(TertiaryQual.equalsIgnoreCase("Yes")){							

//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div/div[2]/div/div[1]/label/span"))).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(9) > div > div:nth-child(2) > div > div.col-sm-3.pl0 > label > span"))).click();
	sleep(300);	
	report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
}							
else{							
	
//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div/div[2]/div/div[2]/label/span"))).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(9) > div > div:nth-child(2) > div > div.col-sm-6.pl0xs > label > span"))).click();
	sleep(300);	
	report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
}									
	}	
	}
							    
      sleep(800);
	   
						
								
	//*****What is your annual Salary******							
								
	
	    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
	    sleep(500);		
	    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
								
	
	
	//*****Click on Continue*******\\
	
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
	
	
//**************************************************************************************************************\\
//****************************COVER CALCULATOR*****************************************************************\\
//*************************************************************************************************************\\

				//******Click on Cost of  Insurance as*******\\		
	sleep(1000);			
	Select COSTTYPE=new Select(driver.findElement(By.name("premFreq")));		
	COSTTYPE.selectByVisibleText(CostType);	
	sleep(250);
	report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
	sleep(800);
							
				
				
				//*******Death Cover Section******\\
				
				if(DeathYN.equalsIgnoreCase("Yes")){
		
					Select deathaction=new Select(driver.findElement(By.name("coverName")));		
					deathaction.selectByVisibleText(DeathAction);	
					sleep(300);
					report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
					sleep(600);
					
				}
				
				
				if(DeathYN.equalsIgnoreCase("Yes")&& DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){
				
					if(AmountType.equalsIgnoreCase("Fixed")){
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);				
						sleep(800);	
						report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
						sleep(800);
						

					}
					else{
						
						wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathUnits);				
						sleep(800);	
						report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
						sleep(800);
					}
				}
			
				
			
			

			//*****Click on Calculate Quote********\\
			
			sleep(500);	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
			report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
			sleep(3000);
			

			if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4")).getText().equalsIgnoreCase("$0.00")){
				
				driver.findElement(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']")).click();
				sleep(5500);
			}
			

            if(driver.findElement(By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4")).getText().equalsIgnoreCase("$0.00")){
				
				driver.findElement(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']")).click();
				sleep(5500);
			}
          
	          
	            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@ng-click='saveQuote()'])[1]"))).click();
	            report.updateTestLog("Save and Exit", "Save and Exit Button is Clicked", Status.PASS);
	            sleep(1000);
	            
	          //****Fetch the Application No******\\
				
				quickSwitchWindows();
				sleep(800);
				String AppNo=driver.findElement(By.xpath("//div[@id='tips_text']/strong[2]")).getText();
				report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);
				
				//******Finish and Close Window******\\
				
				sleep(500);
				driver.findElement(By.xpath("//button[text()='Finish & Close Window ']")).click();
				report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
				sleep(1500);
	            
	            
	            
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}





