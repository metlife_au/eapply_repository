package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import com.gargoylesoftware.htmlunit.html.Keyboard;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.STFTBeneficiariesPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.VOWWebResponsePageObjects;
import uimap.metlife.YBRCoverDetailPageObjects;
import uimap.metlife.YBRWebResponsePageObjects;

public class VOWWebResponsePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public VOWWebResponsePage webresponse_newapp() {
		webresponse_newapplication();
		return new VOWWebResponsePage(scriptHelper);
	}

	public VOWWebResponsePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void webresponse_newapplication() {
	
	
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			Thread.sleep(4000);
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(VOWWebResponsePageObjects.btnNewapp)).click().perform();
			//tryAction(fluentWaitElement(STFTHealthPageObjects.btnNext), "Click", "Apply");
			Thread.sleep(1000);
			
			driver.switchTo().alert().accept();
			Thread.sleep(1000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
