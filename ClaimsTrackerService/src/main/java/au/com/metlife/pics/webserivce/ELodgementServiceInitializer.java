package au.com.metlife.pics.webserivce;


import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import au.com.metlife.exception.MetlifeException;
import au.com.metlife.webservices.EAPPSearchStub;
import au.com.metlife.webservices.PWCBClientHandler;
import au.com.metlife.webservices.PicsLodgementStub;



public class ELodgementServiceInitializer extends MetlifeException{

	
	private static final String PACKAGE_NAME = ELodgementServiceInitializer.class
	.getPackage().getName();

	private static final String CLASS_NAME = ELodgementServiceInitializer.class.getName();
	private static final String PICS_SERVICE_PATH = MetlifeElodgeConstants.CREATE_UPDATE_UW_WSDL ;
	
	//static MetLifeProperty property = new MetLifeProperty();
	ConfigurationContext ctx= null;
	
	
	
	public  PicsLodgementStub doInitPICS(String userName,String password) throws MetlifeException{

		final String METHOD_NAME = "doInitPICS"; //$NON-NLS-1$
		
		PicsLodgementStub picsStub = null;
		EAPPSearchStub stub = null;
		String wsConfigPath = null;
		try {
		ServiceClient client = null;
		
		
		java.util.Properties props = new java.util.Properties();               
		wsConfigPath = "C:\\client_config";//(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.WEB_SER_CONFIG, MetlifeElodgeConstants.WEB_SER_CONFIG);	        
        String servicePath = "http://10.173.91.95/PICSWS/services/PicsLodgement";//(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.PICS_SEARCH_CONFIG, MetlifeElodgeConstants.PICS_SEARCH_CONFIG);
        
			ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(wsConfigPath, null);	

			picsStub = new PicsLodgementStub(ctx, servicePath);

			client = picsStub._getServiceClient();

			client.engageModule(MetlifeElodgeConstants.ENGAGE_MODULE);
		
		Options options = client.getOptions();
		options.setTimeOutInMilliSeconds(100000);

		options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY,
				ELodgementOutflowConfiguration.getOutflowConfiguration(userName));

		PWCBClientHandler myCallback = new PWCBClientHandler();
		myCallback.setUTUsername(userName);
		myCallback.setUTPassword(password);

		options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);

		client.setOptions(options);
		
		} catch (Exception e) {
			MetlifeException exe = new MetlifeException();
			exe.setErrorCode("10024");
			throw new MetlifeException();
		}

		
		return picsStub;

	}
	

	public  EAPPSearchStub doInitEAPP(String userName,String password) throws MetlifeException

	{
		final String METHOD_NAME = "doInitPICS"; //$NON-NLS-1$
		//LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		EAPPSearchStub stub = null;
		String wsConfigPath = null;
		            
		
		try {
			//java.util.Properties props = new java.util.Properties();          

	        wsConfigPath = "C:\\client_config";//(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.WEB_SER_CONFIG, MetlifeElodgeConstants.WEB_SER_CONFIG);	        
	        String servicePath = "http://10.173.91.95/webtop/PICSWS/services/PicsLodgement";//(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.PICS_SEARCH_CONFIG, MetlifeElodgeConstants.PICS_SEARCH_CONFIG);

	        System.out.println("11>"+wsConfigPath+"<22>"+servicePath);
	        ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(wsConfigPath, null);      		
			stub = new EAPPSearchStub(ctx, servicePath);
			ServiceClient client = stub._getServiceClient();
			System.out.println("client ");
			client.engageModule(MetlifeElodgeConstants.ENGAGE_MODULE);
			Options options = client.getOptions();
			options.setTimeOutInMilliSeconds(100000);			
			options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY,
					ELodgementOutflowConfiguration.getOutflowConfiguration(userName));			
			PWCBClientHandler myCallback = new PWCBClientHandler();
			
			System.out.println("call beack handler"+"userName"+userName+"password"+password);
			myCallback.setUTUsername(userName);
			myCallback.setUTPassword(password);
			options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
			client.setOptions(options);
			
		} catch (Exception e) {
			MetlifeException exe = new MetlifeException();
			exe.setErrorCode("10024");
			throw new MetlifeException();
		}
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return stub;

	}

	
	
	
}

