package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIHealthOverviewPageObjects extends PageObjectLibrary{
		
	public static final By txtheight = getObjectIdentifaction("ALI.health.height.text.id");
	public static final By txtweight = getObjectIdentifaction("ALI.health.weight.text.id");
	public static final By chklast5yrsNone = getObjectIdentifaction("ALI.health.5yrs.none.chkbox.css");

	public static final By btnNext = getObjectIdentifaction("ALI.health.next.btn.css");

}
