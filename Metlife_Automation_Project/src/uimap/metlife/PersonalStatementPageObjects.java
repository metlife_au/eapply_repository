/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;
import supportlibraries.PageObjectLibrary;

/**
 * @author sampath
 *
 */
public class PersonalStatementPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public PersonalStatementPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	
		
	public static final By radCitizenPRYes = getObjectIdentifaction("personalStatement.citizenPR.yes.css");
	public static final By radCitizenPRNo =	getObjectIdentifaction("personalStatement.citizenPR.no.css");
	public static final By txtHeight = getObjectIdentifaction("personalStatement.height.css");
	public static final By lstHeightUnits =	getObjectIdentifaction("personalStatement.height.units.css");
	public static final By txtWeight = getObjectIdentifaction("personalStatement.weight.css");
	public static final By lstWeightUnits = getObjectIdentifaction("personalStatement.weight.units.css");
	public static final By radSmokeYes = getObjectIdentifaction("personalStatement.smoked.yes.css");
	public static final By radSmokeNo = getObjectIdentifaction("personalStatement.smoked.no.css");
	public static final By chkLung = getObjectIdentifaction("personalStatement.lung.css");
	public static final By chkAsthma = getObjectIdentifaction("personalStatement.asthma.css");
	public static final By radAsthmaMild = getObjectIdentifaction("personalStatement.asthma.mild.css");
	public static final By radAsthmaModerate = getObjectIdentifaction("personalStatement.asthma.moderate.css");
	public static final By radAsthmaSevere = getObjectIdentifaction("personalStatement.asthma.severe.css");
	public static final By chkMedAdvNota = getObjectIdentifaction("personalStatement.medicaladvise.nota.zero.css");
	public static final By chkBPCholNota = getObjectIdentifaction("personalStatement.medicaladvise.nota.first.css");
	public static final By chkBrainBoneNota = getObjectIdentifaction("personalStatement.medicaladvise.nota.second.css");
	public static final By radAsthmaWorseYes = getObjectIdentifaction("personalStatement.asthma.worsened.yes.css");
	public static final By radAsthmaWorseNo = getObjectIdentifaction("personalStatement.asthma.worsened.no.css");
	public static final By radPregnentYes = getObjectIdentifaction("personalStatement.pregnant.yes.css");
	public static final By radPregnentNo = getObjectIdentifaction("personalStatement.pregnant.no.css");
	public static final By radVisitDocYes = getObjectIdentifaction("personalStatement.visitdictor.yes.css");
	public static final By radVisitDocNo = getObjectIdentifaction("personalStatement.visitdoctor.no.css");
	public static final By radFamHistYes = getObjectIdentifaction("personalStatement.familyHist.yes.css");
	public static final By radFamHistNo = getObjectIdentifaction("personalStatement.familyHist.no.css");
	public static final By radFamHistUnknown = getObjectIdentifaction("personalStatement.familyHist.unknown.css");
	public static final By radTravelPlanYes = getObjectIdentifaction("personalStatement.travelPlans.yes.css");
	public static final By radTravelPlanNo = getObjectIdentifaction("personalStatement.travelPlans.no.css");
	public static final By chkSportsNota = getObjectIdentifaction("personalStatement.sports.noneofabove.css");
	public static final By radDrugYes = getObjectIdentifaction("personalStatement.drugs.yes.css");
	public static final By radDrugNo = getObjectIdentifaction("personalStatement.drugs.no.css");
	public static final By txtAlcoholicDrinksConsumption = getObjectIdentifaction("personalStatement.AFContentCell.css");
	public static final By btnAlcoholicChange = getObjectIdentifaction("personalStatement.btn.change.id");
	public static final By btnAlcoholicEnter = getObjectIdentifaction("personalStatement.btn.enter.css");
	public static final By radReduceAlcoholConsuYes = getObjectIdentifaction("personalStatement.reducealcho.yes.css");
	public static final By radReduceAlcoholConsuNo = getObjectIdentifaction("personalStatement.reducealcho.no.css");
	public static final By radHIVYes = getObjectIdentifaction("personalStatement.HIV.yes.css");
	public static final By radHIVNo = getObjectIdentifaction("personalStatement.HIV.no.css");
	public static final By radHIVRiskNo =	getObjectIdentifaction("personalStatement.HIVRisk.no.css");
	public static final By radSufferFromAnyConditionYes = getObjectIdentifaction("personalStatement.generalQuestions1.yes.css");
	public static final By radSufferFromAnyConditionNo = getObjectIdentifaction("personalStatement.generalQuestions1.no.css");
	public static final By radDisabilityInsuranceYes = getObjectIdentifaction("personalStatement.generalQuestions2.yes.css");
	public static final By radDisabilityInsuranceNo = getObjectIdentifaction("personalStatement.generalQuestions2.no.css");
	public static final By radClaimForReceivedSicknessYes = getObjectIdentifaction("personalStatement.generalQuestions3.yes.css");
	public static final By radClaimForReceivedSicknessNo = getObjectIdentifaction("personalStatement.generalQuestions3.no.css");
	public static final By btnContinuePersonalStatement = getObjectIdentifaction("personalStatement.btn.continue.css");
	//public static final By btnContinuePersonalStatement = getObjectIdentifaction("personalStatement.btn.continue.linktext");
	public static final By chkCholesterol = getObjectIdentifaction("personalStatement.last5years.cholesterol.checkbox.label.css");
	public static final By lstCholesterolTreated = getObjectIdentifaction("personalStatement.last5years.cholesterol.treated.dropdown.css");
	public static final By btnCholesterol = getObjectIdentifaction("personalStatement.last5years.cholesterol.btn.css");
	public static final By radLessThan12Months = getObjectIdentifaction("personalStatement.cholesterol.lessthan12months.radio.css");
	public static final By radMoreThan12Months = getObjectIdentifaction("personalStatement.cholesterol.morethan12months.radio.css");


	//VICS Personal Statement
	public static final By radStoppingDutiesNo = getObjectIdentifaction("vics.personalstatement.stoppingduties.radio.no.css");
	public static final By radStoppingDutiesYes = getObjectIdentifaction("vics.personalstatement.stoppingduties.radio.yes.css");
	public static final By radInsuranceHistYes = getObjectIdentifaction("vics.personalstatement.table_Insurance_history.radio.yes.css");
	public static final By radInsuranceHistNo = getObjectIdentifaction("vics.personalstatement.table_Insurance_history.radio.no.css");
	public static final By chkYourHealthNota = getObjectIdentifaction("vics.personalstatement.table_Your_health.checkbox.nota.css");
	public static final By radRegularMedicationYes =	getObjectIdentifaction("vics.personalstatement.regularmediction.radio.yes.css");
	public static final By radRegularMedicationNo = getObjectIdentifaction("vics.personalstatement.regularmediction.radio.no.css");
	public static final By radLostTheSightYes = getObjectIdentifaction("vics.personalstatement.lostthesight.radio.yes.css");
	public static final By radLostTheSightNo = getObjectIdentifaction("vics.personalstatement.lostthesight.radio.no.css");
	public static final By radMadeAClaimYes = getObjectIdentifaction("vics.personalstatement.madeaclaim.radio.yes.css");
	public static final By radMadeAClaimNo = getObjectIdentifaction("vics.personalstatement.madeaclaim.radio.no.css");
	public static final By radIllnessRestrictsYes = getObjectIdentifaction("vics.personalstatement.illnessrestricts.radio.yes.css");
	public static final By radIllnessRestrictsNo = getObjectIdentifaction("vics.personalstatement.illnessrestricts.radio.no.css");
	public static final By btnContinueVICSPersonalStatement = getObjectIdentifaction("vics.personalstatement.continue.button.css");
	public static final By radInsuranceDetailsNo = getObjectIdentifaction("vics.personalstatement.insurance_details.radio.no.css");
	public static final By radInsuranceDetailsYes = getObjectIdentifaction("vics.personalstatement.insurance_details.radio.yes.css");
	public static final By chkInsuranceDeatilsNota = getObjectIdentifaction("personalStatement.Insurance_Details.nota.css");
	public static final By radLifeStyleDeatilsNo = getObjectIdentifaction("vics.personalstatement.life_style_details.radio.no.css");
	public static final By radSTFTLifeStyleDeatilsNo = getObjectIdentifaction("stft.personalstatement.life_style_details.radio.no.css");
	public static final By radSTFTLifeStyleDeatilsYes = getObjectIdentifaction("stft.personalstatement.life_style_details.radio.yes.css");
	public static final By radLifeStyleDeatilsYes = getObjectIdentifaction("vics.personalstatement.life_style_details.radio.yes.css");
	public static final By radHealthDeatilsNo = getObjectIdentifaction("vics.personalstatement.life_health_details.radio.no.css");
	public static final By radHealthDeatilsYes = getObjectIdentifaction("vics.personalstatement.life_health_details.radio.yes.css");
	
	//VICS Increase Cover for Male
	public static final By radEmployeementYes = getObjectIdentifaction("personalStatement.employment.radio.yes.css");
	public static final By radEmployeementNo = getObjectIdentifaction("personalStatement.employment.radio.no.css");
	public static final By radInsuranceYes = getObjectIdentifaction("personalStatement.insurance.radio.yes.css");
	public static final By radInsuranceNo = getObjectIdentifaction("personalStatement.insurance.radio.no.css");
	public static final By chkboxHealthDeatilsNota = getObjectIdentifaction("personalStatement.health.chkbox.nota.css");
	public static final By chkboxEmphysema = getObjectIdentifaction("personalStatement.health.chkbox.emphysema.css");
	public static final By radHealthNo = getObjectIdentifaction("vics.personalstatement.health.radio.no.css");
	public static final By radHealthYes = getObjectIdentifaction("vics.personalstatement.health.radio.yes.css");
	public static final By btnVICSContinue = getObjectIdentifaction("vics.personalStatement.btn.continue.css");
	
	
	//MTAA, First Super - Insurance Details
	public static final By radListInsuranceDetailsNo = getObjectIdentifaction("personalstatement.insurance_details.list.radio.no.css");
	public static final By radListInsuranceDetailsYes = getObjectIdentifaction("personalstatement.insurance_details.list.radio.yes.css");
	
	//STFT Related Objects
	public static final By notaLungSTFt = getObjectIdentifaction("stft.yourhealth.abthealth.lung.none.checkbox.css");
	public static final By chkHighCholesterol = getObjectIdentifaction("stft.yourhealth.abthealth.lung.none.checkbox.css");
	public static final By chklast5yrs = getObjectIdentifaction("stft.yourhealth.abthealth.last5yrs.none.checkbox.css");
	public static final By chkmedicaladviceNota = getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.none.checkbox.css");
	public static final By radFamilyHistoryNo = getObjectIdentifaction("stft.yourhealth.familyhistory.no.radio.css");
	
	
	public static final By rdlifestyleNo = getObjectIdentifaction("stft.yourhealth.lifestyle.no.radio.css");
	public static final By txtalchohol = getObjectIdentifaction("stft.yourhealth.alchoholicdrink.text.css");
	public static final By rdalchoholreduceNo = getObjectIdentifaction("stft.yourhealth.alchoholicdrink.no.radio.css");
	public static final By rdhivNo = getObjectIdentifaction("stft.yourhealth.hiv.no.radio.css");
	public static final By rdhivYes = getObjectIdentifaction("stft.yourhealth.hiv.yes.radio.css");
	public static final By rdhivriskNo = getObjectIdentifaction("stft.yourhealth.hivrisk.no.radio.css");
	public static final By rsdFemaleYes = getObjectIdentifaction("stft.yourhealth.abturhealth.health.pregnent.radio.yes.css");
	public static final By rsdFemaleNo = getObjectIdentifaction("stft.yourhealth.abturhealth.health.pregnent.radio.no.css");
	
	public static final By rdgeneralNo = getObjectIdentifaction("stft.yourhealth.generalquestion.no.radio.css");
	public static final By rdinsuranceexclusionNo = getObjectIdentifaction("stft.yourhealth.insurancedetails.exclusion.no.radio.css");
	public static final By rdinsuranceclaimNo = getObjectIdentifaction("stft.yourhealth.insurancedetails.claim.no.radio.css");
	public static final By btnNext = getObjectIdentifaction("stft.yourhealth.next.button.css");
/*	public static final By btnsummaryNext = getObjectIdentifaction("stft.summary.next.button.css");*/
	public static final By chkboxCOPD = getObjectIdentifaction("personalstatement.Emphysema.checkbox.css");
	public static final By btnSaveNExit = getObjectIdentifaction("personalStatement.savenexit.btn.id");
	public static final By labelSavedApplicationNumber = getObjectIdentifaction("personalStatement.application.number.label.css");
	
	//INGD Short form Objects
	public static final By chkboxKidney = getObjectIdentifaction("ingd.shortform.kidney.chkbox.css");
	public static final By chkboxDigestive = getObjectIdentifaction("ingd.shortform.digestive.chkbox.css");
	public static final By chkboxThyroid = getObjectIdentifaction("ingd.shortform.thyroid.chkbox.css");
	public static final By chkboxIngdShortformNOTA = getObjectIdentifaction("ingd.shortform.nota.chkbox.css");
	public static final By radOffWorkYes = getObjectIdentifaction("ingd.shortform.offwork.yes.radio.css");
	public static final By radOffWorkNo = getObjectIdentifaction("ingd.shortform.offwork.no.radio.css");
	public static final By btnIngdShortformContinue = getObjectIdentifaction("ingd.shortform.continue.btn.css");
	public static final By chkboxDialysis = getObjectIdentifaction("ingd.shortform.dialysis.chkbox.css");
	public static final By chkboxOesophagus = getObjectIdentifaction("ingd.shortform.Oesophagus.chkbox.css");
	
	
}
