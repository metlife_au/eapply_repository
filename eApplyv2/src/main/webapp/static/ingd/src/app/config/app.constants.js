const AppConstants = {
  // api: 'https://conduit.productionready.io/api',
  api: 'http://localhost:8087/',
  appName: 'ING',
  urlFilePath: 'CareSuperUrls.properties',
  clientMatchUrl:"https://www.e2e.eapplication.metlife.com.au/services/ClientMatch",
  privacyURL : 'https://www.ing.com.au/privacy.html',
  legalNoticeURL : 'https://www.ing.com.au/legal.html',
  regEx: {
    phoneNumber: /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/,
    dateFormat: /^(?:0?[1-9]|[12][0-9]|3[01])\/(?:0?[1-9]|1[0-2])\/(?:(?:19|20)[0-9]{2})$/,
    emailFormat: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  transfer:{
    contactOpts: [{
      text: 'Home',
      value: 'Home'
    },{
      text: 'Work',
      value: 'Work'
    },{
      text: 'Mobile',
      value: 'Mobile'
    }]
  },
  appUrls: {
    "clientDataUrl":"getcustomerdata",
    "specialCvrEligUrl":"getDefaultUnitsForSpecialCover",
    "appNumUrl":"getUniqueNumber",
    "savedAppUrl":"checkSavedApplications",
    "cancelAppUrl":"expireApplication",
    "quoteUrl":"getIndustryList",
    "occupationUrl":"getOccupationName",
    "newOccupationUrl":"getNewOccupationList",
    "auraTransferDataUrl":"initiate",
    "auraPostUrl":"underwriting",
    "submitAuraUrl":"submitAura",
    "clientMatchUrl":"https://www.e2e.eapplication.metlife.com.au/services/ClientMatch",
    "transferCalculateUrl":"calculateTransfer",
    "retrieveSavedAppUrl":"retieveApps",
    "downloadUrl":"download",
    "accAppUrl": "checkAccApplications",
    "dupAppUrl": "checkDuplicateUWApplications",
    "saveEapplyUrlNew":"saveEapplyNew",
    "retrieveAppUrlNew":"retieveApplicationNew",
    "printQuotePageNew":"printQuotePageNew",
    "submitEapplyUrlNew":"submitEapplyNew",
    "calculateAALUrl": "calculateAAL",
    "calculateINGDAll": "calculateINGDAll",
    "getAuraTypeURL": "getAuraType",
    "calculateINGDDeath": "calculateINGDDeathLifeCoverAmount",
    "calculateINGDTPD": "calculateINGDTPDLifeCoverAmount",
    "hostDecOccList": 'HostDecOccList.properties',
    }
};

export default AppConstants;
