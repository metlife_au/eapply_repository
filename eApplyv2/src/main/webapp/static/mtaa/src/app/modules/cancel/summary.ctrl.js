(function(angular){
	'use strict';
    /* global channel, moment */  
	angular
	.module('MtaaApp')
	.controller('cancelSummary', cancelSummary);
	cancelSummary.$inject=['$scope','$rootScope','$location','$timeout','$window','ngDialog','urlService','PersistenceService','submitEapply','auraResponseService','CalculateService'];
	function cancelSummary($scope, $rootScope,$location, $timeout, $window, ngDialog, urlService, PersistenceService,submitEapply, auraResponseService, CalculateService){
		
		var vm = this;
		vm.showOtherReason = false;
		vm.ackErrorFlag = false;
			
		vm.cancelReasonList = [
			'No longer require insurance',
			'Affordability constraints',
			'I have sufficient insurance in place',
			//'I don\u2019t wish to provide details',
			'I don\u0027t wish to provide details',
			'Other \u002d please provide details'
			];
		
		vm.go = function (path){
			$location.path( path );
	  	};
	  	
	  	vm.init = function(){
	  		vm.urlList = urlService.getUrlList();
	  		vm.TPD = PersistenceService.getCancelCoverDetails();
	  		vm.TPD.applicant.cancelOtherReason = '';
	  		vm.TPD.applicant.cancelReason = '';
	  	};
	  	
	  	vm.init();
	  	
	  	vm.checkReason = function(){
	  		
	  		if(vm.TPD.applicant.cancelReason === vm.cancelReasonList[4]){
	  			vm.showOtherReason = true;
	  		}else{
	  			vm.showOtherReason = false;
	  		}
	  		if(vm.TPD.applicant.cancelReason === ''){
	  			vm.cancellationReasonFlag = true;
	  		}else{
	  			vm.cancellationReasonFlag = false;
	  		}
	  	};
	  	
	  	vm.checkAckState = function(clicked){
	  		
	  		if(clicked){
	  			vm.cancelAcknowledgement = !vm.cancelAcknowledgement;
	  		}
	  		
	  		if(vm.cancelAcknowledgement){
	  			vm.ackErrorFlag = false;
	  		}else{
	  			vm.ackErrorFlag = true;
	  		}
	  		vm.cancellationAcknowledgeForm.$submitted = true;
	  	};
	  	
	  	
	  	vm.cancelConfirmationPopUp = function(){
	  		
	  		vm.checkReason();
	  		vm.checkAckState(false);
	  		
	  		if(vm.cancellationAcknowledgeForm.$valid && vm.cancelAcknowledgement){
	  			
	  			var templateContent = '<div class="ngdialog-content"><div class="modal-body"><div class="row  rowcustom"><div class="col-sm-12">';
	  			templateContent += '<p class="aligncenter">You are requesting to cancel your '+(vm.TPD.coverCancellation.death ? 'Death' : '')+(vm.TPD.coverCancellation.death && vm.TPD.coverCancellation.incomeProtection ? ' , ' : vm.TPD.coverCancellation.death && !vm.TPD.coverCancellation.incomeProtection ? ' and ' : '') + (vm.TPD.coverCancellation.tpd ? 'TPD' : '') + (vm.TPD.coverCancellation.tpd && vm.TPD.coverCancellation.incomeProtection ? ' and ' : '') + (vm.TPD.coverCancellation.incomeProtection ? 'Income Protection' : '')+'  cover.';
	  			templateContent += ' If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?</p>';
	  			templateContent += '</div></div></div><div class="ngdialog-buttons aligncenter">';
	  			templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain"  ng-click="confirm()">Yes</button>&nbsp;&nbsp;';
	  			templateContent += '<button type="button" class="btn btn-primary no-arrow" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button>';
	  			templateContent += '</div></div>';	  			
	  			
	  			ngDialog.openConfirm({
	                template: templateContent,
	                className: 'ngdialog-theme-plain',
	                plain: true
	             }).then(function (value) {
	                   vm.confirmCancel();
	                   return true;
	             }, function (value) {
	                 if(value === 'oncancel'){
	                     return false;
	                 }
	            });
	  		}
	  		
	  	};
	  	
	  	vm.confirmCancel = function(){  			
  			
  			if(vm.TPD.coverCancellation.death){
  				vm.TPD.applicant.cover[0].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[0].cost = 0;
  				vm.TPD.applicant.cover[0].additionalUnit = 0;
  			}else{
  				vm.TPD.applicant.cover[0].additionalCoverAmount = vm.TPD.applicant.cover[0].existingCoverAmount;
  				vm.TPD.applicant.cover[0].additionalUnit = vm.TPD.applicant.cover[0].fixedUnit;
  			}
  			
  			if(vm.TPD.coverCancellation.tpd){
  				vm.TPD.applicant.cover[1].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[1].cost = 0;
  				vm.TPD.applicant.cover[1].additionalUnit = 0;
  			}else{
  				vm.TPD.applicant.cover[1].additionalCoverAmount = vm.TPD.applicant.cover[1].existingCoverAmount;
  				vm.TPD.applicant.cover[1].additionalUnit = vm.TPD.applicant.cover[1].fixedUnit;
  			}
  			 
  			if(vm.TPD.coverCancellation.incomeProtection){
  				vm.TPD.applicant.cover[2].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[2].cost = 0;
  				vm.TPD.applicant.cover[2].additionalUnit = 0;
  				vm.TPD.applicant.cover[2].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[2].cost = 0;
  				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = '';
  			}else{
  				vm.TPD.applicant.cover[2].additionalCoverAmount = vm.TPD.applicant.cover[2].existingCoverAmount;
  				vm.TPD.applicant.cover[2].additionalUnit = vm.TPD.applicant.cover[2].fixedUnit;
  			}
  			if(vm.TPD.applicant.cover[2].existingCoverAmount === 0){
  				vm.TPD.applicant.cover[2].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[2].cost = 0;
  				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].totalIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].totalIpBenefitPeriod = '';
  			}
  			
  			if(vm.TPD.applicant.cancelReason === vm.cancelReasonList[4]){
  				vm.TPD.applicant.cancelReason = 'Other';
  			}
  			
  			vm.calculate();
	  	};
	  	
	  	vm.calculate = function(){
	  		
	  		var deathUnits,tpdUnits;
	  		
	  		if (vm.TPD.applicant.cover[0].coverCategory === 'DcFixed') {
	  			deathUnits = parseInt(vm.TPD.applicant.cover[0].additionalUnit);
	  			tpdUnits  = parseInt(vm.TPD.applicant.cover[1].additionalUnit);
            }else{
            	deathUnits = 1;
	  			tpdUnits  = 1;
            }
	  		
	  		var ruleModel = {
                    'age': vm.TPD.applicant.age,
                    'fundCode': 'MTAA',
                    'gender': vm.TPD.applicant.gender,
                    'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
                    'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
                    'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
                    'smoker': false,
                    'deathUnits': deathUnits,
                    'deathFixedAmount': parseInt(vm.TPD.applicant.cover[0].additionalCoverAmount),
                    'deathFixedCost': null,
                    'deathUnitsCost': null,
                    'tpdUnits': tpdUnits,
                    'tpdFixedAmount': parseInt(vm.TPD.applicant.cover[1].additionalCoverAmount),
                    'tpdFixedCost': null,
                    'tpdUnitsCost': null,
                    'ipUnits': parseInt(vm.TPD.applicant.cover[2].additionalUnit),
                    'ipFixedAmount': parseInt(vm.TPD.applicant.cover[2].additionalCoverAmount),
                    'ipFixedCost': null,
                    'ipUnitsCost': null,
                    'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
                    'memberType': null,
                    'manageType': 'CCOVER',
                    'deathCoverType': vm.TPD.applicant.cover[0].coverCategory,
                    'tpdCoverType': vm.TPD.applicant.cover[1].coverCategory ,
                    'ipCoverType': 'IpUnitised',
                    'ipWaitingPeriod': vm.TPD.applicant.cover[2].existingIpWaitingPeriod,
                    'ipBenefitPeriod': vm.TPD.applicant.cover[2].existingIpBenefitPeriod
                };
	  		
	  		console.log(JSON.stringify(ruleModel));
	  		
	  		CalculateService.calculate(ruleModel, vm.urlList.calculateUrl).then(function(res) {
	  			
	  			 var premium = res.data;
	  			 
	  			for (var i = 0; i < premium.length; i++) {
                    
	  				if (premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised' && !vm.TPD.coverCancellation.death) {
                        vm.TPD.applicant.cover[0].cost = premium[i].cost || 0;
                    } else if (premium[i].coverType === 'TPDFixed' || premium[i].coverType === 'TPDUnitised' && !vm.TPD.coverCancellation.tpd) {
                        vm.TPD.applicant.cover[1].cost = premium[i].cost || 0;
                    } else if (premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised' && !vm.TPD.coverCancellation.incomeProtection) {
                        vm.TPD.applicant.cover[2].cost = premium[i].cost || 0;
                    }
                }
	  			
	  			vm.submitCancel();
	  			
	  		}); 		
	  		
	  		
	  	};
	  	
	  	vm.submitCancel = function(){
	  		
	  		auraResponseService.setResponse(vm.TPD);
  			console.log(JSON.stringify(vm.TPD));
  			submitEapply.reqObj(vm.urlList.submitEapplyUrlNew).then(function(response) {
                 if(response.data) {
                     PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                     PersistenceService.setNpsUrl(response.data.npsTokenURL);
                     vm.go('/cancelDecision');
                 } 
             }, function(err){
                 console.log('Error while submitting the application ' + JSON.stringify(err));
             });
  			
	  	};
	}
})(angular);

 /* Cancel Summary Controller Ends  */
