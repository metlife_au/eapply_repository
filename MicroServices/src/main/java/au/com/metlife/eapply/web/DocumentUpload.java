package au.com.metlife.eapply.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import au.com.metlife.eapply.detector.DocumentDetector;
import au.com.metlife.eapply.detector.ExcelDocumentDetectorImpl;
import au.com.metlife.eapply.detector.PdfDocumentDetectorImpl;
import au.com.metlife.eapply.detector.WordDocumentDetectorImpl;
import au.com.metlife.eapply.sanitizer.DocumentSanitizer;

/**
 * Service method to receive the uploaded file.<br>
 * For Excel/Word/Pdf document: It verify if the uploaded document is safe and if it's OK then continue processing...<br>
 * For Image: Try to sanitize the uploaded document and if it succeed to sanitize it then continue processing...<br>
 * Try to use, as much as possible, file upload feature provided by JEE >= 7
 */
@SuppressWarnings({"serial", "boxing"})
@Component
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, maxFileSize = 1024 * 1024 * 10, maxRequestSize = 1024 * 1024 * 50)
public class DocumentUpload{

    /**
     * LOGGER
     */
    private static final Logger LOG = LoggerFactory.getLogger(DocumentUpload.class);

    /**
     * {@inheritDoc}
     * @return 
     *
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)     */
  
    public boolean doPost(File filepath,String fileType,InputStream inputStream) throws ServletException, IOException {
      //  File tmpFile = null;
        Path tmpPath = null;
        boolean isSafe;

            if ((fileType == null) || (fileType.trim().length() == 0)) {
                throw new IllegalArgumentException("Unknown file type specified !");
            }

            // File content           
            if ((inputStream == null)) {
                throw new IllegalArgumentException("Unknown file content specified !");
            }

            // Write a temporary file with uploaded file
            //tmpFile = File.createTempFile("uploaded-", null);
           
            tmpPath = filepath.toPath();
            long copiedBytesCount = Files.copy(inputStream, tmpPath, StandardCopyOption.REPLACE_EXISTING);
            if (copiedBytesCount != filepath.length()) {
                throw new IOException(String.format("Error during stream copy to temporary disk (copied: %s / expected: %s !", copiedBytesCount, filepath));
            }

            /* Step 2: Initialize a detector/sanitizer for the target file type and perform validation */
          //  boolean isSafe;

            // Instantiate the dedicated detector/sanitizer implementation and apply detection/sanitizing
            DocumentDetector documentDetector;
            DocumentSanitizer documentSanitizer;
            String fileTypeExt = "";
            if (fileType.contains("pdf")){
            	fileTypeExt = "PDF";
            } else if (fileType.contains("doc") || fileType.contains("docx")){
            	fileTypeExt = "WORD";
            } else if (fileType.contains("xls") || fileType.contains("xlsx")){
            	fileTypeExt = "EXCEL";        	
			} /**else if (fileType.contains("ppt")){
				fileTypeExt = "POWERPOINT";
			}**/ else if (fileType.contains("jpg") || fileType.contains("jpeg") || fileType.contains("png") || fileType.contains("tif")){
				fileTypeExt = "IMAGE";
			}
            
            switch (fileTypeExt) {
                case "PDF":
                    documentDetector = new PdfDocumentDetectorImpl();
                    isSafe = documentDetector.isSafe(filepath);
                    break;
                case "WORD":
                    documentDetector = new WordDocumentDetectorImpl();
                    isSafe = documentDetector.isSafe(filepath);
                    break;
                case "EXCEL":
                    documentDetector = new ExcelDocumentDetectorImpl();
                    isSafe = documentDetector.isSafe(filepath);
                    break;
//                case "POWERPOINT":
//                    documentDetector = new PowerpointDocumentDetectorImpl();
//                    isSafe = documentDetector.isSafe(tmpFile);
//                    break;
                case "IMAGE":
                    documentSanitizer = new ImageDocumentSanitizerImpl();
                    isSafe = documentSanitizer.madeSafe(filepath);
                    break;
                default:
                    isSafe = false;
            }

            return isSafe;
        }
    
    

    /**
     * Utility methods to safely remove a file
     *
     * @param p file to remove
     */
//    private static void safelyRemoveFile(Path p) {
//        try {
//            if (p != null) {
//                // Remove temporary file
//                if (!Files.deleteIfExists(p)) {
//                    // If remove fail then overwrite content to sanitize it
//                    Files.write(p, "-".getBytes("utf8"), StandardOpenOption.CREATE);
//                }
//            }
//        } catch (Exception e) {
//            LOG.warn("Cannot safely remove file !", e);
//        }
//    }

    /**
     * Utility method (taken from Oracle site) to retrieve the original name of the submitted file.<br>
     * Only useful when your container run version of Servlet API inferior to 3.1 <br>
     * It's the case here because I use the Tomcat 7 Maven plugin and it use Servlet API version 3.0 <br>
     * Unfortunately I haven't found any Maven plugin for Tomcat 8...
     *
     * @param part Multipart file part
     * @return The file name or null if not found
     * @see "https://tomcat.apache.org/whichversion.html"
     * @see "https://docs.oracle.com/javaee/6/tutorial/doc/glraq.html"
     */
//    private static String extractSubmittedFileName(final Part part) {
//        for (String content : part.getHeader("content-disposition").split(";")) {
//            if (content.trim().startsWith("filename")) {
//                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
//            }
//        }
//        return null;
//    }

}
