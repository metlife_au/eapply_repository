package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class AddressJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7418264677922393649L;

	private String addressType;
	
	private String country;
	
	private String line1;
	
	private String line2;
	
	private String postCode;
	
	private String state;
	
	private String suburb;

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	

}
