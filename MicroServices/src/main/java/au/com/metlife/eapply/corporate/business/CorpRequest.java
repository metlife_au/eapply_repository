/**
 * 
 */
package au.com.metlife.eapply.corporate.business;

import java.util.HashMap;
import java.util.Map;


/**
 * @author 599063
 *
 */
public class CorpRequest {

	
	private String requestURI;
	
	private String serverName;
	
	private String serverPort;
	
	private String serverContext;
	
	private String requestURL;
	
	private Map<String, String> parameters;
	
	private CorpEntityRequest requestData; 
	

	public CorpEntityRequest getRequestData() {
		return requestData;
	}

	public void setRequestData(CorpEntityRequest requestData) {
		this.requestData = requestData;
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getServerContext() {
		return serverContext;
	}

	public void setServerContext(String serverContext) {
		this.serverContext = serverContext;
	}

	public String getRequestURL() {
		return requestURL;
	}

	public void setRequestURL(String requestURL) {
		this.requestURL = requestURL;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public void addParameter(String name, String value) {
		if(this.parameters == null){
			this.parameters = new HashMap<>();
		}
		this.parameters.put(name, value);
	}
	public String getParameter(String name){
		String value = null;
		if(this.parameters != null){
			value = this.parameters.get(name);
		}
		return value;
	}
	
	

	
	
	
	
	
	
}
