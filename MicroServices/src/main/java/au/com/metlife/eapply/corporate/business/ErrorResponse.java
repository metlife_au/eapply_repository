package au.com.metlife.eapply.corporate.business;

import au.com.metlife.eapply.business.error.EapplyServiceError;
import au.com.metlife.eapply.common.EmptyErrorElement;
import au.com.metlife.eapply.common.ResponseLink;
import au.com.metlife.eapply.common.ResponseMetaData;

public class ErrorResponse extends CorpResponse{

	private EmptyErrorElement[] errElements;
	
	private EmptyErrorElement errElement;
	
	private ResponseLink[] links;
	
	private ResponseMetaData metadata;
	
	private EapplyServiceError[] errors;

	
	public EmptyErrorElement[] getErrElements() {
		return errElements;
	}

	public void setErrElements(EmptyErrorElement[] errElements) {
		this.errElements = errElements;
	}

	public EmptyErrorElement getErrElement() {
		return errElement;
	}

	public void setErrElement(EmptyErrorElement errElement) {
		this.errElement = errElement;
	}

	public ResponseLink[] getLinks() {
		return links;
	}

	public void setLinks(ResponseLink[] links) {
		this.links = links;
	}

	public ResponseMetaData getMetadata() {
		return metadata;
	}

	public void setMetadata(ResponseMetaData metadata) {
		this.metadata = metadata;
	}

	public EapplyServiceError[] getErrors() {
		return errors;
	}

	public void setErrors(EapplyServiceError[] errors) {
		this.errors = errors;
	}

	

	
	
	
	
}
