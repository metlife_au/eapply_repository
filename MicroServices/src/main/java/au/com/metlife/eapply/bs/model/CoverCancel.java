package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class CoverCancel implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5578135864255998530L;
	private boolean death;
	private boolean tpd;
	private boolean salContinuance;
	
	
	
	public boolean isDeath() {
		return death;
	}
	public void setDeath(boolean death) {
		this.death = death;
	}
	public boolean isTpd() {
		return tpd;
	}
	public void setTpd(boolean tpd) {
		this.tpd = tpd;
	}
	public boolean isSalContinuance() {
		return salContinuance;
	}
	public void setSalContinuance(boolean salContinuance) {
		this.salContinuance = salContinuance;
	}
	
	



}
