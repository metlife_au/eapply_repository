export default class Loading {
  constructor($http) {
    this.restrict = 'EA';
    this._$http = $http;
  }

  link(scope, element, attrs) {
    const _this = this;
    scope.isLoading = function () {
      return _this._$http.pendingRequests.length > 0;
    };
    scope.$watch(scope.isLoading, function (value) {
      if (value) {
        element.removeClass('ng-hide');
      } else {
        element.addClass('ng-hide');
      }
    });
  }
}
