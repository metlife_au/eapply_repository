/**
 * 
 */
package pages.metlife.NEW;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.HOSTPLUS_ChangeYourInsurancePageObjects;

/**
 * @author Hemnath
 * 
 */
public class HOSTPLUS_ChangeYourInsurancePage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HOSTPLUS_ChangeYourInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public HOSTPLUS_ChangeYourInsurancePage HOSTPLUSchangeinsurance() {
		changeyourinsurance();
		return new HOSTPLUS_ChangeYourInsurancePage(scriptHelper);
	} 
	
	private void changeyourinsurance() {
		String EmailId = dataTable.getData("HOSTPLUS_ChangeInsurance", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_ChangeInsurance", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_ChangeInsurance", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_ChangeInsurance", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_ChangeInsurance", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_ChangeInsurance", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_ChangeInsurance", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_ChangeInsurance", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_ChangeInsurance", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_ChangeInsurance", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_ChangeInsurance", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_ChangeInsurance", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_ChangeInsurance", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_ChangeInsurance", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_ChangeInsurance", "AnnualSalary");
		String CostType=dataTable.getData("HOSTPLUS_ChangeInsurance", "CostType");
		String DeathYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathYN");
		String AmountType=dataTable.getData("HOSTPLUS_ChangeInsurance", "AmountType");
		String DeathAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAction");
		String DeathAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAmount");
		String DeathUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathUnits");
		String TPDYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDYN");
		String TPDAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAction");
		String TPDAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAmount");
		String TPDUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDUnits");
		String IPYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPYN");
		String IPAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAction");
		String Insure90Percent=dataTable.getData("HOSTPLUS_ChangeInsurance", "Insure90Percent");
		String WaitingPeriod=dataTable.getData("HOSTPLUS_ChangeInsurance", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("HOSTPLUS_ChangeInsurance", "BenefitPeriod");
		String IPAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAmount");
		String IPUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPUnits");
		String ExpectedDeathcover=dataTable.getData("HOSTPLUS_ChangeInsurance", "ExpectedDeathcover");
		String ExpectedTPDcover=dataTable.getData("HOSTPLUS_ChangeInsurance", "ExpectedTPDcover");
		String ExpectedIPcover=dataTable.getData("HOSTPLUS_ChangeInsurance", "ExpectedIPcover");
		
		try{
			WebDriverWait wait=new WebDriverWait(driver,18);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Change your insurance cover')]"))).click();
			report.updateTestLog("Click on LInk", "Error in performing action ", Status.PASS);
			//tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ChangeYourInsuranceLink), "Click", "Change Your Insurance Cover Link");
			
			//*****Agree to Duty of disclosure*******\\
			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.DutyOfDisclosure), "Click", "Duty of Disclosure");

			
           //*****Agree to Privacy Statement*******\\
			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.PrivacyStatement), "Click", "Privacy Statement");
			
			
			//*****Enter the Email Id*****\\						
			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.EmailId), "SET", "Email Id",EmailId);			
						
						
			//*****Click on the Contact Number Type****\\			
			
			if(!TypeofContact.equalsIgnoreCase("Mobile")){			
						
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ContactType), "Click", "Preferred Type of Contact: "+TypeofContact);			
						
			//*****Choose the Type of Contact*****\			
						
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();			
						
			}			
						
			//****Enter the Contact Number****\\			
			
			waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);			
						
						
						
			//***Select the Preferred time of Contact*****\\			
		
			if(TimeofContact.equalsIgnoreCase("Morning")){			
						
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");			
			}			
			else{			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.AfternoonContact), "Click", "Preferred Contact Time is AfterNoon");			
			}		
			
			
			//****Gender*****\\			
			if(Gender.equalsIgnoreCase("Male")){			
						
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.Male), "Click", "Gender is Male");			
			}			
			else{			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.Female), "Click", "Gender is Female");			
			}	
						
			//****Contact Details-Continue Button*****\\			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");			
			sleep(1200);			
						
						
			//***************************************\\			
			//**********OCCUPATION SECTION***********\\			
			//****************************************\\			
						
				//*****Select the 15 Hours Question*******\\		
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.FifteemHoursYes), "Click", "15 Hours a Week-Yes");		
						
				}		
				else{		
						
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.FifteemHoursNo), "Click", "15 Hours a Week-No");		
						
				}	
				
				//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
				
				if(RegularIncome.equalsIgnoreCase("Yes")){		
						
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.RegularIncomeYes), "Click", "Regular Income-Yes");		
						
				}		
				else{		
						
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.RegularIncomeNo), "Click", "Regular Income-No");		
						
				}	
				
				//******Extra Question based on Regular Income*******\\
				
				if(RegularIncome.equalsIgnoreCase("Yes")){
					if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
						tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.WorkWithoutInjury), "Click", "Work without injuries");	
						sleep(1000);
					}
					else{
						tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.WorkWithInjury), "Click", "Work with Injuries");
						sleep(1000);
					}
				}
				else{
					if(WorkLimitation.equalsIgnoreCase("Yes")){
						tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LimitationYes), "Click", "Limitation Work-Yes");
						sleep(1000);
					}
					else{
						tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LimitationNo), "Click", "Limitation Work-No");
						sleep(1000);
					}
				}
				
				//*****Select if Resident of Australia****\\	
				
				if(Citizen.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.id("valCitizenAus_yes_id__xc_r"))).click();
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
						
					}	
				else{	
					wait.until(ExpectedConditions.elementToBeClickable(By.id("valCitizenAus_no_id__xc_r"))).click();
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
						
					}	
						
				//*****Click on Industry********\\		
						
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.Industry), "Click", "Industry");		
				sleep(800);		
						
				//*****Choose the Industry Type*****\\		
				driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();		
				sleep(800);		
						
				//*****Click On  Occupation*****\\		
						
	            tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.Occupation), "Click", "Occupation");					
	            sleep(800);					
	            					
	            //****Choose the Occupation Type*****\\					
						
				driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();		
				sleep(1000);		
						
						
				//*****Other Occupation *****\\		
				if(OccupationType.equalsIgnoreCase("Other")){		
						
					tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);	
					sleep(1000);	
				}		
				
				
				//*****Extra Questions*****\\							
				
			    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
			    
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\							
			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
			     tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");							
										
			}							
			else{							
										
			       tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");							
										
			}							
										
			      //*****Do You Hold a Tertiary Qualification******\\							
										
			        if(TertiaryQual.equalsIgnoreCase("Yes")){							
			       tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");							
			}							
			   else{							
			       tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");							
			}							
			}							
										
										
										
			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
										
										
										
			//******Do you work in hazardous environment*****\\							
			if(HazardousEnv.equalsIgnoreCase("Yes")){							
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");							
			}							
			else{							
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");							
			}							
										
			//******Do you spend 20% time working outside office*******\\							
			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");							
			}							
			else{							
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");							
			}							
			}							
						
			    

			    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
			    
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\							
			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
			     tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");							
										
			}							
			else{							
										
			       tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");							
										
			}							
										
			      //*****Do You Hold a Tertiary Qualification******\\							
										
			        if(TertiaryQual.equalsIgnoreCase("Yes")){							
			       tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");							
			}							
			   else{							
			       tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");							
			}							
			}		
			    
										
			//*****What is your annual Salary******							
										
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);							
			sleep(500);							
										
			//***Load the Salary Amount****\\							
			waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadSalaryLabel).click();							
			sleep(500);	
			
			//*****Click on Continue*******\\
			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ContinueOccupationDetails), "Click", "Continue after entering Occupation details");
			
			//******Click on Cost of  Insurance as*******\\		
			
			if(!CostType.equalsIgnoreCase("Weekly")){
			
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.CostofInsurance), "Click", "Select the Cost Of Insurance");
			sleep(800);
			
			//*****Choose the cost of Insurance Type******\\
			
			driver.findElement(By.xpath("//span[contains(text(),'"+CostType+"')]")).click();
			
			
			}
			
			//*******Death Cover Section******\\
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ActionOnDeath), "Click",DeathAction+" on Death Cover");	
				sleep(800);	
				driver.findElement(By.xpath("//*[@id='deathSelCvr_div_id']/span/div/div/ul/li/a/span[contains(text(),'"+DeathAction+"')]")).click();
				sleep(800);
				
			}
			
			if(AmountType.equalsIgnoreCase("Fixed")){
				tryAction(fluentWaitElements(HOSTPLUS_ChangeYourInsurancePageObjects.Fixed), "Click", "Fixed Amount");
				sleep(500);
			}

			else{
				tryAction(fluentWaitElements(HOSTPLUS_ChangeYourInsurancePageObjects.Unitised), "Click", "Unitised Amount");
				sleep(500);
			}
			if(DeathYN.equalsIgnoreCase("Yes")&& (!DeathAction.equalsIgnoreCase("Cancel your cover")||!DeathAction.equalsIgnoreCase("No change"))){
			
				if(AmountType.equalsIgnoreCase("Fixed")){
					tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.DeathAmount), "SET", "Death Amount",DeathAmount);	
					sleep(500);		
					waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadDeathAmount).click();

				}
				else{
					tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.DeathAmount), "SET", " Death Units",DeathUnits);
					sleep(500);		
					waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadDeathAmount).click();
				}
			}
		
			//*******TPD Cover Section******\\
			
		if(!DeathAction.equalsIgnoreCase("Cancel your cover"))
		{
			
			if(TPDYN.equalsIgnoreCase("Yes"))
			{
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ActionOnTPD), "Click",TPDAction+" on TPD Cover");	
				sleep(800);	
				driver.findElement(By.xpath("//*[@id='tpdSelCvr_div_id']/span/span/div/div/ul/li/a/span[contains(text(),'"+TPDAction+"')]")).click();
				sleep(400);
				
				
				if(!TPDAction.equalsIgnoreCase("Cancel your cover")||!TPDAction.equalsIgnoreCase("No change"))
				{					
					
					if(AmountType.equalsIgnoreCase("Fixed"))
					{
						tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.TPDAmount), "SET", "TPD Amount",TPDAmount);	
						sleep(500);		
						waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadTPDAmount).click();
					}
					
					else
					{
						tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.TPDAmount), "SET", " TPD Units",TPDUnits);
						sleep(500);		
						waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadTPDAmount).click();
					}	
					
				}
			}			
			
		}
		
		
		                 //*******IP Cover section********\\
		
		if(IPYN.equalsIgnoreCase("Yes")){
			tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ActionOnIP), "Click",IPAction+" on IP Cover");	
			sleep(800);	
			driver.findElement(By.xpath("//*[@id='lifeStagCvr_div_id']/span/div/div/ul/li/a/span[contains(text(),'"+IPAction+"')]")).click();
	
			sleep(500);
			
			//*****Insure 90% of My Salary******\\
			
			if(!IPAction.equalsIgnoreCase("Cancel your cover")||!IPAction.equalsIgnoreCase("Change waiting/benefit period")||!IPAction.equalsIgnoreCase("No change")&&Insure90Percent.equalsIgnoreCase("Yes")){
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.Insure90Percent), "Click", " Insure 90% of my salary ");		
			}
			
			if(!IPAction.equalsIgnoreCase("Cancel your cover")||!IPAction.equalsIgnoreCase("No change")){
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.WaitingPeriod), "Click", "Waiting Period "+WaitingPeriod+" is selected");	
				sleep(800);	
				driver.findElement(By.xpath("//span[contains(text(),'"+WaitingPeriod+"')]")).click();
				sleep(500);
				
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.BenefitPeriod), "Click", "Benefit Period "+BenefitPeriod+" is selected");	
				sleep(800);	
				driver.findElement(By.xpath("//span[contains(text(),'"+BenefitPeriod+"')]")).click();
				sleep(500);			
				
			}
			
			if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")){
				
				if(AmountType.equalsIgnoreCase("Fixed")&&!Insure90Percent.equalsIgnoreCase("Yes"))
				{
					tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.IPAmount), "SET", "TPD Amount",IPAmount);	
					sleep(500);		
					waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadIPAmount).click();
					sleep(500);
				}
				
				else if(AmountType.equalsIgnoreCase("Unitised")&&!Insure90Percent.equalsIgnoreCase("Yes"))
				{
					tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.IPAmount), "SET", " TPD Units",IPUnits);
					sleep(500);		
					waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.LoadIPAmount).click();
					sleep(500);
				}	
			}
		}
		

		//*****Click on Calculate Quote********\\
		
		tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.CalculateQuote), "Click", "Calculate Quote");
		
		sleep(2500);
		
		//********************************************************************************************\\
				//************Capturing the premiums displayed************************************************\\
				//********************************************************************************************\\
				
				//*******Capturing the Death Cost********\\
				WebElement Death=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[1]"));
				Death.click();
				sleep(300);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,-150)", "");
				sleep(500);
				String DeathCost=Death.getText();	

				if(DeathCost.equalsIgnoreCase(ExpectedDeathcover)){
					
					report.updateTestLog("Death Cover", CostType+" cost "+DeathCost+" is displayed as Expected", Status.PASS);
				}
				else{
					
					report.updateTestLog("Death Cover", CostType+" cost "+DeathCost+" is not as Expected", Status.FAIL);
				}
				
				
				
				//*******Capturing the TPD Cost********\\
				
				sleep(500);
				WebElement tpd=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[2]"));
				tpd.click();
				sleep(250);
				String TPDCost=tpd.getText();
				if(TPDCost.equalsIgnoreCase(ExpectedTPDcover)){
				
					report.updateTestLog("TPD Cover amount", CostType+" cost "+TPDCost+" is displayed as Expected", Status.PASS);
				}
				else{
					
					report.updateTestLog("TPD Cover amount", CostType+" cost "+TPDCost+" is not as Expected", Status.FAIL);
				}
				
				
				//*******Capturing the IP Cost********\\
				
				sleep(250);
				WebElement ip=driver.findElement(By.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4[@class='coverval'])[3]"));
				ip.click();
				sleep(500);
				String IPCost=ip.getText();
				if(IPCost.equalsIgnoreCase(ExpectedIPcover)){
					
					report.updateTestLog("IP Cover amount", CostType+" cost "+IPCost+" is displayed as Expected", Status.PASS);
				}
				else{
					
					report.updateTestLog("IP Cover amount", CostType+" cost "+IPCost+" is not as Expected", Status.FAIL);
				}
				
		sleep(500);
		
		//******Click on Continue button******\\
		
		tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.ContinueAfterQuote), "Click", "Continue after Calculating Quote");
		sleep(1000);

		//*****Handling the pop-up*******\\
			
			 if(DeathAction.equalsIgnoreCase("Decrease your cover")||DeathAction.equalsIgnoreCase("Cancel your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")||TPDAction.equalsIgnoreCase("Cancel your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){
				quickSwitchWindows();
				sleep(250);
				tryAction(waitForClickableElement(HOSTPLUS_ChangeYourInsurancePageObjects.PopUpContinue), "Click", "Continue Pop-up");
				sleep(500);
			 }
		 
		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
