package au.com.metlife.eapply.underwriting.model;

import java.io.Serializable;
import java.util.List;

import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;

public class AuraDecision implements Serializable{

	private ResponseObject responseObject;
	private String overallDecision;	
	private String deathDecision;
	private String deathLoading;
	private String deathExclusions;
	private String deathResons;
	private String deathAuraResons;
	private String deathOrigTotalDebitsValue;
	
	private String tpdDecision;
	private String tpdLoading;
	private String tpdExclusions;
	private String tpdResons;
	private String tpdAuraResons;
	private String tpdOrigTotalDebitsValue;
	
	private String ipDecision;
	private String ipLoading;
	private String ipExclusions;
	private String ipResons;
	private String ipAuraResons;
	private String ipOrigTotalDebitsValue;
	private boolean specialTerm; 
	private boolean clientMatched;
	private String clientMatchReason;
	public boolean isClientMatched() {
		return clientMatched;
	}
	public void setClientMatched(boolean clientMatched) {
		this.clientMatched = clientMatched;
	}
	public String getClientMatchReason() {
		return clientMatchReason;
	}
	public void setClientMatchReason(String clientMatchReason) {
		this.clientMatchReason = clientMatchReason;
	}
	public boolean isSpecialTerm() {
		return specialTerm;
	}
	public void setSpecialTerm(boolean specialTerm) {
		this.specialTerm = specialTerm;
	}
	public String getDeathOrigTotalDebitsValue() {
		return deathOrigTotalDebitsValue;
	}
	public void setDeathOrigTotalDebitsValue(String deathOrigTotalDebitsValue) {
		this.deathOrigTotalDebitsValue = deathOrigTotalDebitsValue;
	}
	public String getTpdOrigTotalDebitsValue() {
		return tpdOrigTotalDebitsValue;
	}
	public void setTpdOrigTotalDebitsValue(String tpdOrigTotalDebitsValue) {
		this.tpdOrigTotalDebitsValue = tpdOrigTotalDebitsValue;
	}
	public String getIpOrigTotalDebitsValue() {
		return ipOrigTotalDebitsValue;
	}
	public void setIpOrigTotalDebitsValue(String ipOrigTotalDebitsValue) {
		this.ipOrigTotalDebitsValue = ipOrigTotalDebitsValue;
	}
	public String getDeathAuraResons() {
		return deathAuraResons;
	}
	public void setDeathAuraResons(String deathAuraResons) {
		this.deathAuraResons = deathAuraResons;
	}
	public String getTpdAuraResons() {
		return tpdAuraResons;
	}
	public void setTpdAuraResons(String tpdAuraResons) {
		this.tpdAuraResons = tpdAuraResons;
	}
	public String getIpAuraResons() {
		return ipAuraResons;
	}
	public void setIpAuraResons(String ipAuraResons) {
		this.ipAuraResons = ipAuraResons;
	}
	public String getDeathResons() {
		return deathResons;
	}
	public void setDeathResons(String deathResons) {
		this.deathResons = deathResons;
	}
	public String getTpdResons() {
		return tpdResons;
	}
	public void setTpdResons(String tpdResons) {
		this.tpdResons = tpdResons;
	}
	public String getIpResons() {
		return ipResons;
	}
	public void setIpResons(String ipResons) {
		this.ipResons = ipResons;
	}
	public ResponseObject getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(ResponseObject responseObject) {
		this.responseObject = responseObject;
	}
	public String getOverallDecision() {
		return overallDecision;
	}
	public void setOverallDecision(String overallDecision) {
		this.overallDecision = overallDecision;
	}
	public String getDeathDecision() {
		return deathDecision;
	}
	public void setDeathDecision(String deathDecision) {
		this.deathDecision = deathDecision;
	}
	public String getDeathLoading() {
		return deathLoading;
	}
	public void setDeathLoading(String deathLoading) {
		this.deathLoading = deathLoading;
	}
	public String getDeathExclusions() {
		return deathExclusions;
	}
	public void setDeathExclusions(String deathExclusions) {
		this.deathExclusions = deathExclusions;
	}
	public String getTpdDecision() {
		return tpdDecision;
	}
	public void setTpdDecision(String tpdDecision) {
		this.tpdDecision = tpdDecision;
	}
	public String getTpdLoading() {
		return tpdLoading;
	}
	public void setTpdLoading(String tpdLoading) {
		this.tpdLoading = tpdLoading;
	}
	public String getTpdExclusions() {
		return tpdExclusions;
	}
	public void setTpdExclusions(String tpdExclusions) {
		this.tpdExclusions = tpdExclusions;
	}
	public String getIpDecision() {
		return ipDecision;
	}
	public void setIpDecision(String ipDecision) {
		this.ipDecision = ipDecision;
	}
	public String getIpLoading() {
		return ipLoading;
	}
	public void setIpLoading(String ipLoading) {
		this.ipLoading = ipLoading;
	}
	public String getIpExclusions() {
		return ipExclusions;
	}
	public void setIpExclusions(String ipExclusions) {
		this.ipExclusions = ipExclusions;
	}
	

}
