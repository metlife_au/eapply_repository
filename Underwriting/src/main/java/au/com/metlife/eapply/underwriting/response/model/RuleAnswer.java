package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class RuleAnswer implements Serializable{
	private String alias;
	private String impId;
	private String med;
	private String resultType;
	private String riskType;
	private String sev;
	private String subCatId;
	private String value;
	private boolean answered = false;
	
	public boolean isAnswered() {
		return answered;
	}
	public void setAnswered(boolean answered) {
		this.answered = answered;
	}
	private List <Rule>listRule;
	private List <Decision>listUWDecisions;
	public String getImpId() {
		return impId;
	}
	public void setImpId(String impId) {
		this.impId = impId;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getResultType() {
		return resultType;
	}
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
	public String getRiskType() {
		return riskType;
	}
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	public String getSev() {
		return sev;
	}
	public void setSev(String sev) {
		this.sev = sev;
	}
	public String getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(String subCatId) {
		this.subCatId = subCatId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public List<Rule> getListRule() {
		
		if (null == listRule) {
			listRule = new ArrayList<Rule>();
		}
		return listRule;
	}
	public List<Decision> getListUWDecisions() {
		
		if (null == listUWDecisions) {
			listUWDecisions = new ArrayList<Decision>();
		}
		return listUWDecisions;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
}
