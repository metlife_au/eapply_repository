<!DOCTYPE html>
<html ng-app="CorporateApp" >
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="../../eapply/static/corporate/src/assets/images/favicon.ico">
    <link rel="stylesheet" href="../../eapply/resources/css/bootstrap-4.min.css">
    <link rel="stylesheet" href="../../eapply/resources/css/ngDialog.css">
    <link rel="stylesheet" href="../../eapply/static/corporate/src/assets/styles/ngDialog-theme-plain.css">

    <link rel="stylesheet" href="../../eapply/static/corporate/src/assets/styles/custom.css">
    <link rel="stylesheet" href="../../eapply/static/corporate/src/assets/styles/jquery-ui.min.css">
    <title>MetLife Corporate Insurance</title>
  </head>
  <%
  String headerData = response.getHeader("InputData");
  String nonMvStatus = response.getHeader("nonmv");
  String Channel = response.getHeader("Channel");
  String token = response.getHeader("token");
  String auraId=response.getHeader("auraId");
  String domainName = response.getHeader("domainname");
  %>
  <!-- Add a comment to this line -->
  <script type="text/javascript">
    var inputData = <%=headerData%>;
    var memberStatus =  '<%=nonMvStatus%>';
    var channel = '<%=Channel%>';
    var token ='<%=token%>';
    var auraId ='<%=auraId%>';
    var baseUrl = '<%= domainName%>';
    console.log(inputData);
    console.log(channel);
    console.log(token);
    console.log(auraId);
  </script>
  <body class="tooltip-demo" metlife-shrink-header>
    <div id="body-mask" data-loading>
      <div class="body-mask-loader"></div>
    </div>
    <!-- THIS IS WHERE WE WILL INJECT OUR CONTENT ============================== -->
    <metlife-corp-header-nav></metlife-corp-header-nav>
    <div ui-view autoscroll="true"></div>
    <metlife-corp-footer></metlife-corp-footer>

    <!-- Application library file -->
    <script src="../../eapply/static/corporate/dist/lib.js"></script>
    
    <!-- Application source file -->
    <script src="../../eapply/static/corporate/dist/app.js"></script>
  </body>
</html>