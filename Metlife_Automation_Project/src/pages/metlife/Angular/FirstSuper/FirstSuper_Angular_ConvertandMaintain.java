package pages.metlife.Angular.FirstSuper;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_ConvertandMaintain extends MasterPage {
	WebDriverUtil driverUtil = null;

	public FirstSuper_Angular_ConvertandMaintain ConvertandMaintain_FirstSuper() {
		ConvertandMaintain();
		
		confirmation();
		
		return new FirstSuper_Angular_ConvertandMaintain(scriptHelper);
	}
	
	

	public FirstSuper_Angular_ConvertandMaintain(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ConvertandMaintain() {

		String EmailId = dataTable.getData("FirstSuper_ConvertandMaintain", "EmailId");
		String TypeofContact = dataTable.getData("FirstSuper_ConvertandMaintain", "TypeofContact");
		String ContactNumber = dataTable.getData("FirstSuper_ConvertandMaintain", "ContactNumber");
		String TimeofContact = dataTable.getData("FirstSuper_ConvertandMaintain", "TimeofContact");
		
		String FifteenHoursWork = dataTable.getData("FirstSuper_ConvertandMaintain", "FifteenHoursWork");
		
		String IndustryType = dataTable.getData("FirstSuper_ConvertandMaintain", "IndustryType");
		String OccupationType = dataTable.getData("FirstSuper_ConvertandMaintain", "OccupationType");
		String OtherOccupation = dataTable.getData("FirstSuper_ConvertandMaintain", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("FirstSuper_ConvertandMaintain", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("FirstSuper_ConvertandMaintain", "TertiaryQual");
		String Eightypercenttimeinoffice = dataTable.getData("FirstSuper_ConvertandMaintain", "Eightypercenttimeinoffice");
		
		String AnnualSalary = dataTable.getData("FirstSuper_ConvertandMaintain", "AnnualSalary");
		
		String CostType = dataTable.getData("FirstSuper_ConvertandMaintain", "CostType");
		
		
		
		try{
			
			
			
			//****Click on Convert and Maintain Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Convert and maintain')]"))).click();
			report.updateTestLog("Convert and Maintain", "Clicked on Convert and Maintain Link Link", Status.PASS);
			
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);

			
            //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);
		
            //*****Enter the Email Id*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\			
				
				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
				Contact.selectByVisibleText(TypeofContact);			
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
								
				
			//****Enter the Contact Number****\\	
				
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
				
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
					
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				
				//****Contact Details-Continue Button*****\\			
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);			
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
				//*****Select the 15 Hours Question*******\\		
				
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week?  ']/../following-sibling::div/div/div[1]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()=' Do you work more than 15 hours per week?  ']/../following-sibling::div/div/div[2]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
			
						
				//*****Industry********\\		
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("industry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(800);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
				sleep(500);
				
				//*****Occupation*****\\		
				
				Select Occupation=new Select(driver.findElement(By.name("occupation")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(500);
						
						
				//*****Other Occupation *****\\	
				
				if(OccupationType.equalsIgnoreCase("Other")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);	
				}		
				

				//*****Extra Questions*****\\							
				
			    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
			    
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\	
			    	
			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
			   
			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
										
			}							
			else{							

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
										
			}							
										
			      //*****Do You Hold a Tertiary Qualification******\\							
										
			        if(TertiaryQual.equalsIgnoreCase("Yes")){							
			       
			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}							
			   else{							
			       
				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				
			}		
			        sleep(500);
			        
			}							
										
										
										
			else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
										
				//******Do you spend at least 80% of all working time in an office*****\\
				if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[1]/label/span"))).click();
					report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[2]/label/span"))).click();
					report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
				}
				sleep(500);						
				 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){										

					 //***Select if your office Duties are Undertaken within an Office Environment?\\	
    	
    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
   
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
		report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
							
      }							
            else{							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
	report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
							
         }							
							
      //*****Do You Hold a Tertiary Qualification******\\							
							
        if(TertiaryQual.equalsIgnoreCase("Yes")){							
       
        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
			report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
           }							
   else{							
       
	   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
		report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
           }									
			}	
			}
									    
	          sleep(800);
			   
			   		
			//*****What is your annual Salary******							
										
			
			    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
			    sleep(500);		
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
			  
			    //*****Display the cost of my insurance cover as******\\
				
				Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
				CostofInsurance.selectByVisibleText(CostType);
				sleep(400);
				report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);							
			
			
			//*****Click on Continue*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		
			sleep(3000);
		  //******Click on Continue******\\
		    
		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[1]"))).click();
		 sleep(2500);
		 report.updateTestLog("Continue", "Continue button is selected", Status.PASS); 
		   
			sleep(3000);			
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void confirmation() {
	
	WebDriverWait wait=new WebDriverWait(driver,20);	
		
		try{
			if(driver.getPageSource().contains("You must click and read the General Consent before proceeding")){
				
		//****Validate the existing details******\\
				
		sleep(500);
		driver.findElement(By.xpath("(//h5[text()='Cover type'])[1]")).click();
		report.updateTestLog("Existing Details", "Validate existing details", Status.SCREENSHOT);
		
			

			
		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);
			
			
		//******Click on Submit*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(5000);			
			
		}
		
		//*******Feedback popup******\\
			WebDriverWait waits=new WebDriverWait(driver,40);	
			waits.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1500);
		
		if(driver.getPageSource().contains("APPLICATION NUMBER")){
			
		
		//*****Fetching the Application Status*****\\
		
		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);
		
		//*****Fetching the Application Number******\\
		
		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);
		
		
		
		//******Download the PDF***********\\
		properties = Settings.getInstance();	
		String StrBrowseName=properties.getProperty("currentBrowser");
		if (StrBrowseName.equalsIgnoreCase("Chrome")){
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			sleep(1500);
  		}
		
		
		//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
		
		
		
	  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
	  			sleep(1500);
	  			Robot roboobj=new Robot();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);	
	  		}
	  		
	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
	  			Robot roboobj=new Robot();
	  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
	  				
	  			sleep(4000);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			roboobj.keyPress(KeyEvent.VK_ENTER);	
	  			
	  		}
		
		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
		
		sleep(1000);
	
		 
		  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);
		
			
		 report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);	
		}

		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}


}
