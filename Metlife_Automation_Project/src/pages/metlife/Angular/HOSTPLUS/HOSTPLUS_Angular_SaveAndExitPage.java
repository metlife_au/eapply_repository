package pages.metlife.Angular.HOSTPLUS;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_SaveAndExitPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_SaveAndExitPage SaveandExit_HOSTPLUS() {
		SaveandExit();


		return new HOSTPLUS_Angular_SaveAndExitPage(scriptHelper);
	}

	public HOSTPLUS_Angular_SaveAndExitPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void SaveandExit() {


		String EmailId = dataTable.getData("HOSTPLUS_SaveAndExit", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_SaveAndExit", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_SaveAndExit", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_SaveAndExit", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_SaveAndExit", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_SaveAndExit", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_SaveAndExit", "RegularIncome");
		String Perform_WithoutRestriction = dataTable.getData("HOSTPLUS_SaveAndExit", "Perform_WithoutRestriction");
		String Work_WithoutLimitation = dataTable.getData("HOSTPLUS_SaveAndExit", "Work_WithoutLimitation");
		String Citizen = dataTable.getData("HOSTPLUS_SaveAndExit", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_SaveAndExit", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_SaveAndExit", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_SaveAndExit", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_SaveAndExit", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_SaveAndExit", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_SaveAndExit", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_SaveAndExit", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_SaveAndExit", "AnnualSalary");
		String CostType=dataTable.getData("HOSTPLUS_SaveAndExit", "CostType");
		String DeathYN=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathYN");
		String AmountType=dataTable.getData("HOSTPLUS_SaveAndExit", "AmountType");
		String DeathAction=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathAction");
		String DeathAmount=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathAmount");
		String DeathUnits=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathUnits");
		String TPDAction=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDAction");
		String TPDAmount=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDAmount");
		String TPDUnits=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDUnits");
		String IPYN=dataTable.getData("HOSTPLUS_SaveAndExit", "IPYN");
		String IPAction=dataTable.getData("HOSTPLUS_SaveAndExit", "IPAction");
		String Insure90Percent=dataTable.getData("HOSTPLUS_SaveAndExit", "Insure90Percent");
		String WaitingPeriod=dataTable.getData("HOSTPLUS_SaveAndExit", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("HOSTPLUS_SaveAndExit", "BenefitPeriod");
		String IPAmount=dataTable.getData("HOSTPLUS_SaveAndExit", "IPAmount");
		String TPDYN=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDYN");


		try{



			//****Click on Change Your Insurance Cover Link*****\\
			waitForPageTitle("testing", 16);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Change your insurance cover']"))).click();

			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(2000);

			}

			//*****Agree to Duty of disclosure*******\\
			waitForPageTitle("testing", 9);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);



           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			sleep(200);

			//*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.name("contactType")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


		//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]")).click();

				sleep(400);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]")).click();
				sleep(400);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}


			//***********Select the Gender******************\\

			if(Gender.equalsIgnoreCase("Male")){

				driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}


			//***************************************\\
			//**********OCCUPATION SECTION***********\\
			//****************************************\\

				//*****Select the 15 Hours Question*******\\

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion']/parent::*"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}

		//***Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*****\\

			if(RegularIncome.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
				sleep(500);
				report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
				sleep(300);

				//*****Perform duty without restriction due to illness or injury?*****\\

				if(Perform_WithoutRestriction.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected Yes", Status.PASS);
					sleep(300);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected No", Status.PASS);
					sleep(300);
				}
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
				sleep(500);
				report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
				sleep(300);

                   //*****Work Without Limitations *****\\

				if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Work Without Limitations", "Selected Yes", Status.PASS);
					sleep(300);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Work Without Limitations", "Selected No", Status.PASS);
					sleep(300);
				}

			}

			//*****Resident of Australia****\\

			/*if(Citizen.equalsIgnoreCase("Yes")){
				driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

				}
			else{
				driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

				}*/

			//*****Industry********\\
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("industry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(1500);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();


			//*****Occupation*****\\

			Select Occupation=new Select(driver.findElement(By.name("occupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);


			//*****Other Occupation *****\\

			if(OccupationType.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}


			//*****Extra Questions*****\\

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
				sleep(1000);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
			sleep(1000);
		}

		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					sleep(1000);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				sleep(1000);
		}
		}



		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



		//******Do you work in hazardous environment*****\\

		if(HazardousEnv.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[1]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			sleep(1000);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			sleep(1000);
		}

		//******Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?*******\\

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
			sleep(1000);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
			sleep(1000);
		}
		 sleep(800);

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    	 if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
						sleep(1000);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
					sleep(1000);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
							sleep(1000);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
						sleep(1000);
				}
				}

	}

      sleep(800);


	//*****What is your annual Salary******


	    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
	    sleep(500);

	    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //label[text()='What is your annual salary?']"))).click();
	    sleep(500);
	    sleep(300);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,400)", "");
		sleep(500);

	//*****Click on Continue*******\\
/*
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(occupationForm);']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
	*/

//**************************************************************************************************************\\
//****************************COVER CALCULATOR*****************************************************************\\
//*************************************************************************************************************\\
		//******Click on Cost of  Insurance as*******\\
				sleep(1000);
				Select COSTTYPE=new Select(driver.findElement(By.name("premFreq")));
				COSTTYPE.selectByVisibleText(CostType);
				sleep(250);
				report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
				sleep(500);



							//*******Death Cover Section******\\

				//restricted 3 digits
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
				sleep(500);

							if(DeathYN.equalsIgnoreCase("Yes")){

						/*		Select deathaction=new Select(driver.findElement(By.name("coverName")));
								deathaction.selectByVisibleText(DeathAction);
								sleep(300);
								report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
								sleep(600);
								*/

			if(AmountType.equalsIgnoreCase("unit")){

									wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
									sleep(800);
									report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

								}
								if(AmountType.equalsIgnoreCase("Convert")){

									wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
									sleep(800);
									report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

								}


					//		if(DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){

								if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert")){
									sleep(250);
									wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
									sleep(500);
									wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);
									sleep(800);
									report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
									waitForPageTitle("testing", 3);


								}
								else{
									sleep(250);
									wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
									sleep(500);
									wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathUnits);
									sleep(800);
									report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
									sleep(800);
									waitForPageTitle("testing", 3);
								}
							}
						//	}

							//*******TPD Cover Section******\\
						    sleep(800);


							if(TPDYN.equalsIgnoreCase("Yes"))
							{

						/*		Select tpdaction=new Select(driver.findElement(By.name("tpdCoverName")));
								tpdaction.selectByVisibleText(TPDAction);
								sleep(500);
								report.updateTestLog("TPD Cover action", TPDAction+" is selected on TPD cover", Status.PASS);
								sleep(800);	*/

						//		if(TPDAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")){

									if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert"))
									{
										sleep(250);
										wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
										sleep(500);
										wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDAmount);
										sleep(800);
										report.updateTestLog("TPD Cover Amount", TPDAmount+" is entered", Status.PASS);
										wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
										sleep(800);
										waitForPageTitle("testing", 3);
									}

									else
									{
										sleep(250);
										wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
										sleep(500);
										wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(TPDUnits);
										sleep(800);
										report.updateTestLog("TPD Cover Amount", TPDUnits+" is entered", Status.PASS);
										wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
										sleep(800);
										waitForPageTitle("testing", 3);
									}

								}
						//	}




						                 //*******IP Cover section********\\
						sleep(500);
						if(IPYN.equalsIgnoreCase("Yes")){
					/*
							Select ipaction=new Select(driver.findElement(By.name("ipCoverName")));
							ipaction.selectByVisibleText(IPAction);
							sleep(500);
							report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);*/

							//*****Insure 90% of My Salary******\\

							if(!IPAction.equalsIgnoreCase("No change")){
								if(Insure90Percent.equalsIgnoreCase("Yes")){
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='ipsalarycheck']/parent::*"))).click();
								report.updateTestLog("Insure 90%", "Insure 90% of My Salary is Selected", Status.PASS);
								sleep(500);
								waitForPageTitle("testing", 1);
								}
							}
							sleep(500);
							if(!IPAction.equalsIgnoreCase("No change")){

								Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.waitingPeriod']")));
								waitingperiod.selectByVisibleText(WaitingPeriod);
								sleep(300);
								report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
								sleep(500);


								Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.benefitPeriod']")));
								benefitperiod.selectByVisibleText(BenefitPeriod);
								sleep(300);
								report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
								sleep(2000);
							}
							sleep(500);
							if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){

								if(!Insure90Percent.equalsIgnoreCase("Yes"))
								{
									sleep(250);
									wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
									sleep(500);
									wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(IPAmount);
									sleep(500);
									report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
									sleep(800);
									waitForPageTitle("testing", 1);
								}

							}
						}
						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,700)", "");
						sleep(500);


						sleep(1000);

JavascriptExecutor s = (JavascriptExecutor) driver;
s.executeScript("window.scrollBy(0,1000)", "");
sleep(1500);


				//****Save & Exit******\\
				sleep(800);
				driver.findElement(By.xpath("(//button[@ng-click='saveQuote()'])[1]")).click();
				report.updateTestLog("Save & Exit button", "Save & Exit button is clicked", Status.PASS);

			    //****Fetch the Application No******\\

				sleep(800);
				String AppNo=driver.findElement(By.xpath("//div[@id='tips_text']/strong[2]")).getText();
				report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);

				//******Finish and Close Window******\\

				sleep(500);
			//	quickSwitchWindows();
				driver.findElement(By.xpath("//button[text()='Finish & Close Window ']")).click();
				report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
				sleep(1500);



			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}




	}





