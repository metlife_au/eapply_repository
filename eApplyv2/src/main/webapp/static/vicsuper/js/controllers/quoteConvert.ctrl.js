/* Change Cover Controller,Progressive and Mandatory validations Starts  */
VicsuperApp.controller('quoteConvert',['$scope', '$rootScope', '$routeParams', '$location','$http', '$timeout', '$window', 'QuoteService', 'OccupationService','persoanlDetailService',
    'deathCoverService','tpdCoverService','ipCoverService', 'CalculateService', 'MaxLimitService','ConvertService',
    'auraInputService', 'CalculateDeathService', 'CalculateTPDService', 'PersistenceService','ngDialog','auraResponseService','saveEapply','RetrieveAppDetailsService','urlService','DownloadPDFService','printQuotePage','NewOccupationService','$filter','APP_CONSTANTS',
    function($scope, $rootScope, $routeParams, $location,$http, $timeout, $window, QuoteService, OccupationService, persoanlDetailService, deathCoverService, tpdCoverService,
  		  ipCoverService, CalculateService, MaxLimitService,ConvertService,auraInputService, CalculateDeathService,
  		  CalculateTPDService, PersistenceService,ngDialog,auraResponseService,saveEapply,RetrieveAppDetailsService,urlService,DownloadPDFService,printQuotePage,NewOccupationService,$filter,APP_CONSTANTS){
		
	var  standard = 'general';
	var  whitecolor = 'white collar';
	var  ownoccupation = 'own occupation';
	var  professional = 'professional';
		
	/* Code for appD starts */
	var pageTracker = null;
	if(ADRUM) {
		pageTracker = new ADRUM.events.VPageView();
		pageTracker.start();
	}

	$scope.$on('$destroy', function() {
		pageTracker.end();
		ADRUM.report(pageTracker);
	});
	/* Code for appD ends */
	
		$scope.phoneNumbr = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
		$scope.emailFormat = APP_CONSTANTS.emailFormat;
		$scope.regex = /[0-9]{1,3}/;
		$scope.isDCCoverRequiredDisabled = true;
		$scope.isDCCoverTypeDisabled = true;
		$scope.isTPDCoverRequiredDisabled = true;
		$scope.isTPDCoverTypeDisabled = true;
		$scope.isTPDCoverNameDisabled = true;
		$scope.isWaitingPeriodDisabled = true;
		$scope.isBenefitPeriodDisabled = true;
		$scope.isIPCoverRequiredDisabled = true;
		$scope.isIPCoverNameDisabled = true;
		$scope.isIpSalaryCheckboxDisabled = true;
		$scope.ipWarningFlag = false;
		$scope.coltwo = false;
		$scope.colthree = false;
		$scope.ackFlag = false;
		$scope.modalShown = false;
	    $scope.dcCoverAmount = 0.00;
		$scope.dcWeeklyCost = 0.00;
		$scope.tpdCoverAmount = 0.00;
		$scope.tpdWeeklyCost = 0.00;
		$scope.ipCoverAmount = 0.00;
		$scope.ipWeeklyCost = 0.00;
		$scope.totalWeeklyCost = 0.00;
		$scope.invalidSalAmount = false;
		$scope.dcIncreaseFlag = false;
		$scope.tpdIncreaseFlag = false;
		$scope.ipIncreaseFlag = false;
		$scope.auraDisabled = false;
		$scope.disclaimerFlag = true;
		$scope.otherOccupationObj = {'otherOccupation': ''};
		$scope.showWithinOfficeQuestion = false;
	    $scope.showTertiaryQuestion = false;
	    $scope.showHazardousQuestion = false;
	    $scope.showOutsideOfficeQuestion = false;
		$scope.urlList = urlService.getUrlList();
		$scope.premFreq = 'Weekly';
		$scope.preferredContactType = '';
		$scope.ipWarning = false;
		$scope.prevOtherOcc = null;
		var deathCoverRounded = '';
		$scope.disableGender = false;
		$scope.tpdTapering = 1.00;
		$scope.tpdConvertAmt = 0;
		$rootScope.$broadcast('enablepointer');

		/*Error Flags*/
		$scope.dodFlagErr = null;
		$scope.privacyFlagErr = null;

		$scope.DeathCoverOptionsOne = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    }, {
	    	key: 'option2',
	    	coverOptionName: 'Decrease your cover'
	    },
	    {
	    	key: 'option5',
	    	coverOptionName: 'No change'
	    }];

	    $scope.TPDCoverOptionsOne = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    }, {
	    	key: 'option2',
	    	coverOptionName: 'Decrease your cover'
	    },
	    {
	    	key: 'option5',
	    	coverOptionName: 'No change'
	    },
	    {
	    	key: 'option6',
	    	coverOptionName: 'Same as Death Cover'
	    }];

	    $scope.IPCoverOptionsOne = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    }, {
	    	key: 'option2',
	    	coverOptionName: 'Decrease your cover'
	    },

	    {
	    	key: 'option5',
	    	coverOptionName: 'No change'
	    },
	    {
	    	key: 'option6',
	    	coverOptionName: 'Change Waiting and Benifit Period'
	    }
	    ];

	    $scope.coverOptionsTwo = [{
	    	key: 'option1',
	    	coverOptionName: 'Increase your cover'
	    },
	    {
	    	key: 'option2',
	    	coverOptionName: 'No change'
	    }
	    ];
	    $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
	    $scope.benefitPeriodOptions = ['2 Years','5 Years','Age 65']
	    $scope.premiumFrequencyOptions = ['Weekly','Monthly', 'Yearly'];
	    $scope.contactTypeOptions = ["Home phone", "Work phone", "Mobile"];

		var dynamicFlag = false;
		var fetchAppnum = true;
		var appNum;
		var ackCheck;
		var DCMaxAmount, TPDMaxAmount;
		var FormOneFields;
		var deathAmount, TPDAmount, IPAmount;
		var ipUnchanged = true;
		var mode3Flag = false;
		var unitisedDeathInitialFlag = false;
		var unitisedTpdInitialFlag = false;
		var tpdUnitFlag = true, tpdUnitSuccess = false;
		var maxTPDUnits;
		var disableIpCover = false;
		var OccupationFormFields, OccupationOtherFormFields;
		var annualSalForUpgradeVal;
		var deathDBCategory, tpdDBCategory, ipDBCategory;

		// added for session expiry


		$scope.deathCoverDetails = deathCoverService.getDeathCover();
		$scope.tpdCoverDetails = tpdCoverService.getTpdCover();
		$scope.ipCoverDetails = ipCoverService.getIpCover();
		
		//if($scope.deathCoverDetails.type == "2" || $scope.tpdCoverDetails.type == "2"){
			/*$scope.indexationDeath = $scope.deathCoverDetails.indexation;
			$scope.indexationTpd = $scope.tpdCoverDetails.indexation;*/
			/*if ($scope.deathCoverDetails.indexation == 'Y' || $scope.tpdCoverDetails.indexation == 'Y'){
				$scope.indexationDeath = 'true';
				$scope.indexationTpd = 'true';
			} else if ($scope.deathCoverDetails.indexation == 'N' && $scope.tpdCoverDetails.indexation == 'N'){
				$scope.indexationDeath = 'false';
				$scope.indexationTpd = 'false';
			} else {*/
				$scope.indexationDeath = 'true';
				$scope.indexationTpd = 'true';
			/*}*/
		//}

//		$scope.deathOccupationCategory = $scope.deathCoverDetails.occRating;
//		$scope.tpdOccupationCategory = $scope.tpdCoverDetails.occRating;
//		$scope.ipOccupationCategory = $scope.ipCoverDetails.occRating;
//
//		if($scope.deathCoverDetails && $scope.deathCoverDetails.occRating && $scope.deathCoverDetails.occRating != ''){
//  			$scope.deathOccupationCategory = $scope.deathCoverDetails.occRating;
//  		} else{
//  			$scope.deathOccupationCategory = 'General';
//  		}
//  		if($scope.tpdCoverDetails && $scope.tpdCoverDetails.occRating && $scope.tpdCoverDetails.occRating != ''){
//  			$scope.tpdOccupationCategory = $scope.tpdCoverDetails.occRating;
//  		} else{
//  			$scope.tpdOccupationCategory = 'General';
//  		}
//  		if($scope.ipCoverDetails && $scope.ipCoverDetails.occRating && $scope.ipCoverDetails.occRating != ''){
//  			$scope.ipOccupationCategory = $scope.ipCoverDetails.occRating;
//  		} else{
//  			$scope.ipOccupationCategory = 'General';
//  		}
  		
  		
  		
  		
		
		/*$scope.inputMsgOccRating = Math.max(deathoccRating,tpdoccRating,ipoccRating);		
		
		 if ($scope.inputMsgOccRating == 3){
	        	$scope.deathOccupationCategory = 'Professional';
	        	$scope.tpdOccupationCategory = 'Professional';
	        	$scope.ipOccupationCategory = 'Professional';
	        	
	        } else if ($scope.inputMsgOccRating == 2) {
	        	$scope.deathOccupationCategory = 'White Collar';
	        	$scope.tpdOccupationCategory = 'White Collar';
	        	$scope.ipOccupationCategory = 'White Collar';
	        } else {
	        	$scope.deathOccupationCategory = 'General';
	        	$scope.tpdOccupationCategory = 'General';
	        	$scope.ipOccupationCategory = 'General';
	        }
		 */
		 
		 
//		$scope.deathOccCategory = $scope.inputMsgOccRating || 'Standard';
//		$scope.tpdOccCategory = $scope.inputMsgOccRating || 'Standard';
//		$scope.ipOccCategory = $scope.inputMsgOccRating || 'Standard';

		 
		 switch($scope.ipCoverDetails.waitingPeriod.toLowerCase().trim().substr(0,2)){
			
			case "30":
				$scope.ipCoverDetails.waitingPeriod = "30 Days";
				break;
			case "60":
				$scope.ipCoverDetails.waitingPeriod = "60 Days";
				break;
			case "90":
				$scope.ipCoverDetails.waitingPeriod = "90 Days";
				break;
			}
		switch($scope.ipCoverDetails.benefitPeriod.toLowerCase().trim().substr(0,1)){
			
			case "2":
				$scope.ipCoverDetails.benefitPeriod = "2 Years";
				break;
			case "5":
				$scope.ipCoverDetails.benefitPeriod = "5 Years";
				break;
			case "A":
				$scope.ipCoverDetails.benefitPeriod = "Age 65";
				break;
			case "a":
				$scope.ipCoverDetails.benefitPeriod = "Age 65";
				break;
			}
		 

		if($scope.ipCoverDetails && $scope.ipCoverDetails.waitingPeriod && $scope.ipCoverDetails.waitingPeriod != ''){
			$scope.waitingPeriodAddnl = $scope.ipCoverDetails.waitingPeriod;
		} else {
			$scope.waitingPeriodAddnl = '90 Days';
		}

		if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitPeriod && $scope.ipCoverDetails.benefitPeriod != ''){
			$scope.benefitPeriodAddnl = $scope.ipCoverDetails.benefitPeriod;
		} else{
			$scope.benefitPeriodAddnl = '2 Years';
		}
		
		var deathoccRating = $scope.deathCoverDetails.occRating;
		if (deathoccRating) {
			if (deathoccRating.toLowerCase() == standard){
				$scope.deathOccupationCategory = 'General';
			}
			else if (deathoccRating.toLowerCase() == whitecolor){
				$scope.deathOccupationCategory = 'White Collar';
			}
			else if (deathoccRating.toLowerCase() == professional){
				$scope.deathOccupationCategory = 'Professional';
			}
			else if(deathoccRating.toLowerCase() == ownoccupation)
				{
				$scope.deathOccupationCategory = 'Own Occupation';
				}
			else
				{
				$scope.deathOccupationCategory = 'General';
				}
		}
		else
		{
		$scope.deathOccupationCategory = 'General';
		}
		
		var tpdoccRating = $scope.tpdCoverDetails.occRating;
		if (tpdoccRating) {			
			if (tpdoccRating.toLowerCase() == standard){
				$scope.tpdOccupationCategory = 'General';
			}
			else if (tpdoccRating.toLowerCase() == whitecolor){
				$scope.tpdOccupationCategory = 'White Collar';
			}
			else if (tpdoccRating.toLowerCase() == professional){
				$scope.tpdOccupationCategory = 'Professional';
			}
			else if (tpdoccRating.toLowerCase() == ownoccupation){
				$scope.tpdOccupationCategory = 'Own Occupation';
			}
			else
				{
				$scope.tpdOccupationCategory = 'General';
				}
		}
		else
		{
		$scope.tpdOccupationCategory = 'General';
		}
		
		var ipoccRating = $scope.ipCoverDetails.occRating;
		if (ipoccRating) {			
			if (ipoccRating.toLowerCase() == standard){
				$scope.ipOccupationCategory = 'General';
			}
			else if (ipoccRating.toLowerCase() == whitecolor){
				$scope.ipOccupationCategory = 'White Collar';
			}
			else if (ipoccRating.toLowerCase() == professional){
				$scope.ipOccupationCategory = 'Professional';
			}
			else if (ipoccRating.toLowerCase() == ownoccupation && ($scope.benefitPeriodAddnl === '5 Years' || $scope.benefitPeriodAddnl === 'Age 65')){
				$scope.ipOccupationCategory = 'Own Occupation';
			}
			else
				{
				$scope.ipOccupationCategory = 'General';
				}
		}
		else
		{
		$scope.ipOccupationCategory = 'General';
		}

		var CoverCalculatorFormFields =['coverName','coverType','requireCover','tpdCoverName','tpdCoverType','TPDRequireCover'];
		var inputDetails = persoanlDetailService.getMemberDetails();
		$scope.personalDetails = inputDetails.personalDetails;

		if(inputDetails && inputDetails.contactDetails.emailAddress){
			$scope.email = inputDetails.contactDetails.emailAddress;
		}
		if(inputDetails && inputDetails.contactDetails.prefContactTime){
			if(inputDetails.contactDetails.prefContactTime == "1"){
				$scope.time= "Morning (9am - 12pm)";
			}else{
				$scope.time= "Afternoon (12pm - 6pm)";
			}
		}
		
		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.gender ='';
		}else{
			$scope.gender = $scope.personalDetails.gender;
		}
		
		if($scope.gender && (($scope.gender).toLowerCase() === 'female' || ($scope.gender).toLowerCase() === 'male' ))
    	{
    	$scope.disableGender = true;
    	}
		else
    	{
    	$scope.disableGender = false;
    	}

		if(inputDetails.contactDetails.prefContact==null || inputDetails.contactDetails.prefContact == "")
		{
			inputDetails.contactDetails.prefContact=1;
		}
		if(inputDetails && inputDetails.contactDetails.prefContact){
			if(inputDetails.contactDetails.prefContact == "1"){
				$scope.preferredContactType= "Mobile";
				$scope.changeCvrPhone = inputDetails.contactDetails.mobilePhone;
			}else if(inputDetails.contactDetails.prefContact == "2"){
				$scope.preferredContactType= "Home phone";
				$scope.changeCvrPhone = inputDetails.contactDetails.homePhone;
			}else if(inputDetails.contactDetails.prefContact == "3"){
				$scope.preferredContactType= "Work phone";
				$scope.changeCvrPhone = inputDetails.contactDetails.workPhone;
			}
	   }
		$scope.changePrefContactType = function(){
			if($scope.preferredContactType == "Home phone"){
				$scope.changeCvrPhone = inputDetails.contactDetails.homePhone;
			}else if($scope.preferredContactType == "Work phone"){
				$scope.changeCvrPhone = inputDetails.contactDetails.workPhone;
			}else if($scope.preferredContactType == "Mobile"){
				$scope.changeCvrPhone = inputDetails.contactDetails.mobilePhone;
			} else {
				$scope.changeCvrPhone = '';
			}
		}


				FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime','gender'];

		OccupationFormFields = ['ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
	    OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];

		var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
		$scope.ageLimit=anb;
		
		 switch(anb){
		    
		    case 61 :
		    	$scope.tpdTapering = 0.90;
		    	break;
		    case 62 :
		    	$scope.tpdTapering = 0.80;
		    	break;
		    case 63 :
		    	$scope.tpdTapering = 0.70;
		    	break;
		    case 64 :
		    	$scope.tpdTapering = 0.60;
		    	break;
		    case 65 :
		    	$scope.tpdTapering = 0.50;
		    	break;
		    case 66 :
		    	$scope.tpdTapering = 0.40;
		    	break;
		    case 67 :
		    	$scope.tpdTapering = 0.30;
		    	break;
		    case 68 :
		    	$scope.tpdTapering = 0.20;
		    	break;
		    case 69 :
		    	$scope.tpdTapering = 0.20;
		    	break;
		    
		    }
		var tpdValidAge=64;
		if(parseInt($scope.tpdCoverDetails.amount) > 0 )
			{
			tpdValidAge = 69;
			}
		if($scope.ageLimit > 60)
			{
		 $scope.tpdConvertAmt = Math.ceil(($scope.tpdTapering * ($scope.deathCoverDetails.amount)));
		 if(parseInt($scope.tpdConvertAmt) > parseInt($scope.tpdCoverDetails.amount))
			 {
			 $scope.tpdConvertAmt = $scope.tpdCoverDetails.amount;
			 }
			}
		else
			{
			$scope.tpdConvertAmt = $scope.tpdCoverDetails.amount;
			}
		if($scope.deathCoverDetails && $scope.deathCoverDetails.benefitType && $scope.deathCoverDetails.benefitType == 1 ){
			if($scope.ageLimit < 14 || $scope.ageLimit > 69){
				$('#deathsection').removeClass('active');
				$("#death").css("display", "none");
				$scope.isDeathDisabled = true;
			}
		}
		if($scope.tpdCoverDetails && $scope.tpdCoverDetails.benefitType && $scope.tpdCoverDetails.benefitType == 2 ){
			/*if($scope.ageLimit < 14 || $scope.ageLimit > 64){*/
			if($scope.ageLimit < 14 || $scope.ageLimit > tpdValidAge){
				$('#tpdsection').removeClass('active');
				$("#tpd").css("display", "none");
				$scope.isTPDDisabled = true;
			}
		}
		if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitType && $scope.ipCoverDetails.benefitType == 4 ){
			if($scope.ageLimit < 14 || $scope.ageLimit > 64){
				$('#ipsection').removeClass('active');
				$("#sc").css("display", "none");
				$scope.isIPDisabled = true;
			}
		}

//		QuoteService.getList($scope.urlList.quoteUrl,"VICT").then(function(res){
//			$scope.IndustryOptions = res.data;
//		}, function(err){
//			console.info("Error while fetching industry list " + JSON.stringify(err));
//		});

		MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"VICT",inputDetails.memberType,"CCOVER").then(function(res){
			var limits = res.data;
			annualSalForUpgradeVal = limits[0].annualSalForUpgradeVal;
	    	DCMaxAmount = limits[0].deathMaxAmount;
	    	TPDMaxAmount = limits[0].tpdMaxAmount;

		}, function(error){
			console.info('Something went wrong while fetching limits ' + error);
		});


		$scope.isEmpty = function(value){
			return ((value == "" || value == null) || value == "0");
		};

		$scope.checkLimits = function(){
			if(parseInt($scope.requireCover) > parseInt(DCMaxAmount)){
				$scope.deathMaxFlag = true;
			}
			$scope.calculateOnChange();
		};

//	    $scope.getOccupations = function(){
//	    	if($scope.otherOccupationObj)
//	    		$scope.otherOccupationObj.otherOccupation = '';
//	    	if(!$scope.industry){
//	    		$scope.industry = '';
//	    	}
//	    	OccupationService.getOccupationList($scope.urlList.occupationUrl, "VICT", $scope.industry).then(function(res){
//	    		$scope.OccupationList = res.data;
//	    		$scope.occupation = "";
//	        	$scope.calculateOnChange();
//	    	}, function(err){
//	    		console.info("Error while fetching occupations " + JSON.stringify(err));
//	    	});
//	    };
	    
//	    $scope.getOtherOccupationAS = function(entered) {
//
//		    return $http.get('./occupation.json').then(function(response) {
//		      $scope.occupationList=[];
//	        if(response.data.Other) {
//		        for (var key in response.data.Other) {
//		              var obj={};
//		              obj.id=key;
//		               obj.name=response.data.Other[key];
//	                 //if(obj.name.indexOf(entered) > -1) {
//	                  $scope.occupationList.push(obj.name);
//	                 //}
//		               
//		        }
//		      }
//	        return $filter('filter')($scope.occupationList, entered);
//		    }, function(err){
//		      console.info("Error while fetching occupations " + JSON.stringify(err));
//		    });
//		    
//		  };

//	    $scope.checkOwnBusinessQuestion = function(){
//	    	if($scope.ownBussinessQuestion == 'Yes'){
//		    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
//			    OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
//		    } else if($scope.ownBussinessQuestion == 'No'){
//		    	OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
//			    OccupationOtherFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
//		    }
//	    };

	  //Death, TPD and IP Validations
	    $scope.deathErrorFlag = false;
	    $scope.tpdErrorFlag = false;
	    $scope.ipErrorFlag = false;
	    $scope.deathErrorMsg ="";
	    $scope.tpdErrorMsg ="";
	    $scope.ipErrorMsg ="";


	$scope.validateDeathTpdIpAmounts = function(){
	  	if($scope.coverType == "DcFixed"){
			if($scope.requireCover > TPDMaxAmount){
		  		$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
					return obj.coverOptionName != 'Same as Death Cover';
				});
		  	} else{
		  		var tpdOption = $scope.TPDCoverOptionsOne.some(function(obj){
					return obj.coverOptionName == 'Same as Death Cover';
				});
				if(!tpdOption){
					$scope.TPDCoverOptionsOne.push({'key':'option6', 'coverOptionName':'Same as Death Cover'});
				}
		  	}
	  	}
		/*Death validations Starts*/
	    if($scope.coverName == 'Increase your cover'){
	        if($scope.coverType == "DcFixed"){
  		    	if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.amount) ){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="You cannot reduce your Death Only cover. Please re-enter your cover amount.";
  		    	}else if(parseInt($scope.deathCoverDetails.amount) == parseInt($scope.requireCover)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(parseInt($scope.requireCover) > parseInt(DCMaxAmount)){
		 	  	    $scope.deathErrorFlag = true;
		 	  	    $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is  " + DCMaxAmount + ". Please re-enter your cover.";
		 	  	}else if(parseInt($scope.requireCover) > parseInt($scope.deathCoverDetails.amount)){
		 	  		$scope.deathErrorFlag = false;
		 	  	    $scope.deathErrorMsg="";
		 	  	}
	         }else if($scope.coverType == "DcUnitised"){
  		    	if(deathAmount < parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Your converted amount is " +deathAmount+ ".You cannot reduce your Death Only cover. Please re-enter your cover amount";
  		    	}else if(deathAmount == parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(deathAmount > parseInt(DCMaxAmount)){
  		    		$scope.deathErrorFlag = true;
		 	  	    $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxAmount + ". Please re-enter your cover.";
  		    	}else if(deathAmount > parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = false;
  		    		$scope.deathErrorMsg ="";
  		    	}
	       }
	    }else if($scope.coverName == 'Decrease your cover'){
	    	if($scope.coverType == "DcFixed"){
  		    	if(parseInt($scope.requireCover) > parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Please enter your Death cover less than to your existing cover.";
  		    	}else if(parseInt($scope.deathCoverDetails.amount) == parseInt($scope.requireCover)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(parseInt($scope.requireCover) < parseInt($scope.deathCoverDetails.amount)){
	  		    	$scope.deathErrorFlag = false;
	  		    	$scope.deathErrorMsg ="";
  		        }
	        }else if($scope.coverType == "DcUnitised"){
	        	if(deathAmount > parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Your converted amount is " +deathAmount+ ".Please enter your Death cover less than to your existing cover.";
  		    	}else if(deathAmount == parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = true;
  		    		$scope.deathErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(deathAmount > parseInt(DCMaxAmount)){
  		    		$scope.deathErrorFlag = true;
		 	  	    $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is " + DCMaxAmount + ". Please re-enter your cover.";
  		    	}else if(deathAmount < parseInt($scope.deathCoverDetails.amount)){
  		    		$scope.deathErrorFlag = false;
  		    		$scope.deathErrorMsg ="";
  		    	}
	        }
	   }
	 /*Death validations Ends*/

	 /*TPD validations Starts*/
	    if($scope.tpdCoverName == 'Increase your cover'){
	        if($scope.tpdCoverType == "TPDFixed"){
	        	if($scope.TPDRequireCover != null && $scope.requireCover == null ){
	        		$scope.tpdErrorFlag = true;
	      			$scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
	     	    }
	        	else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.amount) ){
  		    		$scope.tpdErrorFlag = true;
  		    		$scope.tpdErrorMsg ="You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
  		    	}else if(parseInt($scope.tpdCoverDetails.amount) == parseInt($scope.TPDRequireCover)){
  		    		$scope.tpdErrorFlag = true;
  		    		$scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(parseInt($scope.TPDRequireCover) > parseInt(TPDMaxAmount)){
		 	  	    $scope.tpdErrorFlag = true;
		 	  	    $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
		 	  	}else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
		 	  	    $scope.tpdErrorFlag = true;
		 	  	    $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
 		 	  	}else if(parseInt($scope.TPDRequireCover) > parseInt($scope.tpdCoverDetails.amount)){
		 	  		$scope.tpdErrorFlag = false;
		 	  	    $scope.tpdErrorMsg="";
		 	  	}
	         }else if($scope.tpdCoverType == "TPDUnitised"){
	        	    if($scope.TPDRequireCover != null && $scope.requireCover == null ){
		        		$scope.tpdErrorFlag = true;
		      			$scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
		     	    }
	        	    else if(TPDAmount < parseInt($scope.tpdCoverDetails.amount)){
	  		    		$scope.tpdErrorFlag = true;
	  		    		$scope.tpdErrorMsg ="Your converted amount is " +TPDAmount+ ".You cannot reduce your TPD Only cover. Please re-enter your cover amount";
	  		    	}else if(TPDAmount == parseInt($scope.tpdCoverDetails.amount)){
	  		    		$scope.tpdErrorFlag = true;
	  		    		$scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
	  		    	}else if(TPDAmount > TPDMaxAmount){
			 	  	    $scope.tpdErrorFlag = true;
			 	  	    $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
			 	  	}else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
			 	  	    $scope.tpdErrorFlag = true;
			 	  	    $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
	 		 	  	}else if(TPDAmount > parseInt($scope.tpdCoverDetails.amount)){
	  		    		$scope.tpdErrorFlag = false;
	  		    		$scope.tpdErrorMsg ="";
	  		    	}
	       }
	    }else if($scope.tpdCoverName == 'Decrease your cover'){
	    	if($scope.tpdCoverType == "TPDFixed"){
	    		if($scope.TPDRequireCover != null && $scope.requireCover == null ){
	        		$scope.tpdErrorFlag = true;
	      			$scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
	    	    }else if(parseInt($scope.TPDRequireCover) > parseInt($scope.tpdCoverDetails.amount)){
  		    		$scope.tpdErrorFlag = true;
  		    		$scope.tpdErrorMsg ="Please enter your TPD cover less than to your existing cover.";
  		    	}else if(parseInt($scope.tpdCoverDetails.amount) == parseInt($scope.TPDRequireCover)){
  		    		$scope.tpdErrorFlag = true;
  		    		$scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
		 	  	    $scope.tpdErrorFlag = true;
		 	  	    $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
 		 	  	}else if(parseInt($scope.TPDRequireCover) < parseInt($scope.tpdCoverDetails.amount)){
	  		    	$scope.tpdErrorFlag = false;
	  		    	$scope.tpdErrorMsg ="";
  		        }
	        }else if($scope.tpdCoverType == "TPDUnitised"){
	        	if($scope.TPDRequireCover != null && $scope.requireCover == null ){
	        		$scope.tpdErrorFlag = true;
	      			$scope.tpdErrorMsg="You cannot apply for TPD cover without Death Cover.";
    	        }else if(TPDAmount > parseInt($scope.tpdCoverDetails.amount)){
  		    		$scope.tpdErrorFlag = true;
  		    		$scope.tpdErrorMsg ="Your converted amount is " +TPDAmount+ ".Please enter your TPD cover less than to your existing cover.";
  		    	}else if(TPDAmount == parseInt($scope.tpdCoverDetails.amount)){
  		    		$scope.tpdErrorFlag = true;
  		    		$scope.tpdErrorMsg ="Existing and additional cover amounts are same.";
  		    	}else if(TPDAmount > TPDMaxAmount){
		 	  	    $scope.tpdErrorFlag = true;
		 	  	    $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + TPDMaxAmount + ". Please re-enter your cover.";
		 	  	}else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
		 	  	    $scope.tpdErrorFlag = true;
		 	  	    $scope.tpdErrorMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
 		 	  	}else if(TPDAmount < parseInt($scope.tpdCoverDetails.amount)){
  		    		$scope.tpdErrorFlag = false;
  		    		$scope.tpdErrorMsg ="";
  		    	}
	        }
	   }
	 /*TPD validations Ends*/

   /*IP validations Starts*/
    if($scope.ipCoverName == 'Increase your cover'){
    	if(parseInt($scope.IPRequireCover) < parseInt($scope.ipCoverDetails.amount)){
    		$scope.ipErrorFlag = true;
    		$scope.ipErrorMsg="You cannot reduce your IP Only cover. Please re-enter your cover amount";
    	}else if(parseInt($scope.IPRequireCover) == parseInt($scope.ipCoverDetails.amount)){
    		$scope.ipErrorFlag = true;
    		$scope.ipErrorMsg="Existing and additional cover amounts are same.";
    	}else if(parseInt($scope.IPRequireCover) > IPAmount){
    		$scope.IPRequireCover = IPAmount;
	 	    $scope.ipErrorFlag = false;
	 	    $scope.ipErrorMsg="";
	 	    $scope.ipWarningFlag = true;
    	}else if(parseInt($scope.IPRequireCover) > parseInt($scope.ipCoverDetails.amount)){
    		$scope.ipErrorFlag = false;
    		$scope.ipErrorMsg="";
    	}
    }else if($scope.ipCoverName == 'Decrease your cover'){
    	if(parseInt($scope.IPRequireCover) > parseInt($scope.ipCoverDetails.amount)) {
    		$scope.ipErrorFlag = true;
    		$scope.ipErrorMsg="Please enter your IP cover less than to your existing cover.";
    	}else if(parseInt($scope.IPRequireCover) == parseInt($scope.ipCoverDetails.amount)) {
    		$scope.ipErrorFlag = true;
    		$scope.ipErrorMsg="Existing and additional cover amounts are same.";
    	}else if(parseInt($scope.IPRequireCover) < parseInt($scope.ipCoverDetails.amount)){
    		$scope.ipErrorFlag = false;
    		$scope.ipErrorMsg="";
    	}
    }
    /*IP validations Ends*/
  		$scope.calculateOnChange();
 };
 
 $scope.toggleIndexation = function(val) {
	  
	  
	  if (val == 'true'){
		  $scope.indexationTpd = 'true';
		  $scope.indexationDeath = 'true';
	  } else {
		  $scope.indexationTpd = 'false';
		  $scope.indexationDeath = 'false';
	  }
	  
 } 


	    $scope.navigateToLandingPage = function (){

	    	ngDialog.openConfirm({
  	            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
  	            plain: true,
  	            className: 'ngdialog-theme-plain custom-width'
  	        }).then(function(){
  	        	$location.path("/landing");
  	        }, function(e){
  	        	if(e=='oncancel'){
  	        		return false;
  	        	}
  	        });
	    }


	    $scope.syncRadios = function(val){

	    	if($scope.coverName == 'No change') {
	    		$scope.requireCover = val == 'Unitised' ? $scope.deathCoverDetails.units : $scope.deathCoverDetails.amount;

	    	}

	    	if($scope.tpdCoverName == 'No change') {
	    		$scope.TPDRequireCover = val == 'Unitised' ? $scope.tpdCoverDetails.units : $scope.tpdCoverDetails.amount;

	    	}

    		$scope.requireCover = $scope.requireCover || "";
    		$scope.TPDRequireCover = $scope.TPDRequireCover || "";
	    	var radios = $('label[radio-sync]');
	    	var data = $('input[data-sync]');
			data.filter('[data-sync="' + val + '"]').attr('checked','checked');
			if(val == 'Fixed'){
				$scope.deathErrorFlag = false;
	 	  	    $scope.deathErrorMsg="";
	 	  	    $scope.tpdErrorFlag = false;
	    		$scope.tpdErrorMsg ="";
				showhide('dollar1','nodollar1');
				showhide('dollar','nodollar');
				$scope.coverType = 'DcFixed';
				$scope.tpdCoverType = 'TPDFixed';
			} else if(val == 'Unitised'){
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg="";
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				showhide('nodollar1','dollar1');
				showhide('nodollar','dollar');
				$scope.coverType = 'DcUnitised';
				$scope.tpdCoverType = 'TPDUnitised';
			}
			radios.removeClass('active');
			radios.filter('[radio-sync="' + val + '"]').addClass('active');

			$scope.calculateOnChange();
	    };

	    $scope.convertDeathUnitsToAmount = function(){
	    	if($scope.requireCover && $scope.requireCover != ''){
	    		if($scope.tpdCoverName == 'Same as Death Cover')
	    			$scope.TPDRequireCover = $scope.requireCover;
	    		unitisedDeathInitialFlag = true;
    		}
	    	//$scope.getNewOccupation();
	    	//$scope.renderOccupationQuestions();
	    	var deathReqObject = {
	        		"age": anb,
	        		"fundCode": "VICT",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"smoker": false,
	        		"deathUnits": parseInt($scope.requireCover),
	        		"deathUnitsCost": null,
	        		"premiumFrequency": "Weekly",
	        		"memberType": null,
	        		"manageType": "CCOVER",
	        		"deathCoverType": $scope.coverType
	        	};
	    	if(unitisedDeathInitialFlag){
	    		CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
	    			deathAmount = res.data[0].coverAmount;
	    			$scope.validateDeathTpdIpAmounts();
	    		}, function(err){
	    			console.info('Error while fetching death amount ' + err);
	    		});
	    	}

	    	if(tpdUnitFlag){
		    	var unitTPDReqObj = {
		        		"age": anb,
		        		"fundCode": "VICT",
		        		"gender": $scope.gender,
		        		"tpdOccCategory": $scope.tpdOccupationCategory,
		        		"smoker": false,
		        		"tpdUnits": 1,
		        		"tpdUnitsCost": null,
		        		"premiumFrequency": "Weekly",
		        		"memberType": null,
		        		"manageType": "CCOVER",
		        		"tpdCoverType": "TPDUnitised",
		        	};
		    	CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,unitTPDReqObj).then(function(res){
		    		tpdUnitFlag = false;
		    		tpdUnitSuccess = true;
		    		var TPDUnitAmount = res.data[0].coverAmount;
		    		maxTPDUnits = Math.floor(TPDMaxAmount/TPDUnitAmount);
		    	}, function(err){
		    		console.info('Error while fetching TPD amount ' + err);
		    	});
	    	}

	    	if(tpdUnitSuccess){
	    		if($scope.requireCover > maxTPDUnits){
	    			$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
	    				return obj.coverOptionName != 'Same as Death Cover';
	    			});
	    		} else{
	    			var optionExists = $scope.TPDCoverOptionsOne.some(function(obj){
	    				return obj.coverOptionName == 'Same as Death Cover';
	    			});
	    			if(!optionExists){
	    				$scope.TPDCoverOptionsOne.push({'key':'option6', 'coverOptionName':'Same as Death Cover'});
	    			}
	    		}
	    	}
	    };

	    $scope.convertTPDUnitsToAmount = function(){
	    	if($scope.TPDRequireCover && $scope.TPDRequireCover != ''){
	    		unitisedTpdInitialFlag = true;
	    	}
	    	//$scope.getNewOccupation();
	    	//$scope.renderOccupationQuestions();
	    	var TPDReqObject = {
	        		"age": anb,
	        		"fundCode": "VICT",
	        		"gender": $scope.gender,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"smoker": false,
	        		"tpdUnits": parseInt($scope.TPDRequireCover),
	        		"tpdUnitsCost": null,
	        		"premiumFrequency": "Weekly",
	        		"memberType": null,
	        		"manageType": "CCOVER",
	        		"tpdCoverType": $scope.tpdCoverType,
	        	};
	    	CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,TPDReqObject).then(function(res){
	    		TPDAmount = res.data[0].coverAmount;
	    		if(unitisedTpdInitialFlag){
	    			$scope.validateDeathTpdIpAmounts();
	    		}
	    	}, function(err){
	    		console.info('Error while fetching TPD amount ' + err);
	    	});
	    };

	    $scope.checkIpCover = function(){
	    	$scope.ipWarningFlag = false;
      		if(parseInt($scope.annualSalary) <= 1000000){
      			IPAmount  = Math.ceil((0.85 * ($scope.annualSalary/12)));
      		} else if(parseInt($scope.annualSalary) > 1000000){
      			IPAmount = Math.ceil((Math.round(((parseInt($scope.annualSalary))/12)*0.6) ));
      		}
      		$scope.validateDeathTpdIpAmounts();
	    };

	    $scope.indexation= {
	    		death: false,
	    		disable: false
	    }
	    $scope.setIndexation = function ($event) {
	    	$event.stopPropagation();
	    	$event.preventDefault();
	    	$scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
	    	if(!$scope.indexation.death) {
	    		$("#changecvr_index-dth").parent().removeClass('active');
	    		$("#changeCvr-indextpd-disable").parent().removeClass('active');
	    	}
	    }

	    $scope.deathDecOrCancelFlag=false;
	    $scope.tpdDecOrCancelFlag=false;
	    $scope.ipDecOrCancelFlag=false;

		$scope.checkDeathOption = function () {
			if($scope.coverName == 'Cancel your cover'){
				$scope.dcIncreaseFlag = false;
				$scope.tpdIncreaseFlag = false;
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg ="";
				$scope.deathDecOrCancelFlag=true;
				$scope.tpdDecOrCancelFlag=true;
				$scope.isDCCoverRequiredDisabled = true;
				$scope.isDCCoverTypeDisabled = true;
				$scope.isTPDCoverRequiredDisabled = true;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = true;
				$scope.tpdCoverName = 'Cancel your cover';
				if(!mode3Flag){
					$scope.requireCover = '';
					$scope.TPDRequireCover = '';
				}
			} else if($scope.coverName == 'Increase your cover'){
				$scope.dcIncreaseFlag = true;
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg ="";
				$scope.deathDecOrCancelFlag=false;
				$scope.isDCCoverRequiredDisabled = false;
				$scope.isDCCoverTypeDisabled = $scope.deathCoverDetails.type == 2 ? true : false;
				if($scope.isTPDCoverRequiredDisabled != true){
					$scope.isTPDCoverRequiredDisabled = false;
				}
				if($scope.isTPDCoverTypeDisabled != true){
					$scope.isTPDCoverTypeDisabled = false;
				}
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.requireCover = '';
				}
			}else if( $scope.coverName == 'Decrease your cover'){
				$scope.dcIncreaseFlag = false;
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg ="";
				$scope.deathDecOrCancelFlag=true;
				$scope.isDCCoverRequiredDisabled = false;
				$scope.isDCCoverTypeDisabled = $scope.deathCoverDetails.type == 2 ? true : false;
				if($scope.isTPDCoverRequiredDisabled != true){
					$scope.isTPDCoverRequiredDisabled = false;
				}
				if($scope.isTPDCoverTypeDisabled != true){
					$scope.isTPDCoverTypeDisabled = false;
				}
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.requireCover = '';
				}
			} else if($scope.coverName == 'No change'){
				$scope.dcIncreaseFlag = false;
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg ="";
				$scope.deathDecOrCancelFlag=false;
				$scope.isDCCoverRequiredDisabled = true;
				$scope.isDCCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = false;
				$scope.isTPDCoverTypeDisabled = true;
				if($scope.isTPDCoverRequiredDisabled != true){
					$scope.isTPDCoverRequiredDisabled = false;
				}
				//$scope.coverType = $scope.exDcCoverType;
				$scope.tpdCoverType = $scope.exTpdCoverType;
				if(!mode3Flag){
					$scope.TPDRequireCover = "";
					$scope.coverType = $scope.exDcCoverType;
				}
				if($scope.coverType == "DcUnitised"){
					$scope.requireCover = $scope.deathCoverDetails.units;
					 showhide('nodollar1','dollar1');
	                 showhide('nodollar','dollar');
				} else if($scope.coverType == "DcFixed"){
					$scope.requireCover = parseInt($scope.deathCoverDetails.amount);
					showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
				}
			}else if($scope.coverName == 'Convert and maintain cover'){
				$scope.dcIncreaseFlag = false;
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg ="";
                if($scope.deathCoverDetails.type == "1"){
                    $scope.coverType = "DcFixed";
                    $scope.tpdCoverType = "TPDFixed";
                    showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
			    } else if($scope.deathCoverDetails.type == "2"){
                    $scope.coverType = "DcUnitised";
                    $scope.tpdCoverType = 'TPDUnitised';
                    showhide('nodollar1','dollar1');
                    showhide('nodollar','dollar');
			    }
                $scope.deathDecOrCancelFlag=false;
			    $scope.isDCCoverRequiredDisabled = true;
			    $scope.isDCCoverTypeDisabled = true;
			    $scope.isTPDCoverRequiredDisabled = false;
			    $scope.isTPDCoverTypeDisabled = true;
			    if(!mode3Flag){
			    	$scope.requireCover = '';
			    }
			    $scope.isTPDCoverNameDisabled = false;
			    if($scope.coverType != $scope.exDcCoverType){
					var found = false;
					$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
						return obj.coverOptionName !== 'No change';
					});
					for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
					    if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'Convert and maintain cover') {
					        found = true;
					        break;
					    }
					}
					if(!found){
						$scope.TPDCoverOptionsOne.push({key: 'option4',coverOptionName: 'Convert and maintain cover'});
					}
				} else{
					$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
						return obj.coverOptionName !== 'Convert and maintain cover';
					});
				}
//			    $scope.TPDRequireCover = '';

				$scope.convertDeathCover();
			} else if($scope.coverName == undefined || $scope.coverName == ''){
				$scope.deathErrorFlag = false;
				$scope.deathErrorMsg = "";
				$scope.deathDecOrCancelFlag = false;
				$scope.tpdDecOrCancelFlag = false;
				$scope.isDCCoverRequiredDisabled = true;
				$scope.isDCCoverTypeDisabled = true;
				$scope.isTPDCoverRequiredDisabled = true;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = true;
				$scope.requireCover = '';
				$scope.TPDRequireCover = '';
				$scope.dcIncreaseFlag = false;
			}

			if($scope.tpdCoverName == "Convert and maintain cover"){
				var nochangeFound = false;
				$scope.tpdCoverName = "";
				$scope.TPDRequireCover = "";
				$scope.isTPDCoverNameDisabled = false;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverRequiredDisabled = true;

				$scope.TPDCoverOptionsOne = $scope.TPDCoverOptionsOne.filter(function(obj){
					return obj.coverOptionName !== 'Convert and maintain cover';
				});

				if($scope.coverType == $scope.exDcCoverType){
					for(var i = 0; i < $scope.TPDCoverOptionsOne.length; i++) {
					    if ($scope.TPDCoverOptionsOne[i].coverOptionName == 'No change') {
					    	nochangeFound = true;
					        break;
					    }
					}
					if(!nochangeFound){
						$scope.TPDCoverOptionsOne.push({key: 'option5',coverOptionName: 'No change'});
					}
				}
			}

		//	$scope.decOrCancelCovers();
			if($scope.tpdCoverName == 'Same as Death Cover')
	    		$scope.TPDRequireCover = $scope.requireCover;
			$scope.calculateOnChange();
        }

		$scope.checkTPDOption = function () {
			if($scope.tpdCoverName == 'Cancel your cover'){
				$scope.tpdIncreaseFlag = false;
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				$scope.tpdDecOrCancelFlag=true;
				if($scope.isDCCoverRequiredDisabled != true){
					$scope.isDCCoverRequiredDisabled = false;
				}
				if($scope.isDCCoverTypeDisabled != true){
					$scope.isDCCoverTypeDisabled = false;
				}
				$scope.isTPDCoverRequiredDisabled = true;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.TPDRequireCover = '';
				}
			} else if($scope.tpdCoverName == 'Increase your cover'){
				$scope.tpdIncreaseFlag = true;
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				$scope.tpdDecOrCancelFlag=false;
				if($scope.isDCCoverRequiredDisabled != true){
					$scope.isDCCoverRequiredDisabled = false;
				}
				if($scope.isDCCoverTypeDisabled != true){
					$scope.isDCCoverTypeDisabled = false;
				}
				$scope.isTPDCoverRequiredDisabled = false;
				$scope.isTPDCoverTypeDisabled = false;
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.TPDRequireCover = '';
				}
			}else if($scope.tpdCoverName == 'Decrease your cover'){
				$scope.tpdIncreaseFlag = false;
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				$scope.tpdDecOrCancelFlag=true;
				if($scope.isDCCoverRequiredDisabled != true){
					$scope.isDCCoverRequiredDisabled = false;
				}
				if($scope.isDCCoverTypeDisabled != true){
					$scope.isDCCoverTypeDisabled = false;
				}
				$scope.isTPDCoverRequiredDisabled = false;
				$scope.isTPDCoverTypeDisabled = false;
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.TPDRequireCover = '';
				}
			} else if($scope.tpdCoverName == 'No change'){
				$scope.tpdIncreaseFlag = false;
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				$scope.tpdDecOrCancelFlag=false;
				if($scope.isDCCoverRequiredDisabled != true){
					$scope.isDCCoverRequiredDisabled = false;
				}
				if($scope.isDCCoverTypeDisabled != true){
					$scope.isDCCoverTypeDisabled = false;
				}
				$scope.isTPDCoverRequiredDisabled = true;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.tpdCoverType = $scope.exTpdCoverType;
				}
				if($scope.tpdCoverType == "TPDUnitised"){
					$scope.TPDRequireCover = $scope.tpdCoverDetails.units;
					showhide('nodollar1','dollar1');
	                showhide('nodollar','dollar');
				} else if($scope.tpdCoverType == "TPDFixed"){
					$scope.TPDRequireCover = parseInt($scope.tpdCoverDetails.amount);
					showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
				}
			} else if($scope.tpdCoverName == 'Same as Death Cover'){
				$scope.tpdIncreaseFlag = false;
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
				$scope.tpdDecOrCancelFlag=false;
				if($scope.isDCCoverRequiredDisabled != true){
					$scope.isDCCoverRequiredDisabled = false;
				}
				if($scope.isDCCoverTypeDisabled != true){
					$scope.isDCCoverTypeDisabled = false;
				}
				$scope.isTPDCoverRequiredDisabled = true;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = false;
				if(!mode3Flag){
					$scope.TPDRequireCover = $scope.requireCover;
				}
			}else if($scope.tpdCoverName == "Convert and maintain cover"){
				$scope.tpdIncreaseFlag = false;
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg ="";
                if($scope.tpdCoverDetails.type == "1"){
                    $scope.coverType = "DcFixed";
                    $scope.tpdCoverType = "TPDFixed";
                    showhide('dollar1','nodollar1');
                    showhide('dollar','nodollar');
			    } else if($scope.tpdCoverDetails.type == "2"){
                    $scope.coverType = "DcUnitised";
                    $scope.tpdCoverType = 'TPDUnitised';
                    showhide('nodollar1','dollar1');
                    showhide('nodollar','dollar');
			    }
                if($scope.coverName == "Convert and maintain cover"){
                	$scope.isDCCoverRequiredDisabled = true;
                }
                $scope.tpdDecOrCancelFlag=false;
			    $scope.isDCCoverTypeDisabled = true;
			    $scope.isTPDCoverRequiredDisabled = true;
			    $scope.isTPDCoverTypeDisabled = true;
			    if(!mode3Flag){
			    	$scope.TPDRequireCover = '';
			    }
			    $scope.convertTPDCover();
			} else if($scope.tpdCoverName == undefined || $scope.tpdCoverName == ''){
				$scope.tpdErrorFlag = false;
				$scope.tpdErrorMsg = "";
				$scope.tpdDecOrCancelFlag = false;
				$scope.isDCCoverRequiredDisabled = true;
				$scope.isDCCoverTypeDisabled = true;
				$scope.isTPDCoverRequiredDisabled = true;
				$scope.isTPDCoverTypeDisabled = true;
				$scope.isTPDCoverNameDisabled = true;
				$scope.TPDRequireCover = '';
				$scope.tpdIncreaseFlag = false;
			}
		//	$scope.decOrCancelCovers();
			$scope.calculateOnChange();
        }

		$scope.checkIPOption = function () {
			if($scope.ipCoverName == 'Cancel your cover'){
				$scope.ipIncreaseFlag = false;
				$scope.ipErrorFlag = false;
				$scope.ipErrorMsg="";
				$scope.ipDecOrCancelFlag=true;
				$scope.isWaitingPeriodDisabled = true;
				$scope.isBenefitPeriodDisabled = true;
				if(!mode3Flag){
					$scope.IPRequireCover = "";
				}
				$scope.isIPCoverRequiredDisabled = true;
				$scope.isIPCoverNameDisabled = false;
				$scope.ipWarningFlag = false;
				$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
			} else if($scope.ipCoverName == 'Increase your cover'){
				$scope.ipIncreaseFlag = true;
				$scope.ipErrorFlag = false;
				$scope.ipErrorMsg="";
				$scope.ipDecOrCancelFlag=false;
				$scope.isWaitingPeriodDisabled = false;
				$scope.isBenefitPeriodDisabled = false;
				$scope.isIPCoverRequiredDisabled = false;
				$scope.isIpSalaryCheckboxDisabled = false;
				$scope.isIPCoverNameDisabled = false;
				$scope.ipWarningFlag = false;
				if(!mode3Flag){
					$scope.IPRequireCover = '';
				}
			} else if($scope.ipCoverName == 'Decrease your cover'){
				$scope.ipIncreaseFlag = false;
				$scope.ipErrorFlag = false;
				$scope.ipErrorMsg="";
				$scope.ipDecOrCancelFlag=true;
				$scope.isWaitingPeriodDisabled = false;
				$scope.isBenefitPeriodDisabled = false;
				$scope.isIPCoverRequiredDisabled = false;
				$scope.isIpSalaryCheckboxDisabled = true;
				$scope.isIPCoverNameDisabled = false;
				$scope.ipWarningFlag = false;
				if(!mode3Flag){
					$scope.IPRequireCover = '';
				}
				$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
			} else if($scope.ipCoverName == 'No change'){
				$scope.ipIncreaseFlag = false;
				$scope.ipErrorFlag = false;
				$scope.ipErrorMsg="";
				$scope.ipDecOrCancelFlag=false;
				$scope.isWaitingPeriodDisabled = true;
				$scope.isBenefitPeriodDisabled = true;
				$scope.IPRequireCover = parseInt($scope.ipCoverDetails.amount);

				$scope.isIPCoverRequiredDisabled = true;
				$scope.isIpSalaryCheckboxDisabled = true;
				$scope.isIPCoverNameDisabled = false;
				$scope.ipWarningFlag = false;
				$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
			} else if($scope.ipCoverName == 'Change Waiting and Benifit Period'){
				$scope.ipErrorFlag = false;
				$scope.ipErrorMsg="";
				$scope.ipDecOrCancelFlag=false;
				$scope.isWaitingPeriodDisabled = false;
				$scope.isBenefitPeriodDisabled = false;
				if(!mode3Flag){
					$scope.IPRequireCover = parseInt($scope.ipCoverDetails.amount);
				}
				$scope.isIPCoverRequiredDisabled = true;
				$scope.isIpSalaryCheckboxDisabled = true;
				$scope.isIPCoverNameDisabled = false;
				$scope.ipWarningFlag = false;
				$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
			} else if($scope.ipCoverName == undefined || $scope.ipCoverName == ''){
				$scope.ipIncreaseFlag = false;
				$scope.isIPCoverNameDisabled = false;
				$scope.isWaitingPeriodDisabled = true;
				$scope.isBenefitPeriodDisabled = true;
				$scope.isIPCoverRequiredDisabled = true;
				$scope.isIpSalaryCheckboxDisabled = true;
				$scope.IPRequireCover = '';
				$('#ipsalarycheck').removeAttr('checked');
				$('#ipsalarychecklabel').removeClass('active');
			}
			ipUnchanged = false;
			if(mode3Flag){
				mode3Flag = false;
				dynamicFlag = true;
			}
		//	$scope.decOrCancelCovers();
			$scope.calculateOnChange();
        }

		$scope.checkBenefitPeriod = function(){
			if($scope.ipCoverDetails.waitingPeriod == $scope.waitingPeriodAddnl && $scope.ipCoverDetails.benefitPeriod == $scope.benefitPeriodAddnl){
				$scope.ipIncreaseFlag = false;
				$scope.disclaimerFlag = true;
			} else if(($scope.ipCoverDetails.benefitPeriod == '2 Years' && $scope.benefitPeriodAddnl == '5 Years') ||
					($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.waitingPeriodAddnl == '60 Days') ||
					($scope.ipCoverDetails.waitingPeriod == '90 Days' && $scope.waitingPeriodAddnl == '30 Days') ||
					($scope.ipCoverDetails.waitingPeriod == '60 Days' && $scope.waitingPeriodAddnl == '30 Days')){
				$scope.ipIncreaseFlag = true;
				$scope.disclaimerFlag = true;
			} else{
				$scope.ipIncreaseFlag = false;
				$scope.disclaimerFlag = false;
			}
			$scope.calculateOnChange();
		};

	    // added for Decrease or Cancel cover popup "on continue" after calculate quote
		   $scope.showDecreaseOrCancelPopUp = function (val){
		   		if(val == null || val == "" || val == " "){
		   			hideTips();
		   		}else{
		   		 /*ackCheck = $('#termsLabel').hasClass('active');
		    		if(ackCheck){*/
		    			 $scope.ackFlag = false;
				   		 document.getElementById('mymodalDecCancel').style.display = 'block';
				   		 document.getElementById('mymodalDecCancelFade').style.display = 'block';
				   		 document.getElementById('decCancelMsg_text').innerHTML=val;
		    		/*}else{
		    			$scope.ackFlag = true;
		    		}*/

		   		}
		   	//	$scope.saveDataForPersistence();
		   	}
		    $scope.hideTips = function  (){
		   		if(document.getElementById('help_div')){
		   			document.getElementById('help_div').style.display = "none";
		   		}
		   	}

		 // added to check which covers are selected as Decrease/Cancel
			$scope.decCancelCover="";
			$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";

			$scope.checkForAura= function(){
				if(this.formOne.$valid /*&& this.occupationForm.$valid*/ && this.coverCalculatorForm.$valid){
					$scope.continueToNextPage('/convertSummary');
				}
		    }


		$scope.checkAnnualSalary = function(){
						$scope.calculateOnChange();

		};

	    $scope.go = function ( path ) {
	  	  $location.path( path );
	  	};

//	  	$scope.toggleIPCondition = function(){
//	  		/*if(parseInt($scope.annualSalary) == 0){
//	  			$scope.invalidSalAmount = true;
//	  		} else{
//	  			$scope.invalidSalAmount = false;
//	  		}*/
//	  		$scope.ipWarningFlag = false;
//	  		$timeout(function(){
//	  			ipCheckboxState = $('#ipsalarycheck').prop('checked');
//	  			if(ipCheckboxState == true){
//
//	  				if(parseInt($scope.annualSalary) <= 100000){
//	  					$scope.IPRequireCover  = Math.ceil((0.90 * ($scope.annualSalary/12)));
//	  				}else if(parseInt($scope.annualSalary) > 100000){
//		      			$scope.IPRequireCover = Math.ceil((Math.round(((parseInt($scope.annualSalary) - 100000)/12)*0.6) + 30000));
//		      		}
//		      		$scope.isIPCoverRequiredDisabled = true;
//		      		disableIpCover = true;
//	      		} else if(ipCheckboxState == false){
//	      			$scope.IPRequireCover = '';
//	      			disableIpCover = false;
//	      		}
//	  			$scope.checkAnnualSalary();
//
//	  		}, 10);
//
//	  	};

	  	$scope.continueToNextPage = function(path){
	  		$scope.saveDataForPersistence();
	  		$location.path(path);
	  	};

	  	/* Check if your is allowed to proceed to the next accordion */
	  	// TBC
	  	// Need to revisit, need better implementation
	  	$scope.isCollapsible = function(targetEle, event) {
	  		if( targetEle == 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
	  			if($('#dodLabel').is(':visible'))
	  				$scope.dodFlagErr = true;
	  			event.stopPropagation();
	  			return false;
	  		} else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
	  			if($('#privacyLabel').is(':visible'))
	  				$scope.privacyFlagErr = true;
	  			event.stopPropagation();
	  			return false;
	  		}  else if( targetEle == 'collapseTwo' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
	  			if($("#collapseOne form").is(':visible'))
	  				$scope.formOneSubmit($scope.formOne);
	  			event.stopPropagation();
	  			return false;
	  		}  else if( targetEle == 'collapseThree' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
	  			if($("#collapseTwo form").is(':visible'))
	  				$scope.formOneSubmit($scope.occupationForm);
	  			event.stopPropagation();
	  			return false;
	  		}
	  	}

	  	$scope.toggleTwo = function(checkFlag) {
	        $scope.coltwo = checkFlag;
	        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
	    };

	  	$scope.toggleThree = function(checkFlag) {
	        $scope.colthree = checkFlag;
	        if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseThree']").click(); /* Can be improved */
	    };

	    $scope.privacyCol = false;
	    var dodCheck;
	    var privacyCheck;
	    var dodVal = 0;
	    var privacyVal = 0;
	    var contactVal = 0;
	    var occupationVal = 0;
	    var coverVal = 0;

	    /* TBC */
	    $scope.togglePrivacy = function(checkFlag) {
	        $scope.privacyCol = checkFlag;
	        if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
	    };

	    $scope.toggleContact = function(checkFlag) {
	        $scope.contactCol = checkFlag;
	        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
	        	return false;
	        $("a[data-target='#collapseOne']").click(); /* Can be improved */

	    };
	    $scope.checkDodState = function() {
	    	$timeout(function() {
	    		$scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
	    		if($('#dodLabel').hasClass('active')) {
	    			$scope.togglePrivacy(true);
	    		} else {
	    			$scope.togglePrivacy(false);
	    			$scope.toggleContact(false);
	    			/*$scope.toggleTwo(false);*/
	    			$scope.toggleThree(false);
	    		}
	    	}, 1);
	    };

	    $scope.checkPrivacyState  = function() {
	    	$timeout(function() {
	    		$scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
	    		if($('#privacyLabel').hasClass('active')) {
	    			$scope.toggleContact(true);
	    		} else {
	    			$scope.toggleContact(false);
	    			/*$scope.toggleTwo(false);*/
	    			$scope.toggleThree(false);
	    		}
	    	}, 1);
	    };

	    $scope.calculate = function(){
	    	if($scope.requireCover == undefined || $scope.requireCover == ""){
	    		$scope.requireCover = 0;
	    	}
	    	if($scope.TPDRequireCover == undefined || $scope.TPDRequireCover == ""){
	    		$scope.TPDRequireCover = 0;
	    	}
	    	if($scope.IPRequireCover == undefined || $scope.IPRequireCover == ""){
	    		$scope.IPRequireCover = 0;
	    	}
	    	deathCoverRounded=$scope.deathCoverDetails.amount;
	    	tpdCoverRounded=$scope.tpdConvertAmt;
	    	/*if(((parseFloat(deathCoverRounded))%1000)>0)
			{
				
	    		deathCoverRounded=Math.ceil(Math.round((parseFloat($scope.deathCoverDetails.amount)) + (1000 - ((parseFloat($scope.deathCoverDetails.amount))%1000))));
					
			}
	    	if(((parseFloat($scope.tpdCoverDetails.amount))%1000)>0)
			{
				
	    		tpdCoverRounded=Math.ceil(Math.round((parseFloat($scope.tpdConvertAmt)) + (1000 - ((parseFloat($scope.tpdConvertAmt))%1000))));
					
			}*/
	    	var ipAmountFix = $scope.ipCoverDetails.amount;
	    		if($scope.isIPDisabled)
	    			{
	    			ipAmountFix = 0;
	    			}
	    		if($scope.isTPDDisabled)
	    			{
	    			tpdCoverRounded=0;
	    			}
	    	
	    	//$scope.getNewOccupation();
	    	//$scope.renderOccupationQuestions();
	    	var ruleModel = {
	        		"age": anb,
	        		"fundCode": "VICT",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"ipOccCategory": $scope.ipOccupationCategory,
	        		"smoker": false,
	        		"deathUnits": null,
	        		"deathFixedAmount": parseInt(deathCoverRounded),
	        		"deathFixedCost": null,
	        		"deathUnitsCost": null,
	        		"tpdUnits": null,
	        		"tpdFixedAmount": parseInt(tpdCoverRounded),
	        		"tpdFixedCost": null,
	        		"tpdUnitsCost": null,
	        		"ipUnits": null,
	        		"ipFixedAmount": parseInt(ipAmountFix),
	        		"ipFixedCost": null,
	        		"ipUnitsCost": null,
	        		"premiumFrequency": $scope.premFreq,
	        		"memberType": null,
	        		"manageType": "CCOVER",
	        		"deathCoverType": "DcFixed",
	        		"tpdCoverType": "TpdFixed",
	        		"ipCoverType": "IpFixed",
	        		"ipWaitingPeriod": $scope.waitingPeriodAddnl,
	        		"ipBenefitPeriod": $scope.benefitPeriodAddnl
	        	};
	    	CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
	    		var premium = res.data;
	    		dynamicFlag = true;
	    		for(var i = 0; i < premium.length; i++){
	    			if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
	    				$scope.dcCoverAmount = premium[i].coverAmount;
	    				$scope.dcWeeklyCost = premium[i].cost;
	    			} else if(premium[i].coverType == 'TpdFixed' || premium[i].coverType == 'TPDUnitised'){
	    				$scope.tpdCoverAmount = premium[i].coverAmount;
	    				$scope.tpdWeeklyCost = premium[i].cost;
	    				if($scope.isTPDDisabled)
	    					{
	    					$scope.tpdCoverAmount = 0.0;
		    				$scope.tpdWeeklyCost = 0.0;
	    					}
	    			} else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
        				$scope.ipCoverAmount = premium[i].coverAmount;
        				$scope.ipWeeklyCost = premium[i].cost;
        			}
        		}
	    		// to-do
	    		$scope.totalWeeklyCost1 = parseFloat($scope.dcWeeklyCost)+ parseFloat($scope.tpdWeeklyCost);
	    		$scope.totalWeeklyCost = parseFloat($scope.dcWeeklyCost)+ parseFloat($scope.tpdWeeklyCost)+parseFloat($scope.ipWeeklyCost);
        		if(fetchAppnum){
        			fetchAppnum = false;
        			appNum = PersistenceService.getAppNumber();
        		}
	    	}, function(err){
	    		console.info("Something went wrong while calculating..." + JSON.stringify(err));
	    	});
        };

	    $scope.convertDeathCover = function(){
	    	//$scope.getNewOccupation();
	    	//$scope.renderOccupationQuestions();
	    	var ruleModel = {
	        		"age": anb,
	        		"fundCode": "VICT",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"ipOccCategory": $scope.ipOccupationCategory,
	        		"smoker": false,
	        		"premiumFrequency": $scope.premFreq,
	        		"manageType": "CCOVER",
	        		"exDeathCoverType": $scope.exDcCoverType,
	        		"exTpdCoverType":null,
	        		"deathExistingAmount": parseInt($scope.deathCoverDetails.amount),
	        		"tpdExistingAmount":null
	        	};
	    	ConvertService.convert($scope.urlList.convertCoverUrl,ruleModel).then(function(res){
	    	//ConvertService.convert({}, ruleModel, function(res){
	    		var premium = res.data;
	    		for(var i = 0; i < premium.length; i++){
	    			$scope.requireCover = premium[i].convertedAmount;
        		}
        	}, function(err){
        		console.info("Error while converting death amount " + JSON.stringify(err));
        	});
        };
	    $scope.convertTPDCover = function(){
	    	//$scope.getNewOccupation();
	    	//$scope.renderOccupationQuestions();
	    	var ruleModel = {
	        		"age": anb,
	        		"fundCode": "VICT",
	        		"gender": $scope.gender,
	        		"deathOccCategory": $scope.deathOccupationCategory,
	        		"tpdOccCategory": $scope.tpdOccupationCategory,
	        		"ipOccCategory": $scope.ipOccupationCategory,
	        		"smoker": false,
	        		"premiumFrequency": $scope.premFreq,
	        		"manageType": "CCOVER",
	        		"exDeathCoverType": null,
	        		"exTpdCoverType":$scope.exTpdCoverType,
	        		"deathExistingAmount":null,
	        		"tpdExistingAmount":parseInt($scope.tpdCoverDetails.amount)
	        	};
	    	ConvertService.convert($scope.urlList.convertCoverUrl,ruleModel).then(function(res){
	    	//ConvertService.convert({}, ruleModel, function(res){
	    		var premium = res.data;
	    		for(var i = 0; i < premium.length; i++){
	    			$scope.TPDRequireCover = premium[i].convertedAmount;
        		}
        	}, function(err){
        		console.info("Error while converting TPD amount " + JSON.stringify(err));
        	});
        };

       $scope.calculateOnChange = function(){
    	   if(dynamicFlag && !$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag ){
     		   $scope.calculate();
    	   } else{
    		   //$scope.renderOccupationQuestions();
    	   }
       };

       // Progressive validation
	    $scope.checkPreviousMandatoryFields  = function (elementName,formName){
	    	var formFields;
	    	if(formName == 'formOne'){
	    		formFields = FormOneFields;
	    	} else if(formName == 'occupationForm'){
//	    		if($scope.occupation != undefined && $scope.occupation == 'Other'){
//	    			formFields = OccupationOtherFormFields;
//	    		} else{
	    			formFields = OccupationFormFields;
	    		//}
	    	} else if(formName == 'coverCalculatorForm'){
	    		formFields = CoverCalculatorFormFields;
	    	}
	      var inx = formFields.indexOf(elementName);
	      if(inx > 0){
	        for(var i = 0; i < inx ; i++){
	          $scope[formName][formFields[i]].$touched = true;
	        }
	      }
	    };

//	    $scope.getCategoryFromDB = function(){
//	    	/*if( $scope.prevOtherOcc !== $scope.occupationDetails.otherOccupation){*/
//		    	if($scope.occupation != undefined || $scope.otherOccupationObj.otherOccupation != undefined){
//			    	/*var occName = $scope.industry + ":" + $scope.occupation;*/
//		    		/*$scope.withinOfficeQuestion = null;
//		    		$scope.tertiaryQuestion = null;
//		    		$scope.hazardousQuestion = null;   
//		    		$scope.outsideOffice = null;*/
//		    		if($scope.occupation != undefined && ($scope.otherOccupationObj.otherOccupation == null || $scope.otherOccupationObj.otherOccupation == '')){
//		    			var occName = $scope.industry + ":" + $scope.occupation;
//		    		}else if($scope.otherOccupationObj.otherOccupation != undefined){
//		    			$scope.prevOtherOcc = $scope.otherOccupationObj.otherOccupation;
//		    			if(($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.otherOccupation))!== undefined){
//		    				var occName = $scope.industry + ":" + $scope.otherOccupationObj.otherOccupation;
//		    			}else{
//		    				var occName = $scope.industry + ":" + $scope.occupation;
//		    			}		    			
//		    		}
//			    	NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "VICT", occName).then(function(res){
//			    		deathDBCategory = res.data[0].deathfixedcategeory;
//			    		tpdDBCategory = res.data[0].tpdfixedcategeory;
//			    		ipDBCategory = res.data[0].ipfixedcategeory;
//			    		$scope.calculateOnChange();
//			    	}, function(err){
//			    		console.info("Error while getting category from DB " + JSON.stringify(err));
//			    	});
//		    	}
//	      /* }*/
//	    };

	    

	   // validate fields "on continue"
	    $scope.formOneSubmit =  function (form){
	      	if(form.$name == 'coverCalculatorForm') {
	    		$scope.checkIpCover();
	    		if($scope.exTpdCoverType == "TPDUnitised")
	    			$scope.convertTPDUnitsToAmount();

	    		if($scope.exDcCoverType == "DcUnitised")
	    			$scope.convertDeathUnitsToAmount();

	    		if($scope.exTpdCoverType == "TPDFixed" && $scope.exDcCoverType == "DcFixed")
	    			$scope.validateDeathTpdIpAmounts();
	    	}
	      if(!form.$valid){
	    	  form.$submitted=true;
	    	  if(form.$name == 'formOne'){
				  /*$scope.toggleTwo(false);*/
				  $scope.toggleThree(false);
			  }else if(form.$name == 'occupationForm'){
				  $scope.toggleThree(false);
			  }
		  } else{
			  if(form.$name == 'formOne'){
				 /* $scope.toggleTwo(true);*/
				  $scope.toggleThree(true);
				  $scope.calculate();
			  }else if(form.$name == 'occupationForm'){
				  $scope.toggleThree(true);
				  $scope.calculate();
			  }
	       }
	    };

	    $scope.checkAckState = function(){
	    	$timeout(function(){
	    		ackCheck = $('#termsLabel').hasClass('active');
	    		if(ackCheck){
	    			$scope.ackFlag = false;
	    		} else{
	    			$scope.ackFlag = true;
	    		}
	    	}, 10);
	    };
	    $scope.isUnitized = function(value){
	        return (value == "1");
	      };
	    $scope.showHelp = function(msg){
	    	$scope.modalShown = !$scope.modalShown;
	    	$scope.tipMsg = msg;
	    };

	    $scope.clickToOpen = function (hhText) {

	  		var dialog = ngDialog.open({
	  			/*template: '<p>'+hhText+'</p>' +
	  				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
	  			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
	  				className: 'ngdialog-theme-plain',
	  				plain: true
	  		});
	  		dialog.closePromise.then(function (data) {
	  			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
	  		});
	  	};

	    $scope.saveDataForPersistence = function(){

	    	var coverObj = {};
	    	var coverStateObj = {};
	    	var changeCoverOccObj = {};
	    	var deathAddnlCoverObj={};
	    	var tpdAddnlCoverObj={};
	    	var ipAddnlCoverObj={};
//	    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
//	    		return $scope.industry == obj.key;
//	    	});


	    			coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
	    			coverObj['firstName'] = $scope.personalDetails.firstName;
	    			coverObj['lastName'] = $scope.personalDetails.lastName;
	    	    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
	    	    	coverObj['email'] = $scope.email;
	    	    	changeCoverOccObj['gender'] = $scope.gender;
	    	    	coverObj['contactType'] = $scope.preferredContactType;
	    	    	coverObj['contactPhone'] = $scope.changeCvrPhone;
	    	    	coverObj['contactPrefTime'] = $scope.time;
	    	    	coverObj['convertCheck'] = true;


//	    	    	changeCoverOccObj['fifteenHr'] = $scope.fifteenHrsQuestion;
//	    	    	changeCoverOccObj['citizenQue'] = $scope.areyouperCitzQuestion;
	    	    	//changeCoverOccObj['industryName'] = selectedIndustry[0].value;
	    	    	//changeCoverOccObj['industryCode'] = selectedIndustry[0].key;
	    	    	//changeCoverOccObj['occupation'] = $scope.occupation;
	    	    	if( !($scope.showWithinOfficeQuestion && $scope.showTertiaryQuestion)){
	    	    		$scope.withinOfficeQuestion = null;
			    		$scope.tertiaryQuestion = null;
	              	}
	                if(!$scope.showHazardousQuestion && !$scope.showOutsideOfficeQuestion){
	                	$scope.hazardousQuestion = null;   
			    		$scope.outsideOffice = null;
	              	}
//	    	    	changeCoverOccObj['withinOfficeQue']= $scope.withinOfficeQuestion;
//	    	    	changeCoverOccObj['tertiaryQue']= $scope.tertiaryQuestion;
//	    	    	changeCoverOccObj['managementRoleQue']= $scope.outsideOffice;
//	    	    	changeCoverOccObj['hazardousQue']= $scope.hazardousQuestion;
//	    	    	changeCoverOccObj['ownBussinessQues']= $scope.ownBussinessQuestion;
//	    	    	changeCoverOccObj['ownBussinessYesQues']= $scope.ownBussinessYesQuestion;
//	    	    	changeCoverOccObj['ownBussinessNoQues']= $scope.ownBussinessNoQuestion;
//	    	    	changeCoverOccObj['salary'] = $scope.annualSalary;
//	    	    	changeCoverOccObj['otherOccupation'] = $scope.otherOccupationObj.otherOccupation;

	    	    	coverObj['existingDeathAmt'] = parseInt($scope.deathCoverDetails.amount);
	    	    	coverObj['existingDeathUnits'] = $scope.deathCoverDetails.units;
	    	    	coverObj['deathOccCategory'] = $scope.deathOccupationCategory;
	    	    	deathAddnlCoverObj['deathCoverName'] = $scope.coverName;
	    	    	deathAddnlCoverObj['deathCoverType'] = $scope.coverType;
	    	    	deathAddnlCoverObj['deathFixedAmt'] = parseFloat($scope.dcCoverAmount);
	    	    	deathAddnlCoverObj['deathInputTextValue'] = parseFloat($scope.requireCover);
	    	    	deathAddnlCoverObj['deathCoverPremium'] = parseFloat($scope.dcWeeklyCost);

	                coverObj['existingTpdAmt'] = parseFloat($scope.tpdCoverDetails.amount);
	                coverObj['existingTPDUnits'] = $scope.tpdCoverDetails.units;
	                coverObj['tpdOccCategory'] = $scope.tpdOccupationCategory;
	                tpdAddnlCoverObj['tpdCoverName'] = $scope.tpdCoverName;
	                tpdAddnlCoverObj['tpdCoverType'] = $scope.tpdCoverType;
	                tpdAddnlCoverObj['tpdFixedAmt'] = parseFloat($scope.tpdCoverAmount);
	                tpdAddnlCoverObj['tpdInputTextValue'] = parseFloat($scope.TPDRequireCover);
	                tpdAddnlCoverObj['tpdCoverPremium'] = parseFloat($scope.tpdWeeklyCost);

	                coverObj['existingIPUnits'] = $scope.ipCoverDetails.units;
	                coverObj['ipOccCategory'] = $scope.ipOccupationCategory;
	               // coverObj['ipcheckbox'] = ipCheckboxState;
	               // coverObj['ipSalaryPercent'] = 85;
	                ipAddnlCoverObj['ipCoverName'] = $scope.ipCoverName;
	                ipAddnlCoverObj['ipCoverType'] = 'IpUnitised';
	                ipAddnlCoverObj['waitingPeriod'] = $scope.waitingPeriodAddnl;
	                ipAddnlCoverObj['benefitPeriod'] = $scope.benefitPeriodAddnl;
	                ipAddnlCoverObj['ipFixedAmt'] = parseFloat($scope.ipCoverAmount);
	                ipAddnlCoverObj['ipInputTextValue'] = parseFloat($scope.IPRequireCover);
	                ipAddnlCoverObj['ipCoverPremium'] = parseFloat($scope.ipWeeklyCost);

	                coverObj['totalPremium'] = parseFloat($scope.totalWeeklyCost);
	                coverObj['totalPremium1'] = parseFloat($scope.totalWeeklyCost1);
	                coverObj['appNum'] = parseInt(appNum);
	                coverObj['auraDisabled'] = $scope.auraDisabled;
	                coverObj['ackCheck'] = ackCheck;
	                coverObj['dodCheck'] = $('#dodLabel').hasClass('active');
	                coverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');
	                coverObj['age'] = anb;
	                coverObj['manageType'] = 'CCOVER';
	                coverObj['partnerCode'] = 'VICT';
	                coverObj['freqCostType'] = $scope.premFreq;
	                coverObj['existingIPWaitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
	                coverObj['existingIPBenefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;
		            coverObj['lastSavedOn'] = 'Quotepage';


	                coverStateObj['isDCCoverRequiredDisabled'] = $scope.isDCCoverRequiredDisabled;
	                coverStateObj['isDCCoverTypeDisabled'] = $scope.isDCCoverTypeDisabled;
	                coverStateObj['isTPDCoverRequiredDisabled'] = $scope.isTPDCoverRequiredDisabled;
	                coverStateObj['isTPDCoverTypeDisabled'] = $scope.isTPDCoverTypeDisabled;
	                coverStateObj['isTPDCoverNameDisabled'] = $scope.isTPDCoverNameDisabled;
	                coverStateObj['isWaitingPeriodDisabled'] = $scope.isWaitingPeriodDisabled;
	                coverStateObj['isBenefitPeriodDisabled'] = $scope.isBenefitPeriodDisabled;
	                coverStateObj['isIPCoverRequiredDisabled'] = $scope.isIPCoverRequiredDisabled;
	                coverStateObj['isIPCoverNameDisabled'] = $scope.isIPCoverNameDisabled;
	                coverStateObj['isIpSalaryCheckboxDisabled'] = $scope.isIpSalaryCheckboxDisabled;
	                coverObj['indexationDeath'] = $scope.indexationDeath;
	                coverObj['indexationTpd'] = $scope.indexationTpd;
	                coverStateObj['tpdDropdown'] = $scope.TPDCoverOptionsOne;
	                coverStateObj['dynamicFlag'] = dynamicFlag;
	                coverStateObj['deathDecOrCancelFlag'] = $scope.deathDecOrCancelFlag;
	                coverStateObj['tpdDecOrCancelFlag'] = $scope.tpdDecOrCancelFlag;
	                coverStateObj['ipDecOrCancelFlag'] = $scope.ipDecOrCancelFlag;
	                coverStateObj['dcIncreaseFlag'] = $scope.dcIncreaseFlag;
	                coverStateObj['tpdIncreaseFlag'] = $scope.tpdIncreaseFlag;
	                coverStateObj['ipIncreaseFlag'] = $scope.ipIncreaseFlag ;
	                coverStateObj['showWithinOfficeQuestion'] = $scope.showWithinOfficeQuestion;
	                coverStateObj['showTertiaryQuestion'] = $scope.showTertiaryQuestion;
	                coverStateObj['showHazardousQuestion'] = $scope.showHazardousQuestion;
	                coverStateObj['showOutsideOfficeQuestion'] = $scope.showOutsideOfficeQuestion;

	    	    	PersistenceService.setChangeCoverDetails(coverObj);
	    	    	PersistenceService.setChangeCoverStateDetails(coverStateObj);
	    	    	PersistenceService.setChangeCoverOccDetails(changeCoverOccObj);
	    	    	PersistenceService.setDeathAddnlCoverDetails(deathAddnlCoverObj);
	    	    	PersistenceService.setTpdAddnlCoverDetails(tpdAddnlCoverObj);
	    	    	PersistenceService.setIpAddnlCoverDetails(ipAddnlCoverObj);

	    };



	    $scope.saveQuote =function(){
	    	$scope.checkAuraDisable();
	     // $scope.showSaveOrExitPopUp();
	    	$scope.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR>');
	    	$scope.saveDataForPersistence();
    		var quoteObject =  PersistenceService.getChangeCoverDetails();
    		var changeCoverOccDetails = PersistenceService.getChangeCoverOccDetails();
    		var deathAdditionalDetails = PersistenceService.getDeathAddnlCoverDetails();
    		var tpdAdditionalDetails = PersistenceService.getTpdAddnlCoverDetails();
    		var ipAdditionalDetails = PersistenceService.getIpAddnlCoverDetails();
    	    var personalInfo = persoanlDetailService.getMemberDetails();
    	    if(quoteObject != null && changeCoverOccDetails != null && deathAdditionalDetails != null && tpdAdditionalDetails != null && ipAdditionalDetails != null && personalInfo[0] != null ){
    	    	var details={};
        		details.addnlDeathCoverDetails = deathAdditionalDetails;
    			details.addnlTpdCoverDetails = tpdAdditionalDetails;
    			details.addnlIpCoverDetails = ipAdditionalDetails;
    			details.occupationDetails = changeCoverOccDetails;
    			var temp = angular.extend(details,quoteObject);
        	    var saveQuoteObject = angular.extend(temp, personalInfo[0]);
    	    	auraResponseService.setResponse(saveQuoteObject)
    	        $rootScope.$broadcast('disablepointer');
    	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
    	                //console.log(response.data)
    	        });
    	    }
	    };

       $scope.quoteSaveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
					template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};


	    if($routeParams.mode == 2){
	    	var existingDetails = PersistenceService.getChangeCoverDetails();
	    	var stateDetails = PersistenceService.getChangeCoverStateDetails();
	    	var occupationDetails = PersistenceService.getChangeCoverOccDetails();
	    	var deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	    	var tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
	    	var ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
	    	if(!existingDetails || !stateDetails || !occupationDetails || !deathAddnlDetails || !tpdAddnlDetails || !ipAddnlDetails) {
	    		$location.path("/quote/1");
	    		return false;
	    	}
	    	$scope.email = existingDetails.email;
	    	$scope.preferredContactType=existingDetails.contactType;
	    	$scope.changeCvrPhone = existingDetails.contactPhone;
	    	$scope.time = existingDetails.contactPrefTime;
	    	$scope.auraDisabled=existingDetails.auraDisabled;

	    	//Occupation Details
//	    	$scope.fifteenHrsQuestion = occupationDetails.fifteenHr;
//	    	$scope.areyouperCitzQuestion = occupationDetails.citizenQue;
//	    	$scope.industry = occupationDetails.industryCode;
//	    	//$scope.occupation = occupationDetails.occupation;
//	    	$scope.ownBussinessQuestion = occupationDetails.ownBussinessQues;
//	    	$scope.ownBussinessYesQuestion = occupationDetails.ownBussinessYesQues;
//	    	$scope.ownBussinessNoQuestion = occupationDetails.ownBussinessNoQues;
//	    	$scope.withinOfficeQuestion = occupationDetails.withinOfficeQue;
//	    	$scope.tertiaryQuestion = occupationDetails.tertiaryQue;
//	    	$scope.outsideOffice = occupationDetails.managementRoleQue;
//	    	$scope.hazardousQuestion = occupationDetails.hazardousQue;
	    	$scope.gender = occupationDetails.gender;
//	    	$scope.annualSalary = occupationDetails.salary;
//	    	$scope.otherOccupationObj.otherOccupation = occupationDetails.otherOccupation;

	    	$scope.deathCoverDetails.amount = existingDetails.existingDeathAmt;
	    	$scope.deathCoverDetails.units = existingDetails.existingDeathUnits;
	    	$scope.deathOccupationCategory = existingDetails.deathOccCategory;
	    	$scope.premFreq = existingDetails.freqCostType;

	    	$scope.coverName = deathAddnlDetails.deathCoverName;
	    	$scope.coverType = deathAddnlDetails.deathCoverType;
	    	$scope.dcCoverAmount = deathAddnlDetails.deathFixedAmt;
	    	$scope.requireCover = deathAddnlDetails.deathInputTextValue;
	    	$scope.dcWeeklyCost = deathAddnlDetails.deathCoverPremium;

	    	$scope.tpdCoverDetails.amount = existingDetails.existingTpdAmt;
	    	$scope.tpdCoverDetails.units = existingDetails.existingTPDUnits;
	    	$scope.tpdOccupationCategory = existingDetails.tpdOccCategory;

	    	$scope.tpdCoverName = tpdAddnlDetails.tpdCoverName;
	    	$scope.tpdCoverType = tpdAddnlDetails.tpdCoverType;
	    	$scope.tpdCoverAmount = tpdAddnlDetails.tpdFixedAmt;
	    	$scope.TPDRequireCover = tpdAddnlDetails.tpdInputTextValue;
	    	$scope.tpdWeeklyCost = tpdAddnlDetails.tpdCoverPremium;

	    	$scope.ipCoverDetails.units = existingDetails.existingIPUnits;
	    	$scope.ipOccupationCategory = existingDetails.ipOccCategory;
	    //	ipCheckboxState = existingDetails.ipcheckbox;

	    	$scope.ipCoverName = ipAddnlDetails.ipCoverName;
	    	$scope.waitingPeriodAddnl = ipAddnlDetails.waitingPeriod;
	    	$scope.benefitPeriodAddnl = ipAddnlDetails.benefitPeriod;
	    	$scope.ipCoverAmount = ipAddnlDetails.ipFixedAmt;
	    	$scope.IPRequireCover = ipAddnlDetails.ipInputTextValue;
	    	$scope.ipWeeklyCost = ipAddnlDetails.ipCoverPremium;

	    	$scope.totalWeeklyCost = existingDetails.totalPremium;
	    	$scope.totalWeeklyCost1 = existingDetails.totalPremium1;
	    	appNum = existingDetails.appNum;
	    	ackCheck = existingDetails.ackCheck;
	    	dodCheck = existingDetails.dodCheck;
	    	privacyCheck = existingDetails.privacyCheck;

	    	$scope.isDCCoverRequiredDisabled = stateDetails.isDCCoverRequiredDisabled;
	    	$scope.isDCCoverTypeDisabled = stateDetails.isDCCoverTypeDisabled;
	    	$scope.isTPDCoverRequiredDisabled = stateDetails.isTPDCoverRequiredDisabled;
	    	$scope.isTPDCoverTypeDisabled = stateDetails.isTPDCoverTypeDisabled;
	    	$scope.isTPDCoverNameDisabled = stateDetails.isTPDCoverNameDisabled;
	    	$scope.isWaitingPeriodDisabled = stateDetails.isWaitingPeriodDisabled;
	    	$scope.isBenefitPeriodDisabled = stateDetails.isBenefitPeriodDisabled;
	    	$scope.isIPCoverRequiredDisabled = stateDetails.isIPCoverRequiredDisabled;
	    	$scope.isIPCoverNameDisabled = stateDetails.isIPCoverNameDisabled;
	    	$scope.isIpSalaryCheckboxDisabled = stateDetails.isIpSalaryCheckboxDisabled;
	    	$scope.indexationDeath = existingDetails.indexationDeath;
	    	$scope.indexationTpd = existingDetails.indexationTpd;
	    	$scope.tpdDropdown = stateDetails.TPDCoverOptionsOne;
	    	dynamicFlag = stateDetails.dynamicFlag;
	    	$scope.deathDecOrCancelFlag = stateDetails.deathDecOrCancelFlag;
	    	$scope.tpdDecOrCancelFlag = stateDetails.tpdDecOrCancelFlag;
	    	$scope.ipDecOrCancelFlag = stateDetails.ipDecOrCancelFlag;
	    	$scope.dcIncreaseFlag = stateDetails.dcIncreaseFlag;
	    	$scope.tpdIncreaseFlag = stateDetails.tpdIncreaseFlag;
	    	$scope.ipIncreaseFlag = stateDetails.ipIncreaseFlag;
	    	$scope.showWithinOfficeQuestion = stateDetails.showWithinOfficeQuestion;
            $scope.showTertiaryQuestion = stateDetails.showTertiaryQuestion;
            $scope.showHazardousQuestion = stateDetails.showHazardousQuestion;
            $scope.showOutsideOfficeQuestion = stateDetails.showOutsideOfficeQuestion;

	    	deathDBCategory = $scope.deathOccupationCategory;
    		tpdDBCategory = $scope.tpdOccupationCategory;
    		ipDBCategory = $scope.ipOccupationCategory;

	    	if($scope.coverType == "DcUnitised"){
	    		showhide('nodollar1','dollar1');
				showhide('nodollar','dollar');
	    	} else if($scope.coverType == "DcFixed"){
	    		showhide('dollar1','nodollar1');
	    		showhide('dollar','nodollar');
	    	}

//	    	OccupationService.getOccupationList($scope.urlList.occupationUrl, "VICT", $scope.industry).then(function(res){
//	    		$scope.OccupationList = res.data;
//	    		var temp = $scope.OccupationList.filter(function(obj){
//	        		return obj.occupationName == occupationDetails.occupation;
//	        	});
//	        	$scope.occupation = temp[0].occupationName;
//	        	$scope.renderOccupationQuestions();
//	    	}, function(err){
//	    		console.info("Error while fetching occupations " + JSON.stringify(err));
//	    	});

//	    	if(ipCheckboxState){
//	    		$('#ipsalarycheck').parent().addClass('active');
//	    	}
	    	if(ackCheck){
	    	    $timeout(function(){
	    			$('#termsLabel').addClass('active');
	    			   });
	    			 }
	    	if(dodCheck){
	    	    $timeout(function(){
	    			$('#dodLabel').addClass('active');
			   });
			 }
	    	if(privacyCheck){
	    	    $timeout(function(){
	    			$('#privacyLabel').addClass('active');
			   });
			 }

	    	/*$scope.toggleTwo(true);*/
	    	$scope.toggleThree(true);
	    	$scope.togglePrivacy(true);
			$scope.toggleContact(true);
	    }

	    if($routeParams.mode == 3){
	    	mode3Flag = true;
	    	var num = PersistenceService.getAppNumToBeRetrieved();

	    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
	    	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
	    		var appDetails = res.data[0];
	    		$scope.email = appDetails.email;
		    	$scope.preferredContactType = appDetails.contactType;
		    	$scope.changeCvrPhone = appDetails.contactPhone;
		    	$scope.time = appDetails.contactPrefTime;

//		    	$scope.fifteenHrsQuestion = appDetails.occupationDetails.fifteenHr;
//		    	$scope.areyouperCitzQuestion = appDetails.occupationDetails.citizenQue;
//		    	$scope.industry = appDetails.occupationDetails.industryCode;
//		    	$scope.ownBussinessQuestion = appDetails.occupationDetails.ownBussinessQues;
//		    	$scope.ownBussinessYesQuestion = appDetails.occupationDetails.ownBussinessYesQues;
//		    	$scope.ownBussinessNoQuestion = appDetails.occupationDetails.ownBussinessNoQues;
//		    	$scope.withinOfficeQuestion = appDetails.occupationDetails.withinOfficeQue;
//		    	$scope.tertiaryQuestion = appDetails.occupationDetails.tertiaryQue;
//		    	$scope.outsideOffice = appDetails.occupationDetails.managementRoleQue;
//		    	$scope.hazardousQuestion = appDetails.occupationDetails.hazardousQue;
		    	$scope.gender = appDetails.occupationDetails.gender;
		    	//$scope.annualSalary = appDetails.occupationDetails.salary;
		    	//$scope.otherOccupationObj.otherOccupation = appDetails.occupationDetails.otherOccupation;
		    	$scope.auraDisabled = appDetails.auraDisabled;
		    	if(appDetails.auraDisabled && appDetails.auraDisabled == "true"){
		    		$scope.auraDisabled = true;
		    	} else{
		    		$scope.auraDisabled = false;
		    	}

		    	$scope.deathCoverDetails.amount = appDetails.existingDeathAmt;
		    	$scope.deathCoverDetails.units = appDetails.existingDeathUnits;
		    	$scope.deathOccupationCategory = appDetails.deathOccCategory;
		    	$scope.premFreq = appDetails.freqCostType;

		    	$scope.coverName = appDetails.addnlDeathCoverDetails.deathCoverName;
		    	$scope.coverType = appDetails.addnlDeathCoverDetails.deathCoverType;
		    	$scope.dcCoverAmount = appDetails.addnlDeathCoverDetails.deathFixedAmt;
		    	$scope.requireCover = appDetails.addnlDeathCoverDetails.deathInputTextValue;
		    	$scope.dcWeeklyCost = appDetails.addnlDeathCoverDetails.deathCoverPremium;

		    	$scope.tpdCoverDetails.amount = appDetails.existingTpdAmt;
		    	$scope.tpdCoverDetails.units = appDetails.existingTPDUnits;
		    	$scope.tpdOccupationCategory = appDetails.tpdOccCategory;

		    	$scope.tpdCoverName = appDetails.addnlTpdCoverDetails.tpdCoverName;
		    	$scope.tpdCoverType = appDetails.addnlTpdCoverDetails.tpdCoverType;
		    	$scope.tpdCoverAmount = appDetails.addnlTpdCoverDetails.tpdFixedAmt;
		    	$scope.TPDRequireCover = appDetails.addnlTpdCoverDetails.tpdInputTextValue;
		    	$scope.tpdWeeklyCost = appDetails.addnlTpdCoverDetails.tpdCoverPremium;

		    	$scope.ipCoverDetails.units = appDetails.existingIPUnits;
		    	$scope.ipOccupationCategory = appDetails.ipOccCategory;
		    	//ipCheckboxState = appDetails.ipcheckbox;
//		    	if(appDetails.indexationDeath && appDetails.indexationDeath == "true"){
//		    		$scope.indexation.death = true;
//		    	} else{
//		    		$scope.indexation.death = false;
//		    	}
//		    	if(appDetails.indexationTpd && appDetails.indexationTpd == "true"){
//		    		$scope.indexation.disable = true;
//		    	} else{
//		    		$scope.indexation.disable = false;
//		    	}

		    	$scope.ipCoverName = appDetails.addnlIpCoverDetails.ipCoverName;
		    	$scope.waitingPeriodAddnl = appDetails.addnlIpCoverDetails.waitingPeriod;
		    	$scope.benefitPeriodAddnl = appDetails.addnlIpCoverDetails.benefitPeriod;
		    	$scope.ipCoverAmount = appDetails.addnlIpCoverDetails.ipFixedAmt;
		    	$scope.IPRequireCover = appDetails.addnlIpCoverDetails.ipInputTextValue;
		    	$scope.ipWeeklyCost = appDetails.addnlIpCoverDetails.ipCoverPremium;

		    	$scope.totalWeeklyCost = appDetails.totalPremium;
		    	appNum = appDetails.appNum;
		    	ackCheck = appDetails.ackCheck;
		    	dodCheck = appDetails.dodCheck;
		    	privacyCheck = appDetails.privacyCheck;

		    	if($scope.coverType == "DcUnitised"){
		    		showhide('nodollar1','dollar1');
					showhide('nodollar','dollar');
		    	} else if($scope.coverType == "DcFixed"){
		    		showhide('dollar1','nodollar1');
		    		showhide('dollar','nodollar');
		    	}

//		    	OccupationService.getOccupationList($scope.urlList.occupationUrl, "VICT", $scope.industry).then(function(res){
//		    		$scope.OccupationList = res.data;
//		    		var temp = $scope.OccupationList.filter(function(obj){
//		        		return obj.occupationName == appDetails.occupationDetails.occupation;
//		        	});
//		        	$scope.occupation = temp[0].occupationName;
//		        	$scope.renderOccupationQuestions();
//		    	}, function(err){
//		    		console.info("Error while fetching occcupation list " + JSON.stringify(err));
//		    	});

		    	$scope.checkDeathOption();
		    	$scope.checkTPDOption();
		    	$scope.checkIPOption();

//		    	if(ipCheckboxState){
//		    		$('#ipsalarycheck').parent().addClass('active');
//		    		$scope.isIPCoverRequiredDisabled = true;
//		    	}
		    	if($scope.indexation.death){
		    		$('#changecvr_index-dth').parent().addClass('active');
		    	}

		    	if($scope.indexation.disable){
		    		$('#changeCvr-indextpd-disable').parent().addClass('active');
		    	}
		    	if(ackCheck){
		    	    $timeout(function(){
		    			$('#termsLabel').addClass('active');
    			   });
    			 }

    			$('#dodLabel').addClass('active');
    			$('#privacyLabel').addClass('active');

		    	/*$scope.toggleTwo(true);*/
		    	$scope.toggleThree(true);
		    	$scope.togglePrivacy(true);
    			$scope.toggleContact(true);

	    	}, function(err){
	    		console.info("Error fetching the saved app details " + err);
	    	});
	    }


	    $scope.generatePDF = function(){

	    	$scope.saveDataForPersistence();
	    	var CCOccDetails = PersistenceService.getChangeCoverOccDetails();
    		var deathAddnlInfo = PersistenceService.getDeathAddnlCoverDetails();
    		var tpdAddnlInfo = PersistenceService.getTpdAddnlCoverDetails();
    		var ipAddnlInfo = PersistenceService.getIpAddnlCoverDetails();
    	    var personalInformation = persoanlDetailService.getMemberDetails();
    	    var quoteObj =  PersistenceService.getChangeCoverDetails();
    	    if(quoteObj != null && CCOccDetails != null && deathAddnlInfo != null && tpdAddnlInfo != null && ipAddnlInfo != null && personalInformation[0] != null ){
    	    	var info={};
    	    	info.addnlDeathCoverDetails = deathAddnlInfo;
    	    	info.addnlTpdCoverDetails = tpdAddnlInfo;
    	    	info.addnlIpCoverDetails = ipAddnlInfo;
    	    	info.occupationDetails = CCOccDetails;
    			var temp = angular.extend(info,quoteObj);
    			var printObject = angular.extend(temp, personalInformation[0]);
    			auraResponseService.setResponse(printObject);
    			printQuotePage.reqObj($scope.urlList.printQuotePage).then(function(response) {
        		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
        		$scope.downloadPDF();
	    	}, function(err){
	    		console.info("Something went wrong while generating pdf..." + JSON.stringify(err));
	    	});
    	    }
	    };
    	    $scope.downloadPDF = function(){
    	    var pdfLocation =null;
    	    var filename = null;
    	   	var a = null;
	    	pdfLocation = PersistenceService.getPDFLocation();
	    	//console.log(pdfLocation+"pdfLocation");
	    	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  		a = document.createElement("a");
	  	    document.body.appendChild(a);
	  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
	  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
	  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
	  				var extension = res.data.response.type;
	          	  extension = extension.substring(extension.lastIndexOf('/')+1);
	          	  filename = filename+"."+extension;
	   	           window.navigator.msSaveBlob(res.data.response,filename);
	   	       }else{
	  	        var fileURL = URL.createObjectURL(res.data.response);
	  	        a.href = fileURL;
	  	        a.download = filename;
	  	        a.click();
	   	       }
	  		}, function(err){
	  			//console.log("Error downloading the PDF " + err);
	  		});
	  	};


  }]);

 /* Change Cover Controller,Progressive and Mandatory validations Ends */
