/**
 *
 */
package pages.metlife.Angular.HOSTPLUS;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Yuvaraj
 *
 */
public class HOSTPLUS_Angular_UpdateYourOccupation extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HOSTPLUS_Angular_UpdateYourOccupation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}

	public HOSTPLUS_Angular_UpdateYourOccupation UpdateYourOccupation_HOSTPLUS() {
		updateoccupation();
		HealthandLifestyle();
		Declaration();
		return new HOSTPLUS_Angular_UpdateYourOccupation(scriptHelper);
	}

	private void updateoccupation() {

		String EmailId = dataTable.getData("HOSTPLUS_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("HOSTPLUS_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_UpdateOccupation", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_UpdateOccupation", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_UpdateOccupation", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_UpdateOccupation", "AnnualSalary");
		String DutyOfDisclosure = dataTable.getData("HOSTPLUS_UpdateOccupation", "DutyOfDisclosure");
		String PrivacyStatement = dataTable.getData("HOSTPLUS_UpdateOccupation", "PrivacyStatement");

		try{

			//*****Click on Update Occupation Link*******\\
		/*	waitForPageTitle("testing", 6);*/
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Update your occupation')]"))).click();
			report.updateTestLog("Update your Occupation", "Update occupation link is Selected", Status.PASS);
			sleep(1500);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(2000);
			}

			//*****Agree to Duty of disclosure*******\\

			waitForPageTitle("testing", 5);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is checked", Status.PASS);
			sleep(200);

           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
			report.updateTestLog("Privacy Statement", "Privacy Statement label is checked", Status.PASS);
			sleep(200);

			//*****Enter the Email Id*****\\


			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);

			//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);



			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}


			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);


			//***************************************\\
			//**********OCCUPATION SECTION***********\\
			//****************************************\\

			//*****Select the 15 Hours Question*******\\

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion']/parent::*"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}

			//*****Resident of Australia****\\

			/*if(Citizen.equalsIgnoreCase("Yes")){
				driver.findElement(By.xpath("(//input[@name='areyouperCitzWrkUpdateQuestion']/parent::*)[1]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

				}
			else{
				driver.findElement(By.xpath("(//input[@name='areyouperCitzWrkUpdateQuestion']/parent::*)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

				}*/


			//*****Industry********\\
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("workRatingIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your occupation?']"))).click();
			sleep(500);


			waitForPageTitle("testing", 3);
			//*****Occupation*****\\

			Select Occupation=new Select(driver.findElement(By.name("workRatingoccupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);


			//*****Other Occupation *****\\

			if(OccupationType.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}




			//*****Extra Questions*****\\

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingWithinOffcQue']/parent::*)[1]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
			sleep(200);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingWithinOffcQue']/parent::*)[1]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
			sleep(200);
		}

		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingTertQue']/parent::*)[1]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					sleep(200);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingTertQue']/parent::*)[1]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				sleep(200);
		}
		}



		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



		//******Do you work in hazardous environment*****\\

		if(HazardousEnv.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingHazardousQue']/parent::*)[1]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingHazardousQue']/parent::*)[2]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}

		//******Do you spend more than 10% of your working time outside of an office environment?*******\\

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingOutsideOffice']/parent::*)[1]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingOutsideOffice']/parent::*)[2]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}
		sleep(800);
		//*****Extra Questions*****\\

	    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


	//***Select if your office Duties are Undertaken within an Office Environment?\\

	    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

	    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingWithinOffcQue']/parent::*)[1]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

	}
	else{

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingWithinOffcQue']/parent::*)[2]"))).click();
		report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

	}

	      //*****Do You Hold a Tertiary Qualification******\\

	        if(TertiaryQual.equalsIgnoreCase("Yes")){

	        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingTertQue']/parent::*)[1]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
	}
	   else{

		   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workRatingTertQue']/parent::*)[2]"))).click();
			report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
	}
	}


		}

          sleep(800);

        //*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
		    waitForPageTitle("testing", 2);


		//*****Click on Continue*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingOccupForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		sleep(4500);


		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}


		private void HealthandLifestyle() {

			String Restrictedillness = dataTable.getData("HOSTPLUS_UpdateOccupation", "Restrictedillness");
			String PriorClaim = dataTable.getData("HOSTPLUS_UpdateOccupation", "PriorClaim");
			String ReducedLifeExpectancy = dataTable.getData("HOSTPLUS_UpdateOccupation", "ReducedLifeExpectancy");

			try{


      if(driver.getPageSource().contains("Eligibility Check")){

	//*******Are you restricted due to illness or injury******\\

	WebDriverWait wait=new WebDriverWait(driver,18);

	 if(Restrictedillness.equalsIgnoreCase("Yes")){

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
   }

	 else{

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
    }

     //******Are you contemplating, or have you ever made a claim for sickness, accident or disability benefits, Workers� Compensation or any other form of compensation due to illness or injury?***\\

     if(PriorClaim.equalsIgnoreCase("Yes")){

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[2]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
     }

     else{
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[2]"))).click();
    	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
    	sleep(1000);
        }


//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\

    if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
    }
   else{

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
}

          //****Click on Continue******\\

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
    sleep(10000);
      }

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

		}
		private void Declaration() {
			String GeneralConsent = dataTable.getData("HOSTPLUS_UpdateOccupation", "GeneralConsent");
			String Decision = dataTable.getData("HOSTPLUS_UpdateOccupation", "Decision");
			try{

        //*****General Consent******\\

				WebDriverWait wait=new WebDriverWait(driver,18);
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelWR']/span"))).click();
   report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
   sleep(500);

          //******Submit******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitWorkRating()']"))).click();
report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

waitForPageTitle("testing", 3);

//*******Feedback Pop-up******\\


wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
sleep(1000);
/*
String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
System.out.println(Justification);
sleep(400);

if(Justification.equalsIgnoreCase(Decision)){
	report.updateTestLog("Decision", "Wording is displayed as Expected", Status.PASS);

}
else{
	report.updateTestLog("Decision", "Wording is not displayed as Expected", Status.FAIL);

}*/
sleep(200);

String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
sleep(600);
report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);


		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);

		}
			}
	}





