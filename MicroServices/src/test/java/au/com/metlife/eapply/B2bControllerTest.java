package au.com.metlife.eapply;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import au.com.metlife.eapply.b2b.controller.B2bController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@JsonDeserialize(as = InputStream.class)
public class B2bControllerTest {

	@InjectMocks
	private B2bController controller;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	String testData = null;
	String fileName = null;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	// for /getcustomerdata for HOST
	@Test
	public void getClientPostDataTest() throws Exception {

		testData = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdNalkxTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNU9HRTNNRGcxWmkxbE1UVmxMVFEyWWpNdE9ERTVOeTAyTTJOak1EWmhOalJtTURRaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5nZ1BaTTZmRk9JRDhxYUw1aEpvYzJpWTVkZjE5bEEtSDU0b19HUDl5aC1Z\"}";
		mockMvc.perform(post("/getcustomerdata").contentType(MediaType.APPLICATION_JSON).content(testData)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.lineOfBusiness").exists()).andExpect(jsonPath("$.lineOfBusiness").value("Group"))

				.andExpect(jsonPath("$.partnerID").exists()).andExpect(jsonPath("$.partnerID").value("HOST"))

				.andExpect(jsonPath("$.applicant.[0].personalDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").value("HOST"))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].address").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").value("2"))

				.andExpect(jsonPath("$.applicant.[0].contactDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").value("test@test.com"))

				.andExpect(jsonPath("$.applicant.[0].memberType").exists())
				.andExpect(jsonPath("$.applicant.[0].memberType").value("INDUSTRY"));

	}
	// for /getcustomerdata for AEIS

	@Test
	public void getClientPostDataTestAEIS() throws Exception {

		testData = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXlNalk0TlN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJeU9UVTBPR1JtWXkwMVl6VXlMVFEzTWpZdFlqQXlNeTFsWm1GalltWXlOMlE0TWpraUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5KZWtwdWc1SlN0Z2xSc3R6ajZfOUxMakVtSGQzdGljRDU3NUtNWjNQdUFZ \"}";
		mockMvc.perform(post("/getcustomerdata").contentType(MediaType.APPLICATION_JSON).content(testData)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.lineOfBusiness").exists()).andExpect(jsonPath("$.lineOfBusiness").value("Group"))

				.andExpect(jsonPath("$.partnerID").exists()).andExpect(jsonPath("$.partnerID").value("AEIS"))

				.andExpect(jsonPath("$.applicant.[0].personalDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").value("PVTTestAEIS"))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].address").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").value("2"))

				.andExpect(jsonPath("$.applicant.[0].contactDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").value("test@test.com"))

				.andExpect(jsonPath("$.applicant.[0].memberType").exists())
				.andExpect(jsonPath("$.applicant.[0].memberType").value("Personal"));

	}

	// for /getcustomerdata for CARE SUPER

	@Test
	public void getClientPostDataTestCARE() throws Exception {

		testData = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXlORFkxTml3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJMlptUXhaakkxT1MxbFpETXlMVFJrTlRBdE9UYzJOQzAwTldZM05tTXlZV1kyWlRJaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5uS0dTQWIzYTB6Vk1KZmp2QUM5NFVMcGFiUnBtZFo1VHN0X3llNDBmMnFN  \"}";
		mockMvc.perform(post("/getcustomerdata").contentType(MediaType.APPLICATION_JSON).content(testData)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.lineOfBusiness").exists()).andExpect(jsonPath("$.lineOfBusiness").value("Group"))

				.andExpect(jsonPath("$.partnerID").exists()).andExpect(jsonPath("$.partnerID").value("CARE"))

				.andExpect(jsonPath("$.applicant.[0].personalDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").value("test"))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].address").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").value("2"))

				.andExpect(jsonPath("$.applicant.[0].contactDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").value("test@test.com"))

				.andExpect(jsonPath("$.applicant.[0].memberType").exists())
				.andExpect(jsonPath("$.applicant.[0].memberType").value("Personal"));

	}

	@Test
	public void getPostB2BDataTestHost() throws Exception {

		testData = "<request><adminPartnerID>HOST</adminPartnerID><ageCover>60</ageCover><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>4</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>HOST</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>HOST</partnerID><product></product><campaign_code></campaign_code><tracking_code></tracking_code><promotion_code></promotion_code><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>78763647</clientRefNumber><applicantRole>1</applicantRole><memberType>INDUSTRY</memberType><memberDivision>INDUSTRY</memberDivision><segmentCode></segmentCode><personalDetails><firstName>HOST</firstName><lastName>HOSTB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>##QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE56TXhNemc1T1N3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFlUTmxORFkwTlMwNE56VTNMVFJoWlRBdE9UQTRaQzB6TURKbU0yVmhOR00xT0RnaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5FN2dTNlkyX3RIZkhjeFRrbWpXcGFGdUZrX0Vyd3BjNzlxZGVaUmliY3hj";
		mockMvc.perform(post("/postb2bdata").contentType(MediaType.APPLICATION_XML).content(testData)
				.accept(MediaType.APPLICATION_XML)).andExpect(status().isOk());
	}

	@Test
	public void getPostB2BDataTestCare() throws Exception {

		testData = "<request><adminPartnerID>CARE</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>CARE</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>CARE</partnerID><applicant><dateJoined>01/01/2011</dateJoined><welcomeLetterDate>01/01/2010</welcomeLetterDate><clientRefNumber>734534</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>test</firstName><lastName>testB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><indexation>2</indexation><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><indexation>2</indexation><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><indexation>2</indexation><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>##QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE56TXhORFU0Tnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJM1lXWTVNak5pWmkxbU1UazBMVFF6WWpVdE9ERmpOQzFrWkdFeU56Z3dZV0UwTm1VaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS44dkx1SVhuVmpRdWF1cGR0ekJJZVVxMjVkTGRsOG1lT2hBOXhBVFJ2OG53";
		mockMvc.perform(post("/postb2bdata").contentType(MediaType.APPLICATION_XML).content(testData)
				.accept(MediaType.APPLICATION_XML)).andExpect(status().isOk());
	}

	@Test
	public void getPostB2BDataTestAeis() throws Exception {

		testData = "<request><adminPartnerID>MEMA</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>AEIS</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>AEIS</partnerID><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>AEIS7545445</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>PVTTestAEIS</firstName><lastName>DontloadAEIS</lastName><dateOfBirth>01/12/1958</dateOfBirth><gender>Male</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><amount>500000.0</amount><loading></loading><exclusions></exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><amount>100000.0</amount><loading></loading><exclusions></exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><amount>0.0</amount><loading></loading><exclusions></exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>##QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE56TXhORGM1TkN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNE56azFZbUl3WXkxbE5qUmpMVFEyTjJJdFlqazJNQzFoTURKaE16a3pNemRpWXpjaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5rM2h2VEtsak5BajV5TV9OV2tBekNmaDVQb3ljUXhLdERySkNsb2xmcFRn";
		mockMvc.perform(post("/postb2bdata").contentType(MediaType.APPLICATION_XML).content(testData)
				.accept(MediaType.APPLICATION_XML)).andExpect(status().isOk());
	}

	// for /getDefaultUnitsForSpecialCover for HOST

	@Test
	public void getDefaultUnitsForSpecialCoverHOST() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getDefaultUnitsForSpecialCover?fundCode=HOST&anb=60&deathUnits=250&tpdUnits=250&dateJoined=2588&firstName=HOST&clientrefnum=7&lastName=HOSTB&dob=01/11/1958")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(content().string("false"))
				;

	}

	// for /getDefaultUnitsForSpecialCover for Invalid FundCode

	@Test
	public void getDefaultUnitsForSpecialCoverInvalidData() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getDefaultUnitsForSpecialCover?fundCode=adfdf&anb=60&deathUnits=250&tpdUnits=250&dateJoined=2588&firstName=HOST&clientrefnum=7&lastName=HOSTB&dob=01/11/1958")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(content().string("false"))
				;

	}

	// /gethostdata for HOST
	@Test
	public void gethostdataHOST() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.get("/gethostdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdNalkxTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNU9HRTNNRGcxWmkxbE1UVmxMVFEyWWpNdE9ERTVOeTAyTTJOak1EWmhOalJtTURRaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5nZ1BaTTZmRk9JRDhxYUw1aEpvYzJpWTVkZjE5bEEtSDU0b19HUDl5aC1Z")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.string("<request><adminPartnerID>HOST</adminPartnerID><ageCover>60</ageCover><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>4</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>HOST</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>HOST</partnerID><product></product><campaign_code></campaign_code><tracking_code></tracking_code><promotion_code></promotion_code><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>7</clientRefNumber><applicantRole>1</applicantRole><memberType>INDUSTRY</memberType><memberDivision>INDUSTRY</memberDivision><segmentCode></segmentCode><personalDetails><firstName>HOST</firstName><lastName>HOSTB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>"))
				;

	}

	// /gethostdata for AEIS
	@Test
	public void gethostdataAEIS() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.get("/gethostdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXlNalk0TlN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJeU9UVTBPR1JtWXkwMVl6VXlMVFEzTWpZdFlqQXlNeTFsWm1GalltWXlOMlE0TWpraUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5KZWtwdWc1SlN0Z2xSc3R6ajZfOUxMakVtSGQzdGljRDU3NUtNWjNQdUFZ")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.string("<request><adminPartnerID>MEMA</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>AEIS</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>AEIS</partnerID><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>AEIS7</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>PVTTestAEIS</firstName><lastName>DontloadAEIS</lastName><dateOfBirth>01/12/1958</dateOfBirth><gender>Male</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><amount>500000.0</amount><loading></loading><exclusions></exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><amount>100000.0</amount><loading></loading><exclusions></exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><amount>0.0</amount><loading></loading><exclusions></exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>"))
				;

	}

	// /gethostdata for CARE
	@Test
	public void gethostdataCARE() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.get("/gethostdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXlORFkxTml3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJMlptUXhaakkxT1MxbFpETXlMVFJrTlRBdE9UYzJOQzAwTldZM05tTXlZV1kyWlRJaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5uS0dTQWIzYTB6Vk1KZmp2QUM5NFVMcGFiUnBtZFo1VHN0X3llNDBmMnFN")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.string("<request><adminPartnerID>CARE</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>CARE</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>CARE</partnerID><applicant><dateJoined>01/01/2011</dateJoined><welcomeLetterDate>01/01/2010</welcomeLetterDate><clientRefNumber>7</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>test</firstName><lastName>testB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><indexation>2</indexation><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><indexation>2</indexation><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><indexation>2</indexation><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>"))
				;

	}

	// /gethostdata for Invalid TokenId
	@Test
	public void gethostdataInvalidToken() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/gethostdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOsdfsdf")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

	}

	// for /getcustomerdata for HOST
	@Test
	public void getClientData() throws Exception {

		testData = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdNalkxTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNU9HRTNNRGcxWmkxbE1UVmxMVFEyWWpNdE9ERTVOeTAyTTJOak1EWmhOalJtTURRaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5nZ1BaTTZmRk9JRDhxYUw1aEpvYzJpWTVkZjE5bEEtSDU0b19HUDl5aC1Z\"}";
		mockMvc.perform(MockMvcRequestBuilders
				.get("/getcustomerdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdNalkxTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNU9HRTNNRGcxWmkxbE1UVmxMVFEyWWpNdE9ERTVOeTAyTTJOak1EWmhOalJtTURRaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5nZ1BaTTZmRk9JRDhxYUw1aEpvYzJpWTVkZjE5bEEtSDU0b19HUDl5aC1Z")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	// for /getDefaultUnitsForLifeEvent for HOST

	@Test
	public void getDefaultUnitsForLifeEvent() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getDefaultUnitsForLifeEvent?fundCode=HOST&anb=60&deathUnits=250&tpdUnits=250")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

}
