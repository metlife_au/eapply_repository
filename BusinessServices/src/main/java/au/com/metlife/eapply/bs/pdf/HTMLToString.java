package au.com.metlife.eapply.bs.pdf;

public class HTMLToString {
    
	private static String[] dictionary = { "</P>", "</p>", "<BR>", "<br>",
        "<LI>", "<li>", "</UL>", "</ul>", "<P>", "<p>", "<U>", "<B>", "</U>",
        "</B>", "<u>", "<b>", "</u>", "</b>", "</LI>", "</li>", "<U><STRONG>",
        "<u><strong>", "</STRONG></U>", "</strong></u>", "</STRONG>",
        "</strong", "<STRONG>", "<stromg>", "<UL>", "<ul>", "<A", "<a",
        "<span", "<SPAN", "</a>", "</A>", "</span>", "</SPAN>" };
    
    public static String convertHTMLToString(String source) {

        boolean flag = false;
        if (source.contains( "</P>")) {
            source = source.replace( "</P>", "\n\n");
        }
        if (source.contains( "</p>")) {
            source = source.replace( "</p>", "\n\n");
        }
        if (source.contains( "<BR>")) {
            source = source.replace( "<BR>", "\n\n");
        }
        if (source.contains( "<br>")) {
            source = source.replace( "<br>", "\n\n");
        }
        if (source.contains( "<LI>")) {
            source = source.replace( "<LI>", "\n\t-\t");
        }
        if (source.contains( "<li>")) {
            source = source.replace( "<li>", "\n\t-\t");
        }
        if (source.contains( "</UL>")) {
            source = source.replace( "</UL>", "\n\r");
        }
        if (source.contains( "</ul>")) {
            source = source.replace( "</ul>", "\n\r");
        }
        if (source.contains( "<P>")) {
            source = source.replace( "<P>", "");
        }
        if (source.contains( "<p>")) {
            source = source.replace( "<p>", "");
        }
        if (source.contains( "<U>")) {
            source = source.replace( "<U>", "");
        }
        if (source.contains( "<B>")) {
            source = source.replace( "<B>", "");
        }
        if (source.contains( "</U>")) {
            source = source.replace( "</U>", "");
        }
        if (source.contains( "</B>")) {
            source = source.replace( "</B>", "");
        }
        if (source.contains( "<u>")) {
            source = source.replace( "<u>", "");
        }
        if (source.contains( "<b>")) {
            source = source.replace( "<b>", "");
        }
        if (source.contains( "</u>")) {
            source = source.replace( "</u>", "");
        }
        if (source.contains( "</b>")) {
            source = source.replace( "</b>", "");
        }
        if (source.contains( "</LI>")) {
            source = source.replace( "</LI>", "");
        }
        if (source.contains( "</li>")) {
            source = source.replace( "</li>", "");
        }
        if (source.contains( "<U><STRONG>")) {
            source = source.replace( "<U><STRONG>", "");
        }
        if (source.contains( "<u><strong>")) {
            source = source.replace( "<u><strong>", "");
        }
        if (source.contains( "</STRONG></U>")) {
            source = source.replace( "</STRONG></U>", "");
        }
        if (source.contains( "</strong></u>")) {
            source = source.replace( "</strong></u>", "");
        }
        if (source.contains( "</STRONG>")) {
            source = source.replace( "</STRONG>", "");
        }
        if (source.contains( "</strong")) {
            source = source.replace( "</strong>", "");
        }
        if (source.contains( "<STRONG>")) {
            source = source.replace( "<STRONG>", "");
        }
        if (source.contains( "<stromg>")) {
            source = source.replace( "<strong>", "");
        }
        if (source.contains( "<UL>")) {
            source = source.replace( "<UL>", "\t");
        }
        if (source.contains( "<ul>")) {
            source = source.replace( "<ul>", "\t");
        }
        if (source.contains( "<A")) {
            String tempS = source.substring( source.indexOf( "<A"), source.length() - 1);
            String tempA = source.substring( source.indexOf( "<A"), source.indexOf( "<A")
                    + tempS.indexOf( ">") + 1);
            source = source.replace( tempA, "");
            tempA = null;
            tempS = null;
        }
        if (source.contains( "<a")) {
            
            String tempS = source.substring( source.indexOf( "<a"), source.length() - 1);
            String tempA = source.substring( source.indexOf( "<a"), source.indexOf( "<a")
                    + tempS.indexOf( ">") + 1);
            source = source.replace( tempA, "");
            tempA = null;
            tempS = null;
        }
        if (source.contains( "<span")) {
            String tempS = source.substring( source.indexOf( "<span"), source.length() - 1);
            String tempA = source.substring( source.indexOf( "<span"), source.indexOf( "<span")
                    + tempS.indexOf( ">") + 1);
            source = source.replace( tempA, "");
            tempA = null;
            tempS = null;
        }
        if (source.contains( "<SPAN")) {
            String tempS = source.substring( source.indexOf( "<SPAN"), source.length() - 1);
            String tempA = source.substring( source.indexOf( "<SPAN"), source.indexOf( "<SPAN")
                    + tempS.indexOf( ">") + 1);
            source = source.replace( tempA, "");
            tempA = null;
            tempS = null;
        }
        if (source.contains( "</a>")) {
            source = source.replace( "</a>", "");
        }
        if (source.contains( "</A>")) {
            source = source.replace( "</A>", "");
        }
        if (source.contains( "</span>")) {
            source = source.replace( "</span>", "");
        }
        if (source.contains( "</SPAN>")) {
            source = source.replace( "</SPAN>", "");
        }
        
        for (int i = 0; i < dictionary.length; i++) {
            
            if (( null != dictionary[i]) && source.contains( dictionary[i])) {
                flag = true;
                break;
            }
        }
        if (flag) {
            
            source = convertHTMLToString( source);
        }
        
        return source;
    }
}
