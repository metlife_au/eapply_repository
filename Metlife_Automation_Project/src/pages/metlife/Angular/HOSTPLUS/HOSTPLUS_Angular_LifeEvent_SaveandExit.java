package pages.metlife.Angular.HOSTPLUS;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
public class HOSTPLUS_Angular_LifeEvent_SaveandExit extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_LifeEvent_SaveandExit LifeEvent_SaveAndExit() {
		LifeEventsaveHOSTPLUS();
/*		HealthandLifeStyle();
		confirmation();*/
		return new HOSTPLUS_Angular_LifeEvent_SaveandExit(scriptHelper);
	}
	public HOSTPLUS_Angular_LifeEvent_SaveandExit LifeEvent_SaveAndExitValidation() {
		LifeEventsaveHOSTPLUSValidation();
/*		HealthandLifeStyle();
		confirmation();*/
		return new HOSTPLUS_Angular_LifeEvent_SaveandExit(scriptHelper);
	}

	public HOSTPLUS_Angular_LifeEvent_SaveandExit(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void LifeEventsaveHOSTPLUS() {

		String EmailId = dataTable.getData("LifeSaveAndExit", "EmailId");
		String TypeofContact = dataTable.getData("LifeSaveAndExit", "TypeofContact");
		String ContactNumber = dataTable.getData("LifeSaveAndExit", "ContactNumber");
		String TimeofContact = dataTable.getData("LifeSaveAndExit", "TimeofContact");
		String Gender = dataTable.getData("LifeSaveAndExit", "Gender");
		String RegularIncome = dataTable.getData("LifeSaveAndExit", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("LifeSaveAndExit", "RegularIncome");
		String WorkLimitation = dataTable.getData("LifeSaveAndExit", "RegularIncome");
		String Citizen = dataTable.getData("LifeSaveAndExit", "Citizen");
		String IndustryType = dataTable.getData("LifeSaveAndExit", "IndustryType");
		String OccupationType = dataTable.getData("LifeSaveAndExit", "OccupationType");
		String OtherOccupation = dataTable.getData("LifeSaveAndExit", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("LifeSaveAndExit", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("LifeSaveAndExit", "TertiaryQual");
		String HazardousEnv = dataTable.getData("LifeSaveAndExit", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("LifeSaveAndExit", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("LifeSaveAndExit", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("LifeSaveAndExit", "LifeEvent_Date");
		String LifeEvent_PrevApplyin1Year = dataTable.getData("LifeSaveAndExit", "LifeEvent_PrevApplyin1Year");

		String Negative_Scenario=dataTable.getData("LifeSaveAndExit", "Negative_Scenario");
		String FifteenHoursWork = dataTable.getData("LifeSaveAndExit", "FifteenHoursWork");
		String AnnualSalary = dataTable.getData("LifeSaveAndExit", "AnnualSalary");


			try{


				waitForPageTitle("testing", 6);
				//****Click on Life Event Link*****\\

				WebDriverWait wait=new WebDriverWait(driver,18);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Increase your Cover due to a life event']"))).click();
				report.updateTestLog("Life Event", "Life Event Link is Clicked", Status.PASS);

			//	waitForPageTitle("testing", 8);
	           sleep(8000);

				sleep(3000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
				report.updateTestLog("Pop up", "Pop up button clicked", Status.PASS);
			//	}


				if(driver.getPageSource().contains("You have previously saved application(s).")){
					sleep(2000);
					quickSwitchWindows();
					sleep(3000);
					driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
					report.updateTestLog("App Pop-up", "App popup clicked", Status.PASS);
					sleep(2000);

	  		}

				//*****Agree to Duty of disclosure*******\\
				waitForPageTitle("testing", 5);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);


	           //*****Agree to Privacy Statement*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

				//*****Enter the Email Id*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).clear();
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).sendKeys(EmailId);
				report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
				sleep(300);

				//*****Click on the Contact Number Type****\\


				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


					//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

					//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}

		//***********Select the Gender******************\\

				if(Gender.equalsIgnoreCase("Male")){

					driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}

					//****Contact Details-Continue Button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(contactDetailsLifeEventForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);

				if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsQuestion ']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}


					//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\

						if(RegularIncome.equalsIgnoreCase("Yes")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
							sleep(500);
							report.updateTestLog("Regular Income Question", "Yes is Selected", Status.PASS);

						}
						else{

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
							sleep(500);
							report.updateTestLog("Regular Income Question", "No is Selected", Status.PASS);

						}

						//******Extra Question based on Regular Income*******\\
						sleep(500);
						if(RegularIncome.equalsIgnoreCase("Yes")){
							if(WorkWithoutInjury.equalsIgnoreCase("Yes")){

								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
								report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
								sleep(800);
							}
							else{

								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
								report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
								sleep(800);
							}
						}
						else{
							if(WorkLimitation.equalsIgnoreCase("Yes")){

								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
								report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
								sleep(800);
							}
							else{

								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
								report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
								sleep(800);
							}
						}

						//*****Resident of Australia****\\

						/*if(Citizen.equalsIgnoreCase("Yes")){

							driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[1]")).click();
							sleep(250);
							report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

							}
						else{
							driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[2]")).click();
							sleep(250);
							report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

							}*/

						//*****Industry********\\

						    sleep(500);
						    Select Industry=new Select(driver.findElement(By.name("lifeEventIndustry")));
						    Industry.selectByVisibleText(IndustryType);
						    sleep(800);
							report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
							sleep(500);

						//*****Occupation*****\\

							Select Occupation=new Select(driver.findElement(By.name("lifeEventOccupation")));
							Occupation.selectByVisibleText(OccupationType);
						    sleep(800);
							report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
							sleep(500);


						//*****Other Occupation *****\\

						if(OccupationType.equalsIgnoreCase("Other")){

							wait.until(ExpectedConditions.elementToBeClickable(By.name("lifeEventOtherOccupation"))).sendKeys(OtherOccupation);
							report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
							sleep(1000);
						}


						//*****Extra Questions*****\\

					    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


					//***Select if your office Duties are Undertaken within an Office Environment?\\

					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

					    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

					}

					      //*****Do You Hold a Tertiary Qualification******\\

					        if(TertiaryQual.equalsIgnoreCase("Yes")){

					        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}
					   else{

						   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}
					}



					else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



					//******Do you work in hazardous environment*****\\

					if(HazardousEnv.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='hazardousLifeEventQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='hazardousLifeEventQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
					}

					//******Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?*******\\

					if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='lifeEventOutsideOffice']/parent::*)[1]"))).click();
						report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='lifeEventOutsideOffice']/parent::*)[2]"))).click();
						report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
					}
					sleep(800);
					//*****Extra Questions*****\\

				    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}
				}


					}

			          sleep(800);

			        //*****What is your annual Salary******


					    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
					    sleep(500);
					    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
					    waitForPageTitle("testing", 3);


					//*****Click on Continue*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
					report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
					sleep(1000);

					//********************************************************************************************************\\
					//****************************LIFE EVENT SECTION***********************************************************\\
					//*********************************************************************************************************\\


					//*****Please select the specific life event you are applying under to increase your cover*****\\

					Select Event=new Select(driver.findElement(By.name("event")));
					Event.selectByVisibleText(LifeEvent);
					sleep(500);
					report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);

	             if(Negative_Scenario.equalsIgnoreCase("No")){

	            	//*****What is the date of the life event?*******\\

						driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
						sleep(500);
						driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
						report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
						sleep(800);


				//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months
				//within this calendar year?********************\\

				if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[1]"))).click();
					report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
					sleep(800);

				}

				else if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[2]"))).click();
					report.updateTestLog("Prior Request for Increase in Cover this year","No is Selected", Status.PASS);
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[3]"))).click();
					report.updateTestLog("Prior Request for Increase in Cover this year","Unsure is Selected", Status.PASS);
					sleep(800);
				}

	             }






	             //*******Enter Annual salary******\\

	                wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);;
					report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
					sleep(1000);


					//*****Click on Continue*******\\

					/*wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(lifeEvent);']"))).click();
					report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
					sleep(1000);*/
					 //****Save & Exit******\\
					sleep(800);
					driver.findElement(By.xpath("(//button[@ng-click='saveQuoteLifeEvent()'])[1]")).click();
					report.updateTestLog("Save & Exit button", "Save & Exit button is clicked", Status.PASS);

				    //****Fetch the Application No******\\

					sleep(800);
					String AppNo=driver.findElement(By.xpath("//div[@id='tips_text']/strong[2]")).getText();
					report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);

					//******Finish and Close Window******\\

					sleep(500);
				//	quickSwitchWindows();
					driver.findElement(By.xpath("//button[text()='Finish & Close Window ']")).click();
					report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
					sleep(1500);

		        sleep(4000);



				sleep(4000);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}


public void LifeEventsaveHOSTPLUSValidation() {

	String EmailId = dataTable.getData("LifeSaveAndExit", "EmailId");
	String TypeofContact = dataTable.getData("LifeSaveAndExit", "TypeofContact");
	String ContactNumber = dataTable.getData("LifeSaveAndExit", "ContactNumber");
	String TimeofContact = dataTable.getData("LifeSaveAndExit", "TimeofContact");
	String RegularIncome = dataTable.getData("LifeSaveAndExit", "RegularIncome");
	String WorkWithoutInjury = dataTable.getData("LifeSaveAndExit", "RegularIncome");
	String WorkLimitation = dataTable.getData("LifeSaveAndExit", "RegularIncome");
	String Citizen = dataTable.getData("LifeSaveAndExit", "Citizen");
	String IndustryType = dataTable.getData("LifeSaveAndExit", "IndustryType");
	String OccupationType = dataTable.getData("LifeSaveAndExit", "OccupationType");
	String OtherOccupation = dataTable.getData("LifeSaveAndExit", "OtherOccupation");
	String OfficeEnvironment = dataTable.getData("LifeSaveAndExit", "OfficeEnvironment");
	String TertiaryQual = dataTable.getData("LifeSaveAndExit", "TertiaryQual");
	String HazardousEnv = dataTable.getData("LifeSaveAndExit", "HazardousEnv");
	String OutsideOfcPercent = dataTable.getData("LifeSaveAndExit", "OutsideOfcPercent");
	String LifeEvent = dataTable.getData("LifeSaveAndExit", "LifeEvent");
	String LifeEvent_Date = dataTable.getData("LifeSaveAndExit", "LifeEvent_Date");
	String LifeEvent_PrevApplyin1Year = dataTable.getData("LifeSaveAndExit", "LifeEvent_PrevApplyin1Year");
	String DocumentaryEvidence = dataTable.getData("LifeSaveAndExit", "DocumentaryEvidence");
	String AttachPath = dataTable.getData("LifeSaveAndExit", "AttachPath");
	String Negative_Scenario=dataTable.getData("LifeSaveAndExit", "Negative_Scenario");
	String Dodcheckbox = dataTable.getData("LifeSaveAndExit", "Dodcheckbox");
	String Privacycheckbox = dataTable.getData("LifeSaveAndExit", "Privacycheckbox");
	String Gender = dataTable.getData("LifeSaveAndExit", "Gender");

	try{
		waitForPageTitle("testing", 8);
		WebDriverWait wait=new WebDriverWait(driver,20);
		//*** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Saved Applications']"))).click();
		sleep(7000);
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
	    sleep(500);
	//	if(driver.getPageSource().contains("pending")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']"))).click();
	    	report.updateTestLog("Retrieve Saved Application", "Saved Application is Retrieved", Status.PASS);
	    	sleep(4000);


			 WebElement email=driver.findElement(By.name("contactDetailsLifeEventEmail"));
			 String Email_validation=email.getAttribute("value");
			 System.out.println(Email_validation);
			 WebElement contacttype=driver.findElement(By.xpath("//select[@ng-model='preferredContactType']"));
			 String contacttype_validation=contacttype.getAttribute("value");

			 WebElement contactnumber=driver.findElement(By.name("contactDetailsLifeEventPhone"));
			 String contactnumber_validation=contactnumber.getAttribute("value");

			 WebElement morning=driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*"));
			 WebElement afternoon=driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*"));

			    sleep(100);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,400)", "");
				sleep(100);

			 if(Dodcheckbox.equalsIgnoreCase("Yes")){
				 WebElement Dod=driver.findElement(By.xpath(".//input[@name='dod']/parent::*"));
				 Dod.isSelected();
					 report.updateTestLog("Time of contact", "DOD checkbox is selected", Status.PASS);
					}
					else{

						report.updateTestLog("Time of contact", "DOD checkbox is not selected", Status.FAIL);
					}

			    sleep(100);
				JavascriptExecutor a = (JavascriptExecutor) driver;
				a.executeScript("window.scrollBy(0,300)", "");
				sleep(100);

			 if(Privacycheckbox.equalsIgnoreCase("Yes")){
				 WebElement Privacy=driver.findElement(By.xpath(".//input[@name='privacy']/parent::*"));
				 Privacy.isSelected();
					 report.updateTestLog("Time of contact", "Privacy Statement checkbox is selected", Status.PASS);
					}
					else{

						report.updateTestLog("Time of contact", "Privacy Statement checkbox is not selected", Status.FAIL);
					}
			  sleep(100);
				JavascriptExecutor b = (JavascriptExecutor) driver;
				b.executeScript("window.scrollBy(0,400)", "");
				sleep(100);

				 if(EmailId.contains(Email_validation)){

						report.updateTestLog("Email ID", EmailId+" is displayed as Expected", Status.PASS);
					}
					else{

						report.updateTestLog("Email ID", EmailId+" is not displayed as expected", Status.FAIL);
					}

				 if(TypeofContact.contains(contacttype_validation)){

						report.updateTestLog("TypeofContact", TypeofContact+" is displayed as Expected", Status.PASS);
					}
					else{

						report.updateTestLog("TypeofContact", TypeofContact+" is not displayed as expected", Status.FAIL);
					}

				 if(ContactNumber.contains(contactnumber_validation)){

						report.updateTestLog("ContactNumber", ContactNumber+" is displayed as Expected", Status.PASS);
					}
					else{

						report.updateTestLog("ContactNumber", ContactNumber+" is not displayed as expected", Status.FAIL);
					}


				 if(TimeofContact.equalsIgnoreCase("Afternoon")){

					 afternoon.isSelected();
						 report.updateTestLog("Time of contact", TimeofContact+" is displayed as Expected", Status.PASS);
						}
						else{
							morning.isSelected();
							report.updateTestLog("Time of contact", TimeofContact+" is not displayed as expected", Status.FAIL);
						}

				 if(Gender.equalsIgnoreCase("Male")){
					 WebElement GenderM=driver.findElement(By.xpath(".//input[@value='Male']/parent::*"));
					 GenderM.isSelected();
						 report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
						}
						else{
							 WebElement GenderF=driver.findElement(By.xpath(".//input[@value='Male']/parent::*"));
							GenderF.isSelected();
							report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
						}

				    sleep(100);
					JavascriptExecutor c = (JavascriptExecutor) driver;
					c.executeScript("window.scrollBy(0,500)", "");
					sleep(100);


	//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\

					if(RegularIncome.equalsIgnoreCase("Yes")){
						 WebElement RegularIncomeyes=driver.findElement(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"));
						 RegularIncomeyes.isSelected();
							report.updateTestLog("Regular Income Question", "Yes is Selected", Status.PASS);

						}
					else{
						 WebElement RegularIncomeno=driver.findElement(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"));
						 RegularIncomeno.isSelected();
							report.updateTestLog("Regular Income Question", "No is Selected", Status.PASS);

						}


					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
							 WebElement WorkWithoutInjuryyes=driver.findElement(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"));
							 WorkWithoutInjuryyes.isSelected();
								report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);

							}
						else{
							 WebElement WorkWithoutInjuryno=driver.findElement(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"));
							 WorkWithoutInjuryno.isSelected();
								report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);

							}

					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){

							 WebElement WorkWithoutInjuryyes=driver.findElement(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"));
							 WorkWithoutInjuryyes.isSelected();
								report.updateTestLog("Identifiable duties without Injury", WorkLimitation+ "is Selected", Status.PASS);

							}
						else{
							 WebElement WorkWithoutInjuryno=driver.findElement(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"));
							 WorkWithoutInjuryno.isSelected();
								report.updateTestLog("Identifiable duties without Injury", WorkLimitation+"is Selected", Status.PASS);

							}
					}


/*if(Citizen.equalsIgnoreCase("Yes")){
	 WebElement Citizenyes=driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[1]"));
	 Citizenyes.isSelected();
	report.updateTestLog("Citizen or PR", Citizen+" is Selected", Status.PASS);

	}
else{
	 WebElement CitizenNo=driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[2]"));
	 CitizenNo.isSelected();
	report.updateTestLog("Citizen or PR", Citizen+" is Selected", Status.PASS);

	}*/
Select Industry=new Select(driver.findElement(By.name("lifeEventIndustry")));
sleep(1000);
WebElement inds =Industry.getFirstSelectedOption();
String Industry_validation = inds.getText();
sleep(800);

WebElement occupation=driver.findElement(By.name("lifeEventOccupation"));
String occupation_validation=occupation.getAttribute("value");


if(IndustryType.contains(Industry_validation)){



		report.updateTestLog("Industry", IndustryType+" is displayed as Expected", Status.PASS);
	}
	else{

		report.updateTestLog("Industry", IndustryType+" is not displayed as expected", Status.FAIL);
	}


if(OccupationType.contains(occupation_validation)){

		report.updateTestLog("Occupation", OccupationType+" is displayed as Expected", Status.PASS);
	}
	else{

		report.updateTestLog("Occupation", OccupationType+" is not displayed as expected", Status.FAIL);
	}


if(OtherOccupation.contains("Yes")){
	 WebElement otheroccupation=driver.findElement(By.name("lifeEventOtherOccupation"));
	 String otheroccupation_validation=otheroccupation.getAttribute("value");
if(OtherOccupation.contains(otheroccupation_validation)){
		report.updateTestLog("OtherOccupation", OtherOccupation+" is displayed as Expected", Status.PASS);
	}
	else{

		report.updateTestLog("OtherOccupation", OtherOccupation+" is not displayed as expected", Status.FAIL);
	}

}

           sleep(100);
			JavascriptExecutor d = (JavascriptExecutor) driver;
			d.executeScript("window.scrollBy(0,1000)", "");
			sleep(100);

WebElement LifeEventV=driver.findElement(By.name("event"));
String LifeEventV_validation=LifeEventV.getAttribute("value");
System.out.println(LifeEventV_validation);

if(LifeEvent.contains(LifeEventV_validation)){
	report.updateTestLog("Life Event", LifeEvent+" is displayed as Expected", Status.PASS);
}
else{

	report.updateTestLog("Life Event", LifeEvent+" is not displayed as expected", Status.FAIL);
}

if(Negative_Scenario.equalsIgnoreCase("No")){

	WebElement LifeEvent_DateV=driver.findElement(By.name("eventDate"));
	String LifeEvent_Date_validation=LifeEvent_DateV.getAttribute("value");


//*****What is the date of the life event?*******\\

	if(LifeEvent_Date.contains(LifeEvent_Date_validation)){
		report.updateTestLog("Date of Life Event", LifeEvent_Date+" is displayed as Expected", Status.PASS);
	}
	else{

		report.updateTestLog("Date of Life Event", LifeEvent_Date+" is not displayed as expected", Status.FAIL);
	}

//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months
//within this calendar year?********************\\

if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
	WebElement LifeEvent_PrevApplyin1YearYes=driver.findElement(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[1]"));
		LifeEvent_PrevApplyin1YearYes.isSelected();
		report.updateTestLog("Prior Request for Increase in Cover this year", "Yes is Selected", Status.PASS);
	}

if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")){
	WebElement LifeEvent_PrevApplyin1YearNO=driver.findElement(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[2]"));
	LifeEvent_PrevApplyin1YearNO.isSelected();
		report.updateTestLog("Prior Request for Increase in Cover this year", "No is Selected", Status.PASS);
	}


if(!LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")&&!LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
	WebElement LifeEvent_PrevApplyin1Yearunsure=driver.findElement(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[3]"));
	LifeEvent_PrevApplyin1Yearunsure.isSelected();
		report.updateTestLog("Prior Request for Increase in Cover this year", "Unsure is Selected", Status.PASS);
	}

}
sleep(100);
JavascriptExecutor e = (JavascriptExecutor) driver;
e.executeScript("window.scrollBy(0,1000)", "");
sleep(100);
/*if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
	WebElement DocumentYes=driver.findElement(By.xpath("(//input[@name='documentName']/parent::*)[1]"));
		DocumentYes.isSelected();
		report.updateTestLog("Documentary Evidence", "Documentary Evidence Yes is selected", Status.PASS);
	}
	else{
		WebElement DocumentNo=driver.findElement(By.xpath("//*[@id='collapseThree']/form/div/div[5]/div[2]/div/div[2]/label"));
		DocumentNo.isSelected();
		report.updateTestLog("Documentary Evidence", "Documentary Evidence No is selected", Status.PASS);
		WebElement DocumentNocheckbox=driver.findElement(By.xpath("//*[@id='acknowledgeDocAdressCheck']/span"));
		DocumentNocheckbox.isSelected();
		report.updateTestLog("Documentary Evidence", "No checkbox is selected", Status.PASS);

}*/
		/*}else{
			report.updateTestLog("Retrieve Saved Application", "No Applications to Retrieve", Status.PASS);
		}
*/
}catch(Exception e){
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
}
}

