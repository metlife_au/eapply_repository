package au.com.metlife.eapply.controller;
//TEST
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.ct.service.LoginAttemptsService;
import au.com.metlife.eapply.ct.utils.UnZip;
import au.com.metlife.eapply.quote.utility.SystemProperty;


@RestController
@Component
public class ClaimTrackerController {
	private static final Logger log = LoggerFactory.getLogger(ClaimTrackerController.class);

	/*@Value("${spring.docsUploadLoc}")
    private String docsUploadLoc= "/shared/props/eapply2/";*/
   /* private String docsUploadLoc = "C:\\ClaimsTracker\\";*/
    private final LoginAttemptsService loginService;
   
    SystemProperty sys;
    
    @Autowired
    public ClaimTrackerController(final LoginAttemptsService loginService) {        
        this.loginService = loginService;
       /* this.ctHistoryService = ctHistoryService;*/
    }

	@RequestMapping(method=RequestMethod.POST, value = "/login")
	public ResponseEntity<String> login(@RequestParam(value="secret") String secret,@RequestParam(value="response") String response,@RequestParam(value="remoteip") String remoteip) {
		log.info(MetlifeInstitutionalConstants.LOGIN_START);
		RestTemplate resttemplate =new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("secret", secret);
		map.add("response", response);
		map.add("remoteip", remoteip);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

		ResponseEntity<String> responseMsg = resttemplate.postForEntity( "https://www.google.com/recaptcha/api/siteverify", request , String.class );
		log.info(MetlifeInstitutionalConstants.LOGIN_END);
		log.info("responseMsg: {} ",responseMsg);
	    return new ResponseEntity<>(responseMsg.getBody(), HttpStatus.OK);
	} 
	
	@RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    @ResponseBody
    public  void multipleUpload(@RequestParam("claimStatus") String status,@RequestParam("qquuid") String  qquuid,@RequestParam("description") String description ,@RequestParam("qqfile") MultipartFile file, HttpServletResponse response) throws IllegalStateException, IOException, JSONException {
		log.info("multipleUpload() .... start");
       /* String saveDirectory = "C:\\ClaimsTracker\\";*/
        Document document;
        try {
			sys = SystemProperty.getInstance();
		} catch (Exception e) {
			 log.error("Error in upload file :   {}",e);
		}
         try 
         {
            Thread.sleep(1000);
         } 
         catch (InterruptedException e) {
        	 log.error("Error in intrrupt  :{}",e);
        	 Thread.currentThread().interrupt();
         }  
         String fileName = file.getOriginalFilename();
         fileName = FilenameUtils.getName(fileName);
         log.error("fileName : {}",fileName);
         log.error("status : {}",status);
/*         if (!"".equalsIgnoreCase(fileName)) {
             // Handle file content - multipartFile.getInputStream()
        	 log.info("claimsdocurl : {}",sys.getProperty("claimsdocurl")); 
            file.transferTo(new File(sys.getProperty("claimsdocurl") + fileName));
          }
*/         if (!"".equalsIgnoreCase(fileName)) {
             /* Handle file content - multipartFile.getInputStream()
        	  log.info("claimsdocurlll : {}",sys.getProperty("claimsdocurl")); */
	try(FileOutputStream fos = new FileOutputStream(sys.getProperty("claimsdocurl")+fileName)){
		fos.write(file.getBytes());
	}
        	   /* FileOutputStream fos = new FileOutputStream(sys.getProperty("claimsdocurl")+fileName); 
        	    fos.write(file.getBytes());
        	    fos.close(); */
             /*file.transferTo(new File(sys.getProperty("claimsdocurl") + fileName));*/
           }
         
			document = new Document();
			document.setDocumentCode(description);
			document.setDocumentlocation(sys.getProperty("claimsdocurl")+fileName);
			document.setDocumentStatus(status);
			document.setUuId(qquuid);
			document.setFileSize(Long.toString(file.getSize()));
			document.setFileName(file.getOriginalFilename());
            
            JSONObject json1 = new JSONObject(); 
            
            ObjectMapper mapper = new ObjectMapper();
           
           String docString = mapper.writeValueAsString(document);
             json1.put(MetlifeInstitutionalConstants.SUCCESS, true);
             json1.put("doucment", docString);
             response.setCharacterEncoding("UTF-8");
             response.setContentType("application/json");
             response.getWriter().print(json1);
             response.flushBuffer();
             log.info("multipleUpload() .... end");
         } 	
		
	
	@RequestMapping(method=RequestMethod.POST, value = "/mangeLoginAttempts")
	public ResponseEntity<String> mangeLoginAttempts(@RequestBody au.com.metlife.eapply.ct.dto.LoginAttempts loginAttempts) {
		log.info(MetlifeInstitutionalConstants.LOGIN_START);		
		loginService.saveOrUpdateLoginAttempts(loginAttempts);
		log.info(MetlifeInstitutionalConstants.LOGIN_END);
	    return new ResponseEntity("", HttpStatus.OK);
	} 
	
	@RequestMapping(method=RequestMethod.POST, value = "/retrieveloginAttempts")
	public ResponseEntity<String> retrieveloginAttempts(@RequestBody au.com.metlife.eapply.ct.dto.LoginAttempts loginAttempts) {
		log.info(MetlifeInstitutionalConstants.LOGIN_START);		
		 /*final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  long difference = date2.getTime() - date1.getTime();*/
		 
	    loginAttempts= loginService.retrieveLoginAttempts(loginAttempts);
	    loginAttempts.setUnlock(Boolean.FALSE);
	    if (null != loginAttempts.getIpLocked() && loginAttempts.getIpLocked().equals('Y')) {
	    	Calendar currentTime = Calendar.getInstance();
	    	currentTime.setTime(new Date());	   
	    	Calendar loginTime = Calendar.getInstance();
	    	loginTime.setTime(loginAttempts.getLastLogin());
		    /*long millseconds = (new Date().getTime()) - loginAttempts.getLastLogin().getTime();		 */   
		    long seconds = (currentTime.getTimeInMillis() - loginTime.getTimeInMillis()) / 1000;
		    int diffMins = (int) (seconds / 60);		    
		    if (diffMins >30) {
			   loginAttempts.setUnlock(Boolean.TRUE);
		    }
	    }
	   /* JSONObject json = new JSONObject();*/
        ObjectMapper mapper = new ObjectMapper();
        String loginString = null;
        try {
        	loginString = mapper.writeValueAsString(loginAttempts);
        	/*mapper.setDateFormat(dateFormat);*/
		} catch (JsonProcessingException e) {
			
			log.error("Error in json processing : {}",e.getMessage());
		}
		log.info(MetlifeInstitutionalConstants.LOGIN_END);
		return new ResponseEntity<>(loginString, HttpStatus.OK);
	} 
	
	@RequestMapping(method=RequestMethod.POST, value = "/mangeCTHistory")
	public ResponseEntity<String> mangeCTHistory(@RequestBody au.com.metlife.eapply.ct.dto.CTHistoryDto ctHistoryDto) {
		log.info(MetlifeInstitutionalConstants.LOGIN_START);		
		loginService.saveCTHistory(ctHistoryDto);
		log.info(MetlifeInstitutionalConstants.LOGIN_END);
	    return new ResponseEntity("", HttpStatus.OK);
	} 
	
	@RequestMapping(method=RequestMethod.POST, value = "/retrieveFunds")
	public ResponseEntity<String> retrieveFunds() {
		log.info(MetlifeInstitutionalConstants.LOGIN_START);	
		try {
			sys = SystemProperty.getInstance();
		} catch (Exception e) {
			 log.error("Error in upload file : {}",e);
		}
		String saveDirectory = sys.getProperty(MetlifeInstitutionalConstants.FUND_PROP);
		//Change as per Sonar fix "Try with Resource Added" start
		//FileInputStream in = null;
		//String funds = null;
		JSONArray ja = new JSONArray();
		
		try (FileInputStream in = new FileInputStream(saveDirectory)){
			/*in = new FileInputStream(saveDirectory);*/
			Properties props = new Properties();
			props.load(in);
			Set<String> fundNames = props.stringPropertyNames();
			/*in.close();*/			
			
			 //ObjectMapper mapper = new ObjectMapper();		        
		        try {
		        	//funds = mapper.writeValueAsString(fundNames);
		        	JSONObject jo = new JSONObject();
		    		jo.put("partnerId", "NewPartner");
		    		jo.put("PartnerName", "NewPartner");
		    		ja.put(jo);
		        	for (String fundName : fundNames){
					    jo = new JSONObject();
						jo.put("partnerId", fundName);
						jo.put("PartnerName", fundName);
						ja.put(jo);
						
					}
		        	/*mapper.setDateFormat(dateFormat);*/
				} catch (JSONException e) {
					
					log.error("Error in json processing : {}",e.getMessage());
				}			
		}catch (FileNotFoundException e) {
			
			log.error("Error file not found : {}",e);
		}	
		catch (IOException e) {
			
			log.error("Error in IO : {}",e.getMessage());
		}/*finally{
			try{
			if (in != null){
				in.close();
			}			
			}catch (IOException e) {
				log.error("Error in IO: {}",e.getMessage());
			}
		}	*/
		//Change as per Sonar fix "Try with Resource Added" end
		log.info(MetlifeInstitutionalConstants.LOGIN_END);
	    return new ResponseEntity<>(ja.toString(), HttpStatus.OK);
	} 
	
	@RequestMapping(method=RequestMethod.POST, value = "/updateFunds/{fund}")
	public ResponseEntity<String> updateFunds(@PathVariable("fund") String fund) {
		log.info(MetlifeInstitutionalConstants.LOGIN_START);
		//Change as per Sonar fix "Try with Resource Added" start
		/*FileOutputStream out = null;
		FileInputStream in = null;*/
		try {
			sys = SystemProperty.getInstance();
		} catch (Exception e) {
			 log.error("Error in upload file: {}",e);
		}
		String saveDirectory = sys.getProperty(MetlifeInstitutionalConstants.FUND_PROP);
		
		try(FileOutputStream out = new FileOutputStream(saveDirectory); FileInputStream in = new FileInputStream(sys.getProperty(MetlifeInstitutionalConstants.FUND_PROP));) {
						/*in = new FileInputStream(sys.getProperty(MetlifeInstitutionalConstants.FUND_PROP));*/
						/*in = new FileInputStream(saveDirectory);*/
			
			
			 			Properties props = new Properties();
						props.load(in);			
					    /*out = new FileOutputStream(saveDirectory);*/			
		
			 			props.setProperty(fund, fund);
						props.store(out, null);
						/*out.close();*/
						
				
			 		}catch (FileNotFoundException e) {
			 			log.error("Error in  file  : {}",e);
			 		}	
			 		catch (IOException e) {
			 			log.error("Error in  input and output : {}",e);
					}	/*finally{
						try{
						if (in != null){
							in.close();
						}
						if (out != null){
							out.close();
							
						}
						}catch (IOException e) {		
						log.error("Error in   output : {}",e);
						}
					}*/	
		//Change as per Sonar fix "Try with Resource Added" End
		
		log.info(MetlifeInstitutionalConstants.LOGIN_END);
	    return new ResponseEntity("", HttpStatus.OK);
	} 
	
	@RequestMapping(value="/singleSave", method=RequestMethod.POST )
	
    public  ResponseEntity<String> singleSave(@RequestParam("file") MultipartFile file,
    		@RequestParam("partnerName") String partnerName,@RequestParam("newPartner") Boolean newPartner,@RequestParam("filepath") String filepath,HttpServletResponse response){
    	/*System.out.println("File Description:"+desc);
		String directory = "C:\\ClaimsTracker\\";*/
		try {
			sys = SystemProperty.getInstance();
		} catch (Exception e) {
			 log.error("Error in upload file :  {}",e.getMessage());
		}
		String directory = sys.getProperty("adminFileUploadDir");
		/*String directory = "C:\\Users\\metdev\\git\\eapply_repository_Metlife\\eapply_repository\\eApplyv2\\src\\main\\webapp\\static\\pages\\";*/
		 try {
				sys = SystemProperty.getInstance();
			} catch (Exception e) {
				 log.error("Error in upload file : {}",e);
			}
		 /*String directory = sys.getProperty("adminFileUploadDir");*/
		//Change as per Sonar fix "Try with Resource Added" start
		 /*FileOutputStream fos = null;
		 BufferedOutputStream buffStream = null;*/
		if (!newPartner) {
			if("disclaimer".equalsIgnoreCase(filepath)) {
				directory = directory+partnerName+"/";
			} else {
				directory = directory+partnerName+"/"+filepath+"/";
			}
			
		}
    	String fileName = null;
    	JSONObject json1 = new JSONObject(); 
    	if (!file.isEmpty()) {
    		fileName = file.getOriginalFilename();
            try(FileOutputStream fos =  new FileOutputStream(new File(directory + fileName));
            		BufferedOutputStream buffStream = new BufferedOutputStream(fos); ) {
                /*fileName = file.getOriginalFilename();*/                
                byte[] bytes = file.getBytes();
                 //fos =new FileOutputStream(new File(directory + fileName));
                 /*buffStream = 
                        new BufferedOutputStream(fos);*/
                buffStream.write(bytes);   
                buffStream.flush();
                //buffStream.close();
                UnZip unzip = new UnZip();
                unzip.unZip(directory + fileName, directory);              
                        
                 json1.put(MetlifeInstitutionalConstants.SUCCESS, true);
                 /*json1.put("msg", "The file has been uploaded succesfully");*/
                 
            } catch (Exception e) {
            	log.error("Error in  input and output: {}",e.getMessage());
                try {
					json1.put(MetlifeInstitutionalConstants.SUCCESS, false);
					/*json1.put("msg", "The file has not been uploaded");	*/				 
				} catch (Exception e1) {
					 log.error("Error in upload file: {}",e1.getMessage());
					/*return new ResponseEntity<String>("Json is not formatted", HttpStatus.OK);*/
				}
                
               
            }/*finally{
            	try {
            		if(null!= fos){
            			fos.close();
            		}  
            		if (null !=  buffStream) {
            				buffStream.close();
            		}
            	}catch(Exception e1){
            		 log.error("Error in upload file : {}",e1);
            	}
            }*/
          //Change as per Sonar fix "Try with Resource Added" End
        } else{        	
        	return new ResponseEntity<>("File is empty", HttpStatus.OK);
        }
    	return new ResponseEntity<>(json1.toString(), HttpStatus.OK);
    }
    @RequestMapping(value="/multipleUpload")
    public String multiUpload(){
    	return "multipleUpload";
    }
/*   @RequestMapping(value="/multipleSave", method=RequestMethod.POST )
    public @ResponseBody String multipleSave(@RequestParam("file") MultipartFile[] files){
    	String fileName = null;
    	String msg = "";
   	if (files != null && files.length >0) {
    		for(int i =0 ;i< files.length; i++){
	            try {
	                fileName = files[i].getOriginalFilename();
	                byte[] bytes = files[i].getBytes();
	                BufferedOutputStream buffStream = 
	                        new BufferedOutputStream(new FileOutputStream(new File("C:\\ClaimsTracker\\" + fileName)));
	                buffStream.write(bytes);
	                buffStream.close();
	                msg += "You have successfully uploaded " + fileName +"<br/>";
	            } catch (Exception e) {
	                return "You failed to upload " + fileName + ": " + e +"<br/>";
	            }
    		}
   		return msg;
        } else {
            return "Unable to upload. File is empty.";
        }
        }
*/
	
}
