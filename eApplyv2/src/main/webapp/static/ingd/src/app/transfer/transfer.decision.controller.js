class TransferDecisionCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._ngDialog = ngDialog;
    this._$scope = $scope;
    this._EapplyData = EapplyData;    
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
    this.transferData = this._EapplyData.getTransferData();
    
    this.pdfLocation = this._EapplyData.getPdfLocation();
    this.npsURL = this._EapplyData.getNpsUrl();
    
    this.finalDecision = this.transferData.applicant.overallCoverDecision;
    
    this.openNPSPopup();
    
    this._$scope.$on('$locationChangeStart', function(evnt, next, current){
    	var currentURL = current.split("/");
    	if(currentURL[currentURL.length-1] == 'transferdecision'){
    		evnt.preventDefault();
    	}    	
    });
    
  }
  
  downloadPDF(){
	  
	  const _this = this;	  
	  var filename = _this.pdfLocation.substring(_this.pdfLocation.lastIndexOf('/')+1);
	  var a = document.createElement('a');
	  document.body.appendChild(a);
	  
	  _this._CommonAPI.downloadPDF(_this.urlList.downloadUrl, _this.pdfLocation).then(
			  (res) => {
				  if(navigator.appVersion.toString().indexOf('.NET') > 0) {
					  window.navigator.msSaveBlob(res.response,filename);
				  }else{
					  var fileURL = URL.createObjectURL(res.response);
					  a.href = fileURL;
					  a.download = filename;
					  a.click();
				  }
			  }, 
              (err) => {
                console.log('Error while Downloading the PDF' + err);
              }
	  );	  	
  }
  
  openNPSPopup(){
	  var templateContent = '<div class="ngdialog-content"><div class="modal-header">';
	    templateContent += '<h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div>';
	    templateContent += '<div class="modal-body"><div class="row rowcustom"><div class="col-sm-12">';
	    templateContent += '<p class="aligncenter">  </p>';
	    templateContent += '<div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>';
	    templateContent += '<p></p></div></div></div>';
	    templateContent += '<div class="ngdialog-buttons aligncenter">';
	    templateContent += '<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button>';
	    templateContent += '<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button>';
	    templateContent += ' </div></div>';
	    	
	    this._ngDialog.openConfirm({
	   		template: templateContent,
	 		className: 'ngdialog-theme-plain custom-width',
	 		plain: true,
	 		showClose: false
	   	}).then(
	   	      (value) => {
	   	    	if(value==='onYes'){
	   	  			if(npsTokenUrl != null){
	   	  			  var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+this.npsURL;
	   						$window.open(url, '_blank');
	   						return true;
	   	  			}
	   	  		}
	   	      }, 
	   	      (event) => {
	   	        if(event === 'oncancel') {
	   	          return false;
	   	        }
	   	      }
	   	    );
  }
}

export default TransferDecisionCtrl;