package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPSummaryPageObjects extends PageObjectLibrary{
	public static final By btnsummaryNext = getObjectIdentifaction("YBRIP.summary.next.btn.css");
	public static final By chkexclusion = getObjectIdentifaction("YBRIP.summary.exclusion.chkbox.css");	
	public static final By chkloading = getObjectIdentifaction("YBRIP.summary.loading.chkbox.css");	
}
