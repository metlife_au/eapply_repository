package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class INGD_TransferYourCoverPageObjects extends PageObjectLibrary{
		
	public static final By  TransferYourCoverLink = getObjectIdentifaction("INGD.TYC.Link.id");
	public static final By  FifteenHoursYes = getObjectIdentifaction("INGD.TYC.FifteenHoursYes.id");
	public static final By  FifteenHoursNo = getObjectIdentifaction("INGD.TYC.FifteenHoursNo.id");
	public static final By  Industry = getObjectIdentifaction("INGD.TYC.Industry.id");
	public static final By  Occupation = getObjectIdentifaction("INGD.TYC.Occupation.id");	
	public static final By  AnnualSalary = getObjectIdentifaction("INGD.TYC.AnnualSalary.id");
	public static final By  DeathAmount = getObjectIdentifaction("INGD.TYC.DeathAmount.id");
	public static final By  TPDAmount = getObjectIdentifaction("INGD.TYC.TPDAmount.id");
	public static final By  IPAmount = getObjectIdentifaction("INGD.TYC.IPAmount.id");
	public static final By  WaitingPeriod = getObjectIdentifaction("INGD.TYC.WaitingPeriod.id");
	public static final By  BenefitPeriod = getObjectIdentifaction("INGD.TYC.BenefitPeriod.id");
	public static final By  CalculateQuote = getObjectIdentifaction("INGD.TYC.CalculateQuote.xpath");
	public static final By  FetchDeathAmt = getObjectIdentifaction("INGD.TYC.FetchDeathAmt.id");
	public static final By  FetchTPDAmt = getObjectIdentifaction("INGD.TYC.FetchTPDAmt.id");
	public static final By  FetchIPAmt = getObjectIdentifaction("INGD.TYC.FetchIPAmt.id");
	public static final By  TransferCheckbox = getObjectIdentifaction("INGD.TYC.TransferCheckbox.id");
	public static final By  ContinueafterCoverdetails = getObjectIdentifaction("INGD.TYC.ContinueafterCoverdetails.xpath");
	public static final By  RestrictedillnessYes = getObjectIdentifaction("INGD.TYC.RestrictedillnessYes.id");
	public static final By  RestrictedillnessNo = getObjectIdentifaction("INGD.TYC.RestrictedillnessNo.id");
	public static final By  LodgeTPDClaimYes = getObjectIdentifaction("INGD.TYC.LodgeTPDClaimYes.id");
	public static final By  LodgeTPDClaimNo = getObjectIdentifaction("INGD.TYC.LodgeTPDClaimNo.id");
	public static final By  DiagnosedillnessYes = getObjectIdentifaction("INGD.TYC.DiagnosedillnessYes.id");
	public static final By  DiagnosedillnessNo = getObjectIdentifaction("INGD.TYC.DiagnosedillnessNo.id");
	public static final By  PremFundLoadingYes = getObjectIdentifaction("INGD.TYC.PremFundLoadingYes.id");
	public static final By  PremFundLoadingNo = getObjectIdentifaction("INGD.TYC.PremFundLoadingNo.id");
	public static final By  SubmitHealthPage = getObjectIdentifaction("INGD.TYC.SubmitHealthPage.xpath");
	
	
	public static final By  EvidenceofCoverchkbox = getObjectIdentifaction("INGD.TYC.EvidenceofCoverchkbox.id");
	public static final By  Declarationchkbox = getObjectIdentifaction("INGD.TYC.Declarationchkbox.id");
	public static final By  PreferredContact = getObjectIdentifaction("INGD.TYC.PreferredContact.id");
	public static final By  PreferredTime = getObjectIdentifaction("INGD.TYC.PreferredTime.id");
	public static final By  ContactNumber = getObjectIdentifaction("INGD.TYC.ContactNumber.id");
	public static final By  SubmitDeclaration = getObjectIdentifaction("INGD.TYC.SubmitDeclaration.xpath");


}


