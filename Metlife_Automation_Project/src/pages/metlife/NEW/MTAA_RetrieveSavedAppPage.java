package pages.metlife.NEW;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class MTAA_RetrieveSavedAppPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public MTAA_RetrieveSavedAppPage MTAARetreiveApp() {
		RetreiveApp();		
		
		
		return new MTAA_RetrieveSavedAppPage(scriptHelper);
	}

	public MTAA_RetrieveSavedAppPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void RetreiveApp() {

		
		
		try{
			
                 //****Click on Life Event Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='saveandretrieveId']/div/h5"))).click();		
			report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
			sleep(3000);
			
			List<WebElement>Applications=driver.findElements(By.xpath("//table/tbody/tr/td[1]/a/span"));
			
			int NoofApplication=Applications.size();
			
			if(NoofApplication<=0){
				report.updateTestLog("No Saved Applications", "There are no saved applications", Status.PASS);
			}
			
			else{
				report.updateTestLog("Saved Applications", "There are "+NoofApplication+"  saved applications", Status.PASS);
				String FirstApp=Applications.get(0).getText();
				Applications.get(0).click();
				report.updateTestLog("Retrieving Application", FirstApp+"  is Clicked", Status.PASS);
				sleep(2500);
			}
			
			
			
			
			
		
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}






