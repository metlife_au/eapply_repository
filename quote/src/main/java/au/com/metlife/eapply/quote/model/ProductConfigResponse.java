package au.com.metlife.eapply.quote.model;

import java.io.Serializable;

public class ProductConfigResponse implements Serializable{
	
	private int deathMinAge;
	
	private int deathMaxAge;
	
	private int tpdMinAge;
	
	private int tpdMaxAge;
	
	private int ipMinAge;
	
	private int ipMaxAge;
	
	private int deathMinUnits;
	
	private int deathMinAmount;
	
	private int tpdMinUnits;
	
	private int tpdMinAmount;
	
	private int ipMinUnits;
	
	private int ipMinAmount;
	
	private int deathMaxAmount;
	
	private int tpdMaxAmount;
	
	private int ipMaxAmount;
	
	private int deathTpdTransferMaxAmt;
	
	private int deathTpdTransferMaxUnits;
	
	private int ipTransferMaxAmt;
	

}
