package au.com.metlife.eapply.corporate.business.info;

public class EntityName {

	
	private String suffix;
	private String firstName;
	private String middleName;
	private String lastName;
	private String title;
	private String orgName;
	
	
	
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "EntityName [suffix=" + suffix + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", title=" + title + ", orgName=" + orgName + "]";
	}
	
	
}
