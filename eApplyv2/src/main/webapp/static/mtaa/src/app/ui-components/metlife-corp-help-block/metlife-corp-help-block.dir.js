(function(angular){
'use strict';

angular
    .module('Metlife.mtaa.uicomponents',['mtaa.ui.constants'])
    .directive('metlifeMtaaHelpBlock', metlifeMtaaHelpBlock);

function metlifeMtaaHelpBlock(mtaaUIConstants) {
    var directive = {
        restrict: 'EA',
        templateUrl: mtaaUIConstants.path.uiComponents + '/metlife-corp-help-block/metlife-corp-help-block.tmpl.html',
        controller: helpBlockCtrl,
        controllerAs: 'helpBlock',
        bindToController: true
    };

    return directive;
}

helpBlockCtrl.$inject = ['$scope', 'mtaaUIConstants'];

function helpBlockCtrl($scope, mtaaUIConstants) {
    var helpBlock = this;
    helpBlock.constants = {
        helpContact : '1300 362 415'
    };
}

})(angular);