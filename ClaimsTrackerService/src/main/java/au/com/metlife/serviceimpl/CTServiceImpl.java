package au.com.metlife.serviceimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.dto.DocumentDto;
import au.com.metlife.model.ClaimOutput;
import au.com.metlife.model.Document;
import au.com.metlife.model.DocumentRepository;
import au.com.metlife.service.CTService;

@Service
public class CTServiceImpl implements CTService {

	 private final DocumentRepository repository;
	 
	 @Autowired
	    public CTServiceImpl(final DocumentRepository repository) {
	        this.repository = repository;
	    }
	 
	 @Override
	 public void saveDocumentList(List<DocumentDto> documentList) {
		 Document document = null;
	 	// TODO Auto-generated method stub
		 for (DocumentDto documentDto : documentList) {
	        	document = new Document();
	        	document.setPolicyNum(documentDto.getPolicyNum());
	        	document.setDocumentCode(documentDto.getDocumentCode());
	        	document.setDocumentlocation(documentDto.getDocumentlocation());
	        	document.setDocumentStatus(documentDto.getDocumentStatus());
	        	document.setFileId(documentDto.getFileId());
	        	document.setFileName(documentDto.getFileName());
	        	document.setFileSize(documentDto.getFileSize());
	        	document.setUuId(documentDto.getUuId());
	        	document.setCREATEDDATE(new Date());
	        	repository.save(document);
	        	
	        }		 
	 }
	 
	 @Override
	 public List<Document> retriveDocList(String ClaimNo) {
	 	// TODO Auto-generated method stub
		 return repository.findByPolicyNum(ClaimNo);
	 	
	 }

	 
//	@Override
//	@Transactional
//	public Document generateToken(String input, String outhToken) {
//		// TODO Auto-generated method stub	
//		Document eappstring = new Document(input,outhToken, "Active", new Date(), "");
//		System.out.println("inside gen token");
//		eappstring = repository.save(eappstring);
//		return eappstring;
//	}
//	@Override
//	@Transactional
//	public Document retriveClientData(String token){		
//		//Eappstring eappstring = repository.findById(Long.parseLong(token));
//		Document eappstring =repository.findBySecureToken(token);
//		System.out.println(eappstring.getSecurestring());		
//		return eappstring;
//	}
	
	public  List<ClaimOutput> getClaimStatus(String claimNo,String surName,String dob){
		ClaimOutput claimOutput =null;
		//make a connection to GL/SCI
		Connection conn = null;
	   Statement stmt = null;
	   List<ClaimOutput> claimOutputList = new ArrayList<ClaimOutput>();
	   try {
		 //STEP 2: Register JDBC driver
		      Class.forName("com.ibm.db2.jcc.DB2Driver");

		      //STEP 3: Open a connection
		      System.out.println("Connecting to database62...");
		      conn = DriverManager.getConnection("jdbc:db2://10.173.60.142:60000/MADWODS","db2inst1","metlife123");

		      //STEP 4: Execute a query
		      System.out.println("Creating statement...");
		      stmt = conn.createStatement();
		      String sql;			   
		      //sql = "SELECT DISTINCT FCT.FUNDNAME,FCT.FUNDOWNERID,FCT.POLICYNO,FCT.CLAIM_ID,INT(FCT.CLAIMNO) AS CLAIMNO,RTRIM(FCT.PRODUCTTYPE) PRODUCTTYPE, UCASE(FCT.FIRSTNAME) AS FIRSTNAME,UCASE(FCT.SURNAME) AS SURNAME,FCT.DATEOFBIRTH,FCT.GENDER,FCT.REFERENCE,FCT.SCHEME,FCT.WAITPERIOD, FCT.BENEFITPERIOD,FCT.STATUS_CODE, ST.STATUSDESCRIPTION,FCT.DATESUBMITTED,FCT.DISABILITYDATE,FCT.CLAIMTYPE_DES,FCT.SUMINSURED, (FCT.TOTALBENEFITPAID_GROSS-COALESCE(TOTALBENEFITPAID_DEDUCTIONS,0)) AS BENEFITPAID,FCT.DATELASTPAID, FCT.CLAIMCAUSE_DES, '' as ADMITTED_DATE,FCT.SALARYINSURED,FCT.SUPERCONTRIBUTION,FCT.MONTHLYBENEFIT,FCT.CLAIMASSESSOR, FCT.LASTUPDDATE FROM DBUSDWW.FCT_CLM_DTL FCT JOIN DBUSDWW.TBLCLAIMSTATUS ST ON  FCT.STATUS_CODE = ST.CLAIMSTATUS AND FCT.PRODUCTTYPE = ST.PRODUCTTYPE where FCT.CLAIMNO = '"+claimNo+"'";
		      sql="SELECT DISTINCT FCT.FUNDNAME,FCT.FUNDOWNERID,FCT.POLICYNO,FCT.CLAIM_ID,INT(FCT.CLAIMNO) AS CLAIMNO,RTRIM(FCT.PRODUCTTYPE) PRODUCTTYPE, UCASE(FCT.FIRSTNAME) AS FIRSTNAME,UCASE(FCT.SURNAME) AS SURNAME,FCT.DATEOFBIRTH,FCT.GENDER,FCT.REFERENCE,FCT.SCHEME,FCT.WAITPERIOD, FCT.BENEFITPERIOD,FCT.STATUS_CODE, ST.STATUSDESCRIPTION,FCT.DATESUBMITTED,FCT.DISABILITYDATE,FCT.CLAIMTYPE_DES,FCT.SUMINSURED, (FCT.TOTALBENEFITPAID_GROSS-COALESCE(TOTALBENEFITPAID_DEDUCTIONS,0)) AS BENEFITPAID,FCT.DATELASTPAID, FCT.CLAIMCAUSE_DES, '' as ADMITTED_DATE,FCT.SALARYINSURED,FCT.SUPERCONTRIBUTION,FCT.MONTHLYBENEFIT,FCT.CLAIMASSESSOR, FCT.LASTUPDDATE FROM DBUSDWW.FCT_CLM_DTL FCT JOIN DBUSDWW.TBLCLAIMSTATUS ST ON  FCT.STATUS_CODE = ST.CLAIMSTATUS AND FCT.PRODUCTTYPE = ST.PRODUCTTYPE where FCT.CLAIMNO = '"+42966+"'"+" AND FCT.SURNAME = '"+surName+"' AND DATEOFBIRTH='"+dob+"'";
		      ResultSet rs = stmt.executeQuery(sql);

		      //STEP 5: Extract data from result set
		      while(rs.next()){
		    	  claimOutput = new ClaimOutput();
		    	  claimOutput.setFundname( rs.getString("FUNDNAME"));	
		    	  claimOutput.setFundownerId(rs.getString("FUNDOWNERID"));			    	  
		    	  claimOutput.setPolicyNo(rs.getString("POLICYNO"));			    	 
		    	  claimOutput.setClaimId(rs.getString("CLAIM_ID"));			    	 
		    	  claimOutput.setClaimNo( rs.getString("CLAIMNO"));			    	
		    	  claimOutput.setProductType(rs.getString("PRODUCTTYPE"));			    	  
		    	  claimOutput.setFirstName(rs.getString("FIRSTNAME"));			    	
		    	  claimOutput.setSurName( rs.getString("SURNAME"));			    	 
		    	  claimOutput.setDob(rs.getDate("DATEOFBIRTH"));			    	 
		    	  claimOutput.setGender( rs.getString("GENDER"));			    	
		    	  claimOutput.setReference( rs.getString("REFERENCE"));			    	  
		    	  claimOutput.setScheme(rs.getString("SCHEME"));			    	  
		    	  claimOutput.setWaitperiod(rs.getString("WAITPERIOD"));			    	 
		    	  claimOutput.setBenefitperiod(rs.getString("BENEFITPERIOD"));			    
		    	  claimOutput.setStatusCode( rs.getString("STATUS_CODE"));			    	 
		    	  claimOutput.setStatusDescription(rs.getString("STATUSDESCRIPTION"));			    	
		    	  claimOutput.setDatesubmitted(rs.getDate("DATESUBMITTED"));			    	
		    	  claimOutput.setDisabilitydate(rs.getDate("DISABILITYDATE"));			    	
		    	  claimOutput.setClaimTypeDescription(rs.getString("CLAIMTYPE_DES"));			    	 
		    	  claimOutput.setSuminsured( rs.getString("SUMINSURED"));			    	
		    	  claimOutput.setTotalBenPaid( rs.getString("BENEFITPAID"));			    	
		    	  claimOutput.setDateLastPaid( rs.getDate("DATELASTPAID"));			    	 
		    	  claimOutput.setClaimCauseDescription(rs.getString("CLAIMCAUSE_DES"));			    	  
		    	  claimOutput.setAdmittedDate(rs.getString("ADMITTED_DATE"));			    	 
		    	  claimOutput.setSalaryInsured( rs.getDouble("SALARYINSURED"));			    	  
		    	  claimOutput.setSuperContribution(rs.getDouble("SUPERCONTRIBUTION"));			    	  
		    	  claimOutput.setMonthlyBenefit(rs.getDouble("MONTHLYBENEFIT"));
		    	  claimOutput.setClaimAccessor(rs.getString("CLAIMASSESSOR"));			    	 			    	
		    	  claimOutput.setLastupdateDate(rs.getDate("LASTUPDDATE"));
		    	  claimOutputList.add(claimOutput);
		      		        
		      }
		      //STEP 6: Clean-up environment
		     System.out.println("size>>"+claimOutputList.size());
		      rs.close();
		      stmt.close();
		      conn.close();
	} catch(SQLException se){
	      //Handle errors for JDBC
	      se.printStackTrace();
	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(stmt!=null)
	            stmt.close();
	      }catch(SQLException se2){
	      }// nothing we can do
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }//end finally try
	   }
	   return claimOutputList;
}

@Override
public Document generateToken(String input, String outToken) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Document retriveClientData(String token) {
	// TODO Auto-generated method stub
	return null;
}


}
