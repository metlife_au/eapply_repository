package au.com.metlife.eapply.b2b.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.b2b.model.AuthToken;
import au.com.metlife.eapply.b2b.model.Eappstring;
import au.com.metlife.eapply.b2b.service.B2bService;
import au.com.metlife.eapply.b2b.utility.B2bServiceHelper;
import au.com.metlife.eapply.b2b.utility.BlowfishUtil;
import au.com.metlife.eapply.b2b.xsd.beans.Policy;
import au.com.metlife.eapply.b2b.xsd.beans.Request;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.RetrieveApp;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.helper.CorporateHelper;
import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.utility.DroolsHelper;
import au.com.metlife.eapply.quote.utility.EappRulesCacheProvider;
import au.com.metlife.eapply.quote.utility.QuoteConstants;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;

@RestController
/*@Component*/
public class B2bController {
	private static final Logger log = LoggerFactory.getLogger(B2bController.class);
	public static final int APPLICANT_SIZE = 1;
	public static final int ZERO_SIZE = 0;
	SystemProperty sys;
	 private final B2bService b2bService;
		private EapplyService eapplyService;
		
		 @Autowired
	     public void EapplyServicesController(final EapplyService eapplyService) {
	        this.eapplyService = eapplyService;
	     }
	  @Autowired
	    public B2bController(final B2bService b2bService) {
	        this.b2bService = b2bService;
	    }
	  @Autowired(required = true)
		EappRulesCacheProvider eappRulesCacheProvider;
	
	
	/* @RequestMapping("/createToken")
	  public Eappstring getTokenId(@RequestParam(value="secureString") String secureString) {		
		 Eappstring eappstring =  b2bService.generateToken(secureString);
	     return eappstring;
		 
	 }
	 
	 @RequestMapping(value = "/postb2bdata11", method = RequestMethod.POST)
	  public Eappstring getTokens(@RequestBody String eappstring) {		
		b2bService.generateToken(eappstring.getSecurestring());
		 log.info("eappstring>>{}",eappstring);		 
		 Eappstring eappstring1 =  b2bService.generateToken(eappstring);
	     return eappstring1;
		 
	 }
	 */
	  
	  @RequestMapping(value = "/getauthToken", method = RequestMethod.POST)
		public  ResponseEntity<String> getauthToken(InputStream inputStream) {
		  
		  BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		  String line = null;	
		  String aeskey = null;
		  AuthToken authToken = null;
		  try {
//				while ((line = in.readLine()) != null) {
//					//authToken = b2bService.retriveAuthData(line);
					 line = in.readLine();					
//					 break;
//				}			  
			  authToken = b2bService.retriveAuthData(line);			  
			 
			}catch (IOException e) {
				log.error("Input output error {}",e);
			} catch (JSONException e) {
				log.error("Json error {}",e);
			} catch (Exception e) {
				log.error("Other error {}",e);
			}	
		    if(authToken==null){		    	 
		    	authToken = b2bService.saveAuthToken(line);   	
		    	
		    	return new ResponseEntity<>(authToken.getAuthtoken(), HttpStatus.OK);
		    } else {		    	
		    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		    }
		    
		  
	  }
	  
	 @RequestMapping(value = "/postb2bdata", method = RequestMethod.POST)
		public  ResponseEntity<String> getTokens(InputStream inputStream) {
		   log.info("Data generation start");
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;	
		/*	String string = "";*/				
			 Eappstring eappstring = null;		
			 String validXML = null;
			 B2bServiceHelper b2bServiceHelper = null;
			 String[] splitParameters= null;
			 String fundData = null;
			 String token = null;
			 String fundid = null;
			 Boolean isValidInput = null;
			try {
				while ((line = in.readLine()) != null) {
					/*string += line + "\n";*/
					log.info("line>> {}",line);	
					splitParameters = line.split("##");
					for (int i = 0; i < splitParameters.length; i++) {
						if(i==0){
							fundData= splitParameters[i];
							log.info("fundDate>>> {}",fundData);
						}else{
							token= splitParameters[i];
							log.info("token>> {}",token);
						}						
					}					
					if(line.trim().length()>ZERO_SIZE){
						b2bServiceHelper = new B2bServiceHelper();
						sys = SystemProperty.getInstance();
						if(!"Corporate".equalsIgnoreCase(fundData)) {
						if(fundData!=null && fundData.contains("<fundID>") && fundData.contains("</fundID>")){
							fundid = fundData.substring(fundData.indexOf("<fundID>")+8, fundData.indexOf("</fundID>"));
							validXML= b2bServiceHelper.validateXMLString(fundData, sys.getProperty("xsdpath")+fundid+"_WebService_schema_new.xsd");
							log.info("validXML>> {}",validXML);
							if("valid".equalsIgnoreCase(validXML)){
								//logic that will get removed later								
								isValidInput = B2bServiceHelper.validDateIPDeathTPDCover(B2bServiceHelper.unMarshallingMInput(fundData),fundid);								
								if(isValidInput){
//									if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(fundid)){
//										System.out.println("token >>"+token);
//										Eappstring eAppData   = b2bService.retriveClientData(token);									
//										
//										if (null == eAppData) {
//											eappstring =  b2bService.generateToken(fundData,token);
//										} else {
//											return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//										}
//									} else {
										eappstring =  b2bService.generateToken(fundData,token);
									//}
									
								}else{
									eappstring= null;
								}
								/*eappstring =  b2bService.generateToken(fundData,token);*/
							}
							else{
								eappstring= null;
							}
						}
						else{
							eappstring= null;
						}
}
else {
	
		eappstring =  b2bService.generateToken(fundData,token);
	
}
						
						
					}
					 
				}
			} catch (IOException e) {
				log.error("Input output error {}",e);
			} catch (JSONException e) {
				log.error("Json error {}",e);
			} catch (Exception e) {
				log.error("Other error {}",e);
			}	
		    if(eappstring==null){
		        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		    }
		    
		    log.info("END>> {}",new  java.util.Date());
		    return new ResponseEntity<>(eappstring.getSecureToken(), HttpStatus.OK);
		    

		}	 
	
	 @RequestMapping(value = "/getcustomerdata", method = RequestMethod.POST)
	  public ResponseEntity<Policy> getClientPostData(InputStream inputStream) throws IOException, com.metlife.eapplication.common.JSONException {	
		 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_START);
		 BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		 Request request = null;
		 String line;	
		String string = "";	
		String token;
		
			while ((line = in.readLine()) != null) {
					string += line + "\n";
					JSONObject json = new JSONObject(string);        
					token = (String)json.get("tokenId");
					if(token==null){
						return new ResponseEntity<>(HttpStatus.NO_CONTENT);
					}
					Eappstring eappstring =  b2bService.retriveClientData(token);
					if(eappstring==null){
				        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				    }	
					if(eappstring!=null){
						 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
						 StringBuilder inputDataBuffer=new StringBuilder();
							if(null!=request && request.getPolicy()!=null && request.getPolicy().getPartnerID()!=null){
								inputDataBuffer.append("fundid=");
								inputDataBuffer.append(request.getPolicy().getPartnerID());
								inputDataBuffer.append("$#$");
							}
							if(null!=request && request.getPolicy()!=null && request.getPolicy().getPartnerID()!=null){
								inputDataBuffer.append("inputIdentifier=");
								inputDataBuffer.append(request.getPolicy().getPartnerID());
								inputDataBuffer.append("$#$");
							}
							if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE){
								inputDataBuffer.append("dateofbirth=");
								inputDataBuffer.append(request.getPolicy().getApplicant().get(0).getPersonalDetails().getDateOfBirth());
								inputDataBuffer.append("$#$");
							}
							if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE){
								inputDataBuffer.append("gender=");
								inputDataBuffer.append(request.getPolicy().getApplicant().get(0).getPersonalDetails().getGender());
								inputDataBuffer.append("$#$");
							}
							String logMsg = inputDataBuffer.toString();
							log.info("inputDataBuffer.toString()>> {}",logMsg);
							String insCalInput=BlowfishUtil.encrypt(ApplicationConstants.INS_CALC_KEY, inputDataBuffer.toString());
							log.info("inputDataBuffer.toString()>>  {}",insCalInput);
							request.getPolicy().setCalcEncrpytURL(insCalInput);	
							
							if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE
									&& (null!=request.getFundEmail() && !"".equalsIgnoreCase(request.getFundEmail()))){
									request.getPolicy().getApplicant().get(0).getContactDetails().setFundEmailAddress(request.getFundEmail());
							}				
					 }	
			 }	 
		 
		 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_FINISH);
	      return new ResponseEntity<>(request.getPolicy(), HttpStatus.OK);
		 
	 }
	
	 @RequestMapping("/getcustomerdata")
	  public ResponseEntity<Policy> getClientData(@RequestParam(value="tokenid") String token) {	
		 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_START);
		 Request request = null;
		 Eappstring eappstring =  b2bService.retriveClientData(token);
		 if(eappstring!=null){
			 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
			 StringBuilder inputDataBuffer=new StringBuilder();
				if(null!=request && request.getPolicy()!=null && request.getPolicy().getPartnerID()!=null){
					inputDataBuffer.append("fundid=");
					inputDataBuffer.append(request.getPolicy().getPartnerID());
					inputDataBuffer.append("$#$");
				}
				if(null!=request && request.getPolicy()!=null && request.getPolicy().getPartnerID()!=null){
					inputDataBuffer.append("inputIdentifier=");
					inputDataBuffer.append(request.getPolicy().getPartnerID());
					inputDataBuffer.append("$#$");
				}
				if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE){
					inputDataBuffer.append("dateofbirth=");
					inputDataBuffer.append(request.getPolicy().getApplicant().get(0).getPersonalDetails().getDateOfBirth());
					inputDataBuffer.append("$#$");
				}
				if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE){
					inputDataBuffer.append("gender=");
					inputDataBuffer.append(request.getPolicy().getApplicant().get(0).getPersonalDetails().getGender());
					inputDataBuffer.append("$#$");
				}
				String logMsg = inputDataBuffer.toString();
				log.info("inputDataBuffer.toString():: {}",logMsg);
				String insCalInput=BlowfishUtil.encrypt(ApplicationConstants.INS_CALC_KEY, inputDataBuffer.toString());
				log.info("inputDataBuffer.toString()::: {}",insCalInput);
				request.getPolicy().setCalcEncrpytURL(insCalInput);	
				/*Added for getting fund admin email from input XML*/
				if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE
						&& (null!=request.getFundEmail() && !"".equalsIgnoreCase(request.getFundEmail()))){
						request.getPolicy().getApplicant().get(0).getContactDetails().setFundEmailAddress(request.getFundEmail());					
				}				
		 }	
		 if(eappstring==null){
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		    }	
		 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_FINISH);
	      return new ResponseEntity<>(request.getPolicy(), HttpStatus.OK);
		 
	 }
	 @RequestMapping("/getDefaultUnitsForSpecialCover")
	  public ResponseEntity<Boolean> getDefaultUnitsForSpecialCover(@RequestParam(value="fundCode") String fundCode,
			  @RequestParam(value="anb") int anb , @RequestParam(value="deathUnits") int deathUnits, @RequestParam(value="tpdUnits") int tpdUnits,@RequestParam(value="dateJoined") int dateJoined,@RequestParam(value="firstName") String firstName,@RequestParam(value="clientrefnum") String clientrefnum,@RequestParam(value="lastName") String lastName,@RequestParam(value="dob") String dob) {	
		 log.info("Get default units start");
		 String ruleNameAsPerFund;
		 Map ruleInfoMap;
		 int defaultDeathUnits=0;
		 int defaultTpdUnits = 0;
		 boolean defaultUnitsCheck = false;
		 Boolean appStatus= false;
		 CoverDetailsRuleBO coverDetailsRuleBO=null;
		 String manageType=fundCode;
		 try{
			 //changes for Vicsuper Special offer
			 appStatus=eapplyService.checkDupUWApplication(fundCode, clientrefnum, manageType, dob, firstName, lastName);
			 if(!(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(fundCode)))
			 {
			 coverDetailsRuleBO=new CoverDetailsRuleBO();
			 coverDetailsRuleBO.setFundCode(fundCode);
			 coverDetailsRuleBO.setAge(anb);
			 ruleInfoMap = eappRulesCacheProvider.getRuleMap();
			 ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS;
			  if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS)!=null){
					coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS));
			  }
			 coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			 coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,eappRulesCacheProvider.getFilePath());
			 defaultDeathUnits = coverDetailsRuleBO.getDefaultDeathUnits();
			 defaultTpdUnits = coverDetailsRuleBO.getDefaultTpdUnits();
			 if(defaultDeathUnits == deathUnits && defaultTpdUnits == tpdUnits && dateJoined<181 &&!appStatus){
				 defaultUnitsCheck = true;
			 }else{
				 defaultUnitsCheck = false;
			 }
			 }
			 else
			 {
				 defaultUnitsCheck = !appStatus;
				 
			 }
		 }catch (Exception e) {
			 log.error("Error in getting default units for special cover {}",e);
		 }
		 log.info("Get default units finish");
		 return new ResponseEntity<>(defaultUnitsCheck,HttpStatus.OK);
	 }
	 
	 @RequestMapping("/gethostdata")
	  public ResponseEntity<String> getHostData(@RequestParam(value="tokenid") String token) {	
		 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_START);
		 Eappstring eappstring =  b2bService.retriveClientData(token);	
		 String secureString;
		 if(eappstring==null){
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		    }else{
		    	secureString=eappstring.getSecurestring().replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
		    }
		 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_FINISH);
	      return new ResponseEntity<>( secureString, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/getDefaultUnitsForLifeEvent")
	  public ResponseEntity<Boolean> getDefaultUnitsForLifeEvent(@RequestParam(value="fundCode") String fundCode,
			  @RequestParam(value="anb") int anb , @RequestParam(value="deathUnits") int deathUnits, @RequestParam(value="tpdUnits") int tpdUnits) {	
		 log.info("Get default units start - Life Event");
		 String ruleNameAsPerFund;
		 Map ruleInfoMap;
		 int defaultDeathUnits=0;
		 int defaultTpdUnits = 0;
		 boolean defaultUnitsCheck = false;
		 CoverDetailsRuleBO coverDetailsRuleBO=null;
		 try{
			 coverDetailsRuleBO=new CoverDetailsRuleBO();
			 coverDetailsRuleBO.setFundCode(fundCode);
			 coverDetailsRuleBO.setAge(anb);
			 ruleInfoMap = eappRulesCacheProvider.getRuleMap();
			 ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS;
			  if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS)!=null){
					coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEFAULT_UNITS));
			  }
			 coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			 coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,eappRulesCacheProvider.getFilePath());
			 defaultDeathUnits = coverDetailsRuleBO.getDefaultDeathUnits();
			 defaultTpdUnits = coverDetailsRuleBO.getDefaultTpdUnits();
			 if(defaultDeathUnits == deathUnits && defaultTpdUnits == tpdUnits){
				 defaultUnitsCheck = true;
			 }else{
				 defaultUnitsCheck = false;
			 }
		 }catch (Exception e) {
			log.error("Error in getting default units for life event {}",e);
		 }
		 log.info("Get default units finish - Life Event");
		 return new ResponseEntity<>(defaultUnitsCheck,HttpStatus.OK);
	 }
	 
@RequestMapping("/checkSaveAppsNonMem")
 public ResponseEntity<String> retireveApplicationNonMem(@RequestParam(value="fundCode") String fundCode,@RequestParam(value="clientRefNo") String clientRefNo) {	
	 	 log.info("Retrive application start");
	 	 List<RetrieveApp> retrieveAppList=null;
	 	 String savedApps = "No";
	 	 try{
	 		 if(fundCode!=null && clientRefNo!=null){
	 			 retrieveAppList=new ArrayList<>();
	 			 List<Eapplication> savedApplications= eapplyService.retrieveSavedApplications(fundCode, clientRefNo,null);
	 			if(savedApplications!= null && !savedApplications.isEmpty())
	 			{
	 				savedApps = "Yes";
	 			}
	 		 }
	 	 }catch(Exception e){
	 		log.error("Error in retrive application: {}",e);
	 	 }
	   if(retrieveAppList==null){
	       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	   }
	   log.info("Retrive application finish");
	   return new ResponseEntity<>(savedApps, HttpStatus.OK);
	 }
@GetMapping("/getInsurancecustomerdata")
public ResponseEntity<String> getInsuranceClientData(@RequestParam(value="token") String token) throws com.metlife.eapplication.common.JSONException, JsonProcessingException {	
	 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_START);
	 Request request = null;
	 Eappstring eappstring =  b2bService.retriveClientData(token);
	 if(eappstring!=null){
		 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
		 StringBuilder inputDataBuffer=new StringBuilder();
			if(null!=request && request.getPolicy()!=null && request.getPolicy().getPartnerID()!=null){
				inputDataBuffer.append("fundid=");
				inputDataBuffer.append(request.getPolicy().getPartnerID());
				inputDataBuffer.append("$#$");
			}
			if(null!=request && request.getPolicy()!=null && request.getPolicy().getPartnerID()!=null){
				inputDataBuffer.append("inputIdentifier=");
				inputDataBuffer.append(request.getPolicy().getPartnerID());
				inputDataBuffer.append("$#$");
			}
			if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE){
				inputDataBuffer.append("dateofbirth=");
				inputDataBuffer.append(request.getPolicy().getApplicant().get(0).getPersonalDetails().getDateOfBirth());
				inputDataBuffer.append("$#$");
			}
			if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE){
				inputDataBuffer.append("gender=");
				inputDataBuffer.append(request.getPolicy().getApplicant().get(0).getPersonalDetails().getGender());
				inputDataBuffer.append("$#$");
			}
			String logMsg = inputDataBuffer.toString();
			log.info("inputDataBuffer.toString():: {}",logMsg);
			String insCalInput=BlowfishUtil.encrypt(ApplicationConstants.INS_CALC_KEY, inputDataBuffer.toString());
			log.info("inputDataBuffer.toString()::: {}",insCalInput);
			request.getPolicy().setCalcEncrpytURL(insCalInput);	
			/*Added for getting fund admin email from input XML*/
			if(null!=request && request.getPolicy()!=null && request.getPolicy().getApplicant()!=null && request.getPolicy().getApplicant().size()==APPLICANT_SIZE
					&& (null!=request.getFundEmail() && !"".equalsIgnoreCase(request.getFundEmail()))){
					request.getPolicy().getApplicant().get(0).getContactDetails().setFundEmailAddress(request.getFundEmail());					
			}				
	 }
	 
     ObjectMapper mapper = new ObjectMapper();
     String details= mapper.writeValueAsString(request.getPolicy());
	
	 if(eappstring==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }	
	 log.info(MetlifeInstitutionalConstants.CLIENT_DATA_GENERATION_FINISH);
    return new ResponseEntity<>(details, HttpStatus.OK);
	 
}
}