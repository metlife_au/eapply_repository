package au.com.metlife.eapply.ct.exception;



public class MetlifeException extends Exception {
    

    private static final long serialVersionUID = 325854053292643504L;
    
    /**
     * The backend system error code.
     */
    private String errorCode;
    
    /**
     * The code that maps to an error message to be presented to the end user.
     */
    private String msgErrorCode;

	/**
	 * The system that generates the error.
	 */
    private String systemId;

    /**
     * The error message returns by the source system that encountered the error
     */
    private String sourceSystemErrorMsg;
    
    private Throwable exception;
  
	/**
     * Default constructor
     */
    public MetlifeException()
    {
        super();
    }
    
    /**
     * Constructs a new exception with the specified detail message. The cause is not initialized, 
     * and may subsequently be initialized by a call to initCause method.
     * 
     * @param msg   the detail error message
     */
    public MetlifeException(String msg)
    {
        super(msg);
    }
    
    /**
     * Constructs a new exception with the specified detail message and cause. Note that the detail 
     * message associated with cause is not automatically incorporated in this exception's detail message.
     * 
     * @param msg   the detail error message
     * @param e     the cause (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public MetlifeException(String msg, Throwable e)
    {
        super(msg, e);
    }
    
	/**
	 * Constructs a new exception with the specified cause and a detail message of 
	 * (cause==null ? null : cause.toString()) 
	 * 
	 * @param e	execution stack
	 */
    public MetlifeException(Throwable e)
    {
        super(e);
    }

    /**
     * Gets the backend system error code.
     * 
     * @return the backend system error code
     */
    public String getErrorCode()
    {
        return errorCode;
    }

    /**
     * Sets the backend system error code.
     * 
     * @param errorCode     the backend system error code
     */
    public void setErrorCode(String errorCode) 
    {
        this.errorCode = errorCode;
    }

    /**
     * Gets the error message code.
     * 
     * @return the error message
     */
    public String getMsgErrorCode() 
    {
        return msgErrorCode;
    }

    /**
     * Sets the error message code.
     * 
     * @param msgErrorCode      the error message code
     */
    public void setMsgErrorCode(String msgErrorCode) 
    {
        this.msgErrorCode = msgErrorCode;
    }

	/**
	 * Gets the system identifier of the system that encountered the error.
	 * 
	 * @return the system identifier
	 */
	public String getSystemId()
	{
		return systemId;
	}

	/**
	 * Sets the system identifier of the system that encountered the error.
	 * 
	 * @param systemId	the system identifier
	 */
	public void setSystemId(String systemId)
	{
		this.systemId = systemId;
	}

	/**
	 * Gets the error message returns by the source system that encountered the error.
	 * 
	 * @return the error message returns by the source system
	 */
	public String getSourceSystemErrorMsg()
	{
		return sourceSystemErrorMsg;
	}

	/**
	 * Sets the error message returns by the source system that encountered the error.
	 * 
	 * @param sourceSystemErrorMsg	the error message returns by the source system
	 */
	public void setSourceSystemErrorMsg(String sourceSystemErrorMsg)
	{
		this.sourceSystemErrorMsg = sourceSystemErrorMsg;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}
	
	
}

