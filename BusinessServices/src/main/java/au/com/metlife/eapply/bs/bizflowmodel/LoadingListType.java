package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadingListType")


@XmlRootElement
public class LoadingListType {	
	
	private String LoadingType;	
	
	private LoadingValue LoadingValue ;	

	public LoadingValue getLoadingValue() {
		return LoadingValue;
	}
	public void setLoadingValue(LoadingValue loadingValue) {
		LoadingValue = loadingValue;
	}
	public String getLoadingType() {
		return LoadingType;
	}
	public void setLoadingType(String loadingType) {
		LoadingType = loadingType;
	}
	
	
	

	
	
	
}
