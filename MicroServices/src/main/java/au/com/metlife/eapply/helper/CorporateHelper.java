package au.com.metlife.eapply.helper;

import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.repositories.model.MemberFileUpload;

public class CorporateHelper {
	private CorporateHelper (){
		
	}
    
    public static JSONObject LoginJson(MemberFileUpload fileupload) throws JSONException {
           JSONObject  fileuploadJson = new JSONObject();
         
           String[] uploadSplit = fileupload.toString().split(",");
             for (String s:uploadSplit ) {
             String[] jsonSplit=s.split("=");
             if(jsonSplit.length>1) {
                 fileuploadJson.put(jsonSplit[0].toString().trim(),jsonSplit[1].toString());
             }
             else {
                 fileuploadJson.put(jsonSplit[0].toString().trim(),"");
             }
             }        
           return fileuploadJson;
    }
    /****
     * This method is used to Null Check a String Value and to avoid Null
     * pointer exception being thrown on runtime
     * 
     * @param strValue
     * @return
     */
    public static boolean isNullOrEmpty(final String strValue) {
      boolean isNull = false;
      if (strValue == null || strValue.length() == 0) {
        isNull = true;
      }
      return isNull;
    } 

}
