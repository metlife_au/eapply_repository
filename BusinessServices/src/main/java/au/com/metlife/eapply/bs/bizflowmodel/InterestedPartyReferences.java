package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterestedPartyReferences")

@XmlRootElement
public class InterestedPartyReferences {	
	

	private PersonReferences  PersonReferences ;

	public PersonReferences getPersonReferences() {
		return PersonReferences;
	}

	public void setPersonReferences(PersonReferences personReferences) {
		PersonReferences = personReferences;
	}

	
	
	

	
	
}
