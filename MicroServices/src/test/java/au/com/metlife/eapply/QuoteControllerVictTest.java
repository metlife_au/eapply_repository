package au.com.metlife.eapply;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.com.metlife.eapply.quote.controller.QuoteController;
import au.com.metlife.eapply.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
public class QuoteControllerVictTest {

	@InjectMocks
	private QuoteController controller;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;
	String testData1 = null;
	String fileName1 = null;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	String flag = null;

	// valid fundCode for VicSuper
	@Test
	public void getProductConfigVictTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=VICT&memberType=Employee saver&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("VICT")).andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("Employee saver"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").exists())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").value("5000000"))
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").exists())
				.andExpect(jsonPath("$.[0].ipMaxAmount").value("30000"))

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").exists())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").value("150000"))
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").exists())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").value("500001"))
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value("500.0"));

	}

	// Null fundCode for VicSuper
	@Test
	public void NullFundCodeforProductConfig() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getProductConfig?memberType&manageType&fundCode")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

	}

	// invalid fundCode for VicSuper
	@Test
	public void getProductConfigInvalidfundCodeVICTTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=VICT1&memberType=Employee saver&manageType=CCOVER")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
		.andExpect(jsonPath("$.[0].productCode").value("VICT1")).andExpect(jsonPath("$.[0].memberType").exists())
		.andExpect(jsonPath("$.[0].memberType").value("Employee saver"))
		.andExpect(jsonPath("$.[0].manageType").exists())
		.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

		.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
		.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
		.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
		.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
		.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
		.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
		.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

		.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
		.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
		.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
		.andExpect(jsonPath("$.[0].tpdMaxAmount").doesNotExist())
		.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
		.andExpect(jsonPath("$.[0].ipMaxAmount").doesNotExist())

		.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
		.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
		.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
		.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").doesNotExist())
		.andExpect(jsonPath("$.[0].annualSalMaxLimit").doesNotExist())
		.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
		.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(0));

	}



	// for /calculateAll valid FundCode for VicSuper
	@Test
	public void savecalculateAll() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllVict.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(36.28))
				.andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(89.26)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpUnitised"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}
	
	@Test
	public void savecalculateAll1() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteCtrlCalculateAllVictIp.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(36.28))
				.andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(89.26)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpUnitised"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(5000))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(4.47));
	}

	// for /calculateAll Invalid FundCode
	@Test
	public void savecalculateAllInvalidFundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteCtrlCalculateAllInvalidfundCodeVict.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpUnitised"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for /calculateAll null FundCode
	@Test
	public void savecalculateAllEmptyRequestBody() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculateAllNullRequestBody.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[2].coverType").doesNotExist())

				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist());
	}

	// for /calculateAll null FundCode
	@Test
	public void savecalculateAllNullFundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllNullFundCode.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").doesNotExist())

				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}

	// for /calculateAll Invalid CoverType for VicSuper
	@Test
	public void savecalculateAllInvalidCoverType() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteCtrlCalculateAllInvaliCoverTypeVict.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists())
				.andExpect(jsonPath("$.[0].coverType").value("DcFixed1"))
				.andExpect(jsonPath("$.[0].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[0].cost").doesNotExist())

				.andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed1"))
				.andExpect(jsonPath("$.[1].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[1].cost").doesNotExist())

				.andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpUnitised1"))
				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}


	// valid fundCode for VicSuper for calculateTransfer
	@Test

	public void savecalculateTransferForVict() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteCtrlCalculateAllTransferVict.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateTransfer").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(510000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(2.47)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TpdFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(510000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(3.35)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(1000))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0.89));
	}



	// for /getIndustryList

	// valid fundCode for VicSuper getIndustryListVict
	@Test
	public void getIndustryListVictTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode=HOST").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].key").exists())
				.andExpect(jsonPath("$.[0].key").value("001")).andExpect(jsonPath("$.[0].value").exists())
				.andExpect(jsonPath("$.[0].value").value("Advertising & Marketing"))

				.andExpect(jsonPath("$.[1].key").exists()).andExpect(jsonPath("$.[1].key").value("002"))
				.andExpect(jsonPath("$.[1].value").exists())
				.andExpect(jsonPath("$.[1].value").value("Agriculture, Animals, Forestry & Timber"))

				.andExpect(jsonPath("$.[2].key").exists()).andExpect(jsonPath("$.[2].key").value("003"))
				.andExpect(jsonPath("$.[2].value").exists())
				.andExpect(jsonPath("$.[2].value").value("Art & Entertainment"))

				.andExpect(jsonPath("$.[3].key").exists()).andExpect(jsonPath("$.[3].key").value("004"))
				.andExpect(jsonPath("$.[3].value").exists()).andExpect(jsonPath("$.[3].value").value("Aviation"))

				.andExpect(jsonPath("$.[4].key").exists()).andExpect(jsonPath("$.[4].key").value("005"))
				.andExpect(jsonPath("$.[4].value").exists())
				.andExpect(jsonPath("$.[4].value").value("Building, Furniture, Architecture & Construction"))

				.andExpect(jsonPath("$.[5].key").exists()).andExpect(jsonPath("$.[5].key").value("006"))
				.andExpect(jsonPath("$.[5].value").exists())
				.andExpect(jsonPath("$.[5].value").value("Education, Training & Recruitment"))

				.andExpect(jsonPath("$.[6].key").exists()).andExpect(jsonPath("$.[6].key").value("007"))
				.andExpect(jsonPath("$.[6].value").exists())
				.andExpect(jsonPath("$.[6].value").value("Finance & Banking"))

				.andExpect(jsonPath("$.[7].key").exists()).andExpect(jsonPath("$.[7].key").value("008"))
				.andExpect(jsonPath("$.[7].value").exists())
				.andExpect(jsonPath("$.[7].value").value("Fisheries & Marine/Shipping"))

				.andExpect(jsonPath("$.[8].key").exists()).andExpect(jsonPath("$.[8].key").value("009"))
				.andExpect(jsonPath("$.[8].value").exists())
				.andExpect(jsonPath("$.[8].value").value("Government or Community Services"))

				.andExpect(jsonPath("$.[9].key").exists()).andExpect(jsonPath("$.[9].key").value("010"))
				.andExpect(jsonPath("$.[9].value").exists())
				.andExpect(jsonPath("$.[9].value").value("Hospitality & Tourism"))

				.andExpect(jsonPath("$.[10].key").exists()).andExpect(jsonPath("$.[10].key").value("011"))
				.andExpect(jsonPath("$.[10].value").exists())
				.andExpect(jsonPath("$.[10].value").value("Information Technology"))

				.andExpect(jsonPath("$.[11].key").exists()).andExpect(jsonPath("$.[11].key").value("012"))
				.andExpect(jsonPath("$.[11].value").exists()).andExpect(jsonPath("$.[11].value").value("Legal"))

				.andExpect(jsonPath("$.[12].key").exists()).andExpect(jsonPath("$.[12].key").value("013"))
				.andExpect(jsonPath("$.[12].value").exists()).andExpect(jsonPath("$.[12].value").value("Manufacturing"))

				.andExpect(jsonPath("$.[13].key").exists()).andExpect(jsonPath("$.[13].key").value("014"))
				.andExpect(jsonPath("$.[13].value").exists())
				.andExpect(jsonPath("$.[13].value").value("Media & Telecommunications"))

				.andExpect(jsonPath("$.[14].key").exists()).andExpect(jsonPath("$.[14].key").value("015"))
				.andExpect(jsonPath("$.[14].value").exists())
				.andExpect(jsonPath("$.[14].value").value("Medical & Emergency Services"))

				.andExpect(jsonPath("$.[15].key").exists()).andExpect(jsonPath("$.[15].key").value("016"))
				.andExpect(jsonPath("$.[15].value").exists())
				.andExpect(jsonPath("$.[15].value").value("Mining & Energy"))

				.andExpect(jsonPath("$.[16].key").exists()).andExpect(jsonPath("$.[16].key").value("017"))
				.andExpect(jsonPath("$.[16].value").exists()).andExpect(jsonPath("$.[16].value").value("Motor Trade"))

				.andExpect(jsonPath("$.[17].key").exists()).andExpect(jsonPath("$.[17].key").value("018"))
				.andExpect(jsonPath("$.[17].value").exists())
				.andExpect(jsonPath("$.[17].value").value("Police, Defence & Security"))

				.andExpect(jsonPath("$.[18].key").exists()).andExpect(jsonPath("$.[18].key").value("019"))
				.andExpect(jsonPath("$.[18].value").exists())
				.andExpect(jsonPath("$.[18].value").value("Real Estate & Property Management"))

				.andExpect(jsonPath("$.[19].key").exists()).andExpect(jsonPath("$.[19].key").value("020"))
				.andExpect(jsonPath("$.[19].value").exists())
				.andExpect(jsonPath("$.[19].value").value("Retail & Sales"))

				.andExpect(jsonPath("$.[20].key").exists()).andExpect(jsonPath("$.[20].key").value("021"))
				.andExpect(jsonPath("$.[20].value").exists())
				.andExpect(jsonPath("$.[20].value").value("Science & Research"))

				.andExpect(jsonPath("$.[21].key").exists()).andExpect(jsonPath("$.[21].key").value("022"))
				.andExpect(jsonPath("$.[21].value").exists())
				.andExpect(jsonPath("$.[21].value").value("Sport & Fitness"))

				.andExpect(jsonPath("$.[22].key").exists()).andExpect(jsonPath("$.[22].key").value("023"))
				.andExpect(jsonPath("$.[22].value").exists()).andExpect(jsonPath("$.[22].value").value("Transport"))

				.andExpect(jsonPath("$.[23].key").exists()).andExpect(jsonPath("$.[23].key").value("024"))
				.andExpect(jsonPath("$.[23].value").exists()).andExpect(jsonPath("$.[23].value").value("Unemployed"))

				.andExpect(jsonPath("$.[24].key").exists()).andExpect(jsonPath("$.[24].key").value("025"))
				.andExpect(jsonPath("$.[24].value").exists()).andExpect(jsonPath("$.[24].value").value("Home Duties"))

				;

	}

	// Invalid fundCode
	@Test
	public void getIndustryListVictInvalidTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getIndustryList?fundCode=VICT1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$").isEmpty());

	}

	// null fundCode
	@Test
	public void getIndustryListVictNullTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())

				;

	}


	// for/getNewOccupationList

	// valid fundCode for VicSuper and occName
	@Test
	public void getNewOccupationListVict() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?fundId=HOST&occName=001:Business Analyst/Consultant")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].id").exists())
				.andExpect(jsonPath("$.[0].id").value(4362))

				.andExpect(jsonPath("$.[0].deathfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].deathfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].deathunitcategeory").exists())
				.andExpect(jsonPath("$.[0].deathunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].ipfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipunitcategeory").exists())
				.andExpect(jsonPath("$.[0].ipunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].specialrisk").exists()).andExpect(jsonPath("$.[0].specialrisk").isEmpty())

				.andExpect(jsonPath("$.[0].tpdfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].tpdunitcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].fundid").exists()).andExpect(jsonPath("$.[0].fundid").value("HOST"))

				.andExpect(jsonPath("$.[0].occname").exists())
				.andExpect(jsonPath("$.[0].occname").value("001:Business Analyst/Consultant"))

				;

	}

	// Invalid fundCode for VicSuper and Invalid occName

	@Test
	public void getNewOccupationListVictInvalidFun() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getNewOccupationList?fundId=VICT1&occName=001:Business Analyst/Consultant111")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$").isEmpty())
				;

	}

	// null fundCode
	@Test
	public void getNewOccupationListNullFun() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?occName&fundId").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())

				;

	}


	@Test
	public void eapplyInitiateVict() throws Exception {
		String input = "{\"fund\":\"VICT\",\"mode\":\"change\",\"name\":\"TESTVICT LASTVICT\",\"age\":31,\"appnumber\":1522302051923,\"deathAmt\":600000,\"tpdAmt\":0,\"ipAmt\":0,\"waitingPeriod\":\"90 Days\",\"benefitPeriod\":\"2 Years\",\"memberType\":\"INDUSTRY OCCUPATION\",\"gender\":\"male\",\"clientname\":\"metaus\",\"industryOcc\":\"001:Business Analyst/Consultant\",\"country\":\"Australia\",\"salary\":\"200000\",\"fifteenHr\":\"Yes\",\"lastName\":\"LASTVICT\",\"firstName\":\"TESTVICT\",\"dob\":\"12/11/1986\",\"existingTerm\":true}";
		mockMvc.perform(post("/initiate").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}





}
