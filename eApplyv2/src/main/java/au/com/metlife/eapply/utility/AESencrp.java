package au.com.metlife.eapply.utility;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

public class AESencrp {
	private static final Logger log = LoggerFactory.getLogger(AESencrp.class);
     private static final String ALGO = "AES";
     private final static String characterEncoding = "UTF-8";
     private final static String cipherTransformation = "AES/CBC/PKCS5Padding";
     private final static String aesEncryptionAlgorithm = "AES";

    public static void main(String asrgs[]){
    	try{

    		String tt=encrypt("testdata","2acu4m4j3uhdea2f");

    	log.info(tt);
    	String dt=decrypt("7IyVL13CTqFJEfwPTBPR4k/XSWmJK732ZuJ8fhZpsYOvtcQw2rzVlEjm5+TpqB4apEPMuyag97lV qcmu1zDs6K/W1tID+Y1XBNCw+Rg651IC9aiIy7MlENCak1eDbKrs3tNI8xTCgeVYcWSpAnBs5jdc 2Y+rue+gj8XCdrS0wvjfN3WKV2T/ck7UOrk8hmmMeMH2NluDc3ACECQs9U7Hp3KB9rScre7rlwNp TJWO18TQtASsjx7RDN4TUHt+IIKzktrM8mBqpT8Xce+ARmQdZrEuE8WJVzSzKGGE1LMDEvDCNmzM 7EI3KucCoQrKITjQht8l2wY/i9DZbPQrYvA/LszjQFu+zTKTW4S1or6wwy5Sffi3z+arWufRWAQD Vcj9LGEDb6V7FKhAdSVCyVGptR14SHIfI069hPBVsHFUS21TMVbFGLMTRRylpjkFvg6K/xjBNcCW uxLt+tAOb29mtpzIDs/PvtST6VFu2d0AC6+eWT2Vw0Ua6fVYzkTZ1Ol/Uv4K8o/eQnxBQ5S92U4Z /CMN3A4Lr8ZX5/dxuXo0UVsfiATznd88DERxS0ziKOJjf+oBqXVtujSIHIONKUHwbw8pzsj4EUBS bGEsnnaWs2Ws/pCxqJL9MnOkO4UwoapsUfIqnHl2Pi7VrBf2ySfvfNZFD9Badh3xzrYAuYk1RKm+ F+fate4WxRotvm9OHWDc13dewZB68qH/j9WEMAEh2bJCThRfaeYma2ZXUYGhsmyGePnUXahx2DQ6 NiGBD7kUvKSE2b9VA4ajaNLDzdRPGaXialJuMuW0zS+GbER8skRFDnmetZghfG2jf3bmde81Is3H 3PhGeMpN+ndrJiM7Y7Jk4u9X62+Dc1dMzdJyaon659YElXmCAHkGi+9xMv3sjW+9a61MLaYhLOV+ 0OuqZSyO4QkSqVZ+z0e8dMxBXSftsvG1OL9LkPTDfh4Y4OU1sAETBBibyH18dnnhZkE3iRnLScwB BWtezbInwuoeyzibL6yBkCkfhNzY4RgYKa+I67b+TlvV5VN5cipniGN6nalNsfBHh7m3N5uq3BqJ ahlUD5aKd1dt/NoKIAeON6yh0fb/vts7yh5HBJFg3E+q+uC6atjfybm+cFq+bdiUuG3AGRJfcaGp Q9XH6IvMvV5ht7Dg+XvQmyo4xrr/8kZ6T7p2WhH30onBSxT+Nvgey0oNOEPxsNgaalM91uCWuX+D XGxUs6kX4OPjqOdv5qqPMDS5Hzolr36rKOB16J0YDb5Qh2P6aiD5GvEJFqsuMLydvQAJYhd2M2+r WiquZXzed8JT41W04JLTSOasPZ5+37ETjjkYhZojlvsPHB4QkFyrhYkEN+Lxb4woOpW52xxmDIqJ UAFLfwz60BBuAo+f/VQFSHFN/Xar9HrBmPwEAA6bYzheeMKOOzhKJK2j5LLVes/mzKBCARShnSnz CyXlRZn7O2FNWfSQT2PEusfp+4agZdE0YpqADPjQixDGyvhf926F5CrlBlgfXOutQQeXJ53AGktd 5LWYcXqyfsWix8xgvGYHxGzESN3AMi0Z4lz9WzKZxwYTkxJqwbCmUogUAqlYDRMi3FW6gJMLyl9l rd1ri++IcMQtqo/daN+1qpAtIjSZJxkfGtQIyaoVPf+n+vyEGNarUrkR84ZZbBmZFNLrjPVNiQUY pr16GEYbkTp3tp37g27SmgChpDpw2VgAbH309k25zYQOpMZh2XZ4snHFoKcg77CAVr1SZkRfesai D+B+e+m2rJidxDTxPQfr54Huq7NyifWpNgqsAY0y8w8tS2FSRthHwLOfBm7vZprS5Jm85V3iL92Z ELTOxqQsa4pk5EMjDu3fA8uzvjO+T0HVqqnwbte9dJujR7DIOOSOF1qA0w1/WaG7oWuopu8Zc91g rP/lari0SjAManJhjN5A44yg0kL0EIHdnZswHm5cLnRmYpfykYAsKDr94bwb7uY1QUV6k1uQ50Kf mNw0o7iMwoLRr4H/+6OyTHrOIT42a7y1xJXmChcQSauvrcUkY86xM+2ZUN1GerHEEWKurrHk9QTx HgFoSJQv0vB5tx9XpxvKsrD6A78Esw8vl+GlKF7JDp1G+pDnXb/LycLr3hGEU2YB2bkklE+HhvFy 8/Ca6UVXySH/QRjFLs2/NvGf1TT8C9xN8jffDfA5QeTO9oTj6RbiunwA+94+F9hzbDhtJNMTKTa4 B5DQBvYDXkznFNw9XORQ2rZpRdMq9ppl6qCl2js01yXGVpQxUfybnmzcrlgkb02dai+S43fsKEjI O4DrfXT/32e3T4eEQCbb+QAaHhswrkmX9n2JoY3t8btyLTxwJTBSw+Q81kdajbZUWrCfe5jtK6BT R/Y1SCHykkI/M+fE/wHQHVTsourc4sAWmANYphBLSYqeswQngODVmZ0Gm+vIGcvfwlG8pwgrSvrL eYTPywU6d57Rj548nBBKKShZJLrNs3NigAN8shGpkbFJjsLx2n4AhZE/3Yt4NQuws2VR5ERS+lN9 2XGzN8y9A2cg/9OWxe/NP0LzVFPJQfyorL4vtMs5yITLfB5R19mEskSfCjXZg6EGExcElFeDjMv8 L0MpfyOBWinJKRm3Nl7XsmQqgSO/OWVtBRJZQcc1NmLBgURBHHWQ9ybiYCN0S8Pq3IKnwPRFVKOi hegBBbV9GlFpiB3hPwAAVTo6gAPhM4+Zc1z5sQ86jBVVMxlQ9WmmKlzwok1D9xsfMPnZFMA=","SxKKmJLwpnd/rYe4pSei43ah1wsmhmv5rW7BBU9qNYRuP5qO1HMWDHWC05T4+gUKqMIQ3cxln0k48mVQ5HBKf4tq10X5p+Yqs3Sgr1ZAuPn39MThGMMxVBd5fuTmVXu+smwcK+cVBeUTDKlC8pXHDh29+KTbJaJWQHBuqsqTt0XN1JGlzpDmSVfKaYGla03CobchlanKfTABYVga3jrVdFOCqfVWwmIpT3AXkofRCgF7tk896JYP8Gcsf3xNgbmQVS2GsRDO3QxNt32nTul9G1MmZNiJhsdL1RE7nDNj6TKzggDz3lNzJ/Klfxs8iRAf5YFHTI8asEFmiJfP8K662A==");
    	log.info(dt);
    	log.info(decrypt("BTuNEbwkF+IA6t8RwwnzmQ==","ppvoj5v3eurbr4dp"));
    	}catch(Exception e){
    		log.error("Error in main"+e);
    	}
    }



    public static String encrypt(String Data,String ek) throws Exception {
    	log.info("Encryption start");
        Key key = generateKey(ek);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encVal);
        log.info("Encryption finish");
        return encryptedValue;
    }

    public static String decrypt(String encryptedData,String ek) throws Exception {
        log.info("Decryption start");
        Key key = generateKey(ek);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        log.info("Decryption finish");
        return decryptedValue;
    }
    private static Key generateKey(String keyValue) throws Exception {
    	log.info("Generate key start");
        Key key = new SecretKeySpec(keyValue.getBytes(), ALGO);
        log.info("Generate key finish");
        return key;
    }
    
    public static String aeisdecrypt(String encryptedText, String key) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException{
        //byte[] cipheredBytes = Base64.decode(encryptedText, Base64..DEFAULT);
        //byte[] cipheredBytes = Base64.getDecoder().decode(encryptedText);
        byte[] cipheredBytes = new BASE64Decoder().decodeBuffer(encryptedText);
        
        byte[] keyBytes = getKeyBytes(key);
        return new String(decrypt(cipheredBytes, keyBytes, keyBytes), characterEncoding);
    }
    
    private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
        byte[] keyBytes= new byte[16];
        byte[] parameterKeyBytes= key.getBytes(characterEncoding);
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }
    
    public static  byte[] decrypt(byte[] cipherText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }

}

