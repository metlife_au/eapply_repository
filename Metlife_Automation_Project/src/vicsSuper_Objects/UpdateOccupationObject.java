/*package VicsSuper_Objects;

public class UpdateOccupationObject {

}*/
package vicsSuper_Objects;

import org.openqa.selenium.By;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class UpdateOccupationObject extends ReusableLibrary {

	public UpdateOccupationObject(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	// ************************************************************************************************************************
		// -----------------------------------------Personal Details
		// ************************************************************************************************************************
		public static By obj_PersonalDetails_dutyOfDisclosure = By.xpath(".//input[@id='dodlabelCheck']/parent::*");
		public static By obj_PersonalDetails_privacyStatement = By.xpath(".//input[@id='privacylabelCheck']/parent::*");
		// ContactDetails
		public static By obj_PersonalDetails_email = By.name("workRatingEmail");
		public static By obj_PersonalDetails_contactType = By.xpath("//select[@ng-model='preferredContactType']");
		public static By obj_PersonalDetails_phNO = By.name("wrkRatingPrefContactNo");
		public static By obj_PersonalDetails_Morning = By.xpath("(.//input[@name='workRatingPrefTime']/parent::*)[1]");
		public static By obj_PersonalDetails_Afternoon = By.xpath("(.//input[@name='workRatingPrefTime']/parent::*)[2]");
		public static By obj_PersonalDetails_Male = By.xpath("(.//input[@name='gender']/parent::*)[1]");
		public static By obj_PersonalDetails_Female = By.xpath("(.//input[@name='gender']/parent::*)[2]");
		public static By obj_Continue_ContactDetail = By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']");


		// OccupationDetails
		public static By obj_PersonalDetails_FourteenHrsYes = By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*");
		public static By obj_PersonalDetails_FourteenHrsNo = By.xpath("//input[@name='fifteenHrsQuestion']/parent::*");
		public static By obj_PersonalDetails_CitizenYes = By.xpath("(//input[@name='areyouperCitzWrkUpdateQuestion']/parent::*)[1]");
		public static By obj_PersonalDetails_CitizenNo = By.xpath("(//input[@name='areyouperCitzWrkUpdateQuestion']/parent::*)[2]");

		public static By obj_PersonalDetails_AddQuest1aYes = By.xpath("(//input[@name='occRating1a']/parent::*)[1]");
		public static By obj_PersonalDetails_AddQuest1aNo = By.xpath("(//input[@name='occRating1a']/parent::*)[2]");
		public static By obj_PersonalDetails_AddQuest1bYes = By.xpath("(//input[@name='occRating1b']/parent::*)[1]");
		public static By obj_PersonalDetails_AddQuest1bNo = By.xpath("(//input[@name='occRating1b']/parent::*)[2]");
		public static By obj_PersonalDetails_AddQuest2Yes = By.xpath("(//input[@name='occRating2']/parent::*)[1]");
		public static By obj_PersonalDetails_AddQuest2No = By.xpath("(//input[@name='occRating2']/parent::*)[2]");
		public static By obj_PersonalDetails_AnnualSal = By.name("workRatingAnnualSal");

		//Footer
		public static By obj_PersonalDetails_Save = By.xpath("//*[@ng-click='saveQuote()']");
		public static By obj_PersonalDetails_Continue = By.xpath("//button[@ng-click='workRatingFormSubmit(workRatingOccupForm);']");

		public static By obj_SaveReferenceNo = By.xpath("//*[contains(text(), 'Your application reference no')]");
		public static By obj_SaveClose = By.xpath("//button[@ng-click='closeThisDialog()']");
		public static By obj_PersonalDetails_OpenSavedApplication = By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']");
		public static By obj_PersonalDetails_CancelMsg = By.id("decCancelMsg_text");
		public static By obj_PersonalDetails_DecisionContinue = By.xpath("//button[@ng-click='goTo()']");

		// ************************************************************************************************************************
		// -----------------------------------------Confirmation Details
		// ************************************************************************************************************************

		// Personal Details
		public static By obj_Confirmation_FirstName = By.xpath("//*[text()='First name']//following::label");
		public static By obj_Confirmation_LastName = By.xpath("//*[text()='Last name']//following::label");
		public static By obj_Confirmation_Gender = By.xpath("//*[text()='Gender']//following::label");
		public static By obj_Confirmation_ContactType = By.xpath("//*[text()='Preferred contact type']//following::label");
		public static By obj_Confirmation_ContactNo = By.xpath("//*[text()='Preferred contact number ']//following::label");
		public static By obj_Confirmation_Email = By.xpath("//*[text()='Email address']//following::label");
		public static By obj_Confirmation_DOB = By.xpath("//*[text()='Date of birth']//following::label");
		public static By obj_Confirmation_Time = By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label");
		public static By obj_Confirmation_HoursWork = By.xpath("//*[text()='Do you work 14 or more hours per week?']//following::label");
		public static By obj_Confirmation_Citizen = By.xpath("//*[contains(text(),'Are you a citizen or permanent resident of Australia')]//following::label");
		public static By obj_Confirmation_AddQuest1a = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[1]");
		public static By obj_Confirmation_AddQuest1b = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[3]");
		public static By obj_Confirmation_TertiaryQualification = By.xpath("//*[text()='Do you:  ']//following::label");
		public static By obj_Confirmation_AnnualIncome = By.xpath("//*[text()='Annual income']//following::label");

		// Cover Details
		public static By obj_Confirmation_Death_ExistingCover = By.xpath("(//*[text()='Existing Cover'])[1]//following::p");
		public static By obj_Confirmation_Death_NewCover = By.xpath("(//*[text()='New Cover'])[1]//following::p");
		public static By obj_Confirmation_Death_OccupationalCategory = By.xpath("(//*[text()='Occupational Category '])[1]//following::p");
		public static By obj_Confirmation_Death_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[1]//following::p");
		public static By obj_Confirmation_TPD_ExistingCover = By.xpath("(//*[text()='Existing Cover'])[2]//following::p");
		public static By obj_Confirmation_TPD_NewCover = By.xpath("(//*[text()='New Cover'])[2]//following::p");
		public static By obj_Confirmation_TPD_OccupationalCategory = By.xpath("(//*[text()='Occupational Category '])[2]//following::p");
		public static By obj_Confirmation_TPD_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[2]//following::p");
		public static By obj_Confirmation_IP_ExistingCover = By.xpath("(//*[text()='Existing Cover'])[3]//following::p");
		public static By obj_Confirmation_IP_NewCover = By.xpath("(//*[text()='New Cover'])[3]//following::p");
		public static By obj_Confirmation_IP_OccupationalCategory = By.xpath("(//*[text()='Occupational Category '])[3]//following::p");
		public static By obj_Confirmation_IP_WaitingPeriod = By.xpath("//*[text()='Waiting Period']//following::p");
		public static By obj_Confirmation_IP_BenefitPeriod = By.xpath("//*[text()='Benefit Period']//following::p");
		public static By obj_Confirmation_IP_WeeklyCost = By.xpath("//*[text()=' Total Weekly cost']//following::h4");
		public static By obj_Confirmation_TotalWeeklyCost = By.xpath("//*[contains(text(), 'Total Weekly cost')]//following::h4");


		public static By obj_Confirmation_TermsConditionsCheckBox = By.xpath("//*[@id='generalConsentLabelWR']/span");
		public static By obj_Confirmation_Submit = By.xpath("//*[@ng-click='submitWorkRating()']");
		public static By obj_DecisionPopUP_No = By.xpath("//button[text()='No']");
		public static By obj_DecisionPopUP_Appstatus = By.xpath("//a[@class='noarrow']/h4");
		public static By obj_DecisionPopUP_AppMsg = By.xpath("//*[@id='collapseOne']/div/div/div/p");
		public static By obj_DecisionPopUP_App = By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p");
		public static By obj_Confirmation_Download = By.xpath("//strong[text()='Download ']");
		public static By obj_Confirmation_Save = By.xpath("//*[@ng-click='saveSummary()']");

}
