package vicsuper;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class VicSuper_LandingPage extends MasterPage {

	WebDriverUtil driverUtil = null;

	public VicSuper_LandingPage LandingPage_VicSuper() {
		OtherLinks();
		return new VicSuper_LandingPage(scriptHelper);

	}

	public VicSuper_LandingPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void OtherLinks() {

		try {

			String MemberName = dataTable.getData("VicSuper_LandingPage", "MemberName");
			String ApplyNowDeath = dataTable.getData("VicSuper_LandingPage", "ApplyNowDeath");
			String ApplyNowTPD = dataTable.getData("VicSuper_LandingPage", "ApplyNowTPD");
			String ApplyNowIP = dataTable.getData("VicSuper_LandingPage", "ApplyNowIP");
			String DeathYN = dataTable.getData("VicSuper_LandingPage", "DeathYN");
			String TPDYN = dataTable.getData("VicSuper_LandingPage", "TPDYN");
			String IPYN = dataTable.getData("VicSuper_LandingPage", "IPYN");
			String DeathAmount = dataTable.getData("VicSuper_LandingPage", "DeathAmount");
			String DeathAmount_Type = dataTable.getData("VicSuper_LandingPage", "DeathAmount_Type");
			String TPDAmount = dataTable.getData("VicSuper_LandingPage", "TPDAmount");
			String TPDAmount_Type = dataTable.getData("VicSuper_LandingPage", "TPDAmount_Type");
			String IPAmount = dataTable.getData("VicSuper_LandingPage", "IPAmount");
			String IPAmount_Type = dataTable.getData("VicSuper_LandingPage", "IPAmount_Type");
			String WaitingPeriod = dataTable.getData("VicSuper_LandingPage", "WaitingPeriod");
			String BenefitPeriod = dataTable.getData("VicSuper_LandingPage", "BenefitPeriod");
			String CoverType = dataTable.getData("VicSuper_LandingPage", "CoverType");
			String DeathIndex = dataTable.getData("VicSuper_LandingPage", "Death_Indexed");
			String TPDIndex = dataTable.getData("VicSuper_LandingPage", "TPD_Index");
			String Death_Rating = dataTable.getData("VicSuper_LandingPage", "Death_Occu_Rating");
			String TPD_Rating = dataTable.getData("VicSuper_LandingPage", "Death_Occu_Rating");
			String IP_Rating = dataTable.getData("VicSuper_LandingPage", "Death_Occu_Rating");
			String SPOCheck = dataTable.getData("VicSuper_LandingPage", "SpecialOfferCheck");
			String SPOCInvalidUnits = dataTable.getData("VicSuper_LandingPage", "SPO_InvalidUnits");
			String SPOCInvalidSiop = dataTable.getData("VicSuper_LandingPage", "SPO_SIOP");
			String SPOC_PersonalSaver = dataTable.getData("VicSuper_LandingPage", "SPO_PersonalSaver");
			String SPOC_Age13 = dataTable.getData("VicSuper_LandingPage", "SPO_Age13");
			String SPOC_Age70 = dataTable.getData("VicSuper_LandingPage", "SPO_Age70");
			String SPOC_Age14 = dataTable.getData("VicSuper_LandingPage", "SPO_Age14");
			String SPOC_Age69 = dataTable.getData("VicSuper_LandingPage", "SPO_Age69");
			String SPO_Age14_Death = dataTable.getData("VicSuper_LandingPage", "SPO_Age14_Death");
			String SPO_Age65_Death = dataTable.getData("VicSuper_LandingPage", "SPO_Age65_Death");
			String SPO_Invalid_Wait = dataTable.getData("VicSuper_LandingPage", "SPO_Invalid_Wait");
			String SPO_Invalid_Benefit = dataTable.getData("VicSuper_LandingPage", "SPO_Invalid_Benefit");
			String TransferCheck = dataTable.getData("VicSuper_LandingPage", "TransferCheck");
			String Tran_NoExistCover = dataTable.getData("VicSuper_LandingPage", "Tran_NoExistCover");
			String Tran_CoverOption = dataTable.getData("VicSuper_LandingPage", "Tran_CoverOption");

			// ****Validate the Member Name*****\\

			sleep(2000);

			WebDriverWait wait = new WebDriverWait(driver, 18);

			String NameDisplay = wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[1]/div/div[3]/div[2]/p/strong")))
					.getText();

			sleep(200);

			String NameDisplayed = NameDisplay.substring(9);

			NameDisplayed.trim();

			MemberName.trim();

			if (MemberName.contains(NameDisplayed)) {

				report.updateTestLog("Member Name", MemberName + " is displayed as Expected", Status.PASS);

			}

			else {

				report.updateTestLog("Member Name", MemberName + " is not displayed as expected", Status.FAIL);

			}

			sleep(500);

			// *****Check if any Apply Now is Displayed*****\\

			if (ApplyNowDeath.equalsIgnoreCase("Yes")) {

				if (driver.findElement(By.xpath("//*[@ng-if='eligibileForDeathCvrFlag']/div/button")).isDisplayed()) {

					report.updateTestLog("Death-Apply Now", "is displayed as expected", Status.PASS);

				}

				else {

					report.updateTestLog("Death-Apply Now", "is not displayed as expected", Status.FAIL);

				}

			}

			if (ApplyNowTPD.equalsIgnoreCase("Yes")) {

				if (driver.findElement(By.xpath("//*[@ng-if='eligibileForTpdCvrFlag']/div/button")).isDisplayed()) {

					report.updateTestLog("TPD-Apply Now", "is displayed as expected", Status.PASS);

				}

				else {

					report.updateTestLog("TPD-Apply Now", "is not displayed as expected", Status.FAIL);

				}

			}

			if (ApplyNowIP.equalsIgnoreCase("Yes")) {

				if (driver.findElement(By.xpath("//*[@ng-if='eligibileForIPCvrFlag']/div/button")).isDisplayed()) {

					report.updateTestLog("IP-Apply Now", "is displayed as expected", Status.PASS);

				}

				else {

					report.updateTestLog("IP-Apply Now", "is not displayed as expected", Status.FAIL);

				}

			}

			// *******Validating the Death Cover Details********\\

			if (DeathYN.equalsIgnoreCase("Yes")) {

				String DeathAmountDisplayed = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@ng-bind='deathCover.amount | currency:undefined:0']")))
						.getText();

				if (CoverType.equalsIgnoreCase("1")) {

					String DeathAmount_Type_Displayed = wait
							.until(ExpectedConditions
									.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(deathCover.units)']")))
							.getText();

					if (DeathAmount_Type.equalsIgnoreCase(DeathAmount_Type_Displayed)) {

						report.updateTestLog("Death Cover Type", DeathAmount_Type + " is displayed as Expected",
								Status.PASS);

					}

					else {

						report.updateTestLog("Death Cover Type", DeathAmount_Type + " is not displayed as expected",
								Status.FAIL);

					}

				}

				if (DeathAmount.equalsIgnoreCase(DeathAmountDisplayed)) {

					report.updateTestLog("Death Cover", DeathAmount + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("Death Cover", DeathAmount + " is not displayed as expected", Status.FAIL);

				}

				String DeathCoverType = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@ng-if,'deathCover.type')]")))
						.getText();

				String DeathCoverTypeArr[] = DeathCoverType.split("-");

				if (DeathAmount_Type.equalsIgnoreCase(DeathCoverTypeArr[0].trim())) {

					report.updateTestLog("Death Cover Type", DeathCoverTypeArr[0] + " is displayed as Expected",
							Status.PASS);
				}

				else {

					report.updateTestLog("Death Cover Type", DeathCoverTypeArr[0] + " is not displayed as Expected",
							Status.FAIL);

				}

				if ((DeathIndex.isEmpty()) && (DeathCoverTypeArr.length == 1)) {

					report.updateTestLog("No Death Indexing", " is displayed ", Status.PASS);

				} else if (DeathIndex.equalsIgnoreCase(DeathCoverTypeArr[1].trim())) {

					report.updateTestLog("Death Index Type", DeathCoverTypeArr[1] + " is displayed as Expected",
							Status.PASS);

				}

				else {

					report.updateTestLog("Death Index Type", DeathCoverTypeArr[1] + " is not displayed as Expected",
							Status.FAIL);

				}

				String DeathOccurat = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='deathCover.occRating']")))
						.getText();

				if (Death_Rating.equalsIgnoreCase(DeathOccurat)) {

					report.updateTestLog("Death Rating", DeathOccurat + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("Death Rating", DeathOccurat + " is not displayed as expected", Status.FAIL);

				}

			}

			// *****Validating the TPD Cover Details******\\

			if (TPDYN.equalsIgnoreCase("Yes")) {

				String TPDAmountDisplayed = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@ng-bind='tpdCover.amount | currency:undefined:0']")))
						.getText();

				if (CoverType.equalsIgnoreCase("1")) {

					String TPDAmount_Type_Displayed = wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(tpdCover.units)']")))
							.getText();

					if (TPDAmount_Type.equalsIgnoreCase(TPDAmount_Type_Displayed)) {

						report.updateTestLog("TPD Cover Type", TPDAmount_Type + " is displayed as Expected",
								Status.PASS);

					}

					else {

						report.updateTestLog("TPD Cover Type", TPDAmount_Type + " is not displayed as expected",
								Status.FAIL);

					}

				}

				if (TPDAmount.equalsIgnoreCase(TPDAmountDisplayed)) {

					report.updateTestLog("TPD Cover", TPDAmount + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("TPD Cover", TPDAmount + " is not displayed as expected", Status.FAIL);

				}

				String TPDCoverType = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@ng-if,'tpdCover.type')]")))
						.getText();

				String TPDCoverTypeArr[] = TPDCoverType.split("-");

				if (TPDAmount_Type.equalsIgnoreCase(TPDCoverTypeArr[0].trim())) {

					report.updateTestLog("TPD Cover Type", TPDCoverTypeArr[0] + " is displayed as Expected",
							Status.PASS);
				}

				else {

					report.updateTestLog("TPD Cover Type", TPDCoverTypeArr[0] + " is not displayed as Expected",
							Status.FAIL);

				}

				if ((TPDIndex.isEmpty()) && (TPDCoverTypeArr.length == 1)) {

					report.updateTestLog("No TPD Indexing", " is displayed ", Status.PASS);

				}

				else if (TPDIndex.trim().equalsIgnoreCase(TPDCoverTypeArr[1].trim())) {

					report.updateTestLog("TPD Index Type", TPDCoverTypeArr[1] + " is displayed as Expected",
							Status.PASS);

				}

				else {

					report.updateTestLog("TPD Index Type", TPDCoverTypeArr[1] + " is not displayed as Expected",
							Status.FAIL);

				}

			}

			// ****Validating the IP Cover Details******\\

			if (IPYN.equalsIgnoreCase("Yes")) {

				String IPAmountDisplayed = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@ng-bind='ipCover.amount | currency:undefined:0']")))
						.getText();

				String IPAmount_Type_Displayed = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@ng-if,'ipCover.type')]")))
						.getText();

				String WaitingPeriod_Displayed = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='ipCover.waitingPeriod']")))
						.getText();

				String BenefitPeriod_Displayed = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='ipCover.benefitPeriod']")))
						.getText();

				String IP_Type[] = IPAmount_Type_Displayed.split(" ");

				if (IP_Type.length > 1) {

					System.out.println(IP_Type[1]);

				}

				else {

					System.out.println("It is not unitised cover");

				}

				if (IPAmount.trim().equalsIgnoreCase(IPAmountDisplayed.trim())) {

					report.updateTestLog("IP Cover", IPAmount + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("IP Cover", IPAmount + " is not displayed as expected", Status.FAIL);

				}

				if (IPAmount_Type.trim().equalsIgnoreCase(IP_Type[1].trim())) {

					report.updateTestLog("IP Cover Type", IPAmount_Type + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("IP Cover Type", IPAmount_Type + " is not displayed as expected", Status.FAIL);

				}

				if (WaitingPeriod.equalsIgnoreCase(WaitingPeriod_Displayed)) {

					report.updateTestLog("Waiting Period", WaitingPeriod + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("Waiting Period", WaitingPeriod + " is not displayed as expected",
							Status.FAIL);

				}

				if (BenefitPeriod.equalsIgnoreCase(BenefitPeriod_Displayed)) {

					report.updateTestLog("Benefit Period", BenefitPeriod + " is displayed as Expected", Status.PASS);

				}

				else {

					report.updateTestLog("Benefit Period", BenefitPeriod + " is not displayed as expected",
							Status.FAIL);

				}

			}

			// ****Validating the SpecialOffer Cover Details******\\

			Robot robot = new Robot();

			robot.keyPress(KeyEvent.VK_PAGE_DOWN);

			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);

			if (SPOCheck.equalsIgnoreCase("Yes")) {

				if (driver.getPageSource().contains("SPECIAL OFFER FOR NEW MEMBERS")) {

					if (SPOC_Age14.equalsIgnoreCase("Yes") || SPOC_Age69.equalsIgnoreCase("Yes")
							|| SPO_Age65_Death.equalsIgnoreCase("Yes")) {

						report.updateTestLog("Special Offer Cover", "Special Offer is displayed", Status.PASS);

					}
				} else if (SPOCInvalidUnits.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative",
							"Special Offer is not displayed due to Invalid Units", Status.PASS);

				} else if (SPOCInvalidSiop.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative", "Special Offer is not displayed due to Invalid SIOP",
							Status.PASS);

				} else if (SPOC_PersonalSaver.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative",
							"Special Offer is not displayed due to Personal Saver", Status.PASS);

				} else if (SPOC_Age13.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative", "Special Offer is not displayed due to Age 13",
							Status.PASS);

				} else if (SPOC_Age70.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative", "Special Offer is not displayed due to Age 70",
							Status.PASS);

				} else if (SPO_Age14_Death.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative",
							"Special Offer is not displayed due to Age 14 and Death Only", Status.PASS);

				} else if (SPO_Invalid_Wait.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative",
							"Special Offer is not displayed due to Invalid Waiting Period", Status.PASS);

				} else if (SPO_Invalid_Benefit.equalsIgnoreCase("Yes")) {

					report.updateTestLog("Special Offer Negative",
							"Special Offer is not displayed due to Invalid Benefit Period", Status.PASS);

				}

			}

			// ****Validate the presence of links to other flows*****\\

			if (driver.getPageSource().contains("Change your insurance cover")) {

				report.updateTestLog("Change Your Insurance cover", "Change Your Insurance cover is displayed",
						Status.PASS);

			}

			if (driver.getPageSource().contains("Saved applications")) {

				report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application is displayed",
						Status.PASS);

			}

			if (TransferCheck.equalsIgnoreCase("Yes")) {

				if (Tran_NoExistCover.equalsIgnoreCase("Yes") || Tran_CoverOption.equalsIgnoreCase("Yes")) {

					if (driver.getPageSource().contains("Transfer your cover")) {

						report.updateTestLog("transfer your cover", "transfer your cover is displayed ", Status.PASS);

					}

					else {

						report.updateTestLog("transfer your cover", "transfer your cover is not displayed",
								Status.FAIL);

					}

				}

			}

			if (driver.getPageSource().contains("Increase your cover due to a life event")) {

				report.updateTestLog("Life Event", "Increase your cover due to a life event is displayed", Status.PASS);

			}

			if (driver.getPageSource().contains("Update Your Occupation")) {

				report.updateTestLog("Update Your Occupation", "Update Your Occupation is displayed", Status.PASS);

			}

			if (driver.getPageSource().contains("Cancel your cover")) {

				report.updateTestLog("Cancel your cover", "Cancel your cover is displayed", Status.PASS);

			}

			if (driver.getPageSource().contains("Insurance ")) {

				report.updateTestLog("Insurance Cost", "Insurance Cost is displayed", Status.PASS);

			}

			// ****Click on Insurance Calculator Link*****\\

			/*
			 *
			 * wait.until(ExpectedConditions.elementToBeClickable(By.
			 * xpath("//h5[text()='How much insurance']"))).click();
			 *
			 *
			 *
			 * report.updateTestLog("Insurance calculator",
			 * "Insurance calculator Link is Clicked", Status.PASS);
			 *
			 *
			 *
			 * sleep(1500);
			 *
			 *
			 *
			 * ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			 *
			 *
			 *
			 * driver.switchTo().window(tabs.get(1));
			 *
			 * sleep(1500);
			 *
			 * driver.manage().window().maximize();
			 *
			 * sleep(3000);
			 *
			 * driver.findElement(By.xpath("//h2[text()='Insurance Calculator']")).click();
			 *
			 * sleep(1000);
			 *
			 * String
			 * text=driver.findElement(By.xpath("//h2[text()='Insurance Calculator']")).
			 * getText();
			 *
			 *
			 *
			 * report.updateTestLog("Insurance Calculator", text+" page is Displayed",
			 * Status.PASS);
			 *
			 * driver.close();
			 *
			 * driver.switchTo().window(tabs.get(0));
			 *
			 * sleep(1500);
			 */

			// ****Click on Premium Calculator Link*****\\

			/*
			 * wait.until(ExpectedConditions.elementToBeClickable(By.
			 * xpath("//h4[text()='Insurance ']"))).click();
			 *
			 *
			 *
			 * report.updateTestLog("Premium calculator",
			 * "Premium calculator Link is Clicked", Status.PASS);
			 *
			 *
			 *
			 * sleep(1500);
			 *
			 *
			 *
			 * ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			 *
			 *
			 *
			 * driver.switchTo().window(tabs2.get(1));
			 *
			 * sleep(1500);
			 *
			 * driver.manage().window().maximize();
			 *
			 * sleep(3000);
			 *
			 * driver.findElement(By.xpath("//h2[text()=' Premium Calculator ']")).click();
			 *
			 * sleep(1000);
			 *
			 * String
			 * text2=driver.findElement(By.xpath("//h2[text()=' Premium Calculator ']")).
			 * getText();
			 *
			 * report.updateTestLog("Premium Calculator", text2+" page is Displayed",
			 * Status.PASS);
			 *
			 * driver.close();
			 *
			 * driver.switchTo().window(tabs.get(0));
			 */

			sleep(2000);

		} catch (Exception e) {

			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);

		}

	}

}
