package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceAmountItem")


@XmlRootElement
public class InsuranceAmountItem {	
	
	private String TypeCode;
	private ContractAmountItem ContractAmountItem;
	
	public ContractAmountItem getContractAmountItem() {
		return ContractAmountItem;
	}
	public void setContractAmountItem(ContractAmountItem contractAmountItem) {
		ContractAmountItem = contractAmountItem;
	}
	public String getTypeCode() {
		return TypeCode;
	}
	public void setTypeCode(String typeCode) {
		TypeCode = typeCode;
	}
	

	
	
	
}
