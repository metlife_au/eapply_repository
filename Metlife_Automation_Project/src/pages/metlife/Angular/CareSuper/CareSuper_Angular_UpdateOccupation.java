package pages.metlife.Angular.CareSuper;
/**
 * Author
 * Yuvaraj
 */
import pages.metlife.Angular.CareSuper.MasterPage;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;


public class CareSuper_Angular_UpdateOccupation extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public CareSuper_Angular_UpdateOccupation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public CareSuper_Angular_UpdateOccupation Update_Occupation_CareSuper() {
		updateoccupation();
		HealthandLifestyle();
		Declaration();
		return new CareSuper_Angular_UpdateOccupation(scriptHelper);
	} 
	
public void updateoccupation() {
	
		
		String EmailId = dataTable.getData("CareSuper_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("CareSuper_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("CareSuper_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("CareSuper_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("CareSuper_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("CareSuper_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("CareSuper_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("CareSuper_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("CareSuper_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("CareSuper_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("CareSuper_UpdateOccupation", "TertiaryQual");
		String Managementrole = dataTable.getData("CareSuper_UpdateOccupation", "Managementrole");
		String AnnualSalary = dataTable.getData("CareSuper_UpdateOccupation", "AnnualSalary");
		
		
		try{
			

			
			//*****Click on Update Occupation Link*******\\
			sleep(200);
			JavascriptExecutor a = (JavascriptExecutor)driver;
			a.executeScript("window.scrollBy(0,750)", "");			
			Thread.sleep(2000);
			WebDriverWait waiting=new WebDriverWait(driver,35);		
			waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Update your occupational category']"))).click();
			report.updateTestLog("Update your Occupation", "Update occupation link is Selected", Status.PASS);
			
		
			 sleep(1500);
			
			 WebDriverWait wait=new WebDriverWait(driver,18);
			//*****Agree to Duty of disclosure*******\\
				
			 JavascriptExecutor b = (JavascriptExecutor)driver;
				b.executeScript("window.scrollBy(0,700)", "");			
				Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is checked", Status.PASS);
           sleep(300);
			
           //*****Agree to Privacy Statement*******\\
			
			JavascriptExecutor c = (JavascriptExecutor)driver;
			c.executeScript("window.scrollBy(0,500)", "");			
			Thread.sleep(2000);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			report.updateTestLog("Privacy Statement", "Privacy Statement label is checked", Status.PASS);

			//*****Enter the Email Id*****\\										
			

			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);
										
			//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);					
										
			//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
										
										
										
			//***Select the Preferred time of Contact*****\\	
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
				
			//	driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
				driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
			//	driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
				driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}		
									
										
			//****Contact Details-Continue Button*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);							
										
										
			//***************************************\\							
			//**********OCCUPATION SECTION***********\\							
			//****************************************\\							
			JavascriptExecutor d = (JavascriptExecutor)driver;
			d.executeScript("window.scrollBy(0,500)", "");			
			Thread.sleep(2000);						
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			
		
										
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("workRatingIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);		
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("workRatingoccupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
				
			JavascriptExecutor e = (JavascriptExecutor)driver;
			e.executeScript("window.scrollBy(0,500)", "");			
			Thread.sleep(2000);
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}									
										
				
//**Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar 'white collar' tasks which do not 
	//**		involve manual work and are undertaken entirely within an office environment (excluding travel time from one office environment to another)?**//
			
			
			if(OfficeEnvironment.equalsIgnoreCase("Yes")){		
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
					
			}		
		else{		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
					
			}	
			
			//**Do you hold a tertiary qualification or are you a member of a professional institute or registered as a practicing member of your profession by a government body?**//
			
			if(TertiaryQual.equalsIgnoreCase("Yes")){		
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
					
			}		
		else{		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
					
			}	
			
			//**Are you in a management role?**//

			if(Managementrole.equalsIgnoreCase("Yes")){		
	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
		
			}		
			else{		
		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
		
			}	

			
			
			
          sleep(800);
		   
        //*****What is your annual Salary******							
			
			
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
		    
		    JavascriptExecutor f = (JavascriptExecutor)driver;
			f.executeScript("window.scrollBy(0,500)", "");			
			Thread.sleep(2000);
									
		  //**** I agree to all the above terms & conditions*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ackLabel']/span"))).click();
			report.updateTestLog("Continue", "Clicked on I agree to all the above terms & conditions", Status.PASS);
			sleep(2500);
		
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingOccupForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		sleep(2500);	
		
		
		
		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}


		public void HealthandLifestyle() {

			String Restrictedillness = dataTable.getData("CareSuper_UpdateOccupation", "Restrictedillness");
			String PriorClaim = dataTable.getData("CareSuper_UpdateOccupation", "PriorClaim");
			String ReducedLifeExpectancy = dataTable.getData("CareSuper_UpdateOccupation", "ReducedLifeExpectancy");
			
			try{			


      if(driver.getPageSource().contains("Eligibility Check")){

	//*******Off work because you are ill, injured or have had an accident******\\
	
	WebDriverWait wait=new WebDriverWait(driver,25);
         
	 if(Restrictedillness.equalsIgnoreCase("Yes")){
		
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
   }
    
	 else{
		 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
    }

     //******Unable to perform all of the duties of your usual occupation, without any restrictions on a full-time basis (at least 35 hours per week), regardless of whether you are currently working full time, part-time or casually;***\\

     if(PriorClaim.equalsIgnoreCase("Yes")){
    	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
     }
  
     else{
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
    	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
    	sleep(1000);
        }


//******In your usual occupation but your duties have changed or been modified in the last 12 months because of an illness, accident or injury?*****\\

    if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){
	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
    }
   else{
	   
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
}

          //****Click on Continue******\\

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
    sleep(4000);
      }

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

		}
	
		
		
	/////////////////*************Confirmation PAge*********************////////
		
		public void Declaration() {
			
			try{

				JavascriptExecutor a = (JavascriptExecutor)driver;
				a.executeScript("window.scrollBy(0,2200)", "");			
				Thread.sleep(2500);
				
								//*****General Consent check box******\\

				WebDriverWait wait=new WebDriverWait(driver,18);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelWR']/span"))).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);

   										//******Submit******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitWorkRating()']"))).click();
				report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

				Thread.sleep(7000);	

				//*******Feedback Pop-up******\\
				Thread.sleep(6000);	
				if(driver.getPageSource().contains("We value your feedback")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
					report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
					sleep(8000); 
				}

				if(driver.getPageSource().contains("Application number")){
	
	
					//*****Fetching the Application Status*****\\
	
					String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
					sleep(500);
	
					//*****Fetching the Application Number******\\
	
					String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
					sleep(1000);
	
	
	
					//******Download the PDF***********\\
					properties = Settings.getInstance();	
					String StrBrowseName=properties.getProperty("currentBrowser");
					if (StrBrowseName.equalsIgnoreCase("Chrome")){
						wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
						sleep(1500);
					}	
	

	
	
	
  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
  			sleep(1500);
  			Robot roboobj=new Robot();
  			roboobj.keyPress(KeyEvent.VK_ENTER);	
  		}
  		
  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
  			Robot roboobj=new Robot();
  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
  			
  			sleep(4000);	/*	
  			roboobj.keyPress(KeyEvent.VK_TAB);
  			roboobj.keyPress(KeyEvent.VK_TAB);
  			roboobj.keyPress(KeyEvent.VK_TAB);
  			roboobj.keyPress(KeyEvent.VK_TAB);
  			roboobj.keyPress(KeyEvent.VK_ENTER);	
  			
  		}*/
  		}
	
  		report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS); 
	
  		sleep(1000);

	 
  		report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);
	
		
  		report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);

	}

	else{
		 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
	}
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}

}
