package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTCCHealthPageObjects extends PageObjectLibrary{
	public static final By btnNext = getObjectIdentifaction("stft.cc.yourhealth.next.button.css");
}
