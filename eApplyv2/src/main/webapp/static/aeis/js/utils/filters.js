var hhlist =  
    	[
      		{"key":"10001","value":"Such as tension or cluster headaches and migraines."},
      		{"key":"10002","value":"Such as asthma, bronchitis, emphysema, pneumonia, or sleep apnoea."},
      		{"key":"10003","value":"Does <u><b>not</b></u> include contact lens or glasses for near or far sightedness."},
      		{"key":"10004","value":"Such as hearing loss, tinnitus, or swimmer&rsquo;s ear."},
      		{"key":"10006","value":"Such as carpal tunnel syndrome, pinched nerve, or tennis elbow."},
      		{"key":"10007","value":"Such as malaria, Ross River fever, glandular fever, hepatitis, meningitis or meningococcal."},
      		{"key":"10068","value":"Such as regular review by a cancer specialist."},
      		{"key":"10073","value":"A cancerous growth, which in view of its cell structure alone could spread and lead to death independent of its location."},
      		{"key":"10074","value":"A growth, which cell structure is considered to be harmless. However, the location and size of the growth may have health implications, such a benign brain tumour."},
      		{"key":"10075","value":"Such as regular review by a cancer specialist."},
      		{"key":"10098","value":"Not including epilepsy. A seizure is a sudden change in behaviour characterised by unexpected changes of the sense of feeling or movement with or without losing consciousness."},
      		{"key":"10099","value":"Reasonable control of seizures (up to 12 seizures per year) Possible side effects of medication Requirement of several drugs to maintain control Some limitations of functioning within domestic and occupational environment independently."},
      		{"key":"10100","value":"Seizures have never been sufficiently controlled Significant limitations of functioning within domestic and occupational environment Some periods of continuous seizure activity and status pilepticus (seizure continuing for 5 minutes or longer) Requires ongoing support in day to day functioning."},
      		{"key":"10110","value":"Quick control of seizures (within 6 months)."},
      		{"key":"10111","value":"Reasonable control of seizures (up to 12 seizures per year) Possible side effects of medication Requirement of several drugs to maintain control Some limitations of functioning within domestic and occupational environment independently."},
      		{"key":"10112","value":"Seizures have never been sufficiently controlled Significant limitations of functioning within domestic and occupational environment Some periods of continuous seizure activity and status epilepticus (seizure continuing for 5 minutes or longer) Requires ongoing support in day to day functioning."},
      		{"key":"10136","value":"Self-limiting attacks of neurological problems Nil or only slight worsening of condition Complete remission with long duration between relapses Sensory changes in only one area of the body."},
      		{"key":"10137","value":"Significant worsening of condition over time Some periods of remission Sensory changes in various areas of the body Mild bowel or bladder problems."},
      		{"key":"10138","value":"Progressive worsening of the condition sigificant impairment requiring assistance, mobility devices, marked bowel, or bladder problems."},
      		{"key":"10200","value":"Such as heart, kidney, lung, blood vessels or brain."},
      		{"key":"10201","value":"Such as heart, lung, kidney, stomach, or intestines."},
      		{"key":"10287","value":"Are you a permanent resident of Australia?"},
      		{"key":"10293","value":"Such as cigars, cigarettes, tobacco in the last 12 months."},
      		{"key":"10307","value":"Does not include vision loss already corrected by laser treatment."},
      		{"key":"10327","value":"Such as but not limited to persisting fatigue, frequent tiredness, chronic fatigue syndrome, fibromyalgia, lethargy, run down or burn out."},
      		{"key":"10330","value":"Such as arthritis, osteoporosis, repetitive strain injury, broken bones and joint dislocation."},
      		{"key":"10344","value":"Such as pain in the neck, middle or lower back caused by a muscular, tendon, ligament, nerve and/or bone condition."},
      		{"key":"10345","value":"Such as a condition of the oesophagus, stomach, pancreas, gall bladder or intestines."},
      		{"key":"10358","value":"Such as seizure, epilepsy, multiple sclerosis, Parkinson&rsquo;s or Alzheimer&rsquo;s disease or stroke."},
      		{"key":"10365","value":"Not including epilepsy. A seizure is a sudden change in behaviour characterised by unexpected changes of the sense of feeling or movement with or without losing consciousness."},
      		{"key":"10368","value":"Such as bipolar, personality, eating or post traumatic stress disorder, stress, anxiety, depression or schizophrenia."},
      		{"key":"10379","value":"Such as cancer, or other growth of the skin and/or other body parts perceived to be abnormal."},
      		{"key":"10387","value":"Such as an overactive or underactive thyroid, or abnormal growth of the thyroid gland."},
      		{"key":"10396","value":"Such as cancer, keratosis, eczema, dermatitis or psoriasis."},
      		{"key":"10405","value":"Such as conditions of the bladder, urinary tract, or reproductive organs."},
      		{"key":"10970","value":"Such as a condition in which a person&rsquo;s immune system attacks the body&rsquo;s own cells causing abnormal functioning."},
      		{"key":"10429","value":"Such as heart attack, abnormal heart beat, or heart defect including valve or vessel."},
      		{"key":"10438","value":"Such as cyst, abscess, inflammation, stones and&#47or organ failure."},
      		{"key":"10455","value":"Such as gestational, insulin, non-insulin dependent diabetes, Impaired glucose tolerance or other."},
      		{"key":"10457","value":"Diabetes occuring during pregnancy."},
      		{"key":"10462","value":"Conditions which affects the blood vessels that supply the brain."},
      		{"key":"10464","value":"Such as stroke, aneurysm, or transient ischaemic attacks."},
      		{"key":"10465","value":"A condition that causes the sudden interruption of blood supply to a portion of the brain."},
      		{"key":"10467","value":"Such as anaemia, leukaemia, or other blood clotting disorders."},
      		{"key":"10533","value":"375ml full strength beer is 1 standard drink 375ml light beer is 0.8 standard drink 30ml spirit is 1 standard drink 375ml pre-mix spirit can is 1.5 standard drink 150ml glass of wine is 1.5 standard drink 170ml glass of champagne is 1.5 standard drink."},
      		{"key":"10589","value":"Occassional attacks with normal lung function between attacks Last visit to hospital over 5 yrs ago Occassional use of ventolin Occurs with cold/flu with occassional use of a preventer No use of steroids or prophylatic medication."},
      		{"key":"10590","value":"Frequent attacks but normal lung function between attacks Rare visit or short stay in hospital ( 1 visit every 3 yrs) Usually requires steroids/bronchodilators inhalers Occassional use of oral steroids Chest x-ray showing mild changes between attacks Functional ability normal between attacks etc."},
      		{"key":"10591","value":"Very frequent attacks Almost constant wheezing requiring bronchodilators/steriod inhalers Moderately abnormal pulmonary function test between attacks Frequent visits to hospital Oral steroids required more than 20days per year Chest x-ray usually shows air trapping Function ability mildly impaired exercise tolerance at all times."},
      		{"key":"10646","value":"HbA1c is a blood test done by your doctor indicating average diabetic control over a period of time."},
      		{"key":"10719","value":"Stroke, heart and kidney disease, nerve, blood vessel and or vision damage."},
      		{"key":"10774","value":"Such as heart, kidney, liver, lung, spleen or glands."},
      		{"key":"10794","value":"Such as heart, kidney, liver, lung, spleen or glands."},
      		{"key":"11078","value":"What is your occupation?"},
      		{"key":"11241","value":"Such as sexually transmitted infections, hepatitis, HIV/AIDS, malaria, glandular, Ross River fever or other"},
      		{"key":"11747","value":"On average: <br>375ml full strength beer is<br>1.5 standard drinks,<br>375ml light beer is <br>1 standard drink, <br>30ml glass of spirits is<br>1 standard drink,<br>700ml bottle of spirits <br>is 22 standard drinks,<br>375ml pre-mix spirit is <br>2 standard drinks,<br>150ml glass of wine is <br>1.5 standard drinks, <br>750ml bottle of wine is <br>7.5 standard drinks,<br>170ml glass of champagne is 1.5 standard drinks."},
      		{"key":"933001","value":"Such as asthma, bronchitis, emphysema/COPD, pneumonia, pneumothorax or sleep apnoea."},
      		{"key":"933002","value":"Such as amputation, arthritis, bone, ligament, muscle or joint damage, osteoporosis, paralysis, repetitive strain injury, tennis elbow or carpal tunnel syndrome."},
      		{"key":"933003","value":"Such as a condition of the oesophagus, stomach, pancreas, gall bladder, intestines or other."},
      		{"key":"933004","value":"Such as AlzheimerQQs, dementia, aneurysm, epilepsy, multiple sclerosis, muscular dystrophy, Parkinson&rsquo;s, seizure, stroke, transient ischemic attack or other."},
      		{"key":"933005","value":"Such as anxiety, depression, bipolar, eating disorder, personality or stress disorder, schizophrenia or other."},
      		{"key":"933006","value":"Such as cancers including BCC, SCC and melanomas, cysts, growth, tumours, polyps or breast lumps."},
      		{"key":"933007","value":"Such as goitre, hyperthyroidism, hypothyroidism, cancer, thyroiditis or nodule."},
      		{"key":"933008","value":"Such as dermatitis, eczema, impetigo, keratosis or psoriasis."},
      		{"key":"933009","value":"Such as abnormal pap smear, blood in urine, cervical cancer, endometriosis, fibroid, infection, complication of pregnancy or other."},
      		{"key":"933010","value":"Such as blood in urine, prostate or testicular condition, infection or other."},
      		{"key":"933011","value":"Such as CREST, scleroderma or Systemic Lupus Erythematosus (SLE)."},
      		{"key":"933012","value":"Such as angina, cardiomyopathy, heart attack, birth defect, murmur, irregular heart beat or other."},
      		{"key":"933013","value":"Such as organ failure, donation, cyst, tumour or other."},
      		{"key":"933014","value":"Such as haemochromatosis, haemophilia, iron deficiency, leukaemia, lymphoblastic anaemia, thalassemia or other."},
      		{"key":"933015","value":"This visa allows a skilled worker travel to Australia to work in their nominated occupation with an approved sponsor for up to 4 years"}
      	]
AeisApp.filter('split', function() {
    return function(input) {
    	input = input.substring(input.indexOf(']')+1)                
        return input;
    }
});

AeisApp.filter('sectionSplit', function() {
    return function(input) {
    	input = input.substring(input.indexOf('=')+1)                
        return input;
    }
});

AeisApp.filter('checkboxFilter', function() {
    return function(input) { 	
    	angular.forEach(input, function (Object,index) {
    		if(Object.answerText.indexOf('11241')!==-1 || Object.answerText.indexOf('11242')!==-1 || Object.answerText.indexOf('11243')!==-1 || Object.answerText.indexOf('11251')!==-1 || Object.answerText.indexOf('11252')!==-1){
    			input.splice(index, 1);
    		} 
    		
    		angular.forEach(hhlist, function (helpFulHint) {    			
    			if(Object.answerText.indexOf(helpFulHint.key)!==-1){
    				console.log(Object.answerText+":"+helpFulHint.key)
    				input[index].helpText=helpFulHint.value
    			}else{
    				//input[index].helpText='false'
    			}
    		});
    		
    	});       	             
        return input;
    }
});

AeisApp.filter('dateTextSplit', function() {
    return function(input) {
    	console.log(input.indexOf('dd/mm/yyyy'))
    	if(input.indexOf('dd/mm/yyyy')!==-1){
    		input = input.substring(0,input.indexOf('dd/mm/yyyy')-1)   
    	}        	             
        return input;
    }
});