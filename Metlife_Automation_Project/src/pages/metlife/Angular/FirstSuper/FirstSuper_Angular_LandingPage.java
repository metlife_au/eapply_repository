package pages.metlife.Angular.FirstSuper;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_LandingPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public FirstSuper_Angular_LandingPage OtherLinks_FirstSuper() {
		OtherLinks();		
		return new FirstSuper_Angular_LandingPage(scriptHelper);
	}

	public FirstSuper_Angular_LandingPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void OtherLinks() {

		
		
		try{
						
			String MemberName = dataTable.getData("FirstSuper_LandingPage", "MemberName");
			String ApplyNowDeath = dataTable.getData("FirstSuper_LandingPage", "ApplyNowDeath");
			String ApplyNowTPD = dataTable.getData("FirstSuper_LandingPage", "ApplyNowTPD");
			String ApplyNowIP = dataTable.getData("FirstSuper_LandingPage", "ApplyNowIP");
			
			
			String DeathYN = dataTable.getData("FirstSuper_LandingPage", "DeathYN");
			String TPDYN = dataTable.getData("FirstSuper_LandingPage", "TPDYN");
			String IPYN = dataTable.getData("FirstSuper_LandingPage", "IPYN");
			
			String DeathAmount = dataTable.getData("FirstSuper_LandingPage", "DeathAmount");
			
			String DeathAmount_Type = dataTable.getData("FirstSuper_LandingPage", "DeathAmount_Type");
			String TPDAmount = dataTable.getData("FirstSuper_LandingPage", "TPDAmount");
			String TPDAmount_Type = dataTable.getData("FirstSuper_LandingPage", "TPDAmount_Type");
			String IPAmount = dataTable.getData("FirstSuper_LandingPage", "IPAmount");
			String IPAmount_Type = dataTable.getData("FirstSuper_LandingPage", "IPAmount_Type");
			String WaitingPeriod = dataTable.getData("FirstSuper_LandingPage", "WaitingPeriod");
			String BenefitPeriod = dataTable.getData("FirstSuper_LandingPage", "BenefitPeriod");
			
			//****Validate the Member Name*****\\	
			sleep(2000);
			WebDriverWait wait=new WebDriverWait(driver,18);
			String NameDisplay=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-xs-12']/strong"))).getText();
			sleep(200);
			String NameDisplayed=NameDisplay.substring(9);
			NameDisplayed.trim();
			MemberName.trim();
			
			
			if(MemberName.contains(NameDisplayed)){
				
				report.updateTestLog("Member Name", MemberName+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("Member Name", MemberName+" is not displayed as expected", Status.FAIL);
			}
			
			sleep(500);
			
			
			//*****Check if any Apply Now is Displayed*****\\
			

			if(ApplyNowDeath.equalsIgnoreCase("Yes")){
				
				if(driver.findElement(By.xpath("//*[@ng-if='!intvalue(deathCover.amount)']/div[2]/button")).isDisplayed()){
					
					report.updateTestLog("Death-Apply Now", "is displayed as expected", Status.PASS);
				}
				
				else{
					
					report.updateTestLog("Death-Apply Now", "is not displayed as expected", Status.FAIL);
				}
			}
			
			if(ApplyNowTPD.equalsIgnoreCase("Yes")){
				
				if(driver.findElement(By.xpath("//*[@ng-if='!intvalue(tpdCover.amount)']/div[2]/button")).isDisplayed()){
					
					report.updateTestLog("TPD-Apply Now", "is displayed as expected", Status.PASS);
				}
				
				else{
					
					report.updateTestLog("TPD-Apply Now", "is not displayed as expected", Status.FAIL);
				}
			}
			
           if(ApplyNowIP.equalsIgnoreCase("Yes")){
				
				if(driver.findElement(By.xpath("//*[@ng-if='!intvalue(ipCover.amount)']/div[2]/button")).isDisplayed()){
					
					report.updateTestLog("IP-Apply Now", "is displayed as expected", Status.PASS);
				}
				
				else{
					
					report.updateTestLog("IP-Apply Now", "is not displayed as expected", Status.FAIL);
				}
			}
			
			
			//*******Validating the Death Cover Details********\\
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				
				String DeathAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='deathCover.amount | currency:undefined:0']"))).getText();
				String DeathAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(deathCover.units)']"))).getText();
	            
				if(DeathAmount.equalsIgnoreCase(DeathAmountDisplayed)){
					
					report.updateTestLog("Death Cover", DeathAmount+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Death Cover", DeathAmount+" is not displayed as expected", Status.FAIL);
				}
				
	           if(DeathAmount_Type.equalsIgnoreCase(DeathAmount_Type_Displayed)){
					
					report.updateTestLog("Death Cover Type", DeathAmount_Type+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Death Cover Type", DeathAmount_Type+" is not displayed as expected", Status.FAIL);
				}
			}
			
			
           
           
           //*****Validating the TPD Cover Details******\\
           
			if(TPDYN.equalsIgnoreCase("Yes")){
				
				 String TPDAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='tpdCover.amount | currency:undefined:0']"))).getText();
				   String TPDAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(tpdCover.units)']"))).getText();
		           
					if(TPDAmount.equalsIgnoreCase(TPDAmountDisplayed)){
						
						report.updateTestLog("TPD Cover", TPDAmount+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("TPD Cover", TPDAmount+" is not displayed as expected", Status.FAIL);
					}
					
		          if(TPDAmount_Type.equalsIgnoreCase(TPDAmount_Type_Displayed)){
						
						report.updateTestLog("TPD Cover Type", TPDAmount_Type+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("TPD Cover Type", TPDAmount_Type+" is not displayed as expected", Status.FAIL);
					}
		          
		          
			}
			
          
          //****Validating the IP Cover Details******\\
			
			if(IPYN.equalsIgnoreCase("Yes")){
				
				 String IPAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='ipCover.amount | currency:undefined:0']"))).getText();
				   String IPAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(ipCover.units)']"))).getText();
				   String WaitingPeriod_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='ipCover.waitingPeriod']"))).getText();
				   String BenefitPeriod_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='ipCover.benefitPeriod']"))).getText();
		          
					if(IPAmount.equalsIgnoreCase(IPAmountDisplayed)){
						
						report.updateTestLog("IP Cover", IPAmount+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("IP Cover", IPAmount+" is not displayed as expected", Status.FAIL);
					}
					
		           if(IPAmount_Type.equalsIgnoreCase(IPAmount_Type_Displayed)){
						
						report.updateTestLog("IP Cover Type", IPAmount_Type+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("IP Cover Type", IPAmount_Type+" is not displayed as expected", Status.FAIL);
					}
		           
		           if(WaitingPeriod.equalsIgnoreCase(WaitingPeriod_Displayed)){
						
						report.updateTestLog("Waiting Period", WaitingPeriod+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("Waiting Period", WaitingPeriod+" is not displayed as expected", Status.FAIL);
					}
		           
		           if(BenefitPeriod.equalsIgnoreCase(BenefitPeriod_Displayed)){
						
						report.updateTestLog("Benefit Period", BenefitPeriod+" is displayed as Expected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("Benefit Period", BenefitPeriod+" is not displayed as expected", Status.FAIL);
					}
		          
			}
          
          
           
           //****Validate the presence of links to other flows*****\\
           
           if(driver.getPageSource().contains("Change your insurance cover")){
        	  
        	   report.updateTestLog("Change Your Insurance cover", "Change Your Insurance cover is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Saved applications")){
         	  
        	   report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("transfer your cover")){
          	  
        	   report.updateTestLog("transfer your cover", "transfer your cover is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Increase your cover due to a life event")){
          	  
        	   report.updateTestLog("Life Event", "Increase your cover due to a life event is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Update Your Occupation")){
          	  
        	   report.updateTestLog("Update Your Occupation", "Update Your Occupation is displayed", Status.PASS);   
           }
           
           if(driver.getPageSource().contains("Cancel your cover")){
          	  
        	   report.updateTestLog("Cancel your cover", "Cancel your cover is displayed", Status.PASS);   
           }
           
			//****Click on Insurance Calculator Link*****\\
	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[text()='How much insurance cover']"))).click();
			
			report.updateTestLog("Insurance calculator", "Insurance calculator Link is Clicked", Status.PASS);
			
			sleep(1500);
		
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		     
			driver.switchTo().window(tabs.get(1));
			sleep(1500);
			driver.manage().window().maximize();
			sleep(1500);
			driver.findElement(By.xpath("//h1[text()='Insurance Calculators']")).click();
			sleep(1000);
			String text=driver.findElement(By.xpath("//h1[text()='Insurance Calculators']")).getText();
			
			report.updateTestLog("Insurance Calculator", text+" page is Displayed", Status.PASS);		 
			driver.close();
			driver.switchTo().window(tabs.get(0));
			sleep(1500);
			
			//****Click on Premium Calculator Link*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Insurance ']"))).click();
			
			report.updateTestLog("Premium calculator", "Premium calculator Link is Clicked", Status.PASS);
			
			sleep(1500);
		
			ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		     
			driver.switchTo().window(tabs2.get(1));
			sleep(1500);
			driver.manage().window().maximize();
			sleep(1500);
			driver.findElement(By.xpath("//h1[text()='Premium Calculator - Quick Quote']")).click();
			sleep(1000);
			String text2=driver.findElement(By.xpath("//h1[text()='Premium Calculator - Quick Quote']")).getText();
			report.updateTestLog("Premium Calculator", text2+" page is Displayed", Status.PASS);		 
			driver.close();
			driver.switchTo().window(tabs.get(0));
		    
			sleep(2000);
			
	
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}





