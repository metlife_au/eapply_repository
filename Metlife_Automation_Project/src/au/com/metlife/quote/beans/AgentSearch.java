package au.com.metlife.quote.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentSearch", propOrder = {
      "agent"     
    
})
public class AgentSearch {

      @XmlElement(required = true)
      Agent agent[];

      public Agent[] getAgent() {
            return agent;
      }

      public void setAgent(Agent[] agent) {
            this.agent = agent;
      }

      }

