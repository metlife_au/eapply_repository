/* --------------------------------------------------------------------------------------------------------------------------- 
 * Description: EappRulesCacheSerlvet class is the backing bean for UC016- Cover Details
 * ---------------------------------------------------------------------------------------------------------------------------
 * Copyright � 2009 Metlife, Inc. 
 * ---------------------------------------------------------------------------------------------------------------------------
 * Author :     Ravi Reddy
 * Created:     17/7/2009
 * ---------------------------------------------------------------------------------------------------------------------------
 * Modification Log:	mm/dd/yyyy
 * ---------------------------------------------------------------------------------------------------------------------------
 * Date				Author					Description         
 * 7/16/2012	   Pushkar Vashishtha       Added New questions variables for YBR PDF
 * {Please place your information at the top of the list}
 * ---------------------------------------------------------------------------------------------------------------------------
 */
package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;
import java.util.ArrayList;

import au.com.metlife.eapply.underwriting.response.model.ResponseObject;

public class MemberDetails implements Serializable{
	
	private String memberAppRefNumber = null;
	private String memberFName = null;
	private String memberLName = null;
	private String title = null;
	private String dob = null;
	private String gender = null;
	private String memberSmokerStatus = null;
	private String memebrPrefferedContactNum = null;
	private String memberAddress = null;
	private String memberOccupation = null;
	private String memberOtherOccupation = null;
	private String frquencyOpted = null;
	private String totalPremium = null;
	private String smoker = null;
	private String emailId = null;
	private String clientRefNum = null;
	private String clientEmailId = null;
	private String dateJoinedFund = null;
	private String dateJoinedCompany = null;
	private boolean replaceCvrOption = false;
	private String prefferedContactNumber = null;
	private String prefferedContactType = null;
	private String prefferedContactTime = null;
	private String existingExclusion = null;
	private String existingLoading = null;
	private String australiaCitizenship = null;
	private ResponseObject responseObject = null;
	private ArrayList<MemberProductDetails> memberProductList = null;
	private ArrayList<MemberDetails> memberKidsList = null;
	private String relationship = null;
	private String relationshipAuthorization = null;
	private boolean clientMatched = false;
	private boolean transferCover = Boolean.FALSE;
	private boolean shortFormDcl = Boolean.FALSE;
	//Added by Pushkar
	private String memberIndustry = null;
	private String hazardousEnv = null;
	private String workWhollyInOffice = null;
	private String workOutsideOffice = null;
	private String tertiaryQual = null;
	private String annualSal = null;
	private String empStatus = null;
	private Boolean forcedRUW = Boolean.FALSE;
	private String memberType = null;
	private String occCategory = null;
	
	private String unitNo = null;
	private String streetNo = null;
	private String streetName = null;
	private String subrub = null;
	private String state = null;
	private String country = null;
	private String postCode = null;
	private boolean flyBuysRegReq = false;
	private String brand = null;
	private String prefContactDetails = null;
	private String ampReferenceNumber = null;
	
	
	private ArrayList<PersonalDetail> personalDts = new ArrayList<PersonalDetail>();
	
	public ArrayList<PersonalDetail> getPersonalDts() {
		return personalDts;
	}
	public void setPersonalDts(ArrayList<PersonalDetail> personalDts) {
		this.personalDts = personalDts;
	}
	public boolean isTransferCover() {
		return transferCover;
	}
	public void setTransferCover(boolean transferCover) {
		this.transferCover = transferCover;
	}
	public boolean isClientMatched() {
		return clientMatched;
	}
	public void setClientMatched(boolean clientMatched) {
		this.clientMatched = clientMatched;
	}
	public ArrayList<MemberDetails> getMemberKidsList() {
		return memberKidsList;
	}
	public void setMemberKidsList(ArrayList<MemberDetails> memberKidsList) {
		this.memberKidsList = memberKidsList;
	}
	public ArrayList<MemberProductDetails> getMemberProductList() {
		return memberProductList;
	}
	public void setMemberProductList(
			ArrayList<MemberProductDetails> memberProductList) {
		this.memberProductList = memberProductList;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getFrquencyOpted() {
		return frquencyOpted;
	}
	public void setFrquencyOpted(String frquencyOpted) {
		this.frquencyOpted = frquencyOpted;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMemberAddress() {
		return memberAddress;
	}
	public void setMemberAddress(String memberAddress) {
		this.memberAddress = memberAddress;
	}
	public String getMemberAppRefNumber() {
		return memberAppRefNumber;
	}
	public void setMemberAppRefNumber(String memberAppRefNumber) {
		this.memberAppRefNumber = memberAppRefNumber;
	}
	public String getMemberFName() {
		return memberFName;
	}
	public void setMemberFName(String memberFName) {
		this.memberFName = memberFName;
	}
	public String getMemberLName() {
		return memberLName;
	}
	public void setMemberLName(String memberLName) {
		this.memberLName = memberLName;
	}
	public String getMemberOccupation() {
		return memberOccupation;
	}
	public void setMemberOccupation(String memberOccupation) {
		this.memberOccupation = memberOccupation;
	}
	public String getMemberSmokerStatus() {
		return memberSmokerStatus;
	}
	public void setMemberSmokerStatus(String memberSmokerStatus) {
		this.memberSmokerStatus = memberSmokerStatus;
	}
	public String getMemebrPrefferedContactNum() {
		return memebrPrefferedContactNum;
	}
	public void setMemebrPrefferedContactNum(String memebrPrefferedContactNum) {
		this.memebrPrefferedContactNum = memebrPrefferedContactNum;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}
	public ResponseObject getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(ResponseObject responseObject) {
		this.responseObject = responseObject;
	}
	public String getPrefferedContactNumber() {
		return prefferedContactNumber;
	}
	public void setPrefferedContactNumber(String prefferedContactNumber) {
		this.prefferedContactNumber = prefferedContactNumber;
	}
	public String getSmoker() {
		return smoker;
	}
	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getClientRefNum() {
		return clientRefNum;
	}
	public void setClientRefNum(String clientRefNum) {
		this.clientRefNum = clientRefNum;
	}
	public String getDateJoinedFund() {
		return dateJoinedFund;
	}
	public void setDateJoinedFund(String dateJoinedFund) {
		this.dateJoinedFund = dateJoinedFund;
	}
	public String getPrefferedContactTime() {
		return prefferedContactTime;
	}
	public void setPrefferedContactTime(String prefferedContactTime) {
		this.prefferedContactTime = prefferedContactTime;
	}
	public String getPrefferedContactType() {
		return prefferedContactType;
	}
	public void setPrefferedContactType(String prefferedContactType) {
		this.prefferedContactType = prefferedContactType;
	}
	public String getExistingExclusion() {
		return existingExclusion;
	}
	public void setExistingExclusion(String existingExclusion) {
		this.existingExclusion = existingExclusion;
	}
	public String getExistingLoading() {
		return existingLoading;
	}
	public void setExistingLoading(String existingLoading) {
		this.existingLoading = existingLoading;
	}
	public boolean isReplaceCvrOption() {
		return replaceCvrOption;
	}
	public void setReplaceCvrOption(boolean replaceCvrOption) {
		this.replaceCvrOption = replaceCvrOption;
	}
	public String getClientEmailId() {
		return clientEmailId;
	}
	public void setClientEmailId(String clientEmailId) {
		this.clientEmailId = clientEmailId;
	}
	public String getAustraliaCitizenship() {
		return australiaCitizenship;
	}
	public void setAustraliaCitizenship(String australiaCitizenship) {
		this.australiaCitizenship = australiaCitizenship;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public boolean isShortFormDcl() {
		return shortFormDcl;
	}
	public void setShortFormDcl(boolean shortFormDcl) {
		this.shortFormDcl = shortFormDcl;
	}
	public String getMemberIndustry() {
		return memberIndustry;
	}
	public void setMemberIndustry(String memberIndustry) {
		this.memberIndustry = memberIndustry;
	}
	public String getAnnualSal() {
		return annualSal;
	}
	public void setAnnualSal(String annualSal) {
		this.annualSal = annualSal;
	}
	public String getEmpStatus() {
		return empStatus;
	}
	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}
	public String getHazardousEnv() {
		return hazardousEnv;
	}
	public void setHazardousEnv(String hazardousEnv) {
		this.hazardousEnv = hazardousEnv;
	}
	public String getTertiaryQual() {
		return tertiaryQual;
	}
	public void setTertiaryQual(String tertiaryQual) {
		this.tertiaryQual = tertiaryQual;
	}
	public String getWorkOutsideOffice() {
		return workOutsideOffice;
	}
	public void setWorkOutsideOffice(String workOutsideOffice) {
		this.workOutsideOffice = workOutsideOffice;
	}
	public String getWorkWhollyInOffice() {
		return workWhollyInOffice;
	}
	public void setWorkWhollyInOffice(String workWhollyInOffice) {
		this.workWhollyInOffice = workWhollyInOffice;
	}
	public Boolean getForcedRUW() {
		return forcedRUW;
	}
	public void setForcedRUW(Boolean forcedRUW) {
		this.forcedRUW = forcedRUW;
	}
	public String getMemberOtherOccupation() {
		return memberOtherOccupation;
	}
	public void setMemberOtherOccupation(String memberOtherOccupation) {
		this.memberOtherOccupation = memberOtherOccupation;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getOccCategory() {
		return occCategory;
	}
	public void setOccCategory(String occCategory) {
		this.occCategory = occCategory;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getStreetNo() {
		return streetNo;
	}
	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}
	public String getSubrub() {
		return subrub;
	}
	public void setSubrub(String subrub) {
		this.subrub = subrub;
	}
	public String getUnitNo() {
		return unitNo;
	}
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public boolean isFlyBuysRegReq() {
		return flyBuysRegReq;
	}
	public void setFlyBuysRegReq(boolean flyBuysRegReq) {
		this.flyBuysRegReq = flyBuysRegReq;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getRelationshipAuthorization() {
		return relationshipAuthorization;
	}
	public void setRelationshipAuthorization(String relationshipAuthorization) {
		this.relationshipAuthorization = relationshipAuthorization;
	}
	public String getPrefContactDetails() {
		return prefContactDetails;
	}
	public void setPrefContactDetails(String prefContactDetails) {
		this.prefContactDetails = prefContactDetails;
	}
	public String getDateJoinedCompany() {
		return dateJoinedCompany;
	}
	public void setDateJoinedCompany(String dateJoinedCompany) {
		this.dateJoinedCompany = dateJoinedCompany;
	}
	public String getAmpReferenceNumber() {
		return ampReferenceNumber;
	}
	public void setAmpReferenceNumber(String ampReferenceNumber) {
		this.ampReferenceNumber = ampReferenceNumber;
	}
	

}
