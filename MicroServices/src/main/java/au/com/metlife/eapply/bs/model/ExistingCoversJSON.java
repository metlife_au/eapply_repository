package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.util.List;

public class ExistingCoversJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8403007575997653039L;
	private List<CoverJSON> cover;

	public List<CoverJSON> getCover() {
		return cover;
	}

	public void setCover(List<CoverJSON> cover) {
		this.cover = cover;
	}
}
