package au.com.metlife.eapply.business.entity;

public class BaseEntity {
	
	private String status;
	private String fundCode;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	@Override
	public String toString() {
		return "BaseEntity [status=" + status + ", fundCode=" + fundCode + "]";
	}
	
	
	
	

	

}
