package supportlibraries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

/*import com.borland.testmanager.democlient.webservice.ConnectionHandler;
import com.borland.testmanager.democlient.webservice.LoginContext;
import com.borland.testmanager.democlient.webservice.TestPlanningHandler;
import com.borland.testmanager.stubs.tmplanning.scc.NamedEntity;
import com.borland.testmanager.stubs.tmplanning.tm.Project;
import com.borland.testmanager.stubs.tmplanning.tm.PropertyValue;
import com.borland.testmanager.stubs.tmplanning.tm.TestPlanningNode;
*/
import com.cognizant.framework.ExcelDataAccess;
import com.cognizant.framework.FrameworkParameters;
import com.cognizant.framework.ReportSettings;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Util;

public class SilkIntegrator
{
	private static FrameworkParameters frameworkParameters = FrameworkParameters.getInstance();
	private static Properties properties;

	List<String> fileList;
	private ScriptHelper scriptHelper = null;

	private String silkTestResultsDirectoryPath = System.getProperty("#sctm_test_results_dir");

/*	LoginContext loginContext = null;
	TestPlanningHandler planningHandler = null;
	Collection<Project> allProjects = null;
	Collection<NamedEntity> testContainers = null;
	int testContainerId;
	Collection<TestPlanningNode> tests = null;
*/	int testId;

	boolean isProjectPresent = false;
	boolean isTestContainerPresent = false;
	boolean isTestPresent = false;

	public SilkIntegrator()
	{
		super();
		frameworkParameters.setRelativePath(CraftUtility.getRelativePath());
		properties = Settings.getInstance();
	}

	public ScriptHelper getScriptHelper()
	{
		return scriptHelper;
	}

	public void setScriptHelper(ScriptHelper scriptHelper)
	{
		this.scriptHelper = scriptHelper;
	}

	private ReportSettings getReportSettings()
	{
		ReportSettings reportSettings = (ReportSettings) scriptHelper.getValueFromSessionThreadData("Craft_ReportSettings");
		return reportSettings;
	}

	private String craftReportsPath()
	{
		ReportSettings reportSettings = getReportSettings();
		return reportSettings.getReportPath();
	}

	private String getSilkTestResultsDirectory()
	{
		return silkTestResultsDirectoryPath;
	}

	private String getSilkCraftTestResultsDirectory()
	{
		return getSilkTestResultsDirectory() + Util.getFileSeparator() + "CraftResults";
	}

	public void uploadTestCaseResults()
	{
		if (getSilkTestResultsDirectory() != null)
		{
			copyHtmlTestCaseReport();
			copyExcelTestCaseReport();
			copyScreenshots();
			copyDataTables();
			copyErrorLogFile();
		}
		else
		{
		}
	}

	public void uploadTestSuiteResults()
	{
		if (getSilkTestResultsDirectory() != null)
		{
			copyHtmlTestSuiteSummaryReport();
			copyExcelTestSuiteSummaryReport();
			copyScreenshots();
			copyDataTables();
			copyErrorLogFile();
		}
		else
		{
		}
	}

	public void uploadCraftResultsAsZip()
	{
		if (getSilkTestResultsDirectory() != null)
		{
			createCraftResultsZip();
		}
		else
		{
		}
	}

	private void createCraftResultsZip()
	{
		generateFileList(craftReportsPath(), new File(craftReportsPath()));
		zipIt(craftReportsPath(), getSilkTestResultsDirectory() + Util.getFileSeparator() + "CraftResults.zip");
	}

	private void copyHtmlTestSuiteSummaryReport()
	{
		try
		{
			String htmlTestSuiteSummaryReport =
				craftReportsPath() + Util.getFileSeparator() + "HTML Results" + Util.getFileSeparator() + "Summary" + ".html";

			String htmlSilkTestSuiteSummaryReport =
				getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "HTML Results" + Util.getFileSeparator()
					+ "Summary" + ".html";

			FileUtils.copyFile(new File(htmlTestSuiteSummaryReport), new File(htmlSilkTestSuiteSummaryReport));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void copyHtmlTestCaseReport()
	{
		try
		{
			ReportSettings reportSettings = getReportSettings();
			String htmlTestCaseReport =
				craftReportsPath() + Util.getFileSeparator() + "HTML Results" + Util.getFileSeparator()
					+ reportSettings.getReportName() + ".html";

			String htmlSilkTestCaseReport =
				getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "HTML Results" + Util.getFileSeparator()
					+ reportSettings.getReportName() + ".html";

			FileUtils.copyFile(new File(htmlTestCaseReport), new File(htmlSilkTestCaseReport));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void copyExcelTestSuiteSummaryReport()
	{
		try
		{
			String excelTestSuiteSummaryReport =
				craftReportsPath() + Util.getFileSeparator() + "Excel Results" + Util.getFileSeparator() + "Summary" + ".xls";

			String excelSilkTestSuiteSummaryReport =
				getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "Excel Results" + Util.getFileSeparator()
					+ "Summary" + ".xls";

			FileUtils.copyFile(new File(excelTestSuiteSummaryReport), new File(excelSilkTestSuiteSummaryReport));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void copyExcelTestCaseReport()
	{
		try
		{
			ReportSettings reportSettings = getReportSettings();
			String excelTestCaseReport =
				craftReportsPath() + Util.getFileSeparator() + "Excel Results" + Util.getFileSeparator()
					+ reportSettings.getReportName() + ".xls";

			String excelSilkTestCaseReport =
				getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "Excel Results" + Util.getFileSeparator()
					+ reportSettings.getReportName() + ".xls";

			FileUtils.copyFile(new File(excelTestCaseReport), new File(excelSilkTestCaseReport));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void copyScreenshots()
	{
		try
		{
			String screenShotsFolder = craftReportsPath() + Util.getFileSeparator() + "Screenshots";

			String silkScreenShotsFolder = getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "Screenshots";

			FileUtils.copyDirectory(new File(screenShotsFolder), new File(silkScreenShotsFolder));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void copyDataTables()
	{
		try
		{
			String datatablesFolder = craftReportsPath() + Util.getFileSeparator() + "Datatables";

			String silkDatatablesFolder = getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "Datatables";

			FileUtils.copyDirectory(new File(datatablesFolder), new File(silkDatatablesFolder));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void copyErrorLogFile()
	{
		try
		{
			String errorLogFilePath = craftReportsPath() + Util.getFileSeparator() + "ErrorLog.txt";

			String silkErrorLogFilePath = getSilkCraftTestResultsDirectory() + Util.getFileSeparator() + "ErrorLog.txt";

			FileUtils.copyFile(new File(errorLogFilePath), new File(silkErrorLogFilePath));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Zip it
	 * 
	 * @param zipFile
	 *            output ZIP file location
	 */
	public void zipIt(String sourceFolder, String zipFile)
	{

		byte[] buffer = new byte[1024];

		try
		{

			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			System.out.println("Output to Zip : " + zipFile);

			for (String file : this.fileList)
			{

				System.out.println("File Added : " + file);
				ZipEntry ze = new ZipEntry(file);
				zos.putNextEntry(ze);

				FileInputStream in = new FileInputStream(sourceFolder + File.separator + file);

				int len;
				while ((len = in.read(buffer)) > 0)
				{
					zos.write(buffer, 0, len);
				}

				in.close();
			}

			zos.closeEntry();
			zos.close();

			System.out.println("Done");
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * Traverse a directory and get all files, and add the file into fileList
	 * 
	 * @param node
	 *            file or directory
	 */
	public void generateFileList(String sourceFolder, File node)
	{

		if (node.isFile())
		{
			fileList.add(generateZipEntry(sourceFolder, node.getAbsoluteFile().toString()));
		}

		if (node.isDirectory())
		{
			String[] subNote = node.list();
			for (String filename : subNote)
			{
				generateFileList(sourceFolder, new File(node, filename));
			}
		}

	}

	/**
	 * Format the file path for zip
	 * 
	 * @param file
	 *            file path
	 * @return Formatted file path
	 */
	private String generateZipEntry(String sourceFolder, String file)
	{
		return file.substring(sourceFolder.length() + 1, file.length());
	}

/*	public void processSilkCentral(String username, String pwd, String projectToWorkOn, String testContainerToWorkOn)
		throws Exception
	{
		processSilkCentral(username, pwd, projectToWorkOn, testContainerToWorkOn, null);
	}*/

/*	public void processSilkCentral(String username, String pwd, String projectToWorkOn, String testContainerToWorkOn,
		String testToWorkOn) throws Exception
	{
		String hostName = "silkcentral.corp.bayadv";
		intializeSilkWebServiceClient(hostName, username, pwd);
		allProjects = listAvailableProjects();

		isProjectPresent = checkProjectAvailable(projectToWorkOn);
		if (isProjectPresent)
		{
			testContainers = listAvailableTestContainers();
		}
		else
		{
		}

		isTestContainerPresent = checkTestContainerAvailable(testContainerToWorkOn);
		if (isTestContainerPresent)
		{
			tests = listAvailableTests();
		}
		else
		{
			logger.error("Failure: " + testContainerToWorkOn + " is not present under " + projectToWorkOn + " in silk central");
		}

		if (testToWorkOn != null)
		{
			isTestPresent = checkTestAvailable(testToWorkOn);
			if (isTestPresent)
			{
					+ projectToWorkOn + " in silk central");
			}
			else
			{
			}
		}
	}*/

/*	private void intializeSilkWebServiceClient(String hostName, String username, String pwd) throws Exception
	{
		ConnectionHandler connectionHandler = new ConnectionHandler();
		loginContext = connectionHandler.login(hostName + "/Services1.0", username, pwd);

		planningHandler = new TestPlanningHandler(loginContext);
	}

	private Collection<Project> listAvailableProjects() throws Exception
	{
		Collection<Project> allProjects = planningHandler.getProjects();


		for (Project project : allProjects)
		{
			String currProjName = project.getName();
		}
		return allProjects;
	}*/

/*	private boolean checkProjectAvailable(String projectToWorkOn) throws Exception
	{
		boolean processFlag = false;
		for (Project project : allProjects)
		{
			String currProjName = project.getName();
			if (projectToWorkOn.equals(currProjName))
			{
				planningHandler.setCurrentProject(project);
				processFlag = true;
				break;
			}
		}
		return processFlag;
	}*/

/*	private Collection<NamedEntity> listAvailableTestContainers() throws Exception
	{
		Collection<NamedEntity> testContainers = planningHandler.getTestContainers();
		for (NamedEntity container : testContainers)
		{
		}
		return testContainers;
	}*/

/*	private boolean checkTestContainerAvailable(String testContainerToWorkOn) throws Exception
	{
		boolean processFlag = false;
		for (NamedEntity container : testContainers)
		{
			String currTestContainerName = container.getName();
			if (testContainerToWorkOn.equals(currTestContainerName))
			{
				testContainerId = container.getId();
				processFlag = true;
				break;
			}
		}
		return processFlag;
	}*/

/*	private Collection<TestPlanningNode> listAvailableTests() throws Exception
	{
		Collection<TestPlanningNode> childNodes = planningHandler.getChildNodes(testContainerId);
		for (TestPlanningNode node : childNodes)
		{
		}
		return childNodes;
	}*/

/*	private boolean checkTestAvailable(String testToWorkOn) throws Exception
	{
		boolean processFlag = false;
		for (TestPlanningNode node : tests)
		{
			String currTestName = node.getName();
			if (testToWorkOn.equals(currTestName))
			{
				testId = node.getId();
				processFlag = true;
				break;
			}
		}
		return processFlag;
	}*/

/*	private int getExistingTestAvailable(String testToWorkOn) throws Exception
	{
		int testId = 0;
		for (TestPlanningNode node : tests)
		{
			String currTestName = node.getName();
			if (testToWorkOn.equals(currTestName))
			{
				testId = node.getId();
				break;
			}
		}
		return testId;
	}
*/
/*	public void listProperties() throws Exception
	{
		Collection<PropertyValue> propertyValues = planningHandler.getPropertyValues(testId);
		for (PropertyValue propertyValue : propertyValues)
		{
		}
	}
*/
/*	public void addNewSilkJunitTest() throws Exception
	{
		int newId =
			planningHandler.addJunitTestNode(testContainerId, "WebserviceJunitTest", "WebserviceJunitTest",
				"testscripts.Scenario_CF_PPSR.TC_Comprehensive_Test", "./lib/*;./bin");
		boolean flag = planningHandler.deleteJunitTestNode(newId);
		newId =
			planningHandler.addJunitTestNode(testContainerId, "WebserviceJunitTest", "WebserviceJunitTest",
				"testscripts.Scenario_CF_PPSR.TC_Comprehensive_Test", "./lib/*;./bin");
	}*/

	public void addSilkTests(String username, String pwd, String projectToWorkOn, String testContainerToWorkOn)
	{
		try
		{
/*			if (username == null)
			{
				processSilkCentral(properties.getProperty("silkCentralUserName"), properties.getProperty("silkCentralPassword"),
					properties.getProperty("silkCentralProjectName"), properties.getProperty("silkCentralTestContainerName"));
			}
			else
			{
				processSilkCentral(username, pwd, properties.getProperty("silkCentralProjectName"),
					properties.getProperty("silkCentralTestContainerName"));
			}
*/
			File datatablesFolder = new File(frameworkParameters.getRelativePath() + Util.getFileSeparator() + "Datatables");
			File[] packageFiles = datatablesFolder.listFiles();
			for (int i = 0; i < packageFiles.length; i++)
			{
				File packageFile = packageFiles[i];
				String fullFileName = packageFile.getName();
				String scenarioFileName = FilenameUtils.removeExtension(fullFileName);
				String fileExt = FilenameUtils.getExtension(fullFileName);

				if (!scenarioFileName.equalsIgnoreCase("Common Testdata")
					&& (fileExt.equalsIgnoreCase("xls") || fileExt.equalsIgnoreCase("xlsx")))
				{
					List<String> uniqueTestCases = new ArrayList<String>();
					ExcelDataAccess scenarioSheetAccess =
						new ExcelDataAccess(frameworkParameters.getRelativePath() + Util.getFileSeparator() + "Datatables",
							scenarioFileName);
					scenarioSheetAccess.setDatasheetName("Business_Flow");

					int nTestInstances = scenarioSheetAccess.getLastRowNum();
					for (int currentTestInstance = 1; currentTestInstance <= nTestInstances; currentTestInstance++)
					{
						String currentScenario = scenarioFileName.trim();
						String currentTestcase = scenarioSheetAccess.getValue(currentTestInstance, "TC_ID");
						if (currentTestcase != null && currentTestcase.trim().length() > 0
							&& !uniqueTestCases.contains(currentTestcase))
						{
							uniqueTestCases.add(currentTestcase);
							String testclassName =
								"testscripts." + currentScenario.toLowerCase() + "." + currentTestcase + "_CraftJunit";
							//int existingTestId = getExistingTestAvailable("Craft_Junit_" + currentTestcase);
							/*if (existingTestId != 0)
							{
								//boolean flag = planningHandler.deleteJunitTestNode(existingTestId);
								//logger.info("Deleted Old Test Case with:" + existingTestId + " & is deleted:" + flag);
							}else{
								int newId =
									planningHandler.addJunitTestNode(testContainerId, "Craft_Junit_" + currentTestcase,
										"Craft_Junit_" + currentTestcase + " created from craft application daemon", testclassName,
										properties.getProperty("silkCentralJunitClassPath"));
							}*/
							createNewJunitTestNGTestCaseSource(currentScenario, currentTestcase, "Junit");
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void createNewJunitTestNGTestCaseSource(String currentScenario, String currentTestcase, String testCaseType)
		throws Exception
	{
		String templateFileName =
			CraftUtility.getRelativePath() + "/src/supportlibraries/" + testCaseType + "TestCaseTemplate.txt";
		String newJavaFileName =
			CraftUtility.getRelativePath() + "/src/testscripts/" + currentScenario.toLowerCase() + "/" + currentTestcase
				+ "_Craft" + testCaseType + ".java";

		Map<String, Object> valuesByKey = new HashMap<String, Object>();
		valuesByKey.put("scenario_folder_name", currentScenario.toLowerCase());
		valuesByKey.put("testcase_name", currentTestcase + "_Craft" + testCaseType);
		valuesByKey.put("excel_scenario_name", currentScenario);
		valuesByKey.put("excel_testcase_name", currentTestcase);

		Collection<String> javafileContent = new ArrayList<String>();

		BufferedReader br = null;
		br = new BufferedReader(new FileReader(templateFileName));
		String line;
		while ((line = br.readLine()) != null)
		{
			if (line.contains("{scenario_folder_name}") || line.contains("{testcase_name}")
				|| line.contains("{excel_scenario_name}") || line.contains("{excel_testcase_name}"))
			{
				line = CraftUtility.replaceTokens(line, valuesByKey);
			}
			javafileContent.add(line);
		}

		File newJavaFile = new File(newJavaFileName);
		FileUtils.deleteQuietly(newJavaFile);
		newJavaFile = new File(newJavaFileName);
		FileUtils.writeLines(newJavaFile, javafileContent);

		if (br != null) br.close();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{

		String username = null;
		String pwd = null;
		String projectToWorkOn = null;
		String testContainerToWorkOn = null;

		//if (args != null && args.length == 2)
		//{
			//username = args[0];
			//pwd = args[1];
			SilkIntegrator obj = new SilkIntegrator();
			obj.addSilkTests(username, pwd, projectToWorkOn, testContainerToWorkOn);
			obj.addSilkTests("Sam", "gelli", projectToWorkOn, testContainerToWorkOn);
		//}
		//else
		//{
			//System.out.println("Usage: AddSilkTests <Silk Central User Name> <Silk Central Password>");
		//}
	}
}
