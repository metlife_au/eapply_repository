package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesAddkidPageObjects extends PageObjectLibrary {
	public static final By selRelation = getObjectIdentifaction("coles.addkid.relation.dropdown.css");
	public static final By txtKFname = getObjectIdentifaction("coles.addkid.kfname.txt.css");
	public static final By txtKLname = getObjectIdentifaction("coles.addkid.klname.txt.css");
	public static final By txtKDOB = getObjectIdentifaction("coles.addkid.Kdob.txt.css");
	public static final By radKgenMale = getObjectIdentifaction("coles.addkid.kgender.male.radio.css");
	public static final By lnkContinue = getObjectIdentifaction("coles.addkid.continue.link.css");
	public static final By chkDiagnosed = getObjectIdentifaction("coles.addkid.kidcover.diagnosed.chkbox.css");
	public static final By rdkidney = getObjectIdentifaction("coles.addkid.kidcover.kidney.radio.css");
	public static final By rdDrug = getObjectIdentifaction("coles.addkid.kidcover.drug.radio.css");
	public static final By rdHospital = getObjectIdentifaction("coles.addkid.kidcover.hospitalisation.radio.css");
	public static final By lnkApply = getObjectIdentifaction("coles.addkid.apply.link.css");
		
}
