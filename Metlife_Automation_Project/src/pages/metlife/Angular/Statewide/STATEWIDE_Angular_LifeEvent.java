package pages.metlife.Angular.Statewide;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class STATEWIDE_Angular_LifeEvent extends MasterPage {
	WebDriverUtil driverUtil = null;

	public STATEWIDE_Angular_LifeEvent LifeEvent_STATEWIDE() {
		LifeEventSTATEWIDE();
		HealthandLifeStyle();
		confirmation();
		
		return new STATEWIDE_Angular_LifeEvent(scriptHelper);
	}
	
	public STATEWIDE_Angular_LifeEvent LifeEventNegative_STATEWIDE() {
		LifeEventSTATEWIDEPLUSNegativeFlow();
		
		return new STATEWIDE_Angular_LifeEvent(scriptHelper);
	}

	public STATEWIDE_Angular_LifeEvent(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void LifeEventSTATEWIDE() {

		String EmailId = dataTable.getData("STATEWIDE_LifeEvent", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_LifeEvent", "TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_LifeEvent", "ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_LifeEvent", "TimeofContact");		
		String RegularIncome = dataTable.getData("STATEWIDE_LifeEvent", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("STATEWIDE_LifeEvent", "RegularIncome");
		String WorkLimitation = dataTable.getData("STATEWIDE_LifeEvent", "RegularIncome");
		String Citizen = dataTable.getData("STATEWIDE_LifeEvent", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_LifeEvent", "IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_LifeEvent", "OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_LifeEvent", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_LifeEvent", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("STATEWIDE_LifeEvent", "TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_LifeEvent", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_LifeEvent", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("STATEWIDE_LifeEvent", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("STATEWIDE_LifeEvent", "LifeEvent_Date");
		String LifeEvent_PrevApplyin1Year = dataTable.getData("STATEWIDE_LifeEvent", "LifeEvent_PrevApplyin1Year");
		String DocumentaryEvidence = dataTable.getData("STATEWIDE_LifeEvent", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("STATEWIDE_LifeEvent", "AttachPath");
		String Negative_Scenario=dataTable.getData("STATEWIDE_LifeEvent", "Negative_Scenario");
		String AnnualSalary = dataTable.getData("STATEWIDE_LifeEvent", "AnnualSalary");
		
		try{
			
			
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,450)", "");
				try {
				  Thread.sleep(2000);
				} catch (InterruptedException e) {
				  // TODO Auto-generated catch block
				  e.printStackTrace();
				}
			
			
			//****Click on Life Event Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[text()='Increase your  cover due to a life event ']"))).click();
			
			report.updateTestLog("Life Event", "Life Event Link is Clicked", Status.PASS);
		
			sleep(3000);
			
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyCkBoxLblId']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(300);

			//*****Click on the Contact Number Type****\\			
				

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);
				
				
				//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
				
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}		
										
				//****Contact Details-Continue Button*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(contactDetailsLifeEventForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);	
			
			
			
			//*****Resident of Australia****\\	
			
			if(Citizen.equalsIgnoreCase("Yes")){	
				
				driver.findElement(By.xpath("//*[@id='collapseTwo']/form/div/div[1]/div[2]/div/div[1]/label/span")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
					
				}	
			else{	
				driver.findElement(By.xpath("//*[@id='collapseTwo']/form/div/div[1]/div[2]/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
					
				}	
			
			
			
			
//*****Select the industry type*******\\	
			
			Select Industry=new Select(driver.findElement(By.name("lifeEventIndustry")));		
			Industry.selectByVisibleText(IndustryType);			
			report.updateTestLog("Industry Type", "Preferred Industry Type: "+IndustryType+" is Selected", Status.PASS);
			sleep(250);
				
			//*****Select the occupation type*******\\	
			
			Select Occupation=new Select(driver.findElement(By.name("lifeEventOccupation")));		
			Occupation.selectByVisibleText(OccupationType);			
			report.updateTestLog("Occupation Type", "Preferred Industry Type: "+OccupationType+" is Selected", Status.PASS);
			sleep(250);			
										
//*****If other occupation type*******\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
				
				wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}								
		    	

			//*****Extra Questions*****\\		--------------Xpath not changed for below					
			
		    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}							
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
									
					///xpath not changed				
									
		//******Do you work in hazardous environment*****\\	 --xpath changed	
			
		if(HazardousEnv.equalsIgnoreCase("Yes")){							
			                                                              
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}							
		else{							
																			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}							
									
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\	 --xpath changed
		
		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}							
		}							
							    
          sleep(800);
		   
        //*****What is your annual Salary******							
			
			
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
									
		
		
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		sleep(2500);

				
	  		  //********** Are you currently able to carry out all the identifiable duties of your current employment 
	  			//***		    on an ongoing basis for 35 hours per week *******//	
		/*		
	  					    if(WorkLimitation.equalsIgnoreCase("Yes")){							
	  						       
	  				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[3]/div[2]/div/div[1]/label/span"))).click();
	  							report.updateTestLog("35 hours per week", "Selected Yes", Status.PASS);
	  				}							
	  				   else{							
	  				       
	  					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[3]/div[2]/div/div[2]/label/span"))).click();
	  						report.updateTestLog("35 hours per week", "Selected No", Status.PASS);
	  				}		
	  					    
			
			
			
		
							
				
					
				//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
					
					if(RegularIncome.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Yes is Selected", Status.PASS);
							
					}		
					else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "No is Selected", Status.PASS);
							
					}	
					
					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
							sleep(800);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
							sleep(800);
						}
					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
							report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
							sleep(800);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
							report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
							sleep(800);
						}
					}					
					
						
			
				   
								
				//*****Click on Continue*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);				
				sleep(1000);
				
				*/
				
				//********************************************************************************************************\\
				//****************************LIFE EVENT SECTION***********************************************************\\
				//*********************************************************************************************************\\
				
				
				//*****Please select the specific life event you are applying under to increase your cover*****\\
				
				Select Event=new Select(driver.findElement(By.name("event")));
				Event.selectByVisibleText(LifeEvent);				
				sleep(500);
				report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);
				
             if(Negative_Scenario.equalsIgnoreCase("No")){
            	 
            	//*****What is the date of the life event?*******\\					

					driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
					sleep(500);
					driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
					report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
					sleep(800);
			
			
			//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months 
			//within this calendar year?********************\\
			
			if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseThree']/form/div/div[4]/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
				sleep(800);	
				
			}
			
			else if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseThree']/form/div/div[4]/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","No is Selected", Status.PASS);
				sleep(800);	
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseThree']/form/div/div[4]/div[2]/div/div[3]/label/span"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Unsure is Selected", Status.PASS);
				sleep(800);	
			}
			
             }

			   
				//******Do you wish to include documentary evidence of your life event?******\\
				
				if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
					
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 pl0 col-xs-12']/label/span"))).click();
					report.updateTestLog("Documentary Evidence","Yes is Selected", Status.PASS);
					sleep(800);	
				
					
					//****Attach the file******		
					
					driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
					sleep(1500);
					
					//*****Let the attachment load*****\\		
					driver.findElement(By.xpath("//label[contains(text(),'Please include documentary evidence of your event. You are required to upload: ')]")).click();
					sleep(1000);
					
					//*****Add the Attachment*****\\
					
					WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
					sleep(1500);
					report.updateTestLog("Evidence Attachment", "File is successfully attached", Status.PASS);
									
				
				}
				else{
					
					//****No Attachment******\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12 pl0xs']/label/span"))).click();
					report.updateTestLog("Documentary Evidence","No is Selected", Status.PASS);
					sleep(800);	
					
				}
				
				//*****Click on Continue*******\\
				
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(lifeEvent);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
				sleep(8000);			
			 
	


	}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
	

		
	public void HealthandLifeStyle() {

		String Restrictedillness=dataTable.getData("STATEWIDE_LifeEvent", "Restrictedillness");
		String PriorClaim=dataTable.getData("STATEWIDE_LifeEvent", "PriorClaim");
		String Diagnosedillness=dataTable.getData("STATEWIDE_LifeEvent", "Diagnosedillness");
		String DeclinedApplication=dataTable.getData("STATEWIDE_LifeEvent", "DeclinedApplication");
		
		
		
		
		try{
			if(driver.getPageSource().contains("Eligibility Check")){
				
			WebDriverWait wait=new WebDriverWait(driver,18);
						
  //******Are you restricted, due to illness or injury from carrying out any of the identifiable duties*****\\
			
			
			if(Restrictedillness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[2]/div/div[2]/div[1]/div/label/span"))).click();
				report.updateTestLog("Restricted illness", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[2]/div/div[2]/div[2]/div/label/span"))).click();
				report.updateTestLog("Restricted illness", "No is Selected", Status.PASS);
			}
			sleep(800);
			
//******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits*****\\
			
			
			if(PriorClaim.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[3]/div/div[2]/div[1]/div/label/span"))).click();
				report.updateTestLog("Prior Claim", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[3]/div/div[2]/div[2]/div/label/span"))).click();
				report.updateTestLog("Prior Claim", "No is Selected", Status.PASS);
			}
			sleep(800);
			
//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\
			
			
			if(Diagnosedillness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[4]/div/div[2]/div[1]/div/label/span"))).click();
				report.updateTestLog("Diagnosed illness", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[4]/div/div[2]/div[2]/div/label/span"))).click();
				report.updateTestLog("Diagnosed illness", "No is Selected", Status.PASS);
			}
			sleep(800);
			
//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\
			
			
			if(DeclinedApplication.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[5]/div/div[2]/div[1]/div/label/span"))).click();
				report.updateTestLog("Declined Application", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[5]/div/div[2]/div[2]/div/label/span"))).click();
				report.updateTestLog("Declined Application", "No is Selected", Status.PASS);
			}
			sleep(800);
			
            //****Click on Continue****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(3000);
			
			
			}			
		}
	
		catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void confirmation() {
		
		WebDriverWait wait=new WebDriverWait(driver,20);	
			
			try{
			//	if(driver.getPageSource().contains("I agree to all the above Terms & Conditions "))				
				
				

					//******Agree to Terms & Conditions *******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span"))).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(500);
					
					
				//******Click on Submit*******\\
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
					report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
					sleep(1000);			
					
				
				
				//*******Feedback popup******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
				report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
				sleep(1000);
				
				if(driver.getPageSource().contains("APPLICATION NUMBER")){
					
				
				//*****Fetching the Application Status*****\\
				
				String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
				sleep(500);
				
				//*****Fetching the Application Number******\\
				
				String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
				sleep(1000);
				
				
				
				//******Download the PDF***********\\
				properties = Settings.getInstance();	
				String StrBrowseName=properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Chrome")){
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
					sleep(1500);
		  		}			
				
				
			  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
			  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			  			sleep(1500);
			  			Robot roboobj=new Robot();
			  			roboobj.keyPress(KeyEvent.VK_ENTER);	
			  		}
			  		
			  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
			  			Robot roboobj=new Robot();
			  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			  				
			  			sleep(4000);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_ENTER);	
			  			
			  		}
				
				report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
				
				sleep(1000);
			
				 
				  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);
				
					
				 report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);	
				}

				else{
					 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
				}
				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
		}


		
	public void LifeEventSTATEWIDEPLUSNegativeFlow() {
		
		
		String EmailId = dataTable.getData("STATEWIDE_LifeEvent", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_LifeEvent", "TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_LifeEvent", "ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_LifeEvent", "TimeofContact");		
		String RegularIncome = dataTable.getData("STATEWIDE_LifeEvent", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("STATEWIDE_LifeEvent", "RegularIncome");
		String WorkLimitation = dataTable.getData("STATEWIDE_LifeEvent", "RegularIncome");
		String Citizen = dataTable.getData("STATEWIDE_LifeEvent", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_LifeEvent", "IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_LifeEvent", "OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_LifeEvent", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_LifeEvent", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("STATEWIDE_LifeEvent", "TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_LifeEvent", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_LifeEvent", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("STATEWIDE_LifeEvent", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("STATEWIDE_LifeEvent", "LifeEvent_Date");
		String AnnualSalary = dataTable.getData("STATEWIDE_LifeEvent", "AnnualSalary");
		String Negative_Scenario=dataTable.getData("STATEWIDE_LifeEvent", "Negative_Scenario");
		String dateerror=dataTable.getData("STATEWIDE_LifeEvent", "dateerror");
		String Lifeeventerror=dataTable.getData("STATEWIDE_LifeEvent", "Lifeeventerror");
		
		

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,450)", "");
		try {
		  Thread.sleep(2000);
		} catch (InterruptedException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		}
	
	
	//****Click on Life Event Link*****\\
	
	WebDriverWait wait=new WebDriverWait(driver,18);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[text()='Increase your  cover due to a life event ']"))).click();
	
	report.updateTestLog("Life Event", "Life Event Link is Clicked", Status.PASS);

	//*****Agree to Duty of disclosure*******\\
	
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
	sleep(250);
	report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

	
   //*****Agree to Privacy Statement*******\\

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyCkBoxLblId']/span"))).click();
	sleep(250);
	report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
	
	//*****Enter the Email Id*****\\	
	
	wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).clear();
	sleep(250);
	wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).sendKeys(EmailId);
	report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
	sleep(300);

	//*****Click on the Contact Number Type****\\			
		

	Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
	Contact.selectByVisibleText(TypeofContact);			
	report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
	sleep(250);
		
		
		//****Enter the Contact Number****\\	
	
	sleep(250);
	wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
	sleep(500);
	wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(ContactNumber);
	sleep(250);
	report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
		
		//***Select the Preferred time of Contact*****\\			
	
	if(TimeofContact.equalsIgnoreCase("Morning")){			
		
		driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span")).click();
		
		sleep(250);
		report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
	}			
	else{			
		driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span")).click();
		sleep(250);
		report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
	}		
								
		//****Contact Details-Continue Button*****\\	
	
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(contactDetailsLifeEventForm);']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
	sleep(1200);	
	
	
	
	//*****Resident of Australia****\\	
	
	if(Citizen.equalsIgnoreCase("Yes")){	
		
		driver.findElement(By.xpath("//*[@id='collapseTwo']/form/div/div[1]/div[2]/div/div[1]/label/span")).click();
		sleep(250);
		report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
			
		}	
	else{	
		driver.findElement(By.xpath("//*[@id='collapseTwo']/form/div/div[1]/div[2]/div/div[2]/label/span")).click();
		sleep(250);
		report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
			
		}	
	
	
	
	
//*****Select the industry type*******\\	
	
	Select Industry=new Select(driver.findElement(By.name("lifeEventIndustry")));		
	Industry.selectByVisibleText(IndustryType);			
	report.updateTestLog("Industry Type", "Preferred Industry Type: "+IndustryType+" is Selected", Status.PASS);
	sleep(250);
		
	//*****Select the occupation type*******\\	
	
	Select Occupation=new Select(driver.findElement(By.name("lifeEventOccupation")));		
	Occupation.selectByVisibleText(OccupationType);			
	report.updateTestLog("Occupation Type", "Preferred Industry Type: "+OccupationType+" is Selected", Status.PASS);
	sleep(250);			
								
//*****If other occupation type*******\\	
	
	if(OccupationType.equalsIgnoreCase("Other")){		
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
		report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
		sleep(1000);	
	}								
    	

	//*****Extra Questions*****\\		--------------Xpath not changed for below					
	
    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
    
							
//***Select if your office Duties are Undertaken within an Office Environment?\\	
    	
    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
   
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[1]/label/span"))).click();
		report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
							
}							
else{							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[2]/label/span"))).click();
	report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
							
}							
							
      //*****Do You Hold a Tertiary Qualification******\\							
							
        if(TertiaryQual.equalsIgnoreCase("Yes")){							
       
        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
}							
   else{							
       
	   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
		report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
}							
}							
							
							
							
else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
							
			///xpath not changed				
							
//******Do you work in hazardous environment*****\\	 --xpath changed	
	
if(HazardousEnv.equalsIgnoreCase("Yes")){							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span"))).click();
	report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
}							
else{							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span"))).click();
	report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
}							
							
//******Do you spend more than 10% of your working time outside of an office environment?*******\\	 --xpath changed

if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span"))).click();
	report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
}							
else{							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span"))).click();
	report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
}							
}							
					    
  sleep(800);
   
//*****What is your annual Salary******							
	
	
    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
    sleep(500);		
    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
							


//*****Click on Continue*******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
sleep(2500);

		
		  //********** Are you currently able to carry out all the identifiable duties of your current employment 
			//***		    on an ongoing basis for 35 hours per week *******//	
/*		
					    if(WorkLimitation.equalsIgnoreCase("Yes")){							
						       
				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[3]/div[2]/div/div[1]/label/span"))).click();
							report.updateTestLog("35 hours per week", "Selected Yes", Status.PASS);
				}							
				   else{							
				       
					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div[3]/div[2]/div/div[2]/label/span"))).click();
						report.updateTestLog("35 hours per week", "Selected No", Status.PASS);
				}		
					    
	
	
	

					
		
			
		//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
			
			if(RegularIncome.equalsIgnoreCase("Yes")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
				sleep(500);
				report.updateTestLog("Regular Income Question", "Yes is Selected", Status.PASS);
					
			}		
			else{		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
				sleep(500);
				report.updateTestLog("Regular Income Question", "No is Selected", Status.PASS);
					
			}	
			
			//******Extra Question based on Regular Income*******\\
			sleep(500);
			if(RegularIncome.equalsIgnoreCase("Yes")){
				if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
					report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
					sleep(800);
				}
				else{
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
					report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
					sleep(800);
				}
			}
			else{
				if(WorkLimitation.equalsIgnoreCase("Yes")){
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]"))).click();
					report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
					sleep(800);
				}
				else{
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]"))).click();
					report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
					sleep(800);
				}
			}					
			
				
	
		   
						
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);				
		sleep(1000);
		
		*/
		
		//********************************************************************************************************\\
		//****************************LIFE EVENT SECTION***********************************************************\\
		//*********************************************************************************************************\\
		
		
		//*****Please select the specific life event you are applying under to increase your cover*****\\
		
		Select Event=new Select(driver.findElement(By.name("event")));
		Event.selectByVisibleText(LifeEvent);				
		sleep(500);
		report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);
		
		 if(Negative_Scenario.equalsIgnoreCase("DateError")){
         	
         	//*****What is the date of the life event?*******\\		
         	
         	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
				sleep(1000);
				driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
				report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
				sleep(800);
				
				
				if(driver.getPageSource().contains(dateerror)){		
					report.updateTestLog("Life Event Date Error Message", dateerror+" is Displayed", Status.PASS);
				}
				
				else{
					report.updateTestLog("Life Event Date Error Message", "Error Message is not Displayed", Status.FAIL);
				}
         }
				
         else if(Negative_Scenario.equalsIgnoreCase("PriorEventError")){
         	

         	//*****What is the date of the life event?*******\\		
         	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
				sleep(500);
				driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
				report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
				sleep(800);
         	
         	//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months 
				//within this calendar year?********************\\
         	
         	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseThree']/form/div/div[4]/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
				sleep(800);	
				
				
				sleep(500);
				if(driver.getPageSource().contains(Lifeeventerror)){
					report.updateTestLog("Life Event Error Message",Lifeeventerror+" is Displayed", Status.PASS);
				}
				
				else{
					report.updateTestLog("Life Event Error Message", "Error Message is not Displayed", Status.FAIL);
				}
         }		

		 
		 
		 if(Negative_Scenario.equalsIgnoreCase("Nodocument")){
	         	
	         	//***** No documentary evidence provided*******\\		
	         	
	         	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
					sleep(1000);
					driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
					report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
					sleep(800);
					
					//***** Life event no should be selected*******\\	
					
				 	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseThree']/form/div/div[4]/div[2]/div/div[2]/label/span"))).click();
					report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
					sleep(800);	
					
					
					//***** No documentary evidence provided radio button*******\\	
	         	
	         	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseThree']/form/div/div[5]/div[2]/div/div[2]/label/span"))).click();
					report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
					sleep(800);	
					
					
					
					if(driver.getPageSource().contains(Lifeeventerror)){		
						report.updateTestLog("Life Event Date Error Message", dateerror , Status.PASS);
					}
					
					else{
						report.updateTestLog("Life Event Date Error Message", "Error Message is not Displayed", Status.FAIL);
					}
	         }
     }
	}
	

	
