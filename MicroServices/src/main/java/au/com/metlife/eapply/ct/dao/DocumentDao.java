/**
 * 
 */
package au.com.metlife.eapply.ct.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import au.com.metlife.eapply.ct.model.DocumentBO;

/**
 * @author akishore
 *
 */
@Transactional
public interface DocumentDao extends CrudRepository<DocumentBO, Long> {
	
	/*public Document findBySecurestring( String securestring);	*/
	public DocumentBO findById(Long id);
	/*public void saveDocument(Document document);*/

}
