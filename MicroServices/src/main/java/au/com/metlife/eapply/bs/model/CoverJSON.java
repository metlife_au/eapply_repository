package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class CoverJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7773644035666744831L;

	private String amount;
	
	private String benefitPeriod;
	
	private String benefitType;
	
	private String coverStartDate;
	
	private String coverUnit;
	
	private String description;
	
	private String exclusions;
	
	private String loading;
	
	private String occRating;
	
	private String type;
	
	private String units;
	
	private String waitingPeriod;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBenefitPeriod() {
		return benefitPeriod;
	}

	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	public String getBenefitType() {
		return benefitType;
	}

	public void setBenefitType(String benefitType) {
		this.benefitType = benefitType;
	}

	public String getCoverStartDate() {
		return coverStartDate;
	}

	public void setCoverStartDate(String coverStartDate) {
		this.coverStartDate = coverStartDate;
	}

	public String getCoverUnit() {
		return coverUnit;
	}

	public void setCoverUnit(String coverUnit) {
		this.coverUnit = coverUnit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExclusions() {
		return exclusions;
	}

	public void setExclusions(String exclusions) {
		this.exclusions = exclusions;
	}

	public String getLoading() {
		return loading;
	}

	public void setLoading(String loading) {
		this.loading = loading;
	}

	public String getOccRating() {
		return occRating;
	}

	public void setOccRating(String occRating) {
		this.occRating = occRating;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	

}
