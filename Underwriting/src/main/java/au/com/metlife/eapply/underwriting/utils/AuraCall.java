package au.com.metlife.eapply.underwriting.utils;

import java.io.Serializable;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Date;

import com.rgatp.aura.wsapi.AuraService;
import com.rgatp.aura.wsapi.AuraServiceException;
import com.rgatp.aura.wsapi.AuraServiceProxyLocator;
import com.rgatp.aura.wsapi.Questionnaire;
import com.rgatp.aura.wsapi.Response;



public class AuraCall implements Serializable{
	private static String CLASS_NAME = "au.com.metlife.aura.utils.AuraCall";
	//private static Logger logger = new Logger(CLASS_NAME);
	
	 private String _serviceUrl  = null;
	 private AuraService _auraService = null;
	 private Questionnaire _questionnaire = null; // only used for testing ... TestQuestionCreator.createQuestionnaire()
	 private Exception _lastException     = null;
	 private String    _lastExceptionCmd  = null;
	 private Date      _lastExceptionDate = null;


	 
	 

	 public AuraCall(){
		 try {
			 //hardcoded to E2E
			 _serviceUrl="https://www.e2e.underwriting.metlife.com.au/MetAus/services/AuraService";
		 }catch(Exception e){

		 }
	 }


	  public AuraCall(String port,String add){
	 		 try {
	 			//hardcoded to E2E
	 			_serviceUrl="https://www.e2e.underwriting.metlife.com.au/MetAus/services/AuraService";
	 		 //System.out.println("******************************");
	 		 //System.out.println(_serviceUrl);
	 		 //System.out.println("******************************");
	 		 }catch(Exception e){

	 		 }
	 }
	  public Questionnaire processResponse(String clientName, String questionnaireId, Response[] responses){
		  _auraService = null;
	        // locate our service
	        URL url = null;
	        AuraServiceProxyLocator serviceLocator = new AuraServiceProxyLocator();
	        serviceLocator.setMaintainSession(false); // maintain a stateful session!
	        try {
	        	 url = new URL(_serviceUrl);
		         _auraService = serviceLocator.getAuraService(url);
		         _questionnaire=  _auraService.processResponses(clientName,questionnaireId,responses);
	        }catch (Exception e) {
	        	e.printStackTrace();
	            throw new AuraServiceException("*** ERROR *** encountered in locating web-service: " + e.getMessage());
			}
	        return _questionnaire;
	  }

	 /**
	     * Establish a new "stateful" session with a web-service
	     * identified by the supplied serviceUrl.
	     *
	     * @throws AuraServiceException a problem was encountered in
	     * establishing the new session.
	     */
	    public Questionnaire establishNewSessionToAURA(String inputXml){
	    	// set initial state
	        _auraService = null;
	        // locate our service
	        URL url = null;
	        AuraServiceProxyLocator serviceLocator = new AuraServiceProxyLocator();
	        serviceLocator.setMaintainSession(true); // maintain a stateful session!
	        try {
	            url = new URL(_serviceUrl);
	            _auraService = serviceLocator.getAuraService(url);
	            // ping our service to see if all is good
	            _auraService.testQuestionnaireEncoding(null);
	            _questionnaire = _auraService.initiateQuestionnaire(inputXml);
         
	            return _questionnaire;
	        }
	        catch (Exception e) {	 
	        	e.printStackTrace();
	            throw new AuraServiceException("*** ERROR *** encountered in locating web-service: " + e.getMessage());
	        	//return null;
	        }

	    }


	    /**
	     * Process the command -- init {inputXmlFile}
	     */





	    /**
	     * Process the command -- process
	     */
	    public Questionnaire process(Questionnaire qu) throws RemoteException, AuraServiceException{

	        _questionnaire = _auraService.processResponses(qu.getClientName(),
	        		qu.getQuestionnaireId(),
	        		qu.obtainResponses());
        
	       //qu.getQuestions()[0].obtainResponses()
	        return _questionnaire;

	    }


	    public String cancle(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	return _auraService.cancelQuestionnaire(qu.getClientName(),qu.getQuestionnaireId());
	    }

	    public String saveExit(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	String returnStrign="";
	    	try{
	    	returnStrign= _auraService.saveAndExitQuestionnaire(qu.getClientName(),qu.getQuestionnaireId());
	    	//System.out.println(returnStrign);
	    	}catch(Exception e){
	    		e.printStackTrace();
	    		}
	    	return returnStrign;
	    }

	    public String submit(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	return _auraService.submitQuestionnaire(qu.getClientName(),qu.getQuestionnaireId());
	    }


	    /*public static void main(String args[]){

	    }
*/
		public Questionnaire get_questionnaire() {
			return _questionnaire;
		}

		public void set_questionnaire(Questionnaire _questionnaire) {
			this._questionnaire = _questionnaire;
		}


}
