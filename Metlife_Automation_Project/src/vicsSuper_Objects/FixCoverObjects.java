package vicsSuper_Objects;

import org.openqa.selenium.By;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

/*public class FixCoverObjects {

}
*/

public class FixCoverObjects extends ReusableLibrary {

	public FixCoverObjects(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	// ************************************************************************************************************************
		// -----------------------------------------Personal Details
		// ************************************************************************************************************************
		public static By obj_PersonalDetails_dutyOfDisclosure = By.xpath(".//input[@id='dodlabelCheck']/parent::*");
		public static By obj_PersonalDetails_privacyStatement = By.xpath(".//input[@id='privacylabelCheck']/parent::*");
		// ContactDetails
		public static By obj_PersonalDetails_email = By.name("contactEmail");
		public static By obj_PersonalDetails_contactType = By.xpath("//select[@ng-model='preferredContactType']");
		public static By obj_PersonalDetails_phNO = By.name("contactPhone");
		public static By obj_PersonalDetails_Morning = By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]");
		public static By obj_PersonalDetails_Afternoon = By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]");
		public static By obj_PersonalDetails_Male = By.xpath("(.//input[@name='gender']/parent::*)[1]");
		public static By obj_PersonalDetails_Female = By.xpath("(.//input[@name='gender']/parent::*)[2]");
		public static By obj_Continue_ContactDetail = By.xpath("//*[@ng-click='formOneSubmit(formOne);']");

		// CoverDetails (Indexation Not Handled)
		public static By obj_PersonalDetails_PremiumFrequency = By.name("premFreq");
		public static By obj_DeathIndexationYes = By.xpath("(.//input[@name='indexation']/parent::*)[1]");
		public static By obj_DeathIndexationNo = By.xpath("(.//input[@name='indexation']/parent::*)[2]");
		public static By obj_TPDIndexationYes = By.xpath("(.//input[@name='indexation']/parent::*)[3]");
		public static By obj_TPDIndexationNo = By.xpath("(.//input[@name='indexation']/parent::*)[4]");

		public static By obj_PersonalDetails_DeathInsured = By.xpath("(//*[text()='Sum insured']//following::div/h4)[1]");
		public static By obj_PersonalDetails_TPDInsured = By.xpath("(//*[text()='Sum insured']//following::div/h4)[3]");
		public static By obj_PersonalDetails_DeathWeeklyCost = By.xpath("(//*[text()='Weekly cost']//following::div/h4)[1]");
		public static By obj_PersonalDetails_TPDWeeklyCost = By.xpath("(//*[text()='Weekly cost']//following::div/h4)[3]");
		public static By obj_PersonalDetails_TotalWeeklyCost = By.xpath("//*[text()='  Total weekly cost : ']/following-sibling::*");
		public static By obj_PersonalDetails_DeathMonthlyCost = By.xpath("(//*[text()='Monthly cost']//following::div/h4)[1]");
		public static By obj_PersonalDetails_TPDMonthlyCost = By.xpath("(//*[text()='Monthly cost']//following::div/h4)[3]");
		public static By obj_PersonalDetails_TotalMonthlyCost = By.xpath("//*[text()='  Total monthly cost : ']/following-sibling::*");
		public static By obj_PersonalDetails_DeathYearlyCost = By.xpath("(//*[text()='Yearly cost']//following::div/h4)[1]");
		public static By obj_PersonalDetails_TPDYearlyCost = By.xpath("(//*[text()='Yearly cost']//following::div/h4)[3]");
		public static By obj_PersonalDetails_TotalYearlyCost = By.xpath("//*[text()='  Total yearly cost : ']/following-sibling::*");




		//Premium Amount

		//*[text()='the member is not a casual or contractor employee ']
			public static By obj_PersonalDetails_DeathAmount = By.xpath("(//*[@id='death']//h4)");
			public static By obj_PersonalDetails_DeathCost = By.xpath("(//*[@id='death']//h4)[2]");
			public static By obj_PersonalDetails_TPDAmount = By.xpath("(//*[@id='tpd']//h4)");
			public static By obj_PersonalDetails_TPDCost = By.xpath("(//*[@id='tpd']//h4)[2]");
			public static By obj_PersonalDetails_IPAmount = By.xpath("(//*[@id='sc']//h4)");
			public static By obj_PersonalDetails_IPCost = By.xpath("(//*[@id='sc']//h4)[2]");
			public static By obj_PersonalDetails_TotalCostYearly = By.xpath("//*[text()=' Total Yearly cost : ']/following-sibling::h4");
			public static By obj_PersonalDetails_TotalCostMonthly = By.xpath("//*[text()=' Total Monthly cost : ']/following-sibling::h4");
			public static By obj_PersonalDetails_TotalCostWeekly = By.xpath("//*[text()=' Total Weekly cost : ']/following-sibling::h4");

		//Footer
			public static By obj_Continue_PersonalDetails = By.xpath("//*[@ng-click='checkForAura()']");

		// ************************************************************************************************************************
		// -----------------------------------------Confirmation Details
		// ************************************************************************************************************************

		// Personal Details
		public static By obj_Confirmation_FirstName = By.xpath("//*[text()='First name']//following::label");
		public static By obj_Confirmation_LastName = By.xpath("//*[text()='Last name']//following::label");
		public static By obj_Confirmation_Gender = By.xpath("//*[text()='Gender']//following::label");
		public static By obj_Confirmation_ContactType = By.xpath("//*[text()='Preferred contact type']//following::label");
		public static By obj_Confirmation_ContactNo = By.xpath("//*[text()='Preferred contact number ']//following::label");
		public static By obj_Confirmation_Email = By.xpath("//*[text()='Email address']//following::label");
		public static By obj_Confirmation_DOB = By.xpath("//*[text()='Date of birth']//following::label");
		public static By obj_Confirmation_Time = By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label");
		public static By obj_Confirmation_Indexation = By.xpath("//*[text()='Indexation applied at lower of CPI and 7.5% per year']//following::label");

		// Cover Details
		public static By obj_Confirmation_Death_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[1]//following::p");
		public static By obj_Confirmation_Death_TransferCover = By.xpath("(//*[text()='New sum insured'])[1]//following::p");
		public static By obj_Confirmation_Death_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[1]//following::p");
		public static By obj_Confirmation_Death_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[1]//following::p");
		public static By obj_Confirmation_TPD_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[2]//following::p");
		public static By obj_Confirmation_TPD_TransferCover = By.xpath("(//*[text()='New sum insured'])[2]//following::p");
		public static By obj_Confirmation_TPD_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[2]//following::p");
		public static By obj_Confirmation_TPD_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[2]//following::p");
		public static By obj_Confirmation_IP_ExistingCover = By.xpath("(//*[text()='New sum insured'])[3]//following::p");
		//public static By obj_Confirmation_IP_NewCover = By.xpath("(//*[text()='Transfer Cover'])[3]//following::p");
		public static By obj_Confirmation_IP_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[3]//following::p");
		public static By obj_Confirmation_IP_WaitingPeriod = By.xpath("//*[text()='Waiting period']//following::p");
		public static By obj_Confirmation_IP_BenefitPeriod = By.xpath("//*[text()='Benefit period']//following::p");
		public static By obj_Confirmation_IP_WeeklyCost = By.xpath("//*[text()=' Total Weekly cost']//following::h4");
		public static By obj_Confirmation_TotalWeeklyCost = By.xpath("//*[contains(text(), 'Total Weekly cost')]//following::h4");



		public static By obj_Confirmation_TermsConditionsCheckBox = By.xpath("//*[@id='generalConsentLabel']/span");
		public static By obj_Confirmation_Submit = By.xpath("//*[@ng-click='navigateToDecision()']");
		public static By obj_DecisionPopUP_No = By.xpath("//button[text()='No']");
		public static By obj_DecisionPopUP_Appstatus = By.xpath("//a[@class='noarrow']/h4");
		public static By obj_DecisionPopUP_AppMsg = By.xpath("//*[@id='collapseOne']/div/div/div/p");
		public static By obj_DecisionPopUP_App = By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p");
		public static By obj_Confirmation_Download = By.xpath("//strong[text()='Download ']");
		public static By obj_Confirmation_Save = By.xpath("//*[@ng-click='saveSummary()']");

}