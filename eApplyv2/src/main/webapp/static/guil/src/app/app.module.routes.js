(function(angular){
'use strict';
/* global memberStatus  */
angular.module('guildApp').config(['$stateProvider', '$urlRouterProvider', '$httpProvider','guilUIConstants','$templateRequestProvider',function ($stateProvider,$urlRouterProvider,$httpProvider,guilUIConstants,$templateRequestProvider) {
	   $httpProvider.defaults.withCredentials = true;
		/*$urlRouterProvider.otherwise('/login');*/
	   $httpProvider.interceptors.push('eapplyrouter');
	   $templateRequestProvider.httpOptions({_isTemplate: true});
	   $urlRouterProvider.otherwise('/landingpage');
	   $stateProvider
	     .state('landing',{
	       url: '/landingpage',
	       controller: 'landingCtrl',
	       controllerAs: 'vm',
	       templateUrl: guilUIConstants.path.modules + 'landing/landingpage.html',
	       title: 'GUILD Super-Home',
	       reloadOnSearch: false,
 	       resolve: {
 	        urls:function(fetchUrlSvc){
 	          var path = 'CareSuperUrls.properties';
 	          return fetchUrlSvc.getUrls(path);
 	        }
 	       }
	     })
	     .state('cover',{
	       url: '/cover/:mode',
		   controller: 'coverCtrl',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/cover.html',
		   title: 'GUILD Super-Change Cover'
	     })
	     .state('aura',{
	       url: '/aura',
		   controller: 'auraCtrl',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/aura.html',
		   title: 'GUILD Super-Change Aura'
	     })
	     .state('summary',{
	       url: '/summary',
		   controller: 'summaryCtrl',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/summary.html'
	     })
	     .state('changeaccept',{
	       url: '/changeaccept',
		   controller: 'changeCoverAcceptController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/changeCover_decision_accepted.html',
		   title: 'GUILD Super-Change Accept'
	     })
	     .state('changedecline',{
	       url: '/changedecline',
		   controller: 'changeCoverDeclineController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/changeCover_decision_decline.html',
		   title: 'GUILD Super-Change Decline'
	     })
	     .state('changeunderwriting',{
	       url: '/changeunderwriting',
		   controller: 'changeCoverRUWController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/changeCover_decision_ruw.html',
		   title: 'GUILD Super-Change RUW'
	     })
	     .state('changeaspcltermsacc',{
	       url: '/changeaspcltermsacc',
		   controller: 'changeCoverACCSpclTermsController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'quote/changeCover_decision_AcceptWithSpclTerms.html',
		   title: 'GUILD Super-Change Accept with special terms'
	     })
	     .state('quotetransfer',{
		   url: '/quotetransfer/:mode',
		   controller: 'quotetransfer',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'transfer/cover.html',
		   title: 'GUILD Super-Transfer Cover'
		 })
		 .state('auratransfer',{
		   url: '/auratransfer/:mode',
		   controller: 'auratransfer',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'transfer/aura.html',
		   title: 'GUILD Super-Transfer Aura'
		 })
		 .state('transferSummary',{
		   url: '/transferSummary/:mode',
		   controller: 'transferSummary',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'transfer/summary.html',
		   title: 'GUILD Super-Transfer Summary'
		 })
		  .state('quoteLife',{
		   url: '/quoteLife/:mode',
		   controller: 'quoteLife',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'life/cover.html',
		   title: 'GUILD Super-Life Cover'
		 })
		 .state('summaryLife',{
		   url: '/summaryLife/:mode',
		   controller: 'summaryLife',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'life/summary.html',
		   title: 'GUILD Super-Life Summary'
		 })
		 .state('lifeDecision',{
		   url: '/lifeDecision/:mode',
		   controller: 'lifeDecision',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'life/decision.html',
		   title: 'GUILD Super-Life Decision'
		 })
		 .state('quoteCancel',{
		   url: '/quoteCancel/:mode',
		   controller: 'quoteCancel',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'cancel/cover.html',
		   title: 'GUILD Super-Cancel Cover'
		 })
		 .state('cancelSummary',{
		   url: '/cancelSummary',
		   controller: 'cancelSummary',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'cancel/summary.html',
		   title: 'GUILD Super-Cancel Summary'
		 })
		 .state('cancelDecision',{
		   url: '/cancelDecision',
		   controller: 'cancelDecision',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'cancel/decision.html',
		   title: 'GUILD Super-Cancel Decision'
		 })
		 .state('/retrievesavedapplication', {
			 url: '/retrievesavedapplication',
			 controller  : 'retrievesavedapplication',
			 controllerAs: 'vm',
			 templateUrl: guilUIConstants.path.modules + 'retrievesavedapplication/retrieve.html',
			 title: 'GUILD Super-Saved Aapplication'
	    })
	    .state('/retrievesavedapplicationnonmem', {
			 url: '/retrievesavedapplicationNonMem/:mode',
			 controller  : 'retrievesavedapplication',
			 controllerAs: 'vm',
			 templateUrl: guilUIConstants.path.modules + 'retrievesavedapplication/retrieve.html',
			 title: 'GUILD Super-Saved Aapplication',
			 resolve: {
			 	 urls:function(fetchUrlSvc){
			 	    var path = 'CareSuperUrls.properties';
			 	    return fetchUrlSvc.getUrls(path);
			 	 }
			 }
	    })
	    .state('sessionTimeOut', {
	    	url:'/sessionTimeOut',
	    	templateUrl : guilUIConstants.path.modules + 'timeout/timeout.html',
	    	controller  : 'timeOutController',
	    	controllerAs: 'vm',
	    	title: 'GUILD Super-Session Expired'
	    })
	    .state('quoteOccChange',{
		   url: '/quoteoccchange/:mode',
		   controller: 'quoteoccupdate',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'occupation/cover.html',
		   title: 'GUILD Super-Occupation Cover'
		 })
		 .state('auraOcc',{
		   url: '/auraocc/:mode',
		   controller: 'auraocc',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'occupation/aura.html',
		   title: 'GUILD Super-Occupation Aura'
		 })
		 .state('workRatingSummary',{
		   url: '/workRatingSummary/:mode',
		   controller: 'occUpgradeSummary',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'occupation/summary.html',
		   title: 'GUILD Super-Occupation Summary'
		 })
		 .state('workRatingAccept',{
		   url: '/workRatingAccept',
		   controller: 'workRatingAcceptController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'occupation/occupationSummary_accepted.html',
		   title: 'GUILD Super-Occupation Accept'
		 })
		 .state('workRatingDecline',{
		   url: '/workRatingDecline',
		   controller: 'workRatingDeclineController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'occupation/occupationSummary_declined.html',
		   title: 'GUILD Super-Occupation Declined'
		 })
		 .state('workRatingMaintain',{
		   url: '/workRatingMaintain',
		   controller: 'workRatingMaintainController',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'occupation/occupationSummary_maintained.html',
		   title: 'GUILD Super-Occupation Maintained'
		 })
		 .state('nonmember',{
		   url: '/nonmember/:mode',
		   controller: 'nonmemberCoverDetails',
		   controllerAs: 'vm',
		   title: 'GUILD Super-Non Member Change Cover',
		   templateUrl: guilUIConstants.path.modules + 'nonmember/cover.html',
	       	resolve: {
				urls:function(fetchUrlSvc){
					var path = 'CareSuperUrls.properties';
					return fetchUrlSvc.getUrls(path);
				} 
	       	}
		 })
		 .state('auraNonMember',{
		   url: '/auraNonMember/:mode',
		   controller: 'auraNonMember',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'nonmember/aura.html',
	       	resolve: {
				urls:function(fetchUrlSvc){
					var path = 'CareSuperUrls.properties';
					return fetchUrlSvc.getUrls(path);
				} 
	       	}
		 })
		 .state('nonMemberSummary',{
		   url: '/nonMemberSummary/:mode',
		   controller: 'nonMemberSummary',
		   controllerAs: 'vm',
		   templateUrl: guilUIConstants.path.modules + 'nonmember/summary.html',
	       	resolve: {
				urls:function(fetchUrlSvc){
					var path = 'CareSuperUrls.properties';
					return fetchUrlSvc.getUrls(path);
				} 
	       	}
		 })
		 .state('nonMemberDecisionAccept',{
			   url: '/nonMemberaccept',
			   controller: 'nonMemberAcceptController',
			   controllerAs: 'vm',
			   templateUrl: guilUIConstants.path.modules + 'nonmember/nonMember_decision_accepted.html',
		       	resolve: {
					urls:function(fetchUrlSvc){
						var path = 'CareSuperUrls.properties';
						return fetchUrlSvc.getUrls(path);
					} 
		       	}
			 })
			 .state('nonMemberdecline',{
			   url: '/nonMemberdecline',
			   controller: 'nonMemberDeclineController',
			   controllerAs: 'vm',
			   templateUrl: guilUIConstants.path.modules + 'nonmember/nonMember_decision_decline.html',
		       	resolve: {
					urls:function(fetchUrlSvc){
						var path = 'CareSuperUrls.properties';
						return fetchUrlSvc.getUrls(path);
					} 
		       	}
			 })
			 .state('nonMemberDecisionRUW',{
				   url: '/nonMemberunderwriting',
				   controller: 'nonMemberRUWController',
				   controllerAs: 'vm',
				   templateUrl: guilUIConstants.path.modules + 'nonmember/nonMember_decision_ruw.html',
			       	resolve: {
						urls:function(fetchUrlSvc){
							var path = 'CareSuperUrls.properties';
							return fetchUrlSvc.getUrls(path);
						} 
			       	}
				 })
				 .state('nonMemberDecisionSpclTerm',{
					   url: '/nonMemberaspcltermsacc',
					   controller: 'nonMemberACCSpclTermsController',
					   controllerAs: 'vm',
					   templateUrl: guilUIConstants.path.modules + 'nonmember/nonMember_decision_AcceptWithSpclTerms.html',
				       	resolve: {
							urls:function(fetchUrlSvc){
								var path = 'CareSuperUrls.properties';
								return fetchUrlSvc.getUrls(path);
							} 
				       	}
					 });
	  
	}]);

angular.module('guildApp').run( function($location) {
	if( memberStatus === 'newApp') {
		$location.path('/nonmember/1');
		
	}
	if( memberStatus === 'savedApp') {
		$location.path('/retrievesavedapplicationNonMem/3');
		
	}
});

angular.module('guildApp').run(function($rootScope, $location, $state, $timeout){
	$rootScope.$on('$locationChangeStart', function(currentRoute) {
		$timeout(function(){
			$rootScope.title = $state.current.title;
			if(!$rootScope.title){
				$rootScope.title = 'GUILD Super-Home';
			}
			//console.log($rootScope.title);
		},200);				
	});
});

})(angular);