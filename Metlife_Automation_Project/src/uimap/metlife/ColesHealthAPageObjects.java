package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesHealthAPageObjects extends PageObjectLibrary {
	public static final By rdAusCitizenYes = getObjectIdentifaction("coles.yourhealth.aboutyou.australiancitizen.yes.radio.css");
	public static final By rdAusCitizenNo = getObjectIdentifaction("coles.yourhealth.aboutyou.australiancitizen.no.radio.css");
	public static final By rd457Yes = getObjectIdentifaction("coles.yourhealth.aboutyou.hold457.yes.radio.css");
	public static final By rd457No = getObjectIdentifaction("coles.yourhealth.aboutyou.hold457.no.radio.css");
	public static final By txtHeight = getObjectIdentifaction("coles.yourhealth.abturhealth.height.text.id");
	public static final By selHeight = getObjectIdentifaction("coles.yourhealth.abturhealth.height.dropdown.css");
	public static final By linkHeightCm = getObjectIdentifaction("coles.yourhealth.abturhealth.height.cm.linktext");
	public static final By linkHeightFeet = getObjectIdentifaction("coles.yourhealth.abturhealth.height.feet.linktext");
	public static final By txtWeight = getObjectIdentifaction("coles.yourhealth.abturhealth.weight.text.id");
	public static final By selWeight = getObjectIdentifaction("coles.yourhealth.abturhealth.weight.dropdown.id");
	public static final By chlLast5YrsNone = getObjectIdentifaction("coles.yourhealth.abturhealth.last5yrs.none.checkbox.css");
	public static final By chkBoxHBP = getObjectIdentifaction("coles.yourhealth.hbp.radio.css");
	public static final By lnkNextId = getObjectIdentifaction("coles.yourhealth.next.link.id");
	public static final By lnkNext = getObjectIdentifaction("coles.yourhealth.next.link.css");
	public static final By drpdownHBPTreated = getObjectIdentifaction("coles.yourhealtha.hbptreated.dropdown.css");
	public static final By linkTextNotRequire = getObjectIdentifaction("coles.yourhealth.abturhealth.hbptreated.Notrequiringmedicationorlifestylechangesasadvisedbyyourdoctor.linktext");
	public static final By linkTextRequire = getObjectIdentifaction("coles.yourhealth.abturhealth.hbptreated.requiringmedicationorlifestylechangesasadvisedbyyourdoctor.linktext");
	public static final By btnSelect = getObjectIdentifaction("coles.yourhealth.select.btn.css");
	public static final By radLessThan12Months = getObjectIdentifaction("coles.yourhealth.12monthsago.radio.css");
	public static final By radMoreThan12Months = getObjectIdentifaction("coles.yourhealth.lessthan12months.radio.css");
	public static final By linktextSave = getObjectIdentifaction("Coles.yourhealth.save.linktext");
	public static final By txtSaveEmail = getObjectIdentifaction("Coles.yourhealth.save.email.text.id");
	public static final By linkConfirm = getObjectIdentifaction("Coles.yourhealth.save.confirm.css");
	public static final By txtsGetRefNumber = getObjectIdentifaction("Coles.yourhealth.save.Reference.strong.css");
	public static final By linkSavepopclose = getObjectIdentifaction("Coles.yourhealth.save.popup.close.link.id");
	public static final By linktextRetrieve = getObjectIdentifaction("Coles.retrieveapp.xpath");
			
}
