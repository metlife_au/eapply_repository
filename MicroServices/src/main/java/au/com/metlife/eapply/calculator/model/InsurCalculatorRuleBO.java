package au.com.metlife.eapply.calculator.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.drools.KnowledgeBase;

public class InsurCalculatorRuleBO implements Serializable {

	private String ruleFileName = null;

	private transient KnowledgeBase kbase = null;
	
	private String productCode=null;
	
	private boolean ageRend=false;
	
	private boolean genderRend=false;
	
	private boolean livingPartnerRend=false;
	
	private boolean industryTypeRend=false;
	
	private boolean fifteenHrsQRend=false;
	
	private boolean indOccRend=false;
	
	private boolean grossSalRend=false;
	
	private boolean partnerGrossSalRen=false;
	
	private boolean mortgageRepRend=false;
	
	private boolean otherExpRend=false;
	
	private boolean totalDebtsRend=false;
	
	private boolean q8_PROFF_Q_Render=false;
	
	private boolean q9_PROFF_Q_Render=false;
	
	private boolean q10_PROFF_Q_Render=false;
	
	private String otherExpStr=null;
	
	private String totalDebtsStr=null;
	
	private String frequencyStr=null;
	
	private String myGrossSalDefFreq=null;
	
	private String partnerGrossSalDefFreq=null;
	
	private String mortgageRepDefFreq=null;
	
	private String otherExpDefFreq=null;
	
	private int minAge=0;
	
	private int maxAge=0;
	
	private int defaultRetirementAge=0;
	
	private int retirementMinAge=0;
	
	private int retiementMaxAge=0;
	
	private BigDecimal defaultInflationRate;
	
	private BigDecimal minInflationRate;
	
	private BigDecimal maxInflationRate;
	
	private BigDecimal defaultRealIntrRate;
	
	private BigDecimal minRealIntrRate;
	
	private BigDecimal maxRealIntrRate;
	
	private BigDecimal defaultSuperContribRate;
	
	private BigDecimal minSuperContribRate;
	
	private BigDecimal maxSuperContribRate;
	
	private BigDecimal defaultIPBenefitToSA;
	
	private BigDecimal minIPBenefitToSA;
	
	private BigDecimal maxIPBenefitToSA;
	
	private String ipBenefitPeriodTokens=null;
	
	private String defaultBenefitPeriod;
	
	private String ipWaitingPeriodTokens=null;
	
	private String defaultWaitingPeriod;
	
	private BigDecimal defaultIpReplaceRatio;
	
	private BigDecimal minIpReplaceRatio;
	
	private BigDecimal maxIpReplaceRatio;
	
	private int dependSuppMinAge;
	
	private int dependSuppMaxAge;
	
	private int defaultDependSuppAge;
	
	private BigDecimal deathMinCover;
	
	private BigDecimal deathMaxCover;
	
	private BigDecimal deathMinUnits;
	
	private BigDecimal deathMaxUnits;
	
	private BigDecimal tpdMinCover;
	
	private BigDecimal tpdMaxCover;
	
	private BigDecimal tpdMinUnits;
	
	private BigDecimal tpdMaxUnits;
	
	private BigDecimal ipMinCover;
	
	private BigDecimal ipMaxCover;
	
	private BigDecimal deathMultFactor;
	
	private BigDecimal tpdMultFactor;
	
	private BigDecimal ipMultFactor;
	
	private BigDecimal maxDeathAndTpdCover;
	
	private BigDecimal maxIpReplRatio;
	
	private BigDecimal combMaxIpReplRatio;
	
	private int maxNursingCostAge;
	
	private BigDecimal adultFactor;
	
	private BigDecimal childFactor;
	
	private BigDecimal miscFactor;
	
	private BigDecimal defaultFuneralCost;
	
	private BigDecimal minFuneralCost;
	
	private BigDecimal maxFuneralCost;
	
	private BigDecimal defaultMedicalCost;
	
	private BigDecimal minMedicalCost;
	
	private BigDecimal maxMedicalCost;
	
	private double ttlIncome=0;
	
	private BigDecimal taxRate;
	
	private BigDecimal taxPerc;
	
	private BigDecimal yearlyTax;
	
	private BigDecimal  maxGrossSalaryLimit;
	
	private BigDecimal maxPartnerGrossSalaryLimit;
	
	private BigDecimal maxMortgageRepaymentLimit;
	
	private BigDecimal maxOtherExpensesLimit;
	
	private BigDecimal maxTotalDebtsLimit;
	
	private boolean askmanualqRenderFlag;
	
	private boolean spendTimeOutsideRenderFlag;
	
	private boolean workDuties2RenderFlag;
	
	private boolean teritoryQualRenderFlag;
	
	private boolean askProfessionalFlag;
	
	private boolean askManualFlag;
	
	private boolean otherOccupationRend;
	
	private String annualSalForUpgradeVal=null;
	
	private boolean rendAddnlOccupTxtFld=false;
	
	private BigDecimal maxIPSalaryPerc;
	
	private int ipMinAge;
	
	private int ipMaxAge;
	
	private BigDecimal ipUnitisedAmtPerUnit=null;
	
	private boolean calculateCost=false;
	
	private double ipMultiPolicyDiscount=0;
	
	private double sfpsIpUnitCostStandardMultFac=0;
	
	private double sfpsIpUnitCostWhiteColMultFac=0;
	
	private double sfpsIpUnitCostProfMultFac=0;
	

	public double getSfpsIpUnitCostProfMultFac() {
		return sfpsIpUnitCostProfMultFac;
	}

	public void setSfpsIpUnitCostProfMultFac(double sfpsIpUnitCostProfMultFac) {
		this.sfpsIpUnitCostProfMultFac = sfpsIpUnitCostProfMultFac;
	}

	public double getSfpsIpUnitCostStandardMultFac() {
		return sfpsIpUnitCostStandardMultFac;
	}

	public void setSfpsIpUnitCostStandardMultFac(
			double sfpsIpUnitCostStandardMultFac) {
		this.sfpsIpUnitCostStandardMultFac = sfpsIpUnitCostStandardMultFac;
	}

	public double getSfpsIpUnitCostWhiteColMultFac() {
		return sfpsIpUnitCostWhiteColMultFac;
	}

	public void setSfpsIpUnitCostWhiteColMultFac(
			double sfpsIpUnitCostWhiteColMultFac) {
		this.sfpsIpUnitCostWhiteColMultFac = sfpsIpUnitCostWhiteColMultFac;
	}

	public double getIpMultiPolicyDiscount() {
		return ipMultiPolicyDiscount;
	}

	public void setIpMultiPolicyDiscount(double ipMultiPolicyDiscount) {
		this.ipMultiPolicyDiscount = ipMultiPolicyDiscount;
	}

	public boolean isCalculateCost() {
		return calculateCost;
	}

	public void setCalculateCost(boolean calculateCost) {
		this.calculateCost = calculateCost;
	}

	public BigDecimal getIpUnitisedAmtPerUnit() {
		return ipUnitisedAmtPerUnit;
	}

	public void setIpUnitisedAmtPerUnit(BigDecimal ipUnitisedAmtPerUnit) {
		this.ipUnitisedAmtPerUnit = ipUnitisedAmtPerUnit;
	}

	public BigDecimal getMaxIPSalaryPerc() {
		return maxIPSalaryPerc;
	}

	public void setMaxIPSalaryPerc(BigDecimal maxIPSalaryPerc) {
		this.maxIPSalaryPerc = maxIPSalaryPerc;
	}

	public String getAnnualSalForUpgradeVal() {
		return annualSalForUpgradeVal;
	}

	public void setAnnualSalForUpgradeVal(String annualSalForUpgradeVal) {
		this.annualSalForUpgradeVal = annualSalForUpgradeVal;
	}

	public boolean isOtherOccupationRend() {
		return otherOccupationRend;
	}

	public void setOtherOccupationRend(boolean otherOccupationRend) {
		this.otherOccupationRend = otherOccupationRend;
	}

	public boolean isAskManualFlag() {
		return askManualFlag;
	}

	public void setAskManualFlag(boolean askManualFlag) {
		this.askManualFlag = askManualFlag;
	}

	public boolean isAskmanualqRenderFlag() {
		return askmanualqRenderFlag;
	}

	public void setAskmanualqRenderFlag(boolean askmanualqRenderFlag) {
		this.askmanualqRenderFlag = askmanualqRenderFlag;
	}

	public boolean isAskProfessionalFlag() {
		return askProfessionalFlag;
	}

	public void setAskProfessionalFlag(boolean askProfessionalFlag) {
		this.askProfessionalFlag = askProfessionalFlag;
	}

	public boolean isSpendTimeOutsideRenderFlag() {
		return spendTimeOutsideRenderFlag;
	}

	public void setSpendTimeOutsideRenderFlag(boolean spendTimeOutsideRenderFlag) {
		this.spendTimeOutsideRenderFlag = spendTimeOutsideRenderFlag;
	}

	public boolean isTeritoryQualRenderFlag() {
		return teritoryQualRenderFlag;
	}

	public void setTeritoryQualRenderFlag(boolean teritoryQualRenderFlag) {
		this.teritoryQualRenderFlag = teritoryQualRenderFlag;
	}

	public boolean isWorkDuties2RenderFlag() {
		return workDuties2RenderFlag;
	}

	public void setWorkDuties2RenderFlag(boolean workDuties2RenderFlag) {
		this.workDuties2RenderFlag = workDuties2RenderFlag;
	}

	public BigDecimal getYearlyTax() {
		return yearlyTax;
	}

	public void setYearlyTax(BigDecimal yearlyTax) {
		this.yearlyTax = yearlyTax;
	}

	public BigDecimal getDefaultFuneralCost() {
		return defaultFuneralCost;
	}

	public void setDefaultFuneralCost(BigDecimal defaultFuneralCost) {
		this.defaultFuneralCost = defaultFuneralCost;
	}

	public BigDecimal getDefaultMedicalCost() {
		return defaultMedicalCost;
	}

	public void setDefaultMedicalCost(BigDecimal defaultMedicalCost) {
		this.defaultMedicalCost = defaultMedicalCost;
	}

	public String getOtherExpStr() {
		return otherExpStr;
	}

	public void setOtherExpStr(String otherExpStr) {
		this.otherExpStr = otherExpStr;
	}

	public boolean isAgeRend() {
		return ageRend;
	}

	public void setAgeRend(boolean ageRend) {
		this.ageRend = ageRend;
	}

	public boolean isGenderRend() {
		return genderRend;
	}

	public void setGenderRend(boolean genderRend) {
		this.genderRend = genderRend;
	}

	public boolean isGrossSalRend() {
		return grossSalRend;
	}

	public void setGrossSalRend(boolean grossSalRend) {
		this.grossSalRend = grossSalRend;
	}

	public boolean isIndOccRend() {
		return indOccRend;
	}

	public void setIndOccRend(boolean indOccRend) {
		this.indOccRend = indOccRend;
	}

	public boolean isIndustryTypeRend() {
		return industryTypeRend;
	}

	public void setIndustryTypeRend(boolean industryTypeRend) {
		this.industryTypeRend = industryTypeRend;
	}

	public KnowledgeBase getKbase() {
		return kbase;
	}

	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}

	public boolean isLivingPartnerRend() {
		return livingPartnerRend;
	}

	public void setLivingPartnerRend(boolean livingPartnerRend) {
		this.livingPartnerRend = livingPartnerRend;
	}

	public boolean isMortgageRepRend() {
		return mortgageRepRend;
	}

	public void setMortgageRepRend(boolean mortgageRepRend) {
		this.mortgageRepRend = mortgageRepRend;
	}

	public boolean isOtherExpRend() {
		return otherExpRend;
	}

	public void setOtherExpRend(boolean otherExpRend) {
		this.otherExpRend = otherExpRend;
	}

	public boolean isPartnerGrossSalRen() {
		return partnerGrossSalRen;
	}

	public void setPartnerGrossSalRen(boolean partnerGrossSalRen) {
		this.partnerGrossSalRen = partnerGrossSalRen;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getRuleFileName() {
		return ruleFileName;
	}

	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}

	public boolean isTotalDebtsRend() {
		return totalDebtsRend;
	}

	public void setTotalDebtsRend(boolean totalDebtsRend) {
		this.totalDebtsRend = totalDebtsRend;
	}

	public String getTotalDebtsStr() {
		return totalDebtsStr;
	}

	public void setTotalDebtsStr(String totalDebtsStr) {
		this.totalDebtsStr = totalDebtsStr;
	}

	public String getFrequencyStr() {
		return frequencyStr;
	}

	public void setFrequencyStr(String frequencyStr) {
		this.frequencyStr = frequencyStr;
	}

	public String getMortgageRepDefFreq() {
		return mortgageRepDefFreq;
	}

	public void setMortgageRepDefFreq(String mortgageRepDefFreq) {
		this.mortgageRepDefFreq = mortgageRepDefFreq;
	}

	public String getMyGrossSalDefFreq() {
		return myGrossSalDefFreq;
	}

	public void setMyGrossSalDefFreq(String myGrossSalDefFreq) {
		this.myGrossSalDefFreq = myGrossSalDefFreq;
	}

	public String getOtherExpDefFreq() {
		return otherExpDefFreq;
	}

	public void setOtherExpDefFreq(String otherExpDefFreq) {
		this.otherExpDefFreq = otherExpDefFreq;
	}

	public String getPartnerGrossSalDefFreq() {
		return partnerGrossSalDefFreq;
	}

	public void setPartnerGrossSalDefFreq(String partnerGrossSalDefFreq) {
		this.partnerGrossSalDefFreq = partnerGrossSalDefFreq;
	}

	public BigDecimal getAdultFactor() {
		return adultFactor;
	}

	public void setAdultFactor(BigDecimal adultFactor) {
		this.adultFactor = adultFactor;
	}

	public BigDecimal getChildFactor() {
		return childFactor;
	}

	public void setChildFactor(BigDecimal childFactor) {
		this.childFactor = childFactor;
	}

	public BigDecimal getCombMaxIpReplRatio() {
		return combMaxIpReplRatio;
	}

	public void setCombMaxIpReplRatio(BigDecimal combMaxIpReplRatio) {
		this.combMaxIpReplRatio = combMaxIpReplRatio;
	}

	public BigDecimal getDeathMaxCover() {
		return deathMaxCover;
	}

	public void setDeathMaxCover(BigDecimal deathMaxCover) {
		this.deathMaxCover = deathMaxCover;
	}

	public BigDecimal getDeathMinCover() {
		return deathMinCover;
	}

	public void setDeathMinCover(BigDecimal deathMinCover) {
		this.deathMinCover = deathMinCover;
	}

	public BigDecimal getDeathMultFactor() {
		return deathMultFactor;
	}

	public void setDeathMultFactor(BigDecimal deathMultFactor) {
		this.deathMultFactor = deathMultFactor;
	}

	public int getDefaultDependSuppAge() {
		return defaultDependSuppAge;
	}

	public void setDefaultDependSuppAge(int defaultDependSuppAge) {
		this.defaultDependSuppAge = defaultDependSuppAge;
	}

	public BigDecimal getDefaultInflationRate() {
		return defaultInflationRate;
	}

	public void setDefaultInflationRate(BigDecimal defaultInflationRate) {
		this.defaultInflationRate = defaultInflationRate;
	}

	public BigDecimal getDefaultIPBenefitToSA() {
		return defaultIPBenefitToSA;
	}

	public void setDefaultIPBenefitToSA(BigDecimal defaultIPBenefitToSA) {
		this.defaultIPBenefitToSA = defaultIPBenefitToSA;
	}

	public BigDecimal getDefaultRealIntrRate() {
		return defaultRealIntrRate;
	}

	public void setDefaultRealIntrRate(BigDecimal defaultRealIntrRate) {
		this.defaultRealIntrRate = defaultRealIntrRate;
	}

	public int getDefaultRetirementAge() {
		return defaultRetirementAge;
	}

	public void setDefaultRetirementAge(int defaultRetirementAge) {
		this.defaultRetirementAge = defaultRetirementAge;
	}

	public BigDecimal getDefaultSuperContribRate() {
		return defaultSuperContribRate;
	}

	public void setDefaultSuperContribRate(BigDecimal defaultSuperContribRate) {
		this.defaultSuperContribRate = defaultSuperContribRate;
	}

	public int getDependSuppMaxAge() {
		return dependSuppMaxAge;
	}

	public void setDependSuppMaxAge(int dependSuppMaxAge) {
		this.dependSuppMaxAge = dependSuppMaxAge;
	}

	public int getDependSuppMinAge() {
		return dependSuppMinAge;
	}

	public void setDependSuppMinAge(int dependSuppMinAge) {
		this.dependSuppMinAge = dependSuppMinAge;
	}

	public String getIpBenefitPeriodTokens() {
		return ipBenefitPeriodTokens;
	}

	public void setIpBenefitPeriodTokens(String ipBenefitPeriodTokens) {
		this.ipBenefitPeriodTokens = ipBenefitPeriodTokens;
	}

	public BigDecimal getIpMaxCover() {
		return ipMaxCover;
	}

	public void setIpMaxCover(BigDecimal ipMaxCover) {
		this.ipMaxCover = ipMaxCover;
	}

	public BigDecimal getIpMinCover() {
		return ipMinCover;
	}

	public void setIpMinCover(BigDecimal ipMinCover) {
		this.ipMinCover = ipMinCover;
	}

	public BigDecimal getIpMultFactor() {
		return ipMultFactor;
	}

	public void setIpMultFactor(BigDecimal ipMultFactor) {
		this.ipMultFactor = ipMultFactor;
	}

	public String getIpWaitingPeriodTokens() {
		return ipWaitingPeriodTokens;
	}

	public void setIpWaitingPeriodTokens(String ipWaitingPeriodTokens) {
		this.ipWaitingPeriodTokens = ipWaitingPeriodTokens;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public BigDecimal getMaxDeathAndTpdCover() {
		return maxDeathAndTpdCover;
	}

	public void setMaxDeathAndTpdCover(BigDecimal maxDeathAndTpdCover) {
		this.maxDeathAndTpdCover = maxDeathAndTpdCover;
	}

	public BigDecimal getMaxInflationRate() {
		return maxInflationRate;
	}

	public void setMaxInflationRate(BigDecimal maxInflationRate) {
		this.maxInflationRate = maxInflationRate;
	}

	public BigDecimal getMaxIPBenefitToSA() {
		return maxIPBenefitToSA;
	}

	public void setMaxIPBenefitToSA(BigDecimal maxIPBenefitToSA) {
		this.maxIPBenefitToSA = maxIPBenefitToSA;
	}

	public BigDecimal getMaxIpReplRatio() {
		return maxIpReplRatio;
	}

	public void setMaxIpReplRatio(BigDecimal maxIpReplRatio) {
		this.maxIpReplRatio = maxIpReplRatio;
	}

	public int getMaxNursingCostAge() {
		return maxNursingCostAge;
	}

	public void setMaxNursingCostAge(int maxNursingCostAge) {
		this.maxNursingCostAge = maxNursingCostAge;
	}

	public BigDecimal getMaxRealIntrRate() {
		return maxRealIntrRate;
	}

	public void setMaxRealIntrRate(BigDecimal maxRealIntrRate) {
		this.maxRealIntrRate = maxRealIntrRate;
	}

	public BigDecimal getMaxSuperContribRate() {
		return maxSuperContribRate;
	}

	public void setMaxSuperContribRate(BigDecimal maxSuperContribRate) {
		this.maxSuperContribRate = maxSuperContribRate;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public BigDecimal getMinInflationRate() {
		return minInflationRate;
	}

	public void setMinInflationRate(BigDecimal minInflationRate) {
		this.minInflationRate = minInflationRate;
	}

	public BigDecimal getMinIPBenefitToSA() {
		return minIPBenefitToSA;
	}

	public void setMinIPBenefitToSA(BigDecimal minIPBenefitToSA) {
		this.minIPBenefitToSA = minIPBenefitToSA;
	}

	public BigDecimal getMinRealIntrRate() {
		return minRealIntrRate;
	}

	public void setMinRealIntrRate(BigDecimal minRealIntrRate) {
		this.minRealIntrRate = minRealIntrRate;
	}

	public BigDecimal getMinSuperContribRate() {
		return minSuperContribRate;
	}

	public void setMinSuperContribRate(BigDecimal minSuperContribRate) {
		this.minSuperContribRate = minSuperContribRate;
	}

	public int getRetiementMaxAge() {
		return retiementMaxAge;
	}

	public void setRetiementMaxAge(int retiementMaxAge) {
		this.retiementMaxAge = retiementMaxAge;
	}

	public int getRetirementMinAge() {
		return retirementMinAge;
	}

	public void setRetirementMinAge(int retirementMinAge) {
		this.retirementMinAge = retirementMinAge;
	}

	public BigDecimal getTpdMaxCover() {
		return tpdMaxCover;
	}

	public void setTpdMaxCover(BigDecimal tpdMaxCover) {
		this.tpdMaxCover = tpdMaxCover;
	}

	public BigDecimal getTpdMinCover() {
		return tpdMinCover;
	}

	public void setTpdMinCover(BigDecimal tpdMinCover) {
		this.tpdMinCover = tpdMinCover;
	}

	public BigDecimal getTpdMultFactor() {
		return tpdMultFactor;
	}

	public void setTpdMultFactor(BigDecimal tpdMultFactor) {
		this.tpdMultFactor = tpdMultFactor;
	}

	public BigDecimal getMiscFactor() {
		return miscFactor;
	}

	public void setMiscFactor(BigDecimal miscFactor) {
		this.miscFactor = miscFactor;
	}

	public BigDecimal getDefaultIpReplaceRatio() {
		return defaultIpReplaceRatio;
	}

	public void setDefaultIpReplaceRatio(BigDecimal defaultIpReplaceRatio) {
		this.defaultIpReplaceRatio = defaultIpReplaceRatio;
	}

	public BigDecimal getMaxIpReplaceRatio() {
		return maxIpReplaceRatio;
	}

	public void setMaxIpReplaceRatio(BigDecimal maxIpReplaceRatio) {
		this.maxIpReplaceRatio = maxIpReplaceRatio;
	}

	public BigDecimal getMinIpReplaceRatio() {
		return minIpReplaceRatio;
	}

	public void setMinIpReplaceRatio(BigDecimal minIpReplaceRatio) {
		this.minIpReplaceRatio = minIpReplaceRatio;
	}

	public String getDefaultBenefitPeriod() {
		return defaultBenefitPeriod;
	}

	public String getDefaultWaitingPeriod() {
		return defaultWaitingPeriod;
	}

	public void setDefaultBenefitPeriod(String defaultBenefitPeriod) {
		this.defaultBenefitPeriod = defaultBenefitPeriod;
	}

	public void setDefaultWaitingPeriod(String defaultWaitingPeriod) {
		this.defaultWaitingPeriod = defaultWaitingPeriod;
	}

	public boolean isQ10_PROFF_Q_Render() {
		return q10_PROFF_Q_Render;
	}

	public void setQ10_PROFF_Q_Render(boolean render) {
		q10_PROFF_Q_Render = render;
	}

	public boolean isQ8_PROFF_Q_Render() {
		return q8_PROFF_Q_Render;
	}

	public void setQ8_PROFF_Q_Render(boolean render) {
		q8_PROFF_Q_Render = render;
	}

	public boolean isQ9_PROFF_Q_Render() {
		return q9_PROFF_Q_Render;
	}

	public void setQ9_PROFF_Q_Render(boolean render) {
		q9_PROFF_Q_Render = render;
	}

	public BigDecimal getMaxFuneralCost() {
		return maxFuneralCost;
	}

	public void setMaxFuneralCost(BigDecimal maxFuneralCost) {
		this.maxFuneralCost = maxFuneralCost;
	}

	public BigDecimal getMaxMedicalCost() {
		return maxMedicalCost;
	}

	public void setMaxMedicalCost(BigDecimal maxMedicalCost) {
		this.maxMedicalCost = maxMedicalCost;
	}

	public BigDecimal getMinFuneralCost() {
		return minFuneralCost;
	}

	public void setMinFuneralCost(BigDecimal minFuneralCost) {
		this.minFuneralCost = minFuneralCost;
	}

	public BigDecimal getMinMedicalCost() {
		return minMedicalCost;
	}

	public void setMinMedicalCost(BigDecimal minMedicalCost) {
		this.minMedicalCost = minMedicalCost;
	}

	public BigDecimal getTaxPerc() {
		return taxPerc;
	}

	public void setTaxPerc(BigDecimal taxPerc) {
		this.taxPerc = taxPerc;
	}

	public BigDecimal getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate = taxRate;
	}

	public double getTtlIncome() {
		return ttlIncome;
	}

	public void setTtlIncome(double ttlIncome) {
		this.ttlIncome = ttlIncome;
	}

	public BigDecimal getMaxGrossSalaryLimit() {
		return maxGrossSalaryLimit;
	}

	public void setMaxGrossSalaryLimit(BigDecimal maxGrossSalaryLimit) {
		this.maxGrossSalaryLimit = maxGrossSalaryLimit;
	}

	public BigDecimal getMaxPartnerGrossSalaryLimit() {
		return maxPartnerGrossSalaryLimit;
	}

	public void setMaxPartnerGrossSalaryLimit(BigDecimal maxPartnerGrossSalaryLimit) {
		this.maxPartnerGrossSalaryLimit = maxPartnerGrossSalaryLimit;
	}

	public BigDecimal getMaxMortgageRepaymentLimit() {
		return maxMortgageRepaymentLimit;
	}

	public void setMaxMortgageRepaymentLimit(BigDecimal maxMortgageRepaymentLimit) {
		this.maxMortgageRepaymentLimit = maxMortgageRepaymentLimit;
	}

	public BigDecimal getMaxOtherExpensesLimit() {
		return maxOtherExpensesLimit;
	}

	public void setMaxOtherExpensesLimit(BigDecimal maxOtherExpensesLimit) {
		this.maxOtherExpensesLimit = maxOtherExpensesLimit;
	}

	public BigDecimal getMaxTotalDebtsLimit() {
		return maxTotalDebtsLimit;
	}

	public void setMaxTotalDebtsLimit(BigDecimal maxTotalDebtsLimit) {
		this.maxTotalDebtsLimit = maxTotalDebtsLimit;
	}

	public boolean isRendAddnlOccupTxtFld() {
		return rendAddnlOccupTxtFld;
	}

	public void setRendAddnlOccupTxtFld(boolean rendAddnlOccupTxtFld) {
		this.rendAddnlOccupTxtFld = rendAddnlOccupTxtFld;
	}

	public int getIpMaxAge() {
		return ipMaxAge;
	}

	public void setIpMaxAge(int ipMaxAge) {
		this.ipMaxAge = ipMaxAge;
	}

	public int getIpMinAge() {
		return ipMinAge;
	}

	public void setIpMinAge(int ipMinAge) {
		this.ipMinAge = ipMinAge;
	}

	public boolean isFifteenHrsQRend() {
		return fifteenHrsQRend;
	}

	public void setFifteenHrsQRend(boolean fifteenHrsQRend) {
		this.fifteenHrsQRend = fifteenHrsQRend;
	}

	public BigDecimal getDeathMaxUnits() {
		return deathMaxUnits;
	}

	public void setDeathMaxUnits(BigDecimal deathMaxUnits) {
		this.deathMaxUnits = deathMaxUnits;
	}

	public BigDecimal getDeathMinUnits() {
		return deathMinUnits;
	}

	public void setDeathMinUnits(BigDecimal deathMinUnits) {
		this.deathMinUnits = deathMinUnits;
	}

	public BigDecimal getTpdMaxUnits() {
		return tpdMaxUnits;
	}

	public void setTpdMaxUnits(BigDecimal tpdMaxUnits) {
		this.tpdMaxUnits = tpdMaxUnits;
	}

	public BigDecimal getTpdMinUnits() {
		return tpdMinUnits;
	}

	public void setTpdMinUnits(BigDecimal tpdMinUnits) {
		this.tpdMinUnits = tpdMinUnits;
	}

}
