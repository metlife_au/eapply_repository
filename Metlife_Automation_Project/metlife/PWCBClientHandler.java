package pages.metlife;

import org.apache.ws.security.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import java.io.IOException;

public class PWCBClientHandler implements CallbackHandler {
    
    String uTUsername;
    
    String uTPassword;
    
    public void handle(Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {

        for (int i = 0; i < callbacks.length; i++) {
            WSPasswordCallback pwcb = (WSPasswordCallback) callbacks[i];
            // pwcb.setIdentifier("internal01");
            // pwcb.setPassword("abcd1");
            // pwcb.setPassword("metlife123");
            pwcb.setIdentifier( uTUsername);
            pwcb.setPassword( uTPassword);
        }
    }
    
    public String getUTPassword() {

        return uTPassword;
    }
    
    public void setUTPassword(String password) {

        uTPassword = password;
    }
    
    public String getUTUsername() {

        return uTUsername;
    }
    
    public void setUTUsername(String username) {

        uTUsername = username;
    }
    
}
