package pages.metlife.ECLAIMS;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ScriptHelper;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.JavascriptExecutor;
import com.cognizant.framework.Settings;
import org.openqa.selenium.WebElement;

public class Claim_Angular_claimtype_life extends MasterPage {
	WebDriverUtil driverUtil = null;

	public Claim_Angular_claimtype_life E_Lifeclaims() {
		Eclaims_Lifeflow();
		Eclaims_Reviewclaims_life();		
		return new Claim_Angular_claimtype_life(scriptHelper);
	}
	
	public Claim_Angular_claimtype_life E_Lifeclaims_nagativescenario() {
		Eclaims_Lifeevent_negativescenario();
			
		return new Claim_Angular_claimtype_life(scriptHelper);
	}

	public Claim_Angular_claimtype_life(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void Eclaims_Lifeflow() {

		String Creditcard = dataTable.getData("Eclaims_Life", "Creditcard");
		String Title = dataTable.getData("Eclaims_Life", "Title");
		String DOB = dataTable.getData("Eclaims_Life", "DOB");
		String Name = dataTable.getData("Eclaims_Life", "Name");
		String Surname = dataTable.getData("Eclaims_Life", "Surname");
		String Address = dataTable.getData("Eclaims_Life", "Address1");
		String Addresss = dataTable.getData("Eclaims_Life", "Address2");
		String suburb = dataTable.getData("Eclaims_Life", "suburb");
		String state = dataTable.getData("Eclaims_Life", "State");
		String postcode = dataTable.getData("Eclaims_Life", "Postcode");
		String PreferredPhoneNO = dataTable.getData("Eclaims_Life", "PreferredPhoneNO");
		String Otherphoneno = dataTable.getData("Eclaims_Life", "Otherphoneno");
		String Email = dataTable.getData("Eclaims_Life", "Email");
		String Preferredmode = dataTable.getData("Eclaims_Life", "Preferredmode");
		String LifeEvent = dataTable.getData("Eclaims_Life", "LifeEvent");
		String Lifeeventtype = dataTable.getData("Eclaims_Life", "Lifeeventtype");
		String settlementdate = dataTable.getData("Eclaims_Life", "settlementdate");
		String carpurchasedate = dataTable.getData("Eclaims_Life", "carpurchasedate");
		String Mainresidence = dataTable.getData("Eclaims_Life", "Mainresidence");
		String marriagedate = dataTable.getData("Eclaims_Life", "MarriageDate");
		String Childbirthdate = dataTable.getData("Eclaims_Life", "Childbirthdate");
		String Adoptiondate = dataTable.getData("Eclaims_Life", "Adoptiondate");
		String SpouseYN = dataTable.getData("Eclaims_Life", "SpouseYN");
		String Attachmenttypes = dataTable.getData("Eclaims_Life", "Attachmenttypes");
		String Deathcertificate = dataTable.getData("Eclaims_Life", "Deathcertificate");
		String Otherdocument = dataTable.getData("Eclaims_Life", "Otherdocument");
		String Receipt = dataTable.getData("Eclaims_Life", "Receipt");
		String Attachpath = dataTable.getData("Eclaims_Life", "Attachpath");
		String Contacttime = dataTable.getData("Eclaims_Life", "Contacttime");

		
		




		try {

			WebDriverWait wait = new WebDriverWait(driver, 18);

			// ****Click on claim type*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-LIFEEVNTS']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Life Event claim :", "Life Event is Selected", Status.PASS);

			// ****Enter creditcard number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
			report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",
					Creditcard + " is entered", Status.PASS);

			// ****Select title*****\\

			Select titletype = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
			titletype.selectByVisibleText(Title);
			report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

			// ****Enter DOB*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2"))).click();
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
					.sendKeys(DOB);
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Given name ']"))).click();
			report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

			// ****Enter Name*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
					.sendKeys(Name);
			report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

			// ****Enter surname*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
					.sendKeys(Surname);
			report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

			// ****Enter Address1*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
					.sendKeys(Address);
			report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

			// ****Enter Address2*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
					.sendKeys(Addresss);
			report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

			// ****Enter Suburb*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
					.sendKeys(suburb);
			report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

			// ****Select State*****\\

			Select Stateselect = new Select(
					driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
			Stateselect.selectByVisibleText(state);
			report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
			sleep(500);

			// ****Enter Postal code*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
					.sendKeys(postcode);
			sleep(500);
			report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
			sleep(300);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,500)", "");
			sleep(500);

			// ****Enter Emailaddress*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ213_0")))
					.sendKeys(Email);
			report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

			// ****Enter Peferred phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ211_1")))
					.sendKeys(PreferredPhoneNO);
			sleep(500);
			report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
			sleep(500);

			// ****Enter other phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ210_2")))
					.sendKeys(Otherphoneno);
			sleep(500);
			report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
			sleep(500);

			// ****Preferred mode of contact (Phone / Email / Letter)?*****\\

			if (Preferredmode.equalsIgnoreCase("Phone")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*")))
						.click();
				sleep(1500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

				Select mode = new Select(driver.findElement(By.xpath("//*[@id='product-type']")));
				mode.selectByVisibleText(Contacttime);
				report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
						Contacttime + " is selected ", Status.PASS);
				sleep(500);
			}
			if (Preferredmode.equalsIgnoreCase("Email")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			} 
			if (Preferredmode.equalsIgnoreCase("Letter")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*")))
						.click();
				sleep(500);
				report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
			}

			JavascriptExecutor j = (JavascriptExecutor) driver;
			j.executeScript("window.scrollBy(0,200)", "");
			sleep(900);

			// ***************LIFE EVENT***************//

			// ****Life Event*****\\

			sleep(500);
			Select lifeevent = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ3001_2")));
			lifeevent.selectByVisibleText(LifeEvent);
			sleep(500);
			report.updateTestLog("Life Event :", LifeEvent + " is selected ", Status.PASS);

			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("window.scrollBy(0,200)", "");
			sleep(500);
			// ****Date of new car purchase*******\\

			if (Lifeeventtype.equalsIgnoreCase("Newcar")) {

				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3002_3")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3002_3")))
						.sendKeys(carpurchasedate);
				sleep(500);
				report.updateTestLog("Date of new car purchase :", carpurchasedate + " is Entered", Status.PASS);

			}

			if (Lifeeventtype.equalsIgnoreCase("Home")) {

				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3007_4")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3007_4")))
						.sendKeys(settlementdate);
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Life Event ']"))).click();
				sleep(300);
				report.updateTestLog("SettlementDate :", settlementdate + " is Entered", Status.PASS);
				if (Mainresidence.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
							.click();
					sleep(500);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
							.click();
					sleep(500);
				}
			}
			if (Lifeeventtype.equalsIgnoreCase("Marriage")) {
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3003_5")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3003_5")))
						.sendKeys(marriagedate);
				sleep(500);
				report.updateTestLog("Marraige Date :", marriagedate + " is Entered", Status.PASS);

			}

			if (Lifeeventtype.equalsIgnoreCase("child")) {
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3004_6")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3004_6")))
						.sendKeys(Childbirthdate);
				sleep(500);
				report.updateTestLog("Child birth Date :", Childbirthdate + " is Entered", Status.PASS);

			}
			if (Lifeeventtype.equalsIgnoreCase("adoption")) {
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3005_7")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3005_7")))
						.sendKeys(Adoptiondate);
				sleep(500);
				report.updateTestLog("Child Adoption Date :", Adoptiondate + " is Entered", Status.PASS);

			}

			if (Lifeeventtype.equalsIgnoreCase("spouse")) {
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3006_9")))
						.click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3006_9")))
						.sendKeys(Adoptiondate);
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Life Event ']"))).click();
				sleep(300);
				report.updateTestLog("Child Adoption Date :", Adoptiondate + " is Entered", Status.PASS);
				report.updateTestLog(
						"Were you in a bonafide domestic relationship with your spouse immediately prior to their death :",
						Lifeeventtype + " is Selected", Status.PASS);
				if (SpouseYN.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
							.click();
					report.updateTestLog(
							"Were you in a bonafide domestic relationship with your spouse immediately prior to their death :",
							SpouseYN + " is Selected", Status.PASS);
					sleep(500);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
							.click();
					report.updateTestLog(
							"Were you in a bonafide domestic relationship with your spouse immediately prior to their death :",
							SpouseYN + " is Selected", Status.PASS);
					sleep(500);
				}

			}

			// ****Proceed button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary w100p']")))
					.click();
			sleep(500);
			report.updateTestLog("Proceed Button", "clicked", Status.PASS);

			// ***Additional Details*****\\

			// ****Attachpath******\\

			if (Attachmenttypes.equalsIgnoreCase("Brandnewcar")) {
				if (Deathcertificate.equalsIgnoreCase("Yes")) {
					sleep(500);
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[contains(@id,'upload_vehRegistration')]")))
							.sendKeys(Attachpath);
					report.updateTestLog("File attachment:", "File is uploaded", Status.PASS);
					sleep(1500);

				}
				if (Receipt.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[contains(@id,'upload_recieptPurchase')]")))
							.sendKeys(Attachpath);
					sleep(1500);
					JavascriptExecutor exe = (JavascriptExecutor) driver;
					exe.executeScript("window.scrollBy(0,600)", "");
					sleep(500);

				}

				if (Otherdocument.equalsIgnoreCase("Yes")) {

					wait.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//*[contains(@id,'additionalDetailsForm_upload_CQ909_7')]")))
							.sendKeys(Attachpath);
					report.updateTestLog("File attachment:", "File is uploaded", Status.PASS);
					sleep(1300);
					JavascriptExecutor exe = (JavascriptExecutor) driver;
					exe.executeScript("window.scrollBy(0,600)", "");
					sleep(600);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
							.click();
					sleep(1000);
					report.updateTestLog("Attached file", "Attach Button is Selected", Status.PASS);

				}
			}

			if (Attachmenttypes.equalsIgnoreCase("newresidence")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[1]")))
						.sendKeys(Attachpath);
				report.updateTestLog("File attachment:", "File is uploaded", Status.PASS);
				sleep(500);

				report.updateTestLog("Attached file button", "Attach Button is Selected", Status.PASS);

				if (Otherdocument.equalsIgnoreCase("Yes")) {

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='file'])[2]")))
							.sendKeys(Attachpath);
					report.updateTestLog("File attachment:", "File is uploaded", Status.PASS);
					sleep(1300);
					JavascriptExecutor exe = (JavascriptExecutor) driver;
					exe.executeScript("window.scrollBy(0,600)", "");
					sleep(600);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']")))
							.click();
					sleep(500);
					report.updateTestLog("Attached file", "Attach Button is Selected", Status.PASS);

				}
			}

			// ***Agree button**//

			if (Deathcertificate.equalsIgnoreCase("No") && Receipt.equalsIgnoreCase("No")
					&& Otherdocument.equalsIgnoreCase("No")) {

				wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
				sleep(500);
				report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);

			}

			// ****Next button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
			sleep(500);
			report.updateTestLog("Required documents not attached", "Selected next button", Status.PASS);

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	public void Eclaims_Reviewclaims_life() {

		try

		{

			// ********Field validation*******//
			WebDriverWait wait = new WebDriverWait(driver, 18);

			String ClaimType = dataTable.getData("Eclaims_Life", "ClaimType");
			String Creditcard = dataTable.getData("Eclaims_Life", "Creditcard");
			String Title = dataTable.getData("Eclaims_Life", "Title");
			String DOB = dataTable.getData("Eclaims_Life", "DOB");
			String Name = dataTable.getData("Eclaims_Life", "Name");
			String Surname = dataTable.getData("Eclaims_Life", "Surname");
			String Address = dataTable.getData("Eclaims_Life", "Address1");
			String Addresss = dataTable.getData("Eclaims_Life", "Address2");
			String suburb = dataTable.getData("Eclaims_Life", "suburb");
			String state = dataTable.getData("Eclaims_Life", "State");
			String postcode = dataTable.getData("Eclaims_Life", "Postcode");
			String PreferredPhoneNO = dataTable.getData("Eclaims_Life", "PreferredPhoneNO");
			String Email = dataTable.getData("Eclaims_Life", "Email");
			String Preferredmode = dataTable.getData("Eclaims_Life", "Preferredmode");
			String Contacttime = dataTable.getData("Eclaims_Life", "Contacttime");
			String LifeEvent = dataTable.getData("Eclaims_Life", "LifeEvent");
			String Lifeeventtype = dataTable.getData("Eclaims_Life", "Lifeeventtype");
			String settlementdate = dataTable.getData("Eclaims_Life", "settlementdate");
			String carpurchasedate = dataTable.getData("Eclaims_Life", "carpurchasedate");
			String Mainresidence = dataTable.getData("Eclaims_Life", "Mainresidence");
			String marriagedate = dataTable.getData("Eclaims_Life", "MarriageDate");
			String Childbirthdate = dataTable.getData("Eclaims_Life", "Childbirthdate");
			String Adoptiondate = dataTable.getData("Eclaims_Life", "Adoptiondate");
			String SpouseYN = dataTable.getData("Eclaims_Life", "SpouseYN");
			String Attachmenttypes = dataTable.getData("Eclaims_Life", "Attachmenttypes");
			String Otherdocument = dataTable.getData("Eclaims_Life", "Otherdocument");
			String Product = dataTable.getData("Eclaims_Life", "Product");
			String Deathcertificate = dataTable.getData("Eclaims_Life", "Deathcertificate");
			String Otherdocs = dataTable.getData("Eclaims_Life", "Otherdocument");
			String path = dataTable.getData("Eclaims_Life", "path");

			WebElement product = driver.findElement(By.xpath("//label[@class='ng-binding']"));
			WebElement claimtype = driver
					.findElement(By.xpath("//label[text()='Claim Type ']/following-sibling::label"));
			WebElement creditcard = driver.findElement(By.xpath(
					"//label[text()='Credit card / facility no. (Please enter the last 4 digits) ']/following-sibling::label"));
			WebElement title = driver.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[1]"));
			WebElement Dob = driver
					.findElement(By.xpath("//label[text()='Date of birth (dd/mm/yyyy) ']/following-sibling::label"));
			WebElement GivenName = driver
					.findElement(By.xpath("(//label[text()='Given name ']/following-sibling::label)[1]"));
			WebElement SurName = driver
					.findElement(By.xpath("(//label[text()='Surname ']/following-sibling::label)[1]"));
			WebElement Address1 = driver
					.findElement(By.xpath("(//label[text()='Address1 ']/following-sibling::label)[1]"));
			WebElement Address2 = driver
					.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[1]"));
			WebElement Suburb = driver.findElement(By.xpath("(//label[text()='Suburb ']/following-sibling::label)[1]"));
			WebElement State = driver.findElement(By.xpath("(//label[text()='State ']/following-sibling::label)[1]"));
			WebElement postcodes = driver
					.findElement(By.xpath("(//label[text()='Post Code ']/following-sibling::label)[1]"));
			WebElement Preferredphoneno = driver
					.findElement(By.xpath("//label[text()='Preferred phone no ']/following-sibling::label"));
			WebElement Emailaddress = driver
					.findElement(By.xpath("//label[text()='Email address ']/following-sibling::label"));

			String product_validation = product.getText();
			String claimtype_validation = claimtype.getText();
			String creditcard_validation = creditcard.getText();
			String title_validation = title.getText();
			String Dob_validation = Dob.getText();
			String GivenName_validation = GivenName.getText();
			String SurName_validation = SurName.getText();
			String Address1_validation = Address1.getText();
			String Address2_validation = Address2.getText();
			String Suburb_validation = Suburb.getText();
			String State_validation = State.getText();
			String postcode_validation = postcodes.getText();
			String Preferredphoneno_validation = Preferredphoneno.getText();
			String Emailaddress_validation = Emailaddress.getText();

			// ****Product*****\\
			if (Product.contains(product_validation)) {

				report.updateTestLog("Product validation:", Product + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Product validation:", Product + " is not displayed as expected", Status.FAIL);
			}

			JavascriptExecutor a = (JavascriptExecutor) driver;
			a.executeScript("window.scrollBy(0,100)", "");
			sleep(500);

			// ****Validate claim type*****\\
			if (ClaimType.contains(claimtype_validation)) {

				report.updateTestLog("Claim Type validation:", ClaimType + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Claim Type validation:", ClaimType + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Enter creditcard number*****\\

			if (Creditcard.contains(creditcard_validation)) {

				report.updateTestLog("Credit card validation:", Creditcard + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Credit card validation:", Creditcard + " is not displayed as expected",
						Status.FAIL);
			}

			// ****Select title*****\\
			if (Title.contains(title_validation)) {

				report.updateTestLog("Title validation:", Title + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Title validation:", Title + " is not displayed as expected", Status.FAIL);
			}
			JavascriptExecutor b = (JavascriptExecutor) driver;
			b.executeScript("window.scrollBy(0,100)", "");
			sleep(500);

			// ****Enter DOB*****\\

			if (DOB.contains(Dob_validation)) {

				report.updateTestLog("DOB validation:", DOB + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("DOB validation:", DOB + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Name*****\\

			if (Name.contains(GivenName_validation)) {

				report.updateTestLog("Name validation:", Name + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Name validation:", Name + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter surname*****\\

			if (Surname.contains(SurName_validation)) {

				report.updateTestLog("Surname validation:", Surname + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Surname validation:", Surname + " is not displayed as expected", Status.FAIL);
			}

			JavascriptExecutor c = (JavascriptExecutor) driver;
			c.executeScript("window.scrollBy(0,150)", "");
			sleep(500);
			// ****Enter Address1*****\\

			if (Address.contains(Address1_validation)) {

				report.updateTestLog("Address 1 validation:", Address + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Address 1 validation:", Address + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Address2*****\\

			if (Addresss.contains(Address2_validation)) {

				report.updateTestLog("Address 2 validation:", Addresss + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Address 2 validation:", Addresss + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Suburb*****\\

			if (suburb.contains(Suburb_validation)) {

				report.updateTestLog("Suburb validation:", suburb + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Suburb validation:", suburb + " is not displayed as expected", Status.FAIL);
			}

			// ****Select State*****\\

			if (state.contains(State_validation)) {

				report.updateTestLog("State validation:", state + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("State validation:", state + " is not displayed as expected", Status.FAIL);
			}
			JavascriptExecutor d = (JavascriptExecutor) driver;
			d.executeScript("window.scrollBy(0,150)", "");
			sleep(500);

			// ****Enter Postal code*****\\

			if (postcode.contains(postcode_validation)) {

				report.updateTestLog("Post code validation:", postcode + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Post code validation:", postcode + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Emailaddress*****\\

			if (Email.contains(Emailaddress_validation)) {

				report.updateTestLog("Email validation:", Email + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Email validation:", Email + " is not displayed as expected", Status.FAIL);
			}

			// ****Enter Peferred phone number*****\\

			if (PreferredPhoneNO.contains(Preferredphoneno_validation)) {

				report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO + " is displayed as Expected",
						Status.PASS);
			} else {
				report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO + " is not displayed as expected",
						Status.FAIL);
			}

			WebElement Modeofcontact = driver.findElement(By.xpath(
					"//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
			String Modeofcontact_validation = Modeofcontact.getText();

			// ****Preferred mode of contact (Phone / Email / Letter)?*****\\

			if (Preferredmode.equalsIgnoreCase("Phone")) {

				if (Preferredmode.contains(Modeofcontact_validation)) {

					report.updateTestLog("Preferred Mode of Contact validation:",
							Preferredmode + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Preferred Mode of Contact validation:",
							Preferredmode + " is not displayed as expected", Status.FAIL);
				}
				WebElement timeofcontact = driver.findElement(
						By.xpath("//label[text()='Preferred contact time: AM / PM ']/following-sibling::label"));
				String timeofcontact_validation = timeofcontact.getText();
				if (Contacttime.contains(timeofcontact_validation)) {

					report.updateTestLog("Time of contact validation :", Contacttime + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Time of contact validation :", Contacttime + " is not displayed as expected",
							Status.FAIL);
				}

			}
			if (Preferredmode.equalsIgnoreCase("Email")) {

				if (Preferredmode.contains(Modeofcontact_validation)) {

					report.updateTestLog("Email validation:", Preferredmode + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Email validation:", Preferredmode + " is not displayed as expected",
							Status.FAIL);
				}

			} else {
				if (Preferredmode.contains(Modeofcontact_validation)) {

					report.updateTestLog("Letter validation :", Preferredmode + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Letter validation :", Preferredmode + " is not displayed as expected",
							Status.FAIL);
				}

			}

			JavascriptExecutor e = (JavascriptExecutor) driver;
			e.executeScript("window.scrollBy(0,800)", "");
			sleep(500);

			// ****Life Event******\\
			WebElement Lifeevent = driver
					.findElement(By.xpath("//label[text()='Life Event ']/following-sibling::label"));
			String Lifeevent_validation = Lifeevent.getText();

			if (LifeEvent.contains(Lifeevent_validation)) {

				report.updateTestLog("Date of birth validation:", LifeEvent + " is displayed as Expected", Status.PASS);
			} else {
				report.updateTestLog("Date of birth validation:", LifeEvent + " is not displayed as expected",
						Status.FAIL);
			}

			if (Lifeevent.equals("Purchase a brand new car")) {

				WebElement typeoflifeevent = driver
						.findElement(By.xpath("//label[text()='Date of new car purchase ']/following-sibling::label"));

				String typeoflifeevent_validation = typeoflifeevent.getText();

				// ****Date of new purchase*****\\

				if (carpurchasedate.contains(typeoflifeevent_validation)) {

					report.updateTestLog("Date of new purchase Validation:",
							carpurchasedate + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Date of new purchase Validation :",
							carpurchasedate + " is not displayed as expected", Status.FAIL);
				}

			}

			if (Lifeeventtype.equalsIgnoreCase("Home")) {
				WebElement settlementdates = driver.findElement(
						By.xpath("//label[text()='Settlement date of home purchase ']/following-sibling::label"));
				String settlementdate_validation = settlementdates.getText();
				WebElement datepurchase = driver.findElement(By.xpath(
						"//label[text()='Will this house be your main residence from the date of purchase? ']/following-sibling::label"));
				String datepurchase_validation = datepurchase.getText();

				if (settlementdate.contains(settlementdate_validation)) {

					report.updateTestLog("Settlement date of home purchase Validation:",
							settlementdate + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Settlement date of home purchase Validation :",
							settlementdate + " is not displayed as expected", Status.FAIL);
				}

				if (Mainresidence.equalsIgnoreCase("Yes")) {
					if (Mainresidence.contains(datepurchase_validation)) {

						report.updateTestLog("Date of new purchase Validation:",
								Mainresidence + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Date of new purchase Validation :",
								Mainresidence + " is not displayed as expected", Status.FAIL);
					}

				} else {
					if (Mainresidence.contains(datepurchase_validation)) {
						report.updateTestLog("Date of new purchase Validation:",
								Mainresidence + " is displayed as Expected", Status.PASS);
					} else {
						report.updateTestLog("Date of new purchase Validation :",
								Mainresidence + " is not displayed as expected", Status.FAIL);
					}
				}
			}

			if (Lifeeventtype.equalsIgnoreCase("Marriage")) {

				WebElement marriagedates = driver
						.findElement(By.xpath("//label[text()='Wedding date ']/following-sibling::label"));
				String marriagedate_validation = marriagedates.getText();
				if (marriagedate.contains(marriagedate_validation)) {

					report.updateTestLog("Marraige date Validation :", marriagedate + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Marraige date Validation :", marriagedate + " is not displayed as expected",
							Status.FAIL);
				}
			}

			if (Lifeeventtype.equalsIgnoreCase("child")) {
				WebElement marriagedates = driver
						.findElement(By.xpath("//label[text()='Life Event ']/following::label[4]"));
				String marriagedate_validation = marriagedates.getText();
				if (Childbirthdate.contains(marriagedate_validation)) {

					report.updateTestLog("Marraige date Validation :", Childbirthdate + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Marraige date Validation :", Childbirthdate + " is not displayed as expected",
							Status.FAIL);
				}

			}
			if (Lifeeventtype.equalsIgnoreCase("adoption")) {

				WebElement Adoptiondates = driver
						.findElement(By.xpath("//label[text()='Date of adoption ']/following-sibling::label"));
				String Adoptiondates_validation = Adoptiondates.getText();
				if (Adoptiondate.contains(Adoptiondates_validation)) {

					report.updateTestLog("Adoption date Validation :", Adoptiondate + " is displayed as Expected",
							Status.PASS);
				} else {
					report.updateTestLog("Adoption date Validation :", Adoptiondate + " is not displayed as expected",
							Status.FAIL);
				}

			}

			if (Lifeeventtype.equalsIgnoreCase("spouse")) {
				WebElement passeddate = driver.findElement(
						By.xpath("//label[text()='Date your spouse passed away ']/following-sibling::label"));
				String passeddate_validation = passeddate.getText();
				if (Adoptiondate.contains(passeddate_validation)) {

					report.updateTestLog("Settlement date of home purchase Validation:",
							Adoptiondate + " is displayed as Expected", Status.PASS);
				} else {
					report.updateTestLog("Settlement date of home purchase Validation :",
							Adoptiondate + " is not displayed as expected", Status.FAIL);
				}

				if (SpouseYN.equalsIgnoreCase("Yes")) {
					WebElement domestic = driver.findElement(By.xpath(
							"//label[text()='Were you in a bonafide domestic relationship with your spouse immediately prior to their death? ']/following-sibling::label"));
					String domestic_validation = domestic.getText();
					if (Mainresidence.contains(domestic_validation)) {

						report.updateTestLog("Date of new purchase Validation:", SpouseYN + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Date of new purchase Validation :",
								SpouseYN + " is not displayed as expected", Status.FAIL);
					}

				} else {
					WebElement domestic = driver.findElement(By.xpath(
							"//label[text()='Were you in a bonafide domestic relationship with your spouse immediately prior to their death? ']/following-sibling::label"));
					String domestic_validation = domestic.getText();
					if (SpouseYN.contains(domestic_validation)) {
						report.updateTestLog("Date of new purchase Validation:", SpouseYN + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Date of new purchase Validation :",
								SpouseYN + " is not displayed as expected", Status.FAIL);
					}
				}
			}

			JavascriptExecutor t = (JavascriptExecutor) driver;
			t.executeScript("window.scrollBy(0,600)", "");
			sleep(500);

			// ****Attachpath******\\

			if (Attachmenttypes.equalsIgnoreCase("Brandnewcar")) {
				if (Deathcertificate.equalsIgnoreCase("Yes")) {
					WebElement Deathcert = driver.findElement(
							By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[2]"));
					String Deathcert_validation = Deathcert.getText();
					if (path.contains(Deathcert_validation)) {
						report.updateTestLog("Document name validation:", path + " is displayed as Expected",
								Status.PASS);
					} else {
						report.updateTestLog("Document name validation:", path + " is not displayed as expected",
								Status.FAIL);
					}

					if (path.equalsIgnoreCase("Yes")) {
						WebElement Deathcertt = driver.findElement(By
								.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[5]"));
						String Deathcertt_validation = Deathcertt.getText();
						if (Otherdocs.contains(Deathcertt_validation)) {
							report.updateTestLog("Other Document name validation:", path + " is displayed as Expected",
									Status.PASS);

						} else {
							report.updateTestLog("Receipt of purchase name validation:",
									path + " is not displayed as expected", Status.FAIL);
						}
					}

					if (Otherdocument.equalsIgnoreCase("Yes")) {
						WebElement otherdocumentname = driver.findElement(By.xpath("(//table/./following::td)[2]"));
						String otherdocumentname_validation = otherdocumentname.getText();
						if (path.contains(otherdocumentname_validation)) {
							report.updateTestLog("Other Document name validation:", path + " is displayed as Expected",
									Status.PASS);

						} else {
							report.updateTestLog("Other Document name validation:",
									path + " is not displayed as expected", Status.FAIL);
						}
					}
				}
			}

			JavascriptExecutor k = (JavascriptExecutor) driver;
			k.executeScript("window.scrollBy(0,700)", "");
			sleep(500);

			if (Attachmenttypes.equalsIgnoreCase("newresidence")) {
				WebElement Deathcer = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[2]"));
				String Deathcer_validation = Deathcer.getText();

				if (path.contains(Deathcer_validation)) {

					report.updateTestLog("Document name validation:", path + " is displayed as Expected", Status.PASS);

				} else {
					report.updateTestLog("Document name validation:", path + " is not displayed as expected",
							Status.FAIL);
				}

				WebElement otherdocumentname = driver.findElement(
						By.xpath("(//*[text()='Required Documentation']/./following::div/table/tbody/tr/td)[5]"));
				String otherdocumentname_validation = otherdocumentname.getText();

				if (path.contains(otherdocumentname_validation)) {

					report.updateTestLog("Other Document name validation:", path + " is displayed as Expected",
							Status.PASS);

				} else {
					report.updateTestLog("Other Document name validation:", path + " is not displayed as expected",
							Status.FAIL);
				}

			}
			// ****Review Claim Details******\\

			// ***Agree button**//

			wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
			sleep(500);
			report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);

			// ***Submit button**//

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12']/button[@type='submit']")))
					.click();
			sleep(500);
			report.updateTestLog("Acknowledgement checkbox", "Checkbox is clicked", Status.PASS);
			sleep(500);

			// ******Download the PDF***********\\
			properties = Settings.getInstance();
			String StrBrowseName = properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(1500);
			}
			if (StrBrowseName.equalsIgnoreCase("Firefox")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(1500);
				Robot roboobj = new Robot();
				roboobj.keyPress(KeyEvent.VK_ENTER);
			}

			if (StrBrowseName.equalsIgnoreCase("InternetExplorer")) {

				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();

				sleep(1500);
			}

			report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

			sleep(1000);

			JavascriptExecutor h = (JavascriptExecutor) driver;
			h.executeScript("window.scrollBy(0,1000)", "");
			sleep(800);

			// ***Finish button**//

			wait.until(ExpectedConditions.elementToBeClickable(
					By.xpath("//div[@class='col-xs-6 col-sm-2 right-col']/button[@type='submit']"))).click();
			sleep(500);
			report.updateTestLog("Finish button:", "Finish button is clicked", Status.PASS);

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}


public void Eclaims_Lifeevent_negativescenario(){
	try{
		
		String Creditcard = dataTable.getData("Eclaims_Life", "Creditcard");
		String Title = dataTable.getData("Eclaims_Life", "Title");
		String DOB = dataTable.getData("Eclaims_Life", "DOB");
		String Name = dataTable.getData("Eclaims_Life", "Name");
		String Surname = dataTable.getData("Eclaims_Life", "Surname");
		String Address = dataTable.getData("Eclaims_Life", "Address1");
		String Addresss = dataTable.getData("Eclaims_Life", "Address2");
		String suburb = dataTable.getData("Eclaims_Life", "suburb");
		String state = dataTable.getData("Eclaims_Life", "State");
		String postcode = dataTable.getData("Eclaims_Life", "Postcode");
		String PreferredPhoneNO = dataTable.getData("Eclaims_Life", "PreferredPhoneNO");
		String Otherphoneno = dataTable.getData("Eclaims_Life", "Otherphoneno");
		String Email = dataTable.getData("Eclaims_Life", "Email");
		String Preferredmode = dataTable.getData("Eclaims_Life", "Preferredmode");
		String LifeEvent = dataTable.getData("Eclaims_Life", "LifeEvent");
		String Lifeeventtype = dataTable.getData("Eclaims_Life", "Lifeeventtype");
		String settlementdate = dataTable.getData("Eclaims_Life", "settlementdate");
		String carpurchasedate = dataTable.getData("Eclaims_Life", "carpurchasedate");
		String Mainresidence = dataTable.getData("Eclaims_Life", "Mainresidence");
		String marriagedate = dataTable.getData("Eclaims_Life", "MarriageDate");
		String Childbirthdate = dataTable.getData("Eclaims_Life", "Childbirthdate");
		String Adoptiondate = dataTable.getData("Eclaims_Life", "Adoptiondate");
		String SpouseYN = dataTable.getData("Eclaims_Life", "SpouseYN");		
		String Contacttime = dataTable.getData("Eclaims_Life", "Contacttime");
		String Doberror = dataTable.getData("Eclaims_Life", "Doberror");
		String emailerror = dataTable.getData("Eclaims_Life", "emailerror");
		String phonenoerror = dataTable.getData("Eclaims_Life", "phonenoerror");
		String weddingdateerror = dataTable.getData("Eclaims_Life", "weddingdateerror");
		String Otherphonenoerror = dataTable.getData("Eclaims_Life", "Otherphonenoerror");
		String Mandatoryerror = dataTable.getData("Eclaims_Life", "Mandatoryerror");	
		String Postcodeerror = dataTable.getData("Eclaims_Life", "Postcodeerror");
		
		WebDriverWait wait = new WebDriverWait(driver, 18);
		
		
		// ****Click on claim type*****\\

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-LIFEEVNTS']/parent::*")))
				.click();
		sleep(500);
		report.updateTestLog("Life Event claim :", "Life Event is Selected", Status.PASS);

		// ****Enter creditcard number*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
		report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",
				Creditcard + " is entered", Status.PASS);

		// ****Select title*****\\

		Select titletype = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));
		titletype.selectByVisibleText(Title);
		report.updateTestLog("Enter Title:", Title + " is selected ", Status.PASS);

		// ****Enter DOB*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2"))).click();
		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2")))
				.sendKeys(DOB);
		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Given name ']"))).click();
		report.updateTestLog("Enter Date of birth :", DOB + " is entered", Status.PASS);

		// ****Enter Name*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0")))
				.sendKeys(Name);
		report.updateTestLog("Enter Given name:", Name + " is entered", Status.PASS);

		// ****Enter surname*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1")))
				.sendKeys(Surname);
		report.updateTestLog("Enter Surname name:", Surname + " is entered", Status.PASS);

		// ****Enter Address1*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0")))
				.sendKeys(Address);
		report.updateTestLog("Enter Given Address1:", Address + " is entered", Status.PASS);

		// ****Enter Address2*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1")))
				.sendKeys(Addresss);
		report.updateTestLog("Enter Given Address2:", Addresss + " is entered", Status.PASS);

		// ****Enter Suburb*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0")))
				.sendKeys(suburb);
		report.updateTestLog("Enter Suburb:", suburb + " is entered", Status.PASS);

		// ****Select State*****\\

		Select Stateselect = new Select(
				driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));
		Stateselect.selectByVisibleText(state);
		report.updateTestLog("Select State:", state + " is selected ", Status.PASS);
		sleep(500);

		// ****Enter Postal code*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2")))
				.sendKeys(postcode);
		sleep(500);
		report.updateTestLog("Enter Post code:", postcode + " is entered", Status.PASS);
		sleep(300);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		sleep(500);

		// ****Enter Emailaddress*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ213_0")))
				.sendKeys(Email);
		report.updateTestLog("Enter Email:", Email + " is entered", Status.PASS);

		// ****Enter Peferred phone number*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ211_1")))
				.sendKeys(PreferredPhoneNO);
		sleep(500);
		report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO + " is entered", Status.PASS);
		sleep(500);

		// ****Enter other phone number*****\\
		wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ210_2")))
				.sendKeys(Otherphoneno);
		sleep(500);
		report.updateTestLog("Enter otherphonenumber:", Otherphoneno + " is entered", Status.PASS);
		sleep(500);

		// ****Preferred mode of contact (Phone / Email / Letter)?*****\\
		JavascriptExecutor h = (JavascriptExecutor) driver;
		h.executeScript("window.scrollBy(0,400)", "");
		sleep(500);
		if (Preferredmode.equalsIgnoreCase("Phone")) {

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*")))
					.click();
			sleep(1500);
			report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);

			Select mode = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ215_1")));
			mode.selectByVisibleText(Contacttime);
			report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):",
					Contacttime + " is selected ", Status.PASS);
			sleep(500);
		}
		if (Preferredmode.equalsIgnoreCase("Email")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
		}
		if (Preferredmode.equalsIgnoreCase("Letter")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*")))
					.click();
			sleep(500);
			report.updateTestLog("Preferred Mode of Contact ", Preferredmode + " is Selected ", Status.PASS);
		}

		JavascriptExecutor j = (JavascriptExecutor) driver;
		j.executeScript("window.scrollBy(0,200)", "");
		sleep(900);

		// ***************LIFE EVENT***************//

		// ****Life Event*****\\

		sleep(500);
		Select lifeevent = new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ3001_2")));
		lifeevent.selectByVisibleText(LifeEvent);
		sleep(500);
		report.updateTestLog("Life Event :", LifeEvent + " is selected ", Status.PASS);

		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("window.scrollBy(0,200)", "");
		sleep(500);
		// ****Date of new car purchase*******\\

		if (Lifeeventtype.equalsIgnoreCase("Newcar")) {

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3002_3")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3002_3")))
					.sendKeys(carpurchasedate);
			sleep(500);
			report.updateTestLog("Date of new car purchase :", carpurchasedate + " is Entered", Status.PASS);

		}

		if (Lifeeventtype.equalsIgnoreCase("Home")) {

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3007_4")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3007_4")))
					.sendKeys(settlementdate);
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Life Event ']"))).click();
			sleep(300);
			report.updateTestLog("SettlementDate :", settlementdate + " is Entered", Status.PASS);
			if (Mainresidence.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
						.click();
				sleep(500);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
						.click();
				sleep(500);
			}
		}
		if (Lifeeventtype.equalsIgnoreCase("Marriage")) {
			sleep(300);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3003_5")))
					.click();
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3003_5")))
					.sendKeys(marriagedate);
			sleep(500);
			report.updateTestLog("Marraige Date :", marriagedate + " is Entered", Status.PASS);

		}

		if (Lifeeventtype.equalsIgnoreCase("child")) {
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3004_6")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3004_6")))
					.sendKeys(Childbirthdate);
			sleep(500);
			report.updateTestLog("Child birth Date :", Childbirthdate + " is Entered", Status.PASS);

		}
		if (Lifeeventtype.equalsIgnoreCase("adoption")) {
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3005_7")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3005_7")))
					.sendKeys(Adoptiondate);
			sleep(500);
			report.updateTestLog("Child Adoption Date :", Adoptiondate + " is Entered", Status.PASS);

		}

		if (Lifeeventtype.equalsIgnoreCase("spouse")) {
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3006_9")))
					.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ3006_9")))
					.sendKeys(Adoptiondate);
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Life Event ']"))).click();
			sleep(300);
			report.updateTestLog("Child Adoption Date :", Adoptiondate + " is Entered", Status.PASS);
			report.updateTestLog(
					"Were you in a bonafide domestic relationship with your spouse immediately prior to their death :",
					Lifeeventtype + " is Selected", Status.PASS);
			if (SpouseYN.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Yes']/parent::*")))
						.click();
				report.updateTestLog(
						"Were you in a bonafide domestic relationship with your spouse immediately prior to their death :",
						SpouseYN + " is Selected", Status.PASS);
				sleep(500);
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='No']/parent::*")))
						.click();
				report.updateTestLog(
						"Were you in a bonafide domestic relationship with your spouse immediately prior to their death :",
						SpouseYN + " is Selected", Status.PASS);
				sleep(500);
			}

		}

		// ****Proceed button******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary w100p']")))
				.click();
		sleep(500);
		report.updateTestLog("Proceed Button", "clicked", Status.PASS);
		
		WebElement Doberrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[1]"));
		WebElement emailerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[2]"));
		WebElement postcodeerrormessage = driver.findElement(
				By.xpath("(//div[@class='ng-scope'])[2]"));
		WebElement phonenoerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[3]"));
		WebElement otherphonenoerrormessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[4]"));
		WebElement Deathcermessage = driver.findElement(
				By.xpath("(//div[@class='error-message ng-binding ng-scope'])[5]"));
		String Doberror_validation = Doberrormessage.getText();
		String emailerror_validation = emailerrormessage.getText();
		String phonenoerror_validation = phonenoerrormessage.getText();
		String weddingdateerror_validation = Deathcermessage.getText();
		String otherphonenoerror_validation  = otherphonenoerrormessage.getText();
		String postcodeerror_validation  = postcodeerrormessage.getText();
		
		if (Doberror.contains(Doberror_validation)) {

			report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is not displayed as expected",
					Status.FAIL);
		}
		
		if (Postcodeerror.contains(postcodeerror_validation)) {

			report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is not displayed as expected",
					Status.FAIL);
		}
		
		if (emailerror.contains(emailerror_validation)) {

			report.updateTestLog("Email address Error validation:", emailerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Email address Error validation:", emailerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (phonenoerror.contains(phonenoerror_validation)) {

			report.updateTestLog("Phone number Error message validation:", phonenoerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Phone number Error message validation:", phonenoerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (Otherphonenoerror.contains(otherphonenoerror_validation)) {

			report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is not displayed as expected",
					Status.FAIL);
		}
		if (weddingdateerror.contains(weddingdateerror_validation)) {

			report.updateTestLog("Future Wedding date Error message validation:", weddingdateerror + " is displayed as Expected",
					Status.PASS);

		} else {
			report.updateTestLog("Future Wedding date Error message validation:", weddingdateerror + " is not displayed as expected",
					Status.FAIL);
		}
		
		

		
	}catch (Exception e) {
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}
}






