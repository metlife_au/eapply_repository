/*
 * Author 175373
 */
package au.com.metlife.eapply.bs.pdf;




import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.constants.EapplicationPdfConstants;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.helper.EapplyServiceHelper;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.ApplicantInfo;
import au.com.metlife.eapply.bs.model.ContactDetails;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverInfo;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureStatement;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberProductDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.pdf.pdfObject.PersonalDetail;
import au.com.metlife.eapply.bs.pdf.pdfObject.TransferPreviousQuestionAns;
import au.com.metlife.eapply.bs.utility.CommonPDFHelper;
import au.com.metlife.eapply.quote.utility.QuoteConstants;

public class PDFbusinessObjectMapperNew {
	private static final Logger log = LoggerFactory.getLogger(PDFbusinessObjectMapper.class);
	
	 public static final String PACKAGE_NAME = "com.metlife.eapply.pdf";
	 public static final String CLASS_NAME = "PDFbusinessObjectMapperMTAA";
	 


	 
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public static PDFObject getPdfOjectsForEapply(Eapplication applicationDTO, EappInput eappInput, String lob, String propertyLoc) throws IOException{
		log.info("Pdf object mapping start");
		PDFObject pdfObject = new PDFObject();
		MemberProductDetails memberProductDetails = null;
		ArrayList<MemberProductDetails> memberProductDtsList = null;	
		MemberDetails memberDetails = null;
		Applicant applicantDTO = null;
		Cover coversDTO = null;
		Double cost = null;
		StringBuilder alldecreasons = new StringBuilder();
		java.util.Properties property = new java.util.Properties();
        File file = null;
        InputStream url = null;   
		ArrayList<PersonalDetail> personalDtslist = new ArrayList<>();
		DisclosureStatement disclosureStatement = null;
		List<DisclosureStatement> disclosureStatementList = new ArrayList<>();
		List<DisclosureStatement> disclosureStatementListGc = new ArrayList<>();
		List<DisclosureStatement> disclosureStatementListTe = new ArrayList<>();
		List<DisclosureStatement> disclosureStatementListPc = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureincomeQuestionAnsList = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureExpenseQuestionAnsList = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureOtherFinanceQuestionAnsList = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureOtherExpenseQuestionAnsList = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureDebtDetailsQuestionAnsList = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureAssumptionQuestionAnsList = new ArrayList<>();
		List<DisclosureQuestionAns> disclosureChildListQuestionAnsList = new ArrayList<>();
		DisclosureQuestionAns disclosureQuestionAns = null;
		ApplicantInfo applicantInfo = null;
		String preferedTime =  null;
		//EapplyInput eapplyInput = null;//local change
		if(null!=applicationDTO){
			file = new File( propertyLoc);
			log.info("propertyLoc>>> {}",propertyLoc);
	         url = new FileInputStream( file);    
	        property.load( url);
			memberDetails = new MemberDetails();
			log.info("url>>> {}",url);
			if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob)){
				memberDetails.setBrand(applicationDTO.getPartnercode());
			}			
			
			
			 memberDetails.setExistingExclusion("None");
			 if(null!=applicationDTO.getTotalMonthlyPremium()){
				memberDetails.setTotalPremium(CommonPDFHelper.formatAmount(applicationDTO.getTotalMonthlyPremium().toString()));
		     }else{
		    	 memberDetails.setTotalPremium(CommonPDFHelper.formatAmount("0"));
		     }
			 pdfObject.setApplicationDecision(applicationDTO.getAppdecision());
			 
			 pdfObject.setRequestType(applicationDTO.getRequesttype());		
			 pdfObject.setCalculatorFlag(eappInput.getCalculatorFlag());
			 if("insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())){
				 if(eappInput.getApplicant().isNursingCosts()){
					 pdfObject.setIncludeNursingCosts("yes");
				 }else{
					 pdfObject.setIncludeNursingCosts("No");
				 }
				 if(eappInput.getApplicant().isTpdWithoutIP()){
					 pdfObject.setExcludeIP("yes");
				 }else{
					 pdfObject.setExcludeIP("No");
				 }
			 }
			 if( ("premium".equalsIgnoreCase(eappInput.getCalculatorFlag())|| "insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())) && eappInput.isQuickQuoteRender()) {
					pdfObject.setQuickQuoteRender(true);
				}

			 if(!"CORP".equalsIgnoreCase(eappInput.getPartnerCode())) {
				 pdfObject.setDecPrivacy(property.getProperty("label_privacy_stmt_pdf"));
			 }
			 pdfObject.setDecGC(property.getProperty("label_genral_concent_pdf")); 
			 pdfObject.setDecDDS(property.getProperty("label_duty_disclouser_pdf"));

			 disclosureStatement = new DisclosureStatement();
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_tccover_pdf"));
			 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_uwcover_pdf"));
			 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "NCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_nonmembr_pdf"));
			 }else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) {
				 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_scover_pdf"));
			 }
			//Added for Life event general consent Hostplus
			 else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_iccover_pdf"));
			 }
			 else{
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_pdf"));
			 }
			 disclosureStatement.setAnswer("true");
			 disclosureStatement.setAckText(property.getProperty("label_genrl_ack_text"));
			 
			 
			 /*POH-540 - General consent not in the Acknowledgement page for Cancel Cover but its in the PDF generated. - 169682*/
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
					&&  null!=applicationDTO.getAppdecision() && !"".equalsIgnoreCase(applicationDTO.getAppdecision()) && !"DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
				 disclosureStatementListGc.add(disclosureStatement);
			 }
			
			 else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
			 {
				 StringBuilder sb = new StringBuilder();
				 if(null!=eappInput.getCoverCancellation())
				 {
					 sb = sb.append(eappInput.getCoverCancellation().isDeath() ? EapplicationPdfConstants.DEATH : "")
							 .append(eappInput.getCoverCancellation().isDeath() && eappInput.getCoverCancellation().isTpd() && eappInput.getCoverCancellation().isSalContinuance() ? " , " : eappInput.getCoverCancellation().isDeath() && eappInput.getCoverCancellation().isTpd() && !eappInput.getCoverCancellation().isSalContinuance() ? MetlifeInstitutionalConstants.AND_STR : "")
							 .append(eappInput.getCoverCancellation().isTpd() ? EapplicationPdfConstants.TPD : eappInput.getCoverCancellation().isDeath() && !eappInput.getCoverCancellation().isTpd() && eappInput.getCoverCancellation().isSalContinuance() ? MetlifeInstitutionalConstants.AND_STR : "")
							 .append(eappInput.getCoverCancellation().isTpd() && eappInput.getCoverCancellation().isSalContinuance() ? MetlifeInstitutionalConstants.AND_STR : "")
							 .append(eappInput.getCoverCancellation().isSalContinuance() ? EapplicationPdfConstants.IP : "");
				 }
				 
				 
				 if(property.getProperty(MetlifeInstitutionalConstants.LABEL_GENERAL_ACK)!=null && property.getProperty(MetlifeInstitutionalConstants.LABEL_GENERAL_ACK).contains("<%CancelCovers%>"))
				 {
					 disclosureStatement.setStatement("");
					 disclosureStatement.setAnswer("true");
					 disclosureStatement.setAckText(property.getProperty(MetlifeInstitutionalConstants.LABEL_GENERAL_ACK).replace("<%CancelCovers%>", sb.toString()));
					 disclosureStatementListGc.add(disclosureStatement);
				 }
			 
			 }
			 /*POH-540 - General consent not in the Acknowledgement page for Cancel Cover but its in the PDF generated.- 169682 */
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatement.setAckText(property.getProperty("label_dod_ack_text"));
			 /*POH-586 - Duty of Disclosure section not to be displayed for Cancel Cover PDF generated. - 169682 */
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatementList.add(disclosureStatement);
			 }
			 /*POH-586 - Duty of Disclosure section not to be displayed for Cancel Cover PDF generated. - 169682*/
			 
			 if(!"CORP".equalsIgnoreCase(eappInput.getPartnerCode())) {
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_privacy_stmt_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatement.setAckText(property.getProperty("label_privacy_ack_text"));
			 /*POH-586 - Privacy Statement section not to be displayed for Cancel Cover PDF generated.- 169682 */
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatementList.add(disclosureStatement);
			 }
			 }
			 /*POH-586 - Privacy Statement section not to be displayed for Cancel Cover PDF generated. - 169682*/
			//Added for ING Transfer cover
			 if("INGD".equalsIgnoreCase(eappInput.getPartnerCode()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement = new DisclosureStatement();
				 disclosureStatement.setStatement(property.getProperty("label_transfer_evidence_pdf"));
				 //disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_pdf"));
				 
				 disclosureStatement.setAnswer("true");
				 disclosureStatement.setAckText(null);
				 disclosureStatementListTe.add(disclosureStatement);
			 }// END
			  //Added for Host premium calculator
			    	if(("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) || "insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())) && null!=eappInput.getDisclaimerText()){
			    		disclosureStatement.setStatement(eappInput.getDisclaimerText());
			    		disclosureStatement.setAnswer("true");
			    		disclosureStatement.setAckText(null);
			    		disclosureStatementListPc.add(disclosureStatement);
			    		
			    	}
			 /*END This is a temp fix to stop displaying family question. Code need to change later		*/
			
			 if(null!=applicationDTO.getApplicant() && !(applicationDTO.getApplicant().isEmpty())){
				
				for(int itr=0;itr<applicationDTO.getApplicant().size(); itr++) {
					applicantDTO = applicationDTO.getApplicant().get(itr);
					if(null!=applicantDTO && EapplicationPdfConstants.APPLICANT_TYPE_PRIMARY.equalsIgnoreCase(applicantDTO.getApplicanttype())){	
						
						if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob) && null!=applicantDTO.getEmailid()){
							memberDetails.setClientEmailId(applicantDTO.getEmailid());
						}
						
						pdfObject.setFormLength("Long");
						if(null!=applicantDTO.getAustraliacitizenship() && "yes".equalsIgnoreCase(applicantDTO.getAustraliacitizenship())){
							memberDetails.setAustraliaCitizenship("Yes");
						}else{
							memberDetails.setAustraliaCitizenship("No");
						}
						if(eappInput.getApplicant() != null && !eappInput.getApplicant().getCover().isEmpty() 
								&& eappInput.getApplicant().getCover().get(0).getOccupationRating() != null) {
							memberDetails.setOccCategory(eappInput.getApplicant().getCover().get(0).getOccupationRating());
						}
						
						
						memberDetails.setGender(applicantDTO.getGender());
						memberDetails.setMemberFName(applicantDTO.getFirstname());
						memberDetails.setMemberLName(applicantDTO.getLastname());
						memberDetails.setTitle(applicantDTO.getTitle());
						
						if(eappInput.getApplicant()!= null && eappInput.getApplicant().getCustomerReferenceNumber()!= null){
							memberDetails.setEmailId(eappInput.getApplicant().getCustomerReferenceNumber());
						}
						memberDetails.setClientRefNum(applicantDTO.getClientrefnumber());
						
						if(!"CORP".equalsIgnoreCase(eappInput.getPartnerCode())) {
						memberDetails.setDateJoinedFund(applicantDTO.getDatejoinedfund());
						}
						
						if(applicationDTO.getClientmatch()!=null) {
							memberDetails.setClientMatched(applicationDTO.getClientmatch());
						}

							memberDetails.setMemberOccupation(applicantDTO.getOccupation());
							memberDetails.setMemberIndustry(applicantDTO.getIndustrytype());
							memberDetails.setEmpStatus(applicantDTO.getPermanentemptype());
							memberDetails.setAnnualSal(applicantDTO.getAnnualsalary());
							if(null!=applicantDTO.getMembertype() && !"".equalsIgnoreCase(applicantDTO.getMembertype())){
								memberDetails.setMemberType(applicantDTO.getMembertype());
							}
						
							ContactDetails contactInfo = null;
							StringBuilder addressBuilder = new StringBuilder();
							if(eappInput.getApplicant()!= null && eappInput.getApplicant().getContactDetails()!=null) {
								contactInfo = eappInput.getApplicant().getContactDetails();
					                
					                if(contactInfo.getPreferedContactNumber()!=null) {
					                	memberDetails.setPrefferedContactNumber(contactInfo.getPreferedContactNumber());
					                }
					                if (EapplyServiceHelper.isNotNullandNotEmpty(contactInfo.getAddressLine1())){
					                	addressBuilder.append(contactInfo.getAddressLine1()+ " ");
					                }	                
					                	
					                if (EapplyServiceHelper.isNotNullandNotEmpty(contactInfo.getAddressLine2())){
					                	addressBuilder.append( contactInfo.getAddressLine2() + ", ");			                	
					                }
					                if (EapplyServiceHelper.isNotNullandNotEmpty(contactInfo.getSuburb())){
					                	memberDetails.setSubrub(contactInfo.getSuburb());
					                	addressBuilder.append( contactInfo.getSuburb() + ", ");			                	
					                }
					                if (EapplyServiceHelper.isNotNullandNotEmpty(contactInfo.getState())){
					                	memberDetails.setState(contactInfo.getState());
					                	addressBuilder.append( contactInfo.getState() + ", ");			                	
					                }
					                if (contactInfo.getCountry()!=null){
					                	memberDetails.setCountry(contactInfo.getCountry());
					                	addressBuilder.append( contactInfo.getCountry() + ", ");			                	
					                }
					                	
					                if (EapplyServiceHelper.isNotNullandNotEmpty(contactInfo.getPostCode())) {
					                	addressBuilder.append(contactInfo.getPostCode());
					                }
								
							}else {
				            	addressBuilder.append( "");
				            }			
						
						if(eappInput.getApplicant() != null && eappInput.getApplicant().getBirthDate() != null){
							memberDetails.setDob(eappInput.getApplicant().getBirthDate());
						}			
						if(eappInput.getApplicant()!= null && eappInput.getApplicant().getContactDetails()!=null) {
							contactInfo = eappInput.getApplicant().getContactDetails();
							preferedTime = contactInfo.getPreferedContactTime();
							if(preferedTime!= null && "1".equalsIgnoreCase(preferedTime)){
				        		preferedTime = "9am - 12pm";
							}else{
								preferedTime = "12pm - 6pm";
							}
							memberDetails.setPrefferedContactTime(preferedTime);
								if(contactInfo.getPreferedContacType()!=null && ("1").equalsIgnoreCase(contactInfo.getPreferedContacType())){		
									memberDetails.setPrefContactDetails(contactInfo.getPreferedContactNumber());
									memberDetails.setPrefferedContactType("Mobile Phone");
								}else if(contactInfo.getPreferedContacType()!=null && "2".equalsIgnoreCase(contactInfo.getPreferedContacType())){	
									memberDetails.setPrefContactDetails(contactInfo.getPreferedContactNumber());
									memberDetails.setPrefferedContactType("Home Phone");
								}else if(contactInfo.getPreferedContacType()!=null && "3".equalsIgnoreCase(contactInfo.getPreferedContacType())){	
									memberDetails.setPrefContactDetails(contactInfo.getPreferedContactNumber());
									memberDetails.setPrefferedContactType("Work Phone");
								}
						}						
						
						if(eappInput.getApplicant()!= null && eappInput.getApplicant().getIndexationDeath()!= null && eappInput.getApplicant().getIndexationTpd()!= null){	
							if(eappInput.getApplicant().getIndexationDeath().equalsIgnoreCase("true") && eappInput.getApplicant().getIndexationTpd().equalsIgnoreCase("true")){
								memberDetails.setIndexationFlag("Yes");
							}else{
								memberDetails.setIndexationFlag("No");
							}			
						}
						
						
						if(eappInput.getApplicant().getUnitisedCovers()!= null){	
							if(eappInput.getApplicant().getUnitisedCovers().equalsIgnoreCase("true")){
								memberDetails.setUnitisedCvrsFlag("Yes");
							}else{
								memberDetails.setUnitisedCvrsFlag("No");
							}			
						}
						if(eappInput.getApplicant().getCancelReason()!= null){
							memberDetails.setCancelreason(eappInput.getApplicant().getCancelReason());
						}
						if(eappInput.getApplicant().getCancelOtherReason()!= null){
							memberDetails.setCancelotherreason("Other - "+eappInput.getApplicant().getCancelOtherReason());
						}
						
						memberDetails.setMemberAddress(addressBuilder.toString());
						if(eappInput.getResponseObject() != null) {
							memberDetails.setResponseObject(eappInput.getResponseObject());
						}
						if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
							memberDetails.setTransferCover(Boolean.TRUE);
						}						
						memberProductDtsList = new ArrayList<>();
						
						
						memberDetails.setPersonalDts(personalDtslist);												
						
						if(null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
								
						
								for(int covItr=0;covItr<applicantDTO.getCovers().size();covItr++){
									 coversDTO = applicantDTO.getCovers().get(covItr);
									 if(("AEIS".equalsIgnoreCase(eappInput.getPartnerCode())||"CORP".equalsIgnoreCase(eappInput.getPartnerCode()) || "INGD".equalsIgnoreCase(eappInput.getPartnerCode())) && ("Salary Continuance".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Income Protection");
										}
									 if(("CORP".equalsIgnoreCase(eappInput.getPartnerCode())) && ("Total & Permanent Disablement".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Total & Permanent Disability");
										}
									 
									 if(("INGD".equalsIgnoreCase(eappInput.getPartnerCode())) && ("Total & Permanent Disablement".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Total Permanent Disablement");
										}
									 
									 if(null!=coversDTO){	
										 
										 memberDetails.setFrquencyOpted(coversDTO.getFrequencycosttype());
										 memberProductDetails = new MemberProductDetails();								
										 if(null!=coversDTO.getCost()){
											
											 log.info(">>>loading test>> {}",coversDTO.getLoading());
											 log.info(">>11>getPolicyfee test>> {}",coversDTO.getPolicyfee());
											 log.info(">>Application Decision>> {}",pdfObject.getApplicationDecision());
											 log.info(">>Cover Decision>> {}",coversDTO.getCoverdecision());
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision())) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision()) && null!=coversDTO.getLoading()){
												 
												 	log.info(">>>getPolicyfee test>> {}",coversDTO.getPolicyfee());
													Double additionalCost = 0.0;
													String laodtext = " additional ";
													if(!"CORP".equalsIgnoreCase(eappInput.getPartnerCode())) {
														cost = new Double(coversDTO.getCost())+(additionalCost * coversDTO.getLoading().doubleValue()/100);
														memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount(cost.toString()));
													}
													if(!"CORP".equalsIgnoreCase(eappInput.getPartnerCode())) {
															if(coversDTO.getPolicyfee()!=null && coversDTO.getPolicyfee().compareTo(BigDecimal.ZERO)>0) {
																String loadingAmtTxt="We wish to note that in considering the information disclosed in your personal statement your"+laodtext+"cover is subject to a loading of "+ coversDTO.getLoading().intValue()+"%, an"+laodtext+"premium in the amount of $"+ coversDTO.getPolicyfee().toString() + " will be applicable if you accept the terms of the cover."; 
																 /**
																  * To Display the oercentage value									  */
																 pdfObject.setLoadingInd(Boolean.TRUE);
																 memberProductDetails.setLoadings(loadingAmtTxt);	
															}
													}
													else {
														String loadingAmtTxt="We wish to note that in considering the information disclosed in your personal statement your"+laodtext+"cover is subject to a loading of "+ coversDTO.getLoading().intValue()+"%.This loading will be applicable if you accept the terms of the cover."; 
														 /**
														  * To Display the oercentage value									  */
														 pdfObject.setLoadingInd(Boolean.TRUE);
														 memberProductDetails.setLoadings(loadingAmtTxt);	
													}
											}else{
												/**
												  * Work Request 6026 - need to display the loading if it wavied of 
												  * need to implement
												  */
												if(!"null".equalsIgnoreCase(coversDTO.getCost())) {
												 memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount(coversDTO.getCost()));
												}
												 if(coversDTO.getCost()== null){
													 memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount("0"));
												 }
											}										
										 }								
										 memberProductDetails.setProductDecision(coversDTO.getCoverdecision());
										 if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob)){
											 											 
											 if(null != coversDTO && null != coversDTO.getFulamount()){
												 memberProductDetails.setFulCoverAmount(coversDTO.getFulamount());
											 }
											 										 
											 if(coversDTO.getAddunitind()!=null && "1".equalsIgnoreCase(coversDTO.getAddunitind())){
												if(null!=coversDTO.getCoverdecision() && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
													if(new BigDecimal(Integer.toString(coversDTO.getAdditionalunit())).doubleValue() >0){
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
													}else{
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
													}
												}else{
													if("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) && "85".equalsIgnoreCase(eappInput.getApplicant().getInsuredSalary())){
														memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS+","+MetlifeInstitutionalConstants.SLASH_N_85PERCENT_SALARY);
													 }
													else if("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) && "90".equalsIgnoreCase(eappInput.getApplicant().getInsuredSalary())){
														memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS+","+MetlifeInstitutionalConstants.SLASH_N_90PERCENT_SALARY);
													}else{
														memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
													}
													
												}
												
											 }else if(null!=coversDTO.getCovercategory() && !memberDetails.isTransferCover() && !coversDTO.getCovercategory().contains("Fixed")){
												 if(!"DCL".equalsIgnoreCase(applicationDTO.getAppdecision()) ){
													
													 /**
													  * IP selection and non-ip section is the same logic. when we need a different format for death, tpd against IP with the decimals 
													  * This can be used and if we have to display the % of IP cover required - Bala16072014
													  */
													 
													 
													 if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_FIXED));	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 } 
													 }else if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
														 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 if(coversDTO.getAdditionalcoveramount()!=null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
																 } 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }else{
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 if(coversDTO.getAdditionalcoveramount()!=null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
																 }	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }
													 }else if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision()) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())){
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_FIXED));	 
															 }else{
																 	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }else if (coversDTO.getAdditionalcoveramount()!= null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString()));
																 }
															 }
													 }
													 
												 }else{
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
												 }
												 
											 } else if(memberDetails.isTransferCover()){
												 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"");													 
											 }else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
												 
												 if(coversDTO.getAdditionalcoveramount()!=null && new BigDecimal(Integer.toString(coversDTO.getAdditionalunit())).doubleValue()>0){
													 if (MetlifeInstitutionalConstants.FUND_MTAA.equalsIgnoreCase(applicationDTO.getPartnercode())){	//Added for MTAA 													 
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ("+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
													 }
													 else {													 
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
													 }
												 }else if(new BigDecimal(Integer.toString(coversDTO.getFixedunit())).doubleValue()>0){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+MetlifeInstitutionalConstants.UNITS);
												 }else if("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) && coversDTO.getAdditionalcoveramount()!=null){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED); 
												 }
												 else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
												 }
												 
												 
											 }else{
												 if(coversDTO.getAdditionalcoveramount()!=null){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
												 }
												 
											 }
											 
											 memberProductDetails.setProductName(coversDTO.getCovertype());
											 if("IP".equalsIgnoreCase(coversDTO.getCovercode()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
											 {
												 memberProductDetails.setBenefitPeriod(coversDTO.getTotalipbenefitperiod());
												 memberProductDetails.setWaitingPeriod(coversDTO.getTotalipwaitingperiod());
												 memberProductDetails.setExBenefitPeriod(coversDTO.getExistingipbenefitperiod());
												 memberProductDetails.setExWaitingPeriod(coversDTO.getExistingipwaitingperiod());
											 }
											 else
											 {
												 memberProductDetails.setBenefitPeriod(coversDTO.getAdditionalipbenefitperiod());
												 memberProductDetails.setWaitingPeriod(coversDTO.getAdditionalipwaitingperiod());
												 memberProductDetails.setExBenefitPeriod(coversDTO.getExistingipbenefitperiod());
												 memberProductDetails.setExWaitingPeriod(coversDTO.getExistingipwaitingperiod());
											 }
											 						 
											
											 /*need to revisit again*/
											 if(coversDTO.getTransfercoveramount()!=null){
												 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(coversDTO.getTransfercoveramount().toString()));
											 }

												 
											 if(new BigDecimal(Integer.toString(coversDTO.getFixedunit())).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+MetlifeInstitutionalConstants.UNITS);
												 }else{
													 if(null != coversDTO.getExistingcoveramount() && coversDTO.getExistingcoveramount().doubleValue() > 0 ){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
															 
													 }else{
															 coversDTO.setExistingcoveramount(BigDecimal.ZERO);
															 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
													 }
												 }
											 
										 
											 
											 memberProductDetails.setOccupationRating(coversDTO.getOccupationrating());
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision())) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision()) && coversDTO.getCoverexclusion()!=null && coversDTO.getCoverexclusion().trim().length()>0){
												 memberProductDetails.setExclusions(HTMLToString.convertHTMLToString(coversDTO.getCoverexclusion()));
											 }											 									 
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision())) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision()) && null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length()>0){
												 pdfObject.setExclusionInd(Boolean.TRUE);
											 }											 
											 if("DEATH".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_DEATH+coversDTO.getCoverreason());
											 }
											 if("TPD".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 &&  null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_TPD+coversDTO.getCoverreason());
											 }											
											 if("IP".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_IP+coversDTO.getCoverreason());
											 }
											 if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision()) && null!=coversDTO.getCoverreason()){
												 alldecreasons.append(coversDTO.getCoverreason());
											 }
											
										 }
										 if("INGD".equalsIgnoreCase(applicationDTO.getPartnercode()) && "DEATH".equalsIgnoreCase(coversDTO.getCovertype())){
											 
											 if("DcTailored".equalsIgnoreCase(eappInput.getApplicant().getCover().get(0).getExistingCoverCategory())){
												 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Tailored"));											 										 
										     }else{											 
													 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Automatic"));									
											 
											 }
											 if("DcTailored".equalsIgnoreCase(coversDTO.getCovercategory())){
												// memberProductDetails.setTransferCoverAmnt(coversDTO.getAdditionalcoveramount().toString());
												 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_TAILORED);
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_AUTOMATIC);
												 }
											 }
											 if("INGD".equalsIgnoreCase(applicationDTO.getPartnercode()) && "Total Permanent Disablement".equalsIgnoreCase(coversDTO.getCovertype())){
											 if("TPDTailored".equalsIgnoreCase(eappInput.getApplicant().getCover().get(1).getExistingCoverCategory())){
												 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Tailored"));
												
											 }else{											 
												 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Automatic"));									
												 
										     }
											 if("TPDTailored".equalsIgnoreCase(coversDTO.getCovercategory())){
												 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_TAILORED);
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_AUTOMATIC);
												 }
											 }
											 if("INGD".equalsIgnoreCase(applicationDTO.getPartnercode()) && "Income Protection".equalsIgnoreCase(coversDTO.getCovertype())){
											 if("IpTailored".equalsIgnoreCase(eappInput.getApplicant().getCover().get(2).getExistingCoverCategory())){
												 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("( Fixed )", ""));
																							 
											 }
											 if("IpTailored".equalsIgnoreCase(coversDTO.getCovercategory())){
												 if("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
													
													 if("75".equalsIgnoreCase(eappInput.getApplicant().getInsuredSalary())){
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_75PERCENT_SALARY+MetlifeInstitutionalConstants.SLASH_N_FIXED);
													 }else if("85".equalsIgnoreCase(eappInput.getApplicant().getInsuredSalary())){
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_85PERCENT_SALARY+MetlifeInstitutionalConstants.SLASH_N_FIXED);
													 }else{
														 if(eappInput.getApplicant().getCover().get(2).getExistingCoverAmount().compareTo(new BigDecimal(eappInput.getApplicant().getCover().get(2).getAdditionalCoverAmount()))==0){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_NOCHANGE+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_CHOOSEOWN+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }
														 
													 }
													 
												 }else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))); 
												 }
												 else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit()))+MetlifeInstitutionalConstants.SLASH_N_FIXED);
												 }
											 }
											 }/*else{
												 if("Income Protection".equalsIgnoreCase(coversDTO.getCovertype())){
													 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Automatic"));
													 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount().replaceAll("Fixed", "Automatic"));
												 }
											 }*/
											 if("insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())){
												 //memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("( Fixed )", ""));
												 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getExistingcoveramount())));
												 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalunit())));
												 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getExistingcoveramount()))+MetlifeInstitutionalConstants.PER_MONTH);
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(String.valueOf(coversDTO.getAdditionalcoveramount()))+MetlifeInstitutionalConstants.PER_MONTH);
												 }
											 }
										 if((null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
												 && ((coversDTO.getExistingcoveramount()).compareTo(BigDecimal.ZERO) <=0 ))
										 {
											 memberProductDetails = new MemberProductDetails();
										 
										 }
										 else
										 {
										 memberProductDtsList.add(memberProductDetails);
										 }
									 }
									
								}
							}							
						
						memberDetails.setMemberProductList(memberProductDtsList);
												
					}
				}
				
			}		 
			
			 /*End*/
		
			if(null!=applicationDTO.getCampaigncode()){
				pdfObject.setCampaignCode(applicationDTO.getCampaigncode());
			}			
			
			if(null != disclosureStatementList && !(disclosureStatementList.isEmpty())){
				pdfObject.setDisclosureStatementList(disclosureStatementList);	
			}
			
			if(null != disclosureStatementListGc && !(disclosureStatementListGc.isEmpty())){
				pdfObject.setDisclosureStatementListGc(disclosureStatementListGc);	
			}

			}	
			
			if(null != disclosureStatementListGc && !(disclosureStatementListGc.isEmpty())){
				pdfObject.setDisclosureStatementListGc(disclosureStatementListGc);	
			}
			//Added for INGD
			if(null != disclosureStatementListTe && !(disclosureStatementListTe.isEmpty())){
				pdfObject.setDisclosureStatementListTe(disclosureStatementListTe);	
			}
			//End
			//Added for Premium calculator
			if(null != disclosureStatementListPc && !(disclosureStatementListPc.isEmpty())){
				pdfObject.setDisclosureStatementListPc(disclosureStatementListPc);	
			}
			//End
			 pdfObject.setProductName(applicationDTO.getPartnercode());
			 
			/*prepare disclosureList*/
			
			pdfObject.setDisclosureQuestionList(prepareQuestionList(eappInput, property));
			//Added for insurance cover
	    	if("insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())){
	    		if(eappInput.getApplicant() != null) {
	    			applicantInfo = eappInput.getApplicant();
	    			pdfObject.setShowExpenseDetails(applicantInfo.isShowExpenseDetails());
	    			pdfObject.setShowDebtsDetails(applicantInfo.isShowDebtsDetails());
	    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getAnnualSalary())){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_grosssalary"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+applicantInfo.getAnnualSalary()+" "+applicantInfo.getSalaryFrequency());			
	    			disclosureincomeQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getSpouseSalary())){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_partnergrosssalary"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+applicantInfo.getSpouseSalary()+" "+applicantInfo.getSpouseSalaryFrequency());			
	    			disclosureincomeQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getMortgageAmount())){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_mortage"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+applicantInfo.getMortgageAmount()+" "+applicantInfo.getMortgageFrequency());			
	    			disclosureExpenseQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getTotalExpense())){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingexpense"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+applicantInfo.getTotalExpense()+" "+applicantInfo.getTotalExpenseFrequency());			
	    			disclosureExpenseQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if((applicantInfo.isShowExpenseDetails() && null!=applicantInfo.getExpenses())){
	        		for(int itr =0 ;itr<applicantInfo.getExpenses().size();itr++){
	    				disclosureQuestionAns = applicantInfo.getExpenses().get(itr);
	    				disclosureOtherExpenseQuestionAnsList.add(disclosureQuestionAns);
	        		}
	        	}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getTotalDebts())){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_totaldebts"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+applicantInfo.getTotalDebts());			
	    			disclosureOtherFinanceQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	    		}
	    		if( applicantInfo.isShowDebtsDetails() && null!=applicantInfo.getDebts()){
	        		for(int itr =0 ;itr<applicantInfo.getDebts().size();itr++){
	    				disclosureQuestionAns = applicantInfo.getDebts().get(itr);
	    				disclosureDebtDetailsQuestionAnsList.add(disclosureQuestionAns);
	        		}
	        	}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getRetirementAge()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_retirementage"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getRetirementAge())+" "+MetlifeInstitutionalConstants.INSURANCE_YEARS);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getInflationRate()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_inflationrate"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getInflationRate())+MetlifeInstitutionalConstants.PERCENTAGE);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getRealInterestRate()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_investmentreturn"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getRealInterestRate())+MetlifeInstitutionalConstants.PERCENTAGE);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getSuperContribRate()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_superanncontribution"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getSuperContribRate())+MetlifeInstitutionalConstants.PERCENTAGE);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getIpBenefitPaidToSA()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_ipbenefitpaid"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getIpBenefitPaidToSA())+MetlifeInstitutionalConstants.PERCENTAGE);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getIpReplaceRatio()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_ipreplaceratio"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getIpReplaceRatio())+MetlifeInstitutionalConstants.PERCENTAGE);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getFuneralCost()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_funeralcost"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+String.valueOf(eappInput.getInsuranceRuleDetails().getFuneralCost()));			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getAnnualMedAndNursingCost()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_annualnursing"));			
	    			disclosureQuestionAns.setAnswerText(MetlifeInstitutionalConstants.DOLLAR+String.valueOf(eappInput.getInsuranceRuleDetails().getAnnualMedAndNursingCost()));			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(String.valueOf(eappInput.getInsuranceRuleDetails().getDependentSuppAge()))){
	    			disclosureQuestionAns = new DisclosureQuestionAns();
	    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_highestagedep"));			
	    			disclosureQuestionAns.setAnswerText(String.valueOf(eappInput.getInsuranceRuleDetails().getDependentSuppAge())+" "+MetlifeInstitutionalConstants.INSURANCE_YEARS);			
	    			disclosureAssumptionQuestionAnsList.add(disclosureQuestionAns);			
	    		}
	        	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getChildListPDF().toString())){
	        		for(int itr =0 ;itr<applicantInfo.getChildListPDF().size();itr++){
	    				disclosureQuestionAns = applicantInfo.getChildListPDF().get(itr);
	    				disclosureChildListQuestionAnsList.add(disclosureQuestionAns);
	        		}
	    		}
	        	
	        	if(null != disclosureincomeQuestionAnsList && !(disclosureincomeQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureincomeQuestionAnsList(disclosureincomeQuestionAnsList);	
				}
	        	if(null != disclosureExpenseQuestionAnsList && !(disclosureExpenseQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureExpenseQuestionAnsList(disclosureExpenseQuestionAnsList);	
				}
	        	if(null != disclosureOtherFinanceQuestionAnsList && !(disclosureOtherFinanceQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureOtherFinanceQuestionAnsList(disclosureOtherFinanceQuestionAnsList);	
				}
	        	if(null != disclosureAssumptionQuestionAnsList && !(disclosureAssumptionQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureAssumptionQuestionAnsList(disclosureAssumptionQuestionAnsList);	
				}
	        	if(null != disclosureOtherExpenseQuestionAnsList && !(disclosureOtherExpenseQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureOtherExpenseQuestionAnsList(disclosureOtherExpenseQuestionAnsList);	
				}
	        	if(null != disclosureDebtDetailsQuestionAnsList && !(disclosureDebtDetailsQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureDebtDetailsQuestionAnsList(disclosureDebtDetailsQuestionAnsList);	
				}
	        	if(null != disclosureChildListQuestionAnsList && !(disclosureChildListQuestionAnsList.isEmpty())){
					pdfObject.setDisclosureChildListQuestionAnsList(disclosureChildListQuestionAnsList);	
				}
	    	}
			if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				pdfObject.setTransferPrevQuestionList(PDFbusinessObjectMapperNew.prepareTransferPrevQuestionList(eappInput, property));
			}
			pdfObject.setMemberDetails(memberDetails);
			pdfObject.setDeclReasons(alldecreasons.toString());
		
		
		property.clear();
		if(null!=url){
			url.close();
		}	
		log.info("Pdf object mapping finish");
		return pdfObject;
	}
	
	public static List<DisclosureQuestionAns> prepareQuestionList(EappInput eappInput, Properties property){
		log.info("Prepare question list start");
		String upLoadFileName = "";
		List<DisclosureQuestionAns> disclosureQuestionAnsList = new ArrayList<>();
		
		DisclosureQuestionAns disclosureQuestionAns = null;
		ApplicantInfo applicantInfo = null;
		ContactDetails contactInfo = null;
		
		if(eappInput.getApplicant() != null) {
			applicantInfo = eappInput.getApplicant();
		
			if((!"premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) || !"insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())) && applicantInfo.getFirstName()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_first_name"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getFirstName());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			
			if((!"premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) || !"insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())) && applicantInfo.getLastName()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_last_name"));
				log.info("last name---- {}",disclosureQuestionAns.getQuestiontext());
				disclosureQuestionAns.setAnswerText(applicantInfo.getLastName());
				log.info("last name ans----- {}",disclosureQuestionAns.getAnswerText());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}	
			
			if(!"insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag()) && EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getGender())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_gender"));
				log.info("gender---- {}" ,disclosureQuestionAns.getQuestiontext());
				disclosureQuestionAns.setAnswerText(applicantInfo.getGender());
				log.info("gender ans----- {}" ,disclosureQuestionAns.getAnswerText());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			
			if(!"premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) && applicantInfo.getEmailId()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_email_add"));			
				disclosureQuestionAns.setAnswerText(applicantInfo.getEmailId());			
				disclosureQuestionAnsList.add(disclosureQuestionAns);			
			}
			
			if(applicantInfo.getBirthDate() != null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_dob"));		
				disclosureQuestionAns.setAnswerText(applicantInfo.getBirthDate());
				log.info("dob----- {}" ,disclosureQuestionAns.getAnswerText());
				disclosureQuestionAnsList.add(disclosureQuestionAns);		
			}
			
			if(!"premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) && applicantInfo.getContactDetails()!=null) {
				contactInfo = applicantInfo.getContactDetails();
				
				if(contactInfo.getPreferedContactTime()!=null){
					disclosureQuestionAns = new DisclosureQuestionAns();
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));	
					if(contactInfo.getPreferedContactTime()!= null && "1".equalsIgnoreCase(contactInfo.getPreferedContactTime())){
						disclosureQuestionAns.setAnswerText("9am - 12pm");
					}else{
						disclosureQuestionAns.setAnswerText("12pm - 6pm");
					}
					disclosureQuestionAnsList.add(disclosureQuestionAns);
					
				}
				if(contactInfo.getPreferedContacType()!=null){
					disclosureQuestionAns = new DisclosureQuestionAns();
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
					
						if("1".equalsIgnoreCase(contactInfo.getPreferedContacType())) {
							contactInfo.setPreferedContacType(MetlifeInstitutionalConstants.MOBILE_PH);
						}
						else if("2".equalsIgnoreCase(contactInfo.getPreferedContacType())) {
							contactInfo.setPreferedContacType("Home");
						}
						else if("3".equalsIgnoreCase(contactInfo.getPreferedContacType())){
							contactInfo.setPreferedContacType("Work");
						}
				
					if(MetlifeInstitutionalConstants.MOBILE_PH.equalsIgnoreCase(contactInfo.getPreferedContacType())){		
						disclosureQuestionAns.setAnswerText("Mobile Phone");
					}else if("Home".equalsIgnoreCase(contactInfo.getPreferedContacType())){		
						disclosureQuestionAns.setAnswerText("Home Phone");
					}else if("Work".equalsIgnoreCase(contactInfo.getPreferedContacType())){		
						disclosureQuestionAns.setAnswerText("Work Phone");
					}
					disclosureQuestionAnsList.add(disclosureQuestionAns);
				}
				
				if(contactInfo.getPreferedContactNumber()!=null){
					disclosureQuestionAns = new DisclosureQuestionAns();
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));			
					disclosureQuestionAns.setAnswerText(contactInfo.getPreferedContactNumber());			
					disclosureQuestionAnsList.add(disclosureQuestionAns);
				}
			}//Contactinfo ends
			
			// For HOST, Change cover
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOwnBusinessQue())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_que"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getOwnBusinessQue());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOwnBusinessYesQue())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_yes_que"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getOwnBusinessYesQue());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOwnBusinessNoQue())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				if("MTAA".equalsIgnoreCase(eappInput.getPartnerCode()) && "UWCOVER".equalsIgnoreCase(eappInput.getRequestType())){
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_no_que_occupg"));
				}else {
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_no_que"));
				}
				disclosureQuestionAns.setAnswerText(applicantInfo.getOwnBusinessNoQue());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			// For HOST, Change cover
			
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getWorkHours())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_fifteen_hr_que"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getWorkHours());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getAustraliaCitizenship())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_citizen_aus"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getAustraliaCitizenship());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			if(EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getPartnerCode()) && eappInput.getPartnerCode().equalsIgnoreCase("SFPS") && "CCOVER".equalsIgnoreCase(eappInput.getRequestType())
					&&applicantInfo.getSmoker()!=null) {
				
					disclosureQuestionAns = new DisclosureQuestionAns();
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_smoker_que"));
					disclosureQuestionAns.setAnswerText(applicantInfo.getSmoker());
					disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			if(EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getPartnerCode()) && ("AEIS".equalsIgnoreCase(eappInput.getPartnerCode()))
					&& applicantInfo.getSmoker()!=null) {
					disclosureQuestionAns = new DisclosureQuestionAns();
					disclosureQuestionAns.setQuestiontext(property.getProperty("label_smoker_que"));
					if("Yes".equalsIgnoreCase(applicantInfo.getSmoker())){
						disclosureQuestionAns.setAnswerText("Yes"); 
					}else{
						disclosureQuestionAns.setAnswerText("No"); 
					}
					disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			if(!"insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag()) && EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getAnnualSalary())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_annualSalary"));			
				disclosureQuestionAns.setAnswerText(applicantInfo.getAnnualSalary());			
				disclosureQuestionAnsList.add(disclosureQuestionAns);			
			}
			
			 if(!"insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag()) && EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOccupationCode())){
	 				disclosureQuestionAns = new DisclosureQuestionAns();
	 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_industrycnfrm"));			
	 				disclosureQuestionAns.setAnswerText(applicantInfo.getOccupationCode());			
	 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
	 		} 			
 			if(!"insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag()) && EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOccupation())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_occupation_que"));
 				if(applicantInfo.getOccupation().equalsIgnoreCase("Other")){
 					disclosureQuestionAns.setAnswerText(applicantInfo.getOtherOccupation());
 				}else{
 					disclosureQuestionAns.setAnswerText(applicantInfo.getOccupation());
 				}			
 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
 			}
 			
 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOccupationDuties())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_askmanual_que"));
 				disclosureQuestionAns.setAnswerText(applicantInfo.getOccupationDuties()); 
 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
 			}
 			
 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getTertiaryQue())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_ter_qual"));
 				disclosureQuestionAns.setAnswerText(applicantInfo.getTertiaryQue());
 				disclosureQuestionAnsList.add(disclosureQuestionAns);
 			}
 			
 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getSpendTimeOutside())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_work_duty"));	
 				disclosureQuestionAns.setAnswerText(applicantInfo.getSpendTimeOutside()); 
 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
 			}

 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getWorkDuties())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_hazardous_que"));	
 				disclosureQuestionAns.setAnswerText(applicantInfo.getWorkDuties()); 
 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
 			}
 			
 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getPreviousTerminalIllQue())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_previous_terminal_ill_que"));	
 				disclosureQuestionAns.setAnswerText(applicantInfo.getPreviousTerminalIllQue()); 
 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
 			}
 			//Life Event Starts
 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getEventDesc())){
 				disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_name"));			
 				disclosureQuestionAns.setAnswerText(applicantInfo.getEventDesc());			
 				disclosureQuestionAnsList.add(disclosureQuestionAns);
 	    	}
 			
 			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getLifeEventDate())){
 	    		disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_date"));			
 				disclosureQuestionAns.setAnswerText(applicantInfo.getLifeEventDate());			
 				disclosureQuestionAnsList.add(disclosureQuestionAns);
 	    	}
 	    	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getEventAlreadyApplied()) && null!=property.getProperty("label_lifeevent_alreadyApplied")){
 	    		disclosureQuestionAns = new DisclosureQuestionAns();
 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_alreadyApplied"));			
 				disclosureQuestionAns.setAnswerText(applicantInfo.getEventAlreadyApplied());			
 				disclosureQuestionAnsList.add(disclosureQuestionAns);
 	    	}
 	    	
 	    	String deathIncrease = null;
 	    	String tpdIncrease = null;
 	    	if(eappInput.getApplicant() != null && !eappInput.getApplicant().getCover().isEmpty()) {
				for(Iterator itr = eappInput.getApplicant().getCover().iterator(); itr.hasNext();) {
					CoverInfo coverInfo = (CoverInfo)itr.next();
					if(coverInfo.getBenefitType()!= null && ("1").equalsIgnoreCase(coverInfo.getBenefitType()) && coverInfo.getAdditionalUnit() != null) {
						deathIncrease = coverInfo.getAdditionalUnit();
					}else if(coverInfo.getBenefitType()!= null && ("2").equalsIgnoreCase(coverInfo.getBenefitType()) && coverInfo.getAdditionalUnit()!= null){
						tpdIncrease = coverInfo.getAdditionalUnit();
					}
				}
				if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getLifeEventDate())){
	 	    		disclosureQuestionAns = new DisclosureQuestionAns();
	 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_Death_unitincrease"));			
	 				disclosureQuestionAns.setAnswerText(deathIncrease);			
	 				disclosureQuestionAnsList.add(disclosureQuestionAns);
	 	    	}
	 	    	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getLifeEventDate())){
	 	    		disclosureQuestionAns = new DisclosureQuestionAns();
	 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_TPD_unitincrease"));			
	 				disclosureQuestionAns.setAnswerText(tpdIncrease);			
	 				disclosureQuestionAnsList.add(disclosureQuestionAns);
	 	    	}	
 	    	}	
 	    	
 	    	
 	    	
 	    	
 	    	
		}//applicantinfo ends
		
			
		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getEventDesc())){
			if(eappInput.getApplicant()!= null && EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getApplicant().getDocumentEvidence())){
	    		disclosureQuestionAns = new DisclosureQuestionAns();
	    		disclosureQuestionAns.setQuestiontext(property.getProperty("label_document_evidence_life"));
	    		disclosureQuestionAns.setAnswerText(eappInput.getApplicant().getDocumentEvidence());
	    		disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			if(EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getApplicant().getDocumentEvidence())&& ("No").equalsIgnoreCase(eappInput.getApplicant().getDocumentEvidence())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_document_no_evidence"));
				disclosureQuestionAns.setAnswerText("No");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns=new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_document_address"));
				disclosureQuestionAns.setAnswerText(eappInput.getApplicant().getDocumentAddress());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
		}	    	
	    	if(eappInput.getLifeEventDocuments()!= null && eappInput.getApplicant().getDocumentEvidence()!=null && "Yes".equalsIgnoreCase(eappInput.getApplicant().getDocumentEvidence())){
	    		disclosureQuestionAns = new DisclosureQuestionAns();
	    		disclosureQuestionAns.setQuestiontext(property.getProperty("label_summary_documents"));
				for(int i=0;i<eappInput.getLifeEventDocuments().size();i++){
					String filename= eappInput.getLifeEventDocuments().get(i).getName();
					log.info("Document to be attached in Life Event ---------- >>> >>> {} ",filename);
					upLoadFileName = upLoadFileName.concat(filename+"<br>");
				}
				disclosureQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
		 
		 
		if(eappInput.getApplicant() != null) {
			applicantInfo = eappInput.getApplicant();
			
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getRelation())){
	    		disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_reason_addnl_cover"));			
				disclosureQuestionAns.setAnswerText(applicantInfo.getRelation());			
				disclosureQuestionAnsList.add(disclosureQuestionAns);
	    	}
	    	
	    	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getThirtyFiveHrsWrkQue())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_thirty_five_hrs_week_que"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getThirtyFiveHrsWrkQue());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
	    	
	    	if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getPermanenTemplQue())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_permanent_empl"));
				disclosureQuestionAns.setAnswerText(applicantInfo.getPermanenTemplQue());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
		 }
		
    	if("insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())){
    		
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getAge())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_insuranceold"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getAge()+" "+MetlifeInstitutionalConstants.INSURANCE_YEARS);			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		} 	
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getGender())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_insurancegender"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getGender());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		} 
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getLivingWithWife())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingwithpartner"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getLivingWithWife());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		} 
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getDependentChild())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingdependentchild"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getDependentChild());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		} 
    		/*if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getDependentChild())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_fifteen_hr_que"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getDependentChild());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		}*/
    		 if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOccupationCode())){
	 				disclosureQuestionAns = new DisclosureQuestionAns();
	 				disclosureQuestionAns.setQuestiontext(property.getProperty("label_industrycnfrm"));			
	 				disclosureQuestionAns.setAnswerText(applicantInfo.getOccupationCode());			
	 				disclosureQuestionAnsList.add(disclosureQuestionAns);			
	 		} 			
			if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getOccupation())){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_occupation_que"));
				if(applicantInfo.getOccupation().equalsIgnoreCase("Other")){
					disclosureQuestionAns.setAnswerText(applicantInfo.getOtherOccupation());
				}else{
					disclosureQuestionAns.setAnswerText(applicantInfo.getOccupation());
				}			
				disclosureQuestionAnsList.add(disclosureQuestionAns);			
			}
			
    		/*if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getDependentChild())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingdependentchild"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getDependentChild());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		}
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getDependentChild())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingdependentchild"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getDependentChild());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		}
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getDependentChild())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingdependentchild"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getDependentChild());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		}
    		if(EapplyServiceHelper.isNotNullandNotEmpty(applicantInfo.getDependentChild())){
    			disclosureQuestionAns = new DisclosureQuestionAns();
    			disclosureQuestionAns.setQuestiontext(property.getProperty("label_livingdependentchild"));			
    			disclosureQuestionAns.setAnswerText(applicantInfo.getDependentChild());			
    			disclosureQuestionAnsList.add(disclosureQuestionAns);			
    		}*/
    	}
    	//Added for calculator
    	if(("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) || "insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())) && null!=eappInput.getFinalOccupationQuestions()){
    		for(int itr =0 ;itr<eappInput.getFinalOccupationQuestions().size();itr++){
				disclosureQuestionAns = eappInput.getFinalOccupationQuestions().get(itr);
				disclosureQuestionAnsList.add(disclosureQuestionAns);
    		}
    	}
    	if(("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) || "insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag())) && null!=eappInput.getInitialOccupationQuestions()){
    		for(int itr =0 ;itr<eappInput.getInitialOccupationQuestions().size();itr++){
				disclosureQuestionAns = eappInput.getInitialOccupationQuestions().get(itr);
				disclosureQuestionAnsList.add(disclosureQuestionAns);
    		}
    	}//Calcualtor End
    	
		log.info("Prepare question list finish");
		return disclosureQuestionAnsList;
	}

	public static List<TransferPreviousQuestionAns> prepareTransferPrevQuestionList(EappInput eappInput, Properties property){
		log.info("Prepare Transfer Previous question list start");
		String upLoadFileName = "";
		List<TransferPreviousQuestionAns> transferPreviousCoverQuestionAnsList = new ArrayList<>();
		TransferPreviousQuestionAns transferPrevQuestionAns = null;
		ApplicantInfo applicantInfo = null;
		CoverInfo coverInfo = null;
		
		if(eappInput.getApplicant() != null) {
			applicantInfo = eappInput.getApplicant();
			
			if(applicantInfo.getPreviousInsurerName()!=null){
				transferPrevQuestionAns = new TransferPreviousQuestionAns();
				transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_nam_previ_insurer"));
				transferPrevQuestionAns.setAnswerText(applicantInfo.getPreviousInsurerName());
				transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
			}
			if(applicantInfo.getFundMemPolicyNumber()!=null){
				transferPrevQuestionAns = new TransferPreviousQuestionAns();
				transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_fundmem_inspoli_no"));
				transferPrevQuestionAns.setAnswerText(applicantInfo.getFundMemPolicyNumber());
				transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
			}
			if(applicantInfo.getSpinNumber()!=null){
				transferPrevQuestionAns = new TransferPreviousQuestionAns();
				transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_spin_no"));
				transferPrevQuestionAns.setAnswerText(applicantInfo.getSpinNumber());
				transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
			}
			
			if(applicantInfo.getCover()!= null && !applicantInfo.getCover().isEmpty() ) {
				coverInfo = applicantInfo.getCover().get(0);
				if(EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getPartnerCode())&& !"CARE".equalsIgnoreCase(eappInput.getPartnerCode())
						&& coverInfo.getFrequencyCostType()!=null){
						transferPrevQuestionAns = new TransferPreviousQuestionAns();
						transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_prem_freq"));
						transferPrevQuestionAns.setAnswerText(coverInfo.getFrequencyCostType());
						transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
				}
			}
			
		}
		
		if(EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getApplicant().getDocumentEvidence()) ){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_document_evidence"));
			transferPrevQuestionAns.setAnswerText(eappInput.getApplicant().getDocumentEvidence());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		if(EapplyServiceHelper.isNotNullandNotEmpty(eappInput.getApplicant().getDocumentEvidence()) && ("No").equalsIgnoreCase(eappInput.getApplicant().getDocumentEvidence())){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_document_no_evidence"));
			transferPrevQuestionAns.setAnswerText("Yes");
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
			
			transferPrevQuestionAns=new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_document_address"));
			transferPrevQuestionAns.setAnswerText(eappInput.getApplicant().getDocumentAddress());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		
		if(eappInput.getTransferDocuments()!=null && eappInput.getApplicant().getDocumentEvidence()!=null && "Yes".equalsIgnoreCase(eappInput.getApplicant().getDocumentEvidence())){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_documents"));
			for(int i=0;i<eappInput.getTransferDocuments().size();i++){
				String filename= eappInput.getTransferDocuments().get(i).getName();
				log.info("Document to be attached in Transfer ---------- >>> >>> {}",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			transferPrevQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		
		
		log.info("Prepare Transfer Previous question list finish");
		return transferPreviousCoverQuestionAnsList;
	}
	

	
	public static Double toDouble(String string) {
		
		if(isValuePresent(string)) {
			return Double.valueOf(string);
		}
		return null;
	}
	
	public static boolean isValuePresent(String string) {
		
		return (isObjectPresent(string) && string.trim().length() > 0);
	}
	
	public static boolean isObjectPresent(Object object){
	
		return (null != object);
	}
	
	
	

}
