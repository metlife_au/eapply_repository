package au.com.metlife.eapply.bs.service;



import java.util.List;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;



public interface TblOccMappingRuledataService {

	List<TblOccMappingRuledata> getOccupationList(String fundCode,String induCode);
}
