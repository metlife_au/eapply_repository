/*aura occ upgrade controller*/
FirstSuperApp.controller('auraocc',['$scope','$rootScope', '$location','$timeout','$window','$routeParams','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','RetrieveAppDetailsService','ngDialog','urlService','saveEapply',
                                     function($scope, $rootScope,$location,$timeout,$window,$routeParams,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,RetrieveAppDetailsService,ngDialog,urlService,saveEapply){

	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	
    	if($routeParams.mode == 3 && path=='/quoteoccchange/2')
    	{
    		path = "/quoteoccchange/3";
    	}
    	
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	  //$location.path(path);
  	};

 	$scope.proceedNext = function() {
 		$scope.keepGoing = true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true;
  				$scope.keepGoing = false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  				//$scope.go('/workRatingSummary');
  			}
  		});
  		if($scope.keepGoing){
  			$rootScope.$broadcast('disablepointer');
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
  				PersistenceService.setWorkRatingAuraDetails(response.data);
  				//console.log(response.data);
  				$scope.go('/workRatingSummary/1');
  	  		});
  		}
  	};

 	$scope.coverDetails=PersistenceService.getworkRatingCoverDetails();
 	$scope.personalDetails = persoanlDetailService.getMemberDetails();
 	$scope.workRatingOccDetails=PersistenceService.getWorkRatingCoverOccDetails();

  $scope.saveAndExitPopUp = function (hhText) {
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
	var appNum;
    appNum = PersistenceService.getAppNumber();
 	$scope.saveUpdateAura = function() {
 		$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
 		if($scope.coverDetails != null && $scope.workRatingOccDetails != null && $scope.personalDetails != null){
    		$scope.coverDetails.lastSavedOn = 'AuraUpdatePage';
    		// $scope.auraDetails
    		var details={};
    		details.addnlDeathCoverDetails = {};
			details.addnlTpdCoverDetails = {};
			details.addnlIpCoverDetails = {};
			details.occupationDetails = $scope.workRatingOccDetails;
    		var temp = angular.extend($scope.coverDetails,details);
        	var saveUpdateAuraObject = angular.extend(temp, $scope.personalDetails);
        	auraResponseService.setResponse(saveUpdateAuraObject)
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                console.log(response.data)
	        });
    	}
 	}

	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    }

	// added for session expiry
   /* $timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
	
	// changes added for hyperlink navigation
	var init = function()
  	{
	 console.log($scope.coverDetails);

	/*$scope.coverDetails = PersistenceService.getChangeCoverDetails();
	console.log($scope.coverDetails);*/


  	auraInputService.setFund('FIRS')
  	auraInputService.setMode('WorkRating')

  	/////////////////////////
  	/*auraInputService.setAge( moment().diff($scope.coverDetails.dob, 'years')) ;
  	auraInputService.setAppnumber($scope.coverDetails.appNum);
  	auraInputService.setDeathAmt($scope.coverDetails.deathLabelAmt)
  	auraInputService.setTpdAmt($scope.coverDetails.tpdLabelAmt)
  	auraInputService.setIpAmt($scope.coverDetails.ipLabelAmt)
  	auraInputService.setWaitingPeriod($scope.coverDetails.waitingPeriod)
  	auraInputService.setBenefitPeriod($scope.coverDetails.benefitPeriod)

  	auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')

  	auraInputService.setGender($scope.coverDetails.gender)
  	auraInputService.setIndustryOcc($scope.coverDetails.industryCode+":"+$scope.coverDetails.occupation)
  	if($scope.coverDetails.citizenQue=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.coverDetails.citizenQue)
  	}
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary($scope.coverDetails.salary)
  	auraInputService.setFifteenHr($scope.coverDetails.fifteenHr)
  	auraInputService.setName($scope.coverDetails.name)*/
  	//////////////////////

  	//setting deafult vaues for testing
  	auraInputService.setName($scope.coverDetails.name);
  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
  //auraInputService.setAge(moment().diff($scope.coverDetails.dob, 'years'));
  	//below values has to be set from quote page, as of now taking from existing insurance
  	auraInputService.setAppnumber($scope.coverDetails.appNum);
  	auraInputService.setDeathAmt(parseInt($scope.coverDetails.deathAmt));
  	auraInputService.setTpdAmt(parseInt($scope.coverDetails.tpdAmt));
  	auraInputService.setIpAmt(parseInt($scope.coverDetails.ipAmt));
  	auraInputService.setWaitingPeriod($scope.coverDetails.waitingPeriod);
  	auraInputService.setBenefitPeriod($scope.coverDetails.benefitPeriod);
  	if($scope.workRatingOccDetails && $scope.workRatingOccDetails.gender ){
  		auraInputService.setGender($scope.workRatingOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
  //auraInputService.setGender($scope.coverDetails.gender);
  	auraInputService.setIndustryOcc($scope.workRatingOccDetails.industryCode+":"+$scope.workRatingOccDetails.occupation);
  	auraInputService.setCountry('Australia');
  	auraInputService.setSalary($scope.workRatingOccDetails.salary);
  	auraInputService.setFifteenHr($scope.workRatingOccDetails.fifteenHr);
  	auraInputService.setClientname('metaus')
  	auraInputService.setLastName($scope.personalDetails.personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails.personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails.personalDetails.dateOfBirth)
  	auraInputService.setExistingTerm(false);
  	if($scope.personalDetails.memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	}
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
  		$scope.auraResponseDataList = response.data.questions;
  		console.log($scope.auraResponseDataList)
  		angular.forEach($scope.auraResponseDataList, function(Object) {
			$scope.sectionname = Object.questionAlias.substring(3);

			});
  	});
	 
  	};
  	
  	if($routeParams.mode == 3 && !$scope.coverDetails ){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    		var result = res.data[0];
    		var coverDet = {}, occDet ={};
    		
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactType = result.contactType;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		occDet.gender = result.personalDetails.gender;
    		coverDet.transferDeathExistingAmt = result.transferDeathExistingAmt;
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.transferTpdExistingAmt = result.transferTpdExistingAmt;
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.transferIpExistingAmt = result.transferIpExistingAmt;
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium = result.totalPremium;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.previousFundName = result.previousFundName;
    		coverDet.membershipNumber = result.membershipNumber;
    		coverDet.spinNumber = result.spinNumber;
    		coverDet.documentName = result.documentName;
    		coverDet.appNum = result.appNum;
    		coverDet.waitingPeriod=result.waitingPeriod || '';
    		coverDet.benefitPeriod=result.benefitPeriod || '';
    		coverDet.deathAmt=result.existingDeathAmt?parseInt(result.existingTpdAmt):0;
    		coverDet.tpdAmt=result.existingTpdAmt?parseInt(result.existingTpdAmt):0;
    		coverDet.ipAmt=result.ipAmt?parseInt(result.ipAmt):0;
    		coverDet.dodCheck=result.dodCheck;
    		coverDet.privacyCheck=result.privacyCheck;
    		coverDet.age=result.age;
    		coverDet.totalPremium=result.totalPremium;
    		coverDet.deathCoverPremium=result.deathCoverPremium;
    		coverDet.tpdCoverPremium=result.tpdCoverPremium;
    		coverDet.ipCoverPremium=result.ipCoverPremium;
    		coverDet.deathLifeCoverType=result.deathLifeCoverType;
    		coverDet.tpdLifeCoverType=result.tpdLifeCoverType;
    		coverDet.ipLifeCoverType=result.ipLifeCoverType;
    		coverDet.freqCostType=result.freqCostType;
    		coverDet.deathLifeUnits=result.deathLifeUnits;
    		coverDet.tpdLifeUnits=result.tpdLifeUnits;
    		coverDet.manageType = "UWCOVER";
    		coverDet.partnerCode ="FIRS";
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.age = result.age;
			//$scope.transferCoverDetails = coverDet;

    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		occDet.hazardousQue = result.occupationDetails.hazardousQue;
    		occDet.salary = result.occupationDetails.salary;
    		
    		/*deathAddDet.deathTransferAmt = result.addnlDeathCoverDetails.deathTransferAmt;
    		deathAddDet.deathTransferCovrAmt = result.addnlDeathCoverDetails.deathTransferCovrAmt?parseInt(result.addnlDeathCoverDetails.deathTransferCovrAmt):0;
    		deathAddDet.deathTransferWeeklyCost = result.addnlDeathCoverDetails.deathTransferWeeklyCost;
    		
    		tpdAddDet.tpdTransferAmt = result.addnlTpdCoverDetails.tpdTransferAmt;
    		tpdAddDet.tpdTransferCovrAmt = result.addnlTpdCoverDetails.tpdTransferCovrAmt?parseInt(result.addnlTpdCoverDetails.tpdTransferCovrAmt):0;
    		tpdAddDet.tpdTransferWeeklyCost = result.addnlTpdCoverDetails.tpdTransferWeeklyCost;

    		ipAddDet.ipTransferAmt = result.addnlIpCoverDetails.ipTransferAmt;
    		ipAddDet.ipTransferCovrAmt = result.addnlIpCoverDetails.ipTransferCovrAmt?parseInt(result.addnlIpCoverDetails.ipTransferCovrAmt):0;
    		ipAddDet.addnlTransferWaitingPeriod = result.addnlIpCoverDetails.addnlTransferWaitingPeriod;
    		ipAddDet.addnlTransferBenefitPeriod = result.addnlIpCoverDetails.addnlTransferBenefitPeriod;
    		ipAddDet.ipTransferWeeklyCost = result.addnlIpCoverDetails.ipTransferWeeklyCost;*/
    		
    		$scope.svdRtrv = true;
    		coverDet['svRt'] = $scope.svdRtrv;
    		$scope.coverDetails=coverDet;
    	  	$scope.workRatingOccDetails =occDet;
    	  	PersistenceService.setworkRatingCoverDetails($scope.coverDetails);
        	PersistenceService.setWorkRatingCoverOccDetails($scope.workRatingOccDetails);
    	  	/*$scope.deathAddnlCvrDetails =deathAddDet;
    		$scope.tpdAddnlCvrDetails=tpdAddDet;
    		$scope.ipAddnlCvrDetails=ipAddDet;*/
    		init();
    		
    	}, function(err){
    		console.error("Something went wrong while retrieving the details " + JSON.stringify(err));
    	});
	}
	else
		{
		init();
		}
 // changes added for hyperlink navigation end
  	 $scope.updateRadio = function (answerValue, questionObj){

   		questionObj.arrAns[0]=answerValue;
   		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
   		  auraResponseService.setResponse($scope.auraRes)
   		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {
   			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
   			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
   	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
   	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
   	  				//branch complete false for previous questions
   	  				$scope.auraResponseDataList[selectedIndex].error=true;
   	  			}else if(Object.questionComplete){
   	  				$scope.auraResponseDataList[selectedIndex].error=false;
   	  			}

   	  		});

       	}, function () {
       		//console.log('failed');
       	});
   	 };


}]);
