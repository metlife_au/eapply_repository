package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.STFTCCConfirmationPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

public class STFTCCConfirmationPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTCCConfirmationPage yourConfirmation() {
		yourConfirmationDetails();
		return new STFTCCConfirmationPage(scriptHelper);
	}

	public STFTCCConfirmationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourConfirmationDetails() {

	
		String email = dataTable.getData("MetLife_Data", "Email");
		try {

			switchToActiveWindow();
			driver.manage().window().maximize();
			
			tryAction(fluentWaitElement(STFTConfirmationPageObjects.rdowner), "clkradio", "Owner");			
			
			tryAction(waitForClickableElement(STFTCCConfirmationPageObjects.txtemail), "SET", "Email", email);
			tryAction(waitForClickableElement(STFTCCConfirmationPageObjects.txtemail), "TAB", "Email");
			
			tryAction(waitForClickableElement(STFTCCConfirmationPageObjects.txtemailconfirm), "SET", "Conafirm Email", email);
			tryAction(waitForClickableElement(STFTCCConfirmationPageObjects.txtemailconfirm), "TAB", "Confirm Email");
			Thread.sleep(200);
			tryAction(waitForClickableElement(STFTCCConfirmationPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTCCConfirmationPageObjects.btnNext)).click().perform();
			Thread.sleep(1000);
			
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
