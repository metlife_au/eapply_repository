package au.com.metlife.eapply.bs.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.lowagie.text.DocumentException;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.bs.exception.MetLifeBSRuntimeException;
import au.com.metlife.eapply.bs.model.AddressJSON;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Auradetail;
import au.com.metlife.eapply.bs.model.ContactDetailsJSON;
import au.com.metlife.eapply.bs.model.Contactinfo;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverJSON;
import au.com.metlife.eapply.bs.model.DeathAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.DocumentJSON;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.model.EapplyOutput;
import au.com.metlife.eapply.bs.model.ExistingCoversJSON;
import au.com.metlife.eapply.bs.model.IpAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.OccupationCoversJSON;
import au.com.metlife.eapply.bs.model.PersonalDetailsJSON;
import au.com.metlife.eapply.bs.model.RetrieveApp;
import au.com.metlife.eapply.bs.model.TpdAddnlCoversJSON;
import au.com.metlife.eapply.bs.pdf.GenerateEapplyPDF;
import au.com.metlife.eapply.bs.pdf.PDFbusinessObjectMapper;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.bs.utility.BizflowXMLHelper;
import au.com.metlife.eapply.bs.utility.EmailHelper;
import javassist.bytecode.annotation.IntegerMemberValue;

@RestController
@Component
@PropertySource(value = "application.properties")
public class EapplyServicesController {
	
	@Value("${spring.propertyFile}")
	private  String propertyFile;
	
	@Value("${spring.pdfbasepath}")
	private String fileUploadPath;

	public String getFileUploadPath() {
		return fileUploadPath;
	}

	public void setFileUploadPath(String fileUploadPath) {
		this.fileUploadPath = fileUploadPath;
	}

	public String getPropertyFile() {
		return propertyFile;
	}

	public void setPropertyFile(String propertyFile) {
		this.propertyFile = propertyFile;
	}

	private final EapplyService eapplyService;
	
	 @Autowired
     public EapplyServicesController(final EapplyService eapplyService) {
        this.eapplyService = eapplyService;
     }
	 
	 @RequestMapping("/checkSavedApplications")
	  public ResponseEntity<List<RetrieveApp>> checkSavedApplications(@RequestParam(value="fundCode") String fundCode
			  ,@RequestParam(value="clientRefNo") String clientRefNo
			  ,@RequestParam(value="manageType") String manageType) {		
		 List<RetrieveApp> retrieveAppList=null;
		 try{
			 if(fundCode!=null && clientRefNo!=null && manageType!=null){
				 retrieveAppList=new ArrayList<RetrieveApp>();
				 List<Eapplication> savedApplications= eapplyService.retrieveSavedApplications(fundCode, clientRefNo,manageType);
				 if(savedApplications != null){
					 for(Eapplication savedEapplication:savedApplications){
						 if(savedEapplication!=null){
							 RetrieveApp retrieveApp=new RetrieveApp();
							 retrieveApp.setApplicationNumber(savedEapplication.getApplicationumber());
							 retrieveApp.setApplicationStatus(savedEapplication.getApplicationstatus());
							 retrieveApp.setCreatedDate(savedEapplication.getCreatedate());
							 retrieveApp.setRequestType(savedEapplication.getRequesttype());
							 retrieveApp.setLastSavedOnPage(savedEapplication.getLastsavedon());
							 retrieveAppList.add(retrieveApp);
						 }
					 }
				 }
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    if(retrieveAppList==null){
	        return new ResponseEntity<List<RetrieveApp>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<RetrieveApp>>(retrieveAppList, HttpStatus.OK);
	 }
	 
	 @RequestMapping("/check24hrs")
	  public ResponseEntity<Boolean> validateApplicationsSubIn24Hrs(@RequestParam(value="fundCode") String fundCode
			  ,@RequestParam(value="clientRefNo") String clientRefNo
			  ,@RequestParam(value="manageType") String manageType) {		
		 Boolean appsSubmittedinLast24Hrs=Boolean.FALSE;
		 try{
			 if(fundCode!=null && clientRefNo!=null && manageType!=null){
				 appsSubmittedinLast24Hrs= eapplyService.checkAppSubmittedInLast24Hrs(fundCode, clientRefNo,manageType);
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    return new ResponseEntity<Boolean>(appsSubmittedinLast24Hrs, HttpStatus.OK);
	 }
	 
	 
	 @RequestMapping(value = "/download", method = RequestMethod.GET)
	 public @ResponseBody void downloadFile(@RequestParam(value="file_name") String fileName, 
	     HttpServletResponse response) {
	     try {
	       // get your file as InputStream
	    	 if(fileName!=null){
	  	       InputStream is =  new FileInputStream(fileName);
		       // copy it to response's OutputStream
	  	     //  System.out.println(fileName.substring(fileName.lastIndexOf('\\')+1));
	  	       response.addHeader("Content-disposition", "attachment;filename="+fileName.substring(fileName.lastIndexOf('\\')+1));
		       response.setContentType("application/pdf");
		       IOUtils.copy(is, response.getOutputStream());
		       response.flushBuffer();
	    	 }

	     } catch (IOException ex) {
	    	 throw new RuntimeException("IOError writing file to output stream");
	     }

	 }
	 
	 @RequestMapping("/retieveApps")
	  public ResponseEntity<List<RetrieveApp>> retireveApplicationList(@RequestParam(value="fundCode") String fundCode,@RequestParam(value="clientRefNo") String clientRefNo) {		
		 List<RetrieveApp> retrieveAppList=null;
		 try{
			 if(fundCode!=null && clientRefNo!=null){
				 retrieveAppList=new ArrayList<RetrieveApp>();
				 List<Eapplication> savedApplications= eapplyService.retrieveSavedApplications(fundCode, clientRefNo,null);
				 if(savedApplications!= null){
					 for(Eapplication savedEapplication:savedApplications){
						 if(savedEapplication!=null){
							 RetrieveApp retrieveApp=new RetrieveApp();
							 retrieveApp.setApplicationNumber(savedEapplication.getApplicationumber());
							 retrieveApp.setApplicationStatus(savedEapplication.getApplicationstatus());
							 retrieveApp.setCreatedDate(savedEapplication.getCreatedate());
							 retrieveApp.setRequestType(savedEapplication.getRequesttype());
							 retrieveApp.setLastSavedOnPage(savedEapplication.getLastsavedon());
							 retrieveAppList.add(retrieveApp);
						 }
					 } 
				 }
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    if(retrieveAppList==null){
	        return new ResponseEntity<List<RetrieveApp>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<RetrieveApp>>(retrieveAppList, HttpStatus.OK);
	 }
	 
	 
	 @RequestMapping("/retieveApplication")
	  public ResponseEntity<List<EapplyInput>> retieveApplication(@RequestParam(value="applicationNumber") String applicationNumber) {		
		 List<RetrieveApp> retrieveAppList=null;
		 List<EapplyInput> eapplyInput=null;
		 try{
			 if(applicationNumber!=null){
				 Eapplication eapplication=eapplyService.retrieveApplication(applicationNumber);
				 //Convert eapplication to eapplyinout
				 eapplyInput = convertToEapplyInput(eapplication);
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    if(eapplyInput==null){
	        return new ResponseEntity<List<EapplyInput>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<EapplyInput>>(eapplyInput, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/expireApplication", method = RequestMethod.POST)
	  public ResponseEntity<Boolean> expireApplication(@RequestParam(value="applicationNumber") String applicationNumber) {		
		 Boolean expireApplication=Boolean.FALSE;
		 try{
			 if(applicationNumber!=null){
				 Eapplication eapplication=eapplyService.expireApplication(applicationNumber);
				 expireApplication=Boolean.TRUE;
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    return new ResponseEntity<Boolean>(expireApplication, HttpStatus.OK);
	 }
	 
	 
	 
	 @RequestMapping(value = "/submitEapply", method = RequestMethod.POST)
		public  ResponseEntity<EapplyOutput> submitEapply(@RequestBody EapplyInput eapplyInput,    UriComponentsBuilder ucBuilder) {
		 Boolean submitStatus = Boolean.FALSE;
		 System.out.println(">>>inside");
		 Eapplication eapplication = convertToEapplication(eapplyInput,"Submitted");	    
		 PDFbusinessObjectMapper pdFbusinessObjectMapper = new PDFbusinessObjectMapper();
		 Properties property = new Properties();
		 InputStream url = null;
		 List<Document> documentList = null;
		 EapplyOutput eapplyOutput=null;
		 PDFObject pdfObject =null;
		 try {			 
			File file = new File( getPropertyFile());
		     url = new FileInputStream( file);    
		      property.load( url);
			 System.out.println("getPropertyFile>>"+getPropertyFile());
			 pdfObject = pdFbusinessObjectMapper.getPdfOjectsForEapply(eapplication, eapplyInput, "Institutional", getPropertyFile());
			 pdfObject = callpdfGeneration(pdfObject, getPropertyFile(), eapplyInput, eapplication, fileUploadPath);
			 //update document object
			 Document document = null;
			 if(eapplication.getDocuments()!=null && eapplication.getDocuments().size()>0){
				 document = new Document();
				 document.setDocLoc(pdfObject.getClientPDFLoc());
				 document.setDocType("client");
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.getDocuments().add(document);	
				 document = new Document();
				 document.setDocLoc(pdfObject.getUwPDFLoc());
				 document.setDocType("uw");
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.getDocuments().add(document);	
			 }else{
				 documentList = new ArrayList<>();
				 document = new Document();
				 document.setDocLoc(pdfObject.getClientPDFLoc());
				 document.setDocType("client");
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 documentList.add(document);	
				 document = new Document();
				 document.setDocLoc(pdfObject.getUwPDFLoc());
				 document.setDocType("uw");
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 documentList.add(document);	
				
				 eapplication.setDocuments(documentList);
			 }	
			 
			 /*Adding transfer upload documents*/
 	 			 if(eapplyInput.getTransferDocuments()!=null && eapplyInput.getTransferDocuments().size()>0){
 	 				 for(DocumentJSON documentJSON:eapplyInput.getTransferDocuments()){
 	 					 Document transferDocument = new Document();
 	 					 transferDocument.setDocLoc(documentJSON.getFileLocations());
 	 					 transferDocument.setDocType("Transfer Documents");
 	 					 transferDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
 	 					 eapplication.getDocuments().add(transferDocument);	
 	 				 }
 	 			 }
  			 /*Adding transfer upload documents*/
 	 			 
 	 		/*Updating  Clieant and UW PDF in applicant*/
 	 			 if(eapplication.getApplicant()!=null && eapplication.getApplicant().size()>0){
 	 				 Applicant applicant=eapplication.getApplicant().get(0);			 EmailHelper emailHelper = new EmailHelper();
		//	 emailHelper.sendEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), "Institutional", property);
		//	 String bizFlowXML = BizflowXMLHelper.createXMLForBizFlow(eapplication, eapplyInput,property);
		//	 BizflowXMLHelper.sendMetFlowRequestToMQ(bizFlowXML,property);
 	 				applicant.setUwpdfloc(pdfObject.getUwPDFLoc());
 	 				applicant.setClientpdfloc(pdfObject.getClientPDFLoc());
 	 			 }
 	 		/*Updating  Clieant and UW PDF in applicant*/
			 
			 eapplyService.submitEapplication(eapplication);
			 submitStatus=Boolean.TRUE;
			 eapplyOutput=new EapplyOutput();
			 eapplyOutput.setAppStatus(submitStatus);
			 if(pdfObject!=null){
				 eapplyOutput.setClientPDFLocation(pdfObject.getClientPDFLoc());
			 }

		 	} catch (IOException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}catch (MetLifeBSRuntimeException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}   
		//	 catch (JAXBException e) {
				// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	} catch (JMSException e) {
				// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
	     return new ResponseEntity<EapplyOutput>(eapplyOutput, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
		public  ResponseEntity<DocumentJSON> uploadFile(@RequestHeader(value="Content-Type") String contentType,InputStream inputStream) {
		 DocumentJSON documentJSON = null;
		 System.out.println(">>>inside"+contentType);
		// BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		 String filepath = fileUploadPath+contentType;
		 File targetFile = new File(filepath);
		 OutputStream outStream = null;
	        try {	            
	        	outStream = new FileOutputStream(targetFile);
	        	byte[] buffer = new byte[1024];
	        	int len = inputStream.read(buffer);
	        	while (len != -1) {
	        		outStream.write(buffer, 0, len);
	        	    len = inputStream.read(buffer);
	        	}	            
	            IOUtils.copy(inputStream,outStream);
	            inputStream.close();
	            outStream.close();
	            documentJSON = new DocumentJSON();
	            documentJSON.setFileLocations(filepath);
	            documentJSON.setName(contentType);
	            documentJSON.setStatus(Boolean.TRUE);
	            
	        } catch (IOException e) {
	            throw new RuntimeException(e);
	        } 		 
		 
		 return new ResponseEntity<DocumentJSON>(documentJSON, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/clientMatch", method = RequestMethod.POST)
		public  ResponseEntity<Boolean> clientMatch(InputStream inputStream) throws IOException, JSONException {
		String line = null;
		String string = "";	
		JSONObject json = null;
		 String firstName = null;
		 String surName = null;
		 String dob = null;
		 BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		 while ((line = in.readLine()) != null) { 
			string += line + "\n";
			json = new JSONObject(string);  		
			if(json.get("firstName")!=null)
				firstName = (String)json.get("firstName");
			if(json.get("lastName")!=null)
				surName =(String)json.get("lastName");
			if(json.get("dob")!=null)
				dob =(String)json.get("dob");
			}
		 Boolean clientMatch = Boolean.FALSE;
		clientMatch = eapplyService.clientMatch(firstName, surName, dob);
		 return new ResponseEntity<Boolean>(clientMatch, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/saveEapply", method = RequestMethod.POST)
		public  ResponseEntity<Boolean> saveEapply(@RequestBody EapplyInput eapplyInput,    UriComponentsBuilder ucBuilder) {
		 Boolean submitStatus = Boolean.FALSE;
		 System.out.println(">>>inside");
		 Eapplication eapplication = convertToEapplication(eapplyInput,"Pending");
	     try {
			eapplyService.submitEapplication(eapplication);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     return new ResponseEntity<Boolean>(submitStatus, HttpStatus.OK);
	 }
	 
	 
	 private PDFObject callpdfGeneration(PDFObject pdfObject, String  pdfBasePath, EapplyInput eapplyInput, Eapplication eapplication, String fileUploadPath) throws DocumentException, IOException{
		String clientPdfLocation = null;
		String pdfAttachDir = null;
		String logoPath = null;
		String uwPdfLocation = null;
		 InputStream url = null;
		 List<String> questionHideList = new ArrayList<String>();
		Properties property = new Properties();
		File file = new File( pdfBasePath);
        url = new FileInputStream( file);    
        property.load( url);
		//need to set this
        questionHideList.add("11241");
        questionHideList.add("11242");
        questionHideList.add("11243");
        questionHideList.add("11251");
        questionHideList.add("11252");        
     
         pdfObject.setQuestionListForHide(questionHideList);
	        pdfObject.setLob("Institutional");		 
			pdfObject.setFundId("CARE");
			pdfObject.setSectionBackground(property.getProperty("ClientSectionColor_" + pdfObject.getFundId()));
			pdfObject.setColorStyle(property.getProperty("ClientColorStyle_" + pdfObject.getFundId()));
			pdfObject.setSectionTextColor(property.getProperty("ClientSectionTextColor_" + pdfObject.getFundId()));
			pdfObject.setBorderColor(pdfObject.getSectionBackground());
			logoPath = property.getProperty("logo_" + pdfObject.getFundId()); 
		    pdfObject.setLogPath(logoPath);	
			pdfObject.setApplicationNumber(""+eapplyInput.getAppNum());
			clientPdfLocation = getIndvPdfAttachDir(eapplication, "Institutional","Client",fileUploadPath);
			pdfObject.setPdfAttachDir(clientPdfLocation);
			pdfObject.setClientPDFLoc(clientPdfLocation);
			GenerateEapplyPDF generateEapplyPDF = new GenerateEapplyPDF();
			pdfAttachDir = generateEapplyPDF.generatePDF(pdfObject,pdfBasePath);
			
			pdfObject.setFundId("CARE");
			pdfObject.setSectionBackground(property.getProperty("UWSectionColor_" + pdfObject.getFundId()));
    		pdfObject.setColorStyle(property.getProperty("UWColorStyle_" + pdfObject.getFundId()));
    		pdfObject.setSectionTextColor(property.getProperty("UWSectionTextColor_" + pdfObject.getFundId()));
    		pdfObject.setBorderColor(pdfObject.getSectionBackground());
    		pdfObject.setPdfType("UW");
    		uwPdfLocation = getIndvPdfAttachDir(eapplication, "Institutional","UW",fileUploadPath);
    		pdfObject.setPdfAttachDir(uwPdfLocation);		
    		pdfObject.setUwPDFLoc(uwPdfLocation);
    		generateEapplyPDF.generatePDF(pdfObject, pdfBasePath);
			pdfObject.setUwPDFLoc(uwPdfLocation);
			
		return pdfObject;
	 }
	 
	 private String getIndvPdfAttachDir(Eapplication eapplication,String lob,String type, String fileUploadPath) {

	        final String METHOD_NAME = "getIndvPdfAttachDir";
	        String pdfAttachDir = null;
	        Applicant applicantDTO = null;      
	        String baseDirPath =  fileUploadPath;//ConfigurationHelper.getConfigurationValue( "FILE_UPLOAD_PATH", "FILE_UPLOAD_PATH");
	        Applicant applicantDTOPrimary = null;
	        String applicantsNames = null;  
	        StringBuilder builder = null;
	       
	        if (null != eapplication && null != eapplication.getApplicant()) {
	            builder = new StringBuilder();
	            for (int itr = 0; itr < eapplication.getApplicant().size(); itr++) {
	                applicantDTO = (Applicant) eapplication.getApplicant().get( itr);                
	                applicantDTOPrimary = applicantDTO;        
	                /*if (null != applicantDTO
	                        && null != applicantDTO.getApplicantType()
	                        && applicantDTO.getApplicantType().equalsIgnoreCase( "Primary")) {
	                    applicantDTOPrimary = applicantDTO;                   
	                }*/
	               
	            }          
	            if (null != applicantDTOPrimary) {
	                if (null != applicantDTOPrimary.getTitle())
	                    builder.append(applicantDTOPrimary.getTitle());
	                if (null != applicantDTOPrimary.getFirstname())
	                    builder.append(applicantDTOPrimary.getFirstname());
	                if (null != applicantDTOPrimary.getLastname())
	                    builder.append(applicantDTOPrimary.getLastname());
	            }          
	            applicantsNames = builder.toString();
	            
	            if(null != applicantsNames && applicantsNames.contains(" ")){
	            	applicantsNames = applicantsNames.replaceAll(" ", "");
				}
	          
	        }
	        if (null!=type && type.equalsIgnoreCase( "UW")) {                  
	         
	        	pdfAttachDir = baseDirPath + "UW_" + applicantsNames + "_"
	                     + eapplication.getApplicationumber() + ".pdf";
	            
	        }else {
	        	pdfAttachDir = baseDirPath +"CARE"+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
                + ".pdf";
	           
	        }
	        applicantDTO = null;       
	        baseDirPath = null;
	        applicantDTOPrimary = null;
	        applicantsNames = null;
	        applicantDTO = null;  
	        builder = null;
	        
	        System.out.println("Before retun>>>"+pdfAttachDir);
	        return pdfAttachDir;
	    }

	private Eapplication convertToEapplication(EapplyInput eapplyInput,String appStatus) {
		 Eapplication eapplication =new Eapplication();
		 Applicant applicant1=new Applicant();
		 List<Applicant> applicantList=new ArrayList<Applicant>();		 		
		 		System.out.println("getPropertyFile>>"+getPropertyFile()); 
		 if(eapplyInput!=null){
		 			/*Eapplication object mapping start*/
		 			eapplication.setCreatedate(new Timestamp(new Date().getTime()));
		 			eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
		 			eapplication.setApplicationstatus(appStatus);
		 			eapplication.setAppdecision(eapplyInput.getOverallDecision());
		 			eapplication.setApplicationtype("institutional");
		 			eapplication.setLastsavedon(eapplyInput.getLastSavedOn());
		 			if(eapplyInput.getAppNum()!=null){
		 				eapplication.setApplicationumber(eapplyInput.getAppNum().toString());
		 			}
		 			eapplication.setPartnercode(eapplyInput.getPartnerCode());
		 			eapplication.setRequesttype(eapplyInput.getManageType());
		 			DateFormat dateMonthFormat = new SimpleDateFormat("MMyy");
					String monthDir = dateMonthFormat.format(new java.util.Date());
					String campaignCode = "CARE" + monthDir +"WG-" + eapplyInput.getAppNum();
		 			eapplication.setCampaigncode(campaignCode);
		 			if((eapplyInput.getDeathExclusions()!=null && eapplyInput.getDeathExclusions().trim().length()>0)
		 					|| (eapplyInput.getTpdExclusions()!=null && eapplyInput.getTpdExclusions().trim().length()>0)
		 					|| (eapplyInput.getIpExclusions()!=null && eapplyInput.getIpExclusions().trim().length()>0)
		 					){
		 				eapplication.setSaindicator("True");
		 			}else{
		 				eapplication.setSaindicator("False");
		 			}
		 			if((eapplyInput.getDeathLoading()!=null && eapplyInput.getDeathLoading().trim().length()>0)
		 					|| (eapplyInput.getTpdLoading()!=null && eapplyInput.getTpdLoading().trim().length()>0)
		 					|| (eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
		 					){
		 				eapplication.setPaindicator("True");
		 			}else{
		 				eapplication.setPaindicator("False");
		 			}
		 			if(eapplyInput.getTotalPremium() != null){
		 				eapplication.setTotalMonthlyPremium(eapplyInput.getTotalPremium());
		 			}
		 			/*Eapplication object mapping end*/
		 			
		 			/*Applicant object mapping start*/
		            if(eapplyInput.getAppNum()!=null){
		            	applicant1.setQuestionnaireid(eapplyInput.getAppNum().toString());
		 			}
		            if(eapplyInput.getPersonalDetails()!=null){
		            	applicant1.setCreatedate(new Timestamp(new Date().getTime()));
			 			applicant1.setLastupdatedate(new Timestamp(new Date().getTime()));
		            	applicant1.setTitle(eapplyInput.getPersonalDetails().getTitle());
		            	applicant1.setFirstname(eapplyInput.getPersonalDetails().getFirstName());
		            	applicant1.setLastname(eapplyInput.getPersonalDetails().getLastName());
		            	if(eapplyInput.getPersonalDetails().getDateOfBirth()!=null){
		            		applicant1.setBirthdate(new Timestamp(new Date(eapplyInput.getPersonalDetails().getDateOfBirth()).getTime()));
		            	}
		            	if(eapplyInput.getPersonalDetails().getSmoker()!=null){
		            			applicant1.setSmoker(eapplyInput.getPersonalDetails().getSmoker());
		            	}
		            	if(eapplyInput.getPersonalDetails().getGender() != null){
		            		applicant1.setGender(eapplyInput.getPersonalDetails().getGender());
		            	}
		            	
		            	if(eapplyInput.getGender() != null){ // in case of CANCEL cover 
		            		applicant1.setGender(eapplyInput.getGender());
		            	}
		            }
		            
		            if(eapplyInput.getOccupationDetails() != null){
			        	 applicant1.setWorkhours(eapplyInput.getOccupationDetails().getFifteenHr());
			             applicant1.setIndustrytype(eapplyInput.getOccupationDetails().getIndustryCode());
			        	 applicant1.setOccupationcode(eapplyInput.getOccupationDetails().getIndustryName());
		            	 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());
		            	 applicant1.setAnnualsalary(eapplyInput.getOccupationDetails().getSalary());
			        	 applicant1.setAustraliacitizenship(eapplyInput.getOccupationDetails().getCitizenQue());
			        	 applicant1.setGender(eapplyInput.getOccupationDetails().getGender());
			        	 /*
		        	     *  Workduties - Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar 'white collar'
		        	        tasks which do not involve manual work and are undertaken entirely within an office environment (excluding travel time from one office environment to another)? 
			        	 *  Spendtimeoutside - Do you hold a tertiary qualification or are you a member of a professional institute or registered as a practising member of your profession by a government body ?
			        	 *  Occupationduties -  Are you in a management role ? 
			        	 * 
			        	 */
		        	 applicant1.setWorkduties(eapplyInput.getOccupationDetails().getWithinOfficeQue()); //
			        	 applicant1.setOccupationduties(eapplyInput.getOccupationDetails().getManagementRoleQue());
			        	 applicant1.setSpendtimeoutside(eapplyInput.getOccupationDetails().getTertiaryQue());
		           }
		            applicant1.setApplicanttype("Primary");
		            applicant1.setEmailid(eapplyInput.getEmail());
		        	applicant1.setDatejoinedfund(eapplyInput.getDateJoined());
		            applicant1.setClientrefnumber(eapplyInput.getClientRefNumber());	        	
		        	applicant1.setMembertype(eapplyInput.getMemberType());
		            applicant1.setAge(""+eapplyInput.getAge());
		            applicant1.setCustomerreferencenumber(eapplyInput.getEmail());
		        	applicant1.setContacttype(eapplyInput.getContactType());
		        	applicant1.setContactnumber(eapplyInput.getContactPhone());
		        	applicant1.setOverallcoverdecision(eapplyInput.getOverallDecision());
		        	if(eapplyInput.getIndexationDeath()!= null && eapplyInput.getIndexationDeath().equalsIgnoreCase("true")){
		        		applicant1.setDeathindexationflag("1");
		        	}else{
		        		applicant1.setDeathindexationflag("0");
		        	}
		        	
		        	if(eapplyInput.getIndexationTpd()!= null && eapplyInput.getIndexationTpd().equalsIgnoreCase("true")){
		        		applicant1.setTpdindexationflag("1");
		        	}else{
		        		applicant1.setTpdindexationflag("0");
		        	}
		        	if(eapplyInput.getIpcheckbox() != null && eapplyInput.getIpcheckbox().equalsIgnoreCase("true")){
		        		applicant1.setInsuredsalary("Y");
		        	}else{
		        		applicant1.setInsuredsalary("N");
		        	}
		        	// Transfer Previous cover section fields
		        	applicant1.setPreviousinsurername(eapplyInput.getPreviousFundName());
		        	applicant1.setFundmempolicynumber(eapplyInput.getMembershipNumber());
		        	applicant1.setSpinnumber(eapplyInput.getSpinNumber());
		        	applicant1.setPrevioustpdclaimque(eapplyInput.getPreviousTpdClaimQue());
		        	applicant1.setPreviousterminalillque(eapplyInput.getTerminalIllClaimQue()); // for transfer and new member 
		        	applicant1.setDocumentevidence(eapplyInput.getDocumentName()); // for transfer and new member 
		        	
		        	/*Applicant object mapping start*/
		        	
		        	List<Cover> coverList = null;
		            if(eapplyInput.getManageType()!= null){
		            	if(eapplyInput.getManageType().equalsIgnoreCase("CCOVER")){
		            		coverList = createChangeCvCovers(eapplyInput);
		            	}else if(eapplyInput.getManageType().equalsIgnoreCase("TCOVER")){
		            		coverList = createTransferCvCovers(eapplyInput);
		            	}else if(eapplyInput.getManageType().equalsIgnoreCase("UWCOVER")){
		            		coverList = createUpdateWorkRatingCvCovers(eapplyInput);
		            	}else  if(eapplyInput.getManageType().equalsIgnoreCase("NCOVER")){
		            		coverList = createNewMemberCvCovers(eapplyInput);
		            	}else if(eapplyInput.getManageType().equalsIgnoreCase("CANCOVER")){
		            		coverList = createCancelCvCovers(eapplyInput);
 		            	}
 		            }
		            /*Cover object mapping end*/
		            
//		            /*Aura detail object mapping start*/
		            Auradetail auradetail =new Auradetail();

//		            /*Aura detail object mapping end*/
		            
		            applicant1.setCovers(coverList);
		            applicantList.add(applicant1);
		            eapplication.setApplicant(applicantList);
		            eapplication.setAuradetail(auradetail);
//		            eapplication.setFundInfo(fundInfoList);
		            
		            if(eapplyInput.getContactDetails()!=null){
		            	Contactinfo contactinfo=new Contactinfo();
		            	contactinfo.setMobilephone(eapplyInput.getContactDetails().getMobilePhone());
		            	contactinfo.setHomephone(eapplyInput.getContactDetails().getHomePhone());
		            	contactinfo.setWorkphone(eapplyInput.getContactDetails().getWorkPhone());
		            	contactinfo.setPreferedcontactnumber(eapplyInput.getContactPhone());
		            	contactinfo.setPreferedcontacttime(eapplyInput.getContactPrefTime());
		            	contactinfo.setAddressline1(eapplyInput.getAddress().getLine1());
		            	contactinfo.setAddressline2(eapplyInput.getAddress().getLine2());
		            	contactinfo.setCountry(eapplyInput.getAddress().getCountry());
		            	contactinfo.setPostcode(eapplyInput.getAddress().getPostCode());
		            	contactinfo.setState(eapplyInput.getAddress().getState());
		            	contactinfo.setSuburb(eapplyInput.getAddress().getSuburb());
		            	applicant1.setContactinfo(contactinfo);
		            }
		            
		          //  contactinfo.setApplicant(applicant1);
		          //  contactinfo.setAddressline1(addressline1);
		           
		 		}
	           
//	            eapplication.setDocuments(documentList);;
//	            eapplication.setFundInfo(fundInfo);
		return eapplication;
	}
	 
private List<Cover> createChangeCvCovers(EapplyInput eapplyInput) {
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		String ipExistingAmt = null;
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<Cover>();
		
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype("Death");
		deathCover.setCovercode("DEATH");
		deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
		deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());
		deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		if(eapplyInput.getDeathLoading()!=null)
		deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
		deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase("DcUnitised")){
			deathCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue()));
		}else{
			deathCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
		}
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase("DcUnitised")){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase("DcFixed")){
				deathCover.setAddunitind("0");
			}
		}
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
		
		
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype("Total Permanent Disability");
		tpdCover.setCovercode("TPD");
		tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
		tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());
		tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		if(eapplyInput.getTpdLoading()!=null)
			tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
		tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase("TpdUnitised")){
			tpdCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue()));
		}else{
			tpdCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
		}
		tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase("TpdUnitised")){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase("TpdFixed")){
				tpdCover.setAddunitind("0");
			}
		}
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
		if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);
		}		            
		ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype("Income Protection");
		ipCover.setCovercode("IP");
		ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
		ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
		if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
			ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
		ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
		ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase("IpUnitised")){
			ipCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue()));
		}else{
			ipCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
		}
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase("IpUnitised")){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase("IpFixed")){
				ipCover.setAddunitind("0");
			}
		}	
		if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
		    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());

		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")){/*IP*/
					if(coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
					}
				}
				
			}
		}
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		/*Cover object mapping end*/
		return coverList;
}
		        		 	 
private List<Cover> createTransferCvCovers(EapplyInput eapplyInput) {
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		String ipExistingAmt = null;
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<Cover>();
		
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype("Death");
		deathCover.setCovercode("DEATH");
		deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
	//	deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());
		deathCover.setFixedunit(Integer.valueOf(deathExUnit));
	//	if(eapplyInput.getDeathLoading()!=null)
	//	deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		deathCover.setCovercategory("DcFixed");
	//	deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathTransferWeeklyCost());
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt()!= null){
			deathCover.setTransfercoveramount(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt());	
		}
		System.out.println("Death transfer amt>>"+eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt());
		/*if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType().equalsIgnoreCase("DcUnitised")){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType().equalsIgnoreCase("DcFixed")){
				deathCover.setAddunitind("0");
			}
		}*/
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt()));

		System.out.println("Death trans amt>>"+eapplyInput.getAddnlDeathCoverDetails().getDeathTransferWeeklyCost());
		System.out.println("Death Addnl amt>>"+deathCover.getAdditionalcoveramount());
		System.out.println("existing death>>"+eapplyInput.getExistingDeathAmt());
		
		
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype("Total Permanent Disability");
		tpdCover.setCovercode("TPD");
		tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
	//	tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());
		tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
	//	if(eapplyInput.getTpdLoading()!=null)
	//		tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		tpdCover.setCovercategory("TpdFixed");
	//	tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdTransferWeeklyCost());
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt() != null){
			tpdCover.setTransfercoveramount(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt());
		}
		System.out.println("TPD transfer amt>>"+eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt());
		/*if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType().equalsIgnoreCase("TpdUnitised")){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType().equalsIgnoreCase("TpdFixed")){
				tpdCover.setAddunitind("0");
		}
		}*/
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt()));
		if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);
		}		            
		ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype("Income Protection");
		ipCover.setCovercode("IP");
		ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
	//	ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
	//	if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
	//		ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCovercategory("IpFixed");
	//	ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpTransferWeeklyCost());
		ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt()!= null){
			ipCover.setTransfercoveramount(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt());
		}
		System.out.println("IP transfer amt>>"+eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt());
		/*if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase("IpUnitised")){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase("IpFixed")){
				ipCover.setAddunitind("0");
			}
		}*/
		if(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()!= null){
		ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()));
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getAddnlTransferBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getAddnlTransferWaitingPeriod());
		
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")){/*IP*/
					if(coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
					}
				}
				
      }
}
coverList.add(deathCover);
coverList.add(tpdCover);
coverList.add(ipCover);
/*Cover object mapping end*/
 return coverList;
}

private List<Cover> createUpdateWorkRatingCvCovers(EapplyInput eapplyInput) {
	String deathExUnit = "0";
	String tpdExUnit = "0";
	String ipExUnit = "0";
	String ipExistingAmt = null;
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){
		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			CoverJSON coverJSON = (CoverJSON) iterator.next();
			if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
				deathExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
				tpdExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
				ipExUnit = coverJSON.getUnits();
				ipExistingAmt=coverJSON.getAmount();
			}
			
		}
	}
	
	/*Cover object mapping start*/        	
	
	Cover deathCover=new Cover();
	Cover tpdCover=new Cover();
	Cover ipCover=new Cover();
	List<Cover> coverList=new ArrayList<Cover>();
	
	deathCover.setCreatedate(new Timestamp(new Date().getTime()));
	deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	deathCover.setCovertype("Death");
	deathCover.setCovercode("DEATH");
	deathCover.setCoverdecision(eapplyInput.getDeathDecision());
	deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
	deathCover.setFixedunit(Integer.valueOf(deathExUnit));
	deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
	deathCover.setFrequencycosttype("Weekly");
//	deathCover.setAdditionalcoveramount(BigDecimal.ZERO);
	deathCover.setCost("0");
	if(eapplyInput.getExistingDeathAmt()!=null){
		deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
	}
	
	tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
	tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	tpdCover.setCovertype("Total Permanent Disability");
	tpdCover.setCovercode("TPD");
	tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
	tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
	tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
	tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
	tpdCover.setFrequencycosttype("Weekly");
//	tpdCover.setAdditionalcoveramount(BigDecimal.ZERO);
	tpdCover.setCost("0");
	if(eapplyInput.getExistingTpdAmt()!=null){
		tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
	}
		
	if(ipExistingAmt!=null){
		ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
		eapplyInput.setExistingIPAmount(ipExistingAmt);
	}		            
	ipCover.setFixedunit(Integer.valueOf(ipExUnit));
	ipCover.setCreatedate(new Timestamp(new Date().getTime()));
	ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	ipCover.setCovertype("Income Protection");
	ipCover.setCovercode("IP");
	ipCover.setCoverdecision(eapplyInput.getIpDecision());
	ipCover.setCoverreason(eapplyInput.getIpAuraResons());
	ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
	ipCover.setFrequencycosttype("Weekly");
//	ipCover.setAdditionalcoveramount(BigDecimal.ZERO);
	ipCover.setCost("0");
	
	/*if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
	 ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
	}*/
	
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
			CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
			if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
				if(coverJSON.getAmount()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
				
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
				if(coverJSON.getAmount()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")){/*IP*/
				if(coverJSON.getAmount()!=null){
					ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
			}
			
		}
	}
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	return coverList;
}


private List<EapplyInput> convertToEapplyInput(Eapplication eapplication) {
		EapplyInput eapplyInput = new EapplyInput();
		List<EapplyInput> eapplyInputList = new ArrayList<EapplyInput>(); 
		Applicant applicantObj = null;
		Cover coversObj = null;
		Contactinfo contactinfo = null;
		List<Applicant> applicantList = eapplication.getApplicant();
		PersonalDetailsJSON personalDetails = new PersonalDetailsJSON();
		OccupationCoversJSON occupationDetails = new OccupationCoversJSON();
		ContactDetailsJSON contactDetails = new ContactDetailsJSON();
		AddressJSON address = new AddressJSON();
		ExistingCoversJSON existingCovers = new ExistingCoversJSON();
		DeathAddnlCoversJSON addnlDeathCoverDetails = new DeathAddnlCoversJSON();
		TpdAddnlCoversJSON addnlTpdCoverDetails = new TpdAddnlCoversJSON();
		IpAddnlCoversJSON addnlIpCoverDetails = new IpAddnlCoversJSON();
		System.out.println("getPropertyFile>>" + getPropertyFile());
		if (eapplication != null) {
			
			/*Eapplication object mapping start*/

			if(eapplication.getTotalMonthlyPremium()!= null){
				eapplyInput.setTotalPremium(eapplication.getTotalMonthlyPremium());
			}
		
			eapplyInput.setOverallDecision(eapplication.getAppdecision());
			eapplyInput.setLastSavedOn(eapplication.getLastsavedon());
			if (eapplication.getApplicationumber() != null) {
				eapplyInput.setAppNum(Long.parseLong(eapplication.getApplicationumber()));
			}
			eapplyInput.setManageType(eapplication.getRequesttype());
			eapplyInput.setPartnerCode(eapplication.getPartnercode());
			
			
			/*Eapplication object mapping End */
			if (applicantList != null) {
				for (int appItr = 0; appItr < applicantList.size(); appItr++) {
					applicantObj = (Applicant) applicantList.get(appItr);
					
					/*Applicant object mapping start*/
					if (applicantObj != null) {
						personalDetails.setTitle(applicantObj.getTitle());
						personalDetails.setFirstName(applicantObj.getFirstname());
						personalDetails.setLastName(applicantObj.getLastname());
						if(applicantObj.getBirthdate() != null){
							personalDetails.setDateOfBirth(applicantObj.getBirthdate().toString());
						}
						if(applicantObj.getSmoker() != null){
							personalDetails.setSmoker(applicantObj.getSmoker());
						}
						
						if(applicantObj.getDeathindexationflag()!= null && applicantObj.getDeathindexationflag().equalsIgnoreCase("1") ){
							eapplyInput.setIndexationDeath("true");
						}else{
							eapplyInput.setIndexationDeath("false");
						}
						if(applicantObj.getTpdindexationflag()!= null && applicantObj.getTpdindexationflag().equalsIgnoreCase("1") ){
							eapplyInput.setIndexationTpd("true");
						}else{
							eapplyInput.setIndexationTpd("false");
						}
						if(applicantObj.getInsuredsalary()!= null && applicantObj.getInsuredsalary().equalsIgnoreCase("Y") ){
							eapplyInput.setIpcheckbox("true");
						}else{
							eapplyInput.setIpcheckbox("false");
						}
						
						personalDetails.setGender(applicantObj.getGender());
						occupationDetails.setOccupation(applicantObj.getOccupation());
                        eapplyInput.setEmail(applicantObj.getEmailid());
                        eapplyInput.setAge(Integer.parseInt(applicantObj.getAge()));
                        occupationDetails.setSalary(applicantObj.getAnnualsalary());
                        eapplyInput.setDateJoined(applicantObj.getDatejoinedfund());
                        eapplyInput.setOverallDecision(applicantObj.getOverallcoverdecision());
                        eapplyInput.setMemberType(applicantObj.getMembertype());
                        occupationDetails.setIndustryCode(applicantObj.getIndustrytype());
            			occupationDetails.setIndustryName(applicantObj.getOccupationcode());
                        occupationDetails.setFifteenHr(applicantObj.getWorkhours());
                        occupationDetails.setCitizenQue(applicantObj.getAustraliacitizenship());
                        occupationDetails.setWithinOfficeQue(applicantObj.getWorkduties());
                        occupationDetails.setManagementRoleQue(applicantObj.getOccupationduties());
                        occupationDetails.setTertiaryQue(applicantObj.getSpendtimeoutside());
                        eapplyInput.setContactType(applicantObj.getContacttype());
                        eapplyInput.setContactPhone(applicantObj.getContactnumber());
                        eapplyInput.setPersonalDetails(personalDetails);
                        eapplyInput.setOccupationDetails(occupationDetails);
                        /*Applicant object mapping End*/
					}
					
					/*Cover object mapping start*/ 
					if(eapplication.getRequesttype()!= null){
						if(eapplication.getRequesttype().equalsIgnoreCase("CCOVER")){
						eapplyInput = createChangeCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
						addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase("TCOVER")){
						eapplyInput = createTransferCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
						addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase("UWCOVER")){
						eapplyInput = createWorkRatingCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);	
						}else if(eapplication.getRequesttype().equalsIgnoreCase("NCOVER")){
						eapplyInput = createNewMemberCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);		
						}else if(eapplication.getRequesttype().equalsIgnoreCase("CANCOVER")){
							
						}
					}
					
					
					/*Cover object mapping End*/ 
					
					if(null!= applicantObj.getContactinfo()){
						contactinfo = applicantObj.getContactinfo();
						contactDetails.setMobilePhone(contactinfo.getMobilephone());
						contactDetails.setHomePhone(contactinfo.getHomephone());
						contactDetails.setWorkPhone(contactinfo.getWorkphone());
						eapplyInput.setContactPhone(contactinfo.getPreferedcontactnumber());
						eapplyInput.setContactPrefTime(contactinfo.getPreferedcontacttime());
						address.setLine1(contactinfo.getAddressline1());
						address.setLine2(contactinfo.getAddressline2());
						address.setCountry(contactinfo.getCountry());
						address.setPostCode(contactinfo.getPostcode());
						address.setState(contactinfo.getState());
						address.setSuburb(contactinfo.getSuburb());
						eapplyInput.setContactDetails(contactDetails);
						eapplyInput.setAddress(address);
					}
					
				}

			}
		}
		eapplyInputList.add(eapplyInput);
		return eapplyInputList;
	}

private List<Cover> createNewMemberCvCovers(EapplyInput eapplyInput) {
	String deathExUnit = "0";
	String tpdExUnit = "0";
	String ipExUnit = "0";
	String ipExistingAmt = null;
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){
		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			CoverJSON coverJSON = (CoverJSON) iterator.next();
			if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
				deathExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
				tpdExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
				ipExUnit = coverJSON.getUnits();
				ipExistingAmt=coverJSON.getAmount();
			}
			
		}
}
	
	/*Cover object mapping start*/        	
	
	Cover deathCover=new Cover();
	Cover tpdCover=new Cover();
	Cover ipCover=new Cover();
	List<Cover> coverList=new ArrayList<Cover>();
	
	deathCover.setCreatedate(new Timestamp(new Date().getTime()));
	deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	deathCover.setCovertype("Death");
	deathCover.setCovercode("DEATH");
	deathCover.setCoverdecision(eapplyInput.getDeathDecision());
	deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
//	deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());
	deathCover.setFixedunit(Integer.valueOf(deathExUnit));
//	if(eapplyInput.getDeathLoading()!=null)
//	deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
	deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
	deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType());
//	deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
	deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverPremium());
	if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType()!= null && eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType().equalsIgnoreCase("DcUnitised")){
		deathCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlDeathCoverDetails().getNewDeathUpdatedCover()));
	}else{
		deathCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlDeathCoverDetails().getNewDeathLabelAmt()));
	}
	if(eapplyInput.getExistingDeathAmt()!=null){
		deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
	}
	deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType()!=null){
		if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType().equalsIgnoreCase("DcUnitised")){
			deathCover.setAddunitind("1");
		}else if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType().equalsIgnoreCase("DcFixed")){
			deathCover.setAddunitind("0");
		}
	}
	deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getNewDeathLabelAmt()));
	
	
	tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
	tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	tpdCover.setCovertype("Total Permanent Disability");
	tpdCover.setCovercode("TPD");
	tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
	tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
//	tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());
	tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
//	if(eapplyInput.getTpdLoading()!=null)
//		tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
	tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
	tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType());
//	tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
	tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverPremium());
	if(eapplyInput.getExistingTpdAmt()!=null){
		tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
	}
	if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType()!= null && eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType().equalsIgnoreCase("TpdUnitised")){
		tpdCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlTpdCoverDetails().getNewTpdUpdatedCover()));
	}else{
		tpdCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlTpdCoverDetails().getNewTpdLabelAmt()));
	}
	tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType()!=null){
		if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType().equalsIgnoreCase("TpdUnitised")){
			tpdCover.setAddunitind("1");
		}else if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType().equalsIgnoreCase("TpdFixed")){
			tpdCover.setAddunitind("0");
		}
	}
	tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getNewTpdLabelAmt()));
	if(ipExistingAmt!=null){
		ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
		eapplyInput.setExistingIPAmount(ipExistingAmt);
	}		            
	ipCover.setFixedunit(Integer.valueOf(ipExUnit));
	ipCover.setCreatedate(new Timestamp(new Date().getTime()));
	ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	ipCover.setCovertype("Income Protection");
	ipCover.setCovercode("IP");
	ipCover.setCoverdecision(eapplyInput.getIpDecision());
	ipCover.setCoverreason(eapplyInput.getIpAuraResons());
//	ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
//	if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
//		ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
	ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
	ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType());
//	ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
	ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getNewIpCoverPremium());
	ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType()!=null){
		if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType().equalsIgnoreCase("IpUnitised")){
			ipCover.setAddunitind("1");
		}else if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType().equalsIgnoreCase("IpFixed")){
			ipCover.setAddunitind("0");
		}
	}	
	if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType()!= null && eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType().equalsIgnoreCase("IpUnitised")){
		ipCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlIpCoverDetails().getNewIpUpdatedCover()));
	}else{
		ipCover.setAdditionalunit(Integer.parseInt(eapplyInput.getAddnlIpCoverDetails().getNewIpLabelAmt()));
	}
	if(eapplyInput.getAddnlIpCoverDetails().getNewIpLabelAmt()!= null){
	ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getNewIpLabelAmt()));
	}
	ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getNewBenefitPeriod());
	ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getNewWaitingPeriod());

	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
			CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
			if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
				if(coverJSON.getAmount()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
				
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
				if(coverJSON.getAmount()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")){/*IP*/
				if(coverJSON.getAmount()!=null){
					ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
			}
			
		}
	}
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	return coverList;
}

private EapplyInput createChangeCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>0){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = (Cover)applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if("DEATH".equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					
					if(coversObj.getCovercategory()!= null){
						addnlDeathCoverDetails.setDeathInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
						}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	if(coversObj.getCovercategory()!= null){
	            		addnlTpdCoverDetails.setTpdInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getCovercategory()!= null){
	            		addnlIpCoverDetails.setIpInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            	}
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingIPUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	return eapplyInput;
}

private EapplyInput createTransferCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
		Cover coversObj;
		if (applicantObj.getCovers() != null && applicantObj.getCovers().size() > 0) {
			for (int coverItr = 0; coverItr < applicantObj.getCovers().size(); coverItr++) {
				coversObj = (Cover) applicantObj.getCovers().get(coverItr);
				if (null != coversObj) {
					if ("DEATH".equalsIgnoreCase(coversObj.getCovercode())) {
						eapplyInput.setDeathDecision(coversObj.getCoverdecision());
						eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
						eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
						addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
						addnlDeathCoverDetails.setDeathTransferCoverType(coversObj.getCovercategory());
						addnlDeathCoverDetails.setDeathTransferWeeklyCost(coversObj.getCost());
						if (coversObj.getAdditionalcoveramount() != null) {
							addnlDeathCoverDetails
									.setDeathTransferCovrAmt(coversObj.getAdditionalcoveramount().toString());
						}
						eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
						if (coversObj.getLoading() != null) {
							eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}
						if (coversObj.getExistingcoveramount() != null) {
							eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
						eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					}
					if ("TPD".equalsIgnoreCase(coversObj.getCovercode())) {
						eapplyInput.setTpdDecision(coversObj.getCoverdecision());
						eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
						eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
						addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
						addnlTpdCoverDetails.setTpdTransferCoverType(coversObj.getCovercategory());
						addnlTpdCoverDetails.setTpdTransferWeeklyCost(coversObj.getCost());
						if (coversObj.getAdditionalcoveramount() != null) {
							addnlTpdCoverDetails.setTpdTransferCovrAmt(coversObj.getAdditionalcoveramount().toString());
						}
						eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
						if (coversObj.getLoading() != null) {
							eapplyInput.setTpdLoading(coversObj.getLoading().toString());
						}
						if (coversObj.getExistingcoveramount() != null) {
							eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
						}
						eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					}
					if ("IP".equalsIgnoreCase(coversObj.getCovercode())) {
						eapplyInput.setIpDecision(coversObj.getCoverdecision());
						eapplyInput.setIpAuraResons(coversObj.getCoverreason());
						eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
						addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
						addnlIpCoverDetails.setIpTransferCoverType(coversObj.getCovercategory());
						addnlIpCoverDetails.setIpTransferWeeklyCost(coversObj.getCost());
						if (coversObj.getAdditionalcoveramount() != null) {
							addnlIpCoverDetails.setIpTransferCovrAmt(coversObj.getAdditionalcoveramount().toString());
						}
						addnlIpCoverDetails.setAddnlTransferWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
						addnlIpCoverDetails.setAddnlTransferBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
						eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
						if (coversObj.getLoading() != null) {
							eapplyInput.setIpLoading(coversObj.getLoading().toString());
						}
						if (coversObj.getExistingcoveramount() != null) {
							eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
						}
						eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					}
				}

			}
			eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
			eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
			eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		}

		return eapplyInput;
	}

private EapplyInput createNewMemberCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>0){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = (Cover)applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if("DEATH".equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					//addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setNewDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setNewDeathCoverPremium(coversObj.getCost());
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setNewDeathLabelAmt(coversObj.getAdditionalcoveramount().toString());
						}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	//addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setNewTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setNewTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setNewTpdLabelAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            		}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	//addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setNewIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setNewIpLabelAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	addnlIpCoverDetails.setNewWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setNewBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            		}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	return eapplyInput;
}

private List<Cover> createCancelCvCovers(EapplyInput eapplyInput) {
	String deathExUnit = "0";
	String tpdExUnit = "0";
	String ipExUnit = "0";
	String ipExistingAmt = null;
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){
		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			CoverJSON coverJSON = (CoverJSON) iterator.next();
			if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
				deathExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
				tpdExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
				ipExUnit = coverJSON.getUnits();
				ipExistingAmt=coverJSON.getAmount();
			}
			
		}
	}
	
	/*Cover object mapping start*/        	
	
	Cover deathCover=new Cover();
	Cover tpdCover=new Cover();
	Cover ipCover=new Cover();
	List<Cover> coverList=new ArrayList<Cover>();
	
	deathCover.setCreatedate(new Timestamp(new Date().getTime()));
	deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	deathCover.setCovertype("Death");
	deathCover.setCovercode("DEATH");
	deathCover.setCoverdecision(eapplyInput.getDeathDecision());
	deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
	deathCover.setFixedunit(Integer.valueOf(deathExUnit));
	deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
//	deathCover.setAdditionalcoveramount(BigDecimal.ZERO);
	deathCover.setCost("0");
	if(eapplyInput.getExistingDeathAmt()!=null){
		deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
	}
	
	tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
	tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	tpdCover.setCovertype("Total Permanent Disability");
	tpdCover.setCovercode("TPD");
	tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
	tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
	tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
	tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
	tpdCover.setFrequencycosttype("Weekly");
//	tpdCover.setAdditionalcoveramount(BigDecimal.ZERO);
	tpdCover.setCost("0");
	if(eapplyInput.getExistingTpdAmt()!=null){
		tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
	}
		
	if(ipExistingAmt!=null){
		ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
	eapplyInput.setExistingIPAmount(ipExistingAmt);
	}		            
	ipCover.setFixedunit(Integer.valueOf(ipExUnit));
	ipCover.setCreatedate(new Timestamp(new Date().getTime()));
	ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	ipCover.setCovertype("Income Protection");
	ipCover.setCovercode("IP");
	ipCover.setCoverdecision(eapplyInput.getIpDecision());
	ipCover.setCoverreason(eapplyInput.getIpAuraResons());
	ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
	ipCover.setFrequencycosttype("Weekly");
//	ipCover.setAdditionalcoveramount(BigDecimal.ZERO);
	ipCover.setCost("0");
	
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
			CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
			if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
				if(coverJSON.getAmount()!=null){
				deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
				
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
				if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")){/*IP*/
				if(coverJSON.getAmount()!=null){
					ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
			}
			
		}
	}
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	return coverList;
}



private EapplyInput createWorkRatingCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>0){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = (Cover)applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if("DEATH".equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					/*addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
						}*/
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					/*if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}*/
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	/*addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            		}*/
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	/*if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            		}*/
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	/*addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());*/
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	/*if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            		}*/
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	return eapplyInput;
}

}
