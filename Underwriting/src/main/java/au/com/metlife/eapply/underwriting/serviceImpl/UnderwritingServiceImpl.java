package au.com.metlife.eapply.underwriting.serviceImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.drools.KnowledgeBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.xml.sax.SAXException;

import com.rgatp.aura.wsapi.Answer;
import com.rgatp.aura.wsapi.AuraServiceException;
import com.rgatp.aura.wsapi.MessageResource;
import com.rgatp.aura.wsapi.Question;
import com.rgatp.aura.wsapi.Questionnaire;
import com.rgatp.aura.wsapi.Response;

import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;
import au.com.metlife.eapply.underwriting.model.AuraAnswer;
import au.com.metlife.eapply.underwriting.model.AuraDecision;
import au.com.metlife.eapply.underwriting.model.AuraQuestion;
import au.com.metlife.eapply.underwriting.model.AuraQuestionnaire;
import au.com.metlife.eapply.underwriting.model.AuraResponse;
import au.com.metlife.eapply.underwriting.model.AuraSection;
import au.com.metlife.eapply.underwriting.model.AuraSession;
import au.com.metlife.eapply.underwriting.model.PostProcessRuleBO;
import au.com.metlife.eapply.underwriting.model.Quote;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.ItemData;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.underwriting.service.UnderwritingService;
import au.com.metlife.eapply.underwriting.utils.AuraCall;
import au.com.metlife.eapply.underwriting.utils.AuraDroolsHelper;
import au.com.metlife.eapply.underwriting.utils.AuraProcessDecisionHelper;
import au.com.metlife.eapply.underwriting.utils.AuraRulesCacheProvider;
import au.com.metlife.eapply.underwriting.utils.XMLHelper;

@Service("uwService")

@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UnderwritingServiceImpl implements UnderwritingService {

	@Autowired(required = true)
	AuraRulesCacheProvider auraRulesCacheProvider;
	@Override
	public Quote getQuote(Quote input) {
		// TODO Auto-generated method stub
		
		input.setContent("Hello Buddy......");
		return input;
	}
	
	
	

	@Autowired 
	AuraSession auSession;
	
	AuraCall ac= new AuraCall();
//	Questionnaire questionnaire=null;
	int indent = 0;
	int changedBaseQuestion = 0;
	  String auraID="test11-"+new java.util.Date().getTime();   
	    
	    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
	    		+ "<UniqueID>"+auraID+"</UniqueID>"
	    		+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl><QuestionFilters><QuestionFilter>Term-Group</QuestionFilter><QuestionFilter>TPD-Group</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"PVTTest\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"575000.0\" AgeDistrCoverageAmount=\"575000.0\" CoverageAmount=\"575000.0\" FinancialCoverageAmount=\"575000.0\">Term-Group</Product><Product AgeCoverageAmount=\"600000.0\" AgeDistrCoverageAmount=\"600000.0\" CoverageAmount=\"600000.0\" FinancialCoverageAmount=\"600000.0\">TPD-Group</Product></Products><EngineVariables><Variable Name=\"Gender\">Female</Variable><Variable Name=\"Age\">59</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">Yes</Variable><Variable Name=\"Country\">Yes</Variable><Variable Name=\"Salary\">145000</Variable><Variable Name=\"SumInsuredTerm\">575000.00</Variable><Variable Name=\"SumInsuredTPD\">600000.00</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">NSFS</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable><Variable Name=\"SchemeName\">NSFS</Variable><Variable Name=\"FormLength\">TransferCover</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
	    
	    
	    /*String auraID="test11-"+new java.util.Date().getTime();
	    String xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
				+ "<UniqueID>"+auraID+"</UniqueID>"
				+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage>"
				+ "<ExtractLanguage>en</ExtractLanguage> </AuraControl><QuestionFilters><QuestionFilter>TERM-DIRECT</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables>"
				+ " <Variable Name=\"LOB\">Direct</Variable> <Variable Name=\"callCentrePostBackTo\">http://10.173.60.52:9080/CAW2/jsp/public/toall/CallApp/result.jsp</Variable>"
				+ "  <Variable Name=\"callCentrePostErrorsTo\">http://10.173.60.52:9080/CAW2/jsp/public/toall/CallApp/result.jsp</Variable>"
				+ "   <Variable Name=\"callCentreSaveExitPage\">http://10.173.60.52:9080/CAW2/jsp/public/toall/CallApp/result.jsp</Variable>"
				+ "   <Variable Name=\"callCentreIdentifier\">UCMS</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"hjfhjghjg\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\">"
				+ "<Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">TERM-DIRECT</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable>"
				+ "<Variable Name=\"Smoker\">No</Variable><Variable Name=\"Age\">40</Variable><Variable Name=\"Occupation\"/>"
				+ "<Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/>"
						+ "<Variable Name=\"Hours15\"/><Variable Name=\"Country\">Australia</Variable><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable>"
								+ "<Variable Name=\"SumInsuredTPD\">0</Variable><Variable Name=\"SumInsuredTrauma\">0</Variable><Variable Name=\"SumInsuredIP\">0</Variable><Variable Name=\"Partner\">Suncorp</Variable>"
										+ "<Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Suncorp Online</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";*/

	  
	 public AuraDecision submitAuraSession(String clientname, String auraSessionId){
		 Questionnaire qu = null;
		 String auraResponse = null;		
		 ResponseObject responseObject = null;
		 AuraDecision auraDecision = null;	
		 Insured insured = null;
		 PostProcessRuleBO postProcessRuleBO = null;
		 Boolean clientMatch = null;
		 List<String> ratingReason = null;
		 List<String> plannedReason = null;
		 String productOrgDecision = null;
		 Rating rating = null;
		 StringBuffer productdecisionBuffer = new StringBuffer();
		 String transactionType = null;
		 String lob = "Group";
		 String overallDecision = null;
		 try {
			 qu = new Questionnaire();
			 qu.setClientName(clientname);
			 qu.setQuestionnaireId(auraSessionId);		 
			 auraResponse= ac.submit(qu);			
			if (null != auraResponse) {
				responseObject = XMLHelper.prepareResponse(auraResponse);
				if(responseObject!=null){
					auraDecision = new AuraDecision();
					auraDecision.setResponseObject(responseObject);
					String[] reasons;
					//pp logic goes here
					
					for (int itr = 0; itr < responseObject.getClientData().getListExtEngineVar().size(); itr++) {
						ItemData itemData = responseObject.getClientData().getListExtEngineVar().get(itr);
						if("FormLength".equalsIgnoreCase(itemData.getCode())){
							transactionType = itemData.getValue();
						}
					}
					
					for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
						insured = (Insured) responseObject.getClientData().getListInsured().get(itr);				
						
						if (null != insured && null != insured.getListOverallDec()) {
							for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
								Decision decision = (Decision) insured.getListOverallDec().get(insItr);
								
								if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
									
									postProcessRuleBO = new PostProcessRuleBO();
									ratingReason = new ArrayList<String>();
									plannedReason = new ArrayList<String>();
									productOrgDecision= decision.getDecision();
									postProcessRuleBO.setRuleFileName("PostProcessingMaster.xls");
									//Basic Data set Here like LOB, Partner, Product Name
									postProcessRuleBO.setBrand("CARE");
									postProcessRuleBO.setLob("Group");
									postProcessRuleBO.setProduct(decision.getProductName());
									//Step1 fire the rule mater here .
									postProcessRuleBO.setRuleFileName("PostProcessingMaster.xls");									
									
									if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_MASTER)!=null){
										postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_MASTER));
									}
									postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
									if(null!=transactionType && ("WorkRating".equalsIgnoreCase(transactionType)|| "TransferCover".equalsIgnoreCase(transactionType) || "SpecialOffer".equalsIgnoreCase(transactionType))){
										System.out.println("Loop 2");
										clientMatch= Boolean.FALSE;
									}else{
										clientMatch= postProcessRuleBO.getClientMatch();
									}
									//Setting the data for the 2nd Rule
									postProcessRuleBO.setAuraPostProcessID(postProcessRuleBO.getAuraPostProcessID());
									postProcessRuleBO.setOriginalDecision(decision.getDecision());
									postProcessRuleBO.setTotalExlusion(decision.getListExclusion().size());
									postProcessRuleBO.setTotalLExlusion(decision.getLExclusions());
									postProcessRuleBO.setTotalMExlusion(decision.getMExclusions());
									
									//Setting the loadings
									int loadingValue = 0;
									if(null != decision.getTotalDebitsValue()){
										loadingValue = new Double(decision.getTotalDebitsValue()).intValue();
										decision.setOriginalTotalDebitsValue(""+loadingValue);
									}
									postProcessRuleBO.setTotalLoading(loadingValue);
									//Setp2 fire rule 2 here
									if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_1)!=null){
										postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_1));
									}							
									postProcessRuleBO.setRuleFileName("PostProcessing1.xls");
									postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
									
									if(null != postProcessRuleBO.getPostAURADecision()){
										decision.setDecision(postProcessRuleBO.getPostAURADecision());	
										reasons = new String[1];
										reasons[0] = postProcessRuleBO.getReason();
										decision.setReasons(reasons);	
										plannedReason.add(reasons[0]);
									}								
									
									postProcessRuleBO.setPostAURADecision(null);
									if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_2)!=null){
										postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_2));
									}							
									postProcessRuleBO.setRuleFileName("PostProcessing2.xls");
									postProcessRuleBO.setOriginalDecision(decision.getDecision());			
									postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
									
									if(null != postProcessRuleBO.getPostAURADecision()){
										decision.setDecision(postProcessRuleBO.getPostAURADecision());	
										decision.setTotalDebitsReason(postProcessRuleBO.getReason());
										decision.setTotalDebitsValue(postProcessRuleBO.getPostAURALoading().toString());
										reasons = new String[1];
										reasons[0] = postProcessRuleBO.getReason();
										decision.setReasons(reasons);
										if(Double.parseDouble(decision.getTotalDebitsValue())>0.0){
											plannedReason.add(postProcessRuleBO.getReason());
										}										
									}
									productdecisionBuffer.append(decision.getDecision());
									/**
									 * BMI post processing only for institutional now - 05012012 
									 */
									if(("Group".equalsIgnoreCase(lob) && null != decision.getDecision()) || responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
										postProcessRuleBO.setPostAURADecision(null);
										if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_3)!=null){
											postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_3));
										}								
										postProcessRuleBO.setRuleFileName("PostProcessing3.xls");
										postProcessRuleBO.setOriginalDecision(decision.getDecision());
										postProcessRuleBO.setBrand("Care");
										/** 
										 * Loop through the genderAgeBMI tag to get the BMI loadng  
										 */
									List<Decision> bmiList = null;
									/*if(responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
										bmiList = insured.getListGenderAgeBMIRangeTable();
									}else if ("Institutional".equalsIgnoreCase(lob)){
										bmiList = insured.getListGenderAgeBMIDec();
									}*/
									bmiList = insured.getListGenderAgeBMIDec();
									int bmiLoadingValue = 0;
									int bmlMedicalLoading  = 0;
										for (int bmiins = 0; bmiins < bmiList.size(); bmiins++) {
											Decision bmidecision = bmiList.get(bmiins);
											
											if (null != bmidecision && null != bmidecision.getProductName() && null != bmidecision.getTotalDebitsValue() && !"".equalsIgnoreCase(bmidecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(bmidecision.getProductName())) {
												if(null != bmidecision.getTotalDebitsValue()){
													bmiLoadingValue = new Double(bmidecision.getTotalDebitsValue()).intValue();
												}else{
													bmiLoadingValue = 0;
												}
												postProcessRuleBO.setBmiLoading(bmiLoadingValue);
												
												if(new BigDecimal(decision.getTotalDebitsValue()).intValue() >= bmiLoadingValue){
													bmlMedicalLoading = new BigDecimal(decision.getTotalDebitsValue()).subtract(new BigDecimal(bmidecision.getTotalDebitsValue())).intValue();	
												}
												
												//For BMI it should be Medical Loadings only.
												postProcessRuleBO.setTotalExlusion(decision.getMExclusions());
												
												
											}
										} // Endo of BMI LOOP
										int origLoadingValue = 0;
										for (int origins = 0; origins < insured.getListOriginalRuleDec().size(); origins++) {
											Decision origDecision = (Decision) insured.getListOriginalRuleDec().get(origins);
											
											if (null != origDecision && null != origDecision.getProductName() && !"".equalsIgnoreCase(origDecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(origDecision.getProductName())) {
												
												if(null != origDecision.getTotalDebitsValue()){
													origLoadingValue = new Double(origDecision.getTotalDebitsValue()).intValue();
												}else{
													origLoadingValue = 0;
												}
											}
										}
										if(bmlMedicalLoading >= 0){
											postProcessRuleBO.setTotalLoading(bmlMedicalLoading);
											
										}else if(origLoadingValue > 0){
											postProcessRuleBO.setTotalLoading(origLoadingValue);
										}
										postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
										//capturing original decision from Aura
										if("ACC".equalsIgnoreCase(productOrgDecision) ){
											
											if(null!=decision.getListRating() && decision.getListRating().size()>0 ){
												for (int iter=0; iter<decision.getListRating().size(); iter++) {
													 rating = (Rating) decision.getListRating().get(iter);
													if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
															&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
														ratingReason.add(rating.getReason());
													}									
												}
											}
											
											if(null!=decision.getListExclusion() && decision.getListExclusion().size()>0){
												for (Iterator iter = decision.getListExclusion()
														.iterator(); iter.hasNext();) {
													Exclusion exclusion = (Exclusion) iter.next();
													if(null!=exclusion && null!=exclusion.getReason()&& exclusion.getReason().length>0){
														for (int exItr = 0; exItr < exclusion.getReason().length; exItr++) {
															if(null!=exclusion.getReason()[exItr] && exclusion.getReason()[exItr].length()>0 && !"null".equalsIgnoreCase(exclusion.getReason()[exItr].toString())){
																ratingReason.add(exclusion.getReason()[exItr]);
															}
															
														}
													}
													
												}
												
											}
											decision.setOrgReason(ratingReason);
										}else if("DCL".equalsIgnoreCase(productOrgDecision) && null!=decision.getReasons() && decision.getReasons().length>0){
											for (int exItr = 0; exItr < decision.getReasons().length; exItr++) {
												ratingReason.add(decision.getReasons()[exItr]);
											}
											decision.setOrgReason(ratingReason);
										}else if("RUW".equalsIgnoreCase(productOrgDecision)){
											
											for (int iter=0; iter<decision.getListRating().size(); iter++) {
												 rating = (Rating) decision.getListRating().get(iter);
												if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
														&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
													ratingReason.add(rating.getReason());
													
												}									
											}
											decision.setOrgReason(ratingReason);	
										}
										
										if(null != postProcessRuleBO.getPostAURADecision()){
											decision.setDecision(postProcessRuleBO.getPostAURADecision());	
											reasons = new String[1];
											reasons[0] = postProcessRuleBO.getReason();
											decision.setReasons(reasons);
											plannedReason.add(postProcessRuleBO.getReason());
										}
										
										if(null!=plannedReason && plannedReason.size()>0){
											decision.setPlannedReason(plannedReason);
										}										
										 // END of GROUP BMI Loop	
									}									
								
									//end of updation
									if(decision.getProductName().contains("Term")){
										auraDecision.setDeathDecision(decision.getDecision());	
										if(decision.getTotalDebitsValue()!=null && decision.getTotalDebitsValue().trim().length()>0 && !"0.0".equalsIgnoreCase(decision.getTotalDebitsValue().trim())){
											auraDecision.setDeathLoading(decision.getTotalDebitsValue());	
										}										
										auraDecision.setDeathOrigTotalDebitsValue(decision.getOriginalTotalDebitsValue());										
										auraDecision.setDeathExclusions(AuraProcessDecisionHelper.getExclusions(decision));
										if(decision.getDecision().equalsIgnoreCase("DCL")){										
                                    		auraDecision.setDeathAuraResons(AuraProcessDecisionHelper.getAuraDclReason(decision));                                    		
                                    		auraDecision.setDeathResons(AuraProcessDecisionHelper.getReasons(decision));                            		
                                    		
										}
										
									}else if(decision.getProductName().contains("TPD")){
										auraDecision.setTpdDecision(decision.getDecision());
										if(decision.getTotalDebitsValue()!=null && decision.getTotalDebitsValue().trim().length()>0 && !"0.0".equalsIgnoreCase(decision.getTotalDebitsValue().trim())){
											auraDecision.setTpdLoading(decision.getTotalDebitsValue());	
										}										
										auraDecision.setTpdOrigTotalDebitsValue(decision.getOriginalTotalDebitsValue());
										auraDecision.setTpdExclusions(AuraProcessDecisionHelper.getExclusions(decision));
										if(decision.getDecision().equalsIgnoreCase("DCL")){	
											auraDecision.setTpdAuraResons(AuraProcessDecisionHelper.getAuraDclReason(decision));     
											auraDecision.setTpdResons(AuraProcessDecisionHelper.getReasons(decision)); 
										}
										
									}else if(decision.getProductName().contains("IP")){
										auraDecision.setIpDecision(decision.getDecision());
										if(decision.getTotalDebitsValue()!=null && decision.getTotalDebitsValue().trim().length()>0 && !"0.0".equalsIgnoreCase(decision.getTotalDebitsValue().trim())){
											auraDecision.setIpLoading(decision.getTotalDebitsValue());
										}										
										auraDecision.setIpOrigTotalDebitsValue(decision.getOriginalTotalDebitsValue());										
										auraDecision.setIpExclusions(AuraProcessDecisionHelper.getExclusions(decision));
										if(decision.getDecision().equalsIgnoreCase("DCL")){	
											auraDecision.setIpAuraResons(AuraProcessDecisionHelper.getAuraDclReason(decision));     
											auraDecision.setIpResons(AuraProcessDecisionHelper.getReasons(decision));
										}
										 
									}
								}
							}
						}
					}
					if(auraDecision.getDeathDecision()!=null && auraDecision.getTpdDecision()!=null && auraDecision.getIpDecision()!=null){
						if(auraDecision.getDeathDecision().equalsIgnoreCase("DCL") && 
								auraDecision.getTpdDecision().equalsIgnoreCase("DCL") &&
								auraDecision.getIpDecision().equalsIgnoreCase("DCL")){
							auraDecision.setOverallDecision("DCL");
						}else if(auraDecision.getDeathDecision().equalsIgnoreCase("RUW") || 
								auraDecision.getTpdDecision().equalsIgnoreCase("RUW")||
								auraDecision.getIpDecision().equalsIgnoreCase("RUW")){
							auraDecision.setOverallDecision("RUW");
						}else{
							auraDecision.setOverallDecision("ACC");
						}
					}
					//for special term
					if(auraDecision.getDeathLoading()!=null || auraDecision.getDeathExclusions()!=null
							|| auraDecision.getTpdLoading()!=null || auraDecision.getTpdExclusions()!=null
							|| auraDecision.getIpLoading()!=null || auraDecision.getIpExclusions()!=null
							){
						auraDecision.setSpecialTerm(Boolean.TRUE);
					}
					
					/**
					 * Below code is used for the Client Match 
					 */
					if(clientMatch){
						for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
							insured = (Insured) responseObject.getClientData().getListInsured().get(itr);					
							if (null != insured && null != insured.getListOverallDec()) {
								for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
									Decision decision = (Decision) insured.getListOverallDec().get(insItr);
									if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
										productdecisionBuffer.append(decision.getDecision());
									}
								}
							}
						}
						overallDecision = getoverallDecision(productdecisionBuffer);
						auraDecision.setOverallDecision(overallDecision);
						//call eapp service for client match
					}
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AuraServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return auraDecision;
	 }
	 
	 
	 

	public AuraQuestionnaire getAllAuraQuestions(String inputXml) {
		// TODO Auto-generated method stub
		auraID="test3-"+new java.util.Date().getTime();
		System.out.println("Start-- INITAURA()"+new  java.util.Date());		
		System.out.println("inputXml>>"+inputXml);
		auSession.setQuestionnaire(ac.establishNewSessionToAURA(inputXml));		
		
	    System.out.println("END-- AURA INIT()"+new  java.util.Date());
	    return getAllAuraQuestions(auSession.getQuestionnaire());		
		
	}

	public AuraQuestion updateAuraQuestion(AuraResponse res) {
		try{
		// TODO Auto-generated method stub
			
		System.out.println("res.getQuestionID>>"+res.getQuestionID());	
		Question question=(Question)auSession.getQuestionnaire().locateQuestion(res.getQuestionID());
		List<String> answerList = new ArrayList<String>();
		String res1[]= new String[res.getAuraAnswers().size()];
		for(int i=0;i<res.getAuraAnswers().size();i++){
			if(res.getAuraAnswers().get(i)!=null){
				res1[i]=res.getAuraAnswers().get(i).toString().trim();
				answerList.add(res1[i]);			
				System.out.println("ANS-----------"+ res1[i].toString());
			}
			
		}	
		if(res1.length==0){
			res1 =new String[1];
			res1[0] = "";
			System.out.println("ANS-----------"+ res1[0].toString());
			question.respond(res1[0].toString());
			
		}else if(res1.length==1){
			System.out.println("ANS-----------"+ res1[0].toString());
			question.respond(res1[0].toString());
			
		}		
		else
			question.respond(res1);

		Response[] responses = auSession.getQuestionnaire().obtainResponses();// not sure why this is required		       	 
		//auSession.setQuestionnaire(ac.process(auSession.getQuestionnaire()));  
		Questionnaire questionnaire = ac.process(auSession.getQuestionnaire());
		auSession.setQuestionnaire(questionnaire);
		//back and forth issue

		//getAllAuraQuestions(auSession.getQuestionnaire())
		question=(Question)auSession.getQuestionnaire().locateQuestion(res.getQuestionID());
		AuraQuestion q=getQuestion(question);
		
		//temperory.Need to find solution
		//q.setAuraQuestionnare(getAllAuraQuestions(auSession.getQuestionnaire()));	
		if(!q.isBaseQuestion()){
			getparentQuestionObjet(getAllAuraQuestions(auSession.getQuestionnaire()).getQuestions(), res.getQuestionID());
			System.out.println("changedBaseQuestion>>"+changedBaseQuestion);
			question=(Question)auSession.getQuestionnaire().locateQuestion(changedBaseQuestion);
			q.setChangedQuestion(getQuestion(question));
			//q=getQuestion(question);
		}else{
			q.setChangedQuestion(getQuestion(question));
		}
		//question=(Question)auSession.getQuestionnaire().locateQuestion();
		//getparentQuestionObjet(q.getAuraQuestionnare().getQuestions(), res.getQuestionID());	
		
		return q;
		//List<AuraQuestion> questions = getAllAuraQuestions(questionnaire);	
		 
		}catch(Exception e ){
			e.printStackTrace();
			return null;
		}
		
	}
	
	private AuraQuestion getparentQuestionObjet(List<AuraQuestion> auraQuestionList, int subQuestionId){
		AuraQuestion auraQuestion = null;
		int baseQuestionId=0;		
		AuraQuestion baseQuestion = null;
		AuraAnswer auraAnswer = null;
		if(auraQuestionList!=null && auraQuestionList.size()>0){
			for (Iterator iterator = auraQuestionList.iterator(); iterator.hasNext();) {
				auraQuestion = (AuraQuestion) iterator.next();
				if(auraQuestion!=null && auraQuestion.getAuraAnswer().size()>0){
					if(auraQuestion.isBaseQuestion()){	
						changedBaseQuestion=auraQuestion.getQuestionId();
						//baseQuestionId=auraQuestion.getQuestionId();
							 System.out.println("base>>"+auraQuestion.getQuestionId());				
						
					}
					if(auraQuestion.getQuestionId()==subQuestionId){
						 //baseQuestion = auraQuestion;
						/*if(changedBaseQuestion!=0){
							baseQuestionId =changedBaseQuestion;
						}*/	
						 /*System.out.println("baseQuestionId>>"+changedBaseQuestion);
						 break;*/	
						return auraQuestion;
											  
					 }else{
						for (Iterator iterator2 = auraQuestion.getAuraAnswer().iterator(); iterator2.hasNext();) {
							auraAnswer = (AuraAnswer) iterator2.next();
							if(auraAnswer!=null && auraAnswer.getChildQuestions()!=null && auraAnswer.getChildQuestions().size()>0){
								auraQuestion = getparentQuestionObjet(auraAnswer.getChildQuestions(), subQuestionId);								
								if(auraQuestion.getQuestionId()==subQuestionId){
									System.out.println("sub>>"+auraQuestion.getQuestionId());
									System.out.println("baseQuestionIdAub>>"+changedBaseQuestion);
									//changedBaseQuestion= baseQuestionId;
									 //baseQuestion = auraQuestion;
									//break;
									return auraQuestion;
								}
								
							}
							
						}
					}
					
				}
				
			}
		}
		System.out.println("final>>"+changedBaseQuestion);
		return auraQuestion;
	}
	
	
	   
		private AuraQuestionnaire getAllAuraQuestions(Questionnaire questionnaire) {			

			Question[] allQuestions=questionnaire.getQuestions();
			List<AuraQuestion> c_Questions= new ArrayList<AuraQuestion>(allQuestions.length);
			Map<String,String> sectionNameMap = new HashMap<String,String>();
			List<AuraQuestion> auraSectionQuestionList = null;
			AuraSection auraSection = null;
			List<String> sectionList = new ArrayList<>();
			List<AuraSection> auraSectionList = new ArrayList<AuraSection>();
			for(int i=0;i<allQuestions.length;i++){
				AuraQuestion q=getQuestion(allQuestions[i]);
				if(q!=null && q.getQuestionAlias()!=null && !sectionNameMap.containsKey(q.getQuestionAlias())){
					System.out.println("q.getQuestionAlias()>>"+q.getQuestionAlias());					
					sectionNameMap.put(q.getQuestionAlias(),q.getQuestionAlias());
					sectionList.add(q.getQuestionAlias());
				}
				//q.setSessionID(questionnaire.getQuestionnaireId());
				c_Questions.add(q);			
			}
			
			AuraQuestionnaire aq=new AuraQuestionnaire();
			aq.setSessionID(questionnaire.getQuestionnaireId());
			aq.setQuestions(c_Questions);
			//setting section map
			/*for (Map.Entry<String,String> entry : sectionNameMap.entrySet())
			{
				if(entry.getKey()!=null){
					sectionList.add(entry.getKey());
					System.out.println(entry.getKey() + "/" + entry.getValue());
				}
				//System.out.println(entry.getKey() + "/" + entry.getValue());
			}*/
			for (Iterator iterator = sectionList.iterator(); iterator.hasNext();) {
				String sectionName = (String) iterator.next();
				if(sectionName!=null){
					auraSectionQuestionList = new ArrayList<AuraQuestion>();
					for (Iterator iterator2 = c_Questions.iterator(); iterator2.hasNext();) {
						AuraQuestion auraQuestion = (AuraQuestion) iterator2.next();
						if(auraQuestion!=null && auraQuestion.isBaseQuestion() &&  sectionName.equalsIgnoreCase(auraQuestion.getQuestionAlias()) ){							
							auraSectionQuestionList.add(auraQuestion);
						}
					}
					if(auraSectionQuestionList.size()>0){
						auraSection = new AuraSection();
						auraSection.setQuestions(auraSectionQuestionList);
						auraSection.setSectionName(sectionName);
						auraSectionList.add(auraSection);
					}
				}
			}
			if(auraSectionList.size()>0){
				aq.setSections(auraSectionList);
			}
			
			return aq;
			
		}
	

		

		private AuraQuestion getQuestion( Question question){
				
			//System.out.println("In get Question----------");			
			
			AuraQuestion auraQuestion=new AuraQuestion();
			auraQuestion.setClassType(question.getClass().getName().substring(question.getClass().getPackage().getName().length() + 1));
			auraQuestion.setQuestionAlias(question.getQuestionAlias());
			auraQuestion.setQuestionId(question.getQuestionId());
			auraQuestion.setQuestionText(question.getQuestionText());
			auraQuestion.setHelpText(question.getHelpText());
			auraQuestion.setQuestionComplete(question.isComplete());
			auraQuestion.setBaseQuestion(question.isBaseQuestion());
			auraQuestion.setBranchComplete(question.isBranchComplete());
			auraQuestion.setAnswerTest(question.getAnswerText());
			auraQuestion.setVisible(question.isVisible());	
			 if (question.getValidationMessage() != null) {
                 MessageResource messageResource = null;
                 messageResource = question.getValidationMessage();
                 auraQuestion.setValidationMsg(messageResource.getText());
                 auraQuestion.setError(Boolean.TRUE);
			 }else{
				 auraQuestion.setError(Boolean.FALSE);
			 }
			System.out.println("question.getQuestionText()>>"+question.getQuestionText());
			System.out.println("question.getNumber()>>"+question.hashCode());
			if(question.getNumber()!=null){
				if(question.isBaseQuestion()){
					indent = 0;
					auraQuestion.setIndent(indent);					
				}/*else{
					indent= indent+1;
					auraQuestion.setIndent(1);
				}*/
				else if(question.getNumber().equalsIgnoreCase("a)") && !"CheckBoxQuestion".equalsIgnoreCase(auraQuestion.getClassType())){
					auraQuestion.setIndent(1);
				}else if(question.getNumber().equalsIgnoreCase("a)") && "CheckBoxQuestion".equalsIgnoreCase(auraQuestion.getClassType())){
					auraQuestion.setIndent(1);
				}else{
					auraQuestion.setIndent(2);
				}
			}
			//System.out.println("number>>"+question.getNumber());
			Answer ans[]=question.getAnswers();		
			auraQuestion.setArrAns(new ArrayList(ans.length));// This is temp for json
			
		//	System.out.println(auraQuestion.getClassType()+"-----------"+ans.length);
			List<AuraAnswer> c_answers =new ArrayList(ans.length);
						
			//AuraAnswer aurAns[]=new AuraAnswer[ans.length];
			
			for(int i=0;i<ans.length;i++){
				
				AuraAnswer c_ans=new AuraAnswer();			
				c_ans.setAnswerID(question.getQuestionId()+"_"+"A"+i);
				c_ans.setAnswerValue(ans[i].getAnswerValue());
				c_ans.setAnswerText(ans[i].getAnswerText());
				//System.out.println("DDDDDDDDDDDD-1");
				Question childQuestions[]=ans[i].getQuestions();			
				List<AuraQuestion> c_child_Questions= new ArrayList<AuraQuestion>(childQuestions.length);
				//System.out.println("RRRRRRRRRRRRRR-2");
				//AuraQuestion[] childAuraQuestions =new AuraQuestion[childQuestions.length];
				for(int ch=0;ch<childQuestions.length;ch++){
					System.out.println("CH----------" +question.getQuestionId()+"--"+childQuestions[ch].getQuestionText());
					
					
					
					//childAuraQuestions[ch]=getQuestion(childQuestions[ch]);
					c_child_Questions.add(getQuestion(childQuestions[ch]));				
				}
				//System.out.println("RRRRRRRRRRRRRR-3");
				c_ans.setChildQuestions(c_child_Questions);
				//System.out.println("RRRRRRRRRRRRRR-4");
				c_answers.add(c_ans);
				
			}
			
			auraQuestion.setAuraAnswer(c_answers);
			
			return auraQuestion;
		}
		
		private static String getoverallDecision(StringBuffer strBuff){
			final String METHOD_NAME = "getoverallDecision";		
			String decision =null;
			  if(null!=strBuff && !"".equalsIgnoreCase(strBuff.toString())){   
				  if(strBuff.toString().contains("RUW")){
					  decision = "RUW";				
	              }else if(!strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL")){
	            	  decision = "DCL";            	 
	              }else if(strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL")){
	            	  decision="ACC";            	 
	              }else{
	            	  decision="ACC";            	  
	              }
			  }
			  return decision;
		}


}
