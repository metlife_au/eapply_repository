import angular from 'angular';

// Create the module where our functionality can attach to
let quoteModule = angular.module('app.quote', []);

// Include our UI-Router config settings
import QuoteConfig from './quote.config';
quoteModule.config(QuoteConfig);


// Controllers
import QuoteCoverCtrl from './quote.cover.controller';
quoteModule.controller('QuoteCoverCtrl', QuoteCoverCtrl);

import QuoteAuraCtrl from './quote.aura.controller';
quoteModule.controller('QuoteAuraCtrl', QuoteAuraCtrl);

import QuoteSummaryCtrl from './quote.summary.controller';
quoteModule.controller('QuoteSummaryCtrl', QuoteSummaryCtrl);

import QuoteDecisionCtrl from './quote.decision.controller';
quoteModule.controller('QuoteDecisionCtrl', QuoteDecisionCtrl);


export default quoteModule;
