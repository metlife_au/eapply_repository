/* Work Rating Controller,Progressive and Mandatory validations Starts  */  
CareSuperApp.controller('quoteoccupdate',['$scope','$routeParams', '$rootScope','$http', '$location','$timeout','$window','QuoteService', 'CalculateService','OccupationService','NewOccupationService','auraInputService','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','ngDialog','auraResponseService','urlService','$filter','APP_CONSTANTS',
                                          function($scope,$routeParams, $rootScope, $http, $location,$timeout,$window,QuoteService, CalculateService,OccupationService,NewOccupationService,auraInputService,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService ,ngDialog,auraResponseService,urlService,$filter, APP_CONSTANTS){
	
	/* Code for appD starts */
	  var pageTracker = null;
	  if(ADRUM) {
	    pageTracker = new ADRUM.events.VPageView();
	    pageTracker.start();
	  }

	  $scope.$on('$destroy', function() {
	    pageTracker.end();
	    ADRUM.report(pageTracker);
	  });
	  /* Code for appD ends */
	
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
  //$scope.validnumberregEx = '^0[1-8][0-9]{8}';
  //$scope.validAnnualSalaryregEx = '/^[0-9]*$/';
  $scope.phoneNumbrUpdate = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
  $scope.emailFormatUpdate = APP_CONSTANTS.emailFormat;
  $scope.contactTypeOptions = ["Home", "Work", "Mobile"];
  $scope.preferredContactType = '';
  QuoteService.getList($scope.urlList.quoteUrl,"CARE").then(function(res){
  	$scope.IndustryOptions = res.data;
  }, function(err){
  	console.log("Error while fetching industry options " + JSON.stringify(err));
  });
  
  /*Error Flags*/
  $scope.dodFlagErr = null;
  $scope.privacyFlagErr = null;

    $scope.getOccupations = function(){
    	if(!$scope.workRatingIndustry){
    		$scope.workRatingIndustry = '';
    	}
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"CARE",$scope.workRatingIndustry).then(function(res){
    		$scope.OccupationList = res.data;
    	}, function(err){
    		console.log("Error while fetching occupation options " + JSON.stringify(err));
    	});
    };
    
    /*$scope.getOtherOccupationAS = function(entered) {
	    return $http.get('./occupation.json').then(function(response) {
	      $scope.occupationList=[];
        if(response.data.Other) {
	        for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	               obj.name=response.data.Other[key];
                  $scope.occupationList.push(obj.name);
	               
	        }
	      }
        return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });
	    
	  };*/
    
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);    
  	  $location.path( path );
  	};
  	
 // added for session expiry
   /* $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  	/*var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/
    
	$scope.coltwo = false;
  	// $scope.toggleTwo = function() {
   //      $scope.coltwo = !$scope.coltwo; 
   //      $("a[data-target='#collapseTwo']").click();
   //  };
    /* Check if your is allowed to proceed to the next accordion */
    // TBC
    // Need to revisit, need better implementation
    $scope.isCollapsible = function(targetEle, event) {
      if( targetEle == 'accordionprivacy' && !$('#dodLabel').hasClass('active')) {
        event.stopPropagation();
        return false;
      } else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
        event.stopPropagation();
        return false;
      }  else if( targetEle == 'collapseTwo' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
        event.stopPropagation();
        return false;
      }
    }

    $scope.toggleTwo = function(checkFlag) {
        $scope.coltwo = checkFlag;
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };
    
    $scope.privacyCol = false;
    var dodCheck;
    var privacyCheck;
    var dodVal = 0;
    var privacyVal = 0;
    var contactVal = 0;
    var occupationVal = 0;
    $scope.togglePrivacy = function(flag) {
        $scope.privacyCol = flag;
        $("a[data-target='#collapseprivacy']").click();
        
    };
    $scope.toggleContact = function(flag) {
        $scope.contactCol = flag;
        $("a[data-target='#collapseOne']").click();
        
    };
    $scope.checkDodState = function(){
      $timeout(function() {
        $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
        if($('#dodLabel').hasClass('active')) {
          $scope.togglePrivacy(true);
        } else {
          $scope.togglePrivacy(false);
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
        }
      }, 1);
    };
    
    $scope.checkPrivacyState  = function(){
      $timeout(function() {
        $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
        if($('#privacyLabel').hasClass('active')) {
          $scope.toggleContact(true);
        } else {
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
        }
      }, 1);
    };
    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });

    }
    
    
    $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
    $scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
	$scope.ipCoverTransDetails = ipCoverService.getIpCover();
	var fetchAppnum = true;
	var appNum;
	var inputDetails = persoanlDetailService.getMemberDetails();
	var dcWeeklyCost, tpdWeeklyCost, ipWeeklyCost, totalCost;
	var dcCoverAmount, tpdCoverAmount, ipCoverAmount;
	var deathCoverType,tpdCoverType,ipCoverType;
	
	$scope.personalDetails = inputDetails[0].personalDetails;
	$scope.otherOccupationObj = {'workRatingotherOccupation': ''};
	
	if(inputDetails[0] && inputDetails[0].contactDetails.emailAddress){
		$scope.email = inputDetails[0].contactDetails.emailAddress;
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.fundEmailAddress){
		$scope.fundemail = inputDetails[0].contactDetails.fundEmailAddress;
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContactTime){
		if(inputDetails[0].contactDetails.prefContactTime == "1"){
			$scope.time= "Morning (9am - 12pm)";
		}else{
			$scope.time= "Afternoon (12pm - 6pm)";
		}
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContact){
		if(inputDetails[0].contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.updatePhone = inputDetails[0].contactDetails.mobilePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home";
			$scope.updatePhone = inputDetails[0].contactDetails.homePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work";
			$scope.updatePhone = inputDetails[0].contactDetails.workPhone;
		}
   }
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home"){
			$scope.updatePhone = inputDetails[0].contactDetails.homePhone;
		}else if($scope.preferredContactType == "Work"){
			$scope.updatePhone = inputDetails[0].contactDetails.workPhone;
		}else if($scope.preferredContactType == "Mobile"){
			$scope.updatePhone = inputDetails[0].contactDetails.mobilePhone;
		}
	}
	
	if($scope.deathCoverTransDetails.type == '1'){
		deathCoverType = 'DcUnitised';
	} else if($scope.deathCoverTransDetails.type == '2'){
		deathCoverType = 'DcFixed';
	}
	
	if($scope.tpdCoverTransDetails.type == '1'){
		tpdCoverType = 'TPDUnitised';
	} else if($scope.tpdCoverTransDetails.type == '2'){
		tpdCoverType = 'TPDFixed';
	}
	
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    
   //Progressive validation
    var workRatingFormFields = ['workRatingEmail', 'wrkRatingPrefContactNo','workRatingPrefTime'];
    
    while($scope.personalDetails){
		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.genderFlag =  false;
			$scope.gender = '';
			 var workRatingOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','gender','workRatingAnnualSal'];
		     var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','gender','workRatingAnnualSal'];
			    
		} else{
			$scope.genderFlag =  true;
			$scope.gender = $scope.personalDetails.gender;
			 var workRatingOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
		     var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzWrkUpdateQuestion',*/'workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
			    
		}
		break;
	}
    
   /* var workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];
    var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingManagementRole','workRatingAnnualSal'];*/
      $scope.checkPreviousMandatoryFieldsFrWorkRating  = function (workRatingElementName,workRatingFormName){
      	var WorkRatingformFields;
      	if(workRatingFormName == 'workRatingContactForm'){
      		WorkRatingformFields = workRatingFormFields;
    	}else if(workRatingFormName == 'workRatingOccupForm'){
     		//WorkRatingformFields = workRatingOccupationFormFields;
    		if($scope.workRatingoccupation != undefined && $scope.workRatingoccupation == 'Other'){
    			WorkRatingformFields = workRatingOthrOccupationFormFields;
    		} else{
    			WorkRatingformFields = workRatingOccupationFormFields;
    		}
    	}
        var inx = WorkRatingformFields.indexOf(workRatingElementName);
        //console.log(workRatingElementName, inx);
        if(inx > 0){
          for(var i = 0; i < inx ; i++){
            $scope[workRatingFormName][WorkRatingformFields[i]].$touched = true;
          }
        }
      };
      
      $scope.updateAckFlag = false;
 	 var updateAckCheck;
 	  
 	  $scope.checkUpdateAckState = function(){
 	    	$timeout(function(){
 	    		updateAckCheck = $('#ackLabel').hasClass('active');
 	    		if(updateAckCheck){
 	    			$scope.updateAckFlag = false;
 	    		} else{
 	    			$scope.updateAckFlag = true;
 	    		}
 	    	}, 10);
 	    };
   // Validate fields "on continue"
 	   appNum = PersistenceService.getAppNumber();
     $scope.workRatingFormSubmit =  function (form){
        if(!form.$valid){
        //  alert("invalid>>"+$scope["workRatingContactForm"].$invalid);
          form.$submitted=true;
          if(form.$name == 'workRatingContactForm')
            $scope.toggleTwo(false); 
        }else{
        if(form.$name == 'workRatingContactForm'){
            $scope.toggleTwo(true);
        }else if(form.$name == 'workRatingOccupForm'){
        if(fetchAppnum){
          fetchAppnum = false;
          appNum = PersistenceService.getAppNumber();
        }
            $scope.goToAura();
            $rootScope.$broadcast('disablepointer');  
           // $scope.continueWorkRatingPage();
        }
       }
             // console.log("Form Validation");
     };
     
     $scope.invalidSalAmount = false;
	    $scope.checkValidSalary = function(){
	    	if(parseInt($scope.workRatingAnnualSal) == 0){
	  			$scope.invalidSalAmount = true;
	  		} else{
	  			$scope.invalidSalAmount = false;
	  		}
	    };
	 
     $scope.auraDisabled = false;
     $scope.continueWorkRatingPage = function(){
    	 var deathUpgrade = false;
    	 var tpdUpgrade = false;
    	 var ipUpgrade = false;
    	 if($scope.workRatingWithinOffcQue == "Yes"){
				$scope.deathOccupationCategory = "Office";
				if($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes"){
					if(parseInt($scope.workRatingAnnualSal) > 100000){
						$scope.deathOccupationCategory = "Professional";
					}
				} else if($scope.workRatingTertQue == "No" && $scope.workRatingManagementRole == "No"){
					$scope.deathOccupationCategory = "Office";
				}
			} else if($scope.workRatingWithinOffcQue == "No"){
				$scope.deathOccupationCategory = "General";
			}
    	 
    	 if($scope.workRatingWithinOffcQue == "Yes"){
				$scope.tpdOccupationCategory = "Office";
				if($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes"){
					if(parseInt($scope.workRatingAnnualSal) > 100000){
						$scope.tpdOccupationCategory = "Professional";
					}
				} else if($scope.workRatingTertQue == "No" && $scope.workRatingManagementRole == "No"){
					$scope.tpdOccupationCategory = "Office";
				}
			} else if($scope.workRatingWithinOffcQue == "No"){
				$scope.tpdOccupationCategory = "General";
			}
    	 
    	 if($scope.workRatingWithinOffcQue == "Yes"){
				$scope.ipOccupationCategory = "Office";
				if($scope.workRatingTertQue == "Yes" || $scope.workRatingManagementRole == "Yes"){
					if(parseInt($scope.workRatingAnnualSal) > 100000){
						$scope.ipOccupationCategory = "Professional";
					}
				} else if($scope.workRatingTertQue == "No" && $scope.workRatingManagementRole == "No"){
					$scope.ipOccupationCategory = "Office";
				}
			} else if($scope.workRatingWithinOffcQue == "No"){
				$scope.ipOccupationCategory = "General";
			}
    	 
    	 if($scope.deathOccupationCategory == "Professional" && ($scope.deathCoverTransDetails.occRating == "General" || $scope.deathCoverTransDetails.occRating == "Office")){
    		 deathUpgrade = true;
    	 } else if($scope.deathOccupationCategory == "Office" && $scope.deathCoverTransDetails.occRating == "General"){
    		 deathUpgrade = true;
    	 }
    	 
    	 if($scope.tpdOccupationCategory == "Professional" && ($scope.tpdCoverTransDetails.occRating == "General" || $scope.tpdCoverTransDetails.occRating == "Office")){
    		 tpdUpgrade = true;
    	 } else if($scope.tpdOccupationCategory == "Office" && $scope.tpdCoverTransDetails.occRating == "General"){
    		 tpdUpgrade = true;
    	 }
    	 
    	 if($scope.ipOccupationCategory == "Professional" && ($scope.ipCoverTransDetails.occRating == "General" || $scope.ipCoverTransDetails.occRating == "Office")){
    		 ipUpgrade = true;
    	 } else if($scope.ipOccupationCategory == "Office" && $scope.ipCoverTransDetails.occRating == "General"){
    		 ipUpgrade = true;
    	 }
    	 
    	 var ruleModel = {
        		"age": anb,
        		"fundCode": "CARE",
        		"gender": $scope.gender,
        		"deathOccCategory": $scope.deathOccupationCategory,
        		"tpdOccCategory": $scope.tpdOccupationCategory,
        		"ipOccCategory": $scope.ipOccupationCategory,
        		"smoker": false,
        		"deathFixedCost": null,
        		"deathUnitsCost": null,
        		"tpdFixedCost": null,
        		"tpdUnitsCost": null,
        		"ipUnits": parseInt($scope.ipCoverTransDetails.units),
        		"ipFixedCost": null,
        		"ipUnitsCost": null,
        		"premiumFrequency": "Weekly",
        		"memberType": null,
        		"manageType": "UWCOVER",
        		"ipCoverType": "IpUnitised",
        		"ipWaitingPeriod": $scope.ipCoverTransDetails.waitingPeriod,
        		"ipBenefitPeriod": $scope.ipCoverTransDetails.benefitPeriod
        	};
		if($scope.deathCoverTransDetails.type == '1'){
			ruleModel.deathCoverType = 'DcUnitised';
			ruleModel.deathUnits = parseInt($scope.deathCoverTransDetails.units);
		} else if($scope.deathCoverTransDetails.type == '2'){
			ruleModel.deathCoverType = 'DcFixed';
			ruleModel.deathFixedAmount =  parseInt($scope.deathCoverTransDetails.amount);
			ruleModel.deathExistingAmount = parseInt($scope.deathCoverTransDetails.amount);
		}
		
		if($scope.tpdCoverTransDetails.type == '1'){
			ruleModel.tpdCoverType = 'TPDUnitised';
			ruleModel.tpdUnits = parseInt($scope.tpdCoverTransDetails.units);
		} else if($scope.tpdCoverTransDetails.type == '2'){
			ruleModel.tpdCoverType = 'TPDFixed';
			ruleModel.tpdFixedAmount =  parseInt($scope.tpdCoverTransDetails.amount);
			ruleModel.tpdExistingAmount = parseInt($scope.tpdCoverTransDetails.amount);
		}
		
		CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
			var premium = res.data;
			for(var i = 0; i < premium.length; i++){
    			if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
    				dcWeeklyCost = premium[i].cost;
    				dcCoverAmount = premium[i].coverAmount;
    			} else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
    				tpdWeeklyCost = premium[i].cost;
    				tpdCoverAmount = premium[i].coverAmount;
    			} else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
    				ipWeeklyCost = premium[i].cost;
    				ipCoverAmount = premium[i].coverAmount;
    			}
    		}
			totalCost = parseFloat(dcWeeklyCost) + parseFloat(tpdWeeklyCost) + parseFloat(ipWeeklyCost);
			
			if(deathUpgrade || tpdUpgrade || ipUpgrade){ 
  				$scope.auraDisabled = false;
  				$scope.saveDataForPersistence();
      	  		$location.path('/auraocc/1');
      		}else{
      			$scope.auraDisabled = true;
      			$scope.saveDataForPersistence();
      			$location.path('/workRatingSummary');
      		}
		}, function(err){
			console.info("Error while calculating premium " + JSON.stringify(err));
		});
	};
	
	 
	$scope.saveDataForPersistence = function(){
		$scope.personalDetails = persoanlDetailService.getMemberDetails()[0].personalDetails;
    	var coverObj = {};
    	var occObj={};
    	var deathCoverObj = {}, tpdCoverObj = {}, ipCoverObj = {};
    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
    		return $scope.workRatingIndustry == obj.key;
    	});
    	
    	coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
    	coverObj['email'] = $scope.email;
    	occObj['gender'] = $scope.gender;
    	coverObj['contactType']=$scope.preferredContactType;
    	coverObj['contactPhone'] = $scope.updatePhone;
    	coverObj['contactPrefTime'] = $scope.time;
    	
    	coverObj['deathOccCategory'] = $scope.deathOccupationCategory;
    	coverObj['tpdOccCategory'] = $scope.tpdOccupationCategory;
    	coverObj['ipOccCategory'] = $scope.ipOccupationCategory;
    	coverObj['auraDisabled'] = $scope.auraDisabled;
    	
    	occObj['fifteenHr'] = $scope.fifteenHrsQuestion;
    	occObj['industryName'] = selectedIndustry.length ? selectedIndustry[0].value : '';
    	occObj['industryCode'] = selectedIndustry.length ? selectedIndustry[0].key : '';
    	occObj['occupation'] = $scope.workRatingoccupation;
    	occObj['withinOfficeQue']= $scope.workRatingWithinOffcQue;
    	occObj['tertiaryQue']= $scope.workRatingTertQue;
    	occObj['managementRoleQue']= $scope.workRatingManagementRole;
    	occObj['salary'] = $scope.workRatingAnnualSal;
    	occObj['otherOccupation'] = $scope.otherOccupationObj.workRatingotherOccupation;
    	/*occObj['citizenQue'] = $scope.areyouperCitzWrkUpdateQuestion;*/
    	
    	coverObj['deathAmt'] = parseFloat($scope.deathCoverTransDetails.amount);
    	coverObj['tpdAmt'] = parseFloat($scope.tpdCoverTransDetails.amount);
    	coverObj['ipAmt'] = parseFloat($scope.ipCoverTransDetails.amount);
    	coverObj['waitingPeriod'] = $scope.ipCoverTransDetails.waitingPeriod;
    	coverObj['benefitPeriod'] = $scope.ipCoverTransDetails.benefitPeriod;
    	coverObj['appNum'] = appNum;
    	coverObj['dodCheck'] = dodCheck;
    	coverObj['privacyCheck'] = privacyCheck;
    	coverObj['ackCheckbox'] = updateAckCheck;
    	coverObj['lastSavedOn'] = 'QuoteUpdatePage';
    	coverObj['age'] = anb;
        coverObj['manageType'] = 'UWCOVER';
        coverObj['partnerCode'] = 'CARE';
        coverObj['totalPremium'] = parseFloat(totalCost);
        coverObj['deathCoverPremium'] = parseFloat(dcWeeklyCost);
        coverObj['tpdCoverPremium'] = parseFloat(tpdWeeklyCost);
        coverObj['ipCoverPremium'] = parseFloat(ipWeeklyCost);
        coverObj['deathNewAmt'] = parseFloat(dcCoverAmount);
        coverObj['tpdNewAmt'] = parseFloat(tpdCoverAmount);
        coverObj['ipNewAmt'] = parseFloat(ipCoverAmount);
        coverObj['deathCoverType'] = deathCoverType;
    	coverObj['tpdCoverType'] = tpdCoverType;
		coverObj['ipCoverType'] = 'IpUnitised';
		coverObj['freqCostType'] = 'Weekly';
        			
    	PersistenceService.setworkRatingCoverDetails(coverObj);
    	PersistenceService.setWorkRatingCoverOccDetails(occObj);
    };
    
    $scope.goToAura = function (){
    //	$scope.saveDataForPersistence();
    	if(this.workRatingContactForm.$valid && this.workRatingOccupForm.$valid){
	    	$timeout(function(){
				updateAckCheck = $('#ackLabel').hasClass('active');
			   if(updateAckCheck){
					$scope.updateAckFlag = false;
			    	$scope.continueWorkRatingPage();
			   }else{
			  	   $scope.updateAckFlag = true;
			   }
	  	    }, 10);
    	}
    };
    
  $scope.clickToOpen = function (hhText) {
      	
		var dialog = ngDialog.open({
			/*template: '<p>'+hhText+'</p>' +
				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
				template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hint</h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="">Close</button></div></div>',
				className: 'ngdialog-theme-plain',
				plain: true
		});
		dialog.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
	
    if($routeParams.mode == 2){
    	var existingWorkRatingDetails = PersistenceService.getworkRatingCoverDetails();
    	var workRatingOccDetails=PersistenceService.getWorkRatingCoverOccDetails();
    	
    	if(!existingWorkRatingDetails || !workRatingOccDetails) {
        $location.path("/quoteoccchange/1");
        return false;
      }

    	$scope.email = existingWorkRatingDetails.email;
    	$scope.preferredContactType=existingWorkRatingDetails.contactType;
    	$scope.updatePhone = existingWorkRatingDetails.contactPhone;
    	$scope.time = existingWorkRatingDetails.contactPrefTime;
    	$scope.auraDisabled = existingWorkRatingDetails.auraDisabled;
    	
    	$scope.fifteenHrsQuestion = workRatingOccDetails.fifteenHr;
    	/*$scope.areyouperCitzWrkUpdateQuestion = workRatingOccDetails.citizenQue;*/
    	$scope.workRatingIndustry = workRatingOccDetails.industryCode;
    	//$scope.workRatingoccupation = workRatingOccDetails.occupation;
    	$scope.workRatingWithinOffcQue = workRatingOccDetails.withinOfficeQue;
    	$scope.workRatingTertQue = workRatingOccDetails.tertiaryQue;
    	$scope.workRatingManagementRole = workRatingOccDetails.managementRoleQue;
    	$scope.gender = workRatingOccDetails.gender;
    	$scope.workRatingAnnualSal = workRatingOccDetails.salary;
    	$scope.otherOccupationObj.workRatingotherOccupation = workRatingOccDetails.otherOccupation;
    	
    	appNum = existingWorkRatingDetails.appNum;
        dodCheck = existingWorkRatingDetails.dodCheck;
        privacyCheck = existingWorkRatingDetails.privacyCheck;
    	updateAckCheck = existingWorkRatingDetails.ackCheckbox;
    	
    	if(updateAckCheck){
    	    $timeout(function(){
    			$('#ackLabel').addClass('active');
    			   });
    			 }

      $('#dodLabel').addClass('active');
      $('#privacyLabel').addClass('active');

    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"CARE",$scope.workRatingIndustry).then(function(res){
    	//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.workRatingIndustry}, function(occupationList){
        	$scope.OccupationList = res.data;
        	var temp = $scope.OccupationList.filter(function(obj){
        		return obj.occupationName == workRatingOccDetails.occupation;
        	});
        	$scope.workRatingoccupation = temp[0].occupationName;
        }, function(err){
        	console.log("Error while fetching occupation options " + JSON.stringify(err));
        });
    	$scope.togglePrivacy(true);
    	$scope.toggleContact(true);
    	$scope.toggleTwo(true);
    }
    
}]);
  