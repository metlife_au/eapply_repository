package au.com.metlife.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.dto.DocumentDto;
import au.com.metlife.exception.MetlifeException;
import au.com.metlife.model.ClaimOutput;
import au.com.metlife.model.Document;
import au.com.metlife.model.UserInfoBO;
import au.com.metlife.pics.webserivce.DoumentUploadHelper;
import au.com.metlife.service.CTService;
import au.com.metlife.webservices.PicsLodgementStub;


@RestController
@Component
@PropertySource(value = "application.properties")
public class CTController {
	

	
	 private final CTService ctService;

	  @Autowired
	    public CTController(final CTService ctService) {
	        this.ctService = ctService;
	    }
	
	
	
	 
	 @RequestMapping(value = "/saveDocument", method = RequestMethod.GET)
	  public void saveDocument() {		
		//b2bService.generateToken(eappstring.getSecurestring());
		 System.out.println("saveDocument");
		 Document document = new Document();
		 document.setPolicyNum("123456");
		 document.setDocumentCode("description");
		 document.setDocumentlocation("test");
		 document.setUuId("123456");
		 //ctService.saveDocumentList(documentDto); 
	 }
	
	@RequestMapping(value = "/submitFiles", method = RequestMethod.POST)
    @ResponseBody
    public  void submitFiles(@RequestBody List<DocumentDto> list) throws IllegalStateException, IOException, JSONException {
		Document document = null;
		 //List<Document> docList = null;
		//HttpSession session = request.getSession(true);
		// docList = (ArrayList)session.getAttribute("docList");		
        System.out.println("submitFiles contrler");
//	        ObjectMapper mapper = new ObjectMapper();
//	        JsonNode node = mapper.readTree(str);
//	        DocumentList docList = mapper.convertValue(node.get("docList"), DocumentList.class);
        if (null != list && list.size()>0) {
        	ctService.saveDocumentList(list); 
        	UserInfoBO userInfo = new UserInfoBO();
    		userInfo.setFundType("SFPS");
    		//userInfo.setId("345678");
    		userInfo.setFirstname("JUSTIN SHANE");
    		userInfo.setLastname("LUCAS");
    		//userInfo.setTitle("Mr");
    		//userInfo.setGender("female");
    		userInfo.setDateOfBirth(new Date("18/04/1978"));
    		//userInfo.setClientRefNumber("9970");
    		//userInfo.setDateJoinedFund(new Date());
    		//userInfo.setAdditionalInfo("test");
    		userInfo.setApplicationId1("41490");
    		
    		for (DocumentDto docDto :list) {		
    		//DocumentDto claimDocument=new DocumentDto();			
    		docDto.setDocumentlocation("/shared/eLodg/4485801_25022016_173948.pdf");
    		//claimDocument.setDocumentCode("Birth Certificate");
    	}
    		Vector claimDocumentVector = new Vector(list);
    		userInfo.setElodgeDocument(claimDocumentVector);
    		try {
    		DoumentUploadHelper  docHelper = new DoumentUploadHelper();
    		PicsLodgementStub.ReturnObject retObj = docHelper.doLodgeExistingClaims(userInfo, "", "", "", "GL", "");
    		System.out.println(retObj.getCodeDescription());
    		System.out.println(retObj.getCode());
    	} catch (MetlifeException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 }
        
        

//		
//		claimDocument.setDocumentStatus("Pending");
//		claimDocumentVector.add(claimDocument);
//		userInfo.setElodgeDocument(claimDocumentVector);
//		//retObj = doLodgeNewClaims(userInfo,"","","Monica Louise BATES");eNewClaims(userInfo, email, phoneNumber, fullName)		
//		try {
//			PicsLodgementStub.ReturnObject retObj = DoumentUploadHelper.doLodgeExistingClaims(userInfo, "", "", "", "SCI", "");
//			System.out.println(retObj.getCodeDescription());
//			System.out.println(retObj.getCode());
//		} catch (MetlifeException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
	}
	@RequestMapping(value = "/initialFiles/{claimNo}", method = RequestMethod.GET)
    @ResponseBody
    public  void initialFiles(@PathVariable(value="claimNo") String claimNo,HttpServletResponse response) throws IllegalStateException, IOException, JSONException {
		
		JSONArray jsonArray = null;
		if (null != claimNo){
			List<Document> docList = ctService.retriveDocList(claimNo);
			if (null != docList) {
			    jsonArray = new JSONArray();
				for (Document doc : docList) {
					JSONObject json = new JSONObject();
					json.put("name", doc.getFileName());
					json.put("size", doc.getFileSize());
					json.put("id", doc.getFileId());
					json.put("uuid", doc.getUuId());
					json.put("description", doc.getDocumentCode());	
					jsonArray.put(json);
				}
			}
		}
		
//		JSONObject json1 = new JSONObject();
//		 
//         json1.put("name", "53290R_Life_PrivacyStatement_FA.PDF");
//         json1.put("size", "262497");
//         json1.put("id", "0");
//         json1.put("uuid", "9a166683-b2b9-4fac-b5b7-823b874d897a");
//         JSONArray jsonArray = new JSONArray();
//         jsonArray.put(json1);
         response.setCharacterEncoding("UTF-8");
         response.setContentType("text");
         response.getWriter().print(jsonArray);
         response.flushBuffer();
	}
	
}