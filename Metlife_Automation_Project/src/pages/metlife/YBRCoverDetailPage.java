package pages.metlife;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.YBRCoverDetailPageObjects;

public class YBRCoverDetailPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRCoverDetailPage webresponse() {
		webresponseinput();
		return new YBRCoverDetailPage(scriptHelper);
	}

	public YBRCoverDetailPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void webresponseinput() {
	
		String value = dataTable.getData("MetLife_Data", "InputXML");
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			sleep(4000);
			WebElement elmName = driver.findElement(By.id("input_string_id"));
			elmName.clear();
			elmName.sendKeys(value);
			tryAction(fluentWaitElement(YBRCoverDetailPageObjects.btnNext), "Click", "Apply");
			sleep(4000);
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
