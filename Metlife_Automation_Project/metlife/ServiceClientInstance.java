package pages.metlife;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

//import com.chordiant.core.configuration.ConfigurationHelper;


/**
* This is the singleton class to get the client stub to call the webservice
* 
 * @D
* @author bannamreddy
*
*/
public class ServiceClientInstance {
      
      
      private static String  clientConfig = "C:\\eappConfiguration\\client_config"; //clientConfig"C:\\eappConfiguration\\client_config"; 
      private static ConfigurationContext ctx =null;  
      private static ServiceClientInstance serviceClientInstance = null;
      
      
      private ServiceClientInstance()
        {
          // no code req'd
        }

      
      public static synchronized ConfigurationContext getInstance() throws Exception
      {
            if(ctx ==  null && serviceClientInstance == null)
            {
                  
                  serviceClientInstance = new ServiceClientInstance();
                  ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(clientConfig, null);
            }
            return ctx;
      }
      
      /**
      * This API is required to suppor the singleton class, so that others can't clone the 
       * Object and create multiple instance. Hence overrinding the Clone API from the 
       * Object class.
      */
      public Object clone()throws CloneNotSupportedException {
            throw new CloneNotSupportedException(); 
   
  }
}
