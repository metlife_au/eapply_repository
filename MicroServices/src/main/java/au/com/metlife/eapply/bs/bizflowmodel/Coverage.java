package au.com.metlife.eapply.bs.bizflowmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Coverage")

@XmlRootElement
public class Coverage {
	
	private String TypeCode;
	private String CoverageStatusCode;
	private List<Limit> Limit;
	private PolicyInsuredReferences PolicyInsuredReferences;
	private Double ItemAmount;
	private List<CoverageStatusReasonList> CoverageStatusReasonList;
	private List<ExclusionsListType> ExclusionsListType;
	private List<LoadingListType> LoadingListType;
	public String getCoverageStatusCode() {
		return CoverageStatusCode;
	}
	public void setCoverageStatusCode(String coverageStatusCode) {
		CoverageStatusCode = coverageStatusCode;
	}
	public List<CoverageStatusReasonList> getCoverageStatusReasonList() {
		return CoverageStatusReasonList;
	}
	public void setCoverageStatusReasonList(
			List<CoverageStatusReasonList> coverageStatusReasonList) {
		CoverageStatusReasonList = coverageStatusReasonList;
	}
	public List<ExclusionsListType> getExclusionsListType() {
		return ExclusionsListType;
	}
	public void setExclusionsListType(List<ExclusionsListType> exclusionsListType) {
		ExclusionsListType = exclusionsListType;
	}
	public Double getItemAmount() {
		return ItemAmount;
	}
	public void setItemAmount(Double itemAmount) {
		ItemAmount = itemAmount;
	}
	public List<Limit> getLimit() {
		return Limit;
	}
	public void setLimit(List<Limit> limit) {
		Limit = limit;
	}
	public List<LoadingListType> getLoadingListType() {
		return LoadingListType;
	}
	public void setLoadingListType(List<LoadingListType> loadingListType) {
		LoadingListType = loadingListType;
	}
	public PolicyInsuredReferences getPolicyInsuredReferences() {
		return PolicyInsuredReferences;
	}
	public void setPolicyInsuredReferences(
			PolicyInsuredReferences policyInsuredReferences) {
		PolicyInsuredReferences = policyInsuredReferences;
	}
	public String getTypeCode() {
		return TypeCode;
	}
	public void setTypeCode(String typeCode) {
		TypeCode = typeCode;
	}
	
	
	
	
	
	

	
}
