package au.com.metlife.eapply.bs.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.constants.MetLifeEmailConstants;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Document;



/* -------------------------------------------------------------------------------------------------    
 * Description:   This Service is a Singleton Java implementation. And will be used for sending
 * emails from MetLife application. Prior to using this service the
 * MetLifeEmailConfig file needs to be configured with smtp server,Email user
 * id and password information. User will get the instance of this class and
 * will call sendEmail() method and pass CashLinxEmailVO as parameter
 * -------------------------------------------------------------------------------------------------
 * Copyright @ 2009 MetLife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :    khirod.panda
 * Created:     April 22,2009
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * Date(MM/DD/YYYY)  july 03,2009               Author    anand.kishore                                Description   code refactoring  
 * Date(MM/DD/YYYY)  October 27,2009               Author    anand.kishore                                Description   adding functionality to institutional    
 *          
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
public class MetLifeEmailService {
	private static final Logger log = LoggerFactory.getLogger(MetLifeEmailService.class);
	/* package name*/
	/* private static final String PACKAGE_NAME = "com.metlife.eapplication.email";*/
	 /*class name*/
	/* private static final String CLASS_NAME = "MetLifeEmailService";*/
	/* name for met email service */
	private static MetLifeEmailService eService = null;
	/*properties name*/
	Properties props = null;
	/* boolean debug*/
	private boolean debug = false;	
	/*email user name*/
	private String emailUserName = null;
	/*email password*/
	private String emailPw = null;
	/*sender mail id*/
	/*private String sender_mailID = null;*/
	/*constructor for MetLifeEmailService*/
	
	public static final int ZERO_SIZE = 0;
	private MetLifeEmailService(){
	
	}

	public static MetLifeEmailService getInstance(){
		if (eService == null)
			try {
				eService = new MetLifeEmailService();
			} catch (Exception e) {
				log.error("Error in get instance {}",e);
			}
		return eService;
	}
	/**
     * Description: This method instantiates the email properties from the config file
     * 
     * @param 
     * 
     * @return void
     */
	 private void initEmailProperties()
     {
              //Do nothing as written for inialize email properties
	     
     }

	 /**
     * Description: This method can be called for sending emails from MetLife application.
	 * The input parameter would be MetLifeEmailVO. The value object needs to
	 * be set with sender,recipient,subject and a valid email template name.
	 * Prior to sending email the template file needs to populated and kept in
	 * template location
     * 
     * @param 
     * 
     * @return void
     */
	public boolean sendMail(MetLifeEmailVO eVO,String pdfAttachDir) throws SendFailedException{
		log.info("Sending mail start");
	 
	   
		String smtp_host_name = null;
	    /** Email Integration start  */
	    props = new Properties();
	    smtp_host_name = (String)eVO.getEmailDataElementMap().get("EMAIL_SMTP_HOST");
	    
	    props.put(MetLifeEmailConstants.PROP_MAIL_SMTP_HOST, smtp_host_name);
		props.put(MetLifeEmailConstants.PROP_MAIL_SMTP_AUTH,MetLifeEmailConstants.EMAIL_TRUE);
		props.put(MetLifeEmailConstants.PROP_MAIL_SMTP_STARTTTLS_ENABLE,MetLifeEmailConstants.EMAIL_TRUE);
	    	
	    /** Email Integration End  */  
	    
	    InternetAddress iAddress = null;
	    try
	    {	
	    	/*String fileAttachment = getUploadPath();*/
	    	String fileName = null;
	    	if(null!=pdfAttachDir){
	    		fileName = pdfAttachDir.substring(pdfAttachDir.lastIndexOf('/')+1);
	    	}
	    	
	        Authenticator auth = new SMTPAuthenticator();
	        
	        SecurityManager securityManager = System.getSecurityManager();
	        Session session = null;
	        
	        if(securityManager == null){	        	  
	        	  session = Session.getInstance(props, auth);
	        	
	        }else{	        	
	        	session = Session.getDefaultInstance(props, auth);
	        }
	        
	        session.setDebug(debug);
	        Message msg = new MimeMessage(session);	 
	        if(null != eVO.getFromSender()){
	        	 iAddress = eVO.getFromSender();
	        }
	        msg.setFrom(iAddress);
	        /*msg.addRecipients(javax.mail.Message.RecipientType.TO, eVO.getToRecipents());*/
	        log.info("eVO.getToRecipents()>> {}",eVO.getToRecipents());
	        for (int i = 0; i < eVO.getToRecipents().length; i++) {
	        	log.info("eVO.getToRecipents()[i].getAddress()>> {}",eVO.getToRecipents()[i].getAddress());
			}
	       msg.setRecipients(javax.mail.Message.RecipientType.TO, eVO.getToRecipents());
	        /*msg.setRecipients(javax.mail.Message.RecipientType.CC, eVO.getCcReciepent());*/
	        if(null!=eVO.getCcReciepent() && eVO.getCcReciepent().length()>ZERO_SIZE){
	        	 msg.addRecipient(javax.mail.Message.RecipientType.CC, new InternetAddress(eVO.getCcReciepent()));
	        }
	        if(null!=eVO.getBccReciepent() && eVO.getBccReciepent().length()>ZERO_SIZE){
	        	log.info("eVO.getBccReciepent()>> {}",eVO.getBccReciepent());
	        	msg.addRecipient(javax.mail.Message.RecipientType.BCC, new InternetAddress(eVO.getBccReciepent()));
	        }     
	       
	        msg.setSubject(getMessageSubject(eVO));   
	        /*String datasource = null;*/
	        
	       
	        log.info("pdfAttachDir : {}",pdfAttachDir);
	        if(null!=pdfAttachDir){
	        	MimeBodyPart messageBodyPart = new MimeBodyPart();
	        	String emailContent=getMessage(eVO);

	        	if(eVO.isHtmlEmails() || (emailContent!=null && emailContent.contains("html") /*&& emailContent.contains("CARE")*/)){

	    	        messageBodyPart.setContent(emailContent, MetlifeInstitutionalConstants.EMAILCONTENT);
			        /*messageBodyPart.setHeader("Content-Type","text/html; charset=\"utf-8\"");*/
	        	}else{
	        		messageBodyPart.setText(emailContent);
	        	}
		        

		        Multipart multipart = new MimeMultipart();
		        multipart.addBodyPart(messageBodyPart);	
		        
		        messageBodyPart = new MimeBodyPart();	        	 
	        	if(!eVO.getIsAgentEmailReq()){
	        		FileDataSource source = new FileDataSource(pdfAttachDir);
			        messageBodyPart.setDataHandler(new DataHandler(source));
			        messageBodyPart.setFileName(fileName);
	        	}
	        	multipart.addBodyPart(messageBodyPart);		
	        	log.info("inside send mail");
	        	
	        	 if(null != eVO.getDocumentList() && !eVO.getDocumentList().isEmpty()){
	        		 for(Document documentDTO: (List<Document>) eVO.getDocumentList()) {
	        			 	if(documentDTO.getDocLoc()!=null){
	        			 		if(null!=documentDTO.getDocLoc() && documentDTO.getDocLoc().indexOf('/')!=-1){
	        			    		fileName = documentDTO.getDocLoc().substring(documentDTO.getDocLoc().lastIndexOf('/')+1);
	        			    	}
	        	    		 	MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			        			FileDataSource source = new FileDataSource(documentDTO.getDocLoc());				        	
						        messageBodyPart2.setDataHandler(new DataHandler(source));
						        messageBodyPart2.setFileName(fileName);	       	        
						        multipart.addBodyPart(messageBodyPart2);
	        			 	}
	        		 }
			     } 	
		        if(!eVO.getIsAgentEmailReq()){
		        		msg.setContent(multipart);	
		        }else{
		        	emailContent=getMessage(eVO);

		        	if(eVO.isHtmlEmails() || (emailContent!=null && emailContent.contains("html")/* && emailContent.contains("CARE")*/)){

		        		msg.setContent(emailContent,MetlifeInstitutionalConstants.EMAILCONTENT);
		        	}else{
		        		msg.setText(emailContent);
		        	}
		        }		        
		      
	        }else{ 	        	
	        	String emailContent=getMessage(eVO);

	        	if(eVO.isHtmlEmails() || (emailContent!=null && emailContent.contains("html")/*&& emailContent.contains("CARE")*/)){

	        		msg.setContent(emailContent,MetlifeInstitutionalConstants.EMAILCONTENT);
	        	}else{
	        		msg.setText(emailContent);
	        	}
	        }        
	        
	        /*Commented only for the local purpose.*/
	        log.info("msg>> {}",msg);
	        Transport.send(msg);
	    }
	    catch(SendFailedException sFe)
	    {
	    	log.error("Error in mail send-SendFailedException : {}",sFe);
	    	throw sFe;
	    }
	    catch(Exception e)
	    {
	    	log.error("Error in mail send : {}",e);
	    }
	    log.info("Sending mail finish");
	    return true;
	}


	/**
	 * This method will be used to compose the message using the template and
	 * input parameter hashmap
	 * 
	 * @param eVO
	 * @return String
	 * 
	 */
	private String getMessage(MetLifeEmailVO eVO) {
		log.info("Getting message start");
		String emailBody = null;
		
		if(null != eVO && null != eVO.getEmailTemplateId() && !"".equalsIgnoreCase(eVO.getEmailTemplateId())){
			emailBody = eVO.getEmailTemplateId();
			
			
			/*Replace the application number in the body of the email*/
			if(emailBody.contains(MetlifeInstitutionalConstants.APPLICATIONNUMBER)){
				emailBody = emailBody.replace(MetlifeInstitutionalConstants.APPLICATIONNUMBER, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.APPLICATION_NUMBER));
			}
			if(emailBody.contains("<%PRODUCTNAME%>")){
				emailBody = emailBody.replace("<%PRODUCTNAME%>", (String)eVO.getEmailDataElementMap().get("PRODUCTNAME"));
			}
			if(emailBody.contains("<%TEMPLATE_PASSWORD%>")){
				emailBody = emailBody.replace("<%TEMPLATE_PASSWORD%>", (String)eVO.getEmailDataElementMap().get("TEMPLATE_PASSWORD"));
			}
			if(emailBody.contains(MetlifeInstitutionalConstants.EMAIL_TITLE)){
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.TITLE_NAME)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_TITLE, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.TITLE_NAME));	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_TITLE, " ");
				}
			}
			if(emailBody.contains(MetlifeInstitutionalConstants.FIRSTNAME)){
				emailBody = emailBody.replace(MetlifeInstitutionalConstants.FIRSTNAME, (String)eVO.getEmailDataElementMap().get("FIRSTNAME"));
			}
			if(emailBody.contains(MetlifeInstitutionalConstants.LASTNAME)){
				emailBody = emailBody.replace(MetlifeInstitutionalConstants.LASTNAME, (String)eVO.getEmailDataElementMap().get("LASTNAME"));
			}
			if(emailBody.contains("<%UW_DECISION_BLURB%>")){
				emailBody = emailBody.replace("<%UW_DECISION_BLURB%>", (String)eVO.getEmailDataElementMap().get("UW_DECISION_BLURB"));
			}
			if(emailBody.contains("<%URL_RETRIEVE%>")){
				emailBody = emailBody.replace("<%URL_RETRIEVE%>", (String)eVO.getEmailDataElementMap().get("URL_RETRIEVE"));
			}
			if(emailBody.contains("<%ENCR_INPUTID%>") && eVO.getEmailDataElementMap().get("INPUT_IDENT_ENCR")!=null){
				emailBody = emailBody.replace("<%ENCR_INPUTID%>", (String)eVO.getEmailDataElementMap().get("INPUT_IDENT_ENCR"));
			}	
			
			if(emailBody.contains("<%ENCR_INPUTDATA%>") && eVO.getEmailDataElementMap().get("INPUT_DATA_ENCR")!=null){
				emailBody = emailBody.replace("<%ENCR_INPUTDATA%>", (String)eVO.getEmailDataElementMap().get("INPUT_DATA_ENCR"));
			}	
			
			if(emailBody.contains(MetlifeInstitutionalConstants.MEMBERNO)){
				emailBody = emailBody.replace(MetlifeInstitutionalConstants.MEMBERNO, (String)eVO.getEmailDataElementMap().get("MEMBER_NO"));
			}
			
			if(emailBody.contains("<%PRODUCT_DETAILS%>")){
				emailBody = emailBody.replace("<%PRODUCT_DETAILS%>", (String)eVO.getEmailDataElementMap().get("PRODUCT_DETAILS"));
			}
			if(emailBody.contains("<%EXCL_LOAD%>")){
				emailBody = emailBody.replace("<%EXCL_LOAD%>", (String)eVO.getEmailDataElementMap().get("EXCL_LOAD"));
			}
			if(emailBody.contains("<%FREQUENCY%>")){
				emailBody = emailBody.replace("<%FREQUENCY%>", (String)eVO.getEmailDataElementMap().get("FREQUENCY"));
			}
			if(emailBody.contains(MetlifeInstitutionalConstants.APPLICATION_DECISION)){
				emailBody = emailBody.replace(MetlifeInstitutionalConstants.APPLICATION_DECISION, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.APPDECISION));
			}
			if(emailBody.contains(MetlifeInstitutionalConstants.UWDECISION)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.UNDERWRITING_DECISION)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.UWDECISION, eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.UNDERWRITING_DECISION).toString());	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.UWDECISION, " ");
				}
				
			}
			if(emailBody.contains(MetlifeInstitutionalConstants.EMAIL_SUBJECT_NAME)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAILSUBJECTNAME)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_SUBJECT_NAME, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAILSUBJECTNAME));	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_SUBJECT_NAME, " ");
				}
				
				
			}
			
			/*SRKR: 20/10/2014
			 * WR7671 
			 * Scope
			 *Change to email sent to fund administrators 
			 *Change to all funds with invalid member flow
			 *Add member's contact information (preferred contact number, other contact number, preferred contact time, email address) to the email that gets sent to the fund administrator to validate via eLodgement.  
			 * */
			if(emailBody.contains(MetlifeInstitutionalConstants.EMAIL_PREFERRED_NUMBER)){
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_PREFERRED_CONTACT_NUMBER)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_PREFERRED_NUMBER, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_PREFERRED_CONTACT_NUMBER));	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_PREFERRED_NUMBER, " ");
				}
			}
			
			if(emailBody.contains(MetlifeInstitutionalConstants.EMAIL_OTHER_NUMBER)){
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_OTHER_CONTACT_NUMBER)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_OTHER_NUMBER, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_OTHER_CONTACT_NUMBER));	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_OTHER_NUMBER, " ");
				}
			}
			
			if(emailBody.contains(MetlifeInstitutionalConstants.EMAIL_PREFERRED_TIME)){
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_PREFERRED_CONTACT_TIME)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_PREFERRED_TIME, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_PREFERRED_CONTACT_TIME));	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_PREFERRED_TIME, " ");
				}
			}
			
			if(emailBody.contains(MetlifeInstitutionalConstants.EMAIL_ADDRESS)){
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_EMAIL_ADDRESS)){
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_ADDRESS, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAIL_EMAIL_ADDRESS));	
				}else{
					emailBody = emailBody.replace(MetlifeInstitutionalConstants.EMAIL_ADDRESS, " ");
				}
			}
			
			if (emailBody.contains("#DATE_BILL#")) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				java.util.Date date = new java.util.Date();
				String tempStringDate=dateFormat.format(date);
				Date dt1=new Date(tempStringDate);
				Calendar c = Calendar.getInstance(); 
				c.setTime(dt1); 
				c.add(Calendar.DATE, 5); 
				date.setTime(c.getTime().getTime());
				DateFormat format = new SimpleDateFormat( "dd/MM/yyyy" );
				String billDate=format.format(date);
				emailBody = emailBody.replace("#DATE_BILL#",billDate);
			
			}
			if (emailBody.contains("#KEY1#") && null!=eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.KEY1)) {
				emailBody = emailBody.replace("#KEY1#", (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.KEY1));
			}
			if (emailBody.contains("#KEY2#") && null!=eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.KEY2)) {
				emailBody = emailBody.replace("#KEY2#", (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.KEY2));
			}
			if (emailBody.contains("#S_NAME#") && null!=eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.S_NAME)) {
				emailBody = emailBody.replace("#S_NAME#", (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.S_NAME));
			}
			if(emailBody.contains("#P_NAME#") && null!=eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.P_NAME)){
				emailBody = emailBody.replaceAll("#P_NAME#", (String)eVO.getEmailDataElementMap().get("P_NAME"));
			}
			if (emailBody.contains("#S_LAST_NAME#") && null!=eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.S_LAST_NAME)) {
				emailBody = emailBody.replace("#S_LAST_NAME#", (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.S_LAST_NAME));
			}
			if(emailBody.contains("#P_LAST_NAME#") && null!=eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.P_LAST_NAME)){
				emailBody = emailBody.replaceAll("#P_LAST_NAME#", (String)eVO.getEmailDataElementMap().get("P_LAST_NAME"));
			}
			
			
		}else{
			emailBody = "";
		}
		log.info("Getting message finish {}",emailBody);
	    return emailBody;
	}
	
	
	/**
	 * This method will be used to compose the subject  message using the template and
	 * input parameter hashmap
	 * 
	 * @param eVO
	 * @return String
	 * 
	 */
	private String getMessageSubject(MetLifeEmailVO eVO){
		log.info("Getting massage subject start");
		String emailSubject = null;
		
		if(null != eVO && null != eVO.getEmailDataElementMap() && null != eVO.getEmailDataElementMap().get("EMAIL_SUBJECT")){
			
			emailSubject =  eVO.getEmailDataElementMap().get("EMAIL_SUBJECT").toString();
			
			
			
			/*Replace the application number in the body of the email*/
			if(emailSubject.contains(MetlifeInstitutionalConstants.EMAIL_TITLE)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.TITLE_NAME)){
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.EMAIL_TITLE, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.TITLE_NAME));	
				}else{
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.EMAIL_TITLE, " ");
				}
				
			}
			
			if(emailSubject.contains(MetlifeInstitutionalConstants.APPLICATIONNUMBER)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.APPLICATION_NUMBER)){
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.APPLICATIONNUMBER, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.APPLICATION_NUMBER));	
				}else{
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.APPLICATIONNUMBER, " ");
				}
				
			}


			if(emailSubject.contains(MetlifeInstitutionalConstants.FIRSTNAME)){
				emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.FIRSTNAME, (String)eVO.getEmailDataElementMap().get("FIRSTNAME"));
			}
			if(emailSubject.contains(MetlifeInstitutionalConstants.LASTNAME)){
				emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.LASTNAME, (String)eVO.getEmailDataElementMap().get("LASTNAME"));
			}
			if(emailSubject.contains(MetlifeInstitutionalConstants.UWDECISION)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.UNDERWRITING_DECISION)){
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.UWDECISION, eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.UNDERWRITING_DECISION).toString());	
				}else{
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.UWDECISION, " ");
				}
				
			}
			if(emailSubject.contains(MetlifeInstitutionalConstants.EMAIL_SUBJECT_NAME)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAILSUBJECTNAME)){
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.EMAIL_SUBJECT_NAME, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.EMAILSUBJECTNAME));	
				}else{
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.EMAIL_SUBJECT_NAME, " ");
				}
				
				
			}
			
			
			if(emailSubject.contains(MetlifeInstitutionalConstants.APPLICATION_DECISION)){
				
				if(null != eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.APPDECISION)){
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.APPLICATION_DECISION, (String)eVO.getEmailDataElementMap().get(MetlifeInstitutionalConstants.APPDECISION));	
				}else{
					emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.APPLICATION_DECISION, " ");
				}				
				
			}
			
			if(emailSubject.contains(MetlifeInstitutionalConstants.MEMBERNO)){
				emailSubject = emailSubject.replace(MetlifeInstitutionalConstants.MEMBERNO, (String)eVO.getEmailDataElementMap().get("MEMBER_NO"));
			}
			
		}else{
			emailSubject = "";
		}
		log.info("Getting massage subject finish {}",emailSubject);
	    return emailSubject;
	}


	/**
     * Description: This method validates the input parameters
     * 
     * @param MetLifeEmailVO evo
     * 
     * @return void
     */
	/*private void validateInputs(MetLifeEmailVO evo) throws Exception {
		log.info("Validation of input start");
		if (evo.getToRecipents().length == ZERO_SIZE)
			throw new Exception(MetLifeEmailConstants.EMAIL_ERR_1);
		else if (evo.getToRecipents().length > 10)
			throw new Exception(MetLifeEmailConstants.EMAIL_ERR_2);
		if (evo.getSubject() == null || evo.getSubject().equals(""))
			throw new Exception(MetLifeEmailConstants.EMAIL_ERR_4);
		if (evo.getEmailTemplateId() == null
				|| evo.getEmailTemplateId().equals(""))
			throw new Exception(MetLifeEmailConstants.EMAIL_EXCEP_5);
		String str = null;
		for (int i = 0; i < evo.getToRecipents().length; i++) {
			InternetAddress[] addr = evo.getToRecipents();
			str = addr[i].toString();
			if (str.toUpperCase()
					.startsWith(MetLifeEmailConstants.EMAIL_RESR_1)) {
				throw new Exception(MetLifeEmailConstants.EMAIL_ERR_3);
			}
		}
		log.info("Validation of input finish");
	}	*/
	/**
     * Description: SimpleAuthenticator is used to do simple authentication
	 * when the SMTP server requires it.    
     * 
     */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			log.info("Password Authentication start");
			emailUserName="";/*ConfigurationHelper.getConfigurationValue(MetLifeEmailConstants.DEV_METLF_EMAIL_INFO,MetLifeEmailConstants.EMAIL_ID_USERNAME);*/
		    emailPw="";/*ConfigurationHelper.getConfigurationValue(MetLifeEmailConstants.DEV_METLF_EMAIL_INFO,MetLifeEmailConstants.EMAIL_ID_PASSWORD);*/
			String username = emailUserName;
			String password = emailPw;
			log.info("Password Authentication finish");
			return new PasswordAuthentication(username, password);
			
		}
	}
	
	public String getUploadPath(){
        
         log.info("Getting upload path start");
        String basePath ="C:\\files\\"; /*ConfigurationHelper.getConfigurationValue(MetLifeEmailConstants.FILE_UPLOAD_PATH, MetLifeEmailConstants.FILE_UPLOAD_PATH);"C:/MetLife/properties/";*/       
        DateFormat dateMonthFormat = new SimpleDateFormat("MMMyyyy");
        String monthDir = dateMonthFormat.format(new java.util.Date());
        String dayDir  = new SimpleDateFormat("ddMMyyyy").format(new java.util.Date());
        /*File file = new File(basePath+monthDir+"/"+dayDir);*/
        /*boolean success = file.mkdirs();*/
        String  filepath = basePath+monthDir+"/"+dayDir+"/";     
        log.info("Getting upload path finish");
        return filepath;
        
  }
	
	public static String getDateExtension(){
		 log.info("Getting Date extension start");
		java.util.Date currDate = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("hhmmddMMyyyy");		 
	    String theDate = formatter.format(currDate);
	    log.info("Getting Date extension finish");
		return theDate;
	}

}
