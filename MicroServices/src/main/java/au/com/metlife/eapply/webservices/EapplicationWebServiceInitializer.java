package au.com.metlife.eapply.webservices;

import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;

import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetails;
import au.com.metlife.eapply.SoapClient;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.utility.EapplyHelper;

public class EapplicationWebServiceInitializer {
	
	
	private Eapplication getApplicationDto(EapplyInput eapplyInput) throws ParseException {
		Eapplication eapplication = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		 EapplyHelper.setdefaultDecision(eapplication, eapplyInput);
		 //Work rating maintained
		 if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 eapplication.setAppdecision("ACC");
		 }
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision(eapplication.getAppdecision());
		 }
		 return eapplication;
	}

	 /**  Added for SFPS Web service call   
	 * @throws Exception */
	   public EappHistory doInitStatewide(EapplyInput eapplyInput) throws Exception {		  
		   EappHistory historyDto = new EappHistory();
		   UpdateInsuranceDetails request = null;
				
				SoapClient soapClient = new SoapClient();		
				Eapplication applicationDTO = getApplicationDto(eapplyInput);
				
				if("CANCOVER".equalsIgnoreCase(eapplyInput.getManageType())) {
					request = EapplicationtRequestMapper.doMapForSFPSFynSynCancellation(applicationDTO, eapplyInput);	
				}else if("TCOVER".equalsIgnoreCase(eapplyInput.getManageType())) {
					request = EapplicationtRequestMapper.doMapForSFPSFynSynTransfer(applicationDTO, eapplyInput);
				}else {
					request = EapplicationtRequestMapper.doMapForSFPSFynSyn(applicationDTO, eapplyInput);	
				}							
			
				JAXBContext jaxbContext = JAXBContext.newInstance(UpdateInsuranceDetails.class);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();						
				StringWriter sw = new StringWriter();
				jaxbMarshaller.marshal(request, sw);				
				historyDto.setMetadata(sw.toString());
				String clientRefNumber = ((Applicant)applicationDTO.getApplicant().get(0)).getClientrefnumber();
				if(clientRefNumber.length()>10){				
					historyDto.setClientrefnumber(clientRefNumber.substring(10));	
				}else{
					historyDto.setClientrefnumber(clientRefNumber);	
				}			
				au.com.finsyn.acuritywebservices.services.UpdateInsurancePolicyAndPolicyBenefitsOutput updateInsurancePolicyAndPolicyBenefitsOutput = soapClient.callStateWideService(request).getReturn();				
				List<Long> jobNumberList = updateInsurancePolicyAndPolicyBenefitsOutput.getRecordID();
				StringBuffer jobStringBuff = new StringBuffer();
				for (Iterator iterator = jobNumberList.iterator(); iterator.hasNext();) {
					Long long1 = (Long) iterator.next();
					jobStringBuff.append((Long) iterator.next());
					jobStringBuff.append(", ");					
				}				
				historyDto.setJobnumber(jobStringBuff.toString());
				historyDto.setStatus("Success");	
				
				historyDto.setJobnumber(jobStringBuff.toString());
				historyDto.setStatus("Success");
				historyDto.setAppnumber(applicationDTO.getApplicationumber());
				historyDto.setAppdecision(applicationDTO.getAppdecision());
				historyDto.setCreatedate(new Timestamp(new Date().getTime()));
			return historyDto;
	   }
	   
	   public EappHistory statewidePostback(EapplyInput eapplyInput, Eapplication applicationDTO) throws DatatypeConfigurationException, JAXBException {
		   UpdateInsuranceDetails request = null;			
			SoapClient soapClient = new SoapClient();	
			EappHistory historyDto = new EappHistory();
			try {
					
					if("CANCOVER".equalsIgnoreCase(eapplyInput.getManageType())) {
						request = EapplicationtRequestMapper.doMapForSFPSFynSynCancellation(applicationDTO, eapplyInput);	
					}else if("TCOVER".equalsIgnoreCase(eapplyInput.getManageType())) {
						request = EapplicationtRequestMapper.doMapForSFPSFynSynTransfer(applicationDTO, eapplyInput);
					}else {
						request = EapplicationtRequestMapper.doMapForSFPSFynSyn(applicationDTO, eapplyInput);	
					}							
					historyDto.setAppnumber(applicationDTO.getApplicationumber());
					historyDto.setAppdecision(applicationDTO.getAppdecision());
					historyDto.setCreatedate(new Timestamp(new Date().getTime()));
					historyDto.setFundid(eapplyInput.getPartnerCode());
					JAXBContext jaxbContext = JAXBContext.newInstance(UpdateInsuranceDetails.class);
					Marshaller jaxbMarshaller = jaxbContext.createMarshaller();						
					StringWriter sw = new StringWriter();
					jaxbMarshaller.marshal(request, sw);				
					historyDto.setMetadata(sw.toString());
					String clientRefNumber = ((Applicant)applicationDTO.getApplicant().get(0)).getClientrefnumber();
					if(clientRefNumber.length()>10){				
						historyDto.setClientrefnumber(clientRefNumber.substring(10));	
					}else{
						historyDto.setClientrefnumber(clientRefNumber);	
					}			
					au.com.finsyn.acuritywebservices.services.UpdateInsurancePolicyAndPolicyBenefitsOutput updateInsurancePolicyAndPolicyBenefitsOutput;
					
					updateInsurancePolicyAndPolicyBenefitsOutput = soapClient.callStateWideService(request).getReturn();
								
					List<Long> jobNumberList = updateInsurancePolicyAndPolicyBenefitsOutput.getRecordID();
					StringBuffer jobStringBuff = new StringBuffer();
					for (Iterator iterator = jobNumberList.iterator(); iterator.hasNext();) {
						jobStringBuff.append((Long) iterator.next());
						jobStringBuff.append(", ");					
					}				
					historyDto.setJobnumber(jobStringBuff.toString());
					historyDto.setStatus("Success");
					
			} catch (Exception e) {		
				e.printStackTrace();
				historyDto.setStatus("Failed");
			}	
			
			
		return historyDto;
	   }
}
