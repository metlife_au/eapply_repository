package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Rating implements Serializable{
	private String type;
	private String value;
	private String reason;
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
