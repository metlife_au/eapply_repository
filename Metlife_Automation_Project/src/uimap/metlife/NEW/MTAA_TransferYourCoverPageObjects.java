package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class MTAA_TransferYourCoverPageObjects extends PageObjectLibrary{
	
	public static final By  TransferYourCoverLink = getObjectIdentifaction("MTAA.TYC.Link.xpath");
	public static final By  EmailId = getObjectIdentifaction("MTAA.TYC.Email.id");
	public static final By  ContactTypeClick = getObjectIdentifaction("MTAA.TYC.ContactTypeClick.xpath");
	public static final By  ContactNumber = getObjectIdentifaction("MTAA.TYC.ContactNumber.id");
	public static final By  MorningContact = getObjectIdentifaction("MTAA.TYC.MorningContact.id");
	public static final By  AfterNoonContact = getObjectIdentifaction("MTAA.TYC.AfterNoonContact.id");
	public static final By  ContactDetailsContinue = getObjectIdentifaction("MTAA.TYC.ContactDetailsContinue.xpath");
	public static final By  FifteenHoursYes = getObjectIdentifaction("MTAA.TYC.FifteenHoursYes.id");
	public static final By  FifteenHoursNo = getObjectIdentifaction("MTAA.TYC.FifteenHoursNo.id");
	public static final By  CitizenYes = getObjectIdentifaction("MTAA.TYC.CitizenYes.id");
	public static final By  CitizenNo = getObjectIdentifaction("MTAA.TYC.CitizenNo.id");
	public static final By  ClickIndustry = getObjectIdentifaction("MTAA.TYC.ClickIndustry.xpath");
	public static final By  ClickOccupation = getObjectIdentifaction("MTAA.TYC.ClickOccupation.xpath");
	public static final By  OtherOccupation = getObjectIdentifaction("MTAA.TYC.OtherOccupation.id");
	public static final By  OfficeEnvironmentYes = getObjectIdentifaction("MTAA.TYC.OfficeEnvironmentYes.id");
	public static final By  OfficeEnvironmentNo = getObjectIdentifaction("MTAA.TYC.OfficeEnvironmentNo.id");
	public static final By  TertiaryQualYes = getObjectIdentifaction("MTAA.TYC.TertiaryQualYes.id");
	public static final By  TertiaryQualNo = getObjectIdentifaction("MTAA.TYC.TertiaryQualNo.id");
	public static final By  HazardousEncYes = getObjectIdentifaction("MTAA.TYC.HazardousEncYes.id");
	public static final By  HazardousEncNo = getObjectIdentifaction("MTAA.TYC.HazardousEncNo.id");
	public static final By  OutsideOfcPercentYes = getObjectIdentifaction("MTAA.TYC.OutsideOfcPercentYes.id");
	public static final By  OutsideOfcPercentNo = getObjectIdentifaction("MTAA.TYC.OutsideOfcPercentNo.id");
	public static final By  AnnualSalary = getObjectIdentifaction("MTAA.TYC.AnnualSalary.id");
	public static final By  LoadSalaryLabel = getObjectIdentifaction("MTAA.TYC.LoadSalaryLabel.xpath");
	public static final By  ContinueOccupationDetails = getObjectIdentifaction("MTAA.TYC.ContinueOccupationDetails.xpath");
	
	public static final By PreviousInsurer = getObjectIdentifaction("MTAA.TYC.PreviousInsurer.id");
	public static final By PolicyNumber = getObjectIdentifaction("MTAA.TYC.PolicyNumber.id");
	public static final By USINumber = getObjectIdentifaction("MTAA.TYC.USINumber.id");
	public static final By DocumentEvidenceYes = getObjectIdentifaction("MTAA.TYC.DocumentEvidenceYes.id");
	public static final By DocumentEvidenceNo = getObjectIdentifaction("MTAA.TYC.DocumentEvidenceNo.id");
	
	
	public static final By TransferChkbox = getObjectIdentifaction("MTAA.TYC.TransferChkbox.id");
	public static final By CostofInsurance = getObjectIdentifaction("MTAA.TYC.CostofInsurance.xpath");
	public static final By ContinueAfterPreviousCover = getObjectIdentifaction("MTAA.TYC.ContinueAfterPreviousCover.xpath");
	public static final By DeathAmount = getObjectIdentifaction("MTAA.TYC.DeathAmount.id");
	public static final By LoadDeathAmount = getObjectIdentifaction("MTAA.TYC.LoadDeathAmount.xpath");
	public static final By TPDAmount = getObjectIdentifaction("MTAA.TYC.TPDAmount.id");
	public static final By LoadTPDAmount = getObjectIdentifaction("MTAA.TYC.LoadTPDAmount.xpath");
	public static final By IPAmount = getObjectIdentifaction("MTAA.TYC.IPAmount.xpath");
	public static final By LoadIPAmount = getObjectIdentifaction("MTAA.TYC.LoadIPAmount.xpath");
	public static final By Waitingperiod = getObjectIdentifaction("MTAA.TYC.Waitingperiod.xpath");
	public static final By benefitperiod = getObjectIdentifaction("MTAA.TYC.benefitperiod.xpath");
	public static final By CalculateQuote = getObjectIdentifaction("MTAA.TYC.CalculateQuote.xpath");
	
	public static final By ContinueAfterTYC = getObjectIdentifaction("MTAA.TYC.ContinueAfterTYC.xpath");
//	
//	public static final By RestrictedillnessYes = getObjectIdentifaction("MTAA.TYC.RestrictedillnessYes.xpath");
//	public static final By RestrictedillnessNo = getObjectIdentifaction("MTAA.TYC.RestrictedillnessNo.xpath");
//	public static final By DiagnosedillnessYes = getObjectIdentifaction("MTAA.TYC.DiagnosedillnessYes.xpath");
//	public static final By DiagnosedillnessNo = getObjectIdentifaction("MTAA.TYC.DiagnosedillnessNo.xpath");
//	public static final By TPDClaimYes = getObjectIdentifaction("MTAA.TYC.TPDClaimYes.xpath");
//	public static final By TPDClaimNo = getObjectIdentifaction("MTAA.TYC.TPDClaimNo.xpath");
//	public static final By Age65Yes = getObjectIdentifaction("MTAA.TYC.65AgeYes.xpath");
//	public static final By Age65No = getObjectIdentifaction("MTAA.TYC.65AgeNo.xpath");
//	public static final By FundpremloadingYes = getObjectIdentifaction("MTAA.TYC.FundpremloadingYes.xpath");
//	public static final By FundpremloadingNo = getObjectIdentifaction("MTAA.TYC.FundpremloadingNo.xpath");
//	public static final By ContinueAfterHealthSection = getObjectIdentifaction("MTAA.TYC.ContinueAfterHealthSection.xpath");

	//public static final By DutyofDisclosure = getObjectIdentifaction("MTAA.TYCover.DutyofDisclosure.xpath");
	//public static final By PrivacyStatement = getObjectIdentifaction("MTAA.TYCover.PrivacyStatement.xpath");
	//public static final By GeneralConsent = getObjectIdentifaction("MTAA.TYCover.GeneralConsent.xpath");
	//public static final By Submit = getObjectIdentifaction("MTAA.TYCover.Submit.xpath");
	//public static final By FeedbackNo = getObjectIdentifaction("MTAA.TYCover.FeedbackNo.xpath");


}
