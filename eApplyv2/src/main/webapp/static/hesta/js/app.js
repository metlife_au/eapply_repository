// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var CareSuperApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog']);
// configure our routes
CareSuperApp.config(function($routeProvider,$httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $routeProvider
	    .when('/', {
	        templateUrl : 'static/hesta/landingpage.html',
	        controller  : 'login',
	       reloadOnSearch: false,
	       resolve: {
	    	   urls:function(urlService){
	    		   return urlService.getUrls();
	    	   }
	       }
	    })
	    .when('/landing', {
	        templateUrl : 'static/hesta/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(urlService){
	    		   return urlService.getUrls();
	    	   }
	        }
	    })
	    .when('/quote/:mode', {
	        templateUrl : 'static/hesta/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    .when('/newmemberquote/:mode', {
	      templateUrl : 'static/hesta/newMember_changeCover.html',
	       controller  : 'newmemberquote'
	    })
	   .when('/quotecancel/:mode', {
	        templateUrl : 'static/hesta/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/hesta/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })
	     .when('/quotetransfer/:mode', {           
	    	 templateUrl : 'static/hesta/coverDetails_transfer.html',
	        controller  :  'quotetransfer'
	    })
	     .when('/quoteoccchange/:mode', {
	        templateUrl : 'static/hesta/coverDetails_updateDetails.html',
	        controller  : 'quoteoccupdate'
	    })
	     .when('/auratransfer/:mode', {             
	    	 templateUrl : 'static/hesta/aura_transferCover.html',
	        controller  : 'auratransfer'
	    })
	    .when('/auraocc/:mode', {             
	    	 templateUrl : 'static/hesta/aura_updateDetails.html',
	        controller  : 'auraocc'
	    })
	    .when('/auraspecialoffer/:mode', {             
	    	 templateUrl : 'static/hesta/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/aura/:mode', {
	        templateUrl : 'static/hesta/aura.html',
	        controller  : 'aura'
	    })
	    .when('/summary/:mode', {
	        templateUrl : 'static/hesta/confirmation.html',
	        controller  : 'summary'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/hesta/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/hesta/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/hesta/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/hesta/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/hesta/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	     .when('/requirements/:clm', {
	        templateUrl : 'claimtracker/events.html',
	        controller  : 'requirements'
	    })
	    .when('/newsummary', {
	        templateUrl : 'static/hesta/newMember_confirmation.html',
	        controller  : 'newsummary'
	    })
	    .when('/workRatingSummary', {
	        templateUrl : 'static/hesta/workRating_confirmation.html',
	        controller  : 'workRatingSummary'
	    })
	    
	    .when('/workRatingAccept', {
	        templateUrl : 'static/hesta/workRating_decision_accepted.html',
	        controller  : 'workRatingAcceptController'
	    })
	    
	    .when('/workRatingDecline', {
	        templateUrl : 'static/hesta/workRating_decision_decline.html',
	        controller  : 'workRatingDeclineController'
	    })
	    
	    .when('/workRatingMaintain', {
	        templateUrl : 'static/hesta/workRating_decision_maintained.html',
	        controller  : 'workRatingMaintainController'
	    })
	    
	    .when('/transferSummary', {
	        templateUrl : 'static/hesta/transfer_confirmation.html',
	        controller  : 'transferSummary'
	    })
	    
	    .when('/transferAccept', {
	        templateUrl : 'static/hesta/transfer_decision_accepted.html',
	        controller  : 'transferAcceptController'
	    })
	    
	    .when('/transferDecline', {
	        templateUrl : 'static/hesta/transfer_decision_decline.html',
	        controller  : 'transferDeclineController'
	    })
	    .when('/newmemberaccept', {
	        templateUrl : 'static/hesta/newMember_decision_accepted.html',
	        controller  : 'newMemberAcceptController'
	    })
	    .when('/newmemberdecline', {
	        templateUrl : 'static/hesta/newMember_decision_decline.html',
	        controller  : 'newMemberDeclineController'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/hesta/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/hesta/cancel_decision.html',
	        controller  : 'cancelController'
	    })
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/hesta/timeout.html',
	        controller  : 'timeOutController'
	    })

});