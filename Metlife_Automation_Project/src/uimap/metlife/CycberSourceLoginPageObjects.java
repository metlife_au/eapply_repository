package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class CycberSourceLoginPageObjects extends PageObjectLibrary{
		
		public static final By txtMerchid = getObjectIdentifaction("CYBER.login.merchantid.text.id");
		public static final By txtUsername = getObjectIdentifaction("CYBER.login.username.text.id");
		public static final By txtPassword = getObjectIdentifaction("CYBER.login.password.text.id");
		public static final By btnLogin = getObjectIdentifaction("CYBER.login.submit.btn.css");
		public static final By lnkTextTs = getObjectIdentifaction("CYBER.search.transearch.linktext");
		public static final By lnkTextGs = getObjectIdentifaction("CYBER.search.gensearch.linktext");
		public static final By rdSearchType = getObjectIdentifaction("CYBER.search.searchfield.radio.css");
		public static final By selSearchField = getObjectIdentifaction("CYBER.search.searchfieldvalue.select.name");
		public static final By txtSearchValue = getObjectIdentifaction("CYBER.search.value.text.name");
		public static final By btnSearch = getObjectIdentifaction("CYBER.search.value.btn.name");
		public static final By txtReasonCode = getObjectIdentifaction("CYBER.searchresults.reasoncode.text.xpath");		
}
