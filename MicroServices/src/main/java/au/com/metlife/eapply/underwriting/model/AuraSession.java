package au.com.metlife.eapply.underwriting.model;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.rgatp.aura.wsapi.Questionnaire;

@Component
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value="session")
public class AuraSession {
	private Questionnaire questionnaire;

	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

}
