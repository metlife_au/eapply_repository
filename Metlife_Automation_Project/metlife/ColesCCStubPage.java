package pages.metlife;

import java.awt.Robot;
import java.awt.event.InputEvent;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCStubPageObjects;
import uimap.metlife.ColesConfirmationPageObjects;
import uimap.metlife.STFTHealthPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesCCStubPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesCCStubPage enterInputXML() {
		InputXMLDetails();
		return new ColesCCStubPage(scriptHelper);
	}

	public ColesCCStubPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void InputXMLDetails() {
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
/*			int x = 410;
		    int y = 187;
		    Thread.sleep(7000);
	        Robot robot = new Robot();
	        robot.mouseMove(x, y);
	        robot.mousePress(InputEvent.BUTTON1_MASK);
	        robot.mouseRelease(InputEvent.BUTTON1_MASK);*/
	        Thread.sleep(3000);
			/*ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-extensions");
			driver = new ChromeDriver(options);*/
			
			String inputXML = dataTable.getData("MetLife_Data", "XML");
			tryAction(waitForClickableElement(ColesCCStubPageObjects.txtareainput),"SET", "Input",inputXML);
		
			sleep(4000);
		
			switchToActiveWindow();
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ColesCCStubPageObjects.btnApply)).click().perform();
			Thread.sleep(4000);
			
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
