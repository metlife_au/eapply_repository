/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 199306
 *
 */
public class QuoteResponse implements Serializable{

	private String coverType;
	
	private BigDecimal coverAmount;
	
	private BigDecimal cost;
	
	public String getCoverType() {
		return coverType;
	}

	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}

	public BigDecimal getCoverAmount() {
		return coverAmount;
	}

	public void setCoverAmount(BigDecimal coverAmount) {
		this.coverAmount = coverAmount;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}


}
