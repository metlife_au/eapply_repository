package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.metlife.eapply.quote.controller.QuoteController;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.service.QuoteService;
import au.com.metlife.eapply.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
public class QuoteControllerTest {

	@InjectMocks
	private QuoteController controller;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;
	String testData1 = null;
	String fileName1 = null;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	String flag = null;

	// valid fundCode for HOST
	@Test
	public void getProductConfigHostTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=HOST&memberType=INDUSTRY&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("HOST")).andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("INDUSTRY"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").exists())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").value("5000000"))
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").exists())
				.andExpect(jsonPath("$.[0].ipMaxAmount").value("30000"))

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").exists())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").value("150000"))
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").exists())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").value("500001"))
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(0));

	}

	// Null fundCode for HOST
	@Test
	public void NullFundCodeforProductConfig() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getProductConfig?memberType&manageType&fundCode")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())

				;

	}

	// invalid fundCode for HOST
	@Test
	public void getProductConfigInvalidfundCodeHOSTTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=HOST1&memberType=INDUSTRY&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("HOST1"))
				.andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("INDUSTRY"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").doesNotExist())

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").doesNotExist())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(0));

	}

	// valid fundCode for AEIS
	@Test
	public void getProductConfigAEISTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=AEIS&memberType=Personal&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("AEIS")).andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("Personal"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").exists())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").value("5000000"))
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").exists())
				.andExpect(jsonPath("$.[0].ipMaxAmount").value("30000"))

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").exists())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").value("100000"))
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").exists())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").value("500001"))
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(0));

	}

	// invalid fundCode for AEIS
	@Test
	public void getProductConfigInvalidfundCodeAEISTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=AEIS1&memberType=Personal&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("AEIS1"))
				.andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("Personal"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").doesNotExist())

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").doesNotExist())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(0));

	}

	// valid fundCode for CareSuper
	@Test
	public void getProductConfigCareSuperTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=CARE&memberType=Personal&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("CARE")).andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("Personal"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").exists())
				.andExpect(jsonPath("$.[0].deathMaxAmount").value(10000000))
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").exists())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").value("3000000"))
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").exists())
				.andExpect(jsonPath("$.[0].ipMaxAmount").value("20000"))

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").exists())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").value("100000"))
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").exists())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").value("500001"))
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(425));

	}

	// invalid fundCode for CareSuper
	@Test
	public void getProductConfigInvalidfundCodeCareSuperTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getProductConfig?fundCode=AEIS1&memberType=Personal&manageType=CCOVER")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].productCode").exists())
				.andExpect(jsonPath("$.[0].productCode").value("AEIS1"))
				.andExpect(jsonPath("$.[0].memberType").exists())
				.andExpect(jsonPath("$.[0].memberType").value("Personal"))
				.andExpect(jsonPath("$.[0].manageType").exists())
				.andExpect(jsonPath("$.[0].manageType").value("CCOVER"))

				.andExpect(jsonPath("$.[0].defaultPremFreq").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMinAge").exists()).andExpect(jsonPath("$.[0].deathMinAge").value(0))
				.andExpect(jsonPath("$.[0].deathMaxAge").exists()).andExpect(jsonPath("$.[0].deathMaxAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMinAge").exists()).andExpect(jsonPath("$.[0].tpdMinAge").value(0))
				.andExpect(jsonPath("$.[0].tpdMaxAge").exists()).andExpect(jsonPath("$.[0].tpdMaxAge").value(0))
				.andExpect(jsonPath("$.[0].ipMinAge").exists()).andExpect(jsonPath("$.[0].ipMinAge").value(0))
				.andExpect(jsonPath("$.[0].ipMaxAge").exists()).andExpect(jsonPath("$.[0].ipMaxAge").value(0))

				.andExpect(jsonPath("$.[0].deathMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].deathMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].tpdMaxAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMinAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].ipMaxAmount").doesNotExist())

				.andExpect(jsonPath("$.[0].deathTpdTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].deathTpdTransferMaxUnits").doesNotExist())
				.andExpect(jsonPath("$.[0].ipTransferMaxAmt").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalForUpgradeVal").doesNotExist())
				.andExpect(jsonPath("$.[0].annualSalMaxLimit").doesNotExist())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").exists())
				.andExpect(jsonPath("$.[0].ipUnitCostMulitiplier").value(0));

	}

	// for /calculateAll valid FundCode for HOST
	@Test
	public void savecalculateAll() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAll.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				// .andExpect(jsonPath("$.[0].cost").value(0))
				.andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(74.06)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for /calculateAll Invalid FundCode
	@Test
	public void savecalculateAllInvalidFundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllInvalidfundCode.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for /calculateAll null FundCode
	@Test
	public void savecalculateAllEmptyRequestBody() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculateAllNullRequestBody.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[2].coverType").doesNotExist())

				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist());
	}

	// for /calculateAll null FundCode
	@Test
	public void savecalculateAllNullFundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllNullFundCode.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").doesNotExist())

				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}

	// for /calculateAll Invalid CoverType for HOST
	@Test
	public void savecalculateAllInvalidCoverType() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllInvaliCoverType.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists())
				.andExpect(jsonPath("$.[0].coverType").value("DcFixed1"))
				.andExpect(jsonPath("$.[0].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[0].cost").doesNotExist())

				.andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed1"))
				.andExpect(jsonPath("$.[1].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[1].cost").doesNotExist())

				.andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed1"))
				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}

	// For AEIS valid Data
	@Test
	public void savecalculateAllForValidData() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculateAllValidData.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(25.58)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(45.38)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for invalid Cover type for AEIS
	@Test
	public void calculateInvalidCoverForAEIS() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculateAllInvalidCoverForAeis.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists())
				.andExpect(jsonPath("$.[0].coverType").value("DcFixed1"))
				.andExpect(jsonPath("$.[0].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].cost").doesNotExist()).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed1"))
				.andExpect(jsonPath("$.[1].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[1].cost").doesNotExist()).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed1"))
				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}

	// for Valid CareSuper

	@Test
	public void calculateAllForValidDataCareSuper() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "CALCULATEaLLCareSuperValidData.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for Invalid Cover for CareSuper

	@Test
	public void calculateInvalidCoverForCARE() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "CalculateAllInvalidCoverForCareSuper.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists())
				.andExpect(jsonPath("$.[0].coverType").value("DcFixed1"))
				.andExpect(jsonPath("$.[0].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].cost").doesNotExist()).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed1"))
				.andExpect(jsonPath("$.[1].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[1].cost").doesNotExist()).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed1"))
				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}

	// for Invalid Cover for null

	@Test
	public void QuoteControllercalculateAllNull() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllercalculateAllNull.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").doesNotExist())

				.andExpect(jsonPath("$.[0].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].cost").doesNotExist()).andExpect(jsonPath("$.[1].coverType").doesNotExist())

				.andExpect(jsonPath("$.[1].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[1].cost").doesNotExist())

				.andExpect(jsonPath("$.[2].coverType").doesNotExist())
				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist())

				;
	}

	// calculateTransfer For AEIS,HOST,CARESUPER
	@Test
	public void savecalculateTransferAEIS() throws Exception {
		// given
		List<QuoteResponse> quoteResponseList = null;
		RuleModel ruleModel = new RuleModel();
		QuoteResponse quoteRespList1 = new QuoteResponse();
		quoteRespList1.setCost(new BigDecimal(25.58));
		quoteRespList1.setCoverAmount(new BigDecimal(500000));
		quoteRespList1.setCoverType("DcFixed");
		QuoteResponse quoteRespList2 = new QuoteResponse();
		quoteRespList2.setCost(new BigDecimal(45.38));

		ObjectMapper mapper = new ObjectMapper();

		File file = new ClassPathResource("calculateAllValidData.json").getFile();
		RuleModel obj = mapper.readValue(file, RuleModel.class);
		// json =
		// mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);

		QuoteService service = mock(QuoteService.class);
		when(service.calculateDeath(ruleModel)).thenReturn(quoteRespList1);
		when(service.calculateTpd(ruleModel)).thenReturn(quoteRespList2);
		QuoteController controller = new QuoteController(service);

		ResponseEntity<List<QuoteResponse>> response = controller.calculateTransfer(ruleModel);
		List<QuoteResponse> body = response.getBody();
		/*
		 * System.out.println(body.size());
		 * System.out.println(body.get(0).getCoverType());
		 */

		// then
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(body.get(0).getCoverType(), equalTo("DcFixed"));
		assertThat(body.get(0).getCoverAmount(), equalTo(new BigDecimal(500000)));
		assertThat(body.get(1).getCost(), equalTo(new BigDecimal(45.38)));

	}

	// calculateAll For AEIS,HOST,CARESUPER
	@Test
	public void QuoteControllerCalculateAll() throws Exception {
		// given
		List<QuoteResponse> quoteResponseList = null;
		RuleModel ruleModel = new RuleModel();
		QuoteResponse quoteRespList1 = new QuoteResponse();
		quoteRespList1.setCost(new BigDecimal(25.58));
		quoteRespList1.setCoverAmount(new BigDecimal(500000));
		quoteRespList1.setCoverType("DcFixed");
		QuoteResponse quoteRespList2 = new QuoteResponse();
		quoteRespList2.setCost(new BigDecimal(45.38));

		ObjectMapper mapper = new ObjectMapper();

		File file = new ClassPathResource("calculateAllValidData.json").getFile();
		RuleModel obj = mapper.readValue(file, RuleModel.class);

		QuoteService service = mock(QuoteService.class);
		when(service.calculateDeath(ruleModel)).thenReturn(quoteRespList1);
		when(service.calculateTpd(ruleModel)).thenReturn(quoteRespList2);
		QuoteController controller = new QuoteController(service);

		ResponseEntity<List<QuoteResponse>> response = controller.calculateAll(ruleModel);
		List<QuoteResponse> body = response.getBody();
		/*
		 * System.out.println(body.size());
		 * System.out.println(body.get(0).getCoverType());
		 */

		// then
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(body.get(0).getCoverType(), equalTo("DcFixed"));
		assertThat(body.get(0).getCoverAmount(), equalTo(new BigDecimal(500000)));
		assertThat(body.get(1).getCost(), equalTo(new BigDecimal(45.38)));

	}

	// valid fundCode for HOST for calculateTransfer
	@Test

	public void savecalculateTransferForHost() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAll.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateTransfer").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(17.68)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(74.06)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// invalid FundCode for calculateTransfer
	@Test
	public void savecalculateTransferInvalidFundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllInvalidfundCode.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for /calculateTransfer null FundCode
	@Test
	public void savecalculateTransferNullfundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerCalculateAllNullFundCode.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[2].coverType").doesNotExist())

				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist());
	}

	// for /calculateTransfer for empty input

	@Test
	public void savecalculateTransferEmptyRequestBody() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculateAllNullRequestBody.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").doesNotExist())
				.andExpect(jsonPath("$.[0].coverAmount").doesNotExist())
				.andExpect(jsonPath("$.[0].cost").doesNotExist()).andExpect(jsonPath("$.[2].coverType").doesNotExist())

				.andExpect(jsonPath("$.[2].coverAmount").doesNotExist())

				.andExpect(jsonPath("$.[2].cost").doesNotExist());
	}

	// valid fundCode for AEIS for calculateTransfer
	@Test

	public void savecalculateTransferForAEIS() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculateAllValidData.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateTransfer").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(25.58)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(45.38)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// valid fundCode for CARESUPER for calculateTransfer
	@Test

	public void savecalculateTransferForCARESUPER() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "CALCULATEaLLCareSuperValidData.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateTransfer").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].coverType").exists()).andExpect(jsonPath("$.[0].coverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].coverAmount").exists())
				.andExpect(jsonPath("$.[0].coverAmount").value(500000)).andExpect(jsonPath("$.[0].cost").exists())
				.andExpect(jsonPath("$.[0].cost").value(0)).andExpect(jsonPath("$.[1].coverType").exists())
				.andExpect(jsonPath("$.[1].coverType").value("TPDFixed"))
				.andExpect(jsonPath("$.[1].coverAmount").exists())
				.andExpect(jsonPath("$.[1].coverAmount").value(500000)).andExpect(jsonPath("$.[1].cost").exists())
				.andExpect(jsonPath("$.[1].cost").value(0)).andExpect(jsonPath("$.[2].coverType").exists())
				.andExpect(jsonPath("$.[2].coverType").value("IpFixed"))
				.andExpect(jsonPath("$.[2].coverAmount").exists()).andExpect(jsonPath("$.[2].coverAmount").value(0))
				.andExpect(jsonPath("$.[2].cost").exists()).andExpect(jsonPath("$.[2].cost").value(0));
	}

	// for /getIndustryList

	// valid fundCode for HOST getIndustryListHostTest
	@Test
	public void getIndustryListHostTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode=HOST").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].key").exists())
				.andExpect(jsonPath("$.[0].key").value("001")).andExpect(jsonPath("$.[0].value").exists())
				.andExpect(jsonPath("$.[0].value").value("Advertising & Marketing"))

				.andExpect(jsonPath("$.[1].key").exists()).andExpect(jsonPath("$.[1].key").value("002"))
				.andExpect(jsonPath("$.[1].value").exists())
				.andExpect(jsonPath("$.[1].value").value("Agriculture, Animals, Forestry & Timber"))

				.andExpect(jsonPath("$.[2].key").exists()).andExpect(jsonPath("$.[2].key").value("003"))
				.andExpect(jsonPath("$.[2].value").exists())
				.andExpect(jsonPath("$.[2].value").value("Art & Entertainment"))

				.andExpect(jsonPath("$.[3].key").exists()).andExpect(jsonPath("$.[3].key").value("004"))
				.andExpect(jsonPath("$.[3].value").exists()).andExpect(jsonPath("$.[3].value").value("Aviation"))

				.andExpect(jsonPath("$.[4].key").exists()).andExpect(jsonPath("$.[4].key").value("005"))
				.andExpect(jsonPath("$.[4].value").exists())
				.andExpect(jsonPath("$.[4].value").value("Building, Furniture, Architecture & Construction"))

				.andExpect(jsonPath("$.[5].key").exists()).andExpect(jsonPath("$.[5].key").value("006"))
				.andExpect(jsonPath("$.[5].value").exists())
				.andExpect(jsonPath("$.[5].value").value("Education, Training & Recruitment"))

				.andExpect(jsonPath("$.[6].key").exists()).andExpect(jsonPath("$.[6].key").value("007"))
				.andExpect(jsonPath("$.[6].value").exists())
				.andExpect(jsonPath("$.[6].value").value("Finance & Banking"))

				.andExpect(jsonPath("$.[7].key").exists()).andExpect(jsonPath("$.[7].key").value("008"))
				.andExpect(jsonPath("$.[7].value").exists())
				.andExpect(jsonPath("$.[7].value").value("Fisheries & Marine/Shipping"))

				.andExpect(jsonPath("$.[8].key").exists()).andExpect(jsonPath("$.[8].key").value("009"))
				.andExpect(jsonPath("$.[8].value").exists())
				.andExpect(jsonPath("$.[8].value").value("Government or Community Services"))

				.andExpect(jsonPath("$.[9].key").exists()).andExpect(jsonPath("$.[9].key").value("010"))
				.andExpect(jsonPath("$.[9].value").exists())
				.andExpect(jsonPath("$.[9].value").value("Hospitality & Tourism"))

				.andExpect(jsonPath("$.[10].key").exists()).andExpect(jsonPath("$.[10].key").value("011"))
				.andExpect(jsonPath("$.[10].value").exists())
				.andExpect(jsonPath("$.[10].value").value("Information Technology"))

				.andExpect(jsonPath("$.[11].key").exists()).andExpect(jsonPath("$.[11].key").value("012"))
				.andExpect(jsonPath("$.[11].value").exists()).andExpect(jsonPath("$.[11].value").value("Legal"))

				.andExpect(jsonPath("$.[12].key").exists()).andExpect(jsonPath("$.[12].key").value("013"))
				.andExpect(jsonPath("$.[12].value").exists()).andExpect(jsonPath("$.[12].value").value("Manufacturing"))

				.andExpect(jsonPath("$.[13].key").exists()).andExpect(jsonPath("$.[13].key").value("014"))
				.andExpect(jsonPath("$.[13].value").exists())
				.andExpect(jsonPath("$.[13].value").value("Media & Telecommunications"))

				.andExpect(jsonPath("$.[14].key").exists()).andExpect(jsonPath("$.[14].key").value("015"))
				.andExpect(jsonPath("$.[14].value").exists())
				.andExpect(jsonPath("$.[14].value").value("Medical & Emergency Services"))

				.andExpect(jsonPath("$.[15].key").exists()).andExpect(jsonPath("$.[15].key").value("016"))
				.andExpect(jsonPath("$.[15].value").exists())
				.andExpect(jsonPath("$.[15].value").value("Mining & Energy"))

				.andExpect(jsonPath("$.[16].key").exists()).andExpect(jsonPath("$.[16].key").value("017"))
				.andExpect(jsonPath("$.[16].value").exists()).andExpect(jsonPath("$.[16].value").value("Motor Trade"))

				.andExpect(jsonPath("$.[17].key").exists()).andExpect(jsonPath("$.[17].key").value("018"))
				.andExpect(jsonPath("$.[17].value").exists())
				.andExpect(jsonPath("$.[17].value").value("Police, Defence & Security"))

				.andExpect(jsonPath("$.[18].key").exists()).andExpect(jsonPath("$.[18].key").value("019"))
				.andExpect(jsonPath("$.[18].value").exists())
				.andExpect(jsonPath("$.[18].value").value("Real Estate & Property Management"))

				.andExpect(jsonPath("$.[19].key").exists()).andExpect(jsonPath("$.[19].key").value("020"))
				.andExpect(jsonPath("$.[19].value").exists())
				.andExpect(jsonPath("$.[19].value").value("Retail & Sales"))

				.andExpect(jsonPath("$.[20].key").exists()).andExpect(jsonPath("$.[20].key").value("021"))
				.andExpect(jsonPath("$.[20].value").exists())
				.andExpect(jsonPath("$.[20].value").value("Science & Research"))

				.andExpect(jsonPath("$.[21].key").exists()).andExpect(jsonPath("$.[21].key").value("022"))
				.andExpect(jsonPath("$.[21].value").exists())
				.andExpect(jsonPath("$.[21].value").value("Sport & Fitness"))

				.andExpect(jsonPath("$.[22].key").exists()).andExpect(jsonPath("$.[22].key").value("023"))
				.andExpect(jsonPath("$.[22].value").exists()).andExpect(jsonPath("$.[22].value").value("Transport"))

				.andExpect(jsonPath("$.[23].key").exists()).andExpect(jsonPath("$.[23].key").value("024"))
				.andExpect(jsonPath("$.[23].value").exists()).andExpect(jsonPath("$.[23].value").value("Unemployed"))

				.andExpect(jsonPath("$.[24].key").exists()).andExpect(jsonPath("$.[24].key").value("025"))
				.andExpect(jsonPath("$.[24].value").exists()).andExpect(jsonPath("$.[24].value").value("Home Duties"))

				;

	}

	// Invalid fundCode
	@Test
	public void getIndustryListHostInvalidTest() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getIndustryList?fundCode=HOST1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$").isEmpty());

	}

	// null fundCode
	@Test
	public void getIndustryListHostNullTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())

				;

	}

	// valid fundCode for AEIS
	@Test
	public void getIndustryListAEISTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode=AEIS").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].key").exists())
				.andExpect(jsonPath("$.[0].key").value("001")).andExpect(jsonPath("$.[0].value").exists())
				.andExpect(jsonPath("$.[0].value").value("Advertising & Marketing"))

				.andExpect(jsonPath("$.[1].key").exists()).andExpect(jsonPath("$.[1].key").value("002"))
				.andExpect(jsonPath("$.[1].value").exists())
				.andExpect(jsonPath("$.[1].value").value("Agriculture, Animals, Forestry & Timber"))

				.andExpect(jsonPath("$.[2].key").exists()).andExpect(jsonPath("$.[2].key").value("003"))
				.andExpect(jsonPath("$.[2].value").exists())
				.andExpect(jsonPath("$.[2].value").value("Art & Entertainment"))

				.andExpect(jsonPath("$.[3].key").exists()).andExpect(jsonPath("$.[3].key").value("004"))
				.andExpect(jsonPath("$.[3].value").exists()).andExpect(jsonPath("$.[3].value").value("Aviation"))

				.andExpect(jsonPath("$.[4].key").exists()).andExpect(jsonPath("$.[4].key").value("005"))
				.andExpect(jsonPath("$.[4].value").exists())
				.andExpect(jsonPath("$.[4].value").value("Building, Furniture, Architecture & Construction"))

				.andExpect(jsonPath("$.[5].key").exists()).andExpect(jsonPath("$.[5].key").value("006"))
				.andExpect(jsonPath("$.[5].value").exists())
				.andExpect(jsonPath("$.[5].value").value("Education, Training & Recruitment"))

				.andExpect(jsonPath("$.[6].key").exists()).andExpect(jsonPath("$.[6].key").value("007"))
				.andExpect(jsonPath("$.[6].value").exists())
				.andExpect(jsonPath("$.[6].value").value("Finance & Banking"))

				.andExpect(jsonPath("$.[7].key").exists()).andExpect(jsonPath("$.[7].key").value("008"))
				.andExpect(jsonPath("$.[7].value").exists())
				.andExpect(jsonPath("$.[7].value").value("Fisheries & Marine/Shipping"))

				.andExpect(jsonPath("$.[8].key").exists()).andExpect(jsonPath("$.[8].key").value("009"))
				.andExpect(jsonPath("$.[8].value").exists())
				.andExpect(jsonPath("$.[8].value").value("Government or Community Services"))

				.andExpect(jsonPath("$.[9].key").exists()).andExpect(jsonPath("$.[9].key").value("010"))
				.andExpect(jsonPath("$.[9].value").exists())
				.andExpect(jsonPath("$.[9].value").value("Hospitality & Tourism"))

				.andExpect(jsonPath("$.[10].key").exists()).andExpect(jsonPath("$.[10].key").value("011"))
				.andExpect(jsonPath("$.[10].value").exists())
				.andExpect(jsonPath("$.[10].value").value("Information Technology"))

				.andExpect(jsonPath("$.[11].key").exists()).andExpect(jsonPath("$.[11].key").value("012"))
				.andExpect(jsonPath("$.[11].value").exists()).andExpect(jsonPath("$.[11].value").value("Legal"))

				.andExpect(jsonPath("$.[12].key").exists()).andExpect(jsonPath("$.[12].key").value("013"))
				.andExpect(jsonPath("$.[12].value").exists()).andExpect(jsonPath("$.[12].value").value("Manufacturing"))

				.andExpect(jsonPath("$.[13].key").exists()).andExpect(jsonPath("$.[13].key").value("014"))
				.andExpect(jsonPath("$.[13].value").exists())
				.andExpect(jsonPath("$.[13].value").value("Media & Telecommunications"))

				.andExpect(jsonPath("$.[14].key").exists()).andExpect(jsonPath("$.[14].key").value("015"))
				.andExpect(jsonPath("$.[14].value").exists())
				.andExpect(jsonPath("$.[14].value").value("Medical & Emergency Services"))

				.andExpect(jsonPath("$.[15].key").exists()).andExpect(jsonPath("$.[15].key").value("016"))
				.andExpect(jsonPath("$.[15].value").exists())
				.andExpect(jsonPath("$.[15].value").value("Mining & Energy"))

				.andExpect(jsonPath("$.[16].key").exists()).andExpect(jsonPath("$.[16].key").value("017"))
				.andExpect(jsonPath("$.[16].value").exists()).andExpect(jsonPath("$.[16].value").value("Motor Trade"))

				.andExpect(jsonPath("$.[17].key").exists()).andExpect(jsonPath("$.[17].key").value("018"))
				.andExpect(jsonPath("$.[17].value").exists())
				.andExpect(jsonPath("$.[17].value").value("Police, Defence & Security"))

				.andExpect(jsonPath("$.[18].key").exists()).andExpect(jsonPath("$.[18].key").value("019"))
				.andExpect(jsonPath("$.[18].value").exists())
				.andExpect(jsonPath("$.[18].value").value("Real Estate & Property Management"))

				.andExpect(jsonPath("$.[19].key").exists()).andExpect(jsonPath("$.[19].key").value("020"))
				.andExpect(jsonPath("$.[19].value").exists())
				.andExpect(jsonPath("$.[19].value").value("Retail & Sales"))

				.andExpect(jsonPath("$.[20].key").exists()).andExpect(jsonPath("$.[20].key").value("021"))
				.andExpect(jsonPath("$.[20].value").exists())
				.andExpect(jsonPath("$.[20].value").value("Science & Research"))

				.andExpect(jsonPath("$.[21].key").exists()).andExpect(jsonPath("$.[21].key").value("022"))
				.andExpect(jsonPath("$.[21].value").exists())
				.andExpect(jsonPath("$.[21].value").value("Sport & Fitness"))

				.andExpect(jsonPath("$.[22].key").exists()).andExpect(jsonPath("$.[22].key").value("023"))
				.andExpect(jsonPath("$.[22].value").exists()).andExpect(jsonPath("$.[22].value").value("Transport"))

				.andExpect(jsonPath("$.[23].key").exists()).andExpect(jsonPath("$.[23].key").value("024"))
				.andExpect(jsonPath("$.[23].value").exists()).andExpect(jsonPath("$.[23].value").value("Unemployed"))

				.andExpect(jsonPath("$.[24].key").exists()).andExpect(jsonPath("$.[24].key").value("025"))
				.andExpect(jsonPath("$.[24].value").exists()).andExpect(jsonPath("$.[24].value").value("Home Duties"))

				;

	}

	// valid fundCode for CARESUPER
	@Test
	public void getIndustryListCareSuperTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode=CARE").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].key").exists())
				.andExpect(jsonPath("$.[0].key").value("001")).andExpect(jsonPath("$.[0].value").exists())
				.andExpect(jsonPath("$.[0].value").value("Advertising & Marketing"))

				.andExpect(jsonPath("$.[1].key").exists()).andExpect(jsonPath("$.[1].key").value("002"))
				.andExpect(jsonPath("$.[1].value").exists())
				.andExpect(jsonPath("$.[1].value").value("Agriculture, Animals, Forestry & Timber"))

				.andExpect(jsonPath("$.[2].key").exists()).andExpect(jsonPath("$.[2].key").value("003"))
				.andExpect(jsonPath("$.[2].value").exists())
				.andExpect(jsonPath("$.[2].value").value("Art & Entertainment"))

				.andExpect(jsonPath("$.[3].key").exists()).andExpect(jsonPath("$.[3].key").value("004"))
				.andExpect(jsonPath("$.[3].value").exists()).andExpect(jsonPath("$.[3].value").value("Aviation"))

				.andExpect(jsonPath("$.[4].key").exists()).andExpect(jsonPath("$.[4].key").value("005"))
				.andExpect(jsonPath("$.[4].value").exists())
				.andExpect(jsonPath("$.[4].value").value("Building, Furniture, Architecture & Construction"))

				.andExpect(jsonPath("$.[5].key").exists()).andExpect(jsonPath("$.[5].key").value("006"))
				.andExpect(jsonPath("$.[5].value").exists())
				.andExpect(jsonPath("$.[5].value").value("Education, Training & Recruitment"))

				.andExpect(jsonPath("$.[6].key").exists()).andExpect(jsonPath("$.[6].key").value("007"))
				.andExpect(jsonPath("$.[6].value").exists())
				.andExpect(jsonPath("$.[6].value").value("Finance & Banking"))

				.andExpect(jsonPath("$.[7].key").exists()).andExpect(jsonPath("$.[7].key").value("008"))
				.andExpect(jsonPath("$.[7].value").exists())
				.andExpect(jsonPath("$.[7].value").value("Fisheries & Marine/Shipping"))

				.andExpect(jsonPath("$.[8].key").exists()).andExpect(jsonPath("$.[8].key").value("009"))
				.andExpect(jsonPath("$.[8].value").exists())
				.andExpect(jsonPath("$.[8].value").value("Government or Community Services"))

				.andExpect(jsonPath("$.[9].key").exists()).andExpect(jsonPath("$.[9].key").value("010"))
				.andExpect(jsonPath("$.[9].value").exists())
				.andExpect(jsonPath("$.[9].value").value("Hospitality & Tourism"))

				.andExpect(jsonPath("$.[10].key").exists()).andExpect(jsonPath("$.[10].key").value("011"))
				.andExpect(jsonPath("$.[10].value").exists())
				.andExpect(jsonPath("$.[10].value").value("Information Technology"))

				.andExpect(jsonPath("$.[11].key").exists()).andExpect(jsonPath("$.[11].key").value("012"))
				.andExpect(jsonPath("$.[11].value").exists()).andExpect(jsonPath("$.[11].value").value("Legal"))

				.andExpect(jsonPath("$.[12].key").exists()).andExpect(jsonPath("$.[12].key").value("013"))
				.andExpect(jsonPath("$.[12].value").exists()).andExpect(jsonPath("$.[12].value").value("Manufacturing"))

				.andExpect(jsonPath("$.[13].key").exists()).andExpect(jsonPath("$.[13].key").value("014"))
				.andExpect(jsonPath("$.[13].value").exists())
				.andExpect(jsonPath("$.[13].value").value("Media & Telecommunications"))

				.andExpect(jsonPath("$.[14].key").exists()).andExpect(jsonPath("$.[14].key").value("015"))
				.andExpect(jsonPath("$.[14].value").exists())
				.andExpect(jsonPath("$.[14].value").value("Medical & Emergency Services"))

				.andExpect(jsonPath("$.[15].key").exists()).andExpect(jsonPath("$.[15].key").value("016"))
				.andExpect(jsonPath("$.[15].value").exists())
				.andExpect(jsonPath("$.[15].value").value("Mining & Energy"))

				.andExpect(jsonPath("$.[16].key").exists()).andExpect(jsonPath("$.[16].key").value("017"))
				.andExpect(jsonPath("$.[16].value").exists()).andExpect(jsonPath("$.[16].value").value("Motor Trade"))

				.andExpect(jsonPath("$.[17].key").exists()).andExpect(jsonPath("$.[17].key").value("018"))
				.andExpect(jsonPath("$.[17].value").exists())
				.andExpect(jsonPath("$.[17].value").value("Police, Defence & Security"))

				.andExpect(jsonPath("$.[18].key").exists()).andExpect(jsonPath("$.[18].key").value("019"))
				.andExpect(jsonPath("$.[18].value").exists())
				.andExpect(jsonPath("$.[18].value").value("Real Estate & Property Management"))

				.andExpect(jsonPath("$.[19].key").exists()).andExpect(jsonPath("$.[19].key").value("020"))
				.andExpect(jsonPath("$.[19].value").exists())
				.andExpect(jsonPath("$.[19].value").value("Retail & Sales"))

				.andExpect(jsonPath("$.[20].key").exists()).andExpect(jsonPath("$.[20].key").value("021"))
				.andExpect(jsonPath("$.[20].value").exists())
				.andExpect(jsonPath("$.[20].value").value("Science & Research"))

				.andExpect(jsonPath("$.[21].key").exists()).andExpect(jsonPath("$.[21].key").value("022"))
				.andExpect(jsonPath("$.[21].value").exists())
				.andExpect(jsonPath("$.[21].value").value("Sport & Fitness"))

				.andExpect(jsonPath("$.[22].key").exists()).andExpect(jsonPath("$.[22].key").value("023"))
				.andExpect(jsonPath("$.[22].value").exists()).andExpect(jsonPath("$.[22].value").value("Transport"))

				.andExpect(jsonPath("$.[23].key").exists()).andExpect(jsonPath("$.[23].key").value("024"))
				.andExpect(jsonPath("$.[23].value").exists()).andExpect(jsonPath("$.[23].value").value("Unemployed"))

				.andExpect(jsonPath("$.[24].key").exists()).andExpect(jsonPath("$.[24].key").value("025"))
				.andExpect(jsonPath("$.[24].value").exists()).andExpect(jsonPath("$.[24].value").value("Home Duties"));

				

	}
	// valid fundCode for CORPORATE

	@Test
	public void getIndustryListCorporateTest() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/getIndustryList?fundCode=HOST").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].key").exists())
				.andExpect(jsonPath("$.[0].key").value("001")).andExpect(jsonPath("$.[0].value").exists())
				.andExpect(jsonPath("$.[0].value").value("Advertising & Marketing"))

				.andExpect(jsonPath("$.[1].key").exists()).andExpect(jsonPath("$.[1].key").value("002"))
				.andExpect(jsonPath("$.[1].value").exists())
				.andExpect(jsonPath("$.[1].value").value("Agriculture, Animals, Forestry & Timber"))

				.andExpect(jsonPath("$.[2].key").exists()).andExpect(jsonPath("$.[2].key").value("003"))
				.andExpect(jsonPath("$.[2].value").exists())
				.andExpect(jsonPath("$.[2].value").value("Art & Entertainment"))

				.andExpect(jsonPath("$.[3].key").exists()).andExpect(jsonPath("$.[3].key").value("004"))
				.andExpect(jsonPath("$.[3].value").exists()).andExpect(jsonPath("$.[3].value").value("Aviation"))

				.andExpect(jsonPath("$.[4].key").exists()).andExpect(jsonPath("$.[4].key").value("005"))
				.andExpect(jsonPath("$.[4].value").exists())
				.andExpect(jsonPath("$.[4].value").value("Building, Furniture, Architecture & Construction"))

				.andExpect(jsonPath("$.[5].key").exists()).andExpect(jsonPath("$.[5].key").value("006"))
				.andExpect(jsonPath("$.[5].value").exists())
				.andExpect(jsonPath("$.[5].value").value("Education, Training & Recruitment"))

				.andExpect(jsonPath("$.[6].key").exists()).andExpect(jsonPath("$.[6].key").value("007"))
				.andExpect(jsonPath("$.[6].value").exists())
				.andExpect(jsonPath("$.[6].value").value("Finance & Banking"))

				.andExpect(jsonPath("$.[7].key").exists()).andExpect(jsonPath("$.[7].key").value("008"))
				.andExpect(jsonPath("$.[7].value").exists())
				.andExpect(jsonPath("$.[7].value").value("Fisheries & Marine/Shipping"))

				.andExpect(jsonPath("$.[8].key").exists()).andExpect(jsonPath("$.[8].key").value("009"))
				.andExpect(jsonPath("$.[8].value").exists())
				.andExpect(jsonPath("$.[8].value").value("Government or Community Services"))

				.andExpect(jsonPath("$.[9].key").exists()).andExpect(jsonPath("$.[9].key").value("010"))
				.andExpect(jsonPath("$.[9].value").exists())
				.andExpect(jsonPath("$.[9].value").value("Hospitality & Tourism"))

				.andExpect(jsonPath("$.[10].key").exists()).andExpect(jsonPath("$.[10].key").value("011"))
				.andExpect(jsonPath("$.[10].value").exists())
				.andExpect(jsonPath("$.[10].value").value("Information Technology"))

				.andExpect(jsonPath("$.[11].key").exists()).andExpect(jsonPath("$.[11].key").value("012"))
				.andExpect(jsonPath("$.[11].value").exists()).andExpect(jsonPath("$.[11].value").value("Legal"))

				.andExpect(jsonPath("$.[12].key").exists()).andExpect(jsonPath("$.[12].key").value("013"))
				.andExpect(jsonPath("$.[12].value").exists()).andExpect(jsonPath("$.[12].value").value("Manufacturing"))

				.andExpect(jsonPath("$.[13].key").exists()).andExpect(jsonPath("$.[13].key").value("014"))
				.andExpect(jsonPath("$.[13].value").exists())
				.andExpect(jsonPath("$.[13].value").value("Media & Telecommunications"))

				.andExpect(jsonPath("$.[14].key").exists()).andExpect(jsonPath("$.[14].key").value("015"))
				.andExpect(jsonPath("$.[14].value").exists())
				.andExpect(jsonPath("$.[14].value").value("Medical & Emergency Services"))

				.andExpect(jsonPath("$.[15].key").exists()).andExpect(jsonPath("$.[15].key").value("016"))
				.andExpect(jsonPath("$.[15].value").exists())
				.andExpect(jsonPath("$.[15].value").value("Mining & Energy"))

				.andExpect(jsonPath("$.[16].key").exists()).andExpect(jsonPath("$.[16].key").value("017"))
				.andExpect(jsonPath("$.[16].value").exists()).andExpect(jsonPath("$.[16].value").value("Motor Trade"))

				.andExpect(jsonPath("$.[17].key").exists()).andExpect(jsonPath("$.[17].key").value("018"))
				.andExpect(jsonPath("$.[17].value").exists())
				.andExpect(jsonPath("$.[17].value").value("Police, Defence & Security"))

				.andExpect(jsonPath("$.[18].key").exists()).andExpect(jsonPath("$.[18].key").value("019"))
				.andExpect(jsonPath("$.[18].value").exists())
				.andExpect(jsonPath("$.[18].value").value("Real Estate & Property Management"))

				.andExpect(jsonPath("$.[19].key").exists()).andExpect(jsonPath("$.[19].key").value("020"))
				.andExpect(jsonPath("$.[19].value").exists())
				.andExpect(jsonPath("$.[19].value").value("Retail & Sales"))

				.andExpect(jsonPath("$.[20].key").exists()).andExpect(jsonPath("$.[20].key").value("021"))
				.andExpect(jsonPath("$.[20].value").exists())
				.andExpect(jsonPath("$.[20].value").value("Science & Research"))

				.andExpect(jsonPath("$.[21].key").exists()).andExpect(jsonPath("$.[21].key").value("022"))
				.andExpect(jsonPath("$.[21].value").exists())
				.andExpect(jsonPath("$.[21].value").value("Sport & Fitness"))

				.andExpect(jsonPath("$.[22].key").exists()).andExpect(jsonPath("$.[22].key").value("023"))
				.andExpect(jsonPath("$.[22].value").exists()).andExpect(jsonPath("$.[22].value").value("Transport"))

				.andExpect(jsonPath("$.[23].key").exists()).andExpect(jsonPath("$.[23].key").value("024"))
				.andExpect(jsonPath("$.[23].value").exists()).andExpect(jsonPath("$.[23].value").value("Unemployed"))

				.andExpect(jsonPath("$.[24].key").exists()).andExpect(jsonPath("$.[24].key").value("025"))
				.andExpect(jsonPath("$.[24].value").exists()).andExpect(jsonPath("$.[24].value").value("Home Duties"))

				;

	}

	// for/getNewOccupationList

	// valid fundCode for HOST and occName
	@Test
	public void getNewOccupationListHost() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?fundId=HOST&occName=001:Business Analyst/Consultant")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].id").exists())
				.andExpect(jsonPath("$.[0].id").value(4362))

				.andExpect(jsonPath("$.[0].deathfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].deathfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].deathunitcategeory").exists())
				.andExpect(jsonPath("$.[0].deathunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].ipfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipunitcategeory").exists())
				.andExpect(jsonPath("$.[0].ipunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].specialrisk").exists()).andExpect(jsonPath("$.[0].specialrisk").isEmpty())

				.andExpect(jsonPath("$.[0].tpdfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].tpdunitcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].fundid").exists()).andExpect(jsonPath("$.[0].fundid").value("HOST"))

				.andExpect(jsonPath("$.[0].occname").exists())
				.andExpect(jsonPath("$.[0].occname").value("001:Business Analyst/Consultant"))

				;

	}

	// Invalid fundCode for HOST and Invalid occName

	@Test
	public void getNewOccupationListHostInvalidFun() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getNewOccupationList?fundId=HOST1&occName=001:Business Analyst/Consultant111")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$").isEmpty())
				;

	}

	// null fundCode
	@Test
	public void getNewOccupationListNullFun() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?occName&fundId").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())

				;

	}

	// valid fundCode for AEIS and occName
	@Test
	public void getNewOccupationListAEIS() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?fundId=AEIS&occName=001:Business Analyst/Consultant")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].id").exists())
				.andExpect(jsonPath("$.[0].id").value(1554))

				.andExpect(jsonPath("$.[0].deathfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].deathfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].deathunitcategeory").exists())
				.andExpect(jsonPath("$.[0].deathunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].ipfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipunitcategeory").exists())
				.andExpect(jsonPath("$.[0].ipunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].specialrisk").exists()).andExpect(jsonPath("$.[0].specialrisk").isEmpty())

				.andExpect(jsonPath("$.[0].tpdfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].tpdunitcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].fundid").exists()).andExpect(jsonPath("$.[0].fundid").value("AEIS"))

				.andExpect(jsonPath("$.[0].occname").exists())
				.andExpect(jsonPath("$.[0].occname").value("001:Business Analyst/Consultant"))

				;

	}

	// valid fundCode for CARESUPER and occName
	@Test
	public void getNewOccupationListCARE() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?fundId=CARE&occName=001:Business Analyst/Consultant")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].id").exists())
				.andExpect(jsonPath("$.[0].id").value(1547))

				.andExpect(jsonPath("$.[0].deathfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].deathfixedcategeory").value("Professional"))

				.andExpect(jsonPath("$.[0].deathunitcategeory").exists())
				.andExpect(jsonPath("$.[0].deathunitcategeory").value("Professional"))

				.andExpect(jsonPath("$.[0].ipfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].ipfixedcategeory").value("Professional"))

				.andExpect(jsonPath("$.[0].ipunitcategeory").exists())
				.andExpect(jsonPath("$.[0].ipunitcategeory").value("Professional"))

				.andExpect(jsonPath("$.[0].specialrisk").exists()).andExpect(jsonPath("$.[0].specialrisk").isEmpty())

				.andExpect(jsonPath("$.[0].tpdfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdfixedcategeory").value("Professional"))

				.andExpect(jsonPath("$.[0].tpdunitcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdunitcategeory").value("Professional"))

				.andExpect(jsonPath("$.[0].fundid").exists()).andExpect(jsonPath("$.[0].fundid").value("CARE"))

				.andExpect(jsonPath("$.[0].occname").exists())
				.andExpect(jsonPath("$.[0].occname").value("001:Business Analyst/Consultant"))

				;

	}

	// valid fundCode and occName for Corporate
	@Test
	public void getNewOccupationListCorporate() throws Exception {

		mockMvc.perform(
				MockMvcRequestBuilders.get("/getNewOccupationList?fundId=HOST&occName=001:Business Analyst/Consultant")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.[0].id").exists())
				.andExpect(jsonPath("$.[0].id").value(4362))

				.andExpect(jsonPath("$.[0].deathfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].deathfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].deathunitcategeory").exists())
				.andExpect(jsonPath("$.[0].deathunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].ipfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].ipunitcategeory").exists())
				.andExpect(jsonPath("$.[0].ipunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].specialrisk").exists()).andExpect(jsonPath("$.[0].specialrisk").isEmpty())

				.andExpect(jsonPath("$.[0].tpdfixedcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdfixedcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].tpdunitcategeory").exists())
				.andExpect(jsonPath("$.[0].tpdunitcategeory").value("White Collar"))

				.andExpect(jsonPath("$.[0].fundid").exists()).andExpect(jsonPath("$.[0].fundid").value("HOST"))

				.andExpect(jsonPath("$.[0].occname").exists())
				.andExpect(jsonPath("$.[0].occname").value("001:Business Analyst/Consultant"))

				;

	}

	@Test
	public void eapplyInitiateHost() throws Exception {
		String input = "{\"fund\":\"HOST\",\"mode\":\"change\",\"age\":60,\"name\":\"HOST HOSTB\",\"appnumber\":1517301230928,\"gender\":\"Female\",\"country\":\"Australia\",\"fifteenHr\":\"Yes\",\"deathAmt\":0,\"tpdAmt\":0,\"ipAmt\":500,\"waitingPeriod\":\"90 Days\",\"benefitPeriod\":\"2 Years\",\"industryOcc\":\"012:Office desk based duties - Executive or manager\",\"salary\":\"5555\",\"clientname\":\"metaus\",\"lastName\":\"HOSTB\",\"firstName\":\"HOST\",\"dob\":\"01/11/1958\",\"existingTerm\":true,\"memberType\":\"INDUSTRY OCCUPATION\"}";
		mockMvc.perform(post("/initiate").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void eapplyInitiateCare() throws Exception {
		String input = "{\"fund\":\"CARE\",\"mode\":\"change\",\"age\":60,\"name\":\"HOST HOSTB\",\"appnumber\":1517301230928,\"gender\":\"Female\",\"country\":\"Australia\",\"fifteenHr\":\"Yes\",\"deathAmt\":0,\"tpdAmt\":0,\"ipAmt\":500,\"waitingPeriod\":\"90 Days\",\"benefitPeriod\":\"2 Years\",\"industryOcc\":\"012:Office desk based duties - Executive or manager\",\"salary\":\"5555\",\"clientname\":\"metaus\",\"lastName\":\"HOSTB\",\"firstName\":\"HOST\",\"dob\":\"01/11/1958\",\"existingTerm\":true,\"memberType\":\"INDUSTRY OCCUPATION\"}";
		mockMvc.perform(post("/initiate").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void eapplyInitiateAeis() throws Exception {
		String input = "{\"fund\":\"AEIS\",\"mode\":\"change\",\"age\":60,\"name\":\"HOST HOSTB\",\"appnumber\":1517301230928,\"gender\":\"Female\",\"country\":\"Australia\",\"fifteenHr\":\"Yes\",\"deathAmt\":0,\"tpdAmt\":0,\"ipAmt\":500,\"waitingPeriod\":\"90 Days\",\"benefitPeriod\":\"2 Years\",\"industryOcc\":\"012:Office desk based duties - Executive or manager\",\"salary\":\"5555\",\"clientname\":\"metaus\",\"lastName\":\"HOSTB\",\"firstName\":\"HOST\",\"dob\":\"01/11/1958\",\"existingTerm\":true,\"memberType\":\"INDUSTRY OCCUPATION\"}";
		mockMvc.perform(post("/initiate").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void eapplyInitiateCorp() throws Exception {
		String input = "{\"fund\":\"S1173\",\"mode\":\"change\",\"age\":60,\"name\":\"HOST HOSTB\",\"appnumber\":1517301230928,\"gender\":\"Female\",\"country\":\"Australia\",\"fifteenHr\":\"Yes\",\"deathAmt\":0,\"tpdAmt\":0,\"ipAmt\":500,\"waitingPeriod\":\"90 Days\",\"benefitPeriod\":\"2 Years\",\"industryOcc\":\"012:Office desk based duties - Executive or manager\",\"salary\":\"5555\",\"clientname\":\"metaus\",\"lastName\":\"HOSTB\",\"firstName\":\"HOST\",\"dob\":\"01/11/1958\",\"existingTerm\":true,\"memberType\":\"INDUSTRY OCCUPATION\"}";
		mockMvc.perform(post("/initiate").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void calculateTPD() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculate.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateTPD").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	@Test
	public void calculateIP() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculate.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateIP").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	@Test
	public void calculateDeath() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "calculate.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateDeath").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	// for /convertCover valid FundCode for HOST
	@Test
	public void convertCoverHOST() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerConvertCoverHOST.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/convertCover").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].exCoverType").exists())
				.andExpect(jsonPath("$.[0].exCoverType").value("TpdFixed"))
				.andExpect(jsonPath("$.[0].exCoverAmount").exists())
				.andExpect(jsonPath("$.[0].exCoverAmount").value(500000))
				.andExpect(jsonPath("$.[0].convertedAmount").exists())
				.andExpect(jsonPath("$.[0].convertedAmount").value("251"));
	}

	// for /convertCover valid FundCode for CARE
	@Test
	public void convertCoverCARE() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerConvertCoverCARE.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/convertCover").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].exCoverType").exists())
				.andExpect(jsonPath("$.[0].exCoverType").value("TpdFixed"))
				.andExpect(jsonPath("$.[0].exCoverAmount").exists())
				.andExpect(jsonPath("$.[0].exCoverAmount").value(500000))
				.andExpect(jsonPath("$.[0].convertedAmount").doesNotExist())

				;
	}

	// for convertCover InvalidFundCode 
	@Test
	public void convertCoverInvalidFundCode() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerConvertCoverInvalidFundCode.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/convertCover").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].exCoverType").exists())
				.andExpect(jsonPath("$.[0].exCoverType").value("TpdFixed"))
				.andExpect(jsonPath("$.[0].exCoverAmount").exists())
				.andExpect(jsonPath("$.[0].exCoverAmount").value(500000))
				.andExpect(jsonPath("$.[0].convertedAmount").doesNotExist())

				;
	}
	
	
	

	// for /convertCover InvalidtpdExistingAmount HOST
	@Test
	public void convertCoverInvalidtpdExistingAmount() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerConvertCoverInvalidTpdExistingAmount.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/convertCover").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].exCoverType").exists())
				.andExpect(jsonPath("$.[0].exCoverType").value("TpdFixed"))
				.andExpect(jsonPath("$.[0].exCoverAmount").exists()).andExpect(jsonPath("$.[0].exCoverAmount").value(0))
				.andExpect(jsonPath("$.[0].convertedAmount").exists())
				.andExpect(jsonPath("$.[0].convertedAmount").value(0));
	}

	// for /convertCover Invalid exTpdCoverType HOST
	@Test
	public void convertCoverInvalidexTpdCoverType() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerConvertCoverInvalidexTpdCoverType.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/convertCover").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].exCoverType").exists())
				.andExpect(jsonPath("$.[0].exCoverType").value("DcFixed"))
				.andExpect(jsonPath("$.[0].exCoverAmount").exists())
				.andExpect(jsonPath("$.[0].exCoverAmount").value(500000))
				.andExpect(jsonPath("$.[0].convertedAmount").exists())
				.andExpect(jsonPath("$.[0].convertedAmount").value(251));
	}
	
	
// for /calculateFUL valid FundCode for HOST
@Test
public void savecalculateFULHOST() throws Exception {
	TestUtils util = new TestUtils();
	fileName1 = "QuoteControllerCalculateAll.json";
	testData1 = util.createTestDataForQuote(fileName1);
	mockMvc.perform(post("/calculateFUL").contentType(MediaType.APPLICATION_JSON).content(testData1)
			.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
			;
}

// for /calculateFUL valid FundCode for AEIS
@Test
public void savecalculateFULAEIS() throws Exception {
	TestUtils util = new TestUtils();
	fileName1 = "calculateAllValidData.json";
	testData1 = util.createTestDataForQuote(fileName1);
	mockMvc.perform(post("/calculateFUL").contentType(MediaType.APPLICATION_JSON).content(testData1)
			.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
			;
}

// for /calculateFUL valid FundCode for CARE
@Test
public void savecalculateFULCARE() throws Exception {
	TestUtils util = new TestUtils();
	fileName1 = "CALCULATEaLLCareSuperValidData.json";
	testData1 = util.createTestDataForQuote(fileName1);
	mockMvc.perform(post("/calculateFUL").contentType(MediaType.APPLICATION_JSON).content(testData1)
			.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
			;
}

//for /calculateAllOld for HOST
	@Test
	public void calculateAllOldHOST() throws Exception {
		testData1 = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdNalkxTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJNU9HRTNNRGcxWmkxbE1UVmxMVFEyWWpNdE9ERTVOeTAyTTJOak1EWmhOalJtTURRaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5nZ1BaTTZmRk9JRDhxYUw1aEpvYzJpWTVkZjE5bEEtSDU0b19HUDl5aC1Z\"}";
		mockMvc.perform(post("/calculateAllOld")
				.contentType(MediaType.APPLICATION_JSON).content(testData1).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		;
}
	
	
	//for /calculateAll valid FundCode for HOST
	@Test
	public void savecalculateAllnew1() throws Exception {
		TestUtils util=new TestUtils();
	    fileName1 = "IPQuote1.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll")
				.contentType(MediaType.APPLICATION_JSON).content(testData1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
			
	}
	
	//for /calculateAll valid FundCode for HOST
	@Test
	public void savecalculateAllnew2() throws Exception {
		TestUtils util=new TestUtils();
	    fileName1 = "IPQuote2.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll")
				.contentType(MediaType.APPLICATION_JSON).content(testData1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
			
	}
	
	//for /calculateAll valid FundCode for HOST
	@Test
	public void savecalculateAllnew3() throws Exception {
		TestUtils util=new TestUtils();
	    fileName1 = "IPQuote3.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll")
				.contentType(MediaType.APPLICATION_JSON).content(testData1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		
	}
	
	//for /calculateAll valid FundCode for HOST
	@Test
	public void savecalculateAllnew4() throws Exception {
		TestUtils util=new TestUtils();
	    fileName1 = "IPQuote4.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll")
				.contentType(MediaType.APPLICATION_JSON).content(testData1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
	
	}
	
	//for /calculateAll valid FundCode for HOST
	@Test
	public void savecalculateAllnew5() throws Exception {
		TestUtils util=new TestUtils();
	    fileName1 = "IPQuote5.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateAll")
				.contentType(MediaType.APPLICATION_JSON).content(testData1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
			
	}
	
	// for /calculateFUL valid FundCode for HOST
	@Test
	public void savecalculateFULHOSTChangeCover() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "IPQuote1.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateFUL").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
				
	}
	
	@Test
	public void calculateINGDALLTest() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAll.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		String testData2 = null;
		String fileName2 = null;
		fileName2 = "QuoteControllerINGDCalculateAllHeavybluecollar.json";
		testData2 = util.createTestDataForQuote(fileName2);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData2)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
				
	}
	
	@Test
	public void calculateINGDALLTestProfessionalonlyIP() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAllonlyIpProfessional.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	

	@Test
	public void calculateINGDALLTestProfessionalonlyDeath() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAllonlyDeathProfessional.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	

	@Test
	public void calculateINGDALLTestProfessionalonlyTPD() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAllonlyTpdProfessional.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void calculateINGDALLTestProfessional() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAllProfessional.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	@Test
	public void calculateINGDALLTestBlueCollar() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAllBlueCollar.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void calculateINGDALLTestLightBlueCollar() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "QuoteControllerINGDCalculateAllLightBlueCollar.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/calculateINGDAll").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	@Test
	public void getINGDDeathLifeStageCoverAmountAge() throws Exception {
		mockMvc.perform(post("/calculateINGDDeathLifeCoverAmount").param("fundCode", "INGD").param("age", "25").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	
				
	}
	
	@Test
	public void getINGDTPDLifeStageCoverAmountAge() throws Exception {
		mockMvc.perform(post("/calculateINGDTPDLifeCoverAmount").param("fundCode", "INGD").param("age", "25").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	
				
	}
	
	@Test
	public void getINGDDeathLifeStageCoverAmount() throws Exception {
		mockMvc.perform(post("/calculateINGDDeathLifeCoverAmount").param("fundCode", "INGD").param("age", "45").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	
				
	}
	
	@Test
	public void getINGDTPDLifeStageCoverAmount() throws Exception {
		mockMvc.perform(post("/calculateINGDTPDLifeCoverAmount").param("fundCode", "INGD").param("age", "45").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
				
	}
	
	@Test
	public void getINGDDeathLifeStageCoverAmountage() throws Exception {
		mockMvc.perform(post("/calculateINGDDeathLifeCoverAmount").param("fundCode", "INGD").param("age", "59").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	
				
	}
	
	@Test
	public void getINGDTPDLifeStageCoverAmountage() throws Exception {
		mockMvc.perform(post("/calculateINGDTPDLifeCoverAmount").param("fundCode", "INGD").param("age", "59").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	
				
	}
	
	@Test
	public void getINGDDeathLifeStageCoverAmountagebeyondlimit() throws Exception {
		mockMvc.perform(post("/calculateINGDDeathLifeCoverAmount").param("fundCode", "INGD").param("age", "80").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	
				
	}
	
	@Test
	public void getINGDTPDLifeStageCoverAmountagebeyondlimit() throws Exception {
		mockMvc.perform(post("/calculateINGDTPDLifeCoverAmount").param("fundCode", "INGD").param("age", "80").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
				
	}
	
	
	@Test
	public void getAuraTypewithNull() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "AuraType.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/getAuraType").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void getAuraTypewithallValues() throws Exception {
		TestUtils util = new TestUtils();
		fileName1 = "AuraTypeallValues.json";
		testData1 = util.createTestDataForQuote(fileName1);
		mockMvc.perform(post("/getAuraType").contentType(MediaType.APPLICATION_JSON).content(testData1)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
}
