package au.com.metlife.eapply.underwriting.model;

import java.io.Serializable;
import java.util.List;

public class AuraSection implements Serializable{

	private String sectionName;	
	private Boolean sectionStatus= Boolean.FALSE;
	private Boolean collapse = Boolean.FALSE;
	public Boolean getCollapse() {
		return collapse;
	}
	public void setCollapse(Boolean collapse) {
		this.collapse = collapse;
	}
	public Boolean getSectionStatus() {
		return sectionStatus;
	}
	public void setSectionStatus(Boolean sectionStatus) {
		this.sectionStatus = sectionStatus;
	}
	private List<AuraQuestion> questions;
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public List<AuraQuestion> getQuestions() {
		return questions;
	}
	public void setQuestions(List<AuraQuestion> questions) {
		this.questions = questions;
	}
	
	

}
