import angular from 'angular';

// Create the module where our functionality can attach to
let directivesModule = angular.module('app.directives', []);


import CheckboxAndRadioUpdate from './radio-btn-update.dir';
directivesModule.directive('checkboxAndRadioUpdate', () => new CheckboxAndRadioUpdate());

import Loading from './loading.dir';
directivesModule.directive('loading', ['$http', ($http) => new Loading($http)]);

import format from './format.dir';
directivesModule.directive('format', ['$filter', ($filter) => new format($filter)]);

import numbersOnly from './numbers-only.dir';
directivesModule.directive('numbersOnly', () => new numbersOnly());

import dateOnly from './date-only.dir';
directivesModule.directive('dateOnly', () => new dateOnly());

export default directivesModule;
