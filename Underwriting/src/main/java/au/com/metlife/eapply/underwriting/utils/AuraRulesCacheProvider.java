package au.com.metlife.eapply.underwriting.utils;

import java.io.Serializable;
import java.util.HashMap;

import org.drools.KnowledgeBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;

@Component
@Scope("singleton")
public class AuraRulesCacheProvider implements Serializable{
	final String CLASS_NAME = "AuraRulesCacheProvider";
	
	private java.util.HashMap ruleMap;
	
	private String filePath;
    
    public String getFilePath() {
           return filePath;
    }

    public void setFilePath(String filePath) {
           this.filePath = filePath;
    }


	public java.util.HashMap getRuleMap() {
		return ruleMap;
	}

	public AuraRulesCacheProvider(@Value("${spring.ruleFilePath}") String ruleFilePath){
		System.out.println("ruleFilePath>>"+ruleFilePath);
        this.filePath = ruleFilePath;
		loadProperties();
	}
		
	public void loadProperties()	{
		try{		
			ruleMap = getDroolsSet();
			if(ruleMap  == null){
				throw(new Exception("Drools not initialized properly. Terminating process."));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private HashMap getDroolsSet() throws Exception	{
		HashMap obj_props = new HashMap();
		//String RULE_FILE_LOC = "E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";
		try {
			obj_props.put(ApplicationConstants.AURA_QUESTION_FILTER_RULE,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.AURA_QUESTION_FILTER_RULE));
			System.out.println(">>>>Catching completed---AURA_QUESTION_FILTER_RULE");
			obj_props.put(ApplicationConstants.AURA_QUESTION_ID_BY_PROD,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.AURA_QUESTION_ID_BY_PROD));
			System.out.println(">>>>Catching completed---AURA_QUESTION_ID_BY_PROD");
			obj_props.put(ApplicationConstants.POST_PROCESS_MASTER,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_MASTER));
			System.out.println(">>>>Catching completed---POST_PROCESS_MASTER");
			obj_props.put(ApplicationConstants.POST_PROCESS_1,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_1));
			System.out.println(">>>>Catching completed---POST_PROCESS_1");
			obj_props.put(ApplicationConstants.POST_PROCESS_2,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_2));
			System.out.println(">>>>Catching completed---POST_PROCESS_2");
			obj_props.put(ApplicationConstants.POST_PROCESS_3,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_3));
			System.out.println(">>>>Catching completed---POST_PROCESS_3");
			obj_props.put(ApplicationConstants.POST_PROCESS_4,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_4));
			System.out.println(">>>>Catching completed---POST_PROCESS_4");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj_props;
	}

	
	public KnowledgeBase getDroolsFromCache(String arg_property)	{	
		return (KnowledgeBase)ruleMap.get(arg_property);
	}
	

	

}
