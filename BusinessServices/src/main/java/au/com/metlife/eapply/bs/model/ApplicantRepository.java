package au.com.metlife.eapply.bs.model;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;
public interface  ApplicantRepository extends JpaRepository<Applicant, String>,QueryByExampleExecutor<Applicant>{
	
	@Query("SELECT t FROM Applicant t where t.firstname = ?1 AND t.birthdate = ?2")
    public List<Applicant> findByFNamesAndDob(String firstname, Timestamp dob);
	
	@Query("SELECT t FROM Applicant t where t.lastname = ?1 AND t.birthdate = ?2")
    public List<Applicant> findByLNamesAndDob(String lastname, Timestamp dob);
	
	@Query("SELECT t FROM Applicant t where t.clientrefnumber = ?1")
    public List<Applicant> findByClientRefNo(String clientRefNo);
  
	//List<Applicant> find(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("dob") Timestamp dob);
}