package au.com.metlife.eapply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class MicroServicesApplication {
	private static final Logger log = LoggerFactory.getLogger(MicroServicesApplication.class);

	public static void main(String[] args) {
		log.info("Initialize microservice application");
		SpringApplication.run(MicroServicesApplication.class, args);
	}
}
