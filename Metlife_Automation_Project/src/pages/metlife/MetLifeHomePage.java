package pages.metlife;

import supportlibraries.ScriptHelper;

public class MetLifeHomePage extends MasterPage {

	public MetLifeHomePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		if (!driver.getTitle().contains("MetLife Home")) {
			throw new IllegalStateException(
					"MetLife Home page expected, but not displayed!");
		}
	}

}
