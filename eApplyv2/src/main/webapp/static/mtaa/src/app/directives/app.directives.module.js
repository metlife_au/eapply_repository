angular.module('Metlife.mtaa.directives', [])
  .directive('loading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (value) {
          if (value) {
            element.removeClass('ng-hide');
          } else {
            element.addClass('ng-hide');
          }
        });
      }
    };
  }])
  .directive('checkboxAndRadioUpdate', ['$http', function ($http) {
    return {
      replace: false,
      require: 'ngModel',
      scope: false,
      link: function (scope, element, attrs, ngModelCtrl) {
        element.on('change', function () {
            scope.$apply(function () {
                ngModelCtrl.$setViewValue(element[0].type.toLowerCase() === 'radio' ? element[0].value : element[0].checked);
              });
        });
      }
    };
  }])
  .directive('numbersOnly', [function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = text.replace(/[^0-9]/g, '');

            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('wholeNumbersOnly', [function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = (Math.round(text.replace(/[^0-9.]/g, ''))).toString();

            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('roundUpNumbers', [function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = (Math.round(text.replace(/[^0-9.]/g, '')/1000)*1000).toString();

            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('phoneOnly', function () {
	    return {
	        require: 'ngModel',
	        restrict: 'A',
	        link: function (scope, element, attrs, modelCtrl) {

	          modelCtrl.$parsers.push(function (inputValue) {
	            var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;

	            if (transformedInput !== inputValue) {
	              modelCtrl.$setViewValue(transformedInput);
	              modelCtrl.$render();
	            }

	            return transformedInput;
	          });

	          $(element).on('keyup', function () {
	            var TempVal = $(this).val().replace(/[,.]/g, '');
	            var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	          });
	        }
	      };
	    })
	    .directive('helpText', function() {
	  	  return {
	  	    restrict: 'E',
	  	    scope: {
	  	      show: '=',
	  	      msg: '='
	  	    },
	  	    replace: true,
	  	    transclude: true,
	  	    link: function(scope, element, attrs) {
	  	      scope.dialogStyle = {};
	  	      if (attrs.width)
	  	        scope.dialogStyle.width = attrs.width;
	  	      if (attrs.height)
	  	        scope.dialogStyle.height = attrs.height;
	  	      scope.hideModal = function() {
	  	        scope.show = false;
	  	      };
	  	    },
	  	    template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content'><h4 class='modal-title aligncenter' id='myModalLabel'> Helpful hints</h4><div class='row  rowcustom'><div class='col-sm-12'><p class='aligncenter'><div id='tips_text'>{{msg}}</div></p></div></div><div class='modal-footer mt0 pt0'><div class='row'><div class='col-sm-4'></div><div class='col-sm-4 col-xs-12'><a ng-click='hideModal()'><button class='btn btn-primary w100p' type='button'>Close</button></a> </div><div class='col-sm-4'></div> </div></div></div></div></div>"
	  	  };
	  	})
	  	.directive('format', ['$filter', function ($filter) {
    return {
      require: '?ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ctrl) {
        if (!ctrl) {return;}
        elem.bind('focus', function (event) {
          var newVal = $(this).val().replace(/,/g, '');
          $(this).val(newVal);
        });
        elem.bind('blur', function(event) {
          var newVal = $(this).val().replace(/,/g, '');
          if(newVal) {
            $(this).val($filter(attrs.format)(newVal));
          } else {
            elem.val(newVal);
          }  
        });
        ctrl.$formatters.unshift(function (a) {
          return $filter(attrs.format)(ctrl.$modelValue);
        });
        ctrl.$parsers.unshift(function (viewValue) {
          var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
          if(plainNumber) {
            elem.val($filter(attrs.format)(plainNumber));
          } else {
            elem.val(plainNumber);
          }  
          return plainNumber;
        });
      }
    };
}]);