package businesscomponents;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


public class PDFValidation extends ReusableLibrary  {
	
public PDFValidation(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

static String getText(File pdfFile) throws IOException {
	    PDDocument doc = PDDocument.load(pdfFile);
	    return new PDFTextStripper().getText(doc);
	}
public  void pdfValidation_MTAA(){
		
	try {
		if(driver.getPageSource().contains("Application number")){
		WebElement ApplicationNo=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p"));
		String ApplicationNumber=ApplicationNo.getText();
		
		System.out.println(ApplicationNumber);
		sleep(1000);
	   
	    //System.out.println("Text in PDF: " + text);
	    //System.out.println("PDF has been extracted");
	    String Rpath="C:\\Users\\300312\\Downloads\\";
	    String CARE ="MTAA_";
	    String Title ="PVTTESTDONTLOAD_";
	    
	    String pdf=".pdf";
	    String Fullpath=Rpath+CARE+Title+ApplicationNumber+pdf;
	    String Pdfname=CARE+Title+ApplicationNumber;
	    System.out.println(Fullpath);
	    String pdftext = getText(new File(Fullpath));
	    String AppNo=ApplicationNumber;
	    String PhoneNo="0862222222";
	    String Email="test@mail.com";
	   
	    report.updateTestLog("PDF Name", "PDF File: "+Pdfname,Status.DONE);

 	   if(pdftext.contains(AppNo)){
	    	  
	    	  // System.out.println("Application Number is as expected ");
	    	   report.updateTestLog("Application Nmber", "Application number is as expected: "+ApplicationNumber,Status.PASS);
	       }
	       else{
	    	   
	    	   report.updateTestLog("Application Nmber", "Application number is not as expected: ",Status.FAIL);
	       }
	       
	       if(pdftext.contains(PhoneNo)){
	    	   //System.out.println("Contact Number is as expected");
	    	   report.updateTestLog("Contact Number", "Contact number is as expected: "+PhoneNo,Status.PASS);
	    	  
	       }
	       else{
	    	  // System.out.println("Contact Number doesn't match");
	    	   report.updateTestLog("Contact Nmber", "Contact number is not as expected: ",Status.FAIL);
	    	   
	       }
	       
	       if(pdftext.contains(Email)){
	    	  // System.out.println("Contact Number is as expected");
	    	   report.updateTestLog("Email", "Email Id is as expected: "+Email,Status.PASS);
	    	  
	       }
	       else{
	    	  // System.out.println("Contact Number doesn't match");
	    	   report.updateTestLog("Email", "Email Id is not as expected: ",Status.FAIL);
	    	   
	       }
	    
	} 
		else{
			 report.updateTestLog("No PDF", "PDF Not generated as the application is declined",Status.DONE);
		}
	}
	
	
	catch (IOException e) {
	    e.printStackTrace();
	}
	}
	
public  void pdfValidation_NSFS(){
		
		String Gender = dataTable.getData("NSFS_TransferCover", "Gender");
	try {
		if(driver.getPageSource().contains("Application number")){
		WebElement ApplicationNo=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p"));
		String ApplicationNumber=ApplicationNo.getText();
		
		System.out.println(ApplicationNumber);
		sleep(1000);
	   
	    //System.out.println("Text in PDF: " + text);
	    //System.out.println("PDF has been extracted");
	    String Rpath="C:\\Users\\300312\\Downloads\\";
	    String CARE ="NSFS_";
	   
	    String Title ="PVTTestDontload_";
	    
	    String pdf=".pdf";
	    String Fullpath=Rpath+CARE+Gender+Title+ApplicationNumber+pdf;
	    String Pdfname=Rpath+CARE+Gender+Title+ApplicationNumber;
	   // System.out.println(Fullpath);
	    String pdftext = getText(new File(Fullpath));
	    String AppNo=ApplicationNumber;
	    String PhoneNo="0862222222";
	    String Email="test@mail.com";
	   
	    report.updateTestLog("PDF Name", "PDF File Name is: "+Pdfname,Status.DONE);

 	   if(pdftext.contains(AppNo)){
	    	  
	    	  // System.out.println("Application Number is as expected ");
	    	   report.updateTestLog("Application Nmber", "Application number is as expected: "+ApplicationNumber,Status.PASS);
	       }
	       else{
	    	   
	    	   report.updateTestLog("Application Nmber", "Application number is not as expected: ",Status.FAIL);
	       }
	       
	       if(pdftext.contains(PhoneNo)){
	    	   //System.out.println("Contact Number is as expected");
	    	   report.updateTestLog("Contact Number", "Contact number is as expected: "+PhoneNo,Status.PASS);
	    	  
	       }
	       else{
	    	  // System.out.println("Contact Number doesn't match");
	    	   report.updateTestLog("Contact Nmber", "Contact number is not as expected: ",Status.FAIL);
	    	   
	       }
	       
	       if(pdftext.contains(Email)){
	    	  // System.out.println("Contact Number is as expected");
	    	   report.updateTestLog("Email", "Email Id is as expected: "+Email,Status.PASS);
	    	  
	       }
	       else{
	    	  // System.out.println("Contact Number doesn't match");
	    	   report.updateTestLog("Email", "Email Id is not as expected: ",Status.FAIL);
	    	   
	       }
	    
	} 
		else{
			 report.updateTestLog("No PDF", "PDF Not generated as the application is declined",Status.DONE);
		}
	}
	
	
	catch (IOException e) {
	    e.printStackTrace();
	}
	}
	
public  void pdfValidation_HOSTPLUS(){
		
		
	try {
		String PDF = dataTable.getData("HOSTPLUS_PDF", "PDF");
		String Date = dataTable.getData("HOSTPLUS_PDF", "Date");
		String ApplicatioNo = dataTable.getData("HOSTPLUS_PDF", "Date");
		String Email = dataTable.getData("HOSTPLUS_PDF", "Date");
		String DOB = dataTable.getData("HOSTPLUS_PDF", "Date");
		String DOD = dataTable.getData("HOSTPLUS_PDF", "Date");
		String PrivacyStatement = dataTable.getData("HOSTPLUS_PDF", "Date");
		String DeathPremium = dataTable.getData("HOSTPLUS_PDF", "Date");
		String TPDPremium = dataTable.getData("HOSTPLUS_PDF", "Date");
		String IPPremium = dataTable.getData("HOSTPLUS_PDF", "Date");
		String GS = dataTable.getData("HOSTPLUS_PDF", "Date");
		
	   // String Rpath="C:\\Users\\300312\\Downloads\\50eb5cdd1f522ddc354a0ec95f054212210fa6e8a879695cd4808f15ded647fdf8ebe742e31cc5dd34d868862251b414.pdf";
	   
	    
	
	    String pdftext = getText(new File(PDF));
	  
	    if(pdftext.contains("1499340166011")){
	    	report.updateTestLog("Application No", "1499340166011 displayed as Expected", Status.PASS);
	    }
	    else{
	    	report.updateTestLog("Application No", "Not displayed as Expected", Status.FAIL);
	    }
	    
	    if(pdftext.contains("test@mail.com")){
	    	report.updateTestLog("Email", "test@mail.com displayed as Expected", Status.PASS);
	    }
	    else{
	    	report.updateTestLog("Email", "Not displayed as Expected", Status.FAIL);
	    }
	    if(pdftext.contains("22/11/1990")){
	    	report.updateTestLog("Date of Birth", "22/11/1990 displayed as Expected", Status.PASS);
	    }
	    else{
	    	report.updateTestLog("Date of Birth", "Not displayed as Expected", Status.FAIL);
	    }
	    
	    if(pdftext.contains("$7.10")){
	    	report.updateTestLog("Death Premium", " $7.10 displayed as Expected", Status.PASS);
	    }
	    else{
	    	report.updateTestLog("Death Premium", "Not displayed as Expected", Status.FAIL);
	    }
	    if(pdftext.contains("$6.35")){
	    	report.updateTestLog("TPD Premium", "$6.35 displayed as Expected", Status.PASS);
	    }
	    else{
	    	report.updateTestLog("TPD Premium", "Not displayed as Expected", Status.FAIL);
	    }
	    
	    if(pdftext.contains("$1.34")){
	    	report.updateTestLog("IP Premium", "$1.34 displayed as Expected", Status.PASS);
	    }
	    else{
	    	report.updateTestLog("IP Premium", "Not displayed as Expected", Status.FAIL);
	    }
	

	}
	
	
	catch (IOException e) {
	    e.printStackTrace();
	}
	}
	
public  void pdfValidation_REIS(){
		
		String FirstName = dataTable.getData("REIS_PDF", "FirstName");
		String SurName = dataTable.getData("REIS_PDF", "SurName");
		String MemberNo = dataTable.getData("REIS_PDF", "MemberNo");
		String ApplicationNumber = dataTable.getData("REIS_PDF", "ApplicationNumber");
	try {
		
		
		
		
	   
	    //System.out.println("Text in PDF: " + text);
	    //System.out.println("PDF has been extracted");
	    String Rpath="C:\\Users\\300312\\Downloads\\";
	    String CARE ="REIS_";
	   
	    
	    String AppNo = ApplicationNumber;
	    String pdf=".pdf";
	    String Fullpath=Rpath+CARE+FirstName+SurName+"_"+ApplicationNumber+pdf;
	    String Pdfname=CARE+FirstName+SurName+"_"+ApplicationNumber;
	    System.out.println(Fullpath);
	    String pdftext = getText(new File(Fullpath));
	   
	   
	   
	    report.updateTestLog("PDF Name", "PDF File: "+Pdfname,Status.DONE);

 	   if(pdftext.contains(AppNo)){
	    	  
	    	
	    	   report.updateTestLog("Application Nmber", "Application number is as expected: "+ApplicationNumber,Status.PASS);
	       }
	       else{
	    	   
	    	   report.updateTestLog("Application Nmber", "Application number is not as expected: ",Status.FAIL);
	       }
	       
	       if(pdftext.contains(MemberNo)){
	    	   
	    	   report.updateTestLog("Contact Number", "Member Number is as expected: "+MemberNo,Status.PASS);
	    	  
	       }
	       else{
	    	  
	    	   report.updateTestLog("Contact Nmber", "Member Number is not as expected: ",Status.FAIL);
	    	   
	       }
	       
	}
	
	
	catch (IOException e) {
	    e.printStackTrace();
	}
	}
}

	


