import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);


import CommonService from './common.service';
servicesModule.service('CommonAPI', CommonService);

import DataService from './eApply.data.service';
servicesModule.service('EapplyData', DataService);

import AuraService from './aura.service';
servicesModule.service('AuraAPI', AuraService);

export default servicesModule;
