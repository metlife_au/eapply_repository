'use strict';
CareSuperApp.factory('urlService',['$http','$q', function($http,$q){
	var headerConfig = {
                headers : {
                    'Content-Type': 'application/json'
                }
            }  
        
    var urlObject = {};
    urlObject.getUrls = function(){
    	var defer = $q.defer();
    	$http.get('CareSuperUrls.properties',headerConfig).then(function(res){
    		urlObject = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error fetching urls " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    };
    urlObject.setUrlList = function(list){
    	sessionStorage.setItem('urlList',JSON.stringify(list));
    };
    urlObject.getUrlList = function(){
    	return JSON.parse(sessionStorage.getItem('urlList'));
    };
    return urlObject; 
}]);

CareSuperApp.factory('QuoteService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getList = function(url,fundCode){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'fundCode': fundCode
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while getting industries " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.service('tokenNumService', function() {
	var _self = this;
	_self.setTokenId = function (tempToken) {
		_self.tokenId = tempToken;
  }
	_self.getTokenId = function () {
	  return _self.tokenId;
  }
});
CareSuperApp.service('deathCoverService', function() {
	var _self = this;
	_self.setDeathCover = function (tempToken) {
		//_self.deathCover = tempToken;
		sessionStorage.setItem('deathCoverObject',JSON.stringify(tempToken));
  }
	_self.getDeathCover = function () {
		//return _self.deathCover;
		return JSON.parse(sessionStorage.getItem('deathCoverObject'));
  }
});

CareSuperApp.service('tpdCoverService', function() {
	var _self = this;
	_self.setTpdCover = function (tempToken) {
		//_self.tpdCover = tempToken;
		sessionStorage.setItem('tpdCoverObject',JSON.stringify(tempToken));
  }
	_self.getTpdCover = function () {
		 //return _self.tpdCover;
		return JSON.parse(sessionStorage.getItem('tpdCoverObject'));
  }
});

CareSuperApp.service('ipCoverService', function() {
	var _self = this;
	_self.setIpCover = function (tempToken) {
		//_self.ipCover = tempToken;
		sessionStorage.setItem('ipCoverObject',JSON.stringify(tempToken));
  }
	_self.getIpCover = function () {
		//return _self.ipCover;
		return JSON.parse(sessionStorage.getItem('ipCoverObject'));
  }
});

CareSuperApp.service('persoanlDetailService', function() {
	var _self = this;
	_self.setMemberDetails = function (tempToken) {
		//_self.deathCover = tempToken;
		sessionStorage.setItem('memberDetailsObject',JSON.stringify(tempToken));
  }
	_self.getMemberDetails = function () {
		//return _self.deathCover;
		return JSON.parse(sessionStorage.getItem('memberDetailsObject'));
  }
});

CareSuperApp.service('newMemberOfferService', function() {
	var _self = this;
	_self.setNewMemberOfferFlag = function (tempToken) {
		_self.newMemberOfferFlag = tempToken;
  }
	_self.getNewMemberOfferFlag = function () {
	  return _self.newMemberOfferFlag;
  }
});

CareSuperApp.factory('getclientData', function($http, $q,tokenNumService){    	      
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
    	var defer = $q.defer();
    	
		$http.post(url,tokenNumService,{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
				
			defer.resolve(response);
		}, function(response) {
			defer.reject(response);
		});
	 	return defer.promise;
    }
    
    return myFactObj;
});

/*CareSuperApp.factory('getclientData', function($http, $q,tokenNumService){     	    
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
    	var defer = $q.defer();
    	
    	$http.defaults.headers.common['Authorization'] = 'C3PO R2D2';
		$http.get(url,{
			headers: {'Authorization':  tokenNumService.getTokenId()},
			params:{ tokenid: tokenNumService.getTokenId()}}).then(function(response) {
				defer.resolve(response);
		}, function(response) {
			defer.reject(response);
		});
	 	return defer.promise;
    }
    
    return myFactObj;
});*/

CareSuperApp.factory('OccupationService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getOccupationList = function(url,fundId,induCode){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'fundId': fundId,
    		'induCode': induCode
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while getting occupations " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

/*CareSuperApp.factory('AppNumberService', function($resource){
	return $resource('http://localhost\:8087/getUniqueNumber', {}, {
		getAppnumber: {
			headers: {'Content-Type': 'application/json'},
			method: 'GET',
			params: {},
			isArray: false
		}
	});
});*/

CareSuperApp.factory('appNumberService', function($http, $q,tokenNumService){     	      
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
    	var defer = $q.defer();
		$http.get(url,{
			headers: {'Authorization':  tokenNumService.getTokenId()},
			params:{}}).then(function(response) {
				defer.resolve(response);
		}, function(response) {
			defer.reject(response);
		});
	 	return defer.promise;
    }
    
    return myFactObj;
});

CareSuperApp.factory('CalculateService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };  
        
    var factory = {};
    factory.calculate = function(data,url){
    	var defer = $q.defer();
    	$http.post(url, data, headerConfig).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while calculating.." + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('CalculateDeathService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.calculateAmount = function(url,data){
    	var defer = $q.defer();
    	$http.post(url, data, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while calculating death premium " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('CalculateTPDService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.calculateAmount = function(url,data){
    	var defer = $q.defer();
    	$http.post(url, data, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while calculating TPD premium " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('CalculateIPService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.calculateAmount = function(url,data){
    	var defer = $q.defer();
    	$http.post(url, data, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while calculating TPD premium " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('TransferCalculateService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };  
        
    var factory = {};
    factory.calculate = function(data,url){
    	var defer = $q.defer();
    	$http.post(url, data, headerConfig).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while calculating.." + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('NewOccupationService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getOccupation = function(url,fundId,occName){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'fundId': fundId,
    		'occName': occName
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while getting occupations " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('MaxLimitService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getMaxLimits = function(url,fundCode,memberType,manageType){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'fundCode': fundCode,
    		'memberType': memberType,
    		'manageType':manageType
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while getting limits " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('ConvertService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };  
        
    var factory = {};
    factory.convert = function(url,data){
    	var defer = $q.defer();
    	$http.post(url, data, headerConfig).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while calculating.." + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('getAuraTransferData', function($http, $q,auraTransferInitiateService,auraInputService,tokenNumService){    	      
     var myFactObj = {};
     myFactObj.requestObj = function (url) {
     	var defer = $q.defer();
     	
 		$http.post(url,auraInputService,{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
 				
 			defer.resolve(response);
 		}, function(response) {
 			defer.reject(response);
 		});
 	 	return defer.promise;
     }
     
     return myFactObj;
 });
 
 CareSuperApp.service('auraTransferInitiateService', function() {
 	var _self = this;
 	_self.setInput = function (tempInput) {
 		_self.input = tempInput;
   }
 	_self.getInput = function () {
 	  return _self.input;
   }
 });
 
 CareSuperApp.factory('submitEapply', function($http, $q,auraResponseService,tokenNumService){     	
     
     var myFactObj = {};       
    
     myFactObj.reqObj = function (url) {
     	 var defer = $q.defer(); 
 		$http.post(url,auraResponseService.getResponse(),
 				{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
 				
 			defer.resolve(response);	
 				
 		}, function(response) {
 			defer.reject(response);
 		});
 	 	return defer.promise;
     }
     
     return myFactObj;
 });
 
 CareSuperApp.factory('auraPostfactory', function($http, $q,auraResponseService,tokenNumService){     	
         
     var myFactObj = {};       
    
     myFactObj.reqObj = function (url) {
     	 var defer = $q.defer(); 
 		$http.post(url,auraResponseService.getResponse(),
 				{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
 				
 			defer.resolve(response);	
 				
 		}, function(response) {
 			defer.reject(response);
 		});
 	 	return defer.promise;
     }
     
     return myFactObj;
 });   

 CareSuperApp.factory('submitAura', function($http, $q, auraInputService,tokenNumService){     	     
	     var myFactObj = {};
	     myFactObj.requestObj = function (url) {
	     	var defer = $q.defer();    
	     	
	 		$http.post(url,auraInputService,
	 				{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
	 				
	 			defer.resolve(response);
	 		}, function(response) {
	 			defer.reject(response);
	 		});
	 	 	return defer.promise;
	     }
	     
	     return myFactObj;
	 });
 
CareSuperApp.factory('saveEapply', function($http, $q,auraResponseService,tokenNumService){     	
     
     var myFactObj = {};       
    
     myFactObj.reqObj = function (url) {
     	 var defer = $q.defer(); 
 		$http.post(url,auraResponseService.getResponse(),
 				{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
 				
 			defer.resolve(response);	
 				
 		}, function(response) {
 			defer.reject(response);
 		});
 	 	return defer.promise;
     }
     
     return myFactObj;
 });

CareSuperApp.factory('clientMatch', function($http, $q,auraInputService,tokenNumService){     	
    
    var myFactObj = {};       
   
    myFactObj.reqObj = function (url) {

    	 var defer = $q.defer();

		$http.post(url,
				{firstName:auraInputService.getFirstName(), surName:auraInputService.getLastName(), dob:auraInputService.getDob()}
				).then(function(response) {
				
			defer.resolve(response);	
				
		}, function(response) {
			defer.reject(response);
		});
	 	return defer.promise;
    }
    
    return myFactObj;
});
 
 CareSuperApp.service('auraResponseService', function() {
 	var _self = this;
 	_self.setResponse = function (tempInput) {
 		_self.response = tempInput;
   }
 	_self.getResponse = function () {
 	  return _self.response;
   }
 });
 

CareSuperApp.service('auraInputService', function() {   	
 	
 	var _self = this;
 	_self.setFund = function (tempInput) {
 		_self.fund = tempInput;
   }
 	_self.getFund = function () {
 	  return _self.fund;
   }
 	_self.setMode = function (tempInput) {
 		_self.mode = tempInput;
   }
 	_self.getMode = function () {
 	  return _self.mode;
   }
 	_self.setName = function (tempInput) {
 		_self.name = tempInput;
   }
 	_self.getName = function () {
 	  return _self.name;
   }
 	_self.setAge = function (tempInput) {
 		_self.age = tempInput;
   }
 	_self.getAge = function () {
 	  return _self.age;
   }    	
 	_self.setDeathAmt = function (tempInput) {
 		_self.deathAmt = tempInput;
   }
 	_self.getDeathAmt = function () {
 	  return _self.deathAmt;
   }
 	
 	_self.setTpdAmt = function (tempInput) {
 		_self.tpdAmt = tempInput;
   }
 	_self.getTpdAmt = function () {
 	  return _self.tpdAmt;
   }
 	
 	_self.setIpAmt = function (tempInput) {
 		_self.ipAmt = tempInput;
   }
 	_self.getIpAmt = function () {
 	  return _self.ipAmt;
   }
 	
 	_self.setWaitingPeriod = function (tempInput) {
 		_self.waitingPeriod = tempInput;
   }
 	_self.getWaitingPeriod = function () {
 	  return _self.waitingPeriod;
   }
 	
 	_self.setBenefitPeriod = function (tempInput) {
 		_self.benefitPeriod = tempInput;
   }
 	_self.getBenefitPeriod = function () {
 	  return _self.benefitPeriod;
   }
 	_self.setAppnumber = function (tempInput) {
 		_self.appnumber = tempInput;
   }
 	_self.getAppnumber = function () {
 	  return _self.appnumber;
   }
 	
 	_self.setIndustryOcc = function (tempInput) {
 		_self.industryOcc = tempInput;
   }
 	_self.getIndustryOcc = function () {
 	  return _self.industryOcc;
   }
 	
 	_self.setGender = function (tempInput) {
 		_self.gender = tempInput;
   }
 	_self.getGender = function () {
 	  return _self.gender;
   }
 	
 	_self.setCountry = function (tempInput) {
 		_self.country = tempInput;
   }
 	_self.getCountry = function () {
 	  return _self.country;
   }
 	
 	_self.setSalary = function (tempInput) {
 		_self.salary = tempInput;
   }
 	_self.getSalary = function () {
 	  return _self.salary;
   }
 	
 	_self.setFifteenHr = function (tempInput) {
 		_self.fifteenHr = tempInput;
   }
 	_self.getFifteenHr = function () {
 	  return _self.fifteenHr;
   }
 	
 	_self.setCompanyId = function (tempInput) {
 		_self.companyId = tempInput;
   }
 	_self.getCompanyId = function () {
 	  return _self.companyId;
   }
 	
 	_self.setClientname = function (tempInput) {
 		_self.clientname = tempInput;
   }
 	_self.getClientname = function () {
 	  return _self.clientname;
   }
 	
 	_self.setDob = function (tempInput) {
 		_self.dob = tempInput;
   }
 	_self.getDob = function () {
 	  return _self.dob;
   }
 	
 	_self.setFirstName = function (tempInput) {
 		_self.firstName = tempInput;
   }
 	_self.getFirstName = function () {
 	  return _self.firstName;
   }
 	_self.setLastName = function (tempInput) {
 		_self.lastName = tempInput;
   }
 	_self.getLastName = function () {
 	  return _self.lastName;
   }
 	_self.setSpecialTerm = function (tempInput) {
 		_self.specialTerm = tempInput;
   }
 	_self.getSpecialTerm = function () {
 	  return _self.specialTerm;
   }
 	_self.setExistingTerm = function (tempInput) {
 		_self.existingTerm = tempInput;
   }
 	_self.getExistingTerm = function () {
 	  return _self.existingTerm;
   }
 	
 	_self.setMemberType = function (tempInput) {
 		_self.memberType = tempInput;
   }
 	_self.getMemberType = function () {
 	  return _self.memberType;
   }
 	
 });

CareSuperApp.service('PersistenceService', function(){
	var thisObject = this;
	
	thisObject.setChangeCoverDetails = function(coverDetails){
		sessionStorage.setItem('coverDetails',JSON.stringify(coverDetails));
	};
	
	thisObject.getChangeCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('coverDetails'));
	};
	
	thisObject.setChangeCoverStateDetails = function(coverDetails){
		localStorage.setItem('coverStateDetails',JSON.stringify(coverDetails));
	};
	
	thisObject.getChangeCoverStateDetails = function(){
		return JSON.parse(localStorage.getItem('coverStateDetails'));
	};
	
	thisObject.setNewMemberCoverDetails = function(coverDetails){
		sessionStorage.setItem('newMemberCoverDetails',JSON.stringify(coverDetails));
	};
	
	thisObject.getNewMemberCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('newMemberCoverDetails'));
	};
	
	thisObject.setNewMemberCoverStateDetails = function(coverDetails){
		localStorage.setItem('newMemberCoverStateDetails',JSON.stringify(coverDetails));
	};
	
	thisObject.getNewMemberCoverStateDetails = function(){
		return JSON.parse(localStorage.getItem('newMemberCoverStateDetails'));
	};
	
	thisObject.setworkRatingCoverDetails = function(coverDetails){
		sessionStorage.setItem('workRatingCoverDetails',JSON.stringify(coverDetails));
	};
	
	thisObject.getworkRatingCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('workRatingCoverDetails'));
	};
	
	thisObject.settransferCoverDetails = function(coverDetails){
		sessionStorage.setItem('transferCoverDetails',JSON.stringify(coverDetails));
	};
	
	thisObject.gettransferCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('transferCoverDetails'));
	};
	
	thisObject.setChangeCoverOccDetails = function(changeCoverOccDetails){
		sessionStorage.setItem('changeCoverOccDetails',JSON.stringify(changeCoverOccDetails));
	};
			
	thisObject.getChangeCoverOccDetails = function(){
		return JSON.parse(sessionStorage.getItem('changeCoverOccDetails'));
	};
	
	// death Addnlcover details
	thisObject.setDeathAddnlCoverDetails = function(deathAddnlCoverDetails){
		sessionStorage.setItem('deathAddnlCoverDetails',JSON.stringify(deathAddnlCoverDetails));
	};
			
	thisObject.getDeathAddnlCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('deathAddnlCoverDetails'));
	};
	
	// TPD addnl cover details
	thisObject.setTpdAddnlCoverDetails = function(tpdAddnlCoverDetails){
		sessionStorage.setItem('tpdAddnlCoverDetails',JSON.stringify(tpdAddnlCoverDetails));
	};
			
	thisObject.getTpdAddnlCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('tpdAddnlCoverDetails'));
    };
	
	// IP addnl cover details
    thisObject.setIpAddnlCoverDetails = function(ipAddnlCoverDetails){
		sessionStorage.setItem('ipAddnlCoverDetails',JSON.stringify(ipAddnlCoverDetails));
	};
			
	thisObject.getIpAddnlCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('ipAddnlCoverDetails'));
 	};
	
	// tranfer occ details
	thisObject.setTransferCoverOccDetails = function(transferCoverOccDetails){
		sessionStorage.setItem('transferCoverOccDetails',JSON.stringify(transferCoverOccDetails));
	};
			
	thisObject.getTransferCoverOccDetails = function(){
		return JSON.parse(sessionStorage.getItem('transferCoverOccDetails'));
	};
	
	// Transfer death addnl cover details
	thisObject.setTransferDeathAddnlDetails = function(transferDeathAddnlDetails){
		sessionStorage.setItem('transferDeathAddnlDetails',JSON.stringify(transferDeathAddnlDetails));
	};
			
	thisObject.getTransferDeathAddnlDetails = function(){
		return JSON.parse(sessionStorage.getItem('transferDeathAddnlDetails'));
	};
	
	// Transfer TPD addnl Cover details
	thisObject.setTransferTpdAddnlDetails = function(transferTpdAddnlDetails){
		sessionStorage.setItem('transferTpdAddnlDetails',JSON.stringify(transferTpdAddnlDetails));
	};
			
	thisObject.getTransferTpdAddnlDetails = function(){
		return JSON.parse(sessionStorage.getItem('transferTpdAddnlDetails'));
	};
	
	// Transfer IP addnl cover details
	thisObject.setTransferIpAddnlDetails = function(transferIpAddnlDetails){
		sessionStorage.setItem('transferIpAddnlDetails',JSON.stringify(transferIpAddnlDetails));
	};
			
	thisObject.getTransferIpAddnlDetails = function(){
		return JSON.parse(sessionStorage.getItem('transferIpAddnlDetails'));
	};
	thisObject.setWorkRatingCoverOccDetails = function(workRatingCoverOccDetails){
		sessionStorage.setItem('workRatingCoverOccDetails',JSON.stringify(workRatingCoverOccDetails));
	};
			
	thisObject.getWorkRatingCoverOccDetails = function(){
		return JSON.parse(sessionStorage.getItem('workRatingCoverOccDetails'));
	};
	
	thisObject.setNewMemberCoverOccDetails = function(newMemberCoverOccDetails){
		sessionStorage.setItem('newMemberCoverOccDetails',JSON.stringify(newMemberCoverOccDetails));
	};
			
	thisObject.getNewMemberCoverOccDetails = function(){
		return JSON.parse(sessionStorage.getItem('newMemberCoverOccDetails'));
	};
	
	// new member Death addnl cover details
	thisObject.setNewMemberDeathAddnlDetails = function(newMemberDeathAddnlDetails){
		sessionStorage.setItem('newMemberDeathAddnlDetails',JSON.stringify(newMemberDeathAddnlDetails));
	};
			
	thisObject.getNewMemberDeathAddnlDetails = function(){
		return JSON.parse(sessionStorage.getItem('newMemberDeathAddnlDetails'));
	};
	
	// new member TPD addnl cover details
	thisObject.setNewMemberTpdAddnlDetails = function(newMemberTpdAddnlDetails){
		sessionStorage.setItem('newMemberTpdAddnlDetails',JSON.stringify(newMemberTpdAddnlDetails));
	};
			
	thisObject.getNewMemberTpdAddnlDetails = function(){
		return JSON.parse(sessionStorage.getItem('newMemberTpdAddnlDetails'));
	};
	
	// new member IP addnl cover details
	thisObject.setNewMemberIpAddnlDetails = function(newMemberIpAddnlDetails){
		sessionStorage.setItem('newMemberIpAddnlDetails',JSON.stringify(newMemberIpAddnlDetails));
	};
			
	thisObject.getNewMemberIpAddnlDetails = function(){
		return JSON.parse(sessionStorage.getItem('newMemberIpAddnlDetails'));
	};
	thisObject.setAppNumber = function(appnum){
		sessionStorage.setItem('UniqueAppNum',JSON.stringify(appnum));
	};
	
	thisObject.getAppNumber = function(){
		return JSON.parse(sessionStorage.getItem('UniqueAppNum'));
	};
	
	thisObject.setChangeCoverAuraDetails = function(details){
		sessionStorage.setItem('changeCoverAuraDetails',JSON.stringify(details));
	};
	
	thisObject.getChangeCoverAuraDetails = function(){
		return JSON.parse(sessionStorage.getItem('changeCoverAuraDetails'));
	};
	
	thisObject.setTransCoverAuraDetails = function(details){
		sessionStorage.setItem('transCoverAuraDetails',JSON.stringify(details));
	};
	
	thisObject.getTransCoverAuraDetails = function(){
		return JSON.parse(sessionStorage.getItem('transCoverAuraDetails'));
	};
	
	thisObject.setNewMemberAuraDetails = function(details){
		sessionStorage.setItem('newMemebrAuraDetails',JSON.stringify(details));
	};
	
	thisObject.getNewMemberAuraDetails = function(){
		return JSON.parse(sessionStorage.getItem('newMemebrAuraDetails'));
	};
	
	thisObject.setWorkRatingAuraDetails = function(details){
		sessionStorage.setItem('workRatingAuraDetails',JSON.stringify(details));
	};
	
	thisObject.getWorkRatingAuraDetails = function(){
		return JSON.parse(sessionStorage.getItem('workRatingAuraDetails'));
	};
	
	// cancel cover details
	thisObject.setCancelCoverDetails = function(details){
		sessionStorage.setItem('cancelCoverDetails',JSON.stringify(details));
	};
	
	thisObject.getCancelCoverDetails = function(){
		return JSON.parse(sessionStorage.getItem('cancelCoverDetails'));
	};
	
	thisObject.setPDFLocation = function(location){
		sessionStorage.setItem('PDFLocation',JSON.stringify(location));
	};
	
	thisObject.getPDFLocation = function(){
        try {
            if(sessionStorage.getItem('PDFLocation') == 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse(sessionStorage.getItem('PDFLocation'));
            }
        } catch(Error) {
            throw Error;
        }
    };
	
	thisObject.setUploadedFileDetails = function(details){
		sessionStorage.setItem('UploadedFileDetails',JSON.stringify(details));
	};
	
	thisObject.getUploadedFileDetails = function(){
		return JSON.parse(sessionStorage.getItem('UploadedFileDetails'));
	};
	
	thisObject.setUploadedFileDetailsNew = function(fileList){
        thisObject.uploadedFileList = fileList;
                    //sessionStorage.setItem('UploadedFileDetails',JSON.stringify(details));
    };

    thisObject.getUploadedFileDetailsNew = function(){
        return thisObject.uploadedFileList;
                    //return JSON.parse(sessionStorage.getItem('UploadedFileDetails')) === null ? null : JSON.parse(sessionStorage.getItem('UploadedFileDetails'));
    };
	
	thisObject.setAppNumToBeRetrieved = function(num){
		sessionStorage.setItem('AppNumToBeRetrieved',JSON.stringify(num));
	};
	
	thisObject.getAppNumToBeRetrieved = function(){
		return JSON.parse(sessionStorage.getItem('AppNumToBeRetrieved'));
	};
	
	thisObject.setNpsUrl = function(url){
		sessionStorage.setItem('NpsLink',JSON.stringify(url));
	};
	
	thisObject.getNpsUrl = function(){
        try {
            if(sessionStorage.getItem('NpsLink') == 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse(sessionStorage.getItem('NpsLink'));
            }
        } catch(Error) {
            throw Error;
        }
    };
	
});

CareSuperApp.factory('RetrieveAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.retrieveApp = function(url,fundCode,clientRefNo){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'fundCode': fundCode,
    		'clientRefNo': clientRefNo
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while retrieving saved apps " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('RetrieveAppDetailsService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.retrieveAppDetails = function(url,applicationNumber){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'applicationNumber': applicationNumber
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while retrieving application " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('CheckSavedAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.checkSavedApps = function(url,fundCode,clientRefNo,manageType){
    	var defer = $q.defer();
    	$http.get(url, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'fundCode': fundCode,
    		'clientRefNo': clientRefNo,
    		'manageType':manageType
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while getting saved apps " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('CancelSavedAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.cancelSavedApps = function(url,applicationNumber){
    	var defer = $q.defer();
    	$http.post(url, {}, {headers:{
    		'Content-Type': 'application/json',
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'applicationNumber': applicationNumber
    	}}).then(function(res){
    		factory = res.data;
    		defer.resolve(res);
    	}, function(err){
    		console.log("Error while cancelling app " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

CareSuperApp.factory('DownloadPDFService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.download = function(url,fileName){
    	var defer = $q.defer();
    	$http.post(url,{}, {headers:{
    		'Authorization':tokenNumService.getTokenId()
    	}, params:{
    		'file_name': fileName
    	},responseType: 'arraybuffer',
    	transformResponse: function (data) {
	        var pdf;
	        if (data) {
	            pdf = new Blob([data], {
	                type: 'application/pdf'
	            });
	        }
	        return {
	            response: pdf
	        };
	    }}).then(function(res){
	    	factory = res.data;
	    	defer.resolve(res);
    	}, function(err){
    		console.log("Error while getting pdf data " + JSON.stringify(err));
    		defer.reject(err);
    	});
    	return defer.promise;
    }; 
    return factory; 
}]);

