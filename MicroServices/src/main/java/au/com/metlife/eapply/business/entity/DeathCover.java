package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

public class DeathCover implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3846147280515167644L;
	
	private String deathMinAge;
	private String deathMaxAge;
	private String deathMinSumIns;
	private String deathMaxSumIns;
	private String deathAal;
	public String getDeathMinAge() {
		return deathMinAge;
	}
	public void setDeathMinAge(String deathMinAge) {
		this.deathMinAge = deathMinAge;
	}
	public String getDeathMaxAge() {
		return deathMaxAge;
	}
	public void setDeathMaxAge(String deathMaxAge) {
		this.deathMaxAge = deathMaxAge;
	}
	public String getDeathMinSumIns() {
		return deathMinSumIns;
	}
	public void setDeathMinSumIns(String deathMinSumIns) {
		this.deathMinSumIns = deathMinSumIns;
	}
	public String getDeathMaxSumIns() {
		return deathMaxSumIns;
	}
	public void setDeathMaxSumIns(String deathMaxSumIns) {
		this.deathMaxSumIns = deathMaxSumIns;
	}
	public String getDeathAal() {
		return deathAal;
	}
	public void setDeathAal(String deathAal) {
		this.deathAal = deathAal;
	}
	@Override
	public String toString() {
		return "DeathCover [deathMinAge=" + deathMinAge + ", deathMaxAge=" + deathMaxAge + ", deathMinSumIns="
				+ deathMinSumIns + ", deathMaxSumIns=" + deathMaxSumIns + ", deathAal=" + deathAal + "]";
	}
	
	
	
}
