package au.com.metlife.eapply;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class MicroServicesAppConfig extends WebMvcConfigurerAdapter{
	@Value("${spring.origin}")
	private  String origin;
	private static final Logger log = LoggerFactory.getLogger(MicroServicesAppConfig.class);
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		log.info("Add Cors mapping start");
		registry.addMapping("/**").allowedOrigins(origin);
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
}
