/* Cancel cover Controller,Progressive and Mandatory validations Starts  */   
CareSuperApp.controller('quotecancel',['$scope','$rootScope','$location','$timeout','$window','QuoteService','OccupationService','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','auraResponseService','$routeParams','urlService','ngDialog','APP_CONSTANTS',
                         function($scope,$rootScope, $location, $timeout,$window,QuoteService,OccupationService,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,auraResponseService,$routeParams,urlService,ngDialog, APP_CONSTANTS){
	/* Code for appD starts */
	  var pageTracker = null;
	  if(ADRUM) {
	    pageTracker = new ADRUM.events.VPageView();
	    pageTracker.start();
	  }

	  $scope.$on('$destroy', function() {
	    pageTracker.end();
	    ADRUM.report(pageTracker);
	  });
	  /* Code for appD ends */
	
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.validnumberregEx = '^0[1-8][0-9]{8}';
    //$scope.validAnnualSalaryregEx = '/^[0-9]*$/';
    $scope.phoneNumbrCancel =  /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
    $scope.emailFormatCancel = APP_CONSTANTS.emailFormat;
    $scope.contactTypeOptions = ["Home", "Work", "Mobile"];
    $scope.preferredContactType = '';
    // QuoteService.getList($scope.urlList.quoteUrl).then(function(res){
    // 	$scope.IndustryOptions = res.data;
    // }, function(err){
    // 	console.log("Error while getting industry options " + JSON.stringify(err));
    // });
    
    /*Error Flags*/
    // $scope.dodFlagErr = null;
    // $scope.privacyFlagErr = null;

    $scope.getOccupations = function(){
    	if(!$scope.workRatingIndustry){
    		$scope.workRatingIndustry = '';
    	}
    	
    	OccupationService.getOccupationList($scope.urlList.occupationUrl).then(function(res){
    	//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.workRatingIndustry}, function(occupationList){
        	$scope.OccupationList = res.data;
        }, function(err){
        	console.log("Error while getting occupation options " + JSON.stringify(err));
        });
    };
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};
	
 // added for session expiry
    /*$timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  	 /*var timer;
	    angular.element($window).bind('mouseover', function(){
	    	timer = $timeout(function(){
	    		sessionStorage.clear();
	    		localStorage.clear();
	    		$location.path("/sessionTimeOut");
	    	}, 900000);
	    }).bind('mouseout', function(){
	    	$timeout.cancel(timer);
	    });*/
	    

    // Need to revisit, need better implementation
    // $scope.isCollapsible = function(targetEle, event) {
    //   if( targetEle == 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
    //     event.stopPropagation();
    //     return false;
    //   } else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
    //     event.stopPropagation();
    //     return false;
    //   }
    // }

    //$scope.privacyCol = false;
    //var dodCheck;
    //var privacyCheck;

    /* TBC */
    // $scope.togglePrivacy = function(checkFlag) {
    //     $scope.privacyCol = checkFlag;
    //     if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
    //       return false;
    //     $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
    // };

    // $scope.toggleContact = function(checkFlag) {
    //     $scope.contactCol = checkFlag;
    //     if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
    //       return false;
    //     $("a[data-target='#collapseOne']").click(); /* Can be improved */
        
    // };
    // $scope.checkDodState = function() {
    //   $timeout(function() {
    //     $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
    //     if($('#dodLabel').hasClass('active')) {
    //       $scope.togglePrivacy(true);
    //     } else {
    //       $scope.togglePrivacy(false);
    //       $scope.toggleContact(false);
    //     }
    //   }, 1);  
    // };
    
    // $scope.checkPrivacyState  = function() {
    //   $timeout(function() {
    //     $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
    //     if($('#privacyLabel').hasClass('active')) {
    //       $scope.toggleContact(true);
    //     } else {
    //       $scope.toggleContact(false);
    //     }
    //   }, 1);
    // };

  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };

    
   //Progressive validation
 //   var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime'];
     $scope.checkPreviousMandatoryFieldsFrCancelCover  = function (cancelCoverElementName,cancelCoverFormName){
        var inx = cancelCoverFormFields.indexOf(cancelCoverElementName);
        if(inx > 0){
          for(var i = 0; i < inx ; i++){
            $scope[cancelCoverFormName][cancelCoverFormFields[i]].$touched = true;
          }
        }
     };

   // Validate fields "on continue"
     $scope.cancelContactDetailsFormSubmit =  function (form){
        if(!form.$valid){
          //alert("invalid>>"+$scope["cancelContactDetailsForm"].$invalid);
      	  form.$submitted=true; 
  	    }else{
  	    	$rootScope.$broadcast('disablepointer');
  	    	$scope.saveDataForPersistence();
        }
     };
    
    var inputDetails = persoanlDetailService.getMemberDetails();
    $scope.personalDetails = inputDetails[0].personalDetails;
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    $timeout(function(){
    	appNum = PersistenceService.getAppNumber();
    }, 10);
    
    
    while($scope.personalDetails){
		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.genderFlag =  false;
			$scope.gender = '';
			var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime','gender'];
		} else{
			$scope.genderFlag =  true;
			$scope.gender = $scope.personalDetails.gender;
			var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime'];
		}
		break;
	}
    
    if(inputDetails[0] && inputDetails[0].contactDetails.emailAddress){
		$scope.email = inputDetails[0].contactDetails.emailAddress;
	}
    /*Fixed as per CSO-478 - 169682*/
    /*if(inputDetails[0] && inputDetails[0].contactDetails.fundEmailAddress){
		$scope.email = inputDetails[0].contactDetails.fundEmailAddress;
	}*/
    if(inputDetails[0] && inputDetails[0].contactDetails.fundEmailAddress){
		$scope.fundemail = inputDetails[0].contactDetails.fundEmailAddress;
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContactTime){
		if(inputDetails[0].contactDetails.prefContactTime == "1"){
			$scope.time= "Morning (9am - 12pm)";
		}else{
			$scope.time= "Afternoon (12pm - 6pm)";
		}
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContact){
		if(inputDetails[0].contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.cancelPhone = inputDetails[0].contactDetails.mobilePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home";
			$scope.cancelPhone = inputDetails[0].contactDetails.homePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work";
			$scope.cancelPhone = inputDetails[0].contactDetails.workPhone;
		}
   }
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home"){
			$scope.cancelPhone = inputDetails[0].contactDetails.homePhone;
		}else if($scope.preferredContactType == "Work"){
			$scope.cancelPhone = inputDetails[0].contactDetails.workPhone;
		}else if($scope.preferredContactType == "Mobile"){
			$scope.cancelPhone = inputDetails[0].contactDetails.mobilePhone;
		} else {
      $scope.cancelPhone = '';
    }
	}
    //appNum = PersistenceService.getAppNumber();
 	$scope.deathCoverDetails = deathCoverService.getDeathCover();
 	$scope.tpdCoverDetails = tpdCoverService.getTpdCover();
 	$scope.ipCoverDetails = ipCoverService.getIpCover();
 	
 	
 	$scope.saveDataForPersistence = function(){
 		var coverObj={};
 		
 		coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
     	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
     	coverObj['email'] = $scope.email;
     	coverObj['gender'] = $scope.personalDetails.gender; 
     	coverObj['contactType'] = $scope.preferredContactType;
     	coverObj['contactPhone'] = $scope.cancelPhone;
     	coverObj['contactPrefTime'] = $scope.time;
     	coverObj['gender'] = $scope.gender;
 		   
      //coverObj['dodCheck'] = $('#dodLabel').hasClass('active');
      //coverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');

    	coverObj['deathAmt'] = $scope.deathCoverDetails.amount;
    	coverObj['tpdAmt'] = $scope.tpdCoverDetails.amount;
    	coverObj['ipAmt'] = $scope.ipCoverDetails.amount;
    	coverObj['waitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
    	coverObj['benefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;
    	coverObj['appNum'] = appNum;
    	coverObj['age'] = anb;
        coverObj['manageType'] = 'CANCOVER';
        coverObj['partnerCode'] = 'CARE';
        
     	PersistenceService.setCancelCoverDetails(coverObj);
     	$scope.go('/cancelConfirmation');
 	};
 	
 	if($routeParams.mode == 2){
 		var cancelContactDetails =PersistenceService.getCancelCoverDetails();
 		if(!cancelContactDetails) {
      $location.path("/quotecancel/1");
      return false;
    }

    $scope.email =cancelContactDetails.email;
 		$scope.preferredContactType =cancelContactDetails.contactType;
 		$scope.cancelPhone =cancelContactDetails.contactPhone;
 		$scope.time =cancelContactDetails.contactPrefTime;
 		$scope.gender=cancelContactDetails.gender;
 		appNum = cancelContactDetails.appNum;

    //dodCheck = cancelContactDetails.dodCheck;
    //privacyCheck = cancelContactDetails.privacyCheck;

    // if(dodCheck){
    //   $timeout(function(){
    //     $('#dodLabel').addClass('active');
    //   });
    // }
    // if(privacyCheck){
    //   $timeout(function(){
    //     $('#privacyLabel').addClass('active');
    //   });
    // }
    //$scope.togglePrivacy(true);
    //$scope.toggleContact(true);
 	}
}]); 
/* Cancel cover Controller,Progressive and Mandatory validations Ends  */ 

CareSuperApp.directive('phoneOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
            
            $(element).on("keyup", function() {
            	var TempVal=$(this).val().replace(" ","");
             	var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            	if( !regex.test(TempVal)) {
            		// element.controller('ngModel').$setValidity('required', false);
            		// element.controller('ngModel').$touched = true;
            	}
            	
            })
        }
    };
});
  