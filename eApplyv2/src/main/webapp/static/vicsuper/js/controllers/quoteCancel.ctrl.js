/* Cancel cover Controller,Progressive and Mandatory validations Starts  */
VicsuperApp.controller('quotecancel',['$scope','$rootScope','$location','$timeout','$window','QuoteService','OccupationService','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','auraResponseService','$routeParams','urlService','ngDialog','APP_CONSTANTS',
                         function($scope,$rootScope, $location, $timeout,$window,QuoteService,OccupationService,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,auraResponseService,$routeParams,urlService,ngDialog,APP_CONSTANTS){
	
	/* Code for appD starts */
	var pageTracker = null;
	if(ADRUM) {
		pageTracker = new ADRUM.events.VPageView();
		pageTracker.start();
	}

	$scope.$on('$destroy', function() {
		pageTracker.end();
		ADRUM.report(pageTracker);
	});
	/* Code for appD ends */
	
	
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.validnumberregEx = '^0[1-8][0-9]{8}';
    //$scope.validAnnualSalaryregEx = '/^[0-9]*$/';
    $scope.phoneNumbrCancel =  /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
    $scope.emailFormatCancel = APP_CONSTANTS.emailFormat;
    $scope.contactTypeOptions = ["Home", "Work", "Mobile"];
    $scope.preferredContactType = '';
    $scope.coverCancellation = {};
    
    //added for only displaying exiting cover
    $scope.isDeathApplicable = false;
    $scope.isTPDApplicable = false;
    $scope.isIPApplicable = false;
    $scope.autoTPDcancel = false;
    $scope.autoTPDCancelMsg = "If you cancel your Death cover, TPD cover will also be cancelled.";

    /*Error Flags*/
    //$scope.dodFlagErr = null;
    //$scope.privacyFlagErr = null;

    $scope.getOccupations = function(){
    	if(!$scope.workRatingIndustry){
    		$scope.workRatingIndustry = '';
    	}

    	OccupationService.getOccupationList($scope.urlList.occupationUrl).then(function(res){
    	//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.workRatingIndustry}, function(occupationList){
        	$scope.OccupationList = res.data;
        }, function(err){
        	////console.log("Error while getting occupation options " + JSON.stringify(err));
        });
    };
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};

 // added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

  	 /*var timer;
	    angular.element($window).bind('mouseover', function(){
	    	timer = $timeout(function(){
	    		sessionStorage.clear();
	    		localStorage.clear();
	    		$location.path("/sessionTimeOut");
	    	}, 900000);
	    }).bind('mouseout', function(){
	    	$timeout.cancel(timer);
	    });*/


    // Need to revisit, need better implementation
    $scope.isCollapsible = function(targetEle, event) {
     //  if( targetEle == 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
     //  	if($('#dodLabel').is(':visible'))
	  		// $scope.dodFlagErr = true;
     //    event.stopPropagation();
     //    return false;
     //  } else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
     //    if($('#privacyLabel').is(':visible'))
	  		// $scope.privacyFlagErr = true;
     //    event.stopPropagation();
     //    return false;
     //  } else
     if( targetEle == 'collapseTwo' && $("#collapseOne form").hasClass('ng-invalid')) {
  			if($("#collapseOne form").is(':visible'))
  				$scope.submitForm($scope.cancelContactDetailsForm);
  			event.stopPropagation();
  			return false;
  		}
    }

    $scope.privacyCol = false;
    var dodCheck;
    var privacyCheck;

    /* TBC */
    // $scope.togglePrivacy = function(checkFlag) {
    //     $scope.privacyCol = checkFlag;
    //     if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
    //       return false;
    //     $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
    // };

    $scope.toggleContact = function(checkFlag) {
        $scope.contactCol = checkFlag;
        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseOne']").click(); /* Can be improved */

    };

    $scope.toggleCancellation = function(checkFlag) {
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
        	return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };

    // $scope.checkDodState = function() {
    //   $timeout(function() {
    //     $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
    //     if($('#dodLabel').hasClass('active')) {
    //       $scope.togglePrivacy(true);
    //     } else {
    //       $scope.togglePrivacy(false);
    //       $scope.toggleContact(false);
    //       $scope.toggleCancellation(false);
    //     }
    //   }, 1);
    // };

    // $scope.checkPrivacyState  = function() {
    //   $timeout(function() {
    //     $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
    //     if($('#privacyLabel').hasClass('active')) {
    //       $scope.toggleContact(true);
    //     } else {
    //       $scope.toggleContact(false);
    //       $scope.toggleCancellation(false)
    //     }
    //   }, 1);
    // };

    $scope.checkCoverCancellation = function($event, modalName, elemId) {
    	$scope.autoTPDcancel = false;
    	$timeout(function() {
    		$scope.coverCancellation[modalName] = angular.element(document.querySelector('#'+ elemId)).hasClass('active');
    		if($scope.coverCancellation.death == true){
          $('#tpdLable').addClass('active');
    			
    			if($scope.isTPDApplicable)
    				{
    					$scope.coverCancellation.tpd = true;
    					$scope.autoTPDcancel = true;
    				}
    		}
    	});
    }

  	$scope.navigateToLandingPage = function() {
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };


   //Progressive validation
 	//   var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime'];
    $scope.checkPreviousMandatoryFieldsFrCancelCover  = function (cancelCoverElementName,cancelCoverFormName){
        var inx = cancelCoverFormFields.indexOf(cancelCoverElementName);
        if(inx > 0){
          for(var i = 0; i < inx ; i++){
            $scope[cancelCoverFormName][cancelCoverFormFields[i]].$touched = true;
          }
        }
    };

   // Validate fields "on continue"
     $scope.submitForm =  function (form){
        if(!form.$valid){
      	  	form.$submitted=true;
      	  	if(form.$name == 'cancelContactDetailsForm') {
      	  		$scope.toggleCancellation(false);
      	  	} else if(form.$name == 'coverCancellationForm') {

            }
  	    } else {
  	    	if(form.$name == 'cancelContactDetailsForm') {
				$scope.toggleCancellation(true);
  	    	} else if(form.$name == 'coverCancellationForm') {
  	    		if(this.cancelContactDetailsForm.$valid){
				    $rootScope.$broadcast('disablepointer');
				    $scope.saveDataForPersistence();
  	    		}
      	  }
        }
     };

    var inputDetails = persoanlDetailService.getMemberDetails();
    $scope.personalDetails = inputDetails.personalDetails;
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    $timeout(function(){
    	appNum = PersistenceService.getAppNumber();
    }, 10);


    while($scope.personalDetails){
		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.genderFlag =  false;
			$scope.gender = '';
			var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime','gender'];
		} else{
			$scope.genderFlag =  true;
			$scope.gender = $scope.personalDetails.gender;
			var cancelCoverFormFields = ['cancelCoverEmail', 'cancelCoverPhoneNo','cancelCvrPrefTime'];
		}
		break;
	}

    if(inputDetails && inputDetails.contactDetails.emailAddress){
		$scope.email = inputDetails.contactDetails.emailAddress;
	}
   /* if(inputDetails && inputDetails.contactDetails.fundEmailAddress){
		$scope.email = inputDetails.contactDetails.fundEmailAddress;
	}*/
    if(inputDetails && inputDetails.contactDetails.fundEmailAddress){
		$scope.fundemail = inputDetails.contactDetails.fundEmailAddress;
	}
	if(inputDetails && inputDetails.contactDetails.prefContactTime){
		if(inputDetails.contactDetails.prefContactTime == "1"){
			$scope.time= "Morning (9am - 12pm)";
		}else{
			$scope.time= "Afternoon (12pm - 6pm)";
		}
	}
	if(inputDetails.contactDetails.prefContact == null || inputDetails.contactDetails.prefContact == "")
	{
		inputDetails.contactDetails.prefContact=1;
	}
	if(inputDetails && inputDetails.contactDetails.prefContact){
		if(inputDetails.contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.cancelPhone = inputDetails.contactDetails.mobilePhone;
		}else if(inputDetails.contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home";
			$scope.cancelPhone = inputDetails.contactDetails.homePhone;
		}else if(inputDetails.contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work";
			$scope.cancelPhone = inputDetails.contactDetails.workPhone;
		}
   }
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home"){
			$scope.cancelPhone = inputDetails.contactDetails.homePhone;
		}else if($scope.preferredContactType == "Work"){
			$scope.cancelPhone = inputDetails.contactDetails.workPhone;
		}else if($scope.preferredContactType == "Mobile"){
			$scope.cancelPhone = inputDetails.contactDetails.mobilePhone;
		} else {
      $scope.cancelPhone = '';
    }
	}
    //appNum = PersistenceService.getAppNumber();
 	$scope.deathCoverDetails = deathCoverService.getDeathCover();
 	$scope.tpdCoverDetails = tpdCoverService.getTpdCover();
 	$scope.ipCoverDetails = ipCoverService.getIpCover();

 	if($scope.deathCoverDetails.amount && (parseInt($scope.deathCoverDetails.amount) > 0))
 		{
 		$scope.isDeathApplicable = true;
 		}
 	if($scope.tpdCoverDetails.amount && (parseInt($scope.tpdCoverDetails.amount) > 0))
 		{
 			$scope.isTPDApplicable = true;
 		}
 	if($scope.ipCoverDetails.amount && (parseInt($scope.ipCoverDetails.amount) > 0))
 		{
 			$scope.isIPApplicable = true;
 		}

 	$scope.clickToOpen = function (hhText) {

  		var dialog = ngDialog.open({
  			/*template: '<p>'+hhText+'</p>' +
  				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
  			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
  				className: 'ngdialog-theme-plain',
  				plain: true
  		});
  		dialog.closePromise.then(function (data) {
  			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
  		});
  	};

 	$scope.saveDataForPersistence = function(){
 		var coverObj={};

 		coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
     	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
     	coverObj['email'] = $scope.email;
     	coverObj['gender'] = $scope.personalDetails.gender;
     	coverObj['contactType'] = $scope.preferredContactType;
     	coverObj['contactPhone'] = $scope.cancelPhone;
     	coverObj['contactPrefTime'] = $scope.time;
     	coverObj['gender'] = $scope.gender;

      	//coverObj['dodCheck'] = $('#dodLabel').hasClass('active');
      	//coverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');
      	// sest ackk check

    	coverObj['deathAmt'] = $scope.deathCoverDetails.amount;
    	coverObj['tpdAmt'] = $scope.tpdCoverDetails.amount;
    	coverObj['ipAmt'] = $scope.ipCoverDetails.amount;
    	coverObj['waitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
    	coverObj['benefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;
    	coverObj['appNum'] = appNum;
    	coverObj['age'] = anb;
        coverObj['manageType'] = 'CANCOVER';
        coverObj['partnerCode'] = 'HOST';
        coverObj['coverCancellation'] = $scope.coverCancellation;


        // coverObj['appNum'] = parseInt(appNum);
        // coverObj['ackCheck'] = ackCheck;
        // coverObj['dodCheck'] = $('#dodLabel').hasClass('active');
        // coverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');
        // coverObj['age'] = anb;
        // coverObj['manageType'] = 'CANCOVER';
        // coverObj['partnerCode'] = 'HOST';//FIRS, SFPS
        // coverObj['existingIPWaitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
        // coverObj['existingIPBenefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;




     	PersistenceService.setCancelCoverDetails(coverObj);
     	$scope.go('/cancelConfirmation');
 	};

 	if($routeParams.mode == 2){
 		var cancelCoverDetails =PersistenceService.getCancelCoverDetails();
 		if(!cancelCoverDetails) {
	      $location.path("/quotecancel/1");
	      return false;
	    }

    	$scope.email =cancelCoverDetails.email;
 		$scope.preferredContactType =cancelCoverDetails.contactType;
 		$scope.cancelPhone =cancelCoverDetails.contactPhone;
 		$scope.time =cancelCoverDetails.contactPrefTime;
 		$scope.gender=cancelCoverDetails.gender;
 		appNum = cancelCoverDetails.appNum;
 		$scope.coverCancellation = cancelCoverDetails.coverCancellation;

	    dodCheck = cancelCoverDetails.dodCheck;
	    privacyCheck = cancelCoverDetails.privacyCheck;

	    // if(dodCheck){
	    //   $timeout(function(){
	    //     $('#dodLabel').addClass('active');
	    //   });
	    // }
	    // if(privacyCheck){
	    //   $timeout(function(){
	    //     $('#privacyLabel').addClass('active');
	    //   });
	    // }

	    if($scope.coverCancellation.death) {
	      $timeout(function(){
	        $('#deathLable').addClass('active');
	      });
	    }

	    if($scope.coverCancellation.tpd) {
	      $timeout(function(){
	        $('#tpdLable').addClass('active');
	      });
	    }

	    if($scope.coverCancellation.salContinuance) {
	      $timeout(function(){
	        $('#salContinuanceLable').addClass('active');
	      });
	    }

	    //$scope.togglePrivacy(true);
	    $scope.toggleContact(true);
	    $scope.toggleCancellation(true);
 	}
}]);
/* Cancel cover Controller,Progressive and Mandatory validations Ends  */

VicsuperApp.directive('phoneOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });

            $(element).on("keyup", function() {
            	var TempVal=$(this).val().replace(" ","");
             	var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            	if( !regex.test(TempVal)) {
            		// element.controller('ngModel').$setValidity('required', false);
            		// element.controller('ngModel').$touched = true;
            	}

            })
        }
    };
});
