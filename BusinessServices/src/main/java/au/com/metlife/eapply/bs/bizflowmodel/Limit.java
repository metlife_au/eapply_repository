package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.lowagie.text.xml.xmp.DublinCoreSchema;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Limit")


@XmlRootElement
public class Limit {	
	
	private String LimitAppliesCode;
	private String LimitAmount;
	private String CalculationMethodCode;
	private String OccupationalRatingCode ;
	private String WaitingPeriod ;
	private String BenefitPeriod ;
	private String LimitUnits;
	private Double Premium;
	private String PremiumFrequency;

	
	public Double getPremium() {
		return Premium;
	}
	public void setPremium(Double premium) {
		Premium = premium;
	}
	public String getPremiumFrequency() {
		return PremiumFrequency;
	}
	public void setPremiumFrequency(String premiumFrequency) {
		PremiumFrequency = premiumFrequency;
	}
	public String getLimitUnits() {
		return LimitUnits;
	}
	public void setLimitUnits(String limitUnits) {
		LimitUnits = limitUnits;
	}
	public String getBenefitPeriod() {
		return BenefitPeriod;
	}
	public void setBenefitPeriod(String benefitPeriod) {
		BenefitPeriod = benefitPeriod;
	}
	public String getCalculationMethodCode() {
		return CalculationMethodCode;
	}
	public void setCalculationMethodCode(String calculationMethodCode) {
		CalculationMethodCode = calculationMethodCode;
	}
	public String getLimitAmount() {
		return LimitAmount;
	}
	public void setLimitAmount(String limitAmount) {
		LimitAmount = limitAmount;
	}
	public String getLimitAppliesCode() {
		return LimitAppliesCode;
	}
	public void setLimitAppliesCode(String limitAppliesCode) {
		LimitAppliesCode = limitAppliesCode;
	}
	public String getOccupationalRatingCode() {
		return OccupationalRatingCode;
	}
	public void setOccupationalRatingCode(String occupationalRatingCode) {
		OccupationalRatingCode = occupationalRatingCode;
	}
	public String getWaitingPeriod() {
		return WaitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
		WaitingPeriod = waitingPeriod;
	}
	
	
	
	
}
