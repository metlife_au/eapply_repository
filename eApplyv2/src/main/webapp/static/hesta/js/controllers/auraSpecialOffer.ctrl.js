/*aura special offer*/
/*aura transfer controller*/    
CareSuperApp.controller('auraspecialoffer',['$scope', '$location', '$timeout','$window', 'auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService', 'submitAura','persoanlDetailService','auraResponseService','ngDialog','urlService',
                                     function($scope, $location, $timeout,$window, auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,auraResponseService,ngDialog,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	  //$location.path(path);
  	};
  	
 // added for session expiry
    /*$timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}
  	*/
  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
     
  	$scope.proceedNext = function() {
  		$scope.keepGoing = true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true;  
  				$scope.keepGoing = false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  			}
  		});
  		if($scope.keepGoing){
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) { 
  				console.log(response.data);
  				PersistenceService.setNewMemberAuraDetails(response.data);
  				$scope.go('/newsummary');
  	  		});
  		}
  	}
  	
  	
  	
  	$scope.coverDetails=PersistenceService.getNewMemberCoverDetails();
  	$scope.newOccDetails = PersistenceService.getNewMemberCoverOccDetails();
  	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.deathAddnlDetails=PersistenceService.getNewMemberDeathAddnlDetails();
  	$scope.tpdAddnlDetails=PersistenceService.getNewMemberTpdAddnlDetails();
  	$scope.ipAddnlDetails=PersistenceService.getNewMemberIpAddnlDetails();
    console.log($scope.coverDetails);
  	
  	auraInputService.setFund('CARE')
  	auraInputService.setMode('SpecialOffer')
  	
  	
  	/////////////
  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ; 
 // auraInputService.setAge( moment().diff($scope.coverDetails.dob, 'years')) ; 		
  	auraInputService.setAppnumber($scope.coverDetails.appNum)
  	auraInputService.setDeathAmt($scope.deathAddnlDetails.newDeathLabelAmt)
  	auraInputService.setTpdAmt($scope.tpdAddnlDetails.newTpdLabelAmt)
  	auraInputService.setIpAmt($scope.ipAddnlDetails.newIpLabelAmt)
  	auraInputService.setWaitingPeriod($scope.ipAddnlDetails.newWaitingPeriod)
  	auraInputService.setBenefitPeriod($scope.ipAddnlDetails.newBenefitPeriod)  
  	if($scope.newOccDetails && $scope.newOccDetails.gender ){
  		auraInputService.setGender($scope.newOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
//  auraInputService.setGender($scope.newOccDetails.gender)
  	auraInputService.setIndustryOcc($scope.newOccDetails.industryCode+":"+$scope.newOccDetails.occupation)
  	auraInputService.setClientname('metaus')
  	/*if($scope.newOccDetails.areyouperCitzNewMemberQuestion=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.newOccDetails.areyouperCitzNewMemberQuestion)
  	} */ 	
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary($scope.newOccDetails.salary)
  	auraInputService.setFifteenHr($scope.newOccDetails.fifteenHr)
  	auraInputService.setName($scope.coverDetails.name) 
	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
  	auraInputService.setExistingTerm(false);
  	if($scope.personalDetails[0].memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	} 	
  	
  	//for deafult testing
  	/*auraInputService.setName('CARE')
  	auraInputService.setAge(50)
  	//below values has to be set from quote page, as of now taking from existing insurance
  	auraInputService.setAppnumber(14782223482354345)
  	auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')   
  	auraInputService.setGender('Male')
  	auraInputService.setIndustryOcc('026:Transfer')
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary('100000')
  	auraInputService.setFifteenHr('Yes')       	*/
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
  		$scope.auraResponseDataList = response.data.questions;
  		console.log($scope.auraResponseDataList)
  		angular.forEach($scope.auraResponseDataList, function(Object) {
			$scope.sectionname = Object.questionAlias.substring(3);            			
			 
			});
  	});
  	 /*var appNum;
     appNum = PersistenceService.getAppNumber();
  	 $scope.saveNewMemberAura = function (){
  		$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    		if($scope.coverDetails != null && $scope.newOccDetails != null && $scope.deathAddnlDetails != null &&  $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.personalDetails[0] != null){
      		$scope.coverDetails.lastSavedOn = 'NewMemberAuraPage';
      		// $scope.auraDetails
      		$scope.details={};
			$scope.details.newDeathAddnlCoverDetails = $scope.deathAddnlDetails;
			$scope.details.newTpdAddnlCoverDetails = $scope.tpdAddnlDetails;
			$scope.details.newIpAddnlCoverDetails = $scope.ipAddnlDetails;
			$scope.details.newOccupationDetails = $scope.newOccDetails;
      		var temp = angular.extend($scope.details,$scope.coverDetails);
          	var saveNewMemberAuraObject = angular.extend(temp, $scope.personalDetails[0]);
          	auraResponseService.setResponse(saveNewMemberAuraObject)
  	        saveEapply.reqObj().then(function(response) {  
  	                console.log(response.data)
  	        });
      	}
     };
    
     $scope.saveAndExitPopUp = function (hhText) {
       	
 		var dialog1 = ngDialog.open({
 			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
 				className: 'ngdialog-theme-plain custom-width',
 				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
 				plain: true
 		});
 		dialog1.closePromise.then(function (data) {
 			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
 		});
 	};*/
 	
 	 $scope.updateRadio = function (answerValue, questionObj){  		
    		
    		questionObj.arrAns[0]=answerValue;
    		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
    		  auraResponseService.setResponse($scope.auraRes)  		  
    		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {  			 
    			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
    			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
    	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 	  			
    	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
    	  				//branch complete false for previous questions  	  				
    	  				$scope.auraResponseDataList[selectedIndex].error=true;  	  				
    	  			}else if(Object.questionComplete){
    	  				$scope.auraResponseDataList[selectedIndex].error=false;
    	  			}
    	  			
    	  		});
    			  
        	}, function () {
        		//console.log('failed');
        	});
    	 };
  	 
  	
  	
}]);


/////