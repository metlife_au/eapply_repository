package au.com.metlife.eapply.model;

import au.com.metlife.eapply.corporate.business.CorpEntityRequest;

public class CorpEntityRequestTest {
	private CorpEntityRequest corpEntityRequest;

	public CorpEntityRequest getCorpEntityRequest() {
		return corpEntityRequest;
	}

	public void setCorpEntityRequest(CorpEntityRequest corpEntityRequest) {
		this.corpEntityRequest = corpEntityRequest;
	}

}
