package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.YBRIPPaymentsPageObjects;
import uimap.metlife.YBRSummaryPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class YBRIPPaymentsPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRIPPaymentsPage yourPayment() {
		paymentDetails();
		return new YBRIPPaymentsPage(scriptHelper);
	}

	public YBRIPPaymentsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void paymentDetails() {

	
		String aacountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsbcode = dataTable.getData("MetLife_Data", "BSBCode");
		String bsbnumber = dataTable.getData("MetLife_Data", "BSBNumber");
		String ccType = dataTable.getData("MetLife_Data", "CcType");
		String expiryMonth = dataTable.getData("MetLife_Data", "ExpiryMonth");
		String expiryYear = dataTable.getData("MetLife_Data", "ExpiryYear");
		String cardType = dataTable.getData("MetLife_Data", "CardType");
		
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			if (cardType.equalsIgnoreCase("Debit")) {
			tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.rddebit), "clkradio", "Debit");
			Thread.sleep(500);
			
			
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtaccountname), "SET", "AccountName", aacountName);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtbsbcode), "SET", "BSB Code", bsbcode);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtbsbcode), "TAB", "BSB Code");
			
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtbsbnumber), "SET", "BSB Number", bsbnumber);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtbsbnumber), "TAB", "BSB Number");
			}
			else
			{
				
				System.out.println("Entered into Credit Card Section");
			 tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.rdcredit), "clkradio", "Credit");
				Thread.sleep(500);
				
				switch(ccType){
				case "Visa":
					
					tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.rdvisa), "clkradio", "Visa");
				    Thread.sleep(500);
					 break;	 
				case "Master":
					
				    tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.rdmaster), "clkradio", "Master");
			        Thread.sleep(500);
					 break;
				case "Amex":
					
			        tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.rdamex), "clkradio", "Amex");
		            Thread.sleep(500);
		            break;
				case "DC":
					tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.rddc), "clkradio", "DC");
		            Thread.sleep(500);
		            break;
		
						}
				
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtccnumber), "SET", "CCNumber", accountNumber);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtccnumber), "TAB", "CCNumer");
			sleep(1000);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtccname), "SET", "CCName", aacountName);
			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.txtccname), "TAB", "CCNumer");
			sleep(1000);
			tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.selCCExpMonth),"DropDownSelect", "Expiry Month", expiryMonth);
			sleep(2000);
			tryAction(fluentWaitElement(YBRIPPaymentsPageObjects.selCcExpYr),"DropDownSelect", "Expiry Year", expiryYear);
			}

			tryAction(waitForClickableElement(YBRIPPaymentsPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Thread.sleep(500);
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(YBRIPPaymentsPageObjects.btnsubmit)).click().perform();
			Thread.sleep(1000);	
			
			switchToActiveWindow();
			driver.manage().window().maximize();	
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
