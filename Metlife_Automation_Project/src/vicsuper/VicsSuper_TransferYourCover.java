package vicsuper;


import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class VicsSuper_TransferYourCover extends MasterPage {
	WebDriverUtil driverUtil = null;

	public VicsSuper_TransferYourCover TransferCover_VicsSuper() {
		TransferCoverHOSTPLUS();
		healthandlifestyledetails();
		confirmation();

		return new VicsSuper_TransferYourCover(scriptHelper);
	}

	public VicsSuper_TransferYourCover TransferCover_VicsSuper_Negative() {
		TransferCoverHOSTPLUS_Negative();

		return new VicsSuper_TransferYourCover(scriptHelper);
	}

	public VicsSuper_TransferYourCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

public void TransferCoverHOSTPLUS() {

		String EmailId = dataTable.getData("VicsSuper_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("VicsSuper_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("VicsSuper_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("VicsSuper_TransferCover", "TimeofContact");
		String Gender = dataTable.getData("VicsSuper_TransferCover", "Gender");
		String FourteenHoursWork = dataTable.getData("VicsSuper_TransferCover", "FourteenHoursWork");
		String Citizen = dataTable.getData("VicsSuper_TransferCover", "Citizen");
		String EducationalInstitution = dataTable.getData("VicsSuper_TransferCover", "EducationalInstitution");
		String EducationalDuties = dataTable.getData("VicsSuper_TransferCover", "EducationalDuties");
	    String TertiaryQual = dataTable.getData("VicsSuper_TransferCover", "TertiaryQual");
		String AnnualSalary = dataTable.getData("VicsSuper_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("VicsSuper_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("VicsSuper_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("VicsSuper_TransferCover", "FormerFundNo");
		String AttachmentSizeLimitTest	= dataTable.getData("VicsSuper_TransferCover", "AttachmentSizeLimitTest");
		String AttachmentTestPath = dataTable.getData("VicsSuper_TransferCover", "AttachmentTestPath");
		String OverSizeMessage = dataTable.getData("VicsSuper_TransferCover", "OverSizeMessage");
		String AttachPath = dataTable.getData("VicsSuper_TransferCover", "AttachPath");
		String CostType = dataTable.getData("VicsSuper_TransferCover", "CostType");
		String DeathYN = dataTable.getData("VicsSuper_TransferCover", "Death");
		String TPDYN = dataTable.getData("VicsSuper_TransferCover", "TPD");
		String IPYN = dataTable.getData("VicsSuper_TransferCover", "IP");
		String DeathAmount = dataTable.getData("VicsSuper_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("VicsSuper_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("VicsSuper_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("VicsSuper_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("VicsSuper_TransferCover", "BenefitPeriod");

		try{



			//****Click on Transfer Your Cover Link*****\\

			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Transfer your cover']"))).click();
			report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				/*if(driver.findElement(By.xpath("//button[@ng-click='confirm()']")).isDisplayed()){
					sleep(500);
					quickSwitchWindows();
					sleep(1000);
					driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
					report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
					sleep(2000);
			*/
				Robot roboobj=new Robot();
				sleep(100);
				roboobj.keyPress(KeyEvent.VK_TAB);
	  			sleep(100);
	  			roboobj.keyPress(KeyEvent.VK_TAB);
	  			quickSwitchWindows();
	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  			roboobj.keyPress(KeyEvent.VK_ENTER);
	  			sleep(100);
	  		}

			/*if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(2000);

			}*/

			//*****Agree to Duty of disclosure*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
			sleep(400);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);




            //*****Agree to Privacy Statement*******\\


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

			sleep(200);

            //*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


			//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);



				//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}

				/*//***********Select the Gender******************\\

				if(Gender.equalsIgnoreCase("Male")){

					driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}	*/

				//****Contact Details-Continue Button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);

				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\

					//*****Select the 15 Hours Question*******\\


				if(FourteenHoursWork.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

				//*****Resident of Australia****\\

				if(Citizen.equalsIgnoreCase("Yes")){
					driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

					}
				else{
					driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

					}




				// *****Are the duties of your regular occupation limited to
				// either:*****\\

				if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

					// ***Select duties which are undertaken within an Office
					// Environment?\\
					if (EducationalInstitution.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]")))
								.click(); // Dhipika
						report.updateTestLog("Educational institution", "Selected No", Status.PASS);
						sleep(1000);
					}

					// *****Educational duties performed within a school or other
					// educational institution******\\
					if (EducationalDuties.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]")))
								.click(); // Dhipika
						report.updateTestLog("Educational duties", "Selected No", Status.PASS);
						sleep(1000);
					}

				} else {
					report.updateTestLog("Additional Questions",
							"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

				}

				// Do you hold a tertiary qualification or work in a management role
				if (driver.getPageSource().contains("Do you:")) {

					// *****Do You Hold a Tertiary Qualification******\\
					if (TertiaryQual.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[1]"))).click(); // Dhipika
						report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[2]"))).click(); // Dhipika
						report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
						sleep(1000);
					}

				} else {
					report.updateTestLog("Additional Questions",
							"Do you hold a tertiary qualification or work in a management role is not present",
							Status.FAIL);
				}


			//*****What is your annual Salary******


			    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
			    sleep(500);
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);



			//*****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


			//******************************************\\
			//**********PREVIOUS COVER SECTION**********\\
			//*******************************************\\

			//*****What is the name of your previous fund or insurer?*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
			sleep(250);
			report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

            //*****Please enter your fund member or insurance policy number*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
			sleep(250);
			report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

            //*****Please enter your former fund SPIN number (if known)*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
			sleep(250);
			report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

			//*****Attach documentary evidence****\\


				System.out.println("Yes");

				if(AttachmentSizeLimitTest.equalsIgnoreCase("Yes")){

					 //Click Browse
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
					 sleep(2500);
					 System.out.println("look");
					StringSelection stringSelection = new StringSelection(AttachmentTestPath);
					   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

					   //native key strokes for CTRL, V and ENTER keys
			            Robot robot = new Robot();

			            robot.keyPress(KeyEvent.VK_CONTROL);
			            robot.keyPress(KeyEvent.VK_V);
			            robot.keyRelease(KeyEvent.VK_V);
			            robot.keyRelease(KeyEvent.VK_CONTROL);
			            robot.keyPress(KeyEvent.VK_ENTER);
			            robot.keyRelease(KeyEvent.VK_ENTER);
			            sleep(1000);
				          //Click Add
						 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddId']")).click();
						 sleep(2500);



				/*	System.out.println("Yes");
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");
					System.out.println(StrBrowseName);
					//if (StrBrowseName.equalsIgnoreCase("Firefox")){
						 StringSelection sel = new StringSelection(AttachmentTestPath);

						   // Copy to clipboard
						 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);

						 //Click
						 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();

						 // Create object of Robot class
						 Robot robot = new Robot();
						 Thread.sleep(1000);

						  // Press Enter
						 robot.keyPress(KeyEvent.VK_ENTER);

						// Release Enter
						 robot.keyRelease(KeyEvent.VK_ENTER);

						  // Press CTRL+V
						 robot.keyPress(KeyEvent.VK_CONTROL);
						 robot.keyPress(KeyEvent.VK_V);

						// Release CTRL+V
						 robot.keyRelease(KeyEvent.VK_CONTROL);
						 robot.keyRelease(KeyEvent.VK_V);
						 Thread.sleep(1000);

						        // Press Enter
						 robot.keyPress(KeyEvent.VK_ENTER);
						 robot.keyRelease(KeyEvent.VK_ENTER);

						sleep(500);*/
					//}

					/*else{
						driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachmentTestPath);

					}*/

				/*	//*****Let the attachment load*****\\
					driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
					sleep(1000);

					//*****Add the Attachment*****\\

					WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
					add.click();
					//((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
					sleep(1500);*/

					String OversizeMessage=driver.findElement(By.xpath("//label[text()='  File size should not be more than 10MB ']")).getText();
					sleep(250);
					String SizeLimit=OversizeMessage.trim();

					if(OverSizeMessage.equalsIgnoreCase(SizeLimit)){
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is Displayed", Status.PASS);
					}
					else{
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is not Displayed", Status.FAIL);
					}



				}


				//****Attach the file******
				sleep(500);

				/*properties = Settings.getInstance();
				String StrBrowseName=properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Firefox")){
					StringSelection sel = new StringSelection(AttachPath);

					   // Copy to clipboard
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);

					 //Click
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();

					 // Create object of Robot class
					 Robot robot = new Robot();
					 Thread.sleep(1000);

					  // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);

					// Release Enter
					 robot.keyRelease(KeyEvent.VK_ENTER);

					  // Press CTRL+V
					 robot.keyPress(KeyEvent.VK_CONTROL);
					 robot.keyPress(KeyEvent.VK_V);

					// Release CTRL+V
					 robot.keyRelease(KeyEvent.VK_CONTROL);
					 robot.keyRelease(KeyEvent.VK_V);
					 Thread.sleep(1000);

					        // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);
					 robot.keyRelease(KeyEvent.VK_ENTER);

					sleep(500);

			}

			else{
				driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);

			}
				*/



				//Click Browse
				 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
				 sleep(2500);
				StringSelection stringSelection1 = new StringSelection(AttachPath);
				   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection1, null);

				   //native key strokes for CTRL, V and ENTER keys
		            Robot robot = new Robot();

		            robot.keyPress(KeyEvent.VK_CONTROL);
		            robot.keyPress(KeyEvent.VK_V);
		            robot.keyRelease(KeyEvent.VK_V);
		            robot.keyRelease(KeyEvent.VK_CONTROL);
		            robot.keyPress(KeyEvent.VK_ENTER);
		            robot.keyRelease(KeyEvent.VK_ENTER);
		            sleep(1000);
		            //Click Add
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddId']")).click();
					 sleep(2500);

					 System.out.println("Done");
			//*****Choose the cost of Insurance Type******\\

			Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
			CostofInsurance.selectByVisibleText(CostType);
			sleep(400);
			report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);


			//****Click on Continue Button******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
			sleep(500);
			report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);


			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\

			//*****Death transfer amount******

			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);

			}

			//*****TPD transfer amount******

			if(TPDYN.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

			}

			//****IP transfer amount*****\\



			if(IPYN.equalsIgnoreCase("Yes")&&FourteenHoursWork.equalsIgnoreCase("Yes")){


			//*****Select The Waiting Period********\\

				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);



			//*****Select The Benefit  Period********\\

				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);


			//****Enter the IP amount*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);

			}




			//*****Click on calculate Quote*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
			sleep(3000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);




			//********************************************************************************************\\
			//************Capturing the premiums displayed************************************************\\
			//********************************************************************************************\\

			//*******Capturing the Death Cost********\\

			WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
			Death.click();
			sleep(300);
			//JavascriptExecutor jse = (JavascriptExecutor) driver;
			//jse.executeScript("window.scrollBy(0,-150)", "");
			sleep(500);
			String DeathCost=Death.getText();
			report.updateTestLog("Death Cover amount", CostType+" Cost is "+DeathCost, Status.SCREENSHOT);
			sleep(500);

			//*******Capturing the TPD Cost********\\

			WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
			tpd.click();
			sleep(500);
			String TPDCost=tpd.getText();
			report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);

			//*******Capturing the IP Cost********\\

			WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
			ip.click();
			sleep(500);
			String IPCost=ip.getText();

			report.updateTestLog("IP Cover amount", CostType+" Cost is "+IPCost, Status.SCREENSHOT);

			//*****Scroll to the Bottom of the Page

		    //jse.executeScript("window.scrollBy(0,250)", "");

		  //******Click on Continue******\\

		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[2]"))).click();
		 sleep(4500);
		 report.updateTestLog("Continue", "Continue button is selected", Status.PASS);





			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void healthandlifestyledetails() {

		String RestrictedIllness = dataTable.getData("VicsSuper_TransferCover", "RestrictedIllness");
		String PriorClaim = dataTable.getData("VicsSuper_TransferCover", "PriorClaim");
		String RestrictedFromWork = dataTable.getData("VicsSuper_TransferCover", "RestrictedFromWork");
		String DiagnosedIllness = dataTable.getData("VicsSuper_TransferCover", "DiagnosedIllness");
		String MedicalTreatment = dataTable.getData("VicsSuper_TransferCover", "MedicalTreatment");
		String DeclinedApplication = dataTable.getData("VicsSuper_TransferCover", "DeclinedApplication");
		String Fundpremloading = dataTable.getData("VicsSuper_TransferCover", "Fundpremloading");
		String Exclusiontext = dataTable.getData("VicsSuper_TransferCover", "Exclusion");
		WebDriverWait wait=new WebDriverWait(driver,20);

			try{

				if(driver.getPageSource().contains("Eligibility Check")){

			//*******Are you restricted due to illness or injury*********\\

				if(RestrictedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
					sleep(2000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[1]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
					sleep(2000);

				}

            //*******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits,
				//Workers� Compensation or any other form of compensation due to illness or injury?*********\\

				if(PriorClaim.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-Yes", Status.PASS);
					sleep(2000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[2]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Prior Claim-No", Status.PASS);
					sleep(2000);

				}

                //*******Have you been restricted from work or unable to perform any of your regular duties for more than
				//seven consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?*********\\

				if(RestrictedFromWork.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-Yes", Status.PASS);
					sleep(2000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[3]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-No", Status.PASS);
					sleep(2000);

				}


			//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy
		    //to less than 3 years?

				if(DiagnosedIllness.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[4]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
					sleep(2000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[4] "))).click();
					report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
					sleep(2000);
				}


				//*****Are you currently contemplating any medical treatment or advice for any illness or injury for which you
				//have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?


				if(MedicalTreatment.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
					sleep(2000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[5]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
					sleep(2000);
				}

				//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\

				if(DeclinedApplication.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[6]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Declined Application-Yes", Status.PASS);
					sleep(2000);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[6]"))).click();
					sleep(1000);
					report.updateTestLog("Health and Lifestyle Page", "Declined Application-No", Status.PASS);
					sleep(2000);
				}

				//*******Is your cover under the previous fund or individual insurer subject to any premium loadings
				//and/or exclusions, including but not limited to pre-existing conditions exclusions, or restrictions in
				//regards to medical or other conditions?

				if(Fundpremloading.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[7]"))).click();
					sleep(2000);
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-Yes", Status.PASS);
					sleep(500);
					  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']"))).sendKeys(Exclusiontext);
					    sleep(1500);
					    driver.findElement(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']")).click();

					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[7]"))).click();
					report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-No", Status.PASS);
					sleep(2000);
				}

			//******Click on Continue Button********\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
			     sleep(4000);



				}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

private void confirmation() {

	WebDriverWait wait=new WebDriverWait(driver,20);

		try{
			if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){

		//******Agree to general Consent*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='option2']/parent::*"))).click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);




		//******Click on Submit*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitTransferCover()']"))).click();
			report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
			sleep(1500);

		}

		//*******Feedback Pop-up******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1500);






		//*****Fetching the Application Status*****\\

		String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
		sleep(500);
		report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);

		if(!(Appstatus.equalsIgnoreCase("TRANSFER OF COVER")) ){
			System.out.println("Yes");
		//String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
		//System.out.println(Justification);
		sleep(400);

		//*****Fetching the Application Number******\\

		String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
		sleep(1000);


		// ******Download the PDF***********\\
		/*
		 * properties = Settings.getInstance(); String
		 * StrBrowseName=properties.getProperty("currentBrowser"); if
		 * (StrBrowseName.equalsIgnoreCase("Chrome")){
		 */
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']")))
				.click();
		sleep(3000);
		/*
		 * }
		 *
		 *
		 * //driver.findElement(By.xpath(
		 * "//a[@class='underline']/strong")).click();
		 *
		 *
		 *
		 * if (StrBrowseName.equalsIgnoreCase("Firefox")){
		 * wait.until(ExpectedConditions.presenceOfElementLocated(By.
		 * xpath("//strong[text()='Download ']"))).click(); sleep(2000);
		 * Robot roboobj=new Robot();
		 * roboobj.keyPress(KeyEvent.VK_ENTER); }
		 *
		 * if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		 * Robot roboobj=new Robot();
		 * wait.until(ExpectedConditions.presenceOfElementLocated(By.
		 * xpath("//strong[text()='Download ']"))).click();
		 *
		 * sleep(4000); roboobj.keyPress(KeyEvent.VK_TAB);
		 * roboobj.keyPress(KeyEvent.VK_TAB);
		 * roboobj.keyPress(KeyEvent.VK_TAB);
		 * roboobj.keyPress(KeyEvent.VK_TAB);
		 *
		 * roboobj.keyPress(KeyEvent.VK_ENTER);
		 *
		 * }
		 */

		report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

		sleep(1000);

		report.updateTestLog("Decision Page", "Application No: " + App, Status.PASS);
		sleep(500);

		report.updateTestLog("Decision Page", "Application Status: " + Appstatus, Status.PASS);
	}

	else {
		report.updateTestLog("Application Declined", "Application is declined", Status.SCREENSHOT);
	}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}

public void TransferCoverHOSTPLUS_Negative() {




	String EmailId = dataTable.getData("VicsSuper_TransferCover", "EmailId");
	String TypeofContact = dataTable.getData("VicsSuper_TransferCover", "TypeofContact");
	String ContactNumber = dataTable.getData("VicsSuper_TransferCover", "ContactNumber");
	String TimeofContact = dataTable.getData("VicsSuper_TransferCover", "TimeofContact");
	String Gender = dataTable.getData("VicsSuper_TransferCover", "Gender");
	String FourteenHoursWork = dataTable.getData("VicsSuper_TransferCover", "FourteenHoursWork");
	String Citizen = dataTable.getData("VicsSuper_TransferCover", "Citizen");
	String EducationalInstitution = dataTable.getData("VicsSuper_TransferCover", "EducationalInstitution");
	String EducationalDuties = dataTable.getData("VicsSuper_TransferCover", "EducationalDuties");
    String TertiaryQual = dataTable.getData("VicsSuper_TransferCover", "TertiaryQual");
	String AnnualSalary = dataTable.getData("VicsSuper_TransferCover", "AnnualSalary");
	String PreviousInsurer = dataTable.getData("VicsSuper_TransferCover", "PreviousInsurer");
	String PolicyNo = dataTable.getData("VicsSuper_TransferCover", "PolicyNo");
	String FormerFundNo = dataTable.getData("VicsSuper_TransferCover", "FormerFundNo");
	String AttachPath = dataTable.getData("VicsSuper_TransferCover", "AttachPath");
	String CostType = dataTable.getData("VicsSuper_TransferCover", "CostType");
	String DeathYN = dataTable.getData("VicsSuper_TransferCover", "Death");
	System.out.println("DeathYN"+DeathYN);
	String TPDYN = dataTable.getData("VicsSuper_TransferCover", "TPD");
	System.out.println("TPDYN"+TPDYN);
	String DeathAmount = dataTable.getData("VicsSuper_TransferCover", "DeathAmount");
	String TPDAmount = dataTable.getData("VicsSuper_TransferCover", "TPDAmount");
	String TPDAmtGreater = dataTable.getData("VicsSuper_TransferCover", "TPDAmtGreater");
	String AttachmentSizeLimitTest	= dataTable.getData("VicsSuper_TransferCover", "AttachmentSizeLimitTest");
	String AttachmentTestPath = dataTable.getData("VicsSuper_TransferCover", "AttachmentTestPath");
	String OverSizeMessage = dataTable.getData("VicsSuper_TransferCover", "OverSizeMessage");
	String Deathlimiterror = dataTable.getData("VicsSuper_TransferCover", "Deathlimiterror");
	String TPDlimiterror = dataTable.getData("VicsSuper_TransferCover", "TPDlimiterror");
	String IPlimiterror = dataTable.getData("VicsSuper_TransferCover", "IPlimiterror");
	String WaitingPeriod = dataTable.getData("VicsSuper_TransferCover", "WaitingPeriod");
	String BenefitPeriod = dataTable.getData("VicsSuper_TransferCover", "BenefitPeriod");
	String IPYN = dataTable.getData("VicsSuper_TransferCover", "IP");
	System.out.println("IPYN"+IPYN);
	String IPAmount = dataTable.getData("VicsSuper_TransferCover", "IPAmount");



	try{
		//****Click on Transfer Your Cover Link*****\\
		WebDriverWait waiting=new WebDriverWait(driver,25);
		//****Click on Transfer Your Cover Link*****\\
		WebDriverWait wait=new WebDriverWait(driver,18);
		waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Transfer your cover']"))).click();
		report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);



		//*****Agree to Duty of disclosure*******\\
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
		sleep(600);
		report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);




        //*****Agree to Privacy Statement*******\\


		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
		sleep(250);
		report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

		sleep(200);

        //*****Enter the Email Id*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
		sleep(250);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
		report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
		sleep(500);

		//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


		//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);



			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}

			/*//***********Select the Gender******************\\

			if(Gender.equalsIgnoreCase("Male")){

				driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}	*/

			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);


			//***************************************\\
			//**********OCCUPATION SECTION***********\\
			//****************************************\\

				//*****Select the 15 Hours Question*******\\


			if(FourteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}

			//*****Resident of Australia****\\

			if(Citizen.equalsIgnoreCase("Yes")){
				driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

				}
			else{
				driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

				}




			// *****Are the duties of your regular occupation limited to
			// either:*****\\

			if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

				// ***Select duties which are undertaken within an Office
				// Environment?\\
				if (EducationalInstitution.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]")))
							.click(); // Dhipika
					report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]")))
							.click(); // Dhipika
					report.updateTestLog("Educational institution", "Selected No", Status.PASS);
					sleep(1000);
				}

				// *****Educational duties performed within a school or other
				// educational institution******\\
				if (EducationalDuties.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]")))
							.click(); // Dhipika
					report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]")))
							.click(); // Dhipika
					report.updateTestLog("Educational duties", "Selected No", Status.PASS);
					sleep(1000);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

			}

			// Do you hold a tertiary qualification or work in a management role
			if (driver.getPageSource().contains("Do you:")) {

				// *****Do You Hold a Tertiary Qualification******\\
				if (TertiaryQual.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[1]"))).click(); // Dhipika
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[2]"))).click(); // Dhipika
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
					sleep(1000);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Do you hold a tertiary qualification or work in a management role is not present",
						Status.FAIL);
			}


		//*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);



		//*****Click on Continue*******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);



		//******************************************\\
		//**********PREVIOUS COVER SECTION**********\\
		//*******************************************\\

		//*****What is the name of your previous fund or insurer?*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
		sleep(250);
		report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

        //*****Please enter your fund member or insurance policy number*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
		sleep(250);
		report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

        //*****Please enter your former fund SPIN number (if known)*****\\

		wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
		sleep(250);
		report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

		//*****Attach documentary evidence****\\


			System.out.println("Yes");

			if(AttachmentSizeLimitTest.equalsIgnoreCase("Yes")){

				 //Click Browse
				 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
				 sleep(2500);
				 System.out.println("look");
				StringSelection stringSelection = new StringSelection(AttachmentTestPath);
				   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

				   //native key strokes for CTRL, V and ENTER keys
		            Robot robot = new Robot();

		            robot.keyPress(KeyEvent.VK_CONTROL);
		            robot.keyPress(KeyEvent.VK_V);
		            robot.keyRelease(KeyEvent.VK_V);
		            robot.keyRelease(KeyEvent.VK_CONTROL);
		            robot.keyPress(KeyEvent.VK_ENTER);
		            robot.keyRelease(KeyEvent.VK_ENTER);
		            sleep(1000);
			          //Click Add
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddId']")).click();
					 sleep(2500);



			/*	System.out.println("Yes");
				properties = Settings.getInstance();
				String StrBrowseName=properties.getProperty("currentBrowser");
				System.out.println(StrBrowseName);
				//if (StrBrowseName.equalsIgnoreCase("Firefox")){
					 StringSelection sel = new StringSelection(AttachmentTestPath);

					   // Copy to clipboard
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);

					 //Click
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();

					 // Create object of Robot class
					 Robot robot = new Robot();
					 Thread.sleep(1000);

					  // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);

					// Release Enter
					 robot.keyRelease(KeyEvent.VK_ENTER);

					  // Press CTRL+V
					 robot.keyPress(KeyEvent.VK_CONTROL);
					 robot.keyPress(KeyEvent.VK_V);

					// Release CTRL+V
					 robot.keyRelease(KeyEvent.VK_CONTROL);
					 robot.keyRelease(KeyEvent.VK_V);
					 Thread.sleep(1000);

					        // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);
					 robot.keyRelease(KeyEvent.VK_ENTER);

					sleep(500);*/
				//}

				/*else{
					driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachmentTestPath);

				}*/

			/*	//*****Let the attachment load*****\\
				driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
				sleep(1000);

				//*****Add the Attachment*****\\

				WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
				add.click();
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1500);*/

				String OversizeMessage=driver.findElement(By.xpath("//label[text()='  File size should not be more than 10MB ']")).getText();
				sleep(250);
				String SizeLimit=OversizeMessage.trim();

				if(OverSizeMessage.equalsIgnoreCase(SizeLimit)){
					report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is Displayed", Status.PASS);
				}
				else{
					report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is not Displayed", Status.FAIL);
				}



			}


			//****Attach the file******
			sleep(500);

			/*properties = Settings.getInstance();
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Firefox")){
				StringSelection sel = new StringSelection(AttachPath);

				   // Copy to clipboard
				 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);

				 //Click
				 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();

				 // Create object of Robot class
				 Robot robot = new Robot();
				 Thread.sleep(1000);

				  // Press Enter
				 robot.keyPress(KeyEvent.VK_ENTER);

				// Release Enter
				 robot.keyRelease(KeyEvent.VK_ENTER);

				  // Press CTRL+V
				 robot.keyPress(KeyEvent.VK_CONTROL);
				 robot.keyPress(KeyEvent.VK_V);

				// Release CTRL+V
				 robot.keyRelease(KeyEvent.VK_CONTROL);
				 robot.keyRelease(KeyEvent.VK_V);
				 Thread.sleep(1000);

				        // Press Enter
				 robot.keyPress(KeyEvent.VK_ENTER);
				 robot.keyRelease(KeyEvent.VK_ENTER);

				sleep(500);

		}

		else{
			driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);

		}
			*/



			//Click Browse
			 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
			 sleep(2500);
			 System.out.println("look1");
			 System.out.println(AttachPath);
			StringSelection stringSelection1 = new StringSelection(AttachPath);
			   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection1, null);

			   //native key strokes for CTRL, V and ENTER keys
	            Robot robot = new Robot();

	            robot.keyPress(KeyEvent.VK_CONTROL);
	            robot.keyPress(KeyEvent.VK_V);
	            robot.keyRelease(KeyEvent.VK_V);
	            robot.keyRelease(KeyEvent.VK_CONTROL);
	            robot.keyPress(KeyEvent.VK_ENTER);
	            robot.keyRelease(KeyEvent.VK_ENTER);
	            sleep(1000);
	            //Click Add
				 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddId']")).click();
				 sleep(2500);

				 System.out.println("Done");
		//*****Choose the cost of Insurance Type******\\

		Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
		CostofInsurance.selectByVisibleText(CostType);
		sleep(400);
		report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);


		//****Click on Continue Button******\\

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
		sleep(500);
		report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);


		//*************************************************\\
		//**************TRANSFER COVER SECTION***************\\
		//**************************************************\\

		//*****Death transfer amount******


		if(DeathYN.equalsIgnoreCase("Yes")&&TPDYN.equalsIgnoreCase("Yes")&&IPYN.equalsIgnoreCase("Yes")){
		//if(DeathYN.equalsIgnoreCase("Yes")&&TPDYN.equalsIgnoreCase("Yes")){
			if(DeathYN.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
			sleep(500);
			report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[text()='Cover amount'])[1]"))).click();
			sleep(500);

			String DeathmaximumlimitError=driver.findElement(By.xpath("//span[@ng-if='maxDeathErrorFlag']")).getText();

			if(DeathmaximumlimitError.equalsIgnoreCase(Deathlimiterror)){
				report.updateTestLog("Death amount maximum limit Error", "Expecetd Error Message: "+Deathlimiterror, Status.PASS);
			}
			else{
				report.updateTestLog("Death amount maximum limit Error", "Error message is not displayed", Status.FAIL);
			}
		}


		if(TPDYN.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
			sleep(500);
			report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[text()='Cover amount'])[2]"))).click();
		sleep(500);

		String TPDmaximumlimitError=driver.findElement(By.xpath("//span[@ng-if='maxTpdErrorFlag']")).getText();

		if(TPDmaximumlimitError.equalsIgnoreCase(TPDlimiterror)){
			report.updateTestLog("TPD amount maximum limit Error", "Expecetd Error Message: "+TPDmaximumlimitError, Status.PASS);
		}
		else{
			report.updateTestLog("TPD amount maximum limit Error", "Error message is not displayed", Status.FAIL);
		}

		if(IPYN.equalsIgnoreCase("Yes")&&FourteenHoursWork.equalsIgnoreCase("Yes")){


			//*****Select The Waiting Period********\\

				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);



			//*****Select The Benefit  Period********\\

				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);


			//****Enter the IP amount*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[text()='Cover amount'])[2]"))).click();
				sleep(500);
				String IPmaximumlimitError=driver.findElement(By.xpath(".//label[text()='The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum']")).getText();

				if(IPmaximumlimitError.equalsIgnoreCase(IPlimiterror)){
					report.updateTestLog("IP amount maximum limit Error", "Expecetd Error Message: "+IPmaximumlimitError, Status.PASS);
				}
				else{
					report.updateTestLog("IP amount maximum limit Error", "Error message is not displayed", Status.FAIL);
				}

			}
	}

else if (DeathYN.equalsIgnoreCase("Yes")&&TPDYN.equalsIgnoreCase("Yes")){

			System.out.println("Yes");
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
			sleep(500);
			report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);


		//*****TPD transfer amount******
			wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
			sleep(500);
			report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);


			wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).click();
		sleep(500);

		String TPDError=driver.findElement(By.xpath("//*[@ng-if='coverAmtErrFlag']")).getText();

		if(TPDError.equalsIgnoreCase(TPDAmtGreater)){
			report.updateTestLog("TPD Error", "Expecetd Error Message: "+TPDAmtGreater, Status.PASS);
		}
		else{
			report.updateTestLog("TPD Error", "Error message is not displayed", Status.FAIL);
		}

		}


	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}

}
