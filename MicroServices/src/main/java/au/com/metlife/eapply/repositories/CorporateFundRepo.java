/**
 * 
 */
package au.com.metlife.eapply.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import au.com.metlife.eapply.repositories.model.CorporateFund;

/**
 * @author 599063
 *
 */
public interface CorporateFundRepo extends JpaRepository<CorporateFund, Integer>{
	
	/*
	 * To find data from Corporate fund using fund code
	 */
	CorporateFund findByFundCode(String fundCode);
	
	CorporateFund findByFundCodeAndCorpFundCategoriesCategory(String fundCode, Integer category);

}
