package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPConfirmationsPageObjects extends PageObjectLibrary{
		

	public static final By linkTitle = getObjectIdentifaction("YBRIP.confirmation.title.drpdown.css");
	public static final By txtlastName = getObjectIdentifaction("YBRIP.confirmation.lastname.text.id");
	public static final By txtContact = getObjectIdentifaction("YBRIP.confirmation.contact.text.id");
	public static final By txtcontactOther = getObjectIdentifaction("YBRIP.confirmation.contactother.text.id");
	public static final By txtemail = getObjectIdentifaction("YBRIP.confirmation.email.text.id");
//	public static final By drpcontactTime = getObjectIdentifaction("YBRIP.confirmation.contacttime.dropdown.css");
	public static final By txtemailconfirm = getObjectIdentifaction("YBRIP.confirmation.confirm.email.text.id");
	public static final By txtaddress1 = getObjectIdentifaction("YBRIP.confirmation.address1.text.id");
	public static final By txtaddress2 = getObjectIdentifaction("YBRIP.confirmation.address2.text.id");

	public static final By txtSuburb = getObjectIdentifaction("YBRIP.confirmation.suburb.text.id");
	public static final By drpState = getObjectIdentifaction("YBRIP.confirmation.state.dropdown.css");

	
	public static final By txtPostcode = getObjectIdentifaction("YBRIP.confirmation.postcode.txt.id");
	public static final By chkacknowledge = getObjectIdentifaction("YBRIP.confirmation.acknowledge.chkbox.css");
	public static final By btnNext = getObjectIdentifaction("YBRIP.confirmation.next.btn.css");
}
