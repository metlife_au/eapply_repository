package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class Mortgage1300PaymentsObjects extends PageObjectLibrary {
	public static final By drpTitle = getObjectIdentifaction("1300.payment.title.dropdown.css");
	public static final By drpTitler = getObjectIdentifaction("1300.payment.title.right.dropdown.css");
	
	public static final By txtLastname = getObjectIdentifaction("1300.payment.lastname.text.id");
	public static final By txtLastnamer = getObjectIdentifaction("1300.payment.lastname.right.text.id");
	
	public static final By txtContact = getObjectIdentifaction("1300.payment.contactnumber.text.id");
	public static final By txtContactr = getObjectIdentifaction("1300.payment.contactnumber.right.text.id");
	
	public static final By txtContactOther = getObjectIdentifaction("1300.payment.contactnumber.other.text.id");
	public static final By txtContactOtherr = getObjectIdentifaction("1300.payment.contactnumber.other.right.text.id");
	
	public static final By txtemailid = getObjectIdentifaction("1300.payment.emailid.text.id");
	public static final By txtemailidr = getObjectIdentifaction("1300.payment.emailid.right.text.id");
	
	public static final By txtemailidconfirm = getObjectIdentifaction("1300.payment.emailid.confirm.text.id");
	public static final By txtemailidconfirmr = getObjectIdentifaction("1300.payment.emailid.confirm.right.text.id");
	
	public static final By txtaddress1 = getObjectIdentifaction("1300.payment.address1.text.id");
	public static final By txtaddress2 = getObjectIdentifaction("1300.payment.address2.text.id");
	public static final By txtsuburb = getObjectIdentifaction("1300.payment.suburb.confirm.text.id");
	public static final By drpstate = getObjectIdentifaction("1300.payment.state.dropdown.css");
	public static final By txtpostcode = getObjectIdentifaction("1300.payment.postcode.txt.id");
	
	public static final By rdbank = getObjectIdentifaction("1300.payment.bank.radio.css");
	public static final By txtAccountName = getObjectIdentifaction("1300.payment.bank.accountname.id");
	public static final By txtAccountNumber = getObjectIdentifaction("1300.payment.bank.accountnumber.id");
	public static final By txtbsbCode = getObjectIdentifaction("1300.payment.bank.bsbcode.id");
	public static final By txtbsbNumber = getObjectIdentifaction("1300.payment.bank.bsbnumber.id");
	
	public static final By chkagree = getObjectIdentifaction("1300.payment.bank.agree.checkbox.css");
	public static final By chkacknowledge = getObjectIdentifaction("1300.payment.bank.acknowledge.checkbox.css");
	
	public static final By btnSubmit = getObjectIdentifaction("1300.payment.submit.btn.css");
	
}