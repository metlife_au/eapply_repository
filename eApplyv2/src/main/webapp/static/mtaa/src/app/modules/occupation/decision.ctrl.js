(function(angular) {
    'use strict';
    /* global channel, inputData, token, moment */
    /**
     * @ngdoc controller
     * @name MtaaApp.controller:workRatingAcceptController
     *
     * @description
     * This is workRatingAcceptController description
     */
    angular
	.module('MtaaApp')
	.controller('workRatingAcceptController', workRatingAcceptController);
    workRatingAcceptController.$inject=['$scope','$rootScope','$location','appData','DownloadPDFService','ngDialog','$timeout','$window','urlService','fetchAppNumberSvc'];
    function workRatingAcceptController($scope,$rootScope,$location,appData,DownloadPDFService,ngDialog,$timeout,$window,urlService,fetchAppNumberSvc){
        var vm = this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
    	 $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};

      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br>' + 
                '<div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button>' + 
                ' <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };
        history.pushState(null, null, location.href);
	    window.onpopstate = function () {
	        history.go(1);
	    };
     // NPS survey popup

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div>' + 
       		'<!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button> <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      			    var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});

    	}
    angular
    .module('MtaaApp')
    .controller('workRatingDeclineController', workRatingDeclineController);
    workRatingDeclineController.$inject=['$scope','$rootScope','$location','appData','DownloadPDFService','ngDialog','$timeout','$window','urlService','fetchAppNumberSvc'];
    function workRatingDeclineController($scope,$rootScope,$location,appData,DownloadPDFService,ngDialog,$timeout,$window,urlService,fetchAppNumberSvc){
    	var vm=this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
    	 $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};

      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'>' + 
                '<div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };
        history.pushState(null, null, location.href);
	    window.onpopstate = function () {
	        history.go(1);
	    };
     // NPS survey popup

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>' + 
       		'<p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button> <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      			    var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});
    }
    
    angular
    .module('MtaaApp')
    .controller('workRatingMaintainController', workRatingMaintainController);
    workRatingMaintainController.$inject=['$scope','$rootScope','$location','appData','DownloadPDFService','ngDialog','$timeout','$window','urlService','fetchAppNumberSvc'];
    function workRatingMaintainController($scope,$rootScope,$location,appData,DownloadPDFService,ngDialog,$timeout,$window,urlService,fetchAppNumberSvc){
    	var vm=this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
    	 $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};

      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'>' + 
                '<button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };
        history.pushState(null, null, location.href);
	    window.onpopstate = function () {
	        history.go(1);
	    };
     // NPS survey popup

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'>' + 
       		'<div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div>' + 
       		'<div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button> <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      			    var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});
    }
    })(angular);