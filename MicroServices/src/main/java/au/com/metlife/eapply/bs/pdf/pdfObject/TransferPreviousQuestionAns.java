package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;

public class TransferPreviousQuestionAns implements Serializable{
	private String questiontext = null;
	private String answerText = null;

	public String getAnswerText() {
		return answerText;
	}
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	public String getQuestiontext() {
		return questiontext;
	}
	public void setQuestiontext(String questiontext) {
		this.questiontext = questiontext;
	}
}
