(function header () {
	if (jQuery(".page_wrap").length > 0) {
        var stickyOffset = jQuery('.pageWrap').offset().top + 20;
    } else {
        var stickyOffset = 20;
    }
 	 jQuery(window).scroll(function () {
        var scroll_pos = jQuery(window).scrollTop();
        if (scroll_pos >= stickyOffset) {
            jQuery('.global_header').addClass('fixedHeader');
            jQuery('.global_header').removeClass('notFixedHeader');
            jQuery('.pageWrap,.megaMenu').addClass('addMargin');
            jQuery(".loginOpen").css("top", "50px");
        }
        else {
            jQuery('.global_header').removeClass('fixedHeader');
            jQuery('.global_header').addClass('notFixedHeader');
            jQuery('.pageWrap,.megaMenu').removeClass('addMargin');
            jQuery(".loginOpen").css('top', '70px');
            //  jQuery(".ss-gac-table").css('top', '54px');
            if (jQuery(window).width() < 751) {
                jQuery(".loginOpen").css("top", "50px");
                // jQuery(".ss-gac-table").css('top', '103px');
            }
        }
    });
})();