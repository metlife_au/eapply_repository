import angular from 'angular';

// Create the module where our functionality can attach to
let transferModule = angular.module('app.transfer', []);

// Include our UI-Router config settings
import TransferConfig from './transfer.config';
transferModule.config(TransferConfig);


// Controllers
import TransferCoverCtrl from './transfer.cover.controller';
transferModule.controller('TransferCoverCtrl', TransferCoverCtrl);

import TransferAuraCtrl from './transfer.aura.controller';
transferModule.controller('TransferAuraCtrl', TransferAuraCtrl);

import TransferSummaryCtrl from './transfer.summary.controller';
transferModule.controller('TransferSummaryCtrl', TransferSummaryCtrl);

import TransferDecisionCtrl from './transfer.decision.controller';
transferModule.controller('TransferDecisionCtrl', TransferDecisionCtrl);


export default transferModule;
