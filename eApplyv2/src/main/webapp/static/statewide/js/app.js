// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var StateWideApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'restAPI', 'appDataModel', 'eApplyInterceptor']);
// configure our routes
StateWideApp.config(function($routeProvider,$httpProvider, IdleProvider, KeepaliveProvider, $templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});

    $routeProvider
	    .when('/', {
        templateUrl : 'static/statewide/landingpage.html',
        controller  : 'login',
  			reloadOnSearch: false,
        resolve: {
         urls:function(fetchUrlSvc){
           var path = 'CareSuperUrls.properties';
           return fetchUrlSvc.getUrls(path);
         }
        }
	    })
	    .when('/landing', {
	        templateUrl : 'static/statewide/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(fetchUrlSvc) {
             		var path = 'CareSuperUrls.properties';
	    		   	return fetchUrlSvc.getUrls(path);
	    	   }
	        }
	    })
	    .when('/quote', {
	        templateUrl : 'static/statewide/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    .when('/newmemberquote/:mode', {
	      templateUrl : 'static/statewide/newMember_changeCover.html',
	       controller  : 'newmemberquote'
	    })
	   .when('/quotecancel/:mode', {
	        templateUrl : 'static/statewide/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/statewide/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })
	     .when('/quotetransfer/:mode', {
	    	 templateUrl : 'static/statewide/coverDetails_transfer.html',
	        controller  :  'quotetransfer'
	    })
	     .when('/quoteoccchange/:mode', {
	        templateUrl : 'static/statewide/coverDetails_updateDetails.html',
	        controller  : 'quoteoccupdate'
	    })

	    /*Special cover*/
	    .when('/quotespecial/:mode', {
	        templateUrl : 'static/statewide/coverDetails_specialCover.html',
	        controller  : 'quotespecial'
	    })
	    .when('/auraspecial/:mode', {
	        templateUrl : 'static/statewide/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/specialoffersummary/:mode', {
	        templateUrl : 'static/statewide/special_offer_confirmation.html',
	        controller  : 'specialoffersummary'
	    })
	     .when('/specialofferaccept', {
	        templateUrl : 'static/statewide/special_decision_accepted.html',
	        controller  : 'specialOfferAcceptCtlr'
	    })
	     .when('/specialofferdecline', {
	        templateUrl : 'static/statewide/special_decision_decline.html',
	        controller  : 'specialOfferDeclineCtlr'
	    })
	    /*Special cover*/
	     .when('/lifeevent/:mode', {
	        templateUrl : 'static/statewide/coverDetails_LifeEvents.html',
	        controller  : 'lifeevent'
	     })
	     .when('/auralifeevent/:mode', {
	        templateUrl : 'static/statewide/aura_lifeEvent.html',
	        controller  : 'auraLifeEvent'
	     })
	     .when('/lifeeventaccept', {
	        templateUrl : 'static/statewide/lifeEvents_desicion_accepted.html',
	        controller  : 'lifeEventAcceptController'
	    })
	    .when('/lifeeventdecline', {
	        templateUrl : 'static/statewide/lifeEvents_decision_decline.html',
	        controller  : 'lifeEventDeclineController'
	    })
	    .when('/lifeeventunderwriting', {
	        templateUrl : 'static/statewide/lifeEvents_decision_ruw.html',
	        controller  : 'lifeEventRUWController'
	    })
	     .when('/auratransfer/:mode', {
	    	 templateUrl : 'static/statewide/aura_transferCover.html',
	        controller  : 'auratransfer'
	    })
	    .when('/auraocc/:mode', {
	    	 templateUrl : 'static/statewide/aura_updateDetails.html',
	        controller  : 'auraocc'
	    })
	    .when('/auraspecialoffer/:mode', {
	    	 templateUrl : 'static/statewide/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/aura', {
	        templateUrl : 'static/statewide/aura.html',
	        controller  : 'aura'
	    })
	    .when('/auracancel/:mode', {
	        templateUrl : 'static/statewide/aura_cancelCover.html',
	        controller  : 'auracancel'
	    })
	    .when('/summary', {
	        templateUrl : 'static/statewide/confirmation.html',
	        controller  : 'summary'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/statewide/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/statewide/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/statewide/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/statewide/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/statewide/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	     .when('/requirements/:clm', {
	        templateUrl : 'claimtracker/events.html',
	        controller  : 'requirements'
	    })
	    .when('/newsummary', {
	        templateUrl : 'static/statewide/newMember_confirmation.html',
	        controller  : 'newsummary'
	    })
	    .when('/workRatingSummary/:mode', {
	        templateUrl : 'static/statewide/workRating_confirmation.html',
	        controller  : 'workRatingSummary'
	    })

	    .when('/workRatingAccept', {
	        templateUrl : 'static/statewide/workRating_decision_accepted.html',
	        controller  : 'workRatingAcceptController'
	    })

	    .when('/workRatingDecline', {
	        templateUrl : 'static/statewide/workRating_decision_decline.html',
	        controller  : 'workRatingDeclineController'
	    })

	    .when('/workRatingMaintain', {
	        templateUrl : 'static/statewide/workRating_decision_maintained.html',
	        controller  : 'workRatingMaintainController'
	    })

	    .when('/transferSummary/:mode', {
	        templateUrl : 'static/statewide/transfer_confirmation.html',
	        controller  : 'transferSummary'
	    })

	    .when('/transferAccept', {
	        templateUrl : 'static/statewide/transfer_decision_accepted.html',
	        controller  : 'transferAcceptController'
	    })

	    .when('/transferDecline', {
	        templateUrl : 'static/statewide/transfer_decision_decline.html',
	        controller  : 'transferDeclineController'
	    })
	    .when('/newmemberaccept', {
	        templateUrl : 'static/statewide/newMember_decision_accepted.html',
	        controller  : 'newMemberAcceptController'
	    })
	    .when('/newmemberdecline', {
	        templateUrl : 'static/statewide/newMember_decision_decline.html',
	        controller  : 'newMemberDeclineController'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/statewide/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/statewide/cancel_decision.html',
	        controller  : 'cancelController'
	    })
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/statewide/timeout.html',
	        controller  : 'timeOutController'
	    })
	    .when('/cancelDecisionRuw', {
	        templateUrl : 'static/statewide/cancel_decision_ruw.html',
	        controller  : 'cancelRuwController'
	    })
	    .when('/cancelDecisionDecline', {
	        templateUrl : 'static/statewide/cancel_decision_decline.html',
	        controller  : 'cancelDecisionDeclineController'
	    })
	    .when('/lifeeventsummary/:mode', {
	        templateUrl : 'static/statewide/confirmation_LifeEvents.html',
	        controller  : 'lifeeventsummary'
	    })
	    .when('/convertMaintain/:mode', {
	        templateUrl : 'static/statewide/coverDetails_convertCover.html',
	        controller  : 'quoteConvert'
	    })
	    .when('/convertSummary', {
	        templateUrl : 'static/statewide/convert_confirmation.html',
	        controller  : 'convertSummary'
	    })

	     .when('/convertDecision', {
	        templateUrl : 'static/statewide/convertMaintain_decision_accepted.html',
	        controller  : 'convertAcceptController'
	    })

});
StateWideApp.constant('APP_CONSTANTS', function () {
	return {
		'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	}
});
StateWideApp.run(function($rootScope, $window) {
	$rootScope.logout = function() {
      $window.close();
    }
});
