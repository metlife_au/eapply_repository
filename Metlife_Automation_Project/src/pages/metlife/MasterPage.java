package pages.metlife;

import org.openqa.selenium.support.PageFactory;

import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * MasterPage Abstract class
 * @author Cognizant
 */
abstract class MasterPage extends ReusableLibrary
{
   
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	protected MasterPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
		
		PageFactory.initElements(driver, this);
	}	
}