/**
 * 
 */
package pages.metlife;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.HostplusPersonalDetailsPageObjects;
import uimap.metlife.PersonalDetailsPageObjects;


/**
 * @author sampath
 * 
 */
public class PersonalDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public PersonalDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub

		/*if (!driver.getTitle().contains("MTAA Super")) {
			throw new IllegalStateException(
					"MTAA Super page expected, but not displayed!");
		}*/
	}
	
	public PersonalDetailsPage enterPersonalDetails() {
		fillInPersonalDetails();
		return new PersonalDetailsPage(scriptHelper);
	} 
	
	public PersonalDetailsPage enterHostplusPersonalDetails() {
		fillInHostplusPersonalDetails();
		return new PersonalDetailsPage(scriptHelper);
	}
	
	
	public PersonalDetailsPage enterHostplusContactDetails() {
		fillInHostplusContactDetails();
		return new PersonalDetailsPage(scriptHelper);
	}
	
	public PersonalDetailsPage enterHostplusOccupationDetails() {
		fillInHostplusOccupationDetails();
		return new PersonalDetailsPage(scriptHelper);
	}
	
	public PersonalDetailsPage enterACCSPersonalDetails() {
		fillInACCSPersonalDetails();
		return new PersonalDetailsPage(scriptHelper);
	}

	private void fillInPersonalDetails() {
		
		final Properties properties;
		
		properties = Settings.getInstance();
		
		storeSessionThreadData("dyn_currentBrowser",properties.getProperty("currentBrowser"));
		System.out.println("The Current Browser is :: "+getStringValueFromSessionThreadData("dyn_currentBrowser"));
		
		String clntRefNum = dataTable.getData("MetLife_Data", "login.memNo");
		String strFirstName = dataTable.getData("MetLife_Data", "login.fname");
		String strLastName = dataTable.getData("MetLife_Data", "login.lname");
		String strDob = dataTable.getData("MetLife_Data", "login.dob");
		String strMemberType = dataTable.getData("MetLife_Data", "login.membertype");
		String strTCName = dataTable.getData("MetLife_Data", "TC_ID");
		String strCampaignCode = dataTable.getData("MetLife_Data", "campaignCode");
		String strEmail = dataTable.getData("MetLife_Data", "personal.emailAddress");
		storeSessionThreadData("dyn_CompleteName",strFirstName.trim()+strLastName.trim());
		
		try{
					
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtClientRefId),"SET","Client Reference Number",clntRefNum);
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtFirstname),"SET","First Name",strFirstName);
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtSurname),"SET","Surname",strLastName);
			if(strTCName.contains("PSUP")||strTCName.contains("GUILD")||strTCName.contains("VICS")||strTCName.contains("FirstSuper")||strTCName.contains("MTAA")||strTCName.contains("REIS"))
				tryAction(driver.findElement(PersonalDetailsPageObjects.txtDateOfBirth),"SET","Date Of Birth", strDob);
			
			if(!(strTCName.contains("PSUP")||strTCName.contains("GUILD")||strTCName.contains("VICS")||strTCName.contains("FirstSuper")||strTCName.contains("MTAA")||strTCName.contains("REIS")))
				tryAction(driver.findElement(PersonalDetailsPageObjects.txtDateOfBirth),"SET","Date Of Birth",getPremiumDOB(Integer.parseInt("-"+strDob)));
			
			if(strTCName.contains("PWC")||strTCName.contains("PSUP")){
				tryAction(driver.findElement(PersonalDetailsPageObjects.txtCompaignCode),"SET","Campaign Code",strCampaignCode);
				tryAction(driver.findElement(PersonalDetailsPageObjects.txtBrokerMailId),"SET","Broker Email",strEmail);
				tryAction(driver.findElement(PersonalDetailsPageObjects.btnRetrieveApplication),"Click","Go To Next Page");
			}
			if(!(strTCName.contains("PWC")||strTCName.contains("PSUP")||strTCName.contains("GUILD"))){
				tryAction(driver.findElement(PersonalDetailsPageObjects.lstMemberType),"DropDownSelect","Member Type",strMemberType);
				tryAction(driver.findElement(PersonalDetailsPageObjects.btnNewApplication),"clkradio","New Application");
			}
			
			if(strTCName.contains("GUILD")){
				tryAction(driver.findElement(PersonalDetailsPageObjects.btnNewApplication),"clkradio","New Application");
			}
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInHostplusPersonalDetails() {
		
		final Properties properties;
		
		properties = Settings.getInstance();
		
		String clntRefNum = dataTable.getData("MetLife_Data", "login.memNo");
		String strFirstName = dataTable.getData("MetLife_Data", "login.fname");
		String strLastName = dataTable.getData("MetLife_Data", "login.lname");
		String strDob = dataTable.getData("MetLife_Data", "login.dob");
		try{
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.txtClientRefId),"SET","Client Reference Number",clntRefNum);
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.txtFirstname),"SET","First Name",strFirstName);
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.txtSurname),"SET","Surname",strLastName);
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.txtDateOfBirth),"SET","Date Of Birth",strDob);
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.btnNewApplication),"Click","New Application");
			//tryAction(getWebElementBasedOnText(HostplusPersonalDetailsPageObjects.btnNonmeberHostplusPopup,"Yes"),"Click","Yes");
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInHostplusContactDetails() {
		
		String txtEmailId = dataTable.getData("MetLife_Data", "personal.emailAddress");
		String txtContactNumber = dataTable.getData("MetLife_Data", "personal.ContactDetails");
		String radContactTime = dataTable.getData("MetLife_Data", "personal.ContactTime");
		String strGender = dataTable.getData("MetLife_Data", "personal.gender");
		try{
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.txtEmailid),"SET","Email address ",txtEmailId);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.txtContactNumber),"SET","Preferred Contact Number",txtContactNumber);
			tryAction(fluentWaitElements(radContactTime.equalsIgnoreCase("morning")?HostplusPersonalDetailsPageObjects.radMorningTime:HostplusPersonalDetailsPageObjects.radAfternoonTime),"clkradio","What time of day do you prefer to be contacted? - "+radContactTime);
			sleep(500);
			if(strGender.equalsIgnoreCase("male")){
				System.out.println("in male");
			WebElement Gender=driver.findElement(By.id("mem_gender_male_id__xc_r"));
			Gender.click();
			}
			else
			{
				WebElement Gender=driver.findElement(By.id("mem_gender_female_id__xc_r"));
				Gender.click();
			}
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnContactDetContinue),"Click","Contact Deatils Continue");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInHostplusOccupationDetails() {
		
		String str15HrsWork = dataTable.getData("MetLife_Data", "change.work15hours");
		String strOwnBusiness = dataTable.getData("MetLife_Data", "OwnBusiness");
		String strCapableOfPerformDuties = dataTable.getData("MetLife_Data", "CapableOfPerformDuties");
		String strPrCitizen = dataTable.getData("MetLife_Data", "personal.citizen");
		String strIndustry = dataTable.getData("MetLife_Data", "change.industry");
		String strOccupation = dataTable.getData("MetLife_Data", "change.occupation");
		String strWorkInOffice = dataTable.getData("MetLife_Data", "change.workwholly");
		String strTertiaryQualification = dataTable.getData("MetLife_Data", "change.tertiary");
		String strAnnualSalary = dataTable.getData("MetLife_Data", "change.salary");
		String strDCFixUnit = dataTable.getData("MetLife_Data", "change.tcr");
		String strDCAmt = dataTable.getData("MetLife_Data", "personal.deathCover");
		
		
		try{
			tryAction(fluentWaitElements(str15HrsWork.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.rad15HrsYes:HostplusPersonalDetailsPageObjects.rad15HrsNo),"clkradio","Do you work 15 or more hours per week? - "+str15HrsWork);
			tryAction(fluentWaitElements(strOwnBusiness.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radRegIncomeYes:HostplusPersonalDetailsPageObjects.radRegIncomeNo),"clkradio","Do you, directly or indirectly, own all or part of a business from which you earn your regular income? - "+strOwnBusiness);
			tryAction(fluentWaitElements(strCapableOfPerformDuties.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radDutiesYes:HostplusPersonalDetailsPageObjects.radDutiesNo),"clkradio","Capable of, and performing all of the usual identifiable duties from your occupation, without restriction due to injury or illness? - "+strCapableOfPerformDuties);
			if(isElementPresent(HostplusPersonalDetailsPageObjects.radPrCitizenYes))
				tryAction(fluentWaitElements(strPrCitizen.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radPrCitizenYes:HostplusPersonalDetailsPageObjects.radPrCitizenNo),"clkradio","Are you a citizen or permanent resident of Australia? - "+strPrCitizen);
			sleep(2000);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstIndustry),"MoveToElm","What industry do you work in?");
			tryAction(fluentWaitElement(HostplusPersonalDetailsPageObjects.lstIndustry),"specialDropdown", "What industry do you work in?", strIndustry);
			sleep();
			tryAction(fluentWaitElement(HostplusPersonalDetailsPageObjects.lstOccupation),"specialDropdown", "What industry do you work in?",strOccupation);
			//tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstIndustry),"click","What industry do you work in?",strIndustry);
			//tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstIndustry),"linktext","What is your occupation?",strIndustry);
			sleep();
			//tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstOccupation),"click","What industry do you work in?",strIndustry);
			//tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstOccupation),"linktext","What is your occupation?",strOccupation);
			tryAction(fluentWaitElements(strWorkInOffice.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radWorkDutiesYes:HostplusPersonalDetailsPageObjects.radWorkDutiesNo),"clkradio","Are your duties entirely undertaken within an office environment? Yes No - "+strWorkInOffice);
			tryAction(fluentWaitElements(strTertiaryQualification.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radTeritaryYes:HostplusPersonalDetailsPageObjects.radTeritaryNo),"clkradio","Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession? - "+strTertiaryQualification);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.txtAnnualSalary),"SET","What is your annual salary? ",strAnnualSalary);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnOccupationDetContinue),"Click","Occupation Deatils Continue");
			
			
			sleep();
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstDCFunction),"click","DC I want to?","Increase your cover");
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstDCFunction),"linktext","What is your occupation?","Increase your cover");
			tryAction(fluentWaitElements(strDCFixUnit.equalsIgnoreCase("fixed")?HostplusPersonalDetailsPageObjects.radDCFixed:HostplusPersonalDetailsPageObjects.radDCUnitised),"clkradio","Cover Type "+strDCFixUnit);
			sleep(3000);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.txtDCAmount),"SET","Cover Amount is ",strDCAmt);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.txtDCAmount),"TAB","TAB on Death Cover");
			sleep(3000);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstTPDFunction),"MoveToElm","TPD I want to");
			sleep(1000);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstTPDFunction),"specialDropdown","TPD I want to","No change");
			sleep();
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstLifestageFunction),"click","Lifestage I want to","No change");
			sleep(1000);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstLifestageFunction),"linktext","Lifestage I want to","No change");
			sleep();
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.btnHostCalculateQuoteById),"MoveToElm","CalculateQuote");
			sleep();
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.btnHostCalculateQuoteById),"click","CalculateQuote ","CalculateQuote ");
			sleep();
			//tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.btnHostCalculateQuoteById),"TAB","CalculateQuote ","CalculateQuote ");
			//sleep();
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.btnContinuePage),"click","Continue ","Click in Continue.");
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInACCSPersonalDetails() {
		String clntRefNum = dataTable.getData("MetLife_Data", "login.memNo");
		String strFirstName = dataTable.getData("MetLife_Data", "login.fname");
		String strLastName = dataTable.getData("MetLife_Data", "login.lname");
		String strDob = dataTable.getData("MetLife_Data", "login.dob");
		
		try{
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtClientRefId),"SET","Client Reference Number",clntRefNum);
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtFirstname),"SET","First Name",strFirstName);
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtSurname),"SET","Surname",strLastName);
			tryAction(driver.findElement(PersonalDetailsPageObjects.txtDateOfBirth),"SET","Date Of Birth",strDob);
			tryAction(driver.findElement(PersonalDetailsPageObjects.btnNewApplication),"clkradio","New Application");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}	
}
