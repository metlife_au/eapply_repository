(function(angular){
'use strict';
/* global channel,  moment */
angular
.module('CorporateApp')
.controller('summaryCtrl',summaryCtrl);
summaryCtrl.$inject= ['$rootScope', '$location','$timeout','$window', 'fetchUrlSvc', 'ngDialog', 'appData','saveEapplyData', 'submitEapplySvc','PersistenceService', 'auraRespSvc', 'fetchIndustryList','inputDataService'];

function summaryCtrl( $rootScope, $location, $timeout, $window, fetchUrlSvc, ngDialog, appData,saveEapplyData, submitEapplySvc, PersistenceService, auraRespSvc, fetchIndustryList,inputDataService){
var vm = this;
vm.changeCoverSummary = {};
vm.init = function() {
    $window.scrollTo(0, 0);
    vm.auraDetails={};
    vm.changeCoverSummary.auraDetails={};
    vm.urlList = fetchUrlSvc.getUrlList();
    vm.categoryDetails = appData.getFundCategoryDetails();
    vm.brokernumber = vm.categoryDetails.partner.broker.brokerCnctNum;

    angular.extend(vm.changeCoverSummary, appData.getAppData());
    vm.personalDetails = vm.changeCoverSummary.personalDetails || {};
    if(vm.personalDetails.gender===undefined || vm.personalDetails.gender===null){
    	vm.personalDetails.gender=vm.changeCoverSummary.occupationDetails.gender;
    }
    if(vm.changeCoverSummary.corpFundCode===null ||    vm.changeCoverSummary.corpFundCode===undefined){
    	vm.coverDetails = inputDataService.getCoverDetails();
    	vm.changeCoverSummary.corpFundCode=vm.coverDetails.fundId;
    }
    if(!vm.changeCoverSummary.auraDisabled && appData.getChangeCoverAuraDetails()!=null){
   	 	vm.auraDetails = appData.getChangeCoverAuraDetails();
   	 	vm.changeCoverSummary.auraDetails = vm.auraDetails;
   }
  
    vm.auraDisableCustom = (vm.changeCoverSummary.auraDisabled === 'false' || vm.changeCoverSummary.auraDisabled === false) ? false : true;
    if(vm.auraDisableCustom){
    	vm.auraDetails.overallDecision = 'ACC';
    }
    if(vm.auraDetails.overallDecision === undefined && !vm.auraDisableCustom){
    	var savedData = appData.getAppData();
    	vm.changeCoverSummary.auraDetails.overallDecision = savedData.overallDecision;
    	vm.changeCoverSummary.auraDetails.clientMatchReason = savedData.clientMatchReason;
    	
    	vm.changeCoverSummary.auraDetails.clientMatched = savedData.clientMatched;
    	vm.changeCoverSummary.auraDetails.deathAuraResons = savedData.deathAuraResons;
    	vm.changeCoverSummary.auraDetails.deathDecision = savedData.deathDecision;
    	vm.changeCoverSummary.auraDetails.deathExclusions = savedData.deathExclusions;
    	vm.changeCoverSummary.auraDetails.deathLoading = savedData.deathLoading;
    	vm.changeCoverSummary.auraDetails.deathOrigTotalDebitsValue = savedData.deathOrigTotalDebitsValue;
    	vm.changeCoverSummary.auraDetails.deathResons = savedData.deathResons;
    	vm.changeCoverSummary.auraDetails.ipAuraResons = savedData.ipAuraResons;
    	vm.changeCoverSummary.auraDetails.ipDecision = savedData.ipDecision;
    	vm.changeCoverSummary.auraDetails.ipExclusions = savedData.ipExclusions;
    	vm.changeCoverSummary.auraDetails.ipLoading = savedData.ipLoading;
    	vm.changeCoverSummary.auraDetails.ipOrigTotalDebitsValue = savedData.ipOrigTotalDebitsValue;
    	vm.changeCoverSummary.auraDetails.ipResons = savedData.ipResons;
    	vm.changeCoverSummary.auraDetails.tpdAuraResons = savedData.tpdAuraResons;
    	vm.changeCoverSummary.auraDetails.tpdDecision = savedData.tpdDecision;
    	vm.changeCoverSummary.auraDetails.tpdExclusions = savedData.tpdExclusions;
    	vm.changeCoverSummary.auraDetails.tpdLoading = savedData.tpdLoading;
    	vm.changeCoverSummary.auraDetails.tpdOrigTotalDebitsValue = savedData.tpdOrigTotalDebitsValue;
    	vm.changeCoverSummary.auraDetails.tpdResons = savedData.tpdResons;
    	if((vm.changeCoverSummary.auraDetails.deathLoading !== null && vm.changeCoverSummary.auraDetails.deathLoading !== '0.0') || (vm.changeCoverSummary.auraDetails.tpdLoading !== null && vm.changeCoverSummary.auraDetails.tpdLoading !== 0.0) || (vm.changeCoverSummary.auraDetails.ipLoading !== null && vm.changeCoverSummary.auraDetails.ipLoading !== 0.0)){
    		vm.auraDetails.specialTerm = true;
        	vm.changeCoverSummary.auraDetails.specialTerm = true;
    	}else if(vm.changeCoverSummary.auraDetails.deathExclusions!=null||vm.changeCoverSummary.auraDetails.ipExclusions!=null||vm.changeCoverSummary.auraDetails.tpdExclusions!=null){
    		vm.auraDetails.specialTerm = true;
        	vm.changeCoverSummary.auraDetails.specialTerm = true;
    	}
    	else{
    		vm.auraDetails.specialTerm = false;
        	vm.changeCoverSummary.auraDetails.specialTerm = false;
    	}	
    }

    vm.changeCoverSummary.dob = moment(vm.changeCoverSummary.dob, 'DD/MM/YYYY').format('DD/MM/YYYY'); 
    vm.eventName = vm.changeCoverSummary.eventName;
    vm.changeCoverSummary.auraDisabled = (vm.changeCoverSummary.auraDisabled === 'false' || vm.changeCoverSummary.auraDisabled === false) ? false : true;
    if(vm.changeCoverSummary.contactType === '1') {
			vm.contactType = 'Mobile';
		} else if(vm.changeCoverSummary.contactType === '2') {
			vm.contactType = 'Home';
		} else if(vm.changeCoverSummary.contactType === '3') {
			vm.contactType = 'Work';
		} else {
			vm.contactType = '';
    }
    if(vm.changeCoverSummary.otherContactType === '1') {
		vm.otherContactType = 'Mobile';
	} else if(vm.changeCoverSummary.otherContactType === '2') {
		vm.otherContactType = 'Home';
	} else if(vm.changeCoverSummary.otherContactType === '3') {
		vm.otherContactType = 'Work';
	} else {
		vm.otherContactType = '';
	}
	var smokerCustom;
    if(vm.changeCoverSummary.occupationDetails.smoker === undefined){
  		if(vm.changeCoverSummary.occupationDetails.smokerQuestion === '1'){
  			smokerCustom= true;
	     }else{
	    	  smokerCustom = false;
	     }
  	}else{
  		if(vm.changeCoverSummary.occupationDetails.smoker=== 'Yes'){
  			 smokerCustom = true;
	     }else{
	    	  smokerCustom = false;
	     }
  	}

    // Industry name
    vm.changeCoverSummary.industryName = fetchIndustryList.getIndustryName(vm.changeCoverSummary.occupationDetails.industryCode);
	  	if(vm.changeCoverSummary.occupationDetails.smoker === undefined){
	  		if(vm.changeCoverSummary.occupationDetails.smokerQuestion === '1'){
	  		   	   vm.changeCoverSummary.occupationDetails.smoker = 'Yes';
		     }else{
		    	 vm.changeCoverSummary.occupationDetails.smoker = 'No';
		     }
	  	}
  };

  vm.init();
  vm.go = function (path) {
	  $location.path(path);
  };
  

  vm.navigateToLandingPage = function () {
	  var navigatePopUp='<div class="ngdialog-content"><div class="modal-body">';
	  navigatePopUp +='<!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br>';
	  navigatePopUp +='<div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter">';
	  navigatePopUp +='<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button>';
	  navigatePopUp +='<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
	  navigatePopUp +='</div></div>';
	  ngDialog.openConfirm({
        template:navigatePopUp,
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
      }).then(function() {
        $location.path('/landing');
      }, function(e){
        if(e==='oncancel') {
          return false;
        }
      });
   };

   vm.summarySaveAndExitPopUp = function (hhText) {
	   var saveAndExitPopUp='<div class="ngdialog-content"><div class="modal-header">';
	   saveAndExitPopUp +='<h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div>';
	   saveAndExitPopUp +='<div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p>';
	   saveAndExitPopUp +='<div id="tips_text">'+hhText+'</div>  <p></p> ';
	   saveAndExitPopUp += '</div></div>';
	   saveAndExitPopUp += '<!-- Row ends --></div><div class="ngdialog-buttons aligncenter">';
	   saveAndExitPopUp += '<button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button>';
	   saveAndExitPopUp += '</div></div>';
	   var dialog1 = ngDialog.open({
           template: saveAndExitPopUp,
         className: 'ngdialog-theme-plain custom-width',
         preCloseCallback: function(value) {
                var url = '/landing';
                $location.path( url );
                return true;
         },
         plain: true
     });
   };
   vm.clickToOpen = function (hhText, $event) {
    $event.stopPropagation();
		var dialog = ngDialog.open({
			template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-12 text-center"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div></div></div>',
			className: 'ngdialog-theme-plain',
			plain: true
		});
		dialog.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
   };
   vm.saveSummary = function() {
     vm.changeCoverSummary.lastSavedOn = 'SummaryPage';
     
     if(vm.changeCoverSummary.occupationDetails.smoker === 'Yes') {
       	vm.changeCoverSummary.occupationDetails.smokerQuestion = 1;
   	}else{
   		vm.changeCoverSummary.occupationDetails.smokerQuestion = 2;
   	}
     var saveSummaryObject = angular.copy(vm.changeCoverSummary);
     saveSummaryObject = angular.extend(saveSummaryObject, vm.inputDetails);   
     
     if(vm.auraDetails != null){
       saveSummaryObject = angular.extend(saveSummaryObject, vm.auraDetails);
     }
     
     saveSummaryObject.eventName =  vm.changeCoverSummary.eventName;
     
     saveEapplyData.reqObj(vm.urlList.saveCorporateEapply, saveSummaryObject).then(function(response) {
       vm.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ vm.changeCoverSummary.appNum +'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
     });
   };
   var ackCheckCCGC;
   var ackCheckLE;
   vm.navigateToDecision = function() {
     ackCheckCCGC = $('#generalConsentLabel').hasClass('active');

     if(vm.auraDetails != null){
      if(vm.auraDetails.specialTerm !== null && vm.auraDetails.specialTerm === true){
    	   ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');
        }
     }

     if(ackCheckCCGC && ((vm.auraDetails != null && !vm.auraDetails.specialTerm) || (vm.auraDetails != null && vm.auraDetails.specialTerm && ackCheckLE) || vm.auraDetails==null)){
       vm.changeCoverSummary.LEFlag = false;
       vm.CCGCackFlag = false;
       
        var tempobj = {};
       angular.forEach(vm.eventList, function(event, key) {
    	   if(vm.changeCoverSummary.eventName === event.cde){
    		   tempobj = event;
    	   }
       });
       vm.changeCoverSummary.eventDesc =  tempobj.desc;  
       vm.changeCoverSummary.smoker =vm.changeCoverSummary.occupationDetails.smoker;
       vm.changeCoverSummary.lastSavedOn = '';
       var submitSummaryObject = angular.extend(vm.changeCoverSummary, vm.inputDetails);
       if(vm.auraDetails != null){
         submitSummaryObject = angular.extend(submitSummaryObject, appData.getChangeCoverAuraDetails());
       }
       auraRespSvc.setResponse(submitSummaryObject);
       submitEapplySvc.submitObj(vm.urlList.submitEapplyUrl, submitSummaryObject).then(function(response) {
         if(response.data) {
             PersistenceService.setPDFLocation(response.data.clientPDFLocation);
             PersistenceService.setNpsUrl(response.data.npsTokenURL);
             if(vm.auraDetails.overallDecision!==null && vm.auraDetails.overallDecision !== ''){
            	 appData.setSummaryPageAuradecision (appData.getChangeCoverAuraDetails());
            			 $location.path('/decision');
            	}
         }
         else {
                 $window.scrollTo(0, 0);
                 $rootScope.$broadcast('enablepointer');
                 throw {message: 'No data found'};
             }
       }, function(err){
         vm.errorOccured = true;
         $window.scrollTo(0, 0);
       });
     } else {
         if(ackCheckCCGC) {
           vm.CCGCackFlag = false;
         } else{
           vm.CCGCackFlag = true;
         }
         if(ackCheckLE) {
           vm.changeCoverSummary.LEFlag = false;
         } else {
           vm.changeCoverSummary.LEFlag = true;
         }
         vm.scrollToUncheckedElement();
     }
   };

   vm.scrollToUncheckedElement = function(){
	   var ids;
	   var elements;
     if(vm.auraDetails!=null && vm.auraDetails.specialTerm){
       elements = [ackCheckLE, ackCheckCCGC];
       ids = ['lodadingExclusionLabel', 'generalConsentLabel'];
     } else{
        elements = [ackCheckCCGC];
       	ids = ['generalConsentLabel'];
     }
   };

   vm.checkAckStateGC = function(){
     $timeout(function(){
       ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
         if(ackCheckCCGC){
           vm.CCGCackFlag = false;
         }else{
           vm.CCGCackFlag = false;
         }
     }, 10);
   };

   vm.checkAckStateLE = function(){
     $timeout(function(){
       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');

         if(ackCheckLE){
           vm.changeCoverSummary.LEFlag = false;
         }else{
           vm.changeCoverSummary.LEFlag = true;
         }
     }, 10);
   };
}

})(angular);