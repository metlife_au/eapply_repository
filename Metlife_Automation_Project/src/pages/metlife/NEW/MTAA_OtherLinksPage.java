package pages.metlife.NEW;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class MTAA_OtherLinksPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public MTAA_OtherLinksPage MTAAOtherLinks() {
		OtherLinks();		
		
		
		return new MTAA_OtherLinksPage(scriptHelper);
	}

	public MTAA_OtherLinksPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void OtherLinks() {

		
		
		try{
			
			
			
			//****Click on Contact Us  Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@title='Contact Us'])[1]"))).click();
			
			report.updateTestLog("Contact Us", "Contact Us Link is Clicked", Status.PASS);
			
			sleep(1500);
		
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		     
			driver.switchTo().window(tabs.get(1));
			sleep(1000);
			String text=driver.findElement(By.xpath("//h1[contains(text(),'This site can�t be reached')]")).getText();
			report.updateTestLog("Text", text+"is Displayed", Status.PASS);		 
			driver.close();
			driver.switchTo().window(tabs.get(0));
		    
			sleep(2000);
			
	
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}





