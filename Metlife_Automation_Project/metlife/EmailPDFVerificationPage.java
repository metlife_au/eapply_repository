package pages.metlife;

import org.openqa.selenium.By;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class EmailPDFVerificationPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public EmailPDFVerificationPage checkEmailForApplication() {
		verifyPdfNEmail();
		return new EmailPDFVerificationPage(scriptHelper);
	}
	
	public EmailPDFVerificationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void verifyPdfNEmail(){
		try {
			
			String strFirstName = dataTable.getData("MetLife_Data", "login.fname");
			String strLastName = dataTable.getData("MetLife_Data", "login.lname");
			
			tryAction(waitForClickableElement((By.linkText("here"))),"click","here");
			sleep(3000);
			System.out.println(parsePDFDoc(getStringValueFromSessionThreadData("dyn_CompleteName")));
			sleep(60000);
			System.out.println(deleteMessages(strFirstName+" "+strLastName));
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
