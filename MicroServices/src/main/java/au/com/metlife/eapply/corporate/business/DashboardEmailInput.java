package au.com.metlife.eapply.corporate.business;

public class DashboardEmailInput {
private String fundCode;
private String emailid;
private String firstName;
private String lastName;
private String applicationNumber;
public String getFundCode() {
	return fundCode;
}
public void setFundCode(String fundCode) {
	this.fundCode = fundCode;
}
public String getEmailid() {
	return emailid;
}
public void setEmailid(String emailid) {
	this.emailid = emailid;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getApplicationNumber() {
	return applicationNumber;
}
public void setApplicationNumber(String applicationNumber) {
	this.applicationNumber = applicationNumber;
}
}
