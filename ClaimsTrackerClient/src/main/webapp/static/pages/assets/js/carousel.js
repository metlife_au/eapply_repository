(function carousel(){
    $('.carousel').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                dots: false
                //slidesToShow: 2,
                //slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                dots: false,
                arrows: false
                //slidesToShow: 1,
               // slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
})();
