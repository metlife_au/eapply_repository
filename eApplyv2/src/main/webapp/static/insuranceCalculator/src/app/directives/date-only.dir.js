export default class dateOnly {
  constructor() {
    this.restrict = 'EA';
    this.replace = false;
    this.scope = false;
    this.require = 'ngModel';
  }

  link(scope, element, attrs, ngModelCtrl) {
    	ngModelCtrl.$parsers.push(function (text) {    		
    		if (text) {
    			
                var transformedInput = text.replace(/[^0-9/]/gi, '');

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            
    		}
            return undefined;
    	});
    }
}
