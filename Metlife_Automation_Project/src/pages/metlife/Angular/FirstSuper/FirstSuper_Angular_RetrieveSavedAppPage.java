package pages.metlife.Angular.FirstSuper;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_RetrieveSavedAppPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public FirstSuper_Angular_RetrieveSavedAppPage RetreiveApp_FirstSuper() {
		RetreiveApp();		
		
		
		return new FirstSuper_Angular_RetrieveSavedAppPage(scriptHelper);
	}

	public FirstSuper_Angular_RetrieveSavedAppPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void RetreiveApp() {

		
		
		try{
			
                 //****Click on Life Event Link*****\\
			sleep(2500);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Saved applications']"))).click();		
			report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
			sleep(3500);
			
			for(int i=1;i<10;i++){
				
				String Status=driver.findElement(By.xpath("//table/tbody/tr["+i+"]/td[5]")).getText();
				sleep(250);
				if(Status.equalsIgnoreCase("Pending")){
					
					driver.findElement(By.xpath("//table/tbody/tr["+i+"]/td[1]/a")).click();
					sleep(500);
					
					break;
				}
			}
			sleep(1000);
		if(driver.getPageSource().contains("Duty of disclosure")){
			report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		}
		else{
			report.updateTestLog("Retrieve Saved Application", "No Applications to Retrieve", Status.PASS);
		}
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}






