package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;

public class PersonalDetail implements Serializable{
	private String label = null;
	private String value = null;
	
	public PersonalDetail() {
	}
	public PersonalDetail(String label, String value) {
		this.label = label;
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
