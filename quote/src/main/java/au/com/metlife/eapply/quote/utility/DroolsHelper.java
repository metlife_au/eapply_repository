package au.com.metlife.eapply.quote.utility;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;

public class DroolsHelper {
	
	
	public static CoverDetailsRuleBO invokeUC16BusinessRules(CoverDetailsRuleBO coverDetailsRuleBO,String ruleFileLocation){
		System.out.println("----------getting called invokeUC16BusinessRules ------- ");
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		try {
			ruleFileLocation=ruleFileLocation+coverDetailsRuleBO.getRuleFileName();
			if(coverDetailsRuleBO.getKbase()!=null){
				kbase=coverDetailsRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLocation);
				ksession = kbase.newStatelessKnowledgeSession();
			}
			if(coverDetailsRuleBO!=null){
				ksession.execute(coverDetailsRuleBO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		return coverDetailsRuleBO;

	}
	
	public static InstProductRuleBO invokeInstProductBusinessRule(InstProductRuleBO instProductRuleBO, String ruleFileLocation){
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		try {
			ruleFileLocation=ruleFileLocation+instProductRuleBO.getRuleFileName();
			if(instProductRuleBO.getKbase()!=null){
				kbase=instProductRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLocation);
				ksession = kbase.newStatelessKnowledgeSession();
			}
			if(instProductRuleBO!=null){
				ksession.execute(instProductRuleBO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		return instProductRuleBO;

	}
	
	public static KnowledgeBase getRuleSession(String ruleFileLoc,String ruleFileName){
		KnowledgeBase kbase=null;
		try {
			ruleFileLoc=ruleFileLoc+ruleFileName;
			kbase = readKnowledgeBase(ruleFileLoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return kbase;
	}

	/**
	 * This method will read the rules from the sample.xls file.
	 * Each row in the excel sheet will represent as a business rule.
	 *
	 * @return KnowledgeBase object
	 * @throws Exception
	 */

	private static KnowledgeBase readKnowledgeBase(String ruleFileLoc) throws Exception {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		config.setInputType(DecisionTableInputType.XLS);
		kbuilder.add(ResourceFactory.newFileResource(ruleFileLoc), ResourceType.DTABLE, config);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error: errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		return kbase;
	}

}
