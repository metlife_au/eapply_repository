package vicsSuper_Objects;

import org.openqa.selenium.By;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class ChangeCoverObjects extends ReusableLibrary {

	public ChangeCoverObjects(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	// ************************************************************************************************************************
	// -----------------------------------------Personal Details
	// ************************************************************************************************************************
	public static By obj_PersonalDetails_dutyOfDisclosureTxt = By.xpath(".//*[@name='formduty']//strong");
	public static By obj_PersonalDetails_dutyOfDisclosure = By.xpath(".//input[@id='dodlabelCheck']/parent::*");
	public static By obj_PersonalDetails_privacyStatement = By.xpath(".//input[@id='privacylabelCheck']/parent::*");
	// ContactDetails
	public static By obj_PersonalDetails_email = By.name("contactEmail");
	public static By obj_PersonalDetails_contactType = By.name("contactType");
	public static By obj_PersonalDetails_phNO = By.name("contactPhone");
	public static By obj_PersonalDetails_Morning = By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]");
	public static By obj_PersonalDetails_Afternoon = By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]");
	public static By obj_PersonalDetails_Male = By.xpath("(.//input[@name='gender']/parent::*)[1]");
	public static By obj_PersonalDetails_Female = By.xpath("(.//input[@name='gender']/parent::*)[2]");
	// OccupationDetails
	public static By obj_PersonalDetails_FourteenHrsYes = By.xpath("//input[@name='fourteenHrsQuestion ']/parent::*");
	public static By obj_PersonalDetails_FourteenHrsNo = By.xpath("//input[@name='fourteenHrsQuestion']/parent::*");
	public static By obj_PersonalDetails_CitizenYes = By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]");
	public static By obj_PersonalDetails_CitizenNo = By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]");
	public static By obj_PersonalDetails_Industry = By.name("industry");
	public static By obj_PersonalDetails_Occupation = By.name("occupation");
	public static By obj_PersonalDetails_AddQuest1aYes = By
			.xpath("(//input[@name='occoffmangerating1A']/parent::*)[1]");
	public static By obj_PersonalDetails_AddQuest1aNo = By.xpath("(//input[@name='occoffmangerating1A']/parent::*)[2]");
	public static By obj_PersonalDetails_AddQuest1bYes = By
			.xpath("(//input[@name='occoffmangerating1B']/parent::*)[1]");
	public static By obj_PersonalDetails_AddQuest1bNo = By.xpath("(//input[@name='occoffmangerating1B']/parent::*)[2]");
	public static By obj_PersonalDetails_AddQuest2Yes = By.xpath("(//input[@name='occgovrating2']/parent::*)[1]");
	public static By obj_PersonalDetails_AddQuest2No = By.xpath("(//input[@name='occgovrating2']/parent::*)[2]");
	public static By obj_PersonalDetails_AnnualSal = By.id("annualSalCCId");
	// CoverDetails (Indexation Not Handled)
	public static By obj_PersonalDetails_PremiumFrequency = By.name("premFreq");
	public static By obj_PersonalDetails_Fixed = By.xpath("(.//input[@name='coverType']/parent::*)[1]");
	public static By obj_PersonalDetails_Unitised = By.xpath("(.//input[@name='coverType']/parent::*)[2]");
	public static By obj_PersonalDetails_DeathCoverFixed = By.id("dollar");
	public static By obj_PersonalDetails_DeathCoverUnitised = By.id("nodollar");
	public static By obj_PersonalDetails_TPDCoverFixed = By.id("dollar1");
	public static By obj_PersonalDetails_TPDCoverUnitised = By.id("nodollar1");
	public static By obj_PersonalDetails_IPCoverUnitised = By.id("dollar2");
	public static By obj_PersonalDetails_Insure85OfMySalary = By.xpath(".//input[@id='ipsalarycheck']/parent::*");
	public static By obj_PersonalDetails_WaitingPeriod = By
			.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.waitingPeriod']");
	public static By obj_PersonalDetails_BenefitPeriod = By
			.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.benefitPeriod']");
	public static By obj_PersonalDetails_OwnOccupationYes = By.xpath("(.//input[@name='ownoccptn']/parent::*)[1]");
	public static By obj_PersonalDetails_OwnOccupationNo = By.xpath("(.//input[@name='ownoccptn']/parent::*)[2]");
	public static By obj_PersonalDetails_CasualORContractor = By.xpath("//*[text()='I am not a casual or contractor employee']");

	//Validations
	public static By obj_PersonalDetails_TPD_TamperingMSG = By.xpath("(.//span[@ng-if='tpdErrorFlag'])");


	//Premium Amount
	//*[text()='the member is not a casual or contractor employee ']
		public static By obj_PersonalDetails_DeathAmount = By.xpath("(//*[@id='death']//h4)");
		public static By obj_PersonalDetails_DeathCost = By.xpath("(//*[@id='death']//h4)[2]");
		public static By obj_PersonalDetails_TPDAmount = By.xpath("(//*[@id='tpd']//h4)");
		public static By obj_PersonalDetails_TPDCost = By.xpath("(//*[@id='tpd']//h4)[2]");
		public static By obj_PersonalDetails_IPAmount = By.xpath("(//*[@id='sc']//h4)");
		public static By obj_PersonalDetails_IPCost = By.xpath("(//*[@id='sc']//h4)[2]");
		public static By obj_PersonalDetails_TotalCostYearly = By.xpath("//*[text()=' Total Yearly cost : ']/following-sibling::h4");
		public static By obj_PersonalDetails_TotalCostMonthly = By.xpath("//*[text()=' Total Monthly cost : ']/following-sibling::h4");
		public static By obj_PersonalDetails_TotalCostWeekly = By.xpath("//*[text()=' Total Weekly cost : ']/following-sibling::h4");

	//Footer
	public static By obj_PersonalDetails_Save = By.xpath("//*[@ng-click='saveQuote()']");
	public static By obj_PersonalDetails_Continue = By.xpath("//button[@ng-click='goToAura()']");

	public static By obj_SaveReferenceNo = By.xpath("//*[contains(text(), 'Your application reference no')]");
	public static By obj_SaveClose = By.xpath("//button[@ng-click='closeThisDialog()']");
	public static By obj_PersonalDetails_OpenSavedApplication = By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']");
	public static By obj_PersonalDetails_CancelMsg = By.id("decCancelMsg_text");
	public static By obj_PersonalDetails_DecisionContinue = By.xpath("//button[@ng-click='goTo()']");

	// ************************************************************************************************************************
	// -----------------------------------------Confirmation Details
	// ************************************************************************************************************************

	// Personal Details
	public static By obj_Confirmation_FirstName = By.xpath("//*[text()='First name']//following::label");
	public static By obj_Confirmation_LastName = By.xpath("//*[text()='Last name']//following::label");
	public static By obj_Confirmation_Gender = By.xpath("//*[text()='Gender']//following::label");
	public static By obj_Confirmation_ContactType = By.xpath("//*[text()='Preferred contact type']//following::label");
	public static By obj_Confirmation_ContactNo = By.xpath("//*[text()='Preferred contact number ']//following::label");
	public static By obj_Confirmation_Email = By.xpath("//*[text()='Email address']//following::label");
	public static By obj_Confirmation_DOB = By.xpath("//*[text()='Date of birth']//following::label");
	public static By obj_Confirmation_Time = By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label");
	public static By obj_Confirmation_HoursWork = By.xpath("//*[text()='Do you work 14 or more hours per week?']//following::label");
	public static By obj_Confirmation_Citizen = By.xpath("//*[contains(text(),'Are you a citizen or permanent resident of Australia')]//following::label");
	public static By obj_Confirmation_Industry = By.xpath("//*[text()='What industry do you work in?']//following::label");
	public static By obj_Confirmation_Occupation = By.xpath("//*[text()='What is your current occupation?']//following::label");
	public static By obj_Confirmation_AddQuest1a = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[1]");
	public static By obj_Confirmation_AddQuest1b = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[3]");
	public static By obj_Confirmation_TertiaryQualification = By.xpath("//*[text()='Do you:  ']//following::label");
	public static By obj_Confirmation_AnnualIncome = By.xpath("//*[text()='Annual income']//following::label");

	// Cover Details
	public static By obj_Confirmation_Death_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[1]//following::p");
	public static By obj_Confirmation_Death_NewCover = By.xpath("(//*[text()='New sum insured'])[1]//following::p");
	public static By obj_Confirmation_Death_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[1]//following::p");
	public static By obj_Confirmation_Death_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[1]//following::p");
	public static By obj_Confirmation_TPD_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[2]//following::p");
	public static By obj_Confirmation_TPD_NewCover = By.xpath("(//*[text()='New sum insured'])[2]//following::p");
	public static By obj_Confirmation_TPD_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[2]//following::p");
	public static By obj_Confirmation_TPD_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[2]//following::p");
	public static By obj_Confirmation_IP_ExistingCover = By.xpath("(//*[text()='Occupational category '])[2]//following::p[5]");
	public static By obj_Confirmation_IP_NewCover = By.xpath("(//*[text()='Occupational category '])[2]//following::p[6]");
	public static By obj_Confirmation_IP_OccupationalCategory = By.xpath("(//*[text()='Occupational category '])[3]//following::p");
	public static By obj_Confirmation_IP_WaitingPeriod = By.xpath("//*[text()='Waiting period']//following::p");
	public static By obj_Confirmation_IP_BenefitPeriod = By.xpath("//*[text()='Benefit period']//following::p");
	public static By obj_Confirmation_IP_WeeklyCost = By.xpath("//*[text()=' Total Weekly cost']//following::h4");
	public static By obj_Confirmation_TotalWeeklyCost = By.xpath("//*[contains(text(), 'Total Weekly cost')]//following::h4");


	public static By obj_Confirmation_TermsConditionsCheckBox = By.xpath("//*[@id='generalConsentLabel']/span");
	public static By obj_Confirmation_Submit = By.xpath("//*[@ng-click='navigateToDecision()']");
	public static By obj_DecisionPopUP_Continue = By.xpath("//*[@ng-click='goTo()']");
	public static By obj_DecisionPopUP_No = By.xpath("//button[text()='No']");
	public static By obj_DecisionPopUP_Appstatus = By.xpath("//a[@class='noarrow']/h4");
	public static By obj_DecisionPopUP_AppMsg = By.xpath("//*[@id='collapseOne']/div/div/div");
	public static By obj_DecisionPopUP_App = By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p");
	public static By obj_Confirmation_Download = By.xpath("//strong[text()='Download ']");
	public static By obj_Confirmation_Save = By.xpath("//*[@ng-click='saveSummary()']");

}
