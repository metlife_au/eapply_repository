package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALISummaryPageObjects extends PageObjectLibrary{
	public static final By btnsummaryNext = getObjectIdentifaction("ALI.summary.next.btn.css");
	public static final By chkexclusion = getObjectIdentifaction("ALI.summary.exclusion.chkbox.css");	
	public static final By chkloading = getObjectIdentifaction("ALI.summary.loading.chkbox.css");	
}
