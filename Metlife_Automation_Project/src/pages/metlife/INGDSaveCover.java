package pages.metlife;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.CoverDetailsPageObjects;
import uimap.metlife.DeclarationAndConfirmationPageObjects;
import uimap.metlife.MetlifeINGDirPayloadPageObjects;
import uimap.metlife.WebserviceInputStringPageObjects;

public class INGDSaveCover extends MasterPage {
	WebDriverUtil driverUtil = null;

	public INGDSaveCover SaveINGDCover() {
		ToSaveINGDCover();
		return new INGDSaveCover(scriptHelper);
	}

	public INGDSaveCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ToSaveINGDCover() {
				
		try {

			String strContactMethod = dataTable.getData("MetLife_Data", "personal.ContactMethod");
			String strContactTime = dataTable.getData("MetLife_Data", "personal.ContactTime");
			String strContactDetail = dataTable.getData("MetLife_Data", "personal.ContactDetails");
			String strTCName = dataTable.getData("MetLife_Data", "TC_ID");
			if(strTCName.contains("Transfer")){
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkEvidence),"Click","Evidence");
				}
			tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkIngdGeneralConsent),"Click","By ticking this box I acknowledge and confirm");
			tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.lstPreferredContactMethod),"DropDownSelect","Preferred Contact Method",strContactMethod);
			tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.lstPreferredContactTime),"DropDownSelect","Preferred Contact Time",strContactTime);
			Thread.sleep(1000);
			tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.txtDetails),"SET","Contact Detail",strContactDetail);
			Thread.sleep(1000);
			tryAction(fluentWaitElements(MetlifeINGDirPayloadPageObjects.lnkCalquote),"Click","Save and Exit");
			Thread.sleep(1000);
			String strRefNum=waitForClickableElement(MetlifeINGDirPayloadPageObjects.txtSaveRefNumber).getText();
			System.out.println(strRefNum);
			tryAction(fluentWaitElements(MetlifeINGDirPayloadPageObjects.lnkSaveFinish),"Click","Save Finish on Popup");
			Thread.sleep(1000);
			tryAction(fluentWaitElements(MetlifeINGDirPayloadPageObjects.lnkRetrieve),"Click","Save Finish on Popup");
			Thread.sleep(1000);
			WebElement htmltable = waitForClickableElement(MetlifeINGDirPayloadPageObjects.tblmyapp);
			List<WebElement> rows=htmltable.findElements(By.tagName("tr"));
			System.out.println(rows.size());
			String strAppNum=driver.findElement(By.xpath("//*[@id='j_id10']/div[2]/div[2]/div[2]/div[2]/div[3]/div[2]/div/div/table/tbody/tr["+rows.size()+"]/td[1]")).getText();
			if(strRefNum.equalsIgnoreCase(strAppNum)){
				report.updateTestLog("Application Retreival", "Retrieval Sucessfull", Status.PASS);
				driver.findElement(By.xpath("//*[@id='j_id10']/div[2]/div[2]/div[2]/div[2]/div[3]/div[2]/div/div/table/tbody/tr["+rows.size()+"]/td[5]/a")).click();
			}
			else{
				report.updateTestLog("Application Retreival", "Retrieval UnSucessfull", Status.FAIL);
			}
			} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}
}
