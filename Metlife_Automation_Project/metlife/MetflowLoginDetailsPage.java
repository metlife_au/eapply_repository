/**
 * 
 */
package pages.metlife;

import java.util.Properties;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.MetFlowLoginPageObjects;


/**
 * @author sampath
 * 
 */
public class MetflowLoginDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public MetflowLoginDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public MetflowLoginDetailsPage loginToMetflow() {
		enterLoginDetails();
		return new MetflowLoginDetailsPage(scriptHelper);
	} 
	
	private void enterLoginDetails() {
		
/*		final Properties properties;
		
		properties = Settings.getInstance();
		
		storeSessionThreadData("dyn_currentBrowser",properties.getProperty("currentBrowser"));
		System.out.println("The Current Browser is :: "+getStringValueFromSessionThreadData("dyn_currentBrowser"));*/
		
		String strLoginName = dataTable.getData("MetLife_Data", "login.name");
		String strLoginPassword = dataTable.getData("MetLife_Data", "login.password");
//		storeSessionThreadData("dyn_CompleteName",strFirstName.trim()+strLastName.trim());
		
		try{
			switchToFrame(waitForClickableElement(MetFlowLoginPageObjects.frameMainFrame));
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.txtLogin),"SET","Login User Name is ",strLoginName);
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.txtPassword),"SET","Login User Name is ",strLoginPassword);
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.btnSubmit),"Click","Sign In ");
			switchToMainFrame();
			sleep();
			switchToFrame(waitForClickableElement(MetFlowLoginPageObjects.frameDashboard));
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.linkCaseSearch),"Click","Case Search ");
			switchToMainFrame();
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
