(function(angualr){
	'use strict';
	/* global token, moment */
	angular
		.module('CorporateApp')
		.controller('dashboardCtrl',dashboardCtrl);
	dashboardCtrl.$inject = ['$location','fetchUrlSvc','fetchTokenNumSvc','searchMemberDetailsService','resendEmailService','updateStatusService', 'corporateFund','$filter', 'APP_CONSTANTS'];
	function dashboardCtrl($location,fetchUrlSvc,fetchTokenNumSvc,searchMemberDetailsService,resendEmailService,updateStatusService,corporateFund,$filter, APP_CONSTANTS){
		var vm = this;
		vm.token = token || '';
		fetchTokenNumSvc.setTokenId(vm.token);
		vm.urlList= fetchUrlSvc.getUrlList();
		vm.submitForm = false;
		vm.count = 0;
		vm.data={};
		vm.fundcode=$location.absUrl().split('#/')[1].split('=')[1];
		vm.frmDateErrMsg=false;
		vm.toDateErrMsg=false;
		vm.ToDateRequiredErr = false;
		vm.FromDateRequiredErr = false;
		vm.modelOptions = {updateOn: 'blur'};
		vm.constants = {
			dateFormat: APP_CONSTANTS.onboardDetails.dateFormat
		};
		vm.toDatePicker = null;
		corporateFund.getDecryptContent(vm.urlList.decryptContent,	vm.data,	vm.fundcode).then(function(res) {
			var response= res.data;
			vm.data.fundCode=response.FUNDID;
			vm.isAdd= true;	
			vm.fundDetails = {
				firstName: '',
				lastName: '',
				applicationNumber: '',
				fromDate: '',
				toDate: '',
				partner: {
					fundCode: vm.data.fundCode
				},
				status:'All'
			};
			vm.pagination = {
				numPages: 0,
				perPage: 5,
				page: 0
			};
			vm.memberDetails = [];
			vm.toDatePicker = $('#toDate').datepicker({
				showOn: 'both',
				buttonImage: APP_CONSTANTS.path.images + '/cal-icon.png',
				buttonImageOnly: true,
				dateFormat: 'dd/mm/yy',
				setDate: '+0D',
				gotoCurrent: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true,
				autoSize: true,
				minDate: '01/01/2000',
				maxDate: '+0D',
				onSelect: function( selectedDate ) {
					$(this).trigger('change');
				}
			});
			var fromDatePicker = $('#fromDate').datepicker({
				showOn: 'both',
				buttonImage: APP_CONSTANTS.path.images + '/cal-icon.png',
				buttonImageOnly: true,
				dateFormat: 'dd/mm/yy',
				setDate: '+0D',
				gotoCurrent: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true,
				autoSize: true,
				minDate: '01/01/2000',
				maxDate: '+0D',
				onSelect: function( selectedDate ) {
					$(this).trigger('change');
					vm.toDatePicker.datepicker( 'option', 'minDate', selectedDate );
				}
			});
		}, function(err) {
			console.info('Something went wrong while saving...'+ JSON.stringify(err));
		});
		
		/*date validation*/
		vm.dateChanged = function(field,frmDate,toDate) {
			if(field==='fromDate'){
				if(moment(vm.fundDetails.fromDate, 'DD/MM/YYYY').isValid() && !moment(vm.fundDetails.fromDate, 'DD/MM/YYYY').isAfter()) {
					vm.dashboardForm.fromDate.$setValidity('valid', true);
					vm.toDatePicker.datepicker( 'option', 'minDate', vm.fundDetails.fromDate );
				} else {
					vm.dashboardForm.fromDate.$setValidity('valid', false);
				}
			} else {
				if(moment(vm.fundDetails.toDate, 'DD/MM/YYYY').isValid() && !moment(vm.fundDetails.toDate, 'DD/MM/YYYY').isAfter() && moment(vm.fundDetails.toDate,  'DD/MM/YYYY').isSameOrAfter(moment(vm.fundDetails.fromDate, 'DD/MM/YYYY'))) {
					vm.dashboardForm.toDate.$setValidity('valid', true);
				} else {
					vm.dashboardForm.toDate.$setValidity('valid', false);
				}
			}
		};
		/*date validation*/  
	    
		vm.search = function() {
			//condition for empty From or To date 
			vm.memberDetails = [];
				vm.pagination.page = 0; // reinitializing the Pagination current page
				vm.pagination.numPages = 0;// reinitializing the Pagination total page
				
				searchMemberDetailsService.getMemberDetails(vm.urlList.memberDetailsUrlForDashboard,vm.fundDetails.partner.fundCode,vm.fundDetails.firstName,
						vm.fundDetails.lastName,vm.fundDetails.status,vm.fundDetails.applicationNumber,vm.fundDetails.fromDate,vm.fundDetails.toDate).then(function(res){
						vm.memberDetails = res.data; //search result table
						vm.submitForm = true;
						
						vm.pagination.numPages = Math.ceil(vm.memberDetails.length/vm.pagination.perPage);
				},function(err){
					vm.submitForm = true;
					console.log('Error while fectching memeber details'+JSON.stringify(err));
				});
		};
		vm.resendEmail = function(email, firstName, lastName, applicationNumber){
			resendEmailService.reSendEmail(vm.urlList.resendEmailUrlForDashboard,vm.fundDetails.partner.fundCode,email,firstName,lastName,applicationNumber).then(function(res){
				var response = res.data;
			},function(err){
				console.log('Error while resending email'+JSON.stringify(err));
			});
		};
		vm.updateStatus = function(index){
			updateStatusService.updateStatus(vm.urlList.updateUrlForDashboard,vm.fundDetails.partner.fundCode,vm.memberDetails[index].status,
				vm.memberDetails[index].firstName,vm.memberDetails[index].lastName,vm.memberDetails[index].applicationNumber).then(function(res){
				vm.status = res.data.status; //change Expired to Active/Pending in UI
				vm.memberDetails[index].status = vm.status;
			},function(err){
				console.log('Error while updating status in DB'+JSON.stringify(err));
			});
		};
		
		vm.prevPage = function() { /* Pagination function to move to next Page */
			if (vm.pagination.page > 0) {
				vm.pagination.page -= 1;
			}
		};
		
		vm.nextPage = function() { /* Pagination function to move to previous Page */
			if (vm.pagination.page < vm.pagination.numPages - 1) {
				vm.pagination.page += 1;
			}
		};
		
		vm.toPageId = function(id) { /* Pagination function to move to Particular Page */
			if (id >= 0 && id <= vm.pagination.numPages - 1) {
				vm.pagination.page = id;
			}
		};		
			  
	}
})(angular);