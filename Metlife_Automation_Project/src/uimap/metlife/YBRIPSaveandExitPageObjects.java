package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPSaveandExitPageObjects extends PageObjectLibrary{
	public static final By btnSave = getObjectIdentifaction("YBRIP.payment.saveandexit.btn.css");
	public static final By btnConfirm = getObjectIdentifaction("YBRIP.payment.confirm.btn.css");
	public static final By btnFinish= getObjectIdentifaction("YBRIP.payment.finish.btn.css");
}
