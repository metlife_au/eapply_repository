package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

public class TPDCover implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1668879264352249684L;
	
	private String tpdMinAge;
	private String tpdMaxAge;
	private String tpdMinSumIns;
	private String tpdMaxSumIns;
	private String tpdAal;
	public String getTpdMinAge() {
		return tpdMinAge;
	}
	public void setTpdMinAge(String tpdMinAge) {
		this.tpdMinAge = tpdMinAge;
	}
	public String getTpdMaxAge() {
		return tpdMaxAge;
	}
	public void setTpdMaxAge(String tpdMaxAge) {
		this.tpdMaxAge = tpdMaxAge;
	}
	public String getTpdMinSumIns() {
		return tpdMinSumIns;
	}
	public void setTpdMinSumIns(String tpdMinSumIns) {
		this.tpdMinSumIns = tpdMinSumIns;
	}
	public String getTpdMaxSumIns() {
		return tpdMaxSumIns;
	}
	public void setTpdMaxSumIns(String tpdMaxSumIns) {
		this.tpdMaxSumIns = tpdMaxSumIns;
	}
	public String getTpdAal() {
		return tpdAal;
	}
	public void setTpdAal(String tpdAal) {
		this.tpdAal = tpdAal;
	}
	@Override
	public String toString() {
		return "TPDCover [tpdMinAge=" + tpdMinAge + ", tpdMaxAge=" + tpdMaxAge + ", tpdMinSumIns=" + tpdMinSumIns
				+ ", tpdMaxSumIns=" + tpdMaxSumIns + ", tpdAal=" + tpdAal + "]";
	}

	
}
