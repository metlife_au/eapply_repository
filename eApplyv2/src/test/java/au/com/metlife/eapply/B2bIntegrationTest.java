package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigInteger;
import java.net.URL;
import java.security.SecureRandom;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.com.metlife.eapply.utility.AESencrp;
import au.com.metlife.eapply.utility.RSAUtil;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EApplyv2Application.class})
@AutoConfigureMockMvc
public class B2bIntegrationTest {
	@Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
    
    @Test
    public void genericB2bServiceTest() throws Exception {
    	//given
    	//String input = "<request><adminPartnerID>IFSP</adminPartnerID><transRefGUID>73bbfa52-797d-43bd-85db-05f596d209e7</transRefGUID><transType>2</transType><transDate>24/11/2017</transDate><transTime>10:19:09</transTime><fundID>MTAA</fundID><fundEmail>Multifund_insurance@aas.com.au</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>MTAA</partnerID><applicant><dateJoined>18/10/2017</dateJoined><workCommencementDate>18/10/2017</workCommencementDate><firstContributionDate>30/11/2017</firstContributionDate><clientRefNumber>312658452</clientRefNumber><memberType>Employer Sponsored</memberType><applicantRole>1</applicantRole><personalDetails><title>MRS</title><firstName>Tina</firstName><lastName>Smith</lastName><dateOfBirth>19/12/1975</dateOfBirth><gender>Female</gender><priority>2</priority><smoker>2</smoker><applicantSubType /></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><amount>3000.00</amount>			<units>12</units><loading /><exclusions>Y</exclusions><waitingPeriod>90DAYS</waitingPeriod><benefitPeriod>5YEARS</benefitPeriod><indexation>2</indexation></cover></existingCovers><address><addressType>2</addressType><line1>2 Park St</line1><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>AUSTRALIA</country></address><contactDetails><emailAddress>pkotian@metlife.com</emailAddress><mobilePhone>0412345678</mobilePhone></contactDetails></applicant></policy></request>";
    	String input = "<request><adminPartnerID>IFSP</adminPartnerID><transRefGUID>73bbfa52-797d-43bd-85db-05f596d209e7</transRefGUID><transType>2</transType><transDate>24/11/2017</transDate><transTime>10:19:09</transTime><fundID>MTAA</fundID><fundEmail>Multifund_insurance@aas.com.au</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>MTAA</partnerID><applicant><dateJoined>18/10/2017</dateJoined><workCommencementDate>18/10/2017</workCommencementDate><firstContributionDate>30/11/2017</firstContributionDate><clientRefNumber>312658452</clientRefNumber><memberType>Employer Sponsored</memberType><applicantRole>1</applicantRole><personalDetails><title>MRS</title><firstName>Tina</firstName><lastName>Smith</lastName><dateOfBirth>19/12/1975</dateOfBirth><gender>Female</gender><priority>2</priority><smoker>2</smoker><applicantSubType /></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><amount>237300.00</amount><units>3</units><loading /><exclusions /><indexation>2</indexation></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><amount>3000.00</amount>			<units>12</units><loading /><exclusions>Y</exclusions><waitingPeriod>90DAYS</waitingPeriod><benefitPeriod>5YEARS</benefitPeriod><indexation>2</indexation></cover></existingCovers><address><addressType>2</addressType><line1>2 Park St</line1><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>AUSTRALIA</country></address><contactDetails><emailAddress>pkotian@metlife.com</emailAddress><mobilePhone>0412345678</mobilePhone></contactDetails></applicant></policy></request>";
    	SecureRandom sRandom = new SecureRandom();
    	String aesKey=  new BigInteger(80, sRandom).toString(32);	   
		
		/*encrypt input data using AES key*/
		String encryptedDate = AESencrp.encrypt(input, aesKey);
		/*encrypt AES key*/
		String encryptedAesToken =RSAUtil.encryptData(aesKey,"MTAA");
		String authHeader = "Basic "+encryptedAesToken;
		//when        
        mockMvc.perform(
        		post("/genericb2bservice")
        		.header("Authorization", authHeader)
        		.header("fundid", "MTAA")
        		.content(encryptedDate)
        		.contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
        
        .andExpect(status().isOk())
       // .andExpect(jsonPath("$.id", isA(String.class)))
        .andDo(print());
    }

}
