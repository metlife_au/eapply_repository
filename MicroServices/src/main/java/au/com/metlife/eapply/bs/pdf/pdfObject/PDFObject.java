package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;
import java.util.List;

public class PDFObject implements Serializable{
	
	private float logoSize = 0.0f;
	private String colorStyle = null;
	private String pageHeadingBackground = null;
	private String pageHeadingTextColor = null;
	private String sectionBackground = null;
	private String sectionTextColor = null;
	private String tabSectionBackground = null;
	private String tabSectionTextColor = null;
	private String borderColor = null;
	private String fundId = null;
	private String productName = null;
	private String pdfType = null;
	private String pdfFor = null;
	private int fontSize = 7;
	private String pdfAttachDir = null;
	private String logPath = null;
	private boolean freeKidsDiscount = false;
	private boolean promoDiscount = false;
	private String applicationNumber = null;
	private String lob = null;
	private String channel = null;
	private String clientPDFLoc = null;
	private String uwPDFLoc = null;
	private String applicationDecision = null;
	private String campaignCode = null;
	private SpecialConditions specialConditions = null;
	private List<String> partnerAddrList;
	private List<DisclosureStatement> disclosureStatementList;
	private MemberDetails memberDetails = null;	
	private transient List questionListForHide;
	private List<DisclosureQuestionAns> disclosureQuestionList;
	private List<TransferPreviousQuestionAns> transferPrevQuestionList;
	private Boolean accInjuryProdInd = Boolean.FALSE;
	private boolean downSaleInd = Boolean.FALSE;
	private boolean exclusionInd = Boolean.FALSE;
	private boolean loadingInd = Boolean.FALSE;
	private String discountApplied = null;
	/*private DiscountDTO discountDTO = null;*/
	private String declReasons= null;
	private String magazineOffer = null;
	
	private String decCompany = null;
	private String decPrivacy = null;
	private String decGC = null;
	private String decDDS = null;
	private String decPrivacyNoAuth = null;
	private String flyBuysNo = null;
	private String emailAttachDir = null;
	private String policyFee = null;
	
	private String requestType = null;
	
	private boolean nonValidatedMemberFlow=false;
	
	private boolean quickQuoteRender = false;
	
	private String disclosureQuestion = null;	
	
	private String formLength=null;
	
	private String fakeOverallDecision=null;
	
	//Added for rounding message in PDF Transfer Cover
	private boolean deathRoundMsg = false;
	private boolean tpdRoundMsg = false;
	
	//Added for IP rounding message Care Transfer Cover
	private boolean ipRoundMsg = false;
	//Added for Convert and maintain identification
	private boolean convertCheckPdf = false;
	private String fulDeathAmount;
	private String fulTPDAmount;
	private String fulIPAmount;
	private String clientPdfType;
	private String dob;
	//Added for VICS display General consent based on aura
	private boolean disableGenConsent = false;
	
	//Added for INGD
	private List<DisclosureStatement> DisclosureStatementListTe;
	public List<DisclosureStatement> getDisclosureStatementListTe() {
		return DisclosureStatementListTe;
	}
	public void setDisclosureStatementListTe(List<DisclosureStatement> disclosureStatementListTe) {
		DisclosureStatementListTe = disclosureStatementListTe;
	}//END
	//Added for premium calculator & Insurance cover
	private List<DisclosureStatement> DisclosureStatementListPc;
	private String calculatorFlag;
	private List<DisclosureQuestionAns> disclosureincomeQuestionAnsList;
	private List<DisclosureQuestionAns> disclosureExpenseQuestionAnsList;
	private List<DisclosureQuestionAns> disclosureOtherFinanceQuestionAnsList;
	private List<DisclosureQuestionAns> disclosureAssumptionQuestionAnsList;
	private List<DisclosureQuestionAns> disclosureOtherExpenseQuestionAnsList;
	private List<DisclosureQuestionAns> disclosureDebtDetailsQuestionAnsList;
	private List<DisclosureQuestionAns> disclosureChildListQuestionAnsList;
	private boolean showDebtsDetails = false;
	private boolean showExpenseDetails = false;
	private String includeNursingCosts;
	private String excludeIP;
	public String getIncludeNursingCosts() {
		return includeNursingCosts;
	}
	public void setIncludeNursingCosts(String includeNursingCosts) {
		this.includeNursingCosts = includeNursingCosts;
	}
	public String getExcludeIP() {
		return excludeIP;
	}
	public void setExcludeIP(String excludeIP) {
		this.excludeIP = excludeIP;
	}
	public boolean isShowDebtsDetails() {
		return showDebtsDetails;
	}
	public void setShowDebtsDetails(boolean showDebtsDetails) {
		this.showDebtsDetails = showDebtsDetails;
	}
	public boolean isShowExpenseDetails() {
		return showExpenseDetails;
	}
	public void setShowExpenseDetails(boolean showExpenseDetails) {
		this.showExpenseDetails = showExpenseDetails;
	}
	public List<DisclosureQuestionAns> getDisclosureChildListQuestionAnsList() {
		return disclosureChildListQuestionAnsList;
	}
	public void setDisclosureChildListQuestionAnsList(List<DisclosureQuestionAns> disclosureChildListQuestionAnsList) {
		this.disclosureChildListQuestionAnsList = disclosureChildListQuestionAnsList;
	}
	public List<DisclosureQuestionAns> getDisclosureDebtDetailsQuestionAnsList() {
		return disclosureDebtDetailsQuestionAnsList;
	}
	public void setDisclosureDebtDetailsQuestionAnsList(List<DisclosureQuestionAns> disclosureDebtDetailsQuestionAnsList) {
		this.disclosureDebtDetailsQuestionAnsList = disclosureDebtDetailsQuestionAnsList;
	}
	public List<DisclosureQuestionAns> getDisclosureOtherExpenseQuestionAnsList() {
		return disclosureOtherExpenseQuestionAnsList;
	}
	public void setDisclosureOtherExpenseQuestionAnsList(
			List<DisclosureQuestionAns> disclosureOtherExpenseQuestionAnsList) {
		this.disclosureOtherExpenseQuestionAnsList = disclosureOtherExpenseQuestionAnsList;
	}
	public List<DisclosureQuestionAns> getDisclosureincomeQuestionAnsList() {
		return disclosureincomeQuestionAnsList;
	}
	public void setDisclosureincomeQuestionAnsList(List<DisclosureQuestionAns> disclosureincomeQuestionAnsList) {
		this.disclosureincomeQuestionAnsList = disclosureincomeQuestionAnsList;
	}
	public List<DisclosureQuestionAns> getDisclosureExpenseQuestionAnsList() {
		return disclosureExpenseQuestionAnsList;
	}
	public void setDisclosureExpenseQuestionAnsList(List<DisclosureQuestionAns> disclosureExpenseQuestionAnsList) {
		this.disclosureExpenseQuestionAnsList = disclosureExpenseQuestionAnsList;
	}
	public List<DisclosureQuestionAns> getDisclosureOtherFinanceQuestionAnsList() {
		return disclosureOtherFinanceQuestionAnsList;
	}
	public void setDisclosureOtherFinanceQuestionAnsList(
			List<DisclosureQuestionAns> disclosureOtherFinanceQuestionAnsList) {
		this.disclosureOtherFinanceQuestionAnsList = disclosureOtherFinanceQuestionAnsList;
	}
	public List<DisclosureQuestionAns> getDisclosureAssumptionQuestionAnsList() {
		return disclosureAssumptionQuestionAnsList;
	}
	public void setDisclosureAssumptionQuestionAnsList(List<DisclosureQuestionAns> disclosureAssumptionQuestionAnsList) {
		this.disclosureAssumptionQuestionAnsList = disclosureAssumptionQuestionAnsList;
	}
	public String getCalculatorFlag() {
		return calculatorFlag;
	}
	public void setCalculatorFlag(String calculatorFlag) {
		this.calculatorFlag = calculatorFlag;
	}
	public List<DisclosureStatement> getDisclosureStatementListPc() {
		return DisclosureStatementListPc;
	}
	public void setDisclosureStatementListPc(List<DisclosureStatement> disclosureStatementListPc) {
		DisclosureStatementListPc = disclosureStatementListPc;
	}
	public List<TransferPreviousQuestionAns> getTransferPrevQuestionList() {
		return transferPrevQuestionList;
	}
	public void setTransferPrevQuestionList(List<TransferPreviousQuestionAns> transferPrevQuestionList) {
		this.transferPrevQuestionList = transferPrevQuestionList;
	}
	private List<DisclosureStatement> DisclosureStatementListGc;
	

	public List<DisclosureStatement> getDisclosureStatementListGc() {
		return DisclosureStatementListGc;
	}
	public void setDisclosureStatementListGc(List<DisclosureStatement> disclosureStatementListGc) {
		DisclosureStatementListGc = disclosureStatementListGc;
	}
	public String getFakeOverallDecision() {
		return fakeOverallDecision;
	}
	public void setFakeOverallDecision(String fakeOverallDecision) {
		this.fakeOverallDecision = fakeOverallDecision;
	}
	public String getFormLength() {
		return formLength;
	}
	public void setFormLength(String formLength) {
		this.formLength = formLength;
	}	
	public String getDisclosureQuestion() {
		return disclosureQuestion;
	}
	public void setDisclosureQuestion(String disclosureQuestion) {
		this.disclosureQuestion = disclosureQuestion;
	}
	public String getDecDDS() {
		return decDDS;
	}
	public void setDecDDS(String decDDS) {
		this.decDDS = decDDS;
	}
	public String getDecCompany() {
		return decCompany;
	}
	public void setDecCompany(String decCompany) {
		this.decCompany = decCompany;
	}
	public String getDecGC() {
		return decGC;
	}
	public void setDecGC(String decGC) {
		this.decGC = decGC;
	}
	public String getDecPrivacy() {
		return decPrivacy;
	}
	public void setDecPrivacy(String decPrivacy) {
		this.decPrivacy = decPrivacy;
	}
	public String getMagazineOffer() {
		return magazineOffer;
	}
	public void setMagazineOffer(String magazineOffer) {
		this.magazineOffer = magazineOffer;
	}
	public String getDeclReasons() {
		return declReasons;
	}
	public void setDeclReasons(String declReasons) {
		this.declReasons = declReasons;
	}
	public boolean isExclusionInd() {
		return exclusionInd;
	}
	public void setExclusionInd(boolean exclusionInd) {
		this.exclusionInd = exclusionInd;
	}
	public boolean isDownSaleInd() {
		return downSaleInd;
	}
	public void setDownSaleInd(boolean downSaleInd) {
		this.downSaleInd = downSaleInd;
	}
	public Boolean getAccInjuryProdInd() {
		return accInjuryProdInd;
	}
	public void setAccInjuryProdInd(Boolean accInjuryProdInd) {
		this.accInjuryProdInd = accInjuryProdInd;
	}
	public boolean isIpRoundMsg() {
		return ipRoundMsg;
	}
	public void setIpRoundMsg(boolean ipRoundMsg) {
		this.ipRoundMsg = ipRoundMsg;
	}
	public List<DisclosureQuestionAns> getDisclosureQuestionList() {
		return disclosureQuestionList;
	}
	public void setDisclosureQuestionList(
			List<DisclosureQuestionAns> disclosureQuestionList) {
		this.disclosureQuestionList = disclosureQuestionList;
	}
	
	public String getColorStyle() {
		return colorStyle;
	}
	public void setColorStyle(String colorStyle) {
		this.colorStyle = colorStyle;
	}
	public int getFontSize() {
		return fontSize;
	}
	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}
	
	public MemberDetails getMemberDetails() {
		return memberDetails;
	}
	public void setMemberDetails(MemberDetails memberDetails) {
		this.memberDetails = memberDetails;
	}	
	public String getPdfType() {
		return pdfType;
	}
	public void setPdfType(String pdfType) {
		this.pdfType = pdfType;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSectionBackground() {
		return sectionBackground;
	}
	public void setSectionBackground(String sectionBackground) {
		this.sectionBackground = sectionBackground;
	}
	public String getPdfFor() {
		return pdfFor;
	}
	public void setPdfFor(String pdfFor) {
		this.pdfFor = pdfFor;
	}
	public String getPdfAttachDir() {
		return pdfAttachDir;
	}
	public void setPdfAttachDir(String pdfAttachDir) {
		this.pdfAttachDir = pdfAttachDir;
	}
	public String getLogPath() {
		return logPath;
	}
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	public float getLogoSize() {
		return logoSize;
	}
	public void setLogoSize(float logoSize) {
		this.logoSize = logoSize;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public List<String> getPartnerAddrList() {
		return partnerAddrList;
	}
	public void setPartnerAddrList(List<String> partnerAddrList) {
		this.partnerAddrList = partnerAddrList;
	}
	public List<DisclosureStatement> getDisclosureStatementList() {
		return disclosureStatementList;
	}
	public void setDisclosureStatementList(List<DisclosureStatement> disclosureStatementList) {
		this.disclosureStatementList = disclosureStatementList;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	public String getApplicationDecision() {
		return applicationDecision;
	}
	public void setApplicationDecision(String applicationDecision) {
		this.applicationDecision = applicationDecision;
	}
	public String getSectionTextColor() {
		return sectionTextColor;
	}
	public void setSectionTextColor(String sectionTextColor) {
		this.sectionTextColor = sectionTextColor;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	/**
	 * @return the specialConditions
	 */
	public SpecialConditions getSpecialConditions() {
		return specialConditions;
	}
	/**
	 * @param specialConditions the specialConditions to set
	 */
	public void setSpecialConditions(SpecialConditions specialConditions) {
		this.specialConditions = specialConditions;
	}
	public boolean isFreeKidsDiscount() {
		return freeKidsDiscount;
	}
	public void setFreeKidsDiscount(boolean freeKidsDiscount) {
		this.freeKidsDiscount = freeKidsDiscount;
	}
	public String getClientPDFLoc() {
		return clientPDFLoc;
	}
	public void setClientPDFLoc(String clientPDFLoc) {
		this.clientPDFLoc = clientPDFLoc;
	}
	public String getUwPDFLoc() {
		return uwPDFLoc;
	}
	public void setUwPDFLoc(String uwPDFLoc) {
		this.uwPDFLoc = uwPDFLoc;
	}
	public String getCampaignCode() {
		return campaignCode;
	}
	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}
	public boolean isPromoDiscount() {
		return promoDiscount;
	}
	public void setPromoDiscount(boolean promoDiscount) {
		this.promoDiscount = promoDiscount;
	}	
	public String getDiscountApplied() {
		return discountApplied;
	}
	public void setDiscountApplied(String discountApplied) {
		this.discountApplied = discountApplied;
	}
	public List getQuestionListForHide() {
		return questionListForHide;
	}
	public void setQuestionListForHide(List questionListForHide) {
		this.questionListForHide = questionListForHide;
	}
	public String getFundId() {
		return fundId;
	}
	public void setFundId(String fundId) {
		this.fundId = fundId;
	}
	public String getTabSectionBackground() {
		return tabSectionBackground;
	}
	public void setTabSectionBackground(String tabSectionBackground) {
		this.tabSectionBackground = tabSectionBackground;
	}
	public String getTabSectionTextColor() {
		return tabSectionTextColor;
	}
	public void setTabSectionTextColor(String tabSectionTextColor) {
		this.tabSectionTextColor = tabSectionTextColor;
	}
	public String getBorderColor() {
		return borderColor;
	}
	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}
	public boolean isNonValidatedMemberFlow() {
		return nonValidatedMemberFlow;
	}
	public void setNonValidatedMemberFlow(boolean nonValidatedMemberFlow) {
		this.nonValidatedMemberFlow = nonValidatedMemberFlow;
	}
	public boolean isQuickQuoteRender() {
		return quickQuoteRender;
	}
	public void setQuickQuoteRender(boolean quickQuoteRender) {
		this.quickQuoteRender = quickQuoteRender;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public boolean isLoadingInd() {
		return loadingInd;
	}
	public void setLoadingInd(boolean loadingInd) {
		this.loadingInd = loadingInd;
	}
	public String getFlyBuysNo() {
		return flyBuysNo;
	}
	public void setFlyBuysNo(String flyBuysNo) {
		this.flyBuysNo = flyBuysNo;
	}
	public String getEmailAttachDir() {
		return emailAttachDir;
	}
	public void setEmailAttachDir(String emailAttachDir) {
		this.emailAttachDir = emailAttachDir;
	}
	public String getPolicyFee() {
		return policyFee;
	}
	public void setPolicyFee(String policyFee) {
		this.policyFee = policyFee;
	}
	public String getDecPrivacyNoAuth() {
		return decPrivacyNoAuth;
	}
	public void setDecPrivacyNoAuth(String decPrivacyNoAuth) {
		this.decPrivacyNoAuth = decPrivacyNoAuth;
	}
	public String getPageHeadingBackground() {
		return pageHeadingBackground;
	}
	public void setPageHeadingBackground(String pageHeadingBackground) {
		this.pageHeadingBackground = pageHeadingBackground;
	}
	public String getPageHeadingTextColor() {
		return pageHeadingTextColor;
	}
	public void setPageHeadingTextColor(String pageHeadingTextColor) {
		this.pageHeadingTextColor = pageHeadingTextColor;
	}
	public boolean isDeathRoundMsg() {
		return deathRoundMsg;
	}
	public void setDeathRoundMsg(boolean deathRoundMsg) {
		this.deathRoundMsg = deathRoundMsg;
	}
	public boolean isTpdRoundMsg() {
		return tpdRoundMsg;
	}
	public void setTpdRoundMsg(boolean tpdRoundMsg) {
		this.tpdRoundMsg = tpdRoundMsg;
	}
	public String getFulDeathAmount() {
		return fulDeathAmount;
	}
	public void setFulDeathAmount(String fulDeathAmount) {
		this.fulDeathAmount = fulDeathAmount;
	}
	public String getFulTPDAmount() {
		return fulTPDAmount;
	}
	public void setFulTPDAmount(String fulTPDAmount) {
		this.fulTPDAmount = fulTPDAmount;
	}
	public String getFulIPAmount() {
		return fulIPAmount;
	}
	public void setFulIPAmount(String fulIPAmount) {
		this.fulIPAmount = fulIPAmount;
	}
	public boolean isConvertCheckPdf() {
		return convertCheckPdf;
	}
	public void setConvertCheckPdf(boolean convertCheckPdf) {
		this.convertCheckPdf = convertCheckPdf;
	}
	public String getClientPdfType() {
		return clientPdfType;
	}
	public void setClientPdfType(String clientPdfType) {
		this.clientPdfType = clientPdfType;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public boolean isDisableGenConsent() {
		return disableGenConsent;
	}
	public void setDisableGenConsent(boolean disableGenConsent) {
		this.disableGenConsent = disableGenConsent;
	}
	
}
