package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class DeathAddnlCoversJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6796711425995306051L;

	private String deathCoverName;
	
	private String deathCoverPremium;
	
	private String deathCoverType;
	
	private String deathFixedAmt;
	
	private String deathInputTextValue;
	
	private String deathTransferCoverType;
	
	private BigDecimal deathTransferAmt;
	
	private String deathTransferCovrAmt;
	
	private String deathTransferWeeklyCost;
	
	private String newDeathCoverType;
	
	private String newDeathLabelAmt;
	
	private String newDeathUpdatedCover;
	
	private String newDeathCoverPremium;
	
	private String deathLoadingCost;
	
	//added for transfer units
	private String deathTransferUnits;
	
	private String totalDeathUnits;

	public String getDeathLoadingCost() {
		return deathLoadingCost;
	}

	public void setDeathLoadingCost(String deathLoadingCost) {
		this.deathLoadingCost = deathLoadingCost;
	}

	public String getNewDeathCoverType() {
		return newDeathCoverType;
	}

	public void setNewDeathCoverType(String newDeathCoverType) {
		this.newDeathCoverType = newDeathCoverType;
	}

	public String getNewDeathLabelAmt() {
		return newDeathLabelAmt;
	}

	public void setNewDeathLabelAmt(String newDeathLabelAmt) {
		this.newDeathLabelAmt = newDeathLabelAmt;
	}

	public String getNewDeathUpdatedCover() {
		return newDeathUpdatedCover;
	}

	public void setNewDeathUpdatedCover(String newDeathUpdatedCover) {
		this.newDeathUpdatedCover = newDeathUpdatedCover;
	}

	public String getNewDeathCoverPremium() {
		return newDeathCoverPremium;
	}

	public void setNewDeathCoverPremium(String newDeathCoverPremium) {
		this.newDeathCoverPremium = newDeathCoverPremium;
	}

	public String getDeathTransferCoverType() {
		return deathTransferCoverType;
	}

	public void setDeathTransferCoverType(String deathTransferCoverType) {
		this.deathTransferCoverType = deathTransferCoverType;
	}

	public BigDecimal getDeathTransferAmt() {
		return deathTransferAmt;
	}

	public void setDeathTransferAmt(BigDecimal deathTransferAmt) {
		this.deathTransferAmt = deathTransferAmt;
	}

	public String getDeathTransferCovrAmt() {
		return deathTransferCovrAmt;
	}

	public void setDeathTransferCovrAmt(String deathTransferCovrAmt) {
		this.deathTransferCovrAmt = deathTransferCovrAmt;
	}

	public String getDeathTransferWeeklyCost() {
		return deathTransferWeeklyCost;
	}

	public void setDeathTransferWeeklyCost(String deathTransferWeeklyCost) {
		this.deathTransferWeeklyCost = deathTransferWeeklyCost;
	}

	public String getDeathCoverName() {
		return deathCoverName;
	}

	public void setDeathCoverName(String deathCoverName) {
		this.deathCoverName = deathCoverName;
	}

	public String getDeathCoverPremium() {
		return deathCoverPremium;
	}

	public void setDeathCoverPremium(String deathCoverPremium) {
		this.deathCoverPremium = deathCoverPremium;
	}

	public String getDeathCoverType() {
		return deathCoverType;
	}

	public void setDeathCoverType(String deathCoverType) {
		this.deathCoverType = deathCoverType;
	}

	public String getDeathFixedAmt() {
		return deathFixedAmt;
	}

	public void setDeathFixedAmt(String deathFixedAmt) {
		this.deathFixedAmt = deathFixedAmt;
	}

	public String getDeathInputTextValue() {
		return deathInputTextValue;
	}

	public void setDeathInputTextValue(String deathInputTextValue) {
		this.deathInputTextValue = deathInputTextValue;
	}

	public String getDeathTransferUnits() {
		return deathTransferUnits;
	}

	public void setDeathTransferUnits(String deathTransferUnits) {
		this.deathTransferUnits = deathTransferUnits;
	}

	public String getTotalDeathUnits() {
		return totalDeathUnits;
	}

	public void setTotalDeathUnits(String totalDeathUnits) {
		this.totalDeathUnits = totalDeathUnits;
	}

}
