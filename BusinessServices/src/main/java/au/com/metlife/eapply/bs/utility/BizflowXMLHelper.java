package au.com.metlife.eapply.bs.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.Vector;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import au.com.metlife.eapply.bs.bizflowmodel.Address;
import au.com.metlife.eapply.bs.bizflowmodel.AddressRole;
import au.com.metlife.eapply.bs.bizflowmodel.AddressRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.AttachmentType;
import au.com.metlife.eapply.bs.bizflowmodel.CitizenshipList;
import au.com.metlife.eapply.bs.bizflowmodel.Contact;
import au.com.metlife.eapply.bs.bizflowmodel.ContactProfile;
import au.com.metlife.eapply.bs.bizflowmodel.Contract;
import au.com.metlife.eapply.bs.bizflowmodel.ContractList;
import au.com.metlife.eapply.bs.bizflowmodel.Coverage;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddress;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddressRole;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddressRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.Email;
import au.com.metlife.eapply.bs.bizflowmodel.ExclusionsListType;
import au.com.metlife.eapply.bs.bizflowmodel.FileAttachment;
import au.com.metlife.eapply.bs.bizflowmodel.Health;
import au.com.metlife.eapply.bs.bizflowmodel.IdentifierList;
import au.com.metlife.eapply.bs.bizflowmodel.IdentifierValue;
import au.com.metlife.eapply.bs.bizflowmodel.InterestedPartyReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Limit;
import au.com.metlife.eapply.bs.bizflowmodel.LoadingListType;
import au.com.metlife.eapply.bs.bizflowmodel.LoadingValue;
import au.com.metlife.eapply.bs.bizflowmodel.MessageHeader;
import au.com.metlife.eapply.bs.bizflowmodel.OrganizationReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Party;
import au.com.metlife.eapply.bs.bizflowmodel.PartyInterest;
import au.com.metlife.eapply.bs.bizflowmodel.PartyRole;
import au.com.metlife.eapply.bs.bizflowmodel.PersonReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Phone;
import au.com.metlife.eapply.bs.bizflowmodel.PhoneRole;
import au.com.metlife.eapply.bs.bizflowmodel.PhoneRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyInsuredReferences;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyNewBusinessOrderProcess;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyOrder;
import au.com.metlife.eapply.bs.bizflowmodel.Sender;
import au.com.metlife.eapply.bs.bizflowmodel.SendingSystem;



import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnection;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.mq.jms.MQQueueSender;
import com.ibm.mq.jms.MQQueueSession;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverJSON;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.email.SendMail;


public class BizflowXMLHelper {
	
	private static final String PACKAGE_NAME = BizflowXMLHelper.class.getPackage().getName();
    private static final String CLASS_NAME = BizflowXMLHelper.class.getName();
	
	public static String createXMLForBizFlow(Eapplication applicationDTO, EapplyInput eapplyInput,Properties property){
		String xmlOutput = null;
		xmlOutput = marshallingObj(preparePolicyNewBusinessOrderProcessObj(applicationDTO, eapplyInput,property));
		System.out.println("BizFlowXML>>"+xmlOutput);
		return xmlOutput;
		
	}
	
	public  static PolicyNewBusinessOrderProcess unMarshallingInput(String inputXML){
		final String METHOD_NAME = "unMarshallingInput";		
		PolicyNewBusinessOrderProcess input=null;
		try {
			JAXBContext jc = JAXBContext.newInstance (new Class[] {PolicyNewBusinessOrderProcess.class});            
            Unmarshaller u = jc.createUnmarshaller ();         
             input = (PolicyNewBusinessOrderProcess) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8")));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return input;
	}
	
	public  static String marshallingObj(PolicyNewBusinessOrderProcess request){
		final String METHOD_NAME = "marshallingObj";
		String outputXML=null;
		try {
			JAXBContext context = JAXBContext.newInstance(PolicyNewBusinessOrderProcess.class);
		    javax.xml.bind.Marshaller m = context.createMarshaller();
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);	
		    //m.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "UTF-16");
		    //m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
		   // m.setProperty(javax.xml.bind.Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, Boolean.FALSE);
		   // m.setProperty("com.sun.xml.bind.xmlHeaders", "");
		   // m.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, true);
		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		    m.marshal(request, os);		    
		    byte b[]=os.toByteArray();  
		    
		    outputXML=new String(b);
		    outputXML=outputXML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
		    System.out.println("outputXML>>"+outputXML);
		    os.close();
		    //m.m
	    
		 } catch (Exception e) {
	           e.printStackTrace();
		 }
		return outputXML;
	}	
	
	public static PolicyNewBusinessOrderProcess preparePolicyNewBusinessOrderProcessObj(Eapplication applicationDTO, EapplyInput eapplyInput,Properties property){
		Party party = null;
		List<FileAttachment> fileAttachmentList = null;
		Applicant applicantDTO = null;
		List<Cover> coverList = null;
		String e_amt = null;
		String e_unit = null;
		String add_unit = null;
		String add_amt = null;
		String tot_amt = null;
		String add_loading = null;
		String add_loading_cmt = null;
		String decision = null;
		String dec_reason = null;
		String fwd_cvr = null;
		String aal_cvr = null;
		String salPercentage = null;
		String loadingAmt = null;
		String loadingType = null;		
		String ex_wait_period = null;
		String ex_ben_period = null;
		String ex_death_amt = "0";
		String ex_tpd_amt = "0";
		String ex_trauma_amt = "0";
		String ex_ip_amt = "0";
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){
    		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_death_amt = coverJSON.getAmount();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_tpd_amt = coverJSON.getAmount();
				}else if(coverJSON!=null && "3".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_trauma_amt = coverJSON.getAmount();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_wait_period = coverJSON.getWaitingPeriod();
					ex_ben_period = coverJSON.getBenefitPeriod();
					ex_ip_amt = coverJSON.getAmount();
				}
				
			}
    	}	
		
		String add_wait_period = eapplyInput.getWaitingPeriod();
		String add_ben_period = eapplyInput.getBenefitPeriod(); 
		PolicyNewBusinessOrderProcess policyNewBusinessOrderProcess = new PolicyNewBusinessOrderProcess();
		
		MessageHeader messageHeader = new MessageHeader();
		Sender sender = new Sender();
		sender.setRoleCode("FundOrganization");
		OrganizationReferences organizationReferences = new OrganizationReferences();
		organizationReferences.setOrganizationReference("ORG1");
		sender.setOrganizationReferences(organizationReferences);		
		Contact contact = new Contact();
		Email email = new Email();
		email.setEmailAddress(eapplyInput.getEmail());
		contact.setEmail(email);
		sender.setContact(contact);
		messageHeader.setSender(sender);		
		SendingSystem sendingSystem = new SendingSystem();
		sendingSystem.setVendorProductName("EAPPLY");
		messageHeader.setSendingSystem(sendingSystem);	
		messageHeader.setACORDStandardVersionCode("AML_1_0_0");
		messageHeader.setCorrelationId("00000000-0000-0000-0000-000000000000");	
		
		messageHeader.setMessageId(UUID.randomUUID().toString());//need to check UUid logic
		messageHeader.setMessageDateTime(new Timestamp(new Date().getTime()));
		policyNewBusinessOrderProcess.setMessageHeader(messageHeader);
		PolicyOrder policyOrder = new PolicyOrder();
		Contract contract = new Contract();
		contract.setTransactionFunctionCode("CreateApplication");
		
		if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
			contract.setApplicationTypeCode("TRWS");
         }else if("ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("LVET");
         }else if("TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("MCOV");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) || "CANCCVR".equalsIgnoreCase(applicationDTO.getRequesttype()))&& getCoverOptions(applicationDTO)){
        	 contract.setApplicationTypeCode("REDC");
         }else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("SPEC");
         }else if((null !=  ex_death_amt && Double.parseDouble(ex_death_amt.trim())>0) || (null !=  ex_ip_amt && Double.parseDouble(ex_ip_amt.trim())>0) || (null !=  ex_trauma_amt && Double.parseDouble(ex_trauma_amt.trim())>0)
         		||(null !=  ex_tpd_amt && Double.parseDouble(ex_tpd_amt.trim())>0)
                 ){
        	 contract.setApplicationTypeCode("ADDC");
         }else{
        	 contract.setApplicationTypeCode("NEWA"); 
         }	
		
		contract.setDescription(null) ;
		contract.setLineOfBusinessCode("GroupLife");
		contract.setPolicyNumber(null) ;
		contract.setPolicyStatus(null);
		contract.setPolicyStatusDescription(null) ;
		contract.setExternalReferenceNumber(null) ;
		contract.setCaseNumber(null);
		contract.setContractNumber(applicationDTO.getApplicationrefnumber());
		if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus("Accept");
		}else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus("Referred");
		}else if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus("Decline");
		}		
		contract.setContractStatusDescription(null);
		contract.setEffectiveDate(applicationDTO.getCreatedate());
		contract.setMembershipTypeCode(eapplyInput.getMemberType());
		contract.setCancellationTypeCode(null);
		contract.setCancellationReasonCode(null) ;
		contract.setProductVariant(null);
		//contracts
		List<ContractList> contracts = new ArrayList<ContractList>();
		ContractList contractList = new ContractList();
		//contractList.setLifeEvent(newUW.getLodge_life_event());
		contracts.add(contractList);
		contract.setContractList(contracts);
		//party
		List<Party> partyList = new ArrayList<Party>();
		party = new Party();
		party.setKey("ORG1");
		party.setPartyType("Organization");
		party.setPartyUID("CARE");
		partyList.add(party);
		party = new Party();
		party.setKey("ORG2");
		party.setPartyType("Administrator");
		party.setPartyUID("CARE");
		partyList.add(party);
		
		for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			applicantDTO = (Applicant) iter.next();
			if(applicantDTO!=null && "primary".equalsIgnoreCase(applicantDTO.getApplicanttype())){
				party = new Party();
				party.setKey("PERS1");
				party.setPartyType("Person");
				party.setPartyUID(applicantDTO.getClientrefnumber());
				party.setLastName(eapplyInput.getName());
				party.setFirstName(applicantDTO.getFirstname());
				party.setPrefixCode(applicantDTO.getTitle());
				party.setGender(eapplyInput.getGender());
				party.setBirthDate(eapplyInput.getDob());
				party.setOccupation(applicantDTO.getOccupation());
				party.setPartyFundJoinDate(applicantDTO.getDatejoinedfund());
				//party.setIsRollover(isRollover);
				
						
				PartyRole partyRole = new PartyRole();
				ContactProfile contactProfile = new ContactProfile();
				List<AddressRoleList> addressRoles = new ArrayList<AddressRoleList>();
				AddressRoleList addressRoleList = new AddressRoleList();
				AddressRole addressRole = new AddressRole() ;
				Address address = new Address();
				//defaulted to home as we don't ask address type in eapply
				address.setAddressKey("Home");
				
				address.setAddressLine1(eapplyInput.getAddress().getLine1());
				address.setAddressLine2(eapplyInput.getAddress().getLine2());
				address.setCountryName(eapplyInput.getAddress().getCountry());
				//address.setFullAddressDescription(fullAddressDescription);
				address.setPostalCode( eapplyInput.getAddress().getPostCode());
				address.setStateOrProvinceName(eapplyInput.getAddress().getState());
				address.setSuburbName( eapplyInput.getAddress().getSuburb());
				addressRole.setAddress(address);
				addressRoleList.setAddressRole(addressRole);	
				addressRoles.add(addressRoleList);
				contactProfile.setAddressRoleList(addressRoles);
				
				List<PhoneRoleList> phones = new ArrayList<PhoneRoleList>();
				PhoneRoleList phoneRoleList = new PhoneRoleList();
				
				PhoneRole phoneRole = new PhoneRole();
				Phone phone = new Phone();	
			   if (null != eapplyInput.getContactDetails().getWorkPhone() && eapplyInput.getContactDetails().getWorkPhone().trim().length()>0){
				   phone.setPhoneNumber(eapplyInput.getContactDetails().getWorkPhone());
				   phone.setPreferredContact("Yes");
				   phone.setPhoneType("Work");
			   }
		        if (null != eapplyInput.getContactDetails().getHomePhone() && eapplyInput.getContactDetails().getHomePhone().trim().length()>0){
		        	phone.setPhoneNumber(eapplyInput.getContactDetails().getHomePhone());
		        	phone.setPreferredContact("Yes");
		        	phone.setPhoneType("Home");
		        }
		        
		        if (null != eapplyInput.getContactDetails().getMobilePhone() && eapplyInput.getContactDetails().getMobilePhone().trim().length()>0){
		        	phone.setPhoneNumber(eapplyInput.getContactDetails().getMobilePhone());
		        	phone.setPreferredContact("Yes");
		        	phone.setPhoneType("Mobile");
		        }
		        
		        if(phone.getPhoneNumber()==null && null!=eapplyInput.getContactPhone()
		        		&& eapplyInput.getContactPhone().trim().length()>0){
		        	phone.setPhoneNumber(eapplyInput.getContactPhone());
		        }		       
		     
		        
		    	if(eapplyInput.getContactPrefTime()!=null){
		    		
		    		System.out.println("applicantDTO.getContactinfo().getPreferedContactTime()>>"+eapplyInput.getContactPrefTime());
		    		String temp =eapplyInput.getContactPrefTime();
		    				    		
		    		if(temp.contains("(")){
		    			temp = temp.substring(temp.indexOf("(")+1,temp.lastIndexOf(")"));
			    		String startTime= temp.substring(0,temp.indexOf("-"));
			    		if(eapplyInput.getContactPrefTime().contains("Morning")){
			    			startTime= startTime + "am";
			    		}else{
			    			startTime= startTime + "pm";
			    		}
			    		String endTime= temp.substring(temp.indexOf("-")+1);			    		
			    		 phone.setStartTime(startTime);			    		
			    		phone.setEndTime(endTime);
		    		}else if(temp.length()>9){
		    			//temp = temp.substring(temp.indexOf("(")+1,temp.lastIndexOf(")"));
			    		String startTime= temp.substring(0,3);			    		
			    		String endTime= temp.substring(6,10);			    		
			    		 phone.setStartTime(startTime);			    		
			    		phone.setEndTime(endTime);
		    		}  		
		    		
		    		 		
				     
		    	}
		       
		        phoneRole.setPhone(phone);
		        phoneRoleList.setPhoneRole(phoneRole);
		        phones.add(phoneRoleList);
		        contactProfile.setPhoneRoleList(phones);
		        
		        List<ElectronicAddressRoleList> electronicAddresess = new ArrayList<ElectronicAddressRoleList>(); 
		        ElectronicAddressRoleList electronicAddressRoleList = new ElectronicAddressRoleList();
		        ElectronicAddressRole electronicAddressRole = new ElectronicAddressRole();		        
		        ElectronicAddress electronicAddress = new ElectronicAddress();
		        electronicAddress.setCategory("Personal");
		        electronicAddress.setEmail(eapplyInput.getEmail());
		        electronicAddressRole.setElectronicAddress(electronicAddress);		        
		        electronicAddressRoleList.setElectronicAddressRole(electronicAddressRole);
		        electronicAddresess.add(electronicAddressRoleList);
		        contactProfile.setElectronicAddressRoleList(electronicAddresess);
		        
		        partyRole.setContactProfile(contactProfile);
				if(eapplyInput.getAddress()!=null){
					List<CitizenshipList> citizens = new ArrayList<CitizenshipList>();
					CitizenshipList citizenshipList = new CitizenshipList();
					if(eapplyInput.getAddress().getCountry()!=null && eapplyInput.getAddress().getCountry().trim().length()>0){
						citizenshipList.setCode(eapplyInput.getAddress().getCountry());
					}else{
						citizenshipList.setCode("Australia");
					}					
					citizens.add(citizenshipList);
					partyRole.setCitizenshipList(citizens);
				}
				party.setPartyRole(partyRole);
				Health health = new Health();
				if(applicantDTO.getSmoker()!=null && "yes".equalsIgnoreCase(applicantDTO.getSmoker())){
					health.setSmokerIndicator("Yes");
				}else if(applicantDTO.getSmoker()!=null){
					health.setSmokerIndicator("No");
				}else{
					health.setSmokerIndicator("UNKNOWN");
				}
				/*health.setHeight(height);
				health.setWeight(weight);
				health.setBMI(bmi);*/
				party.setHealth(health);				
				//coverage
				coverList = validCoverVector(applicantDTO.getCovers()) ;
				List<Coverage> coverageList = new ArrayList<Coverage>();
				Coverage coverage = null;
				Decision decision2 = null;
				for (Iterator iterator = coverList.iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();
					if(coversDTO!=null){
						if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 = getReasons(eapplyInput, "Term");
							e_amt = eapplyInput.getExistingDeathAmt();
							 e_unit = eapplyInput.getExistingDeathUnits();
							//need to remove hardcoding
							 add_unit = "26";
							 add_amt = eapplyInput.getDeathLabelAmt()+"";
							 tot_amt = eapplyInput.getDeathLabelAmt()+"";
							 add_loading = decision2.getOriginalTotalDebitsValue();
							 add_loading_cmt = decision2.getTotalDebitsReason();
							 decision = eapplyInput.getDeathDecision();							 
							 dec_reason = eapplyInput.getDeathResons();
							// fwd_cvr = newUW.getLodge_death_fwd_cover();
							 //aal_cvr = newUW.getLodge_death_aal_cover();							 
							 loadingAmt = decision2.getOriginalTotalDebitsValue();
							 loadingType = decision2.getTotalDebitsReason();
							
							 ex_wait_period = null;
							 ex_ben_period = null;
							 add_wait_period = null;
							 add_ben_period = null; 
							 
						}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 =getReasons(eapplyInput, "TPD");
							e_amt = eapplyInput.getExistingTpdAmt();
							 e_unit = eapplyInput.getExistingTPDUnits();
							//need to remove hardcoding
							 add_unit ="26";
							 add_amt = eapplyInput.getTpdLabelAmt() +"";
							 tot_amt = eapplyInput.getTpdLabelAmt()+"";
							 add_loading = decision2.getOriginalTotalDebitsValue();
							 add_loading_cmt = decision2.getTotalDebitsReason();
							 decision = eapplyInput.getTpdDecision();
							 dec_reason = eapplyInput.getTpdResons();
							// fwd_cvr = eapplyInput.getLodge_tpd_fwd_cover();
							// aal_cvr = eapplyInput.getLodge_tpd_aal_cover();							
							 loadingAmt = decision2.getOriginalTotalDebitsValue();
							 loadingType = decision2.getTotalDebitsReason();
							 ex_wait_period = null;
							 ex_ben_period = null;
							 add_wait_period = null;
							 add_ben_period = null; 
							 
						}/*else if("TRAUMA".equalsIgnoreCase(coversDTO.getCovercode())){
							e_amt = newUW.getLodge_e_trauma_amount();
							 e_unit = newUW.getLodge_e_trauma_units();
							 add_unit = newUW.getLodge_add_trauma_units();
							 add_amt = newUW.getLodge_add_trauma_amount();
							 tot_amt = newUW.getLodge_tot_trauma_cover();
							 add_loading = newUW.getLodge_trauma_loading_percentage();
							 add_loading_cmt = newUW.getLodge_trauma_loading_comment();
							 decision = newUW.getLodge_trauma_decision();
							 dec_reason = newUW.getLodge_trauma_dec_reason();
							 loadingAmt = newUW.getLodge_trauma_loading_percentage();
							 loadingType = newUW.getLodge_trauma_loading_comment();
							 ex_wait_period = null;
							 ex_ben_period = null;
							 add_wait_period = null;
							 add_ben_period = null; 
							
						}*/else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 =getReasons(eapplyInput, "IP");
							e_amt = eapplyInput.getExistingIPAmount();
							 e_unit = eapplyInput.getExistingIPUnits();
							//need to remove hardcoding
							 add_unit = "10";
							 add_amt = eapplyInput.getIpLabelAmt()+"";
							 tot_amt = eapplyInput.getIpLabelAmt()+"";
							 add_loading = decision2.getOriginalTotalDebitsValue();
							 add_loading_cmt = decision2.getTotalDebitsReason();
							 decision = eapplyInput.getIpDecision();
							 dec_reason = eapplyInput.getIpResons();
							 /*fwd_cvr = newUW.getLodge_ip_fwd_cover();
							 aal_cvr = newUW.getLodge_ip_aal_cover();*/
							
							 //ex_wait_period = eapplyInput.getLodge_e_wait_period();
							 //ex_ben_period = eapplyInput.getLodge_e_ben_period();
							 add_wait_period = eapplyInput.getWaitingPeriod();
							 add_ben_period = eapplyInput.getBenefitPeriod(); 
							 salPercentage= coversDTO.getCovercategory();							  
							  loadingAmt = decision2.getOriginalTotalDebitsValue();
								loadingType = decision2.getTotalDebitsReason();
						}			
						coverageList.add(getCoverage(coversDTO, coversDTO.getCovercode(),
								e_amt, e_unit, add_unit, add_amt, tot_amt, add_loading, add_loading_cmt, decision, dec_reason, fwd_cvr, aal_cvr, ex_wait_period, ex_ben_period, add_wait_period, add_ben_period,loadingType,loadingAmt,eapplyInput));
					}
					
				}
				contract.setCoverage(coverageList);
				if(salPercentage!=null){
					IdentifierList identifierList = new IdentifierList();
					List<IdentifierValue> identifierValueList = new ArrayList<IdentifierValue>();
					IdentifierValue identifierValue = new IdentifierValue();
					identifierValue.setKey("SaleryPercentage");
					System.out.println("salPercentage>>"+salPercentage);
					if(null!=salPercentage && salPercentage.contains("85%")){
						identifierValue.setData("85");
					}else if(null!=salPercentage && salPercentage.contains("75%")){
						identifierValue.setData("75");
					}else if(null!=salPercentage && salPercentage.contains("90%")){
						identifierValue.setData("90");
					}				
					identifierValueList.add(identifierValue);
					identifierList.setIdentifierValue(identifierValueList);			
					party.setIdentifierList(identifierList);
				}
				
				partyList.add(party);		
				
			}
			
		}
		List<PartyInterest> partyInterestList = new ArrayList<PartyInterest>();
		
		PartyInterest partyInterest = new PartyInterest();
		partyInterest.setRoleCode("FundAdministrator");
		InterestedPartyReferences interestedPartyReferences = new InterestedPartyReferences();
		PersonReferences personReferences = new PersonReferences();
		personReferences.setPersonReference("ORG2");
		interestedPartyReferences.setPersonReferences(personReferences);
		partyInterest.setInterestedPartyReferences(interestedPartyReferences);
		partyInterestList.add(partyInterest);
		partyInterest = new PartyInterest();
		partyInterest.setRoleCode("FundOrganization");
		interestedPartyReferences = new InterestedPartyReferences();
		personReferences = new PersonReferences();
		personReferences.setPersonReference("ORG1");
		interestedPartyReferences.setPersonReferences(personReferences);
		partyInterest.setInterestedPartyReferences(interestedPartyReferences);
		partyInterestList.add(partyInterest);
		
		contract.setPartyInterest(partyInterestList);
		contract.setParty(partyList);
		String bizFlowBasePath = null;
		String eApplyFileBasePath = null;
		String fileName = null;
		Document document = null;
		AttachmentType attachmentType = null;
		FileAttachment fileAttachment = null;
		if(applicationDTO.getDocuments()!=null && applicationDTO.getDocuments().size()>0){
			fileAttachmentList = new ArrayList<FileAttachment>();
			for (Iterator iterator = applicationDTO.getDocuments().iterator(); iterator.hasNext();) {
				document = (Document) iterator.next();
				if(document!=null){
					attachmentType = new AttachmentType();
					fileAttachment = new FileAttachment();
					bizFlowBasePath=  property.getProperty("FILE_UPLOAD_PATH_BIZ");
					eApplyFileBasePath =property.getProperty("FILE_UPLOAD_PATH");
					fileName = document.getDocLoc().substring(eApplyFileBasePath.length());
					attachmentType.setAttachmentTypeCode(document.getDocType());
					attachmentType.setDocumentURI(bizFlowBasePath+fileName);
					fileAttachment.setAttachmentType(attachmentType);
					fileAttachmentList.add(fileAttachment);
				}
				
			}
		}			
		contract.setFileAttachment(fileAttachmentList);
		policyOrder.setContract(contract);
		policyNewBusinessOrderProcess.setPolicyOrder(policyOrder);
		return policyNewBusinessOrderProcess;
	}
	
	private static Coverage getCoverage(Cover coversDTO, String coverType,
			String e_amt,String e_unit,String add_unit,String add_amt,String tot_amt,String add_loading,String add_loading_cmt,String decision, String dec_reason,String fwd_cvr,String aal_cvr,
			String ex_wait_period, String ex_ben_period, String add_wait_period,String add_ben_period, String loadingType, String loadingAmt, EapplyInput eapplyInput){

		Coverage coverage = new Coverage();
		if(coverType.equalsIgnoreCase(coversDTO.getCovercode())){
			coverage.setTypeCode(coversDTO.getCovercode());
			if("RUW".equalsIgnoreCase(coversDTO.getCoverdecision())){
				coverage.setCoverageStatusCode("Referred");
			}else if("DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
				coverage.setCoverageStatusCode("Decline");
			}else if("ACC".equalsIgnoreCase(coversDTO.getCoverdecision())){
				coverage.setCoverageStatusCode("Accept");
			}							
			coverage.setItemAmount(Double.parseDouble(coversDTO.getCost()));
			PolicyInsuredReferences policyInsuredReferences = new PolicyInsuredReferences();
			policyInsuredReferences.setKey("PERS1");
			coverage.setPolicyInsuredReferences(policyInsuredReferences);			
			List<Limit> limitList = new ArrayList<Limit>();
			int itr = 0;
			while (itr<3) {
				Limit limit = new Limit();
				if(itr==0){
					limit.setLimitAppliesCode("ExistingCover");							
					limit.setLimitAmount(e_amt);
					limit.setLimitUnits(e_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains("Unitised")){
						limit.setCalculationMethodCode("Unitised");
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains("Fixed")){
						limit.setCalculationMethodCode("Fixed");
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}					
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(ex_wait_period);
					limit.setBenefitPeriod(ex_ben_period);
					limit.setPremium(null);
					limit.setPremiumFrequency(null);				
				}else if(itr==1){
					limit.setLimitAppliesCode("AdditionalCover");
					if(tot_amt!=null && e_amt!=null && new BigDecimal(tot_amt).doubleValue()>0 && new BigDecimal(tot_amt).subtract(new BigDecimal(e_amt)).doubleValue()<0){
						limit.setLimitAmount((new BigDecimal(tot_amt).subtract(new BigDecimal(e_amt))).toString());
					}else{
						limit.setLimitAmount(add_amt);
					}
					
					limit.setLimitUnits(add_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains("Unitised")){
						limit.setCalculationMethodCode("Unitised");
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains("Fixed")){
						limit.setCalculationMethodCode("Fixed");
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}	
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(add_wait_period);
					limit.setBenefitPeriod( add_ben_period);
					//limit.setPremium(coversDTO.getCost());
					limit.setPremiumFrequency(coversDTO.getFrequencycosttype());
				}else if(itr==2){
					limit.setLimitAppliesCode("TotalCover");							
					limit.setLimitAmount(tot_amt);
					limit.setLimitUnits(add_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains("Unitised")){
						limit.setCalculationMethodCode("Unitised");
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains("Fixed")){
						limit.setCalculationMethodCode("Fixed");
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}	
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(add_wait_period);
					limit.setBenefitPeriod( add_ben_period);
					limit.setPremium(Double.parseDouble(coversDTO.getCost()));
					limit.setPremiumFrequency(coversDTO.getFrequencycosttype());
				}								
				itr=itr+1;
				limitList.add(limit);
			}
			if(aal_cvr!=null && aal_cvr.trim().length()>0){
				Limit limit = new Limit();
				limit.setLimitAppliesCode("AAL");							
				limit.setLimitAmount(aal_cvr);
				limitList.add(limit);
			}
			if(fwd_cvr!=null && fwd_cvr.trim().length()>0){
				Limit limit = new Limit();
				limit.setLimitAppliesCode("FUL");							
				limit.setLimitAmount(fwd_cvr);
				limitList.add(limit);
			}
			coverage.setLimit(limitList);
			//exclusions
			coverage.setExclusionsListType(getExclusions(eapplyInput));
			List<ExclusionsListType> exclutionList = new ArrayList<ExclusionsListType>();
			ExclusionsListType exclusionsListType = null;
			/*if(newUW.getExclusions()!=null && newUW.getExclusions().length>0){			
				for (int excItr = 0; excItr < newUW.getExclusions().length; excItr++) {
					System.out.println("coverType>>"+coverType);
					System.out.println("newUW.getExclusions()[excItr].getExclusion_type()>>"+newUW.getExclusions()[excItr].getExclusion_type());
					if(coverType.equalsIgnoreCase(newUW.getExclusions()[excItr].getExclusion_type())){
						exclusionsListType = new ExclusionsListType();
						exclusionsListType.setExclusionNameCode(newUW.getExclusions()[excItr].getExclusion_code());
						exclusionsListType.setExclusionTerm(newUW.getExclusions()[excItr].getExclusion_wording());							
						exclutionList.add(exclusionsListType);
					}				
				}
				coverage.setExclusionsListType(getExclusions(eapplyInput));
			}else{
				exclusionsListType = new ExclusionsListType();
				exclutionList.add(exclusionsListType);
				coverage.setExclusionsListType(exclutionList);
			}*/
			//loading
			List<LoadingListType> loadingList = new ArrayList<LoadingListType>();
			LoadingListType loadingListType = new LoadingListType();
			if(loadingAmt!=null && loadingAmt.trim().length()>0){				
				loadingListType.setLoadingType(loadingType);				
				LoadingValue loadingValue = new LoadingValue();
				loadingValue.setType("Percentage");
				loadingValue.setData(loadingAmt);			
				loadingListType.setLoadingValue(loadingValue);
				loadingList.add(loadingListType);
				coverage.setLoadingListType(loadingList);
			}else{
				loadingList.add(loadingListType);
				coverage.setLoadingListType(loadingList);
			}			
			
		}
		return coverage;
	
	}
	public static Decision getReasons(EapplyInput eapplyInput, String coverType){
		Decision decision= null;
		if (null != eapplyInput && null!=eapplyInput.getResponseObject() && null != eapplyInput.getResponseObject().getClientData()
                && null != eapplyInput.getResponseObject().getClientData().getListInsured()) {
            for (int itr = 0; itr < eapplyInput.getResponseObject().getClientData().getListInsured().size(); itr++) {
                Insured insured = (Insured) eapplyInput.getResponseObject().getClientData().getListInsured().get( itr);
                if (null != insured.getListOverallDec()) {
                    for (int index = 0; index < insured.getListOverallDec().size(); index++) {
                         decision = (Decision) insured.getListOverallDec().get( index);
                       
                        if (null != decision) {
                            if (decision.getProductName().contains( "Term") && 
                            		null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25) {
                            	decision.getOriginalTotalDebitsValue();
                            	decision.getTotalDebitsReason();
                            }else if(decision.getProductName().contains( "TPD")
                            		&& null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25){
                            	decision.getOriginalTotalDebitsValue();
                            	decision.getTotalDebitsReason();
                            }else if(decision.getProductName().contains( "TRAUMA")
                            		&& null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25){
                            	decision.getOriginalTotalDebitsValue();
                            	decision.getTotalDebitsReason();
                            }else if(decision.getProductName().contains( "IP")
                            		&& null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25){
                            	decision.getOriginalTotalDebitsValue();
                            	decision.getTotalDebitsReason();
                            }
                        }
                    }
                }
            }
		}
		return decision;
	}
	
	
	
	public static List<ExclusionsListType> getExclusions(EapplyInput eapplyInput){
		List<ExclusionsListType> exclutionList = new ArrayList<ExclusionsListType>();
		ExclusionsListType exclusionsListType = null;
		if (null != eapplyInput && null!=eapplyInput.getResponseObject() && null != eapplyInput.getResponseObject().getClientData()
                && null != eapplyInput.getResponseObject().getClientData().getListInsured()) {
            for (int itr = 0; itr < eapplyInput.getResponseObject().getClientData().getListInsured().size(); itr++) {
                Insured insured = (Insured) eapplyInput.getResponseObject().getClientData().getListInsured().get( itr);
                if (null != insured.getListOverallDec()) {
                    for (int index = 0; index < insured.getListOverallDec().size(); index++) {
                        Decision decision = (Decision) insured.getListOverallDec().get( index);
                       
                        if (null != decision) {
                            if ("Term-Group".equalsIgnoreCase(decision.getProductName())
                                    || decision.getProductName().contains( "Term")) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    exclusionsListType.setExclusionNameCode( exclusion.getName());
                                    exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                    exclutionList.add( exclusionsListType);
                                }
                            }else if (decision.getProductName().contains( "TPD")) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    exclusionsListType.setExclusionNameCode( exclusion.getName());
                                    exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                    exclutionList.add( exclusionsListType);
                                }
                            }else if (decision.getProductName().contains( "TRAUMA")) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    exclusionsListType.setExclusionNameCode( exclusion.getName());
                                    exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                    exclutionList.add( exclusionsListType);
                                }
                            }else if (decision.getProductName().contains( "IP")) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    exclusionsListType.setExclusionNameCode( exclusion.getName());
                                    exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                    exclutionList.add( exclusionsListType);
                                }
                            }
                            
                        }
                    }
                }
            }
		}
		return exclutionList;
	}
	
	
	public static Vector validCoverVector(List<Cover> coverVec) {
  	  final String METHOD_NAME = "validCoverVector";
      Vector updatedVector = null;
      Vector finalVector = null;
      
      if (null != coverVec) {
          updatedVector = new Vector();
          for (int itr = 0; itr < coverVec.size(); itr++) {
              Cover coversDTO = (Cover) coverVec.get( itr);
              if (null != coversDTO
                      && null!=coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0) {
                  
                  if (coversDTO.getCovercode().equalsIgnoreCase( "DEATH")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
                  if (coversDTO.getCovercode().equalsIgnoreCase( "TPD")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
                  if (coversDTO.getCovercode().contains("TRAUMA")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
                  if (coversDTO.getCovercode().equalsIgnoreCase( "IP")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
              }
              
          }        
      }
      return updatedVector;
  }
  
	private static Boolean getCoverOptions(Eapplication applicationDTO){
   	 final String METHOD_NAME = "getCoverOptions";
   	Boolean isReduceCancel = Boolean.FALSE;
   	StringBuffer strBuffer = new StringBuffer();
   	for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			Applicant applicantDTO = (Applicant) iter.next();
			if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){
				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();	
					if(null!=coversDTO){						
						if(null!=coversDTO.getCoveroption() && "DECREASE".equalsIgnoreCase(coversDTO.getCoveroption())){
							strBuffer.append(coversDTO.getCoveroption());
						}
						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains("Cancel")){
							strBuffer.append("Cancel");
						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains("Increase")){
							strBuffer.append("Increase");
						}
					}
						
					
				}
			}
		}    	
   	if(!strBuffer.toString().contains("Increase") && (strBuffer.toString().contains("DECREASE") || strBuffer.toString().contains("Cancel"))){			
   		isReduceCancel = Boolean.TRUE;
		}
   	return isReduceCancel;
   }
	public static void sendMetFlowRequestToMQ(String inputMessage,Properties property) throws JAXBException, UnsupportedEncodingException, JMSException{
		MQQueue queue = null;
		int mqSelector = 1;
		MQQueueConnection connection= null;
		MQQueueSession session;
		while(mqSelector<5){
			try {
				connection = getBizFlowMQConnection(mqSelector,property);
				break;
			} catch (JMSException jmException) {
				// TODO Auto-generated catch block
				jmException.printStackTrace();
				mqSelector = mqSelector+1;
				if(mqSelector==5){
					SendMail sendMail = new SendMail("metlife.service@ausmetlife.com", "akishore1@metlife.com", "BizFlow MQ connection failure", "MQ failure connection:-"+inputMessage);
					sendMail.sendMQConnectionFailure();
					throw jmException;
				}
			}
		}	  
		
		try {
			session = (MQQueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);		
		  
		  /*First read all MQ data in order to clear message from the output MQ*/
		  synchronized (BizflowXMLHelper.class) {		
				  connection.start();			 
				  ////				  
				  queue = (MQQueue) session.createQueue(property.getProperty("METFLOW_IN_QUEUE_NAME"));
			      //queue = (MQQueue) session.createQueue("queue:///test_in?targetClient=1");
			      MQQueueSender sender =  (MQQueueSender) session.createSender(queue);	 
			      ((MQQueue) queue).setTargetClient(JMSC.MQJMS_CLIENT_NONJMS_MQ );
			      sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			      JMSTextMessage message = (JMSTextMessage) session.createTextMessage(inputMessage); 	     
			      // Start the connection
			      connection.start();    
			      sender.setPriority(0);
			      sender.send(message);     
			      System.out.println("Sent message:\\n" + message);	      
			     			        
			      sender.close();	
			      session.close();
			      connection.close();	 			     
			      
		}   
		} catch (JMSException jmException) {
			// TODO Auto-generated catch block
			jmException.printStackTrace();
			throw jmException;
		}  
		
	}
	
	private static MQQueueConnection getBizFlowMQConnection(int mqSelector,Properties property) throws JMSException{
		MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
	      // Config
		
	      cf.setHostName(property.getProperty("MQ_HOST_NAME_"+mqSelector));
	      cf.setPort(Integer.parseInt(property.getProperty("METFLOW_MQ_PORT_NAME_"+mqSelector)));
	      cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
	      cf.setQueueManager(property.getProperty("METFLOW_QUEUE_MANAGER_"+mqSelector));
	      cf.setChannel(property.getProperty("METFLOW_MQ_CHANNEL"));  
	      
	      MQQueueConnection connection = (MQQueueConnection) cf.createQueueConnection();
	      return connection;
	}

}
