package au.com.metlife.eapply.b2b.xsd.beans;

import java.util.List;


public class Policy {
	private String lineOfBusiness;
	private String partnerID;
	private String clientRefNumber;
	private String dateJoined;
	private String picsCaseID;
	private String product;
	private String campaigncode;
	private String trackingcode;
	private String promotioncode;
	private String payFrequency;
	private String policyUnique;
	private String opt_out_campaign;
	private List<Applicant> applicant;
	private String policyNumber;
	
	Applicant adminApplicant ;
	Applicant dccApplicant ;
	
	private String calcEncrpytURL;

	

	public String getCalcEncrpytURL() {
		return calcEncrpytURL;
	}

	public void setCalcEncrpytURL(String calcEncrpytURL) {
		this.calcEncrpytURL = calcEncrpytURL;
	}

	public Applicant getDccApplicant() {
		return dccApplicant;
	}

	public void setDccApplicant(Applicant dccApplicant) {
		this.dccApplicant = dccApplicant;
	}

	public Applicant getAdminApplicant() {
		return adminApplicant;
	}

	public void setAdminApplicant(Applicant adminApplicant) {
		this.adminApplicant = adminApplicant;
	}

	public List<Applicant> getApplicant() {
		return applicant;
	}

	public void setApplicant(List<Applicant> applicant) {
		this.applicant = applicant;
	}

	public String getLineOfBusiness() {
		return lineOfBusiness;
	}

	public void setLineOfBusiness(String lineOfBusiness) {
		this.lineOfBusiness = lineOfBusiness;
	}

	public String getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getClientRefNumber() {
		return clientRefNumber;
	}

	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}

	public String getDateJoined() {
		return dateJoined;
	}

	public void setDateJoined(String dateJoined) {
		this.dateJoined = dateJoined;
	}

	public String getPicsCaseID() {
		return picsCaseID;
	}

	public void setPicsCaseID(String picsCaseID) {
		this.picsCaseID = picsCaseID;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getCampaigncode() {
		return campaigncode;
	}

	public void setCampaigncode(String campaigncode) {
		this.campaigncode = campaigncode;
	}

	public String getPromotioncode() {
		return promotioncode;
	}

	public void setPromotioncode(String promotioncode) {
		this.promotioncode = promotioncode;
	}

	public String getTrackingcode() {
		return trackingcode;
	}

	public void setTrackingcode(String trackingcode) {
		this.trackingcode = trackingcode;
	}

	public String getPayFrequency() {
		return payFrequency;
	}

	public void setPayFrequency(String payFrequency) {
		this.payFrequency = payFrequency;
	}

	public String getPolicyUnique() {
		return policyUnique;
	}

	public void setPolicyUnique(String policyUnique) {
		this.policyUnique = policyUnique;
	}

	public String getOpt_out_campaign() {
		return opt_out_campaign;
	}

	public void setOpt_out_campaign(String opt_out_campaign) {
		this.opt_out_campaign = opt_out_campaign;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}



	
}
