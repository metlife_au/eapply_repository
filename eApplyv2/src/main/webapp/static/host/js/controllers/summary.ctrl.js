/*Summary Page Controller Starts*/
HostApp.controller('summary',['$scope', '$rootScope', '$location','$timeout','$routeParams','$window', 'fetchUrlSvc', 'ngDialog', 'appData', 'fetchPersoanlDetailSvc', 'calcDeathAmtSvc', 'calcTPDAmtSvc', 'calcIPAmtSvc', 'saveEapplyData', 'submitEapplySvc','PersistenceService', 'auraRespSvc', 'fetchIndustryList', 'auraInputSvc', 'submitAuraSvc', 'clientMatchSvc', '$q',
                         function($scope, $rootScope, $location, $timeout, $routeParams, $window, fetchUrlSvc, ngDialog, appData, fetchPersoanlDetailSvc, calcDeathAmtSvc, calcTPDAmtSvc, calcIPAmtSvc, saveEapplyData, submitEapplySvc, PersistenceService, auraRespSvc, fetchIndustryList, auraInputSvc, submitAuraSvc, clientMatchSvc, $q) {
  $scope.CCS = {};
  $scope.getAuraDetails = function() {
    var defer = $q.defer();
    auraInputSvc.setFund('HOST');         
    auraInputSvc.setAppnumber(parseInt($scope.CCS.appNum));
    auraInputSvc.setClientname('metaus');       
    auraInputSvc.setLastName($scope.personalDetails.lastName)
    auraInputSvc.setFirstName($scope.personalDetails.firstName)
    auraInputSvc.setDob($scope.personalDetails.dateOfBirth);
    submitAuraSvc.requestObj($scope.urlList.submitAuraUrl).then(function(response) {                           
      $scope.auraResponses = response.data;   
      if($scope.auraResponses.overallDecision!='DCL'){
        clientMatchSvc.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
          if(clientMatchResponse.data.matchFound){
            $scope.clientmatchreason = '';
            $scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
            $scope.auraResponses.overallDecision='RUW'
              $scope.information = clientMatchResponse.data.information
              angular.forEach($scope.information, function (infoObj,index) {                        
                if(index==$scope.information.length-1){
                  $scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
                  $scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))                          
                }else{
                  $scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "  
                }                                       
                
              })
            $scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
            $scope.auraResponses.specialTerm = false;
          }
          appData.setChangeCoverAuraDetails($scope.auraResponses);
          defer.resolve($scope.auraResponses);          
        });
      }
    }, function(err) {
      defer.reject(err);
    });
    return defer.promise;
  }

  angular.extend($scope.CCS, appData.getAppData());
  $window.scrollTo(0, 0);
  $scope.urlList = fetchUrlSvc.getUrlList();
  
  $scope.olddeathCoverPremium =$scope.CCS.addnlDeathCoverDetails.deathCoverPremium;
  $scope.oldtpdCoverPremium = $scope.CCS.addnlTpdCoverDetails.tpdCoverPremium;
  $scope.oldipCoverPremium = $scope.CCS.addnlIpCoverDetails.ipCoverPremium;
  
  $scope.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

  $scope.personalDetails = $scope.inputDetails.personalDetails || {};
  $scope.contactDetails = $scope.inputDetails.contactDetails || {};
  $scope.CCS.auraDisabled = ($scope.CCS.auraDisabled == 'false' || $scope.CCS.auraDisabled == false) ? false : true;
  
  if($scope.CCS.contactType == "1") {
	$scope.contactType = 'Mobile';
  }else if($scope.CCS.contactType == "2") {
	$scope.contactType = 'Home';
  }else if($scope.CCS.contactType == "3") {
	$scope.contactType = 'Work';
  }else{
	$scope.contactType = '';
  }

  $scope.init = function() {
    $scope.CCS.auraDetails = $scope.auraDetails;
    $scope.CCS.dob = moment($scope.CCS.dob, "DD/MM/YYYY").format('DD/MM/YYYY');
    
    if($scope.CCS.contactType == "1") {
			$scope.contactType = 'Mobile';
		} else if($scope.CCS.contactType == "2") {
			$scope.contactType = 'Home';
		} else if($scope.CCS.contactType == "3") {
			$scope.contactType = 'Work';
		} else {
      $scope.contactType = '';
    }

    // Industry name
    $scope.CCS.industryName = fetchIndustryList.getIndustryName($scope.CCS.occupationDetails.industryCode);
    // Death loading calculation
  	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.deathLoading && $scope.auraDetails.deathLoading > 0){
      	var deathReqObject = {
          		"age": $scope.CCS.age,
          		"fundCode": "HOST",
          		"gender": $scope.CCS.occupationDetails.gender,
          		"deathOccCategory": $scope.CCS.deathOccCategory,
          		"smoker": false,
          		"deathUnitsCost": null,
          		"premiumFrequency": $scope.CCS.freqCostType,
          		"manageType": "CCOVER",
          		"deathCoverType": $scope.CCS.addnlDeathCoverDetails.deathCoverType
          	};
      	if($scope.CCS.addnlDeathCoverDetails.deathCoverType == 'DcFixed'){
      		deathReqObject['deathFixedAmount'] = parseInt($scope.CCS.addnlDeathCoverDetails.deathInputTextValue)- parseInt($scope.CCS.existingDeathAmt);
      	} else if($scope.CCS.addnlDeathCoverDetails.deathCoverType == 'DcUnitised'){
      		deathReqObject['deathUnits'] = parseInt($scope.CCS.addnlDeathCoverDetails.deathInputTextValue)- parseInt($scope.CCS.existingDeathUnits);
      	}

      	calcDeathAmtSvc.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
      	//CalculateDeathService.calculateAmount({}, deathReqObject, function(res){
      		var deathResponse = res.data[0];
      		$scope.CCS.deathLoadingCost = deathResponse.cost;
      		$scope.CCS.addnlDeathCoverDetails.deathCoverPremium = parseFloat($scope.CCS.addnlDeathCoverDetails.deathCoverPremium) +
      														((parseFloat($scope.auraDetails.deathLoading)/100) * parseFloat($scope.CCS.deathLoadingCost));
      		$scope.CCS.totalPremium = parseFloat($scope.CCS.addnlDeathCoverDetails.deathCoverPremium) + parseFloat($scope.CCS.addnlTpdCoverDetails.tpdCoverPremium) + parseFloat($scope.CCS.addnlIpCoverDetails.ipCoverPremium);
      		$scope.CCS.deathLoadingCost = (parseFloat($scope.auraDetails.deathLoading)/100) * parseFloat($scope.CCS.deathLoadingCost);
      		$scope.CCS.addnlDeathCoverDetails['deathLoadingCost']=Math.round(($scope.CCS.deathLoadingCost + 0.00001)*100)/100;
      		
      	}, function(err){
      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
      	});
      }

  	// TPD loading calculation
  	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.tpdLoading && $scope.auraDetails.tpdLoading > 0){
      	var tpdReqObject = {
          		"age": $scope.CCS.age,
          		"fundCode": "HOST",
          		"gender": $scope.CCS.occupationDetails.gender,
          		"tpdOccCategory": $scope.CCS.tpdOccCategory,
          		"smoker": false,
          		"tpdUnitsCost": null,
          		"premiumFrequency": $scope.CCS.freqCostType,
          		"manageType": "CCOVER",
          		"tpdCoverType": $scope.CCS.addnlTpdCoverDetails.tpdCoverType
          	};
      	if($scope.CCS.addnlTpdCoverDetails.tpdCoverType == 'TPDFixed'){
      		tpdReqObject['tpdFixedAmount'] = parseInt($scope.CCS.addnlTpdCoverDetails.tpdInputTextValue)- parseInt($scope.CCS.existingTpdAmt);
      	} else if($scope.CCS.addnlTpdCoverDetails.tpdCoverType == 'TPDUnitised'){
      		tpdReqObject['tpdUnits'] = parseInt($scope.CCS.addnlTpdCoverDetails.tpdInputTextValue)- parseInt($scope.CCS.existingTPDUnits);
      	}

      	calcTPDAmtSvc.calculateAmount($scope.urlList.calculateTpdUrl,tpdReqObject).then(function(res){
      	//CalculateTPDService.calculateAmount({}, tpdReqObject, function(res){
      		var tpdResponse = res.data[0];
      		$scope.CCS.tpdLoadingCost = tpdResponse.cost;
      		$scope.CCS.addnlTpdCoverDetails.tpdCoverPremium = parseFloat($scope.CCS.addnlTpdCoverDetails.tpdCoverPremium) +
      														((parseFloat($scope.auraDetails.tpdLoading)/100) * parseFloat($scope.CCS.tpdLoadingCost));
      		$scope.CCS.totalPremium = parseFloat($scope.CCS.addnlDeathCoverDetails.deathCoverPremium) + parseFloat($scope.CCS.addnlTpdCoverDetails.tpdCoverPremium) + parseFloat($scope.CCS.addnlIpCoverDetails.ipCoverPremium);
      		$scope.CCS.tpdLoadingCost = (parseFloat($scope.auraDetails.tpdLoading)/100) * parseFloat($scope.CCS.tpdLoadingCost);
      		$scope.CCS.addnlTpdCoverDetails['tpdLoadingCost']=Math.round(($scope.CCS.tpdLoadingCost + 0.00001)*100)/100;
      	}, function(err){
      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
      	});
      }
  	
  	// IP loading calculation
  	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.ipLoading && $scope.auraDetails.ipLoading > 0){
      	var ipReqObject = {
      		"age": $scope.CCS.age,
      		"fundCode": "HOST",
      		"gender": $scope.CCS.occupationDetails.gender,
      		"ipOccCategory": $scope.CCS.ipOccCategory,
      		"smoker": false,
      		"ipUnitsCost": null,
      		"premiumFrequency": $scope.CCS.freqCostType,
      		"manageType": "CCOVER",
      		"ipCoverType": "IpFixed",
      		"ipWaitingPeriod": $scope.CCS.addnlIpCoverDetails.waitingPeriod,
      		"ipBenefitPeriod": $scope.CCS.addnlIpCoverDetails.benefitPeriod,
      		/*"ipUnits":parseInt($scope.addnlIpCoverDetails.ipInputTextValue)- parseInt($scope.changeCoverDetails.existingIPUnits),*/
      		"ipFixedAmount":parseInt($scope.CCS.addnlIpCoverDetails.ipInputTextValue)- parseInt($scope.CCS.existingIPAmount)
      	};

      	calcIPAmtSvc.calculateAmount($scope.urlList.calculateIpUrl,ipReqObject).then(function(res){
      	//CalculateIPService.calculateAmount({}, ipReqObject, function(res){
      		var ipResponse = res.data[0];
      		$scope.CCS.ipLoadingCost = ipResponse.cost;
      		$scope.CCS.addnlIpCoverDetails.ipCoverPremium = parseFloat($scope.CCS.addnlIpCoverDetails.ipCoverPremium) +
      														((parseFloat($scope.auraDetails.ipLoading)/100) * parseFloat($scope.CCS.ipLoadingCost));
      		$scope.CCS.totalPremium = parseFloat($scope.CCS.addnlDeathCoverDetails.deathCoverPremium) + parseFloat($scope.CCS.addnlTpdCoverDetails.tpdCoverPremium) + parseFloat($scope.CCS.addnlIpCoverDetails.ipCoverPremium);
      		$scope.CCS.ipLoadingCost = (parseFloat($scope.auraDetails.ipLoading)/100) * parseFloat($scope.CCS.ipLoadingCost);
      		$scope.CCS.addnlIpCoverDetails['ipLoadingCost']=Math.round(($scope.CCS.ipLoadingCost + 0.00001)*100)/100;
      	}, function(err){
      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
      	});
      }
 
  // Exclusion, remove </br> tag from exclusions
  	
      if($scope.CCS.auraDetails.deathExclusions != null)
  		{
  		var textArray = $scope.CCS.auraDetails.deathExclusions.split('<br><br>');
  		$scope.CCS.auraDetails.deathExclusionsEdit = textArray[0]+"<p>"+textArray[1]+"</p>";
        }
  	 if($scope.CCS.auraDetails.tpdExclusions != null)
  	   {
  		var textArray = $scope.CCS.auraDetails.tpdExclusions.split('<br><br>');
  		$scope.CCS.auraDetails.tpdExclusionsEdit = textArray[0]+"<p>"+textArray[1]+"</p>";
  	   }
  	 if($scope.CCS.auraDetails.ipExclusions != null)
  	  {
  		var textArray = $scope.CCS.auraDetails.ipExclusions.split('<br><br>');
  		$scope.CCS.auraDetails.ipExclusionsEdit= textArray[0]+"<p>"+textArray[1]+"</p>";
  	  }
  	
  
  }

  //$scope.init();
  // .then(function() {
  //  console.log('all done');
  //  angular.extend($scope.CCS, appData.getChngCoverSummaryData());
  // }, function() {
  //
  // });

  if(Object.keys(appData.getChangeCoverAuraDetails()).length && !$scope.CCS.auraDisabled) {
    $scope.auraDetails = appData.getChangeCoverAuraDetails();
    $scope.init();
  } else if(!$scope.CCS.auraDisabled) {
    $scope.getAuraDetails().then(function(result) {
      $scope.auraDetails = result;
      $scope.init();
    });
  }
  else
	  {
	  // Industry name
	    $scope.CCS.industryName = fetchIndustryList.getIndustryName($scope.CCS.occupationDetails.industryCode);
	  }
  
  $scope.go = function (path) {
	  $scope.resetPremiumValue();
    $location.path(path);
  };

  $scope.navigateToLandingPage = function () {
    ngDialog.openConfirm({
        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
      }).then(function() {
        $location.path("/landing");
      }, function(e){
        if(e=='oncancel') {
          return false;
        }
      });
   };

   $scope.summarySaveAndExitPopUp = function (hhText) {
      var dialog1 = ngDialog.open({
           template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
         className: 'ngdialog-theme-plain custom-width',
         preCloseCallback: function(value) {
                var url = "/landing"
                $location.path( url );
                return true
         },
         plain: true
      });
      dialog1.closePromise.then(function (data) {
        console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
      });
   };

   $scope.saveSummary = function() {
	   $scope.resetPremiumValue();
     $scope.CCS.lastSavedOn = 'SummaryPage';
     var saveSummaryObject = angular.copy($scope.CCS);
     saveSummaryObject = angular.extend(saveSummaryObject, $scope.inputDetails);
     if($scope.auraDetails != null){
       saveSummaryObject = angular.extend(saveSummaryObject, $scope.auraDetails);
     }
     saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, saveSummaryObject).then(function(response) {
       console.log(response.data);
       $scope.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ $scope.CCS.appNum +'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
     });
   };
   var ackCheckCCGC;
   var ackCheckLE;
   $scope.navigateToDecision = function() {
     ackCheckCCGC = $('#generalConsentLabel').hasClass('active');

     if($scope.auraDetails != null){
      if($scope.auraDetails.specialTerm != null && $scope.auraDetails.specialTerm == true){
       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');
        }
     }

     if(ackCheckCCGC && (($scope.auraDetails != null && !$scope.auraDetails.specialTerm) || ($scope.auraDetails != null && $scope.auraDetails.specialTerm && ackCheckLE) || $scope.auraDetails==null)){
       $scope.CCS.LEFlag = false;
       $scope.CCGCackFlag = false;
       $rootScope.$broadcast('disablepointer');
       $scope.CCS.lastSavedOn = '';
       if($scope.auraDetails == null){
    	   $scope.CCS.deathExclusions = null;
    	   $scope.CCS.ipExclusions = null;
    	   $scope.CCS.tpdExclusions = null;
    	   $scope.CCS.addnlDeathCoverDetails.deathLoadingCost = 0.0;
    	   $scope.CCS.addnlIpCoverDetails.ipLoadingCost = 0.0;
    	   $scope.CCS.addnlTpdCoverDetails.tpdLoadingCost = 0.0;
       }
       var submitSummaryObject = angular.extend($scope.CCS, $scope.inputDetails);
       if($scope.auraDetails != null){
         submitSummaryObject = angular.extend(submitSummaryObject, $scope.auraDetails);
       }
       auraRespSvc.setResponse(submitSummaryObject);
       submitEapplySvc.submitObj($scope.urlList.submitEapplyUrl, submitSummaryObject).then(function(response) {
         if(response.data) {
             PersistenceService.setPDFLocation(response.data.clientPDFLocation);
             PersistenceService.setNpsUrl(response.data.npsTokenURL);
             if($scope.auraDetails!=null){
               if($scope.auraDetails.overallDecision == 'ACC'){
                     if($scope.auraDetails.specialTerm){
                       $location.path('/changeaspcltermsacc');
                     }else{
                       $location.path('/changeaccept');
                     }
                   } else if($scope.auraDetails.overallDecision == 'DCL'){
                     $location.path('/changedecline');
                   } else if($scope.auraDetails.overallDecision == 'RUW'){
                     $location.path('/changeunderwriting');
                   }else {
                     $location.path('/changemixedaccept');
                   }
             }else{
               $location.path('/changeaccept');
             }
         } else {
                 $window.scrollTo(0, 0);
                 $rootScope.$broadcast('enablepointer');
                 throw {message: 'No data found'};
             }
       }, function(err){
         $scope.errorOccured = true;
         $window.scrollTo(0, 0);
             $rootScope.$broadcast('enablepointer');
       });
     } else{
         if(ackCheckCCGC){
           $scope.CCGCackFlag = false;
         }else{
           $scope.CCGCackFlag = true;
         }
         if(ackCheckLE){
           $scope.CCS.LEFlag = false;
         }else{
           $scope.CCS.LEFlag = true;
         }
         $scope.scrollToUncheckedElement();
     }
   };

   $scope.scrollToUncheckedElement = function(){
     if($scope.auraDetails!=null && $scope.auraDetails.specialTerm){
       var elements = [ackCheckLE, ackCheckCCGC];
       var ids = ['lodadingExclusionLabel', 'generalConsentLabel'];
     } else{
       var elements = [ackCheckCCGC];
       var ids = ['generalConsentLabel'];
     }
   };

   $scope.checkAckStateGC = function(){
     $timeout(function(){
       ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
         if(ackCheckCCGC){
           $scope.CCGCackFlag = false;
         }else{
           $scope.CCGCackFlag = true;
         }
     }, 10);
   };

   $scope.checkAckStateLE = function(){
     $timeout(function(){
       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');

         if(ackCheckLE){
           $scope.CCS.LEFlag = false;
         }else{
           $scope.CCS.LEFlag = true;
         }
     }, 10);
   };
   
   $scope.resetPremiumValue = function(){
	   
	  $scope.CCS.addnlDeathCoverDetails.deathCoverPremium = $scope.olddeathCoverPremium;
	  $scope.CCS.addnlTpdCoverDetails.tpdCoverPremium =$scope.oldtpdCoverPremium;
	  $scope.CCS.addnlIpCoverDetails.ipCoverPremium = $scope.oldipCoverPremium;
	   
	  };
   
}]);
   /*Summary Page Controller Ends*/
