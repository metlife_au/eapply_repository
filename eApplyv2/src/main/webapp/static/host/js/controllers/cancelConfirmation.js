HostApp.controller('cancelConfirmController',['$scope','$rootScope', '$location','$timeout','$window','$routeParams','auraResponseService','PersistenceService','submitEapply','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','ngDialog','urlService', 'CalculateService',
                                           function($scope,$rootScope, $location,$timeout,$window,$routeParams,auraResponseService,PersistenceService,submitEapply,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,ngDialog,urlService, CalculateService){
    $rootScope.$broadcast('enablepointer');
    $scope.urlList = urlService.getUrlList();
    $scope.otherReasonObj = {'otherReason': ''};
    $scope.otherReasonError = false;
    $scope.cancelReasonoptions = {
    		"option1": "No longer require insurance",
            "option2": "Affordability constraints",
            "option3": "I have sufficient insurance in place",
            "option4": "I don\u2019t wish to provide details",
            "option5": "Other"
    }
    $scope.go = function ( path ) {
        $timeout(function(){
            $location.path( path );
        }, 10);
    };
    $scope.navigateToLandingPage = function (){
        ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
            $location.path("/landing");
        }, function(e){
            if(e=='oncancel'){
                return false;
            }
        });
    }

    
    $scope.seletedCovers = PersistenceService.getCancelCoverDetails();
    $scope.cancelCovers = ($scope.seletedCovers.coverCancellation.death ? "Death" : "")+($scope.seletedCovers.coverCancellation.death && $scope.seletedCovers.coverCancellation.tpd && $scope.seletedCovers.coverCancellation.salContinuance ? " , " : $scope.seletedCovers.coverCancellation.death && $scope.seletedCovers.coverCancellation.tpd && !$scope.seletedCovers.coverCancellation.salContinuance ? " and " : "") + ($scope.seletedCovers.coverCancellation.tpd ? "TPD" : $scope.seletedCovers.coverCancellation.death && !$scope.seletedCovers.coverCancellation.tpd && $scope.seletedCovers.coverCancellation.salContinuance ? " and " : "") + ($scope.seletedCovers.coverCancellation.tpd && $scope.seletedCovers.coverCancellation.salContinuance ? " and " : "") + ($scope.seletedCovers.coverCancellation.salContinuance ? "Salary Continuance" : "");
    
    
    $scope.ackErrorFlag = false;
    $scope.cancellationReasonFlag = false;
    $scope.checkAckState = function() {
        $timeout(function(){
            $scope.ackErrorFlag = !$('#ackCancellationLabelId').hasClass('active');
            $scope.cancelAcknowledgement = !$scope.ackErrorFlag;
        });
    };

    $scope.updateCancellationReason = function(value) {
        $timeout(function(){
            $scope.cancellationReason = value;
            $scope.cancellationReasonFlag = false;
            if($scope.cancellationReason != 'option5'){
            	$scope.otherReasonObj.otherReason ='';
            	$scope.otherReasonError = false;
            }

        });
    }

    $scope.updateOtherReason = function() {
        $scope.otherReasonError = !$scope.otherReasonObj.otherReason;
    }
    $scope.cancelConfirmationPopUp = function () {
        $scope.reasonErrorExist = false;
        if($scope.cancellationReason == 'option5' && !$scope.otherReasonObj.otherReason) {
            $scope.reasonErrorExist = true;
        } else if($scope.cancellationReasonFlag && !$scope.cancellationReason){
            $scope.reasonErrorExist = true;
        } else if(!$scope.cancellationReasonFlag && !$scope.cancellationReason){
            $scope.reasonErrorExist = true;
        }
        if(!$scope.reasonErrorExist && (!$scope.ackErrorFlag && $scope.cancelAcknowledgement)){
            $scope.cancelContactDetails = PersistenceService.getCancelCoverDetails();
            var coverCancellation  = $scope.cancelContactDetails.coverCancellation;

            ngDialog.openConfirm({
                template: '<div class="ngdialog-content"><div class="modal-body"><div class="row  rowcustom"><div class="col-sm-12"><p class="aligncenter">You are requesting to cancel your '+(coverCancellation.death ? "Death" : "")+(coverCancellation.death && coverCancellation.tpd && coverCancellation.salContinuance ? " , " : coverCancellation.death && coverCancellation.tpd && !coverCancellation.salContinuance ? " and " : "") + (coverCancellation.tpd ? "TPD" : coverCancellation.death && !coverCancellation.tpd && coverCancellation.salContinuance ? " and " : "") + (coverCancellation.tpd && coverCancellation.salContinuance ? " and " : "") + (coverCancellation.salContinuance ? "Salary Continuance" : "")+'  cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?                </p></div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain"  ng-click="confirm()">Yes</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary no-arrow" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
                className: 'ngdialog-theme-plain',
                plain: true
             }).then(function (value) {
                   $scope.confirmCancel();
                   return true;
             }, function (value) {
                 if(value == 'oncancel'){
                     return false;
                 }
            });
        } else {
            $scope.ackErrorFlag = !$scope.cancelAcknowledgement;
            $scope.cancellationReasonFlag = !$scope.cancellationReason;
            if($scope.cancellationReason == 'option5')
                $scope.otherReasonError = !$scope.otherReasonObj.otherReason;
        }
    };

    $scope.confirmCancel= function(){
        var deathAddnlCoverObj = {};
        var tpdAddnlCoverObj = {};
        var ipAddnlCoverObj = {};
        var coverObj = {};


        $scope.ackErrorFlag = false;
        $scope.cancellationReasonFlag = false;
        $scope.personalDetails = persoanlDetailService.getMemberDetails();
        $scope.cancelContactDetails =PersistenceService.getCancelCoverDetails();
        $scope.deathCoverDetails = deathCoverService.getDeathCover();
        $scope.tpdCoverDetails = tpdCoverService.getTpdCover();
        $scope.ipCoverDetails = ipCoverService.getIpCover();
        var anb = parseInt(moment().diff(moment($scope.personalDetails.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

        coverObj['ackCheck'] = !$scope.ackErrorFlag;
        coverObj['existingDeathUnits'] = $scope.deathCoverDetails.units;
        coverObj['existingTPDUnits'] = $scope.tpdCoverDetails.units;
        coverObj['existingIPUnits'] = $scope.ipCoverDetails.units;

        coverObj['tpdOccCategory'] = $scope.tpdCoverDetails.occRating;
        coverObj['deathOccCategory'] = $scope.deathCoverDetails.occRating;
        coverObj['ipOccCategory'] = $scope.ipCoverDetails.occRating;
        coverObj['freqCostType'] = 'Weekly';
        coverObj['cancelreason'] = $scope.cancelReasonoptions[$scope.cancellationReason];
        coverObj['cancelotherreason'] = $scope.otherReasonObj.otherReason;

        deathAddnlCoverObj['deathCoverName'] = $scope.cancelContactDetails.coverCancellation.death ? 'Cancel' : 'No change';
        deathAddnlCoverObj['deathCoverType'] = $scope.deathCoverDetails.type;
        deathAddnlCoverObj['deathFixedAmt'] = $scope.cancelContactDetails.coverCancellation.death ? 0 : parseFloat($scope.deathCoverDetails.amount);
        deathAddnlCoverObj['deathInputTextValue'] = $scope.cancelContactDetails.coverCancellation.death ? 0 : $scope.deathCoverDetails.type == '1' ? parseFloat($scope.deathCoverDetails.units) : parseFloat($scope.deathCoverDetails.amount);
        deathAddnlCoverObj['deathCoverPremium'] = 0;


        tpdAddnlCoverObj['tpdCoverName'] = $scope.cancelContactDetails.coverCancellation.tpd ? 'Cancel' : 'No change';
        tpdAddnlCoverObj['tpdCoverType'] = $scope.tpdCoverDetails.type;
        tpdAddnlCoverObj['tpdFixedAmt'] = $scope.cancelContactDetails.coverCancellation.tpd ? 0 : parseFloat($scope.tpdCoverDetails.amount);
        tpdAddnlCoverObj['tpdInputTextValue'] = $scope.cancelContactDetails.coverCancellation.tpd ? 0 : $scope.tpdCoverDetails.type == '1' ? parseFloat($scope.tpdCoverDetails.units) : parseFloat($scope.tpdCoverDetails.amount);
        tpdAddnlCoverObj['tpdCoverPremium'] = 0;


        ipAddnlCoverObj['ipCoverName'] = $scope.cancelContactDetails.coverCancellation.salContinuance ? 'Cancel' : 'No change';
        ipAddnlCoverObj['ipCoverType'] = 'IpFixed';
        ipAddnlCoverObj['waitingPeriod'] = $scope.ipCoverDetails.waitingPeriod;
        ipAddnlCoverObj['benefitPeriod'] = $scope.ipCoverDetails.benefitPeriod;
        ipAddnlCoverObj['ipFixedAmt'] = $scope.cancelContactDetails.coverCancellation.salContinuance ? 0 : parseFloat($scope.ipCoverDetails.amount);
        ipAddnlCoverObj['ipInputTextValue'] = $scope.cancelContactDetails.coverCancellation.salContinuance ? 0 : parseFloat($scope.ipCoverDetails.amount);
        ipAddnlCoverObj['ipCoverPremium'] = 0;


      /*  var ruleModel = {
                        "age": anb,
                        "fundCode": "HOST",
                        "gender": $scope.gender,
                        "deathOccCategory": coverObj['deathOccCategory'],
                        "tpdOccCategory": $scope.tpdCoverDetails.occRating,
                        "ipOccCategory": $scope.ipCoverDetails.occRating,
                        "smoker": false,
                        "deathUnits": parseInt(coverObj['existingDeathUnits'] || 0),
                        "deathFixedAmount": parseInt(deathAddnlCoverObj['deathFixedAmt'] || 0),
                        "deathFixedCost": null,
                        "deathUnitsCost": null,
                        "tpdUnits": parseInt(coverObj['existingTPDUnits'] || 0),
                        "tpdFixedAmount": parseInt(tpdAddnlCoverObj['tpdFixedAmt'] || 0),
                        "tpdFixedCost": null,
                        "tpdUnitsCost": null,
                        "ipUnits": 0,
                        "ipFixedAmount": parseInt(ipAddnlCoverObj['ipFixedAmt']  || 0),
                        "ipFixedCost": null,
                        "ipUnitsCost": null,
                        "premiumFrequency": "Weekly",
                        "memberType": null,
                        "manageType": "CANCOVER",
                        "deathCoverType": deathAddnlCoverObj['deathCoverType'],
                        "tpdCoverType": tpdAddnlCoverObj['tpdCoverType'],
                        "ipCoverType": "IpFixed",
                        "ipWaitingPeriod": ipAddnlCoverObj['waitingPeriod'],
                        "ipBenefitPeriod": ipAddnlCoverObj['benefitPeriod']
                  };*/

        var waitingPeriod = $scope.ipCoverDetails.waitingPeriod != '' && $scope.ipCoverDetails.waitingPeriod != null ? $scope.ipCoverDetails.waitingPeriod : '90 Days';
        var benefitPeriod = $scope.ipCoverDetails.benefitPeriod != '' && $scope.ipCoverDetails.benefitPeriod != null ? $scope.ipCoverDetails.benefitPeriod : '2 Years';


        var ruleModel = {
         		"age": anb,
         		"fundCode": "HOST",
         		"gender": $scope.cancelContactDetails.gender,
         		"deathOccCategory": coverObj['deathOccCategory'],
         		"tpdOccCategory": $scope.tpdCoverDetails.occRating,
         		"ipOccCategory": $scope.ipCoverDetails.occRating,
         		"smoker": false,
         		"deathFixedCost": null,
         		"deathUnitsCost": null,
         		"tpdFixedCost": null,
         		"tpdUnitsCost": null,
         		"ipUnits": null,
         		"ipFixedAmount":  ipAddnlCoverObj['ipFixedAmt'],
         		"ipUnitsCost": null,
         		"premiumFrequency": "Weekly",
         		"memberType": null,
         		"manageType": "CANCOVER",
         		"ipCoverType": "IpFixed",
         		"ipWaitingPeriod": waitingPeriod,
         		"ipBenefitPeriod": benefitPeriod
         	};
    	 if($scope.deathCoverDetails.type == '1'){
    		 ruleModel.deathCoverType = 'DcUnitised';
    		 ruleModel.deathUnits = $scope.cancelContactDetails.coverCancellation.death ? 0 : parseInt($scope.deathCoverDetails.units);
 			 deathAddnlCoverObj['deathCoverType'] = 'DcUnitised';
 		} else if($scope.deathCoverDetails.type == '2'){
 		    ruleModel.deathCoverType = 'DcFixed';
 			ruleModel.deathFixedAmount = $scope.cancelContactDetails.coverCancellation.death ? 0 : parseInt($scope.deathCoverDetails.amount);
 		    deathAddnlCoverObj['deathCoverType'] = 'DcFixed';
 		}

 		if($scope.tpdCoverDetails.type == '1'){
 			ruleModel.tpdCoverType = 'TPDUnitised';
 			ruleModel.tpdUnits = $scope.cancelContactDetails.coverCancellation.tpd ? 0 : parseInt($scope.tpdCoverDetails.units);
 			tpdAddnlCoverObj['tpdCoverType'] = 'TPDUnitised';
 		} else if($scope.tpdCoverDetails.type == '2'){
 			ruleModel.tpdCoverType = 'TPDFixed';
 			ruleModel.tpdFixedAmount = $scope.cancelContactDetails.coverCancellation.tpd ? 0 : parseInt($scope.tpdCoverDetails.amount);
 			tpdAddnlCoverObj['tpdCoverType'] = 'TPDFixed';
 		}


      CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
                  var premium = res.data;
                  dynamicFlag = true;
                    for(var i = 0; i < premium.length; i++){
                        if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
                              $scope.dcCoverAmount = premium[i].coverAmount;
                              deathAddnlCoverObj['deathCoverPremium'] = premium[i].cost;
                        } else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
                              $scope.tpdCoverAmount = premium[i].coverAmount;
                              tpdAddnlCoverObj['tpdCoverPremium'] = premium[i].cost;
                        } else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
                              $scope.ipCoverAmount = premium[i].coverAmount;
                              ipAddnlCoverObj['ipCoverPremium'] = premium[i].cost;
                        }
                    }
                    coverObj['totalPremium'] = deathAddnlCoverObj['deathCoverPremium'] + tpdAddnlCoverObj['tpdCoverPremium'] + ipAddnlCoverObj['ipCoverPremium'];
                    if($scope.personalDetails != null && $scope.cancelContactDetails != null && $scope.deathCoverDetails != null &&
                        $scope.tpdCoverDetails != null && $scope.ipCoverDetails != null){
                        $rootScope.$broadcast('disablepointer');
                        var temp1 = angular.extend($scope.personalDetails,$scope.deathCoverDetails);
                        var temp2 = angular.extend(temp1,$scope.tpdCoverDetails);
                        var temp3 = angular.extend(temp2,$scope.ipCoverDetails);
                        var submitObject = angular.extend(temp3,$scope.cancelContactDetails);
                        submitObject = angular.extend(coverObj,submitObject);
                        submitObject['addnlDeathCoverDetails'] = deathAddnlCoverObj;
                        submitObject['addnlTpdCoverDetails'] = tpdAddnlCoverObj;
                        submitObject['addnlIpCoverDetails'] = ipAddnlCoverObj;
                        auraResponseService.setResponse(submitObject);
                        submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
                            if(response.data) {
                                PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                                PersistenceService.setNpsUrl(response.data.npsTokenURL);
                                $scope.go('/cancelDecision');
                            } else {
                                $window.scrollTo(0, 0);
                                $rootScope.$broadcast('enablepointer');
                                throw {message: 'No data found'};
                            }
                        }, function(err){
                            console.log("Error while submitting the application " + JSON.stringify(err));
                            $window.scrollTo(0, 0);
                            $rootScope.$broadcast('enablepointer');
                            throw err;
                        });
                    }
            }, function(err){
                console.info("Something went wrong while calculating..." + JSON.stringify(err));
                $window.scrollTo(0, 0);
                $rootScope.$broadcast('enablepointer');
                throw err;
            });


        //coverObj['totalPremium'] = parseFloat( deathAddnlCoverObj['deathCoverPremium'] + tpdAddnlCoverObj['tpdCoverPremium'] + ipAddnlCoverObj['ipCoverPremium']);






    };
    $scope.cancel= function(){
        $scope.go('/landing');
    };

}]);
