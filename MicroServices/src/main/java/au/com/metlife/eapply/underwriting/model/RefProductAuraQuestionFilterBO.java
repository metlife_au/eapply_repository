package au.com.metlife.eapply.underwriting.model;

import java.util.ArrayList;
import java.util.List;

import org.drools.KnowledgeBase;

public class RefProductAuraQuestionFilterBO {

	private String partnerCode;
	private String productCategory;
	private String productCode;
	private String questionFilter;
	private String coverno;
	private String waitingPeriod;
	private String benefitPeriod;
	private String schemeName;
	private String partnerName;
	private boolean downsaleApp = false;
	private boolean underwritten = false;
	private int coverAmount;
	private String formLength;
	private Integer age;
	
	private String originalCoverNumber;
	private String downsaleCoverNumber;
	
	private String additionalCoverNumber;
	
	private String subSetCode;
	
	private String ruleFileName;
	
	private String occupationName;
	
	private List questionFilterList=new ArrayList();
	
	private String familyQuest;
	private String travelQuest;
	private String memberType=null;
	
	private double exCoverAmt;
	private boolean riskIncrDueToWP_Decr_or_BP_Incr=false;
	
    /*public StatelessKnowledgeSession kSession;*/
	private KnowledgeBase kbase = null;
	public KnowledgeBase getKbase() {
		return kbase;
	}
	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public String getQuestionFilter() {
		return questionFilter;
	}
	public void setQuestionFilter(String questionFilter) {
		this.questionFilter = questionFilter;
	}
	public String getRuleFileName() {
		return ruleFileName;
	}
	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}
/*	public StatelessKnowledgeSession getKSession() {
		return kSession;
	}
	public void setKSession(StatelessKnowledgeSession session) {
		kSession = session;
	}
*/	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getCoverno() {
		return coverno;
	}
	public void setCoverno(String coverno) {
		this.coverno = coverno;
	}
	public String getBenefitPeriod() {
		return benefitPeriod;
	}
	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getSchemeName() {
		return schemeName;
	}
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	public String getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	public boolean isUnderwritten() {
		return underwritten;
	}
	public void setUnderwritten(boolean underwritten) {
		this.underwritten = underwritten;
	}
	public String getOriginalCoverNumber() {
		return originalCoverNumber;
	}
	public void setOriginalCoverNumber(String originalCoverNumber) {
		this.originalCoverNumber = originalCoverNumber;
	}
	public String getDownsaleCoverNumber() {
		return downsaleCoverNumber;
	}
	public void setDownsaleCoverNumber(String downsaleCoverNumber) {
		this.downsaleCoverNumber = downsaleCoverNumber;
	}
	public String getAdditionalCoverNumber() {
		return additionalCoverNumber;
	}
	public void setAdditionalCoverNumber(String additionalCoverNumber) {
		this.additionalCoverNumber = additionalCoverNumber;
	}
	public boolean isDownsaleApp() {
		return downsaleApp;
	}
	public void setDownsaleApp(boolean downsaleApp) {
		this.downsaleApp = downsaleApp;
	}
	public String getSubSetCode() {
		return subSetCode;
	}
	public void setSubSetCode(String subSetCode) {
		this.subSetCode = subSetCode;
	}	
	public String getFormLength() {
		return formLength;
	}
	public void setFormLength(String formLength) {
		this.formLength = formLength;
	}
	public int getCoverAmount() {
		return coverAmount;
	}
	public void setCoverAmount(int coverAmount) {
		this.coverAmount = coverAmount;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public List getQuestionFilterList() {
		return questionFilterList;
	}
	public void setQuestionFilterList(List questionFilterList) {
		this.questionFilterList = questionFilterList;
	}
	public String getOccupationName() {
		return occupationName;
	}
	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}
	public String getFamilyQuest() {
		return familyQuest;
	}
	public void setFamilyQuest(String familyQuest) {
		this.familyQuest = familyQuest;
	}
	public String getTravelQuest() {
		return travelQuest;
	}
	public void setTravelQuest(String travelQuest) {
		this.travelQuest = travelQuest;
	}
	public double getExCoverAmt() {
		return exCoverAmt;
	}
	public void setExCoverAmt(double exCoverAmt) {
		this.exCoverAmt = exCoverAmt;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public boolean isRiskIncrDueToWP_Decr_or_BP_Incr() {
		return riskIncrDueToWP_Decr_or_BP_Incr;
	}
	public void setRiskIncrDueToWP_Decr_or_BP_Incr(
			boolean riskIncrDueToWP_Decr_or_BP_Incr) {
		this.riskIncrDueToWP_Decr_or_BP_Incr = riskIncrDueToWP_Decr_or_BP_Incr;
	}
	
}
