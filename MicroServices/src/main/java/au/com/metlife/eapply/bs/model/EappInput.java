package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;

public class EappInput implements Serializable {

	/**
	 * author 175373
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String partnerCode;
	private String lastSavedOn;
	private String requestType;
	private String clientRefNumber;
	private BigDecimal totalMonthlyPremium;
	private String applicatioNumber = null;
	private boolean ackCheck = false;	
	private boolean privacyCheck = false;	
	private boolean fulCheck = false;
	private boolean dodCheck = false;
	private String appDecision = null;
	private String clientMatchReason;
	private Boolean clientMatch  = false;
	private String auraDisabled;
	private String deathExclusions;
	private String tpdExclusions;
	private String ipExclusions;
	private String deathLoading;
	private String tpdLoading;
	private String ipLoading;
	
	//Added for ING
	private String auraType;
	private String coverType;
	
	//Added for calculator
	private List<DisclosureQuestionAns> initialOccupationQuestions;
	private List<DisclosureQuestionAns> finalOccupationQuestions;
	private String calculatorFlag;
	private boolean quickQuoteRender = false;
	private String disclaimerText;
		
	public String getDisclaimerText() {
		return disclaimerText;
	}
	public void setDisclaimerText(String disclaimerText) {
		this.disclaimerText = disclaimerText;
	}
	public boolean isQuickQuoteRender() {
		return quickQuoteRender;
	}
	public void setQuickQuoteRender(boolean quickQuoteRender) {
		this.quickQuoteRender = quickQuoteRender;
	}
	public String getCalculatorFlag() {
		return calculatorFlag;
	}
	public void setCalculatorFlag(String calculatorFlag) {
		this.calculatorFlag = calculatorFlag;
	}
	public List<DisclosureQuestionAns> getInitialOccupationQuestions() {
		return initialOccupationQuestions;
	}
	public void setInitialOccupationQuestions(List<DisclosureQuestionAns> initialOccupationQuestions) {
		this.initialOccupationQuestions = initialOccupationQuestions;
	}
	public List<DisclosureQuestionAns> getFinalOccupationQuestions() {
		return finalOccupationQuestions;
	}
	public void setFinalOccupationQuestions(List<DisclosureQuestionAns> finalOccupationQuestions) {
		this.finalOccupationQuestions = finalOccupationQuestions;
	}
	//Calculator End
	private ApplicantInfo applicant;
	private InsuranceRuleInfo insuranceRuleDetails;
	
	
	
	public InsuranceRuleInfo getInsuranceRuleDetails() {
		return insuranceRuleDetails;
	}
	public void setInsuranceRuleDetails(InsuranceRuleInfo insuranceRuleDetails) {
		this.insuranceRuleDetails = insuranceRuleDetails;
	}
	private List<DocumentInfo> transferDocuments;
	private List<DocumentInfo> lifeEventDocuments;
	private ResponseObject responseObject = null;

	private CoverCancel coverCancellation;
	
	public ResponseObject getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(ResponseObject responseObject) {
		this.responseObject = responseObject;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getLastSavedOn() {
		return lastSavedOn;
	}
	public void setLastSavedOn(String lastSavedOn) {
		this.lastSavedOn = lastSavedOn;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getClientRefNumber() {
		return clientRefNumber;
	}
	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}
	public BigDecimal getTotalMonthlyPremium() {
		return totalMonthlyPremium;
	}
	public void setTotalMonthlyPremium(BigDecimal totalMonthlyPremium) {
		this.totalMonthlyPremium = totalMonthlyPremium;
	}
	public String getApplicatioNumber() {
		return applicatioNumber;
	}
	public void setApplicatioNumber(String applicatioNumber) {
		this.applicatioNumber = applicatioNumber;
	}
	public boolean isAckCheck() {
		return ackCheck;
	}
	public void setAckCheck(boolean ackCheck) {
		this.ackCheck = ackCheck;
	}
	public boolean isPrivacyCheck() {
		return privacyCheck;
	}
	public void setPrivacyCheck(boolean privacyCheck) {
		this.privacyCheck = privacyCheck;
	}
	public boolean isFulCheck() {
		return fulCheck;
	}
	public void setFulCheck(boolean fulCheck) {
		this.fulCheck = fulCheck;
	}
	public boolean isDodCheck() {
		return dodCheck;
	}
	public void setDodCheck(boolean dodCheck) {
		this.dodCheck = dodCheck;
	}
	public String getAppDecision() {
		return appDecision;
	}
	public void setAppDecision(String appDecision) {
		this.appDecision = appDecision;
	}
	public String getClientMatchReason() {
		return clientMatchReason;
	}
	public void setClientMatchReason(String clientMatchReason) {
		this.clientMatchReason = clientMatchReason;
	}
	public Boolean getClientMatch() {
		return clientMatch;
	}
	public void setClientMatch(Boolean clientMatch) {
		this.clientMatch = clientMatch;
	}
	public String getAuraDisabled() {
		return auraDisabled;
	}
	public void setAuraDisabled(String auraDisabled) {
		this.auraDisabled = auraDisabled;
	}
	/*public String getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(String responseObject) {
		this.responseObject = responseObject;
	}*/
	public String getDeathExclusions() {
		return deathExclusions;
	}
	public void setDeathExclusions(String deathExclusions) {
		this.deathExclusions = deathExclusions;
	}
	public String getTpdExclusions() {
		return tpdExclusions;
	}
	public void setTpdExclusions(String tpdExclusions) {
		this.tpdExclusions = tpdExclusions;
	}
	public String getIpExclusions() {
		return ipExclusions;
	}
	public void setIpExclusions(String ipExclusions) {
		this.ipExclusions = ipExclusions;
	}
	
	public ApplicantInfo getApplicant() {
		return applicant;
	}
	public void setApplicant(ApplicantInfo applicant) {
		this.applicant = applicant;
	}
	public String getDeathLoading() {
		return deathLoading;
	}
	public void setDeathLoading(String deathLoading) {
		this.deathLoading = deathLoading;
	}
	public String getTpdLoading() {
		return tpdLoading;
	}
	public void setTpdLoading(String tpdLoading) {
		this.tpdLoading = tpdLoading;
	}
	public String getIpLoading() {
		return ipLoading;
	}
	public void setIpLoading(String ipLoading) {
		this.ipLoading = ipLoading;
	}
	public List<DocumentInfo> getTransferDocuments() {
		return transferDocuments;
	}
	public void setTransferDocuments(List<DocumentInfo> transferDocuments) {
		this.transferDocuments = transferDocuments;
	}
	
	public List<DocumentInfo> getLifeEventDocuments() {
		return lifeEventDocuments;
	}
	public void setLifeEventDocuments(List<DocumentInfo> lifeEventDocuments) {
		this.lifeEventDocuments = lifeEventDocuments;
	}
	public CoverCancel getCoverCancellation() {
		return coverCancellation;
	}
	public void setCoverCancellation(CoverCancel coverCancellation) {
		this.coverCancellation = coverCancellation;
	}
	public String getAuraType() {
		return auraType;
	}
	public void setAuraType(String auraType) {
		this.auraType = auraType;
	}
	public String getCoverType() {
		return coverType;
	}
	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}
}
