package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import au.com.metlife.eapply.underwriting.response.model.ResponseObject;

public class EapplyInput implements Serializable{
	
	private String name = null;
	private String dob = null;
	private String email = null;
	private String gender = null;
	private String contactType = null;
	private String contactPhone = null;
	private String contactPrefTime = null;
	private String fifteenHr = null;
	private String citizenQue = null;
	private String industryName = null;
	private String otherContactType = null;
	private String otherContactPhone = null;
	private String smoker = null;
	private String title = null;
	private String lastName = null;
	private String firstName = null;
	
	private String industryCode = null;
	private String occupation = null;
	private String withinOfficeQue = null;
	private String tertiaryQue = null;
	private String managementRoleQue = null;
	private String salary = null;
	private String existingDeathAmt = null;
	private String existingDeathUnits = null;
	private String deathCoverName = null;
	
	private String deathCoverType = null;
	private String deathOccCategory = null;
	private double deathLabelAmt;
	private double deathNewCover;
	private double deathCoverPremium ;
	private String existingTpdAmt = null;
	private String existingTPDUnits = null;
	private String tpdCoverName = null;
	private String tpdCoverType = null;
	private String corpFundCode;
	private String auraSessionId;
	private boolean ipDisclaimer;
	private String policyNumber = null;
	private boolean quickQuoteRender = false;
	
	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getAuraSessionId() {
		return auraSessionId;
	}

	public void setAuraSessionId(String auraSessionId) {
		this.auraSessionId = auraSessionId;
	}

	public String getCorpFundCode() {
		return corpFundCode;
	}

	public void setCorpFundCode(String corpFundCode) {
		this.corpFundCode = corpFundCode;
	}

	private String tpdOccCategory = null;
	private double tpdLabelAmt ;
	private double tpdNewCover ;
	private double tpdCoverPremium ;
	private String existingIPUnits = null;
	private String existingIPAmount = null;
	private String existingIPWaitingPeriod = null;
	private String existingIPBenefitPeriod = null;
	
	/*life event place holders*/
	private String deathAmt = null;
	private String tpdAmt = null;
	private String ipAmt = null;
	
	
	public String getDeathAmt() {
		return deathAmt;
	}

	public void setDeathAmt(String deathAmt) {
		this.deathAmt = deathAmt;
	}

	public String getTpdAmt() {
		return tpdAmt;
	}

	public void setTpdAmt(String tpdAmt) {
		this.tpdAmt = tpdAmt;
	}

	public String getIpAmt() {
		return ipAmt;
	}

	public void setIpAmt(String ipAmt) {
		this.ipAmt = ipAmt;
	}

	public String getExistingIPWaitingPeriod() {
		return existingIPWaitingPeriod;
	}

	public void setExistingIPWaitingPeriod(String existingIPWaitingPeriod) {
		this.existingIPWaitingPeriod = existingIPWaitingPeriod;
	}

	public String getExistingIPBenefitPeriod() {
		return existingIPBenefitPeriod;
	}

	public void setExistingIPBenefitPeriod(String existingIPBenefitPeriod) {
		this.existingIPBenefitPeriod = existingIPBenefitPeriod;
	}

	private String clientMatchReason = null;
	private Boolean clientMatched = Boolean.FALSE;
	
	//For transfer existing 
	private String transferDeathExistingAmt;
	private String transferTpdExistingAmt;
	
	private String transferIpExistingAmt;
	private String transferIpWaitingPeriod;
	private String transferIpBenefitPeriod;
	//For Life Event
	private String eventName = null;
	private String eventDate =null;
	private String eventAlreadyApplied = null;
	//for Special cover
	private String previousTpdBenefit= null;
	//for life event calculation
	 	private String deathLifeCoverType = null;
	 	private String tpdLifeCoverType = null;
	 	private String ipLifeCoverType = null;
	 	private String deathNewAmt = null;
	 	private String tpdNewAmt = null;
	 	private String ipNewAmt = null;
	 	private String deathLifeUnits = null;
	 	private String tpdLifeUnits = null;
	 	private String cancelreason;
	 	private String cancelotherreason;
	 	private String eventDesc;
	 	private String ipLifeUnits = null;
		//For firs reason for addnl cover
	 	
	 	
	 	public String getIpLifeUnits() {
			return ipLifeUnits;
		}

		public void setIpLifeUnits(String ipLifeUnits) {
			this.ipLifeUnits = ipLifeUnits;
		}

		private String rsnFrAdnlCvr = null;
	 	
	 	
	 	public String getCancelreason() {
			return cancelreason;
		}

		public void setCancelreason(String cancelreason) {
			this.cancelreason = cancelreason;
		}

		public String getCancelotherreason() {
			return cancelotherreason;
		}

		public void setCancelotherreason(String cancelotherreason) {
			this.cancelotherreason = cancelotherreason;
		}

		public String getRsnFrAdnlCvr() {
			return rsnFrAdnlCvr;
		}

		public void setRsnFrAdnlCvr(String rsnFrAdnlCvr) {
			this.rsnFrAdnlCvr = rsnFrAdnlCvr;
		}
	 			
	 	
	 	public String getDeathNewAmt() {
	 		return deathNewAmt;
	 	}
	 
	 	public void setDeathNewAmt(String deathNewAmt) {
	 		this.deathNewAmt = deathNewAmt;
	 	}
	 
	 	public String getTpdNewAmt() {
	 		return tpdNewAmt;
	 	}
	 
	 	public void setTpdNewAmt(String tpdNewAmt) {
	 		this.tpdNewAmt = tpdNewAmt;
	 	}
	 
	 	public String getIpNewAmt() {
	 		return ipNewAmt;
	 	}
	 
	 	public void setIpNewAmt(String ipNewAmt) {
	 		this.ipNewAmt = ipNewAmt;
	 	}
	 
	 	public String getDeathLifeCoverType() {
	 		return deathLifeCoverType;
	 	}
	 
	 	public void setDeathLifeCoverType(String deathLifeCoverType) {
	 		this.deathLifeCoverType = deathLifeCoverType;
	 	}
	 
	 	public String getTpdLifeCoverType() {
	 		return tpdLifeCoverType;
	 	}
	 
	 	public void setTpdLifeCoverType(String tpdLifeCoverType) {
	 		this.tpdLifeCoverType = tpdLifeCoverType;
	 	}
	 
	 	public String getIpLifeCoverType() {
	 		return ipLifeCoverType;
	 	}
	 
	 	public void setIpLifeCoverType(String ipLifeCoverType) {
	 		this.ipLifeCoverType = ipLifeCoverType;
	 	}
	
	
	public String getPreviousTpdBenefit() {
		return previousTpdBenefit;
	}

	public void setPreviousTpdBenefit(String previousTpdBenefit) {
		this.previousTpdBenefit = previousTpdBenefit;
	}

	public String getTransferDeathExistingAmt() {
		return transferDeathExistingAmt;
	}

	public void setTransferDeathExistingAmt(String transferDeathExistingAmt) {
		this.transferDeathExistingAmt = transferDeathExistingAmt;
	}

	public String getTransferTpdExistingAmt() {
		return transferTpdExistingAmt;
	}

	public void setTransferTpdExistingAmt(String transferTpdExistingAmt) {
		this.transferTpdExistingAmt = transferTpdExistingAmt;
	}

	public String getTransferIpExistingAmt() {
		return transferIpExistingAmt;
	}

	public void setTransferIpExistingAmt(String transferIpExistingAmt) {
		this.transferIpExistingAmt = transferIpExistingAmt;
	}

	public String getTransferIpWaitingPeriod() {
		return transferIpWaitingPeriod;
	}

	public void setTransferIpWaitingPeriod(String transferIpWaitingPeriod) {
		this.transferIpWaitingPeriod = transferIpWaitingPeriod;
	}

	public String getTransferIpBenefitPeriod() {
		return transferIpBenefitPeriod;
	}

	public void setTransferIpBenefitPeriod(String transferIpBenefitPeriod) {
		this.transferIpBenefitPeriod = transferIpBenefitPeriod;
	}

	public String getClientMatchReason() {
		return clientMatchReason;
	}

	public void setClientMatchReason(String clientMatchReason) {
		this.clientMatchReason = clientMatchReason;
	}

	public Boolean getClientMatched() {
		return clientMatched;
	}

	public void setClientMatched(Boolean clientMatched) {
		this.clientMatched = clientMatched;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getExistingIPAmount() {
		return existingIPAmount;
	}

	public void setExistingIPAmount(String existingIPAmount) {
		this.existingIPAmount = existingIPAmount;
	}

	private String ipCoverName = null;
	private String ipOccCategory = null;
	private String ipcheckbox = null;
	private String waitingPeriod = null;
	private int ipSalaryPercent;
    

	private String benefitPeriod = null;
	private double ipLabelAmt ;
	private String ipNewCover ;
	private double ipCoverPremium ;
	private BigDecimal totalPremium ;
	private Long appNum = null;
	private boolean ackCheck = false;	
	private boolean privacyCheck = false;	
	private boolean fulCheck = false;	
	private boolean ccoverIndex = false;
	public boolean isPrivacyCheck() {
		return privacyCheck;
	}

	public void setPrivacyCheck(boolean privacyCheck) {
		this.privacyCheck = privacyCheck;
	}

	public boolean isDodCheck() {
		return dodCheck;
	}

	public void setDodCheck(boolean dodCheck) {
		this.dodCheck = dodCheck;
	}

	private boolean dodCheck = false;	
	
	private String clientRefNumber = null;
	
	private String overallDecision = null;
	private String deathDecision = null;
	private String deathLoading = null;
	private String deathExclusions = null;
	private String deathResons = null;
	private String deathAuraResons = null;
	private String deathOrigTotalDebitsValue = null;
	private String tpdDecision = null;
	private String tpdLoading = null;
	
	private String tpdExclusions = null;
	private String tpdResons = null;
	private String tpdAuraResons = null;
	private String tpdOrigTotalDebitsValue = null;
	private String ipDecision = null;
	private String ipLoading = null;
	private String ipExclusions = null;
	private String ipResons = null;
	private String ipAuraResons = null;
	private String ipOrigTotalDebitsValue = null;
	private String previousFundName = null;
	private String membershipNumber = null;
	private String spinNumber = null;
	private String previousTpdClaimQue = null;
	private String terminalIllClaimQue = null;
	private String documentName = null;
	private String indexationDeath;
	private String indexationTpd;
	private boolean ownOccuptionDeath;
	private boolean ownOccuptionTpd;
	private String ownOccuptionIp;
	private String auraDisabled;
	private String unitisedCovers;
	////Added for document sending address inclusion in PDF
	private String documentAck2 = null;
    private String documentAddress = null;
	private String deathFul;
	private String tpdFul;
	private String productnumber;
	public String getDeathFul() {
		return deathFul;
	}

	public void setDeathFul(String deathFul) {
		this.deathFul = deathFul;
	}

	public String getTpdFul() {
		return tpdFul;
	}

	public void setTpdFul(String tpdFul) {
		this.tpdFul = tpdFul;
	}

	public String getIpFul() {
		return ipFul;
	}

	public void setIpFul(String ipFul) {
		this.ipFul = ipFul;
	}

	private String ipFul;
	
	


	public String getDocumentAddress() {
		return documentAddress;
	}

	public void setDocumentAddress(String documentAddress) {
		this.documentAddress = documentAddress;
	}

	public String getDocumentAck2() {
		return documentAck2;
	}

	public void setDocumentAck2(String documentAck2) {
		this.documentAck2 = documentAck2;
	}

	public String getUnitisedCovers() {
		return unitisedCovers;
	}

	public void setUnitisedCovers(String unitisedCovers) {
		this.unitisedCovers = unitisedCovers;
	}

	public String getAuraDisabled() {
		return auraDisabled;
	}

	public void setAuraDisabled(String auraDisabled) {
		this.auraDisabled = auraDisabled;
	}

	public String getIndexationDeath() {
		return indexationDeath;
	}

	public int getIpSalaryPercent() {
		return ipSalaryPercent;
	}

	public void setIpSalaryPercent(int ipSalaryPercent) {
		this.ipSalaryPercent = ipSalaryPercent;
	}

	public void setIndexationDeath(String indexationDeath) {
		this.indexationDeath = indexationDeath;
	}

	public String getIndexationTpd() {
		return indexationTpd;
	}

	public void setIndexationTpd(String indexationTpd) {
		this.indexationTpd = indexationTpd;
	}

	private ResponseObject responseObject = null;
	private AddressJSON address;
	private ContactDetailsJSON contactDetails;
	private PersonalDetailsJSON personalDetails;
	private ExistingCoversJSON existingCovers;
	private OccupationCoversJSON occupationDetails;
	private DeathAddnlCoversJSON addnlDeathCoverDetails;
	private TpdAddnlCoversJSON addnlTpdCoverDetails;
	private IpAddnlCoversJSON addnlIpCoverDetails;
	private List<DocumentJSON> transferDocuments;
	private List<DocumentJSON> lifeEventDocuments;
	
	//Added for dynamic cancel cover message
	private CoverCancel coverCancellation;
	
	public CoverCancel getCoverCancellation() {
		return coverCancellation;
	}

	public void setCoverCancellation(CoverCancel coverCancellation) {
		this.coverCancellation = coverCancellation;
	}
	
	public List<DocumentJSON> getLifeEventDocuments() {
		return lifeEventDocuments;
	}

	public void setLifeEventDocuments(List<DocumentJSON> lifeEventDocuments) {
		this.lifeEventDocuments = lifeEventDocuments;
	}

	public String getPreviousFundName() {
		return previousFundName;
	}

	public String getPreviousTpdClaimQue() {
		return previousTpdClaimQue;
	}

	public void setPreviousTpdClaimQue(String previousTpdClaimQue) {
		this.previousTpdClaimQue = previousTpdClaimQue;
	}

	public String getTerminalIllClaimQue() {
		return terminalIllClaimQue;
	}

	public void setTerminalIllClaimQue(String terminalIllClaimQue) {
		this.terminalIllClaimQue = terminalIllClaimQue;
	}

	public void setPreviousFundName(String previousFundName) {
		this.previousFundName = previousFundName;
	}

	public String getMembershipNumber() {
		return membershipNumber;
	}

	public void setMembershipNumber(String membershipNumber) {
		this.membershipNumber = membershipNumber;
	}

	public String getSpinNumber() {
		return spinNumber;
	}

	public void setSpinNumber(String spinNumber) {
		this.spinNumber = spinNumber;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public String getClientRefNumber() {
		return clientRefNumber;
	}

	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}

	public List<DocumentJSON> getTransferDocuments() {
		return transferDocuments;
	}

	public void setTransferDocuments(List<DocumentJSON> transferDocuments) {
		this.transferDocuments = transferDocuments;
	}

	public OccupationCoversJSON getOccupationDetails() {
		return occupationDetails;
	}

	public void setOccupationDetails(OccupationCoversJSON occupationDetails) {
		this.occupationDetails = occupationDetails;
	}

	public DeathAddnlCoversJSON getAddnlDeathCoverDetails() {
		return addnlDeathCoverDetails;
	}

	public void setAddnlDeathCoverDetails(DeathAddnlCoversJSON addnlDeathCoverDetails) {
		this.addnlDeathCoverDetails = addnlDeathCoverDetails;
	}

	public TpdAddnlCoversJSON getAddnlTpdCoverDetails() {
		return addnlTpdCoverDetails;
	}

	public void setAddnlTpdCoverDetails(TpdAddnlCoversJSON addnlTpdCoverDetails) {
		this.addnlTpdCoverDetails = addnlTpdCoverDetails;
	}

	public IpAddnlCoversJSON getAddnlIpCoverDetails() {
		return addnlIpCoverDetails;
	}

	public void setAddnlIpCoverDetails(IpAddnlCoversJSON addnlIpCoverDetails) {
		this.addnlIpCoverDetails = addnlIpCoverDetails;
	}

	
	private String dateJoined;
	private String lastSavedOn;
	private String manageType;
	private String partnerCode;
	private int age;
	private String memberType;
	private String ipCoverType;
	private String freqCostType;
	//Added for Convert and Maintain identification
	private boolean convertCheck = false;
	//Added for existing Occupation Rating
	private String existingOccRating;

	public String getFreqCostType() {
		return freqCostType;
	}

	public void setFreqCostType(String freqCostType) {
		this.freqCostType = freqCostType;
	}

	public String getIpCoverType() {
		return ipCoverType;
	}

	public void setIpCoverType(String ipCoverType) {
		this.ipCoverType = ipCoverType;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getLastSavedOn() {
		return lastSavedOn;
	}

	public void setLastSavedOn(String lastSavedOn) {
		this.lastSavedOn = lastSavedOn;
	}

	public String getDateJoined() {
		return dateJoined;
	}

	public void setDateJoined(String dateJoined) {
		this.dateJoined = dateJoined;
	}

	public ExistingCoversJSON getExistingCovers() {
		return existingCovers;
	}

	public void setExistingCovers(ExistingCoversJSON existingCovers) {
		this.existingCovers = existingCovers;
	}

	public AddressJSON getAddress() {
		return address;
	}

	public ContactDetailsJSON getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(ContactDetailsJSON contactDetails) {
		this.contactDetails = contactDetails;
	}

	public PersonalDetailsJSON getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetailsJSON personalDetails) {
		this.personalDetails = personalDetails;
	}

	public void setAddress(AddressJSON address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactPrefTime() {
		return contactPrefTime;
	}

	public void setContactPrefTime(String contactPrefTime) {
		this.contactPrefTime = contactPrefTime;
	}

	public String getFifteenHr() {
		return fifteenHr;
	}

	public void setFifteenHr(String fifteenHr) {
		this.fifteenHr = fifteenHr;
	}

	public String getCitizenQue() {
		return citizenQue;
	}

	public void setCitizenQue(String citizenQue) {
		this.citizenQue = citizenQue;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	
	public String getOtherContactType() {
		return otherContactType;
	}
	
	public void setOtherContactType(String otherContactType) {
		this.otherContactType = otherContactType;
	}
	public String getOtherContactPhone() {
		return otherContactPhone;
	}
	
	public void setOtherContactPhone(String otherContactPhone) {
		this.otherContactPhone = otherContactPhone;
	}
	
	public String getSmoker() {
		return smoker;
	}

	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getWithinOfficeQue() {
		return withinOfficeQue;
	}

	public void setWithinOfficeQue(String withinOfficeQue) {
		this.withinOfficeQue = withinOfficeQue;
	}

	public String getTertiaryQue() {
		return tertiaryQue;
	}

	public void setTertiaryQue(String tertiaryQue) {
		this.tertiaryQue = tertiaryQue;
	}

	public String getManagementRoleQue() {
		return managementRoleQue;
	}

	public void setManagementRoleQue(String managementRoleQue) {
		this.managementRoleQue = managementRoleQue;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getExistingDeathAmt() {
		return existingDeathAmt;
	}

	public void setExistingDeathAmt(String existingDeathAmt) {
		this.existingDeathAmt = existingDeathAmt;
	}

	public String getExistingDeathUnits() {
		return existingDeathUnits;
	}

	public void setExistingDeathUnits(String existingDeathUnits) {
		this.existingDeathUnits = existingDeathUnits;
	}

	public String getDeathCoverName() {
		return deathCoverName;
	}

	public void setDeathCoverName(String deathCoverName) {
		this.deathCoverName = deathCoverName;
	}

	public String getDeathCoverType() {
		return deathCoverType;
	}

	public void setDeathCoverType(String deathCoverType) {
		this.deathCoverType = deathCoverType;
	}

	public String getDeathOccCategory() {
		return deathOccCategory;
	}

	public void setDeathOccCategory(String deathOccCategory) {
		this.deathOccCategory = deathOccCategory;
	}

	public double getDeathLabelAmt() {
		return deathLabelAmt;
	}

	public void setDeathLabelAmt(double deathLabelAmt) {
		this.deathLabelAmt = deathLabelAmt;
	}

	public double getDeathNewCover() {
		return deathNewCover;
	}

	public void setDeathNewCover(double deathNewCover) {
		this.deathNewCover = deathNewCover;
	}

	public double getDeathCoverPremium() {
		return deathCoverPremium;
	}

	public void setDeathCoverPremium(double deathCoverPremium) {
		this.deathCoverPremium = deathCoverPremium;
	}

	public String getExistingTpdAmt() {
		return existingTpdAmt;
	}

	public void setExistingTpdAmt(String existingTpdAmt) {
		this.existingTpdAmt = existingTpdAmt;
	}

	public String getExistingTPDUnits() {
		return existingTPDUnits;
	}

	public void setExistingTPDUnits(String existingTPDUnits) {
		this.existingTPDUnits = existingTPDUnits;
	}

	public String getTpdCoverName() {
		return tpdCoverName;
	}

	public void setTpdCoverName(String tpdCoverName) {
		this.tpdCoverName = tpdCoverName;
	}

	public String getTpdCoverType() {
		return tpdCoverType;
	}

	public void setTpdCoverType(String tpdCoverType) {
		this.tpdCoverType = tpdCoverType;
	}

	public String getTpdOccCategory() {
		return tpdOccCategory;
	}

	public void setTpdOccCategory(String tpdOccCategory) {
		this.tpdOccCategory = tpdOccCategory;
	}

	public double getTpdLabelAmt() {
		return tpdLabelAmt;
	}

	public void setTpdLabelAmt(double tpdLabelAmt) {
		this.tpdLabelAmt = tpdLabelAmt;
	}

	public double getTpdNewCover() {
		return tpdNewCover;
	}

	public void setTpdNewCover(double tpdNewCover) {
		this.tpdNewCover = tpdNewCover;
	}

	public double getTpdCoverPremium() {
		return tpdCoverPremium;
	}

	public void setTpdCoverPremium(double tpdCoverPremium) {
		this.tpdCoverPremium = tpdCoverPremium;
	}

	public String getExistingIPUnits() {
		return existingIPUnits;
	}

	public void setExistingIPUnits(String existingIPUnits) {
		this.existingIPUnits = existingIPUnits;
	}

	public String getIpCoverName() {
		return ipCoverName;
	}

	public void setIpCoverName(String ipCoverName) {
		this.ipCoverName = ipCoverName;
	}

	public String getIpOccCategory() {
		return ipOccCategory;
	}

	public void setIpOccCategory(String ipOccCategory) {
		this.ipOccCategory = ipOccCategory;
	}

	public String getIpcheckbox() {
		return ipcheckbox;
	}

	public void setIpcheckbox(String ipcheckbox) {
		this.ipcheckbox = ipcheckbox;
	}

	public String getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public String getBenefitPeriod() {
		return benefitPeriod;
	}

	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	public double getIpLabelAmt() {
		return ipLabelAmt;
	}

	public void setIpLabelAmt(double ipLabelAmt) {
		this.ipLabelAmt = ipLabelAmt;
	}

	public String getIpNewCover() {
		return ipNewCover;
	}

	public void setIpNewCover(String ipNewCover) {
		this.ipNewCover = ipNewCover;
	}

	public double getIpCoverPremium() {
		return ipCoverPremium;
	}

	public void setIpCoverPremium(double ipCoverPremium) {
		this.ipCoverPremium = ipCoverPremium;
	}

	public BigDecimal getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}
	
	public Long getAppNum() {
		return appNum;
	}

	public void setAppNum(Long appNum) {
		this.appNum = appNum;
	}

	public boolean isAckCheck() {
		return ackCheck;
	}

	public void setAckCheck(boolean ackCheck) {
		this.ackCheck = ackCheck;
	}

	public String getOverallDecision() {
		return overallDecision;
	}

	public void setOverallDecision(String overallDecision) {
		this.overallDecision = overallDecision;
	}

	public String getDeathDecision() {
		return deathDecision;
	}

	public void setDeathDecision(String deathDecision) {
		this.deathDecision = deathDecision;
	}

	public String getDeathLoading() {
		return deathLoading;
	}

	public void setDeathLoading(String deathLoading) {
		this.deathLoading = deathLoading;
	}

	public String getDeathExclusions() {
		return deathExclusions;
	}

	public void setDeathExclusions(String deathExclusions) {
		this.deathExclusions = deathExclusions;
	}

	public String getDeathResons() {
		return deathResons;
	}

	public void setDeathResons(String deathResons) {
		this.deathResons = deathResons;
	}

	public String getDeathAuraResons() {
		return deathAuraResons;
	}

	public void setDeathAuraResons(String deathAuraResons) {
		this.deathAuraResons = deathAuraResons;
	}

	public String getDeathOrigTotalDebitsValue() {
		return deathOrigTotalDebitsValue;
	}

	public void setDeathOrigTotalDebitsValue(String deathOrigTotalDebitsValue) {
		this.deathOrigTotalDebitsValue = deathOrigTotalDebitsValue;
	}

	public String getTpdDecision() {
		return tpdDecision;
	}

	public void setTpdDecision(String tpdDecision) {
		this.tpdDecision = tpdDecision;
	}

	public String getTpdLoading() {
		return tpdLoading;
	}

	public void setTpdLoading(String tpdLoading) {
		this.tpdLoading = tpdLoading;
	}

	public String getTpdExclusions() {
		return tpdExclusions;
	}

	public void setTpdExclusions(String tpdExclusions) {
		this.tpdExclusions = tpdExclusions;
	}

	public String getTpdResons() {
		return tpdResons;
	}

	public void setTpdResons(String tpdResons) {
		this.tpdResons = tpdResons;
	}

	public String getTpdAuraResons() {
		return tpdAuraResons;
	}

	public void setTpdAuraResons(String tpdAuraResons) {
		this.tpdAuraResons = tpdAuraResons;
	}

	public String getTpdOrigTotalDebitsValue() {
		return tpdOrigTotalDebitsValue;
	}

	public void setTpdOrigTotalDebitsValue(String tpdOrigTotalDebitsValue) {
		this.tpdOrigTotalDebitsValue = tpdOrigTotalDebitsValue;
	}

	public String getIpDecision() {
		return ipDecision;
	}

	public void setIpDecision(String ipDecision) {
		this.ipDecision = ipDecision;
	}

	public String getIpLoading() {
		return ipLoading;
	}

	public void setIpLoading(String ipLoading) {
		this.ipLoading = ipLoading;
	}

	public String getIpExclusions() {
		return ipExclusions;
	}

	public void setIpExclusions(String ipExclusions) {
		this.ipExclusions = ipExclusions;
	}

	public String getIpResons() {
		return ipResons;
	}

	public void setIpResons(String ipResons) {
		this.ipResons = ipResons;
	}

	public String getIpAuraResons() {
		return ipAuraResons;
	}

	public void setIpAuraResons(String ipAuraResons) {
		this.ipAuraResons = ipAuraResons;
	}

	public String getIpOrigTotalDebitsValue() {
		return ipOrigTotalDebitsValue;
	}

	public void setIpOrigTotalDebitsValue(String ipOrigTotalDebitsValue) {
		this.ipOrigTotalDebitsValue = ipOrigTotalDebitsValue;
	}

	public ResponseObject getResponseObject() {
		return responseObject;
	}

	public void setResponseObject(ResponseObject responseObject) {
		this.responseObject = responseObject;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventAlreadyApplied() {
		return eventAlreadyApplied;
	}

	public String getDeathLifeUnits() {
		return deathLifeUnits;
	}

	public void setDeathLifeUnits(String deathLifeUnits) {
		this.deathLifeUnits = deathLifeUnits;
	}

	public String getTpdLifeUnits() {
		return tpdLifeUnits;
	}

	public void setTpdLifeUnits(String tpdLifeUnits) {
		this.tpdLifeUnits = tpdLifeUnits;
	}

	public void setEventAlreadyApplied(String eventAlreadyApplied) {
		this.eventAlreadyApplied = eventAlreadyApplied;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public boolean isFulCheck() {
		return fulCheck;
	}

	public void setFulCheck(boolean fulCheck) {
		this.fulCheck = fulCheck;
	}

	public boolean isIpDisclaimer() {
		return ipDisclaimer;
	}

	public void setIpDisclaimer(boolean ipDisclaimer) {
		this.ipDisclaimer = ipDisclaimer;
	}

	public String getProductnumber() {
		return productnumber;
	}

	public void setProductnumber(String productnumber) {
		this.productnumber = productnumber;
	}

	public boolean getOwnOccuptionDeath() {
		return ownOccuptionDeath;
	}

	public void setOwnOccuptionDeath(boolean ownOccuptionDeath) {
		this.ownOccuptionDeath = ownOccuptionDeath;
	}

	public boolean getOwnOccuptionTpd() {
		return ownOccuptionTpd;
	}

	public String getOwnOccuptionIp() {
		return ownOccuptionIp;
	}

	public void setOwnOccuptionTpd(boolean ownOccuptionTpd) {
		this.ownOccuptionTpd = ownOccuptionTpd;
	}

	public void setOwnOccuptionIp(String ownOccuptionIp) {
		this.ownOccuptionIp = ownOccuptionIp;
	}
	
	public boolean isConvertCheck() {
		return convertCheck;
	}

	public void setConvertCheck(boolean convertCheck) {
		this.convertCheck = convertCheck;
	}

	public boolean isCcoverIndex() {
		return ccoverIndex;
	}

	public void setCcoverIndex(boolean ccoverIndex) {
		this.ccoverIndex = ccoverIndex;
	}
	public boolean isQuickQuoteRender() {
		return quickQuoteRender;
	}

	public void setQuickQuoteRender(boolean quickQuoteRender) {
		this.quickQuoteRender = quickQuoteRender;
	}

	public String getExistingOccRating() {
		return existingOccRating;
	}

	public void setExistingOccRating(String existingOccRating) {
		this.existingOccRating = existingOccRating;
	}
	
}
