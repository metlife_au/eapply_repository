package au.com.metlife.eapply.bs.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.bs.pdf.CostumPdfAPI;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;

public class CommonPDFHelper {
	
    public final static String PACKAGE_NAME = "com.metlife.eapply.pdf.helper"; //$NON-NLS-1$
    
    // Name given to this class.
    public final static String CLASS_NAME = "CommonPDFHelper"; //$NON-NLS-1$
    
    
    public static String getDateInString(Date dte) {

        java.util.Date currDate = dte;
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String theDate="";
        
        if(null != currDate && !"".equalsIgnoreCase(""+currDate)){
        	theDate = formatter.format(currDate);	
        }
        
         currDate = null;
         formatter = null;
        return theDate;
    }
	
    public static String getTodaysDate() {

        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String theDate = formatter.format( currDate);
        currDate = null;
        formatter = null;
        return theDate;
    }
    public static String getDateExtension() {

        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "hhmmddMMyyyy");
        String theDate = formatter.format( currDate);
        currDate = null;
        formatter = null;
        return theDate;
    }
    
    /**
     * Formats amount to $#,###,###.00 e.g. $1,000.00
     * 
     * @param amount
     * @return
     */
    public static String formatAmount(String amount) {

        final String METHOD_NAME = "formatAmount";
        String formattedStr = amount;
        NumberFormat formatter = null;
        try {
            formatter = new DecimalFormat( "$#,###,###.00");
            if (amount != null && !amount.trim().equalsIgnoreCase( "")
                    && formatter != null) {
                if (amount.contains( "$")) {
                    amount = amount.replace( "$", "").trim();
                }
                if (amount.contains( ",")) {
                    amount = amount.replace( ",", "").trim();
                }
                formattedStr = formatter.format( new BigDecimal( amount).doubleValue());
                if (formattedStr != null
                        && ( formattedStr.substring( 1, ( formattedStr.indexOf( ".")))).equals( "")) {
                    formattedStr = "$0."
                            + formattedStr.substring( ( formattedStr.indexOf( ".")) + 1);
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
            amount = null;
            formatter = null;
        }        
        return formattedStr;
    }
    
    /**
     * Formats amount to $#,###,###.00 e.g. $1,000.00
     * 
     * @param amount
     * @return
     */
    public static String formatIntAmount(String amount) {

        final String METHOD_NAME = "formatIntAmount";
        String formattedStr = amount;
        NumberFormat formatter = null;
        try {
            formatter = new DecimalFormat( "$#,###,###");
            if (amount != null && !amount.trim().equalsIgnoreCase( "")
                    && formatter != null) {
                if (amount.contains( "$")) {
                    amount = amount.replace( "$", "").trim();
                }
                if (amount.contains( ",")) {
                    amount = amount.replace( ",", "").trim();
                }
                formattedStr = formatter.format( new BigDecimal( amount).intValue());
                /*if (formattedStr != null
                        && ( formattedStr.substring( 1, ( formattedStr.indexOf( ".")))).equals( "")) {
                    formattedStr = "$0."
                            + formattedStr.substring( ( formattedStr.indexOf( ".")) + 1);
                }*/
            }
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
            amount = null;
            formatter = null;
        }       
        return formattedStr;
    }
    
    
    public static String numberFormat (Double number) {
		String pattern = "###,###.##"; 
		DecimalFormat formatter = new DecimalFormat(pattern); 
		if (number != null) {
			return formatter.format(number); 
		} else {
			return null;
		}
	}
    
	public static void dispalyRuleDecision(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject, ResponseObject responseObject) throws DocumentException{
		
		 Insured insured = null;
		
		PdfPTable pdfPEmptyTable = new PdfPTable( 1);
        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPEmptyCell3);
        pdfPEmptyTable.addCell( pdfPEmptyCell3);
        document.add( pdfPEmptyTable);
        
        float[] decFloat = { 0.8f, 0.2f };
        
        PdfPTable pdfDecisionTable = new PdfPTable( decFloat);
        PdfPCell pdfPDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Original Decision Details", MetlifeInstitutionalConstants.COLOR_WHITE, 10));
        pdfPDecisionCell.setBorderWidth( 1);
        pdfPDecisionCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfAPI.disableBordersTop( pdfPDecisionCell);
        pdfPDecisionCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfPDecisionCell.setPaddingBottom( 6);
        pdfPDecisionCell.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfDecisionTable.addCell( pdfPDecisionCell);
        pdfPDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "", MetlifeInstitutionalConstants.COLOR_WHITE, 10));
        pdfAPI.disableBorders( pdfPDecisionCell);
        pdfDecisionTable.addCell( pdfPDecisionCell);
        document.add( pdfDecisionTable);
        
        pdfPEmptyTable = new PdfPTable( 1);
        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPEmptyCell3);
        pdfPEmptyTable.addCell( pdfPEmptyCell3);
        document.add( pdfPEmptyTable);
		
		float[] f1 = { 0.2f, 0.2f, 0.4f };
        PdfPTable pdfDecisionTable1 = new PdfPTable( f1);
        
        PdfPCell pdfDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Decision Type", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
        pdfAPI.disableBorders( pdfDecisionCell);
        pdfDecisionTable1.addCell( pdfDecisionCell);
        pdfDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Product", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
        pdfAPI.disableBorders( pdfDecisionCell);
        pdfDecisionTable1.addCell( pdfDecisionCell);
        pdfDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Decision", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
        pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_MIDDLE);
        pdfAPI.disableBorders( pdfDecisionCell);
        pdfDecisionTable1.addCell( pdfDecisionCell);
        document.add( pdfDecisionTable1);
        
        PdfPTable pdfSingleSpaceTable = new PdfPTable( 1);
        PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        document.add( pdfSingleSpaceTable);
		if (null!=responseObject
				&& null!=responseObject.getClientData()
                && null != responseObject.getClientData().getListInsured()) {
            for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
                insured = (Insured) responseObject.getClientData().getListInsured().get( itr);
                	dispalyRuleDecisionDetails( insured.getListRuleDec(), pdfAPI, document,pdfObject);               
            
                	
                	if("Institutional".equalsIgnoreCase(pdfObject.getLob())){
                		if(null!=insured.getListGenderAgeBMIDec() && insured.getListGenderAgeBMIDec().size()>0){
                    		dispalyGenderAgeBMIDetails(insured.getListGenderAgeBMIDec(), pdfAPI, document,pdfObject);
                    	}            
                	}else{
                		if(null!=insured.getListGenderAgeBMIRangeTable() && insured.getListGenderAgeBMIRangeTable().size()>0){
                    		dispalyGenderAgeBMIDetails( insured.getListGenderAgeBMIRangeTable(), pdfAPI, document,pdfObject);
                    	}                 		
                		if(null!=insured.getListGenderAgeCalculatedBMIRangeMetlifeTable() && insured.getListGenderAgeCalculatedBMIRangeMetlifeTable().size()>0){
                    		dispalyGenderAgeBMIDetails( insured.getListGenderAgeCalculatedBMIRangeMetlifeTable(), pdfAPI, document,pdfObject);
                    	}
                	}
                	
                	
                
                dispalyDependentRiskDetails(insured.getListDepRiskDec(),pdfAPI, document,pdfObject);
                
            }
            
        }
		
		pdfPEmptyTable = null;
        pdfPEmptyCell3 = null;
        pdfDecisionTable = null;
        pdfPDecisionCell = null;
        pdfDecisionTable1 = null;
        pdfDecisionCell = null;
        insured = null;
	}
	
	public static void dispalyINGRuleDecision(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject, ResponseObject responseObject) throws DocumentException{
		
		 Insured insured = null;
		
		PdfPTable pdfPEmptyTable = new PdfPTable( 1);
       PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
       pdfAPI.disableBorders( pdfPEmptyCell3);
       pdfPEmptyTable.addCell( pdfPEmptyCell3);
       document.add( pdfPEmptyTable);
       
       float[] decFloat = { 0.8f, 0.2f };
       
       PdfPTable pdfDecisionTable = new PdfPTable( decFloat);
       PdfPCell pdfPDecisionCell = new PdfPCell(
               pdfAPI.addParagraph("Original Decision Details", FontFactory.HELVETICA, 12, Font.BOLD, MetlifeInstitutionalConstants.COLOR_WHITE));      
       
      // pdfPDecisionCell.setBorderWidth( 1);
       pdfPDecisionCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
       pdfAPI.disableBordersTop( pdfPDecisionCell);
       pdfPDecisionCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
       pdfPDecisionCell.setPaddingBottom( 6);
       pdfPDecisionCell.setVerticalAlignment( Element.ALIGN_CENTER);
       pdfDecisionTable.addCell( pdfPDecisionCell);
       pdfPDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "", MetlifeInstitutionalConstants.COLOR_WHITE, 10));
       pdfAPI.disableBorders( pdfPDecisionCell);
       pdfDecisionTable.addCell( pdfPDecisionCell);
       document.add( pdfDecisionTable);
       
       pdfPEmptyTable = new PdfPTable( 1);
       pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
       pdfAPI.disableBorders( pdfPEmptyCell3);
       pdfPEmptyTable.addCell( pdfPEmptyCell3);
       document.add( pdfPEmptyTable);
		
		float[] f1 = { 0.2f, 0.2f, 0.4f };
       PdfPTable pdfDecisionTable1 = new PdfPTable( f1);
       
       PdfPCell pdfDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "Decision Type", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
       pdfAPI.disableBorders( pdfDecisionCell);
       pdfDecisionTable1.addCell( pdfDecisionCell);
       pdfDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "Product", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
       pdfAPI.disableBorders( pdfDecisionCell);
       pdfDecisionTable1.addCell( pdfDecisionCell);
       pdfDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "Decision", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
       pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_MIDDLE);
       pdfAPI.disableBorders( pdfDecisionCell);
       pdfDecisionTable1.addCell( pdfDecisionCell);
       document.add( pdfDecisionTable1);
       
       PdfPTable pdfSingleSpaceTable = new PdfPTable( 1);
       PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
       pdfAPI.disableBorders( pdfPSingleSpaceCell);
       pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
       document.add( pdfSingleSpaceTable);
		if (null!=responseObject
				&& null!=responseObject.getClientData()
               && null != responseObject.getClientData().getListInsured()) {
           for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
               insured = (Insured) responseObject.getClientData().getListInsured().get( itr);
               	dispalyINGRuleDecisionDetails( insured.getListRuleDec(), pdfAPI, document,pdfObject);               
           
               	
               	if("Institutional".equalsIgnoreCase(pdfObject.getLob())){
               		if(null!=insured.getListGenderAgeBMIDec() && insured.getListGenderAgeBMIDec().size()>0){
                   		dispalyINGGenderAgeBMIDetails(insured.getListGenderAgeBMIDec(), pdfAPI, document,pdfObject);
                   	}            
               	}else{
               		if(null!=insured.getListGenderAgeBMIRangeTable() && insured.getListGenderAgeBMIRangeTable().size()>0){
               			dispalyINGGenderAgeBMIDetails( insured.getListGenderAgeBMIRangeTable(), pdfAPI, document,pdfObject);
                   	}            	
               	}
               	
               	
               
               dispalyDependentRiskDetails(insured.getListDepRiskDec(),pdfAPI, document,pdfObject);
               
           }
           
       }
		
		pdfPEmptyTable = null;
       pdfPEmptyCell3 = null;
       pdfDecisionTable = null;
       pdfPDecisionCell = null;
       pdfDecisionTable1 = null;
       pdfDecisionCell = null;
       insured = null;
	}
	
	
	
	 public static void dispalyDependentRiskDetails(List dependentRiskDetails,
	            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {

	        final String METHOD_NAME = "dispalyGenderAgeBMIDetails"; //$NON-NLS-1$
	       
	        PdfPTable ruleTable = null;
            PdfPCell ruleCell = null;
            PdfPCell ruleSubCell = null;
            Decision decision = null;
            PdfPCell ruleTableSubCell = null;
            PdfPTable ruleSubTable = null;
            StringBuffer strBuff = new StringBuffer();
	        if (null != dependentRiskDetails && dependentRiskDetails.size()>0) {
	            
	            float[] ruleFloat = { 0.12f, 0.68f };
	            ruleTable = new PdfPTable( ruleFloat);
	            ruleCell = new PdfPCell(
	                    pdfAPI.addParagraph( "Dependent Risks", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( ruleCell);
	            ruleTable.addCell( ruleCell);
	            ruleSubCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	            
	            for (int ruleSubItr = 0; ruleSubItr < dependentRiskDetails.size(); ruleSubItr++) {
	                decision = (Decision) dependentRiskDetails.get( ruleSubItr);
	                float[] ruleSubFloat = { 0.28f, 0.4f };
	                ruleSubTable = new PdfPTable( ruleSubFloat);
	                ruleTableSubCell = null;
	                if (null != decision.getProductName()
	                        && !"".equalsIgnoreCase( decision.getProductName())
	                        && decision.getProductName().length() > 1) {
	                	
	                	 ruleTableSubCell = new PdfPCell(
	                             pdfAPI.addParagraph(decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	                    
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    strBuff = new StringBuffer();
	                    strBuff.append( decision.getDecision());
	                    
	                    if (null != decision.getTotalDebitsValue()
	                            && decision.getTotalDebitsValue().length() > 0
	                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
	                   	 strBuff.append( "    Load: (");
	                        strBuff.append(decision.getTotalDebitsValue());
	                        strBuff.append( ", ");
	                        strBuff.append( ");\n");
	                    }
	                    
	                    
	                    if (null != decision.getReasons()
	                            && decision.getReasons().length > 0) {
	                        strBuff.append( "Reason: (");
	                        String reason[] = decision.getReasons();
	                        for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
	                            strBuff.append( reason[reasonItr]);
	                            strBuff.append( ",");
	                        }
	                        strBuff.append( ")");
	                        
	                    }
	                    
	                    ruleTableSubCell = new PdfPCell(
	                            pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    ruleSubCell.addElement( ruleSubTable);
	                    
	                    
	                } 
	            }
	            
	            /*for (int ruleSubItr = 0; ruleSubItr < dependentRiskDetails.size(); ruleSubItr++) {
	                decision = (Decision) dependentRiskDetails.get( ruleSubItr);
	                float[] ruleSubFloat = { 0.28f, 0.4f };
	                ruleSubTable = new PdfPTable( ruleSubFloat);
	                
	                if (null != decision.getProductName()
	                        && !"".equalsIgnoreCase( decision.getProductName())
	                        && decision.getProductName().length() > 1) {
	                	
	                	 ruleTableSubCell = new PdfPCell(
	                             pdfAPI.addParagraph(decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	                    
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    
	                    strBuff.append( decision.getDecision());
	                    
	                    if (null != decision.getTotalDebitsValue()
	                            && decision.getTotalDebitsValue().length() > 0
	                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
	                   	 strBuff.append( "    Load: (");
	                        strBuff.append(decision.getTotalDebitsValue());
	                        strBuff.append( ", ");
	                        strBuff.append( ");\n");
	                    }
	                    
	                    
	                    if (null != decision.getReasons()
	                            && decision.getReasons().length > 0) {
	                        strBuff.append( "Reason: (");
	                        String reason[] = decision.getReasons();
	                        for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
	                            strBuff.append( reason[reasonItr]);
	                            strBuff.append( ",");
	                        }
	                        strBuff.append( ")");
	                        
	                    }
	                    
	                    ruleTableSubCell = new PdfPCell(
	                            pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    ruleSubCell.addElement( ruleSubTable);
	                    strBuff = new StringBuffer();
	                    
	                } 
	            }*/
	            pdfAPI.disableBorders( ruleSubCell);
	            ruleTable.addCell( ruleSubCell);
	            document.add( ruleTable);
	            
	        }
	        ruleTable = null;
            ruleCell = null;
            ruleSubCell = null;
            decision = null;
            ruleTableSubCell = null;
            ruleSubTable = null;
            strBuff = null;
	        
	    }

	
	/**
     * Description: display GenderAgeBMI decision details in the PDF Section
     * 
     * @param
     * java.lang.String,java.lang.String,,java.lang.String,,java.lang.String,
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    public static void dispalyGenderAgeBMIDetails(List genderAgeBMIListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {

        final String METHOD_NAME = "dispalyGenderAgeBMIDetails"; //$NON-NLS-1$
       
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        Decision decision = null;
        PdfPCell ruleTableSubCell = null;
        PdfPTable ruleSubTable = null;
        StringBuffer strBuff = new StringBuffer();
        Rating rating = null;
        Exclusion exclusion = null;
        PdfPCell ruleSubCell = null;
        if (null != genderAgeBMIListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
            ruleTable = new PdfPTable( ruleFloat);
            ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Gender/Age/BMI", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
             ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            
             
             for (int ruleSubItr = 0; ruleSubItr < genderAgeBMIListDecision.size(); ruleSubItr++) {
                 decision = (Decision) genderAgeBMIListDecision.get( ruleSubItr);
                 float[] ruleSubFloat = { 0.28f, 0.4f };
                 ruleSubTable = new PdfPTable( ruleSubFloat);
                 ruleTableSubCell = null;
                 if (null != decision.getProductName()
                         && !"".equalsIgnoreCase( decision.getProductName())
                         && decision.getProductName().length() > 1) {
                 	
                 	 ruleTableSubCell = new PdfPCell(
                              pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                 	 
                 	  pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       strBuff = new StringBuffer();
                       strBuff.append( decision.getDecision());
                       
                       
                       if(null != decision.getListRating() && decision.getListRating().size()>0){
                     	  for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                               rating = (Rating) decision.getListRating().get( ratingItr);
                               strBuff.append( "    Load: (");                              
                               strBuff.append( rating.getValue());
                               strBuff.append( ", ");
                               strBuff.append( ");\n");
                           }
                             
                       }
                       
                       /*if (null != decision.getTotalDebitsValue()
                               && decision.getTotalDebitsValue().length() > 0
                               && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
                      	 strBuff.append( "    Load: (");
                           strBuff.append(decision.getTotalDebitsValue());
                           strBuff.append( ", ");
                           strBuff.append( ");\n");
                       }
                       */
                       if (null != decision.getListExclusion() && decision.getListExclusion().size() > 0) {
                          
                       		strBuff.append( "    Excl: (");
                          
                           for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                               exclusion = (Exclusion) decision.getListExclusion().get( itr);
                               strBuff.append( exclusion.getName());
                               strBuff.append(", ");
                           }
                           strBuff.append(")");
                           
                       }
                       ruleTableSubCell = new PdfPCell(
                               pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                       pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       ruleSubCell.addElement( ruleSubTable);
                       
                 } 
             }
             
           /* for (int ruleSubItr = 0; ruleSubItr < genderAgeBMIListDecision.size(); ruleSubItr++) {
                decision = (Decision) genderAgeBMIListDecision.get( ruleSubItr);
                
                
                float[] ruleSubFloat = { 0.28f, 0.4f };
                ruleSubTable = new PdfPTable( ruleSubFloat);                
                if (null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName())
                        && decision.getProductName().length() > 0) {
                	 ruleTableSubCell = new PdfPCell(
                             pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                } else {
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                }
                pdfAPI.disableBorders( ruleTableSubCell);
                ruleSubTable.addCell( ruleTableSubCell);               
                strBuff.append( decision.getDecision());
                for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                    rating = (Rating) decision.getListRating().get( ratingItr);
                    strBuff.append( "with rating");
                    strBuff.append( rating.getValue());
                    strBuff.append( ",");
                }
                if (null != decision.getListExclusion()) {
                    if (decision.getListExclusion().size() > 0) {
                        strBuff.append( "Exclusion:");
                    }
                    for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                        exclusion = (Exclusion) decision.getListExclusion().get( itr);
                        strBuff.append( exclusion.getText());
                    }
                    
                }
                ruleTableSubCell = new PdfPCell(
                        pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                pdfAPI.disableBorders( ruleTableSubCell);
                ruleSubTable.addCell( ruleTableSubCell);
                ruleSubCell.addElement( ruleSubTable);
                strBuff = new StringBuffer();
            }*/
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }
        ruleTable = null;
        ruleCell = null;
        decision = null;
        ruleTableSubCell = null;
        ruleSubTable = null;
        strBuff =null;
        rating = null;
        exclusion = null;
        ruleSubCell = null;
        
    }
    
    public static void dispalyINGGenderAgeBMIDetails(List genderAgeBMIListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {

        final String METHOD_NAME = "dispalyGenderAgeBMIDetails"; //$NON-NLS-1$
       
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        Decision decision = null;
        PdfPCell ruleTableSubCell = null;
        PdfPTable ruleSubTable = null;
        StringBuffer strBuff = new StringBuffer();
        Rating rating = null;
        Exclusion exclusion = null;
        PdfPCell ruleSubCell = null;
        if (null != genderAgeBMIListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
            ruleTable = new PdfPTable( ruleFloat);
            ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Gender/Age/BMI", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
             ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            
             
             for (int ruleSubItr = 0; ruleSubItr < genderAgeBMIListDecision.size(); ruleSubItr++) {
                 decision = (Decision) genderAgeBMIListDecision.get( ruleSubItr);
                 float[] ruleSubFloat = { 0.28f, 0.4f };
                 ruleSubTable = new PdfPTable( ruleSubFloat);
                 ruleTableSubCell = null;
                 if (null != decision.getProductName()
                         && !"".equalsIgnoreCase( decision.getProductName())
                         && decision.getProductName().length() > 1) {
                 	
                 	 ruleTableSubCell = new PdfPCell(
                              pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                 	 
                 	  pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       strBuff = new StringBuffer();
                       strBuff.append( decision.getDecision());
                       
                       
                       if(null != decision.getListRating() && decision.getListRating().size()>0){
                     	  for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                               rating = (Rating) decision.getListRating().get( ratingItr);
                               strBuff.append( "    Load: (");                              
                               strBuff.append( rating.getValue());
                               strBuff.append( ", ");
                               strBuff.append( ");\n");
                           }
                             
                       }                      
                       if (null != decision.getListExclusion() && decision.getListExclusion().size() > 0) {
                          
                       		strBuff.append( "    Excl: (");
                          
                           for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                               exclusion = (Exclusion) decision.getListExclusion().get( itr);
                               strBuff.append( exclusion.getName());
                               strBuff.append(", ");
                           }
                           strBuff.append(")");
                           
                       }
                       ruleTableSubCell = new PdfPCell(
                               pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                       pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       ruleSubCell.addElement( ruleSubTable);
                       
                 } 
             }
             
         
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }
        ruleTable = null;
        ruleCell = null;
        decision = null;
        ruleTableSubCell = null;
        ruleSubTable = null;
        strBuff =null;
        rating = null;
        exclusion = null;
        ruleSubCell = null;
        
    }
    
	/**
     * Description: display rule decision details in the PDF Section
     * 
     * @param
     * java.lang.String,java.lang.String,,java.lang.String,,java.lang.String,
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    public static void dispalyRuleDecisionDetails(List ruleListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {

        final String METHOD_NAME = "dispalyRuleDecisionDetails"; //$NON-NLS-1$
       
        
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        PdfPCell ruleSubCell = null;
        Decision decision = null;
        PdfPCell ruleTableSubCell = null;  
        PdfPTable ruleSubTable = null;
        Rating rating = null;
        Exclusion exclusion = null;
        StringBuffer strBuff = new StringBuffer();
        if (null != ruleListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
             ruleTable = new PdfPTable( ruleFloat);
             ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Rules", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
            ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            
            
            
            for (int ruleSubItr = 0; ruleSubItr < ruleListDecision.size(); ruleSubItr++) {
                decision = (Decision) ruleListDecision.get( ruleSubItr);
                float[] ruleSubFloat = { 0.28f, 0.4f };

                if (null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName().trim())
                        && decision.getProductName().length() > 0) {
                	
                	ruleSubTable = new PdfPTable( ruleSubFloat);
                    ruleTableSubCell = null;
                    
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    strBuff = new StringBuffer();
                    strBuff.append( decision.getDecision());
                    if (null != decision.getTotalDebitsValue()
                            && decision.getTotalDebitsValue().length() > 0
                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
                   	 strBuff.append( "    Load: (");
                        strBuff.append(decision.getTotalDebitsValue());
                        strBuff.append( ", ");
                        strBuff.append( ");\n");
                    }
                    
                    if (null != decision.getListExclusion() && decision.getListExclusion().size()>0) {
                        if (decision.getListExclusion().size() > 0) {
                       	  strBuff.append( "    Excl: (");
                        }
                        
                        for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                           exclusion = (Exclusion) decision.getListExclusion().get( itr);
                            strBuff.append( exclusion.getName());
                            strBuff.append(", ");
                        }
                        strBuff.append(")");
                        
                    }
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph(strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    // ruleTableSubCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    ruleSubCell.addElement( ruleSubTable);
                  
                } 
                	
              
              
                
            }
            
           /* for (int ruleSubItr = 0; ruleSubItr < ruleListDecision.size(); ruleSubItr++) {
                decision = (Decision) ruleListDecision.get( ruleSubItr);
                
                if(null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName())
                        && decision.getProductName().length()> 0){
                    float[] ruleSubFloat = { 0.28f, 0.4f };
                    ruleSubTable = new PdfPTable( ruleSubFloat);                                      
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));                    
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                  
                    strBuff.append( decision.getDecision());
                    for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                        rating = (Rating) decision.getListRating().get( ratingItr);
                        strBuff.append( "with rating");
                        strBuff.append( rating.getValue());
                        strBuff.append( ",");
                    }
                    if (null != decision.getListExclusion()) {
                        if (decision.getListExclusion().size() > 0) {
                            strBuff.append( "Exclusion:");
                        }
                        
                        for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                            exclusion = (Exclusion) decision.getListExclusion().get( itr);
                            strBuff.append( exclusion.getText());
                        }
                        
                    }
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    // ruleTableSubCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    ruleSubCell.addElement( ruleSubTable);
                    strBuff = new StringBuffer();
                }
    
                
            }*/
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }
         ruleTable = null;
         ruleCell = null;
         ruleSubCell = null;
         decision = null;
         ruleTableSubCell = null;  
         ruleSubTable = null;
         rating = null;
         exclusion = null;
         strBuff = null;
        
    }
    
    public static void dispalyINGRuleDecisionDetails(List ruleListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {

        final String METHOD_NAME = "dispalyRuleDecisionDetails"; //$NON-NLS-1$
       
        
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        PdfPCell ruleSubCell = null;
        Decision decision = null;
        PdfPCell ruleTableSubCell = null;  
        PdfPTable ruleSubTable = null;
        Rating rating = null;
        Exclusion exclusion = null;
        StringBuffer strBuff = new StringBuffer();
        if (null != ruleListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
             ruleTable = new PdfPTable( ruleFloat);
             ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Rules", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
            ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            
            
            
            for (int ruleSubItr = 0; ruleSubItr < ruleListDecision.size(); ruleSubItr++) {
                decision = (Decision) ruleListDecision.get( ruleSubItr);
                float[] ruleSubFloat = { 0.28f, 0.4f };

                if (null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName().trim())
                        && decision.getProductName().length() > 0) {
                	
                	ruleSubTable = new PdfPTable( ruleSubFloat);
                    ruleTableSubCell = null;
                    
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    strBuff = new StringBuffer();
                    strBuff.append( decision.getDecision());
                    if (null != decision.getTotalDebitsValue()
                            && decision.getTotalDebitsValue().length() > 0
                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
                   	 strBuff.append( "    Load: (");
                        strBuff.append(decision.getTotalDebitsValue());
                        strBuff.append( ", ");
                        strBuff.append( ");\n");
                    }
                    
                    if (null != decision.getListExclusion() && decision.getListExclusion().size()>0) {
                        if (decision.getListExclusion().size() > 0) {
                       	  strBuff.append( "    Excl: (");
                        }
                        
                        for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                           exclusion = (Exclusion) decision.getListExclusion().get( itr);
                            strBuff.append( exclusion.getName());
                            strBuff.append(", ");
                        }
                        strBuff.append(")");
                        
                    }
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph(strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    // ruleTableSubCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    ruleSubCell.addElement( ruleSubTable);
                  
                }              
              
                
            }            
          
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }
         ruleTable = null;
         ruleCell = null;
         ruleSubCell = null;
         decision = null;
         ruleTableSubCell = null;  
         ruleSubTable = null;
         rating = null;
         exclusion = null;
         strBuff = null;
        
    }
    
    public static BigDecimal convertToBigDecimal(String bdValue){
    	BigDecimal returnBD=null;
    	try{
		   if(bdValue != null && !bdValue.trim().equalsIgnoreCase("")){
			   returnBD=new BigDecimal(bdValue);
		   }
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return returnBD;
    }

}
