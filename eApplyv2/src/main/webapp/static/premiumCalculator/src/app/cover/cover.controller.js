class CoverCtrl {
  constructor(AppConstants, CommonAPI, $scope, $state, ngDialog, $q, EapplyData, moment, ConfigData, $rootScope, $timeout, $sce) {
    'ngInject';
    
    this._AppConstants = AppConstants;
    this._CommonAPI = CommonAPI;
    this._$scope = $scope;
    this._$state = $state;
    this._ngDialog = ngDialog;
    this._$q = $q;
    this._EapplyData = EapplyData;
    this._MomentAPI = moment;
    this._$rootScope = $rootScope;
    this._ConfigData = ConfigData;
    this._$timeout = $timeout;  
    this._$sce = $sce; 
    
    this.calculationData = {};
    this.premiumOpts = ['Monthly', 'Yearly', 'Weekly'];
    this.modelOptions = {updateOn: "blur"};
    this.calculateFlag = false;
    this.insuredIp = false;
    this.disableIP = false;
    this.config = {};    
  }
  
  init(){
	 
	  this.fundID = inputData.partnerID;
	  this.urlList = this._CommonAPI.getUrlList();
	  this.industryList();
	  this.initiateCalender();
	  
	  this.appicantData = inputData.applicant[0];
	  this.appicantData.age = this._MomentAPI().diff(this._MomentAPI(this.appicantData.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years') + 1;
	  this._EapplyData.setApplicantRawXmlData(this.appicantData);
	  
	  this.calculationData = angular.copy(this._EapplyData.getApplicantData());
	  this.calculationData.partnerCode = this.fundID;
	  this.calculationData.applicant.cover[0] = this._EapplyData.getExistingDeathCover();
	  this.calculationData.applicant.cover[1] = this._EapplyData.getExistingTpdCover();
	  this.calculationData.applicant.cover[2] = this._EapplyData.getExistingIpCover();
	  
	  this.config = this._ConfigData.getPartnerBasedConfiguration(this.fundID);
	  
	  this._$rootScope.setPageTitle(this.config.partnerName);
	  
	  if(this.config.ipType == 'units'){
		  this.calculationData.applicant.cover[2].coverCategory = 'IpUnitised';
	  }else{
		  this.calculationData.applicant.cover[2].coverCategory = 'IpFixed';
	  }
	  
	  this.calculationData.applicant.cover[2].additionalIpWaitingPeriod = this.config.defaultWaitingperiod;
	  
	  this.calculationData.applicant.cover[0].frequencyCostType = this.config.defaultFrequency;
	  this.calculationData.applicant.cover[1].frequencyCostType = this.config.defaultFrequency;
	  this.calculationData.applicant.cover[2].frequencyCostType = this.config.defaultFrequency;
	  this.checkDOB();
  }
  
  initiateCalender(){
	  const _this = this;	  
	  $(document).ready(function(){
		    $('#dob').datepicker({
				showOn: 'both',
				/*buttonImage: _this._AppConstants.calIconPath,*/
				/*buttonImageOnly: false,*/
				defaultDate: '+1w',
				dateFormat: 'dd/mm/yy',
				setDate: '+0D',
				gotoCurrent: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true,
				autoSize: true,
				minDate: '01/01/1945',
				maxDate: '+0D',
				yearRange: '1945:2100',
				onSelect: function( selectedDate ) {
					$(this).trigger('change');
				}
			});
	  });
  }
  
  changeFrequency(){
	  this.calculationData.applicant.cover[1].frequencyCostType = this.calculationData.applicant.cover[0].frequencyCostType;
	  this.calculationData.applicant.cover[2].frequencyCostType = this.calculationData.applicant.cover[0].frequencyCostType;
	  
	  this.checkCalculation();
  }
  
  resetValues(){
	  this.deathMaxErrorFlag = false;
	  this.tpdMaxErrorFlag = false;
	  
	  this.calculationData.applicant.cover[0].additionalCoverAmount = '';
	  this.calculationData.applicant.cover[1].additionalCoverAmount = '';
	  
	  this.calculationData.applicant.cover[0].additionalUnit = '';
	  this.calculationData.applicant.cover[1].additionalUnit = '';
	  
  }
  
  checkDOB(){
	  
	  this.deathRestrict = false;
	  this.tpdRestrict = false;
	  this.ipRestrict = false;
	  this.invalidDOB = false;
	  
	  this.calculationData.applicant.age = this._MomentAPI().diff(this._MomentAPI(this.calculationData.applicant.birthDate, 'DD-MM-YYYY'), 'years') + 1;
	  console.log("Age: "+this.calculationData.applicant.age);
	  
	  if(isNaN(this.calculationData.applicant.age)){
		  this.invalidDOB = true;
		  //this.calculationData.applicant.age = 0;
	  }
	  
	  //death min Check
	  if(parseInt(this.calculationData.applicant.age) < parseInt(this.config.deathMinAge)){
		  this.deathRestrict = true;
		  this.deathMaxErrorFlag = false;
		  this.calculationData.applicant.cover[0].additionalCoverAmount = '';
		  this.calculationData.applicant.cover[0].additionalUnit = '';
	  }
	  
	//TPD min Check
	  if(parseInt(this.calculationData.applicant.age) < parseInt(this.config.tpdMinAge)){
		  this.tpdRestrict = true;
		  this.tpdMaxErrorFlag = false;
		  this.tpdGreaterThanDeathErrorFlag = false;
		  this.calculationData.applicant.cover[1].additionalCoverAmount = '';
		  this.calculationData.applicant.cover[1].additionalUnit = '';
	  }
	  
	//IP min Check
	  if(parseInt(this.calculationData.applicant.age) < parseInt(this.config.ipMinAge)){
		  this.ipRestrict = true;
		  this.insuredIp = false;
		  this.calculationData.applicant.cover[2].additionalCoverAmount = '';
		  this.calculationData.applicant.cover[2].additionalUnit = '';
	  }
	  
	//death max Check
	  if(parseInt(this.calculationData.applicant.age) > parseInt(this.config.deathMaxAge)){
		  this.deathRestrict = true;
		  this.deathMaxErrorFlag = false;
		  this.calculationData.applicant.cover[0].additionalCoverAmount = '';
		  this.calculationData.applicant.cover[0].additionalUnit = '';
	  }
	  
	//TPD max Check
	  if(parseInt(this.calculationData.applicant.age) > parseInt(this.config.tpdMaxAge)){
		  this.tpdRestrict = true;
		  this.tpdMaxErrorFlag = false;
		  this.tpdGreaterThanDeathErrorFlag = false;
		  this.calculationData.applicant.cover[1].additionalCoverAmount = '';
		  this.calculationData.applicant.cover[1].additionalUnit = '';
	  }
	  
	//IP max Check
	  if(parseInt(this.calculationData.applicant.age) > parseInt(this.config.ipMaxAge)){
		  this.ipRestrict = true;
		  this.insuredIp = false;
		  this.calculationData.applicant.cover[2].additionalCoverAmount = '';
		  this.calculationData.applicant.cover[2].additionalUnit = '';
	  }  
	  
	  console.log(this.deathRestrict , this.tpdRestrict ,this.ipRestrict);
	  this.checkCalculation();  
	  
  }
  
  checkWorkHrs(questionName, answer){
	  
	  if(questionName == 'workHours' && answer == 'No'){
		  this.disableIP = true;
		  this.calculationData.applicant.cover[2].additionalCoverAmount = '0';
		  this.calculationData.applicant.cover[2].additionalUnit = '0';
	  }else if(questionName == 'workHours' && answer == 'Yes'){
		  this.disableIP = false; 
	  }
	  this.checkCalculation();  
  }
  
  validateDeathAmount(){
	  
	  this.deathMaxErrorFlag = false;
	  this.tpdMaxErrorFlag = false;
	  this.tpdGreaterThanDeathErrorFlag = false;
	  
	  if(this.calculationData.applicant.cover[0].coverCategory == 'DcFixed'){
		  
		  if(this.calculationData.applicant.cover[0].additionalCoverAmount > this.config.deathMaxAmount){
			  this.deathMaxErrorFlag = true;
		  }
		  
		  if(this.config.tpdLessThanDeath){
			  if(parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount) > parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount)){
				  this.tpdGreaterThanDeathErrorFlag = true;  
			  }else{
				  this.tpdGreaterThanDeathErrorFlag = false; 
			  }
		  }else{
			  this.tpdGreaterThanDeathErrorFlag = false;
		  }
		  
		  if(!this.deathMaxErrorFlag && !this.tpdGreaterThanDeathErrorFlag){
			  this.calculationData.applicant.cover[0].additionalCoverAmount = parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount);
			  this.checkCalculation();
		  } 
	  }else{
		  
		  this.calculatePerUnit().then((res) => {
			  console.log(this.deathPerUnit, this.tpdPerUnit, this.ipPerUnit);
			  
			  var calculatedAmount = parseInt(this.calculationData.applicant.cover[0].additionalUnit)*parseInt(this.deathPerUnit);
			  
			  this.deathMaxAllowed = Math.floor(parseInt(this.config.deathMaxAmount)/parseInt(this.deathPerUnit));
			  
			  if(calculatedAmount > this.config.deathMaxAmount){
				  this.deathMaxErrorFlag = true;
			  }
			  
			  if(this.config.tpdLessThanDeath){
				  if(parseInt(this.calculationData.applicant.cover[1].additionalUnit) > parseInt(this.calculationData.applicant.cover[0].additionalUnit)){
					  this.tpdGreaterThanDeathErrorFlag = true;  
				  }else{
					  this.tpdGreaterThanDeathErrorFlag = false; 
				  }
			  }else{
				  this.tpdGreaterThanDeathErrorFlag = false;
			  }
			  
			  if(!this.deathMaxErrorFlag && !this.tpdGreaterThanDeathErrorFlag){
				  this.checkCalculation();
			  }
			  
		  });
	  
	  }
	  
	  
  }
  
  validateTPDAmount(){
	  
	  this.tpdMaxErrorFlag = false;
	  this.tpdGreaterThanDeathErrorFlag = false;
	  
	  if(this.calculationData.applicant.cover[1].coverCategory == 'TPDFixed'){
		  
		  if(this.calculationData.applicant.cover[1].additionalCoverAmount > this.config.tpdMaxAmount){
			  this.tpdMaxErrorFlag = true;
		  }
		  
		  if(this.config.tpdLessThanDeath){
			  if(parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount) > parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount)){
				  this.tpdGreaterThanDeathErrorFlag = true;  
			  }else{
				  this.tpdGreaterThanDeathErrorFlag = false; 
			  }
		  }else{
			  this.tpdGreaterThanDeathErrorFlag = false;
		  }
		  
		  if(!this.tpdMaxErrorFlag && !this.tpdGreaterThanDeathErrorFlag){
			  this.calculationData.applicant.cover[1].additionalCoverAmount = parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount);
			  this.checkCalculation();
		  } 
	  }else{
		  this.calculatePerUnit().then((res) => {
			  console.log(this.deathPerUnit, this.tpdPerUnit, this.ipPerUnit);
			  
			  var calculatedAmount = parseInt(this.calculationData.applicant.cover[1].additionalUnit)*parseInt(this.tpdPerUnit);
			  
			  this.tpdMaxAllowed = Math.floor(parseInt(this.config.tpdMaxAmount)/parseInt(this.tpdPerUnit));
			  
			  if(calculatedAmount > this.config.tpdMaxAmount){
				  this.tpdMaxErrorFlag = true;
			  }
			  
			  if(this.config.tpdLessThanDeath){
				  if(parseInt(this.calculationData.applicant.cover[1].additionalUnit) > parseInt(this.calculationData.applicant.cover[0].additionalUnit)){
					  this.tpdGreaterThanDeathErrorFlag = true;  
				  }else{
					  this.tpdGreaterThanDeathErrorFlag = false; 
				  }
			  }else{
				  this.tpdGreaterThanDeathErrorFlag = false;
			  }
			  
			  if(!this.tpdMaxErrorFlag && !this.tpdGreaterThanDeathErrorFlag){
				  this.checkCalculation();
			  }
			  
		  });
	  }	  
	  
  }
  
  validateIpAmount(){
	  
	  this.ipRoundedFlag = false;
	  
	  if(this.calculationData.applicant.cover[2].coverCategory == 'IpFixed'){
		  
		  var calculatedAmount = Math.round((this.config.insuredIpValue/100) * (this.calculationData.applicant.annualSalary / 12));
		  
		  var allowedAmount;
		  
		  if(this.config.ipMaxAmount > calculatedAmount){
			  allowedAmount = calculatedAmount;
		  }else{
			  allowedAmount = this.config.ipMaxAmount;
		  }
		  
		  if(this.calculationData.applicant.cover[2].additionalCoverAmount > allowedAmount){
			  this.ipRoundedFlag = true;
			  this.calculationData.applicant.cover[2].additionalCoverAmount  = allowedAmount;
		  }
		  
		  this.checkCalculation();
		  
	  }else{
		  
		  this.calculatePerUnit().then((res) => {
			  
			  console.log(this.deathPerUnit, this.tpdPerUnit, this.ipPerUnit);
			  
			  var calculatedAmount = parseFloat((this.config.insuredIpValue/100) * (this.calculationData.applicant.annualSalary / 12)).toFixed(2);
			  
			  var allowedUnitBasedOnSalary = Math.ceil(parseInt(calculatedAmount)/parseInt(this.ipPerUnit));
			  
			  var allowedUnit;
			  
			  if(this.config.ipMaxUnit > allowedUnitBasedOnSalary){
				  allowedUnit = allowedUnitBasedOnSalary;
			  }else{
				  allowedUnit = this.config.ipMaxUnit;
			  }
			  
			  if(this.calculationData.applicant.cover[2].additionalUnit > allowedUnit){
				  this.ipRoundedFlag = true;
				  this.calculationData.applicant.cover[2].additionalUnit  = allowedUnit;
			  }
			  
			  this.checkCalculation();
		  });
	  }
	  
  }
  
  calculateIPInsured(){
	  
	  if(this.insuredIp && parseInt(this.calculationData.applicant.annualSalary) > 0){
		  
		  if(this.config.ipType == 'fixed'){ //Ip Fixed
			  
			  var calculatedAmount = Math.round((this.config.insuredIpValue/100) * (this.calculationData.applicant.annualSalary / 12));
			  
			  if(calculatedAmount < this.config.ipMaxAmount){
				  this.calculationData.applicant.cover[2].additionalCoverAmount  = calculatedAmount;
			  }else{				  
				  this.calculationData.applicant.cover[2].additionalCoverAmount = this.config.ipMaxAmount;
			  }
			  
		}else{ //IP Unitised
			
			this.calculatePerUnit().then((res) => {
				
				  console.log(this.deathPerUnit, this.tpdPerUnit, this.ipPerUnit);
				  
				  var calculatedAmount = parseFloat((this.config.insuredIpValue/100) * (this.calculationData.applicant.annualSalary / 12)).toFixed(2);				  						
				  
				  if(calculatedAmount > this.config.ipMaxAmount){
					  this.calculationData.applicant.cover[2].additionalUnit  = Math.ceil(parseInt(this.config.ipMaxAmount)/parseInt(this.ipPerUnit));
				  }else{
					  this.calculationData.applicant.cover[2].additionalUnit  = Math.ceil(parseInt(calculatedAmount)/parseInt(this.ipPerUnit));
				  }
				  
			  });			  
		}
		 
		  this.checkCalculation();
	  }else{
		  this.validateIpAmount();
	  }
	  
  }
  
  setOccupationRating(){
	  
	  switch(this.fundID) {
	  	case 'CARE':
	  		this.setOccupationRatingCARE();
	  		break;
	  	case 'SFPS':
	  		this.setOccupationRatingCommon().then((res) => {
	  			this.setOccupationRatingSFPS();
	  		});
	  		break;
	  	case 'HOST':
	  		this.setOccupationRatingCommon().then((res) => {
	  			this.setOccupationRatingHOST();
	  		});
	  		break;
	  	/*case 'VICT':
	  		this.setOccupationRatingCommon().then((res) => {
	  			this.checkCalculation();
	  		});
	  		break;*/
	  	case 'FIRS':
	  		this.setOccupationRatingCommon().then((res) => {
	  			this.checkCalculationFIRS();
	  		});
	  		break;
	  	case 'GUIL':
	  		this.setOccupationRatingCommon().then((res) => {
	  			this.setOccupationRatingGUIL();
	  		});
	  		break;
	  	default:
	  		break;
    }
  }
  
  setOccupationRatingCommon(){
	  
	  let deferred = this._$q.defer();
	 
	  if(this.calculationData.applicant.occupation !== undefined){
		  
		  var occName = this.calculationData.applicant.industryType + ':' + this.calculationData.applicant.occupation;
		  
		  this._CommonAPI.getOccupationRating(this.urlList.newOccupationUrl, this.fundID , occName).then(
				  (res) => {
					  this.calculationData.applicant.cover[0].occupationRating = res[0].deathfixedcategeory;
					  this.calculationData.applicant.cover[1].occupationRating = res[0].tpdfixedcategeory;
					  this.calculationData.applicant.cover[2].occupationRating = res[0].ipfixedcategeory;
					  
					  this.deathDBOccCategory =  res[0].deathfixedcategeory;
					  this.tpdDBOccCategory =  res[0].tpdfixedcategeory;
					  this.ipDBOccCategory =   res[0].ipfixedcategeory;	
					  
					  deferred.resolve({});
				  },
			      (err) => {
			          this.errors = err.data.errors;
			          deferred.reject(err);
			        }
		  );
	   }
	  
	  return deferred.promise;
  }
  
  setOccupationRatingCARE(){
	  
	  this.annualSalForUpgradeVal = 100000;
	  
	  if(this.config.finalOccupationQuestions[0].answerText == "Yes"){
		  
		  this.calculationData.applicant.cover[0].occupationRating = 'Office';
		  this.calculationData.applicant.cover[1].occupationRating = 'Office';
		  this.calculationData.applicant.cover[2].occupationRating = 'Office';
		  
		  if(parseFloat(this.calculationData.applicant.annualSalary) >= parseFloat(this.annualSalForUpgradeVal) && (this.config.finalOccupationQuestions[1].answerText == "Yes" || this.config.finalOccupationQuestions[2].answerText == "Yes")){
			  this.calculationData.applicant.cover[0].occupationRating = 'Professional';
			  this.calculationData.applicant.cover[1].occupationRating = 'Professional';
			  this.calculationData.applicant.cover[2].occupationRating = 'Professional';
		  } 
		  
	  }else{
		  this.calculationData.applicant.cover[0].occupationRating = 'General';
		  this.calculationData.applicant.cover[1].occupationRating = 'General';
		  this.calculationData.applicant.cover[2].occupationRating = 'General';
	  }
	  
	  this.calculateIPInsured();
	  
  }
  
  checkCalculationFIRS(){
	  
	  const _this = this;
	  
	  _this.annualSalForUpgradeVal = 125000;
	  
	  if(_this.calculationData.applicant.occupation){
		  
		  var selectedOcc = _this.occupationList.filter(function(obj){
	          return obj.occupationName == _this.calculationData.applicant.occupation;
	       });
		  
		  var selectedOccObj = selectedOcc[0];
		  
		  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = true; // occupationDuties	
			  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = false; // spendTimeInside	
			 
			  
			  _this.config.finalOccupationQuestions[2].answerText = ''; 
			  
		  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // occupationDuties	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside	
			  
			  if(_this.config.finalOccupationQuestions[2].answerText == 'Yes'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // occupationDuties	
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
			  			  
			  }else{
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // occupationDuties	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';			  
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // occupationDuties	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside	
			 
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = false; // workDuties	
			  _this.config.finalOccupationQuestions[3].showQuestion = false; // spendTimeOutside
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  _this.config.finalOccupationQuestions[2].answerText = ''; 
		  }
		  
		  if(_this.config.finalOccupationQuestions[2].answerText == 'Yes' && _this.deathDBOccCategory == 'Standard'){
			  _this.calculationData.applicant.cover[0].occupationRating = 'Low Risk';
			  _this.calculationData.applicant.cover[1].occupationRating = 'Low Risk';
			  _this.calculationData.applicant.cover[2].occupationRating = 'Low Risk';
		  }
		  
		  if(_this.deathDBOccCategory.toLowerCase() == 'low risk' && _this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
			  
			  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
			  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
			  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
		  }
		  
		 this.calculateIPInsured(); 
	  }
  }
  
  setOccupationRatingGUIL(){
	  
	  const _this = this;
	  
	  _this.annualSalForUpgradeVal = 100000;
	  
	  if(_this.calculationData.applicant.occupation){
		  
		  var selectedOcc = _this.occupationList.filter(function(obj){
	          return obj.occupationName == _this.calculationData.applicant.occupation;
	       });
		  
		  var selectedOccObj = selectedOcc[0];
		  
		  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = true; // tertiaryQue
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // occupationDuties
			  
			  _this.config.finalOccupationQuestions[1].answerText = ''; 
			  
			  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
			  }else{
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
			  }
			  
		  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[1].showQuestion = true; // occupationDuties	
			  
			  if(_this.config.finalOccupationQuestions[1].answerText == 'No'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // tertiaryQue	
			  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';					  
				  }else{
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';					  
				  }				  
			  }else{
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	  
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 			
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[1].showQuestion = true; // occupationDuties
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  
			  if(_this.config.finalOccupationQuestions[1].answerText == 'No'){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
				  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
				  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';
			  }else{			
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // occupationDuties	
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  
			  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
			  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
			  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
		  }
		  
		  if(_this.config.initialOccupationQuestions[0].answerText == 'No'){
			  
			  switch(_this.deathDBOccCategory) {
			  
				  case 'White Collar':				  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
					  break;
					  
				  case 'Professional':					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';
					  break;
				
				  default:
					  console.log('Default');
					  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
					  break;
			  }
			  
		  }
		  
		 this.calculateIPInsured(); 
	  }
  }
  
  setOccupationRatingSFPS(){
	  
	  const _this = this;
	  
	  _this.annualSalForUpgradeVal = 100000;
	  
	  if(_this.calculationData.applicant.occupation){
		  
		  var selectedOcc = _this.occupationList.filter(function(obj){
	          return obj.occupationName == _this.calculationData.applicant.occupation;
	       });
		  
		  var selectedOccObj = selectedOcc[0];
		  
		  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = true; // spendTimeInside	
			  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = false; // workDuties	
			  _this.config.finalOccupationQuestions[3].showQuestion = false; // spendTimeOutside
			  
			  _this.config.finalOccupationQuestions[2].answerText = ''; 
			  _this.config.finalOccupationQuestions[3].answerText = '';
			  
			  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
			  }else{
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
			  }
			  
		  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // workDuties	
			  _this.config.finalOccupationQuestions[3].showQuestion = true; // spendTimeOutside
			  
			  if(_this.config.finalOccupationQuestions[2].answerText == 'No' && _this.config.finalOccupationQuestions[3].answerText == 'No'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
			  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';					  
				  }else{
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';					  
				  }				  
			  }else{
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // workDuties	
			  _this.config.finalOccupationQuestions[3].showQuestion = true; // spendTimeOutside
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  
			  if(_this.config.finalOccupationQuestions[2].answerText == 'No' && _this.config.finalOccupationQuestions[3].answerText == 'No'){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
				  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
				  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';
			  }else{			
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = false; // workDuties	
			  _this.config.finalOccupationQuestions[3].showQuestion = false; // spendTimeOutside
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  _this.config.finalOccupationQuestions[2].answerText = ''; 
			  _this.config.finalOccupationQuestions[3].answerText = '';
			  
			  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
			  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
			  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
		  }
		  
		  if(_this.config.initialOccupationQuestions[0].answerText == 'No'){
			  
			  switch(_this.deathDBOccCategory) {
			  
				  case 'White Collar':				  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
					  break;
					  
				  case 'Professional':					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';
					  break;
				
				  default:
					  console.log('Default');
					  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
					  break;
			  }
			  
		  }
		  
		 this.calculateIPInsured(); 
	  }
  }
  
  setOccupationRatingHOST(){
	  
	  const _this = this;
	  
	  _this.annualSalForUpgradeVal = 150000;
	  
	  if(_this.calculationData.applicant.occupation){
		  
		  var selectedOcc = _this.occupationList.filter(function(obj){
	          return obj.occupationName == _this.calculationData.applicant.occupation;
	       });
		  
		  var selectedOccObj = selectedOcc[0];
		  
		  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // workDuties
			  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside		
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  
			  if(_this.config.finalOccupationQuestions[1].answerText == 'Yes' && _this.config.finalOccupationQuestions[2].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
			  }else{
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
			  }
			  
		  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = true; // workDuties
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside			  
			  
			  if(_this.config.finalOccupationQuestions[0].answerText == 'No' && _this.config.finalOccupationQuestions[2].answerText == 'Yes'){
				  
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
			  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && _this.calculationData.applicant.annualSalary && parseFloat(_this.calculationData.applicant.annualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';					  
				  }else{
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';					  
				  }				  
			  }else{
				  
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue
				  
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = true; // workDuties
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside	
			  
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  
			  if(_this.config.finalOccupationQuestions[0].answerText == 'No' && _this.config.finalOccupationQuestions[2].answerText == 'Yes'){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
				  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
				  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';
			  }else{			
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
			  }
			  
		  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
			  
			  _this.config.finalOccupationQuestions[0].showQuestion = false; // workDuties	
			  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
			  _this.config.finalOccupationQuestions[2].showQuestion = false; // spendTimeInside	
			  
			  _this.config.finalOccupationQuestions[0].answerText = ''; 
			  _this.config.finalOccupationQuestions[1].answerText = '';
			  _this.config.finalOccupationQuestions[2].answerText = ''; 
			  
			  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
			  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
			  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
		  }
		  
		  if(_this.config.initialOccupationQuestions[0].answerText == 'No'){
			  
			  switch(_this.deathDBOccCategory) {
			  
				  case 'White Collar':				  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
					  break;
					  
				  case 'Professional':					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';
					  break;
				
				  default:
					  console.log('Default');
					  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
					  break;
			  }
			  
		  }
		  
		 this.calculateIPInsured(); 
	  }
  }
  
  industryList() {
	  
	  this._CommonAPI.fetchIndustryList(this.urlList.quoteUrl, this.fundID).then(
	      (res) => {
	    	this._CommonAPI.setIndustryList(res);
	        this.IndustryOptions = res;
	      },
	      (err) => {
	        console.log(err.data.errors);
	      });
  }
  
  getOccupations() {
	  this.calculationData.applicant.occupation = '';
	  this.calculationData.applicant.otherOccupation = '';
	  this.calculationData.applicant.industryType = this.calculationData.applicant.industryType === '' ? null : this.calculationData.applicant.industryType;
	  
	  this._CommonAPI.fetchOccupationList(this.urlList.occupationUrl, this.calculationData.applicant.industryType, this.fundID).then(
	      (res) => {
	        this.occupationList = res;
	      },
	      (err) => {
	        this.errors = err.data.errors;
	      }
	  );
  }
  
  checkCalculation(){
	  if(this.calculateFlag && this._$scope.userDetailsForm.$valid && this._$scope.coverForm.$valid){
		  this.calculate();
	  }
  }
  
  calculate(){
	  console.log('Calculation goes here');
	  
	  let calculateJSON = {
			  'age': this.calculationData.applicant.age,
		      'fundCode': this.fundID,
		      'gender': this.calculationData.applicant.gender,
		      'deathOccCategory': this.calculationData.applicant.cover[0].occupationRating,
			  'tpdOccCategory': this.calculationData.applicant.cover[1].occupationRating,
			  'ipOccCategory': this.calculationData.applicant.cover[2].occupationRating,
		      'smoker': false,
		      'deathUnits': parseInt(this.calculationData.applicant.cover[0].additionalUnit),
		      'deathFixedAmount': parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount),
		      'tpdUnits': parseInt(this.calculationData.applicant.cover[1].additionalUnit),
		      'tpdFixedAmount': parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount),
		      'ipFixedAmount': parseInt(this.calculationData.applicant.cover[2].additionalCoverAmount),
		      'ipUnits': parseInt(this.calculationData.applicant.cover[2].additionalUnit),
		      'premiumFrequency': this.calculationData.applicant.cover[0].frequencyCostType,
		      'memberType': '',
		      'manageType': 'CCOVER',
		      'deathCoverType': this.calculationData.applicant.cover[0].coverCategory,
		      'tpdCoverType': this.calculationData.applicant.cover[1].coverCategory,
		      'ipCoverType': this.calculationData.applicant.cover[2].coverCategory,
		      'ipBenefitPeriod': this.calculationData.applicant.cover[2].additionalIpBenefitPeriod,
		      'ipWaitingPeriod': this.calculationData.applicant.cover[2].additionalIpWaitingPeriod,
		      'deathExistingAmount':0,
		      'tpdExistingAmount':0
		    };
	  
	  console.log(JSON.stringify(calculateJSON));
	    
	    this._CommonAPI.calculateAll(this.urlList.calculateUrl, calculateJSON).then(
	      (res) => {
	    	  
	    	  var premium = res;	    	  
	    	  
	    	  for(var i = 0; i < premium.length; i++){
				   if(premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised'){
					   this.calculationData.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
					   this.calculationData.applicant.cover[0].cost = premium[i].cost || 0;
				   }else if(premium[i].coverType === 'TPDFixed' || premium[i].coverType === 'TPDUnitised'){
					   this.calculationData.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
					   this.calculationData.applicant.cover[1].cost = premium[i].cost || 0;
				   }else if(premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised'){
					   this.calculationData.applicant.cover[2].additionalCoverAmount = premium[i].coverAmount||0.00;
					   this.calculationData.applicant.cover[2].cost = premium[i].cost || 0;
				   }
				}
	    	  
	    	  this.calculationData.totalMonthlyPremium = parseFloat(this.calculationData.applicant.cover[0].cost) + parseFloat(this.calculationData.applicant.cover[1].cost) + parseFloat(this.calculationData.applicant.cover[2].cost);
			   
	    	  console.log(this.calculationData.applicant.cover[0].additionalCoverAmount, this.calculationData.applicant.cover[1].additionalCoverAmount, this.calculationData.applicant.cover[2].additionalCoverAmount);
			  console.log(this.calculationData.applicant.cover[0].cost, this.calculationData.applicant.cover[1].cost, this.calculationData.applicant.cover[2].cost);
	        
	      },
	      (err) => {
	        this.errors = err.data.errors;
	      }
	    );
  }
  
  calculatePerUnit(){
	  
	  let deferred = this._$q.defer();

	  let calculateJSON = {
			  'age': this.calculationData.applicant.age,
		      'fundCode': this.fundID,
		      'gender': this.calculationData.applicant.gender,
		      'deathOccCategory': this.calculationData.applicant.cover[0].occupationRating,
			  'tpdOccCategory': this.calculationData.applicant.cover[1].occupationRating,
			  'ipOccCategory': this.calculationData.applicant.cover[2].occupationRating,
		      'deathUnits': 1,
		      'tpdUnits': 1,
		      'ipUnits': 1,
		      'premiumFrequency': this.calculationData.applicant.cover[0].frequencyCostType,
		      'manageType': 'CCOVER',
		      'deathCoverType': 'DcUnitised',
		      'tpdCoverType': 'TPDUnitised',
		      'ipCoverType': 'IpUnitised',
		      };
	  
	  console.log(JSON.stringify(calculateJSON));
	    
	    this._CommonAPI.calculateAll(this.urlList.calculateUrl, calculateJSON).then(
	      (res) => {
	    	  
	    	  var premium = res;	    	  
	    	  
	    	  for(var i = 0; i < premium.length; i++){
				   if(premium[i].coverType === 'DcUnitised'){
					   this.deathPerUnit = premium[i].coverAmount;
				   }else if(premium[i].coverType === 'TPDUnitised'){
					   this.tpdPerUnit = premium[i].coverAmount;
				   }else if(premium[i].coverType === 'IpUnitised'){
					   this.ipPerUnit = premium[i].coverAmount;
				   }
				}
	    	  
	    	  deferred.resolve({});
	      },
	      (err) => {
	        this.errors = err.data.errors;
	        deferred.reject(err);
	      }
	    );
	    
	    return deferred.promise;  
  }
  
  print(){
	  
	  const _this = this;
	  
	  var initialOccupationQuestions = [];
	  angular.forEach(_this.config.initialOccupationQuestions, function (obj, index) {
		  if(obj.answerText == 'Yes' || obj.answerText == 'No'){
			  var currentObj = {};
			  currentObj.answerText = obj.answerText;
			  currentObj.questiontext = obj.labelText.toString();
			  initialOccupationQuestions.push(currentObj);
		  }
		  
	  });
	  
	  var finalOccupationQuestions = [];
	  angular.forEach(_this.config.finalOccupationQuestions, function (obj, index) {
		  if(obj.answerText == 'Yes' || obj.answerText == 'No'){
			  var currentObj = {};
			  currentObj.answerText = obj.answerText;
			  currentObj.questiontext = obj.labelText.toString();
			  finalOccupationQuestions.push(currentObj);
		  }
	  });
	  
	  _this.calculationData.initialOccupationQuestions = initialOccupationQuestions;
	  _this.calculationData.finalOccupationQuestions = finalOccupationQuestions;
	  
	  //occupation code
	  var industryType = _this.calculationData.applicant.industryType;
	  
	  var selectedIndustry = _this.IndustryOptions.filter(function(obj){
          return  industryType === obj.key;
      });
	  
	  _this.calculationData.applicant.occupationCode = selectedIndustry[0].value;
	  
	  //insured IP
	  
	  if(_this.insuredIp){
		  _this.calculationData.applicant.insuredSalary = _this.config.insuredIpValue;
	  }else{
		  _this.calculationData.applicant.insuredSalary = '';
	  }
	  
	  //disclaimer Text
	  
	  _this.calculationData.disclaimerText = _this.config.disclaimerText.toString();
	  
	  console.log(JSON.stringify(_this.calculationData));
	  
	  _this._CommonAPI.printEapply(this.urlList.printQuotePageNew, _this.calculationData).then(
		      (res) => {
		    	  _this.downloadPDF(res.clientPDFLocation);
		    	  console.log(res);
		      },
		      (err) => {
		    	  _this.errors = err.data.errors;
		      }
	   );
  }
  
  downloadPDF(pdfLocation){
	  	var filename = null;
	  	var a = null;
	  	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  	a = document.createElement('a');
	  	document.body.appendChild(a);
	  	
	  	this._CommonAPI.downloadPDF(this.urlList.downloadUrl, pdfLocation).then(
				  (res) => {
					  if(navigator.appVersion.toString().indexOf('.NET') > 0) {
						  window.navigator.msSaveBlob(res.response,filename);
					  }else{
						  var fileURL = URL.createObjectURL(res.response);
						  a.href = fileURL;
						  a.download = filename;
						  a.click();
					  }
				  }, 
	            (err) => {
	              console.log('Error while Downloading the PDF' + err);
	            }
		  );
	  	
	}
  
  clickToOpen(hhText) {
	  this._ngDialog.open({
	      template: '<div class="p-5"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel">PREMIUM CALCULATOR - DISCLAIMER</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn submit-button text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
	      className: 'ngdialog-theme-plain',
	      showClose: false,
	      plain: true
	    });
  }
  
  
  checkPreviousMandatoryFields(currentField, formName){
	  
	  const _this = this;
	  
	  var currentFormFields = [];
	  
	  if(formName == 'coverForm'){
		  _this._$scope.userDetailsForm.$submitted = true;
	  }
	  
	  $("#"+formName).each(function(){
		  var allElements = $(this).find(':input');
		  for(var k = 0; k < allElements.length ; k++){
			  if(allElements[k].nodeName == 'INPUT' || allElements[k].nodeName == 'SELECT'){
			    	var fieldName = allElements[k].name;
			    	if(currentFormFields.indexOf(fieldName) == -1){
			    		currentFormFields.push(fieldName);
			    	}
			    }
		  }
	  });
	  
	  //console.log(currentFormFields);
	  
	  _this._$timeout(function(){
	  
		  var inx = currentFormFields.indexOf(currentField);
		  
		  if(inx > 0){		  
			  for(var i = 0; i < inx ; i++){
				  if(_this._$scope[formName][currentFormFields[i]]){
					  _this._$scope[formName][currentFormFields[i]].$touched = true;  
				  }			  
			  }		  
		  }
	  },100);
  }
  
}

export default CoverCtrl;
