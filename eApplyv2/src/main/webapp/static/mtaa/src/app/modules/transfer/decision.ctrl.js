/* Transfer Decision Controller,Progressive and Mandatory validations Starts  */

(function(angular){
	'use strict';
    /* global channel, inputData, token, moment */
    /**
     * @ngdoc controller
     * @name MtaaApp.controller:transferDecisionController
     *
     * @description
     * This is quotetransfer description
     */
	angular
	.module('MtaaApp')
	.controller('transferDecision', transferDecision);
	transferDecision.$inject=['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService','$stateParams'];
	function transferDecision($scope,$rootScope,$location,PersistenceService,DownloadPDFService,ngDialog,$timeout,$window,urlService,$stateParams){
		
		var vm=this;
		
		vm.urlList = urlService.getUrlList();
		var pdfLocation = PersistenceService.getPDFLocation();
		var npsTokenUrl = PersistenceService.getNpsUrl();
		
		if($stateParams.mode === 'accept'){
			vm.transferDecision = 'Accepted';
		}else{
			vm.transferDecision = 'Declined';
		}
		
		vm.go = function(path) {
			$location.path(path);
	  	};
	  	
	  	vm.appNum = PersistenceService.getAppNumber();
	  	vm.navigateToLandingPage = function (){
	  		
	  		var templateContent = '<div class="ngdialog-content"><div class="modal-body">';
	  		templateContent += '<div class="row rowcustom"><div class="col-sm-8">';
	  		templateContent += '<p> Are you sure you want to navigate to Home Page?</p>';
	  		templateContent += '</div></div></div></br>';
	  		templateContent += '<div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter">';
	  		templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button>';
	  		templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
	  		templateContent += '</div></div>';
	  		
	    	ngDialog.openConfirm({
	            template: templateContent,
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
	    };
	    history.pushState(null, null, location.href);
	    window.onpopstate = function () {
	        history.go(1);
	    };
	    var templateContent = '<div class="ngdialog-content"><div class="modal-header">';
	    templateContent += '<h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div>';
	    templateContent += '<div class="modal-body"><div class="row rowcustom"><div class="col-sm-12">';
	    templateContent += '<p class="aligncenter">  </p>';
	    templateContent += '<div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>';
	    templateContent += '<p></p></div></div></div>';
	    templateContent += '<div class="ngdialog-buttons aligncenter">';
	    templateContent += '<button type="button" class="btn btn-primary no-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button>';
	    templateContent += '<button type="button" class="btn btn-primary no-arrow" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button>';
	    templateContent += ' </div></div>';
	    	
	    ngDialog.openConfirm({
	   		template: templateContent,
	 		className: 'ngdialog-theme-plain custom-width',
	 		plain: true,
	   	}).then(function (value) {
	   		if(value==='onYes'){
	  			if(npsTokenUrl !== null){
	  			   // var url = ""+npsTokenUrl+"&fundCode=HOST&custRefNumber="+clientRefNum+"&TransactionCode="+appNum+""
	  				var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
	  				 $window.open(url, '_blank');
	  		       // $location.path(url);
	  		        return true;
	  			}
	  		}
	   	},function(value){
	   		if(value === 'oncancel'){
	   			return false;
	   		}
	   	});

	  	vm.downloadPDF = function(){
	  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  		var a = document.createElement('a');
	  	    document.body.appendChild(a);
	  	    DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
	  	    	
	  	    	if (navigator.appVersion.toString().indexOf('.NET') > 0) {// for IE browser
	  	    		window.navigator.msSaveBlob(res.data.response,filename);
	  	    	}else{
	  	    		var fileURL = URL.createObjectURL(res.data.response);
	  	    		a.href = fileURL;
	  	    		a.download = filename;
	  	    		a.click();
	  	    	}
	  		}, function(err){
	  			console.log('Error downloading the PDF ' + err);
	  		});
	  	};
		
	}
})(angular);

 /* Transfer Decision Controller,Progressive and Mandatory validations Ends  */
