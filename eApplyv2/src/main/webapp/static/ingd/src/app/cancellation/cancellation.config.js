function CancellationConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.cancellation', {
    url: '/cancellation',
    controller: 'CancellationCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'cancellation/cancellation.html',
    title: 'Cancellation'
  })

  .state('app.cancelAcknowledge', {
    url: '/cancellationAcknowledgement',
    controller: 'CancelAckCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'cancellation/cancellation.acknowledge.html',
    title: 'Cancellation Acknowledgement'
  })
  
  .state('app.cancelDecision', {
    url: '/cancelDecision',
    params: { npsTokenURL: null },
    controller: 'CancelDecisionCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'cancellation/cancellation.decision.html',
    title: 'Cancellation Decision'
  });
}

export default CancellationConfig;
