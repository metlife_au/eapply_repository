/*aura occ upgrade controller*/
(function(angular){
	'use strict';
    /* global channel, moment */
	angular
		.module('MtaaApp')
		.controller('auraocc', auraocc);
	auraocc.$inject=['$scope','$rootScope', '$location','$timeout','$window','$stateParams','auraTransferInitiateService', 'fetchAuraTransferData','auraResponseService', 'auraPostfactory','auraInputSvc','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAuraSvc','persoanlDetailService','RetrieveAppDetailsService','ngDialog','urlService','saveEapplyData','appData','fetchPersoanlDetailSvc','fetchAppNumberSvc'];
	function auraocc($scope, $rootScope,$location,$timeout,$window,$stateParams,auraTransferInitiateService,fetchAuraTransferData,auraResponseService,auraPostfactory,auraInputSvc,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAuraSvc,persoanlDetailService,RetrieveAppDetailsService,ngDialog,urlService,saveEapplyData,appData,fetchPersoanlDetailSvc,fetchAppNumberSvc){

		var vm=this;
		$rootScope.$broadcast('enablepointer');
		vm.urlList = urlService.getUrlList();
	    //vm.claimNo = claimNo
	    vm.go = function (path) {
	    	
	    	if($stateParams.mode === 3 && path==='/quoteoccchange/2')
	    	{
	    		path = '/quoteoccchange/3';
	    	}
	    	
	    	$timeout(function(){
	    		$location.path(path);
	    	}, 10);
	  	  //$location.path(path);
	  	};

	 	vm.proceedNext = function() {
	 		vm.keepGoing = true;
	  		angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
	  			if(!Object.questionComplete){
	  				vm.auraResponseDataList[selectedIndex].error=true;
	  				vm.keepGoing = false;
	  			}else{
	  				vm.auraResponseDataList[selectedIndex].error=false;
	  			}
	  		});
	  		if(vm.keepGoing){
	  			$rootScope.$broadcast('disablepointer');
	  			submitAuraSvc.requestObj(vm.urlList.submitAuraUrl).then(function(response) {
	  				appData.setChangeCoverAuraDetails(response.data);
	  				vm.go('/workRatingSummary/1');
	  	  		});
	  		}
	  	};
	  	vm.quotePageDetails = {}; 
	    vm.coverDetails = {};
	    vm.updateCoverOccDetails={};
	 	angular.extend(vm.quotePageDetails, appData.getAppData());
	    angular.extend(vm.coverDetails, vm.quotePageDetails);
	    angular.extend(vm.updateCoverOccDetails, vm.quotePageDetails.applicant);
	    vm.personalDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

	  vm.saveAndExitPopUp = function (hhText) {
			var dialog1 = ngDialog.open({
				    template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Application saved </h4><!-- Row starts --><div class=\'row  rowcustom\'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>'+hhText+
				    '</div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog=\'secondDialogId\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-dialog-close-previous=\'\' ng-click=\'preCloseCallback()\'>Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = '/landing';
					       $location.path( url );
					       return true;
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
		var appNum;
	    appNum = parseInt(fetchAppNumberSvc.getAppNumber());
	 	vm.saveUpdateAura = function() {
	 		
	 		if(vm.coverDetails != null && vm.updateCoverOccDetails != null && vm.personalDetails != null){
	    		vm.coverDetails.lastSavedOn = 'AuraUpdatePage';
	    		var details={};
				details.applicant = vm.updateCoverOccDetails;
				var saveUpdateAuraObject = angular.extend(details,vm.coverDetails);
	        	auraResponseService.setResponse(saveUpdateAuraObject);
	        	$rootScope.$broadcast('disablepointer');
	        	 saveEapplyData.reqObj(vm.urlList.saveEapplyUrlNew, saveUpdateAuraObject).then(function(response) {
		                console.log(response.data);
		                vm.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
		        });
	    	}
	 	};

		vm.navigateToLandingPage = function (){
			ngDialog.openConfirm({
	            template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br>' + 
	            '<div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
	    }; 

		
		// changes added for hyperlink navigation
		var init = function()
	  	{
		 console.log(vm.coverDetails);

	  	auraInputSvc.setFund('MTAA');
	  	auraInputSvc.setMode('WorkRating');

	  	//setting deafult vaues for testing
	  	auraInputSvc.setName(vm.personalDetails.personalDetails.firstName+' '+vm.personalDetails.personalDetails.lastName);
	  	auraInputSvc.setAge(parseInt(vm.coverDetails.applicant.age)) ;
	  	//below values has to be set from quote page, as of now taking from existing insurance
	  	auraInputSvc.setAppnumber(parseInt(vm.coverDetails.applicatioNumber));
	  	auraInputSvc.setDeathAmt(parseFloat(vm.quotePageDetails.applicant.cover[0].existingCoverAmount));
	  	auraInputSvc.setTpdAmt(parseFloat(vm.quotePageDetails.applicant.cover[1].existingCoverAmount));
	  	auraInputSvc.setIpAmt(vm.quotePageDetails.applicant.cover[2].existingCoverAmount === null ? 0 : parseFloat(vm.quotePageDetails.applicant.cover[2].existingCoverAmount));
	  	auraInputSvc.setWaitingPeriod(vm.quotePageDetails.applicant.cover[2].existingIpWaitingPeriod);
	  	auraInputSvc.setBenefitPeriod(vm.quotePageDetails.applicant.cover[2].existingIpBenefitPeriod);
	  	if(vm.updateCoverOccDetails && vm.updateCoverOccDetails.gender ){
	  		auraInputSvc.setGender(vm.updateCoverOccDetails.gender);
	  	}else if(vm.personalDetails && vm.personalDetails.gender){
	  		auraInputSvc.setGender(vm.personalDetails.gender);
	  	}
	  	auraInputSvc.setIndustryOcc(vm.updateCoverOccDetails.industryType+':'+vm.updateCoverOccDetails.occupation);
	  	auraInputSvc.setCountry('Australia');
	  	auraInputSvc.setSalary(vm.updateCoverOccDetails.annualSalary);
	  	auraInputSvc.setFifteenHr(vm.updateCoverOccDetails.workHours);
	  	auraInputSvc.setClientname('metaus');
	  	auraInputSvc.setLastName(vm.personalDetails.personalDetails.lastName);
	  	auraInputSvc.setFirstName(vm.personalDetails.personalDetails.firstName);
	  	auraInputSvc.setDob(vm.personalDetails.personalDetails.dateOfBirth);
	  	auraInputSvc.setExistingTerm(false);
	  	if(vm.personalDetails.memberType==='Personal'){
	  		auraInputSvc.setMemberType('INDUSTRY OCCUPATION');
	  	}else{
	  		auraInputSvc.setMemberType('None');
	  	}
	  	fetchAuraTransferData.requestObj(vm.urlList.auraTransferDataUrl).then(function(response) {
	  		vm.auraResponseDataList = response.data.questions;
	  		console.log(vm.auraResponseDataList);
	  		angular.forEach(vm.auraResponseDataList, function(Object) {
				vm.sectionname = Object.questionAlias.substring(3);

				});
	  	});
		 
	  	};
	  	
	  	if($stateParams.mode === 3 && !vm.coverDetails ){
	    	var num = parseInt(vm.coverDetails.applicatioNumber);
	    	RetrieveAppDetailsService.retrieveAppDetails(vm.urlList.retrieveAppUrlNew,num).then(function(res){
	    		var result = res.data[0];
	    		angular.extend(vm.OPD.applicant, res.data[0].applicant);
	    		vm.svdRtrv = true;
	    		vm.OPD.applicant.svRt = vm.svdRtrv;
	        	appData.setAppData(vm.OPD);
	    		init();
	    		
	    	}, function(err){
	    		console.error('Something went wrong while retrieving the details ' + JSON.stringify(err));
	    	});
		}
		else
			{
			init();
			}
	 // changes added for hyperlink navigation end
	  	 vm.updateRadio = function (answerValue, questionObj){

	   		questionObj.arrAns[0]=answerValue;
	   		 vm.auraRes={'questionID':questionObj.questionId,'auraAnswers':questionObj.arrAns};
	   		  auraResponseService.setResponse(vm.auraRes);
	   		  auraPostfactory.reqObj(vm.urlList.auraPostUrl).then(function(response) {
	   			  vm.selectedIndex = vm.auraResponseDataList.indexOf(questionObj);
	   			  vm.auraResponseDataList[vm.selectedIndex]=response.data;
	   	  		angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
	   	  			if(vm.selectedIndex>selectedIndex && !Object.questionComplete){
	   	  				//branch complete false for previous questions
	   	  				vm.auraResponseDataList[selectedIndex].error=true;
	   	  			}else if(Object.questionComplete){
	   	  				vm.auraResponseDataList[selectedIndex].error=false;
	   	  			}

	   	  		});

	       	}, function () {
	       		//console.log('failed');
	       	});
	   	 };



	}
	})(angular);