/**
 * 
 */
package au.com.metlife.eapply.bs.exception;

/**
 * @author 199306
 *
 */
public class BusinessExceptionConstants {
	private BusinessExceptionConstants(){}
	
	public static final String ERR_CDE_5100_DUPLICATE_APPNUM="ER5100";
	
	public static final String ERR_CDE_5100_MSG="Duplicate application no. found.";
	
	public static final String ERR_CDE_5200_SMTP_DOWN="ER5200";
	
	public static final String ERR_CDE_5200_MSG="SMTP Server down.";
	
	public static final String ERR_CDE_5300_DB_DOWN="ER5300";
	
	public static final String ERR_CDE_5300_MSG="DB Server down.";
	
	public static final String ERR_CDE_5400_BIZFLOW_DOWN="ER5400";
	
	public static final String ERR_CDE_5400_MSG="Bizflow Server down.";
	
	public static final String ERR_CDE_5500_DB_DATA_ERROR="ER5500";
	
	public static final String ERR_CDE_5500_MSG="Data error.";
	
	
	

}
