/**
 * 
 */
package au.com.metlife.eapply.quote.utility;

import java.math.BigDecimal;

import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.quote.model.RuleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author 199306
 *
 */
public class GeneralHelper {
	private GeneralHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(GeneralHelper.class);
	/**
	 * @param jsonObject
	 * @return
	 * @throws com.metlife.eapplication.common.JSONException
	 */
	public static RuleModel convertToRuleModel(JSONObject jsonObject) throws com.metlife.eapplication.common.JSONException {
		log.info("Convert to rule model start");
		RuleModel ruleModel;
		ruleModel=new RuleModel();
		/*Setting personal details*/
		if(QuoteHelper.isNull(jsonObject.get("age"))){
			ruleModel.setAge(new Integer(jsonObject.get("age").toString()).intValue());
		}
		if(QuoteHelper.isNull(jsonObject.get("fundCode"))){
			ruleModel.setFundCode(jsonObject.get("fundCode").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("gender"))){
			ruleModel.setGender(jsonObject.get("gender").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("smoker"))){
			if((Boolean)jsonObject.get("smoker")){
				ruleModel.setSmoker(true);
			}else{
				ruleModel.setSmoker(false);
			}
		}
		/*Setting personal details*/
		
		/*Setting occupation categories*/
		if(QuoteHelper.isNull(jsonObject.get("deathOccCategory"))){
			ruleModel.setDeathOccCategory(jsonObject.get("deathOccCategory").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("tpdOccCategory"))){
			ruleModel.setTpdOccCategory(jsonObject.get("tpdOccCategory").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("ipOccCategory"))){
			ruleModel.setIpOccCategory(jsonObject.get("ipOccCategory").toString());
		}
		/*Setting occupation categories*/
		
		/*Setting cover details*/
		if(QuoteHelper.isNull(jsonObject.get("deathUnits"))){
			ruleModel.setDeathUnits(new Integer(jsonObject.get("deathUnits").toString()).intValue());
		}
		if(QuoteHelper.isNull(jsonObject.get("deathFixedAmount"))){
			ruleModel.setDeathFixedAmount(new BigDecimal(jsonObject.get("deathFixedAmount").toString()));
		}
		if(QuoteHelper.isNull(jsonObject.get("deathCoverType"))){
			ruleModel.setDeathCoverType(jsonObject.get("deathCoverType").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("tpdUnits"))){
			ruleModel.setTpdUnits(new Integer(jsonObject.get("tpdUnits").toString()).intValue());
		}
		if(QuoteHelper.isNull(jsonObject.get("tpFixedAmount"))){
			ruleModel.setTpdFixedAmount(new BigDecimal(jsonObject.get("tpFixedAmount").toString()));
		}
		if(QuoteHelper.isNull(jsonObject.get("tpdCoverType"))){
			ruleModel.setTpdCoverType(jsonObject.get("tpdCoverType").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("ipUnits"))){
			ruleModel.setIpUnits(new Integer(jsonObject.get("ipUnits").toString()).intValue());
		}
		if(QuoteHelper.isNull(jsonObject.get("ipFixedAmount"))){
			ruleModel.setIpFixedAmount(new BigDecimal(jsonObject.get("ipFixedAmount").toString()));
		}
		if(QuoteHelper.isNull(jsonObject.get("ipCoverType"))){
			ruleModel.setIpCoverType(jsonObject.get("ipCoverType").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("ipWaitingPeriod"))){
			ruleModel.setIpWaitingPeriod(jsonObject.get("ipWaitingPeriod").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("ipBenefitPeriod"))){
			ruleModel.setIpBenefitPeriod(jsonObject.get("ipBenefitPeriod").toString());
		}
		/*Setting cover details*/
		
		if(QuoteHelper.isNull(jsonObject.get("memberType"))){
			ruleModel.setMemberType(jsonObject.get("memberType").toString());
		}
		if(QuoteHelper.isNull(jsonObject.get("manageType"))){
			ruleModel.setManageType(jsonObject.get("manageType").toString());
		}

		if(QuoteHelper.isNull(jsonObject.get("premiumFrequency"))){
			ruleModel.setPremiumFrequency(jsonObject.get("premiumFrequency").toString());
		}
		log.info("Convert to rule model finish");
		return ruleModel;
	}
}
