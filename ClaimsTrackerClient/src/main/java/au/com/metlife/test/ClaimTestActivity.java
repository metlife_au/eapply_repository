package au.com.metlife.test;

public class ClaimTestActivity {
	
	private String id;
	private String name;
	private String date;
	private String requirement;
	private String comments;
	private String requirementReceviedDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getRequirementReceviedDate() {
		return requirementReceviedDate;
	}
	public void setRequirementReceviedDate(String requirementReceviedDate) {
		this.requirementReceviedDate = requirementReceviedDate;
	}
	

}
