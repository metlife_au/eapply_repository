angular.module('globalExceptionhandler',[]);
angular.module('globalExceptionhandler').factory('$exceptionHandler', function ($log, $injector) {
    return function exceptionHandler(exception, cause) {
        var $rootScope = $injector.get('$rootScope');
        
        $rootScope.$on("$routeChangeSuccess", function(scope, current, pre){
            $rootScope.errorOccured = false;
        });

        $rootScope.errors = $rootScope.errors || [];
        $rootScope.errors.push({
            string: exception.toString(),
            message: exception.message,
            lineNumber: exception.lineNumber,
            stack: exception.stack,
            cause: cause
        });
        $log.error($rootScope.errors);
        $rootScope.errorOccured = true;
        $rootScope.$broadcast('enablepointer'); 
    };
});