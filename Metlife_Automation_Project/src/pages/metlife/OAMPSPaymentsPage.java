package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.OAMPSPaymentsPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class OAMPSPaymentsPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public OAMPSPaymentsPage yourPayment() {
		paymentDetails();
		return new OAMPSPaymentsPage(scriptHelper);
	}

	public OAMPSPaymentsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void paymentDetails() {

		//String cardType = dataTable.getData("MetLife_Data", "CardType");
		String aacountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsbcode = dataTable.getData("MetLife_Data", "BSBCode");
		String bsbnumber = dataTable.getData("MetLife_Data", "BSBNumber");
		
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			tryAction(fluentWaitElement(OAMPSPaymentsPageObjects.rddebit), "clkradio", "Debit");
			Thread.sleep(500);
			
			
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtaccountname), "SET", "AccountName", aacountName);
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtbsbcode), "SET", "BSB", bsbcode);
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtbsbcode), "TAB", "BSB");
			
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtbsbnumber), "SET", "BSB", bsbnumber);
			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.txtbsbnumber), "TAB", "BSB");

			tryAction(waitForClickableElement(OAMPSPaymentsPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Thread.sleep(500);
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(OAMPSPaymentsPageObjects.btnsubmit)).click().perform();
			Thread.sleep(1000);	
			
			switchToActiveWindow();
			driver.manage().window().maximize();	
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
