package au.com.metlife.eapply.b2b.security;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	private static final Logger log = LoggerFactory.getLogger(ResourceServerConfiguration.class);
    @Value("${resource.id:spring-boot-application}")
    private String resourceId;
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(resourceId);
    }
    @Override
    public void configure(HttpSecurity http) throws Exception {
    	log.info("Configure application start");
         http.requestMatcher(new OAuthRequestedMatcher())
                .authorizeRequests()
                 .antMatchers(HttpMethod.OPTIONS).permitAll();
                   /*.anyRequest().authenticated();*/
    }
    private static class OAuthRequestedMatcher implements RequestMatcher {
        public boolean matches(HttpServletRequest request) {
        	  boolean haveOauth2Token = false;
        	  boolean haveAccessToken = false;
        	String auth = request.getHeader("Authorization");
        	
            /*Determine if the client request contained an OAuth Authorization*/
           if(auth!=null){
        	   auth =  new String(Base64.getDecoder().decode(auth));
        	   haveOauth2Token = auth.startsWith("Bearer");
               haveAccessToken = request.getParameter("access_token")!=null;
           }else{
        	   haveOauth2Token = true;
           }            
           
   return haveOauth2Token || haveAccessToken;
        }
    }
}