class TransferAuraCtrl {
  constructor(CommonAPI, EapplyData, AuraAPI, AppConstants, $scope, $state, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._AuraAPI = AuraAPI;
    this._ngDialog = ngDialog;
  }

  init() {
	  
	  const _this = this;
	  _this.urlList = _this._CommonAPI.getUrlList();
	  _this.applicantData = _this._EapplyData.getApplicantData();
	  _this.transferData = _this._EapplyData.getTransferData();
    
	  _this.auraData = {
    		'fund': 'INGD',
    		'mode': 'TransferCover',
    		'age': parseInt(_this.transferData.applicant.age),
    		'name': _this.transferData.applicant.firstName + ' ' + _this.transferData.applicant.lastName,
    		'appnumber': parseInt(_this.transferData.applicatioNumber),
    		'gender': _this.transferData.applicant.gender,
    		'country': 'Australia',
    		'fifteenHr': _this.transferData.applicant.workHours,
    		'deathAmt': 1,
    		'tpdAmt': 1,
    		'ipAmt': 1,
    		'waitingPeriod': _this.transferData.applicant.cover[2].totalIpWaitingPeriod,
    	    'benefitPeriod': _this.transferData.applicant.cover[2].totalIpBenefitPeriod == "Age 67" ? "Age 65" :_this.transferData.applicant.cover[2].totalIpBenefitPeriod,
    	    'industryOcc': _this.transferData.applicant.industryType+':'+_this.transferData.applicant.occupation,
    	    'salary':  _this.transferData.applicant.annualSalary.toString(),
    	    'clientname': 'metaus',
    	    'lastName': _this.transferData.applicant.lastName,
    	    'firstName': _this.transferData.applicant.firstName,
    	    'dob': _this.transferData.applicant.birthDate,
    	    'existingTerm': false,
    	    'memberType': 'None',
    	    'auraType':'long'
    	    };
    
    console.log(JSON.stringify(_this.auraData));
    
    _this._AuraAPI.fetchAuraQuestions(_this.urlList.auraTransferDataUrl, _this.auraData).then(
      (res) => {
        _this.auraResponseDataList = res.questions;
        angular.forEach(_this.auraResponseDataList, function (value, key) {
          _this.sectionname = value.questionAlias.substring(3);
        });
      },
      (err) => {
        _this.errors = err.data.errors;
      }
    );
  }

updateRadio(answerValue, questionObj) {
  console.log(this.auraResponseDataList);
  questionObj.arrAns[0] = answerValue;
  let auraRes = {
    'questionID': questionObj.questionId,
    'auraAnswers': questionObj.arrAns
  };
  this._AuraAPI.submitAuraQuestions(this.urlList.auraPostUrl, auraRes).then(
    (response) => {
      const _this = this;
      _this.selectedIndex = this.auraResponseDataList.indexOf(questionObj);
      this.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data);
      _this.auraResponseDataList[this.selectedIndex] = response.data;
      angular.forEach(this.auraResponseDataList, function (obj, selectedIndex) {
        if(_this.selectedIndex > selectedIndex && !obj.questionComplete) {
          //branch complete false for previous questions
          _this.auraResponseDataList[selectedIndex].error = true;
        } else if(obj.questionComplete){
          _this.auraResponseDataList[selectedIndex].error = false;
        }
      });
    },
    (err) => {
      this.errors = err.data.errors;
    }
  );
  }

  updateQuestionList(questionId,changedQuestion, questionObj) {
    const _this = this;
    angular.forEach(this.auraResponseDataList, function (obj, index) {
      if(obj.questionId == changedQuestion.questionId) {
        _this.auraResponseDataList[index]= changedQuestion;
        console.log(_this.auraResponseDataList[index]);
      }
    });
  }

  save() {
	  this.transferData.lastSavedOn = 'transferaura';
	  
	  this._CommonAPI.saveEapply(this.urlList.saveEapplyUrlNew, this.transferData).then(
      (res) => {
        console.log(res);
        this.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+this.transferData.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
      },
      (err) => {
        this.errors = err.data.errors;
      }
    );
	  
  }

  previous() {
	  this._$state.go('app.transfercover');
  }
  
  saveAndExitPopUp(hhText) {
	  
	  this._ngDialog.openConfirm({
		  template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Finish &amp; Close Window </button></div></div>',
		  plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
      }).then(
      () => {
        this._$state.go('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }
  
  submitAura(){
	  
	  const _this = this;
	  _this._AuraAPI.submitAuraSession(_this.urlList.submitAuraUrl, _this.auraData).then(
		        (response) => {
		        	_this.auraResponses = response.data;
		        	_this.transferData.responseObject = _this.auraResponses.responseObject;
		        	if(response.status === 200) {		        		
		        		if(_this.auraResponses.overallDecision !== 'DCL') {
		        			this.proceedToSummary();
		        		}else if(_this.auraResponses.overallDecision === 'DCL') {
		        			this.submitApplication();
		        		}
					}		        	
		        }
	   );
  }
  
  proceedToSummary(){
	  
	  const _this = this;
	  _this.transferData.appDecision = 'ACC';
	  _this.transferData.applicant.overallCoverDecision ='ACC';
	  _this.transferData.applicant.cover[0].coverDecision = _this.auraResponses.deathDecision;
	  _this.transferData.applicant.cover[1].coverDecision = _this.auraResponses.tpdDecision;
	  _this.transferData.applicant.cover[2].coverDecision = _this.auraResponses.ipDecision;
	  
	  _this._EapplyData.setTransferData(_this.transferData);
	  _this._$state.go('app.transfersummary');
	  
  }
  
  submitApplication(){
	  const _this = this;
	  
	  _this.transferData.appDecision ='DCL';
	  _this.transferData.applicant.overallCoverDecision ='DCL';
	  _this.transferData.applicant.cover[0].coverDecision = _this.auraResponses.deathDecision;
	  _this.transferData.applicant.cover[1].coverDecision = _this.auraResponses.tpdDecision;
	  _this.transferData.applicant.cover[2].coverDecision = _this.auraResponses.ipDecision;
	  
	  _this._EapplyData.setTransferData(_this.transferData);
	  
	  _this._CommonAPI.submitEapply(_this.urlList.submitEapplyUrlNew, _this.transferData).then(
              (res) => {
            	  _this._EapplyData.setPdfLocation(res.clientPDFLocation);
            	  _this._EapplyData.setNpsUrl(res.npsTokenURL);
            	  _this._$state.go('app.transferdecision');
              }, 
              (err) => {
                console.log('Error while submitting transfer cover ' + err);
              }
       );	  
  }

  continue() {
    const _this = this;
    _this.keepGoing= true;
    angular.forEach(_this.auraResponseDataList, function (obj, selectedIndex) {
      if(obj.questionComplete && obj.branchComplete) {
        _this.auraResponseDataList[selectedIndex].error = false;
      } else {
        _this.auraResponseDataList[selectedIndex].error = true;
        _this.keepGoing = false;
      }
    });
    if(_this.keepGoing){
    	_this.submitAura();
    }
  }
}

export default TransferAuraCtrl;
