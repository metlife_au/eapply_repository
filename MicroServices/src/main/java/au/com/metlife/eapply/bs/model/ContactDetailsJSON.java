package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class ContactDetailsJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8077540844174412490L;

	private String emailAddress;
	
	private String homePhone;
	
	private String mobilePhone;
	
	private String prefContact;
	
	private String prefContactTime;
	
	private String workPhone;
	
	private String fundEmailAddress;

	public String getFundEmailAddress() {
		return fundEmailAddress;
	}

	public void setFundEmailAddress(String fundEmailAddress) {
		this.fundEmailAddress = fundEmailAddress;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getPrefContact() {
		return prefContact;
	}

	public void setPrefContact(String prefContact) {
		this.prefContact = prefContact;
	}

	public String getPrefContactTime() {
		return prefContactTime;
	}

	public void setPrefContactTime(String prefContactTime) {
		this.prefContactTime = prefContactTime;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	

}
