function QuoteConfig($stateProvider, $httpProvider) {
  'ngInject';
  $httpProvider.defaults.withCredentials = true;
  $stateProvider
    .state('app.quote', {
      url: '/quote',
      controller: 'QuoteCoverCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'quote/quote.cover.html',
      title: 'Quote - Cover',
      resolve: {
          industryList: function(CommonAPI) {
            let urlList = CommonAPI.getUrlList();
            return !CommonAPI.getIndustryList() && CommonAPI.fetchIndustryList(urlList.quoteUrl).then(
              (res) => {
                CommonAPI.setIndustryList(res);
              },
              (err) => {
                console.log(err.data.errors);
              });
          }
        }
    })

    .state('app.quoteaura', {
      url:'/quoteaura',
      controller: 'QuoteAuraCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'quote/quote.aura.html',
      title: 'Quote - Aura'
    })

    .state('app.quotesummary', {
      url:'/quotesummary',
      controller: 'QuoteSummaryCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'quote/quote.summary.html',
      title: 'Quote - Summary'
    })

    .state('app.quotedecision', {
      url:'/quotedecision',
      controller: 'QuoteDecisionCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'quote/quote.decision.html',
      title: 'Quote - Decision'
    });
}

export default QuoteConfig;
