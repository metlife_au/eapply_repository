/**
 * 
 */
package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesConfirmPageObjects;
import uimap.metlife.HostplusPersonalDetailsPageObjects;
import uimap.metlife.PersonalStatementPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;

/**
 * @author sampath
 * 
 */
public class HOSTPLUSPersonalStatementDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HOSTPLUSPersonalStatementDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public HOSTPLUSPersonalStatementDetailsPage enterHostPlusPersonalStatementDetails() {
		fillInHostPlusPersonalStatementDetails();
		return new HOSTPLUSPersonalStatementDetailsPage(scriptHelper);
	}
	
	public HOSTPLUSPersonalStatementDetailsPage enterHostPlusIntroductionDetails() {
		fillIntroductionDetails();
		return new HOSTPLUSPersonalStatementDetailsPage(scriptHelper);
	}
	
	private void fillInInsuranceDetails() {
		
		
		try{
			List<WebElement> radInsuranceDetails = null;
			radInsuranceDetails = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsNo);
			tryAction(fluentWaitElement(radInsuranceDetails.get(0)),"Click","Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined?");
			tryAction(fluentWaitElement(radInsuranceDetails.get(1)),"Click","Are you contemplating or have you ever made a claim for or received sickness?");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillIntroductionDetails() {
		String strFirstName = dataTable.getData("MetLife_Data", "login.fname");
		String strLastName = dataTable.getData("MetLife_Data", "login.lname");
		String strGender = dataTable.getData("MetLife_Data", "personal.gender");
		String strCitigen = dataTable.getData("MetLife_Data", "personal.citizen");
		String strSmoker = dataTable.getData("MetLife_Data", "personal.smoked");
		
		try{
			
			tryAction(fluentWaitElement(ColesConfirmPageObjects.selTitle),"specialDropdown", "Title", "Mr");
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.txtFirstname),"SET","First Name",strFirstName);
			tryAction(driver.findElement(HostplusPersonalDetailsPageObjects.txtLastName),"SET","LastName",strLastName);
			tryAction(fluentWaitElements(strGender.equalsIgnoreCase("Male")?HostplusPersonalDetailsPageObjects.radMale:HostplusPersonalDetailsPageObjects.radFemale),"clkradio","Gender - "+strGender);
			tryAction(fluentWaitElements(strCitigen.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radCitizenYes:HostplusPersonalDetailsPageObjects.radCitizenNo),"clkradio","Are you a citizen or permanent resident of Australia? - "+strCitigen);
			tryAction(fluentWaitElements(strSmoker.equalsIgnoreCase("yes")?HostplusPersonalDetailsPageObjects.radSmokerYes:HostplusPersonalDetailsPageObjects.radSmokerNo),"clkradio","Have you smoked in the last 12 months? - "+strSmoker);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnIntroduction),"CLICK","Continue");
			//tryAction(waitForClickableElement(ColesConfirmPageObjects.selTitle),"click","Title");
			//tryAction(fluentWaitElements(ColesConfirmPageObjects.linkTitleMr),"DropDownSelect","Title","Mr");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInLifeStyleDetails() {
		
		
		try{
			
			String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
			
			List<WebElement> radLifeStyleDetails = null;
			List<WebElement> radLifeStyleDetailsYes = null;
			radLifeStyleDetails = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsNo);
			radLifeStyleDetailsYes= fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsYes);
			
			tryAction(fluentWaitElement(radLifeStyleDetailsYes.get(0)),"Click","Do you have firm plans to travel or reside in another country other than NZ? - Yes");
			tryAction(fluentWaitElement(radLifeStyleDetails.get(1)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you?");
			tryAction(fluentWaitElement(radLifeStyleDetails.get(2)),"Click","Have you ever been advised by a health professional to reduce your alcohol consumption?");
			tryAction(fluentWaitElement(radLifeStyleDetails.get(3)),"Click","Do you have HIV (Human Immunodeficiency Virus) that causes AIDS?");
			
			String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetails.get(3).getAttribute("name")+"_1_0'][value='No']";
			//Click On No for HIV
			tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkSportsNota),"Click","Do you regularly engage in or intend to engage in any of the following hazardous activities?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtAlcoholicDrinksConsumption),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnAlcoholicEnter),"CLICK","Enter");
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}

	private void fillInHostPlusPersonalStatementDetails() {
		
		String strLungDese = dataTable.getData("MetLife_Data", "personal.lung");
		String strLungAsthma = dataTable.getData("MetLife_Data", "personal.asthma");
		String strLungCondition = dataTable.getData("MetLife_Data", "personal.condition");
		String strPregnent = dataTable.getData("MetLife_Data", "personal.prebnent");
		String strRegulatVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
		String strFamHist = dataTable.getData("MetLife_Data", "personal.famHist");
		String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strAlcohol = dataTable.getData("MetLife_Data", "personal.alcohol");
		String strAlcoholConsu = dataTable.getData("MetLife_Data", "personal.alcohlConsumption");
		String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
		String strHIV = dataTable.getData("MetLife_Data", "personal.HIV");
		String strDisclosed = dataTable.getData("MetLife_Data", "personal.disclosed");
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strReceivedClaim = dataTable.getData("MetLife_Data", "personal.receivedClaim");
		String strAsthmaWorsened = dataTable.getData("MetLife_Data", "personal.Asthamaworesen");
		String strBenefitPeriod = dataTable.getData("MetLife_Data", "change.benefitperiod");
		String strHeight = dataTable.getData("MetLife_Data", "personal.height");
		String strHeightUnits = dataTable.getData("MetLife_Data", "personal.heightunits");
		String strWeight = dataTable.getData("MetLife_Data", "personal.weight");
		String strWeightUnits = dataTable.getData("MetLife_Data", "personal.weightunits");
		String strSmoked = dataTable.getData("MetLife_Data", "personal.smoked");
		String strGender = dataTable.getData("MetLife_Data", "personal.gender");
		String strRegVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
		String str3YrDiagnosis = dataTable.getData("MetLife_Data", "last3YearsDiagnosied");
		
		//radHealthDeatilsYes
		List<WebElement> radHealthDetailsYes = null;
		List<WebElement> radHealthDetailsNo = null;
		WebElement pregnentYes = null;
		WebElement pregnentNo = null;
		WebElement travelYes = null;
		WebElement travelNo = null;
		String strPregnentYes = null;
		String strPregnentNo = null;
		String strTravelYes = null;
		String strTravelNo = null;
		sleep(2000);
		
	try{
			
			radHealthDetailsYes = fluentWaitListElements(HostplusPersonalDetailsPageObjects.radHPHealthDeatilsYes);
			radHealthDetailsNo = fluentWaitListElements(HostplusPersonalDetailsPageObjects.radHPHealthDeatilsNo);
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtHeight),"SET","Height",strHeight);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstHPHeightUnits),"specialDropdown","Height Units",strHeightUnits);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.txtWeight),"SET","Weight",strWeight);
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.lstHPWeightUnits),"specialDropdown","Weight Units",strWeightUnits);
			
			tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
			sleep(3000);
			System.out.println(radHealthDetailsNo.size());
			/*radHealthDetailsYes = fluentWaitListElements(HostplusPersonalDetailsPageObjects.radHPHealthDeatilsYes);
			radHealthDetailsNo = fluentWaitListElements(HostplusPersonalDetailsPageObjects.radHPHealthDeatilsNo);
			tryAction(radHealthDetailsYes.get(1),"MoveToElm","What industry do you work in?");
			System.out.println(radHealthDetailsYes.get(1).toString());
			System.out.println(radHealthDetailsYes.get(1).getAttribute("for"));
			strPregnentYes = "label[for='"+getAttribute(radHealthDetailsYes.get(1), "for")+"']";
			sleep();
			strPregnentNo = "label[for='"+getAttribute(radHealthDetailsNo.get(1), "for")+"']";
			sleep();
			strTravelYes = "label[for='"+getAttribute(radHealthDetailsYes.get(2), "for")+"']";
			sleep();
			strTravelNo = "label[for='"+getAttribute(radHealthDetailsNo.get(2), "for")+"']";
			sleep();
			System.out.println(strPregnentYes);
			System.out.println(strPregnentNo);
			System.out.println(strTravelYes);
			System.out.println(strTravelNo);*/
			if(strGender.equalsIgnoreCase("female")){
				System.out.println("in Female");
				/*pregnentYes = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(1)), "for")+"']"));
				pregnentNo = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(1)), "for")+"']"));
				travelYes = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(2)), "for")+"']"));
				travelNo = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(2)), "for")+"']"));
				tryAction(strPregnent.equalsIgnoreCase("yes")?pregnentYes:pregnentNo,"Click","Are you currently pregnant? - "+strPregnent);
				sleep(8000);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
				sleep(5000);*/
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radPreagNo),"Click","Are you currently pregnant?"+strPregnent);
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTA3yrsdiagnosed),"Click","last 3 years have you suffered from, been diagnosed ");
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTA5yrsdiagnosed),"Click","last 5 years have you suffered from, been diagnosed ");
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTAdiagnosed),"Click","Have you ever suffered from, been diagnosed with or sought medical advice");
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radRegVisitNo),"Click","Do you have a usual doctor or medical centre you regularly visit?"+strRegulatVisit);
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnHealthQuestionsF),"Click","Health Questions Button");
				sleep(3000);	
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radFamHistNoF),"Click","You are only required to disclose family history?"+strFamHist);
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnFamilyHistory),"Click","Family History Button");
				if(strTravelPlan.equalsIgnoreCase("No")){
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radTravelPlanNoF),"Click","Do you have firm plans to travel?"+strTravelPlan);
					}
					else{
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radTravelPlanYesF),"Click","Do you have firm plans to travel?"+strTravelPlan);
					}
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTAhazardousF),"Click","engage in any of the following hazardous activities?");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radDrugNoF),"Click","used any drugs that were not prescribed to you?"+strDrugs);
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.txtAlcoholicF),"SET","how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnAlcoholicF),"Click","alcoholic button");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radRedAlcoholicNoF),"Click","health professional to reduce your alcohol consumption?"+strAlcoholConsu);
					sleep(3000);
					if(strHIV.equalsIgnoreCase("yes")){
						tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radHIVYesF),"Click","Are you infected with HIV?"+strHIV);
						sleep(3000);
					}else
					{
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radHIVNoF),"Click","Are you infected with HIV?"+strHIV);
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radHIVPrevNoF),"Click","taking HIV preventative medication?"+strHIV);
					sleep(3000);
					}
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnLifyStyle),"Click","Life Style button");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radDiscloseNoF),"Click","do you suffer Other than already disclosed"+strDisclosed);
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnGeneralQuest),"Click","General Question button");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radTraumaNoF),"Click","Has an application for Life, Trauma, TPD or Disability Declined?"+strInsuTPD);
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radIllInjuryNoF),"Click"," have you ever made a claim for or received sickness?"+strReceivedClaim);
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnHealthLife),"Click","Health Questions Submission Button");
					sleep(3000);
					if(strHIV.equalsIgnoreCase("No")){
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxSelDuty),"Click","NOTICE OF THE DUTY OF DISCLOSURE");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxSelPrevStmt),"Click"," acknowledge the Privacy Statement");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxSelGC),"Click","acknowledge the General Consent before proceeding");
					sleep(3000);
					tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnconfirm),"Click","Final Application Submission Button");
					sleep(3000);
					}
					
					if (isElementPresent(HostplusPersonalDetailsPageObjects.lblOutcome)){
						storeSessionThreadData("dyn_outcome",fluentWaitElements(HostplusPersonalDetailsPageObjects.lblOutcome).getText());
						}
					assertEqualText(dataTable.getData("MetLife_Data", "applicationMessage"),getStringValueFromSessionThreadData("dyn_outcome"));
					if (isElementPresent(HostplusPersonalDetailsPageObjects.lblAppnum)){
						storeSessionThreadData("dyn_appnum",fluentWaitElements(HostplusPersonalDetailsPageObjects.lblAppnum).getText());
						}
					//end
				
			}else{
				System.out.println("in male");
				//tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radPreagNo),"Click","Do you have a usual doctor or medical centre you regularly visit?"+strRegulatVisit);
				//sleep(3000);
				
				//travelYes = waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(1)), "for")+"']+*"));
				//travelNo = waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(1)), "for")+"']+*"));
				//tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
				//tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			
			
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTA3yrsdiagnosedM),"Click","last 3 years have you suffered from, been diagnosed ");
		
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTA5yrsdiagnosedM),"Click","last 5 years have you suffered from, been diagnosed ");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTAdiagnosedM),"Click","Have you ever suffered from, been diagnosed with or sought medical advice");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radRegVisitNoM),"Click","Do you have a usual doctor or medical centre you regularly visit?"+strRegulatVisit);
			sleep(3000);
			}			
			/*if(strGender.equalsIgnoreCase("female")){
				System.out.println("in Female");
				/*pregnentYes = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(1)), "for")+"']"));
				pregnentNo = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(1)), "for")+"']"));
				travelYes = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(2)), "for")+"']"));
				travelNo = (WebElement)waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(2)), "for")+"']"));
				tryAction(strPregnent.equalsIgnoreCase("yes")?pregnentYes:pregnentNo,"Click","Are you currently pregnant? - "+strPregnent);
				sleep(8000);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
				sleep(5000);*/
				/*tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radPreagNo),"Click","Are you currently pregnant?"+strPregnent);
				sleep(3000);
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radRegVisitNo),"Click","Do you have a usual doctor or medical centre you regularly visit?"+strRegulatVisit);
				sleep(3000);
			}else{
				System.out.println("in male");
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radPreagNo),"Click","Do you have a usual doctor or medical centre you regularly visit?"+strRegulatVisit);
				sleep(3000);
				
				//travelYes = waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(1)), "for")+"']+*"));
				//travelNo = waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(1)), "for")+"']+*"));
				//tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
				//tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}*/
			
			//tryAction(strSmoked.equalsIgnoreCase("Yes")?radHealthDetailsYes.get(0):radHealthDetailsNo.get(0),"Click","Are you a smoker? -"+strSmoked);
			//tryAction(fluentWaitElement(strRegulatVisit.equalsIgnoreCase("Yes")?radHealthDetailsYes.get(1):radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegulatVisit);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnHealthQuestions),"Click","Health Questions Button");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radFamHistNo),"Click","You are only required to disclose family history?"+strFamHist);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnFamilyHistory),"Click","Family History Button");
		/*	travelYes = waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsYes.get(1)), "for")+"']+*"));
			travelNo = waitForClickableElement(getObject("label[for='"+getAttribute(fluentWaitElement(radHealthDetailsNo.get(1)), "for")+"']+*"));
			//tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);*/
			sleep(3000);
					
			
			if(strTravelPlan.equalsIgnoreCase("No")){
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radTravelPlanNo),"Click","Do you have firm plans to travel?"+strTravelPlan);
			}
			else{
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radTravelPlanYes),"Click","Do you have firm plans to travel?"+strTravelPlan);
			}
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTAhazardous),"Click","engage in any of the following hazardous activities?");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radDrugNo),"Click","used any drugs that were not prescribed to you?"+strDrugs);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.txtAlcoholic),"SET","how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnAlcoholic),"Click","alcoholic button");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radRedAlcoholicNo),"Click","health professional to reduce your alcohol consumption?"+strAlcoholConsu);
			sleep(3000);
			if(strHIV.equalsIgnoreCase("yes")){
				tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radHIVYes),"Click","Are you infected with HIV?"+strHIV);
				sleep(3000);
			}else
			{
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radHIVNo),"Click","Are you infected with HIV?"+strHIV);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radHIVPrevNo),"Click","taking HIV preventative medication?"+strHIV);
			sleep(3000);
			}
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnLifyStyle),"Click","Life Style button");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radDiscloseNo),"Click","do you suffer Other than already disclosed"+strDisclosed);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnGeneralQuest),"Click","General Question button");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radTraumaNo),"Click","Has an application for Life, Trauma, TPD or Disability Declined?"+strInsuTPD);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.radIllInjuryNo),"Click"," have you ever made a claim for or received sickness?"+strReceivedClaim);
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnHealthLife),"Click","Health Questions Submission Button");
			sleep(3000);
			if(strHIV.equalsIgnoreCase("No")){
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxSelDuty),"Click","NOTICE OF THE DUTY OF DISCLOSURE");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxSelPrevStmt),"Click"," acknowledge the Privacy Statement");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxSelGC),"Click","acknowledge the General Consent before proceeding");
			sleep(3000);
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.btnconfirm),"Click","Final Application Submission Button");
			sleep(3000);
			}
			
			if (isElementPresent(HostplusPersonalDetailsPageObjects.lblOutcome)){
				storeSessionThreadData("dyn_outcome",fluentWaitElements(HostplusPersonalDetailsPageObjects.lblOutcome).getText());
				}
			assertEqualText(dataTable.getData("MetLife_Data", "applicationMessage"),getStringValueFromSessionThreadData("dyn_outcome"));
			if (isElementPresent(HostplusPersonalDetailsPageObjects.lblAppnum)){
				storeSessionThreadData("dyn_appnum",fluentWaitElements(HostplusPersonalDetailsPageObjects.lblAppnum).getText());
				}
			
			/*tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
			tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.chkAsthma),"chkCheck","Asthma");
			if(strLungCondition.equalsIgnoreCase("mild")){
				tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.radAsthmaMild),"Click","Mild Asthma Condition - "+strLungCondition);
			}else if(strLungCondition.equalsIgnoreCase("Moderate")){
				tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.radAsthmaModerate),"Click","Mild Asthma Condition - "+strLungCondition);
			}else if(strLungCondition.equalsIgnoreCase("Severe")){
				tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.radAsthmaSevere),"Click","Mild Asthma Condition - "+strLungCondition);
			}else{
				tryAction(fluentWaitElements(HostplusPersonalDetailsPageObjects.radAsthmaMild),"Click","Mild Asthma Condition - "+strLungCondition);
			}
			sleep();
			String strIdentification = null;
			strIdentification = getAttribute(fluentWaitElements(HostplusPersonalDetailsPageObjects.radAsthmaMild), "for");
			System.out.println("strIdentification : "+strIdentification);
			String strAsthmaWorse = null;
			strAsthmaWorse = org.apache.commons.lang.StringUtils.substring(strIdentification, 0, 7)+"1-"+org.apache.commons.lang.StringUtils.substring(strIdentification, 9, 16)+"_1:0";
			System.out.println("strAsthmaWorse :: "+strAsthmaWorse);
			System.out.println("No radio "+"label[for='"+strAsthmaWorse+"']");
			tryAction(strAsthmaWorsened.equalsIgnoreCase("yes")?waitForClickableElement(getObject("label[for='"+strAsthmaWorse+"']")):waitForClickableElement(getObject("label[for='"+strAsthmaWorse+"']")),"Click","Is your asthma worsened by your occupation? - "+strAsthmaWorsened);
			
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chkBPCholNota),"Click","In the last 5 years have you suffered from");
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.radPregnentNo),"Click","Are you currently pregnant?");
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.radVisitDocNo),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chkBrainBoneNota),"Click","In the last 5 years have you suffered from, been diagnosed with or sought medical advice or treatment for ?");
			sleep();*/
			//tryAction(waitForClickableElement(HostplusPersonalDetailsPageObjects.chekboxNOTA),"Click","Have you ever suffered from, been diagnosed with or sought medical advice or treatment for?");
			//sleep();
	/*		if(strGender.equalsIgnoreCase("female")){
				
				tryAction(strPregnent.equalsIgnoreCase("yes")?pregnentYes:pregnentNo,"Click","Are you currently pregnant? - "+strPregnent);
				sleep(8000);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
				sleep(5000);
			}else{
				tryAction(strRegVisit.equalsIgnoreCase("yes")?travelYes:travelNo,"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}*/

			
			
		//	tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistNo),"Click","Family History");
			
	//		fillInLifeStyleDetails();


			//General Details
		//	tryAction(waitForClickableElement(PersonalStatementPageObjects.radSufferFromAnyConditionNo),"CLICK","do you presently suffer from any condition?");
		//	fillInInsuranceDetails();
		//	tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinuePersonalStatement),"Click","Calculate Quote");
			
	}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
}
