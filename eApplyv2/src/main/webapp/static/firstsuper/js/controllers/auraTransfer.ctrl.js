/*aura transfer controller*/
FirstSuperApp.controller('auratransfer',['$scope','$rootScope','$timeout','$routeParams','$location','$window','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','RetrieveAppDetailsService','ngDialog','urlService','saveEapply','submitEapply',
                                     function($scope,$rootScope,$timeout,$routeParams,$location,$window,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,RetrieveAppDetailsService,ngDialog,urlService,saveEapply,submitEapply){

	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
	$scope.svdRtrv =false;
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	if($scope.svdRtrv && path =='/quotetransfer/2')
    		{
    			path = "/quotetransfer/3";
    		}
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	   //$location.path( path );
  	};

 // added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/

  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    }
 
  	$scope.coverDetails=PersistenceService.gettransferCoverDetails();
  	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.transferOccDetails =PersistenceService.getTransferCoverOccDetails();
  	$scope.deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
	$scope.tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
	$scope.ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();
	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();
  	
  
  	
  	var init = function()
  	{
  		auraInputService.setFund('FIRS');
  	  	auraInputService.setMode('TransferCover');
  	  	//setting values from quote page
  	    auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
  	 //	auraInputService.setAge(moment().diff($scope.coverDetails.dob, 'years')) ;
  	  	auraInputService.setName($scope.coverDetails.name);
  	  	auraInputService.setAppnumber($scope.coverDetails.appNum);

  	  	if($scope.transferOccDetails && $scope.transferOccDetails.gender ){
  	  		auraInputService.setGender($scope.transferOccDetails.gender);
  	  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  	  		auraInputService.setGender($scope.personalDetails.gender);
  	  	}
  	  	auraInputService.setCountry('Australia');
  	  	auraInputService.setFifteenHr($scope.transferOccDetails.fifteenHr);
  	  	auraInputService.setDeathAmt($scope.deathAddnlCvrDetails.deathTransferCovrAmt);
  	  	auraInputService.setTpdAmt($scope.tpdAddnlCvrDetails.tpdTransferCovrAmt);
  	  	auraInputService.setIpAmt($scope.ipAddnlCvrDetails.ipTransferCovrAmt);
  	  	auraInputService.setWaitingPeriod($scope.ipAddnlCvrDetails.addnlTransferWaitingPeriod);
  	  	auraInputService.setBenefitPeriod($scope.ipAddnlCvrDetails.addnlTransferBenefitPeriod) ;
  	  	auraInputService.setIndustryOcc($scope.transferOccDetails.industryCode+":"+$scope.transferOccDetails.occupation);
  	  	auraInputService.setSalary($scope.transferOccDetails.salary);
  	  	auraInputService.setClientname('metaus')
  	  	auraInputService.setLastName($scope.personalDetails.personalDetails.lastName)
  	  	auraInputService.setFirstName($scope.personalDetails.personalDetails.firstName)
  	  	auraInputService.setDob($scope.personalDetails.personalDetails.dateOfBirth)
  	  	auraInputService.setExistingTerm(false);
  	  	if($scope.personalDetails.memberType=="Personal"){
  	  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	  	}else{
  	  		auraInputService.setMemberType("None")
  	  	}
  	  	/*auraInputService.setDeathAmt(200000)
  	  	auraInputService.setTpdAmt(200000)
  	  	auraInputService.setIpAmt(2000)
  	  	auraInputService.setWaitingPeriod('30 Days')
  	  	auraInputService.setBenefitPeriod('2 Years')
  	  	auraInputService.setGender($scope.coverDetails.gender)
  	  	auraInputService.setIndustryOcc($scope.coverDetails.industryCode+":"+$scope.coverDetails.occupation)
  	  	if($scope.transferOccDetails.citizenQue=='Yes'){
  	  		auraInputService.setCountry('Australia')
  	  	}else{
  	  		auraInputService.setCountry($scope.transferOccDetails.citizenQue)
  	  	}
  	  	auraInputService.setSalary($scope.transferOccDetails.salary)
  	  	auraInputService.setFifteenHr($scope.transferOccDetails.fifteenHr)
  	  	auraInputService.setName($scope.coverDetails.name)*/
  	  	///////////////

  	  	 //setting deafult vaues for testing
  		/*auraInputService.setName('HOST')
  	  	auraInputService.setAge(50)
  	  	//below values has to be set from quote page, as of now taking from existing insurance
  	  	auraInputService.setAppnumber(14782223482354338)
  	  	auraInputService.setDeathAmt(200000)
  	  	auraInputService.setTpdAmt(200000)
  	  	auraInputService.setIpAmt(2000)
  	  	auraInputService.setWaitingPeriod('30 Days')
  	  	auraInputService.setBenefitPeriod('2 Years')
  	  	auraInputService.setGender('Male')
  	  	auraInputService.setIndustryOcc('026:Transfer')
  	  	auraInputService.setCountry('Australia')
  	  	auraInputService.setSalary('100000')
  	  	auraInputService.setFifteenHr('Yes')*/
  	  	 //end deafult vaues for testing
  		 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
  	  		$scope.auraResponseDataList = response.data.questions;
  	  		angular.forEach($scope.auraResponseDataList, function(Object) {
  				$scope.sectionname = Object.questionAlias.substring(3);

  				});
  	  	});
  	};
	
	if($routeParams.mode == 3  ){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    		var result = res.data[0];
    		var coverDet = {}, occDet ={}, deathAddDet = {},tpdAddDet={}, ipAddDet={}, personalDt = {};
    		
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactType = result.contactType;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		occDet.gender = result.personalDetails.gender;
    		coverDet.transferDeathExistingAmt = result.transferDeathExistingAmt;
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.transferTpdExistingAmt = result.transferTpdExistingAmt;
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.transferIpExistingAmt = result.transferIpExistingAmt;
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium = result.totalPremium;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.previousFundName = result.previousFundName;
    		coverDet.membershipNumber = result.membershipNumber;
    		coverDet.spinNumber = result.spinNumber;
    		coverDet.documentName = result.documentName;
    		coverDet.appNum = result.appNum;
    		coverDet.manageType = "TCOVER";
    		coverDet.partnerCode ="FIRS";
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.age = result.age;
    		coverDet.documentName = (result.transferDocuments && result.transferDocuments.length > 0) ? 'Yes' : 'No';
    		coverDet.documentName === 'Yes' ? PersistenceService.setUploadedFileDetails(result.transferDocuments) : [];
			//$scope.transferCoverDetails = coverDet;

    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		occDet.hazardousQue = result.occupationDetails.hazardousQue;
    		occDet.salary = result.occupationDetails.salary;
    		
    		deathAddDet.deathTransferAmt = result.addnlDeathCoverDetails.deathTransferAmt;
    		deathAddDet.deathTransferCovrAmt = result.addnlDeathCoverDetails.deathTransferCovrAmt?parseInt(result.addnlDeathCoverDetails.deathTransferCovrAmt):0;
    		deathAddDet.deathTransferWeeklyCost = result.addnlDeathCoverDetails.deathTransferWeeklyCost;
    		
    		tpdAddDet.tpdTransferAmt = result.addnlTpdCoverDetails.tpdTransferAmt;
    		tpdAddDet.tpdTransferCovrAmt = result.addnlTpdCoverDetails.tpdTransferCovrAmt?parseInt(result.addnlTpdCoverDetails.tpdTransferCovrAmt):0;
    		tpdAddDet.tpdTransferWeeklyCost = result.addnlTpdCoverDetails.tpdTransferWeeklyCost;

    		ipAddDet.ipTransferAmt = result.addnlIpCoverDetails.ipTransferAmt;
    		ipAddDet.ipTransferCovrAmt = result.addnlIpCoverDetails.ipTransferCovrAmt?parseInt(result.addnlIpCoverDetails.ipTransferCovrAmt):0;
    		ipAddDet.addnlTransferWaitingPeriod = result.addnlIpCoverDetails.addnlTransferWaitingPeriod;
    		ipAddDet.addnlTransferBenefitPeriod = result.addnlIpCoverDetails.addnlTransferBenefitPeriod;
    		ipAddDet.ipTransferWeeklyCost = result.addnlIpCoverDetails.ipTransferWeeklyCost;
    		
    		$scope.svdRtrv = true;
    		coverDet['svRt'] = $scope.svdRtrv;
    		
    		$scope.coverDetails=coverDet;
    	  	$scope.transferOccDetails =occDet;
    	  	$scope.deathAddnlCvrDetails =deathAddDet;
    		$scope.tpdAddnlCvrDetails=tpdAddDet;
    		$scope.ipAddnlCvrDetails=ipAddDet;
    		
    		PersistenceService.settransferCoverDetails($scope.coverDetails);
            //PersistenceService.settransferCoverStateDetails(coverStateObj);
            PersistenceService.setTransferCoverOccDetails($scope.transferOccDetails);
            PersistenceService.setTransferDeathAddnlDetails($scope.deathAddnlCvrDetails);
            PersistenceService.setTransferTpdAddnlDetails($scope.tpdAddnlCvrDetails);
            PersistenceService.setTransferIpAddnlDetails($scope.ipAddnlCvrDetails);
    		
    		
    		init();
    		
    	}, function(err){
    		console.error("Something went wrong while retrieving the details " + JSON.stringify(err));
    	});
	}
	else
		{
		init();
		}
	
  	/*$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.transferOccDetails =PersistenceService.getTransferCoverOccDetails();
  	$scope.deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
	$scope.tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
	$scope.ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();
	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();*/
    console.log($scope.coverDetails);

 // 	console.log(auraInputService);
  	

  	 $scope.updateRadio = function (answerValue, questionObj){
  		console.log($scope.auraResponseDataList)

  		questionObj.arrAns[0]=answerValue;
  		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
  		  auraResponseService.setResponse($scope.auraRes)
  		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {
  			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)

  			   $scope.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data)

  			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
  	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
  	  				//branch complete false for previous questions
  	  				$scope.auraResponseDataList[selectedIndex].error=true;
  	  			}else if(Object.questionComplete){
  	  				$scope.auraResponseDataList[selectedIndex].error=false;
  	  			}

  	  		});

      	}, function () {
      		//console.log('failed');
      	});
  	 };

  	$scope.updateQuestionList = function(questionId,changedQuestion, questionObj){

	  			//$scope.questionlist = Object.questions
	  			angular.forEach($scope.auraResponseDataList, function (Object,index1) {
	  				if(Object.questionId == changedQuestion.questionId){
	  					$scope.auraResponseDataList[index1]= changedQuestion
	  					// $scope.progressive(questionId,changedQuestion,questionObj);
	  					console.log($scope.auraResponseDataList[index1])
	  				}
	  			});

	  	}

  	$scope.proceedNext = function() {
  		$scope.keepGoing= true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true;
  				$scope.keepGoing= false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  				//$scope.go('/transferSummary');
  			}
  		});
  		if($scope.keepGoing){
  			$rootScope.$broadcast('disablepointer');
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
  				PersistenceService.setTransCoverAuraDetails(response.data);
  				$scope.auraResponses = response.data;
		  		if($scope.auraResponses.overallDecision!='DCL'){
						//console.log(response.data);
						$scope.go('/transferSummary/1');
		  		}else if($scope.auraResponses.overallDecision =='DCL'){
		  			if($scope.coverDetails != null && $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null &&
		      		      $scope.transferOccDetails != null && $scope.auraResponses != null && $scope.personalDetails != null){
		      			$scope.coverDetails.lastSavedOn = '';
		      			$scope.details={};
		      			$scope.details.occupationDetails =$scope.transferOccDetails;
		      			$scope.details.addnlDeathCoverDetails=$scope.deathAddnlCvrDetails;
		      			$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlCvrDetails;
		      			$scope.details.addnlIpCoverDetails=$scope.ipAddnlCvrDetails;
		      			$scope.details.transferDocuments = $scope.uploadedFileDetails;
		      			var coverObject = angular.extend($scope.details,$scope.coverDetails);
		              	var auraObject = angular.extend(coverObject,$scope.auraResponses);
		      			var submitObject = angular.extend(auraObject,$scope.personalDetails);
		      			auraResponseService.setResponse(submitObject);
		      			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
		              		console.log(response.data);
		              		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
		              		PersistenceService.setNpsUrl(response.data.npsTokenURL);
	                  		$scope.go('/transferDecline');
		              	}, function(err){
  		            		console.log('Error while submitting transfer cover ' + err);
  		            	});
		              }
		  		}

  	  		});
  		}
  	}

  	$scope.transferAuraSaveAndExitPopUp = function (hhText) {

		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

 	var appNum;
    appNum = PersistenceService.getAppNumber();
  	$scope.saveAuraTransfer = function() {
  		$scope.transferAuraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
  		if($scope.coverDetails != null && $scope.transferOccDetails != null && $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null && $scope.personalDetails != null){
    		$scope.coverDetails.lastSavedOn = 'AuraTransferPage';
    		//$scope.auraDetails
    		$scope.details = {};
	    	$scope.details.addnlDeathCoverDetails = $scope.deathAddnlCvrDetails;
	    	$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlCvrDetails;
	    	$scope.details.addnlIpCoverDetails = $scope.ipAddnlCvrDetails;
	    	$scope.details.occupationDetails = $scope.transferOccDetails;
	    	$scope.details.transferDocuments = $scope.uploadedFileDetails;
    		var temp = angular.extend($scope.coverDetails, $scope.details);
        	var saveAuraTransferObject = angular.extend(temp, $scope.personalDetails);
        	auraResponseService.setResponse(saveAuraTransferObject)
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                console.log(response.data)
	        });
    	}
  	};
  	
  	
  	
  	
  	$scope.collapseOne = true;
  	$scope.toggleOne = function() {
        $scope.collapseOne = !$scope.collapseOne;
    };
  	$scope.collapseTwo = false;
  	$scope.toggleTwo = function() {
        $scope.collapseTwo = !$scope.collapseTwo;
    };
    $scope.collapseThree = false;
  	$scope.toggleThree = function() {
        $scope.collapseThree = !$scope.collapseThree;
    };
    $scope.collapseFour = false;
  	$scope.toggleFour = function() {
        $scope.collapseFour = !$scope.collapseFour;
    };
    $scope.collapseFive = false;
  	$scope.toggleFive = function() {
        $scope.collapseFive = !$scope.collapseFive;
    };
}]);
////
