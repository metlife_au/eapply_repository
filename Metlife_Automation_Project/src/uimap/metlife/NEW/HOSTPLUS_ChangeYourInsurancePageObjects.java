package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class HOSTPLUS_ChangeYourInsurancePageObjects extends PageObjectLibrary{
		
	public static final By ChangeYourInsuranceLink = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Link.xpath");
	public static final By DutyOfDisclosure = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.DutyOfDisclosure.xpath");
	public static final By PrivacyStatement = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.PrivacyStatement.xpath");
	public static final By EmailId = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.EmailId.id");
	public static final By ContactType = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContactType.xpath");
	public static final By ContactNumber = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContactNumber.id");
	public static final By MorningContact = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.MorningContact.id");
	public static final By AfternoonContact = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.AfternoonContact.id");
	public static final By Male = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Gender.Male.id");
	public static final By Female = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Gender.Female.id");
	public static final By ContactDetailsContinue = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContactDetailsContinue.xpath");
	public static final By FifteemHoursYes = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.FifteemHoursYes.id");
	public static final By FifteemHoursNo = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.FifteemHoursNo.id");
	public static final By RegularIncomeYes = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.RegularIncomeYes.id");
	public static final By RegularIncomeNo = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.RegularIncomeNo.id");
	public static final By WorkWithoutInjury = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.WorkWithoutInjury.id");
	public static final By WorkWithInjury = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.WorkWithInjury.id");
	public static final By LimitationYes = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.LimitationYes.id");
	public static final By LimitationNo = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.LimitationNo.id");
	public static final By CitizenYes = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.CitizenYes.id");
	public static final By CitizenNo = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.CitizenNo.id");
	public static final By Industry = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Industry.xpath");
	public static final By Occupation = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Occupation.xpath");
	public static final By OtherOccupation = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.OtherOccupation.id");
	public static final By	OfficeEnvironmentYes	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.OfficeEnvironmentYes.id");
	public static final By	OfficeEnvironmentNo	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.OfficeEnvironmentNo.id");
	public static final By	TertiaryQualYes	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.TertiaryQualYes.id");
	public static final By	TertiaryQualNo	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.TertiaryQualNo.id");
	public static final By	HazardousEncYes	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.HazardousEncYes.id");
	public static final By	HazardousEncNo	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.HazardousEncNo.id");
	public static final By	OutsideOfcPercentYes	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.OutsideOfcPercentYes.id");
	public static final By	OutsideOfcPercentNo	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.OutsideOfcPercentNo.id");
	public static final By	AnnualSalary	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.AnnualSalary.id");
	public static final By	LoadSalaryLabel	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.LoadSalaryLabel.xpath");
	public static final By	ContinueOccupationDetails	=	getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContinueOccupationDetails.xpath");
	public static final By CostofInsurance = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.CostofInsurance.xpath");
	public static final By ActionOnDeath = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ActionOnDeath.xpath");
	public static final By Fixed = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Fixed.xpath");
	public static final By Unitised = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Unitised.xpath");
	public static final By DeathAmount = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.DeathAmount.id");
	public static final By ActionOnTPD = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ActionOnTPD.xpath");
	public static final By TPDAmount = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.TPDAmount.id");
	public static final By ActionOnIP = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ActionOnIP.xpath");
	public static final By Insure90Percent = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.Insure90Percent.xpath");
	public static final By WaitingPeriod = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.WaitingPeriod.xpath");
	public static final By BenefitPeriod = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.BenefitPeriod.xpath");
	
	
	public static final By IPAmount = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.IPAmount.id");
	public static final By LoadDeathAmount = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.LoadDeathAmount.xpath");	
	public static final By LoadTPDAmount = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.LoadTPDAmount.xpath");	
	public static final By LoadIPAmount = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.LoadIPAmount.xpath");
	public static final By CalculateQuote = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.CalculateQuote.xpath");
	public static final By ContinueAfterQuote = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.ContinueAfterQuote.xpath");
	public static final By PopUpContinue = getObjectIdentifaction("HOSTPLUS.ChangeYourInsurance.PopUpContinue.xpath.xpath");
	
	

	
}
