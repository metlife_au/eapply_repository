package au.com.metlife.quote.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteInput", propOrder = {
    "brandName",
    "productName",
    "paymentFrequency",
    "insured",
    "childInsured",
    "riders",
    "multiCoverDiscount"
    
})


@XmlRootElement
public class QuoteInput {
	
	
	@XmlElement(required = true)
	String brandName;
	@XmlElement(required = true)
	int paymentFrequency;
	@XmlElement(required = true)
	Insured insured[];
	
	Insured childInsured[]=null;	
	Cover riders[]=null;
	
	String productName;
	String multiCoverDiscount;
	
	public Insured[] getChildInsured() {
		return childInsured;
	}
	public void setChildInsured(Insured[] childInsured) {
		this.childInsured = childInsured;
	}
	public Insured[] getInsured() {
		return insured;
	}
	public void setInsured(Insured[] insured) {
		this.insured = insured;
	}
	public int getPaymentFrequency() {
		return paymentFrequency;
	}
	public void setPaymentFrequency(int paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}
	
	public Cover[] getRiders() {
		return riders;
	}
	public void setRiders(Cover[] riders) {
		this.riders = riders;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getMultiCoverDiscount() {
		return multiCoverDiscount;
	}
	public void setMultiCoverDiscount(String multiCoverDiscount) {
		this.multiCoverDiscount = multiCoverDiscount;
	}
	
	
}


