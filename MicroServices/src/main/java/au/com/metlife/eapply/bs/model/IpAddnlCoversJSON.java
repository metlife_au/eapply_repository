package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class IpAddnlCoversJSON implements Serializable{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 5546964965989436801L;

	private String benefitPeriod;
	
	private String ipCoverName;
	
	private String ipCoverPremium;
	
	private String ipCoverType;
	
	private String ipFixedAmt;
	
	private String ipInputTextValue;
	
	private String waitingPeriod;
	
	private String ipTransferCoverType;
	
	private String addnlTransferBenefitPeriod;
	
	private String addnlTransferWaitingPeriod;
	
	private BigDecimal ipTransferAmt;
	
	private String ipTransferCovrAmt;
	
	private String ipTransferWeeklyCost;
	
	private String newWaitingPeriod;
	
	private String newBenefitPeriod;
	
	private String newIpLabelAmt;
	
	private String newIpUpdatedCover;
	
	private String newIpCoverPremium;
	
	private String newIpCoverType;
	
	private String ipLoadingCost;
	
	private String totalipwaitingperiod;
	
	private String totalipbenefitperiod;
	
	//Added for Care additional Units for IP
	private String ipAddnlUnits;
	

	public String getIpLoadingCost() {
		return ipLoadingCost;
	}

	public void setIpLoadingCost(String ipLoadingCost) {
		this.ipLoadingCost = ipLoadingCost;
	}

	public String getNewIpCoverType() {
		return newIpCoverType;
	}

	public void setNewIpCoverType(String newIpCoverType) {
		this.newIpCoverType = newIpCoverType;
	}

	public String getNewWaitingPeriod() {
		return newWaitingPeriod;
	}

	public void setNewWaitingPeriod(String newWaitingPeriod) {
		this.newWaitingPeriod = newWaitingPeriod;
	}

	public String getNewBenefitPeriod() {
		return newBenefitPeriod;
	}

	public void setNewBenefitPeriod(String newBenefitPeriod) {
		this.newBenefitPeriod = newBenefitPeriod;
	}

	public String getNewIpLabelAmt() {
		return newIpLabelAmt;
	}

	public void setNewIpLabelAmt(String newIpLabelAmt) {
		this.newIpLabelAmt = newIpLabelAmt;
	}

	public String getNewIpUpdatedCover() {
		return newIpUpdatedCover;
	}

	public void setNewIpUpdatedCover(String newIpUpdatedCover) {
		this.newIpUpdatedCover = newIpUpdatedCover;
	}

	public String getNewIpCoverPremium() {
		return newIpCoverPremium;
	}

	public void setNewIpCoverPremium(String newIpCoverPremium) {
		this.newIpCoverPremium = newIpCoverPremium;
	}

	public String getIpTransferCoverType() {
		return ipTransferCoverType;
	}

	public void setIpTransferCoverType(String ipTransferCoverType) {
		this.ipTransferCoverType = ipTransferCoverType;
	}

	public String getAddnlTransferBenefitPeriod() {
		return addnlTransferBenefitPeriod;
	}

	public void setAddnlTransferBenefitPeriod(String addnlTransferBenefitPeriod) {
		this.addnlTransferBenefitPeriod = addnlTransferBenefitPeriod;
	}

	public String getAddnlTransferWaitingPeriod() {
		return addnlTransferWaitingPeriod;
	}

	public void setAddnlTransferWaitingPeriod(String addnlTransferWaitingPeriod) {
		this.addnlTransferWaitingPeriod = addnlTransferWaitingPeriod;
	}

	public BigDecimal getIpTransferAmt() {
		return ipTransferAmt;
	}

	public void setIpTransferAmt(BigDecimal ipTransferAmt) {
		this.ipTransferAmt = ipTransferAmt;
	}

	public String getIpTransferCovrAmt() {
		return ipTransferCovrAmt;
	}

	public void setIpTransferCovrAmt(String ipTransferCovrAmt) {
		this.ipTransferCovrAmt = ipTransferCovrAmt;
	}

	public String getIpTransferWeeklyCost() {
		return ipTransferWeeklyCost;
	}

	public void setIpTransferWeeklyCost(String ipTransferWeeklyCost) {
		this.ipTransferWeeklyCost = ipTransferWeeklyCost;
	}

	public String getBenefitPeriod() {
		return benefitPeriod;
	}

	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	public String getIpCoverName() {
		return ipCoverName;
	}

	public void setIpCoverName(String ipCoverName) {
		this.ipCoverName = ipCoverName;
	}

	public String getIpCoverPremium() {
		return ipCoverPremium;
	}

	public void setIpCoverPremium(String ipCoverPremium) {
		this.ipCoverPremium = ipCoverPremium;
	}

	public String getIpCoverType() {
		return ipCoverType;
	}

	public void setIpCoverType(String ipCoverType) {
		this.ipCoverType = ipCoverType;
	}

	public String getIpFixedAmt() {
		return ipFixedAmt;
	}

	public void setIpFixedAmt(String ipFixedAmt) {
		this.ipFixedAmt = ipFixedAmt;
	}

	public String getIpInputTextValue() {
		return ipInputTextValue;
	}

	public void setIpInputTextValue(String ipInputTextValue) {
		this.ipInputTextValue = ipInputTextValue;
	}

	public String getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public String getTotalipwaitingperiod() {
		return totalipwaitingperiod;
	}

	public void setTotalipwaitingperiod(String totalipwaitingperiod) {
		this.totalipwaitingperiod = totalipwaitingperiod;
	}

	public String getTotalipbenefitperiod() {
		return totalipbenefitperiod;
	}

	public void setTotalipbenefitperiod(String totalipbenefitperiod) {
		this.totalipbenefitperiod = totalipbenefitperiod;
	}

	public String getIpAddnlUnits() {
		return ipAddnlUnits;
	}

	public void setIpAddnlUnits(String ipAddnlUnits) {
		this.ipAddnlUnits = ipAddnlUnits;
	}
	
}
