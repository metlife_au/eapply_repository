export default class EapplyData {
  constructor(AppConstants, $http, $state, $q, ngDialog, CommonAPI) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;
    this._ngDialog = ngDialog;
    this._CommonAPI = CommonAPI;
    this.urlList = this._CommonAPI.getUrlList();
    this.exDthCover = null;
    this.exTpdCover = null;
    this.exIpCover = null;
    this.authToken = this._CommonAPI.getAuthToken();
    
    this.eApplyData = {
      "partnerCode": "INGD",
      "lastSavedOn": "",
      "requestType": "",
      "clientRefNumber": "",
      "totalMonthlyPremium": 0,
      "applicatioNumber": "0",
      "ackCheck": false,
      "privacyCheck": false,
      "fulCheck": false,
      "dodCheck": false,
      "appDecision": "",
      "clientMatchReason": "",
      "clientMatch": null,
      "auraDisabled": "true",
      "deathExclusions": "",
      "tpdExclusions": "",
      "ipExclusions": "",
      "deathLoading": null,
      "tpdLoading": null,
      "ipLoading": null,
      "auraType": "",
      "coverType": "",
      "applicant": {
        "firstName": "",
        "lastName": "",
        "birthDate": "",
        "emailId": "",
        "contactType": "",
        "contactNumber": "",
        "smoker": "",
        "title": "",
        "age": "0",
        "memberType": "INDUSTRY",
        "gender": "",
        "customerReferenceNumber": null,
        "clientRefNumber": "",
        "australiaCitizenship": "",
        "dateJoinedFund": "",
        "workHours": "",
        "industryType": "",
        "occupationCode": "",
        "occupation": "",
        "spendTimeOutside": null,
        "annualSalary": "",
        "occupationDuties": null,
        "otherOccupation": "",
        "workDuties": null,
        "ownBusinessQue": "",
        "ownBusinessYesQue": null,
        "ownBusinessNoQue": "",
        "tertiaryQue": null,
        "permanenTemplQue": "",
        "thirtyFiveHrsWrkQue": "",
        "overallCoverDecision": "",
        "previousInsurerName": "",
        "fundMemPolicyNumber": "",
        "spinNumber": "",
        "previousTpdClaimQue": "",
        "previousTerminalIllQue": "",
        "documentEvidence": "",
        "lifeEvent": null,
        "lifeEventDate": null,
        "eventAlreadyApplied": null,
        "cancelReason": null,
        "cancelOtherReason": null,
        "previousTpdBenefit": null,
        "relation": "",
        "indexationDeath": null,
        "indexationTpd": null,
        "unitisedCovers": null,
        "documentAddress": null,
        "contactDetails": {
          "preferedContactTime": "",
          "mobilePhone": "",
          "workPhone": "",
          "homePhone": "",
          "preferedContactNumber": "",
          "preferedContacType": "",
          "country": "Australia",
          "addressLine1": "2 Park stt",
          "addressLine2": "",
          "postCode": "2000",
          "state": "NSW",
          "suburb": "Sydney",
          "otherContactType": "",
          "otherContactNumber": null
        },
        "cover": [
          {
            "benefitType": "1",
            "coverDecision": "",
            "coverReason": "",
            "coverExclusion": null,
            "existingCoverAmount": 0,
            "fixedUnit": "0",
            "loading": null,
            "policyFee": null,
            "occupationRating": "Professional",
            "coverOption": null,
            "cost": "0.0",
            "coverCategory": "DcTailored",
            "deathInputTextValue": null,
            "tpdInputTextValue": null,
            "ipInputTextValue": null,
            "deathFixedAmt": null,
            "additionalCoverAmount": "0",
            "frequencyCostType": "Monthly",
            "optionalUnit": 0,
            "existingIpBenefitPeriod": null,
            "existingIpWaitingPeriod": null,
            "additionalUnit": null,
            "additionalIpBenefitPeriod": null,
            "additionalIpWaitingPeriod": null,
            "transferCoverAmount": null,
            "totalIpWaitingPeriod": null,
            "totalIpBenefitPeriod": null
          },
          {
            "benefitType": "2",
            "coverDecision": "",
            "coverReason": "",
            "coverExclusion": null,
            "existingCoverAmount": 0,
            "fixedUnit": "0",
            "loading": null,
            "policyFee": null,
            "occupationRating": "Professional",
            "coverOption": null,
            "cost": "0.0",
            "coverCategory": "TPDTailored",
            "deathInputTextValue": null,
            "tpdInputTextValue": "",
            "ipInputTextValue": null,
            "deathFixedAmt": null,
            "additionalCoverAmount": "0",
            "frequencyCostType": "Monthly",
            "optionalUnit": 0,
            "existingIpBenefitPeriod": null,
            "existingIpWaitingPeriod": null,
            "additionalUnit": null,
            "additionalIpBenefitPeriod": null,
            "additionalIpWaitingPeriod": null,
            "transferCoverAmount": null,
            "totalIpWaitingPeriod": null,
            "totalIpBenefitPeriod": null
          },
          {
            "benefitType": "4",
            "coverDecision": "",
            "coverReason": "",
            "coverExclusion": null,
            "existingCoverAmount": 0,
            "fixedUnit": "0",
            "loading": null,
            "policyFee": null,
            "occupationRating": "Professional",
            "coverOption": null,
            "cost": "0.0",
            "coverCategory": "IpTailored",
            "deathInputTextValue": null,
            "tpdInputTextValue": null,
            "ipInputTextValue": "",
            "deathFixedAmt": null,
            "additionalCoverAmount": "0",
            "frequencyCostType": "Monthly",
            "optionalUnit": 0,
            "existingIpBenefitPeriod": "Age 67",
            "existingIpWaitingPeriod": "60 Days",
            "additionalUnit": null,
            "additionalIpBenefitPeriod": "Age 67",
            "additionalIpWaitingPeriod": "60 Days",
            "transferCoverAmount": null,
            "totalIpWaitingPeriod": "",
            "totalIpBenefitPeriod": ""
          }
        ],
      "eventDesc": null,
      "insuredSalary": ""
      },
      "transferDocuments": null,
      "lifeEventDocuments": null,
      "coverCancellation": {}
    };
  
    // this.eApplyData = {
    //   'requestType':'',
    //   'partnerCode':'INGD',
    //   'lastSavedOn':'',
    //   'clientRefNumber':null,
    //   'totalMonthlyPremium':null,
    //   'applicatioNumber':null,
    //   'ackCheck':false,
    //   'privacyCheck':false,
    //   'dodCheck':false,
    //   'fulCheck':false,
    //   'appDecision':'',  
    //   'clientMatchReason':'',
    //   'clientMatch':'',
    //   'auraDisabled':true,
    //   'responseObject':null,  
    //   'deathExclusions':'', 
    //   'tpdExclusions':'',	
    //   'ipExclusions':'',
    //   'applicant': {
    //     'firstName':'',
    //     'lastName':'',
    //     'birthDate':'',
    //     'emailId':'',
    //     'contactType':'',
    //     'contactNumber':'',
    //     'smoker':'',
    //     'title':'',
    //     'age':'',
    //     'memberType':'',
    //     'gender':'',
    //     'customerReferenceNumber':'',
    //     'clientRefNumber':null,
    //     'australiaCitizenship':'',
    //     'dateJoinedFund':'',
    //     'workHours':'',
    //     'industryType':'',
    //     'occupationCode':'',
    //     'occupation':'',
    //     'spendTimeOutside':null,
    //     'annualSalary':'',
    //     'occupationDuties':null,
    //     'otherOccupation':'',
    //     'workDuties':null,
    //     'ownBusinessQue':'',
    //     'ownBusinessYesQue':null,
    //     'ownBusinessNoQue':'',
    //     'tertiaryQue':null,
    //     'permanenTemplQue':'',
    //     'thirtyFiveHrsWrkQue':'',
    //     'relation':'',
    //     'overallCoverDecision':'',
    //     'previousInsurerName':'',
    //     'fundMemPolicyNumber':'',
    //     'spinNumber':'',
    //     'previousTpdClaimQue':'',
    //     'previousTerminalIllQue':'',
    //     'documentEvidence':'',
    //     'contactDetails':{
    //     'preferedContactTime':'',
    //     'mobilePhone': '',
    //     'workPhone': '',
    //     'homePhone': '',
    //     'preferedContactNumber':'',
    //     'preferedContacType':'',
    //     'country':'Australia',
    //     'addressLine1':'',
    //     'addressLine2':'',
    //     'postCode':'',
    //     'state':'',
    //     'suburb':'',
    //     'otherContactType':''
    //     },
    //     'cover':[
    //       {
    //         'benefitType':'1',
    //         'coverDecision':'',
    //         'coverReason':'',
    //         'existingCoverAmount':null,
    //         'fixedUnit':0,
    //         'transferCoverAmount':'',
    //         'occupationRating':'',
    //         'cost':null,
    //         'coverCategory':'DcFixed',
    //         'additionalCoverAmount':0,
    //         'frequencyCostType':'Monthly',
    //         'optionalUnit':'',
    //       },
    //       {
    //         'benefitType':'2',
    //         'coverDecision':'',
    //         'coverReason':'',
    //         'existingCoverAmount':'',
    //         'fixedUnit':0,
    //         'transferCoverAmount':'',
    //         'occupationRating':'',
    //         'cost':null,
    //         'coverCategory':'TPDFixed',
    //         'tpdInputTextValue':'',
    //         'additionalCoverAmount':0,
    //         'frequencyCostType':'Monthly',
    //         'optionalUnit':'',
    //       },
    //       {
    //         'benefitType':'4',
    //         'coverDecision':'',
    //         'coverReason':'',
    //         'existingCoverAmount':'',
    //         'transferCoverAmount':'',
    //         'ipInputTextValue':'',
    //         'optionalUnit':'',
    //         'existingIpBenefitPeriod':'',
    //         'existingIpWaitingPeriod':'',
    //         'fixedUnit':0,
    //         'occupationRating':'',
    //         'cost':0,
    //         'coverCategory':'IpUnitised',
    //         'additionalCoverAmount':0,
    //         'additionalUnit':null,
    //         'additionalIpBenefitPeriod':'',
    //         'additionalIpWaitingPeriod':'',
    //         'totalIpWaitingPeriod':'',
    //         'totalIpBenefitPeriod':'',
    //         'frequencyCostType':'Monthly'
    //       }
    //     ]
    //   }
    // };

    this.calcData = {
      age: 33,
      fundCode: "INGD",
      gender: "MALE",
      deathOccCategory: "Professional",
      tpdOccCategory: "Professional",
      ipOccCategory: "Professional",
      smoker: false,
      deathUnits: 0,
      deathFixedAmount: 350000,
      deathFixedCost: 0,
      deathUnitsCost: 0,
      tpdUnits: 0,
      tpdFixedAmount: 300000,
      tpdCoverStartDate: "17/07/2012",
      ipCoverStartDate: "17/07/2012",
      stateCode: "NSW",
      tpdFixedCost: 0,
      tpdUnitsCost: 0,
      annualSalary: 100000,
      ipUnits: 0,
      ipFixedAmount: 7083,
      ipFixedCost: 0,
      ipUnitsCost: 0,
      premiumFrequency: "Monthly",
      memberType: "INDUSTRY",
      deathCoverType: "DcFixed",
      tpdCoverType: "TPDFixed",
      ipCoverType: "IpFixed",
      manageType: "TCOVER",
      ipBenefitPeriod: "2 Years",
      ipWaitingPeriod: "60 Days",
      deathTransferAmount: 0,
      tpdTransferAmount: 0,
      ipTransferAmount: 0,
      deathExistingAmount: 350000,
      tpdExistingAmount: 300000,
      ipExistingAmount: 7083,
      exDeathCoverType: 2,
      exTpdCoverType: 2,
      exIpCoverType: 2,
      ipExBenefitPeriod: "2 Years",
      ipExWaitingPeriod: "60 Days"
    };

    this.auraTransferData = {
      fund: "INGD",
      mode: "TransferCover",
      age: 59,
      name: "INGD INGDB",
      appnumber: 1524645189746,
      gender: "Female",
      country: "Australia",
      fifteenHr: "Yes",
      deathAmt: 1,
      tpdAmt: 1,
      ipAmt: 1,
      waitingPeriod: "90 Days",
      benefitPeriod: "2 Years",
      industryOcc: "001:Business Analyst/Consultant",
      salary: "120000",
      clientname: "metaus",
      lastName: "INGDB",
      firstName: "INGD",
      dob: "01/11/1958",
      existingTerm: false,
      memberType: "None"
    };


  }

/*
* Application client data will be fetched (fetchClientData) and stored (setClientData) here 
* to reference (getClientData) through out the application
*/
  fetchClientData(path) {
    this.authToken = this._CommonAPI.getAuthToken();
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'POST',
      data: {
        tokenId: this.authToken
      }
    }).then(
      (res) => {
        this.setClientData(res.data);
        deferred.resolve(res.data);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }

  setClientData(data) {
    this.clientData = data;
  }

  getClientData() {
    return this.clientData || null;
  }

  setApplicantRawXmlData(data) {
    this.applicantRawXmlData = data;
    this.convertRawXmlCoversToApplicationCovers(data.existingCovers.cover);
    this.convertRawXmlToApplicationData();
  }

  getApplicantRawXmlData() {
    return this.applicantRawXmlData;
  }

  setApplicantData(data) {
    this.applicantData = data;
  }

  getApplicantData() {
    return this.applicantData;
  }

/*
* Application cover data will be stored and referenced throught the app here
*  
*/

convertRawXmlToApplicationData() {
  
  this.eApplyData.applicant.age = this.applicantRawXmlData.age;
  this.eApplyData.applicant.dateJoinedFund = this.applicantRawXmlData.dateJoined;
  this.eApplyData.clientRefNumber = this.applicantRawXmlData.clientRefNumber;
  this.eApplyData.applicant.clientRefNumber = this.applicantRawXmlData.clientRefNumber;
  this.eApplyData.applicant.memberType = this.applicantRawXmlData.memberType;
  
  //personalDetails
  this.eApplyData.applicant.firstName = this.applicantRawXmlData.personalDetails.firstName;
  this.eApplyData.applicant.lastName = this.applicantRawXmlData.personalDetails.lastName;
  this.eApplyData.applicant.gender = this.applicantRawXmlData.personalDetails.gender;
  this.eApplyData.applicant.birthDate =  this.applicantRawXmlData.personalDetails.dateOfBirth;
  this.eApplyData.applicant.title =  this.applicantRawXmlData.personalDetails.title;

  //contactDetails
  this.eApplyData.applicant.emailId = this.applicantRawXmlData.contactDetails.emailAddress;
  this.eApplyData.applicant.contactDetails.preferedContactTime = this.applicantRawXmlData.contactDetails.prefContactTime;
  this.eApplyData.applicant.contactDetails.preferedContacType = this.applicantRawXmlData.contactDetails.prefContact;
  this.eApplyData.applicant.customerReferenceNumber  = this.applicantRawXmlData.contactDetails.fundEmailAddress;
  this.eApplyData.applicant.contactDetails.homePhone  = this.applicantRawXmlData.contactDetails.homePhone;
  this.eApplyData.applicant.contactDetails.mobilePhone  = this.applicantRawXmlData.contactDetails.mobilePhone;
  this.eApplyData.applicant.contactDetails.workPhone  = this.applicantRawXmlData.contactDetails.workPhone;
  
  this.eApplyData.applicant.contactDetails.addressLine1 = this.applicantRawXmlData.address.line1;
  this.eApplyData.applicant.contactDetails.addressLine2 = this.applicantRawXmlData.address.line2;
  this.eApplyData.applicant.contactDetails.postCode = this.applicantRawXmlData.address.postCode;
  this.eApplyData.applicant.contactDetails.country = this.applicantRawXmlData.address.country;
  this.eApplyData.applicant.contactDetails.state = this.applicantRawXmlData.address.state;
  this.eApplyData.applicant.contactDetails.suburb = this.applicantRawXmlData.address.suburb; 
  
  this.setApplicantData(this.eApplyData);
}

populateData(flowObj) {
  // data population
  if (this.applicantRawXmlData && this.applicantRawXmlData.contactDetails.prefContactTime) {
    if (this.applicantRawXmlData.contactDetails.prefContactTime == '1') {
      flowObj.applicant.contactDetails.preferedContactTime = 'Morning (9am - 12pm)';
    } else {
      flowObj.applicant.contactDetails.preferedContactTime = 'Afternoon (12pm - 6pm)';
    }
  }
  
  if (this.applicantRawXmlData.contactDetails.prefContact == null || this.applicantRawXmlData.contactDetails.prefContact == '') {
    this.applicantRawXmlData.contactDetails.prefContact = 1;
  }
  
  if (this.applicantRawXmlData && this.applicantRawXmlData.contactDetails.prefContact) {
    if (this.applicantRawXmlData.contactDetails.prefContact == '1') {
      flowObj.applicant.contactDetails.preferedContacType = 'Mobile';
      flowObj.applicant.contactDetails.preferedContactNumber = this.applicantRawXmlData.contactDetails.mobilePhone;
    } else if (this.applicantRawXmlData.contactDetails.prefContact == '2') {
      flowObj.applicant.contactDetails.preferedContacType = 'Home';
      flowObj.applicant.contactDetails.preferedContactNumber = this.applicantRawXmlData.contactDetails.homePhone;
    } else if (this.applicantRawXmlData.contactDetails.prefContact == '3') {
      flowObj.applicant.contactDetails.preferedContacType = 'Work';
      flowObj.applicant.contactDetails.preferedContactNumber = this.applicantRawXmlData.contactDetails.workPhone;
    }
  }
  return flowObj;
}
convertRawXmlCoversToApplicationCovers(covers) {
  let tempExistingCovers = covers;
  let tempExDthCover = null;
  let tempExTpdCover = null;
  let tempExIpCover = null;
  const _this = this;
  
  angular.forEach(tempExistingCovers, function (value, key) {
    
	//this.cleanObj(value);
    if(value.benefitType === '1') {
      //let tempExDthCover = angular.extend({}, this._EapplyData.defaultCovers[0], value);
      tempExDthCover = {
        'benefitType':'1',
        'coverDecision':'',
        'coverReason':'', 
        'existingCoverAmount': value.amount,
        'fixedUnit':0,
        'transferCoverAmount':'',
        'occupationRating': value.occRating || 'White Collar',
        'cost':null,
        'coverCategory': value.type === '2' ? 'DcTailored' : 'DcAutomatic',
        'existingCoverCategory': value.type === '2' ? 'DcTailored' : 'DcAutomatic',
        'additionalCoverAmount':value.amount,
        'frequencyCostType':'Monthly',
        'optionalUnit':'',        
        'coverStartDate': value.coverStartDate,
        'exclusions': value.exclusions,
        'loading': value.loading
      };
      
    } else if(value.benefitType === '2') {
      tempExTpdCover = {
        'benefitType':'2',
        'coverDecision':'',
        'coverReason':'',
        'existingCoverAmount': value.amount,
        'fixedUnit':0,
        'transferCoverAmount':'',
        'occupationRating': value.occRating || 'White Collar',
        'cost':null,
        'coverCategory': value.type === '2' ? 'TPDTailored' : 'TPDAutomatic',
        'existingCoverCategory': value.type === '2' ? 'TPDTailored' : 'TPDAutomatic',
        'tpdInputTextValue':'',
        'additionalCoverAmount':value.amount,
        'frequencyCostType':'Monthly',
        'optionalUnit':'',
        'coverStartDate': value.coverStartDate,
        'exclusions': value.exclusions,
        'loading': value.loading
      };
      //let tempExTpdCover = angular.extend({}, this._EapplyData.defaultCovers[1], value);
      
    } else if(value.benefitType === '4') {
      tempExIpCover = {
        'benefitType':'4',
        'coverDecision':'',
        'coverReason':'',
        'existingCoverAmount': value.amount,
        'transferCoverAmount':'',
        'ipInputTextValue':'',
        'optionalUnit':'',
        'existingIpBenefitPeriod': value.benefitPeriod ? _this.toTitleCase(value.benefitPeriod) : '2 Years',
        'existingIpWaitingPeriod': value.waitingPeriod ? _this.toTitleCase(value.waitingPeriod) : '90 Days',
        'fixedUnit':0,
        'occupationRating':value.occRating || 'White Collar',
        'cost':0,
        'coverCategory': value.type === '2' ? 'IpTailored' : 'IpTailored',
        'existingCoverCategory': value.type === '2' ? 'IpTailored' : 'IpTailored',
        'additionalCoverAmount':value.amount,
        'additionalUnit':null,
        'additionalIpBenefitPeriod': value.benefitPeriod ? _this.toTitleCase(value.benefitPeriod): '2 Years',
        'additionalIpWaitingPeriod': value.waitingPeriod ? _this.toTitleCase(value.waitingPeriod): '90 Days',
        'totalIpWaitingPeriod': value.waitingPeriod || '90 Days',
        'totalIpBenefitPeriod': value.benefitPeriod || '2 Years',
        'frequencyCostType':'Monthly',
        'coverStartDate': value.coverStartDate,
        'exclusions': value.exclusions,
        'loading': value.loading
      };
      //let tempExIpCover = angular.extend({}, this._EapplyData.defaultCovers[2], value);
      
    }
  });
  _this.setExistingDeathCover(tempExDthCover);
  _this.setExistingTpdCover(tempExTpdCover);
  _this.setExistingIpCover(tempExIpCover);
}

toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

/*
* Application cover data will be stored and referenced throught the app here
*  
*/ 

  fetchCoversData() {
    return this.defaultCovers;
  }

  setExistingDeathCover(cover) {
    this.exDthCover = cover;
  }

  setExistingTpdCover(cover) {
    this.exTpdCover = cover;
  }

  setExistingIpCover(cover) {
    this.exIpCover = cover;
  }
  getExistingDeathCover() {
    return this.exDthCover || this.defaultCovers[0];
  }

  getExistingTpdCover() {
    return this.exTpdCover || this.defaultCovers[1];
  }

  getExistingIpCover() {
    return this.exIpCover || this.defaultCovers[2];
  }
/*
* Application data will be manipulated here 
* and referenced through out the application
*/
  setCancelData(data) {
    this.cancelData = data;
  }

  getCancelData() {
    return this.cancelData || null;
  }

  setTransferData(data) {
    this.transferData = data;
  }

  getTransferData() {
    return this.transferData || null;
  }
  
  setPdfLocation(data) {
	  this.pdfLocation = data;  
  }
  
  getPdfLocation() {
	  return this.pdfLocation || null;  
  }
  
  setNpsUrl(data) {
	  this.NpsUrl = data;  
  }
  
  getNpsUrl() {
	  return this.NpsUrl || null;  
  }
  
  setChangeCoverData(data) {
	 this.changeCoverData = data;
  }

  getChangeCoverData() {
	  return this.changeCoverData || null;
  }
  
  setTransferData(data) {
	  	this.transferData = data;
  }
  
  getTransferData() {
	  return this.transferData || null;
  }

  resetAllFlows() {
    this.setCancelData(null);
  }

  changeState(path) {
    this._$state.go(path);
  }

  /*
  * Application exception 
  * Falsy data will be notified with popup and routes the application to alnding page
  */
  exitOnError() {
    const that = this;
    this._ngDialog.openConfirm({
      template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row"><div class="col-12"><p> Something went wrong, do you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer"><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button></div></div>',
      plain: true,
      showClose: false,
      className: 'ngdialog-theme-plain custom-width'
    }).then(
      () => {
        that.resetAllFlows();
        that.changeState('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }

  /*
* Checking for duplicate application submits for all cover flows
*/
checkDuplicateApps(path, manageType) {
  let applicantData = this.getApplicantData() || {};
  let deferred = this._$q.defer();
  this._$http({
    url: path,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    params: {
      fundCode: 'INGD',
      clientRefNo: this._CommonAPI.getCRN(),
      manageTypeCC: manageType,
      dob: applicantData.applicant.birthDate,
      firstName: applicantData.applicant.firstName,
      lastName: applicantData.applicant.lastName
    }
  }).then(
    (res) => {
      deferred.resolve(res.data);
    },

    (err) => {
      deferred.reject(err);
    }
  );
  return deferred.promise;
}

/*
* Application data retrieval 
* can be used in save & retrieve and other flows
*/

  retrieveApp(app) {
    let url = this.urlList.retrieveAppUrlNew + '?applicationNumber=' + app.applicationNumber;
    this._CommonAPI.retieveEapply(url).then(                			
      (res) => {
                    			    
        console.log(res);
        switch (app.lastSavedOnPage.toLowerCase()) {
          case 'quotepage':
        	  this.setChangeCoverData(res[0]);
        	  this.changeState('app.quote');
        	  break;
          case 'aurapage':
        	  this.setChangeCoverData(res[0]);
        	  this.changeState('app.quoteaura');
        	  break;
          case 'summarypage':
        	  this.setChangeCoverData(res[0]);
        	  this.changeState('app.quotesummary');
        	  break;
          case 'transfercover':
        	  this.setTransferData(res[0]);
        	  this.changeState('app.transfercover');
        	  break;
          case 'transferaura':
        	  this.setTransferData(res[0]);
        	  this.changeState('app.transferaura');
        	  break;
          case 'transfersummary':
        	  this.setTransferData(res[0]);
        	  this.changeState('app.transfersummary');
        	  break;
          default:
            break;
        }               		     
      },
      (err) => {
        this.errors = err.data.errors;
      }               		     
    );
  }

    // Utility to clean undefined, null and empty keys from Object
    cleanObj(value) {
      Object.keys(value).forEach((key) => (value[key] == null || value[key] === '') && delete value[key]);
    }
}
