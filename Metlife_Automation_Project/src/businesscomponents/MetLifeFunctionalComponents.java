package businesscomponents;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;

import pages.metlife.AEISPersonalStatementDetailsPage;
import pages.metlife.ALIAcceptedPage;
import pages.metlife.ALIConfirmationPage;
import pages.metlife.ALIDeclinedPage;
import pages.metlife.ALIGetQuotePage;
import pages.metlife.ALIHealthDetailsPage;
import pages.metlife.ALIHealthOverviewPage;
import pages.metlife.ALIPaymentsPage;
import pages.metlife.ALISaveandExitPage;
import pages.metlife.ALISummaryPage;
import pages.metlife.ColesAddkidPage;
import pages.metlife.ColesCCConfirmPage;
import pages.metlife.ColesCCExactQtPage;
import pages.metlife.ColesCCGetQuotePage;
import pages.metlife.ColesCCHealthAPage;
import pages.metlife.ColesCCPaymentPage;
import pages.metlife.ColesCCStubPage;
import pages.metlife.ColesConfirmPage;
import pages.metlife.ColesConfirmationPage;
import pages.metlife.ColesExactQuotePage;
import pages.metlife.ColesGetQuotePage;
import pages.metlife.ColesHealthAPage;
import pages.metlife.ColesHealthBPage;
import pages.metlife.ColesPaymentPage;
import pages.metlife.CoverDetailsPage;
import pages.metlife.CyberSourceLoginDetailsPage;
import pages.metlife.DPStubPage;
import pages.metlife.DeclarationAndConfirmationDetailsPage;
import pages.metlife.EServiceLoginPage;
import pages.metlife.EServicePolicySearch;
import pages.metlife.EServiceRaiseRequest;
import pages.metlife.EViewLoginPage;
import pages.metlife.EViewSearchUnderwritePDF;
import pages.metlife.EmailPDFVerificationPage;
import pages.metlife.GUILDPersonalStatementDetailsPage;
import pages.metlife.HOSTPLUSPersonalStatementDetailsPage;
import pages.metlife.HostplusManageInsurancePage;
import pages.metlife.INGDCancelInsurancePage;
import pages.metlife.INGDManageInsurancePage;
import pages.metlife.INGDNewInsurancePage;
import pages.metlife.INGDPersonalStatementDetailsPage;
import pages.metlife.INGDSaveCover;
import pages.metlife.INGDTransferInsurancePage;
import pages.metlife.MetflowLoginDetailsPage;
import pages.metlife.Mortgage1300AcceptedPage;
import pages.metlife.Mortgage1300PaymentPage;
import pages.metlife.Mortgage1300QuotePage;
import pages.metlife.NSFSNewInsurancePage;
import pages.metlife.NSFSPersonalStatementDetailsPage;
import pages.metlife.OAMPSAcceptedPage;
import pages.metlife.OAMPSConfirmationPage;
import pages.metlife.OAMPSDeclinePage;
import pages.metlife.OAMPSGetQuotePage;
import pages.metlife.OAMPSHealthPage;
import pages.metlife.OAMPSPaymentsPage;
import pages.metlife.OAMPSSaveandExitPage;
import pages.metlife.OAMPSSummaryPage;
import pages.metlife.PSUPPersonalStatementDetailsPage;
import pages.metlife.PWCPersonalInformationPage;
import pages.metlife.PersonalDetailsPage;
import pages.metlife.PersonalStatementDetailsPage;
import pages.metlife.REISManageInsurancePage;
import pages.metlife.SFPSNewInsurancePage;
import pages.metlife.SFPSPersonalStatementDetailsPage;
import pages.metlife.STFTBeneficiariesPage;
import pages.metlife.STFTCCConfirmationPage;
import pages.metlife.STFTCCGetQuotePage;
import pages.metlife.STFTCCHealthPage;
import pages.metlife.STFTCCPaymentsPage;
import pages.metlife.STFTCCStubPage;
import pages.metlife.STFTCCSummaryPage;
import pages.metlife.STFTConfirmationPage;
import pages.metlife.STFTDeclinePage;
import pages.metlife.STFTGetQuotePage;
import pages.metlife.STFTHealthPage;
import pages.metlife.STFTPaymentsPage;
import pages.metlife.STFTSummaryPage;
import pages.metlife.VICSManageInsurancePage;
import pages.metlife.VICSPersonalStatementDetailsPage;
import pages.metlife.VOWCoverDetailPage;
import pages.metlife.VOWQuotePage;
import pages.metlife.VOWSummaryPage;
import pages.metlife.VOWWebResponsePage;
import pages.metlife.ValidateDetailsPage;
import pages.metlife.YBRCCCoverDetailPage;
import pages.metlife.YBRCoverDetailPage;
import pages.metlife.YBRIPAcceptedPage;
import pages.metlife.YBRIPConfirmationPage;
import pages.metlife.YBRIPDeclinedPage;
import pages.metlife.YBRIPGetQuotePage;
import pages.metlife.YBRIPHealthDetailsPage;
import pages.metlife.YBRIPHealthOverviewPage;
import pages.metlife.YBRIPPaymentsPage;
import pages.metlife.YBRIPSaveandExitPage;
import pages.metlife.YBRIPSummaryPage;
import pages.metlife.YBRQuotePage;
import pages.metlife.YBRSummaryPage;
import pages.metlife.YBRWebResponsePage;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_ApplynowDeathcover;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_ApplynowIPcover;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_ApplynowTPDcover;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_CancelYourCoverPage;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_ChangeYourInsurance;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_RetrieveSaveapp;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_SaveandExit;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_SaveandExit_confirmationpage;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_SaveandExit_healthpage;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_Transfer_existingCover;
import pages.metlife.Angular.CareSuper.CareSuper_Angular_UpdateOccupation;
import pages.metlife.Angular.CareSuper.Caresuper_loginpage;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_CancelYourCoverPage;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_ChangeYourInsuranceCover;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_ConvertandMaintain;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_InsuranceCost;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_LandingPage;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_Login;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_RetrieveSavedAppPage;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_SaveAndExitPage;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_TransferYourCover;
import pages.metlife.Angular.FirstSuper.FirstSuper_Angular_UpdateYourOccupation;
import pages.metlife.Angular.HOSTPLUS.Eview;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_ApplynowDeath;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_ApplynowIp;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_ApplynowTpd;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_CancelYourCoverPage;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_ChangeYourInsuranceCover;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_ConvertandMaintain;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_InsuranceCost;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_Insurancelogin;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_LifeEvent;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_LifeEvent_SaveandExit;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_Login;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_OtherLinksPage;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_Personaldetailsvalidataion;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_RetrieveSavedAppPage;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_SaveAndExitPage;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_SaveAndExit_confirmationPage;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_SaveAndExit_healthPage;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_SpecialOffer;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_TransferYourCover;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_TransferYourCover_SaveandExit;
import pages.metlife.Angular.HOSTPLUS.HOSTPLUS_Angular_UpdateYourOccupation;
import pages.metlife.Angular.HOSTPLUS.MetflowIntegration_CaseSearch;
import pages.metlife.ECLAIMS.Claim_Angular_Deathflow;
import pages.metlife.ECLAIMS.Claim_Angular_claimtype_Injury;
import pages.metlife.ECLAIMS.Claim_Angular_claimtype_criticalillness;
import pages.metlife.ECLAIMS.Claim_Angular_claimtype_life;
import pages.metlife.ECLAIMS.Claim_Angular_claimtype_unemployment;
import pages.metlife.ECLAIMS.Claims_Angular_Login;
import pages.metlife.NEW.HOSTPLUS_CancelYourCoverPage;
import pages.metlife.NEW.HOSTPLUS_ChangeYourInsuranceCoverPage;
import pages.metlife.NEW.HOSTPLUS_ChangeYourInsurancePage;
import pages.metlife.NEW.HOSTPLUS_LifeEventPage;
import pages.metlife.NEW.HOSTPLUS_LoginPage;
import pages.metlife.NEW.HOSTPLUS_OtherLinksPage;
import pages.metlife.NEW.HOSTPLUS_RetrieveSavedAppPage;
import pages.metlife.NEW.HOSTPLUS_SaveAndExitPage;
import pages.metlife.NEW.HOSTPLUS_TransferYourCoverPage;
import pages.metlife.NEW.HOSTPLUS_UpdateYourOccupationPage;
import pages.metlife.NEW.INGD_LoginPage;
import pages.metlife.NEW.INGD_TransferYourCoverPage;
import pages.metlife.NEW.MTAA_CancelYourCoverPage;
import pages.metlife.NEW.MTAA_ChangeYourInsuranceCoverPage;
import pages.metlife.NEW.MTAA_LifeEventPage;
import pages.metlife.NEW.MTAA_LoginPage;
import pages.metlife.NEW.MTAA_OtherLinksPage;
import pages.metlife.NEW.MTAA_RetrieveSavedAppPage;
import pages.metlife.NEW.MTAA_SaveAndExitPage;
import pages.metlife.NEW.MTAA_TransferYourCoverPage;
import pages.metlife.NEW.MTAA_UpdateYourOccupationPage;
import pages.metlife.NEW.NSFS_LoginPage;
import pages.metlife.NEW.NSFS_TransferYourCoverPage;
import pages.metlife.NEW.NSFS_UpdateYourOccupationPage;
import pages.metlife.NEW.REIS_LoginPage;
import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import vicsuper.ChangeCover;
import vicsuper.ChangeCover_metflow;
import vicsuper.FixCover;
import vicsuper.LifeEvent;
import vicsuper.UpdateOccupation;
import vicsuper.VicSuper_LandingPage;
import vicsuper.VicSuper__Login;
import vicsuper.VicsSuper_TransferYourCover;
import vicsuper.VicsSuper_TransferYourCover_SaveandExit;
import vicsuper.VicsSuper_UpdateYourOccupation;
import vicsuper.Vicsuper_NewMemberFunction;
import vicsuper.Vicsuper_NewMemberFunctionOccupationRating;

/**
 * Functional Library class
 *
 * @author Cognizant
 */
public class MetLifeFunctionalComponents extends ReusableLibrary {
	/**
	 * Constructor to initialize the functional library
	 *
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object passed from the
	 *            {@link DriverScript}
	 */
	public MetLifeFunctionalComponents(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	public void invokeMTAASuperApplication() {
		driver.get(properties.getProperty("ApplicationUrl"));
		System.out.println("In inovkeApplication");
		report.updateTestLog(
				"Invoke Application",
				"Invoke the application under test @ "
						+ properties.getProperty("ApplicationUrl"), Status.PASS);
	}







	public void enterDeclarionAndConfirmation() {
		DeclarationAndConfirmationDetailsPage metLifeDeclarationPage = new DeclarationAndConfirmationDetailsPage(scriptHelper);
		metLifeDeclarationPage.enterDeclarationAndConfirmationDetails();
	}

	public void selectInsuranceCoverOption(){
		VICSManageInsurancePage vicsManageInsurancePage = new VICSManageInsurancePage(scriptHelper);
		vicsManageInsurancePage.selectInsuranceCoverOption();
	}


	public void enterVICSPersonalStatementDets(){
		VICSPersonalStatementDetailsPage vicsManageInsurancePage = new VICSPersonalStatementDetailsPage(scriptHelper);
		vicsManageInsurancePage.enterVICSPersonalStatementDets();
	}

	public void selectREISInsuranceCoverOption(){
		REISManageInsurancePage reisManageInsurancePage = new REISManageInsurancePage(scriptHelper);
		reisManageInsurancePage.selectREISInsuranceCoverOption();
	}

	public void checkEmailForApplication(){
		EmailPDFVerificationPage verifyEmail = new EmailPDFVerificationPage(scriptHelper);
		verifyEmail.checkEmailForApplication();
	}

	public void enterPersonalInformationDetails(){
		PWCPersonalInformationPage pwcPersonalInformation = new PWCPersonalInformationPage(scriptHelper);
		pwcPersonalInformation.enterPersonalInformationDetails();
	}

	public void validatePWCDecisionPage(){
		ValidateDetailsPage pwcValidatePage = new ValidateDetailsPage(scriptHelper);
		pwcValidatePage.validatePWCDecisionPage();
	}


	public void enterColesAcceptExtraQuote(){
		ColesExactQuotePage colesExactQuotePage = new ColesExactQuotePage(scriptHelper);
		colesExactQuotePage.enterColesAcceptExtraQuote();
	}

	public void enterColesPersonalDetails(){
		ColesConfirmPage colesPersonalDetailsPage = new ColesConfirmPage(scriptHelper);
		colesPersonalDetailsPage.enterColesPersonalDetails();
	}

	public void verifyColesPolicyDetails(){
		ColesConfirmationPage colesonfirmationPage = new ColesConfirmationPage(scriptHelper);
		colesonfirmationPage.verifyColesPolicyDetail();
	}

	public void enterColesPaymentDetails(){
		ColesPaymentPage colesPaymentPage = new ColesPaymentPage(scriptHelper);
		colesPaymentPage.enterColesPayment();
	}

	public void enterColesQuote(){
		ColesGetQuotePage colesQuotePage = new ColesGetQuotePage(scriptHelper);
		colesQuotePage.enterColesCalculateQuote();
	}

	public void enterColesPremiumCalculation(){
		ColesGetQuotePage colesQuotePage = new ColesGetQuotePage(scriptHelper);
		colesQuotePage.getQuota();
	}

	public void enterColesHealthADetails(){
		ColesHealthAPage colesHealthAPage = new ColesHealthAPage(scriptHelper);
		colesHealthAPage.enterColesHealthADetails();
	}

	public void enterColesHealthBDetails(){
		ColesHealthBPage colesHealthBPage = new ColesHealthBPage(scriptHelper);
		colesHealthBPage.enterColesHealthBPage();
	}

	public void enterLoadinfColesHealthBPage(){
		ColesHealthBPage colesHealthBPage = new ColesHealthBPage(scriptHelper);
		colesHealthBPage.enterLoadinfColesHealthBPage();
	}

	public void enterPersonalStatement() {
		PersonalStatementDetailsPage metLifePersonalStatementPage = new PersonalStatementDetailsPage(scriptHelper);
		metLifePersonalStatementPage.enterPersonalStatementDetails();
	}

	public void enterVICSPersonalStatement() {
		VICSPersonalStatementDetailsPage metLifeVICSPersonalStatementPage = new VICSPersonalStatementDetailsPage(scriptHelper);
		metLifeVICSPersonalStatementPage.enterVICSPersonalStatementDetails();
	}

	public void enterPersonalDetails() {
		PersonalDetailsPage metLifePersonalPage = new PersonalDetailsPage(
				scriptHelper);
		metLifePersonalPage.enterPersonalDetails();
	}

	public void enterHostplusPersonalDetails() {
		PersonalDetailsPage metLifePersonalPage = new PersonalDetailsPage(
				scriptHelper);
		metLifePersonalPage.enterHostplusPersonalDetails();
	}

	public void enterCoverDetails() {
		CoverDetailsPage metLifeCoverPage = new CoverDetailsPage(
				scriptHelper);
		metLifeCoverPage.enterCoverDetails();
	}




		//SFPS Related Functional Components
	public void enterSFPSPayload() {
		SFPSNewInsurancePage sfpsPage = new SFPSNewInsurancePage(
				scriptHelper);
		sfpsPage.enterSFPSPayload();
	}

	public void enterSFPSCoverDetails() {
		CoverDetailsPage metLifeSFPSCoverPage = new CoverDetailsPage(
				scriptHelper);
		metLifeSFPSCoverPage.enterSFPSCoverDetails();
	}

	public void enterSFPSPersonalStatementDetails() {
		SFPSPersonalStatementDetailsPage metLifeSFPSPersonalStatementPage = new SFPSPersonalStatementDetailsPage(scriptHelper);
		metLifeSFPSPersonalStatementPage.enterSFPSPersonalStatementDetails();
	}

//INGD Related Functional Components
	public void enterINGDPayload() {
		INGDNewInsurancePage ingdPage = new INGDNewInsurancePage(
				scriptHelper);
		ingdPage.enterINGDPayload();
	}

	public void enterINGDInsuranceDetails() {
		System.out.println("In enterINGDInsuranceDetails");
		INGDManageInsurancePage ingdInsuranceDetPage = new INGDManageInsurancePage(
				scriptHelper);
		ingdInsuranceDetPage.selectInsuranceCoverOption();
	}

	public void enterINGDCoverDetails() {
		CoverDetailsPage ingdCoverDetPage = new CoverDetailsPage(
				scriptHelper);
		ingdCoverDetPage.enterINGDCoverDetails();
	}

	public void enterINGDPersonalStatementDetails() {
		INGDPersonalStatementDetailsPage metLifeINGDPersonalStatementPage = new INGDPersonalStatementDetailsPage(scriptHelper);
		metLifeINGDPersonalStatementPage.enterINGDPersonalStatementDetails();
	}


	public void enterINGDPersonalStatementDets() {
		INGDPersonalStatementDetailsPage metLifeINGDPersonalStatementPage = new INGDPersonalStatementDetailsPage(scriptHelper);
		metLifeINGDPersonalStatementPage.enterINGDPersonalStatementDets();
	}

	//NSFS Related Functional Components
	public void enterNSFSPayload() {
		NSFSNewInsurancePage nsfsPage = new NSFSNewInsurancePage(
				scriptHelper);
		nsfsPage.fillAndCalculateNSFSQuote();
	}

	public void enterNSFSCoverDetails() {
		CoverDetailsPage metLifeNSFSCoverPage = new CoverDetailsPage(
				scriptHelper);
		metLifeNSFSCoverPage.enterNSFSCoverDetails();
	}

	public void enterNSFSPersonalStatementDetails() {
		NSFSPersonalStatementDetailsPage metLifeNSFSPersonalStatementPage = new NSFSPersonalStatementDetailsPage(scriptHelper);
		metLifeNSFSPersonalStatementPage.enterNSFSPersonalStatementDetails();
	}

	//AEIS related Functional Components

	public void enterAEISCoverDetails() {
		CoverDetailsPage metLifeAEISCoverPage = new CoverDetailsPage(
				scriptHelper);
		metLifeAEISCoverPage.enterAEISCoverDetails();
	}

	public void enterAEISPersonalStatementDetails() {
		AEISPersonalStatementDetailsPage metLifeAEISPersonalStatementPage = new AEISPersonalStatementDetailsPage(scriptHelper);
		metLifeAEISPersonalStatementPage.enterAEISPersonalStatementDetails();
	}

	//REIS related functional components
	public void enterREISCoverDetails() {
		CoverDetailsPage metLifeREISCoverPage = new CoverDetailsPage(
				scriptHelper);
		metLifeREISCoverPage.enterREISCoverDetails();
	}

	//PSUP

	public void enterPSUPPersonalStatementDetails() {
		PSUPPersonalStatementDetailsPage metLifePSUPPersonalStatementPage = new PSUPPersonalStatementDetailsPage(scriptHelper);
		metLifePSUPPersonalStatementPage.enterPSUPPersonalStatementDetails();
	}

	//GUILD related functional components
	public void enterGUILDCoverDetails() {
		CoverDetailsPage metLifeGUILDCoverPage = new CoverDetailsPage(
				scriptHelper);
		metLifeGUILDCoverPage.enterGUILDCoverDetails();
	}

	public void enterGUILDPersonalStatementDetails() {
		GUILDPersonalStatementDetailsPage metLifeGUILDPersonalStatementPage = new GUILDPersonalStatementDetailsPage(scriptHelper);
		metLifeGUILDPersonalStatementPage.enterGUILDPersonalStatementDetails();
	}

	//ACCS related functional components
	public void enterACCSPersonalDetails() {
		PersonalDetailsPage metLifeACCSPersonalPage = new PersonalDetailsPage(
				scriptHelper);
		metLifeACCSPersonalPage.enterACCSPersonalDetails();
	}

	//HOSTPLUS related functional components
	public void enterHostPlusApplyDeathCover() {
		HostplusManageInsurancePage metLifeHostPlusPersonalPage = new HostplusManageInsurancePage(
				scriptHelper);
		metLifeHostPlusPersonalPage.enterHostPlusApplyDeathCover();
	}

	public void enterHostplusContactDetails() {
		PersonalDetailsPage metLifeHostPlusContactPage = new PersonalDetailsPage(
				scriptHelper);
		metLifeHostPlusContactPage.enterHostplusContactDetails();
	}


	public void enterHostPlusIntroductionDetails() {
		HOSTPLUSPersonalStatementDetailsPage metLifeHostPlusContactPage = new HOSTPLUSPersonalStatementDetailsPage(
				scriptHelper);
		metLifeHostPlusContactPage.enterHostPlusIntroductionDetails();
	}

	public void enterHostPlusPersonalStatementDetails(){
		HOSTPLUSPersonalStatementDetailsPage hppsdp = new HOSTPLUSPersonalStatementDetailsPage(scriptHelper);
		hppsdp.enterHostPlusPersonalStatementDetails();
	}

	public void enterHostplusOccupationDetails() {
		PersonalDetailsPage metLifeHostPlusContactPage = new PersonalDetailsPage(
				scriptHelper);
		metLifeHostPlusContactPage.enterHostplusOccupationDetails();
	}


	// STFT related functional components.

	public void enterSteadFastQuoteDetails() {
		STFTGetQuotePage stftQuotePage = new STFTGetQuotePage(scriptHelper);
		stftQuotePage.getQuote();
	}

	public void enterSteadFastHealthDetails() {
		STFTHealthPage stftHealthPage = new STFTHealthPage(scriptHelper);
		stftHealthPage.yourHealth();
	}

	public void enterSteadFastSummaryDetails() {
		STFTSummaryPage stftsummaryPage = new STFTSummaryPage(scriptHelper);
		stftsummaryPage.yourSummary();
	}

	public void enterSteadFastConfirmationDetails() {
		STFTConfirmationPage stftconfirmationPage = new STFTConfirmationPage(scriptHelper);
		stftconfirmationPage.yourConfirmation();
	}

	public void enterSteadFastPaymentsDetails() {
		STFTPaymentsPage stftpaymentspage = new STFTPaymentsPage(scriptHelper);
		stftpaymentspage.yourPayment();
	}

	public void enterSteadFastNewBneficiariesDetails() {
		STFTBeneficiariesPage stftbeneficiariespage = new STFTBeneficiariesPage(scriptHelper);
		stftbeneficiariespage.nominateBeneficiaries();
	}

	public void enterSteadFastDecline() {
		STFTDeclinePage stftdeclinepage = new STFTDeclinePage(scriptHelper);
		stftdeclinepage.coverDecline();
	}

	public void validateConclusionMessage(){
		DeclarationAndConfirmationDetailsPage confirmMessage = new DeclarationAndConfirmationDetailsPage(scriptHelper);
		confirmMessage.validateConclusionMessage();
	}

	public void getSaveApplicationNumber() {
		PersonalStatementDetailsPage savenexit = new PersonalStatementDetailsPage(scriptHelper);
		savenexit.getSaveApplicationNumber();
	}

	//Metflow Specific methods Start
	public void loginToMetflow() {
		MetflowLoginDetailsPage metflowLogin = new MetflowLoginDetailsPage(scriptHelper);
		metflowLogin.loginToMetflow();
	}
	public void eapplycaseSearch() {
		MetflowIntegration_CaseSearch metflowLogin = new MetflowIntegration_CaseSearch(scriptHelper);
		metflowLogin.MetFlowCaseSearch();
	}

	// YBR

		public void enterYBRInput() {
			YBRCoverDetailPage ybrinputpage = new YBRCoverDetailPage(scriptHelper);
			ybrinputpage.webresponse();
		}
		public void enterYBRInputNewApp() {
			YBRWebResponsePage ywebresponsepage = new YBRWebResponsePage(scriptHelper);
			ywebresponsepage.webresponse_newapp();
		}
		public void enterYBRProtectNow() {
			YBRQuotePage ybrquotepage = new YBRQuotePage(scriptHelper);
			ybrquotepage.quoteProtect();
		}
		public void enterYBRPaymentProtectNow() {
			YBRSummaryPage ybrsummarypage = new YBRSummaryPage(scriptHelper);
			ybrsummarypage.quoteProtectPayment();
		}


		// VOW

			public void enterVOWInput() {
				VOWCoverDetailPage vowinputpage = new VOWCoverDetailPage(scriptHelper);
				vowinputpage.webresponse();
			}
			public void enterVOWInputNewApp() {
				VOWWebResponsePage vowwebresponsepage = new VOWWebResponsePage(scriptHelper);
				vowwebresponsepage.webresponse_newapp();
			}
			public void enterVOWProtectNow() {
				VOWQuotePage vowquotepage = new VOWQuotePage(scriptHelper);
				vowquotepage.quoteProtect();
			}
			public void enterVOWPaymentProtectNow() {
				VOWSummaryPage vowsummarypage = new VOWSummaryPage(scriptHelper);
				vowsummarypage.quoteProtectPayment();
			}

			// Coles Call Center

			public void enterInputXMLDetails() {
				ColesCCStubPage ccstubpage = new ColesCCStubPage(scriptHelper);
				ccstubpage.enterInputXML();
			}
			public void enterColesCCQuote() {
				ColesCCGetQuotePage ccgetquotepage = new ColesCCGetQuotePage(scriptHelper);
				ccgetquotepage.fillAndCalculateColesCCQuote();
			}
			public void enterColesCCHealthA() {
				ColesCCHealthAPage cchealthapage = new ColesCCHealthAPage(scriptHelper);
				cchealthapage.fillColesCCHealthA();
			}
			public void enterColesCCExactQuote() {
				ColesCCExactQtPage ccexactquotepage = new ColesCCExactQtPage(scriptHelper);
				ccexactquotepage.CalculateColesCCExactQuote();
			}
			public void enterColesCCConfirm() {
				ColesCCConfirmPage ccconfirmpage = new ColesCCConfirmPage(scriptHelper);
				ccconfirmpage.fillPersonalDetailsCC();
			}
			public void enterColesCCPayment() {
				ColesCCPaymentPage ccpaymentpage = new ColesCCPaymentPage(scriptHelper);
				ccpaymentpage.fillColesCCPayment();
			}

			// STFT Call Center
			public void enterInputXMLDetailsSTFT() {
				STFTCCStubPage stftccstubpage = new STFTCCStubPage(scriptHelper);
				stftccstubpage.enterInputXML();
			}

			public void enterSteadFastCCQuoteDetails() {
				STFTCCGetQuotePage stftccQuotePage = new STFTCCGetQuotePage(scriptHelper);
				stftccQuotePage.getQuote();
			}

			public void enterSteadFastCCHealthDetails() {
				STFTCCHealthPage stftccHealthPage = new STFTCCHealthPage(scriptHelper);
				stftccHealthPage.yourHealth();
			}

			public void enterSteadFastCCSummaryDetails() {
				STFTCCSummaryPage stftccsummaryPage = new STFTCCSummaryPage(scriptHelper);
				stftccsummaryPage.yourSummary();
			}

			public void enterSteadFastCCConfirmationDetails() {
				STFTCCConfirmationPage stftccconfirmationPage = new STFTCCConfirmationPage(scriptHelper);
				stftccconfirmationPage.yourConfirmation();
			}

			public void enterSteadFastCCPaymentsDetails() {
				STFTCCPaymentsPage stftccpaymentspage = new STFTCCPaymentsPage(scriptHelper);
				stftccpaymentspage.yourPayment();
			}


			// Direct Product Landing page
			public void enterDirectProductDetails() {
			//	JOptionPane.showMessageDialog(null,"ALERT MESSAGE","TITLE",JOptionPane.WARNING_MESSAGE);
				DPStubPage dpstubpage = new DPStubPage(scriptHelper);
				dpstubpage.selectProduct();
			}

			// 1300
			public void enter1300QuoteDetails() {
				Mortgage1300QuotePage QuotePage = new Mortgage1300QuotePage(scriptHelper);
				QuotePage.getQuote();
			}

			public void enter1300PaymentDetails() {
				Mortgage1300PaymentPage PaymentPage = new Mortgage1300PaymentPage(scriptHelper);
				PaymentPage.paymentDetails();
			}

			public void enter1300FinishDetails() {
				Mortgage1300AcceptedPage AcceptedPage = new Mortgage1300AcceptedPage(scriptHelper);
				AcceptedPage.acceptedFinish();
			}


			//  OAMPS

			public void enterOAMPSQuoteDetails() {
				OAMPSGetQuotePage oampsQuotePage = new OAMPSGetQuotePage(scriptHelper);
				oampsQuotePage.getQuote();
			}

			public void enterOAMPSHealthDetails() {
				OAMPSHealthPage oampsHealthPage = new OAMPSHealthPage(scriptHelper);
				oampsHealthPage.yourHealth();
			}

			public void enterOAMPSSummaryDetails() {
				OAMPSSummaryPage oampssummaryPage = new OAMPSSummaryPage(scriptHelper);
				oampssummaryPage.yourSummary();
			}

			public void enterOAMPSConfirmationDetails() {
				OAMPSConfirmationPage oampsconfirmationPage = new OAMPSConfirmationPage(scriptHelper);
				oampsconfirmationPage.yourConfirmation();
			}

			public void enterOAMPSPaymentsDetails() {
				OAMPSPaymentsPage oampspaymentspage = new OAMPSPaymentsPage(scriptHelper);
				oampspaymentspage.yourPayment();
			}

			public void enterDeclinePageDetails() {
				OAMPSDeclinePage oampsdeclinePage = new OAMPSDeclinePage(scriptHelper);
				oampsdeclinePage.DeclinePageDetails();
			}

			public void enterSavePageDetails() {
				OAMPSSaveandExitPage oampssavePage = new OAMPSSaveandExitPage(scriptHelper);
				oampssavePage.SaveandExitDetails();
			}

			public void acceptedPageDetails() {
				OAMPSAcceptedPage oampsacceptedPage = new OAMPSAcceptedPage(scriptHelper);
				oampsacceptedPage.acceptedDetails();
			}

		//  ALI

				public void enterALIQuoteDetails() {
					ALIGetQuotePage ALIQuotePage = new ALIGetQuotePage(scriptHelper);
					ALIQuotePage.getQuote();
				}

				public void enterALIHealthOverviewDetails() {
					ALIHealthOverviewPage ALIHealthPage = new ALIHealthOverviewPage(scriptHelper);
					ALIHealthPage.yourHealthOverview();
				}

				public void enterALIHealthDetails() {
					ALIHealthDetailsPage ALIHealthPage1 = new ALIHealthDetailsPage(scriptHelper);
					ALIHealthPage1.yourHealth();
				}


				public void enterALISummaryDetails() {
					ALISummaryPage ALIsummary= new ALISummaryPage(scriptHelper);
					ALIsummary.yourSummary();
				}

				public void enterALIConfirmDetails() {
					ALIConfirmationPage ALIconfirmation = new ALIConfirmationPage(scriptHelper);
					ALIconfirmation.yourConfirmationEnter();
				}

				public void enterALIPaymentDetails() {
					ALIPaymentsPage ALIpaymentpage = new ALIPaymentsPage(scriptHelper);
					ALIpaymentpage.yourPayment();
				}

				public void enterALIAcceptedDetails() {
					ALIAcceptedPage ALIacceptedpage = new ALIAcceptedPage(scriptHelper);
					ALIacceptedpage.AcceptedPage();
				}

				public void enterALIDeclinedDetails() {
					ALIDeclinedPage ALIdeclinedpage = new ALIDeclinedPage(scriptHelper);
					ALIdeclinedpage.DeclinedPage();
				}

				public void enterALISavePageDetails() {
					ALISaveandExitPage alisavePage = new ALISaveandExitPage(scriptHelper);
					alisavePage.SaveandExitDetails();
				}

				public void enterYBRIPAcceptedDetails() {
					YBRIPAcceptedPage YBRIPacceptedpage = new YBRIPAcceptedPage(scriptHelper);
					YBRIPacceptedpage.AcceptedPage();
				}

				public void enterYBRIPConfirmDetails() {
					YBRIPConfirmationPage YBRIPconfirmation = new YBRIPConfirmationPage(scriptHelper);
					YBRIPconfirmation.yourConfirmationEnter();
				}

				public void enterYBRIPDeclinedDetails() {
					YBRIPDeclinedPage YBRIPdeclinedpage = new YBRIPDeclinedPage(scriptHelper);
					YBRIPdeclinedpage.DeclinedPage();
				}

				public void enterYBRIPHealthDetails() {
					YBRIPHealthDetailsPage YBRIPHealthPage1 = new YBRIPHealthDetailsPage(scriptHelper);
					YBRIPHealthPage1.yourHealth();
				}

				public void enterYBRIPHealthOverviewDetails() {
					YBRIPHealthOverviewPage YBRIPHealthPage = new YBRIPHealthOverviewPage(scriptHelper);
					YBRIPHealthPage.yourHealthOverview();
				}

				public void enterYBRIPPaymentDetails() {
					YBRIPPaymentsPage YBRIPpaymentpage = new YBRIPPaymentsPage(scriptHelper);
					YBRIPpaymentpage.yourPayment();
				}

				//  ALI

				public void enterYBRIPQuoteDetails() {
					YBRIPGetQuotePage YBRIPQuotePage = new YBRIPGetQuotePage(scriptHelper);
					YBRIPQuotePage.getQuote();
				}

				public void enterYBRIPSavePageDetails() {
					YBRIPSaveandExitPage YBRIPsavePage = new YBRIPSaveandExitPage(scriptHelper);
					YBRIPsavePage.SaveandExitDetails();
				}

				public void enterYBRIPSummaryDetails() {
					YBRIPSummaryPage YBRIPsummary= new YBRIPSummaryPage(scriptHelper);
					YBRIPsummary.yourSummary();
				}
				public void loginandvalidateCyberSource() throws Exception {
					CyberSourceLoginDetailsPage cybersourceLogin = new CyberSourceLoginDetailsPage(scriptHelper);
					cybersourceLogin.loginandvalidateCyberSource();
				}

				public void entereViewLogin() {
					EViewLoginPage eViewLogin= new EViewLoginPage(scriptHelper);
					eViewLogin.LoginToeView();
				}
				public void searchUnderwritePDF() {
					EViewSearchUnderwritePDF UnderwritePDF= new EViewSearchUnderwritePDF(scriptHelper);
					UnderwritePDF.SearchforUnderwritePDF();
				}

				public void enterColesKidsDetails(){
					ColesAddkidPage ColesAddkidDetailsPage = new ColesAddkidPage(scriptHelper);
					ColesAddkidDetailsPage.ColesKidsDetails();
				}

				// YBR

				public void enterYBRCCInput() {
					YBRCCCoverDetailPage ybrccinputpage = new YBRCCCoverDetailPage(scriptHelper);
					ybrccinputpage.webresponse();
				}
				public void entereServiceLogin() {
					EServiceLoginPage eServiceLogin= new EServiceLoginPage(scriptHelper);
					eServiceLogin.LoginToeService();
				}

				public void policySearch() {
					EServicePolicySearch eServicePolicySearch= new EServicePolicySearch(scriptHelper);
					eServicePolicySearch.PolicySerach();
				}
				public void raiseRequest() {
					EServiceRaiseRequest eServiceRaiseRequest= new EServiceRaiseRequest(scriptHelper);
					eServiceRaiseRequest.RaiseRequest();
				}
				public void cancelINGDCover() {
					INGDCancelInsurancePage CancelINGDInsurancePage = new INGDCancelInsurancePage(scriptHelper);
					CancelINGDInsurancePage.cancelINGDInsurance();
				}
				public void transferINGDCover() {
					INGDTransferInsurancePage TransferINGDInsurancePage = new INGDTransferInsurancePage(scriptHelper);
					TransferINGDInsurancePage.TransferINGDInsurance();
				}
				public void saveINGDCover() {
					INGDSaveCover SaveINGDCover = new INGDSaveCover(scriptHelper);
					SaveINGDCover.ToSaveINGDCover();
				}

				public void invokeMTAAapplication()
				{
					String strApplicationURL = dataTable.getData("MTAA_Login", "URL");
					//driver.get(properties.getProperty("ApplicationUrl"));


					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

					sleep(500);
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  			Dimension d = new Dimension(1340,760);
				  			//Resize the current window to the given dimension
				  			driver.manage().window().setSize(d);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
					//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
							//strApplicationURL, Status.DONE);
					report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
				}

				public void enterMTAALogin() {
					MTAA_LoginPage login = new MTAA_LoginPage(scriptHelper);
					login.enterMTAAlogin();
				}

				public void enterMTAA_TransferYourCoverPage() {
					MTAA_TransferYourCoverPage transfercover = new MTAA_TransferYourCoverPage(scriptHelper);
					transfercover.mtaatransfercover();
				}

				public void enterMTAA_TransferYourCoverNegativeFlow() {
					MTAA_TransferYourCoverPage transfercover = new MTAA_TransferYourCoverPage(scriptHelper);
					transfercover.TransferCoverMTAANegative();
				}
				public void enterMTAA_UpdateYourOccupation() {
					MTAA_UpdateYourOccupationPage updateoccupation = new MTAA_UpdateYourOccupationPage(scriptHelper);
					updateoccupation.enterMTAAUpdateoccupation();
				}

				public void enterMTAA_ChangeYourInsurance() {
					MTAA_ChangeYourInsuranceCoverPage changeinsurance = new MTAA_ChangeYourInsuranceCoverPage(scriptHelper);
					changeinsurance.MTAAChangeYourInsuranceCover();
				}

				public void enterMTAA_LifeEventFlow() {
					MTAA_LifeEventPage lifeevent = new MTAA_LifeEventPage(scriptHelper);
					lifeevent.MTAALifeEvent();
				}

				public void enterMTAA_LifeEventNegativeFlow() {
					MTAA_LifeEventPage lifeevent = new MTAA_LifeEventPage(scriptHelper);
					lifeevent.MTAALifeEventNegativeFlow();
				}
				public void enterMTAA_CancelYourCover() {
					MTAA_CancelYourCoverPage cancelyourcover = new MTAA_CancelYourCoverPage(scriptHelper);
					cancelyourcover.MTAACancelYourCover();
				}

				public void enterMTAA_SaveandExit() {
					MTAA_SaveAndExitPage saveandexit = new MTAA_SaveAndExitPage(scriptHelper);
					saveandexit.MTAASaveandExit();
				}

				public void enterMTAA_RetrieveApp() {
					MTAA_RetrieveSavedAppPage retrieve = new MTAA_RetrieveSavedAppPage(scriptHelper);
					retrieve.MTAARetreiveApp();
				}

				public void enterMTAA_OtherLinks() {
					MTAA_OtherLinksPage otherlinks = new MTAA_OtherLinksPage(scriptHelper);
					otherlinks.MTAAOtherLinks();
				}



				public void invokeNSFSapplication()
				{
					String strApplicationURL = dataTable.getData("NSFS_Login", "URL");
					//driver.get(properties.getProperty("ApplicationUrl"));
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					driver.get(strApplicationURL);
					//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
							//strApplicationURL, Status.DONE);
					report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
				}

				public void enterNSFSLogin() {
					NSFS_LoginPage login = new NSFS_LoginPage(scriptHelper);
					login.enterNSFSlogin();
				}

				public void enterNSFS_TransferYourCoverPage() {
					NSFS_TransferYourCoverPage transfercover = new NSFS_TransferYourCoverPage(scriptHelper);
					transfercover.enternsfstransfercover();
				}

				public void enterNSFS_UpdateYourOccupation() {
					NSFS_UpdateYourOccupationPage updateoccupation = new NSFS_UpdateYourOccupationPage(scriptHelper);
					updateoccupation.enterNSFSupdateoccupation();
				}

				public void invokeHOSTPLUSapplication()
				{
					String strApplicationURL = dataTable.getData("HOSTPLUS_Login", "URL");
					//driver.get(properties.getProperty("ApplicationUrl"));


					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

					sleep(500);
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  			Dimension d = new Dimension(1410,1150);
				  			//Resize the current window to the given dimension
				  			driver.manage().window().setSize(d);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
					//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
							//strApplicationURL, Status.DONE);
					report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
				}

				public void enterHOSTPLUSLogin() {
					HOSTPLUS_LoginPage login = new HOSTPLUS_LoginPage(scriptHelper);
					login.enterHOSTPLUSlogin();
				}
				public void enterpremiumLogin() {
					HOSTPLUS_Angular_Insurancelogin login = new HOSTPLUS_Angular_Insurancelogin(scriptHelper);
					login.HOSTPLUS_Login();
				}


				public void enterHOSTPLUS_TransferYourCoverPage() {
					HOSTPLUS_TransferYourCoverPage transfercover = new HOSTPLUS_TransferYourCoverPage(scriptHelper);
					transfercover.HOSTPLUStransfercover();
				}

				public void enterHOSTPLUS_TransferYourCoverNegativeScenario() {
					HOSTPLUS_TransferYourCoverPage transfercover = new HOSTPLUS_TransferYourCoverPage(scriptHelper);
					transfercover.HOSTPLUStransfercoverNegative();
				}

				public void enterHOSTPLUS_UpdateYourOccupation() {
					HOSTPLUS_UpdateYourOccupationPage updateoccupation = new HOSTPLUS_UpdateYourOccupationPage(scriptHelper);
					updateoccupation.enterHOSTPLUSupdateoccupation();
				}

				public void enterHOSTPLUS_ChangeYourInsurance() {
					HOSTPLUS_ChangeYourInsurancePage changeinsurance = new HOSTPLUS_ChangeYourInsurancePage(scriptHelper);
					changeinsurance.HOSTPLUSchangeinsurance();
				}

				public void enterHOSTPLUS_ChangeYourInsuranceCover() {
					HOSTPLUS_ChangeYourInsuranceCoverPage changeinsurance = new HOSTPLUS_ChangeYourInsuranceCoverPage (scriptHelper);
					changeinsurance.HOSTPLUSChangeYourInsuranceCover();
				}

				public void enterHOSTPLUS_LifeEvent() {
					HOSTPLUS_LifeEventPage lifeevent = new HOSTPLUS_LifeEventPage (scriptHelper);
					lifeevent.HOSTPLUSLifeEvent();
				}

				public void enterHOSTPLUS_LifeEventNegativeFlow() {
					HOSTPLUS_LifeEventPage lifeevent = new HOSTPLUS_LifeEventPage (scriptHelper);
					lifeevent.HOSTPLUSLifeNegativeFlow();
				}

				public void enterHOSTPLUS_CancelYourCoverFlow() {
					HOSTPLUS_CancelYourCoverPage cancelcover = new HOSTPLUS_CancelYourCoverPage (scriptHelper);
					cancelcover.HOSTPLUSCancelYourCoverCover();
				}

				public void enterHOSTPLUS_SaveandExitFlow() {
					HOSTPLUS_SaveAndExitPage saveandexit = new HOSTPLUS_SaveAndExitPage (scriptHelper);
					saveandexit.HOSTPLUSSaveandExit();
				}




				public void enterHOSTPLUS_RetrieveAppFlow() {
					HOSTPLUS_RetrieveSavedAppPage retrieveapp = new HOSTPLUS_RetrieveSavedAppPage (scriptHelper);
					retrieveapp.HOSTPLUSRetreiveApp();
				}

				public void enterHOSTPLUS_OtherLinks() {
					HOSTPLUS_OtherLinksPage otherlinks = new HOSTPLUS_OtherLinksPage (scriptHelper);
					otherlinks.HOSTPLUSOtherLinks();
				}
				public void enterHOSTPLUS_personalvalidation() {
					HOSTPLUS_Angular_Personaldetailsvalidataion personal = new HOSTPLUS_Angular_Personaldetailsvalidataion (scriptHelper);
					personal.SaveandExit_personaldetails();
				}

				public void enterHOSTPLUS_LifeventSaveAndExit() {
					HOSTPLUS_Angular_LifeEvent_SaveandExit Save = new HOSTPLUS_Angular_LifeEvent_SaveandExit (scriptHelper);
					Save.LifeEvent_SaveAndExit();
				}
				public void enterHOSTPLUS_LifeventSaveAndExitValidation() {
					HOSTPLUS_Angular_LifeEvent_SaveandExit Save = new HOSTPLUS_Angular_LifeEvent_SaveandExit (scriptHelper);
					Save.LifeEvent_SaveAndExitValidation();
				}

				public void enterHOSTPLUS_TransferCoverSaveAndExit() {
					HOSTPLUS_Angular_TransferYourCover_SaveandExit transferSave = new HOSTPLUS_Angular_TransferYourCover_SaveandExit (scriptHelper);
					transferSave.TransferCover_HOSTPLUS_SaveAndExit();
				}

				public void enterHOSTPLUS_TransferCoverSaveAndExitValidaiton() {
					HOSTPLUS_Angular_TransferYourCover_SaveandExit validation = new HOSTPLUS_Angular_TransferYourCover_SaveandExit (scriptHelper);
					validation.TransferCover_HOSTPLUS_Validation();
				}





				public void invokeINGDapplication()
				{
					String strApplicationURL = dataTable.getData("INGD_Login", "URL");
					//driver.get(properties.getProperty("ApplicationUrl"));
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					driver.get(strApplicationURL);
					//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
							//strApplicationURL, Status.DONE);
					report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
				}

				public void enterINGDLogin() {
					INGD_LoginPage login = new INGD_LoginPage(scriptHelper);
					login.enterINGDlogin();
				}

				public void enterINGD_TransferYourCoverPage() {
					INGD_TransferYourCoverPage transfercover = new INGD_TransferYourCoverPage(scriptHelper);
					transfercover.enterINGDtransfercover();
				}
				public void invokeREISapplication()
				{
					String strApplicationURL = dataTable.getData("REIS_Login", "URL");
					//driver.get(properties.getProperty("ApplicationUrl"));
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					driver.get(strApplicationURL);
					//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
							//strApplicationURL, Status.DONE);
					report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
				}
				public void enterREISLogin() {
					REIS_LoginPage login = new REIS_LoginPage(scriptHelper);
					login.enterREISlogin();
				}
			/*	public void enterREIS_TransferYourCoverPage() {
					REIS_TransferYourCoverPage transfercover = new REIS_TransferYourCoverPage(scriptHelper);
					transfercover.REIStransfercover();
				}*/

				//*********HOSTPLUS AngularJS business functions*******\\

				public void login_HOSTPLUS() {
					HOSTPLUS_Angular_Login Login = new HOSTPLUS_Angular_Login(scriptHelper);
					Login.HOSTPLUS_Login();
				}

				public void insuranceCost_HOSTPLUS() {
					HOSTPLUS_Angular_InsuranceCost insurancecost = new HOSTPLUS_Angular_InsuranceCost(scriptHelper);
					insurancecost.InsuranceCost_HOSTPLUS();
				}


				public void transferyourcover_HOSTPLUS() {
					HOSTPLUS_Angular_TransferYourCover transfercover = new HOSTPLUS_Angular_TransferYourCover(scriptHelper);
					transfercover.TransferCover_HOSTPLUS();
				}

				public void transferyourcover_HOSTPLUS_Negative() {
					HOSTPLUS_Angular_TransferYourCover transfercover = new HOSTPLUS_Angular_TransferYourCover(scriptHelper);
					transfercover.TransferCover_HOSTPLUS_Negative();
				}

				public void updateoccupation_HOSTPLUS() {
					HOSTPLUS_Angular_UpdateYourOccupation updateoccupation = new HOSTPLUS_Angular_UpdateYourOccupation(scriptHelper);
					updateoccupation.UpdateYourOccupation_HOSTPLUS();
				}

				public void lifeEvent_HOSTPLUS() {
					HOSTPLUS_Angular_LifeEvent lifeevent = new HOSTPLUS_Angular_LifeEvent(scriptHelper);
					lifeevent.LifeEvent_HOSTPLUS();
				}
				public void lifeEventNegative_HOSTPLUS() {
					HOSTPLUS_Angular_LifeEvent lifeeventnegative = new HOSTPLUS_Angular_LifeEvent(scriptHelper);
					lifeeventnegative.LifeEventNegative_HOSTPLUS();
				}

				public void cancelYourCover_HOSTPLUS() {
					HOSTPLUS_Angular_CancelYourCoverPage cancelcover = new HOSTPLUS_Angular_CancelYourCoverPage(scriptHelper);
					cancelcover.CancelYourCoverCover_HOSTPLUS();
				}
				public void specialoffer_HOSTPLUS() {
					HOSTPLUS_Angular_SpecialOffer specialoffer = new HOSTPLUS_Angular_SpecialOffer(scriptHelper);
					specialoffer.SpecialOffer_HOSTPLUS();
				}

				public void convertandMaintain_HOSTPLUS() {
					HOSTPLUS_Angular_ConvertandMaintain convertandmaintain = new HOSTPLUS_Angular_ConvertandMaintain(scriptHelper);
					convertandmaintain.ConvertandMaintain_HOSTPLUS();
				}
				public void changeyourinsurancecover_HOSTPLUS() {
					HOSTPLUS_Angular_ChangeYourInsuranceCover chnageyourinsurance = new HOSTPLUS_Angular_ChangeYourInsuranceCover(scriptHelper);
					chnageyourinsurance.ChangeYourInsuranceCover_HOSTPLUS();
				}
				public void otherlinks_HOSTPLUS() {
					HOSTPLUS_Angular_OtherLinksPage otherlinks = new HOSTPLUS_Angular_OtherLinksPage(scriptHelper);
					otherlinks.OtherLinks_HOSTPLUS();
				}
				public void saveandexit_HOSTPLUS() {
					HOSTPLUS_Angular_SaveAndExitPage saveandexit = new HOSTPLUS_Angular_SaveAndExitPage(scriptHelper);
					saveandexit.SaveandExit_HOSTPLUS();
				}

				public void saveandExithealthFlow_HOSTPLUS() {
					HOSTPLUS_Angular_SaveAndExit_healthPage saveandexithealth = new HOSTPLUS_Angular_SaveAndExit_healthPage (scriptHelper);
					saveandexithealth.SaveandExit_Healthpage();
				}

				public void saveandExitconfirmationFlow_HOSTPLUS() {
					HOSTPLUS_Angular_SaveAndExit_confirmationPage saveandexitconfirm = new HOSTPLUS_Angular_SaveAndExit_confirmationPage (scriptHelper);
					saveandexitconfirm.SaveandExit_Confirmationpage();
				}

				public void validationconfirmationpage_HOSTPLUS() {
					HOSTPLUS_Angular_SaveAndExit_confirmationPage saveandexitconfirm = new HOSTPLUS_Angular_SaveAndExit_confirmationPage (scriptHelper);
					saveandexitconfirm.SaveandExit_Confirmationpagevalidation();
				}


				public void retrieveapp_HOSTPLUS() {
					HOSTPLUS_Angular_RetrieveSavedAppPage retrieveapp = new HOSTPLUS_Angular_RetrieveSavedAppPage(scriptHelper);
					retrieveapp.RetreiveApp_HOSTPLUS();
				}
				public void eapplyratecheck_HOSTPLUS() {
					HOSTPLUS_Angular_ChangeYourInsuranceCover chnageyourinsurance = new HOSTPLUS_Angular_ChangeYourInsuranceCover(scriptHelper);
					chnageyourinsurance.EApplyRateCheck_HOSTPLUS();

									}

				public void applydeath_HOSTPLUS() {
					HOSTPLUS_Angular_ApplynowDeath chnageyourinsurance = new HOSTPLUS_Angular_ApplynowDeath(scriptHelper);
					chnageyourinsurance.applydeathcover_HOSTPLUS();

									}

				public void applytpd_HOSTPLUS() {
					HOSTPLUS_Angular_ApplynowTpd chnageyourinsurance = new HOSTPLUS_Angular_ApplynowTpd(scriptHelper);
					chnageyourinsurance.applytpdcover_HOSTPLUS();

									}

				public void applyip_HOSTPLUS() {
					HOSTPLUS_Angular_ApplynowIp chnageyourinsurance = new HOSTPLUS_Angular_ApplynowIp(scriptHelper);
					chnageyourinsurance.applyipcover_HOSTPLUS();

									}

				//*********FirstSuper AngularJS business functions*******\\

				public void invokeFirstSuperapplication()
				{
					String strApplicationURL = dataTable.getData("FirstSuper_Login", "URL");
					//driver.get(properties.getProperty("ApplicationUrl"));


					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

					sleep(500);
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  			Dimension d = new Dimension(1410,1150);
				  			//Resize the current window to the given dimension
				  			driver.manage().window().setSize(d);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
					//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
							//strApplicationURL, Status.DONE);
					report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
				}
				public void login_FirstSuper() {
					FirstSuper_Angular_Login Login = new FirstSuper_Angular_Login(scriptHelper);
					Login.FirstSuper_Login();
				}

				public void transferyourcover_FirstSuper() {
					FirstSuper_Angular_TransferYourCover transfercover = new FirstSuper_Angular_TransferYourCover(scriptHelper);
					transfercover.TransferCover_FirstSuper();
				}

				public void transferyourcover_FirstSuper_Negative() {
					FirstSuper_Angular_TransferYourCover transfercover = new FirstSuper_Angular_TransferYourCover(scriptHelper);
					transfercover.TransferCover_FirstSuper_Negative();
				}

				public void changeyourinsurancecover_FirstSuper() {
					FirstSuper_Angular_ChangeYourInsuranceCover changecover = new FirstSuper_Angular_ChangeYourInsuranceCover(scriptHelper);
					changecover.ChangeYourInsuranceCover_FirstSuper();
				}
				public void eApplyRateValidation_FirstSuper() {
					FirstSuper_Angular_ChangeYourInsuranceCover changecover = new FirstSuper_Angular_ChangeYourInsuranceCover(scriptHelper);
					changecover.EApplyRateCheck_FirstSuper();
				}

				public void insuranceCost_FirstSuper() {
					FirstSuper_Angular_InsuranceCost changecover = new FirstSuper_Angular_InsuranceCost(scriptHelper);
					changecover.InsuranceCost_FirstSuper();
				}

				public void updateoccupation_FirstSuper() {
					FirstSuper_Angular_UpdateYourOccupation updateoccupation = new FirstSuper_Angular_UpdateYourOccupation(scriptHelper);
					updateoccupation.UpdateYourOccupation_FirstSuper();
				}
				public void cancelcover_FirstSuper() {
					FirstSuper_Angular_CancelYourCoverPage cancelcover = new FirstSuper_Angular_CancelYourCoverPage(scriptHelper);
					cancelcover.CancelYourCoverCover_FirstSuper();
				}
				public void convertandmaintain_FirstSuper() {
					FirstSuper_Angular_ConvertandMaintain convertandmaintain = new FirstSuper_Angular_ConvertandMaintain(scriptHelper);
					convertandmaintain.ConvertandMaintain_FirstSuper();
				}

				public void saveandexit_FirstSuper() {
					FirstSuper_Angular_SaveAndExitPage saveandexit = new FirstSuper_Angular_SaveAndExitPage(scriptHelper);
					saveandexit.SaveandExit_FirstSuper();
				}

				public void retrieveSavedApp_FirstSuper() {
					FirstSuper_Angular_RetrieveSavedAppPage retrieve = new FirstSuper_Angular_RetrieveSavedAppPage(scriptHelper);
					retrieve.RetreiveApp_FirstSuper();
				}

				public void landingpage_FirstSuper() {
					FirstSuper_Angular_LandingPage landingpage = new FirstSuper_Angular_LandingPage(scriptHelper);
					landingpage.OtherLinks_FirstSuper();
				}


				////statewide///////////////////

				public void invokeSTATEapplication() {
					String strApplicationURL = dataTable.getData("STATEWIDE_Login", "URL");
					// driver.get(properties.getProperty("ApplicationUrl"));

					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

					sleep(500);
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  			Dimension d = new Dimension(1410,1150);
				  			//Resize the current window to the given dimension
				  			driver.manage().window().setSize(d);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}

					report.updateTestLog("Invoke Application",
							"Invoked the application under test", Status.DONE);
				}

			/*	public void login_STATEWIDE() {
					STATEWIDE_Angular_Login Login = new STATEWIDE_Angular_Login(scriptHelper);
					Login.HOSTPLUS_Login();
				}

				public void convertandMaintain_STATEWIDE() {
					STATEWIDE_Angular_ConvertandMaintain convertandmaintain = new STATEWIDE_Angular_ConvertandMaintain(
							scriptHelper);
					convertandmaintain.ConvertandMaintain_STATEWIDE();

				}

				public void enterSTATEWIDE_UpdateYourOccupation() {
					STATEWIDE_Angular_UpdateYourOccupation occupation = new STATEWIDE_Angular_UpdateYourOccupation(
							scriptHelper);
					occupation.UpdateYourOccupation_STATEWIDE();
				}

				public void enterSTATEWIDE_SaveandExitFlow() {
					STATEWIDE_SaveAndExitPage statesaveandexit = new STATEWIDE_SaveAndExitPage(
							scriptHelper);
					statesaveandexit.SaveandExit_STATEWIDE();
				}

				public void enterSTATEWIDE_Cancelcover() {
					STATEWIDE_Angular_CancelYourCoverPage cancel = new STATEWIDE_Angular_CancelYourCoverPage(
							scriptHelper);
					cancel.CancelYourCoverCover_STATEWIDE();
				}

				public void enterSTATEWIDE_Retrieveflow() {
					STATEWIDE_RetrieveSavedApp cancel = new STATEWIDE_RetrieveSavedApp(
							scriptHelper);
					cancel.STATEWIDERetreiveApp();
				}

				public void enterSTATEWIDE_Lifeeventpositiveflow() {
					STATEWIDE_Angular_LifeEvent lifeevent = new STATEWIDE_Angular_LifeEvent(
							scriptHelper);
					lifeevent.LifeEvent_STATEWIDE();
				}

				public void enterSTATEWIDE_Lifeeventnegativeflow() {
					STATEWIDE_Angular_LifeEvent lifeeventnegative = new STATEWIDE_Angular_LifeEvent(
							scriptHelper);
					lifeeventnegative.LifeEventNegative_STATEWIDE();
				}

				public void enterSTATEWIDE_Otherlinks() {
					STATEWIDE_OtherLinksPage links = new STATEWIDE_OtherLinksPage(
							scriptHelper);
					links.STATEWIDEOtherLinks();
				}

				public void entertransferyourcover_STATEWIDE() throws Exception {
					STATEWIDE_Angular_TransferYourCover transfercover = new STATEWIDE_Angular_TransferYourCover(
							scriptHelper);
					transfercover.TransferCover_STATEWIDE();
				}

				public void entertransferyourcovernegative_STATEWIDE() throws Exception {
					STATEWIDE_Angular_TransferYourCover transfercovernegative = new STATEWIDE_Angular_TransferYourCover(
							scriptHelper);
					transfercovernegative.TransferCover_STATEWIDE_Negative();
				}

				public void enterChangeyourinsurance_STATEWIDE() throws Exception {
					STATEWIDE_Angular_ChangeYourInsuranceCover Changeinsur = new STATEWIDE_Angular_ChangeYourInsuranceCover(
							scriptHelper);
					Changeinsur.ChangeYourInsuranceCover_STATEWIDE();
				}

				public void entereapply_STATEWIDE() throws Exception {
					STATEWIDE_Angular_ChangeYourInsuranceCover eapply = new STATEWIDE_Angular_ChangeYourInsuranceCover(
							scriptHelper);
					eapply.Eapplyvalidation_STATEWIDE();
				}

				public void enterSTATEWIDE_Retrieveappflow() {
					STATEWIDE_Retrieveflow retrieve = new STATEWIDE_Retrieveflow(
							scriptHelper);
					retrieve.RetreiveApplication_Statewide();
				}

				public void insuranceCost_STATEWIDE() {
					STATEWIDE_Angular_InsuranceCost insurancecost = new STATEWIDE_Angular_InsuranceCost(
							scriptHelper);
					insurancecost.InsuranceCost_STATEWIDE();
				}

				*/


								/////**************CARESUPER*****/////

						public void invokeCareSuperapplication() {
							String strApplicationURL = dataTable.getData("CareSuper_Login", "URL");
							// driver.get(properties.getProperty("ApplicationUrl"));

							driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

							sleep(500);
							properties = Settings.getInstance();
							String StrBrowseName=properties.getProperty("currentBrowser");

						  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
						  			System.out.println(StrBrowseName);
						  			driver.get(strApplicationURL);
						  	//		Dimension d = new Dimension(1410,1150);
						  			Dimension d = new Dimension(1500,1200);
						  			//Resize the current window to the given dimension
						  			driver.manage().window().setSize(d);
						  		}
						  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
						  			System.out.println(StrBrowseName);
						  			driver.get(strApplicationURL);
						  		}
						  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
						  			System.out.println(StrBrowseName);
						  			driver.get(strApplicationURL);
						  		}

							report.updateTestLog("Invoke eclaims Application",
									"Invoked the application under test", Status.DONE);
						}

						public void login_CareSuper() {
							Caresuper_loginpage Login_care = new Caresuper_loginpage(scriptHelper);
							Login_care.CareSuper_Login_details();
						}


						public void enterCareSuper_Cancelcover() {
							CareSuper_Angular_CancelYourCoverPage cancelflow = new CareSuper_Angular_CancelYourCoverPage(
									scriptHelper);
							cancelflow.Cancel_YourCover_CareSuper();
						}

						public void enterCareSuper_UpdateOccupation() {
							CareSuper_Angular_UpdateOccupation occupation = new CareSuper_Angular_UpdateOccupation(
									scriptHelper);
							occupation.Update_Occupation_CareSuper();
						}

						public void enterCareSuper_Transfercover() {
							CareSuper_Angular_Transfer_existingCover occupation = new CareSuper_Angular_Transfer_existingCover(
									scriptHelper);
							occupation.TransferCover_CareSuper();
						}

						public void enterCareSuper_Retrieveapp() {
							CareSuper_Angular_RetrieveSaveapp Retrieve = new CareSuper_Angular_RetrieveSaveapp(
									scriptHelper);
							Retrieve.RetreiveApp_CareSuper();
						}

						public void enterCareSuper_applyDeathCover() {
							CareSuper_Angular_ApplynowDeathcover deathcover = new CareSuper_Angular_ApplynowDeathcover(
									scriptHelper);
							deathcover.Applynow_Death();
						}

						public void enterCareSuper_applytpdCover() {
							CareSuper_Angular_ApplynowTPDcover tpdcover = new CareSuper_Angular_ApplynowTPDcover(
									scriptHelper);
							tpdcover.Applynow_Death();
						}

						public void enterCareSuper_applyIpCover() {
							CareSuper_Angular_ApplynowIPcover Ipcover = new CareSuper_Angular_ApplynowIPcover(
									scriptHelper);
							Ipcover.Applynow_Ip();
						}

						public void enterCareSuper_ChangeYourInsurnace() {
							CareSuper_Angular_ChangeYourInsurance changecover = new CareSuper_Angular_ChangeYourInsurance(
									scriptHelper);
							changecover.ChangeYourInsuranceCover_CareSuper();
						}

						public void enterCareSuper_EapplyRatecheck() {
							CareSuper_Angular_ChangeYourInsurance eapply = new CareSuper_Angular_ChangeYourInsurance(
									scriptHelper);
							eapply.EApplyRateCheck_CareSuper();
						}

						public void enterCareSuper_SaveandExit() {
							CareSuper_Angular_SaveandExit savepersonal = new CareSuper_Angular_SaveandExit(
									scriptHelper);
							savepersonal.SaveandExit_CareSuper();
						}

						public void enterCareSuper_SaveandExithealth() {
							CareSuper_Angular_SaveandExit_healthpage health = new CareSuper_Angular_SaveandExit_healthpage(
									scriptHelper);
							health.SaveandExit_Health_CareSuper();
						}

						public void enterCareSuper_SaveandExitconfirmation() {
							CareSuper_Angular_SaveandExit_confirmationpage confirmation = new CareSuper_Angular_SaveandExit_confirmationpage(
									scriptHelper);
							confirmation.SaveandExit_Confirmationpage_CareSuper();
						}

						public void enterMetflowvalidation() {
							MetflowIntegration_CaseSearch metflow = new MetflowIntegration_CaseSearch(
									scriptHelper);
							metflow.MetFlowCaseSearch();
						}



						/*public void enterCareSuper_Insurancecost() {
							CareSuper_Angular_Insurancecost cost = new CareSuper_Angular_Insurancecost(
									scriptHelper);
							cost.InsuranceCost_CareSuper();
						}
						public void enterCareSuper_RetrieveValidate() {
							CareSuper_Retrieve_Validate validate = new CareSuper_Retrieve_Validate(
									scriptHelper);
							validate.SaveandExit_CareSupervalidate();
						}

						public void enterCareSuper_RetrievedataValidate() {
							CareSuper_Angular_Retrievevalidateconfirmation validata = new CareSuper_Angular_Retrievevalidateconfirmation(
									scriptHelper);
							validata.RetreiveAppvalidate_CareSuper();
						}

						public void enterCareSuper_personaldataValidation() {
							CareSuper_Angular_Retrievevalidatepersonal personal = new CareSuper_Angular_Retrievevalidatepersonal(
									scriptHelper);
							personal.RetreiveAppvalidatepersonal_CareSuper();
						}
						public void enterCareSuper_confirmationdataValidation() {
							CareSuper_Angular_Retrievevalidatehealthpage personal = new CareSuper_Angular_Retrievevalidatehealthpage(
									scriptHelper);
							personal.RetreiveAppvalidatehealth_CareSuper();
						}*/




			/////////Eclaims

				/*public void invokeclaimsapplication() {
					String strApplicationURL = dataTable.getData("Eclaims_Login", "URL");
					// driver.get(properties.getProperty("ApplicationUrl"));

					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

					sleep(500);
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  	//		Dimension d = new Dimension(1410,1150);
				  			Dimension d = new Dimension(1500,1200);
				  			//Resize the current window to the given dimension
				  			driver.manage().window().setSize(d);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}

					report.updateTestLog("Invoke eclaims Application",
							"Invoked the application under test", Status.DONE);
				}*/

				public void invokeclaimsapplication() {
					Claims_Angular_Login login = new Claims_Angular_Login(scriptHelper);
							login.Eclaims_Login();
				}

				public void enterclaims_DEATH() {
					Claim_Angular_Deathflow death = new Claim_Angular_Deathflow(scriptHelper);
					death.E_Deathclaims();
				}

				public void enterclaims_DEATH_negativetc() {
					Claim_Angular_Deathflow death = new Claim_Angular_Deathflow(scriptHelper);
					death.E_Deathclaims_nagativescenario();
				}

				public void enterclaims_Injury() {
					Claim_Angular_claimtype_Injury injury = new Claim_Angular_claimtype_Injury(scriptHelper);
					injury.E_injury();
				}
				public void enterclaims_Injury_negativetc() {
					Claim_Angular_claimtype_Injury injury = new Claim_Angular_claimtype_Injury(scriptHelper);
					injury.Eclaims_injurynegativescenario();
				}

				public void enterclaims_Life() {
					Claim_Angular_claimtype_life life = new Claim_Angular_claimtype_life(scriptHelper);
					life.E_Lifeclaims();
				}
				public void enterclaims_Life_negativetc() {
					Claim_Angular_claimtype_life life = new Claim_Angular_claimtype_life(scriptHelper);
					life.E_Lifeclaims_nagativescenario();
				}

				public void enterclaims_Unemployment() {
					Claim_Angular_claimtype_unemployment employ = new Claim_Angular_claimtype_unemployment(scriptHelper);
					employ.Eclaims_involuntaryunemployment();
				}

				public void enterclaims_Unemployment_negativetc() {
					Claim_Angular_claimtype_unemployment employ = new Claim_Angular_claimtype_unemployment(scriptHelper);
					employ.Eclaims_involuntaryunemploymentnegativetc();
				}
				public void enterclaims_Criticalillness() {
					Claim_Angular_claimtype_criticalillness illness = new Claim_Angular_claimtype_criticalillness(scriptHelper);
					illness.Eclaims_illness();
				}

				public void enterclaims_Criticalillnessnegativetc() {
					Claim_Angular_claimtype_criticalillness criticalillness = new Claim_Angular_claimtype_criticalillness(scriptHelper);
					criticalillness.Eclaims_criticalillnessnegativescenario();
				}
				public void enter_eview() {
					Eview view = new Eview(scriptHelper);
					view.HOSTPLUS_Login();
				}


				public void invokeAEISapplication() {

					String strApplicationURL = dataTable.getData("HOSTPLUS_Login", "URL");
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					sleep(500);
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  			Dimension d = new Dimension(1410,1150);
				  			//Resize the current window to the given dimension
				  			driver.manage().window().setSize(d);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
				  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
				  			System.out.println(StrBrowseName);
				  			driver.get(strApplicationURL);
				  		}
					report.updateTestLog("Invoke Application", "Invoked AEIS application", Status.DONE);
				}

			/*	public void login_AEIS(){

				AEIS_Login login = new AEIS_Login(scriptHelper);
						login.AEIS_Login();
				}
			public void cancelcover_AEIS() {
				AEIS_cancelyourcover cancelflow = new AEIS_cancelyourcover(
						scriptHelper);
				cancelflow.CancelYourCoverCover_AEIS();
			}

			public void applydeath_AEIS() {
				AEIS_ApplyDeathcover death = new AEIS_ApplyDeathcover(scriptHelper);
				death.ApplyDeathcover_AEIS();

								}

			public void applytpd_AEIS() {
				AEIS_ApplynowTPD tpd = new AEIS_ApplynowTPD(scriptHelper);
				tpd.ApplynowTPD_AEIS();

								}

			public void applyip_AEIS() {
				AEIS_ApplynowIPcover ip = new AEIS_ApplynowIPcover(scriptHelper);
				ip.ApplynowIP_AEIS();

								}
			public void retrieveapp_AEIS() {
				AEIS_Retrievesavedapp retrieve = new AEIS_Retrievesavedapp(scriptHelper);
				retrieve.RetreiveApp_AEIS();

								}
			public void changecover_AEIS() {
				AEIS_ChangeYourInsuranceCover changecover = new AEIS_ChangeYourInsuranceCover(scriptHelper);
				changecover.ChangeYourInsuranceCover_AEIS();

								}

			public void changeyourcover_AEIS() {
				AEIS_ChangeYourInsuranceCover changecover = new AEIS_ChangeYourInsuranceCover(scriptHelper);
				changecover.ChangeYourInsuranceCover_AEIS();

								}

			public void savequotepage_AEIS() {
				AEIS_SaveQuotepage quote = new AEIS_SaveQuotepage(scriptHelper);
				quote.Savequote_AEIS();

								}

			public void savehealthpage_AEIS() {
				AEIS_SaveHealthandlifestylepage health = new AEIS_SaveHealthandlifestylepage(scriptHelper);
				health.SaveHealthpage_AEIS();

								}

			public void saveconfirmationpage_AEIS() {
				AEIS_Confirmationpage confirmation = new AEIS_Confirmationpage(scriptHelper);
				confirmation.Saveconfirmationpage_AEIS();

								}*/



						//*********VICSUPER***********\\


					public void invokeVicSuperapplication() {
						String strApplicationURL = dataTable.getData("VicSuper_Login", "URL");
						// String Browser = dataTable.getData("General_Data", "Browser");
						driver.get(strApplicationURL);
						sleep(1000);
					}
					/*
					 * public void invokeHOSTPLUSapplication(){ String strApplicationURL =
					 * dataTable.getData("HOSTPLUS_Login", "URL"); // String Browser =
					 * dataTable.getData("General_Data", "Browser");
					 * driver.get(strApplicationURL);
					 *
					 * }
					 */

					public void login_Vicsuper() {

						VicSuper__Login changecover = new VicSuper__Login(scriptHelper);
						changecover.VicSuper_Login();

					}


					// -----------------------------------------------New
					// MemberFunction--------------------------------------------------------//

					public void vicSuper_NewMemberFunction() throws Exception {
						Vicsuper_NewMemberFunction NewMemberFunction = new Vicsuper_NewMemberFunction(scriptHelper);
						NewMemberFunction.NewMember_vicsuper();
					}

					public void newMemberFunctionOccupationRating_Vicsuper() throws Exception {
						Vicsuper_NewMemberFunctionOccupationRating ChangeYourInsuranceCover = new Vicsuper_NewMemberFunctionOccupationRating(
								scriptHelper);
						ChangeYourInsuranceCover.NewMemberFunctionOccupationRating();
					}

					// -----------------------------------------------Update
					// Occupation--------------------------------------------------------//

					public void updateOccupation_Vicsuper() {

						VicsSuper_UpdateYourOccupation updateOccupation = new VicsSuper_UpdateYourOccupation(scriptHelper);
						updateOccupation.UpdateYourOccupation_VicsSuper();

					}

					// -----------------------------------------------Transfer
					// Cover--------------------------------------------------------//

					public void transferCover_Vicsuper() {

						VicsSuper_TransferYourCover transferCover = new VicsSuper_TransferYourCover(scriptHelper);
						transferCover.TransferCover_VicsSuper();
					}

					public void transferCover_Vicsuper_Negative() {

						VicsSuper_TransferYourCover transferCover = new VicsSuper_TransferYourCover(scriptHelper);
						transferCover.TransferCover_VicsSuper_Negative();
					}

					// Transfer Cover -Save and Exit
					public void transferCoverSaveAndExit_Vicsuper() {

						VicsSuper_TransferYourCover_SaveandExit transferCover = new VicsSuper_TransferYourCover_SaveandExit(
								scriptHelper);
						transferCover.TransferCover_Vicsuper_SaveAndExit();
					}

					// Save Exit Validation
					public void transferCoverSaveAndExitValidaiton_Vicsuper() {

						VicsSuper_TransferYourCover_SaveandExit transferCover = new VicsSuper_TransferYourCover_SaveandExit(
								scriptHelper);
						transferCover.TransferCover_Vicsuper_Validation();
					}


					public void changecover() throws Exception {
				        ChangeCover ChangeYourInsuranceCover = new ChangeCover(scriptHelper);
				        ChangeYourInsuranceCover.ChangeYourInsuranceCover_vicsuper("ChangeCover","Overall");
				 }

				 public void changecoverOccupationRating() throws Exception {
				        ChangeCover ChangeYourInsuranceCover = new ChangeCover(scriptHelper);
				 ChangeYourInsuranceCover.ChangeYourInsuranceCover_vicsuper("ChangeCoverOccupationRating","OccupationRating");
				 }
				 public void changecoverPremium() throws Exception {
				        ChangeCover ChangeYourInsuranceCover = new ChangeCover(scriptHelper);
				 ChangeYourInsuranceCover.ChangeYourInsuranceCover_vicsuper("ChangeCoverPremium","PremiumCalculation");
				 }


				 public void fixcover() throws Exception {
				        FixCover FixYourCover = new FixCover(scriptHelper);
				        FixYourCover.FixYourCover("FixYourCover","Overall");
				 }
				 public void fixcoverOccupationRating() throws Exception {
				        FixCover FixYourCover = new FixCover(scriptHelper);
				        FixYourCover.FixYourCover("FixCoverOccupationRating","OccupationRating");
				 }
				 public void fixcoverPremium() throws Exception {
				        FixCover fixYourCover = new FixCover(scriptHelper);
				        fixYourCover.FixYourCover("FixCoverPremium","PremiumCalculation");
				 }


				 public void lifeEvent() throws Exception {
					 LifeEvent LifeEventobj = new LifeEvent(scriptHelper);
					 LifeEventobj.LifeEvent_vicsuper("LifeEvent","Overall");
					}

					public void lifeEventOccupationRating() throws Exception {
					 LifeEvent LifeEventobj = new LifeEvent(scriptHelper);
					 LifeEventobj.LifeEvent_vicsuper("LifeEvent_OccupationRating","OccupationRating");
					}
					public void lifeEventPremium() throws Exception {
					 LifeEvent LifeEventobj = new LifeEvent(scriptHelper);
					 LifeEventobj.LifeEvent_vicsuper("LifeEvent_Premium","PremiumCalculation");
					}

				 public void updateOccupation() throws Exception {
					 UpdateOccupation UpdateOccupationobj = new UpdateOccupation(scriptHelper);
					 UpdateOccupationobj.UpdateYourOccupation_vicsuper("OccupationUpdate","OccupationRating");
				}
				 public void landingpage() throws Exception {
						VicSuper_LandingPage landing = new VicSuper_LandingPage(scriptHelper);
						landing.LandingPage_VicSuper();
						}
				 public void changecovermetflow() throws Exception {
						ChangeCover_metflow ChangeYourInsuranceCover = new ChangeCover_metflow(scriptHelper);
					    ChangeYourInsuranceCover.ChangeYourInsuranceCover_vicsuper("Overall");
					}

				//***Eapply corperate***//

			/*		public void invoke_eapply() {
						Eapply_Login login = new Eapply_Login(scriptHelper);
						login.eapplyLogin();
					}

					public void eapply_fileupload() {
						Eapply_fileupload upload = new Eapply_fileupload(scriptHelper);
						upload.eapply_fileupload();
					}

					public void eapply_onboarding() {
						Eapply_Onboardingpage onboard = new Eapply_Onboardingpage(scriptHelper);
						onboard.eapply_onboarding();
					}

					public void eapply_onboard() {
						Eapply_Onboardingpage onboard = new Eapply_Onboardingpage(scriptHelper);
						onboard.eapply_onboarding();
					}
					public void eapply_insurancememberflow() {
						Eapply_Insurance insuranceflow = new Eapply_Insurance(scriptHelper);
						insuranceflow.Eapply_Insuranceflow();
					}


					public void eapply_applyinsurancedirect() {
						Eapply_changeinsurance changeinsurance = new Eapply_changeinsurance(scriptHelper);
						changeinsurance.eapply_insurancecover();
					}

					public void eapply_PDF_Validation() {
						Eapply_PDF_Validation PDF = new Eapply_PDF_Validation(scriptHelper);
						PDF.eapply_insurancecover();
					}

					public void eapply_Metflow_Validation() {
						Eapply_Metflow metlfow = new Eapply_Metflow(scriptHelper);
						metlfow.eapply_metflowvalidation();
					}

					public void eapply_Savefunctionality() {
						Eapply_changeinsurance_Save_Exit save = new Eapply_changeinsurance_Save_Exit(scriptHelper);
						save.eapply_Savefunctionality();
					}

					public void eapply_Saveflow() {
						Eapply_Member_Save_Exit save = new Eapply_Member_Save_Exit(scriptHelper);
						save.Eapply_Insuranceflow();
					}


					public void eapply_Errorvalidation() {
						Eapply_error_validation error = new Eapply_error_validation(scriptHelper);
						error.eapply_Errormessage();
					}*/

}