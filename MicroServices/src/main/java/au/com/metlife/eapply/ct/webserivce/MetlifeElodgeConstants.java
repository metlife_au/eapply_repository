package au.com.metlife.eapply.ct.webserivce;

public class MetlifeElodgeConstants {
	private MetlifeElodgeConstants(){
		
	}
	
	public static final String PH_NUM = "1300 555 625";
	
	public static final String FROM_SENDER = "metlife.service@au.metlife.com";
	
	public static final String CLIENT_CONFIG = "/var/WebSphere/props/eApplication/client_config";
	
	public static final String RETRIEVE_UW_WSDL = "https://awweb.e2e.au.metlifeasia.com/CAW2/services/EAPPSearch";
	public static final String CREATE_UPDATE_UW_WSDL = "https://awweb.e2e.au.metlifeasia.com/CAW2/services/PicsLodgement";
	
	
	public static final String ENGAGE_MODULE = "rampart";
	public static final String PICS_USERNAME = "internal01";
	//public final static String PICS_PASSWORD= "metlife890"; 
	public static final String PICS_SUCCESS_CODE= "200";
	
    
    
	
}

