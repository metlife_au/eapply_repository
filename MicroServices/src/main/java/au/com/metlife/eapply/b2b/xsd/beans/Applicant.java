package au.com.metlife.eapply.b2b.xsd.beans;



public class Applicant {
	
	PersonalDetails personalDetails;
	ExistingCover existingCovers;
	Address address;
	ContactDetails contactDetails;
	String memberType;
	String memberDivision;
	String segmentCode;
	public String getMemberDivision() {
		return memberDivision;
	}
	public void setMemberDivision(String memberDivision) {
		this.memberDivision = memberDivision;
	}
	public String getSegmentCode() {
		return segmentCode;
	}
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
	String clientRefNumber;
	String applicantRole;
	String dateJoined;
	String dateJoinedCompany;
	String welcomeLetterDate;
	//Vicsuper Changes :- Added special offer property to accept from input XML.
	String siopEndDate;
	
	public String getWelcomeLetterDate() {
		return welcomeLetterDate;
	}
	public void setWelcomeLetterDate(String welcomeLetterDate) {
		this.welcomeLetterDate = welcomeLetterDate;
	}
	public String getDateJoinedCompany() {
		return dateJoinedCompany;
	}
	public void setDateJoinedCompany(String dateJoinedCompany) {
		this.dateJoinedCompany = dateJoinedCompany;
	}
	public String getDateJoined() {
		return dateJoined;
	}
	public void setDateJoined(String dateJoined) {
		this.dateJoined = dateJoined;
	}
	public String getApplicantRole() {
		return applicantRole;
	}
	public void setApplicantRole(String applicantRole) {
		this.applicantRole = applicantRole;
	}
	public String getClientRefNumber() {
		return clientRefNumber;
	}
	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ContactDetails getContactDetails() {
		return contactDetails;
	}
	public void setContactDetails(ContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}	
	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}
	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public ExistingCover getExistingCovers() {
		return existingCovers;
	}
	public void setExistingCovers(ExistingCover existingCovers) {
		this.existingCovers = existingCovers;
	}
	public String getSiopEndDate() {
		return siopEndDate;
	}
	public void setSiopEndDate(String siopEndDate) {
		this.siopEndDate = siopEndDate;
	}
	
	
}
