package au.com.metlife.eapply.bs.pdf;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;

public class HTMLToString {
	private HTMLToString(){
		
	}
    
	private static String[] dictionary = { "</P>", "</p>", "<BR>", "<br>",
        "<LI>", "<li>", MetlifeInstitutionalConstants.UL, MetlifeInstitutionalConstants.UL_CASE, "<P>", "<p>", "<U>", "<B>", "</U>",
        "</B>", "<u>", "<b>", "</u>", "</b>", MetlifeInstitutionalConstants.LI, MetlifeInstitutionalConstants.LI_SPACE, MetlifeInstitutionalConstants.U_STRONG,
        MetlifeInstitutionalConstants.U_STRONG_CASE, MetlifeInstitutionalConstants.STRONG_CASE_U, MetlifeInstitutionalConstants.STRONG_U, MetlifeInstitutionalConstants.STRONG_CASE_WITH,
        "</strong", MetlifeInstitutionalConstants.STRONG_CASE, "<stromg>", "<UL>", "<ul>", "<A", "<a",
        MetlifeInstitutionalConstants.SPAN, MetlifeInstitutionalConstants.SPAN_CASH, "</a>", "</A>", MetlifeInstitutionalConstants.SPAN_WITH, MetlifeInstitutionalConstants.SPAN_WITH_SPACE };
    
    public static String convertHTMLToString(String source) {
		String output = source;
        boolean flag = false;
        if (output.contains( "</P>")) {
            output = output.replace( "</P>", "\n\n");
        }
        if (output.contains( "</p>")) {
            output = output.replace( "</p>", "\n\n");
        }
        if (output.contains( "<BR>")) {
            output = output.replace( "<BR>", "\n\n");
        }
        if (output.contains( "<br>")) {
            output = output.replace( "<br>", "\n\n");
        }
        if (output.contains( "<LI>")) {
            output = output.replace( "<LI>", "\n\t-\t");
        }
        if (output.contains( "<li>")) {
            output = output.replace( "<li>", "\n\t-\t");
        }
        if (output.contains( MetlifeInstitutionalConstants.UL)) {
            output = output.replace( MetlifeInstitutionalConstants.UL, "\n\r");
        }
        if (output.contains( MetlifeInstitutionalConstants.UL_CASE)) {
            output = output.replace( MetlifeInstitutionalConstants.UL_CASE, "\n\r");
        }
        if (output.contains( "<P>")) {
            output = output.replace( "<P>", "");
        }
        if (output.contains( "<p>")) {
            output = output.replace( "<p>", "");
        }
        if (output.contains( "<U>")) {
            output = output.replace( "<U>", "");
        }
        if (output.contains( "<B>")) {
            output = output.replace( "<B>", "");
        }
        if (output.contains( "</U>")) {
            output = output.replace( "</U>", "");
        }
        if (output.contains( "</B>")) {
            output = output.replace( "</B>", "");
        }
        if (output.contains( "<u>")) {
            output = output.replace( "<u>", "");
        }
        if (output.contains( "<b>")) {
            output = output.replace( "<b>", "");
        }
        if (output.contains( "</u>")) {
            output = output.replace( "</u>", "");
        }
        if (output.contains( "</b>")) {
            output = output.replace( "</b>", "");
        }
        if (output.contains( MetlifeInstitutionalConstants.LI)) {
            output = output.replace( MetlifeInstitutionalConstants.LI, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.LI_SPACE)) {
            output = output.replace( MetlifeInstitutionalConstants.LI_SPACE, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.U_STRONG)) {
            output = output.replace( MetlifeInstitutionalConstants.U_STRONG, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.U_STRONG_CASE)) {
            output = output.replace( MetlifeInstitutionalConstants.U_STRONG_CASE, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.STRONG_CASE_U)) {
            output = output.replace( MetlifeInstitutionalConstants.STRONG_CASE_U, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.STRONG_U)) {
            output = output.replace( MetlifeInstitutionalConstants.STRONG_U, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.STRONG_CASE_WITH)) {
            output = output.replace( MetlifeInstitutionalConstants.STRONG_CASE_WITH, "");
        }
        if (output.contains( "</strong")) {
            output = output.replace( "</strong>", "");
        }
        if (output.contains( MetlifeInstitutionalConstants.STRONG_CASE)) {
            output = output.replace( MetlifeInstitutionalConstants.STRONG_CASE, "");
        }
        if (output.contains( "<stromg>")) {
            output = output.replace( "<strong>", "");
        }
        if (output.contains( "<UL>")) {
            output = output.replace( "<UL>", "\t");
        }
        if (output.contains( "<ul>")) {
            output = output.replace( "<ul>", "\t");
        }
        if (output.contains( "<A")) {
            String tempS = output.substring( output.indexOf( "<A"), output.length() - 1);
            String tempA = output.substring( output.indexOf( "<A"), output.indexOf( "<A")
                    + tempS.indexOf( '>') + 1);
            output = output.replace( tempA, "");
        }
        if (output.contains( "<a")) {
            
            String tempS = output.substring( output.indexOf( "<a"), output.length() - 1);
            String tempA = output.substring( output.indexOf( "<a"), output.indexOf( "<a")
                    + tempS.indexOf( '>') + 1);
            output = output.replace( tempA, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.SPAN)) {
            String tempS = output.substring( output.indexOf( MetlifeInstitutionalConstants.SPAN), output.length() - 1);
            String tempA = output.substring( output.indexOf( MetlifeInstitutionalConstants.SPAN), output.indexOf( MetlifeInstitutionalConstants.SPAN)
                    + tempS.indexOf( '>') + 1);
            output = output.replace( tempA, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.SPAN_CASH)) {
            String tempS = output.substring( output.indexOf( MetlifeInstitutionalConstants.SPAN_CASH), output.length() - 1);
            String tempA = output.substring( output.indexOf( MetlifeInstitutionalConstants.SPAN_CASH), output.indexOf( MetlifeInstitutionalConstants.SPAN_CASH)
                    + tempS.indexOf( '>') + 1);
            output = output.replace( tempA, "");
        }
        if (output.contains( "</a>")) {
            output = output.replace( "</a>", "");
        }
        if (output.contains( "</A>")) {
            output = output.replace( "</A>", "");
        }
        if (output.contains( MetlifeInstitutionalConstants.SPAN_WITH)) {
            output = output.replace( MetlifeInstitutionalConstants.SPAN_WITH, "");
        }
        if (output.contains( MetlifeInstitutionalConstants.SPAN_WITH_SPACE)) {
            output = output.replace( MetlifeInstitutionalConstants.SPAN_WITH_SPACE, "");
        }
        
        for (int i = 0; i < dictionary.length; i++) {
            
            if (( null != dictionary[i]) && output.contains( dictionary[i])) {
                flag = true;
                break;
            }
        }
        if (flag) {
            
            output = convertHTMLToString( output);
        }
        
        return output;
    }
}
