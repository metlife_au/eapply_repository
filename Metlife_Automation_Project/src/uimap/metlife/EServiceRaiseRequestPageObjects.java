/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class EServiceRaiseRequestPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public EServiceRaiseRequestPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	

	
	public static final By linRraiserqst = getObjectIdentifaction("eService.raiserequest.raisenewserrequest.link.css");
	public static final By chkUpdatecredit = getObjectIdentifaction("eService.raiserequest.updatecredit.chkbox.css");
	public static final By drpPaymeth= getObjectIdentifaction("eService.raiserequest.paymethod.dropdown.css");
	public static final By drpCCtype= getObjectIdentifaction("eService.raiserequest.cctype.dropdown.css");
	public static final By txtCCnum1= getObjectIdentifaction("eService.raiserequest.ccnumberfirst4.txt.css");
	public static final By txtCCnum2= getObjectIdentifaction("eService.raiserequest.ccnumbersecond4.txt.css");
	public static final By txtCCnum3= getObjectIdentifaction("eService.raiserequest.ccnumberthird4.txt.css");
	public static final By txtCCnum4= getObjectIdentifaction("eService.raiserequest.ccnumberfourth4.txt.css");
	public static final By txtCCExpmonth= getObjectIdentifaction("eService.raiserequest.ccnexpmonth.txt.css");
	public static final By txtCCExpyear= getObjectIdentifaction("eService.raiserequest.ccnexpyear.txt.css");
	public static final By linkDone= getObjectIdentifaction("eService.raiserequest.updatecreditdone.link.css");
}
