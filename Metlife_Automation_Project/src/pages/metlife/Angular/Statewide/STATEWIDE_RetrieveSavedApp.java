package pages.metlife.Angular.Statewide;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import supportlibraries.ScriptHelper;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
public class STATEWIDE_RetrieveSavedApp extends MasterPage {
	WebDriverUtil driverUtil = null;

	public STATEWIDE_RetrieveSavedApp STATEWIDERetreiveApp() {
		RetreiveApp();		
		
		
		return new STATEWIDE_RetrieveSavedApp(scriptHelper);
	}

	public STATEWIDE_RetrieveSavedApp(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void RetreiveApp() {

		
		
		try{	
			
			
                 //****Click on Change your insurance*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='newMemberId']/div/h4/span"))).click();		
			report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
			sleep(3000);
		
			List<WebElement>Applications=driver.findElements(By.xpath("//*[@id='collapseOne']/div/div/table/tbody/tr[41]/td[1]/a"));
		//	List<WebElement>Applications=driver.findElements(By.xpath("//*[@id='collapseOne']/div/div/table/tbody/tr"));
			
			int NoofApplication=Applications.size();
						
			if(NoofApplication<=0){
				report.updateTestLog("No Saved Applications", "There are no saved applications", Status.PASS);
			}
			
			else{
				report.updateTestLog("Saved Applications", "There are "+NoofApplication+"  saved applications", Status.PASS);
				String FirstApp=Applications.get(0).getText();
				Applications.get(0).click();
				report.updateTestLog("Retrieving Application", FirstApp+"  is Clicked", Status.PASS);
				sleep(2500);
				
				
			}					
		
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}
