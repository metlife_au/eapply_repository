package pages.metlife;

import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.STFTBeneficiariesPageObjects;

public class STFTBeneficiariesPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTBeneficiariesPage nominateBeneficiaries() {
		beneficiariesDetails();
		return new STFTBeneficiariesPage(scriptHelper);
	}

	public STFTBeneficiariesPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void beneficiariesDetails() {

		String title = dataTable.getData("MetLife_Data", "Title");
		String firstName = dataTable.getData("MetLife_Data", "FirstName_ben");
		String lastName = dataTable.getData("MetLife_Data", "LastName_ben");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String strRelation = dataTable.getData("MetLife_Data", "Relation");
		String percentage = dataTable.getData("MetLife_Data", "Percentage");
	//	String straddress = dataTable.getData("MetLife_Data", "aadress");
		
		try {
						
			String parentHandle = driver.getWindowHandle();
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTBeneficiariesPageObjects.btnnominate)).click().perform();
			Thread.sleep(1000);			
						
//			switchToActiveWindow();
			driver.manage().window().maximize();
			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}

			
			
			tryAction(fluentWaitElement(STFTBeneficiariesPageObjects.linkTitle), "specialDropdown", "Title", title);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.txtFirstName), "SET", "FirstName", firstName);
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.txtFirstName), "TAB", "firstName");
			
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.txtLastName), "SET", "LastName", lastName);
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.txtLastName), "TAB", "LastName");
			
			tryAction(fluentWaitElement(STFTBeneficiariesPageObjects.txtDOB), "SET", "Date of birth", getPremiumDOB(Integer.parseInt("-"+dob)));
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(STFTBeneficiariesPageObjects.selRelation), "specialDropdown", "Relation", strRelation);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.txtPercentage), "SET", "Percentage", percentage);
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.txtPercentage), "TAB", "Percentage");
			
			tryAction(waitForClickableElement(STFTBeneficiariesPageObjects.rdAddressYes), "clkradio",
					"Is the residential address the same as the policy owner's - Yes");
			
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(driver.findElement(STFTBeneficiariesPageObjects.btnsubmit)).click().perform();
			
			Thread.sleep(2000);	
			
			Actions actions2 = new Actions(driver);
			actions2.moveToElement(driver.findElement(STFTBeneficiariesPageObjects.btnfinish)).click().perform();
			Thread.sleep(1000);	
//			
//			switchToActiveWindow();
//			driver.manage().window().maximize();
			
			driver.manage().window().maximize();
			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
