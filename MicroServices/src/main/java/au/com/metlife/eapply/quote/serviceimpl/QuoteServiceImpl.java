package au.com.metlife.eapply.quote.serviceimpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.drools.KnowledgeBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.quote.model.ConvertModel;
import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.model.SelectItem;
import au.com.metlife.eapply.quote.service.QuoteService;
import au.com.metlife.eapply.quote.utility.DroolsHelper;
import au.com.metlife.eapply.quote.utility.EappRulesCacheProvider;
import au.com.metlife.eapply.quote.utility.QuoteConstants;
import au.com.metlife.eapply.quote.utility.QuoteHelper;

@Service
public class QuoteServiceImpl implements QuoteService {
	private static final Logger log = LoggerFactory.getLogger(QuoteServiceImpl.class);
	
	@Autowired(required = true)
	EappRulesCacheProvider eappRulesCacheProvider;

	@Override
	public QuoteResponse calculateDeath(RuleModel ruleModel) {
		log.info("Calculate death start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER) 
							&& !(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(ruleModel.getFundCode()))){
						if(!(null!=ruleModel.getDeathFixedAmount() && QuoteConstants.DC_UNITISED.equalsIgnoreCase(ruleModel.getDeathCoverType())))
						{
						if(ruleModel.getDeathExistingAmount()!=null && ruleModel.getDeathTransferAmount()!=null){
							ruleModel.setDeathFixedAmount(ruleModel.getDeathExistingAmount().add(ruleModel.getDeathTransferAmount()));
						}
						}
					}
					/*else if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_CCOVER)){

  				    }*/
					if(ruleModel.getDeathCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getDeathCoverType());
						if(ruleModel.getDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_UNITISED)){
							QuoteHelper.calculateDcUnitisedAmtAndCost(ruleModel,instProductRuleBO,quoteResponse,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}else if(ruleModel.getDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_FIXED)){
							QuoteHelper.calculateDeathFixedCost(instProductRuleBO,ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse);
						}
					}

				}
			}
		}catch(Exception e){
			log.error("Error in calculate death : {}",e);	
		}
		log.info("Calculate death finish");
		return quoteResponse;
	}
	@Override
	public QuoteResponse calculateFUL(RuleModel ruleModel) {
		log.info("Calculate FUL start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					
					/*else if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_CCOVER)){

  				    }*/
					if(ruleModel.getDeathFixedAmount().doubleValue()!=0 || ruleModel.getTpdFixedAmount().doubleValue()!=0 || ruleModel.getIpFixedAmount().doubleValue()!=0) {
							QuoteHelper.calculateFulAmount(instProductRuleBO,ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse);
					}
					}

			}
		}catch(Exception e){
			log.error("Error in calculate death: {}",e);	
		}
		log.info("Calculate death finish");
		return quoteResponse;
	}

	@Override
	public QuoteResponse calculateTpd(RuleModel ruleModel) {
		log.info("Calculate TPD start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					if(ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER)
							&& !(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(ruleModel.getFundCode()))){
						if(!(null!=ruleModel.getTpdFixedAmount() && QuoteConstants.TPD_UNITISED.equalsIgnoreCase(ruleModel.getTpdCoverType())))
						{
						if(ruleModel.getTpdExistingAmount()!=null && ruleModel.getTpdTransferAmount()!=null){
							ruleModel.setTpdFixedAmount(ruleModel.getTpdExistingAmount().add(ruleModel.getTpdTransferAmount()));
						}
						}
					}
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					if(ruleModel.getTpdCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getTpdCoverType());
						if(ruleModel.getTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_UNITISED)){
							QuoteHelper.calculateTpdUnitisedAmtAndCost(ruleModel,instProductRuleBO,quoteResponse,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}else if(ruleModel.getTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_FIXED)){
							QuoteHelper.calculateTPDFixedCost(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse,instProductRuleBO);
						}
					}
				}
			}
		}catch(Exception e){
			log.error("Error in calculate TPD : {}",e);
		}
		log.info("Calculate TPD finish");
		return quoteResponse;
	}

	@Override
	public QuoteResponse calculateIp(RuleModel ruleModel) {
		log.info("Calculate IP start");
		QuoteResponse quoteResponse=null;
		BigDecimal ipUnts=null;
		BigDecimal ipUnitsCvrAmount;
		BigDecimal ipFxdAmount=null;
		/*BigDecimal ipFixedCvrAmount;*/
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
				if(ruleModel.getManageType()!=null){
					ipUnts=new BigDecimal(ruleModel.getIpUnits());
					
					
					
					log.info("ipUnts---> {}",ipUnts);
					ipUnitsCvrAmount=ipUnts.multiply(BigDecimal.valueOf(instProductRuleBO.getIpUnitCostMultipFactor())).setScale(0, BigDecimal.ROUND_CEILING);					
					
					if(null!=ruleModel.getIpFixedAmount()){
						ipFxdAmount = ruleModel.getIpFixedAmount();
						if(ruleModel.getFundCode().equalsIgnoreCase("CARE") && ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_CCOVER) && (ipFxdAmount.intValue()>40000) )
						{
							ipFxdAmount=new BigDecimal("40000");
							if(ipUnitsCvrAmount.intValue()==40375)
							{
								ipFxdAmount=new BigDecimal("40375");
							}
						}
					}
					
					if(ipUnitsCvrAmount.intValue()==40375/* && "CARE".equalsIgnoreCase(ruleModel.getFundCode())*/){

						ipUnitsCvrAmount=new BigDecimal("40000");
					}
					
					/*ruleModel.setIpFixedAmount(ipUnitsCvrAmount); */
					
					if(null!=ruleModel.getIpCoverType() &&  "IpUnitised".equalsIgnoreCase(ruleModel.getIpCoverType())){
						ruleModel.setIpFixedAmount(ipUnitsCvrAmount);
					}else if(null!=ruleModel.getIpCoverType() &&  "IpFixed".equalsIgnoreCase(ruleModel.getIpCoverType())){
						ruleModel.setIpFixedAmount(ipFxdAmount);
					}
					
					
					if((ruleModel.getFundCode().equalsIgnoreCase("CARE") || QuoteConstants.PARTNER_HOST.equalsIgnoreCase(ruleModel.getFundCode()) || QuoteConstants.PARTNER_MTAA.equalsIgnoreCase(ruleModel.getFundCode())
							|| MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(ruleModel.getFundCode())) && ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER)){
						if(ruleModel.getIpExistingAmount()!=null && ruleModel.getIpTransferAmount()!=null
								&& !(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(ruleModel.getFundCode()))){
							/*SRKR: Need to consider which ever is greater of ip existing and transfer amount*/
							/*if(ruleModel.getIpExistingAmount().compareTo(ruleModel.getIpTransferAmount())>0){
								ruleModel.setIpFixedAmount(ruleModel.getIpExistingAmount());
							}else{
								ruleModel.setIpFixedAmount(ruleModel.getIpTransferAmount());
							}*/
							//Changes for Transfer cover Ip amount. Only the user entered value to be transfered.Take Existing only when user entered value is 0.
							if(ruleModel.getIpTransferAmount().intValue() > 0)
							{
							ruleModel.setIpFixedAmount(ruleModel.getIpTransferAmount());
							}
							else
							{
								ruleModel.setIpFixedAmount(ruleModel.getIpExistingAmount());
							}
						}else if(ruleModel.getIpTransferAmount()!=null){
							ruleModel.setIpFixedAmount(ruleModel.getIpTransferAmount());
						}else if(ruleModel.getIpExistingAmount()!=null){
							ruleModel.setIpFixedAmount(ruleModel.getIpExistingAmount());
						}
					}else{
						if(ruleModel.getIpExistingAmount()!=null && ruleModel.getIpTransferAmount()!=null){
							ruleModel.setIpFixedAmount(ruleModel.getIpExistingAmount().add(ruleModel.getIpTransferAmount()));
						}
					}
					if(ruleModel.getIpCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getIpCoverType());
						if(ruleModel.getIpCoverType().equalsIgnoreCase(QuoteConstants.IP_UNITISED) || ruleModel.getIpCoverType().equalsIgnoreCase(QuoteConstants.IP_FIXED)){
							QuoteHelper.calculateIPUntCost(ruleModel,instProductRuleBO,quoteResponse,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}/*else if(ruleModel.getIpCoverType().equalsIgnoreCase(QuoteConstants.IP_FIXED)){
							SRKR for future use
							QuoteHelper.calculateIPUntCost(ruleModel, instProductRuleBO, quoteResponse, eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
						}*/
					}
				}
			}
		}catch(Exception e){
			log.error("Error in calculate IP : {}",e);
		}
		log.info("Calculate IP finish");
		return quoteResponse;
	}

	@Override
	public List<SelectItem> getIndustryList(String fundCode) {
		log.info("Get Industry list start");
		CoverDetailsRuleBO industryRuleBO=null;
		List<SelectItem> list = new ArrayList<>();
		try{
			if(fundCode!=null){
				industryRuleBO=new CoverDetailsRuleBO();
				industryRuleBO.setFundCode(fundCode);
				industryRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING);
				if(eappRulesCacheProvider.getRuleMap()!=null && eappRulesCacheProvider.getRuleMap().get(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING)!=null){
					industryRuleBO.setKbase((KnowledgeBase)eappRulesCacheProvider.getRuleMap().get(QuoteConstants.RULE_NAME_INST_INDUSTRY_MAPPING));
				}
				log.info("industry rule file name >>>>>>> {}",industryRuleBO.getRuleFileName());
				industryRuleBO = DroolsHelper.invokeUC16BusinessRules(industryRuleBO,eappRulesCacheProvider.getFilePath());
				log.info("industry List >>>>>>> {}",industryRuleBO.getIndustryList());
				list = industryRuleBO.getIndustryList();
			}
		}catch(Exception e){
			log.error("Error in get industry list : {}",e);
		}
		log.info("Get Industry list finish");
		return list;
	}

	@Override
	public InstProductRuleBO getProductConfig(String fundCode, String memberType,String manageType) {
		log.info("Get product config start");
		InstProductRuleBO instProductRuleBO=null;
		try{
			if(fundCode!=null && memberType!=null){
					instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(fundCode, memberType,manageType,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
			}
		}catch(Exception e){
			log.error("Error in get product config : {}",e);
		}
		log.info("Get product config finish");
		return instProductRuleBO;
	}

	@Override
	public ConvertModel convertDeathFixedToUnits(RuleModel ruleModel) {
		log.info("Convert death fixed to units start");
		BigDecimal convertedUnits=null;
		ConvertModel convertModel=null;
		try{
			convertedUnits=QuoteHelper.convertDeathFixedToUnits(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath());
			convertModel=new ConvertModel();
			convertModel.setConvertedAmount(convertedUnits);
			convertModel.setExCoverAmount(ruleModel.getDeathExistingAmount());
			convertModel.setExCoverType(ruleModel.getExDeathCoverType());
		}catch(Exception e){
			log.error("Error in convert death fixed to unit: {}",e);
		}
		log.info("Convert death fixed to units finish");
		return convertModel;
	}

	@Override
	public ConvertModel convertTpdFixedToUnits(RuleModel ruleModel) {
		log.info("Convert tpd fixed to units start");
		BigDecimal convertedUnits=null;
		ConvertModel convertModel=null;
		try{
			convertedUnits=QuoteHelper.convertTpdFixedToUnits(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath());
			convertModel=new ConvertModel();
			convertModel.setConvertedAmount(convertedUnits);
			convertModel.setExCoverAmount(ruleModel.getTpdExistingAmount());
			convertModel.setExCoverType(ruleModel.getExTpdCoverType());
		}catch(Exception e){
			log.error("Error in convert tpd fixed to units : {}",e);
		}
		log.info("Convert tpd fixed to units finish");
		return convertModel;
	}
	
	@Override//AAL Logic
	public QuoteResponse calculateAAL(RuleModel ruleModel) {
		log.info("Calculate death start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!= null){
				quoteResponse=new QuoteResponse();
				
					QuoteHelper.calculateAAL(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse);
			
			}
		}catch(Exception e){
			log.error("Error in calculate death : {}",e);	
		}
		log.info("Calculate death finish");
		return quoteResponse;
	}
	

	public QuoteResponse calculateINGDDeath(RuleModel ruleModel) {
		log.info("Calculate INGD death start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					if(ruleModel.getDeathCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getDeathCoverType());
							QuoteHelper.calculateINGDDeathFixedCost(instProductRuleBO,ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse);
					}

				}
			}
		}catch(Exception e){
			log.error("Error in calculate INGD death : {}",e);	
		}
		log.info("Calculate INDG death finish");
		return quoteResponse;
	}
	
	@Override
	public QuoteResponse calculateINGDTpd(RuleModel ruleModel) {
		log.info("Calculate INGD TPD start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				if(ruleModel.getManageType()!=null){
					InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
					if(ruleModel.getTpdCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getTpdCoverType());
							QuoteHelper.calculateINGDTPDFixedCost(ruleModel, eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),quoteResponse,instProductRuleBO);
					}
				}
			}
		}catch(Exception e){
			log.error("Error in calculate TPD : {}",e);
		}
		log.info("Calculate INGD TPD finish");
		return quoteResponse;
	}
	
	@Override
	public QuoteResponse calculateINGDIp(RuleModel ruleModel) {
		log.info("Calculate INGD IP start");
		QuoteResponse quoteResponse=null;
		try{
			if(ruleModel!=null){
				quoteResponse=new QuoteResponse();
				InstProductRuleBO instProductRuleBO=QuoteHelper.invokeInstProductBusinessRule(ruleModel.getFundCode(), ruleModel.getMemberType(),ruleModel.getManageType(),eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath());
				if(ruleModel.getManageType()!=null && ruleModel.getIpCoverType()!=null){
						quoteResponse.setCoverType(ruleModel.getIpCoverType());
							QuoteHelper.calculateINGDIPFixedCost(ruleModel,instProductRuleBO,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath(),quoteResponse);
				}
			}
		}catch(Exception e){
			log.error("Error in calculate IP : {}",e);
		}
		log.info("Calculate INGD IP finish");
		return quoteResponse;
	}
	
	//Added by 561132 for Death LifeStage Cover Amount Value
	@Override
	public QuoteResponse calculateLifeStageDeathCoverAmount(String fundCode,String age) {
		log.info("Calculate INGD Death Life Style Cover Amount");
		QuoteResponse quoteResponse=null;
		try{
			if(fundCode!=null && age!=null ){
				quoteResponse=new QuoteResponse();
							QuoteHelper.calculateDeathLifeStageCoverAmount(fundCode,age,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath(),quoteResponse);
				}
			}
		catch(Exception e){
		log.error("Error in Death Life Style Cover Amount : {}",e);
		}
		log.info("Calculate INGD Death Life Style Cover Amount finish");
		return quoteResponse;
	}
	
	
	//Added by 561132 for Death LifeStage Cover Amount Value
		@Override
		public QuoteResponse calculateLifeStageTPDCoverAmount(String fundCode,String age) {
			log.info("Calculate INGD TPD Life Style Cover Amount");
			QuoteResponse quoteResponse=null;
			try{
				if(fundCode!=null && age!=null ){
					quoteResponse=new QuoteResponse();
								QuoteHelper.calculateTPDLifeStageCoverAmount(fundCode,age,eappRulesCacheProvider.getRuleMap(),eappRulesCacheProvider.getFilePath(),quoteResponse);
					}
				}
			catch(Exception e){
			log.error("Error in TPD Life Style Cover Amount : {}",e);
			}
			log.info("Calculate INGD TPD Life Style Cover Amount finish");
			return quoteResponse;
		}
		
		//Added by 561132 to get the Aura Type
		@Override
		public String getAuraType(RuleModel ruleModel) {
			String auraType=null;
			boolean longAuraFormat=false;
			boolean shortAuraFormat=false;
			if(ruleModel.getDeathFixedAmount()==null) {
				ruleModel.setDeathFixedAmount(BigDecimal.ZERO);
			}
			if(ruleModel.getTpdFixedAmount()==null) {
				ruleModel.setTpdFixedAmount(BigDecimal.ZERO);
			}
			if(ruleModel.getIpFixedAmount()==null) {
				ruleModel.setIpFixedAmount(BigDecimal.ZERO);
			}
			longAuraFormat=QuoteHelper.getLongAuraStatus(ruleModel);
			shortAuraFormat=QuoteHelper.getShortAuraStatus(ruleModel);
			if(longAuraFormat && shortAuraFormat) {
				auraType="long";
			}
		  if(!longAuraFormat && shortAuraFormat) {
				auraType="short";
		 }
		  
		  if(longAuraFormat && !shortAuraFormat) {
				auraType="long";
		 }
			return auraType;
		}
}
