import angular from 'angular';

// Create the module where our functionality can attach to
let coverModule = angular.module('app.cover', []);

// Include our UI-Router config settings
import coverConfig from './cover.config';
coverModule.config(coverConfig);


// Controllers
import CoverCtrl from './cover.controller';
coverModule.controller('CoverCtrl', CoverCtrl);


export default coverModule;
