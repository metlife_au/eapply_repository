package au.com.metlife.eapply.calculator.model;

import java.io.Serializable;

public class KidsVO implements Serializable {
private String childName=null;
	
	private boolean deleteChild=false;
	
	private boolean addChild=false;
	
	private String  childListContentStyle = null;
	
	private Integer age;



	public boolean isAddChild() {
		return addChild;
	}

	public void setAddChild(boolean addChild) {
		this.addChild = addChild;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}

	public boolean isDeleteChild() {
		return deleteChild;
	}

	public void setDeleteChild(boolean deleteChild) {
		this.deleteChild = deleteChild;
	}
	
	
	public String getChildListContentStyle() {
		return childListContentStyle;
	}

	public void setChildListContentStyle(String childListContentStyle) {
		this.childListContentStyle = childListContentStyle;
	}


	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
