package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonReferences")

@XmlRootElement
public class PersonReferences {
	
	@XmlAttribute(name="personReference")
	 String personReference;

	public String getPersonReference() {
		return personReference;
	}

	public void setPersonReference(String personReference) {
		this.personReference = personReference;
	}

	
	
	
}
