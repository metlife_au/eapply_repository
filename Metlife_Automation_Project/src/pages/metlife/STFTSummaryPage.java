package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTSummaryPageObjects;

public class STFTSummaryPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTSummaryPage yourSummary() {
		yourSummaryDetails();
		return new STFTSummaryPage(scriptHelper);
	}

	public STFTSummaryPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourSummaryDetails() {

		String ackexclusion = dataTable.getData("MetLife_Data", "AckExclusion");
		
		try {
			
			if (ackexclusion.equalsIgnoreCase("exclusion")) {
				tryAction(fluentWaitElement(STFTSummaryPageObjects.chkexclusion), "Click", "Exclusion" );
			} 
			
			if (ackexclusion.equalsIgnoreCase("loading")) {
				tryAction(fluentWaitElement(STFTSummaryPageObjects.chkloading), "Click", "Loading" );				
			}
			
			Thread.sleep(1000);
			
			//Clicking Next on Summary page
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(driver.findElement(STFTSummaryPageObjects.btnsummaryNext)).click().perform();
			//tryAction(fluentWaitElement(STFTHealthPageObjects.btnsummaryNext), "Click", "Apply");
			Thread.sleep(1000);
			
			switchToActiveWindow();
			driver.manage().window().maximize();
				
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
