package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPDeclinedPageObjects extends PageObjectLibrary{
		
	public static final By btnfinish = getObjectIdentifaction("YBRIP.declined.finish.btn.css");
}
