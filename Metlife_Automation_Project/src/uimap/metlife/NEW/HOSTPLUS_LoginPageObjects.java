package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class HOSTPLUS_LoginPageObjects extends PageObjectLibrary{
		
	public static final By MemberNo = getObjectIdentifaction("HOSTPLUS.Login.MemberNo.id");
	public static final By FirstName = getObjectIdentifaction("HOSTPLUS.Login.FirstName.id");
	public static final By SurName = getObjectIdentifaction("HOSTPLUS.Login.SurName.id");
	public static final By DOB = getObjectIdentifaction("HOSTPLUS.Login.DOB.id");
	public static final By Login = getObjectIdentifaction("HOSTPLUS.Login.Loginbutton.id");
}
