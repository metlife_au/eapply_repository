package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesCCStubPageObjects extends PageObjectLibrary {
	public static final By txtareainput = getObjectIdentifaction("coles.stub.input.textarea.css");
	public static final By btnApply = getObjectIdentifaction("coles.stub.input.btn.css");
}
