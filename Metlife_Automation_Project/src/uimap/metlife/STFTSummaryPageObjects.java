package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTSummaryPageObjects extends PageObjectLibrary{
	public static final By btnsummaryNext = getObjectIdentifaction("stft.summary.next.button.css");
	public static final By chkexclusion = getObjectIdentifaction("stft.summary.exclusion.acknowledge.checkbox.id");	
	public static final By chkloading = getObjectIdentifaction("stft.summary.loading.acknowledge.checkbox.id");	
}
