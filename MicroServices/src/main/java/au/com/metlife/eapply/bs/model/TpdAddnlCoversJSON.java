package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class TpdAddnlCoversJSON implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5560888620526949563L;

	private String tpdCoverName;
	
	private String tpdCoverPremium;
	
	private String tpdCoverType;
	
	private String tpdFixedAmt;
	
	private String tpdInputTextValue;
	
	private String tpdTransferCoverType;
	
	private BigDecimal tpdTransferAmt;
	
	private String tpdTransferCovrAmt;
	
	private String tpdTransferWeeklyCost;
	
	private String newTpdCoverType;
	
	private String newTpdLabelAmt;
	
	private String newTpdUpdatedCover;
	
	private String newTpdCoverPremium;
	
	private String tpdLoadingCost;
	
	//Added for transfer units
	private String tpdTransferUnits;
	
	private String totalTPDUnits;

	public String getTpdLoadingCost() {
		return tpdLoadingCost;
	}

	public void setTpdLoadingCost(String tpdLoadingCost) {
		this.tpdLoadingCost = tpdLoadingCost;
	}

	public String getNewTpdCoverType() {
		return newTpdCoverType;
	}

	public void setNewTpdCoverType(String newTpdCoverType) {
		this.newTpdCoverType = newTpdCoverType;
	}

	public String getNewTpdLabelAmt() {
		return newTpdLabelAmt;
	}

	public void setNewTpdLabelAmt(String newTpdLabelAmt) {
		this.newTpdLabelAmt = newTpdLabelAmt;
	}

	public String getNewTpdUpdatedCover() {
		return newTpdUpdatedCover;
	}

	public void setNewTpdUpdatedCover(String newTpdUpdatedCover) {
		this.newTpdUpdatedCover = newTpdUpdatedCover;
	}

	public String getNewTpdCoverPremium() {
		return newTpdCoverPremium;
	}

	public void setNewTpdCoverPremium(String newTpdCoverPremium) {
		this.newTpdCoverPremium = newTpdCoverPremium;
	}

	public String getTpdTransferCoverType() {
		return tpdTransferCoverType;
	}

	public void setTpdTransferCoverType(String tpdTransferCoverType) {
		this.tpdTransferCoverType = tpdTransferCoverType;
	}

	public BigDecimal getTpdTransferAmt() {
		return tpdTransferAmt;
	}

	public void setTpdTransferAmt(BigDecimal tpdTransferAmt) {
		this.tpdTransferAmt = tpdTransferAmt;
	}

	public String getTpdTransferCovrAmt() {
		return tpdTransferCovrAmt;
	}

	public void setTpdTransferCovrAmt(String tpdTransferCovrAmt) {
		this.tpdTransferCovrAmt = tpdTransferCovrAmt;
	}

	public String getTpdTransferWeeklyCost() {
		return tpdTransferWeeklyCost;
	}

	public void setTpdTransferWeeklyCost(String tpdTransferWeeklyCost) {
		this.tpdTransferWeeklyCost = tpdTransferWeeklyCost;
	}

	public String getTpdCoverName() {
		return tpdCoverName;
	}

	public void setTpdCoverName(String tpdCoverName) {
		this.tpdCoverName = tpdCoverName;
	}

	public String getTpdCoverPremium() {
		return tpdCoverPremium;
	}

	public void setTpdCoverPremium(String tpdCoverPremium) {
		this.tpdCoverPremium = tpdCoverPremium;
	}

	public String getTpdCoverType() {
		return tpdCoverType;
	}

	public void setTpdCoverType(String tpdCoverType) {
		this.tpdCoverType = tpdCoverType;
	}

	public String getTpdFixedAmt() {
		return tpdFixedAmt;
	}

	public void setTpdFixedAmt(String tpdFixedAmt) {
		this.tpdFixedAmt = tpdFixedAmt;
	}

	public String getTpdInputTextValue() {
		return tpdInputTextValue;
	}

	public void setTpdInputTextValue(String tpdInputTextValue) {
		this.tpdInputTextValue = tpdInputTextValue;
	}

	public String getTpdTransferUnits() {
		return tpdTransferUnits;
	}

	public void setTpdTransferUnits(String tpdTransferUnits) {
		this.tpdTransferUnits = tpdTransferUnits;
	}

	public String getTotalTPDUnits() {
		return totalTPDUnits;
	}

	public void setTotalTPDUnits(String totalTPDUnits) {
		this.totalTPDUnits = totalTPDUnits;
	}
}
