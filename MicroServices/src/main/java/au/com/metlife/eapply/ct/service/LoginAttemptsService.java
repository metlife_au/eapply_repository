package au.com.metlife.eapply.ct.service;



import au.com.metlife.eapply.ct.dto.CTHistoryDto;

public interface LoginAttemptsService {
	
	public au.com.metlife.eapply.ct.dto.LoginAttempts retrieveLoginAttempts(au.com.metlife.eapply.ct.dto.LoginAttempts loginAttemps);	
	public void saveOrUpdateLoginAttempts(au.com.metlife.eapply.ct.dto.LoginAttempts loginAttempts);	
	public void saveCTHistory(CTHistoryDto history);

}
