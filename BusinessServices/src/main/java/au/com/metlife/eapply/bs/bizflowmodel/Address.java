package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address")

@XmlRootElement
public class Address {

	private String AddressKey;
	private String FullAddressDescription;
	private String addressLine1;
	private String addressLine2;
	private String SuburbName;
	private String StateOrProvinceName;
	private String PostalCode;
	private String CountryName;
	public String getAddressKey() {
		return AddressKey;
	}
	public void setAddressKey(String addressKey) {
		AddressKey = addressKey;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCountryName() {
		return CountryName;
	}
	public void setCountryName(String countryName) {
		CountryName = countryName;
	}
	public String getFullAddressDescription() {
		return FullAddressDescription;
	}
	public void setFullAddressDescription(String fullAddressDescription) {
		FullAddressDescription = fullAddressDescription;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getStateOrProvinceName() {
		return StateOrProvinceName;
	}
	public void setStateOrProvinceName(String stateOrProvinceName) {
		StateOrProvinceName = stateOrProvinceName;
	}
	public String getSuburbName() {
		return SuburbName;
	}
	public void setSuburbName(String suburbName) {
		SuburbName = suburbName;
	}
	
	


	
	
}
