package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesPaymentPageObjects extends PageObjectLibrary {
	public static final By rdCreditCard = getObjectIdentifaction("coles.payment.creditcard.radio.id");
	public static final By rdBankAccount = getObjectIdentifaction("coles.payment.bankaccount.radio.id");
	public static final By rdCCVisa = getObjectIdentifaction("coles.payment.cc.visa.radio.id");
	public static final By rdCCMaster = getObjectIdentifaction("coles.payment.cc.mastercard.radio.id");
	public static final By txtCardNumber = getObjectIdentifaction("coles.payment.cardnumber.text.id");
	public static final By txtCardName = getObjectIdentifaction("coles.payment.nameoncard.text.id");
	public static final By selCCExpMonth = getObjectIdentifaction("coles.payment.cc.expmonth.dropdown.css");
	public static final By linkCCExpMonth01 = getObjectIdentifaction("coles.payment.cc.expmonth01.linktext");
	public static final By linkCCExpMonth02 = getObjectIdentifaction("coles.payment.cc.expmonth02.linktext");
	public static final By linkCCExpMonth03 = getObjectIdentifaction("coles.payment.cc.expmonth03.linktext");
	public static final By linkCCExpMonth04 = getObjectIdentifaction("coles.payment.cc.expmonth04.linktext");
	public static final By linkCCExpMonth05 = getObjectIdentifaction("coles.payment.cc.expmonth05.linktext");
	public static final By linkCCExpMonth06 = getObjectIdentifaction("coles.payment.cc.expmonth06.linktext");
	public static final By linkCCExpMonth07 = getObjectIdentifaction("coles.payment.cc.expmonth07.linktext");
	public static final By linkCCExpMonth08 = getObjectIdentifaction("coles.payment.cc.expmonth08.linktext");
	public static final By selCcExpYr = getObjectIdentifaction("coles.payment.cc.expyr.dropdown.css");
	public static final By linkCcExpYr16 = getObjectIdentifaction("coles.payment.cc.expyr2016.linktext");
	public static final By linkCcExpYr17 = getObjectIdentifaction("coles.payment.cc.expyr2017.linktext");
	public static final By linkCcExpYr18 = getObjectIdentifaction("coles.payment.cc.expyr2018.linktext");
	public static final By linkCcExpYr19 = getObjectIdentifaction("coles.payment.cc.expyr2019.linktext");
	public static final By chkAuthorise = getObjectIdentifaction("coles.payment.authorise.checkbox.id");
	public static final By btnSubmit = getObjectIdentifaction("coles.payment.submit.button.id");
}
