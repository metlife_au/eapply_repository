package au.com.metlife.eapply.underwriting.serviceImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.drools.KnowledgeBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.xml.sax.SAXException;

import com.rgatp.aura.wsapi.Answer;
import com.rgatp.aura.wsapi.AuraServiceException;
import com.rgatp.aura.wsapi.MessageResource;
import com.rgatp.aura.wsapi.Question;
import com.rgatp.aura.wsapi.Questionnaire;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;
import au.com.metlife.eapply.underwriting.model.AuraAnswer;
import au.com.metlife.eapply.underwriting.model.AuraDecision;
import au.com.metlife.eapply.underwriting.model.AuraQuestion;
import au.com.metlife.eapply.underwriting.model.AuraQuestionnaire;
import au.com.metlife.eapply.underwriting.model.AuraResponse;
import au.com.metlife.eapply.underwriting.model.AuraSection;
import au.com.metlife.eapply.underwriting.model.AuraSession;
import au.com.metlife.eapply.underwriting.model.PostProcessRuleBO;
import au.com.metlife.eapply.underwriting.model.Quote;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.ItemData;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.underwriting.service.UnderwritingService;
import au.com.metlife.eapply.underwriting.utils.AuraCall;
import au.com.metlife.eapply.underwriting.utils.AuraDroolsHelper;
import au.com.metlife.eapply.underwriting.utils.AuraProcessDecisionHelper;
import au.com.metlife.eapply.underwriting.utils.AuraRulesCacheProvider;
import au.com.metlife.eapply.underwriting.utils.XMLHelper;

@Service("uwService")

@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UnderwritingServiceImpl implements UnderwritingService {
	private static final Logger log = LoggerFactory.getLogger(UnderwritingServiceImpl.class);

	@Autowired(required = true)
	AuraRulesCacheProvider auraRulesCacheProvider;
	@Override
	public Quote getQuote(Quote input) {
		input.setContent("Hello Buddy......");
		return input;
	}
	
	
	

	@Autowired 
	AuraSession auSession;
	
	AuraCall ac= new AuraCall();
  /*  Questionnaire questionnaire=null;*/
	int indent = 0;
	int changedBaseQuestion = 0;
	  String auraID="test11-"+new java.util.Date().getTime();   
	    
	    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
	    		+ "<UniqueID>"+auraID+"</UniqueID>"
	    		+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl><QuestionFilters><QuestionFilter>Term-Group</QuestionFilter><QuestionFilter>TPD-Group</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"PVTTest\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"575000.0\" AgeDistrCoverageAmount=\"575000.0\" CoverageAmount=\"575000.0\" FinancialCoverageAmount=\"575000.0\">Term-Group</Product><Product AgeCoverageAmount=\"600000.0\" AgeDistrCoverageAmount=\"600000.0\" CoverageAmount=\"600000.0\" FinancialCoverageAmount=\"600000.0\">TPD-Group</Product></Products><EngineVariables><Variable Name=\"Gender\">Female</Variable><Variable Name=\"Age\">59</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">Yes</Variable><Variable Name=\"Country\">Yes</Variable><Variable Name=\"Salary\">145000</Variable><Variable Name=\"SumInsuredTerm\">575000.00</Variable><Variable Name=\"SumInsuredTPD\">600000.00</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">NSFS</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable><Variable Name=\"SchemeName\">NSFS</Variable><Variable Name=\"FormLength\">TransferCover</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
	    
	    
	    /*String auraID="test11-"+new java.util.Date().getTime();
	    String xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
				+ "<UniqueID>"+auraID+"</UniqueID>"
				+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage>"
				+ "<ExtractLanguage>en</ExtractLanguage> </AuraControl><QuestionFilters><QuestionFilter>TERM-DIRECT</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables>"
				+ " <Variable Name=\"LOB\">Direct</Variable> <Variable Name=\"callCentrePostBackTo\">http://10.173.60.52:9080/CAW2/jsp/public/toall/CallApp/result.jsp</Variable>"
				+ "  <Variable Name=\"callCentrePostErrorsTo\">http://10.173.60.52:9080/CAW2/jsp/public/toall/CallApp/result.jsp</Variable>"
				+ "   <Variable Name=\"callCentreSaveExitPage\">http://10.173.60.52:9080/CAW2/jsp/public/toall/CallApp/result.jsp</Variable>"
				+ "   <Variable Name=\"callCentreIdentifier\">UCMS</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"hjfhjghjg\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\">"
				+ "<Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">TERM-DIRECT</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable>"
				+ "<Variable Name=\"Smoker\">No</Variable><Variable Name=\"Age\">40</Variable><Variable Name=\"Occupation\"/>"
				+ "<Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/>"
						+ "<Variable Name=\"Hours15\"/><Variable Name=\"Country\">Australia</Variable><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable>"
								+ "<Variable Name=\"SumInsuredTPD\">0</Variable><Variable Name=\"SumInsuredTrauma\">0</Variable><Variable Name=\"SumInsuredIP\">0</Variable><Variable Name=\"Partner\">Suncorp</Variable>"
										+ "<Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Suncorp Online</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";*/

	  
	 public AuraDecision submitAuraSession(String clientname, String auraSessionId,String fund){
		 log.info("Submit aura session start");
		 Questionnaire qu = null;
		 String auraResponse = null;		
		 ResponseObject responseObject = null;
		 AuraDecision auraDecision = null;	
		 Insured insured = null;
		 PostProcessRuleBO postProcessRuleBO = null;
		 /*Boolean clientMatch = null;*/
		 Boolean clientMatch = Boolean.FALSE;
		 List<String> ratingReason = null;
		 List<String> plannedReason = null;
		 String productOrgDecision = null;
		 Rating rating = null;
		 StringBuilder productdecisionBuffer = new StringBuilder();
		 String transactionType = null;
		 String lob = MetlifeInstitutionalConstants.GROUP;
		 String overallDecision = null;
		
		 
		 try {
			 qu = new Questionnaire();
			 qu.setClientName(clientname);
			 qu.setQuestionnaireId(auraSessionId);		 
			 auraResponse= ac.submit(qu);			
			if (null != auraResponse) {
				responseObject = XMLHelper.prepareResponse(auraResponse);
				if(responseObject!=null){
					auraDecision = new AuraDecision();
					auraDecision.setResponseObject(responseObject);
					String[] reasons;
					/*pp logic goes here*/
					
					for (int itr = 0; itr < responseObject.getClientData().getListExtEngineVar().size(); itr++) {
						ItemData itemData = responseObject.getClientData().getListExtEngineVar().get(itr);
						if("FormLength".equalsIgnoreCase(itemData.getCode())){
							transactionType = itemData.getValue();
						}
					}
					
					for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
						insured =  responseObject.getClientData().getListInsured().get(itr);				
						
						if (null != insured && null != insured.getListOverallDec()) {
							for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
								Decision decision =  insured.getListOverallDec().get(insItr);
								
								if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
									
									postProcessRuleBO = new PostProcessRuleBO();
									ratingReason = new ArrayList<>();
									plannedReason = new ArrayList<>();
									productOrgDecision= decision.getDecision();
									postProcessRuleBO.setRuleFileName("PostProcessingMaster.xls");
									/*Basic Data set Here like LOB, Partner, Product Name*/
									postProcessRuleBO.setBrand(fund);
									postProcessRuleBO.setLob(MetlifeInstitutionalConstants.GROUP);
									postProcessRuleBO.setProduct(decision.getProductName());
									/*Step1 fire the rule mater here .*/
									postProcessRuleBO.setRuleFileName("PostProcessingMaster.xls");									
									
									if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_MASTER)!=null){
										postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_MASTER));
									}
									postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
									if(null!=transactionType && ("WorkRating".equalsIgnoreCase(transactionType)|| "TransferCover".equalsIgnoreCase(transactionType) || "SpecialOffer".equalsIgnoreCase(transactionType))){
										log.info("Loop 2");
										clientMatch= Boolean.FALSE;
									}else{
										clientMatch= postProcessRuleBO.getClientMatch();
									}
									/*Setting the data for the 2nd Rule*/
									postProcessRuleBO.setAuraPostProcessID(postProcessRuleBO.getAuraPostProcessID());
									postProcessRuleBO.setOriginalDecision(decision.getDecision());
									postProcessRuleBO.setTotalExlusion(decision.getListExclusion().size());
									postProcessRuleBO.setTotalLExlusion(decision.getLExclusions());
									postProcessRuleBO.setTotalMExlusion(decision.getMExclusions());
									
									/*Setting the loadings*/
									int loadingValue = 0;
									if(null != decision.getTotalDebitsValue()){
										loadingValue = new Double(decision.getTotalDebitsValue()).intValue();
										decision.setOriginalTotalDebitsValue(Integer.toString(loadingValue));
									}
									postProcessRuleBO.setTotalLoading(loadingValue);
									/*Setp2 fire rule 2 here*/
									if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_1)!=null){
										postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_1));
									}							
									postProcessRuleBO.setRuleFileName("PostProcessing1.xls");
									postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
									
									if(null != postProcessRuleBO.getPostAURADecision()){
										decision.setDecision(postProcessRuleBO.getPostAURADecision());	
										reasons = new String[1];
										reasons[0] = postProcessRuleBO.getReason();
										decision.setReasons(reasons);	
										plannedReason.add(reasons[0]);
									}								
									
									postProcessRuleBO.setPostAURADecision(null);
									if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_2)!=null){
										postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_2));
									}							
									postProcessRuleBO.setRuleFileName("PostProcessing2.xls");
									postProcessRuleBO.setOriginalDecision(decision.getDecision());			
									postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
									
									if(null != postProcessRuleBO.getPostAURADecision()){
										decision.setDecision(postProcessRuleBO.getPostAURADecision());	
										decision.setTotalDebitsReason(postProcessRuleBO.getReason());
										decision.setTotalDebitsValue(postProcessRuleBO.getPostAURALoading().toString());
										reasons = new String[1];
										reasons[0] = postProcessRuleBO.getReason();
										decision.setReasons(reasons);
										if(Double.parseDouble(decision.getTotalDebitsValue())>0.0){
											plannedReason.add(postProcessRuleBO.getReason());
										}										
									}
									productdecisionBuffer.append(decision.getDecision());
									/**
									 * BMI post processing only for institutional now - 05012012 
									 */
									if((MetlifeInstitutionalConstants.GROUP.equalsIgnoreCase(lob) && null != decision.getDecision()) || responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
										postProcessRuleBO.setPostAURADecision(null);
										if(auraRulesCacheProvider.getRuleMap()!=null && auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_3)!=null){
											postProcessRuleBO.setKbase((KnowledgeBase) auraRulesCacheProvider.getRuleMap().get(ApplicationConstants.POST_PROCESS_3));
										}								
										postProcessRuleBO.setRuleFileName("PostProcessing3.xls");
										postProcessRuleBO.setOriginalDecision(decision.getDecision());
										postProcessRuleBO.setBrand(fund);
										/** 
										 * Loop through the genderAgeBMI tag to get the BMI loadng  
										 */
									List<Decision> bmiList = null;
									/*if(responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
										bmiList = insured.getListGenderAgeBMIRangeTable();
									}else if ("Institutional".equalsIgnoreCase(lob)){
										bmiList = insured.getListGenderAgeBMIDec();
									}*/
									bmiList = insured.getListGenderAgeBMIDec();
									int bmiLoadingValue = 0;
									int bmlMedicalLoading  = 0;
										for (int bmiins = 0; bmiins < bmiList.size(); bmiins++) {
											Decision bmidecision = bmiList.get(bmiins);
											
											if (null != bmidecision && null != bmidecision.getProductName() && null != bmidecision.getTotalDebitsValue() && !"".equalsIgnoreCase(bmidecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(bmidecision.getProductName())) {
												if(null != bmidecision.getTotalDebitsValue()){
													bmiLoadingValue = new Double(bmidecision.getTotalDebitsValue()).intValue();
												}else{
													bmiLoadingValue = 0;
												}
												postProcessRuleBO.setBmiLoading(bmiLoadingValue);
												
												if(new BigDecimal(decision.getTotalDebitsValue()).intValue() >= bmiLoadingValue){
													bmlMedicalLoading = new BigDecimal(decision.getTotalDebitsValue()).subtract(new BigDecimal(bmidecision.getTotalDebitsValue())).intValue();	
												}
												
											   /*For BMI it should be Medical Loadings only.*/
												postProcessRuleBO.setTotalExlusion(decision.getMExclusions());
												
												
											}
										} /*Endo of BMI LOOP*/
										int origLoadingValue = 0;
										for (int origins = 0; origins < insured.getListOriginalRuleDec().size(); origins++) {
											Decision origDecision =  insured.getListOriginalRuleDec().get(origins);
											
											if (null != origDecision && null != origDecision.getProductName() && !"".equalsIgnoreCase(origDecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(origDecision.getProductName())) {
												
												if(null != origDecision.getTotalDebitsValue()){
													origLoadingValue = new Double(origDecision.getTotalDebitsValue()).intValue();
												}else{
													origLoadingValue = 0;
												}
											}
										}
										if(bmlMedicalLoading >= 0){
											postProcessRuleBO.setTotalLoading(bmlMedicalLoading);
											
										}else if(origLoadingValue > 0){
											postProcessRuleBO.setTotalLoading(origLoadingValue);
										}
										postProcessRuleBO = AuraDroolsHelper.invokepostProcessRule(postProcessRuleBO, auraRulesCacheProvider.getFilePath());
										/*capturing original decision from Aura*/
										if("ACC".equalsIgnoreCase(productOrgDecision) ){
											
											/*if(null!=decision.getListRating() && decision.getListRating().size()>0 ){*/
											if(null!=decision.getListRating() && !(decision.getListRating().isEmpty()) ){
												for (int iter=0; iter<decision.getListRating().size(); iter++) {
													 rating =  decision.getListRating().get(iter);
													if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
															&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
														ratingReason.add(rating.getReason());
													}									
												}
											}
											
											/*if(null!=decision.getListExclusion() && decision.getListExclusion().size()>0){*/
											if(null!=decision.getListExclusion() && !(decision.getListExclusion().isEmpty())){
												for (Iterator iter = decision.getListExclusion()
														.iterator(); iter.hasNext();) {
													Exclusion exclusion = (Exclusion) iter.next();
													if(null!=exclusion && null!=exclusion.getReason()&& exclusion.getReason().length>0){
														for (int exItr = 0; exItr < exclusion.getReason().length; exItr++) {
															if(null!=exclusion.getReason()[exItr] && exclusion.getReason()[exItr].length()>0 && !"null".equalsIgnoreCase(exclusion.getReason()[exItr])){
																ratingReason.add(exclusion.getReason()[exItr]);
															}
															
														}
													}
													
												}
												
											}
											decision.setOrgReason(ratingReason);
										}else if("DCL".equalsIgnoreCase(productOrgDecision) && null!=decision.getReasons() && decision.getReasons().length>0){
											for (int exItr = 0; exItr < decision.getReasons().length; exItr++) {
												ratingReason.add(decision.getReasons()[exItr]);
											}
											decision.setOrgReason(ratingReason);
										}else if("RUW".equalsIgnoreCase(productOrgDecision)){
											
											for (int iter=0; iter<decision.getListRating().size(); iter++) {
												 rating =  decision.getListRating().get(iter);
												if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
														&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
													ratingReason.add(rating.getReason());
													
												}									
											}
											decision.setOrgReason(ratingReason);	
										}
										
										if(null != postProcessRuleBO.getPostAURADecision()){
											decision.setDecision(postProcessRuleBO.getPostAURADecision());	
											reasons = new String[1];
											reasons[0] = postProcessRuleBO.getReason();
											decision.setReasons(reasons);											
											plannedReason.add(postProcessRuleBO.getReason());
										}
										
										/*if(null!=plannedReason && plannedReason.size()>0){*/
										if(null!=plannedReason && !(plannedReason.isEmpty())){
											decision.setPlannedReason(plannedReason);
										}										
										/*  END of GROUP BMI Loop	*/
									}									
								
									/*end of updation*/
									if(decision.getProductName().contains("Term")){
										auraDecision.setDeathDecision(decision.getDecision());	
										if(decision.getTotalDebitsValue()!=null && decision.getTotalDebitsValue().trim().length()>0 && !"0.0".equalsIgnoreCase(decision.getTotalDebitsValue().trim())){
											auraDecision.setDeathLoading(decision.getTotalDebitsValue());	
										}										
										auraDecision.setDeathOrigTotalDebitsValue(decision.getOriginalTotalDebitsValue());										
										auraDecision.setDeathExclusions(AuraProcessDecisionHelper.getExclusions(decision));
										if(decision.getDecision().equalsIgnoreCase("DCL")){										
                                    		auraDecision.setDeathAuraResons(AuraProcessDecisionHelper.getAuraDclReason(decision));                                    		
                                    		auraDecision.setDeathResons(AuraProcessDecisionHelper.getReasons(decision));                            		
                                    		
										}
										
									}else if(decision.getProductName().contains("TPD")){
										auraDecision.setTpdDecision(decision.getDecision());
										if(decision.getTotalDebitsValue()!=null && decision.getTotalDebitsValue().trim().length()>0 && !"0.0".equalsIgnoreCase(decision.getTotalDebitsValue().trim())){
											auraDecision.setTpdLoading(decision.getTotalDebitsValue());	
										}										
										auraDecision.setTpdOrigTotalDebitsValue(decision.getOriginalTotalDebitsValue());
										auraDecision.setTpdExclusions(AuraProcessDecisionHelper.getExclusions(decision));
										if(decision.getDecision().equalsIgnoreCase("DCL")){	
											auraDecision.setTpdAuraResons(AuraProcessDecisionHelper.getAuraDclReason(decision));     
											auraDecision.setTpdResons(AuraProcessDecisionHelper.getReasons(decision)); 
										}
										
									}else if(decision.getProductName().contains("IP")){
										auraDecision.setIpDecision(decision.getDecision());
										if(decision.getTotalDebitsValue()!=null && decision.getTotalDebitsValue().trim().length()>0 && !"0.0".equalsIgnoreCase(decision.getTotalDebitsValue().trim())){
											auraDecision.setIpLoading(decision.getTotalDebitsValue());
										}										
										auraDecision.setIpOrigTotalDebitsValue(decision.getOriginalTotalDebitsValue());										
										auraDecision.setIpExclusions(AuraProcessDecisionHelper.getExclusions(decision));
										if(decision.getDecision().equalsIgnoreCase("DCL")){	
											auraDecision.setIpAuraResons(AuraProcessDecisionHelper.getAuraDclReason(decision));     
											auraDecision.setIpResons(AuraProcessDecisionHelper.getReasons(decision));
										}
										 
									}
								}
							}
						}
					}
					
					StringBuilder strBuff = new StringBuilder();
					if(auraDecision.getDeathDecision()!=null){
						strBuff.append(auraDecision.getDeathDecision());
					}
					if(auraDecision.getTpdDecision()!=null){
						strBuff.append(auraDecision.getTpdDecision());
					}
					if(auraDecision.getIpDecision()!=null){
						strBuff.append(auraDecision.getIpDecision());
					}
					
					if(null!=strBuff && !"".equalsIgnoreCase(strBuff.toString())){   
						  if(strBuff.toString().contains("RUW")){
							  auraDecision.setOverallDecision("RUW");
			              }else if(!strBuff.toString().contains("ACC") && !strBuff.toString().contains("RUW") && strBuff.toString().contains("DCL")){
			            	  auraDecision.setOverallDecision("DCL");
			              }else if(strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL") && !strBuff.toString().contains("RUW")){
			            	  auraDecision.setOverallDecision("ACC");
			              }else{
			            	  auraDecision.setOverallDecision("ACC");
			              }
					  }
					
					/*if(auraDecision.getDeathDecision()!=null && auraDecision.getTpdDecision()!=null && auraDecision.getTpdDecision()!=null){
						if(auraDecision.getDeathDecision().equalsIgnoreCase("DCL") && 
								auraDecision.getTpdDecision().equalsIgnoreCase("DCL") &&
								auraDecision.getIpDecision().equalsIgnoreCase("DCL")){
							auraDecision.setOverallDecision("DCL");
						}else if(auraDecision.getDeathDecision().equalsIgnoreCase("RUW") || 
								auraDecision.getTpdDecision().equalsIgnoreCase("RUW")||
								auraDecision.getIpDecision().equalsIgnoreCase("RUW")){
							auraDecision.setOverallDecision("RUW");
						}else{
							auraDecision.setOverallDecision("ACC");
						}
					}*/
					/*for special term*/
					if("ACC".equalsIgnoreCase(auraDecision.getOverallDecision()) &&
							(auraDecision.getDeathLoading()!=null || auraDecision.getDeathExclusions()!=null
							|| auraDecision.getTpdLoading()!=null || auraDecision.getTpdExclusions()!=null
							|| auraDecision.getIpLoading()!=null || auraDecision.getIpExclusions()!=null)
							){
						auraDecision.setSpecialTerm(Boolean.TRUE);
					}
					
					/**
					 * Below code is used for the Client Match 
					 */
					if(clientMatch){
						for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
							insured =  responseObject.getClientData().getListInsured().get(itr);					
							if (null != insured && null != insured.getListOverallDec()) {
								for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
									Decision decision =  insured.getListOverallDec().get(insItr);
									if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
										productdecisionBuffer.append(decision.getDecision());
									}
								}
							}
						}
						overallDecision = getoverallDecision(productdecisionBuffer);						
						auraDecision.setOverallDecision(overallDecision);
						/*call eapp service for client match*/
					}
					
					/**Specific loadings added for vicsuper by purna  -starts**/
					if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(fund)){
						boolean deathFlag = false;
						boolean tpdFlag = false;
						if (auraDecision.getDeathDecision()!=null && null != auraDecision.getDeathOrigTotalDebitsValue() && updateDeathAuraDecision(auraDecision)){
							deathFlag = true;
							if (null != insured && null != insured.getListOverallDec()) {
								for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
									Decision decision =  insured.getListOverallDec().get(insItr);
									if (decision.getProductName().contains("Term")){
										String reasonTmp = "Decision is  Accepted,Death Loadings is greater than 50 and TPD Loadings is less than Death Loadings"; 
										String[] reasonsArray = new String[1];
										reasonsArray[0] = reasonTmp;
										decision.setReasons(reasonsArray);
										decision.setDecision("RUW");
									}
								}
							}
						}
						if (auraDecision.getTpdDecision()!=null && MetlifeInstitutionalConstants.ACC.equalsIgnoreCase(auraDecision.getTpdDecision()) && updateTpdAuraDecision(auraDecision)){
							tpdFlag = true;
							if (null != insured && null != insured.getListOverallDec()) {
								for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
									Decision decision =  insured.getListOverallDec().get(insItr);
									if (decision.getProductName().contains("TPD")){
										String reasonTmp = "Decision is  Accepted,TPD Loadings is greater than 50 and Death Loadings is less than TPD Loadings"; 
										String[] reasonsArray = new String[1];
										reasonsArray[0] = reasonTmp;
										decision.setReasons(reasonsArray);
										decision.setDecision("RUW");
									}
								}
							}
						}
						if (deathFlag || tpdFlag) {
							StringBuilder strBuff1 = new StringBuilder();
							if(auraDecision.getDeathDecision()!=null){
								strBuff1.append(auraDecision.getDeathDecision());
							}
							if(auraDecision.getTpdDecision()!=null){
								strBuff1.append(auraDecision.getTpdDecision());
							}
							if(auraDecision.getIpDecision()!=null){
								strBuff1.append(auraDecision.getIpDecision());
							}
							
							if(null!=strBuff1 && !"".equalsIgnoreCase(strBuff1.toString())){ 
								auraDecision.setOverallDecision("ACC");
								  if(strBuff1.toString().contains("RUW")){
									  auraDecision.setOverallDecision("RUW");
					              }else if(!strBuff1.toString().contains("ACC") && !strBuff1.toString().contains("RUW") && strBuff1.toString().contains("DCL")){
					            	  auraDecision.setOverallDecision("DCL");
					              }else if(strBuff1.toString().contains("ACC") && strBuff1.toString().contains("DCL") && !strBuff1.toString().contains("RUW")){
					            	  auraDecision.setOverallDecision("ACC");
					              }
							  }
						}
						if ("RUW".equalsIgnoreCase(auraDecision.getOverallDecision())){
							auraDecision.setSpecialTerm(Boolean.FALSE);
						}
					}
					/**Specific loadings added for vicsuper by purna  -ends**/
				}
			}
		} catch (RemoteException e) {
			log.error("Error in remote exception: {}",e);
		} catch (AuraServiceException e) {
			log.error("Error in aura service: {}",e);
		} catch (SAXException e) {
			log.error("Error in SAX: {}",e);
		} catch (IOException e) {
			log.error("Error in Input and output: {}",e);
		} catch (ParserConfigurationException e) {
			log.error("Error in parser configuration: {}",e);
		}
		 log.info("Submit aura session finish");
		 return auraDecision;
	 }
	 
	 
	 

	public AuraQuestionnaire getAllAuraQuestions(String inputXml) {
	log.info(MetlifeInstitutionalConstants.GET_ALL_AURA_QUESTIONS_START);
		auraID="test3-"+new java.util.Date().getTime();
		log.info("Start-- INITAURA() {}",new  java.util.Date());		
		log.info("inputXml>> {}",inputXml);
		auSession.setQuestionnaire(ac.establishNewSessionToAURA(inputXml));		
	    log.info(MetlifeInstitutionalConstants.GET_ALL_AURA_QUESTIONS_START,new  java.util.Date());
	    return getAllAuraQuestions(auSession.getQuestionnaire());		
		
	}

	public AuraQuestion updateAuraQuestion(AuraResponse res) {
		try{
			log.info("Update aura question start");
		log.info("res.getQuestionID>> {}",res.getQuestionID());	
		Question question= auSession.getQuestionnaire().locateQuestion(res.getQuestionID());
		List<String> answerList = new ArrayList<>();
		String[] res1= new String[res.getAuraAnswers().size()];
		for(int i=0;i<res.getAuraAnswers().size();i++){
			if(res.getAuraAnswers().get(i)!=null){
				res1[i]=res.getAuraAnswers().get(i).trim();
				answerList.add(res1[i]);
				String logMsg =  res1[i];
				log.info("ANS----------- {}",logMsg);
			}
			
		}	
		if(res1.length==0){
			res1 =new String[1];
			res1[0] = "";
			String logMsg = res1[0] ;
			log.info("ANS-----------  {}",logMsg);
			question.respond(res1[0]);
			
		}else if(res1.length==1){
			String logMsg = res1[0];
			log.info("ANS-----------  {}",logMsg);
			question.respond(res1[0]);
			
		}		
		else
			question.respond(res1);
		/*not sure why this is required	*/
		/*Response[] responses = auSession.getQuestionnaire().obtainResponses();*/	       	 
		/*auSession.setQuestionnaire(ac.process(auSession.getQuestionnaire())); */ 
		Questionnaire questionnaire = ac.process(auSession.getQuestionnaire());
		auSession.setQuestionnaire(questionnaire);
		/*back and forth issue*/

		/*getAllAuraQuestions(auSession.getQuestionnaire())*/
		question= auSession.getQuestionnaire().locateQuestion(res.getQuestionID());
		AuraQuestion q=getQuestion(question);
		
		/*temperory.Need to find solution
		q.setAuraQuestionnare(getAllAuraQuestions(auSession.getQuestionnaire()));	*/
		if(!q.isBaseQuestion()){
			getparentQuestionObjet(getAllAuraQuestions(auSession.getQuestionnaire()).getQuestions(), res.getQuestionID());
			log.info("changedBaseQuestion>> {}",changedBaseQuestion);
			question= auSession.getQuestionnaire().locateQuestion(changedBaseQuestion);
			q.setChangedQuestion(getQuestion(question));
			/*q=getQuestion(question);*/
		}else{
			q.setChangedQuestion(getQuestion(question));
		}
		/*question=(Question)auSession.getQuestionnaire().locateQuestion();
		getparentQuestionObjet(q.getAuraQuestionnare().getQuestions(), res.getQuestionID());*/	
		log.info("Update aura question finish");
		return q;
		/*List<AuraQuestion> questions = getAllAuraQuestions(questionnaire);	*/
		 
		}catch(Exception e ){
			log.error("Error in update aura question {}",e);
			return null;
		}
		
	}
	
	private AuraQuestion getparentQuestionObjet(List<AuraQuestion> auraQuestionList, int subQuestionId){
		log.info("Get parent question object start");
		AuraQuestion auraQuestion = null;
		/*int baseQuestionId=0;		*/
		/*AuraQuestion baseQuestion = null;*/
		AuraAnswer auraAnswer = null;
		/*if(auraQuestionList!=null && auraQuestionList.size()>0){*/
		if(auraQuestionList!=null && !(auraQuestionList.isEmpty())){
			for (Iterator iterator = auraQuestionList.iterator(); iterator.hasNext();) {
				auraQuestion = (AuraQuestion) iterator.next();
				/*if(auraQuestion!=null && auraQuestion.getAuraAnswer().size()>0){*/
				if(auraQuestion!=null && !(auraQuestion.getAuraAnswer().isEmpty())){
					if(auraQuestion.isBaseQuestion()){	
						changedBaseQuestion=auraQuestion.getQuestionId();
						/*baseQuestionId=auraQuestion.getQuestionId();*/
						log.info("base>> {}",auraQuestion.getQuestionId());				
						
					}
					if(auraQuestion.getQuestionId()==subQuestionId){
						/*baseQuestion = auraQuestion;*/
						/*if(changedBaseQuestion!=0){
							baseQuestionId =changedBaseQuestion;
						}*/	
						 /*log.info("baseQuestionId>> {}",changedBaseQuestion);
						 break;*/	
						return auraQuestion;
											  
					 }else{
						for (Iterator iterator2 = auraQuestion.getAuraAnswer().iterator(); iterator2.hasNext();) {
							auraAnswer = (AuraAnswer) iterator2.next();
							/*if(auraAnswer!=null && auraAnswer.getChildQuestions()!=null && auraAnswer.getChildQuestions().size()>0){*/
							if(auraAnswer!=null && auraAnswer.getChildQuestions()!=null && !(auraAnswer.getChildQuestions().isEmpty())){
								auraQuestion = getparentQuestionObjet(auraAnswer.getChildQuestions(), subQuestionId);								
								if(auraQuestion.getQuestionId()==subQuestionId){
									log.info("sub>> {}",auraQuestion.getQuestionId());
									log.info("baseQuestionIdAub>> {}",changedBaseQuestion);
									/*changedBaseQuestion= baseQuestionId;*/
									/* baseQuestion = auraQuestion;*/
									/*break;*/
									return auraQuestion;
								}
								
							}
							
						}
					}
					
				}
				
			}
		}
		log.info("Get parent question object finsh");
		return auraQuestion;
	}
	
	
	   
		private AuraQuestionnaire getAllAuraQuestions(Questionnaire questionnaire) {			
            log.info(MetlifeInstitutionalConstants.GET_ALL_AURA_QUESTIONS_START);
			Question[] allQuestions=questionnaire.getQuestions();
			List<AuraQuestion> c_Questions= new ArrayList<>(allQuestions.length);
			Map<String,String> sectionNameMap = new HashMap<>();
			List<AuraQuestion> auraSectionQuestionList = null;
			AuraSection auraSection = null;
			List<String> sectionList = new ArrayList<>();
			List<AuraSection> auraSectionList = new ArrayList<>();
			for(int i=0;i<allQuestions.length;i++){
				AuraQuestion q=getQuestion(allQuestions[i]);
				if(q!=null && q.getQuestionAlias()!=null && !sectionNameMap.containsKey(q.getQuestionAlias())){
					log.info("q.getQuestionAlias()>> {}",q.getQuestionAlias());					
					sectionNameMap.put(q.getQuestionAlias(),q.getQuestionAlias());
					sectionList.add(q.getQuestionAlias());
				}
				/*q.setSessionID(questionnaire.getQuestionnaireId());*/
				c_Questions.add(q);			
			}
			
			AuraQuestionnaire aq=new AuraQuestionnaire();
			aq.setSessionID(questionnaire.getQuestionnaireId());
			aq.setQuestions(c_Questions);
			/*setting section map*/
			/*for (Map.Entry<String,String> entry : sectionNameMap.entrySet())
			{
				if(entry.getKey()!=null){
					sectionList.add(entry.getKey());
					log.info(entry.getKey() + "/" + entry.getValue());
				}
				log.info(entry.getKey() + "/" + entry.getValue());
			}*/
			for (Iterator iterator = sectionList.iterator(); iterator.hasNext();) {
				String sectionName = (String) iterator.next();
				if(sectionName!=null){
					auraSectionQuestionList = new ArrayList<>();
					for (Iterator iterator2 = c_Questions.iterator(); iterator2.hasNext();) {
						AuraQuestion auraQuestion = (AuraQuestion) iterator2.next();
						if(auraQuestion!=null && auraQuestion.isBaseQuestion() &&  sectionName.equalsIgnoreCase(auraQuestion.getQuestionAlias()) ){							
							auraSectionQuestionList.add(auraQuestion);
						}
					}
					/*if(auraSectionQuestionList.size()>0){*/
					if(!(auraSectionQuestionList.isEmpty())){
						auraSection = new AuraSection();
						auraSection.setQuestions(auraSectionQuestionList);
						auraSection.setSectionName(sectionName);
						auraSectionList.add(auraSection);
					}
				}
			}
			/*if(auraSectionList.size()>0){*/
			if(!(auraSectionList.isEmpty())){
				aq.setSections(auraSectionList);
			}
			log.info("Get all aura questions finish");
			return aq;
			
		}
	

		

		private AuraQuestion getQuestion( Question question){
				
			log.info("Get question start");		
			
			AuraQuestion auraQuestion=new AuraQuestion();
			auraQuestion.setClassType(question.getClass().getName().substring(question.getClass().getPackage().getName().length() + 1));
			auraQuestion.setQuestionAlias(question.getQuestionAlias());
			auraQuestion.setQuestionId(question.getQuestionId());
			auraQuestion.setQuestionText(question.getQuestionText());
			auraQuestion.setHelpText(question.getHelpText());
			auraQuestion.setQuestionComplete(question.isComplete());
			auraQuestion.setBaseQuestion(question.isBaseQuestion());
			auraQuestion.setBranchComplete(question.isBranchComplete());
			auraQuestion.setAnswerTest(question.getAnswerText());
			auraQuestion.setVisible(question.isVisible());	
			 if (question.getValidationMessage() != null) {
                 MessageResource messageResource = null;
                 messageResource = question.getValidationMessage();
                 auraQuestion.setValidationMsg(messageResource.getText());
                 auraQuestion.setError(Boolean.TRUE);
			 }else{
				 auraQuestion.setError(Boolean.FALSE);
			 }
			 log.info("question.getQuestionText()>> {}",question.getQuestionText());
			 log.info("question.getNumber()>> {}",question.hashCode());
			if(question.getNumber()!=null){
				if(question.isBaseQuestion()){
					indent = 0;
					auraQuestion.setIndent(indent);					
				}/*else{
					indent= indent+1;
					auraQuestion.setIndent(1);
				}*/
				else if(question.getNumber().equalsIgnoreCase("a)") && !"CheckBoxQuestion".equalsIgnoreCase(auraQuestion.getClassType())){
					auraQuestion.setIndent(1);
				}else if(question.getNumber().equalsIgnoreCase("a)") && "CheckBoxQuestion".equalsIgnoreCase(auraQuestion.getClassType())){
					auraQuestion.setIndent(1);
				}else{
					auraQuestion.setIndent(2);
				}
			}
			/*log.info("number>> {}",question.getNumber());*/
			Answer[] ans=question.getAnswers();		
			auraQuestion.setArrAns(new ArrayList(ans.length)); /*This is temp for json*/
			
		  /* log.info(auraQuestion.getClassType()+"-----------"+ans.length);*/
			List<AuraAnswer> c_answers =new ArrayList(ans.length);
						
			/*AuraAnswer aurAns[]=new AuraAnswer[ans.length];*/
			
			for(int i=0;i<ans.length;i++){
				
				AuraAnswer c_ans=new AuraAnswer();			
				c_ans.setAnswerID(question.getQuestionId()+"_"+"A"+i);
				c_ans.setAnswerValue(ans[i].getAnswerValue());
				c_ans.setAnswerText(ans[i].getAnswerText());
				/*log.info("DDDDDDDDDDDD-1");*/
				Question[] childQuestions=ans[i].getQuestions();			
				List<AuraQuestion> c_child_Questions= new ArrayList<>(childQuestions.length);
				/*log.info("RRRRRRRRRRRRRR-2");
				AuraQuestion[] childAuraQuestions =new AuraQuestion[childQuestions.length];*/
				for(int ch=0;ch<childQuestions.length;ch++){
					log.info("CH---------- {} and CH ques -- {}" ,question.getQuestionId(),childQuestions[ch].getQuestionText());
					
					
					
					/*childAuraQuestions[ch]=getQuestion(childQuestions[ch]);*/
					c_child_Questions.add(getQuestion(childQuestions[ch]));				
				}
				/*log.info("RRRRRRRRRRRRRR-3");*/
				c_ans.setChildQuestions(c_child_Questions);
				/*log.info("RRRRRRRRRRRRRR-4");*/
				c_answers.add(c_ans);
				
			}
			
			auraQuestion.setAuraAnswer(c_answers);
			log.info("Get question finish");		
			return auraQuestion;
		}
		
		private static String getoverallDecision(StringBuilder strBuff){
			log.info("Get overall Decision start");		
			String decision =null;
			  if(null!=strBuff && !"".equalsIgnoreCase(strBuff.toString())){   
				  if(strBuff.toString().contains("RUW")){
					  decision = "RUW";				
	              }else if(!strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL")){
	            	  decision = "DCL";            	 
	              }else if(strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL")){
	            	  decision="ACC";            	 
	              }else{
	            	  decision="ACC";            	  
	              }
			  }
			  log.info("Get overall Decision finish");
			  return decision;
		}
		
		/**
		 @author :Purnachandra k
		 @method :updateDeathAuraDecision
		 @param  : AuraDecision
		 @purpose:Added for vicsuper to modify the decision based on loadings.
		 **/
		private static Boolean updateDeathAuraDecision(AuraDecision auraDecision){
			Double deathLoading = new Double(auraDecision.getDeathOrigTotalDebitsValue());
			if (MetlifeInstitutionalConstants.ACC.equalsIgnoreCase(auraDecision.getDeathDecision()) 
					&& MetlifeInstitutionalConstants.DEATH_LOADING < deathLoading
					&& deathLoading > new Double(auraDecision.getTpdOrigTotalDebitsValue())
					//&& null != auraDecision.getTpdExclusions()
					//&& !"".equals(auraDecision.getTpdExclusions())
					){
				auraDecision.setDeathDecision(MetlifeInstitutionalConstants.RUW);
				auraDecision.setDeathAuraResons("");                                    		
        		auraDecision.setDeathResons("Decision is  Accepted,Death Loadings is greater than 50 and TPD Loadings is less than Death Loadings");
        		return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		/**
		 @author :Purnachandra k
		 @method :updateTpdAuraDecision
		 @param  : AuraDecision
		 @purpose:Added for vicsuper to modify the decision based on loadings.
		 **/
		private static Boolean updateTpdAuraDecision(AuraDecision auraDecision){
			
			if (null != auraDecision.getTpdOrigTotalDebitsValue()){
				Double tpdLoading = new Double(auraDecision.getTpdOrigTotalDebitsValue());
				if (null != auraDecision.getDeathOrigTotalDebitsValue() 
						//&& new Double(auraDecision.getDeathLoading()) < new Double(auraDecision.getTpdLoading())
						&& MetlifeInstitutionalConstants.DEATH_LOADING < tpdLoading
						&& tpdLoading > new Double(auraDecision.getDeathOrigTotalDebitsValue())
						){
					auraDecision.setTpdDecision(MetlifeInstitutionalConstants.RUW);
					auraDecision.setTpdAuraResons("");                                    		
	        		auraDecision.setTpdResons("Decision is  Accepted,TPD Loadings is greater than 50 and Death Loadings is less than TPD Loadings");
	        		return Boolean.TRUE;
				}
			}
			return Boolean.FALSE;
		}

}
