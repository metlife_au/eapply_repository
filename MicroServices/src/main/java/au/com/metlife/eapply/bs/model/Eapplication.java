package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the EAPPLICATION database table.
 * 
 */

@Entity
@Table(name = "TBL_A_EAPPLICATION", schema="EAPPDB")
public class Eapplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(precision=8, scale=2)
	private BigDecimal alispriapplicantprem;

	@Column(precision=8, scale=2)
	private BigDecimal alissecapplicantprem;

	@Column(precision=8, scale=2)
	private BigDecimal alistotmonprem;

	@Column(length=10)
	private String appdecision;

	@Column(length=1)
	private String applicantupdate;

	@Column(length=50)
	private String applicationrefnumber;

	@Column(length=10)
	private String applicationstatus;

	@Column(length=20)
	private String applicationtype;

	@Column(nullable=false, length=20)
	private String applicationumber;

	@Column(length=10)
	private String apporgdecision;

	@Column(length=5)
	private String authorizationindicator;

	@Column(length=40)
	private String campaigncode;

	@Column(length=25)
	private String clientid;

	@Column(length=1)
	private Boolean clientmatch;

	@Column(length=4000)
	private String clientmatchreason;

	private Timestamp createdate;

	@Column(length=5)
	private String disclosureindicator;

	@Column(length=5)
	private String emailcopyindicator;

	@Column(length=50)
	private String familydiscount;

	private Timestamp firstsavedate;

	@Column(length=5)
	private String genconsentindicator;
	
	@Column(length=5)
	private String underwritingpolicy;

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_A_EAPPLICATION_SEQ")
	@SequenceGenerator(name="TBL_A_EAPPLICATION_SEQ",sequenceName = "EAPPDB.TBL_A_EAPPLICATION_SEQ", initialValue=1, allocationSize=100)
	private long id;

	@Column(length=20)
	private String lastsavedon;

	private Timestamp lastupdatedate;

	@Column(length=50)
	private String magazineoffer;

	@Column(name="MARKET_OPT_OUT", length=1)
	private String marketOptOut;

	@Column(length=10)
	private String multicoverdiscount;

	@Column(length=1)
	private String nuwflowind;

	@Column(length=1)
	private String occupgrade;

	@Column(length=5)
	private String paindicator;

	@Column(length=7)
	private String partnercode;

	@Column(length=25)
	private String policynumber;

	private Timestamp policystartdate;

	@Column(length=20)
	private String policytype;

	@Column(length=5)
	private String privacystatindicator;

	@Column(length=50)
	private String productname;

	@Column(length=6)
	private String productnumber;

	@Column(length=60)
	private String producttype;

	@Column(length=20)
	private String promocode;

	@Column(length=20)
	private String requesttype;

	@Column(length=5)
	private String saindicator;
	
	@Column(length=5)
	private String fulstatindicator;

	@Column(name="TOTAL_MONTHLY_PREMIUM", precision=8, scale=2)
	private BigDecimal totalMonthlyPremium;

	@Column(length=1)
	private String tpddeclineind;
	
	@Lob
	@Column(name="EAPPSAVEDATA")
	private String eAppSaveDataclob;
	
	
	public String geteAppSaveDataclob() {
		return eAppSaveDataclob;
	}

	public void seteAppSaveDataclob(String eAppSaveDataclob) {
		this.eAppSaveDataclob = eAppSaveDataclob;
	}
	
	
	
	public List<Applicant> getApplicant() {
		return applicant;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public void setApplicant(List<Applicant> applicant) {
		this.applicant = applicant;
	}

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "eapplicationid")
	private List<Applicant> applicant;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "eapplicationid")
	private List<Document> documents;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "eapplicationid")
	private List<Fundinfo> fundInfo;
	
	@OneToOne (cascade=CascadeType.ALL)
	@JoinColumn(name="AURADETAILS_ID", unique= true, nullable=true, insertable=true, updatable=true)
	private Auradetail auradetail;

	public Auradetail getAuradetail() {
		return auradetail;
	}

	public void setAuradetail(Auradetail auradetail) {
		this.auradetail = auradetail;
	}

	public List<Fundinfo> getFundInfo() {
		return fundInfo;
	}

	public void setFundInfo(List<Fundinfo> fundInfo) {
		this.fundInfo = fundInfo;
	}

	public Eapplication() {
		//Do nothing as written for future use
	}

	public BigDecimal getAlispriapplicantprem() {
		return this.alispriapplicantprem;
	}

	public void setAlispriapplicantprem(BigDecimal alispriapplicantprem) {
		this.alispriapplicantprem = alispriapplicantprem;
	}

	public BigDecimal getAlissecapplicantprem() {
		return this.alissecapplicantprem;
	}

	public void setAlissecapplicantprem(BigDecimal alissecapplicantprem) {
		this.alissecapplicantprem = alissecapplicantprem;
	}

	public BigDecimal getAlistotmonprem() {
		return this.alistotmonprem;
	}

	public void setAlistotmonprem(BigDecimal alistotmonprem) {
		this.alistotmonprem = alistotmonprem;
	}

	public String getAppdecision() {
		return this.appdecision;
	}

	public void setAppdecision(String appdecision) {
		this.appdecision = appdecision;
	}

	public String getApplicantupdate() {
		return this.applicantupdate;
	}

	public void setApplicantupdate(String applicantupdate) {
		this.applicantupdate = applicantupdate;
	}

	public String getApplicationrefnumber() {
		return this.applicationrefnumber;
	}

	public void setApplicationrefnumber(String applicationrefnumber) {
		this.applicationrefnumber = applicationrefnumber;
	}

	public String getApplicationstatus() {
		return this.applicationstatus;
	}

	public void setApplicationstatus(String applicationstatus) {
		this.applicationstatus = applicationstatus;
	}

	public String getApplicationtype() {
		return this.applicationtype;
	}

	public void setApplicationtype(String applicationtype) {
		this.applicationtype = applicationtype;
	}

	public String getApplicationumber() {
		return this.applicationumber;
	}

	public void setApplicationumber(String applicationumber) {
		this.applicationumber = applicationumber;
	}

	public String getApporgdecision() {
		return this.apporgdecision;
	}

	public void setApporgdecision(String apporgdecision) {
		this.apporgdecision = apporgdecision;
	}

	public String getAuthorizationindicator() {
		return this.authorizationindicator;
	}

	public void setAuthorizationindicator(String authorizationindicator) {
		this.authorizationindicator = authorizationindicator;
	}

	public String getCampaigncode() {
		return this.campaigncode;
	}

	public void setCampaigncode(String campaigncode) {
		this.campaigncode = campaigncode;
	}

	public String getClientid() {
		return this.clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public Boolean getClientmatch() {
		return this.clientmatch;
	}

	public void setClientmatch(Boolean clientmatch) {
		this.clientmatch = clientmatch;
	}

	public String getClientmatchreason() {
		return this.clientmatchreason;
	}

	public void setClientmatchreason(String clientmatchreason) {
		this.clientmatchreason = clientmatchreason;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getDisclosureindicator() {
		return this.disclosureindicator;
	}

	public void setDisclosureindicator(String disclosureindicator) {
		this.disclosureindicator = disclosureindicator;
	}

	public String getEmailcopyindicator() {
		return this.emailcopyindicator;
	}

	public void setEmailcopyindicator(String emailcopyindicator) {
		this.emailcopyindicator = emailcopyindicator;
	}

	public String getFamilydiscount() {
		return this.familydiscount;
	}

	public void setFamilydiscount(String familydiscount) {
		this.familydiscount = familydiscount;
	}

	public Timestamp getFirstsavedate() {
		return this.firstsavedate;
	}

	public void setFirstsavedate(Timestamp firstsavedate) {
		this.firstsavedate = firstsavedate;
	}

	public String getGenconsentindicator() {
		return this.genconsentindicator;
	}

	public void setGenconsentindicator(String genconsentindicator) {
		this.genconsentindicator = genconsentindicator;
	}

	public String getUnderwritingpolicy() {
		return underwritingpolicy;
	}

	public void setUnderwritingpolicy(String underwritingpolicy) {
		this.underwritingpolicy = underwritingpolicy;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastsavedon() {
		return this.lastsavedon;
	}

	public void setLastsavedon(String lastsavedon) {
		this.lastsavedon = lastsavedon;
	}

	public Timestamp getLastupdatedate() {
		return this.lastupdatedate;
	}

	public void setLastupdatedate(Timestamp lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	public String getMagazineoffer() {
		return this.magazineoffer;
	}

	public void setMagazineoffer(String magazineoffer) {
		this.magazineoffer = magazineoffer;
	}

	public String getMarketOptOut() {
		return this.marketOptOut;
	}

	public void setMarketOptOut(String marketOptOut) {
		this.marketOptOut = marketOptOut;
	}

	public String getMulticoverdiscount() {
		return this.multicoverdiscount;
	}

	public void setMulticoverdiscount(String multicoverdiscount) {
		this.multicoverdiscount = multicoverdiscount;
	}

	public String getNuwflowind() {
		return this.nuwflowind;
	}

	public void setNuwflowind(String nuwflowind) {
		this.nuwflowind = nuwflowind;
	}

	public String getOccupgrade() {
		return this.occupgrade;
	}

	public void setOccupgrade(String occupgrade) {
		this.occupgrade = occupgrade;
	}

	public String getPaindicator() {
		return this.paindicator;
	}

	public void setPaindicator(String paindicator) {
		this.paindicator = paindicator;
	}

	public String getPartnercode() {
		return this.partnercode;
	}

	public void setPartnercode(String partnercode) {
		this.partnercode = partnercode;
	}

	public String getPolicynumber() {
		return this.policynumber;
	}

	public void setPolicynumber(String policynumber) {
		this.policynumber = policynumber;
	}

	public Timestamp getPolicystartdate() {
		return this.policystartdate;
	}

	public void setPolicystartdate(Timestamp policystartdate) {
		this.policystartdate = policystartdate;
	}

	public String getPolicytype() {
		return this.policytype;
	}

	public void setPolicytype(String policytype) {
		this.policytype = policytype;
	}

	public String getPrivacystatindicator() {
		return this.privacystatindicator;
	}

	public void setPrivacystatindicator(String privacystatindicator) {
		this.privacystatindicator = privacystatindicator;
	}

	public String getProductname() {
		return this.productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getProductnumber() {
		return this.productnumber;
	}

	public void setProductnumber(String productnumber) {
		this.productnumber = productnumber;
	}

	public String getProducttype() {
		return this.producttype;
	}

	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}

	public String getPromocode() {
		return this.promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public String getRequesttype() {
		return this.requesttype;
	}

	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}

	public String getSaindicator() {
		return this.saindicator;
	}

	public void setSaindicator(String saindicator) {
		this.saindicator = saindicator;
	}

	public BigDecimal getTotalMonthlyPremium() {
		return this.totalMonthlyPremium;
	}

	public void setTotalMonthlyPremium(BigDecimal totalMonthlyPremium) {
		this.totalMonthlyPremium = totalMonthlyPremium;
	}

	public String getTpddeclineind() {
		return this.tpddeclineind;
	}

	public void setTpddeclineind(String tpddeclineind) {
		this.tpddeclineind = tpddeclineind;
	}

	public String getFulstatindicator() {
		return fulstatindicator;
	}

	public void setFulstatindicator(String fulstatindicator) {
		this.fulstatindicator = fulstatindicator;
	}

}