angular.module('MtaaAPI',[]);
angular.module('MtaaAPI').factory('urlService',['$http','$q', '$sessionstorage', 'fetchUrlSvc', function($http,$q, $sessionstorage, fetchUrlSvc){
    var headerConfig = {
        headers : {
            'Content-Type': 'application/json'
        }
    };
    var urlObject = {};
    urlObject.getUrls = function(){
        var defer = $q.defer();
        $http.get('CareSuperUrls.properties',headerConfig).then(function(res){
            urlObject = res.data;
            defer.resolve(res);
        }, function(err){
            console.log('Error fetching urls' + JSON.stringify(err));
            defer.reject(err);
        });
        return defer.promise;
    };
    urlObject.setUrlList = function(list){
        $sessionstorage.setItem('urlList',JSON.stringify(list));
    };
    urlObject.getUrlList = function(){
        return fetchUrlSvc.getUrlList();
    };
    return urlObject;
}]);

angular.module('MtaaAPI').service('auraTransferInitiateService', function() {
	var _self = this;
	_self.setInput = function (tempInput) {
		_self.input = tempInput;
	};
	_self.getInput = function () {
		return _self.input;
	};
});

angular.module('MtaaAPI').factory('printQuotePage', function($http, $q,auraResponseService,tokenNumService){
	
	var myFactObj = {};
    myFactObj.reqObj = function (url) {
    	var defer = $q.defer();
    	$http.post(url,auraResponseService.getResponse(),
    			{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
    				defer.resolve(response);
    			}, function(response) {
    				defer.reject(response);
    			});
    	return defer.promise;
    };

    return myFactObj;
});

angular.module('MtaaAPI').factory('getAuraTransferData', function($http, $q,auraTransferInitiateService,auraInputService,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
               var defer = $q.defer();

                               $http.post(url,auraInputService,{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                               defer.resolve(response);
                              }, function(response) {
                                              defer.reject(response);
                              });
                              return defer.promise;
    };

    return myFactObj;
});

angular.module('MtaaAPI').factory('auraPostfactory', function($http, $q,auraResponseService,tokenNumService){

    var myFactObj = {};

    myFactObj.reqObj = function (url) {
                var defer = $q.defer();
                               $http.post(url,auraResponseService.getResponse(),
                                                              {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                               defer.resolve(response);

                               }, function(response) {
                                              defer.reject(response);
                              });
                              return defer.promise;
    };

    return myFactObj;
});

angular.module('MtaaAPI').factory('submitAura', function($http, $q, auraInputService,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
               var defer = $q.defer();

                               $http.post(url,auraInputService,
                                                              {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                               defer.resolve(response);
                              }, function(response) {
                                              defer.reject(response);
                              });
                              return defer.promise;
    };

    return myFactObj;
});

angular.module('MtaaAPI').factory('submitEapply', function($http, $q,auraResponseService,tokenNumService){

    var myFactObj = {};

    myFactObj.reqObj = function (url) {
                var defer = $q.defer();
                               $http.post(url,auraResponseService.getResponse(),
                                                              {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                               defer.resolve(response);

                               }, function(response) {
                                              defer.reject(response);
                              });
                              return defer.promise;
    };

    return myFactObj;
});

angular.module('MtaaAPI').factory('saveEapply', function($http, $q,auraResponseService,tokenNumService){
	
	var myFactObj = {};

    myFactObj.reqObj = function (url) {
    	var defer = $q.defer();
    	$http.post(url,auraResponseService.getResponse(),{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {
    		defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    	});
    	return defer.promise;
    };

    return myFactObj;
});

angular.module('MtaaAPI').service('tokenNumService', function(fetchTokenNumSvc) {
    var _self = this;
    _self.setTokenId = function (tempToken) {
                    _self.tokenId = tempToken;
};
    _self.getTokenId = function () {
      return fetchTokenNumSvc.getTokenId();
};
});
angular.module('MtaaAPI').factory('SpecialCvrRenderService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function(url,fundCode,age,deathUnits,tpdUnits,dateJoined,clientrefnum,firstname,lastname,dob){
                var defer = $q.defer();
                $http.get(url, {
            	   headers:{
            		   			'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'anb': parseInt(age),
                                'deathUnits':parseInt(deathUnits),
                                'tpdUnits':parseInt(tpdUnits),
                                'dateJoined':parseInt(dateJoined),
                                'clientrefnum':clientrefnum,
                                'firstName':firstname,
                                'lastName':lastname,
                                'dob':dob
                }}).then(function(res){
                				myFactObj = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while getting default units ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return myFactObj;
}]);
angular.module('MtaaAPI').factory('appNumberService', function($http, $q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
                var defer = $q.defer();
                                $http.get(url,{
                                                headers: {'Authorization':  tokenNumService.getTokenId()},
                                                params:{}}).then(function(response) {
                                                                defer.resolve(response);
                                }, function(response) {
                                                defer.reject(response);
                                });
                               return defer.promise;
    };

    return myFactObj;
});
angular.module('MtaaAPI').factory('LifeEvntRenderService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function(url,fundCode,age,deathUnits,tpdUnits){
                var defer = $q.defer();
                $http.get(url, {
            	   headers:{
            		   			'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'anb': parseInt(age),
                                'deathUnits':parseInt(deathUnits),
                                'tpdUnits':parseInt(tpdUnits)
                }}).then(function(res){
                				myFactObj = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while getting default units ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return myFactObj;
}]);
angular.module('MtaaAPI').factory('MaxLimitService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getMaxLimits = function(url,fundCode,memberType,manageType){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'memberType': memberType,
                                'manageType':manageType
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while getting limits ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').factory('OccupationService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getOccupationList = function(url,fundId,induCode){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundId': fundId,
                                'induCode': induCode
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while getting occupations ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').factory('TransferCalculateService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };

    var factory = {};
    factory.calculate = function(data,url){
                var defer = $q.defer();
                $http.post(url, data, headerConfig).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while calculating..' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').factory('NewOccupationService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getOccupation = function(url,fundId,occName){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundId': fundId,
                                'occName': occName
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while getting occupations ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').service('deathCoverService', function($sessionstorage, fetchDeathCoverSvc) {
    var _self = this;
    _self.setDeathCover = function (tempToken) {
                    //_self.deathCover = tempToken;
                    $sessionstorage.setItem('deathCoverObject',JSON.stringify(tempToken));
};
    _self.getDeathCover = function () {
                    //return _self.deathCover;
                    return fetchDeathCoverSvc.getDeathCover();
};
});
angular.module('MtaaAPI').service('auraInputService', function() {

    var _self = this;
   _self.setFund = function (tempInput) {
                   _self.fund = tempInput;
};
   _self.getFund = function () {
     return _self.fund;
};
   _self.setMode = function (tempInput) {
                   _self.mode = tempInput;
};
   _self.getMode = function () {
     return _self.mode;
};
   _self.setName = function (tempInput) {
                   _self.name = tempInput;
};
   _self.getName = function () {
     return _self.name;
};
   _self.setAge = function (tempInput) {
                   _self.age = tempInput;
};
   _self.getAge = function () {
     return _self.age;
};
    _self.setDeathAmt = function (tempInput) {
                   _self.deathAmt = tempInput;
};
   _self.getDeathAmt = function () {
     return _self.deathAmt;
};

    _self.setTpdAmt = function (tempInput) {
                   _self.tpdAmt = tempInput;
};
   _self.getTpdAmt = function () {
     return _self.tpdAmt;
};

    _self.setIpAmt = function (tempInput) {
                   _self.ipAmt = tempInput;
};
   _self.getIpAmt = function () {
     return _self.ipAmt;
};

    _self.setWaitingPeriod = function (tempInput) {
                   _self.waitingPeriod = tempInput;
};
   _self.getWaitingPeriod = function () {
     return _self.waitingPeriod;
};

    _self.setBenefitPeriod = function (tempInput) {
                   _self.benefitPeriod = tempInput;
};
   _self.getBenefitPeriod = function () {
     return _self.benefitPeriod;
};
   _self.setAppnumber = function (tempInput) {
                   _self.appnumber = tempInput;
};
   _self.getAppnumber = function () {
     return _self.appnumber;
};

    _self.setIndustryOcc = function (tempInput) {
                   _self.industryOcc = tempInput;
};
   _self.getIndustryOcc = function () {
     return _self.industryOcc;
};

    _self.setGender = function (tempInput) {
                   _self.gender = tempInput;
};
   _self.getGender = function () {
     return _self.gender;
};

    _self.setCountry = function (tempInput) {
                   _self.country = tempInput;
};
   _self.getCountry = function () {
     return _self.country;
};

    _self.setSalary = function (tempInput) {
                   _self.salary = tempInput;
};
   _self.getSalary = function () {
     return _self.salary;
};

    _self.setFifteenHr = function (tempInput) {
                   _self.fifteenHr = tempInput;
};
   _self.getFifteenHr = function () {
     return _self.fifteenHr;
};

    _self.setCompanyId = function (tempInput) {
                   _self.companyId = tempInput;
};
   _self.getCompanyId = function () {
     return _self.companyId;
};

    _self.setClientname = function (tempInput) {
                   _self.clientname = tempInput;
};
   _self.getClientname = function () {
     return _self.clientname;
};

    _self.setDob = function (tempInput) {
                   _self.dob = tempInput;
};
   _self.getDob = function () {
     return _self.dob;
};

    _self.setFirstName = function (tempInput) {
                   _self.firstName = tempInput;
};
   _self.getFirstName = function () {
     return _self.firstName;
};
   _self.setLastName = function (tempInput) {
                   _self.lastName = tempInput;
};
   _self.getLastName = function () {
     return _self.lastName;
};
   _self.setSpecialTerm = function (tempInput) {
                   _self.specialTerm = tempInput;
};
   _self.getSpecialTerm = function () {
     return _self.specialTerm;
};
   _self.setExistingTerm = function (tempInput) {
                   _self.existingTerm = tempInput;
};
   _self.getExistingTerm = function () {
     return _self.existingTerm;
};

    _self.setMemberType = function (tempInput) {
                   _self.memberType = tempInput;
};
   _self.getMemberType = function () {
     return _self.memberType;
};

});
angular.module('MtaaAPI').service('tpdCoverService', function($sessionstorage, fetchTpdCoverSvc) {
    var _self = this;
    _self.setTpdCover = function (tempToken) {
                    //_self.tpdCover = tempToken;
                    $sessionstorage.setItem('tpdCoverObject',JSON.stringify(tempToken));
};
    _self.getTpdCover = function () {
                    //return _self.tpdCover;
                    return fetchTpdCoverSvc.getTpdCover();
};
});

angular.module('MtaaAPI').service('ipCoverService', function($sessionstorage, fetchIpCoverSvc) {
    var _self = this;
    _self.setIpCover = function (tempToken) {
                    //_self.ipCover = tempToken;
                    $sessionstorage.setItem('ipCoverObject',JSON.stringify(tempToken));
};
    _self.getIpCover = function () {
                    //return _self.ipCover;
                    return fetchIpCoverSvc.getIpCover();
};
});
angular.module('MtaaAPI').factory('QuoteService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getList = function(url,fundCode){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while getting industries ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').factory('CalculateService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };

    var factory = {};
    factory.calculate = function(data,url){
                var defer = $q.defer();
                $http.post(url, data, headerConfig).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while calculating..' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').factory('propertyService',['$http','$q', '$sessionstorage', function($http,$q, $sessionstorage){
    var headerConfig = {
    headers : {
        'Content-Type': 'application/json'
    }
};

var occObject = {};
   occObject.getOccList = function(){
    var defer = $q.defer();
    $http.get('HostDecOccList.properties',headerConfig).then(function(res){
    				occObject = res.data;
                    defer.resolve(res);
    }, function(err){
                    console.log('Error fetching urls ' + JSON.stringify(err));
                    defer.reject(err);
    });
    return defer.promise;
};
return occObject;
}]);

angular.module('MtaaAPI').service('persoanlDetailService', function($sessionstorage, fetchPersoanlDetailSvc) {
    var _self = this;
    _self.setMemberDetails = function (tempToken) {
                    //_self.deathCover = tempToken;
                    $sessionstorage.setItem('memberDetailsObject',JSON.stringify(tempToken));
};
    _self.getMemberDetails = function () {
                    //return _self.deathCover;
                    return fetchPersoanlDetailSvc.getMemberDetails();
};
});
//Auth service will be placed here
angular.module('MtaaAPI').service('inputDataService',function(){
	var _self= this;
	_self.setCoverDetails= function (temp) {
        _self.CoverDetails= temp;
	};
	_self.getCoverDetails= function () {
		return _self.CoverDetails;
	};
});

angular.module('MtaaAPI').factory('fetchCategoryDetailsService',['$http','$q',function($http,$q){
	var object={};
	object.getCategoryDetails= function(url,fundCode,category){
		var defer= $q.defer();
		$http.get(url,{
			headers:{
				'Content-Type': 'application/json'
			},
			params:{
				'fundCode': fundCode,
				'category': category
			}
		}).then(function(res){
			object= res.data;
			defer.resolve(res);
		},function(err){
			console.log('Something went wrong file fecthing category details...'+JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return object;
}]); 

angular.module('MtaaAPI').service('calculateFUL', ['$http', '$q','fetchTokenNumSvc', function($http, $q,fetchTokenNumSvc) {
	var factory = {};
	factory.calculate = function(url, data){
		var defer = $q.defer();
		$http.post(url,data,{
			 headers : {
                 'Content-Type': 'application/json',
                 'Authorization':fetchTokenNumSvc.getTokenId()
             }
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while calculation FUL..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return factory;
}]);


angular.module('MtaaAPI').factory('printPageService',['$http','$q','fetchTokenNumSvc',function($http,$q,fetchTokenNumSvc){
	var object={};
	object.getPrintObject= function(url,data,token){
		var defer= $q.defer();
		$http.post(url,data,{
			headers: {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
		}).then(function(res){
			object= res.data;
			defer.resolve(res);
		},function(err){
			console.log('Something went wrong file fecthing category details...'+JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return object;
}]); 
angular.module('MtaaAPI').factory('DownloadPDFService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var object = {};
    object.download = function(url,fileName){
        var defer = $q.defer();
        $http.post(url,{},{ 		
			headers: {
					'Authorization':fetchTokenNumSvc.getTokenId()
			}, params:{
	            'file_name': fileName
			},responseType: 'arraybuffer',
			transformResponse: function (data) {
	            var pdf;
	            if (data) {
	                pdf = new Blob([data], {
	                    type: 'application/pdf'
	                });
	            }
	            return {
	                response: pdf
	            };
        }}).then(function(res){
        	object = res.data;
            defer.resolve(res);
        }, function(err){
            console.log('Error while getting pdf data' + JSON.stringify(err));
            defer.reject(err);
        });
        return defer.promise;
    };
    return object;
}]);
angular.module('MtaaAPI').factory('CheckSavedAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.checkSavedApps = function(url,fundCode,clientRefNo,manageType){
        var defer = $q.defer();
        $http.get(url, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
                'fundCode': fundCode,
                'clientRefNo': clientRefNo,
                'manageType':manageType
        	}}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        	}, function(err){
                console.log('Error while getting saved apps' + JSON.stringify(err));
                defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);

angular.module('MtaaAPI').factory('CancelSavedAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.cancelSavedApps = function(url,applicationNumber){
        var defer = $q.defer();
        $http.post(url, {}, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        }, params:{
                'applicationNumber': applicationNumber
        }}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        }, function(err){
                console.log('Error while cancelling app' + JSON.stringify(err));
                defer.reject(err);
        });
        return defer.promise;
    };
    return factory;
}]);
angular.module('MtaaAPI').service('auraResponseService', function() {
    var _self = this;
    _self.setResponse = function (tempInput) {
                    _self.response = tempInput;
};
    _self.getResponse = function () {
      return _self.response;
};
});
angular.module('MtaaAPI').service('PersistenceService', function($sessionstorage, fetchAppNumberSvc){
	var thisObject = this;
	thisObject.uploadedFileList = [];
	thisObject.AuraTransferCoverData = {};
	thisObject.TransferCoverData = {};
	thisObject.LifeEventData = {};
	thisObject.ConvertMaintainData = {};
	thisObject.cancelCoverData = {};
	
	thisObject.setConvertMaintainDetails = function(data){
		thisObject.ConvertMaintainData = data;
	};
	
	thisObject.getConvertMaintainDetails = function(){
		return thisObject.ConvertMaintainData;
	};
	
	thisObject.setCancelCoverDetails = function(data){
		thisObject.cancelCoverData = data;
	};
	
	thisObject.getCancelCoverDetails = function(){
		return thisObject.cancelCoverData;
	};
	
	thisObject.setTransCoverAuraDetails = function(data){
		thisObject.AuraTransferCoverData = data;
		//$sessionstorage.setItem('AuraTransferCoverData',JSON.stringify(data));
	};
	
	thisObject.getTransCoverAuraDetails = function(){
		return thisObject.AuraTransferCoverData;
	    //return JSON.parse($sessionstorage.getItem('AuraTransferCoverData'));
	};
	
	thisObject.setTransferCoverDetails = function(data){
		thisObject.TransferCoverData = data;
	    //$sessionstorage.setItem('TransferCoverData',JSON.stringify(data));
	};
	
	thisObject.getTransferCoverDetails = function(){
		return thisObject.TransferCoverData;
		//return JSON.parse($sessionstorage.getItem('TransferCoverData'));
	};
	
	thisObject.setLifeEventDetails = function(data){
		thisObject.LifeEventData = data;
		//$sessionstorage.setItem('LifeEventData',JSON.stringify(data));
	};
	
	thisObject.getLifeEventDetails = function(){
	    //return JSON.parse($sessionstorage.getItem('LifeEventData'));
		return thisObject.LifeEventData;
	};
	
	thisObject.setUploadedFileDetails = function(fileList){
		thisObject.uploadedFileList = fileList;
        //sessionStorage.setItem('UploadedFileDetails',JSON.stringify(fileList));
    };
    
    thisObject.getUploadedFileDetails = function(){
    	return thisObject.uploadedFileList;
    	//return JSON.parse(sessionStorage.getItem('UploadedFileDetails')) === null ? null : JSON.parse(sessionStorage.getItem('UploadedFileDetails'));
    };
	
	thisObject.setAppNumber = function(appnum){
	    $sessionstorage.setItem('UniqueAppNum',JSON.stringify(appnum));
	};
	
	thisObject.getAppNumber = function(){
	    return fetchAppNumberSvc.getAppNumber();
	};
	
	thisObject.setAppNumToBeRetrieved = function(num){
		$sessionstorage.setItem('AppNumToBeRetrieved',JSON.stringify(num));
	};
	
	thisObject.getAppNumToBeRetrieved = function(){
	    return JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved')) === null ? null : JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved'));
	};
    thisObject.setPDFLocation = function(location){
        $sessionstorage.setItem('PDFLocation',JSON.stringify(location));
};
    thisObject.getPDFLocation = function(){
        try {
            if($sessionstorage.getItem('PDFLocation') === 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse($sessionstorage.getItem('PDFLocation'));
            }
        } catch(Error) {
            throw Error;
        }
    };
    thisObject.setNpsUrl = function(url){
        $sessionstorage.setItem('NpsLink',JSON.stringify(url));
};

thisObject.getNpsUrl = function(){
try {
if($sessionstorage.getItem('NpsLink') === 'undefined') {
throw {message: 'Not found'};
} else {
return JSON.parse($sessionstorage.getItem('NpsLink'));
}
} catch(Error) {
throw Error;
}
};

});

angular.module('MtaaAPI').factory('RetrieveAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.retrieveApp = function(url,fundCode,clientRefNo){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'clientRefNo': clientRefNo
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log('Error while retrieving saved apps ' + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

angular.module('MtaaAPI').factory('$fakeStorage', [
 function(){
     function FakeStorage() {}
     FakeStorage.prototype.setItem = function (key, value) {
         this[key] = value;
     };
     FakeStorage.prototype.getItem = function (key) {
         return typeof this[key] === 'undefined' ? null : this[key];
         };
         FakeStorage.prototype.removeItem = function (key) {
             this[key] = undefined;
         };
         FakeStorage.prototype.clear = function(){
             for (var key in this) {
                 if( this.hasOwnProperty(key) )
                 {
                     this.removeItem(key);
                 }
             }
         };
         FakeStorage.prototype.key = function(index){
             return Object.keys(this)[index];
         };
         return new FakeStorage();
     }
 ]);

angular.module('MtaaAPI').factory('$sessionstorage', ['$window', '$fakeStorage', function($window, $fakeStorage){
     function isStorageSupported(storageName)
     {
         var testKey = 'test',
             storage = $window[storageName];
         try
         {
             storage.setItem(testKey, '1');
             storage.removeItem(testKey);
             return true;
         }
         catch (error)
         {
             return false;
         }
     }
     var storage = isStorageSupported('sessionStorage') ? $window.sessionStorage : $fakeStorage;
         return {
             set: function(key, value) {
                 storage.setItem(key, value);
             },
             get: function(key, defaultValue) {
                 return storage.getItem(key) || defaultValue;
             },
             setItem: function(key, value) {
                 storage.setItem(key, value);
             },
             getItem: function(key) {
                 return storage.getItem(key);
             },
             remove: function(key){
                 storage.removeItem(key);
             },
             clear: function() {
                 storage.clear();
             },
             key: function(index){
                 storage.key(index);
             }
         };
     }
 ]);
angular.module('MtaaAPI').factory('RetrieveAppDetailsService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.retrieveAppDetails = function(url,applicationNumber){
        var defer = $q.defer();
        $http.get(url, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
                'applicationNumber': applicationNumber
        	}}).then(function(res){
                        factory = res.data;
                        defer.resolve(res);
        	},function(err){
                        console.log('Error while retrieving application ' + JSON.stringify(err));
                        defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);
/* Change for Duplicate Application UW message */
angular.module('MtaaAPI').factory('CheckUWDuplicateAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.checkDuplicateApps= function(url,fundCode,clientRefNo,manageTypeCC,dob,firstName,lastName){
        var defer = $q.defer();
        $http.post(url, {}, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
            	'fundCode': fundCode,
                'clientRefNo': clientRefNo,
                'manageTypeCC':manageTypeCC,
                'dob': dob,
                'firstName':firstName,
                'lastName': lastName
        	}}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        	},function(err){
                console.log('Error in UW duplicate application ' + JSON.stringify(err));
                defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);
