package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.VOWCoverDetailPageObjects;
import uimap.metlife.YBRCoverDetailPageObjects;

public class VOWCoverDetailPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public VOWCoverDetailPage webresponse() {
		webresponseinput();
		return new VOWCoverDetailPage(scriptHelper);
	}

	public VOWCoverDetailPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void webresponseinput() {
	
		String value = dataTable.getData("MetLife_Data", "InputXML");
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			Thread.sleep(1000);
			
			WebElement elmName = driver.findElement(By.id("input_string_id"));
			elmName.clear();
			elmName.sendKeys(value);
			
			//tryAction(fluentWaitElement(VOWCoverDetailPageObjects.txtareainput),"SET", "Input",value);
			//Thread.sleep(500);
			//Actions actions = new Actions(driver);
			//actions.moveToElement(driver.findElement(VOWCoverDetailPageObjects.btnNext)).click().perform();
			tryAction(fluentWaitElement(VOWCoverDetailPageObjects.btnNext), "Click", "Apply");
			//tryAction(fluentWaitElement(STFTHealthPageObjects.btnNext), "Click", "Apply");
			Thread.sleep(4000);
			
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
