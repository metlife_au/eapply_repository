package au.com.metlife.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.test.ClaimTestActivity;
import au.com.metlife.test.ClaimTestObj;

@RestController
public class ClaimTrackerController {
	
	private Document document;
//	@Autowired
//	RestTemplate resttemplate;
//	@RequestMapping(value = "/method11",method = RequestMethod.POST)	
//	public  ResponseEntity <HashMap<String, Object>> verifyCaptche(@RequestParam(value="claimNo") String claimNo,@RequestParam(value="surName") String surName,@RequestParam(value="dob") String dob) {
//		
//		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
//		String line = null;	
//		String string = "";	
//		HashMap<String, Object> cmsData = null;
//			try {
//				while ((line = in.readLine()) != null) {
//					string += line + "\n";
//					System.out.println("line>>"+line);
//				}
//					
//				JSONObject jsonObject = new JSONObject(string);		
//				//cmsData = cmsService.refreshCache();				
//				System.out.println("fdfd>"+cmsData);
//			    if(cmsData==null){
//			    	return new ResponseEntity<HashMap<String, Object>>(HttpStatus.NO_CONTENT);			        
//			    }	    
//			    System.out.println("END--"+new  java.util.Date());
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			return new ResponseEntity<HashMap<String, Object>>(cmsData,HttpStatus.OK);
//	    
//
//	}	
	@RequestMapping(method=RequestMethod.POST, value = "/login")
	public ResponseEntity<String> login(@RequestParam(value="secret") String secret,@RequestParam(value="response") String response,@RequestParam(value="remoteip") String remoteip) {
	   // System.out.println("jsonStr  " + jsonStr);
	   // JSONObject jsonObject = new JSONObject(jsonStr);
	   // String username = jsonObject.getString("username");
		RestTemplate resttemplate =new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("secret", secret);
		map.add("response", response);
		map.add("remoteip", remoteip);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		ResponseEntity<String> responseMsg = resttemplate.postForEntity( "https://www.google.com/recaptcha/api/siteverify", request , String.class );
		
	    return new ResponseEntity<String>(responseMsg.getBody(), HttpStatus.OK);
	}  
	
	@RequestMapping(method=RequestMethod.POST, value = "/retrieveClaimTestDetails")
	public  ResponseEntity <ClaimTestObj> retrieveClaimTestDetails(@RequestParam(value="claimId") String claimId,@RequestParam(value="surName") String surName,@RequestParam(value="dob") String dob) {		
		
		
				
		List<ClaimTestActivity> claimActivityTestList = new ArrayList<ClaimTestActivity>();	
		ClaimTestObj claimtestobj = null;
		
		
		
		
		
//		if(claimId.equalsIgnoreCase("123456")){
//			return new ResponseEntity<ClaimTestObj>(claimtestobj, HttpStatus.OK);	 
//	    } 
		if (claimId.equalsIgnoreCase("234567") && surName.equalsIgnoreCase("TEST") && dob.equalsIgnoreCase("12/06/1987")) {
	    	claimtestobj = new ClaimTestObj();
	    	claimtestobj.setClaimId("234567");
			claimtestobj.setDob("12/06/1987");
			claimtestobj.setStatus("Closed");
			claimtestobj.setStatusDate("27/11/2015");
			claimtestobj.setSurName("TEST");
	    	//return new ResponseEntity<ClaimTestObj>(claimtestobj, HttpStatus.OK);	 
	    } else if(claimId.equalsIgnoreCase("345678") && surName.equalsIgnoreCase("TEST TEST") && dob.equalsIgnoreCase("12/11/1987")){
	    	claimtestobj = new ClaimTestObj();
	    	claimtestobj.setClaimId("345678");
			claimtestobj.setDob("12/11/1987");
			claimtestobj.setStatus("Claim Approved");
			claimtestobj.setStatusDate("27/11/2016");
			claimtestobj.setSurName("TEST TEST");
	    }
	    else if(claimId.equalsIgnoreCase("456789") && surName.equalsIgnoreCase("TEST TEST TEST") && dob.equalsIgnoreCase("12/12/1987")){
	    	claimtestobj = new ClaimTestObj();
	    	claimtestobj.setClaimId("345678");
			claimtestobj.setDob("12/11/1987");
			claimtestobj.setStatus("Closed");
			claimtestobj.setStatusDate("27/11/2016");
			claimtestobj.setSurName("TEST TEST");
	    }
	    else {
	    	return new ResponseEntity<ClaimTestObj>(claimtestobj, HttpStatus.OK);
	    }
		
		ClaimTestActivity acvitivity1 = new ClaimTestActivity();
		acvitivity1.setId("1");
		acvitivity1.setName("Claim Created");
		acvitivity1.setDate("19/06/2012");
		acvitivity1.setRequirement(null);
		acvitivity1.setComments(null);
		acvitivity1.setRequirementReceviedDate(null);
		
		ClaimTestActivity acvitivity2 = new ClaimTestActivity();
		acvitivity2.setId("2");
		acvitivity2.setName("Information Received");
		acvitivity2.setDate("19/06/2012");
		acvitivity2.setRequirement(null);
		acvitivity2.setComments(null);
		acvitivity2.setRequirementReceviedDate(null);
		if (!claimId.equalsIgnoreCase("456789")) {
		ClaimTestActivity acvitivity3 = new ClaimTestActivity();
		acvitivity3.setId("3");
		acvitivity3.setName("Information Requested");
		acvitivity3.setDate("19/06/2012");
		acvitivity3.setRequirement("Group Admin Advice");
		acvitivity3.setComments(null);
		acvitivity3.setRequirementReceviedDate("22/06/2012");
		claimActivityTestList.add(acvitivity3);
		ClaimTestActivity acvitivity4 = new ClaimTestActivity();
		acvitivity4.setId("4");
		acvitivity4.setName("Information Requested");
		acvitivity4.setDate("22/06/2012");
		acvitivity4.setRequirement("Group Admin Advice");
		acvitivity4.setComments(null);
		acvitivity4.setRequirementReceviedDate(null);
		claimActivityTestList.add(acvitivity4);
		}
		
		ClaimTestActivity acvitivity5 = new ClaimTestActivity();
		acvitivity5.setId("5");
		acvitivity5.setName("Information Received");
		acvitivity5.setDate("22/06/2012");
		acvitivity5.setRequirement("Management Review");
		acvitivity5.setComments("Review of claim");
		acvitivity5.setRequirementReceviedDate(null);
		
		ClaimTestActivity acvitivity6 = new ClaimTestActivity();
		acvitivity6.setId("5");
		acvitivity6.setName("follow Up");
		acvitivity6.setDate("22/07/2012");
		acvitivity6.setRequirement("Management Review");
		acvitivity6.setComments("Review of claim");
		acvitivity6.setRequirementReceviedDate(null);
		
		claimActivityTestList.add(acvitivity1);
		claimActivityTestList.add(acvitivity2);
		//claimActivityTestList.add(acvitivity3);
		//claimActivityTestList.add(acvitivity4);
		claimActivityTestList.add(acvitivity5);
		claimActivityTestList.add(acvitivity6);
		
		claimtestobj.setClaimActivities(claimActivityTestList);
		
		
		
	    System.out.println("END--"+new  java.util.Date());
	    ResponseEntity responseEntity = new ResponseEntity<ClaimTestObj>(claimtestobj, HttpStatus.OK);
	    return new ResponseEntity<ClaimTestObj>(claimtestobj, HttpStatus.OK);	    

	}
	
//	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
//    public  ResponseEntity<DocumentJSON> uploadFile(@RequestHeader(value="Content-Type") String contentType,InputStream inputStream) {
//    DocumentJSON documentJSON = null;
//    System.out.println(">>>inside"+contentType);
//     BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//    String filepath = fileUploadPath+contentType;
//    File targetFile = new File(filepath);
//    OutputStream outStream = null;
//      try {                 
//          outStream = new FileOutputStream(targetFile);
//          byte[] buffer = new byte[1024];
//          int len = inputStream.read(buffer);
//          while (len != -1) {
//                outStream.write(buffer, 0, len);
//              len = inputStream.read(buffer);
//          }                 
//          IOUtils.copy(inputStream,outStream);
//          inputStream.close();
//          outStream.close();
//          documentJSON = new DocumentJSON();
//          documentJSON.setFileLocations(filepath);
//          documentJSON.setName(contentType);
//          documentJSON.setStatus(Boolean.TRUE);
//          
//      } catch (IOException e) {
//          throw new RuntimeException(e);
//      }          
//     
//     return new ResponseEntity<DocumentJSON>(documentJSON, HttpStatus.OK);
//}
	
//	@RequestMapping(method=RequestMethod.POST, value = "/fileupload")
//	public  ResponseEntity  fileupload(@RequestHeader(value="Content-Type") String contentType,InputStream inputStream) {		
//		System.out.println(">> >> File upload");
//		String filepath = "C:\\ClaimsTracker\\test.pdf";
//	    File targetFile = new File(filepath);
//	    OutputStream outStream = null;	
//		 try {                 
//	          outStream = new FileOutputStream(targetFile);
//	          byte[] buffer = new byte[1024];
//	          int len = inputStream.read(buffer);
//	          while (len != -1) {
//	              outStream.write(buffer, 0, len);
//	              len = inputStream.read(buffer);
//	          }                 
//	         // IOUtils.copy(inputStream,outStream);
//	          inputStream.close();
//	          outStream.close();	          
//	          
//	      } catch (IOException e) {
//	          throw new RuntimeException(e);
//	      }  
//		 
//		 return new ResponseEntity("{\"success\":true}", HttpStatus.OK);
//		
//	}
	
	@RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    @ResponseBody
    public  void multipleUpload(@RequestParam("claimStatus") String status,@RequestParam("qquuid") String  qquuid,@RequestParam("description") String description ,@RequestParam("qqfile") MultipartFile file,  HttpSession session, HttpServletResponse response, ModelAndView model) throws IllegalStateException, IOException, JSONException {
        System.out.println("inside mult***************"+file.getSize());
        System.out.println("new demo contrler");
        String saveDirectory = "C:\\ClaimsTracker\\";
        Document document = null;
         try 
         {
            Thread.sleep(1000);
         } 
         catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }  
         String fileName = file.getOriginalFilename();
         fileName = FilenameUtils.getName(fileName);
         System.out.println("fileName :"+fileName);
         System.out.println("status :"+status);
         if (!"".equalsIgnoreCase(fileName)) {
             // Handle file content - multipartFile.getInputStream()
             file.transferTo(new File(saveDirectory + fileName));
           }
         
			document = new Document();
			document.setDocumentCode(description);
			document.setDocumentlocation(saveDirectory+fileName);
			document.setDocumentStatus(status);
			document.setUuId(qquuid);
			document.setFileSize(""+file.getSize());
			document.setFileName(file.getOriginalFilename());
            
            JSONObject json1 = new JSONObject(); 
            
            ObjectMapper mapper = new ObjectMapper();
           
           String docString = mapper.writeValueAsString(document);
             json1.put("success", true);
             json1.put("doucment", docString);
             response.setCharacterEncoding("UTF-8");
             response.setContentType("text");
             response.getWriter().print(json1);
             response.flushBuffer();
         } 
	
	@RequestMapping(value = "/submitFiles", method = RequestMethod.POST)
    @ResponseBody
    public  void submitFiles(@RequestBody List<Document> list) throws IllegalStateException, IOException, JSONException {
		 //List<Document> docList = null;
		//HttpSession session = request.getSession(true);
		// docList = (ArrayList)session.getAttribute("docList");		
        System.out.println("submitFiles contrler");
//        ObjectMapper mapper = new ObjectMapper();
//        JsonNode node = mapper.readTree(str);
//        DocumentList docList = mapper.convertValue(node.get("docList"), DocumentList.class);
        
         } 
	@RequestMapping(value = "/initialFiles", method = RequestMethod.GET)
    @ResponseBody
    public  void initialFiles(HttpServletResponse response) throws IllegalStateException, IOException, JSONException {
		 JSONObject json1 = new JSONObject();
		 
         json1.put("name", "53290R_Life_PrivacyStatement_FA.PDF");
         json1.put("size", "262497");
         json1.put("id", "0");
         json1.put("uuid", "9a166683-b2b9-4fac-b5b7-823b874d897a");
         JSONArray jsonArray = new JSONArray();
         jsonArray.put(json1);
         response.setCharacterEncoding("UTF-8");
         response.setContentType("text");
         response.getWriter().print(jsonArray);
         response.flushBuffer();
	}

//	@ModelAttribute("document")
//	public Document addStuffToRequestScope() {
//		System.out.println("Inside of addStuffToRequestScope");
//		document = new Document();
//		document.setDocumentCode("1");
//		document.setDocumentlocation("test");
//		return document;
//	}

	
}
