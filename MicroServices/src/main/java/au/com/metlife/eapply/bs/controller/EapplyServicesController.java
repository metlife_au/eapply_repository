package au.com.metlife.eapply.bs.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lowagie.text.DocumentException;
import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.b2b.model.Eappstring;
import au.com.metlife.eapply.b2b.service.B2bService;
import au.com.metlife.eapply.b2b.utility.B2bServiceHelper;
import au.com.metlife.eapply.b2b.xsd.beans.Request;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.exception.MetLifeBSRuntimeException;
import au.com.metlife.eapply.bs.helper.EapplyServiceHelper;
import au.com.metlife.eapply.bs.model.AcceptedAppStatus;
import au.com.metlife.eapply.bs.model.AddressJSON;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Auradetail;
import au.com.metlife.eapply.bs.model.ContactDetailsJSON;
import au.com.metlife.eapply.bs.model.Contactinfo;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverJSON;
import au.com.metlife.eapply.bs.model.DeathAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.DocumentInfo;
import au.com.metlife.eapply.bs.model.DocumentJSON;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.model.EapplyOutput;
import au.com.metlife.eapply.bs.model.IpAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.OccupationCoversJSON;
import au.com.metlife.eapply.bs.model.PersonalDetailsJSON;
import au.com.metlife.eapply.bs.model.RetrieveApp;
import au.com.metlife.eapply.bs.model.TpdAddnlCoversJSON;
import au.com.metlife.eapply.bs.pdf.GenerateEapplyPDF;
import au.com.metlife.eapply.bs.pdf.PDFbusinessObjectMapper;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.bs.utility.BizflowXMLHelper;
import au.com.metlife.eapply.bs.utility.EapplyHelper;
import au.com.metlife.eapply.bs.utility.EmailHelper;
import au.com.metlife.eapply.corporate.service.CorpService;
import au.com.metlife.eapply.quote.utility.QuoteConstants;
import au.com.metlife.eapply.quote.utility.QuoteHelper;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.web.DocumentUpload;
import au.com.metlife.eapply.webservices.EapplicationWebServiceInitializer;

@RestController
/*@Component*/
public class EapplyServicesController {
	private static final Logger log = LoggerFactory.getLogger(EapplyServicesController.class);
	public static final int ZERO_CONST = 0;
	
	SystemProperty sys;
	DocumentUpload documentUpload;
	@Value("${spring.sfpsfeature}")
	private  String sfpsfeature;	
	
	private final B2bService b2bService;
	
	public String getSfpsfeature() {
		return sfpsfeature;
	}

	public void setSfpsfeature(String sfpsfeature) {
		this.sfpsfeature = sfpsfeature;
	}
	private final EapplyService eapplyService;
	
	
	
	 @Autowired
     public EapplyServicesController(final EapplyService eapplyService,final B2bService b2bService,final DocumentUpload documentUpload) {
        this.eapplyService = eapplyService;
        this.b2bService = b2bService;
        this.documentUpload = documentUpload;
     }
	 @Autowired
     CorpService corpService;
	 @RequestMapping("/checkSavedApplications")
	  public ResponseEntity<List<RetrieveApp>> checkSavedApplications(@RequestParam(value="fundCode") String fundCode
			  ,@RequestParam(value="clientRefNo") String clientRefNo
			  ,@RequestParam(value="manageType") String manageType,
			  @RequestHeader(value="Authorization") String Authorization) {
		 log.info("Checking saved application start");
		 List<RetrieveApp> retrieveAppList=null;
		 boolean corpFlow = false;
		 Request request = null;
		 String clientRefNumber = null;
		 if (null == Authorization){
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 } else {
			 Eappstring eappstring =  b2bService.retriveClientData(Authorization);
			 if (null == eappstring.getSecurestring()) {
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 } else {
				 if(eappstring.getSecurestring().contains("Corporate")){
					 corpFlow = true; 
				 }else
				 {
				 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
				 }
			 }
		 }
		 if(!corpFlow){
		 if (null != request){
			 clientRefNumber = request.getPolicy().getApplicant().get(0).getClientRefNumber();
			 if (null == clientRefNumber || null == clientRefNo){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }
			 if (!clientRefNo.equalsIgnoreCase(clientRefNumber)){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }  
		 } else {
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 }
		 }
		 
		 try{
			 if(fundCode!=null && clientRefNo!=null && manageType!=null){
				 retrieveAppList=new ArrayList<>();
				 List<Eapplication> savedApplications= eapplyService.retrieveSavedApplications(fundCode, clientRefNo,manageType);
				 if(savedApplications != null){
					 for(Eapplication savedEapplication:savedApplications){
						 if(savedEapplication!=null){
							 RetrieveApp retrieveApp=new RetrieveApp();
							 retrieveApp.setApplicationNumber(savedEapplication.getApplicationumber());
							 retrieveApp.setApplicationStatus(savedEapplication.getApplicationstatus());
							 retrieveApp.setCreatedDate(savedEapplication.getCreatedate());
							 retrieveApp.setRequestType(savedEapplication.getRequesttype());
							 retrieveApp.setLastSavedOnPage(savedEapplication.getLastsavedon());
							 if(savedEapplication.getAuradetail()!=null &&  savedEapplication.getAuradetail().getShortformstatus()!=null && savedEapplication.getAuradetail().getShortformstatus().trim().length()>0){
								 retrieveApp.setAuraCallRequired(Boolean.TRUE);
							 }
							 retrieveAppList.add(retrieveApp);
						 }
					 }
				 }
			 }
		 }catch(Exception e){
			log.error("Error in checking saved application: {}",e);
		 }
	    if(retrieveAppList==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }
	    log.info("Checking saved application start");
	    return new ResponseEntity<>(retrieveAppList, HttpStatus.OK);
	 }
	 
	 @RequestMapping("/check24hrs")
	  public ResponseEntity<Boolean> validateApplicationsSubIn24Hrs(@RequestParam(value="fundCode") String fundCode
			  ,@RequestParam(value="clientRefNo") String clientRefNo
			  ,@RequestParam(value="manageType") String manageType) {
		 log.info("Checking 24 hours start");
		 Boolean appsSubmittedinLast24Hrs=Boolean.FALSE;
		 try{
			 if(fundCode!=null && clientRefNo!=null && manageType!=null){
				 appsSubmittedinLast24Hrs= eapplyService.checkAppSubmittedInLast24Hrs(fundCode, clientRefNo,manageType);
			 }
		 }catch(Exception e){
			 log.error("Error in checking 24 hours : {}",e);
		 }
		 log.info("Checking 24 hours finish");
	    return new ResponseEntity<>(appsSubmittedinLast24Hrs, HttpStatus.OK);
	 }
	 
	 
	 @RequestMapping(value = "/download", method = RequestMethod.POST)
	 public @ResponseBody void downloadFile(@RequestHeader(value="Authorization") String Authorization,
			 @RequestParam(value="file_name") String fileName, 
	     HttpServletResponse response) {
		 log.info("Download file start");
		//Change as per Sonar fix "Try with Resource Added" start
		 /*InputStream is = null;*/
		 /*Properties property = new Properties();*/
		 String fileNames = fileName;
		 String basePath = null;
		 
		 if (null == Authorization){
			 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		 } else {
		 
		 
		 try {
			sys = SystemProperty.getInstance();
		} catch (Exception e1) {
			log.error("Error in reading system property: {}",e1);
		}
		 basePath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH);
		 if(fileNames!=null){
			 fileNames = basePath+EapplyHelper.formDecryptedFilePath(fileNames);
    		 /*System.out.println("fileNamefileName>>"+fileName);*/
    		 log.info("fileNamefileName>> {}",fileNames);
    		 try(InputStream is =  new FileInputStream(fileNames))
    		 {
    			 response.addHeader("Content-disposition", "attachment;filename="+fileNames.substring(fileNames.lastIndexOf('/')+1));
  		       response.setContentType("application/pdf");
  		       IOUtils.copy(is, response.getOutputStream());
  		       response.flushBuffer(); 
    		 }
	     /*try {
	    	 sys = SystemProperty.getInstance();
				//File file = new File( sys.getProperty("propertyFile"));
	    	 basePath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH);
	    	 if(fileName!=null){
	    		 fileName = basePath+EapplyHelper.formDecryptedFilePath(fileName);
	    		 System.out.println("fileNamefileName>>"+fileName);
	  	        is =  new FileInputStream(fileName);
	  	       response.addHeader("Content-disposition", "attachment;filename="+fileName.substring(fileName.lastIndexOf('/')+1));
		       response.setContentType("application/pdf");
		       IOUtils.copy(is, response.getOutputStream());
		       response.flushBuffer();
	    	 }

	     }*/ 
		 catch (IOException ex) {
	    	 log.error("IOError writing file to output stream {}",ex);
	     } catch (Exception e) {
	    	 log.error("file not found {}",e);			
		}/*finally{
	    	 try {
	    		 if(is!= null){
	    			 is.close();
	    		 }				
			} catch (IOException e) {
				log.error("error closing file in download functionality: {}",e);
			}
	     }*/
		 }
	 }
		//Change as per Sonar fix "Try with Resource Added" end
	     log.info("Download file finish");
	 }
	 
	 @RequestMapping("/retieveApps")
	  public ResponseEntity<List<RetrieveApp>> retireveApplicationList(
			  @RequestParam(value="fundCode") String fundCode,@RequestParam(value="clientRefNo") String clientRefNo,
			  @RequestHeader(value="Authorization") String Authorization) {	
		 log.info("Retrive application start");
		 List<RetrieveApp> retrieveAppList=null;
		 boolean corpFlow = false;
		 Request request = null;
		 String clientRefNumber = null;
		 if (null == Authorization){
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 } else {
			 Eappstring eappstring =  b2bService.retriveClientData(Authorization);
			 if (null == eappstring.getSecurestring()) {
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 } else {
				 if(eappstring.getSecurestring().contains("Corporate")){
					 corpFlow = true; 
				 }else
				 {
				 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
				 }
			 }
		 }
		 if(!corpFlow){
		 if (null != request){
			 clientRefNumber = request.getPolicy().getApplicant().get(0).getClientRefNumber();
			 if (null == clientRefNumber || null == clientRefNo){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }
			 if (!clientRefNo.equalsIgnoreCase(clientRefNumber)){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }  
		 } else {
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 }}
		 
		System.out.println("Authorization >>. "+Authorization); 
		 try{
			 if(fundCode!=null){
				 retrieveAppList=new ArrayList<>();
				 List<Eapplication> savedApplications= eapplyService.retrieveSavedApplications(fundCode, clientRefNo,null);
				 if(savedApplications!= null){
					 for(Eapplication savedEapplication:savedApplications){
						 if(savedEapplication!=null){
							 RetrieveApp retrieveApp=new RetrieveApp();
							 retrieveApp.setApplicationNumber(savedEapplication.getApplicationumber());
							 retrieveApp.setApplicationStatus(savedEapplication.getApplicationstatus());
							 retrieveApp.setCreatedDate(savedEapplication.getCreatedate());
							 retrieveApp.setRequestType(savedEapplication.getRequesttype());
							 retrieveApp.setLastSavedOnPage(savedEapplication.getLastsavedon());
							 if(savedEapplication.getAuradetail()!=null &&  savedEapplication.getAuradetail().getShortformstatus()!=null && savedEapplication.getAuradetail().getShortformstatus().trim().length()>0){
								 retrieveApp.setAuraCallRequired(Boolean.TRUE);
							 }
							 retrieveAppList.add(retrieveApp);
						 }
					 } 
				 }
			 }
		 }catch(Exception e){
			log.error("Error in retrive application: {}",e);
		 }
	    if(retrieveAppList==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }
	    log.info("Retrive application finish");
	    return new ResponseEntity<>(retrieveAppList, HttpStatus.OK);
	 }
	 
	 
	 @RequestMapping("/retieveApplication")
	  public ResponseEntity<List<EapplyInput>> retieveApplication(@RequestParam(value="applicationNumber") String applicationNumber,	  
			  @RequestHeader(value="Authorization") String Authorization) {		
		 log.info("Retrive application start");
		 /*List<RetrieveApp> retrieveAppList=null;*/
		 List<EapplyInput> eapplyInput=null;
		 boolean corpFlow = false;
		 Request request = null;
		 String clientRefNumber = null;
		 if (null == Authorization){
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 } else {
			 Eappstring eappstring =  b2bService.retriveClientData(Authorization);
			 if (null == eappstring.getSecurestring()) {
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 } else {
				 if(eappstring.getSecurestring().contains("Corporate")){
					 corpFlow = true; 
				 }else
				 {
				 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
				 }
			 }
		 }
		 if(!corpFlow){
		 if (null != request){
			 clientRefNumber = request.getPolicy().getApplicant().get(0).getClientRefNumber();
			 if (null == clientRefNumber){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }
			   
		 } else {
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 }
		 }
		System.out.println("Authorization >>. "+Authorization); 
		 
		 
		 try{
			 if(applicationNumber!=null){
				 Eapplication eapplication=eapplyService.retrieveApplication(applicationNumber);
				 
				 if (!corpFlow && !eapplication.getApplicant().get(0).getClientrefnumber().equals(clientRefNumber)){
					 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				 }
				 /*Convert eapplication to eapplyinout*/
				 eapplyInput = convertToEapplyInput(eapplication);		 
				
			 }
		 }catch(Exception e){
			 log.error("Error in retrive application: {}",e);
		 }
	    if(eapplyInput==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }
	    log.info("Retrive application finish");
	    return new ResponseEntity<>(eapplyInput, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value="/expireApplication", method = RequestMethod.POST)
	  public ResponseEntity<Boolean> expireApplication(@RequestParam(value="applicationNumber") String applicationNumber,
			  @RequestHeader(value="Authorization") String Authorization) {		
		 log.info("Expire application checking start");
		 Boolean expireApplication=Boolean.FALSE;
		 boolean corpFlow = false;
		 Request request = null;
		 String clientRefNumber = null;
		 if (null == Authorization){
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 } else {
			 Eappstring eappstring =  b2bService.retriveClientData(Authorization);
			 if (null == eappstring.getSecurestring()) {
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 } else {
				 if(eappstring.getSecurestring().contains("Corporate")){
					 corpFlow = true; 
				 }else
				 {
				 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
				 }
			 }
		 }
		 if(!corpFlow){
		 if (null != request){
			 clientRefNumber = request.getPolicy().getApplicant().get(0).getClientRefNumber();
			 if (null == clientRefNumber){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }
			   
		 } else {
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 }}
		 
		 try{
			 if(applicationNumber!=null){
				 Eapplication eapplication=eapplyService.expireApplication(applicationNumber);
				 
				 if (!corpFlow && !eapplication.getApplicant().get(0).getClientrefnumber().equals(clientRefNumber)){
					 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				 }
				 
				 expireApplication=Boolean.TRUE;
			 }
		 }catch(Exception e){
			 log.error("Error in checking expire application: {}",e);
		 }
		 log.info("Expire application checking finish");
	    return new ResponseEntity<>(expireApplication, HttpStatus.OK);
	 }
	 
	 
	 
	 @RequestMapping(value = "/submitEapply", method = RequestMethod.POST)
		public  ResponseEntity<EapplyOutput> submitEapply(@RequestHeader(value="Authorization") String authId, @RequestBody EapplyInput eapplyInput, UriComponentsBuilder ucBuilder) {
		 Boolean submitStatus = Boolean.FALSE;
		 log.info("Submit eapply satrt");
		 Eapplication eapplication = null;
		 EappHistory eappHistory = null;
		 String bizFlowXML = null;
		 PDFbusinessObjectMapper pdFbusinessObjectMapper = new PDFbusinessObjectMapper();
		 Properties property = new Properties();
		 InputStream url = null;
		 List<Document> documentList = null;
		 EapplyOutput eapplyOutput=null;
		 PDFObject pdfObject =null;
		 String clientRefNumber = null;
		 Eapplication prevRecord = null;
		  CorporateFund corpfund=null;
		 try {
			 String asJSON = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(eapplyInput);   
			 System.out.println("inputjson>>"+asJSON);
			 if(null!=eapplyInput && null!=eapplyInput.getManageType() && eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.NMCOVER)){
			  eapplication = convertToEapplication(eapplyInput,"pending");	
			 }else{
				 eapplication = convertToEapplication(eapplyInput,"Submitted");	
				 EapplyHelper.setdefaultDecision(eapplication, eapplyInput);
				 //Work rating maintained
				 if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eapplyInput.getOverallDecision()==null){
					 eapplyInput.setOverallDecision("ACC");
					 eapplication.setAppdecision("ACC");
				 }
				 if(eapplyInput.getOverallDecision()==null){
					 eapplyInput.setOverallDecision(eapplication.getAppdecision());
				 }
			 }
		
		
			 sys = SystemProperty.getInstance();

			//File file = new File( sys.getProperty("propertyFile"));
			 File file = new File( sys.getProperty(eapplication.getPartnercode()));
		

		     url = new FileInputStream( file);    
		      property.load( url);
		      if(eapplyInput.getCorpFundCode()!=null) {
		    	  corpfund=corpService.getFundCategoryDetails(eapplyInput.getCorpFundCode(),eapplyInput.getProductnumber());
		      }
			 /*log.info("getPropertyFile>> {}",sys.getProperty("propertyFile"));			 
			 pdfObject = pdFbusinessObjectMapper.getPdfOjectsForEapply(eapplication, eapplyInput, MetlifeInstitutionalConstants.INSTITUTIONAL, sys.getProperty("propertyFile"));
			 pdfObject = callpdfGeneration(pdfObject, sys.getProperty("propertyFile"), eapplyInput, eapplication, sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH));*/
		      log.info("getPropertyFile>> {}",sys.getProperty(eapplication.getPartnercode()));
			  pdfObject = pdFbusinessObjectMapper.getPdfOjectsForEapply(eapplication, eapplyInput, MetlifeInstitutionalConstants.INSTITUTIONAL, sys.getProperty(eapplication.getPartnercode()));
			  pdfObject = callpdfGeneration(pdfObject, sys.getProperty(eapplication.getPartnercode()), eapplyInput, eapplication, sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH));
			 

			 /*update document object*/
			  
			 Document document;			 
			 if(eapplication.getDocuments()!=null && eapplication.getDocuments().size()>ZERO_CONST){
				 document = new Document();
				 document.setDocLoc(pdfObject.getClientPDFLoc());
				 document.setDocType(MetlifeInstitutionalConstants.CLIENT);
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.getDocuments().add(document);	
				 document = new Document();
				 document.setDocLoc(pdfObject.getUwPDFLoc());
				 document.setDocType("uw");
				 
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.getDocuments().add(document);	
			 }else{
				 documentList = new ArrayList<>();
				 document = new Document();
				 document.setDocLoc(pdfObject.getClientPDFLoc());
				 document.setDocType(MetlifeInstitutionalConstants.CLIENT);
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 documentList.add(document);	
				 document = new Document();
				 document.setDocLoc(pdfObject.getUwPDFLoc());
				 document.setDocType("uw");
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 documentList.add(document);	
				
				 eapplication.setDocuments(documentList);
			 }	
			 /*Adding transfer upload documents*/
 			 /*if(eapplyInput.getTransferDocuments()!=null && eapplyInput.getTransferDocuments().size()>ZERO_CONST){
 				 for(DocumentJSON documentJSON:eapplyInput.getTransferDocuments()){
 					 Document transferDocument = new Document();
 					 transferDocument.setDocLoc(documentJSON.getFileLocations());
 					 transferDocument.setDocType(MetlifeInstitutionalConstants.TRANSFER_DOCUMENTS);
 					 transferDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
 					 eapplication.getDocuments().add(transferDocument);	
 				 }
 			 }
		 Adding transfer upload documents
 	     
 			 Adding life event upload documents
 			 if(eapplyInput.getLifeEventDocuments()!=null && eapplyInput.getLifeEventDocuments().size()>ZERO_CONST){
 				 for(DocumentJSON documentJSON:eapplyInput.getLifeEventDocuments()){
 					 Document lifeEventDocument = new Document();
 					lifeEventDocument.setDocLoc(documentJSON.getFileLocations());
 					lifeEventDocument.setDocType(MetlifeInstitutionalConstants.LIFE_EVENT_DOCUMANTES);
 					lifeEventDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
 					eapplication.getDocuments().add(lifeEventDocument);	
 				 }
 			 }*/
		 /*Adding life event upload documents*/	 
			 
 	 		/*Updating  Client and UW PDF in applicant*/
 	 			 if(eapplication.getApplicant()!=null && eapplication.getApplicant().size()>ZERO_CONST){
 	 				 Applicant applicant=eapplication.getApplicant().get(0);
 	 				clientRefNumber = applicant.getClientrefnumber();
 	 				 EmailHelper emailHelper = new EmailHelper();
 	 				 boolean isMixedDecision = false;
 	 				 if(eapplyInput!= null && eapplyInput.getResponseObject() != null) {
 	 					 isMixedDecision = emailHelper.isMixedDecision(eapplyInput.getResponseObject());
 	 				 }

					 /*emailHelper.sendEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId);*/
 	 				/*occ maintained*/
 	 				/* if(eapplyInput.getAuraDisabled()!= null && eapplyInput.getAuraDisabled().equalsIgnoreCase(MetlifeInstitutionalConstants.FALSE) && MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) 
 	 						&& !"DCL".equalsIgnoreCase(eapplication.getAppdecision())){
 	 					emailHelper.sendEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId);
 	 				}
 	 				Added by Prasanna Durga - Blocking client email for Decline scenario for Non Care Super Partners
 	 				if("CARE".equalsIgnoreCase(eapplication.getPartnercode())
 	 						|| (null!=eapplication && null!=eapplication.getAppdecision() && !"DCL".equalsIgnoreCase(eapplication.getAppdecision())
 	 	 	 						)){
 	 					emailHelper.sendEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId);
 	 				}*/
 	 				 
 	 				/*Added by Prasanna Durga - Blocking client email for Decline scenario for Non Care Super Partners*/
                     if("CARE".equalsIgnoreCase(eapplication.getPartnercode())){
                           emailHelper.sendEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision, eapplyInput.getPartnerCode());
                     }else if(!"CARE".equalsIgnoreCase(eapplication.getPartnercode()) && !"INGD".equalsIgnoreCase(eapplication.getPartnercode())
                                 && (null!=eapplication.getAppdecision() && !"DCL".equalsIgnoreCase(eapplication.getAppdecision())))
                     		{
                    	 	if((!MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()))||(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype())
                                 && eapplyInput.getAuraDisabled()!= null && eapplyInput.getAuraDisabled().equalsIgnoreCase(MetlifeInstitutionalConstants.FALSE)))
                    	 		{
                    	 			emailHelper.sendEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision,eapplyInput.getPartnerCode());
                    	 		}
                    	 	/*else if(!MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()))
                    	 		{
                    		 emailHelper.sendEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId);
                    	 		}*/
                     		} else if ("DCL".equalsIgnoreCase(eapplication.getAppdecision()) && MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplication.getRequesttype())
                   				 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplication.getPartnercode())){//Sending mail for Decline scenario,VIcsuper changes - Purna)
                     			emailHelper.sendDeclineEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision, eapplyInput.getPartnerCode());
                     		}else if(MetlifeInstitutionalConstants.DCL.equalsIgnoreCase(eapplication.getAppdecision()) && (MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplication.getRequesttype()) || MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplication.getRequesttype())) 
                     				&& MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(eapplication.getPartnercode())){ /*Sending mail for Decline scenario ,Host changes */
                     			emailHelper.sendEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision,eapplyInput.getPartnerCode());
                     		}
 	 				
 	 				/*if("AEIS".equalsIgnoreCase(eapplication.getPartnercode()) || "CARE".equalsIgnoreCase(eapplication.getPartnercode()) || (null!=eapplication && null!=eapplication.getAppdecision() && !"DCL".equalsIgnoreCase(eapplication.getAppdecision()) &&
 	 						(null!=eapplication.getPartnercode() && ("HOST".equalsIgnoreCase(eapplication.getPartnercode()) || "FIRS".equalsIgnoreCase(eapplication.getPartnercode()) || "SFPS".equalsIgnoreCase(eapplication.getPartnercode()))))){
 	 					emailHelper.sendEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId);
 	 				}*/
 	 				
 	 				
					// if(!((MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplication.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplication.getRequesttype()) || "NCOVER".equalsIgnoreCase(eapplication.getRequesttype())) && "ACC".equalsIgnoreCase(eapplication.getAppdecision())) ){
						 bizFlowXML = BizflowXMLHelper.createXMLForBizFlow(eapplication, eapplyInput,property,corpfund);
						 /*System.out.println("bizFlowXML>>"+bizFlowXML);*/
						 log.info("bizFlowXML>> {}",bizFlowXML);
						 BizflowXMLHelper.sendMetFlowRequestToMQ(bizFlowXML,property);
					//}
					
 	 				 applicant.setUwpdfloc(pdfObject.getUwPDFLoc());
 	 				 applicant.setClientpdfloc(pdfObject.getClientPDFLoc());
 	 			 }
 	 		/*Updating  Client and UW PDF in applicant*/

 	 			
 	 			 
 	 			/*Adding Fund Administrator Email*/
 	 			 if(eapplication.getApplicant()!=null && eapplication.getApplicant().size()>ZERO_CONST){
 	 				 Applicant applicant=eapplication.getApplicant().get(0);
 	 				 EmailHelper emailHelper = new EmailHelper();
 	 				 boolean isMixedDecision = false;
	 				 if(eapplyInput!= null && eapplyInput.getResponseObject() != null) {
	 					 isMixedDecision = emailHelper.isMixedDecision(eapplyInput.getResponseObject());
	 				 }
 	 				if((null!=eapplication && null!=eapplication.getAppdecision() && "DCL".equalsIgnoreCase(eapplication.getAppdecision())) &&
 						  	(null!=eapplication.getPartnercode() && !"CARE".equalsIgnoreCase(eapplication.getPartnercode()) && !"INGD".equalsIgnoreCase(eapplication.getPartnercode()))  &&
 						  	!(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplication.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplication.getRequesttype()) || MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype())
 						  			|| (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplication.getPartnercode()) 
 						  					&& MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplication.getRequesttype()))))
 				 {
 	 					if(eapplyInput.getPreviousTpdBenefit()!=null && "No".equalsIgnoreCase(eapplyInput.getPreviousTpdBenefit()) && "No".equalsIgnoreCase(eapplyInput.getTerminalIllClaimQue()) && eapplyInput.getTerminalIllClaimQue()!=null  && MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType())) {
 	 						log.info("No Fund Admin Mail due to Eligibility question fail for {}",eapplyInput.getManageType());
 	 					}
 	 					else {
 		 					emailHelper.sendFundAdminEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund, isMixedDecision,eapplyInput.getPartnerCode() );
 	 					}
 				 }
				 else if((null!=eapplication && null!=eapplication.getAppdecision() && !"DCL".equalsIgnoreCase(eapplication.getAppdecision())) &&
 						  	(null!=eapplication.getPartnercode() && !"CARE".equalsIgnoreCase(eapplication.getPartnercode()) && !"INGD".equalsIgnoreCase(eapplication.getPartnercode())) 
 						  	)
 				 {
 						if((!MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()))||MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eapplyInput.getAuraDisabled()!= null && eapplyInput.getAuraDisabled().equalsIgnoreCase(MetlifeInstitutionalConstants.FALSE)){
 							emailHelper.sendFundAdminEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision,eapplyInput.getPartnerCode());
 						}/*else if(!MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype())){
 							emailHelper.sendFundAdminEmail(pdfObject, eapplication, eapplyInput, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId);
 						}*/
	 					
 				 }
 				 else if(null!=eapplication.getPartnercode() && "CARE".equalsIgnoreCase(eapplication.getPartnercode()))
 				 {
 					emailHelper.sendFundAdminEmail(pdfObject, eapplication,  eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision,eapplyInput.getPartnerCode());
 				 }else if(MetlifeInstitutionalConstants.DCL.equalsIgnoreCase(eapplication.getAppdecision()) && (MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplication.getRequesttype()) || MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplication.getRequesttype())) 
          				&& MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(eapplication.getPartnercode())){ /*Sending mail for Decline scenario ,Host changes */
 					emailHelper.sendFundAdminEmail(pdfObject, eapplication,  eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,corpfund,isMixedDecision,eapplyInput.getPartnerCode());
          		} 
					 applicant.setUwpdfloc(pdfObject.getUwPDFLoc());
 	 			 }
 	 			/*Adding Fund Administrator Email*/
 	 			 prevRecord = eapplyService.retrieveApplication(""+eapplyInput.getAppNum());
	 	 		if(prevRecord!=null){
	 	 			eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
	 	 			eapplication.setCreatedate(prevRecord.getCreatedate());
	 	 		}else{
	 	 			eapplication.setCreatedate(new Timestamp(new Date().getTime()));
	 	 			eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
	 		    }
	 	 		
	 	 		 /*if(prevRecord!=null && prevRecord.getDocuments()!=null && prevRecord.getDocuments().size()>0){*/
	 	 		if(prevRecord!=null && prevRecord.getDocuments()!=null && !(prevRecord.getDocuments().isEmpty())){
	 	 			 /*Commented as per Sonar fix */
		    		 /*documentList=prevRecord.getDocuments();*/
		    		 //for (Iterator iterator = documentList.iterator(); iterator.hasNext();) {
		    		 	eapplyService.deleteDocument(prevRecord.getId());
		    		// }
		    		 
		    	 }
	 	 		if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
					 eapplication.setPartnercode(eapplyInput.getCorpFundCode());
				 }
			 eapplyService.submitEapplication(eapplication);
			 if("SFPS".equalsIgnoreCase(eapplyInput.getPartnerCode()) 
					 && "true".equalsIgnoreCase(getSfpsfeature())
					 && ("CANCOVER".equalsIgnoreCase(eapplyInput.getManageType()) || "CCOVER".equalsIgnoreCase(eapplyInput.getManageType()) || MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType()))) {
				 EapplicationWebServiceInitializer epEapplicationWebServiceInitializer = new EapplicationWebServiceInitializer();
				 eappHistory =  epEapplicationWebServiceInitializer.statewidePostback(eapplyInput, eapplication);
			 }else {
				 /*create record in history*/
				 eappHistory = new EappHistory();
				 eappHistory.setAppnumber(eapplication.getApplicationumber());
				 eappHistory.setAppdecision(eapplication.getAppdecision());
				 eappHistory.setClientrefnumber(clientRefNumber);
				 eappHistory.setCreatedate(new Timestamp(new Date().getTime()));
				 if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
					 eappHistory.setFundid(eapplyInput.getCorpFundCode());
				 }
				 else {
					 eappHistory.setFundid(eapplyInput.getPartnerCode());
				 }				
				 eappHistory.setMetadata(bizFlowXML);
				 eappHistory.setPicscaseid("BIZFLOW");
			 }
			eapplyService.createHistory(eappHistory);
			 submitStatus=Boolean.TRUE;
			 eapplyOutput=new EapplyOutput();
			 eapplyOutput.setAppStatus(submitStatus);
			 if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				 if(null!=eapplyInput.getPersonalDetails().getLastName()) {
					corpService.submitCorporateApp(eapplyInput.getAppNum(),eapplyInput.getPersonalDetails().getLastName());
				 }
				 else {
					 corpService.submitCorporateApp(eapplyInput.getAppNum(),eapplyInput.getLastName());
				 }
				}
			 if(pdfObject!=null){
				 /*System.out.println("pdfObject.getClientPDFLoc()>>"+pdfObject.getClientPDFLoc());*/
				 log.info("pdfObject.getClientPDFLoc()>> {}",pdfObject.getClientPDFLoc());
				 eapplyOutput.setClientPDFLocation(EapplyHelper.formEncryptedFilePath(pdfObject.getClientPDFLoc()));
			 }

			
				 /*npsTokenUrl ="nps_tokenURL"
				 nps_tokenURL= "http://c001.cloudrecording.com.au/cgi-bin/UR/ursurvey_pub_eapply.pl?f=1&token=oRbpWevWskI6gmePupLy"
				 setNpsToken(CoverDetailsHelper.getNPSToken(npsTokenUrl+"&fundCode="+getFundId()+"&custRefNumber="+applicantDTO.getClientRefNo()+"&TransactionCode="+applicationDTO.getApplicationNumber()));*/
				 String npsTokenUrl =property.getProperty("nps_tokenURL")+"&fundCode="+eapplication.getPartnercode()+"&custRefNumber="+clientRefNumber+"&TransactionCode="+eapplication.getApplicationumber();
				 log.info("npsTokenUrl>> {}",npsTokenUrl);
				 if(npsTokenUrl != "" && npsTokenUrl!= null){
					 try{
						 eapplyOutput.setNpsTokenURL(QuoteHelper.getNPSToken(npsTokenUrl));
					    /*setNpsToken(QuoteHelper.getNPSToken(npsTokenUrl));*/
						 log.info("npsToken inside loop>> {}",eapplyOutput.getNpsTokenURL());
					 }catch(Exception e){
						log.error("Error in NPS token: {}",e);
					 }
				 }
		 	} catch (IOException e) {
		 		log.error("Error in input output: {}",e);
		 		 return new ResponseEntity<>(eapplyOutput, HttpStatus.FORBIDDEN);
			} catch (DocumentException e) {
				log.error("Error in document: {}",e);
				 return new ResponseEntity<>(eapplyOutput, HttpStatus.FORBIDDEN);
			}catch (MetLifeBSRuntimeException e) {
				log.error("Error in metlifebs: {}",e);
				 return new ResponseEntity<>(eapplyOutput, HttpStatus.FORBIDDEN);
			}catch (Exception e) {
				log.error("Error in submit eapply: {}",e);
				 return new ResponseEntity<>(eapplyOutput, HttpStatus.FORBIDDEN);
			}   
		 log.info("Submit eapply finish");
	     return new ResponseEntity<>(eapplyOutput, HttpStatus.OK);
	 }
	 @RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
		public  ResponseEntity<DocumentJSON> uploadFile(@RequestHeader(value="Content-Type") String contentType,InputStream inputStream) {
		 log.info("File uplaod start");
		 DocumentJSON documentJSON = null;		
		 log.info(">>>inside {}",contentType);
		 String filepath = null;
		 File targetFile = null;
		//Change as per Sonar fix "Try with Resource Added" start
		 /*OutputStream outStream = null;*/
		 try {
			sys = SystemProperty.getInstance();
		} catch (Exception e1) {
			log.error("Error in reading system property: {}",e1);
		}
		 filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
		 targetFile = new File(filepath);
	        try(OutputStream outStream = new FileOutputStream(targetFile)) {
	        	 /*sys = SystemProperty.getInstance();
	        	filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
	        	targetFile = new File(filepath);
	        	outStream = new FileOutputStream(targetFile);*/
	        	byte[] buffer = new byte[1024];
	        	int len = inputStream.read(buffer);
	        	while (len != -1) {
	        		outStream.write(buffer, 0, len);
	        	    len = inputStream.read(buffer);
	        	}	            
	            IOUtils.copy(inputStream,outStream);
	            inputStream.close();
	            documentJSON = new DocumentJSON();
	            documentJSON.setFileLocations(filepath);
	            documentJSON.setName(contentType);
	            documentJSON.setStatus(Boolean.TRUE);
	            
	        } /*catch (IOException e) {
	            throw new RuntimeException(e);
	        }*/ catch (Exception e) {
			    log.error("Error in file uplaod: {}",e);
			} 	/*finally{
				try {
					if(outStream!=null){
						outStream.close();
					}					
				} catch (IOException e) {
					log.error("error closing file in fileupload functionality: {}",e);
				}
			}*/
	      //Change as per Sonar fix "Try with Resource Added" end
	        log.info("File uplaod finish");
		 return new ResponseEntity<>(documentJSON, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/fileUploadPost", method = RequestMethod.POST)
		public  ResponseEntity<DocumentJSON> uploadFile(@RequestHeader(value="Authorization") String Authorization,
				@RequestHeader(value="Content-Type") String contentType,InputStream inputStream,HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException, NoSuchAlgorithmException {
		 log.info("File uplaod start");
		 DocumentJSON documentJSON = null;		
		 log.info(">>>inside {}",contentType);
		 String filepath = null;
		 File targetFile = null;
		 File tmpFile = null;
	       Path tmpPath = null;
	       
		//Change as per Sonar fix "Try with Resource Added" start
		 /*OutputStream outStream = null;*/
	       
	    if (null == Authorization){
	  		 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	  	 }
	       
		 try {
			sys = SystemProperty.getInstance();
		} catch (Exception e1) {
			log.error("Error in reading system property: {}",e1);
		}
		 filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
		 targetFile = new File(filepath);
		 
		  boolean isSafe= documentUpload.doPost(targetFile, contentType, inputStream);
		 
		 	 
		 /* Step 3 : Take decision based on sfa status detected */
         // Take action is the file is not safe
         if (!isSafe) {
            // LOG.warn("Detection of a unsafe file upload or cannot sanitize uploaded document !");
             // Remove temporary file
             safelyRemoveFile(targetFile.toPath());
             // Return error
            // resp.sendError(HttpServletResponse.SC_FORBIDDEN);
             return new ResponseEntity<>(documentJSON, HttpStatus.FORBIDDEN);
         } else {
             // Here print file infos...
//             resp.setStatus(HttpServletResponse.SC_OK);
//             resp.setContentType("text/plain");
//             // For submitted file name, if you are on a container using Servlet API 3.1 then you can use method
//             // filePart.getSubmittedFileName() instead of my workaround method....
//             //resp.getWriter().printf("Submitted file name       : %s\n", extractSubmittedFileName((Part)req.getPart("fileContent")));
//             resp.getWriter().printf("Submitted file size       : %s\n", req.getPart("fileContent").getSize());
//             resp.getWriter().printf("Received temp file name   : %s\n", tmpFile.getName());
//             resp.getWriter().printf("Received temp file path   : %s\n", tmpFile.getAbsolutePath());
//             // Create a HASH of the file to check the integrity of the uploaded content
//             byte[] content = Files.readAllBytes(tmpPath);
//             MessageDigest digester = MessageDigest.getInstance("sha-256");
//             byte[] hash = digester.digest(content);
//             String hashHex = DatatypeConverter.printHexBinary(hash);
//             resp.getWriter().printf("Received temp file SHA256 : %s\n", hashHex);
         
		 
//		 filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
//		 targetFile = new File(filepath);
//	        try(OutputStream outStream = new FileOutputStream(targetFile)) {
//	        	 /*sys = SystemProperty.getInstance();
//	        	filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
//	        	targetFile = new File(filepath);
//	        	outStream = new FileOutputStream(targetFile);*/
//	        	byte[] buffer = new byte[1024];
//	        	int len = inputStream.read(buffer);
//	        	while (len != -1) {
//	        		outStream.write(buffer, 0, len);
//	        	    len = inputStream.read(buffer);
//	        	}	            
//	            IOUtils.copy(inputStream,outStream);
//	            inputStream.close();
	            documentJSON = new DocumentJSON();
	            documentJSON.setFileLocations(filepath);
	            documentJSON.setName(contentType);
	            documentJSON.setStatus(Boolean.TRUE);
	            
//	        } /*catch (IOException e) {
//	            throw new RuntimeException(e);
//	        }*/ catch (Exception e) {
//			    log.error("Error in file uplaod: {}",e);
//			 // Remove temporary file
//	            safelyRemoveFile(tmpPath);
//	            // Log error
//	         //   LOG.error("Error during detection of file upload safe status !", e);
//	            // Return an access denied to stay consistent from a client point of
//	            // view (not discrepancy on response)
//	           // resp.sendError(HttpServletResponse.SC_FORBIDDEN);
//	            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
//			} 	/*finally{
//				try {
//					if(outStream!=null){
//						outStream.close();
//					}					
//				} catch (IOException e) {
//					log.error("error closing file in fileupload functionality: {}",e);
//				}
			//}*/
	      //Change as per Sonar fix "Try with Resource Added" end
	        log.info("File uplaod finish");
		 return new ResponseEntity<>(documentJSON, HttpStatus.OK);
         }
	 }
	 
	 /**
	     * Utility methods to safely remove a file
	     *
	     * @param p file to remove
	     */
	    private static void safelyRemoveFile(Path p) {
	        try {
	            if (p != null) {
	                // Remove temporary file
	                if (!Files.deleteIfExists(p)) {
	                    // If remove fail then overwrite content to sanitize it
	                    Files.write(p, "-".getBytes("utf8"), StandardOpenOption.CREATE);
	                }
	            }
	        } catch (Exception e) {
	           // LOG.warn("Cannot safely remove file !", e);
	        }
	    }
	 
	 @RequestMapping(value = "/clientMatch", method = RequestMethod.POST)
		public  ResponseEntity<Boolean> clientMatch(InputStream inputStream) throws IOException, JSONException {
		 log.info("Client match service call start");
		String line;
		String string = "";	
		JSONObject json;
		 String firstName = null;
		 String surName = null;
		 String dob = null;
		 BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		 while ((line = in.readLine()) != null) { 
			string += line + "\n";
			json = new JSONObject(string);  		
			if(json.get("firstName")!=null)
				firstName = (String)json.get("firstName");
			if(json.get("lastName")!=null)
				surName =(String)json.get("lastName");
			if(json.get("dob")!=null)
				dob =(String)json.get("dob");
			}
		 Boolean clientMatch = eapplyService.clientMatch(firstName, surName, dob);
		 log.info("Client match service call finish");
		 return new ResponseEntity<>(clientMatch, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/saveEapply", method = RequestMethod.POST)
		public  ResponseEntity<Boolean> saveEapply(@RequestBody EapplyInput eapplyInput,    UriComponentsBuilder ucBuilder) {
		 log.info("Save eapply data start");
		 Boolean submitStatus = Boolean.FALSE;
		 Eapplication prevRecord = null;
		 Eapplication eapplication = null;
		 Boolean docRecord = null;
		 log.info(">>>inside");	
		 /*List<Document> documentList = new ArrayList<>();*/
	     try {
	    	  eapplication = convertToEapplication(eapplyInput,"Pending");
	    	  prevRecord = eapplyService.retrieveApplication(""+eapplyInput.getAppNum());
	    	 if(prevRecord!=null){
	    		 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
	    		 eapplication.setCreatedate(prevRecord.getCreatedate());
	    	 }else{
	    		 eapplication.setCreatedate(new Timestamp(new Date().getTime()));
	    		 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
	    	 }
	    	 
	    	 /*if(prevRecord!=null && prevRecord.getDocuments()!=null && prevRecord.getDocuments().size()>0){*/
	    	 if(prevRecord!=null && prevRecord.getDocuments()!=null && !(prevRecord.getDocuments().isEmpty())){
	    		 /*commented as per sonar fix*/
	    		 /*documentList=prevRecord.getDocuments();*/
	    		 //for (Iterator iterator = documentList.iterator(); iterator.hasNext();) {
	    		 docRecord=eapplyService.deleteDocument(prevRecord.getId());
	    		// }
	    		 
	    	 }
	    	 
			eapplyService.submitEapplication(eapplication);
		} catch (Exception e) {
			log.error("Error in save eapply: {}",e);
		}
	     log.info("Save eapply data finish");
	     return new ResponseEntity<>(submitStatus, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/saveCorporateEapply", method = RequestMethod.POST)
		public  ResponseEntity<Boolean> saveCorporateEapply(@RequestHeader(value="Authorization") String authId,@RequestBody EapplyInput eapplyInput,    UriComponentsBuilder ucBuilder) {
		 log.info("Save eapply data start");
		 Boolean submitStatus = Boolean.FALSE;
		 Eapplication prevRecord = null;
		 Eapplication eapplication = null;
		 Boolean docRecord = null;
		 log.info(">>>inside");	
		 /*List<Document> documentList = new ArrayList<>();*/
	     try {
	    	  eapplication = convertToEapplication(eapplyInput,"Pending");
	    	  prevRecord = eapplyService.retrieveApplication(""+eapplyInput.getAppNum());
	    	 if(prevRecord!=null){
	    		 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
	    		 eapplication.setCreatedate(prevRecord.getCreatedate());
	    	 }else{
	    		 eapplication.setCreatedate(new Timestamp(new Date().getTime()));
	    		 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
	    	 }
	    	 
	    	 /*if(prevRecord!=null && prevRecord.getDocuments()!=null && prevRecord.getDocuments().size()>0){*/
	    	 if(prevRecord!=null && prevRecord.getDocuments()!=null && !(prevRecord.getDocuments().isEmpty())){
	    		 docRecord=eapplyService.deleteDocument(prevRecord.getId());
	    	 }
	    	 
			eapplyService.submitEapplication(eapplication);
			if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
			 if(null!=eapplyInput.getPersonalDetails().getLastName()) {
					 corpService.saveCorporateApp(eapplyInput.getAppNum(),eapplyInput.getPersonalDetails().getLastName());
			}
			else {
						 corpService.saveCorporateApp(eapplyInput.getAppNum(),eapplyInput.getLastName());
			 }
				corpService.sendSaveEmail(eapplyInput,authId);
				corpService.sendFundSaveEmail(eapplyInput,authId);
			}
		} catch (Exception e) {
			log.error("Error in save eapply: {}",e);
		}
	     log.info("Save eapply data finish");
	     return new ResponseEntity<>(submitStatus, HttpStatus.OK);
	 }
	 private PDFObject callpdfGeneration(PDFObject pdfObject, String  pdfBasePath, EapplyInput eapplyInput, Eapplication eapplication, String fileUploadPath)throws DocumentException{
		log.info("Pdf generation start"); 
		String clientPdfLocation;
		String pdfAttachDir;
		String logoPath;
		String uwPdfLocation;
		 String corpFundId = null;
		 InputStream url;
		 List<String> questionHideList = new ArrayList<>();
		Properties property = new Properties();
		sys = SystemProperty.getInstance();
		File file = new File( pdfBasePath);
        try {
			url = new FileInputStream( file);
		    property.load( url);
		
		/*need to set this*/
        questionHideList.add("11241");
        questionHideList.add("11242");
        questionHideList.add("11243");
        questionHideList.add("11251");
        questionHideList.add("11252");        
     
         pdfObject.setQuestionListForHide(questionHideList);
	        pdfObject.setLob(MetlifeInstitutionalConstants.INSTITUTIONAL);		 

			/*pdfObject.setFundId("CARE");*/
	        pdfObject.setFundId(eapplication.getPartnercode());
			
	        pdfObject.setConvertCheckPdf(eapplyInput.isConvertCheck());
			pdfObject.setSectionBackground(property.getProperty("ClientSectionColor_" + pdfObject.getFundId()));
			pdfObject.setColorStyle(property.getProperty("ClientColorStyle_" + pdfObject.getFundId()));
			pdfObject.setSectionTextColor(property.getProperty("ClientSectionTextColor_" + pdfObject.getFundId()));
			pdfObject.setBorderColor(pdfObject.getSectionBackground());
			logoPath = property.getProperty("logo_" + pdfObject.getFundId()); 
		    pdfObject.setLogPath(logoPath);	
			pdfObject.setApplicationNumber(""+eapplyInput.getAppNum());
			if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				corpFundId=eapplyInput.getCorpFundCode();
			}
			clientPdfLocation = getIndvPdfAttachDir(eapplication, MetlifeInstitutionalConstants.INSTITUTIONAL,MetlifeInstitutionalConstants.CLIENT,sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH),property.getProperty("fund_pdf_name"),corpFundId);
			pdfObject.setPdfAttachDir(clientPdfLocation);
			pdfObject.setClientPDFLoc(clientPdfLocation);
			GenerateEapplyPDF generateEapplyPDF = new GenerateEapplyPDF();
			if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
			{
				pdfObject.setClientPdfType(MetlifeInstitutionalConstants.CLIENT);
				pdfObject.setDob(eapplyInput.getDob().replace("/", ""));
			}
			pdfAttachDir = generateEapplyPDF.generatePDF(pdfObject,pdfBasePath);
			pdfObject.setFundId(eapplication.getPartnercode());
			pdfObject.setSectionBackground(property.getProperty("UWSectionColor_" + pdfObject.getFundId()));
    		pdfObject.setColorStyle(property.getProperty("UWColorStyle_" + pdfObject.getFundId()));
    		pdfObject.setSectionTextColor(property.getProperty("UWSectionTextColor_" + pdfObject.getFundId()));
    		pdfObject.setBorderColor(pdfObject.getSectionBackground());
    		pdfObject.setPdfType("UW");
    		pdfObject.setClientPdfType(pdfObject.getPdfType());
    		uwPdfLocation = getIndvPdfAttachDir(eapplication, MetlifeInstitutionalConstants.INSTITUTIONAL,"UW",sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH),"",corpFundId);
    		pdfObject.setPdfAttachDir(uwPdfLocation);		
    		pdfObject.setUwPDFLoc(uwPdfLocation);
    		
				generateEapplyPDF.generatePDF(pdfObject, pdfBasePath);
			
			pdfObject.setUwPDFLoc(uwPdfLocation);
        } catch (FileNotFoundException e) {
			log.error("Error in getting file {}",e);
		} catch (IOException e) {
			log.error("Error in input and output {}",e);
		}  
			log.info("Pdf generation finish");
		return pdfObject;
	 }
	 
	 private String getIndvPdfAttachDir(Eapplication eapplication,String lob,String type, String fileUploadPath,String fundPdfName,String corpFundId) {
		    log.info("Individual pdf attach directory start");
	        String pdfAttachDir;
	        Applicant applicantDTO;      
	        String baseDirPath =  fileUploadPath;
	        Applicant applicantDTOPrimary = null;
	        String applicantsNames = null;  
	        StringBuilder builder;
	       
	        if (null != eapplication && null != eapplication.getApplicant()) {
	            builder = new StringBuilder();
	            for (int itr = 0; itr < eapplication.getApplicant().size(); itr++) {
	                applicantDTO = eapplication.getApplicant().get( itr);                
	                applicantDTOPrimary = applicantDTO;        
	                /*if (null != applicantDTO
	                        && null != applicantDTO.getApplicantType()
	                        && applicantDTO.getApplicantType().equalsIgnoreCase( "Primary")) {
	                    applicantDTOPrimary = applicantDTO;                   
	                }*/
	               
	            }          
	            if (null != applicantDTOPrimary) {
	                if (null != applicantDTOPrimary.getTitle())
	                    builder.append(applicantDTOPrimary.getTitle());
	                if (null != applicantDTOPrimary.getFirstname())
	                    builder.append(applicantDTOPrimary.getFirstname());
	                if (null != applicantDTOPrimary.getLastname())
	                    builder.append(applicantDTOPrimary.getLastname());
	            }          
	            applicantsNames = builder.toString();
	            //WR 19005:Added or modified to remove the single and double quotes in the PDF file names.
	            if(null != applicantsNames /*&& applicantsNames.contains(" ")*/){
	            	applicantsNames = applicantsNames.replaceAll("[\'\" ]", "");
				}
	          
	        }
	        if (null!=type && type.equalsIgnoreCase( "UW")) {                  
	         
	        	/*pdfAttachDir = baseDirPath + "UW_" + applicantsNames + "_"
	                     + eapplication.getApplicationumber() + ".pdf";*/
	        	
	        	/*Attaching Client email for Fund Administrator mails -- Added by 169682*/
	        	if (null != eapplication && null != eapplication.getPartnercode()) {
	        		if(null!=corpFundId) {
	        		pdfAttachDir = baseDirPath +corpFundId+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        		}
	        		
	        		/*else if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplication.getPartnercode()))
	        		{
	        			pdfAttachDir = baseDirPath +eapplication.getPartnercode()+"_"+applicantsNames + "_" + eapplication.getApplicationumber()+"_"+applicantDTOPrimary.getClientrefnumber()
		                + ".pdf";
	        		}*/
	        		
	        		else {
	        			pdfAttachDir = baseDirPath +eapplication.getPartnercode()+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
		                + ".pdf";
		        		}
	        	}else{
	        		pdfAttachDir = baseDirPath +MetlifeInstitutionalConstants.CLIENT+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        	}
	            
	        }else {
	        	if(null!=fundPdfName && !"".equalsIgnoreCase(fundPdfName)){
	        		if(null!=corpFundId) {
		        		pdfAttachDir = baseDirPath +corpFundId+fundPdfName+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
		                + ".pdf";
		        		}
	        		else if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplication.getPartnercode()))
	        		{
	        			pdfAttachDir = baseDirPath +fundPdfName/*+"_"+applicantsNames*/ + "_" + eapplication.getApplicationumber()+"_"+applicantDTOPrimary.getClientrefnumber()
		                + ".pdf";
	        		}
	        		else {
		        	pdfAttachDir = baseDirPath +fundPdfName+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        		}
	        	}else{
	        		pdfAttachDir = baseDirPath +MetlifeInstitutionalConstants.CLIENT+"_"+applicantsNames + "_" + eapplication.getApplicationumber()
	                + ".pdf";
	        	}
	        }
	        /*commented as per sonar fix*/
	        /*applicantDTO = null;       
	        baseDirPath = null;
	        applicantDTOPrimary = null;
	        applicantsNames = null;
	        applicantDTO = null;  
	        builder = null;*/
	        
	        log.info("Individual pdf attach directory finsh {}" ,pdfAttachDir);
	        return pdfAttachDir;
	    }

	private Eapplication convertToEapplication(EapplyInput eapplyInput,String appStatus) throws ParseException {
		log.info("Convert to eapplication start");
		 Eapplication eapplication =new Eapplication();
		 Applicant applicant1=new Applicant();
		 List<Applicant> applicantList=new ArrayList<>();	
		 Calendar dob;
 		DateFormat df1;
		 if(eapplyInput!=null){
		 			/*Eapplication object mapping start*/
		 			//eapplication.setCreatedate(new Timestamp(new Date().getTime()));
		 			//eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
		 			eapplication.setApplicationstatus(appStatus);
		 			
		 			//temperory fix
		 			if(eapplyInput.getDeathDecision()==null && !"DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
		 				eapplyInput.setDeathDecision("ACC");
		 			}
					if(eapplyInput.getTpdDecision()==null && !"DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
						eapplyInput.setTpdDecision("ACC");
					}
					if(eapplyInput.getIpDecision()==null && !"DCL".equalsIgnoreCase(eapplyInput.getOverallDecision())){
						eapplyInput.setIpDecision("ACC");	
					}
					if(eapplyInput.isAckCheck()){
						eapplication.setGenconsentindicator("true");						
					}
					if(eapplyInput.isDodCheck()){
						eapplication.setDisclosureindicator("true");
					}
					if(eapplyInput.isPrivacyCheck()){
						eapplication.setPrivacystatindicator("true");
					}
					if(eapplyInput.isFulCheck()){
						eapplication.setFulstatindicator("true");
					}
					/**Vicsuper changes made by purna - starts**/
					if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())){
						if(eapplyInput.isIpDisclaimer()){
							eapplication.setNuwflowind(MetlifeInstitutionalConstants.Y);
						} else {
							eapplication.setNuwflowind(MetlifeInstitutionalConstants.N);
						}						
						if(null!= eapplyInput && eapplyInput.getOwnOccuptionTpd()){
							eapplication.setOccupgrade(MetlifeInstitutionalConstants.Y);
						} else {
							eapplication.setOccupgrade(MetlifeInstitutionalConstants.N);
						}
						if(null!= eapplyInput && eapplyInput.getOwnOccuptionDeath())
						{
							eapplication.setMarketOptOut(MetlifeInstitutionalConstants.Y);
						}
						else
						{
							eapplication.setMarketOptOut(MetlifeInstitutionalConstants.N);
						}
						if(eapplyInput.getOwnOccuptionIp() != null && eapplyInput.getOwnOccuptionIp().equalsIgnoreCase(MetlifeInstitutionalConstants.TRUE)){
							eapplication.setEmailcopyindicator(MetlifeInstitutionalConstants.TRUE);
						} else {
							eapplication.setEmailcopyindicator(MetlifeInstitutionalConstants.FALSE);
						}
					}
					/**Vicsuper changes made by purna - ends**/
					
		 			if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.CANCOVER) && "CARE".equalsIgnoreCase(eapplyInput.getPartnerCode())){
		 				if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		 					for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
		 						CoverJSON coverJSON = (CoverJSON) iterator.next();
		 						if(coverJSON.getBenefitType()!=null){
		 							if("1".equalsIgnoreCase(coverJSON.getBenefitType())){
		 								DeathAddnlCoversJSON deathAddnlCoversJSON = new DeathAddnlCoversJSON();
		 								eapplyInput.setExistingDeathAmt(coverJSON.getAmount());
		 								eapplyInput.setDeathOccCategory(coverJSON.getOccRating());
		 								//eapplyInput.setDeathCoverType(deathCoverType);
		 								if("1".equalsIgnoreCase(coverJSON.getType())){		 									
		 									eapplyInput.setExistingDeathUnits(coverJSON.getUnits());
		 									deathAddnlCoversJSON.setDeathCoverType(MetlifeInstitutionalConstants.TPDUNITISED);
		 									deathAddnlCoversJSON.setDeathInputTextValue("0");
		 								}else{
		 									deathAddnlCoversJSON.setDeathCoverType(MetlifeInstitutionalConstants.DC_FIXED);
		 								}
		 								deathAddnlCoversJSON.setDeathFixedAmt("0");
		 								deathAddnlCoversJSON.setDeathCoverName(MetlifeInstitutionalConstants.CANCEL);
		 								deathAddnlCoversJSON.setDeathCoverPremium("0");
		 								eapplyInput.setAddnlDeathCoverDetails(deathAddnlCoversJSON);	
		 								
		 								
		 							}else if("2".equalsIgnoreCase(coverJSON.getBenefitType())){
		 								TpdAddnlCoversJSON tpdAddnlCoversJSON = new TpdAddnlCoversJSON();
		 								eapplyInput.setExistingTpdAmt(coverJSON.getAmount());
		 								eapplyInput.setTpdOccCategory(coverJSON.getOccRating());
		 								if("1".equalsIgnoreCase(coverJSON.getType())){
		 									eapplyInput.setExistingTPDUnits(coverJSON.getUnits());
		 									tpdAddnlCoversJSON.setTpdCoverType(MetlifeInstitutionalConstants.TPDUNITISED);
		 									tpdAddnlCoversJSON.setTpdInputTextValue("0");
		 								}else{
		 									tpdAddnlCoversJSON.setTpdCoverType(MetlifeInstitutionalConstants.TPDFIXED);
		 									
		 								}
		 								tpdAddnlCoversJSON.setTpdFixedAmt("0");
		 								tpdAddnlCoversJSON.setTpdCoverName(MetlifeInstitutionalConstants.CANCEL);
		 								tpdAddnlCoversJSON.setTpdCoverPremium("0");
		 								eapplyInput.setAddnlTpdCoverDetails(tpdAddnlCoversJSON);
		 							}else if("4".equalsIgnoreCase(coverJSON.getBenefitType())){
		 								IpAddnlCoversJSON ipAddnlCoversJSON = new IpAddnlCoversJSON();
		 								eapplyInput.setExistingIPAmount(coverJSON.getAmount());
		 								eapplyInput.setWaitingPeriod(coverJSON.getWaitingPeriod());
		 								eapplyInput.setBenefitPeriod(coverJSON.getBenefitPeriod());
		 								eapplyInput.setIpOccCategory(coverJSON.getOccRating());
		 								if("1".equalsIgnoreCase(coverJSON.getType())){
		 									eapplyInput.setExistingIPUnits(coverJSON.getUnits());
		 									ipAddnlCoversJSON.setIpCoverType(MetlifeInstitutionalConstants.IP_UNITISED);
		 									ipAddnlCoversJSON.setIpInputTextValue("0");
		 								}else{
		 									ipAddnlCoversJSON.setIpCoverType(MetlifeInstitutionalConstants.IP_FIXED);		 									
		 								}
		 								ipAddnlCoversJSON.setBenefitPeriod(coverJSON.getBenefitPeriod());
		 								ipAddnlCoversJSON.setWaitingPeriod(coverJSON.getWaitingPeriod());		 								
		 								ipAddnlCoversJSON.setIpFixedAmt("0");
		 								ipAddnlCoversJSON.setIpCoverName(MetlifeInstitutionalConstants.CANCEL);
		 								ipAddnlCoversJSON.setIpCoverPremium("0");
		 								eapplyInput.setAddnlIpCoverDetails(ipAddnlCoversJSON);
		 							}
		 						}
		 						
								
							}
		 				}
		 				eapplication.setAppdecision("ACC");
		 				eapplyInput.setOverallDecision("ACC");
		 			}else if(eapplyInput.getOverallDecision()==null){
		 				eapplyInput.setOverallDecision("ACC");
		 				//convert& maintain scenario- by default all cover are acc
		 				eapplication.setAppdecision("ACC");
		 			}else{
		 				eapplication.setAppdecision(eapplyInput.getOverallDecision());
		 			}
		 			//temperory fix end
		 			eapplication.setApplicationtype(MetlifeInstitutionalConstants.INSTITUTIONAL);
		 			eapplication.setLastsavedon(eapplyInput.getLastSavedOn());
		 			if(eapplyInput.getClientMatchReason()!= null && eapplyInput.getClientMatchReason().length()<3000){
		 				eapplication.setClientmatchreason(eapplyInput.getClientMatchReason());
		 			}
		 			eapplication.setClientmatch(eapplyInput.getClientMatched());
		 			/*if(eapplyInput.getClientMatched()){
		 				
		 			}else{
		 				eapplication.setClientmatch("No");
		 			}*/		 			
		 			
		 			if(eapplyInput.getAuraDisabled()!= null && eapplyInput.getAuraDisabled().equalsIgnoreCase(MetlifeInstitutionalConstants.FALSE)){
		 				eapplication.setUnderwritingpolicy("Yes");
		 			}else if(eapplyInput.getResponseObject()!=null || "AuraPage".equalsIgnoreCase(eapplyInput.getLastSavedOn())){
		 				eapplication.setUnderwritingpolicy("Yes");
		 			}else{
		 				eapplication.setUnderwritingpolicy("No");
		 			}
		 			if(null!=eapplyInput && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())
		 					&& eapplyInput.isCcoverIndex())
		 			{
		 				eapplication.setUnderwritingpolicy("No");
		 			}
		 			if(eapplyInput.getAppNum()!=null){
		 				eapplication.setApplicationumber(eapplyInput.getAppNum().toString());
		 			}
		 			if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getProductnumber()!=null) {
		 				eapplication.setProductnumber(eapplyInput.getProductnumber());
		 			}
		 			eapplication.setPartnercode(eapplyInput.getPartnerCode());
		 			eapplication.setRequesttype(eapplyInput.getManageType());
		 			DateFormat dateMonthFormat = new SimpleDateFormat("MMyy");
					String monthDir = dateMonthFormat.format(new java.util.Date());
					if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getCorpFundCode()!=null)
					{
						String campaignCode = eapplyInput.getCorpFundCode() + monthDir +"WG-" + eapplyInput.getAppNum();
						eapplication.setCampaigncode(campaignCode);
					}
					else {
						String campaignCode = eapplication.getPartnercode() + monthDir +"WG-" + eapplyInput.getAppNum();
						eapplication.setCampaigncode(campaignCode);
					}
		 			
		 			if((eapplyInput.getDeathExclusions()!=null && eapplyInput.getDeathExclusions().trim().length()>ZERO_CONST)
		 					|| (eapplyInput.getTpdExclusions()!=null && eapplyInput.getTpdExclusions().trim().length()>ZERO_CONST)
		 					|| (eapplyInput.getIpExclusions()!=null && eapplyInput.getIpExclusions().trim().length()>ZERO_CONST)
		 					){
		 				eapplication.setSaindicator("True");
		 			}else{
		 				eapplication.setSaindicator(MetlifeInstitutionalConstants.FALSE);
		 			}
		 			if((eapplyInput.getDeathLoading()!=null && eapplyInput.getDeathLoading().trim().length()>ZERO_CONST)
		 					|| (eapplyInput.getTpdLoading()!=null && eapplyInput.getTpdLoading().trim().length()>ZERO_CONST)
		 					|| (eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
		 					){
		 				eapplication.setPaindicator("True");
		 			}else{
		 				eapplication.setPaindicator(MetlifeInstitutionalConstants.FALSE);
		 			}
		 			if(eapplyInput.getTotalPremium() != null){
		 				eapplication.setTotalMonthlyPremium(eapplyInput.getTotalPremium());
		 			}
		 			/*Eapplication object mapping end*/
		 			
		 			/*Applicant object mapping start*/
		            if(eapplyInput.getAppNum()!=null){
		            	applicant1.setQuestionnaireid(eapplyInput.getAppNum().toString());
		 			}
		            if(eapplyInput.getPersonalDetails()!=null){
		            	applicant1.setCreatedate(new Timestamp(new Date().getTime()));
			 			applicant1.setLastupdatedate(new Timestamp(new Date().getTime()));
			 			if(eapplyInput.getPersonalDetails().getTitle() != null && eapplyInput.getPersonalDetails().getTitle()!=""){
			 				applicant1.setTitle(eapplyInput.getPersonalDetails().getTitle());
			 			}else if(eapplyInput.getTitle() != null && eapplyInput.getTitle() != ""){
			 				applicant1.setTitle(eapplyInput.getTitle());
			 			}
		            	if(eapplyInput.getPersonalDetails().getFirstName() != null && eapplyInput.getPersonalDetails().getFirstName() !=""){
		            		applicant1.setFirstname(eapplyInput.getPersonalDetails().getFirstName());
		            	}else if(eapplyInput.getFirstName() != null && eapplyInput.getFirstName() != ""){
		            		applicant1.setFirstname(eapplyInput.getFirstName());
		            	}
		            	if(eapplyInput.getPersonalDetails().getLastName()!= null && eapplyInput.getPersonalDetails().getLastName() !=""){
		            		applicant1.setLastname(eapplyInput.getPersonalDetails().getLastName());
		            	}else if(eapplyInput.getLastName() != null && eapplyInput.getLastName() !=""){
		            		applicant1.setLastname(eapplyInput.getLastName());
		            	}
		            	
		            	if(eapplyInput.getPersonalDetails().getDateOfBirth()!=null){
		            		dob = Calendar.getInstance();
		            		df1 = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
		            		dob.setTime(df1.parse(eapplyInput.getPersonalDetails().getDateOfBirth()));
		            		applicant1.setBirthdate(dob.getTime());
		            	}else if(eapplyInput.getDob()!= null && eapplyInput.getDob()!=""){
		            		dob = Calendar.getInstance();
		            		df1 = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);
		            		dob.setTime(df1.parse(eapplyInput.getDob()));
		            		applicant1.setBirthdate(dob.getTime());
		            	}
		            	if(eapplyInput.getPersonalDetails().getSmoker()!=null){
	            			applicant1.setSmoker(eapplyInput.getPersonalDetails().getSmoker());
	            			//Added for metflow part
	            			
		            	}else if(eapplyInput.getSmoker()!= null && eapplyInput.getPersonalDetails().getSmoker() !=""){
		            		applicant1.setSmoker(eapplyInput.getSmoker());
		            	}
		            	if(eapplyInput.getPersonalDetails().getGender() != null){
		            		applicant1.setGender(eapplyInput.getPersonalDetails().getGender());
		            	}
		            	/*in case of CANCEL cover */
		            	if(eapplyInput.getGender() != null){  
		            		applicant1.setGender(eapplyInput.getGender());
		            	}
		            }
		            
		            if(eapplyInput.getOccupationDetails() != null){
			        	 applicant1.setWorkhours(eapplyInput.getOccupationDetails().getFifteenHr());
			             /**Vicsuper Code changes starts - Made by Purna**/
			        	 //if (MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) 
			        			// && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
			        	 	eapplication.setMagazineoffer(eapplyInput.getOccupationDetails().getOccRating1a());
			        	 	eapplication.setAuthorizationindicator(eapplyInput.getOccupationDetails().getOccRating1b());			        		 
			        	 //} else {
			        		 applicant1.setIndustrytype(eapplyInput.getOccupationDetails().getIndustryCode());
				        	 applicant1.setOccupationcode(eapplyInput.getOccupationDetails().getIndustryName());
			        	// }
			        	 /**Vicsuper Code changes ends - Made by Purna**/
			        	 /* applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());*/
		            	 /*if(eapplyInput.getOccupationDetails().getOccupation().equalsIgnoreCase("Other")){
		            		 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOtherOccupation()); 
		            	 }else{
		            		 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());
		            	 }*/
			        	 
			        	 /*169682: Added code for Other occupation*/
			        	 applicant1.setOccupation(eapplyInput.getOccupationDetails().getOccupation());
			        	 if(eapplyInput.getOccupationDetails().getOccupation() !=null && eapplyInput.getOccupationDetails().getOccupation().equalsIgnoreCase("Other")){
		            		 applicant1.setOtheroccupation(eapplyInput.getOccupationDetails().getOtherOccupation()); 
		            	 }
			        	 
		            	 applicant1.setAnnualsalary(eapplyInput.getOccupationDetails().getSalary());
		            	 if(eapplyInput.getOccupationDetails().getCitizenQue() != null){
		            		 applicant1.setAustraliacitizenship(eapplyInput.getOccupationDetails().getCitizenQue()); 
		            	 }
			        	 applicant1.setGender(eapplyInput.getOccupationDetails().getGender());
			        	 
			        	 // own business questions for Change and Cancel cover
			        	 if(eapplyInput.getOccupationDetails().getOwnBussinessQues() != null){
			        		 applicant1.setOwnbusinessque(eapplyInput.getOccupationDetails().getOwnBussinessQues()); 
			        	 }
			        	 if(eapplyInput.getOccupationDetails().getOwnBussinessYesQues() != null){
			        		 applicant1.setOwnbusinessyesque(eapplyInput.getOccupationDetails().getOwnBussinessYesQues());
			        	 }
			        	 if(eapplyInput.getOccupationDetails().getOwnBussinessNoQues() != null){
			        		 applicant1.setOwnbusinesnosque(eapplyInput.getOccupationDetails().getOwnBussinessNoQues());
			        	 }
			        	 // for Statewide
			        	 if(eapplyInput.getOccupationDetails().getSmokerQuestion() != null){
			        		 applicant1.setSmoker(eapplyInput.getOccupationDetails().getSmokerQuestion());
			        	 }
			        	 if(eapplyInput.getOccupationDetails().getPermanentlyEmployedQuestion() != null){
			        		 applicant1.setPermanentemplque(eapplyInput.getOccupationDetails().getPermanentlyEmployedQuestion());
			        	 }	
			        	 if(eapplyInput.getOccupationDetails().getHoursPerWeekQuestion() != null){
			        		 applicant1.setThirtyfivehrswrkque(eapplyInput.getOccupationDetails().getHoursPerWeekQuestion());
			        	 }
			        	 
			        	 /* For HOST
		        	     *  Workduties -  Do you work in a hazardous environment and/or perform any duties of a manual nature? hazardousQue
			        	 *  Spendtimeoutside - Do you spend more than 10% of your working time outside of an office environment? managementRoleQue
			        	 *  Occupationduties -  Are your duties entirely undertaken within an office environment? withinOfficeQue
			        	 *  Tertiaryque - Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a
			        	 *  			  government body related to your profession? tertiaryQue
			        	 */
			        	 
		        	     applicant1.setWorkduties(eapplyInput.getOccupationDetails().getHazardousQue()); 
			        	 applicant1.setOccupationduties(eapplyInput.getOccupationDetails().getWithinOfficeQue());
			        	 applicant1.setSpendtimeoutside(eapplyInput.getOccupationDetails().getManagementRoleQue());
			        	 /**Vicsuper Code changes starts - Made by Purna**/
			        	 if ((MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType())
			        			 || MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())
			        			 || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType())
			        			 || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplyInput.getManageType())
			        			 || MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplyInput.getManageType())) 
			        			 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
			        		 applicant1.setTertiaryque(eapplyInput.getOccupationDetails().getOccRating2());
			        	 } else {
			        		 applicant1.setTertiaryque(eapplyInput.getOccupationDetails().getTertiaryQue());
			        	 }
			        	 /**Vicsuper Code changes ends - Made by Purna**/
		           }
		            if(null != eapplyInput.getRsnFrAdnlCvr() && !"".equalsIgnoreCase(eapplyInput.getRsnFrAdnlCvr())){
		            	applicant1.setRelation(eapplyInput.getRsnFrAdnlCvr());
		            }

		            applicant1.setApplicanttype("Primary");
		            /*if(eapplyInput.getContactDetails().getEmailAddress()!=null){
		            	System.out.println("email updated"+eapplyInput.getEmail());
		            	//applicant1.setCustomerreferencenumber(eapplyInput.getContactDetails().getEmailAddress());
		            	applicant1.setEmailid(eapplyInput.getContactDetails().getEmailAddress());
		            }*/
		            if(eapplyInput.getEmail()!=null){
	            	applicant1.setEmailid(eapplyInput.getEmail());
	            }
		        	applicant1.setDatejoinedfund(eapplyInput.getDateJoined());
		            applicant1.setClientrefnumber(eapplyInput.getClientRefNumber());
		        	applicant1.setMembertype(eapplyInput.getMemberType());
		            applicant1.setAge(Integer.toString(eapplyInput.getAge()));
		            if(eapplyInput.getContactDetails().getFundEmailAddress()!=null){
		            	//applicant1.setEmailid(eapplyInput.getContactDetails().getFundEmailAddress());
		            	applicant1.setCustomerreferencenumber(eapplyInput.getContactDetails().getFundEmailAddress());
		            }
		        	applicant1.setContacttype(eapplyInput.getContactType());
		        	applicant1.setContactnumber(eapplyInput.getContactPhone());
		        	applicant1.setOverallcoverdecision(eapplyInput.getOverallDecision());
		        	if(eapplyInput.getIndexationDeath()!= null && eapplyInput.getIndexationDeath().equalsIgnoreCase("true")){
		        		applicant1.setDeathindexationflag("1");
		        	}else{
		        		applicant1.setDeathindexationflag("0");
		        	}
		        	
		        	if(eapplyInput.getIndexationTpd()!= null && eapplyInput.getIndexationTpd().equalsIgnoreCase("true")){
		        		applicant1.setTpdindexationflag("1");
		        	}else{
		        		applicant1.setTpdindexationflag("0");
		        	}
		        	
		        	if(eapplyInput.getUnitisedCovers()!= null && eapplyInput.getUnitisedCovers().equalsIgnoreCase("true")){
		        		applicant1.setUnitcheckflag("1");
		        	}else{
		        		applicant1.setUnitcheckflag("0");
		        	}
		        	
		        	
		        	if(eapplyInput.getIpcheckbox() != null && eapplyInput.getIpcheckbox().equalsIgnoreCase("true")){
		        		applicant1.setInsuredsalary("Y");
		        	}else{
		        		applicant1.setInsuredsalary("N");
		        	}
	        		
		           /* Transfer Previous cover section fields*/
		        	applicant1.setPreviousinsurername(eapplyInput.getPreviousFundName());
		        	applicant1.setFundmempolicynumber(eapplyInput.getMembershipNumber());
		        	applicant1.setSpinnumber(eapplyInput.getSpinNumber());
		        	applicant1.setPrevioustpdclaimque(eapplyInput.getPreviousTpdClaimQue());
		        	if(eapplyInput != null && eapplyInput.getTerminalIllClaimQue()!= null){
		        		applicant1.setPreviousterminalillque(eapplyInput.getTerminalIllClaimQue());  /*for transfer and new member */
		        	}
		        	if(eapplyInput != null && eapplyInput.getDocumentName() != null){
		        		applicant1.setDocumentevidence(eapplyInput.getDocumentName());  /*for transfer and new member */
		        	}
		        	
		        	/*lifeevent mapping */
		        	
		        	if(null!=eapplyInput && null!=eapplyInput.getEventName() && !"".equalsIgnoreCase(eapplyInput.getEventName())){
		        		applicant1.setLifeevent(eapplyInput.getEventName());
		        		
		        	}
		        	if(null!=eapplyInput && null!=eapplyInput.getEventDate() && !"".equalsIgnoreCase(eapplyInput.getEventDate())){
		        		applicant1.setLifeeventdate(eapplyInput.getEventDate());
		        	}
		        	if(null!=eapplyInput && null!=eapplyInput.getEventAlreadyApplied() && !"".equalsIgnoreCase(eapplyInput.getEventAlreadyApplied())){
		        		applicant1.setEventalreadyapplied(eapplyInput.getEventAlreadyApplied());
		        		
		        	}
		        	/*For Cancel Cover*/
		        	if(null!=eapplyInput && null!=eapplyInput.getCancelreason() && !"".equalsIgnoreCase(eapplyInput.getCancelreason())){
		        		applicant1.setCancelreason(eapplyInput.getCancelreason());
		        	}
		        	if(null!=eapplyInput && null!=eapplyInput.getCancelotherreason() && !"".equalsIgnoreCase(eapplyInput.getCancelotherreason())){
		        		applicant1.setCancelotherreason(eapplyInput.getCancelotherreason());
		        	}
		        		
		        	// For special cover	
		        	if(null!=eapplyInput && null!=eapplyInput.getPreviousTpdBenefit() && eapplyInput.getPreviousTpdBenefit() != ""){
		        		applicant1.setPreviostpdbenefit(eapplyInput.getPreviousTpdBenefit());
		        		
		        	}
		        		
		        	
		        	
		        	/*Applicant object mapping start*/
		        	
		        	List<Cover> coverList = null;
		            if(eapplyInput.getManageType()!= null){
		            	if(eapplyInput.getManageType().equalsIgnoreCase("CCOVER")){
		            		coverList = createChangeCvCovers(eapplyInput);
		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.TCOVER)){
		            		coverList = createTransferCvCovers(eapplyInput);
		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.UWCOVER)){
		            		coverList = createUpdateWorkRatingCvCovers(eapplyInput);
		            	}else  if(eapplyInput.getManageType().equalsIgnoreCase("NCOVER")){
		            		coverList = createNewMemberCvCovers(eapplyInput);
		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.CANCOVER)){
		            		coverList = createCancelCvCovers(eapplyInput);
 		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.ICOVER)){
		            		coverList = createLifeEventCovers(eapplyInput);
 		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.SCOVER)){
		            		coverList = createSpecialCvCovers(eapplyInput);
 		            	}else if(eapplyInput.getManageType().equalsIgnoreCase(MetlifeInstitutionalConstants.NMCOVER)){
		            		coverList = createNonMemberCvCovers(eapplyInput);
 		            	}
 		            }
		            /*Cover object mapping end*/
		            
		            /*Aura detail object mapping start*/
		            Auradetail auradetail =new Auradetail();
		            auradetail.setCreatedate(new Timestamp(new Date().getTime()));
		            auradetail.setLastupdatedate(new Timestamp(new Date().getTime()));		           
		            if(eapplyInput.getResponseObject()!=null){
		            	  auradetail.setShortformstatus("Long");
		            }
		            if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())
		            		&& null!=eapplyInput.getAuraSessionId()) {
		            		auradetail.setAurasessionid(eapplyInput.getAuraSessionId());
		            }
		          
		            /*Aura detail object mapping end*/
		            
		            applicant1.setCovers(coverList);
		            applicantList.add(applicant1);
		            eapplication.setApplicant(applicantList);
		            eapplication.setAuradetail(auradetail);
		            eapplication.setPolicynumber(eapplyInput.getPolicyNumber());
		           // System.out.println("eapplyInput.getPolicyNumber()>>"+eapplyInput.getPolicyNumber());
		            /*eapplication.setFundInfo(fundInfoList);*/
		            
		            if(eapplyInput.getContactDetails()!=null){
		            	Contactinfo contactinfo=new Contactinfo();
		            	contactinfo.setMobilephone(eapplyInput.getContactDetails().getMobilePhone());
		            	contactinfo.setHomephone(eapplyInput.getContactDetails().getHomePhone());
		            	contactinfo.setWorkphone(eapplyInput.getContactDetails().getWorkPhone());
		            	contactinfo.setPreferedcontactnumber(eapplyInput.getContactPhone());
		            	contactinfo.setPreferedcontacttype(eapplyInput.getContactType());
		            	contactinfo.setPreferedcontacttime(eapplyInput.getContactPrefTime());
		            	contactinfo.setAddressline1(eapplyInput.getAddress().getLine1());
		            	contactinfo.setAddressline2(eapplyInput.getAddress().getLine2());
		            	contactinfo.setCountry(eapplyInput.getAddress().getCountry());
		            	contactinfo.setPostcode(eapplyInput.getAddress().getPostCode());
		            	contactinfo.setState(eapplyInput.getAddress().getState());
		            	contactinfo.setSuburb(eapplyInput.getAddress().getSuburb());
		            	contactinfo.setOthercontacttype(eapplyInput.getOtherContactType());
		            	contactinfo.setOthercontactnumber(eapplyInput.getOtherContactPhone());
		            	applicant1.setContactinfo(contactinfo);
		            }
		            
		          /*contactinfo.setApplicant(applicant1);
		          contactinfo.setAddressline1(addressline1);*/
		           /*document details*/
		            /*Adding transfer upload documents*/
		            List<Document> documentList = new ArrayList<>();
	 	 			 if(eapplyInput.getTransferDocuments()!=null && eapplyInput.getTransferDocuments().size()>ZERO_CONST){
	 	 				 for(DocumentJSON documentJSON:eapplyInput.getTransferDocuments()){
	 	 					 Document transferDocument = new Document();
	 	 					 transferDocument.setDocLoc(documentJSON.getFileLocations());
	 	 					 transferDocument.setDocType(MetlifeInstitutionalConstants.TRANSFER_DOCUMENTS);
	 	 					 transferDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
	 	 					documentList.add(transferDocument);	 	 					
	 	 					
	 	 				 }
	 	 				eapplication.setDocuments(documentList);
	 	 			 }
	 	 			 /*below code is throwing null pointer while saving when no file is uploaded*/
	 	 			 /*else{
	 	 				 eapplication.getDocuments().add(null);	
	 	 			 }*/
	  			 /*Adding transfer upload documents*/
	 	 	     
	 	 			 /*Adding life event upload documents*/
	 	 			 if(eapplyInput.getLifeEventDocuments()!=null && eapplyInput.getLifeEventDocuments().size()>ZERO_CONST){
	 	 				 for(DocumentJSON documentJSON:eapplyInput.getLifeEventDocuments()){
	 	 					 Document lifeEventDocument = new Document();
	 	 					lifeEventDocument.setDocLoc(documentJSON.getFileLocations());
	 	 					if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())){
	 	 						lifeEventDocument.setDocType(eapplyInput.getEventName());
	 	 					} else {
	 	 						lifeEventDocument.setDocType(MetlifeInstitutionalConstants.LIFE_EVENT_DOCUMANTES);
	 	 					}	 	 					
	 	 					lifeEventDocument.setLastupdatedate(new Timestamp(new Date().getTime()));
	 	 					documentList.add(lifeEventDocument);	 	 
	 	 				 }
	 	 				eapplication.setDocuments(documentList);
	 	 			 } /*below code is throwing null pointer while saving when no file is uploaded*/
	 	 			 /*else{
	 	 				 eapplication.getDocuments().add(null);	
	 	 			 }*/
		 		}
	           
	           /* eapplication.setDocuments(documentList);;
               eapplication.setFundInfo(fundInfo);*/
		 log.info("Convert to eapplication finish");
		return eapplication;
	}
	 
private List<Cover> createChangeCvCovers(EapplyInput eapplyInput) {
	   log.info("Create change cover start");
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		String ipExistingAmt = null;
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<>();
		
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
		deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
		deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
		deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());

		if(deathExUnit != null && deathExUnit!=""){

			deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		}
		if(eapplyInput.getDeathLoading()!=null){
		 deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
		}
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost() != null){
			deathCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost()));
		}
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
		deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());			
		}else{	
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt())).intValue());
		}
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
				deathCover.setAddunitind("0");
			}
		}
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
		
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		tpdCover.setCovercode("TPD");
		tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
		tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());

		if(tpdExUnit!=null && tpdExUnit!=""){

			tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		}
		if(eapplyInput.getTpdLoading()!=null)
			tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost() != null){
			tpdCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost()));
		}
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
		tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());			
		}else{	
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt())).intValue());
			}
		tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
				tpdCover.setAddunitind("0");
			}
		}
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
		if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);
		}else if(eapplyInput.getExistingIPAmount()!=null){
			ipCover.setExistingcoveramount(new BigDecimal((eapplyInput.getExistingIPAmount())));
			eapplyInput.setExistingIPAmount(eapplyInput.getExistingIPAmount());
		}
		
		if(ipExUnit!= null && ipExUnit !=""){
			ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		}


		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype("Salary Continuance");
		ipCover.setCovercode("IP");
		ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
		ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
		if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
			ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
		if(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()!= null){
			ipCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()));
		}
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
		ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
		ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue())).intValue());			
		}else{
			if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!=null) {
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt())).intValue());
			}
			
		}
		ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
		
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
				ipCover.setAddunitind("0");
			}
		}	
		if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
		    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
		}
		if(null!= eapplyInput.getAddnlIpCoverDetails().getIpAddnlUnits())
		{
			ipCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlIpCoverDetails().getIpAddnlUnits()));
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());

		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
						&& coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
				
			}
		}
		if("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()))
		{
			tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
			ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
		}
		if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getExistingIPAmount()!=null
				&& eapplyInput.isConvertCheck())
		{
			eapplyInput.setExistingIPUnits(ipExUnit);
			ipCover.setAdditionalunit(Integer.valueOf(ipExUnit));
		}
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		/*Cover object mapping end*/
		log.info("Create change cover finish");
		return coverList;
}
private List<Cover> createNonMemberCvCovers(EapplyInput eapplyInput) {
	   log.info("Create non member cover start");
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		/*Commented as per sonar fix*/
		/*String ipExistingAmt = null;*/
		/*if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}*/
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<>();
		
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
		deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
		deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
		deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());

		if(deathExUnit != null && deathExUnit!=""){

			deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		}
		if(eapplyInput.getDeathLoading()!=null){
		 deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
		}
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost() != null){
			deathCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost()));
		}
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
		deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());			
		}else{
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt())).intValue());
			
		}
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
				deathCover.setAddunitind("0");
			}
		}
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
		
		
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		tpdCover.setCovercode("TPD");
		tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
		tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());

		if(tpdExUnit!=null && tpdExUnit!=""){

			tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		}
		if(eapplyInput.getTpdLoading()!=null)
			tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost() != null){
			tpdCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost()));
		}
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
		tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());			
		}else{
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt())).intValue());			
		}
		tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
				tpdCover.setAddunitind("0");
			}
		}
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
		/*if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);
		}*/	

		if(ipExUnit!= null && ipExUnit !=""){
			ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		}


		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
		ipCover.setCovercode("IP");
		ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
		ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
		if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
			ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
		if(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()!= null){
			ipCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()));
		}
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
		ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
		ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue())).intValue());			
		}else{
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt())).intValue());			
		}
		ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
		
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
				ipCover.setAddunitind("0");
			}
		}
		if(eapplyInput.getExistingIPAmount() != null && eapplyInput.getExistingIPAmount() !=""){
			ipCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingIPAmount()));
		}
		
		if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
		    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());

		/*if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){Death
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){TPD
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")){IP
					if(coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
					}
				}
				
			}
		}*/
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		/*Cover object mapping end*/
		log.info("Finish non member cover finish");
		return coverList;
}	        		 	 
private List<Cover> createTransferCvCovers(EapplyInput eapplyInput) {
	    log.info("Create transfer cover start");
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		String ipExistingAmt = null;
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<>();
		
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
		deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
		deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
		/*deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());*/

		if(deathExUnit!= null && deathExUnit !=""){
			deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		}

		/*if(eapplyInput.getDeathLoading()!=null)
		deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));*/
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		//Changes for Hostplus Transfer cover unitized
		//deathCover.setCovercategory(MetlifeInstitutionalConstants.DC_FIXED);
		if(null!=eapplyInput && (QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()) || QuoteConstants.PARTNER_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())))
		{
			deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType());
		}
		else
		{
		deathCover.setCovercategory(MetlifeInstitutionalConstants.DC_FIXED);
		}
		
		/*deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());*/
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathTransferWeeklyCost());
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		else if(null!= eapplyInput && null!= eapplyInput.getTransferDeathExistingAmt())
		{
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getTransferDeathExistingAmt()));
			eapplyInput.setExistingDeathAmt(eapplyInput.getTransferDeathExistingAmt());
		}
		else{						
			eapplyInput.setExistingDeathAmt(""+(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt())).subtract(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt()));			
		}
		deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt()!= null){
			deathCover.setTransfercoveramount(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt());	
		}
		/*log.info("Death transfer amt>> {}",eapplyInput.getAddnlDeathCoverDetails().getDeathTransferAmt());*/
		/*if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
				deathCover.setAddunitind("0");
			}
		}*/
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt()));
		if(eapplyInput.getAddnlDeathCoverDetails()!=null && eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt()!=null){
			eapplyInput.getAddnlDeathCoverDetails().setDeathFixedAmt(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCovrAmt());
		}		
		/*log.info("Death trans amt>> {}",eapplyInput.getAddnlDeathCoverDetails().getDeathTransferWeeklyCost());
		log.info("Death Addnl amt>> {}",deathCover.getAdditionalcoveramount());
		log.info("existing death>> {}",eapplyInput.getExistingDeathAmt());*/
		
		
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		tpdCover.setCovercode("TPD");
		tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
	    /*tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());*/

		if(tpdExUnit!= null && tpdExUnit !=""){

			tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		}
		/*if(eapplyInput.getTpdLoading()!=null)
		tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));*/
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		//Changes for Hostplus Transfer cover unitized
		//tpdCover.setCovercategory(MetlifeInstitutionalConstants.TPDFIXED);
		if(null!=eapplyInput && (QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()) || QuoteConstants.PARTNER_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())))
		{
			tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType());
		}
		else
		{
		tpdCover.setCovercategory(MetlifeInstitutionalConstants.TPDFIXED);
		}
	    /*tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());*/
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdTransferWeeklyCost());
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		else if(null!= eapplyInput && null!= eapplyInput.getTransferTpdExistingAmt())
		{
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getTransferTpdExistingAmt()));
			eapplyInput.setExistingTpdAmt(eapplyInput.getTransferTpdExistingAmt());
		}
		else{						
			eapplyInput.setExistingTpdAmt(""+(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt())).subtract(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt()));			
		}
		tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt() != null){
			tpdCover.setTransfercoveramount(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt());
		}
		log.info("TPD transfer amt>> {}",eapplyInput.getAddnlTpdCoverDetails().getTpdTransferAmt());
		/*if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
				tpdCover.setAddunitind("0");
		}
		}*/
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt()));
		if(eapplyInput.getAddnlTpdCoverDetails()!=null && eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt()!=null){
			eapplyInput.getAddnlTpdCoverDetails().setTpdFixedAmt(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCovrAmt());
		}
		if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);

		}
		if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && null!= ipExistingAmt)
		{
			eapplyInput.setExistingIPUnits(ipExUnit);
		}
		
		if(ipExUnit!=null && ipExUnit!=""){

			ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		}
		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
		ipCover.setCovercode("IP");
		ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
		/*ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
		if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
		ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));*/
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCovercategory(MetlifeInstitutionalConstants.IP_FIXED);
		
	    /*ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());*/
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpTransferWeeklyCost());
		ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt()!= null){
			ipCover.setTransfercoveramount(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt());
		}
		log.info("IP transfer amt>> {}",eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt());
		/*if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
				ipCover.setAddunitind("0");
			}
		}*/
		if(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()!= null){
			ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()));
		}
		if(eapplyInput.getAddnlTpdCoverDetails()!=null && eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt()!=null){
			eapplyInput.getAddnlIpCoverDetails().setIpFixedAmt(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt());
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getAddnlTransferBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getAddnlTransferWaitingPeriod());
		ipCover.setTotalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getTotalipwaitingperiod());
		ipCover.setTotalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getTotalipbenefitperiod());
		
		if("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
		{
			ipCover.setCovercategory(MetlifeInstitutionalConstants.IP_UNITISED);
			ipCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlIpCoverDetails().getIpAddnlUnits()));
		}
		
		//eapplyInput.setExistingIPAmount(""+(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpTransferCovrAmt())).subtract(eapplyInput.getAddnlIpCoverDetails().getIpTransferAmt()));			
		
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
						&& coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
				
      }
}
		//Added for Units inclusion in transfer cover
if(((QuoteConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType())) 
		 || (QuoteConstants.TPD_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()))) && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()))
		{
			deathCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferUnits()));
			tpdCover.setAdditionalunit(Integer.valueOf(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferUnits()));
		}

// Added for Units in transfer for VICS
if(null!=eapplyInput.getPartnerCode() && QuoteConstants.PARTNER_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
{
	if(QuoteConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType()))
	{
		deathCover.setAdditionalunit(Integer.valueOf(deathExUnit));
		deathCover.setAddunitind("1");
	}
	if(QuoteConstants.TPD_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()))
	{
		tpdCover.setAdditionalunit(Integer.valueOf(tpdExUnit));
		tpdCover.setAddunitind("1");
	}
	
}
		
coverList.add(deathCover);
coverList.add(tpdCover);
coverList.add(ipCover);
/*Cover object mapping end*/
log.info("Create transfer cover finish");
 return coverList;
}

private List<Cover> createUpdateWorkRatingCvCovers(EapplyInput eapplyInput) {
	log.info("Create update workrating start");
	String deathExUnit = "0";
	String tpdExUnit = "0";
	String ipExUnit = "0";
	String ipExistingAmt = null;
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			CoverJSON coverJSON = (CoverJSON) iterator.next();
			if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
				deathExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
				tpdExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
				ipExUnit = coverJSON.getUnits();
				ipExistingAmt=coverJSON.getAmount();
			}
			
		}
	}
	
	/*Cover object mapping start*/        	
	
	Cover deathCover=new Cover();
	Cover tpdCover=new Cover();
	Cover ipCover=new Cover();
	List<Cover> coverList=new ArrayList<>();
	if(eapplyInput.getDeathAmt()!=null){
		eapplyInput.setExistingDeathAmt(eapplyInput.getDeathAmt());
		if(eapplyInput.getAddnlDeathCoverDetails()!=null){
			eapplyInput.getAddnlDeathCoverDetails().setDeathFixedAmt(eapplyInput.getDeathNewAmt());
		}		
	}
	deathCover.setCreatedate(new Timestamp(new Date().getTime()));
	deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
	deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
	deathCover.setCoverdecision(eapplyInput.getDeathDecision());
	deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
	if(deathExUnit!= null && deathExUnit !=""){
		deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		deathCover.setAdditionalunit(Integer.valueOf(deathExUnit));
	}
	deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
	/*deathCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);*/
    /*deathCover.setAdditionalcoveramount(BigDecimal.ZERO);*/
	deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	deathCover.setCovercategory(eapplyInput.getDeathCoverType());
	if(eapplyInput.getDeathCoverType()!=null){
		if(eapplyInput.getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
			deathCover.setAddunitind("1");
		}else if(eapplyInput.getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
			deathCover.setAddunitind("0");
		}
	}
	deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
	if(eapplyInput.getExistingDeathAmt()!=null){
		deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
	}
	if(eapplyInput.getDeathNewAmt() != null){
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getDeathNewAmt()));
	}
	
	if(eapplyInput.getTpdNewAmt()!=null){
		eapplyInput.setExistingTpdAmt(eapplyInput.getTpdNewAmt());
		if(eapplyInput.getAddnlTpdCoverDetails()!=null){
			eapplyInput.getAddnlTpdCoverDetails().setTpdFixedAmt(eapplyInput.getTpdNewAmt());
		}		
	}	
	tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
	tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	tpdCover.setCovercode("TPD");
	tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
	tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());

	if(tpdExUnit!=null && tpdExUnit!=""){
		tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		tpdCover.setAdditionalunit(Integer.valueOf(tpdExUnit));
	}
	
	tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
	/*tpdCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);*/
    /*tpdCover.setAdditionalcoveramount(BigDecimal.ZERO);*/
	tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	tpdCover.setCovercategory(eapplyInput.getTpdCoverType());
	if(eapplyInput.getTpdCoverType()!=null){
		if(eapplyInput.getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
			tpdCover.setAddunitind("1");
		}else if(eapplyInput.getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
			tpdCover.setAddunitind("0");
		}
	}
	tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
	if(eapplyInput.getExistingTpdAmt()!=null){
		tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
	}
	if(eapplyInput.getTpdNewAmt()!=null){
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getTpdNewAmt()));
	}
	if(ipExistingAmt!=null){
		ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
		eapplyInput.setExistingIPAmount(ipExistingAmt);
		ipCover.setAdditionalcoveramount(new BigDecimal (eapplyInput.getExistingIPAmount()));
	}	
		            
	if(ipExUnit!=null && ipExUnit!=""){
		ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		ipCover.setAdditionalunit(Integer.valueOf(ipExUnit));
	}
	
	if(eapplyInput.getIpNewAmt()!=null){
		eapplyInput.setExistingIPAmount(eapplyInput.getIpNewAmt());
		if(eapplyInput.getAddnlIpCoverDetails()!=null){
			eapplyInput.getAddnlIpCoverDetails().setIpFixedAmt(eapplyInput.getIpNewAmt());
		}		
	}
	
	ipCover.setCreatedate(new Timestamp(new Date().getTime()));
	ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
	ipCover.setCovercode("IP");
	ipCover.setCoverdecision(eapplyInput.getIpDecision());
	ipCover.setCoverreason(eapplyInput.getIpAuraResons());
	ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
	/*ipCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);*/
    /*ipCover.setAdditionalcoveramount(BigDecimal.ZERO);*/
	ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	ipCover.setCovercategory(eapplyInput.getIpCoverType());
	if(eapplyInput.getIpCoverType()!=null){
		if(eapplyInput.getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
			ipCover.setAddunitind("1");
		}else if(eapplyInput.getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
			ipCover.setAddunitind("0");
		}
	}
	ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
	if(eapplyInput.getIpNewAmt()!= null){
		ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getIpNewAmt()));
	}
	
	/*if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
	 ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
	}*/
	if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getAge() > 64)
	{
		ipCover.setAdditionalcoveramount(new BigDecimal (eapplyInput.getIpNewAmt()));
		ipCover.setAddunitind("0");
	}
	
	
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
			CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
			if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
				if(coverJSON.getAmount()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
				
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
				if(coverJSON.getAmount()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
					&& coverJSON.getAmount()!=null){
					ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
					ipCover.setAdditionalipbenefitperiod(coverJSON.getBenefitPeriod());
					ipCover.setAdditionalipwaitingperiod(coverJSON.getWaitingPeriod());
			}
			
		}
	}
	
	if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
	{
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getWaitingPeriod());
	}
	
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	log.info("Create update workrating finish");
	return coverList;
}
private List<Cover> createLifeEventCovers(EapplyInput eapplyInput) {
	log.info("Create lifeevent start");
	String deathExUnit = "0";
	String tpdExUnit = "0";
	String ipExUnit = "0";
	String ipExistingAmt = null;
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			CoverJSON coverJSON = (CoverJSON) iterator.next();
			if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
				deathExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
				tpdExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
				ipExUnit = coverJSON.getUnits();
				ipExistingAmt=coverJSON.getAmount();
			}
			
		}
	}
	
	/*Cover object mapping start*/        	
	
	Cover deathCover=new Cover();
	Cover tpdCover=new Cover();
	Cover ipCover=new Cover();
	List<Cover> coverList=new ArrayList<>();
	
	deathCover.setCreatedate(new Timestamp(new Date().getTime()));
	deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
	deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
	deathCover.setCoverdecision(eapplyInput.getDeathDecision());
	deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
	if(deathExUnit!= null && deathExUnit !=""){
		deathCover.setFixedunit(Integer.valueOf(deathExUnit));
	}
	deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
	deathCover.setCovercategory(eapplyInput.getDeathLifeCoverType());
	deathCover.setCost(Double.toString(eapplyInput.getDeathCoverPremium()));
	if(eapplyInput.getDeathLifeCoverType() != null && eapplyInput.getDeathLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED) 
		&& eapplyInput.getDeathLifeUnits()!= null && eapplyInput.getDeathLifeUnits()!= ""){
		deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getDeathLifeUnits())).intValue());		
	}else if(eapplyInput.getDeathLifeCoverType() != null && eapplyInput.getDeathLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED) 
		&& eapplyInput.getDeathNewAmt()!= null && eapplyInput.getDeathNewAmt()!= ""){
		deathCover.setAdditionalcoveramount((new BigDecimal(eapplyInput.getDeathNewAmt())));
	}
	
	if(eapplyInput.getAddnlDeathCoverDetails()==null && eapplyInput.getDeathNewAmt()!= null && eapplyInput.getDeathNewAmt()!= ""){
		DeathAddnlCoversJSON deathAddnlCoversJSON = new DeathAddnlCoversJSON();
		deathAddnlCoversJSON.setDeathFixedAmt(eapplyInput.getDeathNewAmt());
		eapplyInput.setAddnlDeathCoverDetails(deathAddnlCoversJSON);
	}
	if(eapplyInput.getDeathAmt()!=null){
		eapplyInput.setExistingDeathAmt(eapplyInput.getDeathAmt());
	}
	if(eapplyInput.getExistingDeathAmt()!=null){
		deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
	}
	deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getDeathLifeCoverType()!=null){
		if(eapplyInput.getDeathLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
			deathCover.setAddunitind("1");
		}else if(eapplyInput.getDeathLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
			deathCover.setAddunitind("0");
		}
	}
	deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getDeathNewAmt()));
	
	if(eapplyInput.getTpdAmt()!=null){
		eapplyInput.setExistingTpdAmt(eapplyInput.getTpdAmt());
	}
	tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
	tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	tpdCover.setCovercode("TPD");
	tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
	tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
	if(tpdExUnit!= null && tpdExUnit !=""){
		tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
	}
	tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
	tpdCover.setCovercategory(eapplyInput.getTpdLifeCoverType());
	tpdCover.setCost(Double.toString(eapplyInput.getTpdCoverPremium()));
	if(eapplyInput.getExistingTpdAmt()!=null){
		tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
	}
	if(eapplyInput.getTpdLifeCoverType() != null && eapplyInput.getTpdLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)
			&& eapplyInput.getTpdLifeUnits() != null && eapplyInput.getTpdLifeUnits()!= ""){
		tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getTpdLifeUnits())).intValue());		
	}else if(eapplyInput.getTpdLifeCoverType() != null && eapplyInput.getTpdLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)
			&& eapplyInput.getTpdNewAmt()!= null && eapplyInput.getTpdNewAmt() != ""){
		tpdCover.setAdditionalcoveramount((new BigDecimal(eapplyInput.getTpdNewAmt())));		
	}
	if(eapplyInput.getAddnlTpdCoverDetails()==null && eapplyInput.getTpdNewAmt()!= null && eapplyInput.getTpdNewAmt()!= ""){
		TpdAddnlCoversJSON tpdAddnlCoversJSON = new TpdAddnlCoversJSON();
		tpdAddnlCoversJSON.setTpdFixedAmt(eapplyInput.getTpdNewAmt());
		eapplyInput.setAddnlTpdCoverDetails(tpdAddnlCoversJSON);
	}
	tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getTpdLifeCoverType()!=null){
		if(eapplyInput.getTpdLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
			tpdCover.setAddunitind("1");
		}else if(eapplyInput.getTpdLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
			tpdCover.setAddunitind("0");
		}
	}
	tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getTpdNewAmt()));
	
	if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
	{
		tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());
		deathCover.setCoveramount(eapplyInput.getAddnlDeathCoverDetails().getTotalDeathUnits());
		deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());
		tpdCover.setCoveramount(eapplyInput.getAddnlTpdCoverDetails().getTotalTPDUnits());
		
	}
	
	if(ipExistingAmt!=null){
		ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
		eapplyInput.setExistingIPAmount(ipExistingAmt);
	}		
	if(ipExUnit != null && ipExUnit !=""){
		ipCover.setFixedunit(Integer.valueOf(ipExUnit));
	}
	if(eapplyInput.getIpAmt()!=null){
		eapplyInput.setExistingIPAmount(eapplyInput.getIpAmt());
	}
	ipCover.setCreatedate(new Timestamp(new Date().getTime()));
	ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
	ipCover.setCovercode("IP");
	ipCover.setCoverdecision(eapplyInput.getIpDecision());
	ipCover.setCoverreason(eapplyInput.getIpAuraResons());
	ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
	ipCover.setCovercategory(eapplyInput.getIpLifeCoverType());
	ipCover.setCost(Double.toString(eapplyInput.getIpCoverPremium()));
	ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getIpLifeCoverType() != null && eapplyInput.getIpLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED) && eapplyInput.getIpNewAmt()!= null &&  eapplyInput.getIpNewAmt()!= "" ){
		ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getIpNewAmt())).intValue());		
	}
	else if("SFPS".equalsIgnoreCase(eapplyInput.getPartnerCode()) && null!= eapplyInput.getIpLifeCoverType() && MetlifeInstitutionalConstants.IP_UNITISED.equalsIgnoreCase(eapplyInput.getIpLifeCoverType())
			&& null!=eapplyInput.getIpLifeUnits() && eapplyInput.getIpLifeUnits()!="")
	{
		ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getIpLifeUnits())).intValue());		
	}
	ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
	
	if(eapplyInput.getIpLifeCoverType()!=null){
		if(eapplyInput.getIpLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
			ipCover.setAddunitind("1");
		}else if(eapplyInput.getIpLifeCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
			ipCover.setAddunitind("0");
		}
	}	
	if(eapplyInput.getIpNewAmt()!= null){
	    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getIpNewAmt()));
	}
	
	if(eapplyInput.getAddnlIpCoverDetails()==null && eapplyInput.getIpNewAmt()!= null && eapplyInput.getIpNewAmt()!= ""){
		IpAddnlCoversJSON ipAddnlCoversJSON = new IpAddnlCoversJSON();
		ipAddnlCoversJSON.setIpFixedAmt(eapplyInput.getIpNewAmt());
		ipAddnlCoversJSON.setBenefitPeriod(eapplyInput.getBenefitPeriod());
		ipAddnlCoversJSON.setWaitingPeriod(eapplyInput.getWaitingPeriod());
		if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
		{
			ipAddnlCoversJSON.setIpAddnlUnits(ipExUnit);
			ipCover.setAdditionalunit(Integer.valueOf(ipExUnit));
		}
		eapplyInput.setAddnlIpCoverDetails(ipAddnlCoversJSON);
	}
	
	ipCover.setAdditionalipbenefitperiod(eapplyInput.getBenefitPeriod());
	ipCover.setAdditionalipwaitingperiod(eapplyInput.getWaitingPeriod());

	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
			CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
			if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
				if(coverJSON.getAmount()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
				
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
				if(coverJSON.getAmount()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
					&& coverJSON.getAmount()!=null){
					ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
			}
			
		}
	}
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	log.info("Create lifeevent finish");
	return coverList;
}

private List<Cover> createSpecialCvCovers(EapplyInput eapplyInput) {
	   log.info("Create special cover start");
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		String ipExistingAmt = null;
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<>();
		
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
		deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
		deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
		deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());
		if(deathExUnit!= null && deathExUnit !=""){
			deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		}
		if(eapplyInput.getDeathLoading()!=null){
		 deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
		}
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost() != null){
			deathCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost()));
		}
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
		deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){			
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());
		}else{			
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt())).intValue());
		}
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
				deathCover.setAddunitind("0");
			}
		}
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
		
		
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		tpdCover.setCovercode("TPD");
		tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
		tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());
		if(tpdExUnit!=null && tpdExUnit !=""){
			tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		}
		if(eapplyInput.getTpdLoading()!=null)
			tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost() != null){
			tpdCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost()));
		}
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
		tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){			
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());
		}else{			
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt())).intValue());
		}
		tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
				tpdCover.setAddunitind("0");
			}
		}
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
		if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);
		}	
		if(ipExUnit!=null && ipExUnit!=""){
			ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		}
		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
		ipCover.setCovercode("IP");
		ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
		ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
		if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
			ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
		if(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()!= null){
			ipCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()));
		}
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
		ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
		ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){			
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue())).intValue());
		}else{			
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt())).intValue());
		}
		ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
		
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
				ipCover.setAddunitind("0");
			}
		}	
		if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
		    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());

		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
						&& coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
				
			}
		}
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		/*Cover object mapping end*/
		log.info("Create special cover finish");
		return coverList;
}

private List<EapplyInput> convertToEapplyInput(Eapplication eapplication) {
	   log.info("Convert to eapply input start");
		EapplyInput eapplyInput = new EapplyInput();
		List<EapplyInput> eapplyInputList = new ArrayList<>(); 
		Applicant applicantObj;
		Contactinfo contactinfo;
		List<Applicant> applicantList = eapplication.getApplicant();
		PersonalDetailsJSON personalDetails = new PersonalDetailsJSON();
		OccupationCoversJSON occupationDetails = new OccupationCoversJSON();
		ContactDetailsJSON contactDetails = new ContactDetailsJSON();
		AddressJSON address = new AddressJSON();
		/*Commented as per Sonar Fix*/
		/*ExistingCoversJSON existingCovers = new ExistingCoversJSON();*/
		DeathAddnlCoversJSON addnlDeathCoverDetails = new DeathAddnlCoversJSON();
		TpdAddnlCoversJSON addnlTpdCoverDetails = new TpdAddnlCoversJSON();
		IpAddnlCoversJSON addnlIpCoverDetails = new IpAddnlCoversJSON();
		/*Calendar dob;*/
		DateFormat df1;		
		if (eapplication != null) {
			
			/*Eapplication object mapping start*/
			if("true".equalsIgnoreCase(eapplication.getGenconsentindicator())){					
				eapplyInput.setAckCheck(Boolean.TRUE);
			}
			if("true".equalsIgnoreCase(eapplication.getDisclosureindicator())){				
				eapplyInput.setDodCheck(Boolean.TRUE);
			}
			if("true".equalsIgnoreCase(eapplication.getPrivacystatindicator())){				
				eapplyInput.setPrivacyCheck(Boolean.TRUE);
			}
			if("true".equalsIgnoreCase(eapplication.getFulstatindicator())){				
				eapplyInput.setFulCheck(Boolean.TRUE);
			}
			if("true".equalsIgnoreCase(eapplication.getNuwflowind())){				
				eapplyInput.setIpDisclaimer(Boolean.TRUE);
			}
			
			/** vicsuper own occupation changes made by purna - starts **/
			if (MetlifeInstitutionalConstants.Y.equalsIgnoreCase(eapplication.getOccupgrade())){
				eapplyInput.setOwnOccuptionTpd(true);
			} else {
				eapplyInput.setOwnOccuptionTpd(false);
			}
			if(MetlifeInstitutionalConstants.Y.equalsIgnoreCase(eapplication.getMarketOptOut()))
			{
				eapplyInput.setOwnOccuptionDeath(true);
			}
			else
			{
				eapplyInput.setOwnOccuptionDeath(false);
			}
			
			if (MetlifeInstitutionalConstants.TRUE.equalsIgnoreCase(eapplication.getEmailcopyindicator())){
				eapplyInput.setOwnOccuptionIp(MetlifeInstitutionalConstants.TRUE);
			} else {
				eapplyInput.setOwnOccuptionIp(MetlifeInstitutionalConstants.FALSE);
			}
			/** vicsuper own occupation changes made by purna - ends **/
			//Added for change cover ipDisclaimer flag
			if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplication.getPartnercode()) && MetlifeInstitutionalConstants.Y.equalsIgnoreCase(eapplication.getNuwflowind()))
			{
				eapplyInput.setIpDisclaimer(Boolean.TRUE);
			}
			
			if(eapplication.getTotalMonthlyPremium()!= null){
				eapplyInput.setTotalPremium(eapplication.getTotalMonthlyPremium());
			}
		
			eapplyInput.setOverallDecision(eapplication.getAppdecision());
			eapplyInput.setLastSavedOn(eapplication.getLastsavedon());
			if (eapplication.getApplicationumber() != null) {
				eapplyInput.setAppNum(Long.parseLong(eapplication.getApplicationumber()));
			}
			if(eapplication.getUnderwritingpolicy()!= null && eapplication.getUnderwritingpolicy().equalsIgnoreCase("yes")){
				eapplyInput.setAuraDisabled(MetlifeInstitutionalConstants.FALSE);
			}else if("AuraPage".equalsIgnoreCase(eapplication.getLastsavedon())){
				eapplyInput.setAuraDisabled(MetlifeInstitutionalConstants.FALSE);
			}
			else{
				eapplyInput.setAuraDisabled("true");
			}
			if("CORP".equalsIgnoreCase(eapplication.getPartnercode())
					&& null!=eapplication.getAuradetail().getAurasessionid()) {
					eapplyInput.setAuraSessionId(eapplication.getAuradetail().getAurasessionid());
			}
			eapplyInput.setManageType(eapplication.getRequesttype());
			eapplyInput.setPartnerCode(eapplication.getPartnercode());
			
			
			/*Eapplication object mapping End */
			if (applicantList != null) {
				for (int appItr = 0; appItr < applicantList.size(); appItr++) {
					applicantObj =  applicantList.get(appItr);
					/*Applicant object mapping start*/
					if (applicantObj != null) {
						if(applicantObj.getTitle()!= null && applicantObj.getTitle() !=""){
							personalDetails.setTitle(applicantObj.getTitle());
							eapplyInput.setTitle(applicantObj.getTitle());
						}
						if(applicantObj.getClientrefnumber()!=null && "CORP".equalsIgnoreCase(eapplication.getPartnercode())) {
							eapplyInput.setClientRefNumber(applicantObj.getClientrefnumber());
							contactDetails.setFundEmailAddress(applicantObj.getCustomerreferencenumber());
						}
						if(applicantObj.getFirstname()!= null && applicantObj.getFirstname() !=""){
							personalDetails.setFirstName(applicantObj.getFirstname());
							eapplyInput.setFirstName(applicantObj.getFirstname());
						}
						if(applicantObj.getLastname() != null && applicantObj.getLastname() !=""){
							personalDetails.setLastName(applicantObj.getLastname());
							eapplyInput.setLastName(applicantObj.getLastname());
						}
						
						if(applicantObj.getBirthdate() != null){
							/*commented as per sonar fix*/
							/*dob = Calendar.getInstance();*/
		            		df1 = new SimpleDateFormat(MetlifeInstitutionalConstants.DATE_FORMAT);		            		 		
							eapplyInput.setDob(df1.format(applicantObj.getBirthdate()));
						}
						if(applicantObj.getSmoker() != null && applicantObj.getSmoker() !=""){
							if(applicantObj.getSmoker().equalsIgnoreCase("1")){
								personalDetails.setSmoker("Yes");
								eapplyInput.setSmoker("Yes");
							}else if(applicantObj.getSmoker().equalsIgnoreCase("2")){
								personalDetails.setSmoker("No");
								eapplyInput.setSmoker("No");
							}
						}
						
						if(applicantObj.getDeathindexationflag()!= null && applicantObj.getDeathindexationflag().equalsIgnoreCase("1") ){
							eapplyInput.setIndexationDeath("true");
						}else{
							eapplyInput.setIndexationDeath(MetlifeInstitutionalConstants.FALSE);
						}
						if(applicantObj.getTpdindexationflag()!= null && applicantObj.getTpdindexationflag().equalsIgnoreCase("1") ){
							eapplyInput.setIndexationTpd("true");
						}else{
							eapplyInput.setIndexationTpd(MetlifeInstitutionalConstants.FALSE);
						}
						
						if(applicantObj.getUnitcheckflag()!= null && applicantObj.getUnitcheckflag().equalsIgnoreCase("1") ){
							eapplyInput.setUnitisedCovers("true");
						}else{
							eapplyInput.setUnitisedCovers(MetlifeInstitutionalConstants.FALSE);
						}
						
						if(applicantObj.getInsuredsalary()!= null && applicantObj.getInsuredsalary().equalsIgnoreCase("Y") ){
							eapplyInput.setIpcheckbox("true");
						}else{
							eapplyInput.setIpcheckbox(MetlifeInstitutionalConstants.FALSE);
						}
						
						/*For Transfer, previous cover section*/
			        	if(applicantObj.getPreviousinsurername()!= null){
			        		eapplyInput.setPreviousFundName(applicantObj.getPreviousinsurername());
			        	}
			        	if(applicantObj.getFundmempolicynumber() != null){
			        		eapplyInput.setMembershipNumber(applicantObj.getFundmempolicynumber());
			        	}
			        	if(applicantObj.getSpinnumber() != null){
			        		eapplyInput.setSpinNumber(applicantObj.getSpinnumber());
			        	}
			        	if(applicantObj.getPrevioustpdclaimque()!= null){
			        		eapplyInput.setPreviousTpdClaimQue(applicantObj.getPrevioustpdclaimque());
			        	}
			        	/*For Transfer, previous cover section*/
			        	
			        	/*for transfer and new member */
						if(applicantObj.getPreviousterminalillque()!= null){
							eapplyInput.setTerminalIllClaimQue(applicantObj.getPreviousterminalillque());
						}
						if(applicantObj.getDocumentevidence()!= null){
							eapplyInput.setDocumentName(applicantObj.getDocumentevidence());
						}
						/*for transfer and new member */
						
						occupationDetails.setGender(applicantObj.getGender());
						personalDetails.setGender(applicantObj.getGender());
						occupationDetails.setOccupation(applicantObj.getOccupation());						
						/*169682: Added code for Other occupation*/
						occupationDetails.setOtherOccupation(applicantObj.getOtheroccupation());                       
                        eapplyInput.setEmail(applicantObj.getEmailid());                        
                        eapplyInput.setAge((new BigDecimal(applicantObj.getAge())).intValue());
                        occupationDetails.setSalary(applicantObj.getAnnualsalary());
                        eapplyInput.setDateJoined(applicantObj.getDatejoinedfund());
                        eapplyInput.setOverallDecision(applicantObj.getOverallcoverdecision());
                        eapplyInput.setMemberType(applicantObj.getMembertype());
                        if(eapplication.getProductnumber()!=null && "CORP".equalsIgnoreCase(eapplication.getPartnercode())) {
                        	eapplyInput.setProductnumber(eapplication.getProductnumber());
                        }
                        /**VicSuper Code Changes Starts - Purna**/
                        //if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) && applicantObj.getIndustrytype()!= null && applicantObj.getOccupationcode()!= null && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())){
                            occupationDetails.setOccRating1a(eapplication.getMagazineoffer());
                            occupationDetails.setOccRating1b(eapplication.getAuthorizationindicator());
                       // }
                        //else {
                            occupationDetails.setIndustryCode(applicantObj.getIndustrytype());
                           occupationDetails.setIndustryName(applicantObj.getOccupationcode());
                      //  }

                        /**VicSuper Code Changes Ends - Purna**/
                      
                        occupationDetails.setFifteenHr(applicantObj.getWorkhours());
                        
                        if(applicantObj != null && applicantObj.getAustraliacitizenship()!= null){
                        	 occupationDetails.setCitizenQue(applicantObj.getAustraliacitizenship());
                        } 
                        
                        // special cover
                        if(applicantObj != null && applicantObj.getPreviostpdbenefit()!= null){
                       	 	eapplyInput.setPreviousTpdBenefit(applicantObj.getPreviostpdbenefit());
                        }
                        
                        // own business questions for Change and Cancel cover
			        	 if(applicantObj != null && applicantObj.getOwnbusinessque() != null){
			        		 occupationDetails.setOwnBussinessQues(applicantObj.getOwnbusinessque());
			        	 }
			        	 if(applicantObj != null && applicantObj.getOwnbusinessyesque() != null){
			        		 occupationDetails.setOwnBussinessYesQues(applicantObj.getOwnbusinessyesque()); 
			        	 }
			        	 if(applicantObj != null && applicantObj.getOwnbusinesnosque() != null){
			        		 occupationDetails.setOwnBussinessNoQues(applicantObj.getOwnbusinesnosque()); 
			        	 }
			        	 
			        	// for Statewide
			        	 if(applicantObj != null && applicantObj.getSmoker() != null){
			        		 occupationDetails.setSmokerQuestion(applicantObj.getSmoker());
			        	 }	
			        	 if(applicantObj != null && applicantObj.getPermanentemplque() != null){
			        		 occupationDetails.setPermanentlyEmployedQuestion(applicantObj.getPermanentemplque());
			        	 }
			        	 if(applicantObj != null && applicantObj.getThirtyfivehrswrkque() != null){
			        		 occupationDetails.setHoursPerWeekQuestion(applicantObj.getThirtyfivehrswrkque());
			        	 }
			        	 
			        	 
			        	 
                        // Occupation 
                        if(applicantObj != null && applicantObj.getWorkduties()!= null){
                        	occupationDetails.setHazardousQue(applicantObj.getWorkduties());
                        }
                        if(applicantObj != null && applicantObj.getOccupationduties()!= null){
                        	occupationDetails.setWithinOfficeQue(applicantObj.getOccupationduties());
                        }
                        if(applicantObj != null && applicantObj.getSpendtimeoutside()!= null){
                        	occupationDetails.setManagementRoleQue(applicantObj.getSpendtimeoutside());
                        }
                        /**VicSuper Code Changes Starts - Purna**/
                        if(applicantObj != null && applicantObj.getTertiaryque()!= null){
                        	 if ((MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType())
    			        			 || MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())
    			        			 || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType())
    			        			 || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplyInput.getManageType())
    			        			 || MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplyInput.getManageType())) 
    			        			 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
                        		 occupationDetails.setOccRating2(applicantObj.getTertiaryque()); 
                        	 } else {
                        		 occupationDetails.setTertiaryQue(applicantObj.getTertiaryque());
                        	 }
                        }
                        /**VicSuper Code Changes Ends - Purna**/
                        eapplyInput.setContactType(applicantObj.getContacttype());
                        eapplyInput.setContactPhone(applicantObj.getContactnumber());
                        eapplyInput.setPersonalDetails(personalDetails);
                        eapplyInput.setOccupationDetails(occupationDetails);
                        /*Applicant object mapping End*/
					}
					
					/*Cover object mapping start*/ 
					if(eapplication.getRequesttype()!= null){
						if(eapplication.getRequesttype().equalsIgnoreCase("CCOVER")){
							eapplyInput = createChangeCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase(MetlifeInstitutionalConstants.TCOVER)){
							eapplyInput = createTransferCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase(MetlifeInstitutionalConstants.UWCOVER)){
							eapplyInput = createWorkRatingCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);	
						}else if(eapplication.getRequesttype().equalsIgnoreCase("NCOVER")){
							eapplyInput = createNewMemberCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);		
						}else if(eapplication.getRequesttype().equalsIgnoreCase(MetlifeInstitutionalConstants.CANCOVER)){
							eapplyInput = createCancelCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase(MetlifeInstitutionalConstants.ICOVER)){
							eapplyInput = createLifeEventCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase(MetlifeInstitutionalConstants.SCOVER)){
							eapplyInput = createSpecialCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);
						}else if(eapplication.getRequesttype().equalsIgnoreCase(MetlifeInstitutionalConstants.NMCOVER)){
							eapplyInput = createNonMemberCvrRetrieve(eapplyInput, applicantObj, addnlDeathCoverDetails, addnlTpdCoverDetails,
									addnlIpCoverDetails);
						}
					}
					
					
					/*Cover object mapping End*/ 
					
					if(null!= applicantObj.getContactinfo()){
						contactinfo = applicantObj.getContactinfo();
						contactDetails.setMobilePhone(contactinfo.getMobilephone());
						contactDetails.setHomePhone(contactinfo.getHomephone());
						contactDetails.setWorkPhone(contactinfo.getWorkphone());
						eapplyInput.setContactPhone(contactinfo.getPreferedcontactnumber());
						eapplyInput.setContactPrefTime(contactinfo.getPreferedcontacttime());
						address.setLine1(contactinfo.getAddressline1());
						address.setLine2(contactinfo.getAddressline2());
						address.setCountry(contactinfo.getCountry());
						address.setPostCode(contactinfo.getPostcode());
						address.setState(contactinfo.getState());
						address.setSuburb(contactinfo.getSuburb());
						eapplyInput.setOtherContactType(contactinfo.getOthercontacttype());
						eapplyInput.setOtherContactPhone(contactinfo.getOthercontactnumber());
						eapplyInput.setContactDetails(contactDetails);
						eapplyInput.setAddress(address);
						if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
							eapplyInput.setContactType(contactinfo.getPreferedcontacttype());
						}
					}
					
					
				/*lifeevent mapping */
		        	
		        	if(null!=applicantObj && null!=applicantObj.getLifeevent() && !"".equalsIgnoreCase(applicantObj.getLifeevent())){
		        		eapplyInput.setEventName(applicantObj.getLifeevent());
		        		
		        	}
		        	if(null!=applicantObj && null!=applicantObj.getLifeeventdate() && !"".equalsIgnoreCase(applicantObj.getLifeeventdate())){
		        		eapplyInput.setEventDate(applicantObj.getLifeeventdate());
		        	}
		        	if(null!=applicantObj && null!=applicantObj.getEventalreadyapplied() && !"".equalsIgnoreCase(applicantObj.getEventalreadyapplied())){
		        		eapplyInput.setEventAlreadyApplied(applicantObj.getEventalreadyapplied());
		        		
		        	}
		        	if(null!= applicantObj && null !=applicantObj.getRelation() && !"".equalsIgnoreCase(applicantObj.getRelation())){
		        		eapplyInput.setRsnFrAdnlCvr(applicantObj.getRelation());
		        		
		        	}
					
				}

			}
			
			/*if(eapplication.getDocuments()!=null && eapplication.getDocuments().size()>0){*/
			if(eapplication.getDocuments()!=null && !(eapplication.getDocuments().isEmpty())){
				List<DocumentJSON> lifeEventDocuments = new ArrayList<>();
				List<DocumentJSON> transferDocuments = new ArrayList<>();
				String [] docTypes = 
					{"MARR","ANNMARR","DIVO","ANNDIVO","BRTH","FRST","CUGA","DCSS"};


				List<String> docTypeList = CollectionUtils.arrayToList(docTypes);

				
				for (Iterator iterator = eapplication.getDocuments().iterator(); iterator.hasNext();) {
					Document  document = (Document ) iterator.next();
					if(document!=null){
						DocumentJSON documentJSON = new DocumentJSON();
						documentJSON.setFileLocations(document.getDocLoc());
						 String filepath;
						if(MetlifeInstitutionalConstants.TRANSFER_DOCUMENTS.equalsIgnoreCase(document.getDocType())){
							documentJSON.setDocType(document.getDocType());							
							filepath = document.getDocLoc();
							String []name =filepath.split("/+");
							String docName = name[name.length-1];
							documentJSON.setName(docName);
							transferDocuments.add(documentJSON);
						}else if((MetlifeInstitutionalConstants.LIFE_EVENT_DOCUMANTES.equalsIgnoreCase(document.getDocType()))
								|| docTypeList.contains(document.getDocType())){
							documentJSON.setDocType(document.getDocType());
							filepath = document.getDocLoc();
							String []name =filepath.split("/+");
							String docName = name[name.length-1];
							documentJSON.setName(docName);
							lifeEventDocuments.add(documentJSON);							
						}
					}
					
				}
				/*if(lifeEventDocuments.size()>0){*/
				if(!(lifeEventDocuments.isEmpty())){
					eapplyInput.setLifeEventDocuments(lifeEventDocuments);
				}else{
					eapplyInput.setLifeEventDocuments(null);
				}
				/*if(transferDocuments.size()>0){*/
				if(!(transferDocuments.isEmpty())){
					eapplyInput.setTransferDocuments(transferDocuments);
				}else{
					eapplyInput.setTransferDocuments(null);
				}
				
			}			
			
		}
		eapplyInputList.add(eapplyInput);
		 log.info("Convert to eapply input finish");
		return eapplyInputList;
	}

private List<Cover> createNewMemberCvCovers(EapplyInput eapplyInput) {
	log.info("Create new member cover start");
	String deathExUnit = "0";
	String tpdExUnit = "0";
	String ipExUnit = "0";
	String ipExistingAmt = null;
	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
			CoverJSON coverJSON = (CoverJSON) iterator.next();
			if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
				deathExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
				tpdExUnit = coverJSON.getUnits();
			}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
				ipExUnit = coverJSON.getUnits();
				ipExistingAmt=coverJSON.getAmount();
			}
			
		}
}
	
	/*Cover object mapping start*/        	
	
	Cover deathCover=new Cover();
	Cover tpdCover=new Cover();
	Cover ipCover=new Cover();
	List<Cover> coverList=new ArrayList<>();
	
	deathCover.setCreatedate(new Timestamp(new Date().getTime()));
	deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
	deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
	deathCover.setCoverdecision(eapplyInput.getDeathDecision());
	deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
    /*deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());*/
	if(deathExUnit!=null && deathExUnit !=""){
		deathCover.setFixedunit(Integer.valueOf(deathExUnit));
	}
	/*if(eapplyInput.getDeathLoading()!=null)
    deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));*/
	deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
	deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType());
    /*deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());*/
	deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverPremium());
	if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType()!= null && eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){		
		deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getNewDeathUpdatedCover())).intValue());
	}else{		
		deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getNewDeathLabelAmt())).intValue());
	}
	if(eapplyInput.getExistingDeathAmt()!=null){
		deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
	}
	deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType()!=null){
		if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
			deathCover.setAddunitind("1");
		}else if(eapplyInput.getAddnlDeathCoverDetails().getNewDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
			deathCover.setAddunitind("0");
		}
	}
	deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getNewDeathLabelAmt()));
	
	
	tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
	tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	tpdCover.setCovercode("TPD");
	tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
	tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
	/*tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());*/
	if(tpdExUnit!=null && tpdExUnit !=""){
		tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
	}
	/*if(eapplyInput.getTpdLoading()!=null)
    tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));*/
	tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
	tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType());
   /* tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());*/
	tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverPremium());
	if(eapplyInput.getExistingTpdAmt()!=null){
		tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
	}
	if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType()!= null && eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
		tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getNewTpdUpdatedCover())).intValue());		
	}else{		
		tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getNewTpdLabelAmt())).intValue());
	}
	tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType()!=null){
		if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
			tpdCover.setAddunitind("1");
		}else if(eapplyInput.getAddnlTpdCoverDetails().getNewTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
			tpdCover.setAddunitind("0");
		}
	}
	tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getNewTpdLabelAmt()));
	if(ipExistingAmt!=null){
		ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
		eapplyInput.setExistingIPAmount(ipExistingAmt);
	}		            
	if(ipExUnit!=null && ipExUnit!=""){
		ipCover.setFixedunit(Integer.valueOf(ipExUnit));
	}
	ipCover.setCreatedate(new Timestamp(new Date().getTime()));
	ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
	ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
	ipCover.setCovercode("IP");
	ipCover.setCoverdecision(eapplyInput.getIpDecision());
	ipCover.setCoverreason(eapplyInput.getIpAuraResons());
	/*ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
    if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>0)
    ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));*/
	ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
	ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType());
	/*ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());*/
	ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getNewIpCoverPremium());
	ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
	if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType()!=null){
		if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
			ipCover.setAddunitind("1");
		}else if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
			ipCover.setAddunitind("0");
		}
	}	
	if(eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType()!= null && eapplyInput.getAddnlIpCoverDetails().getNewIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
		ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getNewIpUpdatedCover())).intValue());		
	}else{		
		ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getNewIpLabelAmt())).intValue());
	}
	if(eapplyInput.getAddnlIpCoverDetails().getNewIpLabelAmt()!= null){
	ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getNewIpLabelAmt()));
	}
	ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getNewBenefitPeriod());
	ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getNewWaitingPeriod());

	if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
		for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
			CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
			if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
				if(coverJSON.getAmount()!=null){
					deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
				
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
				if(coverJSON.getAmount()!=null){
					tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
				}
			}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
					&& (coverJSON.getAmount()!=null)){
					ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
					ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
			}
			
		}
	}
	coverList.add(deathCover);
	coverList.add(tpdCover);
	coverList.add(ipCover);
	/*Cover object mapping end*/
	log.info("Create new member cover finish");
	return coverList;
}


private EapplyInput createChangeCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("Change cover retrive start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					
					//if(coversObj.getCovercategory()!= null){
						addnlDeathCoverDetails.setDeathInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					//}
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
					}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
					}
					if(coversObj.getPolicyfee()!=null){
						addnlDeathCoverDetails.setDeathLoadingCost((coversObj.getPolicyfee().toString())); 
					}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
					}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingDeathUnits(Integer.toString(coversObj.getFixedunit()));
	            	}			
					
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	//if(coversObj.getCovercategory()!= null){
	            		addnlTpdCoverDetails.setTpdInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					//}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlTpdCoverDetails.setTpdLoadingCost((coversObj.getPolicyfee().toString())); 
					}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingTPDUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	
	            	eapplyInput.setExistingIPWaitingPeriod(coversObj.getExistingipwaitingperiod());
	            	eapplyInput.setExistingIPBenefitPeriod(coversObj.getExistingipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	//if(coversObj.getCovercategory()!= null){
	            		addnlIpCoverDetails.setIpInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					//}
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlIpCoverDetails.setIpLoadingCost(coversObj.getPolicyfee().toString());
	            	}
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingIPUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
	            	eapplyInput.setIpSalaryPercent(coversObj.getOptionalunit());
				}
			}
			
		}
		/*Added for the CI/CD Demo*/
		if(null!=addnlDeathCoverDetails){
			log.info(MetlifeInstitutionalConstants.ADDITIONAL_DEATHCOVER_DETAILS,addnlDeathCoverDetails.getDeathCoverType());
		}		
		/*Added for the CI/CD Demo*/
		
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	log.info("Change cover retrive finish");
	return eapplyInput;
}
private EapplyInput createNonMemberCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("Non member cover retrive start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					
					if(coversObj.getCovercategory()!= null){
						addnlDeathCoverDetails.setDeathInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
					}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
					}
					if(coversObj.getPolicyfee()!=null){
						addnlDeathCoverDetails.setDeathLoadingCost((coversObj.getPolicyfee().toString())); 
					}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
					}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingDeathUnits(Integer.toString(coversObj.getFixedunit()));
	            	}			
					
				}
				 if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	if(coversObj.getCovercategory()!= null){
	            		addnlTpdCoverDetails.setTpdInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlTpdCoverDetails.setTpdLoadingCost((coversObj.getPolicyfee().toString())); 
					}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingTPDUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getCovercategory()!= null){
	            		addnlIpCoverDetails.setIpInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlIpCoverDetails.setIpLoadingCost(coversObj.getLoading().toString());
	            	}
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingIPUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		/*Added for the CI/CD Demo*/
		if(null!=addnlDeathCoverDetails){
			log.info(MetlifeInstitutionalConstants.ADDITIONAL_DEATHCOVER_DETAILS,addnlDeathCoverDetails.getDeathCoverType());
		}		
		/*Added for the CI/CD Demo*/
		
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	log.info("Non member cover retrive finish");
	return eapplyInput;
}
private EapplyInput createCancelCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("Cancel cover retrive start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					
					if(coversObj.getCovercategory()!= null){
						addnlDeathCoverDetails.setDeathInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
					}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
					}
					if(coversObj.getPolicyfee()!=null){
						addnlDeathCoverDetails.setDeathLoadingCost((coversObj.getPolicyfee().toString())); 
					}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
					}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	if(coversObj.getCovercategory()!= null){
	            		addnlTpdCoverDetails.setTpdInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlTpdCoverDetails.setTpdLoadingCost((coversObj.getPolicyfee().toString())); 
					}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getCovercategory()!= null){
	            		addnlIpCoverDetails.setIpInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlIpCoverDetails.setIpLoadingCost(coversObj.getLoading().toString());
	            	}
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingIPUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		/*Added for the CI/CD Demo*/
		if(null!=addnlDeathCoverDetails){
			log.info(MetlifeInstitutionalConstants.ADDITIONAL_DEATHCOVER_DETAILS,addnlDeathCoverDetails.getDeathCoverType());
		}		
		/*Added for the CI/CD Demo*/
		
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	log.info("Cancel cover retrive finish");
	return eapplyInput;
}

private EapplyInput createTransferCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	    log.info("Transfer cover retrive start");
		Cover coversObj;
		if (applicantObj.getCovers() != null && applicantObj.getCovers().size() > ZERO_CONST) {
			for (int coverItr = 0; coverItr < applicantObj.getCovers().size(); coverItr++) {
				coversObj =  applicantObj.getCovers().get(coverItr);
				if (null != coversObj) {
					if (MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())) {
						eapplyInput.setDeathDecision(coversObj.getCoverdecision());
						eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
						eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
						addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
						addnlDeathCoverDetails.setDeathTransferCoverType(coversObj.getCovercategory());
						addnlDeathCoverDetails.setDeathTransferWeeklyCost(coversObj.getCost());
						
						if(coversObj.getTransfercoveramount()!= null){
							addnlDeathCoverDetails.setDeathTransferAmt(coversObj.getTransfercoveramount());
						}
						
						if (coversObj.getAdditionalcoveramount() != null) {
							addnlDeathCoverDetails.setDeathTransferCovrAmt(coversObj.getAdditionalcoveramount().toString());
						}
						//Added for HostPlus Transfer cover unitized
						if(coversObj.getAdditionalunit() > 0)
						{
							addnlDeathCoverDetails.setDeathTransferUnits(String.valueOf(coversObj.getAdditionalunit()));
						}
						if(coversObj.getFixedunit() > 0)
						{
							eapplyInput.setExistingDeathUnits(String.valueOf(coversObj.getFixedunit()));
						}
						
						eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
						if (coversObj.getLoading() != null) {
							eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}
						if (coversObj.getExistingcoveramount() != null) {
							eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
						eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					}
					if ("TPD".equalsIgnoreCase(coversObj.getCovercode())) {
						eapplyInput.setTpdDecision(coversObj.getCoverdecision());
						eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
						eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
						addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
						addnlTpdCoverDetails.setTpdTransferCoverType(coversObj.getCovercategory());
						addnlTpdCoverDetails.setTpdTransferWeeklyCost(coversObj.getCost());
						
						if(coversObj.getTransfercoveramount()!=null){
							addnlTpdCoverDetails.setTpdTransferAmt(coversObj.getTransfercoveramount());
						}
						
						if (coversObj.getAdditionalcoveramount() != null) {
							addnlTpdCoverDetails.setTpdTransferCovrAmt(coversObj.getAdditionalcoveramount().toString());
						}
						//Added for HostPlus Transfer cover unitized
						if(coversObj.getAdditionalunit() > 0)
						{
							addnlTpdCoverDetails.setTpdTransferUnits(String.valueOf(coversObj.getAdditionalunit()));
						}
						if(coversObj.getFixedunit() > 0)
						{
							eapplyInput.setExistingTPDUnits(String.valueOf(coversObj.getFixedunit()));
						}
						
						eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
						if (coversObj.getLoading() != null) {
							eapplyInput.setTpdLoading(coversObj.getLoading().toString());
						}
						if (coversObj.getExistingcoveramount() != null) {
							eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
						}
						eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					}
					if ("IP".equalsIgnoreCase(coversObj.getCovercode())) {
						eapplyInput.setIpDecision(coversObj.getCoverdecision());
						eapplyInput.setIpAuraResons(coversObj.getCoverreason());
						eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
						addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
						addnlIpCoverDetails.setIpTransferCoverType(coversObj.getCovercategory());
						addnlIpCoverDetails.setIpTransferWeeklyCost(coversObj.getCost());
						
						if(coversObj.getTransfercoveramount()!= null){
							addnlIpCoverDetails.setIpTransferAmt(coversObj.getTransfercoveramount());
						}
						
						if (coversObj.getAdditionalcoveramount() != null) {
							addnlIpCoverDetails.setIpTransferCovrAmt(coversObj.getAdditionalcoveramount().toString());
						}
						if(coversObj.getAdditionalunit() > 0)
						{
						addnlIpCoverDetails.setIpAddnlUnits(String.valueOf(coversObj.getAdditionalunit()));
						}
						addnlIpCoverDetails.setAddnlTransferWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
						addnlIpCoverDetails.setAddnlTransferBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
						addnlIpCoverDetails.setTotalipwaitingperiod(coversObj.getTotalipwaitingperiod());
						addnlIpCoverDetails.setTotalipbenefitperiod(coversObj.getTotalipbenefitperiod());
						eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
						if (coversObj.getLoading() != null) {
							eapplyInput.setIpLoading(coversObj.getLoading().toString());
						}
						if (coversObj.getExistingcoveramount() != null) {
							eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
						}
						if(coversObj.getExistingipwaitingperiod()!= null){
							eapplyInput.setTransferIpWaitingPeriod(coversObj.getExistingipwaitingperiod());
						}
						if(coversObj.getExistingipbenefitperiod()!= null){
							eapplyInput.setTransferIpBenefitPeriod(coversObj.getExistingipbenefitperiod());
						}
						eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					}
				}

			}
			eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
			eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
			eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		}
		log.info("Transfer cover retrive finish");
		return eapplyInput;
	}

private EapplyInput createNewMemberCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("New member cover retrive start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					/*addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());*/
					addnlDeathCoverDetails.setNewDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setNewDeathCoverPremium(coversObj.getCost());
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setNewDeathLabelAmt(coversObj.getAdditionalcoveramount().toString());
						}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	/*addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());*/
	            	addnlTpdCoverDetails.setNewTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setNewTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setNewTpdLabelAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            		}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	/*addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());*/
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setNewIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setNewIpLabelAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	addnlIpCoverDetails.setNewWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setNewBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            		}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	log.info("New member cover retrive finish");
	return eapplyInput;
}

private EapplyInput createSpecialCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("Special cover retrive start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					
					if(coversObj.getCovercategory()!= null){
						addnlDeathCoverDetails.setDeathInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
					}
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
					}
					if(coversObj.getPolicyfee()!=null){
						addnlDeathCoverDetails.setDeathLoadingCost((coversObj.getPolicyfee().toString())); 
					}
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
					}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	if(coversObj.getCovercategory()!= null){
	            		addnlTpdCoverDetails.setTpdInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlTpdCoverDetails.setTpdLoadingCost((coversObj.getPolicyfee().toString())); 
					}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            	}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	if(coversObj.getCovercategory()!= null){
	            		addnlIpCoverDetails.setIpInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
					}
	            	if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            	}
	            	if(coversObj.getPolicyfee()!=null){
	            		addnlIpCoverDetails.setIpLoadingCost(coversObj.getLoading().toString());
	            	}
	            	if(Integer.valueOf(coversObj.getFixedunit())!= null){
	            		eapplyInput.setExistingIPUnits(Integer.toString(coversObj.getFixedunit()));
	            	}
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            	}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}

		/*Added for the CI/CD Demo*/
		if(null!=addnlDeathCoverDetails){
			log.info(MetlifeInstitutionalConstants.ADDITIONAL_DEATHCOVER_DETAILS,addnlDeathCoverDetails.getDeathCoverType());
		}		
		/*Added for the CI/CD Demo*/

	
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
	}
	
	log.info("Special cover retrive finish");
	return eapplyInput;
}


private List<Cover> createCancelCvCovers(EapplyInput eapplyInput) {
	 log.info("Create Cancel cover start");
		String deathExUnit = "0";
		String tpdExUnit = "0";
		String ipExUnit = "0";
		String ipExistingAmt = null;
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>ZERO_CONST){
			for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					deathExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					tpdExUnit = coverJSON.getUnits();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ipExUnit = coverJSON.getUnits();
					ipExistingAmt=coverJSON.getAmount();
				}
				
			}
		}
		
		/*Cover object mapping start*/        	
		
		Cover deathCover=new Cover();
		Cover tpdCover=new Cover();
		Cover ipCover=new Cover();
		List<Cover> coverList=new ArrayList<>();
		if(eapplyInput.getDeathAmt()!=null){
			eapplyInput.setExistingDeathAmt(eapplyInput.getDeathAmt());
		}
		deathCover.setCreatedate(new Timestamp(new Date().getTime()));
		deathCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		deathCover.setCovertype(MetlifeInstitutionalConstants.DEATH_CASE);
		deathCover.setCovercode(MetlifeInstitutionalConstants.DEATH);
		//deathCover.setCoverdecision(eapplyInput.getDeathDecision());
		deathCover.setCoverdecision("ACC");
		deathCover.setCoverreason(eapplyInput.getDeathAuraResons());
		deathCover.setCoverexclusion(eapplyInput.getDeathExclusions());
		if(deathExUnit!=null && deathExUnit !=""){
			deathCover.setFixedunit(Integer.valueOf(deathExUnit));
		}
		if(eapplyInput.getDeathLoading()!=null){
		 deathCover.setLoading(new BigDecimal((eapplyInput.getDeathLoading())));
		}
		
		deathCover.setOccupationrating(eapplyInput.getDeathOccCategory());
		deathCover.setCost("0");
		if(eapplyInput.getExistingDeathAmt()!=null){
			deathCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingDeathAmt()));
		}
		
		/*For Host Freq type is weekly,monthly  and yearly where as for caresuper it is weekly */
		
		if(null != eapplyInput.getFreqCostType() && !"".equalsIgnoreCase(eapplyInput.getFreqCostType())){
			deathCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		}else{
			deathCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);
		}
		
		
		if(null != eapplyInput.getAddnlDeathCoverDetails() && !"".equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().toString())){
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost() != null){
			deathCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathLoadingCost()));
		}
		
		deathCover.setCovercategory(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType());
		deathCover.setCoveroption(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverName());
		deathCover.setCost(""+eapplyInput.getAddnlDeathCoverDetails().getDeathCoverPremium());
		
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue())).intValue());			
		}else{			
			deathCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt())).intValue());
		}
		
		
		if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null){
			if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)){
				deathCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_FIXED)){
				deathCover.setAddunitind("0");
			}
		}
		deathCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt()));
		
		}
		
		if(eapplyInput.getTpdAmt()!=null){
			eapplyInput.setExistingTpdAmt(eapplyInput.getTpdAmt());
		}
		tpdCover.setCreatedate(new Timestamp(new Date().getTime()));
		tpdCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		tpdCover.setCovertype(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		tpdCover.setCovercode("TPD");
		//tpdCover.setCoverdecision(eapplyInput.getTpdDecision());
		tpdCover.setCoverdecision("ACC");
		tpdCover.setCoverreason(eapplyInput.getTpdAuraResons());
		tpdCover.setCoverexclusion(eapplyInput.getTpdExclusions());
		if(tpdExUnit!=null && tpdExUnit!=""){
			tpdCover.setFixedunit(Integer.valueOf(tpdExUnit));
		}
		if(eapplyInput.getTpdLoading()!=null)
			tpdCover.setLoading(new BigDecimal((eapplyInput.getTpdLoading())));
		tpdCover.setOccupationrating(eapplyInput.getTpdOccCategory());
		tpdCover.setCost("0");
		if(eapplyInput.getExistingTpdAmt()!=null){
			tpdCover.setExistingcoveramount(new BigDecimal(eapplyInput.getExistingTpdAmt()));
		}
		
		if(null!=eapplyInput.getFreqCostType() && !"".equalsIgnoreCase(eapplyInput.getFreqCostType())){
			tpdCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		}else{
			tpdCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);
		}
		if(null!=eapplyInput.getAddnlTpdCoverDetails() && !"".equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().toString())){
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost() != null){
			tpdCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdLoadingCost()));
		}
		
		tpdCover.setCovercategory(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType());
		tpdCover.setCoveroption(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverName());
		tpdCover.setCost(""+eapplyInput.getAddnlTpdCoverDetails().getTpdCoverPremium());
		
		
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType() != null && eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue())).intValue());			
		}else{			
			tpdCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt())).intValue());
		}
		
		if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null){
			if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDUNITISED)){
				tpdCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.TPDFIXED)){
				tpdCover.setAddunitind("0");
			}
		}
		tpdCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt()));
		}
		
		if(eapplyInput.getIpAmt()!=null){
			eapplyInput.setExistingIPAmount(eapplyInput.getIpAmt());
		}
		
		if(ipExistingAmt!=null){
			ipCover.setExistingcoveramount(new BigDecimal((ipExistingAmt)));
			eapplyInput.setExistingIPAmount(ipExistingAmt);
		}		            
		if(ipExUnit!=null && ipExUnit!=""){
			ipCover.setFixedunit(Integer.valueOf(ipExUnit));
		}
		ipCover.setCreatedate(new Timestamp(new Date().getTime()));
		ipCover.setLastupdatedate(new Timestamp(new Date().getTime()));
		ipCover.setCovertype(MetlifeInstitutionalConstants.INCOME_PROTECTION);
		ipCover.setCovercode("IP");
		//ipCover.setCoverdecision(eapplyInput.getIpDecision());
		ipCover.setCoverdecision("ACC");
		ipCover.setCoverreason(eapplyInput.getIpAuraResons());
		ipCover.setCoverexclusion(eapplyInput.getIpExclusions());
		if(eapplyInput.getIpLoading()!=null && eapplyInput.getIpLoading().trim().length()>ZERO_CONST)
			ipCover.setLoading(new BigDecimal((eapplyInput.getIpLoading())));
		ipCover.setOccupationrating(eapplyInput.getIpOccCategory());
		ipCover.setCost("0");
		if(null!=eapplyInput.getFreqCostType() && !"".equalsIgnoreCase(eapplyInput.getFreqCostType())){
			ipCover.setFrequencycosttype(eapplyInput.getFreqCostType());
		}else{
			ipCover.setFrequencycosttype(MetlifeInstitutionalConstants.WEEKLY);
		}
		ipCover.setOptionalunit(eapplyInput.getIpSalaryPercent());  /*added for 85% salary check-box */
		
		if(null!=eapplyInput.getAddnlIpCoverDetails() && !"".equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().toString())){
		if(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()!= null){
			ipCover.setPolicyfee(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpLoadingCost()));
		}
		
		
		if(null!=eapplyInput && QuoteConstants.PARTNER_SFPS.equalsIgnoreCase(eapplyInput.getPartnerCode())
				&& null!= eapplyInput.getAddnlIpCoverDetails().getIpCoverType() 
				&& QuoteConstants.IP_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()))
		{
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue()).intValue()));
			ipCover.setAddunitind("1");
		}
		
		ipCover.setCovercategory(eapplyInput.getAddnlIpCoverDetails().getIpCoverType());
		ipCover.setCoveroption(eapplyInput.getAddnlIpCoverDetails().getIpCoverName());
		ipCover.setCost(""+eapplyInput.getAddnlIpCoverDetails().getIpCoverPremium());
		
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType() != null && eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue())).intValue());			
		}else{			
			ipCover.setAdditionalunit((new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt())).intValue());
		}
		
		
		if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null){
			if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_UNITISED)){
				ipCover.setAddunitind("1");
			}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.IP_FIXED)){
				ipCover.setAddunitind("0");
			}
		}	
		if(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()!= null){
		    ipCover.setAdditionalcoveramount(new BigDecimal(eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt()));
		}
		ipCover.setAdditionalipbenefitperiod(eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod());
		ipCover.setAdditionalipwaitingperiod(eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod());
		}

		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
			for(int index=0;index<eapplyInput.getExistingCovers().getCover().size();index++){
				CoverJSON  coverJSON=eapplyInput.getExistingCovers().getCover().get(index);
				if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("1")){/*Death*/
					if(coverJSON.getAmount()!=null){
						deathCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
					
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("2")){/*TPD*/
					if(coverJSON.getAmount()!=null){
						tpdCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
					}
				}else if(coverJSON.getBenefitType()!=null && coverJSON.getBenefitType().equalsIgnoreCase("4")
						&& coverJSON.getAmount()!=null){
						ipCover.setExistingcoveramount(new BigDecimal(coverJSON.getAmount()));
						ipCover.setExistingipwaitingperiod(coverJSON.getWaitingPeriod());
						ipCover.setExistingipbenefitperiod(coverJSON.getBenefitPeriod());
				}
				
			}
		}
		coverList.add(deathCover);
		coverList.add(tpdCover);
		coverList.add(ipCover);
		/*Cover object mapping end*/
		log.info("Create cancel cover finish");
		return coverList;
}



private EapplyInput createWorkRatingCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("Work rating cover retrive start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					/*addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());*/
					
					if(coversObj.getAdditionalcoveramount()!= null){
						eapplyInput.setDeathNewAmt(coversObj.getAdditionalcoveramount().toString());
					}
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					/*if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}*/
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
					}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	/*addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());*/
	            	if(coversObj.getAdditionalcoveramount()!= null){
						eapplyInput.setTpdNewAmt(coversObj.getAdditionalcoveramount().toString());
					}
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	/*if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            		}*/
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	/*addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());*/
	            	if(coversObj.getAdditionalcoveramount()!= null){
						eapplyInput.setIpNewAmt(coversObj.getAdditionalcoveramount().toString());
					}
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	/*if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            		}*/
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
				}
			}
			
		}
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	log.info("Work rating cover retrive finish");
	return eapplyInput;
}
private EapplyInput createLifeEventCvrRetrieve(EapplyInput eapplyInput, Applicant applicantObj,
		DeathAddnlCoversJSON addnlDeathCoverDetails, TpdAddnlCoversJSON addnlTpdCoverDetails,
		IpAddnlCoversJSON addnlIpCoverDetails) {
	log.info("create LifeEvent Cvr Retrieve start");
	Cover coversObj;
	if(applicantObj.getCovers()!=null && applicantObj.getCovers().size()>ZERO_CONST){
		for(int coverItr = 0; coverItr < applicantObj.getCovers().size();coverItr++){
		coversObj = applicantObj.getCovers().get(coverItr);
			if(null!= coversObj){
				if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversObj.getCovercode())){
					eapplyInput.setDeathDecision(coversObj.getCoverdecision());
					eapplyInput.setDeathAuraResons(coversObj.getCoverreason());
					eapplyInput.setDeathExclusions(coversObj.getCoverexclusion());
					if(coversObj.getAdditionalcoveramount() != null ){
						eapplyInput.setDeathNewAmt(coversObj.getAdditionalcoveramount().toString());
					}
					
					/*addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
					addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
					addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
					if(coversObj.getAdditionalcoveramount()!= null){
						addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
						}*/
					eapplyInput.setDeathOccCategory(coversObj.getOccupationrating());
					/*if(coversObj.getLoading()!=null){
						eapplyInput.setDeathLoading(coversObj.getLoading().toString());
						}*/
					if(coversObj.getExistingcoveramount()!= null){
						eapplyInput.setExistingDeathAmt(coversObj.getExistingcoveramount().toString());
						}
					eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
					if(coversObj.getCost() != null){						
						eapplyInput.setDeathCoverPremium((new BigDecimal(coversObj.getCost())).doubleValue());
						}
					if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
					{
						addnlDeathCoverDetails.setDeathCoverName(coversObj.getCoveroption());
						addnlDeathCoverDetails.setDeathCoverType(coversObj.getCovercategory());
						addnlDeathCoverDetails.setDeathCoverPremium(coversObj.getCost());
						addnlDeathCoverDetails.setDeathInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
						addnlDeathCoverDetails.setTotalDeathUnits(coversObj.getCoveramount());
						if(coversObj.getAdditionalcoveramount()!= null){
							addnlDeathCoverDetails.setDeathFixedAmt(coversObj.getAdditionalcoveramount().toString());
							}
					}
				}
	           if("TPD".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setTpdDecision(coversObj.getCoverdecision());
	            	eapplyInput.setTpdAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setTpdExclusions(coversObj.getCoverexclusion());
	            	/*addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
	            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
	            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            		}*/
	            	eapplyInput.setTpdOccCategory(coversObj.getOccupationrating());
	            	/*if(coversObj.getLoading()!=null){
	            		eapplyInput.setTpdLoading(coversObj.getLoading().toString());
	            		}*/
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingTpdAmt(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
	            	if(coversObj.getAdditionalcoveramount() != null ){
						eapplyInput.setTpdNewAmt(coversObj.getAdditionalcoveramount().toString());
					}
	            	if(coversObj.getCost() != null){
	            		eapplyInput.setTpdCoverPremium((new BigDecimal(coversObj.getCost())).doubleValue());
	            	}
	            	if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
	            	{
	            		addnlTpdCoverDetails.setTpdCoverName(coversObj.getCoveroption());
		            	addnlTpdCoverDetails.setTpdCoverType(coversObj.getCovercategory());
		            	addnlTpdCoverDetails.setTpdCoverPremium(coversObj.getCost());
		            	addnlTpdCoverDetails.setTpdInputTextValue(Integer.toString(coversObj.getAdditionalunit()));
		            	addnlTpdCoverDetails.setTotalTPDUnits(coversObj.getCoveramount());
		            	if(coversObj.getAdditionalcoveramount()!= null){
		            		addnlTpdCoverDetails.setTpdFixedAmt(coversObj.getAdditionalcoveramount().toString());
		            		}
	            	}
				}
	           if("IP".equalsIgnoreCase(coversObj.getCovercode())){
	            	eapplyInput.setIpDecision(coversObj.getCoverdecision());
	            	eapplyInput.setIpAuraResons(coversObj.getCoverreason());
	            	eapplyInput.setIpExclusions(coversObj.getCoverexclusion());
	            	/*addnlIpCoverDetails.setIpCoverName(coversObj.getCoveroption());
	            	addnlIpCoverDetails.setIpCoverType(coversObj.getCovercategory());
	            	addnlIpCoverDetails.setIpCoverPremium(coversObj.getCost());
	            	if(coversObj.getAdditionalcoveramount()!= null){
	            		addnlIpCoverDetails.setIpFixedAmt(coversObj.getAdditionalcoveramount().toString());
	            		}
	            	addnlIpCoverDetails.setWaitingPeriod(coversObj.getAdditionalipwaitingperiod());
	            	addnlIpCoverDetails.setBenefitPeriod(coversObj.getAdditionalipbenefitperiod());*/
	            	eapplyInput.setIpOccCategory(coversObj.getOccupationrating());
	            	/*if(coversObj.getLoading()!=null){
	            		eapplyInput.setIpLoading(coversObj.getLoading().toString());
	            		}*/
	            	if(coversObj.getExistingcoveramount()!= null){
	            		eapplyInput.setExistingIPAmount(coversObj.getExistingcoveramount().toString());
	            		}
	            	eapplyInput.setFreqCostType(coversObj.getFrequencycosttype());
	            	if(coversObj.getAdditionalcoveramount() != null ){
						eapplyInput.setIpNewAmt(coversObj.getAdditionalcoveramount().toString());
					}
	            	if(coversObj.getCost() != null){
	            		eapplyInput.setIpCoverPremium((new BigDecimal(coversObj.getCost())).doubleValue());
	            	}
	            	if(coversObj.getExistingipwaitingperiod() != null){
	            		eapplyInput.setWaitingPeriod(coversObj.getExistingipwaitingperiod());
	            	}
	            	if(coversObj.getExistingipbenefitperiod() != null){
	            		eapplyInput.setBenefitPeriod(coversObj.getExistingipbenefitperiod());
	            	}
				}
			}
			
		}
		eapplyInput.setAddnlDeathCoverDetails(addnlDeathCoverDetails);
		eapplyInput.setAddnlTpdCoverDetails(addnlTpdCoverDetails);
		eapplyInput.setAddnlIpCoverDetails(addnlIpCoverDetails);
		
	}
	log.info("create LifeEvent Cvr Retrieve finish");
	return eapplyInput;
}

@RequestMapping(value = "/printQuotePage", method = RequestMethod.POST)
public  ResponseEntity<EapplyOutput> printQuotePage(@RequestHeader(value="Authorization") String authId, @RequestBody EapplyInput eapplyInput,    UriComponentsBuilder ucBuilder) {
 log.info("printQuotePage  start");
    
 PDFbusinessObjectMapper pdFbusinessObjectMapper = new PDFbusinessObjectMapper();
 Properties property = new Properties();
 InputStream url = null;
 EapplyOutput eapplyOutput=null;
 PDFObject pdfObject =null;
 String logoPath = null;
 String clientPdfLocation = null;
 String pdfAttachDir = null;
 try {
	 	Eapplication eapplication = convertToEapplication(eapplyInput,"");	
		 sys = SystemProperty.getInstance();
		 File file = new File( sys.getProperty(eapplication.getPartnercode()));
	     url = new FileInputStream( file);    
	      property.load( url);
	      log.info("getPropertyFile>> {}",sys.getProperty(eapplication.getPartnercode()));
		  pdfObject = pdFbusinessObjectMapper.getPdfOjectsForEapply(eapplication, eapplyInput, MetlifeInstitutionalConstants.INSTITUTIONAL, sys.getProperty(eapplication.getPartnercode()));
		  pdfObject.setChannel("coverDetails");
	      pdfObject.setLob(MetlifeInstitutionalConstants.INSTITUTIONAL);		 
	      pdfObject.setFundId(eapplication.getPartnercode());
		  pdfObject.setSectionBackground(property.getProperty("ClientSectionColor_" + pdfObject.getFundId()));
		  pdfObject.setColorStyle(property.getProperty("ClientColorStyle_" + pdfObject.getFundId()));
		  pdfObject.setSectionTextColor(property.getProperty("ClientSectionTextColor_" + pdfObject.getFundId()));
		  pdfObject.setBorderColor(pdfObject.getSectionBackground());
		  logoPath = property.getProperty("logo_" + pdfObject.getFundId()); 
		  pdfObject.setLogPath(logoPath);	
		  if(!pdfObject.isQuickQuoteRender() && eapplyInput.getAppNum() != null) {
			  pdfObject.setApplicationNumber(""+eapplyInput.getAppNum());
		  }
		  if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
			  clientPdfLocation = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+ eapplyInput.getCorpFundCode()+"_"+eapplication.getApplicationumber()+".pdf";
		  }
		  else {
			  clientPdfLocation = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+ eapplication.getPartnercode()+"_"+eapplication.getApplicationumber()+".pdf";
		  }
		  pdfObject.setPdfAttachDir(clientPdfLocation);
		  pdfObject.setClientPDFLoc(clientPdfLocation);
		  GenerateEapplyPDF generateEapplyPDF = new GenerateEapplyPDF();
		  generateEapplyPDF.generatePDF(pdfObject,sys.getProperty(eapplication.getPartnercode()));
		  eapplyOutput=new EapplyOutput();
		 
		  eapplyOutput.setClientPDFLocation(EapplyHelper.formEncryptedFilePath(pdfObject.getClientPDFLoc()));

			
 	} catch (IOException e) {
 		log.error("Error in input output of printquote page: {}",e.getMessage());
	} catch (DocumentException e) {
		log.error("Error in document printquote: {}",e.getMessage());
	} catch (MetLifeBSRuntimeException e) {
		log.error("Error in metlifebs printquote : {}",e.getMessage());
	}catch (Exception e) {
		log.error("Error in printing quote page: {} ",e.getMessage());
	}   
 log.info("printQuotePage  finish");
 return new ResponseEntity<>(eapplyOutput, HttpStatus.OK);
}

//Added for checking UW duplicate Applications
@RequestMapping(value = "/checkDuplicateUWApplications", method = RequestMethod.POST)
public ResponseEntity<Boolean> checkDupUWApplications(@RequestParam(value="fundCode") String fundCode
		  ,@RequestParam(value="clientRefNo") String clientRefNo
		  ,@RequestParam(value="manageTypeCC") String manageTypeCC
		  ,@RequestParam(value="dob") String dob
		  ,@RequestParam(value="firstName") String firstName
		  ,@RequestParam(value="lastName") String lastName
		  ,@RequestHeader(value="Authorization") String Authorization) {
	
	log.info("Start for Check Duplicate Application RUW");
	Boolean dupUWApplication=Boolean.TRUE;
	Boolean corpFlow = Boolean.FALSE;
	Request request = null;
	 String clientRefNumber = null;
	 if (null == Authorization){
		 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	 } else {
		 Eappstring eappstring =  b2bService.retriveClientData(Authorization);
		 if (null == eappstring.getSecurestring()) {
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 } else {
			 if(eappstring.getSecurestring().contains("Corporate")){
				 corpFlow = true; 
			 }else
			 {
			 request = B2bServiceHelper.unMarshallingMInput(eappstring.getSecurestring());
			 }
		 }
	 }
	 if (!corpFlow){ 
		 if (null != request){
			 clientRefNumber = request.getPolicy().getApplicant().get(0).getClientRefNumber();
			 if (null == clientRefNumber || null == clientRefNo){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }
			 if (!clientRefNo.equalsIgnoreCase(clientRefNumber)){
				 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			 }  
		 } else {
			 return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		 }
	 }
	System.out.println("Authorization >>. "+Authorization); 
	
	try 
	{
		if(fundCode!=null && clientRefNo!=null && manageTypeCC!=null && firstName!=null && lastName!=null )
			{
				dupUWApplication =eapplyService.checkDupUWApplication(fundCode, clientRefNo, manageTypeCC,dob,firstName,lastName);
			}
	}
	catch(Exception e)
	{
		log.error("Error in checking duplicate application: {}",e);
	}
	
	return new ResponseEntity<>(dupUWApplication, HttpStatus.OK);
}

@RequestMapping(value = "/checkAccApplications", method = RequestMethod.POST)
public ResponseEntity<AcceptedAppStatus> checkAccpplications(@RequestParam(value="fundCode") String fundCode
		  ,@RequestParam(value="clientRefNo") String clientRefNo
		  ,@RequestParam(value="manageTypeCC") String manageTypeCC
		  ,@RequestParam(value="dob") String dob
		  ,@RequestParam(value="firstName") String firstName
		  ,@RequestParam(value="lastName") String lastName) {
	
	log.info("Start for Check Duplicate Application RUW");
	AcceptedAppStatus accAppstatus = new AcceptedAppStatus();
	try 
	{
		if(fundCode!=null && clientRefNo!=null && manageTypeCC!=null && firstName!=null && lastName!=null )
			{
			accAppstatus =eapplyService.checkAccApplicationStatus(fundCode, clientRefNo, manageTypeCC,dob,firstName,lastName);
			}
	}
	catch(Exception e)
	{
		log.error("Error in checking Accepted application: {}",e);
	}
	
	return new ResponseEntity<>(accAppstatus, HttpStatus.OK);
}

/*
@RequestMapping(value = "/fileUploadnew", method = RequestMethod.POST)
	public  ResponseEntity<DocumentJSON> uploadFileNew(@RequestHeader(value="Content-Type") String contentType,InputStream inputStream) {
	 log.info("File uplaod start");
	 DocumentJSON documentJSON = null;		
	 log.info(">>>inside {}",contentType);
	 String filepath = null;
	 File targetFile = null;
	 OutputStream outStream = null;
	 String fileName= contentType.substring((contentType.indexOf("/")+1), contentType.length());
       try {
       	
       	
       	sys = SystemProperty.getInstance();
       	filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
       	targetFile = new File(filepath);
       	if(!targetFile.getParentFile().exists())
       	{
       		targetFile.getParentFile().mkdirs();
       	}
       	outStream = new FileOutputStream(targetFile);
       	byte[] buffer = new byte[1024];
       	int len = inputStream.read(buffer);
       	while (len != -1) {
       		outStream.write(buffer, 0, len);
       	    len = inputStream.read(buffer);
       	}	            
           IOUtils.copy(inputStream,outStream);
           inputStream.close();
           documentJSON = new DocumentJSON();
           documentJSON.setFileLocations(filepath);
           documentJSON.setName(fileName);
           documentJSON.setStatus(Boolean.TRUE);
           
       } catch (IOException e) {
           throw new RuntimeException(e);
       } catch (Exception e) {
		    log.error("Error in file uplaod: {}",e);
		} 	finally{
			try {
				if(outStream!=null){
					outStream.close();
				}					
			} catch (IOException e) {
				log.error("error closing file in fileupload functionality: {}",e);
			}
		}
       log.info("File uplaod finish");
	 return new ResponseEntity<DocumentJSON>(documentJSON, HttpStatus.OK);
}
@RequestMapping(value = "/fetchfileUpload", method = RequestMethod.POST)
public  ResponseEntity<List<FetchedFiles>> fetchuploadFile(@RequestParam(value="path") String path) {
 log.info("Fetch File upload start");
 //DocumentJSON documentJSON = null;		
 log.info(">>>inside {}",path);
 String filepath = null;
 File targetFile = null;
 File[] listOfFiles = null;
 List<FetchedFiles> fileList = new ArrayList<FetchedFiles>();
    try {
    	 sys = SystemProperty.getInstance();
    	filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+path;
    	targetFile = new File(filepath);
    	
    	listOfFiles=targetFile.listFiles();
    	 for(File file : listOfFiles)
    	 {
    		 FetchedFiles fecthfile=new FetchedFiles();
    		 fecthfile.setName(file.getName());
    		 fecthfile.setFile(file);
    		 fileList.add(fecthfile);
    	 }
        
    } catch (IOException e) {
        throw new RuntimeException(e);
    } catch (Exception e) {
	    log.error("Error in fetch file uplaod: {}",e);
	}
    log.info("fetch File uplaod finish");
 return new ResponseEntity<List<FetchedFiles>>(fileList, HttpStatus.OK);
}

@RequestMapping(value = "/deleteFile", method = RequestMethod.POST)
public  ResponseEntity<Boolean> deleteuploadFile(@RequestParam(value="path") String path) {
 log.info("File delete start");
 //DocumentJSON documentJSON = null;		
 log.info(">>>inside {}",path);
 String filepath = null;
 File targetFile = null;
 File[] listOfFiles = null;
    try {
    	 sys = SystemProperty.getInstance();
    	filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+path;
    	targetFile = new File(filepath);
    	
    	if(targetFile.exists())
    	{
    		listOfFiles=targetFile.listFiles();
    		for(File deleteFile : listOfFiles)
    		{
    			if(deleteFile.isFile()){
    				deleteFile.delete();
    			}
    		}
    	}
        
    } catch (IOException e) {
        throw new RuntimeException(e);
    } catch (Exception e) {
	    log.error("Error in delete file: {}",e);
	}
    log.info("delete File finish");
 return new ResponseEntity<Boolean>(HttpStatus.OK);
}*/



	
	/**
	 * @param eappInput
	 * @param ucBuilder
	 * @return
	 * 
	 * 		This Method is used to save the application info sent from UI into db
	 */
	@RequestMapping(value = "/saveEapplyNew", method = RequestMethod.POST)
	public  ResponseEntity<Boolean> saveEapplyNew(@RequestBody EappInput eappInput, UriComponentsBuilder ucBuilder) {
	 log.info("saveEapplyNew Method start");

	 Boolean submitStatus = Boolean.FALSE;
	 Eapplication prevRecord = null;
	 Eapplication eapplication = null;
	 Boolean docRecord = Boolean.FALSE;
	 
	 try {
		
		 eapplication = EapplyServiceHelper.convertToEapplication(eappInput, "Pending");
		 prevRecord = eapplyService.retrieveApplication(""+eappInput.getApplicatioNumber());
		 
		 if(prevRecord!=null){
			 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
			 eapplication.setCreatedate(prevRecord.getCreatedate());
		 }else{
			 eapplication.setCreatedate(new Timestamp(new Date().getTime()));
			 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
		 }
		 
		 if(prevRecord!=null && prevRecord.getDocuments()!=null && !(prevRecord.getDocuments().isEmpty())){
			 docRecord=eapplyService.deleteDocument(prevRecord.getId());
		 }
		 log.info("Previous record Deleted: {}",docRecord);
		eapplyService.submitEapplication(eapplication);
		
	} catch (Exception e) {
		log.error("Error in saveEapplyNew: ",e);
	}
	 return new ResponseEntity<>(submitStatus, HttpStatus.OK);
	}


	/**
	 * @param applicationNumber
	 * @return
	 * 
	 * 		This Method is used to retrieve and return the 
	 * application info from db based on the application number
	 */
	@RequestMapping(value = "/retieveApplicationNew", method = RequestMethod.GET)
	public ResponseEntity<List<EappInput>> retieveApplicationNew(@RequestParam(value="applicationNumber") String applicationNumber) {		
		 log.info("retieveApplicationNew method start");
		 List<EappInput> eapplyInputList = new ArrayList<>();
		 EappInput eappInput=null;
		 ObjectMapper objmp = null;
		 try{
			 if(applicationNumber!=null){
				Eapplication eapplication = eapplyService.retrieveApplication(applicationNumber);
				String savedeApply = eapplication.geteAppSaveDataclob();
				objmp = new ObjectMapper();
				eappInput = objmp.readValue(savedeApply, EappInput.class);
				eapplyInputList.add(eappInput);
			 }
		 }catch(Exception e){
			 log.error("Error in retrive application: {}",e);
		 }
		  if(eappInput==null){
		      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		  }
	  log.info("retieveApplicationNew method finish");
	  
	  return new ResponseEntity<>(eapplyInputList, HttpStatus.OK);
	}
	
	/**
	 * @param authId
	 * @param eappInput
	 * @param ucBuilder
	 * @return
	 * 
	 * 		This Method is submit the Eapply objects
	 */
	@RequestMapping(value = "/submitEapplyNew", method = RequestMethod.POST)
	public  ResponseEntity<EapplyOutput> submitEapplyNew(@RequestHeader(value="Authorization") String authId, @RequestBody EappInput eappInput, UriComponentsBuilder ucBuilder) {
		log.info("submitEapplyNew method Start");
				
		EapplyOutput eapplyOutput = null;
		eapplyOutput =  eapplyService.submitEapplyNew(authId, eappInput, ucBuilder);	
		
		log.info("submitEapplyNew method Ends");
		return new ResponseEntity<>(eapplyOutput, HttpStatus.OK);
	}
	
	/**
	 * @param authId
	 * @param eappInput
	 * @param ucBuilder
	 * @return
	 * 
	 * 		This Method is used to print the pdf
	 */
	@RequestMapping(value = "/printQuotePageNew", method = RequestMethod.POST)
	public  ResponseEntity<EapplyOutput> printQuotePageNew(@RequestHeader(value="Authorization") String authId, @RequestBody EappInput eappInput, UriComponentsBuilder ucBuilder) {
	 log.info("printQuotePageNew Method  start");
	 
	 	EapplyOutput eapplyOutput =  null;
	 	eapplyOutput =  eapplyService.printQuotePageNew(authId, eappInput, ucBuilder);
	 
	 log.info("printQuotePageNew method  finish");
	 return new ResponseEntity<>(eapplyOutput, HttpStatus.OK);
	}

	/**
	 * @param contentType
	 * @param inputStream
	 * @return
	 * 
	 * 		This Method is used to upload the file 
	 */
	@RequestMapping(value = "/fileUploadNew", method = RequestMethod.POST)
	public  ResponseEntity<DocumentInfo> uploadFileNew(@RequestHeader(value="Content-Type") String contentType,InputStream inputStream) {
	 log.info("uploadFileNew method start");
	 	
	 	DocumentInfo docInfo = null;
	 	docInfo = eapplyService.uploadFileNew(contentType, inputStream);
	 	
        log.info("uploadFileNew method finish");
	 return new ResponseEntity<>(docInfo, HttpStatus.OK);
 }

}
