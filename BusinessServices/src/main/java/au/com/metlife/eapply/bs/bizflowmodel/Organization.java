package au.com.metlife.eapply.bs.bizflowmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Organization")

@XmlRootElement
public class Organization {
	
	@XmlAttribute(name="key")
	 String key;
	private List<AssignedIdentifier> AssignedIdentifier;

	public List<AssignedIdentifier> getAssignedIdentifier() {
		return AssignedIdentifier;
	}

	public void setAssignedIdentifier(List<AssignedIdentifier> assignedIdentifier) {
		AssignedIdentifier = assignedIdentifier;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	
	
}
