/**
 * 
 */
package vicsuper;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Yuvaraj
 * 
 */
public class VicsSuper_UpdateYourOccupation extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public VicsSuper_UpdateYourOccupation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public VicsSuper_UpdateYourOccupation UpdateYourOccupation_VicsSuper() {
		
		updateoccupation();
		confirmation();
		return new VicsSuper_UpdateYourOccupation(scriptHelper);
	} 
	
	private void updateoccupation() {
		
		String EmailId = dataTable.getData("VicsSuper_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("VicsSuper_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("VicsSuper_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("VicsSuper_UpdateOccupation", "TimeofContact");
		String FourteenHoursWork = dataTable.getData("VicsSuper_UpdateOccupation", "FourteenHoursWork");
		String Citizen = dataTable.getData("VicsSuper_UpdateOccupation", "Citizen");
		String EducationalInstitution = dataTable.getData("VicsSuper_UpdateOccupation", "EducationalInstitution");
		String EducationalDuties = dataTable.getData("VicsSuper_UpdateOccupation", "EducationalDuties");
		String TertiaryQual = dataTable.getData("VicsSuper_UpdateOccupation", "TertiaryQual");
		String AnnualSalary = dataTable.getData("VicsSuper_UpdateOccupation", "AnnualSalary");
		
		try{
			
			//*****Click on Update Occupation Link*******\\		
			WebDriverWait wait=new WebDriverWait(driver,18);			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Update your occupation')]"))).click();
			report.updateTestLog("Update your Occupation", "Update occupation link is Selected", Status.PASS);
			sleep(1500);	
			
			if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(2000);
			}
			
			
			
// -----------------------------------------------------Personal Details Page------------------------------------------------------------------------//
			
			
						// *****Agree to Duty of disclosure*******\\
			sleep(2000);
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*")))
								.click();
						sleep(250);
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

						// *****Agree to Privacy Statement*******\\
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*")))
								.click();
						sleep(250);
						report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
						sleep(200);

			
// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(By.xpath("//*[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId")))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(ContactNumber);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

										
										
			// ***Select the Preferred time of Contact*****\\

						if (TimeofContact.equalsIgnoreCase("Morning")) {
							driver.findElement(By.xpath("(.//input[@name='workRatingPrefTime']/parent::*)[1]")).click();
							report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
						} else {
							driver.findElement(By.xpath("(.//input[@name='workRatingPrefTime']/parent::*)[2]")).click();
							report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
						}

						
						/* // ***********Select the Gender******************\\
						 * 
						 * if (Gender.equalsIgnoreCase("Male")) {
						 * driver.findElement(By.xpath(".//input[@value='Male']/parent::*"))
						 * .click(); sleep(250); report.updateTestLog("Gender",
						 * "Gender selected is: " + Gender, Status.PASS); } else {
						 * driver.findElement(By.xpath(".//input[@value='Female']/parent::*"
						 * )).click(); sleep(250); report.updateTestLog("Gender",
						 * "Gender selected is: " + Gender, Status.PASS); }
						 */
						 

						//*****Click on Continue*******\\	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']"))).click();
						report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
						sleep(3000);
						
						
// -----------------------------------------------------Occupation------------------------------------------------------------------------//
						
						
						// *****Select the 14 Hours Question*******\\

						if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(By.xpath("//*[@name='fifteenHrsQuestion ']/parent::*"))).click(); 
							report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						} else {
							wait.until(ExpectedConditions
									.elementToBeClickable(By.xpath("//*[@name='fifteenHrsQuestion']/parent::*"))).click(); 
							report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						}

						// *****Resident of Australia****\\

						if (Citizen.equalsIgnoreCase("Yes")) {
							driver.findElement(By.xpath("(//input[@name='areyouperCitzWrkUpdateQuestion']/parent::*)[1]")).click();
							sleep(250);
							report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
						} else {
							driver.findElement(By.xpath("(//input[@name='areyouperCitzWrkUpdateQuestion']/parent::*)[2]")).click();
							sleep(250);
							report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
						}

						
						// *****Are the duties of your regular occupation limited to
						// either:*****\\

						if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

							// ***Select duties which are undertaken within an Office
							// Environment?\\
							if (EducationalInstitution.equalsIgnoreCase("Yes")) {
								wait.until(ExpectedConditions
										.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]")))
										.click(); // Dhipika
								report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
								sleep(1000);
							} else {
								wait.until(ExpectedConditions
										.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]")))
										.click(); // Dhipika
								report.updateTestLog("Educational institution", "Selected No", Status.PASS);
								sleep(1000);
							}

							// *****Educational duties performed within a school or other
							// educational institution******\\
							if (EducationalDuties.equalsIgnoreCase("Yes")) {
								wait.until(ExpectedConditions
										.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]")))
										.click(); // Dhipika
								report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
								sleep(1000);
							} else {
								wait.until(ExpectedConditions
										.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]")))
										.click(); // Dhipika
								report.updateTestLog("Educational duties", "Selected No", Status.PASS);
								sleep(1000);
							}

						} else {
							report.updateTestLog("Additional Questions",
									"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

						}

						// Do you hold a tertiary qualification or work in a management role
						if (driver.getPageSource().contains("Do you:")) {

							// *****Do You Hold a Tertiary Qualification******\\
							if (TertiaryQual.equalsIgnoreCase("Yes")) {
								wait.until(ExpectedConditions
										.elementToBeClickable(By.xpath("(//input[@name='occRating2']/parent::*)[1]"))).click(); // Dhipika
								report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
								sleep(1000);
							} else {
								wait.until(ExpectedConditions
										.elementToBeClickable(By.xpath("(//input[@name='occRating2']/parent::*)[2]"))).click(); // Dhipika
								report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
								sleep(1000);
							}

						} else {
							report.updateTestLog("Additional Questions",
									"Do you hold a tertiary qualification or work in a management role is not present",
									Status.FAIL);
						}

						// *****What is your annual Salary******

						wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingAnnualSal"))).sendKeys(AnnualSalary); // Dhipika
						sleep(1000);
						report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);
						
						//*****Click on Continue*******\\	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingOccupForm);']"))).click();
						report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
						sleep(3000);

			
		
	
		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	

private void confirmation() {
			
		try {
			String DecisionMessage = dataTable.getData("VicsSuper_UpdateOccupation", "DecisionMessage");
			confirmationScreenValidation();

			// *****General Consent******\\

			WebDriverWait wait = new WebDriverWait(driver, 18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelWR']/span")))
					.click();
			report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
			sleep(500);

			// ******Submit******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitWorkRating()']")))
					.click();
			report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

			// *******Feedback Pop-up******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1000);
			
			 /* String
			 * Justification=driver.findElement(By.xpath("//p[@align='justify']"
			 * )).getText(); System.out.println(Justification); sleep(400);
			 * 
			 * if(Justification.equalsIgnoreCase(Decision)){
			 * report.updateTestLog("Decision",
			 * "Wording is displayed as Expected", Status.PASS);
			 * 
			 * } else{ report.updateTestLog("Decision",
			 * "Wording is not displayed as Expected", Status.FAIL);
			 * }
			 */
			
			 
			sleep(200);

			String Appstatus = driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(600);
			report.updateTestLog("Application status", "Application status is: " + Appstatus, Status.PASS);
			
			if(driver.getPageSource().contains(DecisionMessage)){
				report.updateTestLog("Decision Message", "Decision Message" + DecisionMessage +" is displayed", Status.PASS);
			}
			else
			{
				report.updateTestLog("Decision Message", "Decision Message" + DecisionMessage +" is not displayed", Status.FAIL);
				
			}
			
		
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		
		}
			}
		
		
		public void confirmationScreenValidation() {
			
			String Gender = dataTable.getData("VicsSuper_UpdateOccupation", "Gender");
			String EmailId = dataTable.getData("VicsSuper_UpdateOccupation", "EmailId");
			String TypeofContact = dataTable.getData("VicsSuper_UpdateOccupation", "TypeofContact");
			String ContactNumber = dataTable.getData("VicsSuper_UpdateOccupation", "ContactNumber");
			String TimeofContact = dataTable.getData("VicsSuper_UpdateOccupation", "TimeofContact");
			String FourteenHoursWork = dataTable.getData("VicsSuper_UpdateOccupation", "FourteenHoursWork");
			String Citizen = dataTable.getData("VicsSuper_UpdateOccupation", "Citizen");
			String EducationalInstitution = dataTable.getData("VicsSuper_UpdateOccupation", "EducationalInstitution");
			String EducationalDuties = dataTable.getData("VicsSuper_UpdateOccupation", "EducationalDuties");
			String TertiaryQual = dataTable.getData("VicsSuper_UpdateOccupation", "TertiaryQual");
			String AnnualSalary = dataTable.getData("VicsSuper_UpdateOccupation", "AnnualSalary");
			//To add $ before the amount value
			AnnualSalary = "$"+AnnualSalary;			
			
			String XML = dataTable.getData("VicSuper_Login", "XML");
			
			String[] part;
			String part2;
			String[] value;
			
			//To get firstName
			part = XML.split("<firstName>");
			part2 = part[1]; 
			value = part2.split("</firstName>");
			String firstName = value[0]; 
			
			//To get lastName
			part = XML.split("<lastName>");
			part2 = part[1]; 
			value = part2.split("</lastName>");
			String lastName = value[0]; 
			
			//To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1]; 
			value = part2.split("</dateOfBirth>");
			String dob = value[0]; 	
					
			//First Name
			 WebElement objFName = driver.findElement(By.xpath("//*[text()='First name']//following::label"));
			 fieldValidation("WebElement",objFName,firstName,"First Name");
			 
			//Last Name
			 WebElement objLName = driver.findElement(By.xpath("//*[text()='Last name']//following::label"));
			 fieldValidation("WebElement",objLName,lastName,"Last Name");	 
			 
			//Gender
			 WebElement ObjGender = driver.findElement(By.xpath("//*[text()='Gender']//following::label"));
			 fieldValidation("WebElement",ObjGender,Gender,"Genderquestion");	
			 
			//Phone Number type
			 WebElement objcontactType = driver.findElement(By.xpath("//*[text()='Preferred contact type']//following::label"));
			 fieldValidation("WebElement",objcontactType,TypeofContact,"contactType");
			
			//PhoneNumber
			 WebElement objpreferrednumCCId = driver.findElement(By.xpath("//*[text()='Preferred contact number ']//following::label"));
			 fieldValidation("WebElement",objpreferrednumCCId,ContactNumber,"preferrednumCCId"); 
			 
			//Email
			 WebElement objcontactEmail = driver.findElement(By.xpath("//*[text()='Email address']//following::label"));
			 fieldValidation("WebElement",objcontactEmail,EmailId,"Email");
			 
			//DOB
			 WebElement objDOB = driver.findElement(By.xpath("//*[text()='Date of birth']//following::label"));
			 fieldValidation("WebElement",objDOB,dob,"DOB");
		
			//Morning Afternoon
			 WebElement objTime = driver.findElement(By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label"));
			 String eletext = objTime.getText();
			 System.out.println(eletext);
			 System.out.println(TimeofContact);
			 if (eletext.contains(TimeofContact)) {
				 report.updateTestLog("Time of contact ",  "Time of contact "+TimeofContact+" is displayed", Status.PASS);
					
			}
			 else {
				 report.updateTestLog("Time of contact ",  "Time of contact "+TimeofContact+" is not displayed", Status.FAIL);
					
			}
		
		
			//Do you work more than 14 hours per week? 
			 
			 
			 WebElement ObjWorkHRS = driver.findElement(By.xpath("//*[text()='Do you work 15 or more hours per week?']//following::label"));
			 fieldValidation("WebElement",ObjWorkHRS,FourteenHoursWork,"FourteenHoursWork");
			
			//Are you a citizen or permanent resident of Australia? Yes No
			
			 WebElement objCitizen = driver.findElement(By.xpath("//*[contains(text(),'Are you a citizen or permanent resident of Australia')]//following::label"));
			 fieldValidation("WebElement",objCitizen,Citizen,"Citizen");
			
				
			 //office Duties are Undertaken within an Office Environment?
				WebElement eduIns = driver.findElement(By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[1]"));
				fieldValidation("WebElement",eduIns,EducationalInstitution,"Are the duties of your regular occupation limited to Question1");
			
				WebElement eduDut = driver.findElement(By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[3]"));
				fieldValidation("WebElement",eduDut,EducationalDuties,"Are the duties of your regular occupation limited to Question2");
		
			//	TertiaryQualification / ManagementRole
			
				 WebElement objQual = driver.findElement(By.xpath("//*[text()='Do you:  ']//following::label"));
				 fieldValidation("WebElement",objQual,TertiaryQual,"TertiaryQualification / ManagementRole");
					
			//  Current annual salary?
				 WebElement objannualSalCCId = driver.findElement(By.xpath("//*[text()='Annual salary']//following::label"));
				 fieldValidation("Amount Validation",objannualSalCCId,AnnualSalary,"AnnualSalary");
				
									
		}		
		
		
	}

	
	
	

