class QuoteSummaryCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog, $q, $sce) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._ngDialog = ngDialog;
    this._EapplyData = EapplyData;
    this._$q = $q;
    this._$sce = $sce;
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
    this.changeCoverData = this._EapplyData.getChangeCoverData();
    this.enableLoadingSection = false;
    this.enableExclusionSection = false;
    this.deathLoading = false;
    this.tpdLoading = false;
    this.ipLoading = false;
    this.deathExclusion = false;
    this.tpdExclusion = false;
    this.ipExclusion = false;
    
    if(this.changeCoverData.applicant.overallCoverDecision === 'ACC'){
    	this.checkLoading();
    	this.checkExclusion();
    }
  }
  
  checkExclusion(){
	  if(this.changeCoverData.applicant.cover[0].coverExclusion != null){
		  this.deathExclusion = true;
		  this.deathExclusionText = this._$sce.trustAsHtml(this.changeCoverData.applicant.cover[0].coverExclusion);
	  }	
	  
	  if(this.changeCoverData.applicant.cover[1].coverExclusion != null){
		  this.tpdExclusion = true;
		  this.tpdExclusionText = this._$sce.trustAsHtml(this.changeCoverData.applicant.cover[1].coverExclusion);
	  }
	  
	  if(this.changeCoverData.applicant.cover[2].coverExclusion != null){
		  this.ipExclusion = true;
		  this.ipExclusionText = this._$sce.trustAsHtml(this.changeCoverData.applicant.cover[2].coverExclusion);
	  }
	  
	  if(this.deathExclusion || this.tpdExclusion || this.ipExclusion){
	  		this.enableExclusionSection = true;
	  } 
  }
  
  checkLoading(){  	
  	
  	if(this.changeCoverData.applicant.cover[0].loading > 0){
  		this.deathLoading = true;
  		this.deathLoadingCalculated = false;
  		this.deathLoadingCalculation().then((res) => {
  			this.calculateTotal();
  		});
  	}else{
  		this.deathLoadingCalculated = true;
  	}
  	
  	if(this.changeCoverData.applicant.cover[1].loading > 0){
  		this.tpdLoading = true;
  		this.tpdLoadingCalculated = false;
  		this.tpdLoadingCalculation().then((res) => {
  			this.calculateTotal();
  		});
  	}else{
  		this.tpdLoadingCalculated = true;
  	}
  	
  	if(this.changeCoverData.applicant.cover[2].loading > 0){
  		this.ipLoading = true;
  		this.ipLoadingCalculated = false;
  		this.ipLoadingCalculation().then((res) => {
  			this.calculateTotal();
  		});
  	}else{
  		this.ipLoadingCalculated = true;
  	}
  	
  	if(this.deathLoading || this.tpdLoading || this.ipLoading){
  		this.enableLoadingSection = true;
  	}    	
  
  }
  
  calculateTotal(){
	  if(this.deathLoadingCalculated && this.tpdLoadingCalculated && this.ipLoadingCalculated){
		  this.changeCoverData.totalMonthlyPremium = parseFloat(this.changeCoverData.applicant.cover[0].cost) + parseFloat(this.changeCoverData.applicant.cover[1].cost) + parseFloat(this.changeCoverData.applicant.cover[2].cost); 
	  }
  }
  
  deathLoadingCalculation(){
	  
	  let deferred = this._$q.defer();
	  
	  if(this.changeCoverData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.changeCoverData.applicant.cover[0].coverCategory === 'DcTailored'){
		  this.deathAmount = this.changeCoverData.applicant.cover[0].additionalCoverAmount;	 
	 }else{
		 this.deathAmount = parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
	 }
	  
	  const calculateJSON = {
			  'age': this.changeCoverData.applicant.age,
			  'fundCode': 'INGD',
			  'gender': this.changeCoverData.applicant.gender,
			  'deathOccCategory': this.changeCoverData.applicant.cover[0].occupationRating,
			  'smoker': false,
			  'premiumFrequency': this.changeCoverData.applicant.cover[0].frequencyCostType,
			  'manageType': 'CCOVER',
			  'deathCoverType': this.changeCoverData.applicant.cover[0].coverCategory,
			  'deathFixedAmount': this.deathAmount,
			  'stateCode': this.changeCoverData.applicant.contactDetails.state
        	};
	  
	  console.log(JSON.stringify(calculateJSON));
	  
	  this._CommonAPI.calculateINGAll(this.urlList.calculateINGDAll, calculateJSON).then(
		      (res) => {
		    	  
		    	  this.deathLoadingCost = res[0].cost;
		    	  this.changeCoverData.applicant.cover[0].cost = parseFloat(this.changeCoverData.applicant.cover[0].cost) + ((parseFloat(this.changeCoverData.applicant.cover[0].loading)/100) * parseFloat(this.deathLoadingCost));		    	  
		    	  this.deathLoadingCost = (parseFloat(this.changeCoverData.applicant.cover[0].loading)/100) * parseFloat(this.deathLoadingCost);
		    	  this.changeCoverData.applicant.cover[0].policyFee = Math.round((this.deathLoadingCost + 0.00001)*100)/100;
		    	  this.deathLoadingCalculated = true;
		    	  deferred.resolve({});
		    	  
		      },
		      (err) => {
		          this.errors = err.data.errors;
		          deferred.reject(err);
		        }
	);
	  
	  return deferred.promise;

	  
  }
  
  tpdLoadingCalculation(){
	  
	  let deferred = this._$q.defer();
	  
	  if(this.changeCoverData.applicant.cover[0].existingCoverCategory === 'DcAutomatic' && this.changeCoverData.applicant.cover[0].coverCategory === 'DcTailored'){
		  this.tpdAmount = this.changeCoverData.applicant.cover[1].additionalCoverAmount;
		  this.deathAmount = this.changeCoverData.applicant.cover[0].additionalCoverAmount;	 
	  }else{
		  this.tpdAmount = parseInt(this.changeCoverData.applicant.cover[1].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[1].existingCoverAmount);
		  this.deathAmount = parseInt(this.changeCoverData.applicant.cover[0].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[0].existingCoverAmount);
	  }
	  
	  const calculateJSON = {
			  'age': this.changeCoverData.applicant.age,
			  'fundCode': 'INGD',
			  'gender': this.changeCoverData.applicant.gender,
			  'tpdOccCategory': this.changeCoverData.applicant.cover[1].occupationRating,
			  'smoker': false,
			  'premiumFrequency': this.changeCoverData.applicant.cover[1].frequencyCostType,
			  'manageType': 'CCOVER',
			  'tpdCoverType': this.changeCoverData.applicant.cover[1].coverCategory,
			  'tpdFixedAmount': this.tpdAmount,
			  'stateCode': this.changeCoverData.applicant.contactDetails.state
        	};
	  
	  console.log(JSON.stringify(calculateJSON));
	  
	  this._CommonAPI.calculateINGAll(this.urlList.calculateINGDAll, calculateJSON).then(
		      (res) => {
		    	  
		    	  this.tpdLoadingCost = res[0].cost;
		    	  this.changeCoverData.applicant.cover[1].cost = parseFloat(this.changeCoverData.applicant.cover[1].cost) + ((parseFloat(this.changeCoverData.applicant.cover[1].loading)/100) * parseFloat(this.tpdLoadingCost));		    	  
		    	  this.tpdLoadingCost = (parseFloat(this.changeCoverData.applicant.cover[1].loading)/100) * parseFloat(this.tpdLoadingCost);
		    	  this.changeCoverData.applicant.cover[1].policyFee = Math.round((this.tpdLoadingCost + 0.00001)*100)/100;
		    	  this.tpdLoadingCalculated = true;
		    	  deferred.resolve({});		    	  
		      },
		      (err) => {
		          this.errors = err.data.errors;
		          deferred.reject(err);
		        }
	);
	  
	  return deferred.promise;
  }
  
  ipLoadingCalculation(){
	  
	  let deferred = this._$q.defer();
	  
	  this.ipAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount) - parseInt(this.changeCoverData.applicant.cover[2].existingCoverAmount);
	  
	  if(this.ipAmount == 0){
		  if(this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod != this.changeCoverData.applicant.cover[2].existingIpBenefitPeriod || this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod != this.changeCoverData.applicant.cover[2].existingIpWaitingPeriod){
			  this.ipAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount);
		  }
	  }else if(this.ipAmount < 0){
		  this.ipAmount = parseInt(this.changeCoverData.applicant.cover[2].additionalCoverAmount);
	  }
	  
	  const calculateJSON = {
			  'age': this.changeCoverData.applicant.age,
			  'fundCode': 'INGD',
			  'gender': this.changeCoverData.applicant.gender,
			  'ipOccCategory': this.changeCoverData.applicant.cover[2].occupationRating,
			  'smoker': false,
			  'premiumFrequency': this.changeCoverData.applicant.cover[2].frequencyCostType,
			  'manageType': 'CCOVER',
			  'ipCoverType': this.changeCoverData.applicant.cover[2].coverCategory,
			  'ipFixedAmount': this.ipAmount,
			  'ipWaitingPeriod': this.changeCoverData.applicant.cover[2].additionalIpWaitingPeriod,
	      	  'ipBenefitPeriod': this.changeCoverData.applicant.cover[2].additionalIpBenefitPeriod,
	      	  'stateCode': this.changeCoverData.applicant.contactDetails.state,
	      	  'tpdFixedAmount': this.changeCoverData.applicant.cover[1].additionalCoverAmount,
	      	  'deathFixedAmount': this.changeCoverData.applicant.cover[0].additionalCoverAmount,
        	};
	  
	  console.log(JSON.stringify(calculateJSON));
	  
	  this._CommonAPI.calculateINGAll(this.urlList.calculateINGDAll, calculateJSON).then(
		      (res) => {
		    	  
		    	  this.ipLoadingCost = res[2].cost;
		    	  this.changeCoverData.applicant.cover[2].cost = parseFloat(this.changeCoverData.applicant.cover[2].cost) + ((parseFloat(this.changeCoverData.applicant.cover[2].loading)/100) * parseFloat(this.ipLoadingCost));		    	  
		    	  this.ipLoadingCost = (parseFloat(this.changeCoverData.applicant.cover[2].loading)/100) * parseFloat(this.ipLoadingCost);
		    	  this.changeCoverData.applicant.cover[2].policyFee = Math.round((this.ipLoadingCost + 0.00001)*100)/100;
		    	  this.ipLoadingCalculated = true;
		    	  deferred.resolve({});		    	  
		      },
		      (err) => {
		          this.errors = err.data.errors;
		          deferred.reject(err);
		        }
	);
	  return deferred.promise;
	  
  }
  
  goToPath(path){
	  this.checkAndRemoveLoadingCost();
	  this._$state.go(path); 
  }
  
  previous(){
	  this.checkAndRemoveLoadingCost();
	  if(this.changeCoverData.auraDisabled === true || this.changeCoverData.auraDisabled === 'true'){
		  this._$state.go('app.quote');
	  }else{
		  this._$state.go('app.quoteaura');
	  }
	  
  }
  
  saveAndExitPopUp(hhText) {	  
	  this._ngDialog.openConfirm({
		  template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Finish &amp; Close Window </button></div></div>',
		  plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
      }).then(
      () => {
        this._$state.go('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }
  save() {
	  
	  this.changeCoverData.lastSavedOn = 'summarypage';
	  
	  this.checkAndRemoveLoadingCost();
	  
	  console.log(JSON.stringify(this.changeCoverData));
	  this._CommonAPI.saveEapply(this.urlList.saveEapplyUrlNew, this.changeCoverData).then(
	      (res) => {
	        console.log(res);
	        this.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+this.changeCoverData.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
	      },
	      (err) => {
	        this.errors = err.data.errors;
	      }
	   );
  
  }
  
  checkAndRemoveLoadingCost(){
	  if(this.enableLoadingSection){
		  
		  if(this.changeCoverData.applicant.cover[0].loading > 0){
			  this.changeCoverData.applicant.cover[0].cost = parseFloat(this.changeCoverData.applicant.cover[0].cost) - parseFloat(this.changeCoverData.applicant.cover[0].policyFee);
		  }
		  
		  if(this.changeCoverData.applicant.cover[1].loading > 0){
			  this.changeCoverData.applicant.cover[1].cost = parseFloat(this.changeCoverData.applicant.cover[1].cost) - parseFloat(this.changeCoverData.applicant.cover[1].policyFee);
		  }
		  
		  if(this.changeCoverData.applicant.cover[2].loading > 0){
			  this.changeCoverData.applicant.cover[2].cost = parseFloat(this.changeCoverData.applicant.cover[2].cost) - parseFloat(this.changeCoverData.applicant.cover[2].policyFee);
		  }
		  
		  this.changeCoverData.totalMonthlyPremium = parseFloat(this.changeCoverData.applicant.cover[0].cost) + parseFloat(this.changeCoverData.applicant.cover[1].cost) + parseFloat(this.changeCoverData.applicant.cover[2].cost); 
	  }
  }
  
  submit() {
	  
	  const _this = this;
	  
	  _this._$scope.termsForm.$submitted = true;
	  _this._$scope.loadingExculsionForm.$submitted = true;
	  
	  if(_this._$scope.termsForm.$valid && _this._$scope.loadingExculsionForm.$valid){
		  
		  /*_this._$state.go('app.quotedecision');
		   return false;*/
		  
		   console.log(JSON.stringify(_this.changeCoverData));
		  
		  _this._CommonAPI.submitEapply(_this.urlList.submitEapplyUrlNew, _this.changeCoverData).then(
	              (res) => {
	            	  console.log(res);
	            	  _this._EapplyData.setPdfLocation(res.clientPDFLocation);
	            	  if(res.npsTokenUR){
	            		  _this._EapplyData.setNpsUrl(res.npsTokenURL);
	            	  }	            	  
	            	  _this._$state.go('app.quotedecision');
	              }, 
	              (err) => {
	                console.log('Error while submitting transfer cover ' + err);
	              }
	       );
		  
	  }
  }
}

export default QuoteSummaryCtrl;
