<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style> html{display : none ; } </style>
<script> if( self == top ) { document.documentElement.style.display = 'block' ; } else { document.write(''); }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VicSuper</title>
<link rel="shortcut icon" href="static/vicsuper/images/favicon.ico">
<link rel="stylesheet" href="https://www.e2e.eapplication.metlife.com.au/eApplication/xAdvisorWeb/CMS/Group/VICS/css/eservicestyle.css" media="screen"  />

<script type="text/javascript">
	function formSubmit(){
		var errorStatus = false;
		if(document.getElementById('input_string_id').value == "" || document.getElementById('input_string_id').value == null ){
			document.getElementById('errorMessage1').innerHTML = "Input String is a mandatory field";
			errorStatus = true;
		}else{
		document.getElementById('HDInputString').value=document.getElementById('input_string_id').value;
		}
		if(!errorStatus){
			document.instform.submit(); 
		}
		
		
	}
function openLegalNoticenew(value)
{
//alert(value);
var sUrl = '/eApplication/faces/xAdvisorWeb/bundles/institutional/login/LegalNotice.jsp?fundID=NSFS';
window.open(sUrl,"Window1","menubar=no,width=1000,height=1200,toolbar=no,resizable=yes,scrollbars=yes");

//alert(value);
// write to window
//Window1.document.writeln(value); 
}
</script>
</head>

<body>
<div class="outerWrap"> 
  <!-- Top navigation container starts -->
  <div class="topNavCont">
    <div class="cornerTopLeft"></div>
    <div class="topNavContent">
      <ul class="topNav">
        <li class="cornerLeft"></li>
        <li> <a href="https://www.vicsuper.com.au/about-us" target="_new" title="About us"> About us </a> </li>
        <li>|</li>
        <li> <a href="https://www.vicsuper.com.au/contact-us" target="_new" title="Contact us"> Contact us </a> </li>
        <li class="cornerRight"></li>
      </ul>
    </div>
    <div class="cornerTopRight"></div>
    <!-- Top navigation container ends --> 
  </div>
  
  <!-- Main Container starts -->
  <div class="mainCont"> 
    <!-- Logo container starts -->
    <div class="logoCont">
      <div class="logoLeft"> <a href="#"> <img src="static/vicsuper/images/logoApplication.png" alt="" title="VicSuper" /></a> </div>
      <div class="logoRight none"> </div>
    </div>
    <!-- Logo container ends --> 
    
    <!-- Content container starts -->
    <div class="contentCont">

      <div class="leftCol">
        <div class="titleCont">
          <h1 class="title">Login</h1>
        </div>
        <h2 class="subTitle">Please enter the following information.</h2>
        <div class="blueLine"></div>
       <form name="instform" id="instform"  action="/eapply/vicsuperWebStub">
       <input type="hidden" name=HDInputString id="HDInputString" value=""/>
	   <input type="hidden" name="emailID" id="emailID" value="sambamurthy.lade@metlife.com"/>
	   <input type="hidden" name="request_type" id="request_type" value="newBusiness"/>
	   <input type="hidden" name="fund_id" id="fund_id" value="VICT"/>
	   <input type="hidden" name="stubRespPage_id" id="stubRespPage_id" value="vicSuperStubResp.jsp"/>
          <!-- Forms started -->
         			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td valign="top" style="padding-left:5px; padding-right:5px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						style="height: 5px">
						<tr> 
							<td>
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tbody>
									
										<tr class="td_alt_white">
											<td width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><label class="label">Input String</label></td>
											<td><div id="errorMessage1" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:red;font-size:13px;"> </div></td>
										</tr>
										<tr>
											<td colspan="2" width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><textarea class="medium" rows="5" cols="100" id="input_string_id" name="inputString"></textarea></td>
										</tr>
										<tr>
											<td>
											<textarea rows="5" cols="100" ><request><adminPartnerID>VICT</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>4</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>VICT</fundID><fundEmail>sambamurthy.lade@metlife.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>VICT</partnerID><product></product><campaign_code></campaign_code><tracking_code></tracking_code><promotioncode>https://onlinesit.vspl.com.au/sonataweb/ria/employee/accounts/change-insurance?accountId=100455968</promotioncode><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>met70</clientRefNumber><applicantRole>1</applicantRole><memberType>Employee saver</memberType><memberDivision>Employee saver</memberDivision><siopEndDate>12/12/1986</siopEndDate><segmentCode></segmentCode><personalDetails><firstName>PVTTEST</firstName><lastName>PVTTEST</lastName><dateOfBirth>12/11/1956</dateOfBirth><gender>male</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><units>0</units><indexation>Y</indexation><amount>500000.0</amount><loading>0.0</loading><exclusions></exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><units>0</units><indexation>Y</indexation><amount>500000.0</amount><loading>0.0</loading><exclusions></exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>1</type><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions></exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>sambamurthy.lade@metlife.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>
											</textarea>
											</td>
										</tr>								
									</tbody>
								</table>
								<br />

							</td>
						</tr>

						

					</table> 
					</td>
				</tr>
			</table>
          <div class="btnHolder fright">
             <a href="#" class="submit" onclick="formSubmit();"><span>PROCEED TO NEXT PAGE</span></a>
          </div>
          <!-- Forms ends -->
        </form>

      </div>
     <div class="rightCol"> 
        <!-- Rounded edge box starts -->        
        <div class="boxCont">
          <div class="popupCont">
          	<div class="popupContRight">
                <h2 class="subHead">Customer service</h2>
                <div class="greyWhite"></div>
                <span class="icoPhone"></span>
                <h2 class="tel">1300 366 216</h2>
            </div>
          </div>
          <div class="bottomCont">
            <div></div>
          </div>
        </div>
        <!-- Rounded edge box ends --> 
      </div>
      <!-- Content container ends --> 
    </div>
    <!-- Main Container ends --> 
      <div class="footerCont">
	    <ul class="footer">
	      <li><a href="https://www.vicsuper.com.au/privacy" target="_new" title="Privacy policy">Privacy policy</a></li>
	      <li>|</li>
	     <li><a href="https://www.vicsuper.com.au/disclaimer" class="" target="_new" >Disclaimer</a></li>
	    </ul>
	    <p class="footeTxt">VicSuper Pty Ltd ABN 69 087 619 412, VicSuper Fund 85 977 964 496, AFSL No. 237333</p>
	  </div>
  </div>

  
  <!-- Bottom container starts -->
  <div class="footerCurve">
    <div class="cornerBottomLeft"></div>
    <div class="BottomMid">&nbsp;</div>
    <div class="cornerBottomRight"></div>
    <!-- Bottom container ends --> 
  </div>
</div>
</body>
</html>
