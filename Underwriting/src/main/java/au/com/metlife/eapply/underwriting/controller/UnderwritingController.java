package au.com.metlife.eapply.underwriting.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.underwriting.model.AuraDecision;
import au.com.metlife.eapply.underwriting.model.AuraQuestion;
import au.com.metlife.eapply.underwriting.model.AuraQuestionnaire;
import au.com.metlife.eapply.underwriting.model.AuraResponse;
import au.com.metlife.eapply.underwriting.model.Quote;
import au.com.metlife.eapply.underwriting.service.UnderwritingService;


@RestController
public class UnderwritingController {


	@Autowired 
	UnderwritingService  uwService;
	
	
    @RequestMapping("/uw")
    public Quote greeting(@RequestParam(value="name", defaultValue="World") String name) {
    	Quote q=new Quote("Hello");
    	//QuoteService qs=new QuoteServiceImpl();
        return  uwService.getQuote(q);
    }
    
    
    
    @RequestMapping(value = "/initiate", method = RequestMethod.POST)
    public ResponseEntity <AuraQuestionnaire> listXML(InputStream inputStream) throws IOException, JSONException {	
    	
    	String inputXml = null;
    	 Calendar cal = Calendar.getInstance();
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
     	 String deathQfilter = null;
    	 String tpdQfilter = null;
    	 String ipQfilter = null;    	 
    	 String deathProduct = null;
    	 String tpdProduct = null;
    	 String ipProduct = null;    	 
    	 Integer addBenPeriod = 0;
    	 Integer addWaitPeriod = 0;  	 
    	StringBuffer questionFilter = new StringBuffer();
    	StringBuffer products = new StringBuffer();
    	String line = null;    	
    	 String string = "";	
    	 String benefitPeriod = null;
    	 Integer deathAmt = null;
    	 Integer tpdAmt = null;
    	 String waitingPeriod = null;
    	 Integer ipAmt = null;
    	 String mode = null;
    	 String fund = null;
    	 String age = null;
    	 String name = null;
    	 String xml = null;
    	 Long appnumber = null;
    	 //
    	 String occupation =null;
    	 String country =null;
    	 String gender = null;   
    	 String salary = null;
    	 String fiftennHr = null;
    	while ((line = in.readLine()) != null) { 
    		string += line + "\n";
    		System.out.println("line>>"+line);	
    		JSONObject json = new JSONObject(string);        
    		name = (String)json.get("name");
        	 age =  (Integer)json.get("age")+"";
        	 if(json.get("deathAmt")!=null && !(json.get("deathAmt").toString()).equalsIgnoreCase("null")){
        		 deathAmt =  (Integer)json.get("deathAmt");
        	 }
        	 if(json.get("tpdAmt")!=null && !(json.get("tpdAmt").toString()).equalsIgnoreCase("null")){
        		 tpdAmt =  (Integer)json.get("tpdAmt");
        	 }
        	 if(json.get("ipAmt")!=null && !(json.get("ipAmt").toString()).equalsIgnoreCase("null")){
        		 ipAmt =  (Integer)json.get("ipAmt");
        	 }
        	 waitingPeriod = (String)json.get("waitingPeriod");
        	 benefitPeriod =  (String)json.get("benefitPeriod");
        	 mode =  (String)json.get("mode");
        	 fund =  (String)json.get("fund");
        	 fund =  (String)json.get("fund");
        	 appnumber = (Long)json.get("appnumber");
        	 if(json.get("gender") != null){
        		 gender = (String)json.get("gender"); 
        	 }
        	 
        	 occupation =  (String)json.get("industryOcc");
        	 if(json.get("country")!=null && "Australia".equalsIgnoreCase((String)json.get("country"))){
        		 country = "Yes";
        	 }else{
        		 country = "No";
        	 }       	    	 
        	 salary = (String)json.get("salary");
        	 if(json.get("fifteenHr")!=null){
        		 fiftennHr =  (String)json.get("fifteenHr");
        	 }
        	
    	}    
  
    	 String auraID= appnumber+"_"+fund;//"test11-"+new java.util.Date().getTime();   
  
    	 
    	 
    	 if(deathAmt!=null  && deathAmt>0){    		 
    		 deathQfilter = "<QuestionFilter>Term-Group</QuestionFilter>";
    		 questionFilter.append(deathQfilter);
    		 deathProduct = "<Product AgeCoverageAmount=\""+deathAmt+"\" AgeDistrCoverageAmount=\""+deathAmt+"\" CoverageAmount=\""+deathAmt+"\" FinancialCoverageAmount=\""+deathAmt+"\">Term-Group</Product>";
    		 products.append(deathProduct);
    	 }
    	 if(tpdAmt!=null && tpdAmt>0){
    		 tpdQfilter = "<QuestionFilter>TPD-Group</QuestionFilter>";
    		 questionFilter.append(tpdQfilter);
    		 tpdProduct = "<Product AgeCoverageAmount=\""+tpdAmt+"\" AgeDistrCoverageAmount=\""+tpdAmt+"\" CoverageAmount=\""+tpdAmt+"\" FinancialCoverageAmount=\""+tpdAmt+"\">TPD-Group</Product>";
    		 products.append(tpdProduct);
    	 }
    	 if(ipAmt!=null && ipAmt>0){
    		 
    		 if(benefitPeriod!=null && benefitPeriod.contains("Years")){
 				addBenPeriod = Integer.parseInt(benefitPeriod.substring(0,1).trim());
    		 }else if(benefitPeriod!=null && benefitPeriod.contains("Age")){
 				addBenPeriod = Integer.parseInt(benefitPeriod.substring(4, 6).trim());
    		 }     	 
     	 
    		if(waitingPeriod!=null && waitingPeriod.contains("820")){
 				addWaitPeriod = Integer.parseInt(waitingPeriod.substring(0, 3).trim());
 			}else if(waitingPeriod!=null ){
 				addWaitPeriod = Integer.parseInt(waitingPeriod.substring(0, 2).trim());
 			}
    		
    		if(addWaitPeriod!=null && addBenPeriod!=null ){
    			String ipProductName = "IP W"+addWaitPeriod+ " B"+addBenPeriod+"-Group";
       		 ipQfilter = "<QuestionFilter>"+ipProductName+"</QuestionFilter>";
       		 questionFilter.append(ipQfilter);
       		 ipProduct = "<Product AgeCoverageAmount=\""+ipAmt+"\" AgeDistrCoverageAmount=\""+ipAmt+"\" CoverageAmount=\""+ipAmt+"\" FinancialCoverageAmount=\""+ipAmt+"\">"+ipProductName+"</Product>";
       		 products.append(ipProduct);
    		}
    		 
    	 }
    	 
    	if(mode!=null && mode.length()>0){
    		switch (mode) {
    		case "TransferCover":
    			 xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
    		 	    		+ "<UniqueID>"+auraID+"</UniqueID>"
    		 	    		+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl>"
    		 	    		+ "<QuestionFilters>"
    		 	    		+questionFilter.toString() 	    	
    		 	    		+"</QuestionFilters>"
    		 	    		+ "<PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
    		 	    		+ "<Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\""+name+"\" UniqueId=\"1\" Waived=\"false\">"
    		 	    		+ "<Products>"
    		 	    		+products.toString() 	    		
    		 	    		+ "</Products>"
    		 	    		+ "<EngineVariables><Variable Name=\"Gender\">Female</Variable><Variable Name=\"Age\">"+age+"</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+"</Variable>"
    		 	    		+ "<Variable Name=\"Country\">"+country+"</Variable><Variable Name=\"Salary\">"+salary+"</Variable><Variable Name=\"SumInsuredTerm\">"+deathAmt+"</Variable><Variable Name=\"SumInsuredTPD\">"+tpdAmt+"</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">"+ipAmt+"</Variable>"
    		 	    		+ "<Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable>"
    		 	    		+ "<Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">TransferCover</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			break;
    		case "WorkRating":
    			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
		 	    		+ "<UniqueID>"+auraID+"</UniqueID>"
		 	    		+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl>"
		 	    		+ "<QuestionFilters>"
		 	    		+questionFilter.toString() 	    	
		 	    		+"</QuestionFilters>"
		 	    		+ "<PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
		 	    		+ "<Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\""+name+"\" UniqueId=\"1\" Waived=\"false\">"
		 	    		+ "<Products>"
		 	    		+products.toString() 	    		
		 	    		+ "</Products>"
		 	    		+ "<EngineVariables><Variable Name=\"Gender\">Female</Variable><Variable Name=\"Age\">"+age+"</Variable><Variable Name=\"Occupation\">"+occupation+"</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+"</Variable>"
		 	    		+ "<Variable Name=\"Country\">"+country+"</Variable><Variable Name=\"Salary\">"+salary+"</Variable><Variable Name=\"SumInsuredTerm\">"+deathAmt+"</Variable><Variable Name=\"SumInsuredTPD\">"+tpdAmt+"</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">"+ipAmt+"</Variable>"
		 	    		+ "<Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable>"
		 	    		+ "<Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">WorkRating</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			break;
    		case "SpecialOffer":
    			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
		 	    		+ "<UniqueID>"+auraID+"</UniqueID>"
		 	    		+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl>"
		 	    		+ "<QuestionFilters>"
		 	    		+questionFilter.toString() 	    	
		 	    		+"</QuestionFilters>"
		 	    		+ "<PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
		 	    		+ "<Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\""+name+"\" UniqueId=\"1\" Waived=\"false\">"
		 	    		+ "<Products>"
		 	    		+products.toString() 	    		
		 	    		+ "</Products>"
		 	    		+ "<EngineVariables><Variable Name=\"Gender\">Female</Variable><Variable Name=\"Age\">"+age+"</Variable><Variable Name=\"Occupation\">026:Transfer</Variable><Variable Name=\"SpecialTerms\">Yes</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+"</Variable>"
		 	    		+ "<Variable Name=\"Country\">"+country+"</Variable><Variable Name=\"Salary\">"+salary+"</Variable><Variable Name=\"SumInsuredTerm\">"+deathAmt+"</Variable><Variable Name=\"SumInsuredTPD\">"+tpdAmt+"</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">"+ipAmt+"</Variable>"
		 	    		+ "<Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable>"
		 	    		+ "<Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">SpecialOffer</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			break;
    		default:
    			xml= "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><AURATX><AuraControl>"
    	    			+ "<UniqueID>"+auraID+"</UniqueID>"
    	    			+ "<Company>metaus</Company><SetCode>93</SetCode><SubSet>2</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage /></AuraControl>"
    	    			+ "<QuestionFilters>"
    	    			+questionFilter.toString()
    	    			+ "</QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring /><SeasonSummer /><SeasonFall /><SeasonWinter /></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Group</Variable></EngineVariables>"
    	    					+ "<Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\""+name+"\" UniqueId=\"1\" Waived=\"false\">"
    	    							+ "<Products>"
    	    							+ products.toString() 	
    	    							+ "</Products><EngineVariables><Variable Name=\"Gender\">"+gender+"</Variable><Variable Name=\"Age\">"+age+"</Variable><Variable Name=\"Occupation\">"+occupation+"</Variable><Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\">No</Variable><Variable Name=\"Hours15\">"+fiftennHr+"</Variable><Variable Name=\"Country\">"+country+"</Variable><Variable Name=\"Salary\">"+salary+"</Variable><Variable Name=\"SumInsuredTerm\">"+deathAmt+"</Variable><Variable Name=\"SumInsuredTPD\">"+tpdAmt+"</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">"+ipAmt+"</Variable><Variable Name=\"Partner\">"+fund+"</Variable><Variable Name=\"HideFamilyHistory\"></Variable><Variable Name=\"HidePastTime\">No</Variable><Variable Name=\"GroupPool\">Industry Fund</Variable><Variable Name=\"SchemeName\">"+fund+"</Variable><Variable Name=\"FormLength\">Long</Variable><Variable Name=\"OccupationScheme\">INDUSTRY OCCUPATION</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
    			break;
    		}
    	}    
    	
    	AuraQuestionnaire aq =uwService.getAllAuraQuestions(xml);
        if(aq==null){
            return new ResponseEntity<AuraQuestionnaire>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
                    
        }
        
       // System.out.println("END--"+new  java.util.Date());
        return new ResponseEntity<AuraQuestionnaire>(aq, HttpStatus.OK);
    	   
    	
    }
    

@RequestMapping(value = "/underwriting", method = RequestMethod.POST)
public  ResponseEntity <AuraQuestion> updateUnderwriting(@RequestBody AuraResponse res,    UriComponentsBuilder ucBuilder) {
	
	
	AuraQuestion q=uwService.updateAuraQuestion(res);
	
    if(q==null){
        return new ResponseEntity<AuraQuestion>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
    }
    System.out.println("q.getAnswerTest()>>"+q.getAnswerTest());
    System.out.println("END--"+new  java.util.Date());
    return new ResponseEntity<AuraQuestion>(q, HttpStatus.OK);
    

}
@RequestMapping(value = "/submitAura", method = RequestMethod.POST)
public ResponseEntity <AuraDecision> submitAuraSession(InputStream inputStream) throws IOException, JSONException {	
	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
	String line = null;
	 String string = "";	
	 JSONObject json = null;
	 Long auraSessionId = null;
	 String clientname = null;
	 AuraDecision responseObject = null;
	 String fund = null;
	 String firstName = null;
	 String surName = null;
	 String dob = null;
	while ((line = in.readLine()) != null) { 
		string += line + "\n";
		json = new JSONObject(string);        
		clientname = (String)json.get("clientname");
		auraSessionId = (Long)json.get("appnumber");
		fund = (String)json.get("fund");
		if(json.get("firstName")!=null)
			firstName = (String)json.get("firstName");
		if(json.get("lastName")!=null)
			surName =(String)json.get("lastName");
		if(json.get("dob")!=null)
			dob =(String)json.get("dob");
	}
	if(clientname!=null && clientname.length()>0 && auraSessionId!=null){
		responseObject = uwService.submitAuraSession(clientname, auraSessionId+"_"+fund);
		//client match call to GL/SCI
		/*String ca=  oDSSearch(firstName, surName, dob);
		if(null != ca && (ca.contains("GL") || ca.contains("SCI"))){
			responseObject.setClientMatched(true);
			
			if(null != responseObject.getClientMatchReason()){
				ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
				responseObject.setClientMatchReason(responseObject.getClientMatchReason()+" AND "+ca);
			}else{
				ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
				responseObject.setClientMatchReason(ca);
			}
			
		}*/
	}	
	if(responseObject==null){
        return new ResponseEntity<AuraDecision>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
    }
	 return new ResponseEntity<AuraDecision>(responseObject, HttpStatus.OK);
}

/*public static String oDSSearch(String firstName, String surName, Date dob ) {
	*//**
	 * Checking the GL/SCI system for a client match>>
	 *//*
	
	String oDSSearchWebServiceURL = "https://www.e2e.equery.metlife.com.au/CAW2/services/EAPPSearch";// ConfigurationHelper.getConfigurationValue("OdsSearch", "weserviceURL").trim(); //"https://www.e2e.eapplication.metlife.com.au/CAW2/services/EServicing?wsdl";//System.getProperty("policySearchWebServiceURL"); //http://localhost:9080/CAW2/services/EServicing?wsdl
	
	EAPPSearchStub  stub1=null; 
	String ca= null;
	boolean status = false; 
	try {
		
		stub1 = new EAPPSearchStub(ServiceClientInstance.getInstance(),oDSSearchWebServiceURL);		 
		ServiceClient client = stub1._getServiceClient();		
		client.engageModule("rampart");
			
		Options options = client.getOptions();
		options.setTimeOutInMilliSeconds(100000);
		options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, getOutflowConfiguration("internal01"));
		PWCBClientHandler myCallback = new PWCBClientHandler();
		myCallback.setUTUsername("internal01");
		myCallback.setUTPassword("metlife980");
		options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
		client.setOptions(options);
		
		EAPPSearchStub.SearchExistingMember req = new EAPPSearchStub.SearchExistingMember();
		SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String dateString = formatter.format(dob);

        if(null == surName){
        	surName = "";
        }
        
		req.setInputXml("<Root><message>" +
    			"<Filters>" +
    			"<Filter>" +
    			"<FilterName>firstname</FilterName>" +
    			"<FilterValue>"+firstName+"</FilterValue>" +
    			"</Filter>" +
    			"<Filter>" +
    			"<FilterName>surname</FilterName>" +
    			"<FilterValue>"+surName+"</FilterValue>" +
    			"</Filter>" +
    			"<Filter>" +
    			"<FilterName>dob</FilterName>" +
    			"<FilterValue>"+dateString+"</FilterValue>" +
    			"</Filter>" +
    			"</Filters>" +
    			"</message></Root>");
		
		
        System.out.println("For GLSCI Search Last Name :"+req.getInputXml());
        
        
        
        EAPPSearchStub.SearchExistingMemberResponse res = stub1.searchExistingMember(req);
		System.out.println(res.get_return());
        ca = res.get_return();


		
	}catch (AxisFault e) {			
		e.printStackTrace();
	}catch (Exception e) {			
		e.printStackTrace();
	}
	
	return ca;
	
}*/

/*private static Parameter getOutflowConfiguration(String username) {
	 final String METHOD_NAME = "getOutflowConfiguration";	//$NON-NLS-1$
       OutflowConfiguration ofc = new OutflowConfiguration();
       ofc.setActionItems("UsernameToken Timestamp");
       ofc.setPasswordType("PasswordText");
       ofc.setUser(username);	       
       return ofc.getProperty();
   }*/
}
