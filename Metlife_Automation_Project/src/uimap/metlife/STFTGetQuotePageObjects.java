package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTGetQuotePageObjects extends PageObjectLibrary{
		
	public static final By txtFirstName = getObjectIdentifaction("stft.getquote.firstname.text.id");
	public static final By txtLastName = getObjectIdentifaction("stft.getquote.lastname.text.id");
	public static final By txtDOB = getObjectIdentifaction("stft.getquote.dob.text.css");
	public static final By rdMale = getObjectIdentifaction("stft.getquote.gender.male.radio.id");
	public static final By rdFemale = getObjectIdentifaction("stft.getquote.gender.female.radio.id");
	public static final By rdSmokedIn12MthsYes = getObjectIdentifaction("stft.getquote.smokedin12months.yes.radio.id");
	public static final By rdSmokedIn12MthsNo = getObjectIdentifaction("stft.getquote.smokedin12months.no.radio.id");
	public static final By rdCitizenYes = getObjectIdentifaction("stft.getquote.citizen.yes.radio.id");
	public static final By rdCitizenNo = getObjectIdentifaction("stft.getquote.citizen.no.radio.id");
	public static final By selIndustry = getObjectIdentifaction("stft.getquote.Industry.dropdown.css");
	public static final By selOccupation = getObjectIdentifaction("stft.getquote.Occupation.dropdown.css");
	
	public static final By txtLifeCoverAmt = getObjectIdentifaction("stft.getquote.coveramount.text.id");	
	public static final By txtTotalCoverAmt = getObjectIdentifaction("stft.getquote.txtTotalCoverAmt.text.id");	
	public static final By selFrequency = getObjectIdentifaction("stft.getquote.frequency.dropdown.css");
	public static final By rdBrokerYes = getObjectIdentifaction("stft.getquote.broker.client.yes.radio.id");
	public static final By rdBrokerNo = getObjectIdentifaction("stft.getquote.broker.client.no.radio.id");
	public static final By btnCalculateQuote = getObjectIdentifaction("stft.getquote.calculatequote.button.css");
	public static final By  btnApply = getObjectIdentifaction("stft.getquote.apply.button.css");

	public static final By lblDCCostAmt = getObjectIdentifaction("stft.getquote.dcCost.xpath");
	public static final By lblTPDCostAmt = getObjectIdentifaction("stft.getquote.tpdCost.xpath");
	public static final By lblPFCostAmt = getObjectIdentifaction("stft.getquote.pfCost.xpath");
	public static final By lblEstTotCostAmt = getObjectIdentifaction("stft.getquote.etCost.xpath");
	
}
