package au.com.metlife.eapply.bs.utility;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;

public class HTMLToString {
	private HTMLToString(){
		
	}
    

	
    protected static final String[] dictionary = { "</P>", "</p>", "<BR>", "<br>",
        "<LI>", "<li>", MetlifeInstitutionalConstants.UL, MetlifeInstitutionalConstants.UL_CASE, "<P>", "<p>", "<U>", "<B>", "</U>",
        "</B>", "<u>", "<b>", "</u>", "</b>", MetlifeInstitutionalConstants.LI, MetlifeInstitutionalConstants.LI_SPACE, MetlifeInstitutionalConstants.U_STRONG,
        MetlifeInstitutionalConstants.U_STRONG_CASE, MetlifeInstitutionalConstants.STRONG_CASE_U, MetlifeInstitutionalConstants.STRONG_U, MetlifeInstitutionalConstants.STRONG_CASE_WITH,
        MetlifeInstitutionalConstants.STRONG_WITHOUT, MetlifeInstitutionalConstants.STRONG_CASE, MetlifeInstitutionalConstants.STRONG, "<UL>", "<ul>", "<A", "<a",
        MetlifeInstitutionalConstants.SPAN, MetlifeInstitutionalConstants.SPAN_CASH, "</a>", "</A>", MetlifeInstitutionalConstants.SPAN_WITH, MetlifeInstitutionalConstants.SPAN_WITH_SPACE , MetlifeInstitutionalConstants.BR_SPACE, 
        MetlifeInstitutionalConstants.DIV, "/DIV", MetlifeInstitutionalConstants.P_CLASS, MetlifeInstitutionalConstants.UL_CLASS, "<P class=\"normal\">",
        MetlifeInstitutionalConstants.UL_CLASS, MetlifeInstitutionalConstants.DIV_CLASS, "<DIV class=\"desc\">", MetlifeInstitutionalConstants.UL_CLASS, MetlifeInstitutionalConstants.UL_CLASS};
    
    public static String convertHTMLToString(String source) {		
    	
    	String src = source;
	if(src!=null) {
	       /* boolean flag = false;*/
	        	        
	        if (src.contains( "<P>")) {
	            src = src.replaceAll( "<P>", "");
	        }
	        if (src.contains( "<p>")) {
	            src = src.replaceAll( "<p>", "");
	        }
	        if (src.contains( "</P>")) {
	            src = src.replaceAll( "</P>", "\n");
	        }	
	        if (src.contains( "</p>")) {
	            src = src.replaceAll( "</p>", "\n");
	        }
	        if (src.contains(MetlifeInstitutionalConstants.P_STYLE)) {
	            src = src.replaceAll(MetlifeInstitutionalConstants.P_STYLE, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.BR_CASE)) {
	            src = src.replaceAll(MetlifeInstitutionalConstants.BR_CASE, "\n\n");
	        }
	        if (src.contains( "<BR>")) {
	            src = src.replaceAll( "<BR>", "\n");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.BR_SINGLE_CASE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.BR_SINGLE_CASE, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.BR_SINGLE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.BR_SINGLE, "");
	        }
	        if (src.contains( "<br>")) {
	            src = src.replaceAll( "<br>", "\n");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.BR)) {
	            src = src.replaceAll(MetlifeInstitutionalConstants.BR, "\n\n");
	        }
	        if(src.contains(MetlifeInstitutionalConstants.SPAN_STYLE)){
	        	 src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_STYLE, "");
	        }
	        if(src.contains(MetlifeInstitutionalConstants.SPAN_WITH)){
	        	 src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_WITH, "");
	        } 	     
	        if (src.contains( MetlifeInstitutionalConstants.SPAN_WITH_SPACE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_WITH_SPACE, "");
	        }	        
	        if(src.contains(MetlifeInstitutionalConstants.STYLE_FONT)){
	        	 src = src.replaceAll( MetlifeInstitutionalConstants.STYLE_FONT, "");
	        } 

	        if (src.contains( "<SPAN ")) {
	            src = src.replaceAll( "<SPAN style='font-weight:bold;'>", "");
	        }
	        
	        if (src.contains( "<span ")) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_STYLE, "");
	        }
	        
	        if (src.contains( "<SPAN ")) {
	            src = src.replaceAll( "<SPAN style='color:blue;'>", "");
	        }
	        
	        if (src.contains( "<span ")) {
	            src = src.replaceAll( "<span style='color:blue;'>", "");
	        }
	        
	        if (src.contains( "<p ")) {
	            src = src.replaceAll( "<p style='padding:0 0 0 20px;line-height:17px;'>", "");
	        }
	        
	        if (src.contains( "<ul ")) {
	            src = src.replaceAll( "<ul style='padding:0 0 0 35px;list-style-type:disc;'>", "");
	        }
	      
	        if (src.contains( "<LI>")) {
	            src = src.replaceAll( "<LI>", "\t-\t");
	        }
	        if (src.contains( "<li>")) {
	            src = src.replaceAll( "<li>", "\t-\t");
	        }	
	        if (src.contains( MetlifeInstitutionalConstants.LI)) {
	            src = src.replace( MetlifeInstitutionalConstants.LI, "\n");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.LI_SPACE)) {
	            src = src.replace( MetlifeInstitutionalConstants.LI_SPACE, "\n");
	        }
	        if (src.contains( "<UL>")) {
	            src = src.replaceAll( "<UL>", "\t");
	        }
	        if (src.contains( "<ul>")) {
	            src = src.replaceAll( "<ul>", "\t");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.UL)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.UL, "\n\r");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.UL_CASE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.UL_CASE, "\n\r");
	        }       	      	       
	       if (src.contains(MetlifeInstitutionalConstants.UL_STYLE_PADDING)) {
	            src = src.replaceAll(MetlifeInstitutionalConstants.UL_STYLE_PADDING, "");
	        }
	        if (src.contains( "<U>")) {
	            src = src.replaceAll( "<U>", "");
	        }	       
	        if (src.contains( "</U>")) {
	            src = src.replaceAll( "</U>", "");
	        }
	        if (src.contains( "</B>")) {
	            src = src.replaceAll( "</B>", "");
	        }
	        if (src.contains( "<b>")) {
	            src = src.replaceAll( "<b>", "");
	        }
	        if (src.contains( "</b>")) {
	            src = src.replaceAll( "</b>", "");
	        }
	        if (src.contains( "<u>")) {
	            src = src.replaceAll( "<u>", "");
	        }	        
	        if (src.contains( "</u>")) {
	            src = src.replaceAll( "</u>", "");
	        }	     
	        if (src.contains( MetlifeInstitutionalConstants.U_STRONG)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.U_STRONG, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.U_STRONG_CASE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.U_STRONG_CASE, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.STRONG_CASE_U)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_CASE_U, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.STRONG_U)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_U, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.STRONG_CASE_WITH)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_CASE_WITH, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.STRONG_WITHOUT)) {
	            src = src.replaceAll( "</strong>", "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.STRONG_CASE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_CASE, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.STRONG)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG, "");
	        }
	        if (src.contains(MetlifeInstitutionalConstants.UL_STYLE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.UL_STYLE, "\t");
	        }
	        if (src.contains(MetlifeInstitutionalConstants.UL_STYLE_CASE)) {
	            src = src.replaceAll( MetlifeInstitutionalConstants.UL_STYLE_CASE, "\t");
	        }
	        
	        if(src.contains(MetlifeInstitutionalConstants.UL_STYLE_LINE_HEIGHT)){
	        	 src = src.replaceAll( MetlifeInstitutionalConstants.UL_STYLE_LINE_HEIGHT, "\t");
	        }
	        
	        if(src.contains("<P style=\"padding:0 0 0 20px;line-height:17px;\">")){
	        	 src = src.replaceAll( "<P style=\"padding:0 0 0 20px;line-height:17px;\">", "\t");
	        }
	        if(src.contains("<p style=\"padding-bottom:5px;line-height:20px;\">")){
	        	 src = src.replaceAll( "<p style=\"padding-bottom:5px;line-height:20px;\">", "");
	        }
	        if(src.contains("<ul style=\"padding:0px 0px 10px 25px;list-style-type:disc;line-height:25px \">")){
	        	 src = src.replaceAll( "<ul style=\"padding:0px 0px 10px 25px;list-style-type:disc;line-height:25px \">", "\t");
	        }
	        
	        if(src.contains("<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc;\">")){
	        	 src = src.replaceAll( "<UL style=\"line-height:140%;padding:0 0 0 35px;list-style-type:disc;\">", "\t");
	        }
	        if(src.contains("<UL style=\"padding:0 0 0 35px;line-height:15px;list-style-type:disc;\">")){
	        	 src = src.replaceAll( "<UL style=\"padding:0 0 0 35px;line-height:15px;list-style-type:disc;\">", "\t");
	        }
	        if(src.contains("<UL style=\"padding:0 0 0 35px;list-style-type:disc;\">")){
	        	 src = src.replaceAll( "<UL style=\"padding:0 0 0 35px;list-style-type:disc;\">", "\t");
	        }
	        
	        src = src.replaceAll("<A", "<a");
	        src = src.replaceAll("</A>", "</a>");
	        
	     
	        if (src.contains( "<A")) {
	            String tempS = src.substring( src.indexOf( "<A"), src.length() - 1);
	            String tempA = src.substring( src.indexOf( "<A"), src.indexOf( "<A")
	                    + tempS.indexOf( '>') + 1);
	            src = src.replace( tempA, "");
	        }
	        
	        if (src.contains( "<a")) {
	            
	            String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
	            String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
	                    + tempS.indexOf( '>') + 1);
	            src = src.replace( tempA, "");
	        }

	        if (src.contains( "<a")) {
  
			     String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
			     String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
			             + tempS.indexOf( '>') + 1);
			     src = src.replace( tempA, "");
			 }

			 if (src.contains( "<a")) {
			     
			     String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
			     String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
			             + tempS.indexOf( '>') + 1);
			     src = src.replace( tempA, "");
			 }
	        if (src.contains( "<a")) {
	            
	            String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
	            String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
	                    + tempS.indexOf( '>') + 1);
	            src = src.replace( tempA, "");
	        }
	        
	        if (src.contains( "<a")) {
	            
	            String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
	            String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
	                    + tempS.indexOf( '>') + 1);
	            src = src.replace( tempA, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.SPAN)) {
	            String tempS = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN), src.length() - 1);
	            String tempA = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN), src.indexOf( MetlifeInstitutionalConstants.SPAN)
	                    + tempS.indexOf( '>') + 1);
	            src = src.replaceAll( tempA, "");
	        }
	        if (src.contains( MetlifeInstitutionalConstants.SPAN_CASH)) {
	            String tempS = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN_CASH), src.length() - 1);
	            String tempA = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN_CASH), src.indexOf( MetlifeInstitutionalConstants.SPAN_CASH)
	                    + tempS.indexOf( '>') + 1);
	            src = src.replaceAll( tempA, "");
	        }
	        if (src.contains( "</a>")) {
	            src = src.replaceAll( "</a>", "");
	        }
	        if (src.contains( "</A>")) {
	            src = src.replaceAll( "</A>", "");
	        }  
	        
	       /* for (int i = 0; i < dictionary.length; i++) {
	            
	            if (( null != dictionary[i]) && src.contains( dictionary[i])) {
	                flag = true;
	                break;
	            }
	        }
	        if (flag) {
	            
	            src = convertHTMLToString( src);
	        }*/
 	}
     
     return src;
 }
    
    public static String convertHTMLToStringForInsCalc(String source) {		
    	
    	String src = source;
	
	if(src!=null) {
        if (src.contains( MetlifeInstitutionalConstants.BR_SPACE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.BR_SPACE, "\n");
        }
        if (src.contains( MetlifeInstitutionalConstants.DIV_CLASS)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.DIV_CLASS, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.P_CLASS)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.P_CLASS, "\n");
        }
        if (src.contains( MetlifeInstitutionalConstants.UL_CLASS)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.UL_CLASS, "");
        }
        if (src.contains( "&#38;")) {
            src = src.replaceAll( "&#38;", "&");
        }
        if (src.contains( "<div>")) {
            src = src.replaceAll( "<div>", "");
        }
        if (src.contains( MetlifeInstitutionalConstants.DIV)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.DIV, "");
        }
        if (src.contains( "<P>")) {
            src = src.replaceAll( "<P>", "");
        }
        if (src.contains( "<p>")) {
            src = src.replaceAll( "<p>", "");
        }
        if (src.contains( "</P>")) {
            src = src.replaceAll( "</P>", "\n");
        }	
        if (src.contains( "</p>")) {
            src = src.replaceAll( "</p>", "\n");
        }
        if (src.contains(MetlifeInstitutionalConstants.P_STYLE)) {
            src = src.replaceAll(MetlifeInstitutionalConstants.P_STYLE, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.BR_CASE)) {
            src = src.replaceAll(MetlifeInstitutionalConstants.BR_CASE, "\n\n");
        }
        if (src.contains( "<BR>")) {
            src = src.replaceAll( "<BR>", "\n");
        }
        if (src.contains( MetlifeInstitutionalConstants.BR_SINGLE_CASE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.BR_SINGLE_CASE, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.BR_SINGLE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.BR_SINGLE, "");
        }
        if (src.contains( "<br>")) {
            src = src.replaceAll( "<br>", "\n");
        }
        if (src.contains( MetlifeInstitutionalConstants.BR)) {
            src = src.replaceAll(MetlifeInstitutionalConstants.BR, "\n\n");
        }
        if(src.contains(MetlifeInstitutionalConstants.SPAN_STYLE)){
        	 src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_STYLE, "");
        }
        if(src.contains(MetlifeInstitutionalConstants.SPAN_WITH)){
        	 src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_WITH, "");
        } 	     
        if (src.contains( MetlifeInstitutionalConstants.SPAN_WITH_SPACE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.SPAN_WITH_SPACE, "");
        }	        
        if(src.contains(MetlifeInstitutionalConstants.STYLE_FONT)){
        	 src = src.replaceAll( MetlifeInstitutionalConstants.STYLE_FONT, "");
        }         
      
        if (src.contains( "<LI>")) {
            src = src.replaceAll( "<LI>", "\n\t-\t");
        }
        if (src.contains( "<li>")) {
            src = src.replaceAll( "<li>", "\n\t-\t");
        }	
        if (src.contains( MetlifeInstitutionalConstants.LI)) {
            src = src.replace( MetlifeInstitutionalConstants.LI, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.LI_SPACE)) {
            src = src.replace( MetlifeInstitutionalConstants.LI_SPACE, "\n");
        }
        if (src.contains( "<UL>")) {
            src = src.replaceAll( "<UL>", "\t");
        }
        if (src.contains( "<ul>")) {
            src = src.replaceAll( "<ul>", "\t");
        }
        if (src.contains( MetlifeInstitutionalConstants.UL)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.UL, "\n\r");
        }
        if (src.contains( MetlifeInstitutionalConstants.UL_CASE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.UL_CASE, "\n\r");
        }       	      	       
       if (src.contains(MetlifeInstitutionalConstants.UL_STYLE_PADDING)) {
            src = src.replaceAll(MetlifeInstitutionalConstants.UL_STYLE_PADDING, "");
        }
        if (src.contains( "<U>")) {
            src = src.replaceAll( "<U>", "");
        }	       
        if (src.contains( "</U>")) {
            src = src.replaceAll( "</U>", "");
        }
        if (src.contains( "</B>")) {
            src = src.replaceAll( "</B>", "");
        }
        if (src.contains( "<b>")) {
            src = src.replaceAll( "<b>", "");
        }
        if (src.contains( "</b>")) {
            src = src.replaceAll( "</b>", "");
        }
        if (src.contains( "<u>")) {
            src = src.replaceAll( "<u>", "");
        }	        
        if (src.contains( "</u>")) {
            src = src.replaceAll( "</u>", "");
        }	     
        if (src.contains( MetlifeInstitutionalConstants.U_STRONG)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.U_STRONG, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.U_STRONG_CASE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.U_STRONG_CASE, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.STRONG_CASE_U)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_CASE_U, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.STRONG_U)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_U, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.STRONG_CASE_WITH)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_CASE_WITH, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.STRONG_WITHOUT)) {
            src = src.replaceAll( "</strong>", "");
        }
        if (src.contains( MetlifeInstitutionalConstants.STRONG_CASE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG_CASE, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.STRONG)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.STRONG, "");
        }
        if (src.contains(MetlifeInstitutionalConstants.UL_STYLE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.UL_STYLE, "\t");
        }
        if (src.contains(MetlifeInstitutionalConstants.UL_STYLE_CASE)) {
            src = src.replaceAll( MetlifeInstitutionalConstants.UL_STYLE_CASE, "\t");
        }
        
        if(src.contains(MetlifeInstitutionalConstants.UL_STYLE_LINE_HEIGHT)){
        	 src = src.replaceAll( MetlifeInstitutionalConstants.UL_STYLE_LINE_HEIGHT, "\t");
        }
        
        src = src.replaceAll("<A", "<a");
        src = src.replaceAll("</A>", "</a>");
        
     
        if (src.contains( "<A")) {
            String tempS = src.substring( src.indexOf( "<A"), src.length() - 1);
            String tempA = src.substring( src.indexOf( "<A"), src.indexOf( "<A")
                    + tempS.indexOf( '>') + 1);
            src = src.replace( tempA, "");
            /*tempA = null;
            tempS = null;*/
        }
        
        if (src.contains( "<a")) {
            
            String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
            String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
                    + tempS.indexOf( '>') + 1);
            src = src.replace( tempA, "");
        }

        if (src.contains( "<a")) {
 
		     String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
		     String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
		             + tempS.indexOf( '>') + 1);
		     src = src.replace( tempA, "");
		 }

		 if (src.contains( "<a")) {
		     
		     String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
		     String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
		             + tempS.indexOf( '>') + 1);
		     src = src.replace( tempA, "");
		 }
        if (src.contains( "<a")) {
            
            String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
            String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
                    + tempS.indexOf( '>') + 1);
            src = src.replace( tempA, "");
        }
        
        if (src.contains( "<a")) {
            
            String tempS = src.substring( src.indexOf( "<a"), src.length() - 1);
            String tempA = src.substring( src.indexOf( "<a"), src.indexOf( "<a")
                    + tempS.indexOf( '>') + 1);
            src = src.replace( tempA, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.SPAN)) {
            String tempS = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN), src.length() - 1);
            String tempA = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN), src.indexOf( MetlifeInstitutionalConstants.SPAN)
                    + tempS.indexOf( '>') + 1);
            src = src.replaceAll( tempA, "");
        }
        if (src.contains( MetlifeInstitutionalConstants.SPAN_CASH)) {
            String tempS = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN_CASH), src.length() - 1);
            String tempA = src.substring( src.indexOf( MetlifeInstitutionalConstants.SPAN_CASH), src.indexOf( MetlifeInstitutionalConstants.SPAN_CASH)
                    + tempS.indexOf( '>') + 1);
            src = src.replaceAll( tempA, "");
        }
        if (src.contains( "</a>")) {
            src = src.replaceAll( "</a>", "");
        }
        if (src.contains( "</A>")) {
            src = src.replaceAll( "</A>", "");
        }  
        
       /* for (int i = 0; i < dictionary.length; i++) {
            
            if (( null != dictionary[i]) && src.contains( dictionary[i])) {
                flag = true;
                break;
            }
        }
        if (flag) {
            
            src = convertHTMLToString( src);
        }*/       
	}        
    return src;
}
}
