package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

public class EmployerDet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3901099806672452141L;
	
	private String employerCnctNum;
	private String employerEmail;
	private String employerKeyCnctName;
	public String getEmployerCnctNum() {
		return employerCnctNum;
	}
	public void setEmployerCnctNum(String employerCnctNum) {
		this.employerCnctNum = employerCnctNum;
	}
	public String getEmployerEmail() {
		return employerEmail;
	}
	public void setEmployerEmail(String employerEmail) {
		this.employerEmail = employerEmail;
	}
	public String getEmployerKeyCnctName() {
		return employerKeyCnctName;
	}
	public void setEmployerKeyCnctName(String employerKeyCnctName) {
		this.employerKeyCnctName = employerKeyCnctName;
	} 
	

}
