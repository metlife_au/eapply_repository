/* ------------------------------------------------------------------------------------------------- 
 * Description:   BizflowXMLHelper class is the helper class that is used to interact eapply with metflow applicaruion
 * -------------------------------------------------------------------------------------------------
 * Copyright � 2018 Metlife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :     Cognizant
 * Created:     02/02/2017
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -------------------------------------------------------------------------------------------------
 * Date				Author			Description   
 * 02/02/2017 	Anand Kishore		Base version
 * 19/06/2018   Prasanna durga      Added Change Application Category Codes passed to MetFlow 
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
package au.com.metlife.eapply.bs.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnection;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.mq.jms.MQQueueSender;
import com.ibm.mq.jms.MQQueueSession;

import au.com.metlife.eapply.bs.bizflowmodel.Address;
import au.com.metlife.eapply.bs.bizflowmodel.AddressRole;
import au.com.metlife.eapply.bs.bizflowmodel.AddressRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.AttachmentType;
import au.com.metlife.eapply.bs.bizflowmodel.CitizenshipList;
import au.com.metlife.eapply.bs.bizflowmodel.Contact;
import au.com.metlife.eapply.bs.bizflowmodel.ContactProfile;
import au.com.metlife.eapply.bs.bizflowmodel.Contract;
import au.com.metlife.eapply.bs.bizflowmodel.ContractList;
import au.com.metlife.eapply.bs.bizflowmodel.Coverage;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddress;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddressRole;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddressRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.Email;
import au.com.metlife.eapply.bs.bizflowmodel.ExclusionsListType;
import au.com.metlife.eapply.bs.bizflowmodel.FileAttachment;
import au.com.metlife.eapply.bs.bizflowmodel.Health;
import au.com.metlife.eapply.bs.bizflowmodel.IdentifierList;
import au.com.metlife.eapply.bs.bizflowmodel.IdentifierValue;
import au.com.metlife.eapply.bs.bizflowmodel.InterestedPartyReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Limit;
import au.com.metlife.eapply.bs.bizflowmodel.LoadingListType;
import au.com.metlife.eapply.bs.bizflowmodel.LoadingValue;
import au.com.metlife.eapply.bs.bizflowmodel.MessageHeader;
import au.com.metlife.eapply.bs.bizflowmodel.OrganizationReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Party;
import au.com.metlife.eapply.bs.bizflowmodel.PartyInterest;
import au.com.metlife.eapply.bs.bizflowmodel.PartyRole;
import au.com.metlife.eapply.bs.bizflowmodel.PersonReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Phone;
import au.com.metlife.eapply.bs.bizflowmodel.PhoneRole;
import au.com.metlife.eapply.bs.bizflowmodel.PhoneRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyInsuredReferences;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyNewBusinessOrderProcess;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyOrder;
import au.com.metlife.eapply.bs.bizflowmodel.Sender;
import au.com.metlife.eapply.bs.bizflowmodel.SendingSystem;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.email.SendMail;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverJSON;
import au.com.metlife.eapply.bs.model.DeathAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.model.IpAddnlCoversJSON;
import au.com.metlife.eapply.bs.model.TpdAddnlCoversJSON;
import au.com.metlife.eapply.constants.EtoolkitConstants;
import au.com.metlife.eapply.corporate.business.CorpRequest;
import au.com.metlife.eapply.quote.utility.QuoteConstants;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Insured;


public class BizflowXMLHelper {
	private BizflowXMLHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(BizflowXMLHelper.class);
	
	/*private static final String PACKAGE_NAME = BizflowXMLHelper.class.getPackage().getName();
    private static final String CLASS_NAME = BizflowXMLHelper.class.getName();*/
	
	public static String createXMLForBizFlow(Eapplication applicationDTO, EapplyInput eapplyInput,Properties property,CorporateFund corpFund){
		log.info("Create xml for Bizflow start");
		String xmlOutput = null;
		xmlOutput = marshallingObj(preparePolicyNewBusinessOrderProcessObj(applicationDTO, eapplyInput,property,corpFund));
		log.info("Create xml for Bizflow finish {}",xmlOutput);
		return xmlOutput;
		
	}
	
	public  static PolicyNewBusinessOrderProcess unMarshallingInput(String inputXML){
		log.info("Unmarshalling input start");		
		PolicyNewBusinessOrderProcess input=null;
		try {
			JAXBContext jc = JAXBContext.newInstance (new Class[] {PolicyNewBusinessOrderProcess.class});            
            Unmarshaller u = jc.createUnmarshaller ();         
             input = (PolicyNewBusinessOrderProcess) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8")));
       } catch (Exception e) {
          log.error("Error in un marshalling input: {}" ,e);
       }
		log.info("Unmarshalling input finish");
       return input;
	}
	
	public  static String marshallingObj(PolicyNewBusinessOrderProcess request){
		log.info("Marhalling object start");
		String outputXML=null;
		try {
			JAXBContext context = JAXBContext.newInstance(PolicyNewBusinessOrderProcess.class);
		    javax.xml.bind.Marshaller m = context.createMarshaller();
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);	
		   /* m.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "UTF-16");
		    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, Boolean.FALSE);
		    m.setProperty("com.sun.xml.bind.xmlHeaders", "");
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, true);*/
		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		    m.marshal(request, os);		    
		    byte[] b=os.toByteArray();  
		    
		    outputXML=new String(b);
		    outputXML=outputXML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
		    log.info("outputXML>> {}",outputXML);
		    os.close();
		   /* m.m*/
	    
		 } catch (Exception e) {
	          log.error("Error in marshalling object {}",e);
		 }
		log.info("Marhalling object finish");
		return outputXML;
	}	
	
	public static PolicyNewBusinessOrderProcess preparePolicyNewBusinessOrderProcessObj(Eapplication applicationDTO, EapplyInput eapplyInput,Properties property,CorporateFund corpfund){
		log.info("Prepare Policy new business order project object start");
		Party party = null;
		List<FileAttachment> fileAttachmentList = null;
		Applicant applicantDTO = null;
		List<Cover> coverList = null;
		String e_amt = null;
		//String e_unit = null;
		//String add_unit = null;
		String add_amt = null;
		String tot_amt = null;
		String add_loading = null;
		String add_loading_cmt = null;
		String decision = null;
		String dec_reason = null;
		String fwd_cvr = null;
		String aal_cvr = null;
		String salPercentage = null;
		String loadingAmt = null;
		String loadingType = null;		
		String ex_wait_period = null;
		String ex_ben_period = null;
		String ex_death_amt = "0";
		String ex_tpd_amt = "0";
		String ex_trauma_amt = "0";
		String ex_ip_amt = "0";
		String ex_occRatingDeath = null;
		String ex_occRatingTpd = null;
		String ex_occRatingIp = null;
		String ex_wait_period_ip = null;
		String ex_ben_period_ip = null;
		
				
		/*if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && eapplyInput.getExistingCovers().getCover().size()>0){*/
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null && !(eapplyInput.getExistingCovers().getCover().isEmpty())){
    		for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
				CoverJSON coverJSON = (CoverJSON) iterator.next();
				if(coverJSON!=null && "1".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_death_amt = coverJSON.getAmount();
					ex_occRatingDeath = coverJSON.getOccRating();
				}else if(coverJSON!=null && "2".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_tpd_amt = coverJSON.getAmount();
					ex_occRatingTpd = coverJSON.getOccRating();
				}else if(coverJSON!=null && "3".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_trauma_amt = coverJSON.getAmount();
				}else if(coverJSON!=null && "4".equalsIgnoreCase(coverJSON.getBenefitType())){
					ex_wait_period = coverJSON.getWaitingPeriod();
					ex_ben_period = coverJSON.getBenefitPeriod();				
					ex_ip_amt = coverJSON.getAmount();
					ex_occRatingIp = coverJSON.getOccRating();
					ex_wait_period_ip = coverJSON.getWaitingPeriod();
					ex_ben_period_ip = coverJSON.getBenefitPeriod();
				}
				
			}
    	}	
		
		String add_wait_period = eapplyInput.getWaitingPeriod();
		String add_ben_period = eapplyInput.getBenefitPeriod(); 
		
				
		PolicyNewBusinessOrderProcess policyNewBusinessOrderProcess = new PolicyNewBusinessOrderProcess();
		
		MessageHeader messageHeader = new MessageHeader();
		Sender sender = new Sender();
		sender.setRoleCode("FundOrganization");
		OrganizationReferences organizationReferences = new OrganizationReferences();
		organizationReferences.setOrganizationReference("ORG1");
		sender.setOrganizationReferences(organizationReferences);		
		Contact contact = new Contact();
		Email email = new Email();
		email.setEmailAddress(eapplyInput.getEmail());
		contact.setEmail(email);
		sender.setContact(contact);
		messageHeader.setSender(sender);		
		SendingSystem sendingSystem = new SendingSystem();
		sendingSystem.setVendorProductName("EAPPLY");
		messageHeader.setSendingSystem(sendingSystem);	
		messageHeader.setACORDStandardVersionCode("AML_1_0_0");
		messageHeader.setCorrelationId("00000000-0000-0000-0000-000000000000");	
		
		messageHeader.setMessageId(UUID.randomUUID().toString());/*need to check UUid logic*/
		messageHeader.setMessageDateTime(new Timestamp(new Date().getTime()));
		policyNewBusinessOrderProcess.setMessageHeader(messageHeader);
		PolicyOrder policyOrder = new PolicyOrder();
		Contract contract = new Contract();
		contract.setTransactionFunctionCode("CreateApplication");
		
		if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
			contract.setApplicationTypeCode("TRWS");
         }else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("LVET");
         }else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("MCOV");
         }else if("CANCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && !"HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("REDC");
         }else if("CANCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("CANC");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) && getCoverOptions(applicationDTO) && !"HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("REDC");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) && getFixedToUnitised(eapplyInput) && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("FIXU");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) && getUnitisedToFixed(eapplyInput) && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("UNIF");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) && getCoverOptionsOnlyCancel(applicationDTO) && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("CANC");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())) && getCoverOptionsOnDecreaseAlone(applicationDTO) && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
        	 contract.setApplicationTypeCode("DECR");
         }else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("SPEC");
         }else if((null !=  ex_death_amt && Double.parseDouble(ex_death_amt.trim())>0) || (null !=  ex_ip_amt && Double.parseDouble(ex_ip_amt.trim())>0) || (null !=  ex_trauma_amt && Double.parseDouble(ex_trauma_amt.trim())>0)
         		||(null !=  ex_tpd_amt && Double.parseDouble(ex_tpd_amt.trim())>0)
         ){
        	 contract.setApplicationTypeCode("ADDC");
         }else{
        	 contract.setApplicationTypeCode("NEWA"); 
         }	
		
		log.info("contract.getApplicationTypeCode() >>> "+ contract.getApplicationTypeCode());
		
		contract.setDescription(null) ;
		contract.setLineOfBusinessCode("GroupLife");
		contract.setPolicyNumber(null) ;
		contract.setPolicyStatus(null);
		contract.setPolicyStatusDescription(null) ;
		contract.setExternalReferenceNumber(null) ;
		contract.setCaseNumber(null);
		contract.setContractNumber(applicationDTO.getApplicationumber());
			
					
		if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())){
			if((MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) 
					&& ("CARE".equalsIgnoreCase(applicationDTO.getPartnercode()))) || rUWLifeEvntTransferDocs(applicationDTO.getRequesttype(), applicationDTO.getPartnercode())){
				contract.setContractStatus(MetlifeInstitutionalConstants.REFERRED);
			}
//			else if(rUWLifeEvntTransferDocs(applicationDTO.getRequesttype(), applicationDTO.getPartnercode())){ /** Made changes for Host Plus by Prasanna Durga as a part of WR18483**/
//				contract.setContractStatus(MetlifeInstitutionalConstants.REFERRED);
//			}
			else{
				contract.setContractStatus("Accept");
			}		
		}else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus(MetlifeInstitutionalConstants.REFERRED);
		}else if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus("Decline");
			
		}		
		contract.setContractStatusDescription(null);
		contract.setEffectiveDate(applicationDTO.getCreatedate());
		contract.setMembershipTypeCode(eapplyInput.getMemberType());
		contract.setCancellationTypeCode(null);
		contract.setCancellationReasonCode(null) ;
		contract.setProductVariant(null);
		/*contracts*/
		List<ContractList> contracts = new ArrayList<>();
		ContractList contractList = new ContractList();
		contractList.setLifeEvent(eapplyInput.getEventName());
		contracts.add(contractList);
		contract.setContractList(contracts);
		/*party*/
		List<Party> partyList = new ArrayList<>();
		party = new Party();
		party.setKey("ORG1");
		party.setPartyType("Organization");
		if(!"CORP".equalsIgnoreCase(applicationDTO.getPartnercode())) {
		party.setPartyUID(applicationDTO.getPartnercode());
		}
		else {
			if(corpfund!=null) {
				party.setPartyUID(corpfund.getFundCode());
			}
		}
		partyList.add(party);
		party = new Party();
		party.setKey("ORG2");
		party.setPartyType("Administrator");
		if(!"CORP".equalsIgnoreCase(applicationDTO.getPartnercode())) {
		party.setPartyUID(applicationDTO.getPartnercode());
		}
		else {
			if(corpfund!=null) {
				party.setPartyUID(corpfund.getFundCode());
			}
		}
		partyList.add(party);
		
		for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			applicantDTO = (Applicant) iter.next();
			if(applicantDTO!=null && "primary".equalsIgnoreCase(applicantDTO.getApplicanttype())){
				party = new Party();
				party.setKey("PERS1");
				party.setPartyType("Person");
				party.setPartyUID(applicantDTO.getClientrefnumber());
				if(eapplyInput.getPersonalDetails()!=null){
					party.setLastName(eapplyInput.getPersonalDetails().getLastName());
					if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
						party.setGender(eapplyInput.getOccupationDetails().getGender());
					} else {
						party.setGender(eapplyInput.getPersonalDetails().getGender());
					}					
				}
				
				party.setFirstName(applicantDTO.getFirstname());
				party.setPrefixCode(applicantDTO.getTitle());
				
				party.setBirthDate(eapplyInput.getDob());
				party.setOccupation(applicantDTO.getOccupation());
				party.setPartyFundJoinDate(applicantDTO.getDatejoinedfund());
				/*party.setIsRollover(isRollover);*/
				
						
				PartyRole partyRole = new PartyRole();
				ContactProfile contactProfile = new ContactProfile();
				List<AddressRoleList> addressRoles = new ArrayList<>();
				AddressRoleList addressRoleList = new AddressRoleList();
				AddressRole addressRole = new AddressRole() ;
				Address address = new Address();
				/*defaulted to home as we don't ask address type in eapply*/
				address.setAddressKey("Home");
				
				address.setAddressLine1(eapplyInput.getAddress().getLine1());
				address.setAddressLine2(eapplyInput.getAddress().getLine2());
				if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				address.setCountryName(eapplyInput.getAddress().getCountry());
				}
				else {
					if(!"OTH".equalsIgnoreCase(eapplyInput.getAddress().getState())) {
					address.setCountryName("AUSTRALIA");
					}
					else {
						address.setCountryName("OVERSEAS");
					}
				}
				/*address.setFullAddressDescription(fullAddressDescription);*/
				address.setPostalCode( eapplyInput.getAddress().getPostCode());
				address.setStateOrProvinceName(eapplyInput.getAddress().getState());
				address.setSuburbName( eapplyInput.getAddress().getSuburb());
				addressRole.setAddress(address);
				addressRoleList.setAddressRole(addressRole);	
				addressRoles.add(addressRoleList);
				contactProfile.setAddressRoleList(addressRoles);
				
				List<PhoneRoleList> phones = new ArrayList<>();
				PhoneRoleList phoneRoleList = new PhoneRoleList();
				
				PhoneRole phoneRole = new PhoneRole();
				Phone phone = new Phone();				
				
				
			   /*if (null != eapplyInput.getContactDetails().getWorkPhone() && eapplyInput.getContactDetails().getWorkPhone().trim().length()>0){
				   phone.setPhoneNumber(eapplyInput.getContactDetails().getWorkPhone());
				   phone.setPreferredContact("Yes");
				   phone.setPhoneType("Work");
			   }
		        if (null != eapplyInput.getContactDetails().getHomePhone() && eapplyInput.getContactDetails().getHomePhone().trim().length()>0){
		        	phone.setPhoneNumber(eapplyInput.getContactDetails().getHomePhone());
		        	phone.setPreferredContact("Yes");
		        	phone.setPhoneType("Home");
		        }
		        
		        if (null != eapplyInput.getContactDetails().getMobilePhone() && eapplyInput.getContactDetails().getMobilePhone().trim().length()>0){
		        	phone.setPhoneNumber(eapplyInput.getContactDetails().getMobilePhone());
		        	phone.setPreferredContact("Yes");
		        	phone.setPhoneType("Mobile");
		        }*/
		        
		        if(phone.getPhoneNumber()==null && null!=eapplyInput.getContactPhone()
		        		&& eapplyInput.getContactPhone().trim().length()>0){
		        	phone.setPhoneNumber(eapplyInput.getContactPhone());
		        	//phone.setPhoneNumber(eapplyInput.getContactPhone());
					phone.setPreferredContact("Yes");
					phone.setPhoneType(eapplyInput.getContactType());
		        }		       
		     
		        
		    	if(eapplyInput.getContactPrefTime()!=null){
		    		
		    		log.info("applicantDTO.getContactinfo().getPreferedContactTime()>> {}",eapplyInput.getContactPrefTime());
		    		String temp =eapplyInput.getContactPrefTime();
		    		
		    				    		 		
		    		if(temp.contains("(")){
		    			temp = temp.substring(temp.indexOf('(')+1,temp.lastIndexOf(')'));
			    		String startTime= temp.substring(0,temp.indexOf('-'));
			    		/*if(eapplyInput.getContactPrefTime().contains("Morning")){
			    			startTime= startTime;//+ "am";
			    		}else{
			    			startTime= startTime;// + "pm";
			    		}*/
			    		String endTime= temp.substring(temp.indexOf('-')+1);			    		
			    		 phone.setStartTime(startTime.trim());			    		
			    		phone.setEndTime(endTime.trim());
		    		}else if(temp.length()>9){
		    			/*temp = temp.substring(temp.indexOf("(")+1,temp.lastIndexOf(")"));*/
			    		String startTime= temp.substring(0,3);			    		
			    		String endTime= temp.substring(6,10);			    		
			    		 phone.setStartTime(startTime.trim());			    		
			    		phone.setEndTime(endTime.trim());
		    		}  		
		    		
		    		 		
				     
		    	}
		       
		        phoneRole.setPhone(phone);
		        phoneRoleList.setPhoneRole(phoneRole);
		        phones.add(phoneRoleList);
		        contactProfile.setPhoneRoleList(phones);
		        
		        List<ElectronicAddressRoleList> electronicAddresess = new ArrayList<>(); 
		        ElectronicAddressRoleList electronicAddressRoleList = new ElectronicAddressRoleList();
		        ElectronicAddressRole electronicAddressRole = new ElectronicAddressRole();		        
		        ElectronicAddress electronicAddress = new ElectronicAddress();
		        electronicAddress.setCategory("Personal");
		        electronicAddress.setEmail(eapplyInput.getEmail());
		        electronicAddressRole.setElectronicAddress(electronicAddress);		        
		        electronicAddressRoleList.setElectronicAddressRole(electronicAddressRole);
		        electronicAddresess.add(electronicAddressRoleList);
		        contactProfile.setElectronicAddressRoleList(electronicAddresess);
		        
		        partyRole.setContactProfile(contactProfile);
				if(eapplyInput.getAddress()!=null){
					List<CitizenshipList> citizens = new ArrayList<>();
					CitizenshipList citizenshipList = new CitizenshipList();
					if(eapplyInput.getAddress().getCountry()!=null && eapplyInput.getAddress().getCountry().trim().length()>0){
						citizenshipList.setCode(eapplyInput.getAddress().getCountry());
					}else{
						citizenshipList.setCode("Australia");
					}					
					citizens.add(citizenshipList);
					partyRole.setCitizenshipList(citizens);
				}
				party.setPartyRole(partyRole);
				Health health = new Health();
				if("yes".equalsIgnoreCase(eapplyInput.getSmoker())){
					applicantDTO.setSmoker("Yes");
					
				}else if("No".equalsIgnoreCase(eapplyInput.getSmoker())){
					applicantDTO.setSmoker("No");
					
				}
				if(applicantDTO.getSmoker()!=null && "yes".equalsIgnoreCase(applicantDTO.getSmoker())){
					health.setSmokerIndicator("Yes");
				}else if(applicantDTO.getSmoker()!=null && "no".equalsIgnoreCase(applicantDTO.getSmoker())){
					health.setSmokerIndicator("No");
				}else{
					health.setSmokerIndicator("UNKNOWN");
				}
				/*health.setHeight(height);
				health.setWeight(weight);
				health.setBMI(bmi);*/
				party.setHealth(health);				
				/*coverage*/
				
				coverList = validCoverVector(applicantDTO.getCovers(),corpfund) ;
				List<Coverage> coverageList = new ArrayList<>();
				/*Coverage coverage = null;*/
				Decision decision2 = null;
				for (Iterator iterator = coverList.iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();
					String add_unit = null;
					String e_unit = null;
					if(coversDTO!=null){
						if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 = getReasons(eapplyInput, "Term");
							e_amt = eapplyInput.getExistingDeathAmt();
							 e_unit = eapplyInput.getExistingDeathUnits();
							 /*need to remove hardcoding*/
							 if(eapplyInput.getAddnlDeathCoverDetails()!=null){
									if(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null && !"DcFixed".equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType())){
										 add_unit = eapplyInput.getAddnlDeathCoverDetails().getDeathInputTextValue();
									}
									else if(null!=eapplyInput && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()) 
											&& QuoteConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType()))
									{
										add_unit = eapplyInput.getAddnlDeathCoverDetails().getDeathTransferUnits();
									}
									else if(null!=eapplyInput && QuoteConstants.PARTNER_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) 
											&& QuoteConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathTransferCoverType()))
									{
										add_unit = e_unit;
									}
									tot_amt = eapplyInput.getAddnlDeathCoverDetails().getDeathFixedAmt();
									if(tot_amt!=null && e_amt!=null){
										add_amt= 	((new BigDecimal(tot_amt)).subtract(new BigDecimal(e_amt))).toString();
									}
									
									
									
							}else{
								add_amt="0";
								tot_amt = add_amt;
							}
							if(decision2!=null){
								add_loading = decision2.getOriginalTotalDebitsValue();
								 add_loading_cmt = decision2.getTotalDebitsReason();
								 loadingAmt = decision2.getOriginalTotalDebitsValue();
								 loadingType = decision2.getTotalDebitsReason();
							}
							 
							 decision = eapplyInput.getDeathDecision();							 
							 dec_reason = eapplyInput.getDeathResons();
							 /*fwd_cvr = newUW.getLodge_death_fwd_cover();
							 aal_cvr = newUW.getLodge_death_aal_cover();*/							 
							 
							
							 ex_wait_period = null;
							 ex_ben_period = null;
							 add_wait_period = null;
							 add_ben_period = null;
							 if(null!=eapplyInput && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
							 {
								eapplyInput.setExistingOccRating(ex_occRatingDeath); 
							 }
							 
						}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 =getReasons(eapplyInput, "TPD");
							e_amt = eapplyInput.getExistingTpdAmt();
							 e_unit = eapplyInput.getExistingTPDUnits();
							 /*need to remove hardcoding*/							
							 if(eapplyInput.getAddnlTpdCoverDetails()!=null){
									if(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null && !"TPDFixed".equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType())){
										 add_unit = eapplyInput.getAddnlTpdCoverDetails().getTpdInputTextValue();
									}
									else if(null!=eapplyInput && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()) 
											&& QuoteConstants.TPD_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()))
									{
										add_unit = eapplyInput.getAddnlTpdCoverDetails().getTpdTransferUnits();
									}
									else if(null!=eapplyInput && QuoteConstants.PARTNER_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) 
											&& QuoteConstants.TPD_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()))
									{
										add_unit = e_unit;
									}
									tot_amt = eapplyInput.getAddnlTpdCoverDetails().getTpdFixedAmt();
									if(tot_amt!=null && e_amt!=null){
										add_amt= 	((new BigDecimal(tot_amt)).subtract(new BigDecimal(e_amt))).toString();
									}
									
							}else{
								add_amt="0";
								tot_amt = add_amt;
							}		 
							 if(decision2!=null){
								 add_loading = decision2.getOriginalTotalDebitsValue();
								 add_loading_cmt = decision2.getTotalDebitsReason();
								 loadingAmt = decision2.getOriginalTotalDebitsValue();
								 loadingType = decision2.getTotalDebitsReason();
							 }
							
							 decision = eapplyInput.getTpdDecision();
							 dec_reason = eapplyInput.getTpdResons();
							/* fwd_cvr = eapplyInput.getLodge_tpd_fwd_cover();
							 aal_cvr = eapplyInput.getLodge_tpd_aal_cover();	*/						
							 ex_wait_period = null;
							 ex_ben_period = null;
							 add_wait_period = null;
							 add_ben_period = null; 
							 
							 if(null!=eapplyInput && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
							 {
								eapplyInput.setExistingOccRating(ex_occRatingTpd); 
							 }
							 
						}/*else if("TRAUMA".equalsIgnoreCase(coversDTO.getCovercode())){
							e_amt = newUW.getLodge_e_trauma_amount();
							 e_unit = newUW.getLodge_e_trauma_units();
							 add_unit = newUW.getLodge_add_trauma_units();
							 add_amt = newUW.getLodge_add_trauma_amount();
							 tot_amt = newUW.getLodge_tot_trauma_cover();
							 add_loading = newUW.getLodge_trauma_loading_percentage();
							 add_loading_cmt = newUW.getLodge_trauma_loading_comment();
							 decision = newUW.getLodge_trauma_decision();
							 dec_reason = newUW.getLodge_trauma_dec_reason();
							 loadingAmt = newUW.getLodge_trauma_loading_percentage();
							 loadingType = newUW.getLodge_trauma_loading_comment();
							 ex_wait_period = null;
							 ex_ben_period = null;
							 add_wait_period = null;
							 add_ben_period = null; 
							
						}*/else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 =getReasons(eapplyInput, "IP");
							e_amt = eapplyInput.getExistingIPAmount();
							 e_unit = eapplyInput.getExistingIPUnits();
							 
							 //Below if condition added for displaying waiting & benefit period.
							 if(eapplyInput!=null && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(eapplyInput.getManageType()) ){
								 ex_wait_period = coversDTO.getExistingipwaitingperiod();
								 ex_ben_period = coversDTO.getExistingipbenefitperiod(); 
							 }
							 else{
								 ex_wait_period = eapplyInput.getWaitingPeriod();
								 ex_ben_period = eapplyInput.getBenefitPeriod();
							 }
							
							 if(null!=eapplyInput && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
							 {
								eapplyInput.setExistingOccRating(ex_occRatingIp);
								ex_wait_period = ex_wait_period_ip;
								ex_ben_period = ex_ben_period_ip; 
							 }
							 
							 if (ex_wait_period!=null && ex_wait_period.toUpperCase().contains( "DAYS")) {
									ex_wait_period =  ex_wait_period.substring( 0, ex_wait_period.toUpperCase().indexOf( "DAYS")).trim();
								}else if(ex_wait_period!=null && ex_wait_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
									int yesrs = Integer.parseInt( ex_wait_period.substring( 0, ex_wait_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim());
					                yesrs = 365 * yesrs;
					                ex_wait_period =  Integer.toString(yesrs);
								}
								if (ex_ben_period!=null && ex_ben_period.toUpperCase().contains( "DAYS")) {
									ex_ben_period= ex_ben_period.substring( 0, ex_ben_period.toUpperCase().indexOf( "DAYS")).trim()+ "MTH";
								}else if(ex_ben_period!=null && ex_ben_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
									ex_ben_period = ex_ben_period.substring( 0, ex_ben_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim()+ "YR";
								}else if(ex_ben_period!=null && ex_ben_period.toUpperCase().contains( "AGE")){
									ex_ben_period = ex_ben_period.toUpperCase().replaceAll( " ", "");
								}
							 
							 /*need to remove hardcoding*/
							if(eapplyInput.getAddnlIpCoverDetails()!=null){
								if(null!=eapplyInput && "CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()) && QuoteConstants.MTYPE_CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) && coversDTO.getAdditionalunit() < (coversDTO.getAdditionalcoveramount()).intValue())
								{
									add_unit = String.valueOf(coversDTO.getAdditionalunit());
									if(coversDTO.getAdditionalcoveramount().compareTo(coversDTO.getExistingcoveramount()) == 0 && null!=e_unit && Integer.valueOf(e_unit)>0)
									{
										add_unit=null;
									}
								}else if(eapplyInput.getAddnlIpCoverDetails().getIpCoverType()!=null && "IpUnitised".equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().getIpCoverType())
										&& !("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()) && QuoteConstants.MTYPE_CCOVER.equalsIgnoreCase(eapplyInput.getManageType()))){
									 add_unit = eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue();
								}
								
								if(null!= eapplyInput.getAddnlIpCoverDetails().getIpTransferCoverType() && QuoteConstants.IP_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlIpCoverDetails().getIpTransferCoverType())
										&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(eapplyInput.getManageType()))
								{
									add_unit = String.valueOf(coversDTO.getAdditionalunit());
									if(Integer.valueOf(e_unit) >= Integer.valueOf(add_unit))
									{
										add_unit=null;
									}
								}
								
								tot_amt = eapplyInput.getAddnlIpCoverDetails().getIpFixedAmt();
								if(tot_amt!=null && e_amt!=null){
									add_amt= 	((new BigDecimal(tot_amt)).subtract(new BigDecimal(e_amt))).toString();
								}
								
							if(eapplyInput!=null && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(eapplyInput.getManageType()) && !("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()))){
                                    add_wait_period = eapplyInput.getAddnlIpCoverDetails().getTotalipwaitingperiod();
                                    add_ben_period = eapplyInput.getAddnlIpCoverDetails().getTotalipbenefitperiod();
                            }
                            else
                            {
		                            add_wait_period = eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod();
		                            add_ben_period = eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod();

                            }

								
							/*	add_wait_period = eapplyInput.getAddnlIpCoverDetails().getWaitingPeriod();
								add_ben_period = eapplyInput.getAddnlIpCoverDetails().getBenefitPeriod();*/
								
								if (add_wait_period!=null && add_wait_period.toUpperCase().contains( "DAYS")) {
									add_wait_period =  add_wait_period.substring( 0, add_wait_period.toUpperCase().indexOf( "DAYS")).trim();
								}else if(add_wait_period!=null && add_wait_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
									int yesrs = Integer.parseInt( add_wait_period.substring( 0, add_wait_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim());
						            yesrs = 365 * yesrs;
						            add_wait_period = Integer.toString(yesrs);
								}
								if (add_ben_period!=null && add_ben_period.toUpperCase().contains( "DAYS")) {
									add_ben_period= add_ben_period.substring( 0, add_ben_period.toUpperCase().indexOf( "DAYS")).trim()+ "MTH";
								}else if(add_ben_period!=null && add_ben_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
									add_ben_period = add_ben_period.substring( 0, add_ben_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim()+ "YR";
								}else if(add_ben_period!=null && add_ben_period.toUpperCase().contains( "AGE")){
									add_ben_period = add_ben_period.toUpperCase().replaceAll( " ", "");
								}
								
								
							}else{
								add_amt="0";
								tot_amt = add_amt;
							}			
							if(decision2!=null){
								add_loading = decision2.getOriginalTotalDebitsValue();
								add_loading_cmt = decision2.getTotalDebitsReason();
								loadingAmt = decision2.getOriginalTotalDebitsValue();
								loadingType = decision2.getTotalDebitsReason();
							}
							 
							 decision = eapplyInput.getIpDecision();
							 dec_reason = eapplyInput.getIpResons();						
							
							 if( eapplyInput.getIpSalaryPercent()!=0){
								 salPercentage=  Integer.toString(eapplyInput.getIpSalaryPercent());	
							 }
													  
							 
						}			
						coverageList.add(getCoverage(coversDTO, coversDTO.getCovercode(),
								e_amt, e_unit, add_unit, add_amt, tot_amt, add_loading, add_loading_cmt, decision, dec_reason, fwd_cvr, aal_cvr, ex_wait_period, ex_ben_period, add_wait_period, add_ben_period,loadingType,loadingAmt,eapplyInput,applicationDTO));
					}
					
				}
				contract.setCoverage(coverageList);
				if(salPercentage!=null){
					IdentifierList identifierList = new IdentifierList();
					List<IdentifierValue> identifierValueList = new ArrayList<>();
					IdentifierValue identifierValue = new IdentifierValue();
					identifierValue.setKey("SaleryPercentage");
					log.info("salPercentage>> {}",salPercentage);
					if(null!=salPercentage && salPercentage.contains("85%")){
						identifierValue.setData("85");
					}else if(null!=salPercentage && salPercentage.contains("75%")){
						identifierValue.setData("75");
					}else if(null!=salPercentage && salPercentage.contains("90%")){
						identifierValue.setData("90");
					}else{
						identifierValue.setData(salPercentage);
					}
					identifierValueList.add(identifierValue);
					identifierList.setIdentifierValue(identifierValueList);			
					party.setIdentifierList(identifierList);
				}
				
				partyList.add(party);		
				
			}
			
		}
		List<PartyInterest> partyInterestList = new ArrayList<>();
		
		PartyInterest partyInterest = new PartyInterest();
		partyInterest.setRoleCode("FundAdministrator");
		InterestedPartyReferences interestedPartyReferences = new InterestedPartyReferences();
		PersonReferences personReferences = new PersonReferences();
		personReferences.setPersonReference("ORG2");
		interestedPartyReferences.setPersonReferences(personReferences);
		partyInterest.setInterestedPartyReferences(interestedPartyReferences);
		partyInterestList.add(partyInterest);
		partyInterest = new PartyInterest();
		partyInterest.setRoleCode("FundOrganization");
		interestedPartyReferences = new InterestedPartyReferences();
		personReferences = new PersonReferences();
		personReferences.setPersonReference("ORG1");
		interestedPartyReferences.setPersonReferences(personReferences);
		partyInterest.setInterestedPartyReferences(interestedPartyReferences);
		partyInterestList.add(partyInterest);
		
		contract.setPartyInterest(partyInterestList);
		contract.setParty(partyList);
		String bizFlowBasePath = null;
		String eApplyFileBasePath = null;
		String fileName = null;
		Document document = null;
		AttachmentType attachmentType = null;
		FileAttachment fileAttachment = null;
		/*if(applicationDTO.getDocuments()!=null && applicationDTO.getDocuments().size()>0){*/
		if(applicationDTO.getDocuments()!=null && !(applicationDTO.getDocuments().isEmpty())){
			fileAttachmentList = new ArrayList<>();
			for (Iterator iterator = applicationDTO.getDocuments().iterator(); iterator.hasNext();) {
				document = (Document) iterator.next();
				if(document!=null){
					attachmentType = new AttachmentType();
					fileAttachment = new FileAttachment();
					bizFlowBasePath=  property.getProperty("FILE_UPLOAD_PATH_BIZ");
					eApplyFileBasePath =property.getProperty("FILE_UPLOAD_PATH");
					fileName = document.getDocLoc().substring(eApplyFileBasePath.length());
					fileName = replaceSpecialChar(fileName);
					//attachmentType.setAttachmentTypeCode(document.getDocType());
					attachmentType.setAttachmentTypeCode("Application Form");
					attachmentType.setDocumentURI(bizFlowBasePath+fileName);
					fileAttachment.setAttachmentType(attachmentType);
					fileAttachmentList.add(fileAttachment);
				}
				
			}
		}			
		contract.setFileAttachment(fileAttachmentList);
		policyOrder.setContract(contract);
		policyNewBusinessOrderProcess.setPolicyOrder(policyOrder);
		log.info("Prepare Policy new business order project object finish");
		return policyNewBusinessOrderProcess;
	}
	
//	Vinay - WR 19005: replace single and double quote characters from the file name.
	public static String replaceSpecialChar(String str){
		log.info("Start : replace single and double quote characters from the file name");
		String replaceSpecialCharr = str.replaceAll("[\'\"]","");
		log.info("End: replace single and double quote characters from the file name");
		return replaceSpecialCharr;
}
	
	
	private static Coverage getCoverage(Cover coversDTO, String coverType,
			String e_amt,String e_unit,String add_unit,String add_amt,String tot_amt,String add_loading,String add_loading_cmt,String decision, String dec_reason,String fwd_cvr,String aal_cvr,
			String ex_wait_period, String ex_ben_period, String add_wait_period,String add_ben_period, String loadingType, String loadingAmt, EapplyInput eapplyInput,Eapplication applicationDTO){
		log.info("Get coverage start");

		Coverage coverage = new Coverage();
		if(coverType.equalsIgnoreCase(coversDTO.getCovercode())){
			coverage.setTypeCode(coversDTO.getCovercode());
			if("RUW".equalsIgnoreCase(coversDTO.getCoverdecision())){
				coverage.setCoverageStatusCode(MetlifeInstitutionalConstants.REFERRED);
			}else if("DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
				if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
					coverage.setCoverageStatusCode("Ineligible");
				}else{
					coverage.setCoverageStatusCode("Decline");
				}				
			}else if("ACC".equalsIgnoreCase(coversDTO.getCoverdecision())){
				if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
					coverage.setCoverageStatusCode("Eligible");
				}else{
					coverage.setCoverageStatusCode("Accept");
				}						
				
			}		
			if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
			coverage.setItemAmount(Double.parseDouble(coversDTO.getCost()));
			}
			PolicyInsuredReferences policyInsuredReferences = new PolicyInsuredReferences();
			policyInsuredReferences.setKey("PERS1");
			coverage.setPolicyInsuredReferences(policyInsuredReferences);			
			List<Limit> limitList = new ArrayList<>();
			int itr = 0;
			while (itr<3) {
				Limit limit = new Limit();
				if(itr==0){
					limit.setLimitAppliesCode("ExistingCover");							
					limit.setLimitAmount(e_amt);
					limit.setLimitUnits(e_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.UNITISED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.UNITISED);
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.FIXED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.FIXED);
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}
					if(null!=eapplyInput && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))
					{
					limit.setOccupationalRatingCode(eapplyInput.getExistingOccRating());
					}
					else
					{
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					}
					limit.setWaitingPeriod(ex_wait_period);
					limit.setBenefitPeriod(ex_ben_period);
					limit.setPremium(null);
					limit.setPremiumFrequency(null);				
				}else if(itr==1){
					limit.setLimitAppliesCode("AdditionalCover");
					if(tot_amt!=null && e_amt!=null && new BigDecimal(tot_amt).doubleValue()>0 && new BigDecimal(tot_amt).subtract(new BigDecimal(e_amt)).doubleValue()<0){
						limit.setLimitAmount((new BigDecimal(tot_amt).subtract(new BigDecimal(e_amt))).toString());
					}else{
						limit.setLimitAmount(add_amt);
					}
					
					limit.setLimitUnits(add_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.UNITISED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.UNITISED);
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.FIXED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.FIXED);
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}	
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(add_wait_period);
					limit.setBenefitPeriod( add_ben_period);
					/*limit.setPremium(coversDTO.getCost());*/
					limit.setPremiumFrequency(coversDTO.getFrequencycosttype());
				}else if(itr==2){
					limit.setLimitAppliesCode("TotalCover");							
					limit.setLimitAmount(tot_amt);
					if(null!=eapplyInput && "CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()) && QuoteConstants.MTYPE_CCOVER.equalsIgnoreCase(eapplyInput.getManageType()))
					{
						if(null!=add_unit)
						{
							limit.setLimitUnits(add_unit);
						}
						else
						{
						limit.setLimitUnits(e_unit);
						}
					}
					else
					{
					limit.setLimitUnits(add_unit);
					}
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.UNITISED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.UNITISED);
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.FIXED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.FIXED);
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}	
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(add_wait_period);
					limit.setBenefitPeriod( add_ben_period);
					if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
					limit.setPremium(Double.parseDouble(coversDTO.getCost()));
					}
					limit.setPremiumFrequency(coversDTO.getFrequencycosttype());
				}								
				itr=itr+1;
				limitList.add(limit);
			}
			if(aal_cvr!=null && aal_cvr.trim().length()>0){
				Limit limit = new Limit();
				limit.setLimitAppliesCode("AAL");							
				limit.setLimitAmount(aal_cvr);
				limitList.add(limit);
			}
			if(fwd_cvr!=null && fwd_cvr.trim().length()>0){
				Limit limit = new Limit();
				limit.setLimitAppliesCode("FUL");							
				limit.setLimitAmount(fwd_cvr);
				limitList.add(limit);
			}
			coverage.setLimit(limitList);
			/*exclusions*/
			coverage.setExclusionsListType(getExclusions(eapplyInput,coverType));
			/*List<ExclusionsListType> exclutionList = new ArrayList<>();*/
			/*ExclusionsListType exclusionsListType = null;*/
			/*if(newUW.getExclusions()!=null && newUW.getExclusions().length>0){			
				for (int excItr = 0; excItr < newUW.getExclusions().length; excItr++) {
					log.info("coverType>> {}",coverType);
					log.info("newUW.getExclusions()[excItr].getExclusion_type()>> {}",newUW.getExclusions()[excItr].getExclusion_type());
					if(coverType.equalsIgnoreCase(newUW.getExclusions()[excItr].getExclusion_type())){
						exclusionsListType = new ExclusionsListType();
						exclusionsListType.setExclusionNameCode(newUW.getExclusions()[excItr].getExclusion_code());
						exclusionsListType.setExclusionTerm(newUW.getExclusions()[excItr].getExclusion_wording());							
						exclutionList.add(exclusionsListType);
					}				
				}
				coverage.setExclusionsListType(getExclusions(eapplyInput));
			}else{
				exclusionsListType = new ExclusionsListType();
				exclutionList.add(exclusionsListType);
				coverage.setExclusionsListType(exclutionList);
			}*/
			/*loading*/
			List<LoadingListType> loadingList = new ArrayList<>();
			LoadingListType loadingListType = new LoadingListType();
			if(loadingAmt!=null && loadingAmt.trim().length()>0){				
				loadingListType.setLoadingType(loadingType);				
				LoadingValue loadingValue = new LoadingValue();
				loadingValue.setType("Percentage");
				loadingValue.setData(loadingAmt);			
				loadingListType.setLoadingValue(loadingValue);
				loadingList.add(loadingListType);
				coverage.setLoadingListType(loadingList);
			}else{
				loadingList.add(loadingListType);
				coverage.setLoadingListType(loadingList);
			}			
			
		}
		log.info("Get coverage finish");
		return coverage;
	
	}
	public static Decision getReasons(EapplyInput eapplyInput, String coverType){
		log.info("Get reasons start");
		Decision decision= null;
		if (null != eapplyInput && null!=eapplyInput.getResponseObject() && null != eapplyInput.getResponseObject().getClientData()
                && null != eapplyInput.getResponseObject().getClientData().getListInsured()) {
            for (int itr = 0; itr < eapplyInput.getResponseObject().getClientData().getListInsured().size(); itr++) {
                Insured insured =  eapplyInput.getResponseObject().getClientData().getListInsured().get( itr);
                if (null != insured.getListOverallDec()) {
                    for (int index = 0; index < insured.getListOverallDec().size(); index++) {
                         decision =  insured.getListOverallDec().get( index);
                       
                        if (null != decision
                        		&& (decision.getProductName().contains(coverType) && 
                                		null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25)) {
                            /*if (decision.getProductName().contains(coverType) && 
                            		null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25) {*/
                            	break;
                            /*}*//*else if(decision.getProductName().contains(coverType)
                            		&& null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25){
                            	break;
                            }else if(decision.getProductName().contains(coverType)
                            		&& null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25){
                            	decision.getOriginalTotalDebitsValue();
                            	decision.getTotalDebitsReason();
                            }else if(decision.getProductName().contains(coverType)
                            		&& null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25){
                            	break;
                            }*/
                        }
                    }
                }
            }
		}
		log.info("Get reasons finish");
		return decision;
	}
	
	private static Boolean  rUWLifeEvntTransferDocs(String requestType,String partnerode){
		Boolean retVal = Boolean.FALSE;
		if((MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(requestType) 
				|| MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(requestType))  
				&& (MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(partnerode) 
						|| MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(partnerode/**Made changes for vics super by purna**/))){
			retVal = Boolean.TRUE;
		}
		return retVal;
	}
	
	public static List<ExclusionsListType> getExclusions(EapplyInput eapplyInput, String coverType){
		log.info("Get exclusions start");
		List<ExclusionsListType> exclutionList = new ArrayList<>();
		ExclusionsListType exclusionsListType = null;
		String cvrType = coverType;
		if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(cvrType)){
			cvrType = "Term";
		}
		if (null != eapplyInput && null!=eapplyInput.getResponseObject() && null != eapplyInput.getResponseObject().getClientData()
                && null != eapplyInput.getResponseObject().getClientData().getListInsured()) {
            for (int itr = 0; itr < eapplyInput.getResponseObject().getClientData().getListInsured().size(); itr++) {
                Insured insured =  eapplyInput.getResponseObject().getClientData().getListInsured().get( itr);
                if (null != insured.getListOverallDec()) {
                    for (int index = 0; index < insured.getListOverallDec().size(); index++) {
                        Decision decision =  insured.getListOverallDec().get( index);
                       
                        if (null != decision && (decision.getProductName().contains(cvrType))) {
                        	/*if (decision.getProductName().contains(coverType)) {*/
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion =  decision.getListExclusion().get( exclusionItr);                                    
                                   if(exclusion!=null){
                                	   exclusionsListType = new ExclusionsListType();
                                	   exclusionsListType.setExclusionNameCode( exclusion.getName());
                                       exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                       exclutionList.add( exclusionsListType);
                                   }
                                   
                                }
                            	break;
                            /*}*//*else if (decision.getProductName().contains(coverType)) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    if(exclusion!=null){
                                    	 exclusionsListType = new ExclusionsListType(); 
                                    	exclusionsListType.setExclusionNameCode( exclusion.getName());
                                         exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                         exclutionList.add( exclusionsListType);
                                    }
                                   
                                }
                            }else if (decision.getProductName().contains(coverType)) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    if(exclusion!=null){
                                    	 exclusionsListType = new ExclusionsListType(); 
                                    	exclusionsListType.setExclusionNameCode( exclusion.getName());
                                         exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                         exclutionList.add( exclusionsListType);
                                    }
                                   
                                }
                            }else if (decision.getProductName().contains(coverType)) {
                            	for (int exclusionItr = 0; exclusionItr < decision.getListExclusion().size(); exclusionItr++) {
                                    Exclusion exclusion = (Exclusion) decision.getListExclusion().get( exclusionItr);                                    
                                    if(exclusion!=null){
                                    	 exclusionsListType = new ExclusionsListType();
                                    	exclusionsListType.setExclusionNameCode( exclusion.getName());
                                        exclusionsListType.setExclusionTerm( exclusion.getText());                                    
                                        exclutionList.add( exclusionsListType);
                                    }
                                    
                                }
                            }*/
                            
                        }
                    }
                }
            }
		}
		log.info("Get exclusions finish");
		return exclutionList;
	}
	
	
	public static List validCoverVector(List<Cover> coverVec,CorporateFund corpfund) {
		log.info("Valid cover vector start");
     /* Vector updatedVector = null;
      Vector finalVector = null;*/
		ArrayList updatedVector = null;
		/*ArrayList finalVector = null;*/
      
      if (null != coverVec) {
          updatedVector = new ArrayList();
          if(corpfund!=null && corpfund.getCorpFundCategories()!=null) {
          	for(CorporateFundCat coverreq:corpfund.getCorpFundCategories()) {
          for (int itr = 0; itr < coverVec.size(); itr++) {
              Cover coversDTO =  coverVec.get( itr);
    
            	if (null != coversDTO
                        && null!=coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0) {
                    if (coversDTO.getCovercode().equalsIgnoreCase( MetlifeInstitutionalConstants.DEATH)
                            && null != coversDTO.getCoverdecision() && "Y".equalsIgnoreCase(coverreq.getDeathRequired().toString())) {
                        updatedVector.add( coversDTO);
                    }
                    if (coversDTO.getCovercode().equalsIgnoreCase( "TPD")
                            && null != coversDTO.getCoverdecision() && "Y".equalsIgnoreCase(coverreq.getTpdRequired().toString())) {
                        updatedVector.add( coversDTO);
                    }
                    if (coversDTO.getCovercode().contains("TRAUMA")
                            && null != coversDTO.getCoverdecision()) {
                        updatedVector.add( coversDTO);
                    }
                    if (coversDTO.getCovercode().equalsIgnoreCase( "IP")
                            && null != coversDTO.getCoverdecision() && "Y".equalsIgnoreCase(coverreq.getIpRequired().toString() )) {
                        updatedVector.add( coversDTO);
                    }
                }
            	}
            }
          }
            else {
            	 for (int itr = 0; itr < coverVec.size(); itr++) {
                     Cover coversDTO =  coverVec.get( itr);
              if (null != coversDTO
                      && null!=coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0) {
                  if (coversDTO.getCovercode().equalsIgnoreCase( MetlifeInstitutionalConstants.DEATH)
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
                  if (coversDTO.getCovercode().equalsIgnoreCase( "TPD")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
                  if (coversDTO.getCovercode().contains("TRAUMA")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
                  if (coversDTO.getCovercode().equalsIgnoreCase( "IP")
                          && null != coversDTO.getCoverdecision()) {
                      updatedVector.add( coversDTO);
                  }
              }
            }
              
          }        
      }
      log.info("Valid cover vector finish");
      return updatedVector;
  }
  
	private static Boolean getCoverOptions(Eapplication applicationDTO){
		log.info("Get cover options start");
   	Boolean isReduceCancel = Boolean.FALSE;
   	StringBuilder strBuffer = new StringBuilder();
   	for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			Applicant applicantDTO = (Applicant) iter.next();
			/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
			if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();	
					if(null!=coversDTO){						
						if(null!=coversDTO.getCoveroption() && "Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption())){
							strBuffer.append("DECREASE");
						}
						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.CANCEL)){
							strBuffer.append(MetlifeInstitutionalConstants.CANCEL);
						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE)){
							strBuffer.append(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE);
						}
					}
						
					
				}
			}
		}    	
   	if(!strBuffer.toString().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE) && (strBuffer.toString().contains("DECREASE") || strBuffer.toString().contains(MetlifeInstitutionalConstants.CANCEL))){			
   		isReduceCancel = Boolean.TRUE;
		}
	log.info("Get cover options finish");
   	return isReduceCancel;
   }
	
	private static Boolean getCoverOptionsOnlyCancel(Eapplication applicationDTO){
		log.info("Get cover options only cancel start");
   	Boolean isReduceCancel = Boolean.FALSE;
   	StringBuilder strBuffer = new StringBuilder();
   	for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			Applicant applicantDTO = (Applicant) iter.next();
			/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
			if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();	
					if(null!=coversDTO){						
						if(null!=coversDTO.getCoveroption() && "Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption())){
							strBuffer.append("DECREASE");
						}
						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.CANCEL)){
							strBuffer.append(MetlifeInstitutionalConstants.CANCEL);
						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE)){
							strBuffer.append(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE);
						}
					}
						
					
				}
			}
		}    	
   	if(!strBuffer.toString().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE) && (!strBuffer.toString().contains("DECREASE") || strBuffer.toString().contains(MetlifeInstitutionalConstants.CANCEL))){			
   		isReduceCancel = Boolean.TRUE;
		}
	log.info("Get cover options only cancel finish");
   	return isReduceCancel;
   }
	
	/*169682:  Added Change Application Category Codes passed to MetFlow - Start*/
	private static Boolean getCoverOptionsOnDecreaseAlone(Eapplication applicationDTO){
		log.info("Get cover options Death Alone start");
   	Boolean isDecreaseCover = Boolean.FALSE;
   	StringBuilder strBuffer = new StringBuilder();
   	for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			Applicant applicantDTO = (Applicant) iter.next();
			/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
			if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();	
					if(null!=coversDTO){						
						if(null!=coversDTO.getCoveroption() && "Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption())){
							strBuffer.append("DECREASE");
						}
						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.CANCEL)){
							strBuffer.append(MetlifeInstitutionalConstants.CANCEL);
						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE)){
							strBuffer.append(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE);
						}
					}
						
					
				}
			}
		}    	
   	if(!strBuffer.toString().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE) && (strBuffer.toString().contains("DECREASE") && !strBuffer.toString().contains(MetlifeInstitutionalConstants.CANCEL))){			
   		isDecreaseCover = Boolean.TRUE;
	}
	log.info("Get cover options Death Alone finish");
   	return isDecreaseCover;
   }
	
	
	private static Boolean getUnitisedToFixed(EapplyInput eapplyInput){
		log.info("Get Unitised to fixed start");
		Boolean isUnitisedToFixedDeath = Boolean.FALSE;
		Boolean isUnitisedToFixedTPD = Boolean.FALSE;
		Boolean isUnitisedToFixed = Boolean.FALSE;		
		
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
				for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
					CoverJSON coverJSON = (CoverJSON) iterator.next();
					if(coverJSON.getBenefitType()!=null){
							if("1".equalsIgnoreCase(coverJSON.getBenefitType())){
								if((coverJSON.getType()!=null && !"".equalsIgnoreCase(coverJSON.getType()) && "1".equalsIgnoreCase(coverJSON.getType())) 
										&& (eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null && !"".equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()) && MetlifeInstitutionalConstants.DC_FIXED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()))){		 									
									isUnitisedToFixedDeath = Boolean.TRUE;
								}else{
									isUnitisedToFixedDeath = Boolean.FALSE;
								}
								
							}else if("2".equalsIgnoreCase(coverJSON.getBenefitType())){
								if((coverJSON.getType()!=null && !"".equalsIgnoreCase(coverJSON.getType()) && "1".equalsIgnoreCase(coverJSON.getType())) 
										&& (eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null && !"".equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()) && MetlifeInstitutionalConstants.TPDFIXED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()))){		 									
									isUnitisedToFixedTPD = Boolean.TRUE;
								}else{
									isUnitisedToFixedTPD = Boolean.FALSE;
								}
							}
						}
				}
		}
		
		if(isUnitisedToFixedDeath && isUnitisedToFixedTPD){
			isUnitisedToFixed = Boolean.TRUE;
		}
   		log.info("Get Unitised to fixed finish");
   	return isUnitisedToFixed;
   }
	
	private static Boolean getFixedToUnitised(EapplyInput eapplyInput){
		log.info("Get Fixed to Unitised start");
		Boolean isFixedToUnitisedDeath = Boolean.FALSE;
		Boolean isFixedToUnitisedTPD = Boolean.FALSE;
		Boolean isFixedToUnitised = Boolean.FALSE;		
		
		if(eapplyInput.getExistingCovers()!=null && eapplyInput.getExistingCovers().getCover()!=null){
				for (Iterator iterator = eapplyInput.getExistingCovers().getCover().iterator(); iterator.hasNext();) {
					CoverJSON coverJSON = (CoverJSON) iterator.next();
					if(coverJSON.getBenefitType()!=null){
							if("1".equalsIgnoreCase(coverJSON.getBenefitType())){
								if((coverJSON.getType()!=null && !"".equalsIgnoreCase(coverJSON.getType()) && "2".equalsIgnoreCase(coverJSON.getType())) 
										&& (eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()!=null && !"".equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()) && MetlifeInstitutionalConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType()))){		 									
									isFixedToUnitisedDeath = Boolean.TRUE;
								}else{
									isFixedToUnitisedDeath = Boolean.FALSE;
								}
								
							}else if("2".equalsIgnoreCase(coverJSON.getBenefitType())){
								if((coverJSON.getType()!=null && !"".equalsIgnoreCase(coverJSON.getType()) && "2".equalsIgnoreCase(coverJSON.getType())) 
										&& (eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()!=null && !"".equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()) && MetlifeInstitutionalConstants.TPDUNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdCoverType()))){		 									
									isFixedToUnitisedTPD = Boolean.TRUE;
								}else{
									isFixedToUnitisedTPD = Boolean.FALSE;
								}
							}
						}
				}
		}
		
		if(isFixedToUnitisedDeath && isFixedToUnitisedTPD){
			isFixedToUnitised = Boolean.TRUE;
		}
   		log.info("Get Fixed to Unitised finish");
   	return isFixedToUnitised;
   }
	/*169682: Added Change Application Category Codes passed to MetFlow - End*/
	
	public static void sendMetFlowRequestToMQ(String inputMessage,Properties property) throws JAXBException, UnsupportedEncodingException, JMSException{
		log.info("Send met flow request to MQ start");
		MQQueue queue = null;
		int mqSelector = 1;
		MQQueueConnection connection= null;
		MQQueueSession session = null;
		MQQueueSender sender = null;
		while(mqSelector<5){
			try {
				connection = getBizFlowMQConnection(mqSelector,property);
				break;
			} catch (JMSException jmException) {
				log.error("Error in send metflow request to mq {}",jmException);
				mqSelector = mqSelector+1;
				if(mqSelector==5){
					SendMail sendMail = new SendMail("metlife.service@ausmetlife.com", "akishor@metlife.com", "BizFlow MQ connection failure", "MQ failure connection:-"+inputMessage);
					sendMail.sendMQConnectionFailure();
					throw jmException;
				}
			}
		}	  
		
		try {
			if(null!= connection){
			session = (MQQueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);		
			
		  
		  /*First read all MQ data in order to clear message from the output MQ*/
		  synchronized (BizflowXMLHelper.class) {		
				  connection.start();			 		  
				  queue = (MQQueue) session.createQueue(property.getProperty("METFLOW_IN_QUEUE_NAME"));
			      /*queue = (MQQueue) session.createQueue("queue:///test_in?targetClient=1");*/
			      sender =  (MQQueueSender) session.createSender(queue);	 
			      (queue).setTargetClient(JMSC.MQJMS_CLIENT_NONJMS_MQ );
			      sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			      JMSTextMessage message = (JMSTextMessage) session.createTextMessage(inputMessage); 	     
			      /*Start the connection*/
			      connection.start();    
			      sender.setPriority(0);
			      sender.send(message);     
			      log.info("Sent message:\\n {}" , message);	      
			     			        
			      	 
		  }  
			      
		}   
		} catch (JMSException jmException) {
			log.error("Error in JMS : {}",jmException.getMessage());
			throw jmException;
		}
		finally
		{
			if(null != connection && null!= sender)
			{
				sender.close();	
				session.close();
				connection.close();
			}
		     
		}
	
		log.info("Send met flow request to MQ finish");
	}
	
	private static MQQueueConnection getBizFlowMQConnection(int mqSelector,Properties property) throws JMSException{
		log.info("Get biz flow MQ connection start");
		MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
	      /* Config*/
		
	      cf.setHostName(property.getProperty("MQ_HOST_NAME_"+mqSelector));
	      cf.setPort(Integer.parseInt(property.getProperty("METFLOW_MQ_PORT_NAME_"+mqSelector)));
	      cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
	      cf.setQueueManager(property.getProperty("METFLOW_QUEUE_MANAGER_"+mqSelector));
	      cf.setChannel(property.getProperty("METFLOW_MQ_CHANNEL"));  
	      
	      MQQueueConnection connection = (MQQueueConnection) cf.createQueueConnection();
	      log.info("Get biz flow MQ connection finish");
	      return connection;
	}

}
