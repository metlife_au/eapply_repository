package au.com.metlife.eapply.calculator.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.metlife.eapply.bs.model.InsuranceModel;
import au.com.metlife.eapply.bs.model.InsuranceRuleInfo;
import au.com.metlife.eapply.calculator.model.InsurCalculatorVO;
import au.com.metlife.eapply.calculator.service.CalcService;





@RestController
@Component
public class CalcController {
	private static final Logger log = LoggerFactory.getLogger(CalcController.class);
	private final CalcService calcService;
	
	@Autowired
    public CalcController(final CalcService calcService) {
        this.calcService = calcService;
    }
	
	 @RequestMapping(value = "/insuranceCalc", method = RequestMethod.POST)
     public ResponseEntity<List<InsurCalculatorVO>> calculateInsuranceCost(@RequestBody InsuranceModel insuranceModel) {
		 log.info("Calculate calculateInsuranceCost start");
           List<InsurCalculatorVO> insuranceCalcvoList=null;
           try {
        	   insuranceCalcvoList=new ArrayList<>();
                 if(insuranceModel!=null){
                	 insuranceCalcvoList.add(calcService.calculateInsuranceCost(insuranceModel));
                 }
           } catch (Exception e) {
                 log.error("Error in calculateInsuranceCost: {}",e);
           }     
         if(insuranceCalcvoList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate calculateInsuranceCost finish");
         return new ResponseEntity<>(insuranceCalcvoList, HttpStatus.OK);
   }
	 
	 @RequestMapping("/getInsuranceRule")
	  public ResponseEntity<List<InsuranceRuleInfo>> getInsuranceRule(@RequestParam(value="fundId") String fundId) {	
		 log.info("Get getInsuranceRule start");
		 List<InsuranceRuleInfo> insuranceRuleList=null;
		 
		 try{
			 if(fundId!=null){
				 insuranceRuleList=calcService.getInsuranceRule(fundId);
					
			 }
		 }catch(Exception e){
			 log.error("Error in get getInsuranceRule : {}",e);
		 }
	    if(insuranceRuleList==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
	    }
	    log.info("Get getInsuranceRule finish");
	    return new ResponseEntity<>(insuranceRuleList, HttpStatus.OK);
		 
	 }
}
