package businesscomponents;

import pages.metlife.MetLifeHomePage;
import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

/**
 * Functional Library class
 * 
 * @author Cognizant
 */
public class MetLifeVerificationComponents extends ReusableLibrary {
	/**
	 * Constructor to initialize the functional library
	 * 
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object passed from the
	 *            {@link DriverScript}
	 */
	public MetLifeVerificationComponents(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
}