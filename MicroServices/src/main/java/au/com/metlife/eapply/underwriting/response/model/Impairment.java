package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Impairment implements Serializable{
	private String base;
	private String cat;
	private String med;
	private String name;
	private String riskType;
	private String sev;
	
	private List<Decision> listDecision;

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getMed() {
		return med;
	}

	public void setMed(String med) {
		this.med = med;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getSev() {
		return sev;
	}

	public void setSev(String sev) {
		this.sev = sev;
	}

	public List<Decision> getListDecision() {
		
		if (null == listDecision) {
			listDecision = new ArrayList<>();
		}
		return listDecision;
	}
}
