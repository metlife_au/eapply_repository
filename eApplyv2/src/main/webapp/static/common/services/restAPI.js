angular.module('restAPI',[]);
angular.module('restAPI')
  .service('fetchUrlSvc',['$http','$q', function($http,$q) {
    var _this = this;
    _this.headerConfig = {
       headers : {
         'Content-Type': 'application/json'
       }
     }

    _this.getUrls = function(path) {
      var defer = $q.defer();
      $http.get(path, _this.headerConfig).then(function(res) {
       _this.setUrlList(res.data);
       defer.resolve(res);
      }, function(err){
       //console.log("Error fetching urls " + JSON.stringify(err));
       defer.reject(err);
      });
      return defer.promise;
    };

    _this.setUrlList = function(list) {
     _this.urlList = list;
    };

    _this.getUrlList = function(){
     return _this.urlList;
    };
 }]);

angular.module('restAPI')
.service('fetchTokenNumSvc', function() {
  var _self = this;
  _self.setTokenId = function (tempToken) {
    _self.tokenId = tempToken;
  }
  _self.getTokenId = function () {
    return _self.tokenId;
  }
});

angular.module('restAPI')
.service('fetchClientDataSvc', function($http, $q,fetchTokenNumSvc) {
    var _self = this;
    _self.requestObj = function(url) {
      var defer = $q.defer();
      $http.post(url,fetchTokenNumSvc,{headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
        defer.resolve(response);
      }, function(response) {
        defer.reject(response);
      });
     return defer.promise;
    }
});

angular.module('restAPI')
.service('fetchDeathCoverSvc', function($sessionstorage) {
  var _self = this;
  _self.setDeathCover = function (deathDetails) {
    _self.deathCoverObject = deathDetails;
  }
  _self.getDeathCover = function () {
    return _self.deathCoverObject;
  }
});

angular.module('restAPI')
.service('fetchTpdCoverSvc', function($sessionstorage) {
  var _self = this;
  _self.setTpdCover = function (tpdDetails) {
    _self.tpdCoverObject = tpdDetails;
  }
  _self.getTpdCover = function () {
    return _self.tpdCoverObject;
  }
});

angular.module('restAPI')
.service('fetchIpCoverSvc', function($sessionstorage) {
  var _self = this;
  _self.setIpCover = function (ipDetails) {
    _self.ipCoverObject = ipDetails;
  }
  _self.getIpCover = function () {
    return   _self.ipCoverObject;
  }
});

angular.module('restAPI')
.service('fetchPersoanlDetailSvc', function($sessionstorage) {
  var _self = this;
  _self.setMemberDetails = function (details) {
    _self.memberDetailsObject = details;
  }
  _self.getMemberDetails = function () {
    return _self.memberDetailsObject;
  }
});

angular.module('restAPI')
.service('fetchNewMemberOfferSvc', function() {
  var _self = this;
  _self.setNewMemberOfferFlag = function (tempFlag) {
    _self.newMemberOfferFlag = tempFlag;
  }
  _self.getNewMemberOfferFlag = function () {
    return _self.newMemberOfferFlag;
  }
});

angular.module('restAPI')
.service('fetchQuoteSvc',['$http', '$q', 'fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc){
  var _self = this;
  _self.getList = function(url, fundCode) {
    var defer = $q.defer();
    $http.get(url, {
      headers:{
        'Content-Type': 'application/json',
        'Authorization':fetchTokenNumSvc.getTokenId()
      }, params:{
        'fundCode': fundCode
      }}).then(function(res){
        defer.resolve(res);
      }, function(err){
        //console.log("Error while getting industries " + JSON.stringify(err));
        defer.reject(err);
      });
    return defer.promise;
  };
}]);

angular.module('restAPI')
.service('fetchOccupationSvc', ['$http', '$q', 'fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc) {
  var _self = this;
  _self.getOccupationList = function(url,fundId,induCode) {
    var defer = $q.defer();
    $http.get(url, {
      headers:{
      'Content-Type': 'application/json',
      'Authorization':fetchTokenNumSvc.getTokenId()
      }, params:{
        'fundId': fundId,
        'induCode': induCode
    }}).then(function(res) {
      defer.resolve(res);
    }, function(err) {
      //console.log("Error while getting occupations " + JSON.stringify(err));
      defer.reject(err);
    });
    return defer.promise;
  };
}]);

angular.module('restAPI')
.service('fetchAppNumberSvc', function() {
  var _self = this;
  _self.setAppNumber = function(appnum) {
    _self.UniqueAppNum = appnum;
  };

  _self.getAppNumber = function() {
    return _self.UniqueAppNum;
  };
});

angular.module('restAPI')
.service('fetchIndustryList', function(fetchQuoteSvc, $q) {
  var _self = this;
  _self.industryOptions = {};
  _self.getObj = function(url) {
    var defer = $q.defer();
    fetchQuoteSvc.getList(url, "HOST").then(function(res) {
      _self.setIndustryOptions(res.data);
      defer.resolve(res.data);
    }, function(err){
      defer.reject(err);
      console.info("Error while fetching industry list " + JSON.stringify(err));
    });
    return defer.promise;
  }

  _self.getIndustryOptions = function() {
      return _self.industryOptions;
  }

  _self.setIndustryOptions = function(obj) {
      _self.industryOptions = obj;
  }

  _self.getIndustryName = function(industryCode) {
    var selectedIndustry = _self.industryOptions.filter(function(obj) {
      return industryCode == obj.key;
    });
    return selectedIndustry[0].value;
  }

});



angular.module('restAPI')
.service('saveEapplyData', function($http, $q,fetchTokenNumSvc) {
  var _self = this;
  _self.reqObj = function (url, data) {
    var defer = $q.defer();
    $http.post(url, data,
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
      defer.resolve(response);
    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  }
});

angular.module('restAPI')
.service('calcDeathAmtSvc',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var _self = this;
    _self.calculateAmount = function(url,data){
      var defer = $q.defer();
      $http.post(url, data, {headers:{
        'Content-Type': 'application/json',
        'Authorization':fetchTokenNumSvc.getTokenId()
      }}).then(function(res){
        factory = res.data;
        defer.resolve(res);
      }, function(err){
        //console.log("Error while calculating death premium " + JSON.stringify(err));
        defer.reject(err);
      });
      return defer.promise;
    };
}]);

angular.module('restAPI')
.service('calcTPDAmtSvc',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var _self = this;
    _self.calculateAmount = function(url,data){
      var defer = $q.defer();
      $http.post(url, data, {headers:{
        'Content-Type': 'application/json',
        'Authorization':fetchTokenNumSvc.getTokenId()
      }}).then(function(res){
        factory = res.data;
        defer.resolve(res);
      }, function(err){
        //console.log("Error while calculating TPD premium " + JSON.stringify(err));
        defer.reject(err);
      });
      return defer.promise;
    };
}]);

angular.module('restAPI')
.service('calcIPAmtSvc',['$http', '$q', 'fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc){
    var _self = this;
    _self.calculateAmount = function(url,data){
      var defer = $q.defer();
      $http.post(url, data, {headers:{
        'Content-Type': 'application/json',
        'Authorization':fetchTokenNumSvc.getTokenId()
      }}).then(function(res){
        factory = res.data;
        defer.resolve(res);
      }, function(err){
        //console.log("Error while calculating TPD premium " + JSON.stringify(err));
        defer.reject(err);
      });
      return defer.promise;
    };
}]);

angular.module('restAPI')
.service('extendAppModel',['fetchPersoanlDetailSvc', 'fetchAppNumberSvc', function(fetchPersoanlDetailSvc, fetchAppNumberSvc){
    var _self = this;
    _self.extendObj = function(dataObj) {
      _self.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
      _self.personalDetails = _self.inputDetails.personalDetails || {};
      _self.contactDetails = _self.inputDetails.contactDetails || {};

      if(dataObj) {
          _self.dataObj = dataObj;
      } else {
        _self.dataObj = {occupationDetails:{}};
        _self.dataObj.email = _self.contactDetails.emailAddress;
        _self.dataObj.contactType = _self.contactDetails.prefContact;
        _self.dataObj.contactPrefTime = _self.contactDetails.prefContactTime;
        _self.dataObj.dob = _self.personalDetails.dateOfBirth;
        _self.dataObj.occupationDetails.gender = _self.personalDetails.gender;
        _self.dataObj.firstName = _self.personalDetails.firstName;
        _self.dataObj.lastName = _self.personalDetails.lastName;
        _self.dataObj.name = _self.personalDetails.firstName+" "+_self.personalDetails.lastName;
        _self.dataObj.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      }
      
      return _self.dataObj;
    };
}]);

angular.module('restAPI')
.service('extendAppModelMTAA',['fetchPersoanlDetailSvc', 'fetchAppNumberSvc', function(fetchPersoanlDetailSvc, fetchAppNumberSvc){
    var _self = this;
    _self.extendObj = function(dataObj) {
      _self.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
      _self.personalDetails = _self.inputDetails.personalDetails || {};
      _self.contactDetails = _self.inputDetails.contactDetails || {};
      _self.addressDetails = _self.inputDetails.address || {}

      if(dataObj) {
          _self.dataObj = dataObj;
      } else {
        _self.dataObj = {applicant:{}};
        _self.dataObj.clientRefNumber = _self.inputDetails.clientRefNumber;
        _self.dataObj.applicant.clientRefNumber =  _self.inputDetails.clientRefNumber;
        _self.dataObj.applicant.dateJoinedFund = _self.inputDetails.dateJoined;
        _self.dataObj.applicant.memberType = _self.inputDetails.memberType;
        _self.dataObj.applicant.contactDetails={};
        _self.dataObj.applicant.emailId = _self.contactDetails.emailAddress;
        _self.dataObj.applicant.contactDetails.preferedContacType = _self.contactDetails.prefContact;
        _self.dataObj.applicant.contactDetails.preferedContactTime = _self.contactDetails.prefContactTime;
        _self.dataObj.applicant.contactDetails.preferedContactNumber =_self.contactDetails.mobilePhone;
        _self.dataObj.applicant.contactDetails.mobilePhone = _self.contactDetails.mobilePhone;
        _self.dataObj.applicant.contactDetails.workPhone =_self.contactDetails.workPhone;
        _self.dataObj.applicant.contactDetails.homePhone = _self.contactDetails.homePhone;
        _self.dataObj.applicant.contactDetails.country = _self.addressDetails.country;
        _self.dataObj.applicant.contactDetails.addressLine1 =_self.addressDetails.line1;
        _self.dataObj.applicant.contactDetails.addressLine2 = _self.addressDetails.line2;
        _self.dataObj.applicant.contactDetails.postCode =_self.addressDetails.postCode;
        _self.dataObj.applicant.contactDetails.state = _self.addressDetails.state;
        _self.dataObj.applicant.contactDetails.suburb = _self.addressDetails.suburb;
        _self.dataObj.applicant.contactDetails.otherContactType = _self.addressDetails.otherContactType;
        _self.dataObj.applicant.contactDetails.otherContactNumber = _self.addressDetails.otherContactNumber;
        _self.dataObj.applicant.customerReferenceNumber = _self.contactDetails.fundEmailAddress;
        _self.dataObj.applicant.birthDate = _self.personalDetails.dateOfBirth;
        _self.dataObj.applicant.gender = _self.personalDetails.gender;
        _self.dataObj.applicant.firstName = _self.personalDetails.firstName;
        _self.dataObj.applicant.lastName = _self.personalDetails.lastName;
        _self.dataObj.applicant.contactType = _self.contactDetails.prefContact;
        _self.dataObj.name = _self.personalDetails.firstName+" "+_self.personalDetails.lastName;
       _self.dataObj.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
      }
      
      return _self.dataObj;
    };
}]);

angular.module('restAPI')
.service('extendAppModelGuild',['fetchPersoanlDetailSvc', 'fetchAppNumberSvc', function(fetchPersoanlDetailSvc, fetchAppNumberSvc){
    var _self = this;
    _self.extendObj = function(dataObj) {
      _self.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
      _self.personalDetails = _self.inputDetails.personalDetails || {};
      _self.contactDetails = _self.inputDetails.contactDetails || {};
      _self.addressDetails = _self.inputDetails.address || {}

      if(dataObj) {
          _self.dataObj = dataObj;
      } else {
        _self.dataObj = {applicant:{}};
        _self.dataObj.clientRefNumber = _self.inputDetails.clientRefNumber;
        _self.dataObj.applicant.clientRefNumber =  _self.inputDetails.clientRefNumber;
        _self.dataObj.applicant.dateJoinedFund = _self.inputDetails.dateJoined;
        _self.dataObj.applicant.memberType = _self.inputDetails.memberType;
        _self.dataObj.applicant.contactDetails={};
        _self.dataObj.applicant.emailId = _self.contactDetails.emailAddress;
        _self.dataObj.applicant.contactDetails.preferedContacType = _self.contactDetails.prefContact;
        _self.dataObj.applicant.contactDetails.preferedContactTime = _self.contactDetails.prefContactTime;
        _self.dataObj.applicant.contactDetails.preferedContactNumber =_self.contactDetails.mobilePhone;
        _self.dataObj.applicant.contactDetails.mobilePhone = _self.contactDetails.mobilePhone;
        _self.dataObj.applicant.contactDetails.workPhone =_self.contactDetails.workPhone;
        _self.dataObj.applicant.contactDetails.homePhone = _self.contactDetails.homePhone;
        _self.dataObj.applicant.contactDetails.country = _self.addressDetails.country;
        _self.dataObj.applicant.contactDetails.addressLine1 =_self.addressDetails.line1;
        _self.dataObj.applicant.contactDetails.addressLine2 = _self.addressDetails.line2;
        _self.dataObj.applicant.contactDetails.postCode =_self.addressDetails.postCode;
        _self.dataObj.applicant.contactDetails.state = _self.addressDetails.state;
        _self.dataObj.applicant.contactDetails.suburb = _self.addressDetails.suburb;
        _self.dataObj.applicant.contactDetails.otherContactType = _self.addressDetails.otherContactType;
        _self.dataObj.applicant.contactDetails.otherContactNumber = _self.addressDetails.otherContactNumber;
        _self.dataObj.applicant.customerReferenceNumber = _self.contactDetails.fundEmailAddress;
        _self.dataObj.applicant.birthDate = _self.personalDetails.dateOfBirth;
        _self.dataObj.applicant.gender = _self.personalDetails.gender;
        _self.dataObj.applicant.firstName = _self.personalDetails.firstName;
        _self.dataObj.applicant.lastName = _self.personalDetails.lastName;
        _self.dataObj.applicant.contactType = _self.contactDetails.prefContact;
        _self.dataObj.name = _self.personalDetails.firstName+" "+_self.personalDetails.lastName;
       _self.dataObj.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
      }
      
      return _self.dataObj;
    };
}]);

angular.module('restAPI')
.service('mapAppData',['auraRespSvc', function(auraRespSvc) {
  var _self = this;
  _self.mapObj = function() {
    _self.inputDetails = angular.copy(auraRespSvc.getResponse()) || {};

    _self.mapObj.contactPrefTime  = "Morning (9am \55 12pm)";
    _self.mapObj.contactType = 'Mobile';

    if( _self.inputDetails.contactPrefTime == '2')
      _self.mapObj.contactPrefTime  = "Afternoon (12pm \55 6pm)";

    if(_self.inputDetails.contactType == "2") {
      _self.mapObj.contactType = 'Home';
    } else if(_self.inputDetails.contactType == "3") {
      _self.mapObj.contactType = 'Work';
    }

    angular.extend(_self.inputDetails, _self.mapObj);
    return _self.inputDetails;
  };
}]);

// All Aura services

angular.module('restAPI')
.service('fetchAuraTransferData', function($http, $q, auraTransferInitiateSvc, auraInputSvc, fetchTokenNumSvc){
  var _self = this;
  _self.requestObj = function (url) {
    var defer = $q.defer();
    $http.post(url, auraInputSvc, {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
      defer.resolve(response);
    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  }
});

angular.module('restAPI')
.service('auraTransferInitiateSvc', function() {
  var _self = this;
  _self.setInput = function (tempInput) {
  _self.input = tempInput;
  }
  _self.getInput = function () {
  return _self.input;
  }
});

angular.module('restAPI')
.service('auraPostSvc', function($http, $q,auraRespSvc, fetchTokenNumSvc){
  var _self = this;
  _self.reqObj = function (url) {
    var defer = $q.defer();
    $http.post(url,auraRespSvc.getResponse(),
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {

      defer.resolve(response);

    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  }
});

angular.module('restAPI')
.service('clientMatchSvc', function($http, $q, auraInputSvc, fetchTokenNumSvc){

  var _self = this;
 _self.reqObj = function (url) {

    var defer = $q.defer();

    $http.post(url, {firstName:auraInputSvc.getFirstName(), surName:auraInputSvc.getLastName(), dob:auraInputSvc.getDob()}).then(function(response) {

      defer.resolve(response);

    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  }

});

angular.module('restAPI')
.service('auraInputSvc', function() {

  var _self = this;
 _self.setFund = function (tempInput) {
    _self.fund = tempInput;
  }
   _self.getFund = function () {
     return _self.fund;
  }
   _self.setMode = function (tempInput) {
                   _self.mode = tempInput;
  }
   _self.getMode = function () {
     return _self.mode;
  }
   _self.setName = function (tempInput) {
                   _self.name = tempInput;
  }
   _self.getName = function () {
     return _self.name;
  }
   _self.setAge = function (tempInput) {
                   _self.age = tempInput;
  }
   _self.getAge = function () {
     return _self.age;
  }
    _self.setDeathAmt = function (tempInput) {
                   _self.deathAmt = tempInput;
  }
   _self.getDeathAmt = function () {
     return _self.deathAmt;
  }

    _self.setTpdAmt = function (tempInput) {
                   _self.tpdAmt = tempInput;
  }
   _self.getTpdAmt = function () {
     return _self.tpdAmt;
  }

    _self.setIpAmt = function (tempInput) {
                   _self.ipAmt = tempInput;
  }
   _self.getIpAmt = function () {
     return _self.ipAmt;
  }

    _self.setWaitingPeriod = function (tempInput) {
                   _self.waitingPeriod = tempInput;
  }
   _self.getWaitingPeriod = function () {
     return _self.waitingPeriod;
  }

    _self.setBenefitPeriod = function (tempInput) {
                   _self.benefitPeriod = tempInput;
  }
   _self.getBenefitPeriod = function () {
     return _self.benefitPeriod;
  }
   _self.setAppnumber = function (tempInput) {
                   _self.appnumber = tempInput;
  }
   _self.getAppnumber = function () {
     return _self.appnumber;
  }

    _self.setIndustryOcc = function (tempInput) {
                   _self.industryOcc = tempInput;
  }
   _self.getIndustryOcc = function () {
     return _self.industryOcc;
  }

    _self.setGender = function (tempInput) {
                   _self.gender = tempInput;
  }
   _self.getGender = function () {
     return _self.gender;
  }

    _self.setCountry = function (tempInput) {
                   _self.country = tempInput;
  }
   _self.getCountry = function () {
     return _self.country;
  }

    _self.setSalary = function (tempInput) {
                   _self.salary = tempInput;
  }
   _self.getSalary = function () {
     return _self.salary;
  }

    _self.setFifteenHr = function (tempInput) {
                   _self.fifteenHr = tempInput;
  }
   _self.getFifteenHr = function () {
     return _self.fifteenHr;
  }

    _self.setCompanyId = function (tempInput) {
                   _self.companyId = tempInput;
  }
   _self.getCompanyId = function () {
     return _self.companyId;
  }

    _self.setClientname = function (tempInput) {
                   _self.clientname = tempInput;
  }
   _self.getClientname = function () {
     return _self.clientname;
  }

    _self.setDob = function (tempInput) {
                   _self.dob = tempInput;
  }
   _self.getDob = function () {
     return _self.dob;
  }

    _self.setFirstName = function (tempInput) {
                   _self.firstName = tempInput;
  }
   _self.getFirstName = function () {
     return _self.firstName;
  }
   _self.setLastName = function (tempInput) {
                   _self.lastName = tempInput;
  }
   _self.getLastName = function () {
     return _self.lastName;
  }
   _self.setSpecialTerm = function (tempInput) {
                   _self.specialTerm = tempInput;
  }
   _self.getSpecialTerm = function () {
     return _self.specialTerm;
  }
   _self.setExistingTerm = function (tempInput) {
                   _self.existingTerm = tempInput;
  }
   _self.getExistingTerm = function () {
     return _self.existingTerm;
  }

    _self.setMemberType = function (tempInput) {
                   _self.memberType = tempInput;
  }
   _self.getMemberType = function () {
     return _self.memberType;
  }
   _self.setSmoker = function (tempInput) {
	    _self.smoker = tempInput;
	  }
	   _self.getSmoker = function () {
	     return _self.smoker;
	  }

 });

angular.module('restAPI')
.service('submitAuraSvc', function($http, $q, auraInputSvc, fetchTokenNumSvc){
  var _self = this;
  _self.requestObj = function (url) {
    var defer = $q.defer();

    $http.post(url,auraInputSvc,
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {

    defer.resolve(response);
    }, function(response) {
    defer.reject(response);
    });
    return defer.promise;
  }
});



angular.module('restAPI')
.service('auraRespSvc', function() {
  var _self = this;
  _self.setResponse = function (tempInput) {
    _self.response = tempInput;
  }
  _self.getResponse = function () {
    return _self.response;
  }
});

angular.module('restAPI')
.service('submitEapplySvc', function($http, $q, fetchTokenNumSvc, mapAppData){
  var _self = this;
  _self.submitObj = function (url) {
    var defer = $q.defer();
    $http.post(url, mapAppData.mapObj(),
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
      defer.resolve(response);
    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  }
});

angular.module('restAPI')
.service('printPageSvc', function($http, $q, mapAppData, fetchTokenNumSvc){

  var _self = this;
  _self.reqObj = function (url) {
    var defer = $q.defer();
    $http.post(url, mapAppData.mapObj(),
      {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {

        defer.resolve(response);

      }, function(response) {
        defer.reject(response);
    });
    return defer.promise;
  }
});

angular.module('restAPI')
.service('fetchIndustryListVict', function(fetchQuoteSvc, $q) {
  var _self = this;
  _self.industryOptions = {};
  _self.getObj = function(url) {
    var defer = $q.defer();
    fetchQuoteSvc.getList(url, "VICT").then(function(res) {
      _self.setIndustryOptions(res.data);
      defer.resolve(res.data);
    }, function(err){
      defer.reject(err);
      console.info("Error while fetching industry list " + JSON.stringify(err));
    });
    return defer.promise;
  }

  _self.getIndustryOptions = function() {
      return _self.industryOptions;
  }

  _self.setIndustryOptions = function(obj) {
      _self.industryOptions = obj;
  }

  _self.getIndustryName = function(industryCode) {
    var selectedIndustry = _self.industryOptions.filter(function(obj) {
      return industryCode == obj.key;
    });
    return selectedIndustry[0].value;
  }

});

angular.module('restAPI')
.service('fetchIndustryListMtaa', function(fetchQuoteSvc, $q) {
  var _self = this;
  _self.industryOptions = {};
  _self.getObj = function(url) {
    var defer = $q.defer();
    fetchQuoteSvc.getList(url, "MTAA").then(function(res) {
      _self.setIndustryOptions(res.data);
      defer.resolve(res.data);
    }, function(err){
      defer.reject(err);
      console.info("Error while fetching industry list " + JSON.stringify(err));
    });
    return defer.promise;
  }

  _self.getIndustryOptions = function() {
      return _self.industryOptions;
  }

  _self.setIndustryOptions = function(obj) {
      _self.industryOptions = obj;
  }

  _self.getIndustryName = function(industryCode) {
    var selectedIndustry = _self.industryOptions.filter(function(obj) {
      return industryCode == obj.key;
    });
    return selectedIndustry[0].value;
  }

});

angular.module('restAPI')
.service('fetchIndustryListGuild', function(fetchQuoteSvc, $q) {
  var _self = this;
  _self.industryOptions = {};
  _self.getObj = function(url) {
    var defer = $q.defer();
    fetchQuoteSvc.getList(url, "GUIL").then(function(res) {
      _self.setIndustryOptions(res.data);
      defer.resolve(res.data);
    }, function(err){
      defer.reject(err);
      console.info("Error while fetching industry list " + JSON.stringify(err));
    });
    return defer.promise;
  }

  _self.getIndustryOptions = function() {
      return _self.industryOptions;
  }

  _self.setIndustryOptions = function(obj) {
      _self.industryOptions = obj;
  }

  _self.getIndustryName = function(industryCode) {
    var selectedIndustry = _self.industryOptions.filter(function(obj) {
      return industryCode == obj.key;
    });
    return selectedIndustry[0].value;
  }

});

angular.module('restAPI')
.service('fetchIndustryListGuil', function(fetchQuoteSvc, $q) {
  var _self = this;
  _self.industryOptions = {};
  _self.getObj = function(url) {
    var defer = $q.defer();
    fetchQuoteSvc.getList(url, "GUIL").then(function(res) {
      _self.setIndustryOptions(res.data);
      defer.resolve(res.data);
    }, function(err){
      defer.reject(err);
      console.info("Error while fetching industry list " + JSON.stringify(err));
    });
    return defer.promise;
  }

  _self.getIndustryOptions = function() {
      return _self.industryOptions;
  }

  _self.setIndustryOptions = function(obj) {
      _self.industryOptions = obj;
  }

  _self.getIndustryName = function(industryCode) {
    var selectedIndustry = _self.industryOptions.filter(function(obj) {
      return industryCode == obj.key;
    });
    return selectedIndustry[0].value;
  }

});

angular.module('restAPI')
.service('DownloadPDFSvc',['$http', '$q', 'fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc){
    var _self = this;
  _self.download = function(url,fileName) {
    var defer = $q.defer();
    $http.post(url, {},{headers:{
      'Authorization':fetchTokenNumSvc.getTokenId()
    }, params:{
      'file_name': fileName
    },responseType: 'arraybuffer',
    transformResponse: function (data) {
      var pdf;
      if (data) {
        pdf = new Blob([data], {
          type: 'application/pdf'
        });
      }
      return {
        response: pdf
      };
    }}).then(function(res){
      factory = res.data;
      defer.resolve(res);
    }, function(err){
      //console.log("Error while getting pdf data " + JSON.stringify(err));
      defer.reject(err);
    });
    return defer.promise;
    };
}]);