package au.com.metlife.eapply.ct.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface  DocumentRepository extends JpaRepository<au.com.metlife.eapply.ct.model.DocumentBO, String>{
	public List<au.com.metlife.eapply.ct.model.DocumentBO> findByPolicyNum(String policyNum);
}
