package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class AcceptedAppStatus implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean deathOnlyStatus;
	private boolean tpdStatus;
	private boolean ipStatus;
	public boolean isDeathOnlyStatus() {
		return deathOnlyStatus;
	}
	public void setDeathOnlyStatus(boolean deathOnlyStatus) {
		this.deathOnlyStatus = deathOnlyStatus;
	}
	public boolean isTpdStatus() {
		return tpdStatus;
	}
	public void setTpdStatus(boolean tpdStatus) {
		this.tpdStatus = tpdStatus;
	}
	public boolean isIpStatus() {
		return ipStatus;
	}
	public void setIpStatus(boolean ipStatus) {
		this.ipStatus = ipStatus;
	}

}
