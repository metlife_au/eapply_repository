package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ALIAcceptedPageObjects;
import uimap.metlife.ALIDeclinedPageObjects;
import uimap.metlife.ALISummaryPageObjects;


public class ALIDeclinedPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public ALIDeclinedPage DeclinedPage() {
		DeclineDetails();
		return new ALIDeclinedPage(scriptHelper);
	}

	public ALIDeclinedPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void DeclineDetails() {

			
		try {
			
			tryAction(fluentWaitElement(ALIDeclinedPageObjects.btnfinish), "Click", "Finish");
			Thread.sleep(1000);
			
			switchToActiveWindow();
			driver.manage().window().maximize();
				
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
