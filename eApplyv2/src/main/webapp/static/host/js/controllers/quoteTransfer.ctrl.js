/* Transfer Cover Controller,Progressive and Mandatory validations Starts  */
HostApp.controller('quotetransfer',['$scope','$rootScope', '$routeParams','$location','$http','$timeout','$window','persoanlDetailService','MaxLimitService','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'TransferCalculateService', 'NewOccupationService','auraInputService','PersistenceService','ngDialog','auraResponseService','Upload','urlService','saveEapply','RetrieveAppDetailsService','DownloadPDFService','printQuotePage','tokenNumService', '$q','$filter','APP_CONSTANTS',
                                         function($scope,$rootScope,$routeParams,$location,$http,$timeout,$window,persoanlDetailService,MaxLimitService,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService, TransferCalculateService, NewOccupationService, auraInputService,PersistenceService,ngDialog,auraResponseService,Upload,urlService,saveEapply,RetrieveAppDetailsService,DownloadPDFService,printQuotePage,tokenNumService, $q, $filter, APP_CONSTANTS){
  	/* Code for appD starts */
    var pageTracker = null;
    if(ADRUM) {
      pageTracker = new ADRUM.events.VPageView();
      pageTracker.start();
    }
  
    $scope.$on('$destroy', function() {
      pageTracker.end();
      ADRUM.report(pageTracker);
    });
    /* Code for appD ends */
    $scope.urlList = urlService.getUrlList();
    $scope.phoneNumbrTrans = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
    $scope.emailFormatTrans = APP_CONSTANTS.emailFormat;
    // ($scope.deathCoverTransDetails.amount=='0')
    $scope.indexation= {
        death: false,
        disable: false
    };
    $scope.premiumFrequencyOptions = ['Monthly', 'Yearly', 'Weekly'];
    $scope.showWithinOfficeTransferQuestion = false;
    $scope.showTertiaryTransferQuestion = false;
    $scope.showHazardousTransferQuestion = false;
    $scope.showOutsideOffice = false;
    $scope.premTransFreq = "Weekly";
    $scope.contactTypeOptions = ["Home", "Work", "Mobile"];
    $scope.isIPCoverRequiredDisabled = true;
    $scope.occupUpgradeNotEligible = false;
    $scope.modelOptions = {updateOn: 'blur'};
    var deathTransDBCategory, tpdTransDBCategory, ipTransDBCategory;

    $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
    $scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
    $scope.ipCoverTransDetails = ipCoverService.getIpCover();

    $rootScope.$broadcast('enablepointer');
   /* $scope.fileNotUploadedError = false;*/
    $scope.fileFormatError = false;
    /*$scope.prevOtherOcc = null;*/
    /*Error Flags*/
    $scope.dodFlagErr = null;
    $scope.privacyFlagErr = null;
    $scope.TransTPDRequireCover = 0;
    $scope.TransDeathRequireCover = 0;
    $scope.lowerThanExitingFlag = false;
    $scope.setIndexation = function ($event) {
      $event.stopPropagation();
      $event.preventDefault();
      $scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
      if(!$scope.indexation.death) {
        $("#indexation-death").parent().removeClass('active');
        $("#indexation-disable").parent().removeClass('active');
      }
    }
    QuoteService.getList($scope.urlList.quoteUrl,"HOST").then(function(res){
      $scope.IndustryOptions = res.data;
    }, function(err){
      console.log("Error while getting industry options " + JSON.stringify(err));
    });

    var annualSalForTransUpgradeVal;

    //Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
    $scope.isEmpty = function(value){
      return ((value == "" || value == null) || value == "0");
    };

    
    $scope.isUnitized = function(value){
        return (value == "1");
      };
    
     /*var unitCheck = false;
      $scope.checkUnitisedCheckbox = function(){
        $timeout(function(){
          unitCheck = $('#equivalentUnit').hasClass('active');
        },1);
      };*/
    $scope.getOccupations = function(){
      if($scope.otherOccupationObj)
          $scope.otherOccupationObj.transferotherOccupation = '';
      $scope.occupUpgradeNotEligible = false;
      OccupationService.getOccupationList($scope.urlList.occupationUrl,"HOST",$scope.transferIndustry).then(function(res){
        $scope.OccupationList = res.data;
          $scope.occupationTransfer = '';
          if($scope.toggleThree)
    	  {
    	  $scope.toggleThree(false);
          $scope.toggleFour(false);
    	  }
      }, function(err){
        console.log("Error while fetching occupation options " + JSON.stringify(err));
      });
    };
    
    /*$scope.getOtherOccupationAS = function(entered) {

    	return $http.get('./occupation.json').then(function(response) {
		      $scope.occupationList=[];
		  if(response.data.Other) {
			  for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	               obj.name=response.data.Other[key];
	               $scope.occupationList.push(obj.name);
	        }
	      }
		  return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });
	  };  */

    $scope.go = function ( path ) {
      $location.path( path );
    };

    if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
	{
    /*$scope.deathUnitsForTransfer = $scope.deathCoverTransDetails.units;
    $scope.tpdUnitsForTransfer = $scope.tpdCoverTransDetails.units;*/
    	$scope.unitRoundMsg = "The total cover amount has been rounded up to the nearest number of units. ";
    	$scope.roundingIndDeath = false;
    	$scope.roundingIndTPD = false;
	}

 // Added to get user details and Maximum limits from Rulesheet
    var DCTransMaxAmount, TPDTransMaxAmount, IPTransMaxAmount;
    var mode3Flag = false;
    var inputDetails = persoanlDetailService.getMemberDetails();
    $scope.personalDetails = inputDetails.personalDetails;


    MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"HOST",inputDetails.memberType,"TCOVER").then(function(res){
    var limits = res.data;
    annualSalForTransUpgradeVal = limits[0].annualSalForUpgradeVal;
  }, function(error){
    console.info('Something went wrong while fetching limits ' + error);
  });

  if(inputDetails && inputDetails.contactDetails.emailAddress){
    $scope.transferEmail = inputDetails.contactDetails.emailAddress;
  }
  if(inputDetails && inputDetails.contactDetails.prefContactTime){
    if(inputDetails.contactDetails.prefContactTime == "1"){
      $scope.TransferTime= "Morning (9am - 12pm)";
    }else{
      $scope.TransferTime= "Afternoon (12pm - 6pm)";
    }
  }
  if(inputDetails.contactDetails.prefContact == null || inputDetails.contactDetails.prefContact == "")
	{
		inputDetails.contactDetails.prefContact =1;
	}
    if(inputDetails && inputDetails.contactDetails.prefContact){
      if(inputDetails.contactDetails.prefContact == "1"){
        $scope.preferredContactType= "Mobile";
        $scope.transferPhone = inputDetails.contactDetails.mobilePhone;
      }else if(inputDetails.contactDetails.prefContact == "2"){
        $scope.preferredContactType= "Home";
        $scope.transferPhone = inputDetails.contactDetails.homePhone;
      }else if(inputDetails.contactDetails.prefContact == "3"){
        $scope.preferredContactType= "Work";
        $scope.transferPhone = inputDetails.contactDetails.workPhone;
      }
     }
    $scope.changePrefContactType = function(){
      if($scope.preferredContactType == "Home"){
        $scope.transferPhone = inputDetails.contactDetails.homePhone;
      } else if($scope.preferredContactType == "Work"){
        $scope.transferPhone = inputDetails.contactDetails.workPhone;
      } else if($scope.preferredContactType == "Mobile"){
        $scope.transferPhone = inputDetails.contactDetails.mobilePhone;
      } else {
        $scope.transferPhone = '';
      }
    }
    console.log($scope.personalDetails);
    var fetchAppnum = true;
    var appNum;
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    $scope.otherOccupationObj = {'transferotherOccupation': ''};

    $scope.navigateToLandingPage = function (){
      /*if(window.confirm('Are you sure you want to navigate to Home Page?')){
        $location.path("/landing");
      }*/
      ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
          $location.path("/landing");
        }, function(e){
          if(e=='oncancel'){
            return false;
          }
        });
    }


  //disabling death tpd IP based on age
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
	$scope.ageLimit=anb;
	if($scope.deathCoverTransDetails && $scope.deathCoverTransDetails.benefitType && $scope.deathCoverTransDetails.benefitType == 1 ){
		if($scope.ageLimit <= 11 || $scope.ageLimit > 70){
			$('#deathsection').removeClass('active');
			$("#death").css("display", "none");
			$scope.isDeathDisabled = true;
		}
	}
	if($scope.tpdCoverTransDetails && $scope.tpdCoverTransDetails.benefitType && $scope.tpdCoverTransDetails.benefitType == 2 ){
		if($scope.ageLimit <= 11 || $scope.ageLimit > 65){
			$('#tpdsection').removeClass('active');
			$("#tpd").css("display", "none");
			$scope.isTPDDisabled = true;
		}
	}
	if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitType && $scope.ipCoverTransDetails.benefitType == 4 ){
		if($scope.ageLimit < 15 || $scope.ageLimit > 65){
			$('#ipsection').removeClass('active');
			$("#sc").css("display", "none");
			$scope.isIPDisabled = true;
		}
	}

    //end

  //maximum validation
    $scope.validateDeathMaxAmount = function(){
    	/*if(!checkAuto)
    	  {
    	  	$scope.autoCalculate();
    	  }*/
      $scope.coverAmtErrFlag = false;
      $scope.maxDeathErrorFlag = false;
      $scope.maxTotalTPDAmt = false;
       // if(parseInt($scope.deathCoverTransDetails.amount) > 2000000){
       //  $scope.TransDeathRequireCover = 0;
       //  $scope.maxDeathErrorFlag = true;
       // } else 
      if(parseInt($scope.TransDeathRequireCover) > 1500000) {
        $scope.maxDeathErrorFlag = true;
      } /*else if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)) {
           $scope.coverAmtErrFlag = true;
           $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
      }
      if(parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount)))) {
    	  $scope.maxTotalTPDAmt = true;
          $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
    	  }
      $scope.autoCalculate();*/
      if(autoCalculate && parseInt($scope.TransTPDRequireCover) > 0 && parseInt($scope.tpdTransCoverAmount) > parseInt($scope.dcTransCoverAmount)) {
    	  $scope.maxTotalTPDAmt = true;
          $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
    	  }
     };

     //15 hour question check to disable IP Start
     $scope.checkFifteenHourQuestion = function(){
			if($scope.fifteenHrsTransferQuestion == 'No'){
				/*$scope.isIPCoverNameDisabled = true;*/
				$scope.isIPCoverRequiredDisabled = true;
				$scope.TransIPRequireCover =' ';
				$scope.maxIpErrorFlag = false;

			} else {
				/*$scope.isIPCoverNameDisabled = false;*/
				$scope.isIPCoverRequiredDisabled = false;
			}
     }
   //15 hour question check to disable IP End

     $scope.validateTpdMaxAmount = function(){
    	 /*if(!checkAuto)
	  		{
  		$scope.autoCalculate();
	  		}*/
      $timeout(function() {
        $scope.coverAmtErrFlag = false;
        $scope.maxTpdErrorFlag = false;
        $scope.maxTpdCoverErrorFlag = false;
        $scope.maxTotalTPDAmt = false;
        var maxTpdvalues = 1500000;
        if(($scope.TransTPDRequireCover != null && parseInt($scope.TransTPDRequireCover) > 0) && ($scope.TransDeathRequireCover == null || parseInt($scope.TransDeathRequireCover) <= 0) ){
          $scope.coverAmtErrFlag = true;
          $scope.coverAmtErrMsg="You cannot apply for TPD cover without Death Cover.";
        } /*else if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
          $scope.coverAmtErrFlag = true;
          $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
        }else  if(parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount)))) {
    	  $scope.maxTotalTPDAmt = true;
          $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
    	  }*/
        else  if(autoCalculate && parseInt($scope.TransTPDRequireCover) > 0 && parseInt($scope.tpdTransCoverAmount) > parseInt($scope.dcTransCoverAmount)) {
        	  $scope.maxTotalTPDAmt = true;
              $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
        }
        else if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues) {
          $scope.maxTpdErrorFlag = true;
        } else if(parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount) > 5000000) {
          $scope.maxTpdCoverErrorFlag = true;
        }
        
        //$scope.autoCalculate();
      });
    	 // $scope.coverAmtErrFlag = false;
      //    $scope.maxTpdErrorFlag = false;
      //    var maxTpdvalues = 1500000;
         
      //    if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues) {
      //        $scope.TransTPDRequireCover = 1500000;
      //        $scope.maxTpdErrorFlag = true;
             
      //        if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
      //       	 $scope.coverAmtErrFlag = true;
      //       	 $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
      //       }
      //    }else if($scope.TransTPDRequireCover != null && $scope.TransDeathRequireCover == null ){
      //      $scope.coverAmtErrFlag = true;
      //      $scope.coverAmtErrMsg="You cannot apply for TPD cover without Death Cover.";
      //    } else if(parseInt($scope.tpdCoverTransDetails.amount) >= 5000000){
      //      $scope.TransTPDRequireCover = 0;
      //        $scope.maxTpdErrorFlag = true;
      //    } else if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
      //   	 $scope.coverAmtErrFlag = true;
      //   	 $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
        	 
      //   }
      //    else if(parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount) > 5000000){
      //      $scope.TransTPDRequireCover = 5000000 - $scope.tpdCoverTransDetails.amount;
      //      $scope.maxTpdErrorFlag = true;
      //    }
      //    else if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues && parseInt($scope.TransDeathRequireCover)== 1500000 ){
      //        $scope.maxTpdErrorFlag = true;
      //        $scope.TransTPDRequireCover=maxTpdvalues;
           
      //    }
      //   $scope.autoCalculate();
    };
    
        
    $scope.validateIpMaxAmount = function(checkAuto){
      var ipMaxAmount = 10000;
      $scope.ipCoverAmtErrFlag = false;
      $scope.maxIpErrorFlag = false;
      //var totalAllowableTransfer = 30000;
      var ninetyPercentOfSal = parseInt(0.90 * (parseInt($scope.annualTransferSalary)/12));
      var tempTotalCover = parseInt($scope.TransIPRequireCover);
      if(ipMaxAmount > ninetyPercentOfSal)
    	  {
    	  ipMaxAmount = ninetyPercentOfSal;
    	  }
      if(tempTotalCover > ipMaxAmount)
	  {
	  $scope.maxIpErrorFlag = true;
      $scope.TransIPRequireCover = ipMaxAmount;
	  }
  else if(tempTotalCover > ninetyPercentOfSal)
	  {
	  $scope.maxIpErrorFlag = true;
      $scope.TransIPRequireCover = ninetyPercentOfSal;
	  }
      /*if(ninetyPercentOfSal < parseInt($scope.ipCoverTransDetails.amount)) {
        $scope.TransIPRequireCover = 0;
        $scope.maxIpErrorFlag = true;
      } else if(ninetyPercentOfSal > parseInt($scope.ipCoverTransDetails.amount)) { 
        if(ninetyPercentOfSal >= totalAllowableTransfer) {
          //var tempTotalCover = parseInt($scope.TransIPRequireCover) + parseInt($scope.ipCoverTransDetails.amount);
          if(tempTotalCover > totalAllowableTransfer) {
            $scope.TransIPRequireCover = totalAllowableTransfer;
            $scope.maxIpErrorFlag = true;
          } else if(parseInt($scope.TransIPRequireCover) > ipMaxAmount) {
            $scope.maxIpErrorFlag = true;
            $scope.TransIPRequireCover = ipMaxAmount;
          }    
        } else {
          //var tempTotalCover = parseInt($scope.TransIPRequireCover) + parseInt($scope.ipCoverTransDetails.amount);
          if(tempTotalCover >= ninetyPercentOfSal) {
            $scope.TransIPRequireCover = ninetyPercentOfSal;
            $scope.maxIpErrorFlag = true;
            if(parseInt($scope.TransIPRequireCover) > ipMaxAmount) {
              $scope.maxIpErrorFlag = true;
              $scope.TransIPRequireCover = ipMaxAmount;
            }
          } else if(parseInt($scope.TransIPRequireCover) > ipMaxAmount) {
            $scope.maxIpErrorFlag = true;
            $scope.TransIPRequireCover = ipMaxAmount;
          }
        }
      }*/
      if(!checkAuto)
		{
		$scope.autoCalculate();
		}
    };

    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

    $scope.calculateTransfer = function(){
    	$scope.lowerThanExitingFlag = false;
      if(typeof($scope.TransDeathRequireCover) == "undefined" || $scope.TransDeathRequireCover==''){
        $scope.TransDeathRequireCover = 0;
      }
      if(typeof($scope.TransTPDRequireCover) == "undefined" || $scope.TransTPDRequireCover==''){
        $scope.TransTPDRequireCover = 0;
      }
      if(typeof($scope.TransIPRequireCover) == "undefined" || $scope.TransIPRequireCover==''){
        $scope.TransIPRequireCover = 0;
      }
      if(($scope.deathCoverTransDetails && $scope.deathCoverTransDetails.amount && typeof($scope.deathCoverTransDetails.amount) == "undefined") || $scope.deathCoverTransDetails.amount==''){
        $scope.deathCoverTransDetails.amount = 0;
      }
      if(($scope.tpdCoverTransDetails && $scope.tpdCoverTransDetails.amount && typeof($scope.tpdCoverTransDetails.amount) == "undefined") || $scope.tpdCoverTransDetails.amount==''){
        $scope.tpdCoverTransDetails.amount = 0;
      }
      if(($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.amount && typeof($scope.ipCoverTransDetails.amount) == "undefined") || $scope.ipCoverTransDetails.amount==''){
        $scope.ipCoverTransDetails.amount = 0;
      }

     /* if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitPeriod && $scope.ipCoverTransDetails.benefitPeriod != ''){
          $scope.benefitPeriodTransAddnl = $scope.ipCoverTransDetails.benefitPeriod;
      } else{
        $scope.benefitPeriodTransAddnl = $scope.benefitPeriodTransPer == 'Age 65' ? $scope.benefitPeriodTransPer : '2 Years';
      }*/
      
      if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
	  {
	  $scope.calculateForUnits();
	  }
  else
	  {
      
      var ruleModel = {
            "age": anb,
            "fundCode": "HOST",
            "gender": $scope.gender,
            "deathOccCategory": $scope.transferDeathOccupationCategory,
            "tpdOccCategory": $scope.transferTpdOccupationCategory,
            "ipOccCategory": $scope.transferIpOccupationCategory,
            "manageType": "TCOVER",
            "deathCoverType": "DcFixed",
            "tpdCoverType": "TpdFixed",
            "ipCoverType": "IpFixed",
            "premiumFrequency": $scope.premTransFreq,
            "ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
            "ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
            "deathTransferAmount": parseInt($scope.TransDeathRequireCover),
            "tpdTransferAmount": parseInt($scope.TransTPDRequireCover),
            "ipTransferAmount": parseInt($scope.TransIPRequireCover),
            "deathExistingAmount": parseInt($scope.deathCoverTransDetails.amount),
            "tpdExistingAmount": parseInt($scope.tpdCoverTransDetails.amount),
            "ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
          };

      TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
      //TransferCalculateService.calculate({}, ruleModel, function(res){
        var premium = res.data;
        autoCalculate = true;
        for(var i = 0; i < premium.length; i++){
          if(premium[i].coverType == 'DcFixed'){
            $scope.dcTransCoverAmount = premium[i].coverAmount;
            $scope.dcTransCost = premium[i].cost;
            if($scope.dcTransCost == null){
                $scope.dcTransCost = 0.00;
              }
          } else if(premium[i].coverType == 'TpdFixed'){
            $scope.tpdTransCoverAmount = premium[i].coverAmount;
            $scope.tpdTransCost = premium[i].cost;
            if($scope.tpdTransCost == null){
                $scope.tpdTransCost = 0.00;
              }
          } else if(premium[i].coverType == 'IpFixed'){
            $scope.ipTransCoverAmount = premium[i].coverAmount||0.00;
            $scope.ipTransCost = premium[i].cost;
            if($scope.ipTransCost == null){
                $scope.ipTransCost = 0.00;
              }
          }
      }

        $scope.totalTransCost = parseFloat($scope.dcTransCost) + parseFloat($scope.tpdTransCost) + parseFloat($scope.ipTransCost);
        if(parseInt($scope.ipTransCoverAmount) < parseInt($scope.ipCoverTransDetails.amount))
    	{
    	$scope.lowerThanExitingFlag = true;
    	}
        if(fetchAppnum){
          fetchAppnum = false;
          appNum = PersistenceService.getAppNumber();
        }
        $scope.validateAll();
      }, function(err){
        console.log("Error while calculating transfer premium " + JSON.stringify(err));
      });
	  }
    };
    $scope.coverAmtErrFlag= false;
    var autoCalculate = false;
    $scope.autoCalculate = function(){
      if(autoCalculate /*&& !$scope.coverAmtErrFlag && !$scope.maxTotalTPDAmt && !$scope.maxDeathErrorFlag && !$scope.maxTpdErrorFlag && !$scope.maxTpdCoverErrorFlag*/ ){
        $scope.calculateTransfer();
      }
    };

    $scope.getTransCategoryFromDB = function(fromOccupation){
		     if($scope.otherOccupationObj){
		   	     $scope.otherOccupationObj.transferotherOccupation = '';
		     }
		     /*if(fromOccupation && $scope.occupationTransfer!=="Other"){
		    	 $scope.otherOccupationObj.transferotherOccupation = '';
		      }*/
		     /*if( $scope.prevOtherOcc !== $scope.otherOccupationObj.transferotherOccupation){*/
		      if($scope.occupationTransfer != undefined /*|| $scope.otherOccupationObj.transferotherOccupation!= undefined*/){
		    	  if(fromOccupation) {
		    	        $scope.withinOfficeTransferQuestion = null;
		    	        $scope.tertiaryTransferQuestion = null;
		    	        $scope.hazardousTransferQuestion = null;
		    	        $scope.outsideOffice = null;                                                     
		    	   }
		        var occName = $scope.transferIndustry + ":" + $scope.occupationTransfer;
		    	  /*if($scope.occupationTransfer != undefined && ($scope.otherOccupationObj.transferotherOccupation == null || $scope.otherOccupationObj.transferotherOccupation == '')){
		    		  var occName = $scope.transferIndustry + ":" + $scope.occupationTransfer;
		          }else if ($scope.otherOccupationObj.transferotherOccupation != undefined){
		        	  	$scope.prevOtherOcc = $scope.otherOccupationObj.transferotherOccupation;
			  	    	if(($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.transferotherOccupation))!== undefined){
			  	    		 var occName = $scope.transferIndustry + ":" + $scope.otherOccupationObj.transferotherOccupation;
			  	    	}else{
			  	    		var occName = $scope.transferIndustry + ":" + $scope.occupationTransfer;
			  	    	}
		        	 
		          }*/
		    	  
		    	  
		        NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "HOST", occName).then(function(res){
		          deathTransDBCategory = res.data[0].deathfixedcategeory;
		          tpdTransDBCategory = res.data[0].tpdfixedcategeory;
		          ipTransDBCategory = res.data[0].ipfixedcategeory;
		          $scope.renderOccupationQuestions();
		        }, function(err){
		          console.info("Error while getting transfer category from DB " + JSON.stringify(err));
		        });
		      }
    };

    $scope.renderOccupationQuestions = function(){

      /*if($scope.fifteenHrsTransferQuestion == 'Yes'){*/
        if($scope.OccupationList /*|| $scope.otherOccupationObj.transferotherOccupation*/){
          var selectedOcc = $scope.OccupationList.filter(function(obj){
            return obj.occupationName == $scope.occupationTransfer;
        	/*if($scope.occupationTransfer && $scope.otherOccupationObj.transferotherOccupation == ''){
                  return obj.occupationName == $scope.occupationTransfer;
         	}else if($scope.otherOccupationObj.transferotherOccupation && (($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.transferotherOccupation))!== undefined)){
         		 return obj.occupationName == $scope.otherOccupationObj.transferotherOccupation;
         	}else{
         		return obj.occupationName == $scope.occupationTransfer;
         	}*/
          });
          var selectedOccObj = selectedOcc[0];
          $scope.occupUpgradeNotEligible = false;
          if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
              $scope.showWithinOfficeTransferQuestion = true;
        	 /* $scope.showWithinOfficeTransferQuestion = false;*/
              $scope.showTertiaryTransferQuestion = true;
              $scope.showHazardousTransferQuestion = false;
              $scope.showOutsideOffice = false;
              /*$scope.showOutsideOffice = true;*/
              occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','annualTransferSalary'];

              /*if($scope.withinOfficeTransferQuestion == 'No' && $scope.tertiaryTransferQuestion == 'Yes' && $scope.annualTransferSalary && parseFloat($scope.annualTransferSalary) >= parseFloat(annualSalForTransUpgradeVal)){*/
              if($scope.withinOfficeTransferQuestion == 'Yes' && $scope.tertiaryTransferQuestion == 'Yes' && $scope.annualTransferSalary && parseFloat($scope.annualTransferSalary) >= parseFloat(annualSalForTransUpgradeVal)){
                $scope.transferDeathOccupationCategory = 'Professional';
                $scope.transferTpdOccupationCategory = 'Professional';
                $scope.transferIpOccupationCategory = 'Professional';
              } else{
                $scope.transferDeathOccupationCategory = deathTransDBCategory;
                $scope.transferTpdOccupationCategory = tpdTransDBCategory;
                $scope.transferIpOccupationCategory = ipTransDBCategory;
              }
          } else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
            $scope.showWithinOfficeTransferQuestion = false;
              $scope.showTertiaryTransferQuestion = false;
              $scope.showHazardousTransferQuestion = true;
              $scope.showOutsideOffice = true;
              occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','hazardousTransferQuestion','outsideOffice','annualTransferSalary'];

              /*if($scope.hazardousTransferQuestion == 'No' && $scope.outsideOffice == 'No'){*/
              if($scope.hazardousTransferQuestion == 'No' && $scope.outsideOffice == 'Yes'){
                $scope.showWithinOfficeTransferQuestion = true;
                  $scope.showTertiaryTransferQuestion = true;
                occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','hazardousTransferQuestion','outsideOffice','withinOfficeTransferQuestion','tertiaryTransferQuestion','annualTransferSalary'];
                $scope.transferDeathOccupationCategory = 'White Collar';
                $scope.transferTpdOccupationCategory = 'White Collar';
                $scope.transferIpOccupationCategory = 'White Collar';
                /*if($scope.withinOfficeTransferQuestion == 'No' && $scope.tertiaryTransferQuestion == 'Yes' && $scope.annualTransferSalary && parseFloat($scope.annualTransferSalary) >= parseFloat(annualSalForTransUpgradeVal)){*/
                if($scope.withinOfficeTransferQuestion == 'Yes' && $scope.tertiaryTransferQuestion == 'Yes' && $scope.annualTransferSalary && parseFloat($scope.annualTransferSalary) >= parseFloat(annualSalForTransUpgradeVal)){
                  $scope.transferDeathOccupationCategory = 'Professional';
                  $scope.transferTpdOccupationCategory = 'Professional';
                  $scope.transferIpOccupationCategory = 'Professional';
                } else{
                  $scope.transferDeathOccupationCategory = 'White Collar';
                  $scope.transferTpdOccupationCategory = 'White Collar';
                  $scope.transferIpOccupationCategory = 'White Collar';
                }
              } else{
                $scope.showWithinOfficeTransferQuestion = false;
                  $scope.showTertiaryTransferQuestion = false;
                occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','hazardousTransferQuestion','outsideOffice','annualTransferSalary'];

                $scope.transferDeathOccupationCategory = deathTransDBCategory;
                $scope.transferTpdOccupationCategory = tpdTransDBCategory;
                $scope.transferIpOccupationCategory = ipTransDBCategory;
              }
          } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
            $scope.showWithinOfficeTransferQuestion = false;
              $scope.showTertiaryTransferQuestion = false;
              $scope.showHazardousTransferQuestion = true;
              $scope.showOutsideOffice = true;
              occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','hazardousTransferQuestion','outsideOffice','annualTransferSalary'];

            /*if($scope.hazardousTransferQuestion == 'No' && $scope.outsideOffice == 'No'){*/
              if($scope.hazardousTransferQuestion == 'No' && $scope.outsideOffice == 'Yes'){
              $scope.transferDeathOccupationCategory = 'White Collar';
                $scope.transferTpdOccupationCategory = 'White Collar';
                $scope.transferIpOccupationCategory = 'White Collar';
            } else{
              $scope.transferDeathOccupationCategory = deathTransDBCategory;
                $scope.transferTpdOccupationCategory = tpdTransDBCategory;
                $scope.transferIpOccupationCategory = ipTransDBCategory;
            }
          } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
            $scope.showWithinOfficeTransferQuestion = false;
              $scope.showTertiaryTransferQuestion = false;
              $scope.showHazardousTransferQuestion = false;
              $scope.showOutsideOffice = false;
              occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','annualTransferSalary'];

              $scope.transferDeathOccupationCategory = deathTransDBCategory;
            $scope.transferTpdOccupationCategory = tpdTransDBCategory;
            $scope.transferIpOccupationCategory = ipTransDBCategory;
          }
        }
      /*}*/ else{
    	  /*if($scope.OccupationList){
              var selectedOcc = $scope.OccupationList.filter(function(obj){
                return obj.occupationName == $scope.occupationTransfer;
              });
              var selectedOccObj = selectedOcc[0];*/
          $scope.showWithinOfficeTransferQuestion = false;
          $scope.showTertiaryTransferQuestion = false;
          $scope.showHazardousTransferQuestion = false;
          $scope.showOutsideOffice = false;
        occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','annualTransferSalary'];
        var occupationDownGrdeBtoC = false;
        if(deathTransDBCategory == 'White Collar'){
            $scope.transferDeathOccupationCategory = 'Standard';
            occupationDownGrdeBtoC = true;
          } else if(deathTransDBCategory == 'Professional'){
            $scope.transferDeathOccupationCategory = 'Professional';
            occupationDownGrdeBtoC = false;
          } else{
            $scope.transferDeathOccupationCategory = 'Standard';
            occupationDownGrdeBtoC = false;
          }

          if(tpdTransDBCategory == 'White Collar'){
            $scope.transferTpdOccupationCategory = 'Standard';
            occupationDownGrdeBtoC = true;
          } else if(tpdTransDBCategory == 'Professional'){
            $scope.transferTpdOccupationCategory = 'Professional';
            occupationDownGrdeBtoC = false;
          } else{
            $scope.transferTpdOccupationCategory = 'Standard';
            occupationDownGrdeBtoC = false;
          }

        if(ipTransDBCategory == 'White Collar'){
            $scope.transferIpOccupationCategory = 'Standard';
            occupationDownGrdeBtoC = true;
          } else if(ipTransDBCategory == 'Professional'){
            $scope.transferIpOccupationCategory = 'Professional';
            occupationDownGrdeBtoC = false;
          } else{
            $scope.transferIpOccupationCategory = 'Standard';
            occupationDownGrdeBtoC = false;
          }
        /*Added to show warning message for the occupation which has manual flag set to true or down grade occupation from white collar to standard */
        if($scope.OccupationList){
            var selectedOcc = $scope.OccupationList.filter(function(obj){
              return obj.occupationName == $scope.occupationTransfer;
            });
            var selectedOccObj = selectedOcc[0];
        if(occupationDownGrdeBtoC || selectedOccObj.manualFlag.toLowerCase() == 'true'){
        	$scope.occupUpgradeNotEligible = true;
        }else{
        	$scope.occupUpgradeNotEligible = false;
        }
      }
      }
      $scope.checkFifteenHourQuestion();
      $scope.autoCalculate();
    };


   /* Check if your is allowed to proceed to the next accordion */
  // TBC
  // Need to revisit, need better implementation
  $scope.isCollapsible = function(targetEle, event) {
    if( targetEle == 'collapseprivacy' && !$('#dodCkBoxLblId').hasClass('active')) {
      if($('#dodCkBoxLblId').is(':visible'))
          $scope.dodFlagErr = true;
      event.stopPropagation();
      return false;
    } else if( targetEle == 'collapseOne' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active'))) {
      if($('#privacyCkBoxLblId').is(':visible'))
          $scope.privacyFlagErr = true;
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseTwo' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
      if($("#collapseOne form").is(':visible'))
          $scope.CoverDetailsTransferFormSubmit($scope.coverDetailsTransferForm);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseThree' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
      if($("#collapseTwo form").is(':visible'))
          $scope.CoverDetailsTransferFormSubmit($scope.occupationDetailsTransferForm);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseFour' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid') || $("#collapseThree form").hasClass('ng-invalid'))) {
      if($("#collapseThree form").is(':visible'))
          $scope.CoverDetailsTransferFormSubmit($scope.previousCoverForm);
      event.stopPropagation();
      return false;
    }
  }

   // added for toggle and collapse the sections
    $scope.privacyCol = false;
    $scope.contactCol = false;
    $scope.occupationCol = false;
    $scope.previousSectionCol = false;
    $scope.transferSectionCol = false;
    var dodCheck;
    var privacyCheck;
    var privacyVal = 0;
    var contactVal = 0;
    var occupationVal = 0;
    var previousSectionVal = 0;
    var transferCoverVal = 0;

    // // privacy section
    // $scope.togglePrivacy = function(flag) {
    //     $scope.privacyCol = flag;
    //     $("a[data-target='#collapseprivacy']").click();

    // };

    // // contact section
    // $scope.toggleContact = function(flag) {
    //     $scope.contactCol = flag;
    //     $("a[data-target='#collapseOne']").click();

    // };

    /* TBC */
    $scope.togglePrivacy = function(checkFlag) {
        $scope.privacyCol = checkFlag;
        if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
    };

    $scope.toggleContact = function(checkFlag) {
        $scope.contactCol = checkFlag;
        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseOne']").click(); /* Can be improved */

    };

    // occupation section
    $scope.toggleTwo = function(checkFlag) {
        $scope.occupationCol = checkFlag;
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };

    // previous cover section
    $scope.toggleThree = function(checkFlag) {
        $scope.previousSectionCol = checkFlag;
        if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseThree']").click(); /* Can be improved */
    };

    // transfer quote section
    $scope.toggleFour = function(checkFlag) {
        $scope.transferSectionCol = checkFlag;
        if((checkFlag && $('#collapseFour').hasClass('collapse in')) || (!checkFlag && !$('#collapseFour').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseFour']").click(); /* Can be improved */
    };

   //  // occupation section
    // $scope.toggleTwo = function(flag) {
   //      $scope.occupationCol = flag;
   //      occupationVal++;
   //      $("a[data-target='#collapseTwo']").click();
   //  };

   //  // previous cover section
    // $scope.toggleThree = function(flag) {
   //      $scope.previousSectionCol = flag;
   //      previousSectionVal++;
   //      $("a[data-target='#collapseThree']").click();
   //  };

   //  // transfer quote section
    // $scope.toggleFour = function(flag) {
   //      $scope.transferSectionCol = flag;
   //      transferCoverVal++;
   //      $("a[data-target='#collapseFour']").click();
   //  };

  // validation for DOD checkbox
    $scope.checkDodState = function(){
      $timeout(function() {
        $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodCkBoxLblId').hasClass('active') : !$scope.dodFlagErr;
        if($('#dodCkBoxLblId').hasClass('active')) {
          $scope.togglePrivacy(true);
        } else {
          $scope.togglePrivacy(false);
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
      }, 1);
      // $timeout(function(){
      //  dodCheck = $('#dodCkBoxLblId').hasClass('active');
      //  if(dodCheck){
      //    $scope.dodFlagErr = false;
      //    privacyVal++;
      //    if(privacyVal > 0){
      //      $scope.togglePrivacy(true);
      //    } else{
      //      $scope.togglePrivacy(false);
      //    }
      //    if(contactVal > 0){
      //      $scope.toggleContact(true);
      //    } else{
      //      $scope.toggleContact(false);
      //    }
      //    if(occupationVal > 0){
      //      $scope.toggleTwo(true);
      //    }else{
      //      $scope.toggleTwo(false);
      //    }
      //    if(previousSectionVal > 0){
      //      $scope.toggleThree(true);
      //    }else{
      //      $scope.toggleThree(false);
      //    }
      //    if(transferCoverVal > 0){
      //      $scope.toggleFour(true);
      //    }else{
      //      $scope.toggleFour(false);
      //    }
      //  } else{
      //    $scope.dodFlagErr = true;
      //    $scope.togglePrivacy(false);
      //    $scope.toggleContact(false);
      //    $scope.toggleTwo(false);
      //    $scope.toggleThree(false);
      //    $scope.toggleFour(false);
      //  }
      // }, 1);
    };

    // validation for Privacy checkbox
    $scope.checkPrivacyState  = function(){
      $timeout(function() {
        $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyCkBoxLblId').hasClass('active') : !$scope.privacyFlagErr;
        if($('#privacyCkBoxLblId').hasClass('active')) {
          $scope.toggleContact(true);
        } else {
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
      }, 1);
    //   if(dodCheck){
   //     $timeout(function(){
    //      privacyCheck = $('#privacyCkBoxLblId').hasClass('active');
    //      if(privacyCheck){
    //        $scope.privacyFlagErr = false;
     //       contactVal++;
    //        if(contactVal > 0){
    //          $scope.toggleContact(true);
    //        } else{
    //          $scope.toggleContact(false);
    //        }
    //        if(occupationVal > 0){
    //          $scope.toggleTwo(true);
    //        }else{
    //          $scope.toggleTwo(false);
    //        }
    //        if(previousSectionVal > 0){
    //          $scope.toggleThree(true);
    //        }else{
    //          $scope.toggleThree(false);
    //        }
    //        if(transferCoverVal > 0){
    //          $scope.toggleFour(true);
    //        }else{
    //          $scope.toggleFour(false);
    //        }
    //      } else{
    //        $scope.privacyFlagErr = true;
    //        $scope.toggleContact(false);
    //        $scope.toggleTwo(false);
    //        $scope.toggleThree(false);
    //        $scope.toggleFour(false);
    //      }
    //    }, 1);
    //   }else{
    //    $scope.dodFlagErr = true;
    //    $scope.togglePrivacy(false);
      // $scope.toggleContact(false);
      // $scope.toggleTwo(false);
      // $scope.toggleThree(false);
      // $scope.toggleFour(false);
    //   }
    };

  if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
    $scope.gender ='';
  }else{
    $scope.gender = $scope.personalDetails.gender;
  }

  var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','annualTransferSalary'];
    var occupationDetailsOtherTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','transferotherOccupation','annualTransferSalary'];
    var coverDetailsTransferFormFields = ['coverDetailsTransferEmail', 'coverDetailsTransferPhone','coverDetailsTransferPrefTime','gender'];

    var previousCoverFormFields = ['previousFundName','membershipNumber'/*,'documentName'*/];
    var previousCoverFormFieldsWithChkBox = ['previousFundName','membershipNumber'/*,'documentName'*/];
    var TranscoverCalculatorFormFields = ['TransDeathRequireCover','TransTPDRequireCover','TransIPRequireCover'];
    $scope.checkCoverDetailsTransferFormPreviousMandatoryFields  = function (elementName,formName){
      var transferFormFields;
      if(formName == 'coverDetailsTransferForm'){
        transferFormFields = coverDetailsTransferFormFields;
      } else if(formName == 'occupationDetailsTransferForm'){
        if($scope.occupationTransfer != undefined && $scope.occupationTransfer == 'Other'){
          transferFormFields = occupationDetailsOtherTransferFormFields;
        } else{
          transferFormFields = occupationDetailsTransferFormFields;
        }
      } else if(formName == 'previousCoverForm'){
    	/*$scope.documentName ? $scope.toggleFour(false) : '' ;
        if($scope.documentName != undefined && $scope.documentName =='No'){
          transferFormFields = previousCoverFormFieldsWithChkBox;
          $scope.files = [];
          PersistenceService.setUploadedFileDetails($scope.files);
        } else{*/
          transferFormFields = previousCoverFormFields;
        /*}*/
      }else if(formName == 'TranscoverCalculatorForm'){
  		transferFormFields = TranscoverCalculatorFormFields;
  	}
      var inx = transferFormFields.indexOf(elementName);
      if(inx > 0){
        for(var i = 0; i < inx ; i++){
          $scope[formName][transferFormFields[i]].$touched = true;
        }
      }
    };

    $scope.CoverDetailsTransferFormSubmit =  function (form){
      /*if(form.$name == 'previousCoverForm' && $("#transferitrId_div_id").is(":visible")) {
        if($scope.files && $scope.files.length) {
          $scope.fileNotUploadedError = false;
        } else {
          $scope.fileNotUploadedError = true;
          return false;
        }
      }*/

      if(!form.$valid){
        form.$submitted=true;
        if(form.$name == 'coverDetailsTransferForm')
        {
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
        else if(form.$name == 'occupationDetailsTransferForm')
        {
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
        else if(form.$name == 'previousCoverForm'){
          $scope.toggleFour(false);
        }
     } else {
      if(form.$name == 'coverDetailsTransferForm')
        {
          $scope.toggleTwo(true);
        }
      else if(form.$name == 'occupationDetailsTransferForm')
        {
        $scope.toggleThree(true);
        }
      else if(form.$name == 'previousCoverForm'){
       // if($scope.tpdClaimTrans == 'No' && $scope.terminalIllnessClaimTrans == 'No'){
          $scope.toggleFour(true);
      //    toggleFlag = true;
      //  }
      }else if(form.$name == 'TranscoverCalculatorForm'){
        if(!$scope.coverAmtErrFlag && !$scope.maxTotalTPDAmt && !$scope.maxDeathErrorFlag && !$scope.maxTpdErrorFlag && !$scope.maxTpdCoverErrorFlag){
          $scope.calculateTransfer();
        }
      }
       }
      //console.log("Form Validation");
    };

  /*  $scope.industryTransferOptions = [{
      options: 'option1',
      industryName: 'Industry 1'
    }, {
      options: 'option2',
      industryName: 'Industry 2'
    }
    ];*/

    $scope.occupationTransferOptions = [{
      options: 'option1',
      occupationName: 'Occupation 1'
      }, {
        options: 'option2',
        occupationName: 'Occupation 2'
      }
    ];


   /* $scope.files = [];
    $scope.selectedFile = null;
    $scope.uploadFiles = function(files, errFiles) {
      $scope.fileSizeErrFlag = false;
      $scope.fileFormatError = errFiles.length > 0 ? true : false;
      $scope.selectedFile =  files[0];
      
    };
    $scope.addFilesToStack = function () {
      var fileSize = ($scope.selectedFile.size / 1048576).toFixed(3);
      if(fileSize > 10) {
        $scope.fileSizeErrFlag=true;
        $scope.fileSizeErrorMsg ="File size should not be more than 10MB";
        $scope.selectedFile = null;
        return;
      }else{
        $scope.fileSizeErrFlag=false;
      }
      if(!$scope.files)
        $scope.files = [];
      $scope.files.push($scope.selectedFile);
      //PersistenceService.setUploadedFileDetails($scope.files);
      $scope.fileNotUploadedError = false;
      $scope.selectedFile = null;
    }

  $scope.removeFile = function(index) {
    $scope.files.splice(index, 1);
    PersistenceService.setUploadedFileDetails($scope.files);
    if($scope.files.length < 1) {
      $scope.fileNotUploadedError = true;
    }
  }

  
  $scope.submitFiles = function () {
    $scope.uploadedFiles = $scope.uploadedFiles || [];
    var defer = $q.defer();
    if(!$scope.files){
      $scope.files = [];
    }
    if(!$scope.files.length) {
      defer.resolve({});
    }
    var upload;
    var numOfFiles = $scope.files.length;
    angular.forEach($scope.files, function(file, index) {
      if(Upload.isFile(file)) {
        upload = Upload.http({
          url: $scope.urlList.fileUploadUrl,
          headers : {
            'Content-Type': file.name,
            'Authorization':tokenNumService.getTokenId()
          },
          data: file
        });
        upload.then(function(res){
        	numOfFiles--;
          $scope.uploadedFiles[index] = res.data;
          if(numOfFiles == 0){
            PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
            defer.resolve(res);
          }
        }, function(err){
          console.log("Error uploading the file " + err);
          defer.reject(err);
        });
      } else {
    	  numOfFiles--;
        if(numOfFiles == 0) {
          PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
          defer.resolve({});
        }
      }
    });
    return defer.promise;
  };
*/



  $scope.displayExistingWpBp = false;
  if(parseFloat($scope.ipCoverTransDetails.amount) > 0.0){
    $scope.displayExistingWpBp = true;
  } else{
    $scope.displayExistingWpBp = false;
  }


  $scope.transferDeathOccupationCategory = $scope.deathCoverTransDetails.occRating;
  $scope.transferTpdOccupationCategory = $scope.tpdCoverTransDetails.occRating;
  $scope.transferIpOccupationCategory = $scope.ipCoverTransDetails.occRating;

  $scope.waitingPeriodTransOptions = ["14 Days", "30 Days", "45 Days", "60 Days", "90 Days", "Not Listed"];
    $scope.benefitPeriodTransOptions = ["2 Years", "5 Years", "Age 60", "Age 65", "Not Listed"];

    $scope.waitingPeriodTransAdlnOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodTransAdlnOptions = ['2 Years', "5 Years",'Age 65'];

    $scope.preferredContactTransOptions = ['Mobile','Office','Home'];

    $scope.regex = /[0-9]{1,3}/;
    $scope.dcTransCoverAmount = 0.00;
  $scope.dcTransCost = 0.00;
  $scope.tpdTransCoverAmount = 0.00;
  $scope.tpdTransCost = 0.00;
  $scope.ipTransCoverAmount = 0.00;
  $scope.ipTransCost = 0.00;
  $scope.totalTransCost = 0.00;

  if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.waitingPeriod && $scope.ipCoverTransDetails.waitingPeriod != ''){
      $scope.waitingPeriodTransPer = $scope.ipCoverTransDetails.waitingPeriod;
      $scope.waitingPeriodTransAddnl = $scope.ipCoverTransDetails.waitingPeriod;
  } else {
    $scope.waitingPeriodTransPer = $scope.waitingPeriodTransPer ? $scope.waitingPeriodTransPer : '90 Days';
    $scope.waitingPeriodTransAddnl = $scope.waitingPeriodTransPer;
  }

  if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitPeriod && $scope.ipCoverTransDetails.benefitPeriod != ''){
      $scope.benefitPeriodTransPer = $scope.ipCoverTransDetails.benefitPeriod;
      $scope.benefitPeriodTransAddnl = $scope.ipCoverTransDetails.benefitPeriod;
  } else{
    $scope.benefitPeriodTransPer = $scope.benefitPeriodTransPer ? $scope.benefitPeriodTransPer : '2 Years';
    $scope.benefitPeriodTransAddnl = $scope.benefitPeriodTransPer;
  }

   $scope.changeWaitingPeriod = function() {
     if(($scope.waitingPeriodTransPer == '14 Days') || ($scope.waitingPeriodTransPer == '30 Days')){
        $scope.waitingPeriodTransAddnl = '30 Days';
     }else  if(($scope.waitingPeriodTransPer == '45 Days') || ($scope.waitingPeriodTransPer == '60 Days')){
        $scope.waitingPeriodTransAddnl = '60 Days';
     }else if($scope.waitingPeriodTransPer == '90 Days'){
          $scope.waitingPeriodTransAddnl = '90 Days';
     } else if($scope.waitingPeriodTransPer == 'Not Listed'){
       $scope.waitingPeriodTransAddnl = '90 Days';
     }
     $scope.autoCalculate();
      };

      $scope.clickToOpen = function (hhText) {

      var dialog = ngDialog.open({
        /*template: '<p>'+hhText+'</p>' +
          '<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
            template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
          className: 'ngdialog-theme-plain',
          plain: true
      });
      dialog.closePromise.then(function (data) {
        console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
      });
    };

    $scope.changeBenefitPeriod = function() {
        $timeout(function(){
      	  if(($scope.benefitPeriodTransPer == '2 Years')){
            $scope.benefitPeriodTransAddnl = '2 Years';
          } else if($scope.benefitPeriodTransPer == '5 Years'){
            $scope.benefitPeriodTransAddnl = '2 Years';
          } else if($scope.benefitPeriodTransPer == 'Not Listed'){
            $scope.benefitPeriodTransAddnl = '2 Years';
          } else if($scope.benefitPeriodTransPer == 'Age 65'){
            $scope.benefitPeriodTransAddnl = 'Age 65';
          } else if($scope.benefitPeriodTransPer == 'Age 60'){
            $scope.benefitPeriodTransAddnl = '2 Years';
          }
          $scope.autoCalculate();
        }, 10);
      };  

          $scope.transferAckFlag = false;
        var transferAckCheck;

        $scope.checkTransferAckState = function(){
            $timeout(function(){
              transferAckCheck = $('#transferTermsLabel').hasClass('active');
              if(transferAckCheck){
                $scope.transferAckFlag = false;
              } else{
                $scope.transferAckFlag = true;
              }
            }, 10);
          };
          var ackDocument;
          $scope.saveDataForPersistence = function(){
            var defer = $q.defer();
            var coverObj = {};
            var coverStateObj = {};
            var transferOccObj={};
            var transferDeathAddnlObj={};
            var transferTpdAddnlObj={};
            var transferIpAddnlObj={};
            var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
              return $scope.transferIndustry == obj.key;
            });


                  coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
                  coverObj['firstName'] = $scope.personalDetails.firstName;
                  coverObj['lastName'] = $scope.personalDetails.lastName;
                  coverObj['dob'] = $scope.personalDetails.dateOfBirth;
                  coverObj['country'] =persoanlDetailService.getMemberDetails().address.country;
                  coverObj['email'] = $scope.transferEmail;
                  transferOccObj['gender'] = $scope.gender;
                  coverObj['contactType']=$scope.preferredContactType;
                  coverObj['contactPhone'] = $scope.transferPhone;
                  coverObj['contactPrefTime'] = $scope.TransferTime;

                  transferOccObj['fifteenHr'] = $scope.fifteenHrsTransferQuestion;
                  transferOccObj['citizenQue'] = $scope.areyouperCitzTransferQuestion;
                  transferOccObj['industryName'] = selectedIndustry[0].value;
                  transferOccObj['industryCode'] = selectedIndustry[0].key;
                  transferOccObj['occupation'] = $scope.occupationTransfer;
                  if( !$scope.showWithinOfficeTransferQuestion)
                	  {
                	  $scope.withinOfficeTransferQuestion = null;
                	  
                	  }
                  if(!$scope.showTertiaryTransferQuestion)
                	  {
                	  $scope.tertiaryTransferQuestion = null;
                	  }
                  if(!$scope.showHazardousTransferQuestion)
                	  {
                	  $scope.hazardousTransferQuestion = null;
                	  }
                  if(!$scope.showOutsideOffice)
                	  {
                	  $scope.outsideOffice = null;
                	  }
                  transferOccObj['withinOfficeQue']= $scope.withinOfficeTransferQuestion;
                  transferOccObj['tertiaryQue']= $scope.tertiaryTransferQuestion;
                  
                  transferOccObj['managementRoleQue']= $scope.outsideOffice;
                  transferOccObj['hazardousQue']= $scope.hazardousTransferQuestion;
                  transferOccObj['salary'] = $scope.annualTransferSalary;
                  transferOccObj['otherOccupation'] = $scope.otherOccupationObj.transferotherOccupation;

                  coverObj['previousFundName'] = $scope.previousFundName;
                  coverObj['membershipNumber'] = $scope.membershipNumber;
                  coverObj['spinNumber'] = $scope.spinNumber;
                 /* coverObj['documentName'] = $scope.documentName;*/
                //  coverObj['uploadedDocument'] = $scope.selectedFile.name;
                  /*ackDocument = $('#acknowledgeDocAdressCheck').hasClass('active');
                  if(ackDocument){
                	coverObj['documentAck2'] = "Yes";
                    coverObj['documentAddress'] = "Postal address:\n\nHostplus,\nLocked Bag 5046,\nParramatta,\nNSW 2124";
                    
                  }*/
                //  coverObj['documentAddressCheckbox'] =acknowledgeDocAdressCheckState;
                  /*coverObj['previousTpdClaimQue'] = $scope.tpdClaimTrans;
                  coverObj['terminalIllClaimQue'] = $scope.terminalIllnessClaimTrans;*/

                  coverObj['transferDeathExistingAmt'] = $scope.deathCoverTransDetails.amount;
                  coverObj['transferTpdExistingAmt'] = $scope.tpdCoverTransDetails.amount;
                  
                  if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
              	{
                  coverObj['existingDeathUnits'] = $scope.deathCoverTransDetails.units;
                  coverObj['existingTPDUnits'] = $scope.tpdCoverTransDetails.units;
              	}
                  coverObj['deathOccCategory'] = $scope.transferDeathOccupationCategory;
                  coverObj['tpdOccCategory'] = $scope.transferTpdOccupationCategory;
                  coverObj['ipOccCategory'] = $scope.transferIpOccupationCategory;
                  coverObj['transferIpExistingAmt'] = $scope.ipCoverTransDetails.amount;
                  
                  /*coverObj['transferIpWaitingPeriod'] = $scope.waitingPeriodTransPer;
                  coverObj['transferIpBenefitPeriod'] = $scope.benefitPeriodTransPer;*/

                  transferDeathAddnlObj['deathTransferAmt'] = $scope.TransDeathRequireCover;
                  transferDeathAddnlObj['deathTransferCovrAmt'] = parseFloat($scope.dcTransCoverAmount);
                  transferDeathAddnlObj['deathTransferWeeklyCost'] = parseFloat($scope.dcTransCost);
                  if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
            	  {
                	  transferDeathAddnlObj['deathTransferCoverType'] = 'DcUnitised';
                	  transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDUnitised';
                	  transferDeathAddnlObj['deathTransferUnits'] = $scope.deathUnitsForTransfer;
                	  transferTpdAddnlObj['tpdTransferUnits'] = $scope.tpdUnitsForTransfer;
            	  }
                  else
            	  {
            		  transferDeathAddnlObj['deathTransferCoverType'] = 'DcFixed';
            		  transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDFixed';
            	  }

                  transferTpdAddnlObj['tpdTransferAmt'] = $scope.TransTPDRequireCover;
                  transferTpdAddnlObj['tpdTransferCovrAmt'] = parseFloat($scope.tpdTransCoverAmount);
                  transferTpdAddnlObj['tpdTransferWeeklyCost'] = parseFloat($scope.tpdTransCost);
                  //transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDFixed';


                  transferIpAddnlObj['ipTransferAmt'] = $scope.TransIPRequireCover;
                  transferIpAddnlObj['ipTransferCovrAmt'] =parseFloat($scope.ipTransCoverAmount);
                  transferIpAddnlObj['ipTransferWeeklyCost'] = parseFloat($scope.ipTransCost);
                  
                  transferIpAddnlObj['addnlTransferWaitingPeriod'] = $scope.waitingPeriodTransPer;
                  transferIpAddnlObj['addnlTransferBenefitPeriod'] = $scope.benefitPeriodTransPer;
                  transferIpAddnlObj['totalipwaitingperiod'] = $scope.waitingPeriodTransAddnl;
                  transferIpAddnlObj['totalipbenefitperiod'] = $scope.benefitPeriodTransAddnl;
                  
                  transferIpAddnlObj['ipTransferCoverType'] = 'IpFixed';

                  coverObj['totalPremium']=parseFloat($scope.totalTransCost);
                  coverObj['autoCalculateFlag']=autoCalculate;
                  coverObj['appNum'] = parseInt(appNum);
                  coverObj['transferAckCheck'] = transferAckCheck;
                  coverObj['lastSavedOn'] = 'Transferpage';
                  coverObj['manageType'] = 'TCOVER';
	              coverObj['partnerCode'] = 'HOST';
	              coverObj['freqCostType'] = $scope.premTransFreq;
	              coverObj['age'] = anb;
	              coverObj['dodCheck'] = $('#dodCkBoxLblId').hasClass('active');
	              coverObj['privacyCheck'] = $('#privacyCkBoxLblId').hasClass('active');
	              //coverObj['unitisedCovers'] = unitCheck;

	              coverStateObj['showWithinOfficeTransferQuestion'] = $scope.showWithinOfficeTransferQuestion;
	              coverStateObj['showTertiaryTransferQuestion'] =  $scope.showTertiaryTransferQuestion;
	              coverStateObj['showHazardousTransferQuestion'] = $scope.showHazardousTransferQuestion;
	              coverStateObj['showOutsideOffice'] = $scope.showOutsideOffice;
	              /*uploaded transfer documents*/
	              /*$scope.submitFiles().then(function(res) {*/
                  PersistenceService.settransferCoverDetails(coverObj);
                  PersistenceService.settransferCoverStateDetails(coverStateObj);
                  PersistenceService.setTransferCoverOccDetails(transferOccObj);
                  PersistenceService.setTransferDeathAddnlDetails(transferDeathAddnlObj);
                  PersistenceService.setTransferTpdAddnlDetails(transferTpdAddnlObj);
                  PersistenceService.setTransferIpAddnlDetails(transferIpAddnlObj);
                 defer.resolve();
                /*}, function(err) {
                  defer.reject(err);
                });*/
                return defer.promise;
          };

          if($routeParams.mode == 2){
            var existingDetails = PersistenceService.gettransferCoverDetails();
            var stateDetails = PersistenceService.gettransferCoverStateDetails();
            var occDetails =PersistenceService.getTransferCoverOccDetails();
            var deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
            var tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
            var ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();

            if(!existingDetails || !occDetails || !deathAddnlCvrDetails || !tpdAddnlCvrDetails || !ipAddnlCvrDetails) {
              $location.path("/quotetransfer/1");
              return false;
            }

            $scope.transferEmail = existingDetails.email;
            $scope.preferredContactType = existingDetails.contactType;
            $scope.transferPhone = existingDetails.contactPhone;
            $scope.TransferTime = existingDetails.contactPrefTime;
            $scope.premTransFreq = existingDetails.freqCostType;

            $scope.fifteenHrsTransferQuestion = occDetails.fifteenHr;
            $scope.areyouperCitzTransferQuestion = occDetails.citizenQue;
            $scope.transferIndustry = occDetails.industryCode;
            //$scope.occupationTransfer = occDetails.occupation;
            $scope.withinOfficeTransferQuestion = occDetails.withinOfficeQue;
            $scope.tertiaryTransferQuestion = occDetails.tertiaryQue;
            $scope.hazardousTransferQuestion = occDetails.hazardousQue;
            $scope.outsideOffice = occDetails.managementRoleQue;
            $scope.annualTransferSalary = occDetails.salary;
            $scope.gender = occDetails.gender;
            $scope.otherOccupationObj.transferotherOccupation = occDetails.otherOccupation;

            $scope.previousFundName=existingDetails.previousFundName;
            $scope.membershipNumber=existingDetails.membershipNumber;
            $scope.spinNumber=existingDetails.spinNumber;
            /*$scope.documentName=existingDetails.documentName;*/
          //  $scope.selectedFile.name=existingDetails.uploadedDocument;
           /* ackDocument=existingDetails.documentAddress;*/
          //  $scope.acknowledgeDocAdressCheckState=existingDetails.documentAddressCheckbox;
            /*$scope.tpdClaimTrans=existingDetails.previousTpdClaimQue;
            $scope.terminalIllnessClaimTrans=existingDetails.terminalIllClaimQue;*/
            
           /* if(existingDetails.documentName == "No"){
            	$scope.ackDocument2 = true;
            }*/


            $scope.deathCoverTransDetails.amount=existingDetails.transferDeathExistingAmt;
            $scope.transferDeathOccupationCategory=existingDetails.deathOccCategory;
            $scope.transferTpdOccupationCategory=existingDetails.tpdOccCategory;
            $scope.transferIpOccupationCategory=existingDetails.ipOccCategory;
            $scope.tpdCoverTransDetails.amount=existingDetails.transferTpdExistingAmt;
            $scope.ipCoverTransDetails.amount=existingDetails.transferIpExistingAmt;
            /*$scope.waitingPeriodTransPer=existingDetails.transferIpWaitingPeriod;
            $scope.benefitPeriodTransPer=existingDetails.transferIpBenefitPeriod;*/

            $scope.TransDeathRequireCover=deathAddnlCvrDetails.deathTransferAmt;
            $scope.dcTransCoverAmount=deathAddnlCvrDetails.deathTransferCovrAmt;
            $scope.dcTransCost=deathAddnlCvrDetails.deathTransferWeeklyCost;

            $scope.TransTPDRequireCover=tpdAddnlCvrDetails.tpdTransferAmt;
            $scope.tpdTransCoverAmount=tpdAddnlCvrDetails.tpdTransferCovrAmt;
            $scope.tpdTransCost=tpdAddnlCvrDetails.tpdTransferWeeklyCost;


            $scope.TransIPRequireCover=ipAddnlCvrDetails.ipTransferAmt;
            $scope.ipTransCoverAmount=ipAddnlCvrDetails.ipTransferCovrAmt;
            $scope.ipTransCost=ipAddnlCvrDetails.ipTransferWeeklyCost;
            
            $scope.waitingPeriodTransPer=ipAddnlCvrDetails.addnlTransferWaitingPeriod;
            $scope.benefitPeriodTransPer=ipAddnlCvrDetails.addnlTransferBenefitPeriod;
            $scope.waitingPeriodTransAddnl=ipAddnlCvrDetails.totalipwaitingperiod;
            $scope.benefitPeriodTransAddnl=ipAddnlCvrDetails.totalipbenefitperiod;

            $scope.showWithinOfficeTransferQuestion = stateDetails.showWithinOfficeTransferQuestion;
            $scope.showTertiaryTransferQuestion =  stateDetails.showTertiaryTransferQuestion;
            $scope.showHazardousTransferQuestion = stateDetails.showHazardousTransferQuestion;
            $scope.showOutsideOffice = stateDetails.showOutsideOffice;

            $scope.totalTransCost=existingDetails.totalPremium;
            autoCalculate=existingDetails.autoCalculateFlag;
            appNum = existingDetails.appNum;
            transferAckCheck = existingDetails.transferAckCheck;
            ackCheck = existingDetails.ackCheck;
            dodCheck = existingDetails.dodCheck;
            privacyCheck = existingDetails.privacyCheck;
            //unitCheck = existingDetails.unitisedCovers;
           /* $scope.files = PersistenceService.getUploadedFileDetails();
            $scope.uploadedFiles = $scope.files;*/

            OccupationService.getOccupationList($scope.urlList.occupationUrl,"HOST",$scope.transferIndustry).then(function(res){
              //OccupationService.getOccupationList({fundId:"HOST", induCode:$scope.transferIndustry}, function(occupationList){
              $scope.OccupationList = res.data;
              var temp = $scope.OccupationList.filter(function(obj){
                return obj.occupationName == occDetails.occupation;
              });
              $scope.occupationTransfer = temp[0].occupationName;
              $scope.getTransCategoryFromDB(false);
            }, function(err){
              console.log("Error while getting occupatio list " + JSON.stringify(err));
            });

            if(transferAckCheck){
                $timeout(function(){
                $('#transferTermsLabel').addClass('active');
                   });
                 }
            /*if(acknowledgeDocAdressCheckState){
              $('#acknowledgeDocAdressCheck').parent().addClass('active');
            }*/

            if(ackDocument){
              $timeout(function(){
              $('#acknowledgeDocAdressCheck').addClass('active');
              });
              }

            if(dodCheck){
                $timeout(function(){
                $('#dodCkBoxLblId').addClass('active');
             });
           }
            if(privacyCheck){
                $timeout(function(){
                $('#privacyCkBoxLblId').addClass('active');
             });
           }
            /*if(unitCheck){
              $('#equivalentUnit').addClass('active');
            }*/
            $scope.togglePrivacy(true);
            $scope.toggleContact(true);
            $scope.toggleTwo(true);
            $scope.toggleThree(true);
            $scope.toggleFour(true);

          };

          if($routeParams.mode == 3){
             mode3Flag = true;
             var num = PersistenceService.getAppNumToBeRetrieved();
             RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
              var appDetails = res.data[0];

              $scope.transferEmail = appDetails.email;
              $scope.preferredContactType = appDetails.contactType;
              $scope.transferPhone = appDetails.contactPhone;
              $scope.TransferTime = appDetails.contactPrefTime;
              $scope.premTransFreq = appDetails.freqCostType;
              /*$scope.files = appDetails.transferDocuments;
              $scope.uploadedFiles = $scope.files;*/

              $scope.fifteenHrsTransferQuestion = appDetails.occupationDetails.fifteenHr;
              $scope.areyouperCitzTransferQuestion = appDetails.occupationDetails.citizenQue;
              $scope.transferIndustry = appDetails.occupationDetails.industryCode;
              //$scope.occupationTransfer = occupationDetails.occupation;
              $scope.withinOfficeTransferQuestion = appDetails.occupationDetails.withinOfficeQue;
              $scope.tertiaryTransferQuestion = appDetails.occupationDetails.tertiaryQue;
              $scope.hazardousTransferQuestion = appDetails.occupationDetails.hazardousQue;
              $scope.outsideOffice = appDetails.occupationDetails.managementRoleQue;
              $scope.annualTransferSalary = appDetails.occupationDetails.salary;
              $scope.gender = appDetails.occupationDetails.gender;
              $scope.otherOccupationObj.transferotherOccupation = appDetails.occupationDetails.otherOccupation;

              $scope.previousFundName=appDetails.previousFundName;
              $scope.membershipNumber=appDetails.membershipNumber;
              $scope.spinNumber=appDetails.spinNumber;
             /* $scope.documentName=appDetails.documentName;
              if(appDetails.documentName == "No"){
                $scope.ackDocument2 = true;
              }*/
            //  $scope.selectedFile.name=appDetails.uploadedDocument;
              /*ackDocument=appDetails.documentAddress;*/
            //  $scope.acknowledgeDocAdressCheckState=appDetails.documentAddressCheckbox;
              /*$scope.tpdClaimTrans=appDetails.previousTpdClaimQue;
              $scope.terminalIllnessClaimTrans=appDetails.terminalIllClaimQue;*/


              $scope.deathCoverTransDetails.amount=appDetails.existingDeathAmt;
              $scope.transferDeathOccupationCategory=appDetails.deathOccCategory;
              $scope.transferTpdOccupationCategory=appDetails.tpdOccCategory;
              $scope.transferIpOccupationCategory=appDetails.ipOccCategory;
              $scope.tpdCoverTransDetails.amount=appDetails.existingTpdAmt;
              $scope.ipCoverTransDetails.amount=appDetails.existingIPAmount;
              /*$scope.waitingPeriodTransPer=appDetails.transferIpWaitingPeriod;
              $scope.benefitPeriodTransPer=appDetails.transferIpBenefitPeriod;*/

              $scope.TransDeathRequireCover=appDetails.addnlDeathCoverDetails.deathTransferAmt;
              $scope.dcTransCoverAmount=appDetails.addnlDeathCoverDetails.deathTransferCovrAmt;
              $scope.dcTransCost=appDetails.addnlDeathCoverDetails.deathTransferWeeklyCost;

              $scope.TransTPDRequireCover=appDetails.addnlTpdCoverDetails.tpdTransferAmt;
              $scope.tpdTransCoverAmount=appDetails.addnlTpdCoverDetails.tpdTransferCovrAmt;
              $scope.tpdTransCost=appDetails.addnlTpdCoverDetails.tpdTransferWeeklyCost;


              $scope.TransIPRequireCover=appDetails.addnlIpCoverDetails.ipTransferAmt;
              $scope.ipTransCoverAmount=appDetails.addnlIpCoverDetails.ipTransferCovrAmt;
              $scope.ipTransCost=appDetails.addnlIpCoverDetails.ipTransferWeeklyCost;
              $scope.waitingPeriodTransPer=appDetails.addnlIpCoverDetails.addnlTransferWaitingPeriod;
              $scope.benefitPeriodTransPer=appDetails.addnlIpCoverDetails.addnlTransferBenefitPeriod;
              $scope.waitingPeriodTransAddnl=appDetails.addnlIpCoverDetails.totalipwaitingperiod;
              $scope.benefitPeriodTransAddnl=appDetails.addnlIpCoverDetails.totalipbenefitperiod;
              
              $scope.deathUnitsForTransfer = appDetails.addnlDeathCoverDetails.deathTransferUnits||0;
              $scope.tpdUnitsForTransfer = appDetails.addnlTpdCoverDetails.tpdTransferUnits||0;
              
              $scope.totalTransCost=appDetails.totalPremium;
              autoCalculate=true;
              appNum = appDetails.appNum;
              transferAckCheck = appDetails.transferAckCheck;
              ackCheck = appDetails.ackCheck;
              dodCheck = appDetails.dodCheck;
              privacyCheck = appDetails.privacyCheck;
             /* $scope.waitingPeriodTransPer = '30 Days';
              $scope.benefitPeriodTransPer = '2 Years';*/

              

              OccupationService.getOccupationList($scope.urlList.occupationUrl,"HOST",$scope.transferIndustry).then(function(res){
              //OccupationService.getOccupationList({fundId:"HOST", induCode:$scope.transferIndustry}, function(occupationList){
                  $scope.OccupationList = res.data;
                  var temp = $scope.OccupationList.filter(function(obj){
                    return obj.occupationName == appDetails.occupationDetails.occupation;
                  });
                  $scope.occupationTransfer = temp[0].occupationName;
                  $scope.renderOccupationQuestions();
                  $scope.getTransCategoryFromDB(false);
                }, function(err){
                  console.log("Error while getting occupatio list " + JSON.stringify(err));
                });

              if(parseFloat($scope.ipCoverTransDetails.amount) > 0.0){
                $scope.displayExistingWpBp = true;
              } else{
                $scope.displayExistingWpBp = false;
              }

              if(transferAckCheck){
                  $timeout(function(){
                  $('#transferTermsLabel').addClass('active');
                     });
                   }
              /*if(acknowledgeDocAdressCheckState){
                $('#acknowledgeDocAdressCheck').parent().addClass('active');
              }*/

              if($scope.ackDocument2){
                $timeout(function(){
                $('#acknowledgeDocAdressCheck').addClass('active');
                });
                }


                

              $('#dodCkBoxLblId').addClass('active');
              $('#privacyCkBoxLblId').addClass('active');

              $scope.togglePrivacy(true);
              $scope.toggleContact(true);
              $scope.toggleTwo(true);
              $scope.toggleThree(true);
              $scope.toggleFour(true);
              
              /*if(!$scope.isDeathDisabled)
            	  {
            	  $scope.validateDeathMaxAmount(true);
            	  }
              if(!$scope.isTPDDisabled)
            	  {
            	  $scope.validateTpdMaxAmount(true);
            	  }
              if(!$scope.isIPDisabled)
              	{
            	  $scope.validateIpMaxAmount(true);
              	}*/
              $scope.autoCalculate();

             },function(err){
                console.info("Error fetching the saved app details " + err);
             });
          }

          $scope.goToAura = function(){
            if(this.coverDetailsTransferForm.$valid && this.occupationDetailsTransferForm.$valid && this.previousCoverForm.$valid && this.TranscoverCalculatorForm.$valid){
              if((!$scope.coverAmtErrFlag) && !$scope.maxTotalTPDAmt && (!$scope.maxDeathErrorFlag) && !$scope.maxTpdErrorFlag && !$scope.maxTpdCoverErrorFlag){
              $timeout(function(){
                $scope.saveDataForPersistence().then(function() {
                    $scope.go('/auratransfer/1');
                }, function(err) {
                    //console.log(err);
                });
                // submit uploaded to server
                //$scope.submitFiles();
            }, 10);

	            }else{
	            	return false;
	            }
            }
          };

          $scope.invalidSalAmount = false;
          $scope.checkValidSalary = function(){
            /*if(parseInt($scope.annualTransferSalary) == 0){
              $scope.invalidSalAmount = true;
            } else{
              $scope.invalidSalAmount = false;
            }*/
            $scope.validateIpMaxAmount(true);
           $scope.renderOccupationQuestions();
          };
          $scope.saveQuoteTransfer = function() {
            $scope.saveDataForPersistence().then(function() {
              var quoteTransferObject =  PersistenceService.gettransferCoverDetails();
              var transferOccDetails =PersistenceService.getTransferCoverOccDetails();
              var deathAddnlTransferCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
              var tpdAddnlTransferCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
              var ipAddnlTransferCvrDetails=PersistenceService.getTransferIpAddnlDetails();
              var personalDetails = persoanlDetailService.getMemberDetails();
              var transferUploadedFiles = PersistenceService.getUploadedFileDetails();
              if(quoteTransferObject != null && transferOccDetails != null && deathAddnlTransferCvrDetails != null && tpdAddnlTransferCvrDetails != null &&
                  ipAddnlTransferCvrDetails != null && personalDetails != null) {
                var details = {};
                details.addnlDeathCoverDetails = deathAddnlTransferCvrDetails;
                details.addnlTpdCoverDetails = tpdAddnlTransferCvrDetails;
                details.addnlIpCoverDetails = ipAddnlTransferCvrDetails;
                details.occupationDetails = transferOccDetails;
                //added for uploaded file details
                if(transferUploadedFiles != null) {
                   details.transferDocuments = transferUploadedFiles;
                }
                var temp = angular.extend(quoteTransferObject,details);
                var saveQuoteTransferObject = angular.extend(temp, personalDetails);
                auraResponseService.setResponse(saveQuoteTransferObject);
                saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
                  console.log(response.data);
                  $scope.transferQuoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
                },function(err) {
                  console.log("Something went wrong while saving..."+JSON.stringify(err));
                });
              }
            }, function(err) {
                //console.log(err);
            });
          };

          $scope.transferQuoteSaveAndExitPopUp = function (hhText) {

          var dialog1 = ngDialog.open({
                template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
              className: 'ngdialog-theme-plain custom-width',
              preCloseCallback: function(value) {
                     var url = "/landing"
                     $location.path( url );
                     return true
              },
              plain: true
          });
          dialog1.closePromise.then(function (data) {
            console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
          });
        };

        $scope.generatePDF = function(){
          $scope.saveDataForPersistence().then(function() {
            var quoteTransferObjectPrint =  PersistenceService.gettransferCoverDetails();
            var transferOccDetailsPrint =PersistenceService.getTransferCoverOccDetails();
            var deathAddnlTransferCvrDetailsPrint =PersistenceService.getTransferDeathAddnlDetails();
            var tpdAddnlTransferCvrDetailsPrint=PersistenceService.getTransferTpdAddnlDetails();
            var ipAddnlTransferCvrDetailsPrint=PersistenceService.getTransferIpAddnlDetails();
              var personalDetailsPrint = persoanlDetailService.getMemberDetails();
              if(quoteTransferObjectPrint != null && transferOccDetailsPrint != null && deathAddnlTransferCvrDetailsPrint != null && tpdAddnlTransferCvrDetailsPrint != null &&
                  ipAddnlTransferCvrDetailsPrint != null && personalDetailsPrint != null){
                var details = {};
                details.addnlDeathCoverDetails = deathAddnlTransferCvrDetailsPrint;
                details.addnlTpdCoverDetails = tpdAddnlTransferCvrDetailsPrint;
                details.addnlIpCoverDetails = ipAddnlTransferCvrDetailsPrint;
                details.occupationDetails = transferOccDetailsPrint;
                var temp = angular.extend(quoteTransferObjectPrint,details)
                  var printQuoteTransferObject = angular.extend(temp,personalDetailsPrint);
                auraResponseService.setResponse(printQuoteTransferObject);
                  printQuotePage.reqObj($scope.urlList.printQuotePage).then(function(response){
                    PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                  $scope.downloadPDF();
                  },function(err){
                    console.log("Something went wrong while saving..."+JSON.stringify(err));
                  });
              }
          }, function(err) {
              //console.log(err);
          });
        };
          $scope.downloadPDF = function(){
              var pdfLocation =null;
              var filename = null;
              var a = null;
            pdfLocation = PersistenceService.getPDFLocation();
            console.log(pdfLocation+"pdfLocation");
            filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
            a = document.createElement("a");
              document.body.appendChild(a);
              DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
              //DownloadPDFService.download({file_name: pdfLocation}, function(res){
              if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
                      window.navigator.msSaveBlob(res.data.response,filename);
                  }else{
                    var fileURL = URL.createObjectURL(res.data.response);
                    a.href = fileURL;
                    a.download = filename;
                    a.click();
                  }
            }, function(err){
              console.log("Error downloading the PDF " + err);
            });
          };

          $scope.calculateForUnits = function() {
       	   
      	    var deathtotal = parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount);
      	    var tpdtotal = parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount);
      	  $scope.roundingIndDeath = false;
      	$scope.roundingIndTPD = false;
      	    var ruleModel = {
      	            "age": anb,
      	            "fundCode": "HOST",
      	            "gender": $scope.gender,
      	            "deathOccCategory": $scope.transferDeathOccupationCategory,
      	            "tpdOccCategory": $scope.transferTpdOccupationCategory,
      	            "ipOccCategory": $scope.transferIpOccupationCategory,
      	            "manageType": "TCOVER",
      	            "deathCoverType": "DcUnitised",
      	            "tpdCoverType": "TPDUnitised",
      	            "ipCoverType": "IpFixed",
      	            "deathUnits": 1,
      	            "tpdUnits": 1,
      	            "premiumFrequency": $scope.premTransFreq,
      	            "ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
      	            "ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
      	            "deathFixedAmount": parseInt(deathtotal),
      	            "tpdFixedAmount": parseInt(tpdtotal),
      	            "ipTransferAmount": parseInt($scope.TransIPRequireCover),
      	            "ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
      	          };
      	   
      	    
      	    TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
      	    	var coverAmtPerUnit = 0;
      	    	var premium = res.data;
      	      
      	      for(var i = 0; i < premium.length; i++){
      	        if(premium[i].coverType == 'DcUnitised'){
      	        	coverAmtPerUnit = premium[i].coverAmount || 0;
      	          if(coverAmtPerUnit > 0)
      	        	  {
      	        	$scope.deathUnitsForTransfer = Math.floor(deathtotal/coverAmtPerUnit);
      	        	if($scope.deathUnitsForTransfer>0 && !(deathtotal%coverAmtPerUnit == 0))
      	        		{
      	        		$scope.deathUnitsForTransfer = $scope.deathUnitsForTransfer + 1;
      	        		$scope.roundingIndDeath = true;
      	        		}
      	        	  }
      	          else
      	        	  {
      	        	  $scope.deathUnitsForTransfer = 0;
      	        	  }
      	        	
      	        } else if(premium[i].coverType == 'TPDUnitised'){
      	        	coverAmtPerUnit = premium[i].coverAmount || 0;
      	        	if(coverAmtPerUnit > 0)
      	        		{
      	        		$scope.tpdUnitsForTransfer = Math.floor(tpdtotal/coverAmtPerUnit);
      	        		if($scope.tpdUnitsForTransfer > 0 && !(tpdtotal % coverAmtPerUnit == 0))
      	        			{
      	        			$scope.tpdUnitsForTransfer = $scope.tpdUnitsForTransfer + 1;
      	        			$scope.roundingIndTPD = true;
      	        			}
      	        		}
      	        	else
      	        		{
      	        		$scope.tpdUnitsForTransfer = 0;
      	        		}
      	        }
      	        
      	        $scope.calculateUnits(); 
      	        
      	      }
      	      //$scope.QPD.totalPremium = parseFloat($scope.QPD.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.QPD.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.QPD.addnlIpCoverDetails.ipCoverPremium);
      	    }, function(err){
      	      console.info("Something went wrong while calculating..." + JSON.stringify(err));
      	    });
      	  }; 

      	  
$scope.calculateUnits = function() {
         	   
        	    var deathtotal = parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount);
        	    var tpdtotal = parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount);
        	  var ruleModel = {
    	            "age": anb,
    	            "fundCode": "HOST",
    	            "gender": $scope.gender,
    	            "deathOccCategory": $scope.transferDeathOccupationCategory,
    	            "tpdOccCategory": $scope.transferTpdOccupationCategory,
    	            "ipOccCategory": $scope.transferIpOccupationCategory,
    	            "manageType": "TCOVER",
    	            "deathUnits": $scope.deathUnitsForTransfer,
    	            "deathFixedAmount": parseInt(deathtotal),
    	            "tpdUnits": $scope.tpdUnitsForTransfer,
    	            "tpdFixedAmount": parseInt(tpdtotal),
    	            "deathCoverType": "DcUnitised",
    	            "tpdCoverType": "TPDUnitised",
    	            "ipCoverType": "IpFixed",
    	            "premiumFrequency": $scope.premTransFreq,
    	            "ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
    	            "ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
    	            "ipTransferAmount": parseInt($scope.TransIPRequireCover),
    	            "ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
    	          };

TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
    	      //TransferCalculateService.calculate({}, ruleModel, function(res){
    	        var premium = res.data;
    	        autoCalculate = true;
    	        for(var i = 0; i < premium.length; i++){
    	          if(premium[i].coverType == 'DcUnitised'){
    	            $scope.dcTransCoverAmount = premium[i].coverAmount;
    	            $scope.dcTransCost = premium[i].cost || 0;
    	          } else if(premium[i].coverType == 'TPDUnitised'){
    	            $scope.tpdTransCoverAmount = premium[i].coverAmount;
    	            $scope.tpdTransCost = premium[i].cost || 0;
    	          } else if(premium[i].coverType == 'IpFixed'){
    	            $scope.ipTransCoverAmount = premium[i].coverAmount||0.00;
    	            $scope.ipTransCost = premium[i].cost || 0;
    	          }
    	      }

    	        $scope.totalTransCost = parseFloat($scope.dcTransCost) + parseFloat($scope.tpdTransCost) + parseFloat($scope.ipTransCost);
    	        if(parseInt($scope.ipTransCoverAmount) < parseInt($scope.ipCoverTransDetails.amount))
            	{
            	$scope.lowerThanExitingFlag = true;
            	}
    	        if(fetchAppnum){
    	          fetchAppnum = false;
    	          appNum = PersistenceService.getAppNumber();
    	        }
    	        $scope.validateAll();
    	      }, function(err){
    	        console.log("Error while calculating transfer premium " + JSON.stringify(err));
    	      });
        	  };

$scope.validateAll = function()
       	  {
       	 	 if(!$scope.isDeathDisabled)
       	 	  {
       	 	  $scope.validateDeathMaxAmount();
       	 	  }
       	 	 if(!$scope.isTPDDisabled)
        	  {
        	   $scope.validateTpdMaxAmount();
        	  }
       	  }   
          
    }]);

 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */
