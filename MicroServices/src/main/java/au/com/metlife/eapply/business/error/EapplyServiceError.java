/**
 * 
 */
package au.com.metlife.eapply.business.error;

import org.springframework.http.HttpStatus;

/**
 * @author 599063
 *
 */
public class EapplyServiceError {

	
	private String code;
	private HttpStatus httpCode;
	private String description;
	private String element;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public HttpStatus getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(HttpStatus httpCode) {
		this.httpCode = httpCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}
	
	
	
}
