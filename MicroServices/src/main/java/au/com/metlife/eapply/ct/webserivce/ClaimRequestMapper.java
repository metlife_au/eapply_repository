package au.com.metlife.eapply.ct.webserivce;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.ct.dto.DocumentDto;
import au.com.metlife.eapply.ct.model.UserInfoBO;
import au.com.metlife.webservices.PicsLodgementStub;



public class ClaimRequestMapper {
	private ClaimRequestMapper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(ClaimRequestMapper.class);

	/*private static final String PACKAGE_NAME = ClaimRequestMapper.class
			.getPackage().getName();

	private static final String CLASS_NAME = ClaimRequestMapper.class
			.getName();*/
	
	
	public static PicsLodgementStub.NewUnderwritingMetaData doMapForLodgeNewUnderwriting(
			UserInfoBO userInfo,String email,String phoneNumber,String fullName) {
		log.info("Do map for lodge new underwriting start");
		
		PicsLodgementStub.NewUnderwritingMetaData newUW = new PicsLodgementStub.NewUnderwritingMetaData();
		
		newUW.setLodge_fund_id(userInfo.getFundType());
		newUW.setLodge_mem_fname(userInfo.getFirstname());
		newUW.setLodge_mem_lname(userInfo.getLastname());
		
		if(email!=null){
			newUW.setLodge_submitted_email(email);
		}
		if(phoneNumber!=null){
			newUW.setLodge_submitted_phone(phoneNumber);
		}
		if(fullName!=null){
			newUW.setLodge_submitted_by(fullName);
		}
		/*case ref number*/
		if(null!=userInfo.getId())
			newUW.setLodge_e_ref_no(userInfo.getId());
		if (null != userInfo.getTitle())
			newUW.setLodge_mem_title(userInfo.getTitle());
		newUW.setLodge_mem_gender(userInfo.getGender());
		
		if(null!=userInfo.getDateOfBirth())			
			newUW.setLodge_mem_dob(ClaimRequestMapper.getDate(userInfo.getDateOfBirth()));       
		
		if (null != userInfo.getClientRefNumber())
			newUW.setLodge_cl_ref_no(userInfo.getClientRefNumber());
		if (null != userInfo.getDateJoinedFund())
			newUW.setLodge_date_joined(ClaimRequestMapper.getDate(userInfo.getDateJoinedFund()));
		newUW.setLodge_add_info(userInfo.getAdditionalInfo());
		
/*		if(null!=contactDTO){
			newUW.setLodge_add_line1(contactDTO.getAddress1());
			newUW.setLodge_add_line2(contactDTO.getAddress2());
			newUW.setLodge_suburb(contactDTO.getSuburb());
			newUW.setLodge_state(contactDTO.getState());
			newUW.setLodge_postcode(contactDTO.getPostCode());
			newUW.setLodge_contact_pref(contactDTO.getContactPreference());
			newUW.setLodge_email(contactDTO.getEmailId());
			newUW.setLodge_mobile_no(contactDTO.getMobileNumber());
			newUW.setLodge_phone_business(contactDTO.getPhNumBusHr());
			newUW.setLodge_phone_after_hours(contactDTO.getPhNumAftBusHr());
			if("Yes".equalsIgnoreCase(contactDTO.getRolloverpending())){
				newUW.setLodge_rollover("yes");
			}else if("No".equalsIgnoreCase(contactDTO.getRolloverpending())){
				newUW.setLodge_rollover("no");
			}		
			newUW.setLodge_insurance_scale(contactDTO.getInsuranceScale());	
			
			newUW.setLodge_application_category(contactDTO.getApplicationCategory());
			log.info("contactDTO.getLifeEventValue()>> {}",contactDTO.getLifeEventValue());
			newUW.setLodge_life_event(contactDTO.getLifeEventValue());
		}
*/
		
		ArrayList docList ;

		if (null != userInfo.getElodgeDocument()) {
			docList = userInfo.getElodgeDocument();
			PicsLodgementStub.Documents[] picsDocArr = new PicsLodgementStub.Documents[docList
					.size()];

			DocumentDto doc;
			for (int i = 0; i < picsDocArr.length; i++) {

				picsDocArr[i] = new PicsLodgementStub.Documents();
				doc = (DocumentDto) docList.get(i);
				picsDocArr[i].setDocument_path(doc.getDocumentlocation());
            /*	picsDocArr[i].setDocument_path("/shared/eLodg/Oct2010/19102010/SystemOut_2.log");
				picsDocArr[i].setDocument_path("/usr/WebSphere/AppServer/profiles/au04qap003Node01/installedApps/au04qap003Cell01/PICSWS_war.ear/PICSWS.war/WEB-INF/services/PicsLodgementService.aar");*/				
				picsDocArr[i].setDocument_format("");
				picsDocArr[i].setDocument_type(doc.getDocumentCode());

			}
			newUW.setDocuments(picsDocArr);
		}
 	log.info("Do map for lodge new underwriting finish");
		return newUW;

	}

	public static PicsLodgementStub.NewClaimsMetaData doMapForLodgeNewClaims(
			UserInfoBO userInfo,String email,String phoneNumber,String fullName) {
		log.info("Do map for lodge new claims start");
		PicsLodgementStub.NewClaimsMetaData newClaim = new PicsLodgementStub.NewClaimsMetaData();
		newClaim.setLodge_fund_id(userInfo.getFundType());
		newClaim.setLodge_mem_fname(userInfo.getFirstname());
		newClaim.setLodge_mem_lname(userInfo.getLastname());
		if(email!=null){
			newClaim.setLodge_submitted_email(email);
		}
		if(phoneNumber!=null){
			newClaim.setLodge_submitted_phone(phoneNumber);
		}
		if(fullName!=null){
			newClaim.setLodge_submitted_by(fullName);
		}
		/*case reference number*/
		if(null!=userInfo.getId())
			newClaim.setLodge_e_ref_no(userInfo.getId());
		if (null != userInfo.getTitle())
			newClaim.setLodge_mem_title(userInfo.getTitle());
		newClaim.setLodge_mem_gender(userInfo.getGender());
		if(null!=userInfo.getDateOfBirth())
		{
		newClaim.setLodge_mem_dob(ClaimRequestMapper.getDate(userInfo.getDateOfBirth()));
		}
		if (null != userInfo.getClientRefNumber())
		{
			newClaim.setLodge_cl_ref_no(userInfo.getClientRefNumber());
		}
		if (null != userInfo.getDateJoinedFund())
		{
		 newClaim.setLodge_date_joined(ClaimRequestMapper.getDate(userInfo.getDateJoinedFund()));
		}
		 newClaim.setLodge_add_info(userInfo.getAdditionalInfo());
		 ArrayList docList;

		if (null != userInfo.getElodgeDocument()) {
			docList = userInfo.getElodgeDocument();
			PicsLodgementStub.Documents[] picsDocArr = new PicsLodgementStub.Documents[docList
					.size()];

			DocumentDto doc;			
			for (int i = 0; i < picsDocArr.length; i++) {

				picsDocArr[i] = new PicsLodgementStub.Documents();
				doc = (DocumentDto) docList.get(i);
				picsDocArr[i].setDocument_path(doc.getDocumentlocation());				
				picsDocArr[i].setDocument_type(doc.getDocumentCode());
				picsDocArr[i].setDocument_format("");
			}
			newClaim.setDocuments(picsDocArr);
		}	
		log.info("Do map for lodge new claims finish");
		return newClaim;


	}

	public static PicsLodgementStub.ExistingClaimsMetaData doMapForLodgeExistingClaims(
			UserInfoBO userInfo,String email,String phoneNumber,String fullName,String productType,String randomNum) {
		log.info("Do map for lodge existing claims start");
		PicsLodgementStub.ExistingClaimsMetaData exisitingCL = new PicsLodgementStub.ExistingClaimsMetaData();
		
		exisitingCL.setLodge_fund_id(userInfo.getFundType());
		if(email!=null){
			exisitingCL.setLodge_submitted_email(email);
		}
		if(phoneNumber!=null){
			exisitingCL.setLodge_submitted_phone(phoneNumber);
		}
		if(fullName!=null){
			exisitingCL.setLodge_submitted_by(fullName);
		}
		if (null != userInfo.getClientRefNumber()) {
			exisitingCL.setLodge_e_ref_no(randomNum);			
		}
		if(productType!=null){
			exisitingCL.setLodge_claim_product(productType);
		}
		exisitingCL.setLodge_add_info(userInfo.getAdditionalInfo());
		if(userInfo.getApplicationId1()!=null){
			exisitingCL.setLodge_claim_no(userInfo.getApplicationId1());
		}
		ArrayList docList;

		if (null != userInfo.getElodgeDocument()) {
			docList = userInfo.getElodgeDocument();
			PicsLodgementStub.Documents[] picsDocArr = new PicsLodgementStub.Documents[docList
					.size()];
			DocumentDto doc ;
			for (int i = 0; i < picsDocArr.length; i++) {
				picsDocArr[i] = new PicsLodgementStub.Documents();
				doc = (DocumentDto) docList.get(i);
				picsDocArr[i].setDocument_path(doc.getDocumentlocation());
				picsDocArr[i].setDocument_type(doc.getDocumentCode());
				picsDocArr[i].setDocument_format("");
			}
			exisitingCL.setDocuments(picsDocArr);
		}
		log.info("Do map for lodge existing claims finish");
		return exisitingCL;

	}

	
	private static String getDate(Date dte){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.format(dte);
	}

	
	
	
	

}

