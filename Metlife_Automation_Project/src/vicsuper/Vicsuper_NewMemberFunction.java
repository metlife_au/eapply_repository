package vicsuper;

import java.awt.Robot;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class Vicsuper_NewMemberFunction extends MasterPage {
	WebDriverUtil driverUtil = null;
//	JavascriptExecutor jse = (JavascriptExecutor)driver.getWebDriver() ;
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	WebDriverWait wait = new WebDriverWait(driver, 20);

	public Vicsuper_NewMemberFunction NewMember_vicsuper() {
		String ConfirmationPage = dataTable.getData("VicsSuper_NewMemberFunction", "ConfirmationPage");
		System.out.println(ConfirmationPage);
			NewMemberFunction();
			HealthandLifeStyle();
			if (ConfirmationPage.equalsIgnoreCase("Yes")) {
				confirmation();
			}
			else {
				report.updateTestLog("Confirmation Page", "Confirmation Page is not displayed",
						Status.SCREENSHOT);
			}

			feedbackPopup();

			return new Vicsuper_NewMemberFunction(scriptHelper);
		}


		public Vicsuper_NewMemberFunction(ScriptHelper scriptHelper) {
			super(scriptHelper);
			driverUtil = new WebDriverUtil(driver);
		}

		public void NewMemberFunction() {
			System.out.println("Change");
			String EmailId = dataTable.getData("VicsSuper_NewMemberFunction", "EmailId");
			String TypeofContact = dataTable.getData("VicsSuper_NewMemberFunction", "TypeofContact");
			String ContactNumber = dataTable.getData("VicsSuper_NewMemberFunction", "ContactNumber");
			String TimeofContact = dataTable.getData("VicsSuper_NewMemberFunction", "TimeofContact");
			String Gender = dataTable.getData("VicsSuper_NewMemberFunction", "Gender");
			String FourteenHoursWork = dataTable.getData("VicsSuper_NewMemberFunction", "FourteenHoursWork");
			String Citizen = dataTable.getData("VicsSuper_NewMemberFunction", "Citizen");
			String EducationalInstitution = dataTable.getData("VicsSuper_NewMemberFunction", "EducationalInstitution");
			String EducationalDuties = dataTable.getData("VicsSuper_NewMemberFunction", "EducationalDuties");
			String TertiaryQual = dataTable.getData("VicsSuper_NewMemberFunction", "TertiaryQual");
			String AnnualSalary = dataTable.getData("VicsSuper_NewMemberFunction", "AnnualSalary");
			String CostType = dataTable.getData("VicsSuper_NewMemberFunction", "CostType");
			String DeathYN = dataTable.getData("VicsSuper_NewMemberFunction", "DeathYN");
			String DeathUnits = dataTable.getData("VicsSuper_NewMemberFunction", "DeathUnits");
			String TPDYN = dataTable.getData("VicsSuper_NewMemberFunction", "TPDYN");
			String TPDUnits = dataTable.getData("VicsSuper_NewMemberFunction", "TPDUnits");
			String IPYN = dataTable.getData("VicsSuper_NewMemberFunction", "IPYN");
			String IPUnits = dataTable.getData("VicsSuper_NewMemberFunction", "IPUnits");
			String Insure85Percent = dataTable.getData("VicsSuper_NewMemberFunction", "Insure85Percent");
			String WaitingPeriod = dataTable.getData("VicsSuper_NewMemberFunction", "WaitingPeriod");
			String BenefitPeriod = dataTable.getData("VicsSuper_NewMemberFunction", "BenefitPeriod");



			try {

				Robot roboobj = new Robot();


				// ****Click on Change Your Insurance Cover Link*****\\
				sleep(500);



					WebElement splOff = driver.findElement(By.xpath("//h4[text()='Special offer for new VicSuper EmployeeSaver members']"));
					jse.executeScript("arguments[0].scrollIntoView(true);", splOff);
					 sleep(500);

					report.updateTestLog("SPECIAL OFFER FOR NEW MEMBERS", "Special offer for new members is present",
							Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Special offer for new VicSuper EmployeeSaver members']")))
					.click();

			report.updateTestLog("SPECIAL OFFER FOR NEW MEMBERS", "Special offer for new members link is clicked",
					Status.PASS);




				sleep(5000);

	// -----------------------------------------------------Personal Details Page------------------------------------------------------------------------//


				// *****Agree to Duty of disclosure*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*")))
						.click();
				sleep(250);
				report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

				// *****Agree to Privacy Statement*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*")))
						.click();
				sleep(250);
				report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
				sleep(200);

	// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

				// *****Enter the Email Id*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
				report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
				sleep(500);

				// *****Click on the Contact Number Type****\\

				Select Contact = new Select(driver.findElement(By.xpath("//*[@ng-model='preferredContactType']")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
						Status.PASS);
				sleep(250);

				// ****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId")))
						.sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

				// ***Select the Preferred time of Contact*****\\

				if (TimeofContact.equalsIgnoreCase("Morning")) {
					driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]")).click();
					sleep(400);
					report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
				} else {
					driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]")).click();
					sleep(400);
					report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
				}

				/*
				 * // ***********Select the Gender******************\\
				 *
				 * if (Gender.equalsIgnoreCase("Male")) {
				 * driver.findElement(By.xpath(".//input[@value='Male']/parent::*"))
				 * .click(); sleep(250); report.updateTestLog("Gender",
				 * "Gender selected is: " + Gender, Status.PASS); } else {
				 * driver.findElement(By.xpath(".//input[@value='Female']/parent::*"
				 * )).click(); sleep(250); report.updateTestLog("Gender",
				 * "Gender selected is: " + Gender, Status.PASS); }
				 *
				 */

				//*****Click on Continue*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				sleep(3000);

	// -----------------------------------------------------Occupation------------------------------------------------------------------------//

				// *****Select the 14 Hours Question*******\\

				if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@name='fifteenHrsQuestion ']/parent::*"))).click(); // Dhipika
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@name='fifteenHrsQuestion']/parent::*"))).click(); // Dhipika
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
				}

				// *****Resident of Australia****\\

				if (Citizen.equalsIgnoreCase("Yes")) {
					driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
				} else {
					driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
				}

				/*
				// *****Industry********\\

				sleep(500);
				Select Industry = new Select(driver.findElement(By.name("industry")));
				Industry.selectByVisibleText(IndustryType);
				sleep(500);
				report.updateTestLog("Industry", "Industry Selected is: " + IndustryType, Status.PASS);
				sleep(1500);

				// *****Occupation*****\\

				Select Occupation = new Select(driver.findElement(By.name("occupation")));
				Occupation.selectByVisibleText(OccupationType);
				sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: " + OccupationType, Status.PASS);
				sleep(1000);
	*/

				// *****Are the duties of your regular occupation limited to
				// either:*****\\

				if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

					// ***Select duties which are undertaken within an Office
					// Environment?\\
					if (EducationalInstitution.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]")))
								.click(); // Dhipika
						report.updateTestLog("Educational institution", "Selected No", Status.PASS);
						sleep(1000);
					}

					// *****Educational duties performed within a school or other
					// educational institution******\\
					if (EducationalDuties.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]")))
								.click(); // Dhipika
						report.updateTestLog("Educational duties", "Selected No", Status.PASS);
						sleep(1000);
					}

				} else {
					report.updateTestLog("Additional Questions",
							"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

				}

				// Do you hold a tertiary qualification or work in a management role
				if (driver.getPageSource().contains("Do you:")) {

					// *****Do You Hold a Tertiary Qualification******\\
					if (TertiaryQual.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[1]"))).click(); // Dhipika
						report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[2]"))).click(); // Dhipika
						report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
						sleep(1000);
					}

				} else {
					report.updateTestLog("Additional Questions",
							"Do you hold a tertiary qualification or work in a management role is not present",
							Status.FAIL);
				}

				// *****What is your annual Salary******

				wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary); // Dhipika
				sleep(1000);
				report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);

				//*****Click on Continue*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				sleep(3000);



	// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//


				// ******Click on Cost of Insurance as*******\\
				sleep(1000);
				Select COSTTYPE = new Select(driver.findElement(By.name("premFreq")));
				COSTTYPE.selectByVisibleText(CostType);
				report.updateTestLog("Cost of Insurance", "Cost frequency selected is: " + CostType, Status.PASS);
				sleep(100);


				// *******Death Cover Section******\\
				if (DeathYN.equalsIgnoreCase("Yes")) {

					// To Select - Extra cover required
					if (DeathUnits.equalsIgnoreCase("0")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[1]"))).click(); // Dhipika
						report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
						sleep(1000);

					}

					else if (DeathUnits.equalsIgnoreCase("1")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[2]"))).click(); // Dhipika
						report.updateTestLog("Death Extra Cover", "Death Extra Cover - 1 Unit is selected", Status.PASS);
						sleep(1000);

					} else if (DeathUnits.equalsIgnoreCase("2")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[3]"))).click(); // Dhipika
						report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
						sleep(1000);

					} else {
						report.updateTestLog("Data Table", "Invalid input for DeathUnits in Data Sheet. Please enter 0/1/2",
								Status.FAIL);
					}

				}

				else if (DeathYN.equalsIgnoreCase("No")) {

				}
				else {
					report.updateTestLog("Data Table", "Invalid input for DeathYN in Data Sheet", Status.FAIL);
				}
				// *******TPD Cover Section******\\
				sleep(800);

				if (TPDYN.equalsIgnoreCase("Yes")) {

					// To Select - Extra cover required
					if (TPDUnits.equalsIgnoreCase("0")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[1]"))).click(); // Dhipika
						report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
						sleep(1000);

					}

					else if (TPDUnits.equalsIgnoreCase("1")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[2]"))).click(); // Dhipika
						report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 1 Unit is selected", Status.PASS);
						sleep(1000);

					} else if (TPDUnits.equalsIgnoreCase("2")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[3]"))).click(); // Dhipika
						report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 2 Unit is selected", Status.PASS);
						sleep(1000);

					} else {
						report.updateTestLog("Data Table", "Invalid input for TPDUnits in Data Sheet. Please enter 0/1/2",
								Status.FAIL);
					}

					if ((DeathYN.equalsIgnoreCase("Yes"))) {
						Integer DU = Integer.parseInt(DeathUnits);
						Integer TPDU = Integer.parseInt(TPDUnits);

						if (TPDU>DU) {


						if (driver.getPageSource().contains("Your TPD cover amount should not be greater than Death cover amount.")) {
							report.updateTestLog("TPD Validation",
									"Error Message 'Your TPD cover amount should not be greater than Death cover amount.' is displayed",
									Status.PASS);

							// To Select None value for Death
							wait.until(ExpectedConditions
									.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[3]")))
									.click(); // Dhipika
							report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
							sleep(1000);

						} else {
							report.updateTestLog("TPD Validation",
									"Error Message 'Your TPD cover amount should not be greater than Death cover amount.' is not displayed",
									Status.FAIL);
						}

						}

					}
					else if ((DeathYN.equalsIgnoreCase("No"))) {

						if (driver.getPageSource().contains("You cannot apply for TPD cover without Death Cover.")) {
							report.updateTestLog("TPD Validation",
									"Error Message 'You cannot apply for TPD cover without Death Cover.' is displayed",
									Status.PASS);

							// To Select None value for Death
							wait.until(ExpectedConditions
									.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[3]")))
									.click(); // Dhipika
							report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
							sleep(1000);

						} else {
							report.updateTestLog("TPD Validation",
									"Error Message 'You cannot apply for TPD cover without Death Cover.' is not displayed",
									Status.FAIL);
						}
					}
				}
				else if (TPDYN.equalsIgnoreCase("No")) {

				}
				else {
					report.updateTestLog("Data Table", "Invalid input for TPDYN in Data Sheet", Status.FAIL);
				}


				// *******IP Cover section********\\
				sleep(3000);
				if (IPYN.equalsIgnoreCase("Yes")) {

					// *****Insure 85% of My Salary******\\

					if (Insure85Percent.equalsIgnoreCase("Yes")) {

						WebElement noUnitIP = driver
								.findElement(By.xpath("(//input[@name='IPRequireCover']/parent::*)[1]"));
						WebElement oneUnitIP = driver
								.findElement(By.xpath("(//input[@name='IPRequireCover']/parent::*)[2]/parent::*"));
						WebElement twoUnitIP = driver
								.findElement(By.xpath("(//input[@name='IPRequireCover']/parent::*)[3]/parent::*"));

						String oneUnitIPatt = oneUnitIP.getAttribute("disabled");
						String twoUnitIPatt = twoUnitIP.getAttribute("disabled");
						System.out.println("ATT:"+oneUnitIPatt);
						System.out.println("ATT:"+twoUnitIPatt);

						// To vlaidate none unit
						if (noUnitIP.isEnabled()) {
							report.updateTestLog("IP Extra Cover",
									"IP Unit 'None' is enabled when IP amount is greater than 85% of the annual salary.",
									Status.PASS);
						} else {
							report.updateTestLog("IP Extra Cover",
									"IP Unit 'None' is not enabled when IP amount is greater than 85% of the annual salary.",
									Status.FAIL);
						}
						// To vlaidate 1 unit
						if (oneUnitIPatt.equalsIgnoreCase("true")) {

							report.updateTestLog("IP Extra Cover",
									"IP Unit '1 Unit' is not enabled when IP amount is greater than 85% of the annual salary.",
									Status.PASS);
						} else {
							report.updateTestLog("IP Extra Cover",
									"IP Unit '1 Unit' is enabled when IP amount is greater than 85% of the annual salary.",
									Status.FAIL);
						}
						// To vlaidate 2 unit
						if (twoUnitIPatt.equalsIgnoreCase("true")) {
							report.updateTestLog("IP Extra Cover",
									"IP Unit '2 Unit' is not enabled when IP amount is greater than 85% of the annual salary.",
									Status.PASS);
						} else {
							report.updateTestLog("IP Extra Cover",
									"IP Unit '2 Unit' is enabled when IP amount is greater than 85% of the annual salary.",
									Status.FAIL);
						}

					}


					if ((WaitingPeriod.equalsIgnoreCase("Validation")) || (BenefitPeriod.equalsIgnoreCase("Validation"))) {
						String[] WP = { "30 Days", "60 Days", "90 Days" };
						String[] BP = { "2 Years", "5 Years" };

						WebElement dropdownWP = driver.findElement(By.xpath("//*[@ng-model='waitingPeriodAddnl']"));
						WebElement dropdownBP = driver.findElement(By.xpath("//*[@ng-model='benefitPeriodAddnl']"));

						verify_Dropdown(dropdownWP, WP, "Waiting Period");
						verify_Dropdown(dropdownBP, BP, "Benefit Period");

					}

					else {

						Select waitingperiod = new Select(
								driver.findElement(By.xpath("//select[@ng-model='waitingPeriodAddnl']")));
						waitingperiod.selectByVisibleText(WaitingPeriod);
						report.updateTestLog("Waiting Period", "Waiting Period of: " + WaitingPeriod + " is Selected",
								Status.PASS);
						sleep(2000);

						Select benefitperiod = new Select(
								driver.findElement(By.xpath("//select[@ng-model='benefitPeriodAddnl']")));
						benefitperiod.selectByVisibleText(BenefitPeriod);
						report.updateTestLog("Benefit Period", "Benefit Period: " + BenefitPeriod + " is Selected",
								Status.PASS);
						sleep(2000);


						if (BenefitPeriod.equalsIgnoreCase("5 Years")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(By.xpath("//*[text()='the member is not a casual or contractor employee']"))).click();
							report.updateTestLog("Casual or contractor employee", "The member is not a casual or contractor employee is selected", Status.PASS);
							sleep(3000);

						}


					}

					// To Select - Extra cover required
					if (IPUnits.equalsIgnoreCase("0")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[1]"))).click(); // Dhipika
						report.updateTestLog("IP Extra Cover", "IP Extra Cover - None is selected", Status.PASS);
						sleep(1000);

					}

					else if (IPUnits.equalsIgnoreCase("1")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[2]"))).click(); // Dhipika
						report.updateTestLog("IP Extra Cover", "IP Extra Cover - 1 Unit is selected", Status.PASS);
						sleep(1000);

					} else if (IPUnits.equalsIgnoreCase("2")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[3]"))).click(); // Dhipika
						report.updateTestLog("IP Extra Cover", "IP Extra Cover - 2 Unit is selected", Status.PASS);
						sleep(1000);

					} else {
						report.updateTestLog("Data Table", "Invalid input for IPUnits in Data Sheet. Please enter 0/1/2",
								Status.FAIL);
					}

				}
				else if (IPYN.equalsIgnoreCase("No")) {

				}
				else {
					report.updateTestLog("Data Table", "Invalid input for IPYN in Data Sheet", Status.FAIL);
				}

				if ( (DeathYN.equalsIgnoreCase("No")) && (TPDYN.equalsIgnoreCase("No")) && (IPYN.equalsIgnoreCase("No")) ) {
					System.out.println("Inside None selected");

					//To click on calculate
					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//button[@ng-click='formOneSubmit(coverCalculatorForm);']")))
							.click();
					sleep(2000);
					report.updateTestLog("Calculate", "Calculate button is clicked", Status.PASS);
					System.out.println("error disp");
					String validation1 = driver.findElement(By.xpath("//*[@id='death']//*[contains(text(), 'Please select required Cover')]")).getText();
					String validation2 = driver.findElement(By.xpath("//*[@id='tpd']//*[contains(text(), 'Please select required Cover')]")).getText();
					String validation3 = driver.findElement(By.xpath("//*[@id='sc']//*[contains(text(), 'Please select required Cover')]")).getText();

					if ( (validation1.equalsIgnoreCase("Please select required Cover")) && (validation2.equalsIgnoreCase("Please select required Cover")) && (validation3.equalsIgnoreCase("Please select required Cover")) ) {
						report.updateTestLog("Death, TPD, IP Validation", "Error message for Death, TPD and IP are displayed when the extra Unit is not selected", Status.PASS);
						System.out.println("error not disp");
						//To select Death none
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='requireCover']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
						sleep(1000);

						//To select TPD none
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='TPDRequireCover']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
						sleep(3000);

						//To select IP none
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='IPRequireCover']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("IP Extra Cover", "IP Extra Cover - None is selected", Status.PASS);
						sleep(1000);

					}

					else {
						report.updateTestLog("Death, TPD, IP Validation", "Error message for Death / TPD / IP are not displayed when the extra Unit is not selected", Status.FAIL);
					}

				}

				//To click on calculate
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//button[@ng-click='formOneSubmit(coverCalculatorForm);']")))
						.click();
				sleep(4000);
				report.updateTestLog("Calculate", "Calculate button is clicked", Status.PASS);

				//To click on continue
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//button[@ng-click='goToAura()']")))
						.click();
				report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);

				sleep(4000);
			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}

		}

		public void HealthandLifeStyle() {
			sleep(3000);
			String Restricted_Ilness = dataTable.getData("VicsSuper_NewMemberFunction", "Restricted_Ilness");
			String PreviousClaim = dataTable.getData("VicsSuper_NewMemberFunction", "PreviousClaim");
			String Restricted_Work = dataTable.getData("VicsSuper_NewMemberFunction", "Restricted_Work");
			String Diagnosed_Illness = dataTable.getData("VicsSuper_NewMemberFunction", "Diagnosed_Illness");
			String MedicalTreatment = dataTable.getData("VicsSuper_NewMemberFunction", "MedicalTreatment");
			String Previous_Insurance = dataTable.getData("VicsSuper_NewMemberFunction", "Previous_Insurance");

			try {

				// *****Select the Restricted_Ilness Question*******\\

				if (Restricted_Ilness.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you restricted, due to illness or injury from carrying out any of the identifiable duties of your current and normal occupation on a full time basis (even if you are not currently working on a full time basis). Full time basis is considered to be at least 35 hours per week.']/../following-sibling::div/div/div[1]/label)[1]")))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted due to ilness Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you restricted, due to illness or injury from carrying out any of the identifiable duties of your current and normal occupation on a full time basis (even if you are not currently working on a full time basis). Full time basis is considered to be at least 35 hours per week.']/../following-sibling::div/div/div[1]/label)[2]")))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted due to ilness Question", "Selected No", Status.PASS);
				}

				// *****Select the PreviousClaim Question*******\\

				if (PreviousClaim.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Are you contemplating or have you ever made a claim')]/../following-sibling::div/div/div[1]/label)[1]")))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Claim Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Are you contemplating or have you ever made a claim')]/../following-sibling::div/div/div[1]/label)[2]")))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Claim Question", "Selected No", Status.PASS);
				}

				// *****Select the Restricted_Work Question*******\\

				if (Restricted_Work.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you been restricted from work or unable to perform any of your regular duties for more than ten consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?']/../following-sibling::div/div/div[1]/label)[1]")))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted Work Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you been restricted from work or unable to perform any of your regular duties for more than ten consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?']/../following-sibling::div/div/div[1]/label)[2]")))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted Work Question", "Selected No", Status.PASS);
				}

				// *****Select the Diagnosed_Illness Question*******\\

				if (Diagnosed_Illness.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Have you been diagnosed with an illness')]/../following-sibling::div/div/div[1]/label)[1]")))
							.click();
					sleep(1000);
					report.updateTestLog("Diagnosed with illness Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[contains(text(), 'Have you been diagnosed with an illness')]/../following-sibling::div/div/div[1]/label)[2]")))
							.click();
					sleep(1000);
					report.updateTestLog("Diagnosed with illness Question", "Selected No", Status.PASS);
				}

				// *****Select the MedicalTreatment Question*******\\

				if (MedicalTreatment.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you currently contemplating any medical treatment or advice for any illness or injury for which you have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?']/../following-sibling::div/div/div[1]/label)[1]")))
							.click();
					sleep(1000);
					report.updateTestLog("Medical Treatment Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Are you currently contemplating any medical treatment or advice for any illness or injury for which you have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?']/../following-sibling::div/div/div[1]/label)[2]")))
							.click();
					sleep(1000);
					report.updateTestLog("Medical Treatment Question", "Selected No", Status.PASS);
				}

				jse.executeScript("window.scrollTo(0, document.body.scrollHeight);");


				// *****Select the Previous_Insurance Question*******\\

				if (Previous_Insurance.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you had an application for Life, TPD, Trauma, Income Protection insurance declined, deferred or accepted with exclusions or loadings by an insurer?']/../following-sibling::div/div/div[1]/label)[1]")))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Insurance Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Have you had an application for Life, TPD, Trauma, Income Protection insurance declined, deferred or accepted with exclusions or loadings by an insurer?']/../following-sibling::div/div/div[1]/label)[2]")))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Insurance Question", "Selected No", Status.PASS);
				}
				sleep(3000);
				//To click on Continue
				wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//button[@ng-click='proceedNext()']")))
						.click();
				report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);


				sleep(1000);
			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

		private void confirmation() {
			sleep(3000);
			System.out.println("confirmation");
			String Confirmation_DetailsValidations = dataTable.getData("VicsSuper_NewMemberFunction", "Confirmation_DetailsValidations");


			if (Confirmation_DetailsValidations.equalsIgnoreCase("Yes")) {
				confirmationScreenValidation();
			}


			try {

				// ******Agree to general Consent*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span")))
						.click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(3000);

				// ******Click on Submit*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']")))
						.click();
				report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
				sleep(2000);

			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

		// **********************************************************************************************************************************************//
		// ------------------------------------------------PREMIUM
		// CALCULATION-----------------------------------------------------------------------//
		// **********************************************************************************************************************************************//



		/*
		 * public String amountFormatter(String expectedDeathcover) { String amount
		 * = expectedDeathcover; if (amount.contains(",")) {
		 * System.out.println("inside ,"); String[] part = amount.split("$");
		 * System.out.println("splited"); String amount1 = part[0];
		 * System.out.println("a1"); String amount2 = part[1];
		 * System.out.println("a2");
		 *
		 * amount = amount1+""+amount2; System.out.println("amount:"+amount); }
		 *
		 * return amount;
		 *
		 * }
		 */

		public void confirmationScreenValidation() {

			String DOB = dataTable.getData("VicsSuper_NewMemberFunction", "DOB");
			String Gender = dataTable.getData("VicsSuper_NewMemberFunction", "Gender");
			String EmailId = dataTable.getData("VicsSuper_NewMemberFunction", "EmailId");
			String TypeofContact = dataTable.getData("VicsSuper_NewMemberFunction", "TypeofContact");
			String ContactNumber = dataTable.getData("VicsSuper_NewMemberFunction", "ContactNumber");
			String TimeofContact = dataTable.getData("VicsSuper_NewMemberFunction", "TimeofContact");
			String FourteenHoursWork = dataTable.getData("VicsSuper_NewMemberFunction", "FourteenHoursWork");
			String Citizen = dataTable.getData("VicsSuper_NewMemberFunction", "Citizen");
			String EducationalInstitution = dataTable.getData("VicsSuper_NewMemberFunction", "EducationalInstitution");
			String EducationalDuties = dataTable.getData("VicsSuper_NewMemberFunction", "EducationalDuties");
			String TertiaryQual = dataTable.getData("VicsSuper_NewMemberFunction", "TertiaryQual");
			String AnnualSalary = dataTable.getData("VicsSuper_NewMemberFunction", "AnnualSalary");
			String OneUnitValue = dataTable.getData("VicsSuper_NewMemberFunction", "Cover for one unit");
			String DeathAmount;
			String DeathUnits = dataTable.getData("VicsSuper_NewMemberFunction", "DeathUnits");
			String TPDAmount ;
			String TPDUnits = dataTable.getData("VicsSuper_NewMemberFunction", "TPDUnits");
			String IPAmount;
			String IPUnits = dataTable.getData("VicsSuper_NewMemberFunction", "IPUnits");
			String WaitingPeriod=dataTable.getData("VicsSuper_NewMemberFunction", "WaitingPeriod");
			String BenefitPeriod=dataTable.getData("VicsSuper_NewMemberFunction", "BenefitPeriod");

			Integer unitValue = Integer.parseInt(OneUnitValue);
			Integer DU = Integer.parseInt(DeathUnits);
			Integer TPDU = Integer.parseInt(TPDUnits);
			Integer IPU = Integer.parseInt(IPUnits);




			String XML = dataTable.getData("VicSuper_Login", "XML");

			String[] part;
			String part2;
			String[] value;

			//To get firstName
			part = XML.split("<firstName>");
			part2 = part[1];
			value = part2.split("</firstName>");
			String firstName = value[0];

			//To get lastName
			part = XML.split("<lastName>");
			part2 = part[1];
			value = part2.split("</lastName>");
			String lastName = value[0];

			//To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1];
			value = part2.split("</dateOfBirth>");
			String dob = value[0];

			if (dob.equalsIgnoreCase(DOB)) {
				System.out.println("Correct DOB in DataShaeet");
			}
			else {
				report.updateTestLog("Date of Birth", "DOB in XML and DOB in Data Sheet'VicsSuper_NewMemberFunction' does not match.", Status.FAIL);
			}



			//Existing Cover Death
			part = XML.split("<amount>");
			String part2Death = part[1];
			String part2Tpd = part[2];
			String part2IP = part[3];

			String[] valueDeath = part2Death.split("</amount>");
			String[] valueTpd = part2Tpd.split("</amount>");
			String[] valueIP = part2IP.split("</amount>");

			String Death = valueDeath[0];
			String TPD = valueTpd[0];
			String IP = valueIP[0];

			double dD = Double.parseDouble(Death);
			int D = (int) dD;
			double dT = Double.parseDouble(TPD);
			int T = (int) dT;
			double dI = Double.parseDouble(IP);
			int I = (int) dI;

			Death = "$"+D;
			TPD = "$"+T;
			IP = "$"+I;
			System.out.println(Death);
			System.out.println(TPD);
			System.out.println(IP);


			//To calculate new Death Cover

			System.out.println("Unit val:"+unitValue);
			System.out.println("DU val:"+DU);
			System.out.println("TPDU val:"+TPDU);
			System.out.println("D val:"+D);
			System.out.println("T val:"+T);


			int addamount = unitValue*DU;
			System.out.println("addamount"+addamount);
			addamount = unitValue*TPDU;
			System.out.println("addamount"+addamount);


			int NewDeathAmount = D + (unitValue*DU);
			DeathAmount = NewDeathAmount+"";
			System.out.println("New value:"+DeathAmount);
			int NewTPDAmount = T + (unitValue*TPDU);
			TPDAmount = NewTPDAmount+"";
			System.out.println("New value:"+TPDAmount);
			int NewIPAmount = I + (500*IPU);
			IPAmount = NewIPAmount+"";
			System.out.println("New value:"+IPAmount);


			//To add $ before the amount value
			AnnualSalary = "$"+AnnualSalary;
			DeathAmount = "$"+DeathAmount;
			TPDAmount = "$"+TPDAmount;
			IPAmount = "$"+IPAmount;



			//First Name
			 WebElement objFName = driver.findElement(By.xpath("//*[text()='First name']//following::label"));
			 fieldValidation("WebElement",objFName,firstName,"First Name");

			//Last Name
			 WebElement objLName = driver.findElement(By.xpath("//*[text()='Last name']//following::label"));
			 fieldValidation("WebElement",objLName,lastName,"Last Name");

			//Gender
			 WebElement ObjGender = driver.findElement(By.xpath("//*[text()='Gender']//following::label"));
			 fieldValidation("WebElement",ObjGender,Gender,"Genderquestion");

			//Phone Number type
			 WebElement objcontactType = driver.findElement(By.xpath("//*[text()='Preferred contact type']//following::label"));
			 fieldValidation("WebElement",objcontactType,TypeofContact,"contactType");

			//PhoneNumber
			 WebElement objpreferrednumCCId = driver.findElement(By.xpath("//*[text()='Preferred contact number ']//following::label"));
			 fieldValidation("WebElement",objpreferrednumCCId,ContactNumber,"preferrednumCCId");

			//Email
			 WebElement objcontactEmail = driver.findElement(By.xpath("//*[text()='Email address']//following::label"));
			 fieldValidation("WebElement",objcontactEmail,EmailId,"Email");

			//DOB
			 WebElement objDOB = driver.findElement(By.xpath("//*[text()='Date of birth']//following::label"));
			 fieldValidation("WebElement",objDOB,dob,"DOB");

			//Morning Afternoon
			 WebElement objTime = driver.findElement(By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label"));
			 String eletext = objTime.getText();
			 System.out.println(eletext);
			 System.out.println(TimeofContact);
			 if (eletext.contains(TimeofContact)) {
				 report.updateTestLog("Time of contact ",  "Time of contact "+TimeofContact+" is displayed", Status.PASS);

			}
			 else {
				 report.updateTestLog("Time of contact ",  "Time of contact "+TimeofContact+" is not displayed", Status.FAIL);

			}


			//Do you work more than 14 hours per week?


			 WebElement ObjWorkHRS = driver.findElement(By.xpath("//*[text()='Do you work 14 or more hours per week?']//following::label"));
			 fieldValidation("WebElement",ObjWorkHRS,FourteenHoursWork,"FourteenHoursWork");

			//Are you a citizen or permanent resident of Australia? Yes No

			 WebElement objCitizen = driver.findElement(By.xpath("//*[contains(text(),'Are you a citizen or permanent resident of Australia')]//following::label"));
			 fieldValidation("WebElement",objCitizen,Citizen,"Citizen");


			 //office Duties are Undertaken within an Office Environment?
				WebElement eduIns = driver.findElement(By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[1]"));
				fieldValidation("WebElement",eduIns,EducationalInstitution,"Are the duties of your regular occupation limited to Question1");

				WebElement eduDut = driver.findElement(By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[3]"));
				fieldValidation("WebElement",eduDut,EducationalDuties,"Are the duties of your regular occupation limited to Question2");

			//	TertiaryQualification / ManagementRole

				 WebElement objQual = driver.findElement(By.xpath("//*[text()='Do you:  ']//following::label"));
				 fieldValidation("WebElement",objQual,TertiaryQual,"TertiaryQualification / ManagementRole");

			//  Current annual salary?
				 WebElement objannualSalCCId = driver.findElement(By.xpath("//*[text()='Annual income']//following::label"));
				 fieldValidation("Amount Validation",objannualSalCCId,AnnualSalary,"AnnualSalary");

				 Death = "$"+D;
					TPD = "$"+T;
					IP = "$"+I;
				//  Death

					//  Death Existing Amount

					WebElement deathEXTAmount = driver.findElement(By.xpath("(//*[text()='Existing Cover'])[1]//following::p"));
					fieldValidation("Amount Validation",deathEXTAmount,Death,"Death Existing Cover");

					//  Death Amount

					WebElement deathAmount = driver.findElement(By.xpath("(//*[text()='New Cover'])[1]//following::p"));
					fieldValidation("Amount Validation",deathAmount,DeathAmount,"Death New Cover");
					//amountValidator("Death Amount",deathAmount,DeathAmount );

					/*//  Death Weekly Cost

					WebElement DeathWeeklycost = driver.findElement(By.xpath("(//*[text()='Weekly cost'])[1]//following::p"));
					fieldValidation("WebElement",DeathWeeklycost,DeathWeeklyCost,"Death Weekly Cost");
				*/

			//  TPD Amount

					//  TPD Existing Amount

					WebElement tpdExtAmount = driver.findElement(By.xpath("(//*[text()='Existing Cover'])[2]//following::p"));
					fieldValidation("Amount Validation",tpdExtAmount,TPD,"TPD Existing Cover");

					//  TPD Amount

					WebElement tpdAmount = driver.findElement(By.xpath("(//*[text()='New Cover'])[2]//following::p"));
					fieldValidation("Amount Validation",tpdAmount,TPDAmount,"TPD New Cover");
					//amountValidator("Death Amount",deathAmount,DeathAmount );

					/*//  TPD Weekly Cost

					WebElement tpdWeeklycost = driver.findElement(By.xpath("(//*[text()='Weekly cost'])[2]//following::p"));
					fieldValidation("WebElement",tpdWeeklycost,TPDWeeklyCost,"TPD Weekly Cost");
					*/



				//  IP Amount

							//  IP Existing Amount

							WebElement ipAmount = driver.findElement(By.xpath("(//*[text()='Existing Cover'])[3]//following::p"));
							fieldValidation("Amount Validation",ipAmount,IP,"IP Existing Cover");

							//  IP Amount

							WebElement ipExtAmount = driver.findElement(By.xpath("(//*[text()='New Cover'])[3]//following::p"));
							fieldValidation("Amount Validation",ipExtAmount,IPAmount,"IP New Cover");
							//amountValidator("Death Amount",deathAmount,DeathAmount );

							//  Waiting Period

							WebElement ipWaitingPeriod = driver.findElement(By.xpath("(//*[text()='Waiting Period'])[1]//following::p"));
							fieldValidation("WebElement",ipWaitingPeriod,WaitingPeriod,"Waiting Period");
							//amountValidator("Death Amount",deathAmount,DeathAmount );

							//  Benefit Period

							WebElement ipBenefitPeriod = driver.findElement(By.xpath("(//*[text()='Benefit Period'])[1]//following::p"));
							fieldValidation("WebElement",ipBenefitPeriod,BenefitPeriod,"Benefit Period");
							//amountValidator("Death Amount",deathAmount,DeathAmount );

		}


		public void verify_Dropdown(WebElement dropdown, String[] dropDownOptions, String field) {
			String input = null;
			Select select = new Select(dropdown);
			int index = 0;
			List<WebElement> options = select.getOptions();
			for (WebElement we : options) {
				index++;
				// for (int i=0; i<exp.length(); i++){
				String s = we.getText();
				System.out.println("Option of webpase index " + index + " is " + s);

				if (s.equals(dropDownOptions[index - 1])) {
					System.out.println("Option " + index + " is" + s);
					input = input+","+s;
				} else {
					System.out.println("Invalid options");
					report.updateTestLog("DropDowm Validation", "Invalid Options in DropDown "+field, Status.FAIL);
					break;
				}
				// }

			}
			report.updateTestLog("DropDowm Validation", "DropDown -"+field+" contains the Given Values ="+input, Status.PASS);

		}

		public void feedbackPopup()
		{
			System.out.println("feedbackPopup");
			String ConfirmationPage = dataTable.getData("VicsSuper_NewMemberFunction", "ConfirmationPage");

			// *******Feedback popup******\\
			sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(5000);

			//wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail")))

			if (ConfirmationPage.equalsIgnoreCase("Yes")) {

				// *****Fetching the Application Status*****\\

				String Appstatus = driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
				sleep(500);

				// *****Fetching the Application Number******\\

				String App = driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p"))
						.getText();
				sleep(1000);

				System.out.println(App);

				// ******Download the PDF***********\\
				/*
				 * properties = Settings.getInstance(); String
				 * StrBrowseName=properties.getProperty("currentBrowser"); if
				 * (StrBrowseName.equalsIgnoreCase("Chrome")){
				 */
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
				sleep(3000);

				report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

				sleep(1000);

				report.updateTestLog("Decision Page", "Application No: " + App, Status.PASS);
				sleep(500);

				report.updateTestLog("Decision Page", "Application Status: " + Appstatus, Status.PASS);

			}

			else {
				report.updateTestLog("Application Declined", "Application is declined", Status.SCREENSHOT);
			}
		}












	}
