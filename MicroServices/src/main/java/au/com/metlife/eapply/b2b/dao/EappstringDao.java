/**
 * 
 */
package au.com.metlife.eapply.b2b.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import au.com.metlife.eapply.b2b.model.Eappstring;

/**
 * @author akishore
 *
 */
@Transactional
public interface EappstringDao extends CrudRepository<Eappstring, Long> {
	
	public Eappstring findBySecurestring( String securestring);	
	public Eappstring findById(Long id);	

}
