/*Summary Page Controller Starts*/
AeisApp.controller('summary',['$scope', '$rootScope', '$location','$timeout','$routeParams','$window', 'fetchUrlSvc', 'ngDialog', 'appData', 'fetchPersoanlDetailSvc', 'calcDeathAmtSvc', 'calcTPDAmtSvc', 'calcIPAmtSvc', 'saveEapplyData', 'submitEapplySvc','PersistenceService', 'auraRespSvc', 'fetchIndustryList',
                         function($scope, $rootScope, $location, $timeout, $routeParams, $window, fetchUrlSvc, ngDialog, appData, fetchPersoanlDetailSvc, calcDeathAmtSvc, calcTPDAmtSvc, calcIPAmtSvc, saveEapplyData, submitEapplySvc, PersistenceService, auraRespSvc, fetchIndustryList) {
  $scope.changeCoverSummary = {};
  
  $scope.eventList = 
	  [
		  {
			  "cde": "BRTH",
			  "desc": "Birth or adoption of a child"
		 },
		 {
			 "cde": "FRST",
  		      "desc": "Purchase property",
  		 },
  		{
			 "cde": "MARR",
  		      "desc": "Marriage",
  		 },
  		{
			 "cde": "NJOB",
  		      "desc": "New job",
  		 },
  		{
			 "cde": "OTHER",
  		      "desc": "Others",
  		 }];
  $scope.init = function() {
    $rootScope.$broadcast('enablepointer');
    $window.scrollTo(0, 0);
    $scope.urlList = fetchUrlSvc.getUrlList();
    $scope.auraDetails = appData.getChangeCoverAuraDetails();
    $scope.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

    $scope.personalDetails = $scope.inputDetails.personalDetails || {};
    $scope.contactDetails = $scope.inputDetails.contactDetails || {};
    angular.extend($scope.changeCoverSummary, appData.getAppData());
    $scope.changeCoverSummary.auraDetails = $scope.auraDetails;
    
    $scope.resetValue = {};
    $scope.resetValue.deathCoverPremium = $scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium;
    $scope.resetValue.tpdCoverPremium = $scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium;
    $scope.resetValue.ipCoverPremium = $scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium;
    
   console.log($scope.changeCoverSummary);
    //if(!$scope.changeCoverSummary.auraDetails.overallDecision && $scope.changeCoverSummary.auraDisabled == false){
    var auraDisableCustom = ($scope.changeCoverSummary.auraDisabled == 'false' || $scope.changeCoverSummary.auraDisabled == false) ? false : true;
    if($scope.changeCoverSummary.auraDetails.overallDecision == undefined && !auraDisableCustom){
    	//console.log("code here");
    	console.log(appData.getAppData());
    	var savedData = appData.getAppData();
    	$scope.changeCoverSummary.auraDetails.overallDecision = savedData.overallDecision;
    	$scope.changeCoverSummary.auraDetails.clientMatchReason = savedData.clientMatchReason;
    	
    	$scope.changeCoverSummary.auraDetails.clientMatched = savedData.clientMatched;
    	$scope.changeCoverSummary.auraDetails.deathAuraResons = savedData.deathAuraResons;
    	$scope.changeCoverSummary.auraDetails.deathDecision = savedData.deathDecision;
    	$scope.changeCoverSummary.auraDetails.deathExclusions = savedData.deathExclusions;
    	$scope.changeCoverSummary.auraDetails.deathLoading = savedData.deathLoading;
    	$scope.changeCoverSummary.auraDetails.deathOrigTotalDebitsValue = savedData.deathOrigTotalDebitsValue;
    	$scope.changeCoverSummary.auraDetails.deathResons = savedData.deathResons;
    	$scope.changeCoverSummary.auraDetails.ipAuraResons = savedData.ipAuraResons;
    	$scope.changeCoverSummary.auraDetails.ipDecision = savedData.ipDecision;
    	$scope.changeCoverSummary.auraDetails.ipExclusions = savedData.ipExclusions;
    	$scope.changeCoverSummary.auraDetails.ipLoading = savedData.ipLoading;
    	$scope.changeCoverSummary.auraDetails.ipOrigTotalDebitsValue = savedData.ipOrigTotalDebitsValue;
    	$scope.changeCoverSummary.auraDetails.ipResons = savedData.ipResons;
    	$scope.changeCoverSummary.auraDetails.tpdAuraResons = savedData.tpdAuraResons;
    	$scope.changeCoverSummary.auraDetails.tpdDecision = savedData.tpdDecision;
    	$scope.changeCoverSummary.auraDetails.tpdExclusions = savedData.tpdExclusions;
    	$scope.changeCoverSummary.auraDetails.tpdLoading = savedData.tpdLoading;
    	$scope.changeCoverSummary.auraDetails.tpdOrigTotalDebitsValue = savedData.tpdOrigTotalDebitsValue;
    	$scope.changeCoverSummary.auraDetails.tpdResons = savedData.tpdResons;
    	
    	if(($scope.changeCoverSummary.auraDetails.deathLoading != null && $scope.changeCoverSummary.auraDetails.deathLoading != '0.0') || ($scope.changeCoverSummary.auraDetails.tpdLoading != null && $scope.changeCoverSummary.auraDetails.tpdLoading != 0.0) || ($scope.changeCoverSummary.auraDetails.ipLoading != null && $scope.changeCoverSummary.auraDetails.ipLoading != 0.0)){
    		$scope.auraDetails.specialTerm = true;
        	$scope.changeCoverSummary.auraDetails.specialTerm = true;
    	}else{
    		$scope.auraDetails.specialTerm = false;
        	$scope.changeCoverSummary.auraDetails.specialTerm = false;
    	}	
    }
    
    if(auraDisableCustom){
    	$scope.auraDetails.specialTerm = false;
    	$scope.changeCoverSummary.auraDetails.specialTerm = false;
    	$scope.auraDetails.overallDecision = "";
    }
    //console.log($scope.changeCoverSummary.auraDetails);
    
    //$scope.changeCoverSummary.dob = moment($scope.changeCoverSummary.dob).format('DD-MM-YYYY');
    $scope.changeCoverSummary.dob = moment($scope.changeCoverSummary.dob, "DD/MM/YYYY").format('DD/MM/YYYY'); 
    $scope.eventName = $scope.changeCoverSummary.eventName;
    $scope.changeCoverSummary.auraDisabled = ($scope.changeCoverSummary.auraDisabled == 'false' || $scope.changeCoverSummary.auraDisabled == false) ? false : true;
    if($scope.changeCoverSummary.contactType == "1") {
			$scope.contactType = 'Mobile';
		} else if($scope.changeCoverSummary.contactType == "2") {
			$scope.contactType = 'Home';
		} else if($scope.changeCoverSummary.contactType == "3") {
			$scope.contactType = 'Work';
		} else {
      $scope.contactType = '';
    }
    
    if($scope.changeCoverSummary.occupationDetails.smoker == undefined){
  		if($scope.changeCoverSummary.occupationDetails.smokerQuestion == "1"){
  			var smokerCustom = true;
	     }else{
	    	 var smokerCustom = false;
	     }
  	}else{
  		if($scope.changeCoverSummary.occupationDetails.smoker== "Yes"){
  			var smokerCustom = true;
	     }else{
	    	 var smokerCustom = false;
	     }
  	}

    // Industry name
    $scope.changeCoverSummary.industryName = fetchIndustryList.getIndustryName($scope.changeCoverSummary.occupationDetails.industryCode);
    // Death loading calculation
  	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.deathLoading && $scope.auraDetails.deathLoading > 0){
      	var deathReqObject = {
          		"age": $scope.changeCoverSummary.age,
          		"fundCode": "AEIS",
          		"gender": $scope.changeCoverSummary.occupationDetails.gender,
          		"deathOccCategory": $scope.changeCoverSummary.deathOccCategory,
          		"smoker": smokerCustom,
          		"deathUnitsCost": null,
          		"premiumFrequency": "Monthly",
          		"manageType": "CCOVER",
          		"deathCoverType": $scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverType,
          		"memberType": $scope.inputDetails.memberType,
          	};
      	if($scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverType == 'DcFixed'){
      		deathReqObject['deathFixedAmount'] = parseInt($scope.changeCoverSummary.addnlDeathCoverDetails.deathInputTextValue)- parseInt($scope.changeCoverSummary.existingDeathAmt);
      	} else if($scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverType == 'DcUnitised'){
      		deathReqObject['deathUnits'] = parseInt($scope.changeCoverSummary.addnlDeathCoverDetails.deathInputTextValue)- parseInt($scope.changeCoverSummary.existingDeathUnits);
      	}
    	if($scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverType == 'TPDFixed'){
    		deathReqObject['tpdFixedAmount'] = parseInt($scope.changeCoverSummary.addnlTpdCoverDetails.tpdInputTextValue)- parseInt($scope.changeCoverSummary.existingTpdAmt);
      	}
      	calcDeathAmtSvc.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
      	//CalculateDeathService.calculateAmount({}, deathReqObject, function(res){
      		var deathResponse = res.data[0];
      		$scope.changeCoverSummary.deathLoadingCost = deathResponse.cost;
      		$scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium = parseFloat($scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium) +
      														((parseFloat($scope.auraDetails.deathLoading)/100) * parseFloat($scope.changeCoverSummary.deathLoadingCost));
      		$scope.changeCoverSummary.totalPremium = $scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium + $scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium + $scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium;
      		$scope.changeCoverSummary.addnlDeathCoverDetails['deathLoadingCost']=$scope.changeCoverSummary.deathLoadingCost;
      		$scope.changeCoverSummary.deathLoadingCost=((parseFloat($scope.auraDetails.deathLoading)/100) * parseFloat($scope.changeCoverSummary.deathLoadingCost));
      		
      		$scope.changeCoverSummary.addnlDeathCoverDetails.deathLoadingCost = Math.round(($scope.changeCoverSummary.deathLoadingCost + 0.00001)*100)/100;
      		  
      	}, function(err){
      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
      	});
      }

  	// TPD loading calculation
  	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.tpdLoading && $scope.auraDetails.tpdLoading > 0){
      	var tpdReqObject = {
          		"age": $scope.changeCoverSummary.age,
          		"fundCode": "AEIS",
          		"gender": $scope.changeCoverSummary.occupationDetails.gender,
          		"tpdOccCategory": $scope.changeCoverSummary.tpdOccCategory,
          		"smoker": smokerCustom,
          		"tpdUnitsCost": null,
          		"premiumFrequency": "Monthly",
          		"manageType": "CCOVER",
          		"tpdCoverType": $scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverType,
          		"memberType": $scope.inputDetails.memberType,
          	};
      	if($scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverType == 'TPDFixed'){
      		tpdReqObject['tpdFixedAmount'] = parseInt($scope.changeCoverSummary.addnlTpdCoverDetails.tpdInputTextValue)- parseInt($scope.changeCoverSummary.existingTpdAmt);
      	} else if($scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverType == 'TPDUnitised'){
      		tpdReqObject['tpdUnits'] = parseInt($scope.changeCoverSummary.addnlTpdCoverDetails.tpdInputTextValue)- parseInt($scope.changeCoverSummary.existingTPDUnits);
      	}

      	calcTPDAmtSvc.calculateAmount($scope.urlList.calculateTpdUrl,tpdReqObject).then(function(res){
      	//CalculateTPDService.calculateAmount({}, tpdReqObject, function(res){
      		var tpdResponse = res.data[0];
      		$scope.changeCoverSummary.tpdLoadingCost = tpdResponse.cost;
      		$scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium = parseFloat($scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium) +
      														((parseFloat($scope.auraDetails.tpdLoading)/100) * parseFloat($scope.changeCoverSummary.tpdLoadingCost));
      		$scope.changeCoverSummary.totalPremium = parseFloat($scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium)+parseFloat($scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium);
      		$scope.changeCoverSummary.addnlTpdCoverDetails['tpdLoadingCost']=$scope.changeCoverSummary.tpdLoadingCost;
      		$scope.changeCoverSummary.tpdLoadingCost =((parseFloat($scope.auraDetails.tpdLoading)/100) * parseFloat($scope.changeCoverSummary.tpdLoadingCost));
      		
      		$scope.changeCoverSummary.addnlTpdCoverDetails.tpdLoadingCost = Math.round(($scope.changeCoverSummary.tpdLoadingCost + 0.00001)*100)/100;
    		  
      	}, function(err){
      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
      	});
      }

  	// IP loading calculation
  	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.ipLoading && $scope.auraDetails.ipLoading > 0){
      	var ipReqObject = {
      		"age": $scope.changeCoverSummary.age,
      		"fundCode": "AEIS",
      		"gender": $scope.changeCoverSummary.occupationDetails.gender,
      		"ipOccCategory": $scope.changeCoverSummary.ipOccCategory,
      		"smoker": smokerCustom,
      		"memberType": $scope.inputDetails.memberType,
      		"ipUnitsCost": null,
      		"premiumFrequency": "Monthly",
      		"manageType": "CCOVER",
      		"ipCoverType": "IpFixed",
      		"ipWaitingPeriod": $scope.changeCoverSummary.addnlIpCoverDetails.waitingPeriod,
      		"ipBenefitPeriod": $scope.changeCoverSummary.addnlIpCoverDetails.benefitPeriod,
      		/*"ipUnits":parseInt($scope.addnlIpCoverDetails.ipInputTextValue)- parseInt($scope.changeCoverDetails.existingIPUnits),*/
      		"ipFixedAmount":parseInt($scope.changeCoverSummary.addnlIpCoverDetails.ipInputTextValue)- parseInt($scope.changeCoverSummary.existingIPAmount)
      	};

      	calcIPAmtSvc.calculateAmount($scope.urlList.calculateIpUrl,ipReqObject).then(function(res){
      	//CalculateIPService.calculateAmount({}, ipReqObject, function(res){
      		var ipResponse = res.data[0];
      		$scope.changeCoverSummary.ipLoadingCost = ipResponse.cost;
      		$scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium = parseFloat($scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium) +
      														((parseFloat($scope.auraDetails.ipLoading)/100) * parseFloat($scope.changeCoverSummary.ipLoadingCost));
      		$scope.changeCoverSummary.totalPremium = parseFloat($scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium)+parseFloat($scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium);
      		$scope.changeCoverSummary.addnlIpCoverDetails['ipLoadingCost']=$scope.changeCoverSummary.ipLoadingCost;
      		$scope.changeCoverSummary.ipLoadingCost=((parseFloat($scope.auraDetails.ipLoading)/100) * parseFloat($scope.changeCoverSummary.ipLoadingCost));
      		
      		$scope.changeCoverSummary.addnlIpCoverDetails.ipLoadingCost = Math.round(($scope.changeCoverSummary.ipLoadingCost + 0.00001)*100)/100;
      		
      	}, function(err){
      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
      	});
      }
  	
	  	if($scope.changeCoverSummary.occupationDetails.smoker == undefined){
	  		if($scope.changeCoverSummary.occupationDetails.smokerQuestion == "1"){
	  		   	   $scope.changeCoverSummary.occupationDetails.smoker = "Yes";
		     }else{
		    	 $scope.changeCoverSummary.occupationDetails.smoker = "No";
		     }
	  	}
	  	
	 // Exclusion, remove </br> tag from exclusions
	  		  		  	 
	  	 if($scope.changeCoverSummary.auraDetails.deathExclusions != null)
	  		{
	  		var textArray = $scope.changeCoverSummary.auraDetails.deathExclusions.split('<br><br>');
	  		$scope.changeCoverSummary.auraDetails.deathExclusionsEdit = textArray[0]+"<p>"+textArray[1]+"</p>";
	        }
	  	 if($scope.changeCoverSummary.auraDetails.tpdExclusions != null)
	  	   {
	  		var textArray = $scope.changeCoverSummary.auraDetails.tpdExclusions.split('<br><br>');
	  		$scope.changeCoverSummary.auraDetails.tpdExclusionsEdit = textArray[0]+"<p>"+textArray[1]+"</p>";
	  	   }
	  	 if($scope.changeCoverSummary.auraDetails.ipExclusions != null)
	  	  {
	  		var textArray = $scope.changeCoverSummary.auraDetails.ipExclusions.split('<br><br>');
	  		$scope.changeCoverSummary.auraDetails.ipExclusionsEdit= textArray[0]+"<p>"+textArray[1]+"</p>";
	  	  }
  }

  $scope.init();
  // .then(function() {
  //  console.log('all done');
  //  angular.extend($scope.changeCoverSummary, appData.getChngCoverSummaryData());
  // }, function() {
  //
  // });
  $scope.go = function (path) {
	  $scope.resetPremiumValue();
	  $location.path(path);
  };
  
  $scope.resetPremiumValue = function(){
	  $scope.changeCoverSummary.addnlDeathCoverDetails.deathCoverPremium = $scope.resetValue.deathCoverPremium;
	  $scope.changeCoverSummary.addnlTpdCoverDetails.tpdCoverPremium = $scope.resetValue.tpdCoverPremium;
	  $scope.changeCoverSummary.addnlIpCoverDetails.ipCoverPremium = $scope.resetValue.ipCoverPremium;
  };

  $scope.navigateToLandingPage = function () {
    ngDialog.openConfirm({
        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
      }).then(function() {
        $location.path("/landing");
      }, function(e){
        if(e=='oncancel') {
          return false;
        }
      });
   };

   $scope.summarySaveAndExitPopUp = function (hhText) {
     var dialog1 = ngDialog.open({
           template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
         className: 'ngdialog-theme-plain custom-width',
         preCloseCallback: function(value) {
                var url = "/landing"
                $location.path( url );
                return true
         },
         plain: true
     });
   };

   $scope.saveSummary = function() {
	   $scope.resetPremiumValue();
     $scope.changeCoverSummary.lastSavedOn = 'SummaryPage';
     
     if($scope.changeCoverSummary.occupationDetails.smoker == 'Yes') {
       	$scope.changeCoverSummary.occupationDetails.smokerQuestion = 1;
   	}else{
   		$scope.changeCoverSummary.occupationDetails.smokerQuestion = 2;
   	}
     var saveSummaryObject = angular.copy($scope.changeCoverSummary);
     saveSummaryObject = angular.extend(saveSummaryObject, $scope.inputDetails);   
     
     if($scope.auraDetails != null){
       saveSummaryObject = angular.extend(saveSummaryObject, $scope.auraDetails);
     }
     
     saveSummaryObject.eventName =  $scope.changeCoverSummary.eventName;
     
     saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, saveSummaryObject).then(function(response) {
       console.log(response.data);
       $scope.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ $scope.changeCoverSummary.appNum +'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
     });
   };
   var ackCheckCCGC;
   var ackCheckLE;
   $scope.navigateToDecision = function() {
     ackCheckCCGC = $('#generalConsentLabel').hasClass('active');

     if($scope.auraDetails != null){
      if($scope.auraDetails.specialTerm != null && $scope.auraDetails.specialTerm == true){
       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');
        }
     }

     if(ackCheckCCGC && (($scope.auraDetails != null && !$scope.auraDetails.specialTerm) || ($scope.auraDetails != null && $scope.auraDetails.specialTerm && ackCheckLE) || $scope.auraDetails==null)){
       $scope.changeCoverSummary.LEFlag = false;
       $scope.CCGCackFlag = false;
       $rootScope.$broadcast('disablepointer');
       
        var tempobj = {};
       angular.forEach($scope.eventList, function(event, key) {
    	   if($scope.changeCoverSummary.eventName == event.cde){
    		   tempobj = event;
    	   }
       });
       $scope.changeCoverSummary.eventDesc =  tempobj.desc;  
       $scope.changeCoverSummary.smoker =$scope.changeCoverSummary.occupationDetails.smoker;
       $scope.changeCoverSummary.lastSavedOn = '';
       if($scope.changeCoverSummary.auraDisabled == true){
    	   $scope.changeCoverSummary.deathExclusions = null;
    	   $scope.changeCoverSummary.ipExclusions = null;
    	   $scope.changeCoverSummary.tpdExclusions = null;
    	   $scope.auraDetails.deathExclusions = null;
    	   $scope.auraDetails.ipExclusions = null;
    	   $scope.auraDetails.tpdExclusions = null;
    	   $scope.changeCoverSummary.addnlDeathCoverDetails.deathLoadingCost = 0.0;
    	   $scope.changeCoverSummary.addnlIpCoverDetails.ipLoadingCost = 0.0;
    	   $scope.changeCoverSummary.addnlTpdCoverDetails.tpdLoadingCost = 0.0;
       }
       var submitSummaryObject = angular.extend($scope.changeCoverSummary, $scope.inputDetails);
       if($scope.auraDetails != null){
         submitSummaryObject = angular.extend(submitSummaryObject, $scope.auraDetails);
       }
       auraRespSvc.setResponse(submitSummaryObject);
       submitEapplySvc.submitObj($scope.urlList.submitEapplyUrl, submitSummaryObject).then(function(response) {
         if(response.data) {
             PersistenceService.setPDFLocation(response.data.clientPDFLocation);
             PersistenceService.setNpsUrl(response.data.npsTokenURL);
             if($scope.auraDetails.overallDecision!=null && $scope.auraDetails.overallDecision != ""){
            	 
            	 if($scope.auraDetails.overallDecision == 'ACC'){
            		 if($scope.auraDetails.specialTerm){
            			 $location.path('/changeaspcltermsacc');
                     }else{
                       $location.path('/changeaccept');
                     }
            	}else if($scope.auraDetails.overallDecision == 'DCL'){
            		$location.path('/changedecline');
            	} else if($scope.auraDetails.overallDecision == 'RUW'){
            		$location.path('/changeunderwriting');
            	}else {
            		$location.path('/changemixedaccept');
            	}
             
             }else{
               $location.path('/changeaccept');
             }
         } else {
                 $window.scrollTo(0, 0);
                 $rootScope.$broadcast('enablepointer');
                 throw {message: 'No data found'};
             }
       }, function(err){
         $scope.errorOccured = true;
         $window.scrollTo(0, 0);
             $rootScope.$broadcast('enablepointer');
       });
     } else{
         if(ackCheckCCGC){
           $scope.CCGCackFlag = false;
         }else{
           $scope.CCGCackFlag = true;
         }
         if(ackCheckLE){
           $scope.changeCoverSummary.LEFlag = false;
         }else{
           $scope.changeCoverSummary.LEFlag = true;
         }
         $scope.scrollToUncheckedElement();
     }
   };

   $scope.scrollToUncheckedElement = function(){
     if($scope.auraDetails!=null && $scope.auraDetails.specialTerm){
       var elements = [ackCheckLE, ackCheckCCGC];
       var ids = ['lodadingExclusionLabel', 'generalConsentLabel'];
     } else{
       var elements = [ackCheckCCGC];
       var ids = ['generalConsentLabel'];
     }
   };

   $scope.checkAckStateGC = function(){
     $timeout(function(){
       ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
         if(ackCheckCCGC){
           $scope.CCGCackFlag = false;
         }else{
           $scope.CCGCackFlag = true;
         }
     }, 10);
   };

   $scope.checkAckStateLE = function(){
     $timeout(function(){
       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');

         if(ackCheckLE){
           $scope.changeCoverSummary.LEFlag = false;
         }else{
           $scope.changeCoverSummary.LEFlag = true;
         }
     }, 10);
   };
}]);
   /*Summary Page Controller Ends*/
