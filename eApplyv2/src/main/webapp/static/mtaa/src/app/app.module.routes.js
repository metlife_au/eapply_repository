(function(angular){
'use strict';
/* global memberStatus  */
angular.module('MtaaApp').config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'mtaaUIConstants', '$templateRequestProvider',function ($stateProvider, $urlRouterProvider, $httpProvider, mtaaUIConstants, $templateRequestProvider) {
	   $httpProvider.defaults.withCredentials = true;
		/*$urlRouterProvider.otherwise('/login');*/
	   $httpProvider.interceptors.push('eapplyrouter');
	   $templateRequestProvider.httpOptions({_isTemplate: true});
	   $urlRouterProvider.otherwise('/landingpage');
	   $stateProvider
	     .state('landing',{
	       url: '/landingpage',
	       controller: 'landingCtrl',
	       controllerAs: 'vm',
	       templateUrl: mtaaUIConstants.path.modules + 'landing/landingpage.html',
	       title: 'MTAA Super-Home',
	       reloadOnSearch: false,
 	       resolve: {
 	        urls:function(fetchUrlSvc){
 	          var path = 'CareSuperUrls.properties';
 	          return fetchUrlSvc.getUrls(path);
 	        }
 	       }
	     })
	     .state('cover',{
	       url: '/cover/:mode',
		   controller: 'coverCtrl',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/cover.html',
		   title: 'MTAA Super-Change Cover'
	     })
	     .state('aura',{
	       url: '/aura',
		   controller: 'auraCtrl',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/aura.html',
		   title: 'MTAA Super-Change Aura'
	     })
	     .state('summary',{
	       url: '/summary',
		   controller: 'summaryCtrl',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/summary.html'
	     })
	     .state('changeaccept',{
	       url: '/changeaccept',
		   controller: 'changeCoverAcceptController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/changeCover_decision_accepted.html',
		   title: 'MTAA Super-Change Accept'
	     })
	     .state('changedecline',{
	       url: '/changedecline',
		   controller: 'changeCoverDeclineController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/changeCover_decision_decline.html',
		   title: 'MTAA Super-Change Decline'
	     })
	     .state('changeunderwriting',{
	       url: '/changeunderwriting',
		   controller: 'changeCoverRUWController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/changeCover_decision_ruw.html',
		   title: 'MTAA Super-Change RUW'
	     })
	     .state('changeaspcltermsacc',{
	       url: '/changeaspcltermsacc',
		   controller: 'changeCoverACCSpclTermsController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'quote/changeCover_decision_AcceptWithSpclTerms.html',
		   title: 'MTAA Super-Change Accept with special terms'
	     })
	     .state('quotetransfer',{
		   url: '/quotetransfer/:mode',
		   controller: 'quotetransfer',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'transfer/cover.html',
		   title: 'MTAA Super-Transfer Cover'
		 })
		 .state('auratransfer',{
		   url: '/auratransfer/:mode',
		   controller: 'auratransfer',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'transfer/aura.html',
		   title: 'MTAA Super-Transfer Aura'
		 })
		 .state('transferSummary',{
		   url: '/transferSummary/:mode',
		   controller: 'transferSummary',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'transfer/summary.html',
		   title: 'MTAA Super-Transfer Summary'
		 })
		 .state('transferDecision',{
		   url: '/transferDecision/:mode',
		   controller: 'transferDecision',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'transfer/decision.html',
		   title: 'MTAA Super-Transfer Decision'
		 })
		 .state('quoteConvert',{
		   url: '/quoteConvert/:mode',
		   controller: 'quoteConvert',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'convert/cover.html',
		   title: 'MTAA Super-Convert and Maintain Cover'
		 })
		 .state('summaryConvert',{
		   url: '/summaryConvert/:mode',
		   controller: 'summaryConvert',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'convert/summary.html',
		   title: 'MTAA Super-Convert and Maintain Summary'
		 })
		 .state('convertDecision',{
		   url: '/convertDecision/:mode',
		   controller: 'convertDecision',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'convert/decision.html',
		   title: 'MTAA Super-Convert and Maintain Decision'
		 })
		  .state('quoteLife',{
		   url: '/quoteLife/:mode',
		   controller: 'quoteLife',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'life/cover.html',
		   title: 'MTAA Super-Life Cover'
		 })
		 .state('summaryLife',{
		   url: '/summaryLife/:mode',
		   controller: 'summaryLife',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'life/summary.html',
		   title: 'MTAA Super-Life Summary'
		 })
		 .state('lifeDecision',{
		   url: '/lifeDecision/:mode',
		   controller: 'lifeDecision',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'life/decision.html',
		   title: 'MTAA Super-Life Decision'
		 })
		 .state('quoteCancel',{
		   url: '/quoteCancel/:mode',
		   controller: 'quoteCancel',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'cancel/cover.html',
		   title: 'MTAA Super-Cancel Cover'
		 })
		 .state('cancelSummary',{
		   url: '/cancelSummary',
		   controller: 'cancelSummary',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'cancel/summary.html',
		   title: 'MTAA Super-Cancel Summary'
		 })
		 .state('cancelDecision',{
		   url: '/cancelDecision',
		   controller: 'cancelDecision',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'cancel/decision.html',
		   title: 'MTAA Super-Cancel Decision'
		 })
		 .state('/retrievesavedapplication', {
			 url: '/retrievesavedapplication',
			 controller  : 'retrievesavedapplication',
			 controllerAs: 'vm',
			 templateUrl: mtaaUIConstants.path.modules + 'retrievesavedapplication/retrieve.html',
			 title: 'MTAA Super-Saved Aapplication'
	    })
	    .state('/retrievesavedapplicationnonmem', {
			 url: '/retrievesavedapplicationNonMem/:mode',
			 controller  : 'retrievesavedapplication',
			 controllerAs: 'vm',
			 templateUrl: mtaaUIConstants.path.modules + 'retrievesavedapplication/retrieve.html',
			 title: 'MTAA Super-Saved Aapplication'
	    })
	    .state('sessionTimeOut', {
	    	url:'/sessionTimeOut',
	    	templateUrl : mtaaUIConstants.path.modules + 'timeout/timeout.html',
	    	controller  : 'timeOutController',
	    	controllerAs: 'vm',
	    	title: 'MTAA Super-Session Expired'
	    })
	    .state('quoteOccChange',{
		   url: '/quoteoccchange/:mode',
		   controller: 'quoteoccupdate',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'occupation/cover.html',
		   title: 'MTAA Super-Occupation Cover'
		 })
		 .state('auraOcc',{
		   url: '/auraocc/:mode',
		   controller: 'auraocc',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'occupation/aura.html',
		   title: 'MTAA Super-Occupation Aura'
		 })
		 .state('workRatingSummary',{
		   url: '/workRatingSummary/:mode',
		   controller: 'occUpgradeSummary',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'occupation/summary.html',
		   title: 'MTAA Super-Occupation Summary'
		 })
		 .state('workRatingAccept',{
		   url: '/workRatingAccept',
		   controller: 'workRatingAcceptController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'occupation/occupationSummary_accepted.html',
		   title: 'MTAA Super-Occupation Accept'
		 })
		 .state('workRatingDecline',{
		   url: '/workRatingDecline',
		   controller: 'workRatingDeclineController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'occupation/occupationSummary_declined.html',
		   title: 'MTAA Super-Occupation Declined'
		 })
		 .state('workRatingMaintain',{
		   url: '/workRatingMaintain',
		   controller: 'workRatingMaintainController',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'occupation/occupationSummary_maintained.html',
		   title: 'MTAA Super-Occupation Maintained'
		 });
		 /*.state('nonmember',{
		   url: '/nonmember/:mode',
		   controller: 'nonmemberCoverDetails',
		   controllerAs: 'vm',
		   title: 'MTAA Super-Non Member Change Cover',
		   templateUrl: mtaaUIConstants.path.modules + 'nonmember/cover.html',
	       	resolve: {
				urls:function(fetchUrlSvc){
					var path = './CareSuperUrls.properties';
					return fetchUrlSvc.getUrls(path);
				} 
	       	}
		 })
		 .state('auraNonMember',{
		   url: '/auraNonMember/:mode',
		   controller: 'auraNonMember',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'nonmember/aura.html',
	       	resolve: {
				urls:function(fetchUrlSvc){
					var path = './CareSuperUrls.properties';
					return fetchUrlSvc.getUrls(path);
				} 
	       	}
		 })
		 .state('nonMemberSummary',{
		   url: '/nonMemberSummary/:mode',
		   controller: 'nonMemberSummary',
		   controllerAs: 'vm',
		   templateUrl: mtaaUIConstants.path.modules + 'nonmember/summary.html',
	       	resolve: {
				urls:function(fetchUrlSvc){
					var path = './CareSuperUrls.properties';
					return fetchUrlSvc.getUrls(path);
				} 
	       	}
		 })
		 .state('nonMemberDecisionAccept',{
			   url: '/nonMemberaccept',
			   controller: 'nonMemberAcceptController',
			   controllerAs: 'vm',
			   templateUrl: mtaaUIConstants.path.modules + 'nonmember/nonMember_decision_accepted.html',
		       	resolve: {
					urls:function(fetchUrlSvc){
						var path = './CareSuperUrls.properties';
						return fetchUrlSvc.getUrls(path);
					} 
		       	}
			 })
			 .state('nonMemberDecisionDecline',{
			   url: '/nonMemberdecline',
			   controller: 'nonMemberDeclineController',
			   controllerAs: 'vm',
			   templateUrl: mtaaUIConstants.path.modules + 'nonmember/nonMember_decision_decline.html',
		       	resolve: {
					urls:function(fetchUrlSvc){
						var path = './CareSuperUrls.properties';
						return fetchUrlSvc.getUrls(path);
					} 
		       	}
			 })
			 .state('nonMemberDecisionRUW',{
				   url: '/nonMemberunderwriting',
				   controller: 'nonMemberRUWController',
				   controllerAs: 'vm',
				   templateUrl: mtaaUIConstants.path.modules + 'nonmember/nonMember_decision_ruw.html',
			       	resolve: {
						urls:function(fetchUrlSvc){
							var path = './CareSuperUrls.properties';
							return fetchUrlSvc.getUrls(path);
						} 
			       	}
				 })
				 .state('nonMemberDecisionSpclTerm',{
					   url: '/nonMemberaspcltermsacc',
					   controller: 'nonMemberACCSpclTermsController',
					   controllerAs: 'vm',
					   templateUrl: mtaaUIConstants.path.modules + 'nonmember/nonMember_decision_AcceptWithSpclTerms.html',
				       	resolve: {
							urls:function(fetchUrlSvc){
								var path = './CareSuperUrls.properties';
								return fetchUrlSvc.getUrls(path);
							} 
				       	}
					 });*/
	  
	}]);

/*angular.module('MtaaApp').run( function($location) {
	if( memberStatus === 'newApp') {
		$location.path('/nonmember/1');
		
	}
	if( memberStatus === 'savedApp') {
		$location.path('/retrievesavedapplicationNonMem/3');
		
	}
});*/

angular.module('MtaaApp').run(function($rootScope, $location, $state, $timeout){
	$rootScope.$on('$locationChangeStart', function(currentRoute) {
		$timeout(function(){
			$rootScope.title = $state.current.title;
			if(!$rootScope.title){
				$rootScope.title = 'MTAA Super-Home';
			}
			//console.log($rootScope.title);
		},200);				
	});
});

})(angular);