package au.com.metlife.eapply.bs.serviceimpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.bs.exception.BusinessExceptionConstants;
import au.com.metlife.eapply.bs.exception.MetLifeBSRuntimeException;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.ApplicantRepository;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplicationRepository;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.bs.utility.EapplyHelper;

@Service
public class EapplyServiceImpl implements EapplyService {

	private final EapplicationRepository repository;
	
	private final ApplicantRepository applicantRepository;

	@Autowired
    public EapplyServiceImpl(final EapplicationRepository repository, final ApplicantRepository applicantRepository) {
        this.repository = repository;
        this.applicantRepository = applicantRepository;
    }
	 
	
	public EapplicationRepository getRepository() {
		return repository;
	}
	public ApplicantRepository getApplicantRepository() {
		return applicantRepository;
	}

	@Override
	@Transactional
	public Eapplication submitEapplication(Eapplication eapplication) throws Exception{
		if(eapplication!=null){
			if(eapplication.getApplicationumber()!=null){
				List<Eapplication> eapplicationList=repository.findByApplicationumber(eapplication.getApplicationumber());
				if(eapplicationList!=null && eapplicationList.size()>1){
					throw new MetLifeBSRuntimeException(BusinessExceptionConstants.ERR_CDE_5100_DUPLICATE_APPNUM, BusinessExceptionConstants.ERR_CDE_5100_MSG);
				}else if(eapplicationList!=null && eapplicationList.size()==1){
					/*Save and retrieve flow*/
					Eapplication exEapplication=eapplicationList.get(0);
					eapplication.setId(exEapplication.getId());
					//exEapplication.getAuradetail()
					
					if(eapplication.getApplicant()!=null && eapplication.getApplicant().size()==1){
						Applicant exApplicant=exEapplication.getApplicant().get(0);
						eapplication.getApplicant().get(0).setId(exApplicant.getId());
						eapplication.getApplicant().get(0).setEapplicationid(exEapplication.getId());
						
						if(exApplicant.getContactinfo()!=null){
							if(eapplication.getApplicant().get(0).getContactinfo()!=null){
								eapplication.getApplicant().get(0).getContactinfo().setId(exApplicant.getContactinfo().getId());
							}
						}
						if(exApplicant.getCovers()!=null && exApplicant.getCovers().size()>0){
							for(Cover exCovers: exApplicant.getCovers()){
								if(exCovers!=null && exCovers.getCovercode().equalsIgnoreCase("DEATH")){
									if(eapplication.getApplicant().get(0).getCovers()!=null && eapplication.getApplicant().get(0).getCovers().size()>0){
										for(Cover newCovers: eapplication.getApplicant().get(0).getCovers()){
											if(newCovers!=null && newCovers.getCovercode().equalsIgnoreCase("DEATH")){
												newCovers.setApplicantid(exApplicant.getId());
												newCovers.setId(exCovers.getId());
												break;
											}
										}
									}
								}else if(exCovers!=null && exCovers.getCovercode().equalsIgnoreCase("TPD")){
									if(eapplication.getApplicant().get(0).getCovers()!=null && eapplication.getApplicant().get(0).getCovers().size()>0){
										for(Cover newCovers: eapplication.getApplicant().get(0).getCovers()){
											if(newCovers!=null && newCovers.getCovercode().equalsIgnoreCase("TPD")){
												newCovers.setApplicantid(exApplicant.getId());
												newCovers.setId(exCovers.getId());
												break;
											}
										}
									}
								}else if(exCovers!=null && exCovers.getCovercode().equalsIgnoreCase("IP")){
									if(eapplication.getApplicant().get(0).getCovers()!=null && eapplication.getApplicant().get(0).getCovers().size()>0){
										for(Cover newCovers: eapplication.getApplicant().get(0).getCovers()){
											if(newCovers!=null && newCovers.getCovercode().equalsIgnoreCase("IP")){
												newCovers.setApplicantid(exApplicant.getId());
												newCovers.setId(exCovers.getId());
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			try {
				repository.save(eapplication);
			} catch (Exception e) {
				e.printStackTrace();
				throw new MetLifeBSRuntimeException(BusinessExceptionConstants.ERR_CDE_5500_DB_DATA_ERROR, BusinessExceptionConstants.ERR_CDE_5500_MSG);
			}
		}
		return eapplication;
	}
	@Override
	@Transactional
	public Boolean clientMatch(String firstName, String lastName, String dob) {
		Boolean clientMatch = Boolean.FALSE;
		Applicant applicant = new Applicant();
		applicant.setFirstname(firstName);
		//applicant.setLastname(lastName);		
		applicant.setBirthdate(new Timestamp(new Date(dob).getTime()));
		
		List<Applicant> applicantList =  applicantRepository.findByFNamesAndDob(firstName, new Timestamp(new Date(dob).getTime()));
		if(applicantList!=null && applicantList.size()>0){
			clientMatch = Boolean.TRUE;
		}else{
			applicantList =  applicantRepository.findByLNamesAndDob(lastName, new Timestamp(new Date(dob).getTime()));
			if(applicantList!=null && applicantList.size()>0){
				clientMatch = Boolean.TRUE;
			}
		}
		/*Example<Applicant> example = Example.of(applicant);
		List<Applicant> applicantList = applicantRepository.findAll(example);
		if(applicantList!=null && applicantList.size()>0){
			clientMatch = Boolean.TRUE;
		}else{
			applicant = new Applicant();
			applicant.setLastname(lastName);
			applicant.setBirthdate(new Timestamp(new Date(dob).getTime()));
			example = Example.of(applicant);
			applicantList = applicantRepository.findAll(example);
			if(applicantList!=null && applicantList.size()>0){
				clientMatch = Boolean.TRUE;
			}
		}*/
		return clientMatch;
	}


	@Override
	public Eapplication retrieveApplication(String applicationNumber) {
		List<Eapplication> eapplicationList=null;
		try {
			if(applicationNumber!=null){
				eapplicationList = repository.findByApplicationumber(applicationNumber);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(eapplicationList!=null && eapplicationList.size()>0){
			return eapplicationList.get(0);
		}else{
			return null;
		}
	}


	@Override
	public List<Eapplication> retrieveSavedApplications(String fundCode, String clientRefNo,String manageType) {
		List<Eapplication> eapplicationList=null;
		try {
			if(manageType!=null && clientRefNo!=null && fundCode!=null && manageType!=null){
				List<Applicant> applicantList =  applicantRepository.findByClientRefNo(clientRefNo);
				eapplicationList=new ArrayList<Eapplication>();
				for(Applicant applicant:applicantList){
					Eapplication eapplicationTmp=repository.findByIdAndApplicationstatusAndPartnercodeAndRequesttype(new Long(applicant.getEapplicationid()), "Pending", fundCode,manageType);
					/*Ignore if applicaton is expired , no need to check while clicking on landing page tile*/
					if(!EapplyHelper.determineSavedAppliactionValidatity(eapplicationTmp, fundCode)){
						eapplicationList.add(eapplicationTmp);
					}
				}
			}else if(clientRefNo!=null && fundCode!=null){
				List<Applicant> applicantList =  applicantRepository.findByClientRefNo(clientRefNo);
				eapplicationList=new ArrayList<Eapplication>();
				for(Applicant applicant:applicantList){
					Eapplication eapplicationTmp=repository.findByIdAndPartnercode(new Long(applicant.getEapplicationid()),fundCode);
					if(EapplyHelper.determineSavedAppliactionValidatity(eapplicationTmp, fundCode)){
						if(eapplicationTmp.getApplicationstatus()!=null && eapplicationTmp.getApplicationstatus().equalsIgnoreCase("Pending")){
							eapplicationTmp.setApplicationstatus("Expired");
						}
						eapplicationList.add(eapplicationTmp);
					}else{
						eapplicationList.add(eapplicationTmp);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(eapplicationList!=null && eapplicationList.size()>0){
			return eapplicationList;
		}else{
			return null;
		}
	}


	@Override
	@Transactional
	public Eapplication expireApplication(String applicationNumber) {
		List<Eapplication> eapplicationList=null;
		Eapplication eapplication=null;
		try {
			if(applicationNumber!=null){
				eapplicationList = repository.findByApplicationumber(applicationNumber);
				if(eapplicationList!=null && eapplicationList.size()>0){
					eapplication= eapplicationList.get(0);
					eapplication.setApplicationstatus("Expired");
					repository.save(eapplication);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eapplication;
	}


	@Override
	public Boolean checkAppSubmittedInLast24Hrs(String fundCode, String clientRefNo, String manageType) {
		 Boolean appsSubmittedinLast24Hrs=Boolean.FALSE;
		try {
			if(manageType!=null && clientRefNo!=null && fundCode!=null && manageType!=null){
				List<Applicant> applicantList =  applicantRepository.findByClientRefNo(clientRefNo);
				for(Applicant applicant:applicantList){
					Calendar cal = Calendar.getInstance();
					System.out.println(cal.getTime()); // 
					cal.set(Calendar.DATE, cal.get(Calendar.DATE)-1);
					Eapplication eapplicationTmp=repository.findByIdAndStatusAndLastUpdateDate(new Long(applicant.getEapplicationid()), "Submitted",cal.getTime());
					if(eapplicationTmp!=null){
						appsSubmittedinLast24Hrs=Boolean.TRUE;
					}
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return appsSubmittedinLast24Hrs;
	}


}
