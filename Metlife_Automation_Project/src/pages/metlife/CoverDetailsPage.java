/**
 * 
 */
package pages.metlife;

import java.awt.Robot;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.cognizant.framework.Status;
import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

import supportlibraries.ScriptHelper;
import uimap.metlife.CoverDetailsPageObjects;

/**
 * @author sampath
 * 
 */
public class CoverDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public CoverDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public CoverDetailsPage enterCoverDetails() {
		fillInCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	}
	
	public CoverDetailsPage enterSFPSCoverDetails() {
		fillInSFPSCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	} 
	
	public CoverDetailsPage enterNSFSCoverDetails() {
		fillInNSFSCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	} 
	
	public CoverDetailsPage enterAEISCoverDetails() {
		fillInAEISCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	}
	
	public CoverDetailsPage enterREISCoverDetails() {
		fillInREISCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	}
	
		
	public CoverDetailsPage enterGUILDCoverDetails() {
		fillInGUILDCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	}
	
	public CoverDetailsPage enterINGDCoverDetails() {
		fillInINGDCoverDetails();
		( ( JavascriptExecutor ) driver ).executeScript( "window.onbeforeunload = function(e){};" );
		return new CoverDetailsPage(scriptHelper);
	}
	
	
	private void fillInINGDCoverDetails() {
		// TODO Auto-generated method stub
		String work15hrs=dataTable.getData("MetLife_Data", "Work 15 hours and more");
		String industry=dataTable.getData("MetLife_Data", "Industry");
		String currentOccupation=dataTable.getData("MetLife_Data", "currentOccupation");
		String annualSalary=dataTable.getData("MetLife_Data", "Annual Salary");
		
		try {
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"TAB","Annual Salary");
			sleep();
			enterINGDCoverGrid();
			sleep(1000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void enterINGDCoverGrid() {
		// TODO Auto-generated method stub
		String deathSelCover=dataTable.getData("MetLife_Data", "DeathSelectCover");
		String ipSelCover=dataTable.getData("MetLife_Data", "incomeprotectionselectcover");
		String strBenefitPeriod = dataTable.getData("MetLife_Data", "change.benefitperiod");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");
		String strDeathCover = dataTable.getData("MetLife_Data", "DeathCover");
		String strTPDCover = dataTable.getData("MetLife_Data", "TPDCover");
		String strIPCover = dataTable.getData("MetLife_Data", "change.ipaddlcover");
		try {
			sleep();
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selDeathselectcovertype), "DropDownSelect", "Death Select Cover",deathSelCover);
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selDeathselectcovertype), "TAB", "Death Select Cover");
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selIncomeprotectionselectcovertype), "DropDownSelect", "Income Protection Select Cover",ipSelCover);
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selIncomeprotectionselectcovertype), "TAB", "Income Protection Select Cover");
			sleep(1000);
			if(!deathSelCover.equalsIgnoreCase("Life Stage Cover")){
			tryAction(fluentWaitElement(CoverDetailsPageObjects.txtDeathunits), "SET", "Death Cover",strDeathCover);
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.txtTotalpermdisabilityunits), "SET", "TPD Cover",strTPDCover);
			sleep(1000);
			//tryAction(fluentWaitElements(CoverDetailsPageObjects.lstBenefitPeriod), "DropDownSelect", "Benefit Period ",strBenefitPeriod);
			//sleep();
			}
			if(ipSelCover.equalsIgnoreCase("Choose your own")){
			tryAction(fluentWaitElement(CoverDetailsPageObjects.txtIncomeprotectionunits), "SET", "IP Cover",strIPCover);
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote), "click", "Calculate Quote");
			sleep(2000);
			}
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote), "click", "Calculate Quote");
			sleep(2000);
			
			getPremiumsFromScreen();
			report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
			if(strTCID.contains("PremiumCalculation")){
			assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
			assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
			assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
			}
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply), "clkradio", "Apply Button");
			sleep();
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInGUILDCoverDetails() {
		// TODO Auto-generated method stub
		String work15hrs=dataTable.getData("MetLife_Data", "change.work15hours");
		String industry=dataTable.getData("MetLife_Data", "Industry");
		String currentOccupation=dataTable.getData("MetLife_Data", "current occupation");
		String blnWorkDuties=dataTable.getData("MetLife_Data", "workDuties");
		String annualSalary=dataTable.getData("MetLife_Data", "change.salary");
		String empStatus = dataTable.getData("MetLife_Data", "cover.empStatus");
		String insuranceCover=dataTable.getData("MetLife_Data", "Insurance Cover");
		String payfreq=dataTable.getData("MetLife_Data", "change.premiumFreq");
		
		try {
			if(empStatus.equalsIgnoreCase("Employed"))
			{
				tryAction(fluentWaitElement(CoverDetailsPageObjects.radEmpStatusEmployed), "click", "What is your employment status? - "+empStatus);
			}
			else if(empStatus.equalsIgnoreCase("Self-employed")){
				tryAction(fluentWaitElement(CoverDetailsPageObjects.radEmpStatusSelfEmployed), "click", "What is your employment status? - "+empStatus);
			}
			else if(empStatus.equalsIgnoreCase("Unemployed")){
				tryAction(fluentWaitElement(CoverDetailsPageObjects.radEmpStatusUnEmployed), "click", "What is your employment status? - "+empStatus);
			}else{
				tryAction(fluentWaitElement(CoverDetailsPageObjects.radEmpStatusEmployed), "click", "What is your employment status? - "+empStatus);
			}
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			tryAction(fluentWaitElements(blnWorkDuties.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWorkDutiesYes:CoverDetailsPageObjects.radWorkDutiesNo), "click", "Do you hold a tertiary qualification relevant to your current occupation or are you a member of a professional body relevant to your current occupation or a member of your organisation's senior management team? -"+blnWorkDuties);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"TAB","Annual Salary");
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstPremiumFrequency), "DropDownSelect", "Express my cost of insurance cover as a ",payfreq);
			//enterSFPSCoverGrid();
			enterGUILDCoverGrid();
			sleep(2000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInREISCoverDetails() {
		// TODO Auto-generated method stub
		String work15hrs=dataTable.getData("MetLife_Data", "change.work15hours");
		String industry=dataTable.getData("MetLife_Data", "change.industry");
		String currentOccupation=dataTable.getData("MetLife_Data", "change.occupation");
		String annualSalary=dataTable.getData("MetLife_Data", "change.salary");
		String insuranceCover=dataTable.getData("MetLife_Data", "change.premiumFreq");
		String permEmployed=dataTable.getData("MetLife_Data", "Permanently Employed");
		
		try {
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(2000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "TAB", "What is your current occupation?",currentOccupation);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"TAB","Annual Salary");
			
			tryAction(fluentWaitElements(permEmployed.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radPermEmpYes:CoverDetailsPageObjects.radPermEmpNo), "click", "Are you permanently employed? -"+permEmployed);
			sleep();
			
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstPremiumFrequency), "DropDownSelect", "Express my cost of insurance cover as a ",insuranceCover);
			enterREISCoverGrid();
			sleep(2000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
		
	private void fillInAEISCoverDetails() {
		// TODO Auto-generated method stub
		String smoker=dataTable.getData("MetLife_Data", "Smoker");
		String work15hrs=dataTable.getData("MetLife_Data", "Work 15 hours and more");
		String industry=dataTable.getData("MetLife_Data", "Industry");
		String currentOccupation=dataTable.getData("MetLife_Data", "current occupation");
		String officeEnvironment=dataTable.getData("MetLife_Data", "Office Environment");
		String blnWorkDuties=dataTable.getData("MetLife_Data", "workDuties");
		String annualSalary=dataTable.getData("MetLife_Data", "Annual Salary");
		String insuranceCover=dataTable.getData("MetLife_Data", "Insurance Cover");
		
		try {
			tryAction(fluentWaitElement(smoker.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radSmokerYes:CoverDetailsPageObjects.radSmokerNo), "click", "Have you smoked in the past 12 months? - "+smoker);
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			tryAction(fluentWaitElements(officeEnvironment.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radOfficeEnvironmentYes:CoverDetailsPageObjects.radOfficeEnvironmentNo), "click", "Do you work wholly within an office environment? -"+officeEnvironment);
			tryAction(fluentWaitElements(blnWorkDuties.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWorkDutiesYes:CoverDetailsPageObjects.radWorkDutiesNo), "click", "Do you hold a tertiary qualification relevant to your current occupation or are you a member of a professional body relevant to your current occupation or a member of your organisation's senior management team? -"+blnWorkDuties);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"TAB","Annual Salary");
			sleep();
			enterAEISCoverGrid();
			sleep(2000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInNSFSCoverDetails() {
		// TODO Auto-generated method stub
		String work15hrs=dataTable.getData("MetLife_Data", "Work 15 hours and more");
		String industry=dataTable.getData("MetLife_Data", "Industry");
		String currentOccupation=dataTable.getData("MetLife_Data", "current occupation");
		String officeEnvironment=dataTable.getData("MetLife_Data", "Office Environment");
		String blnWorkDuties=dataTable.getData("MetLife_Data", "workDuties");
		String annualSalary=dataTable.getData("MetLife_Data", "Annual Salary");
		//quickSwitchWindows();
		try {
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			tryAction(fluentWaitElements(officeEnvironment.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radOfficeEnvironmentYes:CoverDetailsPageObjects.radOfficeEnvironmentNo), "click", "Do you work wholly within an office environment? -"+officeEnvironment);
			tryAction(fluentWaitElements(blnWorkDuties.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWorkDutiesYes:CoverDetailsPageObjects.radWorkDutiesNo), "click", "Do you hold a tertiary qualification relevant to your current occupation or are you a member of a professional body relevant to your current occupation or a member of your organisation's senior management team? -"+blnWorkDuties);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"TAB","Annual Salary");
			sleep();
			enterNSFSCoverGrid();
			sleep();
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void enterNSFSCoverGrid() {
		// TODO Auto-generated method stub
		String strUnitsTotalDeathCover = dataTable.getData("MetLife_Data", "unitsTotalDeathCover");
		String strUnitsTotalPermanentDisability = dataTable.getData("MetLife_Data", "unitsTotalPermanentDisability");
		String strSalInsure = dataTable.getData("MetLife_Data", "salaryInsure");
		String strIpAddlCover = dataTable.getData("MetLife_Data", "change.ipaddlcover");
		String strWaitingPeriod = dataTable.getData("MetLife_Data", "change.waitngperiod");
		String strBenefitPeriod = dataTable.getData("MetLife_Data", "change.benefitperiod");
		String strMemberType = dataTable.getData("MetLife_Data", "applicant2_memberType");
		String strTotalDeathCover = dataTable.getData("MetLife_Data", "radioTotalDeathCover");
		String strTPDCover = dataTable.getData("MetLife_Data", "radioTotalPermanentDisability");
		
		try {
			if(strMemberType.equalsIgnoreCase("Employer Division")){
				tryAction(waitForClickableElement(strTotalDeathCover.equalsIgnoreCase("Unitised")?CoverDetailsPageObjects.radTotalDeathCoverUnitised:CoverDetailsPageObjects.radTotalDeathCoverFixed), "click", "Total Death Cover Required Selected is Unitised -"+strTotalDeathCover);
				sleep();
				tryAction(waitForClickableElement(strTPDCover.equalsIgnoreCase("Unitised")?CoverDetailsPageObjects.radTotPermDisabilityUnitised:CoverDetailsPageObjects.radTotPermDisabilityFixed), "click", "Total Death Cover Required Selected is Unitised -"+strTotalDeathCover);
			}
			
			
			tryAction(waitForClickableElement(CoverDetailsPageObjects.txtDcAddnlCvrIpTxt), "SET", "Total Death Cover",
					strUnitsTotalDeathCover);
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtDcAddnlCvrIpTxt), "TAB", "Total Death Cover");
			sleep();
			tryAction(waitForClickableElement(CoverDetailsPageObjects.txttpdAddnlCvrIpTxt), "SET",
					"Total Permanent Disability Cover", strUnitsTotalPermanentDisability);
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txttpdAddnlCvrIpTxt), "TAB",
					"Total Permanent Disability Cover");
			if(!strIpAddlCover.equalsIgnoreCase("none"))
			{
				if (strWaitingPeriod!=null || strWaitingPeriod!="")
				{
					sleep();
					if (strSalInsure.equalsIgnoreCase("Checked")) {
						tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal), "chkCheck",
								"Insure 85% of my salary");
					} else {
						tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal), "chkUnCheck",
								"Insure 85% of my salary");
					}
				}
				
				sleep();
				tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddlCover), "SET", "IP Additional Cover",
						strIpAddlCover);
				sleep();
				
				//Special Handling only for this control.
				if(strWaitingPeriod.equalsIgnoreCase("30 days")){
					fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
					sleep(1000);
					new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(0);
					report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
				}else if(strWaitingPeriod.equalsIgnoreCase("60 days")){
					fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
					sleep(1000);
					new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(1);
					report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
				}else{
					fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
					sleep(1000);
					new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(2);
					report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
				}
				sleep();
				tryAction(fluentWaitElements(CoverDetailsPageObjects.lstBenefitPeriod), "DropDownSelect", "Benefit Period ",strBenefitPeriod);
			}
			sleep();
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote), "click", "Calculate Quote");
			
			sleep(2000);
			report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
			getPremiumsFromScreen();
			assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
			assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
			assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
			//assertEqualText(dataTable.getData("MetLife_Data", "salaryInsure"),"");
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply), "clkradio", "Apply Button");
			sleep();
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}
	
	private void enterAEISCoverGrid() {
		// TODO Auto-generated method stub
		String strUnitsTotalDeathCover = dataTable.getData("MetLife_Data", "unitsTotalDeathCover");
		String strUnitsTotalPermanentDisability = dataTable.getData("MetLife_Data", "unitsTotalPermanentDisability");
		String strSalInsure = dataTable.getData("MetLife_Data", "salaryInsure");
		String strWaitingPeriod = dataTable.getData("MetLife_Data", "change.waitngperiod");
		String strBenefitPeriod = dataTable.getData("MetLife_Data", "change.benefitperiod");
		String strMemberType = dataTable.getData("MetLife_Data", "applicant2_memberType");
		String strTotalDeathCover = dataTable.getData("MetLife_Data", "radioTotalDeathCover");
		String strTPDCover = dataTable.getData("MetLife_Data", "radioTotalPermanentDisability");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");

		try {
			if(strMemberType.equalsIgnoreCase("Personal")){
				tryAction(waitForClickableElement(strTotalDeathCover.equalsIgnoreCase("Unitised")?CoverDetailsPageObjects.radTotalDeathCoverUnitised:CoverDetailsPageObjects.radTotalDeathCoverFixed), "click", "Total Death Cover Required Selected is Unitised -"+strTotalDeathCover);
				sleep();
				tryAction(waitForClickableElement(strTPDCover.equalsIgnoreCase("Unitised")?CoverDetailsPageObjects.radTotPermDisabilityUnitised:CoverDetailsPageObjects.radTotPermDisabilityFixed), "click", "Total Death Cover Required Selected is Unitised -"+strTotalDeathCover);
			}
			
			
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtDcAddnlCvrIpTxt), "SET", "Total Death Cover",
					strUnitsTotalDeathCover);
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtDcAddnlCvrIpTxt), "TAB", "Total Death Cover");
			sleep();
			tryAction(waitForClickableElement(CoverDetailsPageObjects.txttpdAddnlCvrIpTxt), "SET",
					"Total Permanent Disability Cover", strUnitsTotalPermanentDisability);
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txttpdAddnlCvrIpTxt), "TAB",
					"Total Permanent Disability Cover");
			sleep();
			
			if(!(strWaitingPeriod==null))
			{
				if (strSalInsure.equalsIgnoreCase("Checked")) {
					tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal), "chkCheck",
							"Insure 85% of my salary");
				} else {
					tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal), "chkUnCheck",
							"Insure 85% of my salary");
				}
			}
			
			if(strWaitingPeriod.equalsIgnoreCase("30 days")){
				System.out.println("Hi");
				sleep(1000);
				driver.findElement(By.id("ipWaitingPeriodLst")).click();
				sleep(1000);
				Select drpdwn = new Select(driver.findElement(By.id("ipWaitingPeriodLst")));
				drpdwn.selectByIndex(0);
				report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
			}else if(strWaitingPeriod.equalsIgnoreCase("60 days")){
				fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
				sleep(1000);
				new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(1);
				report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
			}else{
				fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
				sleep(1000);
				new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(2);
				report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
			}
			//sleep(1000);
			//tryAction(fluentWaitElements(CoverDetailsPageObjects.lstBenefitPeriod), "DropDownSelect", "Benefit Period ",strBenefitPeriod);
			//sleep();

			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote), "click", "Calculate Quote");
			sleep(2000);
			report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
			getPremiumsFromScreen();
			getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1);
			if(strTCID.contains("PremiumCalculation")){
			assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
			assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
			assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
			}
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply), "clkradio", "Apply Button");
			tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply), "clkradio", "Apply Button");
			sleep();
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}

	private void fillInSFPSCoverDetails() {
		// TODO Auto-generated method stub
		String smoker=dataTable.getData("MetLife_Data", "Smoker");
		String work15hrs=dataTable.getData("MetLife_Data", "Work 15 hours and more");
		String industry=dataTable.getData("MetLife_Data", "Industry");
		String currentOccupation=dataTable.getData("MetLife_Data", "current occupation");
		String officeEnvironment=dataTable.getData("MetLife_Data", "Office Environment");
		String blnWorkDuties=dataTable.getData("MetLife_Data", "workDuties");
		String annualSalary=dataTable.getData("MetLife_Data", "Annual Salary");
		String work35hrs=dataTable.getData("MetLife_Data", "Work 35 hours and more");
		String permEmployed=dataTable.getData("MetLife_Data", "Permanently Employed");
		String permResident=dataTable.getData("MetLife_Data", "Permanently Resident");
		String insuranceCover=dataTable.getData("MetLife_Data", "Insurance Cover");
		
		
		try {
			tryAction(fluentWaitElement(smoker.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radSmokerYes:CoverDetailsPageObjects.radSmokerNo), "click", "Are you a smoker? -"+smoker);
			tryAction(fluentWaitElements(work15hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrkHoursYes:CoverDetailsPageObjects.radWrkHoursNo), "click", "Do you work more than 15 hours per week? -"+work15hrs);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrkId), "DropDownSelect", "What industry do you work in?",industry);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			// modified by Sandeep
			if(!(currentOccupation.contains("Prison Officer/Guard")||currentOccupation.contains("Paramedic rescue"))){			
			tryAction(fluentWaitElements(officeEnvironment.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radOfficeEnvironmentYes:CoverDetailsPageObjects.radOfficeEnvironmentNo), "click", "Do you work wholly within an office environment? -"+officeEnvironment);
			tryAction(fluentWaitElements(blnWorkDuties.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWorkDutiesYes:CoverDetailsPageObjects.radWorkDutiesNo), "click", "Do you hold a tertiary qualification relevant to your current occupation or are you a member of a professional body relevant to your current occupation or a member of your organisation's senior management team? -"+blnWorkDuties);
			}
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",annualSalary);
			tryAction(fluentWaitElements(work35hrs.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radWrk35HoursYes:CoverDetailsPageObjects.radWrk35HoursNo), "click", "Are you currently able to carry out all the identifiable duties of your current employment on an ongoing basis for 35 hours per week (whether or not you currently work these hours) without restriction due to accident, illness or injury? -"+work35hrs);
			tryAction(fluentWaitElements(permEmployed.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radPermEmpYes:CoverDetailsPageObjects.radPermEmpNo), "click", "Are you permanently employed? -"+permEmployed);
			tryAction(fluentWaitElements(permResident.equalsIgnoreCase("Yes")?CoverDetailsPageObjects.radResidentYes:CoverDetailsPageObjects.radResidentNo), "click", "Are you a citizen or permanent resident of Australia? -"+permResident);
			sleep(1000);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstPremiumFrequency), "DropDownSelect", "Express my cost of insurance cover as a ",insuranceCover);
			enterSFPSCoverGrid();
			//sleep(1000);
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
	
	private void enterGUILDCoverGrid(){
		String strTotalDeathCover = dataTable.getData("MetLife_Data", "radioTotalDeathCover");
		String strUnitsTotalDeathCover = dataTable.getData("MetLife_Data", "unitsTotalDeathCover");
		String strUnitsTotalPermanentDisability = dataTable.getData("MetLife_Data", "unitsTotalPermanentDisability");
		String strIncomeProtection = dataTable.getData("MetLife_Data", "unitsIncomeProtection");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");
		
		try{
		tryAction(waitForClickableElement(strTotalDeathCover.equalsIgnoreCase("Unitised")?CoverDetailsPageObjects.radTotalDeathCoverUnitised:CoverDetailsPageObjects.radTotalDeathCoverFixed), "click", "Total Death Cover Required Selected is Unitised -"+strTotalDeathCover);
		sleep();
		tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalDeathCover),"SET","Total Death Cover",strUnitsTotalDeathCover);
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalDeathCover),"TAB","Total Death Cover");
		sleep();
		tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"SET","Total Permanent Disability Cover",strUnitsTotalPermanentDisability);
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalPermDisability),"TAB","Total Permanent Disability Cover");
		tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddnlCover),"SET","Income Protection",strIncomeProtection);
		sleep(1000);
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtIpAddnlCover),"TAB","Income Protection");
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtIpAddnlCover),"TAB","Income Protection");
		sleep(1000);
		tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote),"click","Calculate Quote");
		
		sleep(2000);
		report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
		getPremiumsFromScreen();
		
		if(strTCID.contains("PremiumCalculation")){

		assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
		assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
		assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
		}
		tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply),"clkradio","Apply Button");
		sleep();
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void enterSFPSCoverGrid(){
		String strTotalDeathCover = dataTable.getData("MetLife_Data", "radioTotalDeathCover");
		String strUnitsTotalDeathCover = dataTable.getData("MetLife_Data", "unitsTotalDeathCover");
		String strUnitsTotalPermanentDisability = dataTable.getData("MetLife_Data", "unitsTotalPermanentDisability");
		String strIncomeProtection = dataTable.getData("MetLife_Data", "unitsIncomeProtection");
		String strWaitingPeriod = dataTable.getData("MetLife_Data", "change.waitngperiod");
		String strBenefitPeriod = dataTable.getData("MetLife_Data", "change.benefitperiod");
		String strTCName = dataTable.getData("MetLife_Data", "TC_ID");	
		String strAge = dataTable.getData("MetLife_Data", "lodge_mem_dob");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");
		
		int i = Integer.parseInt(strAge);
		System.out.println("Hi"+i);
		
		try{
		tryAction(waitForClickableElement(strTotalDeathCover.equalsIgnoreCase("Unitised")?CoverDetailsPageObjects.radTotalDeathCoverUnitised:CoverDetailsPageObjects.radTotalDeathCoverFixed), "click", "Total Death Cover Required Selected is Unitised -"+strTotalDeathCover);
		sleep();
		tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalDeathCover),"SET","Total Death Cover",strUnitsTotalDeathCover);
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalDeathCover),"TAB","Total Death Cover");
		sleep();
		
		// modified by Sandeep
		if (i <= 60) {
		tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"SET","Total Permanent Disability Cover",strUnitsTotalPermanentDisability);
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalPermDisability),"TAB","Total Permanent Disability Cover");
		tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddnlCover),"SET","Income Protection",strIncomeProtection);
		}
		
		sleep(1000);
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtIpAddnlCover),"TAB","Income Protection");
		tryAction(fluentWaitElements(CoverDetailsPageObjects.txtIpAddnlCover),"TAB","Income Protection");
		sleep(1000);
		//Special Handling only for this control.
		//if(strWaitingPeriod.equalsIgnoreCase("30 days")){
			fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
			sleep(1000);
			new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(0);
			report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
	//	}else if(strWaitingPeriod.equalsIgnoreCase("60 days")){
	//		fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
	//		sleep(1000);
	//		new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(1);
	//		report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
	//	}else{
	//		fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
	//		sleep(1000);
	//		new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(2);
	//		report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
	//	}
		sleep();
		//tryAction(fluentWaitElements(CoverDetailsPageObjects.lstBenefitPeriod), "DropDownSelect", "Benefit Period ",strBenefitPeriod);
		//sleep();
		
		tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote),"click","Calculate Quote");
		
		sleep(2000);
		report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
		getPremiumsFromScreen();
		
		if(strTCID.contains("PremiumCalculation")){
		assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
		assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
		assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
		}
		tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply),"clkradio","Apply Button");
		sleep();

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void enterREISCoverGrid(){
		String strDeathSelectCover =dataTable.getData("MetLife_Data", "change.deathcoverselect");
		String strTPDSelectCover =dataTable.getData("MetLife_Data", "change.tpdSelectCover");
		String tcIpSelectCover = dataTable.getData("MetLife_Data", "change.ipSelectCover");
		String strTCR = dataTable.getData("MetLife_Data", "change.tcr");
		String strSalInsure =dataTable.getData("MetLife_Data", "change.insuresalary");
		String strTotalDeathCover =dataTable.getData("MetLife_Data", "totaldeathcover");
		String strTPDCover =dataTable.getData("MetLife_Data", "totaltpdcover");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");

		try{
		tryAction(fluentWaitElement(CoverDetailsPageObjects.lstDeathSelectCover),"DropDownSelect","Death Select Cover",strDeathSelectCover);
		Thread.sleep(1000);
		tryAction(fluentWaitElement(CoverDetailsPageObjects.lstIpSelectCover),"DropDownSelect","IP Select Cover",tcIpSelectCover);
		Thread.sleep(1000);
		tryAction(fluentWaitElement(CoverDetailsPageObjects.lstTpdSelectCover),"DropDownSelect","TPD Select Cover",strTPDSelectCover);
		Thread.sleep(1000);
		System.out.println("strTCR :: "+strTCR);
		tryAction(strTCR.equalsIgnoreCase("unitised")?fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverUnitised):fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverFixed),"clkradio","Total Death Cover Required Selected is "+strTCR);
		sleep();
		if(strDeathSelectCover.contains("Increase"))
		{
			tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalDeathCover),"SET","Total Death Cover",strTotalDeathCover);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalDeathCover),"TAB","Total Death Cover");
			sleep();
		}
		
		if(strTPDSelectCover.contains("Increase"))
		{
			tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"SET","Total Permanent Disability Cover",strTPDCover);
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalPermDisability),"TAB","Total Death Cover");
			sleep();
		}
		/*if (strSalInsure.equalsIgnoreCase("Checked")) {
			tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal),"chkCheck","Insure 85% of my salary");
		}else{
			tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal),"chkUnCheck","Insure 85% of my salary");
		}*/
		sleep();
		tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote),"click","Calculate Quote");
		sleep(2000);
		report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
		getPremiumsFromScreen();
		if(strTCID.contains("PremiumCalculation")){
		//assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
		assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
		assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
		}		
		tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply),"clkradio","Apply Button");
		sleep();
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void fillInCoverDetails() {
		
		String strWorkHours = dataTable.getData("MetLife_Data", "change.work15hours");
		String strGender =dataTable.getData("MetLife_Data", "change.gender");
		String strIndustry = dataTable.getData("MetLife_Data", "change.industry");
		String strOccupation = dataTable.getData("MetLife_Data", "change.occupation");
		String strSalary = dataTable.getData("MetLife_Data", "change.salary");
		String strPremFreq = dataTable.getData("MetLife_Data", "change.premiumFreq");
		String strWorkWholly = dataTable.getData("MetLife_Data", "change.workwholly");
		String strWorkTertiary = dataTable.getData("MetLife_Data", "change.tertiary");
		String strTCR = dataTable.getData("MetLife_Data", "change.tcr");
		String strDeathTcr =dataTable.getData("MetLife_Data", "change.deathTCR");
		String strDeathTPDTcr =dataTable.getData("MetLife_Data", "change.TPDTCR");
		String strSalInsure =dataTable.getData("MetLife_Data", "change.insuresalary");
		String tcName = dataTable.getData("MetLife_Data", "TC_ID");
		String strIpCover = dataTable.getData("MetLife_Data", "ipcover");
		String strAddnlIpCover = dataTable.getData("MetLife_Data", "addnlipcover");
		String strDeathExCover =dataTable.getData("MetLife_Data", "deathexistingcover");
		String strTpdExCover = dataTable.getData("MetLife_Data", "tpdexistingcover");
		String strIpExCover = dataTable.getData("MetLife_Data", "ipexistingcover");
		String strIpAddnlCover = dataTable.getData("MetLife_Data", "ipaddnlcover");
		String strWhiteLabel = dataTable.getData("MetLife_Data", "whiteLabelName");
		String strSmoker = dataTable.getData("MetLife_Data", "personal.smoked");
		String strBenefitPeriod = dataTable.getData("MetLife_Data", "change.benefitperiod");
		String strWaitingPeriod = dataTable.getData("MetLife_Data", "change.waitngperiod");
		String strIpAddlCover=dataTable.getData("MetLife_Data", "change.ipaddlcover");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");
		
		try{	
			if (!(tcName.contains("PWC")||tcName.contains("PSUP")))
			{
				tryAction(strWorkHours.equalsIgnoreCase("yes")?fluentWaitElements(CoverDetailsPageObjects.radWrkHoursYes):fluentWaitElements(CoverDetailsPageObjects.radWrkHoursNo),"clkradio","Do you work more than 15 hours per week? "+strWorkHours);
			}
			if (tcName.contains("PWC")||tcName.contains("PSUP"))
			{
				tryAction(strGender.equalsIgnoreCase("female")?fluentWaitElements(CoverDetailsPageObjects.radFemale):fluentWaitElements(CoverDetailsPageObjects.radMale),"Click",strGender);
			}
			if (tcName.contains("PSUP"))
			{
				tryAction(strSmoker.equalsIgnoreCase("yes")?fluentWaitElements(CoverDetailsPageObjects.radSmokerYes):fluentWaitElements(CoverDetailsPageObjects.radSmokerNo),"Click",strSmoker);
			}
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstIndustrytoWrk),"DropDownSelect","What industry do you work in?",strIndustry);
			sleep();
			System.out.println("Tested");
			tryAction(fluentWaitElements(CoverDetailsPageObjects.lstOccupation),"DropDownSelect","Current Occupation",strOccupation);
			//driver.findElement(By.id("industryOccupListId")).click();
			//driver.findElement(By.linkText(strOccupation)).click();
			if (!(tcName.contains("VICS")|| tcName.contains("PWC")))
			{
				tryAction(fluentWaitElements(CoverDetailsPageObjects.radOfficeEnvironmentYes),"clkradio","Do you work wholly within an office environment");
				tryAction(fluentWaitElements(CoverDetailsPageObjects.radTertiaryOccupationYes),"clkradio","Tertiary Occuption");
			}
			sleep();
			tryAction(fluentWaitElements(CoverDetailsPageObjects.txtSalary),"SET","Annual Salary",strSalary);
			if(!tcName.contains("PWC"))
				tryAction(fluentWaitElements(CoverDetailsPageObjects.lstPremFrequency),"DropDownSelect","Express my cost of insurance cover as a ",strPremFreq);

			if(tcName.contains("PSUP")){
				tryAction(strTCR.equalsIgnoreCase("unitised")?fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverUnitised):fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverFixed),"clkradio","Total Death Cover Required Selected is "+strTCR);
				sleep();
				if(!strDeathExCover.equalsIgnoreCase("none")){
					sleep();
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtDeathExistingCover),"SET","Death Existing Cover",strDeathExCover);
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtDeathExistingCover),"TAB","Death Existing Cover");
					sleep();
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTpdExistingCover),"SET","TPD Existing Cover",strTpdExCover);
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTpdExistingCover),"TAB","TPD Existing Cover");
					sleep();
				}
				if(!strIpExCover.equalsIgnoreCase("none"))
				{
					sleep();
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpExistingCover),"SET","IP Existing Cover",strIpExCover);
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpExistingCover),"TAB","IP Existing Cover");
					sleep();
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddnlCover),"SET","IP Additional Cover",strIpAddnlCover);
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddnlCover),"TAB","IP Additional Cover");
					sleep();
				}
			}
			
			if(tcName.contains("VICS")){
				String strDeathSelectCover =dataTable.getData("MetLife_Data", "change.deathcoverselect");
				String strTPDSelectCover =dataTable.getData("MetLife_Data", "change.tpdSelectCover");
				String tcIpSelectCover = dataTable.getData("MetLife_Data", "change.ipSelectCover");
				tryAction(fluentWaitElement(CoverDetailsPageObjects.lstDeathSelectCover),"DropDownSelect","Death Select Cover",strDeathSelectCover);
				Thread.sleep(1000);
				tryAction(fluentWaitElement(CoverDetailsPageObjects.lstIpSelectCover),"DropDownSelect","IP Select Cover",tcIpSelectCover);
				Thread.sleep(1000);
				tryAction(fluentWaitElement(CoverDetailsPageObjects.lstTpdSelectCover),"DropDownSelect","TPD Select Cover",strTPDSelectCover);
				Thread.sleep(1000);
				if(!strDeathSelectCover.contains("Cancel"))
					tryAction(strTCR.equalsIgnoreCase("unitised")?fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverUnitised):fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverFixed),"clkradio","Total Death Cover Required Selected is "+strTCR);
				sleep();
				if(strDeathSelectCover.contains("Increase"))
				{
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalDeathCover),"SET","Total Death Cover",String.valueOf(Integer.parseInt(getStringValueFromSessionThreadData("dyn_previousDB"))+2));
					tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalDeathCover),"TAB","Total Death Cover");
					sleep();
				}else if (strDeathSelectCover.contains("Decrease")){
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalDeathCover),"SET","Total Death Cover",String.valueOf(Integer.parseInt(getStringValueFromSessionThreadData("dyn_previousDB"))-2));
					tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalDeathCover),"TAB","Total Death Cover");
				}
				
				if(strTPDSelectCover.contains("Increase"))
				{
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"SET","Total Permanent Disability Cover",String.valueOf(Integer.parseInt(getStringValueFromSessionThreadData("dyn_previousTPD"))+1));
					tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalPermDisability),"TAB","Total Death Cover");
					sleep();
				}else if (strDeathSelectCover.contains("Decrease")){
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"SET","Total Permanent Disability Cover",String.valueOf(Integer.parseInt(getStringValueFromSessionThreadData("dyn_previousTPD"))-3));
					tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalPermDisability),"TAB","Total Death Cover");
				}
				if(!tcIpSelectCover.equalsIgnoreCase("No change"))
				{
					if (strSalInsure.equalsIgnoreCase("Checked")) {
						tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal),"chkCheck","Insure 85% of my salary");
					}else{
						tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal),"chkUnCheck","Insure 85% of my salary");
					}
				}
			}else if(tcName.contains("PWC")){
				tryAction(fluentWaitElements(CoverDetailsPageObjects.txtIpCover),"SET","Income Protection",strIpCover);
				tryAction(fluentWaitElements(CoverDetailsPageObjects.txtIpCover),"TAB","Income Protection");
				sleep(1000);
				tryAction(fluentWaitElements(CoverDetailsPageObjects.txtAddnlIpCover),"SET","Additional Income Protection",strAddnlIpCover);
				//sleep(1000);
			}else{
				tryAction(strTCR.equalsIgnoreCase("unitised")?fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverUnitised):fluentWaitElements(CoverDetailsPageObjects.radTotalDeathCoverFixed),"clkradio","Total Death Cover Required Selected is "+strTCR);
				sleep();
				tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalDeathCover),"SET","Total Death Cover",strDeathTcr);
				sleep();
				tryAction(fluentWaitElements(CoverDetailsPageObjects.txtTotalDeathCover),"TAB","Total Death Cover");
				sleep();
				tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"SET","Total Permanent Disability Cover",strDeathTPDTcr);
				sleep(1000);
				if(!tcName.contains("PSUP")){
					tryAction(waitForClickableElement(CoverDetailsPageObjects.txtTotalPermDisability),"TAB","Total Permanent Disability Cover");
					if(!strIpAddlCover.equalsIgnoreCase("none"))
					{
						if (strSalInsure.equalsIgnoreCase("Checked")) {
							sleep();
							tryAction(waitForClickableElement(CoverDetailsPageObjects.chkPercentInsureSal),"clkradio","Insure 85% of my salary");
							sleep();
						}else{
							sleep();
							tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddlCover), "SET", "IP Additional Cover",strIpAddlCover);
							sleep(1000);
							tryAction(waitForClickableElement(CoverDetailsPageObjects.txtIpAddlCover), "TAB", "IP Additional Cover");
							sleep(1000);
							
							if(strWaitingPeriod.equalsIgnoreCase("30 days")){
								fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
								sleep(1000);
								new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(0);
								report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
							}else if(strWaitingPeriod.equalsIgnoreCase("60 days")){
								fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
								sleep(1000);
								new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(1);
								report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
							}else{
								fluentWaitElements(By.id("ipWaitingPeriodLst")).click();
								sleep(1000);
								new Select(fluentWaitElements(By.cssSelector("#ipWaitingPeriodLst"))).selectByIndex(2);
								report.updateTestLog("List Select",	"'Waiting Period Element Present in the stipulated time and Selected data is "	+ strWaitingPeriod, Status.PASS);
							}
							sleep(2000);
							tryAction(fluentWaitElements(CoverDetailsPageObjects.lstBenefitPeriod), "DropDownSelect", "Benefit Period ",strBenefitPeriod);
							sleep(1000);
						}
					}
				}
			}
			if(tcName.contains("PWC")||tcName.contains("PSUP")){
				tryAction(waitForClickableElement(CoverDetailsPageObjects.btnPwcCalcQuote),"clkradio","Calculate Quote");
			}
			else{
				sleep();
				tryAction(waitForClickableElement(CoverDetailsPageObjects.btnCalcQuote),"click","Calculate Quote");
				sleep(3000);
				report.updateTestLog("Cpaturte Premium Rates", "Cpaturte Premium Rates", Status.SCREENSHOT);
				getPremiumsFromScreen();		
				
				// Added by Sandeep
				
				if(strTCID.contains("PremiumCalculation")){
				assertEqualText(dataTable.getData("MetLife_Data", "IP_Monthly"),getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
				assertEqualText(dataTable.getData("MetLife_Data", "TPD_Monthly"),getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
				assertEqualText(dataTable.getData("MetLife_Data", "Death_Monthly"),getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
				}
			}
			sleep();
			if(tcName.contains("PWC")){
				storeSessionThreadData("dyn_addnlIpCover",fluentWaitElements(CoverDetailsPageObjects.lblAddnlCover).getText());
				System.out.println("The Dynamic Value is :: "+getStringValueFromSessionThreadData("dyn_addnlIpCover"));
			}else{
				//storeSessionThreadData("dyn_dcCoverCost",fluentWaitElements(CoverDetailsPageObjects.dyn_strDcCoverCost).getText());
				//System.out.println("The Dynamic Value is :: "+getStringValueFromSessionThreadData("dyn_dcCoverCost").substring(1));
			}
			if(tcName.contains("VICS")){
				String strDeathSelectCover =dataTable.getData("MetLife_Data", "change.deathcoverselect");
				String strTPDSelectCover =dataTable.getData("MetLife_Data", "change.tpdSelectCover");
				String tcIpSelectCover = dataTable.getData("MetLife_Data", "change.ipSelectCover");
				//sleep();
				if(strDeathSelectCover.contains("Cancel")||strDeathSelectCover.contains("Decrease")||strTPDSelectCover.contains("Cancel")||strTPDSelectCover.contains("Decrease")){
					sleep();
					tryAction(waitForClickableElement(CoverDetailsPageObjects.btnVicsContinue),"Click","Continue Button");
					sleep(1000);
					List<WebElement> btnPopup = null;
					btnPopup = fluentWaitListElements(By.cssSelector(".popOuter .xq"));
			//		System.out.println("Hi");
			//		System.out.println("Hi"+btnPopup.get(1));
					sleep(500);
					btnPopup.get(1).click();
				}else{
					
					tryAction(waitForClickableElement(CoverDetailsPageObjects.btnContinue),"Click","Continue Button");
				}
			}else{
				tryAction(waitForClickableElement(CoverDetailsPageObjects.btnApply),"clkradio","Apply Button");
			}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void getPremiumsFromScreen(){
		
		if (isElementPresent(CoverDetailsPageObjects.lblDeathAddlCoverAmt)){
			storeSessionThreadData("dyn_dcAddlCoverCost",fluentWaitElements(CoverDetailsPageObjects.lblDeathAddlCoverAmt).getText());
		}
		
		if (isElementPresent(CoverDetailsPageObjects.lblTPDAddlCoverAmt)){
			storeSessionThreadData("dyn_TPDAddlCoverCost",fluentWaitElements(CoverDetailsPageObjects.lblTPDAddlCoverAmt).getText());
		}
		
		if (isElementPresent(CoverDetailsPageObjects.lblIPAddlCoverAmt)){
			storeSessionThreadData("dyn_IPAddlCoverCost",fluentWaitElements(CoverDetailsPageObjects.lblIPAddlCoverAmt).getText());
		}
		
		if (isElementPresent(CoverDetailsPageObjects.lblDCCostAmt)){
			storeSessionThreadData("dyn_dcCostAmt",fluentWaitElements(CoverDetailsPageObjects.lblDCCostAmt).getText());
		}
		
		if (isElementPresent(CoverDetailsPageObjects.lblTPDCostAmt)){
			storeSessionThreadData("dyn_TPDCostAmt",fluentWaitElements(CoverDetailsPageObjects.lblTPDCostAmt).getText());
		}
		
		if (isElementPresent(CoverDetailsPageObjects.lblIPCostAmt)){
			storeSessionThreadData("dyn_IPCostAmt",fluentWaitElements(CoverDetailsPageObjects.lblIPCostAmt).getText());
			System.out.println(getStringValueFromSessionThreadData("dyn_IPCostAmt").substring(1));
		}
		System.out.println(getStringValueFromSessionThreadData("dyn_dcAddlCoverCost").substring(1));
		System.out.println(getStringValueFromSessionThreadData("dyn_TPDAddlCoverCost").substring(1));
		System.out.println(getStringValueFromSessionThreadData("dyn_IPAddlCoverCost").substring(1));
		System.out.println(getStringValueFromSessionThreadData("dyn_dcCostAmt").substring(1));
		System.out.println(getStringValueFromSessionThreadData("dyn_TPDCostAmt").substring(1));
	
	}
	
	
}
