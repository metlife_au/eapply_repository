package vicsuper;

import java.math.BigDecimal;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import vicsSuper_Objects.LandingPageObject;
import vicsSuper_Objects.SpecialOfferObject;
import vicsSuper_Objects.TransferCoverObjects;

public class SpecialOffer extends MasterPage {
	WebDriverWait wait = new WebDriverWait(driver, 20);
	WebDriverUtil driverUtil = null;
	//JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	public SpecialOffer(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	String XML;
	String SenarioType;
	String DataSheet;
	String occRating1;
	String occRating2;
	String occRating3;
	String BenefitPeriod;
	String SecondPage;

	// Actions action = new Actions(driver.getWebDriver());

	// SpecialOfferObject PerDet= new SpecialOfferObject(scriptHelper);

	public SpecialOffer NewMember_vicsuper(String DataSheetName, String Type) {

		SenarioType = Type;
		// PerDet.SpecialOfferObjectInitialization(AppPage);
		XML = dataTable.getData("VicSuper_Login", "XML");
		DataSheet = DataSheetName;
		if (SenarioType.equalsIgnoreCase("Overall")) {

		} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {

			PremiumRateInputValidation();
		} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {

			occupationRatingInputValidation();
		} else {
			report.updateTestLog("Error Type", "Invalid Type value-" + SenarioType, Status.FAIL);
		}
		PersonalDetails();
		return new SpecialOffer(scriptHelper);

	}

	public void PersonalDetails() {
		String DutyOfDisclosure = null;
		String PrivacyStatement = null;
		if (SenarioType.equalsIgnoreCase("Overall")) {
		DutyOfDisclosure = dataTable.getData(DataSheet, "DutyOfDisclosure");
		PrivacyStatement = dataTable.getData(DataSheet, "PrivacyStatement");
		}
		String Gender = dataTable.getData(DataSheet, "Gender");
		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
		String Citizen = dataTable.getData(DataSheet, "Citizen");
		String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
		String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
		String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
		String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");


		try {

			sleep(500);
			WebElement splOff = driver.findElement(LandingPageObject.newMember);
			js.executeScript("arguments[0].scrollIntoView(true);", splOff);
			 sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.newMember)).click();
			sleep(4000);
			// Popup Validation
			/*
			 * if (driver.findElement(By.xpath("//button[text()='Continue']")).
			 * isDisplayed() == true) {
			 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
			 * "//button[text()='Continue']"))).click(); sleep(5000); }
			 */
			sleep(5000);

			// -----------------------------------------------------PersonalDetails------------------------------------------------------------------------//
			// *****Agree to Duty of disclosure*******\\
			if (SenarioType.equalsIgnoreCase("Overall")) {
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_dutyOfDisclosure));


			String[] ArrayHeaderDD;
			int flag = 1;

			String part2;
			String[] value;
			String HeaderDD = null;

			String[] parts = DutyOfDisclosure.split("---");
			String DDTxt = driver.findElement(By.xpath(".//*[@name='formduty']//p[2]/strong")).getText();
			 if (DDTxt.equalsIgnoreCase(parts[0])) {
				}
				else {
					flag =0;
					report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
				}

			parts = parts[1].split("\\n\\n");

			int j=3;
			for (int a = 0; a < 9; a++) {

					if ( !(a==3) ) {
						HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//p)["+j+"]")).getText();
						j++;

						 if (HeaderDD.equalsIgnoreCase(parts[a])) {
							}
							else {
								flag =0;
								report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}


					}
				else {
					String[] parts1 = parts[a].split("\\n");
					int k=1;
					for (int b = 0; b < 5; b++) {

						System.out.println("K="+k);
						if ( !(b==4) ) {
							 HeaderDD = driver.findElement(By.xpath(".//*[@name='formduty']//ul/li["+k+"]")).getText();


						}
						else {
							 HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//strong)[2]")).getText();

						}
						 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

						 }
							else {
								flag =0;
								report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}

						k++;

					}

				}


			}



					if (flag==0) {
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is not as expected", Status.FAIL);
					}
					else {
						report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is present as expected", Status.PASS);

					}

			}
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_dutyOfDisclosure))
					.click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);



			// *****Agree to Privacy Statement*******\\
			if (SenarioType.equalsIgnoreCase("Overall")) {
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_privacyStatement));

			String[] ArrayHeaderDD;
			int flag = 1;

			String part2;
			String[] value;
			String HeaderDD = null;


			String[] parts = PrivacyStatement.split("---");
			String DDTxt = driver.findElement(By.xpath(".//*[@name='cancelprivacyPolicyForm']//p[2]/strong")).getText();
			 if (DDTxt.equalsIgnoreCase(parts[0])) {

				}
				else {
					flag =0;
					report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
				}

			parts = parts[1].split("\\n\\n\\n");
			int j=3;
				for (int a = 0; a < 2; a++) {

			HeaderDD = driver.findElement(By.xpath("(.//*[@name='cancelprivacyPolicyForm']//p)["+j+"]")).getText();

			 if (HeaderDD.equalsIgnoreCase(parts[a])) {

				}
				else {
					flag =0;
					report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
				}
			j++;
			}

				if (flag==0) {
					report.updateTestLog("Privacy statement", "Privacy statement - Statement is not as expected", Status.FAIL);

				}
				else {
					report.updateTestLog("Privacy statement", "Privacy statement - Statement is present as expected", Status.PASS);
				}

			}

			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_privacyStatement))
					.click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_email)).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_email))
					.sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
			sleep(300);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(SpecialOfferObject.obj_PersonalDetails_contactType));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_phNO))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_phNO))
					.sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {
				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Morning))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			} else if (TimeofContact.equalsIgnoreCase("Afternoon")) {
				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Afternoon))
						.click();
				sleep(500);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for TimeofContact", Status.FAIL);
			}

			/*
			 * //***********Select the Gender******************\\
			 *
			 * if (Gender.equalsIgnoreCase("Male")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * SpecialOfferObject.obj_PersonalDetails_Morning)).click();
			 * sleep(250); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS); } else if
			 * (Gender.equalsIgnoreCase("Female")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * SpecialOfferObject.obj_PersonalDetails_Afternoon)).click();
			 * sleep(500); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS);
			 *
			 * } else { report.updateTestLog("DataTable",
			 * "Invalid value for TimeofContact", Status.FAIL); }
			 */
			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_ContinueContactDetails)).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);


			// -----------------------------------------------------Occupation------------------------------------------------------------------------//

			// *****Select the 14 Hours Question*******\\

			if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_FourteenHrsYes))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
			} else if (FourteenHoursWork.equalsIgnoreCase("No")) {
				wait.until(
						ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_FourteenHrsNo))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for FourteenHoursWork", Status.FAIL);
			}

			// *****Resident of Australia****\\
			if (Citizen.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_CitizenYes))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
			} else if (Citizen.equalsIgnoreCase("No")) {
				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_CitizenNo))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
			} else {
				report.updateTestLog("DataTable", "Invalid value for Citizen", Status.FAIL);
			}


			// *****Are the duties of your regular occupation limited to
			// either:*****\\

			if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

				// ***Select duties which are undertaken within an Office
				// Environment?\\
				if (EducationalInstitution.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AddQuest1aYes)).click();
					report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalInstitution.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AddQuest1aNo)).click();
					report.updateTestLog("Educational institution", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalInstitution", Status.FAIL);
				}

				// *****Educational duties performed within a school or other
				// educational institution******\\
				if (EducationalDuties.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AddQuest1bYes)).click();
					report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalDuties.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AddQuest1bNo)).click();
					report.updateTestLog("Educational duties", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalDuties", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

			}

			// Do you hold a tertiary qualification or work in a management role
			if (driver.getPageSource().contains("Do you:")) {

				// *****Do You Hold a Tertiary Qualification******\\
				if (TertiaryQual.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AddQuest2Yes)).click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (TertiaryQual.equalsIgnoreCase("No")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AddQuest2No))
							.click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for TertiaryQual", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Do you hold a tertiary qualification or work in a management role is not present",
						Status.FAIL);
			}

			// *****What is your annual Salary******

			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AnnualSal))
					.clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AnnualSal))
					.sendKeys(AnnualSalary);
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_AnnualSal))
					.sendKeys(Keys.ENTER);
			sleep(1000);
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);

			//*****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_ContinueOccupation)).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);



			// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//
			if (SenarioType.equalsIgnoreCase("Overall")) {

				String CoverValidation = dataTable.getData(DataSheet, "CoverValidation");
				if (!(CoverValidation.equalsIgnoreCase(""))) {
					cover_Validations();
					return;
				}
				cover_Overall();
			} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {
				cover_Premium();
			} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
				cover_OccupationRating();
			}

			if ((SenarioType.equalsIgnoreCase("PremiumCalculation"))) {
				return;
			} else if ((SenarioType.equalsIgnoreCase("Overall"))) {

					wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Continue))
							.click();
					sleep(3000);

					report.updateTestLog("Continue - Personal Details",
							"Continue - Personal Details button is selected", Status.PASS);
					HealthandLifeStyle();


			} else if ((SenarioType.equalsIgnoreCase("OccupationRating"))) {
				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Continue))
				.click();
		sleep(3000);

		report.updateTestLog("Continue - Personal Details",
				"Continue - Personal Details button is selected", Status.PASS);
		HealthandLifeStyle();
			}

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}

	public void HealthandLifeStyle() {

		sleep(3000);
		String Restricted_Ilness = dataTable.getData(DataSheet, "Restricted_Ilness");
		String PreviousClaim = dataTable.getData(DataSheet, "PreviousClaim");
		String Restricted_Work = dataTable.getData(DataSheet, "Restricted_Work");
		String Diagnosed_Illness = dataTable.getData(DataSheet, "Diagnosed_Illness");
		String MedicalTreatment = dataTable.getData(DataSheet, "MedicalTreatment");
		String Previous_Insurance = dataTable.getData(DataSheet, "Previous_Insurance");
		String Decline_DueToAura = null;
		if (SenarioType.equalsIgnoreCase("Overall")) {
			Decline_DueToAura = dataTable.getData(DataSheet, "Decline_DueToAura");
		}



			try {

				// *****Select the Restricted_Ilness Question*******\\

				if (Restricted_Ilness.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_RestrictedIllnessYes))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted due to ilness Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_RestrictedIllnessNo))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted due to ilness Question", "Selected No", Status.PASS);
				}

				// *****Select the PreviousClaim Question*******\\

				if (PreviousClaim.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_PriorClaimYes))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Claim Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_PriorClaimNo))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Claim Question", "Selected No", Status.PASS);
				}

				// *****Select the Restricted_Work Question*******\\

				if (Restricted_Work.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_RestrictedFromWorkYes))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted Work Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_RestrictedFromWorkNo))
							.click();
					sleep(1000);
					report.updateTestLog("Restricted Work Question", "Selected No", Status.PASS);
				}

				// *****Select the Diagnosed_Illness Question*******\\

				if (Diagnosed_Illness.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_DiagnosedIllnessYes))
							.click();
					sleep(1000);
					report.updateTestLog("Diagnosed with illness Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_DiagnosedIllnessNo))
							.click();
					sleep(1000);
					report.updateTestLog("Diagnosed with illness Question", "Selected No", Status.PASS);
				}

				// *****Select the MedicalTreatment Question*******\\

				if (MedicalTreatment.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_MedicalTreatmentYes))
							.click();
					sleep(1000);
					report.updateTestLog("Medical Treatment Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_MedicalTreatmentNo))
							.click();
					sleep(1000);
					report.updateTestLog("Medical Treatment Question", "Selected No", Status.PASS);
				}

				js.executeScript("window.scrollTo(0, document.body.scrollHeight);");


				// *****Select the Previous_Insurance Question*******\\

				if (Previous_Insurance.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Aura_DeclinedApplicationYes))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Insurance Question", "Selected Yes", Status.PASS);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_DeclinedApplicationNo))
							.click();
					sleep(1000);
					report.updateTestLog("Previous Insurance Question", "Selected No", Status.PASS);
				}
				sleep(3000);
				//To click on Continue
				wait.until(ExpectedConditions
						.elementToBeClickable(SpecialOfferObject.obj_Aura_Continue))
						.click();
				report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);
				sleep(3000);
				if ((SenarioType.equalsIgnoreCase("Overall"))) {
				if (Decline_DueToAura.equalsIgnoreCase("Yes")) {
			    	 declinePopUP();
				}
			     else {
			    	 confirmation();
				}
				}
				 else {

						 confirmation();
				 }
				sleep(1000);

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void confirmation() {
		sleep(3000);
		System.out.println("confirmation");
		String Confirmation_DetailsValidations = dataTable.getData(DataSheet, "Confirmation_DetailsValidations");
		System.out.println(Confirmation_DetailsValidations);
		if (Confirmation_DetailsValidations.equalsIgnoreCase("Yes")
				|| Confirmation_DetailsValidations.equalsIgnoreCase("Occupation")) {
			detailsValidation();
		}

		if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			return;
		} else if (SenarioType.equalsIgnoreCase("Overall")) {

			String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
			String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");
			String GeneralConsent = dataTable.getData(DataSheet, "GeneralConsent");

			try {


					// ******Agree to general Consent*******\\
		String part2;
		String[] value;
		String HeaderDD = null;
		int flag=1;
				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Confirmation_TermsConditionsCheckBox));
				String[] parts = GeneralConsent.split("\\n\\n");



				 int j=1;
				for (int a = 0; a < 4; a++) {

					if ( !(a==3) ) {
						HeaderDD = driver.findElement(By.xpath("(.//*[@id='collapse4']/div/div/div/div/p)["+j+"]")).getText();
						j++;

						 if (HeaderDD.equalsIgnoreCase(parts[a])) {

							}
							else {
								flag =0;
								report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
							}

					}
					else {
						System.out.println("<><><><><><>");

						String[] parts1 = parts[a].split("\\n");
						System.out.println("1)"+parts1[0]);
						System.out.println("2)"+parts1[1]);
						System.out.println("3)"+parts1[2]);
						System.out.println("4)"+parts1[3]);
						System.out.println("5)"+parts1[4]);
						System.out.println("6)"+parts1[5]);
						System.out.println("7)"+parts1[6]);
						System.out.println("8)"+parts1[7]);
						System.out.println("9)"+parts1[8]);
						System.out.println("10)"+parts1[9]);


						int k=1;
						for (int b = 0; b < 10; b++) {


								 HeaderDD = driver.findElement(By.xpath(".//*[@id='collapse4']/div/div/div/div/ul/li["+k+"]")).getText();

							 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

							 }
								else {
									flag =0;
									report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
								}

							k++;

						}

					}
				}

				if (flag==0) {
					report.updateTestLog("General Consent", "General Consent - Statement is not as expected", Status.FAIL);

				}
				else {
					report.updateTestLog("General Consent", "General Consent - Statement is present as expected", Status.PASS);
				}


					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_Confirmation_TermsConditionsCheckBox)).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(3000);



							// ****Click on Continue****\\
							sleep(3000);
							wait.until(
									ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_Confirmation_Submit))
									.click();
							report.updateTestLog("Submit", "Submit button is Clicked", Status.PASS);
							sleep(5000);



				// *******Feedback popup******\\

				wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_DecisionPopUP_No)).click();
				report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
				sleep(2500);

				if (driver.getPageSource().contains("APPLICATION NUMBER")||driver.getPageSource().contains("Application Number")||driver.getPageSource().contains("Application number")) {

					// *****Fetching the Application Status*****\\

					String Appstatus = driver.findElement(SpecialOfferObject.obj_DecisionPopUP_Appstatus).getText();
					sleep(500);

					if (Appstatus.equalsIgnoreCase(DecisionHeader)) {
						report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
					}

					String AppMsg = driver.findElement(SpecialOfferObject.obj_DecisionPopUP_AppMsg).getText();
					sleep(500);
					System.out.println("////"+AppMsg+"////");
					if (AppMsg.equals(DecisionMsg)) {
						report.updateTestLog("Decision Message", "Decision Message - "+AppMsg+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+DecisionMsg+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
					}


					// *****Fetching the Application Number******\\

					String App = driver.findElement(SpecialOfferObject.obj_DecisionPopUP_App).getText();
					sleep(1000);

					wait.until(
							ExpectedConditions.presenceOfElementLocated(SpecialOfferObject.obj_Confirmation_Download))
							.click();
					sleep(3000);

					report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

					sleep(1000);

					report.updateTestLog("Decision Page", "Application No: " + App, Status.PASS);
					sleep(500);

				}

				else {
					report.updateTestLog("Application Declined", "Application is declined", Status.FAIL);

				}

			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);

			}

		}

	}
private void declinePopUP() {
	String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
	String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");

		wait.until(ExpectedConditions.elementToBeClickable(TransferCoverObjects.obj_DecisionPopUP_No)).click();
		report.updateTestLog("Application Decline", "Application is declined", Status.PASS);
		report.updateTestLog("Application Decline", "Selected No on the Feedback popup", Status.PASS);

		sleep(2500);

		String Appstatus = driver.findElement(SpecialOfferObject.obj_DecisionPopUP_Appstatus).getText();
		sleep(500);

		if (Appstatus.equalsIgnoreCase(DecisionHeader)) {
			report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
		}else {
			report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
		}

		String AppMsg = driver.findElement(SpecialOfferObject.obj_DecisionPopUP_AppMsg).getText();
		sleep(500);
		System.out.println("////"+AppMsg+"////");
		if (AppMsg.equals(DecisionMsg)) {
			report.updateTestLog("Decision Message", "Decision Message - "+AppMsg+" is present", Status.PASS);
		}else {
			report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+DecisionMsg+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
		}




	}
	public void detailsValidation() {

		if (SenarioType.equalsIgnoreCase("Overall")) {
			String Gender = dataTable.getData(DataSheet, "Gender");
			String EmailId = dataTable.getData(DataSheet, "EmailId");
			String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
			String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
			String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
			String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
			String Citizen = dataTable.getData(DataSheet, "Citizen");
			String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
			String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
			String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
			String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");


			// String DeathAmount = dataTable.getData(DataSheet,
			// "DeathAmount");
			String WaitingPeriod = dataTable.getData(DataSheet, "WaitingPeriod");
			String BenefitPeriod = dataTable.getData(DataSheet, "BenefitPeriod");

			AnnualSalary = "$" + AnnualSalary;


			String XML = dataTable.getData("VicSuper_Login", "XML");

			String[] part;
			String part2;
			String[] value;

			// To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1];
			value = part2.split("</dateOfBirth>");
			String dob = value[0];

			// Existing Cover Death
			part = XML.split("<amount>");
			String part2Death = part[1];
			String part2Tpd = part[2];
			String part2IP = part[3];

			String[] valueDeath = part2Death.split("</amount>");
			String[] valueTpd = part2Tpd.split("</amount>");
			String[] valueIP = part2IP.split("</amount>");

			String Death = valueDeath[0];
			String TPD = valueTpd[0];
			String IP = valueIP[0];

			double dD = Double.parseDouble(Death);
			int D = (int) dD;
			double dT = Double.parseDouble(TPD);
			int T = (int) dT;
			double dI = Double.parseDouble(IP);
			int I = (int) dI;

			Death = "$" + D;
			TPD = "$" + T;
			IP = "$" + I;
			System.out.println(Death);
			System.out.println(TPD);
			System.out.println(IP);
			// First Name
			WebElement objFName = driver.findElement(SpecialOfferObject.obj_Confirmation_FirstName);
			fieldValidation("WebElement", objFName, VicSuper__Login.firstName, "First Name");

			// Last Name
			WebElement objLName = driver.findElement(SpecialOfferObject.obj_Confirmation_LastName);
			fieldValidation("WebElement", objLName, VicSuper__Login.lastName, "Last Name");

			// Gender
			WebElement ObjGender = driver.findElement(SpecialOfferObject.obj_Confirmation_Gender);
			fieldValidation("WebElement", ObjGender, Gender, "Genderquestion");

			// Phone Number type
			WebElement objcontactType = driver.findElement(SpecialOfferObject.obj_Confirmation_ContactType);
			fieldValidation("WebElement", objcontactType, TypeofContact, "contactType");

			// PhoneNumber
			WebElement objContactNo = driver.findElement(SpecialOfferObject.obj_Confirmation_ContactNo);
			fieldValidation("WebElement", objContactNo, ContactNumber, "ContactNo");

			// Email
			WebElement objcontactEmail = driver.findElement(SpecialOfferObject.obj_Confirmation_Email);
			fieldValidation("WebElement", objcontactEmail, EmailId, "Email");
			js.executeScript("window.scrollBy(0,500)", "");
			// DOB
			WebElement objDOB = driver.findElement(SpecialOfferObject.obj_Confirmation_DOB);
			fieldValidation("WebElement", objDOB, dob, "DOB");

			// Morning Afternoon
			WebElement objTime = driver.findElement(SpecialOfferObject.obj_Confirmation_Time);
			String eletext = objTime.getText();
			System.out.println(eletext);
			System.out.println(TimeofContact);
			if (eletext.contains(TimeofContact)) {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is displayed",
						Status.PASS);

			} else {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is not displayed",
						Status.FAIL);

			}

			// Do you work more than 14 hours per week?

			WebElement ObjWorkHRS = driver.findElement(SpecialOfferObject.obj_Confirmation_HoursWork);
			fieldValidation("WebElement", ObjWorkHRS, FourteenHoursWork, "FourteenHoursWork");

			// Are you a citizen or permanent resident of Australia? Yes No

			WebElement objCitizen = driver.findElement(SpecialOfferObject.obj_Confirmation_Citizen);
			fieldValidation("WebElement", objCitizen, Citizen, "Citizen");

			// office Duties are Undertaken within an Office Environment?
			WebElement eduIns = driver.findElement(SpecialOfferObject.obj_Confirmation_AddQuest1a);
			fieldValidation("WebElement", eduIns, EducationalInstitution,
					"Are the duties of your regular occupation limited to Question1");

			WebElement eduDut = driver.findElement(SpecialOfferObject.obj_Confirmation_AddQuest1b);
			fieldValidation("WebElement", eduDut, EducationalDuties,
					"Are the duties of your regular occupation limited to Question2");

			// TertiaryQualification / ManagementRole

			WebElement objQual = driver.findElement(SpecialOfferObject.obj_Confirmation_TertiaryQualification);
			fieldValidation("WebElement", objQual, TertiaryQual, "TertiaryQualification / ManagementRole");

			// Current annual salary?
			WebElement objannualSalCCId = driver.findElement(SpecialOfferObject.obj_Confirmation_AnnualIncome);
			fieldValidation("Amount Validation", objannualSalCCId, AnnualSalary, "AnnualSalary");

			Death = "$" + D;
			TPD = "$" + T;
			IP = "$" + I;
			js.executeScript("window.scrollBy(0,500)", "");

			// Death

			// Death Existing Amount

			WebElement deathEXTAmount = driver.findElement(SpecialOfferObject.obj_Confirmation_Death_ExistingCover);
			fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");

			// Death Amount

			/*WebElement deathAmount = driver.findElement(SpecialOfferObject.obj_Confirmation_Death_NewCover);
			fieldValidation("Amount Validation", deathAmount, DeathAmount, "Death New Cover");*/


			// amountValidator("Death Amount",deathAmount,DeathAmount );

			/*
			 * // Death Weekly Cost WebElement DeathWeeklycost =
			 * driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[1]//following::p"));
			 * fieldValidation("WebElement",DeathWeeklycost,
			 * DeathWeeklyCost,"Death Weekly Cost");
			 */

			// TPD Amount

			// TPD Existing Amount

			WebElement tpdExtAmount = driver.findElement(SpecialOfferObject.obj_Confirmation_TPD_ExistingCover);
			fieldValidation("Amount Validation", tpdExtAmount, TPD, "TPD Existing Cover");

			// TPD Amount

/*			WebElement tpdAmount = driver.findElement(SpecialOfferObject.obj_Confirmation_TPD_NewCover);
	-->		fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");*/


			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// TPD Weekly Cost

			/*
			 * WebElement tpdWeeklycost = driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[2]//following::p"));
			 * fieldValidation("WebElement",tpdWeeklycost,
			 * TPDWeeklyCost,"TPD Weekly Cost");
			 */
//temp
			// IP Amount
/*
			// IP Existing Amount

			WebElement ipAmount = driver.findElement(SpecialOfferObject.obj_Confirmation_IP_ExistingCover);
			fieldValidation("Amount Validation", ipAmount, IP, "IP Existing Cover");

			// IP Amount

			WebElement ipExtAmount = driver.findElement(SpecialOfferObject.obj_Confirmation_IP_NewCover);
			fieldValidation("Amount Validation", ipExtAmount, IPAmount, "IP New Cover");
			// amountValidator("Death Amount",deathAmount,DeathAmount );
*/
			// Waiting Period

			WebElement ipWaitingPeriod = driver.findElement(SpecialOfferObject.obj_Confirmation_IP_WaitingPeriod);
			fieldValidation("WebElement", ipWaitingPeriod, WaitingPeriod, "Waiting Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// Benefit Period

			WebElement ipBenefitPeriod = driver.findElement(SpecialOfferObject.obj_Confirmation_IP_BenefitPeriod);
			fieldValidation("WebElement", ipBenefitPeriod, BenefitPeriod, "Benefit Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );

		}

		else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			String OutcomeForDeath = dataTable.getData(DataSheet, "OutcomeForDeath");
			String OutcomeForTPD = dataTable.getData(DataSheet, "OutcomeForTPD");
			String OutcomeForIP = dataTable.getData(DataSheet, "OutcomeForIP");

			WebElement OccupationalCategory_Death = driver
					.findElement(SpecialOfferObject.obj_Confirmation_Death_OccupationalCategory);
			js.executeScript("arguments[0].scrollIntoView(true);", OccupationalCategory_Death);
			fieldValidation("WebElement", OccupationalCategory_Death, OutcomeForDeath, "OccupationalCategory - Death");

			WebElement OccupationalCategory_tpd = driver
					.findElement(SpecialOfferObject.obj_Confirmation_TPD_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_tpd, OutcomeForTPD, "OccupationalCategory - TPD");

			WebElement OccupationalCategory_ip = driver
					.findElement(SpecialOfferObject.obj_Confirmation_IP_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_ip, OutcomeForIP, "OccupationalCategory - IP");

		}

	}

	public void occupationRatingInputValidation() {
		System.out.println("starttttttttt-" + DataSheet);
		String ExistingDeath = dataTable.getData(DataSheet, "ExistingDeath");
		String ExistingTPD = dataTable.getData(DataSheet, "ExistingTPD");
		String ExistingIP = dataTable.getData(DataSheet, "ExistingIP");
		System.out.println("enddddddddddddd");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;

		String[] value;

		// To get firstName
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(ExistingDeath))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + ExistingDeath + ".", Status.FAIL);
		}
		if (!(occRating2.equals(ExistingTPD))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + ExistingTPD + ".", Status.FAIL);
		}
		if (!(occRating3.equals(ExistingIP))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + ExistingIP + ".", Status.FAIL);
		}
		System.out.println("///valend");
	}

	public void PremiumRateInputValidation() {}

	public void cover_Overall() {

		String CostType = dataTable.getData(DataSheet, "CostType");
		String DeathUnits = dataTable.getData(DataSheet, "DeathUnits");
		String TPDUnits = dataTable.getData(DataSheet, "TPDUnits");
		String IPUnits = dataTable.getData(DataSheet, "IPUnits");
		String Insure85Percent = dataTable.getData(DataSheet, "Insure85Percent");
		String WaitingPeriod = dataTable.getData(DataSheet, "WaitingPeriod");
		String BenefitPeriod = dataTable.getData(DataSheet, "BenefitPeriod");
		// ******Click on Cost of Insurance as*******\\
					sleep(1000);
					Select COSTTYPE = new Select(driver.findElement(SpecialOfferObject.obj_PersonalDetails_PremiumFrequency));
					COSTTYPE.selectByVisibleText(CostType);
					sleep(1000);
					report.updateTestLog("Cost of Insurance", "Cost frequency selected is: " + CostType, Status.PASS);

					// *******Death Cover Section******\\


						// To Select - Extra cover required
						if (DeathUnits.equalsIgnoreCase("0")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Death0Uunit)).click();
							report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
							sleep(1000);

						}

						else if (DeathUnits.equalsIgnoreCase("1")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Death1Uunit)).click();
							report.updateTestLog("Death Extra Cover", "Death Extra Cover - 1 Unit is selected", Status.PASS);
							sleep(1000);

						} else if (DeathUnits.equalsIgnoreCase("2")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Death2Uunit)).click();
							report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
							sleep(1000);

						} else {
							report.updateTestLog("Data Table", "Invalid input for DeathUnits in Data Sheet. Please enter 0/1/2",
									Status.FAIL);
						}




					// *******TPD Cover Section******\\
					sleep(800);



						// To Select - Extra cover required
						if (TPDUnits.equalsIgnoreCase("0")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_TPD0Uunit)).click(); // Dhipika
							report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
							sleep(1000);

						}

						else if (TPDUnits.equalsIgnoreCase("1")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_TPD1unit)).click(); // Dhipika
							report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 1 Unit is selected", Status.PASS);
							sleep(1000);

						} else if (TPDUnits.equalsIgnoreCase("2")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_TPD2Uunit)).click(); // Dhipika
							report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 2 Unit is selected", Status.PASS);
							sleep(1000);

						} else {
							report.updateTestLog("Data Table", "Invalid input for TPDUnits in Data Sheet. Please enter 0/1/2",
									Status.FAIL);
						}






					// *******IP Cover section********\\
					sleep(3000);

							Select waitingperiod = new Select(
									driver.findElement(SpecialOfferObject.obj_PersonalDetails_WaitingPeriod));
							waitingperiod.selectByVisibleText(WaitingPeriod);
							report.updateTestLog("Waiting Period", "Waiting Period of: " + WaitingPeriod + " is Selected",
									Status.PASS);
							sleep(2000);

							Select benefitperiod = new Select(
									driver.findElement(SpecialOfferObject.obj_PersonalDetails_BenefitPeriod));
							benefitperiod.selectByVisibleText(BenefitPeriod);
							report.updateTestLog("Benefit Period", "Benefit Period: " + BenefitPeriod + " is Selected",
									Status.PASS);
							sleep(2000);


							if (BenefitPeriod.equalsIgnoreCase("5 Years")) {
								wait.until(ExpectedConditions
										.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_CasualORContractor)).click();
								report.updateTestLog("Casual or contractor employee", "The member is not a casual or contractor employee is selected", Status.PASS);
								sleep(3000);

							}



						// To Select - Extra cover required
						if (IPUnits.equalsIgnoreCase("0")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_IP0Uunit)).click(); // Dhipika
							report.updateTestLog("IP Extra Cover", "IP Extra Cover - None is selected", Status.PASS);
							sleep(1000);

						}

						else if (IPUnits.equalsIgnoreCase("1")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_IP1unit)).click(); // Dhipika
							report.updateTestLog("IP Extra Cover", "IP Extra Cover - 1 Unit is selected", Status.PASS);
							sleep(1000);

						} else if (IPUnits.equalsIgnoreCase("2")) {
							wait.until(ExpectedConditions
									.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_IP2Uunit)).click(); // Dhipika
							report.updateTestLog("IP Extra Cover", "IP Extra Cover - 2 Unit is selected", Status.PASS);
							sleep(1000);

						} else {
							report.updateTestLog("Data Table", "Invalid input for IPUnits in Data Sheet. Please enter 0/1/2",
									Status.FAIL);
						}

					}


	public void cover_Premium() {}

	public void amountValidation(String DollarAmount, String Amount, String Field) {
		System.out.println(DollarAmount);
		System.out.println(Amount);
		String[] part;
		String amount1;
		String[] value;
		String a1;
		String a2;
		String amount;
		Amount = "$" + Amount;
		if (DollarAmount.contains(",")) {

			// To get firstName
			String[] p = DollarAmount.split(",");
			String p1 = p[0];
			String p2 = p[1];
			DollarAmount = p1 +""+ p2;
		}

		if (DollarAmount.equalsIgnoreCase(Amount)) {
			report.updateTestLog(Field, Field + " -" + Amount + " is present", Status.PASS);
		} else {
			report.updateTestLog(Field,
					Field + " Expected Value-" + Amount + " / Value Present in page-" + DollarAmount, Status.FAIL);
		}

	}

	public void cover_OccupationRating() {

		String BenefitPeriod = dataTable.getData(DataSheet, "BenefitPeriod");
		Select benefitperiod = new Select(
				driver.findElement(SpecialOfferObject.obj_PersonalDetails_BenefitPeriod));
		benefitperiod.selectByVisibleText(BenefitPeriod);
		report.updateTestLog("Benefit Period", "Benefit Period: " + BenefitPeriod + " is Selected",
				Status.PASS);
		sleep(2000);

		String OwnOccupation = dataTable.getData(DataSheet, "OwnOccupation");

		if ((BenefitPeriod.equalsIgnoreCase("5 Years")) || (BenefitPeriod.equalsIgnoreCase("Age 65"))) {
			// Check box Validation for the member is not a casual or contractor
			// employee

			wait.until(ExpectedConditions
						.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_CasualORContractor)).click();
				report.updateTestLog("Casual or contractor employee",
						"The member is not a casual or contractor employee is selected", Status.PASS);
				sleep(3000);

			// OwnOccupation Validation

			if (occRating3.equalsIgnoreCase("Own Occupation")) {
				WebElement OwnOccYes = driver.findElement(SpecialOfferObject.obj_PersonalDetails_OwnOccupationYes);
				fieldValidation("WebButton", OwnOccYes, "Yes", "Own Occupation");
			} else {
				WebElement OwnOccYes = driver.findElement(SpecialOfferObject.obj_PersonalDetails_OwnOccupationNo);
				fieldValidation("WebButton", OwnOccYes, "No", "Own Occupation");
			}

			// OwnOccupation selection

				if (OwnOccupation.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_OwnOccupationYes)).click();
					sleep(3000);
					report.updateTestLog("Own Occupation", "Selected Yes", Status.PASS);
				} else if (OwnOccupation.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions
							.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_OwnOccupationNo)).click();
					sleep(3000);
					report.updateTestLog("Own Occupation", "Selected No", Status.PASS);

				} else {
					report.updateTestLog("DataTable", "Invalid value for OwnOccupation", Status.FAIL);
				}



		}


	}

	public void saveAndExitPopUP() {
		if (driver.getPageSource().contains("Your application reference no")) {

			WebElement WErefNo = driver.findElement(SpecialOfferObject.obj_SaveReferenceNo);
			String refNo = WErefNo.getText();

			String[] part = refNo.split("Your application reference no. is ");
			String SE = part[1];

			String[] part1 = SE.split("\\n\\n");
			String ref = part1[0];

			report.updateTestLog("Reference No", "Reference No:" + ref, Status.PASS);
			driver.findElement(SpecialOfferObject.obj_SaveClose).click();
			sleep(2000);
			report.updateTestLog("Finish and close", "Finish and close button is clicked", Status.PASS);
			return;
		} else {
			System.out.println("ref no not present");
			report.updateTestLog("Saved And Exit", "Application Saved pop-up does not appear", Status.FAIL);
		}
	}

	public void decisionPopUP_Negative() {


		if (driver.findElement(SpecialOfferObject.obj_PersonalDetails_CancelMsg).isDisplayed()) {
			// *******Feedback popup******\\
			wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_DecisionPopUP_Continue)).click();
			report.updateTestLog("Feedback", "Selected Continue on the Feedback popup", Status.PASS);
			sleep(2500);
			report.updateTestLog("Application Declined", "Application is declined", Status.PASS);
			confirmation();
		} else {
			report.updateTestLog("Application Declined", "Cancel Msg is not displayed", Status.FAIL);
		}
	}

	public void premiumValidation() {
		String DeathYN = dataTable.getData(DataSheet, "DeathYN");
		String TPDYN = dataTable.getData(DataSheet, "TPDYN");
		String IPYN = dataTable.getData(DataSheet, "IPYN");
		String ExpectedDeathcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedDeathcover");
		String ExpectedTPDcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedTPDcover");
		String ExpectedIPcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedIPcover");
		ExpectedDeathcover = "$" + ExpectedDeathcover;
		ExpectedTPDcover = "$" + ExpectedTPDcover;
		ExpectedIPcover = "$" + ExpectedIPcover;

		// ExpectedDeathcover = amountFormatter(ExpectedDeathcover);
		System.out.println("Formatted amount :" + ExpectedDeathcover);

		if (DeathYN.equalsIgnoreCase("Yes")) {
			WebElement Death = driver.findElement(SpecialOfferObject.obj_PersonalDetails_DeathAmount);
			fieldValidation("Amount Validation", Death, ExpectedDeathcover, "Death Cover Premium");
		}

		if (TPDYN.equalsIgnoreCase("Yes")) {
			WebElement tpd = driver.findElement(SpecialOfferObject.obj_PersonalDetails_TPDAmount);
			fieldValidation("Amount Validation", tpd, ExpectedTPDcover, "TPD Cover Premium");
		}

		if (IPYN.equalsIgnoreCase("Yes")) {
			WebElement ip = driver.findElement(SpecialOfferObject.obj_PersonalDetails_IPAmount);
			fieldValidation("Amount Validation", ip, ExpectedIPcover, "IP Cover Premium");
		}

	}

	public void validateChangeCoverPersonalDetail() {}

	public void validateChangeCoverAURA() {

		String Height = dataTable.getData(DataSheet, "Height");
		String HeightType = dataTable.getData(DataSheet, "HeightType");
		String Weight = dataTable.getData(DataSheet, "Weight");
		String WeightType = dataTable.getData(DataSheet, "WeightType");
		String Smoker = dataTable.getData(DataSheet, "Smoker");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String Pregnant = dataTable.getData(DataSheet, "Pregnant");
		String Exclusion_3Years_Question_for_TPDandIP = dataTable.getData(DataSheet,
				"Exclusion_3Years_Question_for_TPDandIP");
		String Loading_3Years_Questions_for_Death50_TPD50_IP100 = dataTable.getData(DataSheet,
				"Loading_3Years_Questions_for_Death50_TPD50_IP100");
		String Loading_3Years_Questions_for_Death100_TPD100_IP150 = dataTable.getData(DataSheet,
				"Loading_3Years_Questions_for_Death100_TPD100_IP150");
		String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore = dataTable.getData(DataSheet,
				"Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");
		String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP = dataTable.getData(DataSheet,
				"Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
		String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP = dataTable.getData(DataSheet,
				"Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");
		String UsualDoctororMedicalCentre = dataTable.getData(DataSheet, "UsualDoctororMedicalCentre");
		String UsualDoctor_Name = dataTable.getData(DataSheet, "UsualDoctor_Name");
		String UsualDoctor_ContactNumber = dataTable.getData(DataSheet, "UsualDoctor_ContactNumber");
		String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50 = dataTable.getData(DataSheet,
				"Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
		String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP = dataTable.getData(DataSheet,
				"Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
		String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP = dataTable.getData(DataSheet,
				"Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
		String Drugs_Last5Years = dataTable.getData(DataSheet, "Drugs_Last5Years");
		String Alcohol_DrinksPerDay = dataTable.getData(DataSheet, "Alcohol_DrinksPerDay");
		String Alcohol_Professionaladvice = dataTable.getData(DataSheet, "Alcohol_Professionaladvice");
		String HIVInfected = dataTable.getData(DataSheet, "HIVInfected");
		String PriorApplication = dataTable.getData(DataSheet, "PriorApplication");
		String PreviousClaims = dataTable.getData(DataSheet, "PreviousClaims");
		String PreviousClaims_PaidBenefit_terminalillness = dataTable.getData(DataSheet,
				"PreviousClaims_PaidBenefit_terminalillness");
		String CurrentPolicies = dataTable.getData(DataSheet, "CurrentPolicies");
		String DeathAction = dataTable.getData(DataSheet, "DeathAction");
		String TPDAction = dataTable.getData(DataSheet, "TPDAction");
		String IPAction = dataTable.getData(DataSheet, "IPAction");
		String SaveExit_Aura = dataTable.getData(DataSheet, "SaveExit_HealthLifestyle");

		WebDriverWait wait = new WebDriverWait(driver, 20);
		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);

		// What is your height?
		WebElement objHeight = driver.findElement(By.xpath("//*[@ng-model='heightval']"));
		fieldValidation("WebEdit", objHeight, Height, "Height");

		WebElement objHeightType = driver.findElement(By.xpath("//*[@ng-model='heightDropDown']"));
		fieldValidation("WebList", objHeightType, HeightType, "HeightType");

		// What is your weight?
		WebElement objWeight = driver.findElement(By.xpath("//*[@ng-model='weightVal']"));
		fieldValidation("WebEdit", objWeight, Weight, "Weight");

		WebElement objWeightType = driver.findElement(By.xpath("//*[@ng-model='weightDropDown']"));
		fieldValidation("WebList", objWeightType, WeightType, "WeightType");

		// Are you smoker?
		if (Smoker.equalsIgnoreCase("Yes")) {
			WebElement objSmokerYes = driver.findElement(By.xpath(
					"//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objSmokerYes, Smoker, "Smoker");
		} else if (Smoker.equalsIgnoreCase("No")) {
			WebElement objSmokerNo = driver.findElement(By.xpath(
					"//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objSmokerNo, Smoker, "Smoker");
		} else {
			report.updateTestLog("Data Table", " Invalid input for Smoker", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");

		// Headaches or migraines
		if (Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")) {
			WebElement objHeadaches = driver.findElement(By.xpath("//*[text()[contains(.,'Headaches or migraines')]]"));
			fieldValidation("WebCheckBox", objHeadaches, "Checked", "Headaches or migraines");

			// ******Are you currently under investigations or contemplating
			// investigations for your headaches?*****\\
			WebElement objunderI = driver.findElement(By.xpath(
					"//div/label[text()='Are you currently under investigations or contemplating investigations for your headaches?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objunderI, "No", "Under Investigation");

			// ******How would you describe your headaches?******\\
			WebElement objRecurring = driver.findElement(By.xpath("//label[text()='Recurring or severe episodes']"));
			fieldValidation("WebElement", objRecurring, "Recurring or severe episodes", "Recurring or severe episodes");

			// ***Have your headaches been fully investigated with all
			// underlying causes excluded?****\\
			WebElement objHeadachesIn = driver.findElement(By.xpath(
					"//div/label[text()='Have your headaches been fully investigated with all underlying causes excluded?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objHeadachesIn, "Yes", "HeadachesIn");

			// How many headaches do you suffer in a week?
			WebElement objepisodes = driver.findElement(By.xpath("//label[text()='3 episodes or less']"));
			fieldValidation("WebElement", objepisodes, "3 episodes or less", "3 episodes or less");

			// *******Is this easily controlled with over the counter
			// medication?******\\
			WebElement objmedication = driver.findElement(By.xpath(
					"//div/label[text()='Is this easily controlled with over the counter medication?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objmedication, "Yes", "Easily Controlled with Meditation");
		}

		// Lung or breathing conditions
		else if (Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")) {
			WebElement objLung = driver
					.findElement(By.xpath("//*[text()[contains(.,'Lung or breathing conditions')]]/span"));
			fieldValidation("WebCheckBox", objLung, "Checked", "Lung or breathing conditions");
			// Asthma
			WebElement objAsthma = driver.findElement(By.xpath("//*[text()[contains(.,'Asthma')]]"));
			fieldValidation("WebCheckBox", objAsthma, "Checked", "Asthma");
			// ****Is your condition?******\\
			WebElement objModerate = driver.findElement(By.xpath("//*[text()[contains(.,'Moderate')]]"));
			fieldValidation("WebButton", objModerate, "Yes", "Is your condition");
			// ****Is your Asthma worsened by your Occupation****\\

			if (Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")) {
				WebElement objworsened = driver.findElement(By.xpath(
						"//div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[1]/div/div/label"));
				fieldValidation("WebButton", objworsened, "Yes", "Is your Asthma worsened by your Occupation");
			} else if (Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")) {
				WebElement objworsenedNo = driver.findElement(By.xpath(
						"//div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[2]/div/div/label"));
				fieldValidation("WebButton", objworsenedNo, "No", "Is your Asthma worsened by your Occupation");
			}
		}

		else if (Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")
				&& Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")
				&& Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")) {
			WebElement obj3Yrs = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[1]"));
			fieldValidation("WebCheckBox", obj3Yrs, "Checked", "3 YearsQuestion - None of these");
		} else {
			report.updateTestLog("Data Table",
					" Invalid input for Exclusion_3Years_Question_for_TPDandIP / Loading_3Years_Questions_for_Death100_TPD100_IP150",
					Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// 5 years have you suffered from

		if (Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")) {
			WebElement objcholesterol = driver.findElement(By.xpath("//*[text()[contains(.,'High cholesterol')]]"));
			fieldValidation("WebCheckBox", objcholesterol, "Checked", "High cholesterol");

			// How is your high cholesterol being treated?
			WebElement objMedicationPrescribed = driver
					.findElement(By.xpath("//label[text()='Medication prescribed by your doctor']"));
			fieldValidation("WebElement", objMedicationPrescribed, "Medication prescribed by your doctor",
					"How is your high cholesterol being treated?");

			// When did you last have a reading taken?
			WebElement objReading = driver.findElement(By.xpath(
					"//div/label[text()='When did you last have a reading taken?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objReading, "Yes", "when did u taken reading ? 12 months ago");

			// How did your doctor describe your most recent cholesterol
			// reading?
			WebElement objcholesterolReading = driver.findElement(By.xpath(
					"//div/label[text()='How did your doctor describe your most recent cholesterol reading?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objcholesterolReading, "Yes",
					"How did your doctor describe your most recent cholesterol reading?- Elevated");

			// ****Do you recall your last reading?***\\
			WebElement objRecall = driver.findElement(By.xpath(
					"//div/label[text()='Do you recall your last reading?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objRecall, "Yes", "Do you recall your last reading?");
			WebElement objreadingVal = driver.findElement(By.xpath("//label[text()='7.1']"));
			fieldValidation("WebElement", objreadingVal, "7.1", "High cholesterol Value");

			// *****Have you been advised that your triglycerides are
			// elevated?****\\
			WebElement objtriglycerides = driver.findElement(By.xpath(
					"//div/label[text()='Have you been advised that your triglycerides are elevated? ']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objtriglycerides, "No",
					"Have you been advised that your triglycerides are elevated");

		}

		else if (Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")) {
			WebElement obj5Yrs = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[2]"));
			fieldValidation("WebCheckBox", obj5Yrs, "Checked", "5 YearsQuestion - None of these");
		}

		else {
			report.updateTestLog("Data Table",
					" Invalid input for Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore", Status.FAIL);
		}
		sleep(800);
		js.executeScript("window.scrollBy(0,500)", "");

		if (Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")) {
			// Thyroid conditions
			WebElement objThyroid = driver.findElement(By.xpath("//*[text()[contains(.,'Thyroid conditions')]]"));
			fieldValidation("WebCheckBox", objThyroid, "Yes", "Thyroid conditions");
			// *****What was the specific condition*****\\
			WebElement objHyperthyroidism = driver
					.findElement(By.xpath("//*[text()[contains(.,'Hyperthyroidism (Overactive)')]]"));
			fieldValidation("WebCheckBox", objHyperthyroidism, "Yes", "Hyperthyroidism");
			// ****Has your condition been fully investigated?******\\
			WebElement objinvestigated = driver.findElement(By.xpath(
					"//div/label[text()='Has your condition been fully investigated?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objinvestigated, "Yes", "Has your condition been fully investigated");
			// Is your condition fully controlled?
			WebElement objcontrolled = driver.findElement(By.xpath(
					"//div/label[text()='Is your condition fully controlled?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objcontrolled, "No", "Is your condition fully controlled");

		}

		else if (Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")
				&& Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("No")) {
			WebElement objpriTre = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[3]"));
			fieldValidation("WebCheckBox", objpriTre, "Checked", "PriorTreatmentorDiagnosis - None of these");
		}

		else {
			report.updateTestLog("Data Table",
					" Invalid input for Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP / Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP",
					Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// Do you have a usual doctor or medical centre you regularly visit?
		if (UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")) {
			WebElement objdoctor = driver.findElement(By.xpath(
					"(//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div/div/div/label)[1]"));
			fieldValidation("WebButton", objdoctor, UsualDoctororMedicalCentre,
					"Do you have a usual doctor or medical centre you regularly visit");

			// Stevenson
			WebElement objStevenson = driver.findElement(By.xpath("//label[text()='Stevenson']"));
			fieldValidation("WebElement", objStevenson, UsualDoctor_Name, "UsualDoctor_Name");

			// PhoneNumber
			WebElement objreadingVal = driver.findElement(By.xpath("//label[text()='9178563574']"));
			fieldValidation("WebElement", objreadingVal, UsualDoctor_ContactNumber, "UsualDoctor_ContactNumber");

			// Add another Doctor?
			WebElement objAddAnotherDoc = driver.findElement(By.xpath(
					"//div/label[text()='Add another Doctor?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objAddAnotherDoc, "No", "AddAnotherDoc");
		}

		else if (UsualDoctororMedicalCentre.equalsIgnoreCase("No")) {
			WebElement obj5DMCenter = driver.findElement(By.xpath(
					"//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", obj5DMCenter, UsualDoctororMedicalCentre, "Usual Doctoror or Medical Centre");
		}

		else {
			report.updateTestLog("Data Table", " Invalid input for UsualDoctororMedicalCentre", Status.FAIL);
		}
		js.executeScript("window.scrollBy(0,500)", "");
		// Family History
		if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")) {
			WebElement objFamily = driver.findElement(By.xpath(
					"//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objFamily, "Yes", "FamilyHealthHistory");
			// ***Select the health condition****\\
			WebElement objDiabetes = driver.findElement(By.xpath("(//*[text()[contains(.,'Diabetes')]])[2]"));
			fieldValidation("WebCheckBox", objDiabetes, "Checked", "Diabetes");
			// ***How many family members were affected?****\\
			WebElement objAffectedNo = driver.findElement(By.xpath("//label[text()='3']"));
			fieldValidation("WebElement", objAffectedNo, "3", "AffectedNo");
			// ***were two or more family members diagnosed over age 19****\\
			WebElement objfamilymembers = driver.findElement(By.xpath(
					"//div/label[text()='Were two or more family members diagnosed over the age of 19?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objfamilymembers, Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50,
					"were two or more family members diagnosed over age 19");

		}

		else if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("No")) {
			WebElement obj5DMCenter = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[3]"));
			fieldValidation("WebButton", obj5DMCenter, Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50,
					"Family Health History");
		} else if (Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("UnKnown")) {
			WebElement obj5DMCenter = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[3]"));
			fieldValidation("WebButton", obj5DMCenter, Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50,
					"Family Health History");
		} else {
			report.updateTestLog("Data Table", " Invalid input for FamilyHealthHistory", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// Life Style questions
		// *******Do you have firm plans to travel or reside in another
		// country*******\\
		if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.trim().length() > 0) {
			if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")) {
				WebElement objOtherCountryYes = driver.findElement(By.xpath(
						"//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[1]/div/div/label"));
				fieldValidation("WebButton", objOtherCountryYes, "Yes",
						"travel or reside in another country other than NZ, the United States of America");
			} else if (Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
				WebElement objOtherCountryNo = driver.findElement(By.xpath(
						"//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[2]/div/div/label"));
				fieldValidation("WebButton", objOtherCountryNo, "No",
						"travel or reside in another country other than NZ, the United States of America");
			}

		}

		if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")) {

			// ******Do you regularly engage in or intend to engage in any of
			// the following hazardous activities*****\\
			WebElement ObjWatersports = driver.findElement(By.xpath("//*[text()[contains(.,'Water sports')]]"));
			fieldValidation("WebButton", ObjWatersports, "Yes", "Water sports");
			// UnderWater
			WebElement objUnderwaterdiving = driver
					.findElement(By.xpath("//*[text()[contains(.,'Underwater diving')]]"));
			fieldValidation("WebButton", objUnderwaterdiving, "Yes", "Underwater diving");
			// 40 meters
			WebElement obj40Meters = driver.findElement(By.xpath("//*[text()[contains(.,'More than 40 metres')]]"));
			fieldValidation("WebButton", obj40Meters, "Yes", "40Meters diving");

		}

		else if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
			WebElement objHazAct = driver.findElement(By.xpath("(//*[text()[contains(.,'None of the above')]])[4]"));
			fieldValidation("WebCheckBox", objHazAct, "Checked", "HazardousActivities - None of these");
		}

		else {
			report.updateTestLog("Data Table", " Invalid input for hazardous activities", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// *****Have you within the last 5 years used any drugs that were not
		// prescribed to you (other than those available over the counter)
		// or have you exceeded the recommended dosage for any medication?
		System.out.println("111111111111111111111" + Drugs_Last5Years);
		if (Drugs_Last5Years.equalsIgnoreCase("Yes")) {

			WebElement obj5YearsYes = driver.findElement(By.xpath(
					"//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", obj5YearsYes, Drugs_Last5Years,
					" have you exceeded the recommended dosage for any medication");
		} else if (Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")) {
			WebElement obj5YearsNo = driver.findElement(By.xpath(
					"//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", obj5YearsNo, Drugs_Last5Years,
					" have you exceeded the recommended dosage for any medication");
		}

		else {
			report.updateTestLog("Data Table", " Invalid input for Drugs in Last 5 Years", Status.FAIL);
		}

		// *****How many alcoholic drinks you have in a day?*****\\
		WebElement objalcoholicmany = driver.findElement(By.xpath("//label[text()='1']"));
		fieldValidation("WebElement", objalcoholicmany, "1", "How many alcoholic drinks you have in a day");

		// ****Have you ever been advised by a health professional to reduce
		// your alcohol consumption?****\\
		if (Alcohol_Professionaladvice.equalsIgnoreCase("Yes")) {
			WebElement objprofessionalYes = driver.findElement(By.xpath(
					"//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objprofessionalYes, Alcohol_Professionaladvice,
					"Have you ever been advised by a health professional to reduce your alcohol consumption");
		} else if (Alcohol_Professionaladvice.equalsIgnoreCase("No")) {
			WebElement objprofessionalNo = driver.findElement(By.xpath(
					"//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objprofessionalNo, Alcohol_Professionaladvice,
					"Have you ever been advised by a health professional to reduce your alcohol consumption");
		} else {
			report.updateTestLog("Data Table", " Invalid input for Alcohol_Professionaladvice", Status.FAIL);
		}
		// ******Are you infected with HIV (Human Immunodeficiency Virus******\\
		if (HIVInfected.equalsIgnoreCase("Yes")) {
			WebElement objHIVyes = driver.findElement(By.xpath(
					"//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objHIVyes, HIVInfected,
					"Are you infected with HIV (Human Immunodeficiency Virus");
		} else if (HIVInfected.equalsIgnoreCase("No")) {
			WebElement objHIVNo = driver.findElement(By.xpath(
					"//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objHIVNo, HIVInfected,
					"Are you infected with HIV (Human Immunodeficiency Virus");
			WebElement objreferred = driver.findElement(By.xpath(
					"//div/label[text()='Have you been referred for or waiting on an HIV test result and/or are taking preventative medication?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objreferred, HIVInfected,
					"Have you been referred for or waiting on an HIV test result and/or are taking preventative medication");

		} else {
			report.updateTestLog("Data Table", " Invalid input for HIVInfected", Status.FAIL);
		}

		js.executeScript("window.scrollBy(0,500)", "");
		// General Questions
		WebElement ObjOther = driver.findElement(By.xpath(
				"//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label"));
		fieldValidation("WebButton", ObjOther, "No",
				"Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future");

		// Insurance Details
		// Has an application for Life, Trauma, TPD or Disability Insurance on
		// your life ever been declined
		if (PriorApplication.equalsIgnoreCase("Yes")) {
			WebElement objDisabilityYes = driver
					.findElement(By.xpath("//*[@id='collapseOne']/div/div[1]/div/div[2]/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objDisabilityYes, PriorApplication,
					"Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms");
		} else if (PriorApplication.equalsIgnoreCase("No")) {
			WebElement objDisabilityNo = driver.findElement(By.xpath(
					"//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objDisabilityNo, PriorApplication,
					"Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms");
		} else {
			report.updateTestLog("Data Table", " Invalid input for PriorApplication", Status.FAIL);
		}
		// Are you contemplating or have you ever made a claim for or received
		// sickness
		if (PreviousClaims.equalsIgnoreCase("Yes")) {
			WebElement objcontemplatingYes = driver.findElement(By.xpath(
					"//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[1]/div/div/label"));
			fieldValidation("WebButton", objcontemplatingYes, PreviousClaims,
					"Are you contemplating or have you ever made a claim for or received sickness");
		} else if (PreviousClaims.equalsIgnoreCase("No")) {
			WebElement objcontemplatingNo = driver.findElement(By.xpath(
					"//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[2]/div/div/label"));
			fieldValidation("WebButton", objcontemplatingNo, PreviousClaims,
					"Are you contemplating or have you ever made a claim for or received sickness");
		} else {
			report.updateTestLog("Data Table", " Invalid input for PreviousClaims", Status.FAIL);
		}

	}

	public void validateChangeCoverConfirm() {
		sleep(2000);

		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);

		WebElement objAgree = driver.findElement(By.xpath("//*[@id='generalConsentLabel']"));
		fieldValidation("WebCheckBox", objAgree, "UnChecked", "Agree");
		detailsValidation();

	}

	public static double round(double value, int numberOfDigitsAfterDecimalPoint) {
		BigDecimal bigDecimal = new BigDecimal(value);
		bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint,
				BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
	}


	public void cover_Validations() {



		String CoverValidation = dataTable.getData(DataSheet, "CoverValidation");
		String DeathUnits = dataTable.getData(DataSheet, "DeathUnits");
		String TPDUnits = dataTable.getData(DataSheet, "TPDUnits");
		String DeathError = dataTable.getData(DataSheet, "DeathError");
		String TPDError = dataTable.getData(DataSheet, "TPDError");

		// To Select - Extra cover required
		if (DeathUnits.equalsIgnoreCase("0")) {
			wait.until(ExpectedConditions
					.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Death0Uunit)).click();
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
			sleep(1000);

		}

		else if (DeathUnits.equalsIgnoreCase("1")) {
			wait.until(ExpectedConditions
					.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Death1Uunit)).click();
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - 1 Unit is selected", Status.PASS);
			sleep(1000);

		} else if (DeathUnits.equalsIgnoreCase("2")) {
			wait.until(ExpectedConditions
					.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_Death2Uunit)).click();
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);
			sleep(1000);

		} else {
			report.updateTestLog("Data Table", "Invalid input for DeathUnits in Data Sheet. Please enter 0/1/2",
					Status.FAIL);
		}




	// *******TPD Cover Section******\\
	sleep(800);



		// To Select - Extra cover required
		if (TPDUnits.equalsIgnoreCase("0")) {
			wait.until(ExpectedConditions
					.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_TPD0Uunit)).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
			sleep(1000);

		}

		else if (TPDUnits.equalsIgnoreCase("1")) {
			wait.until(ExpectedConditions
					.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_TPD1unit)).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 1 Unit is selected", Status.PASS);
			sleep(1000);

		} else if (TPDUnits.equalsIgnoreCase("2")) {
			wait.until(ExpectedConditions
					.elementToBeClickable(SpecialOfferObject.obj_PersonalDetails_TPD2Uunit)).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 2 Unit is selected", Status.PASS);
			sleep(1000);

		} else {
			report.updateTestLog("Data Table", "Invalid input for TPDUnits in Data Sheet. Please enter 0/1/2",
					Status.FAIL);
		}





		if ( CoverValidation.equalsIgnoreCase("WaitingPeriod-BenifitPeriod") ) {

			String[] WP = { "30 Days", "60 Days", "90 Days" };
			String[] BP = { "2 Years", "5 Years" };

			WebElement dropdownWP = driver.findElement(SpecialOfferObject.obj_PersonalDetails_WaitingPeriod);
			WebElement dropdownBP = driver.findElement(SpecialOfferObject.obj_PersonalDetails_BenefitPeriod);

			verify_Dropdown(dropdownWP, WP, "Waiting Period");
			verify_Dropdown(dropdownBP, BP, "Benefit Period");
			return;

		}

		else if ( CoverValidation.equalsIgnoreCase("TPD>Death") ) {

			Integer DU = Integer.parseInt(DeathUnits);
			Integer TPDU = Integer.parseInt(TPDUnits);

			if (TPDU>DU) {


			if (driver.getPageSource().contains(TPDError)) {
				report.updateTestLog("TPD greater than Death Validation",
						"Error Message "+TPDError+" is displayed",
						Status.PASS);

			return;

			} else {
				report.updateTestLog("TPD greater than Death Validation",
						"Error Message "+TPDError+" is not displayed",
						Status.FAIL);
			}

			}
			else {
				report.updateTestLog("TPD greater than Death Validation",
						"Death Value is not greater than TPD value",
						Status.FAIL);
			}
			return;

		}
		else if ( CoverValidation.equalsIgnoreCase("TPD without Death") ) {
			Integer DU = Integer.parseInt(DeathUnits);
			if (DU==0) {
			if (driver.getPageSource().contains(TPDError)) {
				report.updateTestLog("TPD without Death Validation",
						"Error Message "+TPDError+" is displayed",
						Status.PASS);
			} else {
				report.updateTestLog("TPD without Death Validation",
						"Error Message "+TPDError+" is not displayed",
						Status.FAIL);
			}
			}
			else {
				report.updateTestLog("TPD without Death Validation",
						"Death unit is not 0",
						Status.FAIL);
			}
			return;

		}

	}
	public void verify_Dropdown(WebElement dropdown, String[] dropDownOptions, String field) {
		String input = null;
		Select select = new Select(dropdown);
		int index = 0;
		List<WebElement> options = select.getOptions();
		for (WebElement we : options) {
			index++;
			// for (int i=0; i<exp.length(); i++){
			String s = we.getText();
			System.out.println("Option of webpase index " + index + " is " + s);

			if (s.equals(dropDownOptions[index - 1])) {
				System.out.println("Option " + index + " is" + s);
				input = input+","+s;
			} else {
				System.out.println("Invalid options");
				report.updateTestLog("DropDowm Validation", "Invalid Options in DropDown "+field, Status.FAIL);
				break;
			}
			// }

		}
		report.updateTestLog("DropDowm Validation", "DropDown -"+field+" contains the Given Values ="+input, Status.PASS);

	}
}
