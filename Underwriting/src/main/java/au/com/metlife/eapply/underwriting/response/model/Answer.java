
/**-------------------------------------------------------------------------------------------------------
 * 
 *--------------------------------------------------------------------------------------------------------
 * Copyright 2009 MetLife, Inc.
 *--------------------------------------------------------------------------------------------------------
 * Author :177727
 * Created:Oct 8, 2009
 *--------------------------------------------------------------------------------------------------------
 * Modification Log:
 *--------------------------------------------------------------------------------------------------------
 * Date(MM/DD/YYYY)		Author :				 Description
 *--------------------------------------------------------------------------------------------------------	 	
 */
package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Answer implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private boolean accepted;
	
	private List<TransferObject> vectorQuestions;
	
	private String answerText;

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public List<TransferObject> getVectorQuestions() {
		if(null == vectorQuestions){
			vectorQuestions = new ArrayList<TransferObject>();
		}
		return vectorQuestions;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
}
