package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.STFTCCGetQuotePageObjects;
import uimap.metlife.STFTGetQuotePageObjects;

public class STFTCCGetQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTCCGetQuotePage getQuote() {
		premiumCalculation();
		return new STFTCCGetQuotePage(scriptHelper);
	}

	public STFTCCGetQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void premiumCalculation() {

		String strIndustry = dataTable.getData("MetLife_Data", "Industry");
		String stroccupation = dataTable.getData("MetLife_Data", "Occupation");
		String strlifeCoverAmt = dataTable.getData("MetLife_Data", "LifeCoverAmount");
		String strTotalCoverAmt = dataTable.getData("MetLife_Data", "TotalCoverAmount");
		try {

			Thread.sleep(2000);
			driver.manage().window().maximize();
			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
						
			tryAction(fluentWaitElement(STFTCCGetQuotePageObjects.selIndustry), "specialDropdown", "Industry",strIndustry);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(STFTCCGetQuotePageObjects.selOccupation), "specialDropdown", "Occupation",stroccupation);		
			Thread.sleep(1000);			
			tryAction(fluentWaitElement(STFTCCGetQuotePageObjects.txtLifeCoverAmt), "ENTER", "Cover Amount");			
			tryAction(fluentWaitElement(STFTCCGetQuotePageObjects.txtLifeCoverAmt), "SET", "Cover Amount", strlifeCoverAmt);
			Thread.sleep(1000);
			tryAction(waitForClickableElement(STFTCCGetQuotePageObjects.txtTotalCoverAmt), "ENTER", "Total Amount");
			Thread.sleep(1000);
			tryAction(waitForClickableElement(STFTCCGetQuotePageObjects.txtTotalCoverAmt), "SET", "Total Amount", strTotalCoverAmt);
			Thread.sleep(1000);			
			tryAction(waitForClickableElement(STFTCCGetQuotePageObjects.txtTotalCoverAmt), "TAB", "Total Amount");
			Thread.sleep(1000);
			
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTCCGetQuotePageObjects.btnCalculateQuote)).click().perform();
			tryAction(waitForClickableElement(STFTCCGetQuotePageObjects.btnCalculateQuote), "Click", "Apply");
			Thread.sleep(10000);

			sleep();
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(driver.findElement(STFTCCGetQuotePageObjects.btnApply)).click().perform();
			Thread.sleep(10000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
