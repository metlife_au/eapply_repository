package au.com.metlife.eapply.bs.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
public interface  EapplicationRepository extends JpaRepository<Eapplication, String>{
	public List<Eapplication> findById(Long id);
	public List<Eapplication> findByApplicationumber(String applicationumber);	
	@Query("SELECT t FROM Eapplication t where t.id = ?1 AND t.applicationstatus = ?2")
    public List<Eapplication> findByEidAndStatus(String eapplicationID,String status);
	public Eapplication findByIdAndApplicationstatusAndPartnercodeAndRequesttype(Long id,String applicationstatus,String partnercode,String requesttype);
	public Eapplication findByIdAndPartnercode(Long id,String partnercode);
	@Query("SELECT t FROM Eapplication t where t.id = ?1 AND t.applicationstatus = ?2 AND t.lastupdatedate >=?3")
	public Eapplication findByIdAndStatusAndLastUpdateDate(Long id,String applicationstatus,Date lastUpdateDate);



}