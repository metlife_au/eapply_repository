package au.com.metlife.eapply.bs.bizflowmodel;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Party")

@XmlRootElement
public class Party {
	
	@XmlAttribute(name="key")
	 String key;
	
	private String PartyType;
	private String PartyUID;
	private String LastName ;
	private String FirstName ;
	private String PrefixCode ;
	private String Gender ;
	private String BirthDate ;
	private String Occupation ;
	private String PartyName ;
	private String PartyFundJoinDate ;
	private String IsRollover ;
	private PartyRole PartyRole ;
	private Health Health;
	private IdentifierList IdentifierList;
	public String getBirthDate() {
		return BirthDate;
	}
	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}
	public String getPartyFundJoinDate() {
		return PartyFundJoinDate;
	}
	public void setPartyFundJoinDate(String partyFundJoinDate) {
		PartyFundJoinDate = partyFundJoinDate;
	}
	public IdentifierList getIdentifierList() {
		return IdentifierList;
	}
	public void setIdentifierList(IdentifierList identifierList) {
		IdentifierList = identifierList;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public Health getHealth() {
		return Health;
	}
	public void setHealth(Health health) {
		Health = health;
	}
	public String getIsRollover() {
		return IsRollover;
	}
	public void setIsRollover(String isRollover) {
		IsRollover = isRollover;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getOccupation() {
		return Occupation;
	}
	public void setOccupation(String occupation) {
		Occupation = occupation;
	}
	public String getPartyName() {
		return PartyName;
	}
	public void setPartyName(String partyName) {
		PartyName = partyName;
	}
	public PartyRole getPartyRole() {
		return PartyRole;
	}
	public void setPartyRole(PartyRole partyRole) {
		PartyRole = partyRole;
	}
	public String getPartyType() {
		return PartyType;
	}
	public void setPartyType(String partyType) {
		PartyType = partyType;
	}
	public String getPartyUID() {
		return PartyUID;
	}
	public void setPartyUID(String partyUID) {
		PartyUID = partyUID;
	}
	public String getPrefixCode() {
		return PrefixCode;
	}
	public void setPrefixCode(String prefixCode) {
		PrefixCode = prefixCode;
	}
	
	

	
	
}
