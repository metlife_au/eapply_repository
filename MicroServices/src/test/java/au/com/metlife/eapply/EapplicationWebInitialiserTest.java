package au.com.metlife.eapply;

import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.finsyn.acuritywebservices.services.InsuranceBenefitDetailsInput;
import au.com.finsyn.acuritywebservices.services.InsurancePolicyDetailsInput;
import au.com.finsyn.acuritywebservices.services.ObjectFactory;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetails;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetailsInput;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetailsResponse;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.utility.EapplyHelper;
import au.com.metlife.eapply.webservices.EapplicationWebServiceInitializer;

public class EapplicationWebInitialiserTest {
	
	@Test
	public void shouldGeneraterecordSFPSRecord() throws Exception {
		//Given
		//String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		String fileName = "eapplySubmitSfps.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		//When
		EapplicationWebServiceInitializer eapplicationWebServiceInitializer = new EapplicationWebServiceInitializer();
		eapplicationWebServiceInitializer.doInitStatewide(eapplyInput);
	}
	
	
	@Test
	public void shouldGenerateStatewideSuccessResponse() throws DatatypeConfigurationException, JAXBException, FileNotFoundException, IOException, ParseException, java.text.ParseException {
		String fileName = "eapplySubmitSfps.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		
		Eapplication eapplication = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		 EapplyHelper.setdefaultDecision(eapplication, eapplyInput);
		 //Work rating maintained
		 if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 eapplication.setAppdecision("ACC");
		 }
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision(eapplication.getAppdecision());
		 }		
		//When
		EapplicationWebServiceInitializer eapplicationWebServiceInitializer = new EapplicationWebServiceInitializer();
		EappHistory eappHistory = eapplicationWebServiceInitializer.statewidePostback(eapplyInput, eapplication);
		assertNotNull(eappHistory.getJobnumber());
	}
	
	@Test
	public void shouldGenerateStatewideOccUpgSuccessResponse() throws DatatypeConfigurationException, JAXBException, FileNotFoundException, IOException, ParseException, java.text.ParseException {
		String fileName = "eApplyOccUpgSFPS.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		
		Eapplication eapplication = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		 EapplyHelper.setdefaultDecision(eapplication, eapplyInput);
		 //Work rating maintained
		 if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 eapplication.setAppdecision("ACC");
		 }
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision(eapplication.getAppdecision());
		 }		
		//When
		EapplicationWebServiceInitializer eapplicationWebServiceInitializer = new EapplicationWebServiceInitializer();
		EappHistory eappHistory = eapplicationWebServiceInitializer.statewidePostback(eapplyInput, eapplication);
		assertNotNull(eappHistory.getJobnumber());
	}
	
	@Test
	public void shouldGenerateStatewideConvertMaintainSuccessResponse() throws DatatypeConfigurationException, JAXBException, FileNotFoundException, IOException, ParseException, java.text.ParseException {
		String fileName = "eApplySubmitConvertMaintain.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;		
		EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
		
		Eapplication eapplication = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
		 EapplyHelper.setdefaultDecision(eapplication, eapplyInput);
		 //Work rating maintained
		 if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision("ACC");
			 eapplication.setAppdecision("ACC");
		 }
		 if(eapplyInput.getOverallDecision()==null){
			 eapplyInput.setOverallDecision(eapplication.getAppdecision());
		 }		
		//When
		EapplicationWebServiceInitializer eapplicationWebServiceInitializer = new EapplicationWebServiceInitializer();
		EappHistory eappHistory = eapplicationWebServiceInitializer.statewidePostback(eapplyInput, eapplication);
		assertNotNull(eappHistory.getJobnumber());
	}
	
	
	
	@Test
	public void generateSFPSRecord() throws Exception {
		//Given
		GregorianCalendar gc = new GregorianCalendar();
		DatatypeFactory dtf = DatatypeFactory.newInstance();
		XMLGregorianCalendar xgc = dtf.newXMLGregorianCalendar(gc);
		ObjectFactory factory = new ObjectFactory();
		UpdateInsuranceDetails updateInsuranceDetails = new UpdateInsuranceDetails();
		UpdateInsuranceDetailsInput updateInsuranceDetailsInput = new UpdateInsuranceDetailsInput();
		updateInsuranceDetails.setEndUser("AWS");
		
		InsurancePolicyDetailsInput insurancePolicyDetailsInput = new InsurancePolicyDetailsInput();
		insurancePolicyDetailsInput.setPLcAtWorkOnJoin("Y");
		insurancePolicyDetailsInput.setPLdCommencement(xgc);
		insurancePolicyDetailsInput.setPLlPolicyNumber(73392L);
		insurancePolicyDetailsInput.setPLzClient("451059");
		insurancePolicyDetailsInput.setPLzFund("SWAC");
		insurancePolicyDetailsInput.setPLzInsurer("METLFE");
		insurancePolicyDetailsInput.setPLzMember("026445");	
		updateInsuranceDetailsInput.setInsurancePolicyDetailsInput(factory.createUpdateInsuranceDetailsInputInsurancePolicyDetailsInput(insurancePolicyDetailsInput));
				
		
		InsuranceBenefitDetailsInput insuranceBenefitDetailsInput = new InsuranceBenefitDetailsInput();
		insuranceBenefitDetailsInput.setPIdEffective(xgc);
		insuranceBenefitDetailsInput.setPIfDollarDefault(new BigDecimal(700000));
		insuranceBenefitDetailsInput.setPIfUnitsDefault(new BigDecimal(0));
		insuranceBenefitDetailsInput.setPIlPolicyNumber(73392L);
		insuranceBenefitDetailsInput.setPIzBenefitFeature("F");
		insuranceBenefitDetailsInput.setPIzBenefitType("D");
		insuranceBenefitDetailsInput.setPIzFinancialUwrite("P");
		insuranceBenefitDetailsInput.setPIzInsuranceCat("ES");
		insuranceBenefitDetailsInput.setPIzOccupation("S");
		insuranceBenefitDetailsInput.setPIzStatus("PE");		
		updateInsuranceDetailsInput.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInput);
		
		insuranceBenefitDetailsInput = new InsuranceBenefitDetailsInput();
		insuranceBenefitDetailsInput.setPIdEffective(xgc);
		insuranceBenefitDetailsInput.setPIfDollarDefault(new BigDecimal(700000));
		insuranceBenefitDetailsInput.setPIfUnitsDefault(new BigDecimal(0));
		insuranceBenefitDetailsInput.setPIlPolicyNumber(73392L);
		insuranceBenefitDetailsInput.setPIzBenefitFeature("F");
		insuranceBenefitDetailsInput.setPIzBenefitType("T");
		insuranceBenefitDetailsInput.setPIzFinancialUwrite("P");
		insuranceBenefitDetailsInput.setPIzInsuranceCat("ES");
		insuranceBenefitDetailsInput.setPIzOccupation("S");
		insuranceBenefitDetailsInput.setPIzStatus("PE");		
		updateInsuranceDetailsInput.getInsuranceBenefitDetailsInput().add(insuranceBenefitDetailsInput);
		
		updateInsuranceDetails.setUpdateInsuranceDetailsInput(updateInsuranceDetailsInput);
		//When
		SoapClient soapClient = new SoapClient();
		UpdateInsuranceDetailsResponse updateInsuranceDetailsResponse = soapClient.callStateWideService(updateInsuranceDetails);				
		List<Long> jobNumberList = updateInsuranceDetailsResponse.getReturn().getRecordID();
		StringBuffer jobStringBuff = new StringBuffer();
		for (Iterator iterator = jobNumberList.iterator(); iterator.hasNext();) {
			Long long1 = (Long) iterator.next();
			jobStringBuff.append((Long) iterator.next());
			jobStringBuff.append(", ");
			
		}				
		System.out.println(jobStringBuff.toString());
	}

}
