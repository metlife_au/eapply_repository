/*Change cover decision page controllers starts*/
(function(angular) {
    'use strict';
    /* global channel, inputData, token, moment */
    /**
     * @ngdoc controller
     * @name guildApp.controller:nonMemberAcceptController
     *
     * @description
     * This is nonMemberAcceptController description
     */
    angular
	.module('guildApp')
	.controller('nonMemberAcceptController', nonMemberAcceptController);
    nonMemberAcceptController.$inject=['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService','appData','fetchAppNumberSvc'];
    function nonMemberAcceptController($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService,appData,fetchAppNumberSvc){
    	var vm = this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
    	 $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};
      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'>' + 
                '<div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div>' + 
                ' </div></div></br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'>' + 
                '<button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };

      /*NPS survey popup*/

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p>' + 
       		'<div id=\'tips_text\'><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'>' + 
       		'<button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button> <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      				var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});

        vm.downloadPDF = function(){
      		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
      		var a = document.createElement('a');
      	    document.body.appendChild(a);
      	  DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
      			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
       	           window.navigator.msSaveBlob(res.data.response,filename);
       	       }else{
      	        var fileURL = URL.createObjectURL(res.data.response);
      	        a.href = fileURL;
      	        a.download = filename;
      	        a.click();
       	       }
      		}, function(err){
      			console.log('Error downloading the PDF ' + err);
      		});
      	};
    }
    
    angular
	.module('guildApp')
	.controller('nonMemberDeclineController', nonMemberDeclineController);
    nonMemberDeclineController.$inject=['$scope','$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','urlService','appData','fetchAppNumberSvc'];
    function nonMemberDeclineController($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService,appData,fetchAppNumberSvc){
    	var vm=this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
    	 $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};

      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom  \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div>' + 
                '</br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> ' + 
                '<button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };
     // NPS survey popup

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div>' + 
       		'<div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom  \'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button>' + 
       		' <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      			  var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      			  	console.log(url);
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});
        
    }
    
    angular
	.module('guildApp')
	.controller('nonMemberRUWController', nonMemberRUWController);
    nonMemberRUWController.$inject=['$scope', '$rootScope', '$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService','appData','fetchAppNumberSvc'];
    function nonMemberRUWController($scope, $rootScope, $location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService,appData,fetchAppNumberSvc){
    	var vm =this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
      $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};

      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div> ' + 
                '</br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button>' + 
                ' <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };

     // NPS survey popup

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>' + 
       		'<p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button> ' + 
       		'<button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      				 var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});

      	vm.downloadPDF = function(){
      		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
      		var a = document.createElement('a');
      	    document.body.appendChild(a);
      	    DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
      	       if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
      	           window.navigator.msSaveBlob(res.data.response,filename);
      	       }else{
      	    	var fileURL = URL.createObjectURL(res.data.response);
       	        a.href = fileURL;
       	        a.download = filename;
       	        a.click();
      	       }

      		}, function(err){
      			console.log('Error downloading the PDF ' + err);
      		});
      	};
    }
    
    angular
	.module('guildApp')
	.controller('nonMemberDecisionSpclTerm', nonMemberDecisionSpclTerm);
    nonMemberDecisionSpclTerm.$inject=['$scope','$rootScope','$location','PersistenceService', 'DownloadPDFService','ngDialog','$timeout','$window','urlService','appData','fetchAppNumberSvc'];
    function nonMemberDecisionSpclTerm($scope,$rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,urlService,appData,fetchAppNumberSvc){
    	var vm=this;
    	vm.urlList = urlService.getUrlList();
    	var pdfLocation = appData.getPDFLocation();
    	var npsTokenUrl = appData.getNpsUrl();
    	 $rootScope.$broadcast('enablepointer');
    	vm.go = function ( path ) {
      	  $location.path( path );
      	};

      	vm.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      	vm.navigateToLandingPage = function (){
      		ngDialog.openConfirm({
                template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'>' + 
                '<div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ' + 
                'ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                plain: true,
                className: 'ngdialog-theme-plain custom-width'
            }).then(function(){
            	$location.path('/landing');
            }, function(e){
            	if(e==='oncancel'){
            		return false;
            	}
            });
        };

     // NPS survey popup

        ngDialog.openConfirm({
       		template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'> We value your feedback</h4></div><div class=\'modal-body\'>' + '<!-- Row starts --><div class=\'row  rowcustom \'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>  <p></p>  </div></div><!-- Row ends --> ' + 
       		'</div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"confirm(\'onYes\')\">Yes</button> <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\"closeThisDialog(\'oncancel\')\">No</button></div></div>',
     		className: 'ngdialog-theme-plain custom-width',
     		plain: true,
       	}).then(function (value) {
       		if(value==='onYes'){
      			if(npsTokenUrl != null){
      			    var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
      		        $window.open(url, '_blank');
      		        return true;
      			}
      		}
       	},function(value){
       		if(value === 'oncancel'){
       			return false;
       		}
       	});

        vm.downloadPDF = function(){
      		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
      		var a = document.createElement('a');
      	    document.body.appendChild(a);
      	    DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
      			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
       	           window.navigator.msSaveBlob(res.response,filename);
       	       }else{
      	        var fileURL = URL.createObjectURL(res.data.response);
      	        a.href = fileURL;
      	        a.download = filename;
      	        a.click();
       	       }
      		}, function(err){
      			console.log('Error downloading the PDF ' + err);
      		});
      	};
    }
    
})(angular);