package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;

public class DisclosureStatement implements Serializable{
private String statement;
    
    private String answer;
    
    private String ackText;
    
    public String getAckText() {
		return ackText;
	}

	public void setAckText(String ackText) {
		this.ackText = ackText;
	}

	public String getAnswer() {

        return answer;
    }
    
    public void setAnswer(String answer) {

        this.answer = answer;
    }
    
    public String getStatement() {

        return statement;
    }
    
    public void setStatement(String statement) {

        this.statement = statement;
    }
}
