package au.com.metlife.eapply.ct.webserivce;


import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import au.com.metlife.eapply.ct.exception.MetlifeException;
import au.com.metlife.webservices.PicsLodgementStub;


public class ELodgementResponseObjects  {
	private static final Logger log = LoggerFactory.getLogger(ELodgementResponseObjects.class);

	/*private static final String PACKAGE_NAME = ELodgementResponseObjects.class
			.getPackage().getName();

	private static final String CLASS_NAME = ELodgementResponseObjects.class
			.getName();*/

	/*private static final String USERNAME = "picstest";"internal01";ConfigurationHelper.getConfigurationValue("WebServiceUName", "WebServiceUName");"internal01";

	private static final String PASSWORD = "metlife123e2e";"metlife890";ConfigurationHelper.getConfigurationValue("WebServicePass", "WebServicePass");"metlife890";
*/
	private PicsLodgementStub.ReturnObject retObj = null;
	
	private XStream stream = new XStream(new DomDriver());
	
	String inputXml="";
	String outputXml="";
	/**
     * Description: This method call the web servise method for create underwriting
     * 
     * @param PicsLodgementStub.LodgeNewUnderwriting lodgeNewUnderwriting
     * 
     * @return PicsLodgementStub.ReturnObject retObj
     * @throws MetlifeException  
     */
/*	public PicsLodgementStub.ReturnObject execLodgeNewUnderwriting(
			PicsLodgementStub.LodgeNewUnderwriting lodgeNewUnderwriting)
			 throws MetlifeException {
		final String METHOD_NAME = "execLodgeNewUnderwriting"; //$NON-NLS-1$
		
		
		PicsLodgementStub.LodgeNewUnderwritingResponse res;

		try {
			ELodgementServiceInitializer eLodgementServiceInitializer = new ELodgementServiceInitializer();
			
			inputXml = stream.toXML(lodgeNewUnderwriting);
			
						res = (eLodgementServiceInitializer.doInitPICS(USERNAME, PASSWORD,))
					.lodgeNewUnderwriting(lodgeNewUnderwriting);
			retObj = res.get_return();
			if(null!= retObj && !retObj.getCode().equalsIgnoreCase("200") && !retObj.getCode().equalsIgnoreCase("201")
					&& !(null!=retObj.getCaseID() && retObj.getCaseID().length>=1 && 	null!=retObj.getCaseID()[0] && 	retObj.getCaseID()[0].trim().length()>0)		
			){
				MetlifeException metlifeException = new MetlifeException();
				if(null!=retObj.getCode())
				metlifeException.setErrorCode(retObj.getCode());
				if(null!=retObj.getCodeDescription())
				metlifeException.setMsgErrorCode(retObj.getCodeDescription());
				throw metlifeException;
			}
			if(LogHelper.isDebugLogOn())
			{
				if(retObj!=null)
				{
				outputXml=stream.toXML(retObj);
				//LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, outputXml);
				}
			//}

		} catch (Exception e) {	
			//log.info("Actual PICS Error  : {}",retObj.getCode());
			
				e.printStackTrace();
			
			MetlifeException metlifeException = new MetlifeException();	
			
			
			if (e instanceof AxisFault) {
				 metlifeException.setErrorCode("10020");
				 metlifeException.setMsgErrorCode("PICS web service unavailable");	
				
			}else{				
				metlifeException.setErrorCode("10020");
				metlifeException.setMsgErrorCode("PICS web service down");	
				
			}			
			throw metlifeException;	

		}
		
		return retObj;

	}
*/	
	/**
     * Description: This method call the web servise method for create underwriting
     * 
     * @param PicsLodgementStub.LodgeExistingUnderwriting lodgeExistUnderwriting
     * 
     * @return PicsLodgementStub.ReturnObject retObj
     * @throws MetlifeException  
     */
/*	public PicsLodgementStub.ReturnObject execLodgeExistingUnderwriting(
			PicsLodgementStub.LodgeExistingUnderwriting lodgeExistUnderwriting)
	throws MetlifeException {
		final String METHOD_NAME = "execLodgeExistingUnderwriting"; //$NON-NLS-1$
		//LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		PicsLodgementStub.LodgeExistingUnderwritingResponse res;

		try {
			ELodgementServiceInitializer eLodgementServiceInitializer = new ELodgementServiceInitializer();
			
			inputXml = stream.toXML(lodgeExistUnderwriting);
			if(LogHelper.isDebugLogOn())
			{
				LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, inputXml);
			}
			res = (eLodgementServiceInitializer.doInitPICS(USERNAME, PASSWORD))
					.lodgeExistingUnderwriting(lodgeExistUnderwriting);
			
			retObj = res.get_return();			
			
			if(null!= retObj && !retObj.getCode().equalsIgnoreCase("200") && !retObj.getCode().equalsIgnoreCase("201")
			&& !(null!=retObj.getCaseID() && retObj.getCaseID().length>=1 && 	null!=retObj.getCaseID()[0] && 	retObj.getCaseID()[0].trim().length()>0)
			){
				MetlifeException metlifeException = new MetlifeException();
				if(null!=retObj.getCode())
				metlifeException.setErrorCode(retObj.getCode());
				if(null!=retObj.getCodeDescription())
				metlifeException.setMsgErrorCode(retObj.getCodeDescription());
				throw metlifeException;
			}
			if(LogHelper.isDebugLogOn())
			{
				if(retObj!=null)
				{
				outputXml=stream.toXML(retObj);
				//LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, outputXml);
				}
			//}

		} catch (Exception e) {	
			//if(LogHelper.isDebugLogOn()){
				e.printStackTrace();
			//}
			MetlifeException metlifeException = new MetlifeException();	
			if (e instanceof AxisFault) {
				 metlifeException.setErrorCode("10020");
				 metlifeException.setMsgErrorCode("PICS web service unavailable");	
				// LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, e.toString());
			}else{				
				metlifeException.setErrorCode("10020");
				metlifeException.setMsgErrorCode("PICS web service down");	
				//LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, e.toString());
			}
			throw metlifeException;

		}
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return retObj;

	}
*/	
	/**
     * Description: This method call the web servise method for create underwriting
     * 
     * @param PicsLodgementStub.LodgeNewClaims lodgeNewClaims
     * 
     * @return PicsLodgementStub.ReturnObject retObj
     * @throws MetlifeException  
     */
/*	public PicsLodgementStub.ReturnObject execLodgeNewClaims(
			PicsLodgementStub.LodgeNewClaims lodgeNewClaims) throws MetlifeException {
		final String METHOD_NAME = "execLodgeNewClaims"; //$NON-NLS-1$
		
		PicsLodgementStub.LodgeNewClaimsResponse res;

		try {
			ELodgementServiceInitializer eLodgementServiceInitializer = new ELodgementServiceInitializer();
			inputXml = stream.toXML(lodgeNewClaims);
			
			log.info("inputXml >> {}",inputXml);
		
			res = (eLodgementServiceInitializer.doInitPICS(USERNAME, PASSWORD))
					.lodgeNewClaims(lodgeNewClaims);
			
			retObj = res.get_return();
			if(null!= retObj && !retObj.getCode().equalsIgnoreCase("200") && !retObj.getCode().equalsIgnoreCase("201")
					&& !(null!=retObj.getCaseID() && retObj.getCaseID().length>=1 && 	null!=retObj.getCaseID()[0] && 	retObj.getCaseID()[0].trim().length()>0)		
			){
				MetlifeException metlifeException = new MetlifeException();
				if(null!=retObj.getCode())
				metlifeException.setErrorCode(retObj.getCode());
				if(null!=retObj.getCodeDescription())
				metlifeException.setMsgErrorCode(retObj.getCodeDescription());
				throw metlifeException;
			}
			if(LogHelper.isDebugLogOn())
			{
				if(retObj!=null)
				{
				outputXml=stream.toXML(retObj);
				//LogHelper.debug(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, outputXml);
			}
			//}

		} catch (Exception e) {	
			
			MetlifeException metlifeException = new MetlifeException();	
			if (e instanceof AxisFault) {
				 metlifeException.setErrorCode("10020");
				 metlifeException.setMsgErrorCode("PICS web service unavailable");	
				// LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, e.toString());
			}else{				
				metlifeException.setErrorCode("10020");
				metlifeException.setMsgErrorCode("PICS web service down");	
				//LogHelper.error(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, e.toString());
			}
			throw metlifeException;
		}
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return retObj;

	}
*/	
	/**
     * Description: This method call the web servise method for create underwriting
     * 
     * @param PicsLodgementStub.LodgeExistingClaims lodgeExistingClaims
     * 
     * @return PicsLodgementStub.ReturnObject retObj
     * @throws MetlifeException  
     */
	public PicsLodgementStub.ReturnObject execLodgeExistingClaims(	
	PicsLodgementStub.LodgeExistingClaims lodgeExistingClaims,String userName,String password,String clientConfig,String picsUrl) throws MetlifeException {
		log.info("Execute lodge existing claims start");
		PicsLodgementStub.LodgeExistingClaimsResponse res;

		try {
			ELodgementServiceInitializer eLodgementServiceInitializer = new ELodgementServiceInitializer();
			inputXml = stream.toXML(lodgeExistingClaims);
			

			res = (eLodgementServiceInitializer.doInitPICS(userName, password,clientConfig,picsUrl))
					.lodgeExistingClaims(lodgeExistingClaims);

			
							if(retObj!=null)
							{
							outputXml=stream.toXML(retObj);
							}
						
			retObj = res.get_return();
			if(null!= retObj && !retObj.getCode().equalsIgnoreCase("200") && !retObj.getCode().equalsIgnoreCase("201")
					&& !(null!=retObj.getCaseID() && retObj.getCaseID().length>=1 && 	null!=retObj.getCaseID()[0] && 	retObj.getCaseID()[0].trim().length()>0)		
			){
				MetlifeException metlifeException = new MetlifeException();
				if(null!=retObj.getCode())
				{
				metlifeException.setErrorCode(retObj.getCode());
				}
				if(null!=retObj.getCodeDescription())
				{
				metlifeException.setMsgErrorCode(retObj.getCodeDescription());
				}
				throw metlifeException;
			}

		} catch (Exception e) {	
			
				log.error("Error in execute lodge existing claims: {} ",e);
			
			MetlifeException metlifeException = new MetlifeException();	
			if (e instanceof AxisFault) {
				 metlifeException.setErrorCode("10020");
				 metlifeException.setMsgErrorCode("PICS web service unavailable");	
			}else{				
				metlifeException.setErrorCode("10020");
				metlifeException.setMsgErrorCode("PICS web service down");	
			}
			throw metlifeException;

		}
		log.info("Execute lodge existing claims finish");
		return retObj;

	}

}

