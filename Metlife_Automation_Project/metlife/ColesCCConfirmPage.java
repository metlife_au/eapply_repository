package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCConfirmPageObjects;
import uimap.metlife.ColesConfirmPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesCCConfirmPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	

	public ColesCCConfirmPage enterColesCCPersonalDetails() {
		fillPersonalDetailsCC();
		return new ColesCCConfirmPage(scriptHelper);
	} 

	public ColesCCConfirmPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillPersonalDetailsCC() {
		String title = dataTable.getData("MetLife_Data", "Title");
		String lastName = dataTable.getData("MetLife_Data", "LastName");
		
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			tryAction(fluentWaitElement(ColesCCConfirmPageObjects.selTitle),"DropDownSelect", "Title", title);

			tryAction(waitForClickableElement(ColesCCConfirmPageObjects.txtLastName),
					"SET", "Last Name", lastName);

			tryAction(
					waitForClickableElement(ColesCCConfirmPageObjects.chkkDeclarationAgree),
					"Click", "Agree Declaration");
			sleep(2000);
			tryAction(fluentWaitElement(ColesCCConfirmPageObjects.lnkNext),"Click", "Next");
		
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

}
