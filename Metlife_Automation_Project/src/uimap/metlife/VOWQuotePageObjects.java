package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class VOWQuotePageObjects extends PageObjectLibrary{
		
		public static final By btnCalcQuote = getObjectIdentifaction("vow.webresponse.quote.btn.css");
		public static final By btnProceed= getObjectIdentifaction("vow.webresponse.protect.btn.css");

}
