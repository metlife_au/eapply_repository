package au.com.metlife.eapply.bs.email;
/* -------------------------------------------------------------------------------------------------    
 * Description:   Helper class for generating email template
 * -------------------------------------------------------------------------------------------------
 * Copyright @ 2009 MetLife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :    khirod.panda
 * Created:     April 22,2009
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * Date(MM/DD/YYYY)  july 03,2009        Author anand.kishore Description   code refactoring      
 *          
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.constants.MetLifeEmailConstants;
 

public class MetLifeEmailTempletHelper {
	private static final Logger log = LoggerFactory.getLogger(MetLifeEmailTempletHelper.class);
	/*BEGIN_MARKER*/
	public static final String BEGIN_MARKER = "<%";
	/*END_MARKER*/
	public static final String END_MARKER = "%>";
	/*emailTempHelper*/
	private static MetLifeEmailTempletHelper emailTempHelper = null;
	/*constructor*/
	private MetLifeEmailTempletHelper() {
	}
	
	public static MetLifeEmailTempletHelper getInstance() {
		if (emailTempHelper == null)
			emailTempHelper = new MetLifeEmailTempletHelper();
		return emailTempHelper;
	}

	/**
     * Description: This method defines indexes for email template
     * 
     * @param String word
     * 
     * @return List
     * @throws Exception 
     */
	private ArrayList getDataElementKeys(String word){
		log.info("Get data element keys start");
		ArrayList listOfHolders = new ArrayList();
		try {
			int beginIndex = 0;
			int endIndex = 0;
			int times = 0;
			String val = null;
			while (true) {
				beginIndex = word.indexOf(BEGIN_MARKER, beginIndex);
				if (beginIndex == -1) {
					break;
				} else {
					beginIndex += 2;
				}
				endIndex = word.indexOf(END_MARKER, beginIndex);
				if (endIndex == -1)
					break;

				try {
					val = word.substring(beginIndex, endIndex);
					if (val != null && !(val.trim().equals(""))) {
						listOfHolders.add(val);
					}
				} catch (Exception e) {
					log.error("Error in getting data keys: {}",e);
					break;
				}

				
				times++;
				if (times > 20)
					break;

			}

		} catch (Exception e) {
			log.error("Error in getting data keys: {}",e);
			throw e;
		}
		log.info("Get data element keys finish");
		return listOfHolders;
	}	
	/**
     * Description: This method will be reffered by EmailService for replacing the data
	 * element keys with data element values
     * 
     * @param String word
     * 
     * @return List
     * 
     */
	public String getPopulatedTemplate(File tempFile, Map valForPlHolders) {
		log.info("Populate template start");
		StringBuilder contents = new StringBuilder();
		if(tempFile.exists())
		{
		try (FileReader tempFileReader = new FileReader(tempFile);
				BufferedReader input = new BufferedReader(tempFileReader);){
			String line = null; /* not declared within while loop*/
			
			while ((line = input.readLine()) != null) {
				
				if (!line.trim().equals(MetLifeEmailConstants.EMPTY)) {
					line = getUpdatedLine(line, valForPlHolders);
				}
				contents.append(line);
				
				contents.append(System.getProperty(MetLifeEmailConstants.LINE_SEPERATOR));
			}
			
		}  catch (FileNotFoundException ex) {
			log.error("Error in File: {}",ex.getMessage());
		} catch (IOException ex) {
			log.error("Error in Input output: {}",ex.getMessage());
		}
		}
		log.info("Populate template finish");
		return contents.toString();
	}	
	/**
     * Description:  This function will be reffered internally ,for the given line which was
	 * read from the template file this function replaces the data element key
	 * with data element value, if value not found updates with null value
     * 
     * @param String line, HashMap valForPlHolders
     * 
     * @return String
     * 
     */
	private String getUpdatedLine(String line, Map valForPlHolders) {
		log.info("Getting updated line start");
		StringBuilder buff = null;
		String keyVal = null;
		String valToReplace = null;
		String lines = line;
		try {
			if (lines.indexOf(BEGIN_MARKER) == -1)
				return lines;
			ArrayList list = getDataElementKeys(lines);
			for (int i = 0; i < list.size(); i++) {
				buff = new StringBuilder(BEGIN_MARKER);
				keyVal = (String) list.get(i);
				buff.append(keyVal);
				buff.append(END_MARKER);
				if (valForPlHolders.containsKey(keyVal)) {
					valToReplace = (String) valForPlHolders.get(keyVal);
					if(valToReplace!=null){
						lines = lines.replaceAll(buff.toString(), valToReplace);
					}else{
						lines = lines.replaceAll(buff.toString(), "");
					}
				
				} else {
					lines = lines.replaceAll(buff.toString(), MetLifeEmailConstants.NULL);
				}
			}

		} catch (Exception e) {
			log.error("Error in updated line: {}",e);
		}		
		log.info("Getting updated line finish");
		return lines;
	}

}