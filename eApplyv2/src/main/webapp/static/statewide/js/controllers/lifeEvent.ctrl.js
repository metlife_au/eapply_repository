/* Transfer Cover Controller,Progressive and Mandatory validations Starts  */
StateWideApp.controller('lifeevent',['$scope','$rootScope', '$routeParams','$location','$timeout','$window','persoanlDetailService','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'NewOccupationService','auraInputService','PersistenceService','ngDialog','auraResponseService','Upload','urlService','saveEapply','RetrieveAppDetailsService','CalculateService','tokenNumService','$q','APP_CONSTANTS', 
                                         function($scope,$rootScope, $routeParams,$location,$timeout,$window,persoanlDetailService,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService, NewOccupationService, auraInputService,PersistenceService,ngDialog,auraResponseService,Upload,urlService,saveEapply,RetrieveAppDetailsService,CalculateService,tokenNumService,$q,APP_CONSTANTS){
	$scope.urlList = urlService.getUrlList();
    $scope.phoneNumbrLifeEvent = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
    $scope.emailFormatLifeEvent = APP_CONSTANTS.emailFormat;
    $scope.indexation= {
    		death: false,
    		disable: false
    };
    var incrementLimit = 200000;
    $scope.showhazardousLifeEventQuestion = false;
    $scope.showLifeEventOutsideOffice = false;
    $scope.privacyFlagErr = false;
    $scope.otherOccupationObj = {'lifeEventOtherOccupation': ''};
    $scope.privacyCol = false;
    $scope.contactCol = false;
    $scope.occupationCol = false;
    $scope.lifeEventSectionCol = false;
    $scope.files = [];
    $scope.selectedFile = null;
    $scope.deathCoverLEDetails = deathCoverService.getDeathCover();
	$scope.tpdCoverLEDetails = tpdCoverService.getTpdCover();
	$scope.ipCoverLEDetails = ipCoverService.getIpCover();
    $scope.preferredContactTransOptions = ['Mobile','Office','Home'];
    $scope.regex = /[0-9]{1,3}/;
    $scope.dcTransCoverAmount = 0.00;
	$scope.dcTransCost = 0.00;
	$scope.tpdTransCoverAmount = 0.00;
	$scope.tpdTransCost = 0.00;
	$scope.ipTransCoverAmount = 0.00;
	$scope.ipTransCost = 0.00;
	$scope.totalTransCost = 0.00;
	$scope.transferAckFlag = false;
	$scope.invalidSalAmount = false;
	$scope.fileNotUploadedError = false;
	$scope.fileFormatError = false;
	var allowNext = false;
	$rootScope.$broadcast('enablepointer');
	$scope.eventList = [{
	      "cde": "MARR",
	      "desc": "Marriage",
	      "docDesc": "A marriage that is recognised as valid under the Marriage Act 1961(Cth).",
	      "evidenceHelpText": "A certified copy of the marriage certificate which must be recognised as valid under the Marriage Act 1961(Cth)."
	  			},
	  			{
	      "cde": "BRTH",
	      "desc": "Birth or adoption of a child",
	      "docDesc": "Adopting or becoming the natural parent of a child.",
	      "evidenceHelpText": "A certified copy of the birth certificate or adoption papers."
	  			},
	  			{
	      "cde": "FRST",
	      "desc": "Obtaining a new mortgage or increasing an existing mortgage",
	      "docDesc": "Obtaining either a new mortgage or increasing an existing mortgage on your residence.",
	      "evidenceHelpText": "A certified copy of all the following:"
	  			}/*,
	  			{
	      "cde": "DIVO",
	      "desc": "Divorce",
	      "docDesc": "Divorcing from a spouse.",
	      "evidenceHelpText": "A certified copy of the divorce certificate."
	  			},
	  			{
	      "cde": "DSPO",
	      "desc": "Death of a spouse",
	      "docDesc": "Death of a spouse.",
	      "evidenceHelpText": "A certified copy of the death certification and the provision of sufficient evidence that the deceased person was your spouse."
	  			},
	  			{
	      "cde": "CUGA",
	      "desc": "Completion of an undergraduate degree",
	      "docDesc": "Completing an undergraduate degree at an Australian University.",
	      "evidenceHelpText": "A certified copy of the transcript showing completion of the degree."
	  			},
	  			{
	      "cde": "DCSS",
	      "desc": "Dependent child starts secondary school",
	      "docDesc": "A dependent child starting secondary school.",
	      "evidenceHelpText": "A certified copy of the enrolment letter for the child."
	  			},
	  			{
	      "cde": "BCFM",
	      "desc": "Becoming a carer of an immediate family member",
	      "docDesc": "Becoming a carer of an immediate family member for the first time and being financially responsible for such care and/or are physically providing such care.",
	      "evidenceHelpText": "A signed and dated letter from a Medical Practitioner confirming the following:"
	  			},
	  			{
	      "cde": "NBLO",
	      "desc": "Obtaining a new business loan/increasing an existing business loan",
	      "docDesc": "Obtaining either a new business loan in excess of $100,000 or increasing an existing business loan by at least $100,000 (excluding re-draw and refinancing) on your business.",
	      "evidenceHelpText": "A certified copy of all the loan papers showing the amount of the loan as well as the effective date of the loan. "
	}*/];

    /*Error Flags*/
    $scope.dodFlagErr = null;
    $scope.privacyFlagErr = null;
    
    var deathLEDBCategory, tpdLEDBCategory, ipLEDBCategory;
    var annualSalForTransUpgradeVal;
    var DCTransMaxAmount, TPDTransMaxAmount, IPTransMaxAmount;
   	var mode3Flag = false;
   	var inputDetails = persoanlDetailService.getMemberDetails();
   	$scope.personalDetails = inputDetails.personalDetails;
   	$scope.gender = $scope.personalDetails.gender;
   	var fetchAppnum = true;
   	var appNum = PersistenceService.getAppNumber();
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    var dodCheck;
    var privacyCheck;
    var privacyVal = 0;
    var contactVal = 0;
    var occupationVal = 0;
    var previousSectionVal = 0;
    var transferCoverVal = 0;
    var occupationDetailsLifeEventFormFields = ['areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','annualSalary'];
    var occupationDetailsOtherLifeEventFormFields = ['areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation','annualSalary'];
    var contactDetailsLifeEventFormFields = ['contactDetailsLifeEventEmail', 'contactDetailsLifeEventPhone','contactDetailsLifeEventPrefTime'];
    var lifeEventFields = ['event','eventDate','eventAlreadyApplied','documentName'];
    var lifeEventFieldsWithChkBox = ['event','eventDate','eventAlreadyApplied','documentName','ackDocument2'];
    var uploadedFiles = [];
	var transferAckCheck;
	var ackDocument;
	var unitIncrement = 1;
	var amountIncrement = 0.25;
	var dcCoverAmount = dcWeeklyCost = tpdCoverAmount = tpdWeeklyCost = ipCoverAmount = ipWeeklyCost = totalCost = 0;
	$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
	$scope.preferredContactType = '';
	
    
    QuoteService.getList($scope.urlList.quoteUrl,"SFPS").then(function(res){
    	$scope.IndustryOptions = res.data;
    }, function(err){
    	console.log("Error while getting industry options " + JSON.stringify(err));
    });
    
    if(inputDetails && inputDetails.contactDetails.emailAddress){
		$scope.lifeEventEmail = inputDetails.contactDetails.emailAddress;
	}
	if(inputDetails && inputDetails.contactDetails.prefContactTime){
		if(inputDetails.contactDetails.prefContactTime == "1"){
			$scope.lifeEventTime= "Morning (9am - 12pm)";
		}else{
			$scope.lifeEventTime= "Afternoon (12pm - 6pm)";
		}
	}
	if(inputDetails && inputDetails.contactDetails.prefContact){
		if(inputDetails.contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.lifeEventPhone = inputDetails.contactDetails.mobilePhone;
		}else if(inputDetails.contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home";
			$scope.lifeEventPhone = inputDetails.contactDetails.homePhone;
		}else if(inputDetails.contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work";
			$scope.lifeEventPhone = inputDetails.contactDetails.workPhone;
		}
   }
    
    $scope.setIndexation = function ($event) {
    	$event.stopPropagation();
    	$event.preventDefault();
    	$scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
    	if(!$scope.indexation.death) {
    		$("#indexation-death").parent().removeClass('active');
    		$("#indexation-disable").parent().removeClass('active');
    	}
    };
    
    $scope.getOccupations = function(){
      if($scope.otherOccupationObj)
          $scope.otherOccupationObj.lifeEventOtherOccupation = '';
    	if(!$scope.lifeEventIndustry){
    		$scope.lifeEventIndustry = '';
    	}
    	if($scope.toggleThree)
		{
		$scope.toggleThree(false);
		}
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"SFPS",$scope.lifeEventIndustry).then(function(res){
    		$scope.OccupationList = res.data;
    	}, function(err){
    		console.log("Error while fetching occupation options " + JSON.stringify(err));
    	});
    };
    
    $scope.go = function (path){
  		$location.path(path);
  	};
  	
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home"){
			$scope.lifeEventPhone = inputDetails.contactDetails.homePhone;
		}else if($scope.preferredContactType == "Work"){
			$scope.lifeEventPhone = inputDetails.contactDetails.workPhone;
		}else if($scope.preferredContactType == "Mobile"){
			$scope.lifeEventPhone = inputDetails.contactDetails.mobilePhone;
		}else {
			$scope.lifeEventPhone = '';
		}
	};
	
	if(!($scope.ipCoverLEDetails.waitingPeriod && $scope.ipCoverLEDetails.waitingPeriod !=''))
		{
		$scope.ipCoverLEDetails.waitingPeriod = '45 Days';
		}
	
	if(!($scope.ipCoverLEDetails.benefitPeriod && $scope.ipCoverLEDetails.benefitPeriod !=''))
		{
		$scope.ipCoverLEDetails.benefitPeriod = '2 Years';
		}
	
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
    
    $scope.validCheck = function(){
    	
    	$scope.salMaxlimit = false;
    	$scope.invalidSalAmount = false;
    	if(parseInt($scope.annualSalary) == 0){
  			$scope.invalidSalAmount = true;
  			$scope.toggleThree(false);
  		}
    	else if(parseInt($scope.annualSalary) >= 500001)
    		{
    		$scope.salMaxlimit = true;
    		$scope.toggleThree(false);
    		}
    	
    	else{
    		$scope.invalidSalAmount = false;
    		if(allowNext)
    			{
    		$scope.toggleThree(true);
    			}
  		}
    }
    
    if($scope.deathCoverLEDetails.type == '1'){
	   $scope.deathCoverType = 'DcUnitised';
	} else if($scope.deathCoverLEDetails.type == '2'){
		$scope.deathCoverType = 'DcFixed';
	}
	if($scope.tpdCoverLEDetails.type == '1'){
		$scope.tpdCoverType = 'TPDUnitised';
	} else if($scope.tpdCoverLEDetails.type == '2'){
		$scope.tpdCoverType = 'TPDFixed';
	}
    
  
	$scope.continueToNextPage = function(){

  		var ruleModel = {
         		"age": anb,
         		"fundCode": "SFPS",
         		"gender": $scope.gender,
         		"deathOccCategory": $scope.lifeEventDeathOccCategory,
         		"tpdOccCategory": $scope.lifeEventTpdOccCategory,
         		"ipOccCategory": $scope.lifeEventIpOccCategory,
         		"smoker": false,
         		"deathFixedCost": null,
         		"deathUnitsCost": null,
         		"tpdFixedCost": null,
         		"tpdUnitsCost": null,
         		"ipUnits": parseInt($scope.ipCoverLEDetails.units),
         		"ipUnitsCost": null,
         		"premiumFrequency": "Weekly",
         		"memberType": null,
         		"manageType": "ICOVER",
         		"ipCoverType": "IpUnitised",
         		"ipWaitingPeriod": $scope.ipCoverLEDetails.waitingPeriod,
         		"ipBenefitPeriod": $scope.ipCoverLEDetails.benefitPeriod,
         		"annualSalary":$scope.annualSalary
         	};
  		if(this.contactDetailsLifeEventForm.$valid && this.occupationDetailsLifeEventForm.$valid && this.lifeEvent.$valid){
  		if($scope.deathCoverLEDetails.type == '1'){
 			ruleModel.deathCoverType = 'DcUnitised';
 			if($scope.deathCoverLEDetails.units == undefined || $scope.deathCoverLEDetails.units == ''){
 				ruleModel.deathUnits = 0;
 			} else{
 				ruleModel.deathUnits = parseInt($scope.deathCoverLEDetails.units) + unitIncrement;
 				$scope.deathUnits = ruleModel.deathUnits ;
 			}
 		} else if($scope.deathCoverLEDetails.type == '2'){
 			ruleModel.deathCoverType = 'DcFixed';
 			if($scope.deathCoverLEDetails.amount == undefined || $scope.deathCoverLEDetails.amount == ''){
 				ruleModel.deathFixedAmount = 0;
 			} else{
 				if((parseInt($scope.deathCoverLEDetails.amount)*amountIncrement) < incrementLimit){
 					ruleModel.deathFixedAmount = parseInt($scope.deathCoverLEDetails.amount) + (parseInt($scope.deathCoverLEDetails.amount)*amountIncrement);
 				}else{
 					ruleModel.deathFixedAmount = parseInt($scope.deathCoverLEDetails.amount) + incrementLimit;
 				}

 			}
 		}

  		if($scope.tpdCoverLEDetails.type == '1'){
 			ruleModel.tpdCoverType = 'TPDUnitised';
 			if($scope.tpdCoverLEDetails.units == undefined || $scope.tpdCoverLEDetails.units == ''){
 				ruleModel.tpdUnits = 0;
 			} else{
 				ruleModel.tpdUnits = parseInt($scope.tpdCoverLEDetails.units) + unitIncrement;
 				$scope.tpdUnits = ruleModel.tpdUnits;
 			}
 		} else if($scope.tpdCoverLEDetails.type == '2'){
 			ruleModel.tpdCoverType = 'TPDFixed';
 			if($scope.tpdCoverLEDetails.amount == undefined || $scope.tpdCoverLEDetails.amount == ''){
 				ruleModel.tpdFixedAmount = 0;
 			} else{
 				if((parseInt($scope.tpdCoverLEDetails.amount)*amountIncrement) < incrementLimit){
 					ruleModel.tpdFixedAmount = parseInt($scope.tpdCoverLEDetails.amount) + (parseInt($scope.tpdCoverLEDetails.amount)*amountIncrement);
 				}else{
 					ruleModel.tpdFixedAmount = parseInt($scope.tpdCoverLEDetails.amount) + incrementLimit;
 				}

 			}
 		}

  		if($scope.ipCoverLEDetails.amount == undefined || $scope.ipCoverLEDetails.amount == ''){
  			ruleModel.ipFixedAmount = 0;
  		} else{
  			ruleModel.ipFixedAmount = parseInt($scope.ipCoverLEDetails.amount);
  		}

  		CalculateService.calculate(ruleModel, $scope.urlList.calculateUrl).then(function(res){
  			var premium = res.data;
    		for(var i = 0; i < premium.length; i++){
    			if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
    				dcCoverAmount = premium[i].coverAmount;
    				if(dcCoverAmount > 5000000){
    					dcCoverAmount = 5000000;
    				}
    				dcWeeklyCost = premium[i].cost;
    			} else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
    				tpdCoverAmount = premium[i].coverAmount;
    				if(tpdCoverAmount >3000000){
    					tpdCoverAmount= 3000000;
    				} 
    				tpdWeeklyCost = premium[i].cost;
    			} else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
    				ipCoverAmount = (premium[i].coverAmount==null?0:premium[i].coverAmount);
    				ipWeeklyCost = (premium[i].cost==null?0:premium[i].cost);
    			}
    		}
    		totalCost = parseFloat(dcWeeklyCost) + parseFloat(tpdWeeklyCost) + parseFloat(ipWeeklyCost);
    		//To-do: Implement logic for navigation
    		// $scope.saveDataForPersistence();
    		// $scope.submitFiles();
    		// $location.path('/auralifeevent/1');
        $scope.saveDataForPersistence().then(function() {
          $scope.go('/auralifeevent/1');
        }, function(err) {
            //console.log(err);
        });
  		}, function(err){
  			console.info("Error while calculating life event premium..", JSON.stringify(err));
  		});
  	  }
  	};


  	/* Validating event date start
    *  Do not reuse this block
    *  Need to revisit, need to write the logic in small functions
    */
    $scope.isValidEventDate = function(eventDate) {
      $scope.eventDate = eventDate.replace(/[^0-9\/]/g, "");
    }

    
    $scope.validateEventDate = function(eventDate, formName, inputNmae) {
        if(!eventDate)
            return false;
          var dateObj = new Date();
          var sixMonthsOldDate = dateObj.setMonth(dateObj.getMonth() - 6);
          var sixtydays = moment().diff(moment($scope.eventDate, 'DD-MM-YYYY'), 'days');
          //console.log("date "+sixtydays);
          var dateYear = moment($scope.eventDate,"DD/MM/YYYY").year();
          var userEventDate = new Date($scope.convertDate(eventDate));
          $scope[formName][inputNmae].$setValidity('futureDate', true);
          $scope[formName][inputNmae].$setValidity('notInRange', true);
          if(!$scope.isValidDate(userEventDate)) {
            $scope.eventDate = '';
          }else if(userEventDate.withoutTime() > new Date().withoutTime()){
            var isValidDate = userEventDate.withoutTime() < new Date().withoutTime();
          	$scope[formName][inputNmae].$setValidity('futureDate', isValidDate);
          	var d = eventDate.split("/");
            $scope.eventDate = [$scope.datePadding(parseInt(d[0])), $scope.datePadding(parseInt(d[1])), dateYear].join('/');
            }  else {
            //var validDate = userEventDate.withoutTime() >= new Date(sixMonthsOldDate).withoutTime();
            var validDate = sixtydays<=60;	
            $scope[formName][inputNmae].$setValidity('notInRange', validDate);
            var d = eventDate.split("/");
            $scope.eventDate = [$scope.datePadding(parseInt(d[0])), $scope.datePadding(parseInt(d[1])), dateYear].join('/');;
          }
        }
    // Move it to utilities
    Date.prototype.withoutTime = function () {
      var d = new Date(this);
      d.setHours(0, 0, 0, 0);
      return d;
    }
    
    $scope.showHelp = function(msg){
    	$scope.modalShown = !$scope.modalShown;
    	$scope.tipMsg = msg;
    };
    
    $scope.clickToOpen = function (hhText) {
      	
		var dialog = ngDialog.open({
			template: '<p>'+hhText+'</p>' +
				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',
				template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="">Close</button></div></div>',
				className: 'ngdialog-theme-plain',
				plain: true
		});
		dialog.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
    
    $scope.isValidDate = function(userEventDate) {
      return userEventDate instanceof Date && isFinite(userEventDate);
    }

    $scope.datePadding = function (s) { return (s < 10) ? '0' + s : s; };

    $scope.convertDate = function(inputFormat) {
      function pad(s) { return (s < 10) ? '0' + s : s; }
      var d = inputFormat.split("/");
      var formatedDate = [$scope.datePadding(parseInt(d[1])), $scope.datePadding(parseInt(d[0])), d[2]].join('/');
      var regEx = /^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$/;
      return regEx.test(formatedDate) ? formatedDate : '';
    }
    /*Validating event date end*/

  	$scope.getLECategoryFromDB = function(){
  		if($scope.otherOccupationObj){
    		$scope.otherOccupationObj.lifeEventOtherOccupation = '';
    	}
  		if($scope.lifeEventOccupation != undefined){
	  		var occName = $scope.lifeEventIndustry + ":" + $scope.lifeEventOccupation;
	    	NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "SFPS", occName).then(function(res){
	    		deathLEDBCategory = res.data[0].deathfixedcategeory;
	    		tpdLEDBCategory = res.data[0].tpdfixedcategeory;
	    		ipLEDBCategory = res.data[0].ipfixedcategeory;
	    		$scope.renderOccupationQuestions();
	    	}, function(err){
	    		console.info("Error while getting transfer category from DB " + JSON.stringify(err));
	    	});
  		}
  	};
	
    $scope.renderOccupationQuestions = function(){
    	if($scope.lifeEventOccupation){
    		var selectedOcc = $scope.OccupationList.filter(function(obj){
	  			return obj.occupationName == $scope.lifeEventOccupation;
	  		});
	  		var selectedOccObj = selectedOcc[0];
	  		
    		if(selectedOccObj.manualFlag.toLowerCase() == 'true'){
    			$scope.showLifeEventOutsideOffice = true;
    		    $scope.showhazardousLifeEventQuestion = true;
    		    occupationDetailsLifeEventFormFields = ['areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','hazardousLifeEventQuestion','lifeEventOutsideOffice'];
    		    if($scope.hazardousLifeEventQuestion == 'No' && $scope.lifeEventOutsideOffice == 'No'){
    		    	$scope.lifeEventDeathOccCategory = 'White Collar';
    		    	$scope.lifeEventTpdOccCategory = 'White Collar';
    		    	$scope.lifeEventIpOccCategory = 'White Collar';
    		    } else{
    		    	$scope.lifeEventDeathOccCategory = 'Standard';
    		    	$scope.lifeEventTpdOccCategory = 'Standard';
    		    	$scope.lifeEventIpOccCategory = 'Standard';
    		    }
    		} else{
    			$scope.showLifeEventOutsideOffice = false;
    		    $scope.showhazardousLifeEventQuestion = false;
    		    occupationDetailsLifeEventFormFields = ['areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation'];   		    
    		    $scope.lifeEventDeathOccCategory = deathLEDBCategory;
		    	$scope.lifeEventTpdOccCategory = tpdLEDBCategory;
		    	$scope.lifeEventIpOccCategory = ipLEDBCategory;
    		}
    	}
    };
  	
    /* Check if your is allowed to proceed to the next accordion */
      // TBC
      // Need to revisit, need better implementation
      $scope.isCollapsible = function(targetEle, event) {
        if( targetEle == 'collapseprivacy' && !$('#dodCkBoxLblId').hasClass('active')) {
          if($('#dodCkBoxLblId').is(':visible'))
              $scope.dodFlagErr = true;
          event.stopPropagation();
          return false;
        } else if( targetEle == 'collapseOne' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active'))) {
          if($('#privacyCkBoxLblId').is(':visible'))
              $scope.privacyFlagErr = true;
          event.stopPropagation();
          return false;
        }  else if( targetEle == 'collapseTwo' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
          if($("#collapseOne form").is(':visible'))
              $scope.lifeEventFormSubmit($scope.contactDetailsLifeEventForm);
          event.stopPropagation();
          return false;
        }  else if( targetEle == 'collapseThree' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
          if($("#collapseTwo form").is(':visible'))
              $scope.lifeEventFormSubmit($scope.occupationDetailsLifeEventForm);
          event.stopPropagation();
          return false;
        }
      }

    /* TBC */
    // privacy section
    $scope.togglePrivacy = function(checkFlag) {
        $scope.privacyCol = checkFlag;
        if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
    };
    
    // contact section
    $scope.toggleContact = function(checkFlag) {
        $scope.contactCol = checkFlag;
        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseOne']").click(); /* Can be improved */
        
    };
  	
    // occupation section
  	$scope.toggleTwo = function(checkFlag) {
        $scope.coltwo = checkFlag;
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };
  	
    // life Event section
  	$scope.toggleThree = function(checkFlag) {
        $scope.colthree = checkFlag;
        if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseThree']").click(); /* Can be improved */
    };
   
    
  // validation for DOD checkbox
    $scope.checkDodState = function(){
      $timeout(function() {
        $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodCkBoxLblId').hasClass('active') : !$scope.dodFlagErr;
        if($('#dodCkBoxLblId').hasClass('active')) {
          $scope.togglePrivacy(true);
        } else {
          $scope.togglePrivacy(false);
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
        }
      }, 1);
    };
    
    // validation for Privacy checkbox
    $scope.checkPrivacyState  = function(){
      $timeout(function() {
        $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyCkBoxLblId').hasClass('active') : !$scope.privacyFlagErr;
        if($('#privacyCkBoxLblId').hasClass('active')) {
          $scope.toggleContact(true);
        } else {
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
        }
      }, 1);
    };
    
   /* $scope.checkOwnBusinessQuestion = function(){
    	if($scope.ownBussinessQuestion == 'Yes'){
    		occupationDetailsLifeEventFormFields = ['ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation'];
    		occupationDetailsOtherLifeEventFormFields = ['ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation'];
	    } else if($scope.ownBussinessQuestion == 'No'){
	    	occupationDetailsLifeEventFormFields = ['ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation'];
	    	occupationDetailsOtherLifeEventFormFields = ['ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation'];
	    }
    };*/
	
    $scope.checkLifeEventFormPreviousMandatoryFields  = function (elementName,formName){
    	var lifeEventFormFields;
    	if(formName == 'contactDetailsLifeEventForm'){
    		lifeEventFormFields = contactDetailsLifeEventFormFields;
    	} else if(formName == 'occupationDetailsLifeEventForm'){
    		if($scope.occupationTransfer != undefined && $scope.occupationTransfer == 'Other'){
    			lifeEventFormFields = occupationDetailsOtherLifeEventFormFields;
    		} else{
    			lifeEventFormFields = occupationDetailsLifeEventFormFields;
    		}
    	} else if(formName == 'lifeEvent'){
    		if($scope.documentName != undefined && $scope.documentName =='No'){
    			lifeEventFormFields = lifeEventFieldsWithChkBox;
          $scope.files = [];
          PersistenceService.setUploadedFileDetails($scope.files);
    		} else{
    			lifeEventFormFields = lifeEventFields;
    		}
    	}
      var inx = lifeEventFormFields.indexOf(elementName);
      if(inx > 0){
        for(var i = 0; i < inx ; i++){
          $scope[formName][lifeEventFormFields[i]].$touched = true;
        }
      }
    };

    $scope.lifeEventFormSubmit =  function (form){
    	if(form.$name == 'lifeEvent' && $("#transferitrId_div_id").is(":visible")) {
	        if($scope.files && $scope.files.length ) {
	          $scope.fileNotUploadedError = false;
        } else {
	          $scope.fileNotUploadedError = true;
          return false;
	        }
	      }
      if(!form.$valid){
    	  form.$submitted=true;
        if(form.$name == 'contactDetailsLifeEventForm'){
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
      } else if(form.$name == 'occupationDetailsLifeEventForm'){
        $scope.toggleThree(false);
      } 
	  } else{
		  if(form.$name == 'contactDetailsLifeEventForm'){
    	    $scope.toggleTwo(true);
		  } else if(form.$name == 'occupationDetailsLifeEventForm'){
			  if(!$scope.invalidSalAmount){
				  allowNext = true;
			  $scope.toggleThree(true);
			  }
		  } else if(form.$name == 'lifeEvent'){
			  if($scope.eventAlreadyApplied != 'Yes'){
				  $scope.continueToNextPage(true);
			  }
		  }
       }
    };
    
    $scope.uploadFiles = function(files, errFiles) {
    	$scope.fileSizeErrFlag = false;
    	$scope.fileFormatError = errFiles.length > 0 ? true : false;
    	$scope.selectedFile =  files[0] ;
    };
    
    $scope.addFilesToStack = function () {
		var fileSize = ($scope.selectedFile.size / 1048576).toFixed(3);
		if(fileSize > 10) {
			$scope.fileSizeErrFlag=true;
			$scope.fileSizeErrorMsg ="File size should not be more than 10MB";
			$scope.selectedFile = null;
			return;
		}else{
			$scope.fileSizeErrFlag=false;
		}
    if(!$scope.files)
      $scope.files = [];
		$scope.files.push($scope.selectedFile);
		//PersistenceService.setUploadedFileDetails($scope.files);
    $scope.fileNotUploadedError = false;
		$scope.selectedFile = null;
	};
	 $scope.removeFile = function(index) {
	    $scope.files.splice(index, 1);
	    PersistenceService.setUploadedFileDetails($scope.files);
	    if($scope.files.length > 0) {
	      $scope.fileNotUploadedError = true;
	    }
	  }
	 $scope.submitFiles = function () {
		 $scope.uploadedFiles = $scope.uploadedFiles || [];
				var defer = $q.defer();
			if(!$scope.files){
		      $scope.files = [];
		    }
		    if(!$scope.files.length) {
		      defer.resolve({});
		    }
				var upload;
				var numOfFiles = $scope.files.length;
		    angular.forEach($scope.files, function(file, index) {
		    	if(Upload.isFile(file)) {
		    	upload = Upload.http({
		    		url: $scope.urlList.fileUploadUrl,
		    		headers : {
		    			'Content-Type': file.name,
		    	        'Authorization':tokenNumService.getTokenId()
		    		},
		    		data: file
				  });
		    	upload.then(function(res){
		    		numOfFiles--;
		    		$scope.uploadedFiles[index] = res.data;
		    		if(numOfFiles == 0){
		    			PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
		          defer.resolve(res);
		    		}
		    	}, function(err){
		    		console.log("Error uploading the file " + err);
		        defer.reject(err);
		    	});
		    	} else {
		    		numOfFiles--;
		            if(numOfFiles == 0) {
		              PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
		              defer.resolve({});
		            }
		          }
		    });
		    return defer.promise;
			};
			    
      $scope.saveDataForPersistence = function(){
    	    var defer = $q.defer();
	    	var coverObj = {};
	    	var coverStateObj ={};	
	    	var lifeEventOccObj={};
	    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
	    		return $scope.lifeEventIndustry == obj.key;
	    	});
	    	
	    	
				    	coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
				    	coverObj['firstName'] = $scope.personalDetails.firstName;
				        coverObj['lastName'] = $scope.personalDetails.lastName;
				    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
				    	coverObj['country'] =persoanlDetailService.getMemberDetails().address.country;
				    	coverObj['email'] = $scope.lifeEventEmail;
				    	coverObj['contactType']=$scope.preferredContactType;
				    	coverObj['contactPhone'] = $scope.lifeEventPhone;
				    	coverObj['contactPrefTime'] = $scope.lifeEventTime;
				    	
				    	
				    	lifeEventOccObj['gender']=$scope.personalDetails.gender;
				    	/*lifeEventOccObj['ownBussinessQues']= $scope.ownBussinessQuestion;
				    	lifeEventOccObj['ownBussinessYesQues']= $scope.ownBussinessYesQuestion;
				    	lifeEventOccObj['ownBussinessNoQues']= $scope.ownBussinessNoQuestion;*/
				    	lifeEventOccObj['citizenQue'] = $scope.areyouperCitzLifeEventQuestion;
				    	lifeEventOccObj['industryName'] = selectedIndustry[0].value;
				    	lifeEventOccObj['industryCode'] = selectedIndustry[0].key;
				    	lifeEventOccObj['occupation'] = $scope.lifeEventOccupation;
				    	lifeEventOccObj['managementRoleQue']= $scope.lifeEventOutsideOffice;
				    	lifeEventOccObj['hazardousQue']= $scope.hazardousLifeEventQuestion;
				    	lifeEventOccObj['otherOccupation'] = $scope.otherOccupationObj.lifeEventOtherOccupation;
				    	lifeEventOccObj['salary'] = $scope.annualSalary;
				    	
				    	
				    	coverObj['eventName'] = $scope.event.cde;
				    	coverObj['eventDesc'] = $scope.event.desc;
				    	coverObj['eventDate'] = $scope.eventDate;
				    	coverObj['eventAlreadyApplied'] = $scope.eventAlreadyApplied;
				    	coverObj['documentName'] = $scope.documentName;
				    	ackDocument = $('#acknowledgeDocAdressCheck').hasClass('active');
				    	if(ackDocument){
				    		//coverObj['documentAddress'] = ackDocument;
				    		coverObj['documentAck2'] = "Yes";
		                    coverObj['documentAddress'] = "Postal address:\n\nStatewide Super,\nGPO Box 1749,\nADELAIDE SA 5001";
				    	}
				    	coverObj['deathOccCategory'] = $scope.lifeEventDeathOccCategory;
				    	coverObj['tpdOccCategory'] = $scope.lifeEventTpdOccCategory;
				    	coverObj['ipOccCategory'] = $scope.lifeEventIpOccCategory;
				    	coverObj['deathAmt'] = parseFloat($scope.deathCoverLEDetails.amount);
				    	coverObj['tpdAmt'] = parseFloat($scope.tpdCoverLEDetails.amount);
				    	coverObj['ipAmt'] = parseFloat($scope.ipCoverLEDetails.amount);
				    	coverObj['deathNewAmt'] = parseFloat(dcCoverAmount);
				    	coverObj['tpdNewAmt'] = parseFloat(tpdCoverAmount);
				    	coverObj['ipNewAmt'] = parseFloat(ipCoverAmount);
				    	coverObj['waitingPeriod'] = $scope.ipCoverLEDetails.waitingPeriod;
				    	coverObj['benefitPeriod'] = $scope.ipCoverLEDetails.benefitPeriod;
				    	coverObj['appNum'] = appNum;
				    	coverObj['dodCheck'] = $('#dodCkBoxLblId').hasClass('active');
                        coverObj['privacyCheck'] = $('#privacyCkBoxLblId').hasClass('active');
				    	coverObj['lastSavedOn'] = 'LifeEventPage';
				    	coverObj['age'] = anb;
				        coverObj['manageType'] = 'ICOVER';
				        coverObj['partnerCode'] = 'SFPS';
				   
				        coverObj['totalPremium'] = parseFloat(totalCost);
				        coverObj['deathCoverPremium'] =parseFloat(dcWeeklyCost);
				        coverObj['tpdCoverPremium'] = parseFloat(tpdWeeklyCost);
				        coverObj['ipCoverPremium'] = parseFloat(ipWeeklyCost);
				        
				        coverObj['deathLifeCoverType'] = $scope.deathCoverType;
				        coverObj['tpdLifeCoverType'] = $scope.tpdCoverType;
				        coverObj['ipLifeCoverType'] = 'IpUnitised';
				        coverObj['freqCostType'] = 'Weekly';
				        
				        coverObj['deathLifeUnits'] = $scope.deathUnits;
				        coverObj['tpdLifeUnits'] = $scope.tpdUnits;
				        coverObj['ipLifeUnits'] = $scope.ipCoverLEDetails.units;
				        coverStateObj['showLifeEventOutsideOffice'] = $scope.showLifeEventOutsideOffice;
				        coverStateObj['showhazardousLifeEventQuestion'] = $scope.showhazardousLifeEventQuestion;
				        $scope.submitFiles().then(function(res) {
					    	PersistenceService.setlifeEventCoverDetails(coverObj);
					    	PersistenceService.setlifeEventCoverStateDetails(coverStateObj);
					    	PersistenceService.setLifeEventCoverOccDetails(lifeEventOccObj);
			          defer.resolve(res);
			        }, function(err) {
			          defer.reject(err);
			        });
			        return defer.promise;
	    };
			    
	    if($routeParams.mode == 2){
	    	var existingDetails = PersistenceService.getlifeEventCoverDetails();
	    	var occDetails =PersistenceService.getLifeEventCoverOccDetails();
	    	var stateDetails = PersistenceService.getlifeEventCoverStateDetails();
	    	
	    	$scope.lifeEventEmail = existingDetails.email;
	    	$scope.preferredContactType = existingDetails.contactType;
	    	$scope.lifeEventPhone = existingDetails.contactPhone;
	    	$scope.lifeEventTime = existingDetails.contactPrefTime;
	    	
	    	/*$scope.ownBussinessQuestion = occDetails.ownBussinessQues;
	    	$scope.ownBussinessYesQuestion = occDetails.ownBussinessYesQues;
	    	$scope.ownBussinessNoQuestion = occDetails.ownBussinessNoQues;*/
	    	$scope.areyouperCitzLifeEventQuestion = occDetails.citizenQue;
	    	$scope.lifeEventIndustry = occDetails.industryCode;
	    	$scope.hazardousLifeEventQuestion = occDetails.hazardousQue;
	    	$scope.lifeEventOutsideOffice = occDetails.managementRoleQue;
	        $scope.otherOccupationObj.lifeEventOtherOccupation = occDetails.otherOccupation;
	        $scope.annualSalary = occDetails.salary;
	        
	        $scope.showLifeEventOutsideOffice = stateDetails.showLifeEventOutsideOffice;
        	$scope.showhazardousLifeEventQuestion = stateDetails.showhazardousLifeEventQuestion;
	    	
	    //	$scope.event=existingDetails.eventName;
	    	$scope.eventDate=existingDetails.eventDate;
	    	$scope.eventAlreadyApplied=existingDetails.eventAlreadyApplied;
	    	$scope.documentName=existingDetails.documentName;
	    	ackDocument=existingDetails.documentAddress;
	    	appNum = existingDetails.appNum;
	    	ackCheck = existingDetails.ackCheck;
	    	dodCheck = existingDetails.dodCheck;
	    	privacyCheck = existingDetails.privacyCheck;
	    	
	    	$scope.files = PersistenceService.getUploadedFileDetails();
	    	$scope.uploadedFiles = $scope.files;
	    	var tempEvent = $scope.eventList.filter(function(obj){
	    		return obj.cde == existingDetails.eventName;
	    	});
	    	$scope.event = tempEvent[0];
	    	
	    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"SFPS",$scope.lifeEventIndustry).then(function(res){
	        	$scope.OccupationList = res.data;
	        	var temp = $scope.OccupationList.filter(function(obj){
	        		return obj.occupationName == occDetails.occupation;
	        	});
	        	$scope.lifeEventOccupation = temp[0].occupationName;
	        	$scope.getLECategoryFromDB();
	        }, function(err){
	        	console.log("Error while getting occupatio list " + JSON.stringify(err));
	        });
	    	
	    	if(ackDocument){
	    		$timeout(function(){
	    			$('#acknowledgeDocAdressCheck').addClass('active');
	    			$scope.ackDocument2 = true;
	    		});
	       	}
	    	
	    	if(dodCheck){
	    	    $timeout(function(){
	    			$('#dodCkBoxLblId').addClass('active');
			   });
			 }
	    	if(privacyCheck){
	    	    $timeout(function(){
	    			$('#privacyCkBoxLblId').addClass('active');
			   });
			 }
	    	
	    	$scope.togglePrivacy(true);
	    	$scope.toggleContact(true);
	    	$scope.toggleTwo(true);
	    	$scope.toggleThree(true);
	    	allowNext=true;
	    };
			    
	    if($routeParams.mode == 3){
	    	 mode3Flag = true;
	    	 var num = PersistenceService.getAppNumToBeRetrieved();
	    	 RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
	    		var appDetails = res.data[0];
	    		
	    		$scope.lifeEventEmail = appDetails.email;
		    	$scope.preferredContactType = appDetails.contactType;
		    	$scope.lifeEventPhone = appDetails.contactPhone;
		    	$scope.lifeEventTime = appDetails.contactPrefTime;
		    	 $scope.annualSalary = appDetails.occupationDetails.salary;
		    	/*$scope.ownBussinessQuestion = appDetails.occupationDetails.ownBussinessQues;
		    	$scope.ownBussinessYesQuestion = appDetails.occupationDetails.ownBussinessYesQues;
		    	$scope.ownBussinessNoQuestion = appDetails.occupationDetails.ownBussinessNoQues;*/
		    	$scope.areyouperCitzLifeEventQuestion = appDetails.occupationDetails.citizenQue;
		    	$scope.lifeEventIndustry = appDetails.occupationDetails.industryCode;
		    	$scope.hazardousLifeEventQuestion = appDetails.occupationDetails.hazardousQue;
		    	$scope.lifeEventOutsideOffice = appDetails.occupationDetails.managementRoleQue;
		        $scope.otherOccupationObj.lifeEventOtherOccupation = appDetails.occupationDetails.otherOccupation;
		        
		      //  $scope.event=appDetails.eventName;
		    	$scope.eventDate=appDetails.eventDate;
		    	$scope.eventAlreadyApplied=appDetails.eventAlreadyApplied;
		    	$scope.documentName=appDetails.documentName;
		    	if(appDetails.documentName == "No"){
	                $scope.ackDocument2 = true;
	              }
		    	ackDocument=appDetails.documentAddress;
		    	appNum = appDetails.appNum;
		    	ackCheck = appDetails.ackCheck;
		    	dodCheck = appDetails.dodCheck;
		    	privacyCheck = appDetails.privacyCheck;
		    
		    	$scope.files = appDetails.lifeEventDocuments;
		    	$scope.uploadedFiles = $scope.files;
		    	var tempEvt = $scope.eventList.filter(function(obj){
		    		return obj.cde == appDetails.eventName;
		    	});
		    	$scope.event = tempEvt[0];
		    	
		    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"SFPS",$scope.lifeEventIndustry).then(function(res){
		        	$scope.OccupationList = res.data;
		        	var temp = $scope.OccupationList.filter(function(obj){
		        		return obj.occupationName == appDetails.occupationDetails.occupation;
		        	});
		        	$scope.lifeEventOccupation = temp[0].occupationName;
		        	$scope.renderOccupationQuestions();
		        }, function(err){
		        	console.log("Error while getting occupatio list " + JSON.stringify(err));
		        });
		    	
		    	if($scope.ackDocument2){
		    		$timeout(function(){
		    			$('#acknowledgeDocAdressCheck').addClass('active');
		    			$scope.ackDocument2 = true;
		    		});
		       	}
		    	$('#dodCkBoxLblId').addClass('active');
                $('#privacyCkBoxLblId').addClass('active');
				  
                $scope.saveDataForPersistence();
		    	$scope.togglePrivacy(true);
		    	$scope.toggleContact(true);
		    	$scope.toggleTwo(true);
		    	$scope.toggleThree(true);
		    	allowNext=true;
	    		
	    	 },function(err){
		    		console.info("Error fetching the saved app details " + err);
	    	 });
	    }
			    
	    $scope.goToAura = function(){
	    	if(this.contactDetailsLifeEventForm.$valid && this.occupationDetailsTransferForm.$valid && this.previousCoverForm.$valid && this.TranscoverCalculatorForm.$valid){
	    		$rootScope.$broadcast('disablepointer');
	    		$timeout(function(){
	    			//$scope.saveDataForPersistence();
					  // submit uploaded to server
				    //$scope.submitFiles();
		    	 //$scope.go('/auralifeevent/1');

            $scope.saveDataForPersistence().then(function() {
              $scope.go('/auralifeevent/1');
            }, function(err) {
                //console.log(err);
            });
		      }, 10);
	    	}
	    };
	    
	    $scope.saveQuoteLifeEvent = function() {
	    	$scope.saveDataForPersistence().then(function() {
          $scope.quoteLifeEventObject =  PersistenceService.getlifeEventCoverDetails();
          $scope.lifeEventOccDetails =PersistenceService.getLifeEventCoverOccDetails();
          $scope.personalDetails = persoanlDetailService.getMemberDetails();
          var lifeEventUploadedFiles = PersistenceService.getUploadedFileDetails();
          
          if($scope.quoteLifeEventObject != null && $scope.lifeEventOccDetails != null && $scope.personalDetails != null){
            $scope.details = {};
            $scope.details.occupationDetails = $scope.lifeEventOccDetails;
            //added for uploaded file details
            if(lifeEventUploadedFiles != null) {
              $scope.details.lifeEventDocuments = lifeEventUploadedFiles;
            }
            var temp = angular.extend($scope.quoteLifeEventObject,$scope.details)
            var saveQuoteLifeEventObject = angular.extend(temp,$scope.personalDetails);
            auraResponseService.setResponse(saveQuoteLifeEventObject);
              saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
                console.log(response.data);
                $scope.lifeEventQuoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
              },function(err){
                console.log("Something went wrong while saving..."+JSON.stringify(err));
              });
          }
        }, function(err) {
            //console.log(err);
        });
	    	
	    };
			    
	    $scope.lifeEventQuoteSaveAndExitPopUp = function (hhText) {
	      	
			var dialog1 = ngDialog.open({
				    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
		
    }]);