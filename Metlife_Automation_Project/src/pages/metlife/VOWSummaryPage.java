package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import com.gargoylesoftware.htmlunit.html.Keyboard;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.STFTBeneficiariesPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTPaymentsPageObjects;
import uimap.metlife.YBRCoverDetailPageObjects;
import uimap.metlife.YBRQuotePageObjects;
import uimap.metlife.YBRSummaryPageObjects;
import uimap.metlife.VOWSummaryPageObjects;
import uimap.metlife.YBRWebResponsePageObjects;

public class VOWSummaryPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public VOWSummaryPage quoteProtectPayment() {
		quoteProtectPaymentDetails();
		return new VOWSummaryPage(scriptHelper);
	}

	public VOWSummaryPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void quoteProtectPaymentDetails() {
	
		String title = dataTable.getData("MetLife_Data", "Title");
		String lastName = dataTable.getData("MetLife_Data", "LastName");
		String accountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsb = dataTable.getData("MetLife_Data", "BSB");
		String ccType = dataTable.getData("MetLife_Data", "CcType");
		String expiryMonth = dataTable.getData("MetLife_Data", "ExpiryMonth");
		String expiryYear = dataTable.getData("MetLife_Data", "ExpiryYear");
		String cardType = dataTable.getData("MetLife_Data", "CardType");
		String cYSTest = dataTable.getData("MetLife_Data", "CYSTest");
		try {
			
			switchToActiveWindow();
			driver.manage().window().maximize();
							
			Thread.sleep(1000);
			tryAction(fluentWaitElement(VOWSummaryPageObjects.linkTitle1), "specialDropdown", "Title1", title);
			Thread.sleep(1000);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtLastName1), "SET", "LastName_1", lastName);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtLastName1), "TAB", "LastName_1");
			Thread.sleep(1000);
			
			Thread.sleep(1000);
			tryAction(fluentWaitElement(VOWSummaryPageObjects.linkTitle2), "specialDropdown", "Title2", title);
			Thread.sleep(1000);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtLastName2), "SET", "LastName_2", lastName);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtLastName2), "TAB", "LastName_2");
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(VOWSummaryPageObjects.chkacknowledge), "Click", "Acknowledge");
			

			if (cardType.equalsIgnoreCase("Debit")) {
			tryAction(fluentWaitElement(VOWSummaryPageObjects.rddebit), "clkradio", "Debit");
			Thread.sleep(500);			
			
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtaccountname), "SET", "AccountName", accountName);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtbsb), "SET", "BSB", bsb);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtbsb), "TAB", "BSB");
			}
			else
			{
				
				System.out.println("Entered into Credit Card Section");
			 tryAction(fluentWaitElement(VOWSummaryPageObjects.rdcredit), "clkradio", "Credit");
				Thread.sleep(500);
				
				switch(ccType){
				case "Visa":
					System.out.println("Entered into the Visa Switch");
					tryAction(fluentWaitElement(VOWSummaryPageObjects.rdvisa), "clkradio", "Visa");
				    Thread.sleep(500);
					 break;	 
				case "Master":
					System.out.println("Entered into the Master Switch");
				    tryAction(fluentWaitElement(VOWSummaryPageObjects.rdmaster), "clkradio", "Master");
			        Thread.sleep(500);
					 break;
				case "Amex":
					System.out.println("Entered into the Amex Switch");
			        tryAction(fluentWaitElement(VOWSummaryPageObjects.rdamex), "clkradio", "Amex");
		            Thread.sleep(500);
		
						}
				
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtccnumber), "SET", "CCNumber", accountNumber);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtccnumber), "TAB", "CCNumer");
			sleep(1000);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtccname), "SET", "CCName", accountName);
			tryAction(waitForClickableElement(VOWSummaryPageObjects.txtccname), "TAB", "CCNumer");
			sleep(1000);
			tryAction(fluentWaitElement(VOWSummaryPageObjects.selCCExpMonth),"specialDropdown", "Expiry Month", expiryMonth);
			sleep(2000);
			tryAction(fluentWaitElement(VOWSummaryPageObjects.selCcExpYr),"specialDropdown", "Expiry Year", expiryYear);
		}

			tryAction(waitForClickableElement(VOWSummaryPageObjects.chkauthorise), "Click", "Acknowledge");
							
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(VOWSummaryPageObjects.btnsubmit)).click().perform();
			Thread.sleep(4000);			
			
			driver.manage().window().maximize();
			report.updateTestLog("App Submission", "Application submitted sucessfully", Status.SCREENSHOT);
			String appNumber1=driver.findElement(VOWSummaryPageObjects.textAppnum).getText();
			String appNumber = appNumber1.substring(0, appNumber1.length() - 1); 
			dataTable.putData("MetLife_Data","AppNumber", appNumber);
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(driver.findElement(VOWSummaryPageObjects.btnclosewindow)).click().perform();
			Thread.sleep(1000);
			
			driver.manage().window().maximize();
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			if (cYSTest.equalsIgnoreCase("Yes")) {	
				driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
				driver.manage().window().maximize();
			}		
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
