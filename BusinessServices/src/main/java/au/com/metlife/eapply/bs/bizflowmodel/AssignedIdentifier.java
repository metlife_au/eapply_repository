package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssignedIdentifier")
@XmlRootElement
public class AssignedIdentifier {
	
	private String RoleCode;
	private String Id;
	private String IdentifierDescription;
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getIdentifierDescription() {
		return IdentifierDescription;
	}
	public void setIdentifierDescription(String identifierDescription) {
		IdentifierDescription = identifierDescription;
	}
	public String getRoleCode() {
		return RoleCode;
	}
	public void setRoleCode(String roleCode) {
		RoleCode = roleCode;
	}



	
	
}
