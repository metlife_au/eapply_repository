(function(angular){
	'use strict';
    /* global channel, moment */  
	angular
	.module('MtaaApp')
	.controller('summaryConvert', summaryConvert);
	summaryConvert.$inject=['$scope','$rootScope','$location','$timeout','$window','auraInputService','getAuraTransferData','deathCoverService','tpdCoverService','submitAura','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService','RetrieveAppDetailsService','saveEapply' ,'$stateParams'];
	function summaryConvert($scope, $rootScope,$location, $timeout, $window, auraInputService,getAuraTransferData,deathCoverService,tpdCoverService,submitAura,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService,RetrieveAppDetailsService, saveEapply, $stateParams){
		
		var vm=this;
		vm.urlList = urlService.getUrlList();		
		
		vm.init = function(){
			
			vm.savedData =  PersistenceService.getConvertMaintainDetails();
			vm.totalPremium = parseFloat(vm.savedData.applicant.cover[0].cost) + parseFloat(vm.savedData.applicant.cover[1].cost) + parseFloat(vm.savedData.applicant.cover[2].cost);
			
			if($stateParams.mode === '3'){
				if(!vm.savedData.responseObject){
					vm.savedData.responseObject = {};
				}
			}			
		};
		vm.init();
		
		vm.go = function (path) {
			
	    	$timeout(function(){
	    		$location.path(path);
	    	}, 10);
	  	};
		
		
		vm.checkAckStateGCTR = function(){
	    	$timeout(function(){
	    		var ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
	        	if(ackCheckTRGC){
	        		vm.TRGCackFlag = false;
	        	}else{
	        		vm.TRGCackFlag = true;
	        	}
	    	}, 10);
	    };
		
		vm.submitCover = function(){
			var ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
			var submitTransferObject =  PersistenceService.getConvertMaintainDetails();
			console.log(JSON.stringify(submitTransferObject));
			
			if(ackCheckTRGC){
				if(submitTransferObject !== null){					
					submitTransferObject.lastSavedOn = '';
					submitTransferObject.appDecision = 'ACC';
					auraResponseService.setResponse(submitTransferObject);
					submitEapply.reqObj(vm.urlList.submitEapplyUrlNew).then(function(response) {
						PersistenceService.setPDFLocation(response.data.clientPDFLocation);
						PersistenceService.setNpsUrl(response.data.npsTokenURL);						
						vm.go('/convertDecision/accept');							
		            });
				}
			}else{
				vm.TRGCackFlag = true;
			}			
		};
		
	}
})(angular);
