(function(angular) {
	'use strict';
	var mtaaApp = angular.module('MtaaApp', ['ui.router', 'ngDialog','mtaa.ui.constants',
			'Metlife.mtaa.uicomponents', 'Metlife.mtaa.uicomponentsheader',
			'Metlife.mtaa.directives','restAPI','MtaaAPI','appDataModel','MtaaFilters','ngFileUpload','sessionTimeOut','eApplyInterceptor','ngSanitize']);
})(angular);
