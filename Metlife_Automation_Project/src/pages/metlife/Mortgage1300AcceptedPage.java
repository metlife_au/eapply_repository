package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.Mortgage1300AcceptedObjects;
import uimap.metlife.Mortgage1300PaymentsObjects;
import uimap.metlife.Mortgage1300QuoteObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class Mortgage1300AcceptedPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public Mortgage1300AcceptedPage acceptedFinish() {
		acceptedFinishDetails();
		return new Mortgage1300AcceptedPage(scriptHelper);
	}

	public Mortgage1300AcceptedPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void acceptedFinishDetails() {

			
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			Thread.sleep(1000);
			
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(Mortgage1300AcceptedObjects.btnFinish)).click().perform();
			tryAction(fluentWaitElement(Mortgage1300AcceptedObjects.btnFinish), "Click", "Finish");
			Thread.sleep(10000);

			switchToActiveWindow();
			driver.manage().window().maximize();
			Thread.sleep(1000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
