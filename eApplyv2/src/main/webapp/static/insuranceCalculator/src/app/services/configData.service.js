export default class ConfigData {
  constructor($sce) {
    'ngInject';
    
    this.termOptions = ['Annually','Quarterly','Monthly','Fortnightly','Weekly'];
    this.debtsFields = [
    	{
    		'labelText' : 'Home Mortgage',
    		'fieldName' : 'homeMortgage',
    	},
    	{
    		'labelText' : 'Other Home Loans',
    		'fieldName' : 'homeLoan',
    	},
    	{
    		'labelText' : 'Personal Loans',
    		'fieldName' : 'personalLoan',
    	},
    	{
    		'labelText' : 'Credit Card Debts',
    		'fieldName' : 'creditCard',
    	},
    	{
    		'labelText' : 'Other Debts',
    		'fieldName' : 'otherDebts',
    	}
    ];
    this.expenseFields = [
    	{
    		'labelText' : 'Rent',
    		'fieldName' : 'rent',
    		'termValue' : 'Weekly'
    	},
    	{
    		'labelText' : 'Council rates and/or strata levies',
    		'fieldName' : 'councilRate',
    		'termValue' : 'Quarterly'
    	},
    	{
    		'labelText' : 'Water, electricity and gas',
    		'fieldName' : 'waterElectGas',
    		'termValue' : 'Quarterly'
    	},
    	{
    		'labelText' : 'Pay TV, internet, telephone, mobile phone',
    		'fieldName' : 'tvMobile',
    		'termValue' : 'Monthly'
    	},
    	{
    		'labelText' : 'Groceries',
    		'fieldName' : 'groceries',
    		'termValue' : 'Weekly'
    	},
    	{
    		'labelText' : 'Food and entertainment',
    		'fieldName' : 'foodEntertainment',
    		'termValue' : 'Weekly'
    	},
    	{
    		'labelText' : 'Child care fees',
    		'fieldName' : 'childCare',
    		'termValue' : 'Monthly'
    	},
    	{
    		'labelText' : 'School and/or tuition fees',
    		'fieldName' : 'tuitionFees',
    		'termValue' : 'Monthly'
    	},
    	{
    		'labelText' : 'Clothing, books, gifts, classes etc.',
    		'fieldName' : 'clothBookGifts',
    		'termValue' : 'Monthly'
    	},
    	{
    		'labelText' : 'Transport, petrol and/or car maintenance',
    		'fieldName' : 'petrolCareMaintenance',
    		'termValue' : 'Monthly'
    	},
    	{
    		'labelText' : 'Home insurance',
    		'fieldName' : 'homeInsurance',
    		'termValue' : 'Annually'
    	},
    	{
    		'labelText' : 'Health and life insurance (outside superannuation)',
    		'fieldName' : 'healthLifeInsurance',
    		'termValue' : 'Annually'
    	},
    	{
    		'labelText' : 'Car and other insurance',
    		'fieldName' : 'carInsurance',
    		'termValue' : 'Annually'
    	},
    	{
    		'labelText' : 'Other expenses (such as holidays, home repairs, hobbies)',
    		'fieldName' : 'otherExpense',
    		'termValue' : 'Monthly'
    	}
    ];

    this.careConfiguration = {
        	'partnerName' : 'Care Super',
        	'helpline' : '1300 360 149',
        	'privacy' : 'https://www.caresuper.com.au/privacy-policy',
        	'disclaimer' : 'https://www.caresuper.com.au/disclaimer',
        	'aboutUs' : 'https://www.caresuper.com.au/about-us',
        	'contactUs' : 'https://www.caresuper.com.au/super/contact-us',
        	'copyRights' : $sce.trustAsHtml('&copy; Care Super Fund'),
        	'deathMinAge' : 16,
        	'deathMaxAge' : 70,
        	'tpdMinAge' : 16,
        	'tpdMaxAge' : 65,
        	'ipMinAge' : 16,
        	'ipMaxAge' : 65,
        	'fixedLabelText' : 'Fixed',
        	'unitisedLabelText' : 'Unit-based',
        	'deathLabelText' : 'Death',
        	'tpdLabelText' : 'Total and Permanent Disablement',
        	'ipLabelText' : 'Income protection',
        	'insuredIpLabelText' : 'Insure 85% of my Salary',
        	'insuredIpValue' : 85,
        	'waitingOpts' : ['30 Days','60 Days', '90 Days'],
        	'benefitOpts' : ['2 Years', '5 Years'],
        	'defaultWaitingperiod' : '30 Days',
        	'defaultFrequency' : 'Weekly',
        	'deathMaxAmount' : 10000000,
        	'tpdMaxAmount' : 3000000,
        	'ipMaxAmount' : 40375,
        	'tpdLessThanDeath' : false,
        	'haveMortage' : '',
        	'expensePopup' : true,
        	'debtsPopup' : true,
        	'acknowledgeNeeded' : true,
        	'disclaimerText' : '',
        	'disclaimerPopupText' : $sce.trustAsHtml('<p><strong>Assumptions:</strong></p> <p><strong>Inflation Rate</strong></p> <ul> <li>Our estimate of the required amount of Death and/or TPD cover includes the cost of paying for future expenses, which requires the calculator to make assumptions in relation to the rate of inflation in the future.</li> <li>The inflation rate is set at a default level of 3.5% per annum (p.a.). This is consistent with the average historic wage inflation in Australia over the last 15 years as measured by increases in Average Wage Price Index (available <a href="http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/6345.0Dec%202015?OpenDocument" target="_blank">here</a>). Wage inflation, as opposed to the Consumer Price Index, has been selected as it is typically higher then CPI.</li> </ul> <p><strong>Investment Return </strong></p> <ul> <li>The default investment return rate has been set at 5.7% p.a. and reflects the median 10 year return (post investment tax) for Growth Risk profile investments per the <a href="https://www.chantwest.com.au/investment-returns/super-fund-returns" target="_blank">Chant West Survey</a>. The growth risk profile is suitable for industry superannuation funds based on asset allocation data available in the APRA 2015 Annual Fund Level Superannuation Statistics Report available <a href="https://www.apra.gov.au/Super/Publications/Pages/superannuation-fund-level-publications.aspx" target="_blank">here</a>.</li> <li>The investment return rate is an estimate of the annual investment return, assuming your superannuation fund\'s insurer has paid a TPD benefit to your superannuation fund, and it has remained inside super to enable you to take advantage of the concessional taxation of investment earnings within the superannuation environment</li> </ul> <p><strong>Superannuation Contribution Rate</strong></p> <ul> <li>The superannuation contribution rate is set at 9.5%. This is the assumed rate of superannuation contributions as a proportion of your before tax income and is based on the current minimum Superannuation Guarantee (SG) contribution rate available <a href="https://www.ato.gov.au/Calculators-and-tools/Super-guarantee-contributions/" target="_blank">here</a> . This is currently 9.5% of ordinary time earnings<li> </ul> <p><strong>Funeral Costs</strong></p> <ul> <li>The default value for funeral costs is $15,000 and is based on MoneySmart\'s estimate for funeral costs (available <a href="https://www.moneysmart.gov.au/life-events-and-you/over-55s/paying-for-your-funeral" target="_blank">here</a> ). The highest figure of the estimated range has been used<li> </ul> <p><strong>Retirement age</strong></p> <ul> <li>The retirement age is the age at which we assume you plan to retire and is set at 65. The default retirement age is based on the age that you can have full access to your superannuation even if you haven\'t retired. (Information <a href="https://www.ato.gov.au/Individuals/Super/Accessing-your-super/" target="_blank">here</a>)</li> </ul> <p><strong>Annual nursing and medical costs</strong></p> <ul> <li>The default value for annual nursing and medical costs (which you may require if you become totally and permanently disabled) is $46,000 and is based on an average of high care and mixed-care nursing services (including personal care, catering, cleaning, etc.) in Australia as surveyed by Grant Thornton (report available <a href="http://www.agedcare.org.au/news/2012-news/documents/cost-of-care-study" target="_blank">here</a>).</li> </ul> <p><strong>Highest age that your dependants will be financially supported</strong></p> <ul> <li>The default value for the age at which dependants will no longer be financially supported is 18, which is the age at which a person will be treated by law as an adult</li> </ul> <p><strong>Income Protection replacement ratio</strong></p> <ul> <li>The default value for the Income Protection replacement ratio is set at 75% of your before tax income, which is the maximum amount typically payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Income Protection benefit paid to superannuation</strong></p> <ul> <li>The default value for the Income Protection paid to replace foregone superannuation contributions is 10% of your before tax income, which is the typical benefit paid to your superannuation ratio is 10% of your before tax income, which is the typical maximum amount payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Premiums</strong></p> <ul> <li>The estimated premiums and levels of cover for Death, TPD and Salary Continuance determined by this calculator are based on those available in the Product Disclosure Statement dated 29 September 2015</li> </ul> <p><strong>Other</strong></p> <ul> <li>The estimates provided by the calculator do not take account of your other assets, including savings, other insurance that you may have, or any social security benefits you may be entitled to. You should consider these before making any decision about your insurance cover.</li> <li>This calculator assumes that Death, TPD and Salary Continuance cover ceases at age 65.</li> </ul> <p><strong>Disclaimer</strong></p> <p>Insurance cover through CARE Super is provided by MetLife Insurance Limited ABN 75 004 274 882 AFSL No. 238096 (MetLife). This insurance calculator is provided to CARE Super by MetLife and is owned by MetLife. MetLife has prepared this calculator based on the terms and conditions of the CARE Super Pty Ltd (Trustee) insurance policies as they apply in April 2017.</p> <p>The calculator provides illustrative calculations of the amount of Death and TPD insurance cover you may need based on certain assumptions which you are able to change.</p> <p>The information provided by the calculator is not a statement of your superannuation benefits. It has been prepared without taking into account your objectives, financial situation, or needs. You should consider your own circumstances and consider the Product Disclosure Statement for the cover before making a decision about your insurance. You may also wish to consult a licensed financial adviser before making any decision about your insurance or superannuation.</p> <p>CARE Super Pty Ltd and MetLife do not accept any liability for any loss resulting from reliance on information obtained using the calculator.</p> <p>Before applying for any insurance cover within superannuation, you should consider the cost of that cover and ensure that you will be able to pay the premiums from your superannuation account balance.</p>'),
        	'initialOccupationQuestions':[
      	    	{
      	    		'name' : 'workHours',
      	    		'labelText' : $sce.trustAsHtml('Do you work 15 or more hours per week?'),
      	    		'showQuestion' : true
      	    	}
      	    ],
      	    'finalOccupationQuestions': [
      	    	{
      	    		'name' : 'occupationDuties',
      	    		'labelText' : $sce.trustAsHtml('Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar \'white collar\' tasks which do not involve manual work and are undertaken entirely within an office environment (excluding travel time from one office environment to another)?'),
      	    		'showQuestion' : true
      	    	},
      	    	{
      	    		'name' : 'tertiaryQue',
      	    		'labelText' : $sce.trustAsHtml('Do you hold a tertiary qualification or are you a member of a professional institute or registered as a practising member of your profession by a government body?'),
      	    		'showQuestion' : true
      	    	},
      	    	{
      	    		'name' : 'managementRole',
      	    		'labelText' : $sce.trustAsHtml('Are you in a management role?'),
      	    		'showQuestion' : true
      	    	}
      	    ]        
    };
    
    this.hostConfiguration = {
        	'partnerName' : 'HOSTPlus',
        	'helpline' : '1300 467 875',
        	'privacy' : 'https://hostplus.com.au/privacy',
        	'disclaimer' : 'https://hostplus.com.au/disclaimer',
        	'aboutUs' : 'http://www.hostplus.com.au/about',
        	'contactUs' : 'https://hostplus.com.au/help/contact-us',
        	'copyRights' : 'HOSTPLUS Pty Limited ABN 79 008 634 704, AFSL No. 244392.',
        	'deathMinAge' : 12,
        	'deathMaxAge' : 70,
        	'tpdMinAge' : 12,
        	'tpdMaxAge' : 65,
        	'ipMinAge' : 16,
        	'ipMaxAge' : 65,
        	'fixedLabelText' : 'Fixed',
        	'unitisedLabelText' : 'Unitised',
        	'deathLabelText' : 'Death',
        	'tpdLabelText' : 'Total and Permanent Disablement',
        	'ipLabelText' : 'Income Protection',
        	'insuredIpLabelText' : 'Insure 90% of my Salary',
        	'insuredIpValue' : 90,
        	'waitingOpts' : ['30 Days','60 Days', '90 Days'],
        	'benefitOpts' : ['2 Years', 'Age 65'],
        	'defaultWaitingperiod' : '90 Days',
        	'defaultFrequency' : 'Weekly',
        	'deathMaxAmount' : 100000000,
        	'tpdMaxAmount' : 5000000,
        	'ipMaxAmount' : 30000,
        	'tpdLessThanDeath' : false,
        	'haveMortage' : '',
        	'expensePopup' : true,
        	'debtsPopup' : true,
        	'acknowledgeNeeded' : true,
        	'disclaimerText' : $sce.trustAsHtml('<p>The Insurance fee calculator is provided by Host-Plus Pty Limited ABN 79 008 634 704, AFSL No. 244392, the Trustee for Hostplus Superannuation Fund ABN 68 657 495 890.</p><p>The insurance fee calculator provides calculations based on the cost of insurance cover as at 27/04/2018. The premiums for your cover will be subject to change from time to time.</p><p>The information provided by the calculator is not a recommendation or a statement of your superannuation benefits. It has been prepared without taking into account your objectives, financial situation, or needs. You should consider your own circumstances and consider the Product Disclosure Statement for the cover before making a decision about your insurance. You may also wish to consult a licensed financial adviser before making any decision about your insurance or superannuation. You should also reassess your insurance needs regularly as your circumstances and the cost of your cover may change.</p><p>Hostplus does not accept any liability either directly or indirectly, arising from any person relying, either wholly or partially, upon any information, provided by, resulting from, shown in, or omitted from, the calculator. Under no circumstances will Hostplus be liable for any loss or damage caused by a user\'s reliance on information obtained using this calculator.</p><p>Before applying for any insurance cover within superannuation, you should consider the cost of that cover and ensure that you will be able to pay the insurance fee from your superannuation account balance.</p><p>This quotation is for illustrative purposes only. Any offer of cover is subject to the assessment of insurability. To enable our insurer to assess your insurability we require the completion of a personal statement and application for insurance including any other information that may be reasonably required. The insurer relies on the information that you disclose, and other information obtained during the assessment process to determine whether to accept your cover and on what terms.</p><p>I have read the disclaimer and assumptions</p>'),
        	'disclaimerPopupText' : $sce.trustAsHtml('<p>The Insurance fee calculator is provided by Host-Plus Pty Limited ABN 79 008 634 704, AFSL No. 244392, the Trustee for Hostplus Superannuation Fund ABN 68 657 495 890.</p> <p>The insurance fee calculator provides calculations based on the cost of insurance cover as at 27/04/2018. The premiums for your cover will be subject to change from time to time.</p> <p>The information provided by the calculator is not a recommendation or a statement of your superannuation benefits. It has been prepared without taking into account your objectives, financial situation, or needs. You should consider your own circumstances and consider the Product Disclosure Statement for the cover before making a decision about your insurance. You may also wish to consult a licensed financial adviser before making any decision about your insurance or superannuation. You should also reassess your insurance needs regularly as your circumstances and the cost of your cover may change.</p> <p>Hostplus does not accept any liability either directly or indirectly, arising from any person relying, either wholly or partially, upon any information, provided by, resulting from, shown in, or omitted from, the calculator. Under no circumstances will Hostplus be liable for any loss or damage caused by a user\'s reliance on information obtained using this calculator.</p> <p>Before applying for any insurance cover within superannuation, you should consider the cost of that cover and ensure that you will be able to pay the insurance fee from your superannuation account balance.</p> <p>This quotation is for illustrative purposes only. Any offer of cover is subject to the assessment of insurability. To enable our insurer to assess your insurability we require the completion of a personal statement and application for insurance including any other information that may be reasonably required. The insurer relies on the information that you disclose, and other information obtained during the assessment process to determine whether to accept your cover and on what terms.</p> <p><strong>Assumptions:</strong></p> <p><strong>Inflation Rate</strong></p> <ul> <li>Our estimate of the required amount of Death and/or TPD cover includes the cost of paying for future expenses, which requires the calculator to make assumptions in relation to the rate of inflation in the future.</li> <li>The inflation rate is set at a default level of 3.5% per annum (p.a.). This is consistent with the average historic wage inflation in Australia over the last 15 years as measured by increases in Average Wage Price Index (available <a href="http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/6345.0Dec%202015?OpenDocument" target="_blank">here</a>). Wage inflation, as opposed to the Consumer Price Index, has been selected as it is typically higher then CPI.</li> </ul> <p><strong>Investment Return </strong></p> <ul> <li>The default investment return rate has been set at 5.7% p.a. and reflects the median 10 year return (post investment tax) for Growth Risk profile investments per the <a href="https://www.chantwest.com.au/investment-returns/super-fund-returns" target="_blank">Chant West Survey</a>. The growth risk profile is suitable for industry superannuation funds based on asset allocation data available in the APRA 2015 Annual Fund Level Superannuation Statistics Report available <a href="https://www.apra.gov.au/Super/Publications/Pages/superannuation-fund-level-publications.aspx" target="_blank">here</a>.</li> <li>The investment return rate is an estimate of the annual investment return, assuming your superannuation fund\'s insurer has paid a TPD benefit to your superannuation fund, and it has remained inside super to enable you to take advantage of the concessional taxation of investment earnings within the superannuation environment</li> </ul> <p><strong>Superannuation Contribution Rate</strong></p> <ul> <li>The superannuation contribution rate is set at 9.5%. This is the assumed rate of superannuation contributions as a proportion of your before tax income and is based on the current minimum Superannuation Guarantee (SG) contribution rate available <a href="https://www.ato.gov.au/Calculators-and-tools/Super-guarantee-contributions/" target="_blank">here</a> . This is currently 9.5% of ordinary time earnings<li> </ul> <p><strong>Funeral Costs</strong></p> <ul> <li>The default value for funeral costs is $15,000 and is based on MoneySmart\'s estimate for funeral costs (available <a href="https://www.moneysmart.gov.au/life-events-and-you/over-55s/paying-for-your-funeral" target="_blank">here</a> ). The highest figure of the estimated range has been used<li> </ul> <p><strong>Retirement age</strong></p> <ul> <li>The retirement age is the age at which we assume you plan to retire and is set at 65. The default retirement age is based on the age that you can have full access to your superannuation even if you haven\'t retired. (Information <a href="https://www.ato.gov.au/Individuals/Super/Accessing-your-super/" target="_blank">here</a>)</li> </ul> <p><strong>Annual nursing and medical costs</strong></p> <ul> <li>The default value for annual nursing and medical costs (which you may require if you become totally and permanently disabled) is $46,000 and is based on an average of high care and mixed-care nursing services (including personal care, catering, cleaning, etc.) in Australia as surveyed by Grant Thornton (report available <a href="http://www.agedcare.org.au/news/2012-news/documents/cost-of-care-study" target="_blank">here</a>).</li> </ul> <p><strong>Highest age that your dependants will be financially supported</strong></p> <ul> <li>The default value for the age at which dependants will no longer be financially supported is 18, which is the age at which a person will be treated by law as an adult</li> </ul> <p><strong>Income Protection replacement ratio</strong></p> <ul> <li>The default value for the Income Protection replacement ratio is set at 75% of your before tax income, which is the maximum amount typically payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Income Protection benefit paid to superannuation</strong></p> <ul> <li>The default value for the Income Protection paid to replace foregone superannuation contributions is 10% of your before tax income, which is the typical benefit paid to your superannuation ratio is 10% of your before tax income, which is the typical maximum amount payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Premiums</strong></p> <ul> <li>The estimated premiums and levels of cover for Death, TPD and Salary Continuance determined by this calculator are based on those available in the Product Disclosure Statement dated 29 September 2015</li> </ul> <p><strong>Other</strong></p> <ul> <li>The estimates provided by the calculator do not take account of your other assets, including savings, other insurance that you may have, or any social security benefits you may be entitled to. You should consider these before making any decision about your insurance cover.</li> <li>This calculator assumes that Death, TPD and Salary Continuance cover ceases at age 65.</li> </ul>'),
        	'initialOccupationQuestions':[
      	    	{
      	    		'name' : 'workHours',
      	    		'labelText' : $sce.trustAsHtml('Do you work 15 or more hours per week?'),
      	    		'showQuestion' : true
      	    	}
      	    ],
      	    'finalOccupationQuestions': [
      	    	{
      	    		'name' : 'workDuties',
      	    		'labelText' : $sce.trustAsHtml('Do you work in a hazardous environment and/or perform any duties of a manual nature?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'tertiaryQue',
      	    		'labelText' : $sce.trustAsHtml('Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'spendTimeInside',
      	    		'labelText' : $sce.trustAsHtml('Do you spend at least 90% of your working time in an office? (For example 34.2 hours out of a 38-hour working week)'),
      	    		'showQuestion' : false
      	    	}
      	    ]        
    };
    
    this.vicSuperConfiguration = {
        	'partnerName' : 'VIC Super',
        	'helpline' : '1300 366 216',
        	'privacy' : 'https://www.vicsuper.com.au/privacy',
        	'disclaimer' : 'https://www.vicsuper.com.au/disclaimer',
        	'aboutUs' : 'https://www.vicsuper.com.au/about-us',
        	'contactUs' : 'https://www.vicsuper.com.au/contact-us',
        	'copyRights' : 'VicSuper Pty Ltd ABN 69 087 619 412, VicSuper Fund 85 977 964 496, AFSL No. 237333',
        	'deathMinAge' : 14,
        	'deathMaxAge' : 69,
        	'tpdMinAge' : 14,
        	'tpdMaxAge' : 69,
        	'ipMinAge' : 14,
        	'ipMaxAge' : 64,
        	'fixedLabelText' : 'Fixed',
        	'unitisedLabelText' : 'Unit-based',
        	'deathLabelText' : 'Death',
        	'tpdLabelText' : 'Total and Permanent Disablement',
        	'ipLabelText' : 'Income protection',
        	'insuredIpLabelText' : 'Insure 90% of my Salary',
        	'insuredIpValue' : 90,
        	'waitingOpts' : ['30 Days','60 Days', '90 Days'],
        	'benefitOpts' : ['2 Years', '5 Years'],
        	'defaultWaitingperiod' : '90 Days',
        	'defaultFrequency' : 'Weekly',
        	'deathMaxAmount' : 99999999,
        	'tpdMaxAmount' : 450000,
        	'ipMaxAmount' : 30000,
        	'tpdLessThanDeath' : false,
        	'haveMortage' : '',
        	'expensePopup' : true,
        	'debtsPopup' : true,
        	'acknowledgeNeeded' : true,
        	'disclaimerText' : '',
        	'initialOccupationQuestions':[
      	    	{
      	    		'name' : 'workHours',
      	    		'labelText' : $sce.trustAsHtml('Do you work 15 or more hours per week'),
      	    		'showQuestion' : true
      	    	}
      	    ],
      	    'finalOccupationQuestions': [
      	    	{
      	    		'name' : 'occupationDuties',
      	    		'labelText' : $sce.trustAsHtml('Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar \'white collar\' tasks which do not involve manual work and are undertaken entirely within an office environment (excluding travel time from one office environment to another)?'),
      	    		'showQuestion' : true
      	    	},
      	    	{
      	    		'name' : 'tertiaryQue',
      	    		'labelText' : $sce.trustAsHtml('Do you hold a tertiary qualification or are you a member of a professional institute or registered as a practising member of your profession by a government body?'),
      	    		'showQuestion' : true
      	    	},
      	    	{
      	    		'name' : 'managementRole',
      	    		'labelText' : $sce.trustAsHtml('Are you in a management role?'),
      	    		'showQuestion' : true
      	    	}
      	    ]        
    };
    
    this.stateWideConfiguration = {
        	'partnerName' : 'State Wide',
        	'helpline' : '1300 65 18 65',
        	'privacy' : 'https://www.statewide.com.au/privacy-policy/',
        	'disclaimer' : 'https://www.statewide.com.au/terms-and-conditions/',
        	'aboutUs' : 'http://www.statewide.com.au/about_us',
        	'contactUs' : 'https://www.statewide.com.au/about-us/',
        	'copyRights' : $sce.trustAsHtml('&copy; Statewide Superannuation Pty Ltd ABN 62 008 099 223 AFSL 243171.'),
        	'deathMinAge' : 16,
        	'deathMaxAge' : 70,
        	'tpdMinAge' : 16,
        	'tpdMaxAge' : 70,
        	'ipMinAge' : 16,
        	'ipMaxAge' : 67,
        	'fixedLabelText' : 'Fixed',
        	'unitisedLabelText' : 'Unitised',
        	'deathLabelText' : 'Death',
        	'tpdLabelText' : 'Total and Permanent Disablement',
        	'ipLabelText' : 'Income protection',
        	'insuredIpLabelText' : 'Insure 90% of my Salary',
        	'insuredIpValue' : 90,
        	'waitingOpts' : ['45 Days'],
        	'benefitOpts' : ['2 Years', 'Age 65'],
        	'defaultWaitingperiod' : '45 Days',
        	'defaultFrequency' : 'Weekly',
        	'deathMaxAmount' : 5000000,
        	'tpdMaxAmount' : 3000000,
        	'ipMaxAmount' : 30000,
        	'tpdLessThanDeath' : true,
        	'haveMortage' : '',
        	'expensePopup' : true,
        	'debtsPopup' : true,
        	'acknowledgeNeeded' : true,
        	'disclaimerText' : '',
        	'disclaimerPopupText' : $sce.trustAsHtml('<p>This calculator is provided by Statewide Superannuation Pty Ltd, ABN 62 008 099 223, AFSL 243171 (Statewide Super), the Trustee and RSE licensee of Statewide Superannuation Trust ABN 54 145 196 298. The calculator is provided in good faith and produces estimates only based on the information you input and certain assumptions. This calculator is based on the terms and conditions of the Statewide Super insurance policy as at 10 March 2017.</p> <p>The information resulting from the calculations should not be relied upon as a true representation of any actual insurance entitlements or benefits from any particular scheme or relied on as a basis upon which to alter your insurance or financial arrangements without advice from a professional.</p> <p>This information is not a substitute for professional advice from a qualified financial adviser. </p> <p>Statewide Super does not accept any liability, either directly or indirectly, arising from any person relying, either wholly or partially, upon any information, provided by, resulting from, shown in, or omitted from, the calculator. Under no circumstances will Statewide Super be liable for any loss or damage caused by a user\'s reliance on information obtained using this calculator.</p> <p>This information has been prepared without taking into account your particular financial needs, circumstances and objectives and is therefore not suitable to be acted upon as investment or financial advice. You should assess your own financial affairs and consult a financial adviser before you make any changes to your financial affairs.</p> <p>Increasing your Death, Total Permanent Disability (TPD) and Income Protection insurance premiums will reduce your retirement account balance. </p> <p>Before applying for any additional Death, TPD or Income Protection cover, you should always check the total cost of that cover. If your application for additional cover is accepted you must ensure you maintain sufficient funds in your superannuation account to pay for the ongoing premiums.</p> <p>The calculated cover only considers your total life insurance need. Before applying for insurance, you should also consider any alternative means you have to support this need, which might include existing assets, other insurance policies, superannuation and/or government provisions that you would be able to access.</p> <p><strong>Assumptions:</strong></p> <p><strong>Inflation Rate</strong></p> <ul> <li>Our estimate of the required amount of Death and/or TPD cover includes the cost of paying for future expenses, which requires the calculator to make assumptions in relation to the rate of inflation in the future.</li> <li>The inflation rate is set at a default level of 3.5% per annum (p.a.). This is consistent with the average historic wage inflation in Australia over the last 15 years as measured by increases in Average Wage Price Index (available <a href="http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/6345.0Dec%202015?OpenDocument" target="_blank">here</a>). Wage inflation, as opposed to the Consumer Price Index, has been selected as it is typically higher then CPI.</li> </ul> <p><strong>Investment Return </strong></p> <ul> <li>The default investment return rate has been set at 5.7% p.a. and reflects the median 10 year return (post investment tax) for Growth Risk profile investments per the <a href="https://www.chantwest.com.au/investment-returns/super-fund-returns" target="_blank">Chant West Survey</a>. The growth risk profile is suitable for industry superannuation funds based on asset allocation data available in the APRA 2015 Annual Fund Level Superannuation Statistics Report available <a href="https://www.apra.gov.au/Super/Publications/Pages/superannuation-fund-level-publications.aspx" target="_blank">here</a>.</li> <li>The investment return rate is an estimate of the annual investment return, assuming your superannuation fund\'s insurer has paid a TPD benefit to your superannuation fund, and it has remained inside super to enable you to take advantage of the concessional taxation of investment earnings within the superannuation environment</li> </ul> <p><strong>Superannuation Contribution Rate</strong></p> <ul> <li>The superannuation contribution rate is set at 9.5%. This is the assumed rate of superannuation contributions as a proportion of your before tax income and is based on the current minimum Superannuation Guarantee (SG) contribution rate available <a href="https://www.ato.gov.au/Calculators-and-tools/Super-guarantee-contributions/" target="_blank">here</a> . This is currently 9.5% of ordinary time earnings</li> </ul> <p><strong>Funeral Costs</strong></p> <ul> <li>The default value for funeral costs is $15,000 and is based on MoneySmart\'s estimate for funeral costs (available <a href="https://www.moneysmart.gov.au/life-events-and-you/over-55s/paying-for-your-funeral" target="_blank">here</a> ). The highest figure of the estimated range has been used</li> </ul> <p><strong>Retirement age</strong></p> <ul> <li>The retirement age is the age at which we assume you plan to retire and is set at 65. The default retirement age is based on the age that you can have full access to your superannuation even if you haven\'t retired. (Information <a href="https://www.ato.gov.au/Individuals/Super/Accessing-your-super/" target="_blank">here</a>)</li> </ul> <p><strong>Annual nursing and medical costs</strong></p> <ul> <li>The default value for annual nursing and medical costs (which you may require if you become totally and permanently disabled) is $46,000 and is based on an average of high care and mixed-care nursing services (including personal care, catering, cleaning, etc.) in Australia as surveyed by Grant Thornton (report available <a href="http://www.agedcare.org.au/news/2012-news/documents/cost-of-care-study" target="_blank">here</a>).</li> </ul> <p><strong>Highest age that your dependants will be financially supported</strong></p> <ul> <li>The default value for the age at which dependants will no longer be financially supported is 18, which is the age at which a person will be treated by law as an adult</li> </ul> <p><strong>Income Protection replacement ratio</strong></p> <ul> <li>The default value for the Income Protection replacement ratio is set at 75% of your before tax income, which is the maximum amount typically payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Income Protection benefit paid to superannuation</strong></p> <ul> <li>The default value for the Income Protection paid to replace foregone superannuation contributions is 10% of your before tax income, which is the typical benefit paid to your superannuation ratio is 10% of your before tax income, which is the typical maximum amount payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Premiums</strong></p> <ul> <li>The estimated premiums and levels of cover for Death, TPD and Salary Continuance determined by this calculator are based on those available in the Product Disclosure Statement dated 29 September 2015</li> </ul> <p><strong>Other</strong></p> <ul> <li>The estimates provided by the calculator do not take account of your other assets, including savings, other insurance that you may have, or any social security benefits you may be entitled to. You should consider these before making any decision about your insurance cover.</li> <li>This calculator assumes that Death, TPD and Salary Continuance cover ceases at age 65.</li> </ul>'),
        	'initialOccupationQuestions':[
      	    	{
      	    		'name' : 'workHours',
      	    		'labelText' : $sce.trustAsHtml('Do you work 15 or more hours per week?'),
      	    		'showQuestion' : true
      	    	}
      	    ],
      	    'finalOccupationQuestions': [
      	    	{
      	    		'name' : 'spendTimeInside',
      	    		'labelText' : $sce.trustAsHtml('Do you work wholly within an office environment?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'tertiaryQue',
      	    		'labelText' : $sce.trustAsHtml('Do you hold a tertiary qualification relevant to your current occupation or is a member of a professional institute or registered by a government body, or is a member of the executive leadership team of the employer with more than 10 years\' experience in the applicable industry?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'workDuties',
      	    		'labelText' : $sce.trustAsHtml('Do you work in a hazardous environment and/or perform any duties of a manual nature?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'spendTimeOutside',
      	    		'labelText' : $sce.trustAsHtml('Do you spend more than 20% of your working time outside of an office environment?'),
      	    		'showQuestion' : false
      	    	}
      	    ]        
    };
    
    this.firstSuperConfiguration = {
        	'partnerName' : 'FIRST Super',
        	'helpline' : '1300 360 988',
        	'privacy' : 'http://www.firstsuper.com.au/privacy',
        	'disclaimer' : 'https://www.firstsuper.com.au/disclaimer/',
        	'aboutUs' : 'https://www.firstsuper.com.au/about-us/',
        	'contactUs' : 'https://www.firstsuper.com.au/contact-us/',
        	'copyRights' : $sce.trustAsHtml('&copy; Copyright First Super'),
        	'introText' : 'To help you find out how much insurance cover you could need First Super have provided you with two calculators. A QUICK Quote calculator that is useful if you are looking for a fast estimated quote based on a particular amount of insurance cover and a more comprehensive calculator that lets you take into account all your personal and financial details.',
        	'premiumLink': true,
        	'deathMinAge' : 12,
        	'deathMaxAge' : 70,
        	'tpdMinAge' : 12,
        	'tpdMaxAge' : 70,
        	'ipMinAge' : 16,
        	'ipMaxAge' : 65,
        	'fixedLabelText' : 'Fixed',
        	'unitisedLabelText' : 'Unitised',
        	'deathLabelText' : 'Death',
        	'tpdLabelText' : 'Total and Permanent Disablement',
        	'ipLabelText' : 'Income protection',
        	'insuredIpLabelText' : 'Insure 90% of my Salary',
        	'insuredIpValue' : 90,
        	'waitingOpts' : ['30 Days','60 Days', '90 Days'],
        	'benefitOpts' : ['2 Years'],
        	'defaultWaitingperiod' : '90 Days',
        	'defaultFrequency' : 'Monthly',
        	'deathMaxAmount' : 100000000,
        	'tpdMaxAmount' : 5000000,
        	'ipMaxAmount' : 25000,
        	'tpdLessThanDeath' : true,
        	'haveMortage' : 'Yes',
        	'expensePopup' : false,
        	'debtsPopup' : false,
        	'acknowledgeNeeded' : true,
        	'disclaimerText' : '',
        	'disclaimerPopupText' : $sce.trustAsHtml('<p>This insurance calculator is provided by First Super ABN 42 053 498 472, AFSL 223988, the Trustee of First Super ABN 56 286 625 181. First Super is authorised under its Australian financial service licence to provide financial product advice in relation to superannuation, including superannuation products that provide insured Death, TPD and Superannuation benefits.</p> <p>The calculator provides illustrative calculations of the amount of Death and TPD insurance cover you may need based on certain assumptions which you are able to change. The assumptions are based on the law as it applies in April 2016.</p> <p>The information provided by the calculator is not a statement of your superannuation benefits. It has been prepared without taking into account your objectives, financial situation, or needs. You should consider your own circumstances and consider the Product Disclosure Statement for the cover before making a decision about your insurance. You may also wish to consult a licensed financial adviser before making any decision about your insurance or superannuation.</p> <p>First Super does not accept any liability for any loss resulting from reliance on information obtained using the calculator.</p> <p>Before applying for any insurance cover within superannuation, you should consider the cost of that cover and ensure that you will be able to pay the premiums from your superannuation account balance.</p> <p><strong>Assumptions:</strong></p> <p><strong>Inflation Rate</strong></p> <ul> <li>Our estimate of the required amount of Death and/or TPD cover includes the cost of paying for future expenses, which requires the calculator to make assumptions in relation to the rate of inflation in the future.</li> <li>The inflation rate is set at a default level of 3.5% per annum (p.a.). This is consistent with the average historic wage inflation in Australia over the last 15 years as measured by increases in Average Wage Price Index (available <a href="http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/6345.0Dec%202015?OpenDocument" target="_blank">here</a>). Wage inflation, as opposed to the Consumer Price Index, has been selected as it is typically higher then CPI.</li> </ul> <p><strong>Investment Return </strong></p> <ul> <li>The default investment return rate has been set at 5.7% p.a. and reflects the median 10 year return (post investment tax) for Growth Risk profile investments per the <a href="https://www.chantwest.com.au/investment-returns/super-fund-returns" target="_blank">Chant West Survey</a>. The growth risk profile is suitable for industry superannuation funds based on asset allocation data available in the APRA 2015 Annual Fund Level Superannuation Statistics Report available <a href="https://www.apra.gov.au/Super/Publications/Pages/superannuation-fund-level-publications.aspx" target="_blank">here</a>.</li> <li>The investment return rate is an estimate of the annual investment return, assuming your superannuation fund\'s insurer has paid a TPD benefit to your superannuation fund, and it has remained inside super to enable you to take advantage of the concessional taxation of investment earnings within the superannuation environment</li> </ul> <p><strong>Superannuation Contribution Rate</strong></p> <ul> <li>The superannuation contribution rate is set at 9.5%. This is the assumed rate of superannuation contributions as a proportion of your before tax income and is based on the current minimum Superannuation Guarantee (SG) contribution rate available <a href="https://www.ato.gov.au/Calculators-and-tools/Super-guarantee-contributions/" target="_blank">here</a> . This is currently 9.5% of ordinary time earnings</li> </ul> <p><strong>Funeral Costs</strong></p> <ul> <li>The default value for funeral costs is $15,000 and is based on MoneySmart\'s estimate for funeral costs (available <a href="https://www.moneysmart.gov.au/life-events-and-you/over-55s/paying-for-your-funeral" target="_blank">here</a> ). The highest figure of the estimated range has been used</li> </ul> <p><strong>Retirement age</strong></p> <ul> <li>The retirement age is the age at which we assume you plan to retire and is set at 65. The default retirement age is based on the age that you can have full access to your superannuation even if you haven\'t retired. (Information <a href="https://www.ato.gov.au/Individuals/Super/Accessing-your-super/" target="_blank">here</a>)</li> </ul> <p><strong>Annual nursing and medical costs</strong></p> <ul> <li>The default value for annual nursing and medical costs (which you may require if you become totally and permanently disabled) is $46,000 and is based on an average of high care and mixed-care nursing services (including personal care, catering, cleaning, etc.) in Australia as surveyed by Grant Thornton (report available <a href="http://www.agedcare.org.au/news/2012-news/documents/cost-of-care-study" target="_blank">here</a>).</li> </ul> <p><strong>Highest age that your dependants will be financially supported</strong></p> <ul> <li>The default value for the age at which dependants will no longer be financially supported is 18, which is the age at which a person will be treated by law as an adult</li> </ul> <p><strong>Income Protection replacement ratio</strong></p> <ul> <li>The default value for the Income Protection replacement ratio is set at 75% of your before tax income, which is the maximum amount typically payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Income Protection benefit paid to superannuation</strong></p> <ul> <li>The default value for the Income Protection paid to replace foregone superannuation contributions is 10% of your before tax income, which is the typical benefit paid to your superannuation ratio is 10% of your before tax income, which is the typical maximum amount payable under income protection policies issued by life insurers to superannuation funds</li> </ul> <p><strong>Premiums</strong></p> <ul> <li>The estimated premiums and levels of cover for Death, TPD and Salary Continuance determined by this calculator are based on those available in the Product Disclosure Statement dated 29 September 2015</li> </ul> <p><strong>Other</strong></p> <ul> <li>The estimates provided by the calculator do not take account of your other assets, including savings, other insurance that you may have, or any social security benefits you may be entitled to. You should consider these before making any decision about your insurance cover.</li> <li>This calculator assumes that Death, TPD and Salary Continuance cover ceases at age 65.</li> </ul>'),
        	'initialOccupationQuestions':[
      	    	{
      	    		'name' : 'workHours',
      	    		'labelText' : $sce.trustAsHtml('Do you work more than 15 hours per week?'),
      	    		'showQuestion' : true
      	    	}
      	    ],
      	    'finalOccupationQuestions': [      	    	
      	    	{
      	    		'name' : 'occupationDuties',
      	    		'labelText' : $sce.trustAsHtml('Are your duties entirely undertaken within an office environment?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'tertiaryQue',
      	    		'labelText' : $sce.trustAsHtml('Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'spendTimeInside',
      	    		'labelText' : $sce.trustAsHtml('Do you spend at least 80% of all working time in an office environment?'),
      	    		'showQuestion' : false
      	    	}
      	    ]        
    };
    
    this.guildConfiguration = {
        	'partnerName' : 'GUILD Super',
        	'helpline' : '1300 361 477',
        	'privacy' : 'https://guildsuper.com.au/privacy',
        	'disclaimer' : 'http://www.guildsuper.com.au/about/disclaimer',
        	'aboutUs' : 'http://www.guildsuper.com.au/about',
        	'contactUs' : 'http://www.guildsuper.com.au/contact-us',
        	'copyRights' : $sce.trustAsHtml('&copy; Guild Trustee Services Pty Limited ABN 84 068 826 728, AFS Licence No. 233815 as Trustee for GuildSuper ABN 22 599 554 834'),
        	'deathMinAge' : 16,
        	'deathMaxAge' : 70,
        	'tpdMinAge' : 16,
        	'tpdMaxAge' : 70,
        	'ipMinAge' : 16,
        	'ipMaxAge' : 65,
        	'fixedLabelText' : 'Fixed',
        	'unitisedLabelText' : 'Unitised',
        	'deathLabelText' : 'Death',
        	'tpdLabelText' : 'Total and Permanent Disablement',
        	'ipLabelText' : 'Income protection',
        	'insuredIpLabelText' : 'Insure 90% of my Salary',
        	'insuredIpValue' : 90,
        	'waitingOpts' : ['30 Days','60 Days', '90 Days'],
        	'benefitOpts' : ['5 Years','Age 65'],
        	'defaultWaitingperiod' : '90 Days',
        	'defaultFrequency' : 'Weekly',
        	'deathMaxAmount' : 5000000,
        	'tpdMaxAmount' : 3000000,
        	'ipMaxAmount' : 30000,
        	'tpdLessThanDeath' : false,
        	'haveMortage' : '',
        	'expensePopup' : true,
        	'debtsPopup' : true,
        	'acknowledgeNeeded' : true,
        	'disclaimerText' : '',
        	'initialOccupationQuestions':[
      	    	{
      	    		'name' : 'workHours',
      	    		'labelText' : $sce.trustAsHtml('Do you work more than 15 hours per week?'),
      	    		'showQuestion' : true
      	    	}
      	    ],
      	    'finalOccupationQuestions': [
      	    	{
      	    		'name' : 'tertiaryQue',
      	    		'labelText' : $sce.trustAsHtml('Are you tertiary qualified and a member of a professional institute, registered with a government body or an executive with more than 10 years industry experience?'),
      	    		'showQuestion' : false
      	    	},
      	    	{
      	    		'name' : 'occupationDuties',
      	    		'labelText' : $sce.trustAsHtml('Are the duties of your occupation limited to professional, managerial, administrative, clerical, secretarial or similar white collar tasks that do not involve manual work and take place entirely (or at least 80%) within an office environment (excluding travel time from one office environment to another)?'),
      	    		'showQuestion' : false
      	    	}      	    	
      	    ]        
    };
    
  }

/*
* Partner configuration return method
*  
*/
  getPartnerBasedConfiguration(fundID){
	  
	  let configuration;
	  
	  switch(fundID) {
	  
	  	case 'CARE':
	  		configuration = this.careConfiguration;	  	    
	  		break;
	  	case 'FIRS':
	  		configuration = this.firstSuperConfiguration;	  	    
	  		break;
	  	case 'HOST':
	  		configuration = this.hostConfiguration;	  	    
	  		break;
	  	case 'SFPS':
	  		configuration = this.stateWideConfiguration;	  	    
	  		break;
	  	/*case 'VICT':
	  		configuration = this.vicSuperConfiguration;	  	    
	  		break; */
	  	case 'GUIL':
	  		configuration = this.guildConfiguration;	  	    
	  		break;
	  		
	  	default:
	  		break;
	  }
		return configuration;
  }
}
