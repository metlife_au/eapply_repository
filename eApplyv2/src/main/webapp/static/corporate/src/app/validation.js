//for numeric validation
 function isNumeric(target){

 	if (null!= document.getElementById(target)) {
	 	var v=document.getElementById(target).value;
	 	var regExp= /^[0-9]*$/;
	 					 	
	 	if (v.length > 0){
				
			if ((v.charAt(v.length-1)).match(regExp)) {					 			
				return true;
	 		}else {
		 		document.getElementById(target).value=v.substring(0,v.length-1);
		 		return false;
	 		}
	 	}
 	}	 
 	
 }
 function isObject( what ) { return (typeof what === 'object'); } 


function isAlphaNumeric(target){
 	var v=document.getElementById(target).value;
 	var regExp= /^[0-9 a-z A-Z]*$/;
	var str1, str2;
 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);

 			document.getElementById(target).value=str1+str2;
 	
 		return false;
 		}
 	}
 } 

 function isAlphaWithSlash(target){
	if (null!= document.getElementById(target)) {
		var v=document.getElementById(target).value;
		var regExp= /^[0-9/]*$/;
							
		if (v.length > 0){
			
		if ((v.charAt(v.length-1)).match(regExp)) {					 			
			return true;
			}else {
				document.getElementById(target).value=v.substring(0,v.length-1);
				return false;
			}
		}
	}	 
	
}

function isAlpha(target){

 	if (null!= document.getElementById(target)) {
	 	var v=document.getElementById(target).value;
	 	var regExp= /^[a-z A-Z]*$/;
	 					 	
	 	if (v.length > 0){
				
			if ((v.charAt(v.length-1)).match(regExp)) {					 			
				return true;
	 		}else {
		 		document.getElementById(target).value=v.substring(0,v.length-1);
		 		return false;
	 		}
	 	}
 	}	 
 	
 }
 

function isIntegerFun(obj){
 	var v=obj.value;
	var regExp= /^[0-9]*$/;
	var str1, str2;
 	for(var k=0;k<v.length;k++){
 		if (!(v.charAt(k)).match(regExp)) {
 			str1=v.substring(0, k);
 			str2=v.substring(k+1);
 			obj.value=str1+str2;
 			return false;
 		}
 	}
 }

function visible(idshow) {
	if(document.getElementById(idshow) != null){
		document.getElementById(idshow).style.visibility = 'visible';
	}
}
 		