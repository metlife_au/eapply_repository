package pages.metlife.Angular.CareSuper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class CareSuper_Angular_SaveandExit extends MasterPage {
	WebDriverUtil driverUtil = null;

	public CareSuper_Angular_SaveandExit SaveandExit_CareSuper() {
		ChangeYourCover();
		return new CareSuper_Angular_SaveandExit(scriptHelper);
	}



	public CareSuper_Angular_SaveandExit(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ChangeYourCover() {

		String EmailId = dataTable.getData("CareSuper_SaveandExit", "EmailId");
		String TypeofContact = dataTable.getData("CareSuper_SaveandExit", "TypeofContact");
		String ContactNumber = dataTable.getData("CareSuper_SaveandExit", "ContactNumber");
		String TimeofContact = dataTable.getData("CareSuper_SaveandExit", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("CareSuper_SaveandExit", "FifteenHoursWork");
		String IndustryType = dataTable.getData("CareSuper_SaveandExit", "IndustryType");
		String OccupationType = dataTable.getData("CareSuper_SaveandExit", "OccupationType");
		String OtherOccupation = dataTable.getData("CareSuper_SaveandExit", "OtherOccupation");
		String WithinOffice=dataTable.getData("CareSuper_SaveandExit", "WithinOffice");
		String TertiaryQual = dataTable.getData("CareSuper_SaveandExit", "TertiaryQual");
		String ManagementRole = dataTable.getData("CareSuper_SaveandExit", "ManagementRole");

		String AnnualSalary = dataTable.getData("CareSuper_SaveandExit", "AnnualSalary");
		String DeathYN=dataTable.getData("CareSuper_SaveandExit", "DeathYN");
		String AmountType=dataTable.getData("CareSuper_SaveandExit", "AmountType");
		String DeathAction=dataTable.getData("CareSuper_SaveandExit", "DeathAction");
		String indexfixedcover5percent=dataTable.getData("CareSuper_SaveandExit", "indexfixedcover5percent");
		String DeathAmount=dataTable.getData("CareSuper_SaveandExit", "DeathAmount");
		String DeathUnits=dataTable.getData("CareSuper_SaveandExit", "DeathUnits");
		String TPDYN=dataTable.getData("CareSuper_SaveandExit", "TPDYN");
		String TPDAction=dataTable.getData("CareSuper_SaveandExit", "TPDAction");

		String TPDAmount=dataTable.getData("CareSuper_SaveandExit", "TPDAmount");
		String TPDUnits=dataTable.getData("CareSuper_SaveandExit", "TPDUnits");
		String IPYN=dataTable.getData("CareSuper_SaveandExit", "IPYN");
		String IPAction=dataTable.getData("CareSuper_SaveandExit", "IPAction");
		String Insure85Percent=dataTable.getData("CareSuper_SaveandExit", "Insure85Percent");
		String WaitingPeriod=dataTable.getData("CareSuper_SaveandExit", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("CareSuper_SaveandExit", "BenefitPeriod");
		String IPAmount=dataTable.getData("CareSuper_SaveandExit", "IPAmount");





		try{




			//****Click on Change Your Insurance Cover Link*****\\
			sleep(200);
			JavascriptExecutor a = (JavascriptExecutor)driver;
			a.executeScript("window.scrollBy(0,750)", "");
			Thread.sleep(2000);
		    sleep(1500);
			WebDriverWait waiting=new WebDriverWait(driver,30);
			waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-12 col-md-8']//button)[1]"))).click();
			sleep(3000);
			report.updateTestLog("Apply Death Cover", "Apply Death Cover Link is Clicked", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[text()='Cancel']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(1000);

			}
			WebDriverWait wait=new WebDriverWait(driver,18);

			//*****Agree to Duty of disclosure*******\\
			 JavascriptExecutor b = (JavascriptExecutor)driver;
				b.executeScript("window.scrollBy(0,700)", "");
				Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			sleep(400);


           //*****Agree to Privacy Statement*******\\
			JavascriptExecutor c = (JavascriptExecutor)driver;
			c.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			sleep(400);

			//*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


		//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath("(//label/input[@value='Morning (9am - 12pm)']/./following::span)[1]")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath("(//label/input[@value='Afternoon (12pm - 6pm)']/./following::span)[1]")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}



			//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);


				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\
			JavascriptExecutor d = (JavascriptExecutor)driver;
			d.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(2000);

			//*****Select the 15 Hours Question*******\\

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='fifteenHrsQuestion']/./following::span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='fifteenHrsQuestion']/./following::span)[2]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}



		//*****Industry********\\

	    sleep(500);
	    Select Industry=new Select(driver.findElement(By.name("industry")));

	    Industry.selectByVisibleText(IndustryType);
	    sleep(1000);
	    WebElement inds =Industry.getFirstSelectedOption();
	    System.out.println(inds);
	    sleep(800);
		report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
		sleep(500);

		//*****Occupation*****\\

		Select Occupation=new Select(driver.findElement(By.name("occupation")));
		Occupation.selectByVisibleText(OccupationType);
	    sleep(800);
		report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
		sleep(500);
		JavascriptExecutor e = (JavascriptExecutor)driver;
		e.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(2000);

		//*****Other Occupation *****\\

		if(OccupationType.equalsIgnoreCase("Other")){

			wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
			report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
			sleep(1000);
		}


        //********************************************************\\
		//***************Extra Questions**************************\\
		//*********************************************************\\


	    //***Are the duties of your occupation limited to professional, managerial, administrative, clerical,
		//secretarial or similar 'white collar' tasks which do not involve manual work and are undertaken
		//entirely within an office environment (excluding travel time from one office environment to
		//another)? ***\\

		if(WithinOffice.equalsIgnoreCase("Yes")){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='withinOfficeQuestion']/./following::span)[1]"))).click();
		report.updateTestLog("Work within Office", "Yes is Selected", Status.PASS);
		sleep(500);

		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='withinOfficeQuestion']/./following::span)[2]"))).click();
			report.updateTestLog("Work within Office", "No is Selected", Status.PASS);
			sleep(500);
		}

		//****Do you hold a tertiary qualification or are you a member of a professional institute or
		//registered as a practising member of your profession by a government body?

		if(TertiaryQual.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='tertiaryQuestion']/./following::span)[1]"))).click();
			report.updateTestLog("Tertiary Qualification", "Yes is Selected", Status.PASS);
			sleep(500);
		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='tertiaryQuestion']/./following::span)[2]"))).click();
			report.updateTestLog("Tertiary Qualification", "No is Selected", Status.PASS);
			sleep(500);
		}

		//**********Are you in a management role? ***************\\

		if(ManagementRole.equalsIgnoreCase("Yes")){
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='managementRole']/./following::span)[1]"))).click();
			report.updateTestLog("Mangement Role", "Yes is Selected", Status.PASS);
			sleep(500);
		}
		else{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='managementRole']/./following::span)[1]"))).click();
			report.updateTestLog("Mangement Role", "No is Selected", Status.PASS);
			sleep(500);
		}


	//*****What is your annual Salary******


	    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
	    sleep(500);
	    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

	    JavascriptExecutor f = (JavascriptExecutor)driver;
		f.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(2000);

	//*****Click on Continue*******\\

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


//**************************************************************************************************************\\
//****************************COVER CALCULATOR*****************************************************************\\
//*************************************************************************************************************\\




	//*******Death Cover Section******\\

	if(DeathYN.equalsIgnoreCase("Yes")){

		Select deathaction=new Select(driver.findElement(By.name("coverName")));
		deathaction.selectByVisibleText(DeathAction);
		sleep(300);
		report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
		sleep(600);

		if(AmountType.equalsIgnoreCase("Convert")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@ng-model='coverType']/./following::span)[1]"))).click();
			sleep(800);
			report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

		}

		if(DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){
//		if(DeathYN.equalsIgnoreCase("Yes")&& DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){

		if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert")){
			if(indexfixedcover5percent.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@id='changecvr_index-dth']/./following::span)[1]"))).click();
				sleep(800);
			    report.updateTestLog("Index 5% of Death Cover","is Selected", Status.PASS);
			}
			sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);
				sleep(800);
				report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
				sleep(800);


		}
		else{
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='death']/div[2]/div[6]/div[2]/h4"))).click();

			sleep(200);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathUnits);
			sleep(800);
			report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
			sleep(800);
		}
	}
	}
	JavascriptExecutor w = (JavascriptExecutor)driver;
	w.executeScript("window.scrollBy(0,900)", "");
	Thread.sleep(2000);
	//*******TPD Cover Section******\\
    sleep(800);


	if(TPDYN.equalsIgnoreCase("Yes"))

	{

		Select tpdaction=new Select(driver.findElement(By.name("tpdCoverName")));
		tpdaction.selectByVisibleText(TPDAction);
		sleep(500);
		report.updateTestLog("TPD Cover action", TPDAction+" is selected on TPD cover", Status.PASS);
		sleep(800);



		if(TPDAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Decrease your cover"))
		{

			if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert"))
			{
				if(indexfixedcover5percent.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label/input[@id='changecvr_index-dth']/./following::span)[7]"))).click();
					sleep(800);
				    report.updateTestLog("Index 5% of Death Cover","is Selected", Status.PASS);
				}
				sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDAmount);
				sleep(800);
				report.updateTestLog("TPD Cover Amount", TPDAmount+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
				sleep(800);
			}

			else
			{

				wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(TPDUnits);
				sleep(800);
				report.updateTestLog("TPD Cover Amount", TPDUnits+" is entered", Status.PASS);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
				sleep(800);
			}

		}


	}

	JavascriptExecutor v = (JavascriptExecutor)driver;
	v.executeScript("window.scrollBy(0,1500)", "");
	Thread.sleep(2000);


                 //*******IP Cover section********\\
sleep(500);
if(IPYN.equalsIgnoreCase("Yes")){

	Select ipaction=new Select(driver.findElement(By.name("ipCoverName")));
	ipaction.selectByVisibleText(IPAction);
	sleep(500);
	report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);

	//*****Insure 85% of My Salary******\\

	if(!IPAction.equalsIgnoreCase("No change")){
		if(Insure85Percent.equalsIgnoreCase("Yes")){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ipsalarychecklabel']/span"))).click();
		report.updateTestLog("Insure 90%", "Insure 85% of My Salary is Selected", Status.PASS);
		sleep(500);
		}
	}
	sleep(500);
	if(!IPAction.equalsIgnoreCase("No change")){

		Select waitingperiod=new Select(driver.findElement(By.xpath("//*[@ng-model='waitingPeriodAddnl']")));
		waitingperiod.selectByVisibleText(WaitingPeriod);
		sleep(300);
		report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
		sleep(500);


		Select benefitperiod=new Select(driver.findElement(By.xpath("//*[@ng-model='benefitPeriodAddnl']")));
		benefitperiod.selectByVisibleText(BenefitPeriod);
		sleep(300);
		report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
		sleep(500);
	}
	sleep(500);
	if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")){

		if(!Insure85Percent.equalsIgnoreCase("Yes"))
		{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@name='IPRequireCover'])[2]"))).sendKeys(IPAmount);
			sleep(800);
			report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
			sleep(800);

		}
		sleep(500);
////		if(IPAction.equalsIgnoreCase("Decrease your cover")){
//
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(IPAmount);
//				sleep(800);
//				report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
//				sleep(800);

			}

	}

JavascriptExecutor n = (JavascriptExecutor)driver;
n.executeScript("window.scrollBy(0,700)", "");
Thread.sleep(2000);

//*****Click on Calculate Quote********\\

sleep(500);
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
sleep(3000);


			//*****Check the terms label*****\\

			driver.findElement(By.xpath("//*[@id='termsLabel']/span")).click();
			report.updateTestLog("Terms Label", "Checkbox is Selected", Status.PASS);
			sleep(3000);

			//*****Click on Save and Exit ********\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@ng-click='saveQuote()'])[1]"))).click();
			report.updateTestLog("Save and Exit button ", "Save and Exit button is Clicked", Status.PASS);
			sleep(3000);

         //*****Finish and close ********\\

			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Finish & Close Window ']"))).click();
			report.updateTestLog("Finish and close button ", "Finish and close button is Clicked", Status.PASS);
			sleep(4000);



		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
