package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.OAMPSConfirmationPageObjects;
import uimap.metlife.OAMPSHealthPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

public class OAMPSConfirmationPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public OAMPSConfirmationPage yourConfirmation() {
		yourConfirmationDetails();
		return new OAMPSConfirmationPage(scriptHelper);
	}

	public OAMPSConfirmationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourConfirmationDetails() {

		String lastname = dataTable.getData("MetLife_Data", "LastName");
	//	String owner = dataTable.getData("MetLife_Data", "Owner");
		String title = dataTable.getData("MetLife_Data", "Title");
		String contact = dataTable.getData("MetLife_Data", "ContactNumber");
		String contactOther = dataTable.getData("MetLife_Data", "ContactOther");
		String email = dataTable.getData("MetLife_Data", "Email");
		String contacttime = dataTable.getData("MetLife_Data", "ContactTime");
	//	String unit = dataTable.getData("MetLife_Data", "Unit");
		String address1 = dataTable.getData("MetLife_Data", "Address1");
		String address2 = dataTable.getData("MetLife_Data", "Address2");
		String suburb = dataTable.getData("MetLife_Data", "Suburb");
		String state = dataTable.getData("MetLife_Data", "State");
		String postcode = dataTable.getData("MetLife_Data", "Postcode");


		String loading = dataTable.getData("MetLife_Data", "AckExclusion");
	
		try {
			
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.rdowner), "clkradio", "Owner");
			
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.linkTitle), "DropDownSelect", "Title", title);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtlastName), "SET", "Last Name", lastname);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtlastName), "TAB", "Last Name");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtContact), "SET", "Contact", contact);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtContact), "TAB", "Contact");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtcontactOther), "SET", "Contact", contactOther);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtcontactOther), "TAB", "Contact");
			
			if ((!loading.equalsIgnoreCase("loading") && !loading.equalsIgnoreCase("exclusion") && !loading.equalsIgnoreCase("loading&exclusion") )) {
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.drpcontactTime), "DropDownSelect", "Contact Time", contacttime);
			Thread.sleep(1000);
			}
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtemail), "SET", "Email", email);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtemail), "TAB", "Email");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtemailconfirm), "SET", "Conafirm Email", email);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtemailconfirm), "TAB", "Confirm Email");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtaddress1), "SET", "Address 1", address1);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtaddress1), "TAB", "Address 1");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtaddress2), "SET", "Address 2", address2	);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtaddress2), "TAB", "Address 2");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtSuburb), "SET", "Suburb", suburb);
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.txtSuburb), "TAB", "Suburb");
						
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.drpState), "DropDownSelect", "State", state);
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.txtPostcode), "SET", "Post Code", postcode);
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.txtPostcode), "TAB", "Post Code");
			
			tryAction(waitForClickableElement(OAMPSConfirmationPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(OAMPSConfirmationPageObjects.btnNext), "Click", "Next");
			Thread.sleep(4000);
			
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
