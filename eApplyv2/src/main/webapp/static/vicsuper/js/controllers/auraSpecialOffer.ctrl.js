/*aura special offer*/
/*aura transfer controller*/
VicsuperApp.controller('auraspecialoffer',['$scope','$rootScope', '$location', '$timeout','$window', 'auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService', 'submitAura','persoanlDetailService','auraResponseService','ngDialog','urlService','saveEapply','submitEapply','$routeParams','RetrieveAppDetailsService',
                                     function($scope,$rootScope ,$location, $timeout,$window, auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,auraResponseService,ngDialog,urlService,saveEapply,submitEapply, $routeParams, RetrieveAppDetailsService){
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
	
	$scope.go = function (path) {
    	
    	if($routeParams.mode == 3 && path=='/quotespecial/2')
    	{
    		path = "/quotespecial/3";
    	}
    	
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}
  	*/
  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/

  	$scope.proceedNext = function() {
  		$scope.keepGoing = true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true;
  				$scope.keepGoing = false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  			}
  		});
  		if($scope.keepGoing){
//  			$rootScope.$broadcast('disablepointer');
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
  				//console.log(response.data);
  				$scope.auraResponses = response.data;
  				PersistenceService.setChangeCoverAuraDetails(response.data);
  				if($scope.auraResponses.overallDecision!='DCL'){
					//console.log(response.data);
  					$scope.go('/specialoffersummary/1');
	  		}else if($scope.auraResponses.overallDecision =='DCL'){
	  			if($scope.coverDetails != null && $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null &&
	      		      $scope.specialOccDetails != null && $scope.auraResponses != null && $scope.personalDetails != null){
	  				$scope.coverDetails.lastSavedOn = '';
	      			$scope.details={};
	      			$scope.details.occupationDetails =$scope.specialOccDetails;
	      			$scope.details.addnlDeathCoverDetails=$scope.deathAddnlDetails;
	      			$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlDetails;
	      			$scope.details.addnlIpCoverDetails=$scope.ipAddnlDetails;
	      			$scope.details.transferDocuments = $scope.uploadedFileDetails;
	      			var coverObject = angular.extend($scope.details,$scope.coverDetails);
	              	var auraObject = angular.extend(coverObject,$scope.auraResponses);
	      			var submitObject = angular.extend(auraObject,$scope.personalDetails);
	      			auraResponseService.setResponse(submitObject);
	      			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
	              		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
	              		PersistenceService.setNpsUrl(response.data.npsTokenURL);
                  		$scope.go('/specialofferdecline');
	              	}, function(err){
		            		//console.log('Error while submitting Special cover ' + err);
		            	});
	              }
	  		}
  				
  	  		});
  		}
  	}

	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.coverDetails = PersistenceService.getChangeCoverDetails();
	$scope.specialOccDetails = PersistenceService.getChangeCoverOccDetails();
	$scope.deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	$scope.tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
	$scope.ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();

	var init = function(){
		auraInputService.setFund('VICT')
	  	auraInputService.setMode('SpecialOffer')
	  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
	  	auraInputService.setAppnumber($scope.coverDetails.appNum)
	  	auraInputService.setDeathAmt($scope.deathAddnlDetails.deathFixedAmt)
	  	auraInputService.setTpdAmt($scope.tpdAddnlDetails.tpdFixedAmt)
	  	auraInputService.setIpAmt($scope.ipAddnlDetails.ipFixedAmt)
	  	auraInputService.setWaitingPeriod($scope.ipAddnlDetails.waitingPeriod)
	  	auraInputService.setBenefitPeriod($scope.ipAddnlDetails.benefitPeriod)
	  	if($scope.specialOccDetails && $scope.specialOccDetails.gender ){
	  		auraInputService.setGender($scope.specialOccDetails.gender);
	  	}else if($scope.personalDetails && $scope.personalDetails.gender){
	  		auraInputService.setGender($scope.personalDetails.gender);
	  	}
	  	
	  	//auraInputService.setIndustryOcc($scope.specialOccDetails.industryCode+":"+$scope.specialOccDetails.occupation)
	  	auraInputService.setIndustryOcc("None")
	  	auraInputService.setClientname('metaus')
	  	/*if($scope.specialOccDetails.areyouperCitzNewMemberQuestion=='Yes'){
	  		auraInputService.setCountry('Australia')
	  	}else{
	  		auraInputService.setCountry($scope.specialOccDetails.areyouperCitzNewMemberQuestion)
	  	} */
	  	auraInputService.setCountry('Australia')
	  	auraInputService.setSalary($scope.specialOccDetails.salary)
	  	auraInputService.setFifteenHr($scope.specialOccDetails.fifteenHr)
	  	auraInputService.setName($scope.coverDetails.name)
		auraInputService.setLastName($scope.personalDetails.personalDetails.lastName)
	  	auraInputService.setFirstName($scope.personalDetails.personalDetails.firstName)
	  	auraInputService.setDob($scope.personalDetails.personalDetails.dateOfBirth)
	  	auraInputService.setExistingTerm(false);
//	  	if($scope.personalDetails.memberType=="Personal"){
//	  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
//	  	}else{
//	  		auraInputService.setMemberType("None")
//	  	}
	  	auraInputService.setMemberType("None")
	
		 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
	  		$scope.auraResponseDataList = response.data.questions;
	  		//console.log($scope.auraResponseDataList)
	  		angular.forEach($scope.auraResponseDataList, function(Object) {
				$scope.sectionname = Object.questionAlias.substring(3);
	
				});
	  	});
	}
	
	if($routeParams.mode == 3 && !$scope.coverDetails ){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    		var result = res.data[0];
    		var coverDet = {}, occDet ={}, deathAddDet = {},tpdAddDet={}, ipAddDet={}, personalDt = {}; ipAddnlDetails = {};
    		
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactType = result.contactType;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		occDet.gender = result.personalDetails.gender;
    		coverDet.transferDeathExistingAmt = result.transferDeathExistingAmt;
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.transferTpdExistingAmt = result.transferTpdExistingAmt;
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.transferIpExistingAmt = result.transferIpExistingAmt;
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium = result.totalPremium;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.previousFundName = result.previousFundName;
    		coverDet.membershipNumber = result.membershipNumber;
    		coverDet.spinNumber = result.spinNumber;
    		coverDet.documentName = result.documentName;
    		coverDet.appNum = result.appNum;
    		coverDet.manageType = "TCOVER";
    		coverDet.partnerCode ="VICT";
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.age = result.age;
    		
    		//$scope.transferCoverDetails = coverDet;

    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		occDet.hazardousQue = result.occupationDetails.hazardousQue;
    		occDet.salary = result.occupationDetails.salary;
    		occDet.industryCode = result.occupationDetails.industryCode;
    		
    		
    		deathAddDet.deathTransferAmt = result.addnlDeathCoverDetails.deathTransferAmt;
    		deathAddDet.deathTransferCovrAmt = result.addnlDeathCoverDetails.deathTransferCovrAmt?parseInt(result.addnlDeathCoverDetails.deathTransferCovrAmt):0;
    		deathAddDet.deathTransferWeeklyCost = result.addnlDeathCoverDetails.deathTransferWeeklyCost;
    		deathAddDet.deathFixedAmt = parseInt(result.addnlDeathCoverDetails.deathFixedAmt);
    		
    		tpdAddDet.tpdTransferAmt = result.addnlTpdCoverDetails.tpdTransferAmt;
    		tpdAddDet.tpdTransferCovrAmt = result.addnlTpdCoverDetails.tpdTransferCovrAmt?parseInt(result.addnlTpdCoverDetails.tpdTransferCovrAmt):0;
    		tpdAddDet.tpdTransferWeeklyCost = result.addnlTpdCoverDetails.tpdTransferWeeklyCost;
    		tpdAddDet.tpdFixedAmt = parseInt(result.addnlTpdCoverDetails.tpdFixedAmt);

    		ipAddDet.ipTransferAmt = result.addnlIpCoverDetails.ipTransferAmt;
    		ipAddDet.ipTransferCovrAmt = result.addnlIpCoverDetails.ipTransferCovrAmt?parseInt(result.addnlIpCoverDetails.ipTransferCovrAmt):0;
    		ipAddDet.addnlTransferWaitingPeriod = result.addnlIpCoverDetails.addnlTransferWaitingPeriod;
    		ipAddDet.addnlTransferBenefitPeriod = result.addnlIpCoverDetails.addnlTransferBenefitPeriod;
    		ipAddDet.ipTransferWeeklyCost = result.addnlIpCoverDetails.ipTransferWeeklyCost;
    		
    		
    		ipAddnlDetails.waitingPeriod = result.addnlIpCoverDetails.waitingPeriod || '';
    		ipAddnlDetails.benefitPeriod = result.addnlIpCoverDetails.benefitPeriod || '';
    		ipAddnlDetails.ipFixedAmt = parseInt(result.addnlIpCoverDetails.ipFixedAmt);
    		
    		$scope.svdRtrv = true;
    		
    		$scope.coverDetails=coverDet;
    		$scope.ipAddnlDetails = ipAddnlDetails;
    	  	$scope.specialOccDetails =occDet;
    	  	$scope.deathAddnlDetails =deathAddDet;
    		$scope.tpdAddnlDetails=tpdAddDet;
    		$scope.ipAddnlCvrDetails=ipAddDet;
    		init();
    		
    	}, function(err){
    		console.error("Something went wrong while retrieving the details " + JSON.stringify(err));
    	});
	}else{
		init();
	}
  	 var appNum;
     appNum = PersistenceService.getAppNumber();
  	 $scope.saveSpecialCoverAura = function (){
  		$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
    		if($scope.coverDetails != null && $scope.specialOccDetails != null && $scope.deathAddnlDetails != null &&  $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.personalDetails != null){
      		$scope.coverDetails.lastSavedOn = 'SpecialCoverAuraPage';
      		// $scope.auraDetails
      		$scope.details={};
			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
			$scope.details.occupationDetails = $scope.specialOccDetails;
      		var temp = angular.extend($scope.details,$scope.coverDetails);
          	var saveSpecialCoverAuraObject = angular.extend(temp, $scope.personalDetails);
          	auraResponseService.setResponse(saveSpecialCoverAuraObject)
          	$rootScope.$broadcast('disablepointer');
  	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
  	                //console.log(response.data)
  	        });
      	}
     };

     $scope.saveAndExitPopUp = function (hhText) {

 		var dialog1 = ngDialog.open({
 			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
 				className: 'ngdialog-theme-plain custom-width',
 				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
 				plain: true
 		});
 		dialog1.closePromise.then(function (data) {
 			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
 		});
 	};

 	 $scope.updateRadio = function (answerValue, questionObj){

    		questionObj.arrAns[0]=answerValue;
    		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
    		  auraResponseService.setResponse($scope.auraRes)
    		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {
    			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
    			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
    	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
    	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
    	  				//branch complete false for previous questions
    	  				$scope.auraResponseDataList[selectedIndex].error=true;
    	  			}else if(Object.questionComplete){
    	  				$scope.auraResponseDataList[selectedIndex].error=false;
    	  			}

    	  		});

        	}, function () {
        		//console.log('failed');
        	});
    	 };



}]);


/////
