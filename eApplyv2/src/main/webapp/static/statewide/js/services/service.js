'use strict';
StateWideApp.factory('$fakeStorage', [
    function(){
        function FakeStorage() {};
        FakeStorage.prototype.setItem = function (key, value) {
            this[key] = value;
        };
        FakeStorage.prototype.getItem = function (key) {
            return typeof this[key] == 'undefined' ? null : this[key];
        }
        FakeStorage.prototype.removeItem = function (key) {
            this[key] = undefined;
        };
        FakeStorage.prototype.clear = function(){
            for (var key in this) {
                if( this.hasOwnProperty(key) )
                {
                    this.removeItem(key);
                }
            }
        };
        FakeStorage.prototype.key = function(index){
            return Object.keys(this)[index];
        };
        return new FakeStorage();
    }
]);

StateWideApp.factory('$sessionstorage', [
    '$window', '$fakeStorage',
    function($window, $fakeStorage) {
        function isStorageSupported(storageName)
        {
            var testKey = 'test',
                storage = $window[storageName];
            try
            {
                storage.setItem(testKey, '1');
                storage.removeItem(testKey);
                return true;
            }
            catch (error)
            {
                return false;
            }
        }
        var storage = isStorageSupported('sessionStorage') ? $window.sessionStorage : $fakeStorage;
        return {
            set: function(key, value) {
                storage.setItem(key, value);
            },
            get: function(key, defaultValue) {
                return storage.getItem(key) || defaultValue;
            },
            setItem: function(key, value) {
                storage.setItem(key, value);
            },
            getItem: function(key) {
                return storage.getItem(key);
            },
            remove: function(key){
                storage.removeItem(key);
            },
            clear: function() {
                storage.clear();
            },
            key: function(index){
                storage.key(index);
            }
        }
    }
]);
StateWideApp.factory('urlService',['$http','$q', '$sessionstorage', 'fetchUrlSvc', function($http,$q, $sessionstorage, fetchUrlSvc){
                var headerConfig = {
                headers : {
                    'Content-Type': 'application/json'
                }
            }

    var urlObject = {};
    urlObject.getUrls = function(){
                var defer = $q.defer();
                $http.get('CareSuperUrls.properties',headerConfig).then(function(res){
                                urlObject = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error fetching urls " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    urlObject.setUrlList = function(list){
                $sessionstorage.setItem('urlList',JSON.stringify(list));
    };
    urlObject.getUrlList = function(){
                return fetchUrlSvc.getUrlList();
    };
    return urlObject;
}]);

StateWideApp.factory('propertyService',['$http','$q', '$sessionstorage', function($http,$q, $sessionstorage){
    var headerConfig = {
    headers : {
        'Content-Type': 'application/json'
    }
}

var occObject = {};
   occObject.getOccList = function(){
    var defer = $q.defer();
    $http.get('StatewideDecOccList.properties',headerConfig).then(function(res){
    				occObject = res.data;
                    defer.resolve(res);
    }, function(err){
                    console.log("Error fetching urls " + JSON.stringify(err));
                    defer.reject(err);
    });
    return defer.promise;
};
return occObject;
}]);


StateWideApp.factory('QuoteService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getList = function(url,fundCode){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting industries " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.service('tokenNumService', function(fetchTokenNumSvc) {
                var _self = this;
                _self.setTokenId = function (tempToken) {
                                _self.tokenId = tempToken;
  }
                _self.getTokenId = function () {
                  return fetchTokenNumSvc.getTokenId();
  }
});
StateWideApp.service('deathCoverService', function($sessionstorage, fetchDeathCoverSvc) {
                var _self = this;
                _self.setDeathCover = function (tempToken) {
                                //_self.deathCover = tempToken;
                                $sessionstorage.setItem('deathCoverObject',JSON.stringify(tempToken));
  }
                _self.getDeathCover = function () {
                                //return _self.deathCover;
                                return fetchDeathCoverSvc.getDeathCover();
  }
});

StateWideApp.service('tpdCoverService', function($sessionstorage, fetchTpdCoverSvc) {
                var _self = this;
                _self.setTpdCover = function (tempToken) {
                                //_self.tpdCover = tempToken;
                                $sessionstorage.setItem('tpdCoverObject',JSON.stringify(tempToken));
  }
                _self.getTpdCover = function () {
                                //return _self.tpdCover;
                                return fetchTpdCoverSvc.getTpdCover();
  }
});

StateWideApp.service('ipCoverService', function($sessionstorage, fetchIpCoverSvc) {
                var _self = this;
                _self.setIpCover = function (tempToken) {
                                //_self.ipCover = tempToken;
                                $sessionstorage.setItem('ipCoverObject',JSON.stringify(tempToken));
  }
                _self.getIpCover = function () {
                                //return _self.ipCover;
                                return fetchIpCoverSvc.getIpCover();
  }
});

StateWideApp.service('persoanlDetailService', function($sessionstorage, fetchPersoanlDetailSvc) {
                var _self = this;
                _self.setMemberDetails = function (tempToken) {
                                //_self.deathCover = tempToken;
                                $sessionstorage.setItem('memberDetailsObject',JSON.stringify(tempToken));
  }
                _self.getMemberDetails = function () {
                                //return _self.deathCover;
                                return fetchPersoanlDetailSvc.getMemberDetails();
  }
});

StateWideApp.service('newMemberOfferService', function() {
                var _self = this;
                _self.setNewMemberOfferFlag = function (tempToken) {
                                _self.newMemberOfferFlag = tempToken;
  }
                _self.getNewMemberOfferFlag = function () {
                  return _self.newMemberOfferFlag;
  }
});
/*StateWideApp.factory('getclientData', function($http, $q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
                var defer = $q.defer();

                $http.defaults.headers.common['Authorization'] = 'C3PO R2D2';
                                $http.get(url,{
                                                headers: {'Authorization':  tokenNumService.getTokenId()},
                                                params:{ tokenid: tokenNumService.getTokenId()}}).then(function(response) {
                                                                defer.resolve(response);
                                }, function(response) {
                                                defer.reject(response);
                                });
                               return defer.promise;
    }

    return myFactObj;
});*/

StateWideApp.factory('getclientData', function($http, $q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
                var defer = $q.defer();

                                $http.post(url,tokenNumService,{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                defer.resolve(response);
                                }, function(response) {
                                                defer.reject(response);
                                });
                               return defer.promise;
    }

    return myFactObj;
});

StateWideApp.factory('SpecialCvrRenderService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function(url,fundCode,age,deathUnits,tpdUnits,dateJoined,clientrefnum,firstname,lastname,dob){
                var defer = $q.defer();
                $http.get(url, {
            	   headers:{
            		   			'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'anb': parseInt(age),
                                'deathUnits':parseInt(deathUnits),
                                'tpdUnits':parseInt(tpdUnits),
                                'dateJoined':parseInt(dateJoined),
                                'clientrefnum':clientrefnum,
                                'firstName':firstname,
                                'lastName':lastname,
                                'dob':dob
                }}).then(function(res){
                				myFactObj = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting default units " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return myFactObj;
}]);

StateWideApp.factory('OccupationService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getOccupationList = function(url,fundId,induCode){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundId': fundId,
                                'induCode': induCode
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting occupations " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

/*StateWideApp.factory('AppNumberService', function($resource){
                return $resource('http://localhost\:8087/getUniqueNumber', {}, {
                                getAppnumber: {
                                                headers: {'Content-Type': 'application/json'},
                                                method: 'GET',
                                                params: {},
                                                isArray: false
                                }
                });
});*/

StateWideApp.factory('appNumberService', function($http, $q,tokenNumService){
    var myFactObj = {};
    myFactObj.requestObj = function (url) {
                var defer = $q.defer();
                                $http.get(url,{
                                                headers: {'Authorization':  tokenNumService.getTokenId()},
                                                params:{}}).then(function(response) {
                                                                defer.resolve(response);
                                }, function(response) {
                                                defer.reject(response);
                                });
                               return defer.promise;
    }

    return myFactObj;
});

StateWideApp.factory('CalculateService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };

    var factory = {};
    factory.calculate = function(data,url){
                var defer = $q.defer();
                $http.post(url, data, headerConfig).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while calculating.." + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    factory.calculateAAL = function(data,url){
        var defer = $q.defer();
        $http.post(url, data, headerConfig).then(function(res){
                        factory = res.data;
                        defer.resolve(res);
        }, function(err){
                        console.log("Error while calculating.." + JSON.stringify(err));
                        defer.reject(err);
        });
        return defer.promise;
};
    return factory;
}]);

StateWideApp.factory('CalculateDeathService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.calculateAmount = function(url,data){
                var defer = $q.defer();
                $http.post(url, data, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while calculating death premium " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('CalculateTPDService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.calculateAmount = function(url,data){
                var defer = $q.defer();
                $http.post(url, data, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while calculating TPD premium " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('CalculateIPService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.calculateAmount = function(url,data){
                var defer = $q.defer();
                $http.post(url, data, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while calculating TPD premium " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('TransferCalculateService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
            };

    var factory = {};
    factory.calculate = function(data,url){
                var defer = $q.defer();
                $http.post(url, data, headerConfig).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while calculating.." + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('NewOccupationService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getOccupation = function(url,fundId,occName){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundId': fundId,
                                'occName': occName
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting occupations " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('MaxLimitService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.getMaxLimits = function(url,fundCode,memberType,manageType){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'memberType': memberType,
                                'manageType':manageType
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting limits " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('ConvertService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var headerConfig = {
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization':tokenNumService.getTokenId()
                }
           };

    var factory = {};
    factory.convert = function(url,data){
                var defer = $q.defer();
                $http.post(url, data, headerConfig).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while calculating.." + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('getAuraTransferData', function($http, $q,auraTransferInitiateService,auraInputService,tokenNumService){
     var myFactObj = {};
     myFactObj.requestObj = function (url) {
                var defer = $q.defer();

                                $http.post(url,auraInputService,{headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                defer.resolve(response);
                               }, function(response) {
                                               defer.reject(response);
                               });
                               return defer.promise;
     }

     return myFactObj;
});

 StateWideApp.service('auraTransferInitiateService', function() {
               var _self = this;
               _self.setInput = function (tempInput) {
                               _self.input = tempInput;
   }
               _self.getInput = function () {
                 return _self.input;
   }
});

 StateWideApp.factory('submitEapply', function($http, $q,auraResponseService,tokenNumService){

     var myFactObj = {};

     myFactObj.reqObj = function (url) {
                 var defer = $q.defer();
                                $http.post(url,auraResponseService.getResponse(),
                                                               {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                defer.resolve(response);

                                }, function(response) {
                                               defer.reject(response);
                               });
                               return defer.promise;
     }

     return myFactObj;
});

StateWideApp.factory('printQuotePage', function($http, $q,auraResponseService,tokenNumService){

     var myFactObj = {};

     myFactObj.reqObj = function (url) {
                 var defer = $q.defer();
                                $http.post(url,auraResponseService.getResponse(),
                                                               {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                defer.resolve(response);

                                }, function(response) {
                                               defer.reject(response);
                               });
                               return defer.promise;
     }

     return myFactObj;
});


 StateWideApp.factory('auraPostfactory', function($http, $q,auraResponseService,tokenNumService){

     var myFactObj = {};

     myFactObj.reqObj = function (url) {
                 var defer = $q.defer();
                                $http.post(url,auraResponseService.getResponse(),
                                                               {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                defer.resolve(response);

                                }, function(response) {
                                               defer.reject(response);
                               });
                               return defer.promise;
     }

     return myFactObj;
});

StateWideApp.factory('submitAura', function($http, $q, auraInputService,tokenNumService){
                     var myFactObj = {};
                     myFactObj.requestObj = function (url) {
                                var defer = $q.defer();

                                                $http.post(url,auraInputService,
                                                                               {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                                defer.resolve(response);
                                               }, function(response) {
                                                               defer.reject(response);
                                               });
                                               return defer.promise;
                     }

                     return myFactObj;
                });

StateWideApp.factory('saveEapply', function($http, $q,auraResponseService,tokenNumService){

     var myFactObj = {};

     myFactObj.reqObj = function (url) {
                 var defer = $q.defer();
                                $http.post(url,auraResponseService.getResponse(),
                                                               {headers:{'Authorization':  tokenNumService.getTokenId()}}).then(function(response) {

                                                defer.resolve(response);

                                }, function(response) {
                                               defer.reject(response);
                               });
                               return defer.promise;
     }

     return myFactObj;
});

StateWideApp.factory('clientMatch', function($http, $q,auraInputService,tokenNumService){

    var myFactObj = {};

    myFactObj.reqObj = function (url) {

                 var defer = $q.defer();

                                $http.post(url,
                                                                {firstName:auraInputService.getFirstName(), surName:auraInputService.getLastName(), dob:auraInputService.getDob()}
                                                                ).then(function(response) {

                                                defer.resolve(response);

                                }, function(response) {
                                                defer.reject(response);
                                });
                               return defer.promise;
    }

    return myFactObj;
});

 StateWideApp.service('auraResponseService', function() {
               var _self = this;
               _self.setResponse = function (tempInput) {
                               _self.response = tempInput;
   }
               _self.getResponse = function () {
                 return _self.response;
   }
});


StateWideApp.service('auraInputService', function() {

                var _self = this;
               _self.setFund = function (tempInput) {
                               _self.fund = tempInput;
   }
               _self.getFund = function () {
                 return _self.fund;
   }
               _self.setMode = function (tempInput) {
                               _self.mode = tempInput;
   }
               _self.getMode = function () {
                 return _self.mode;
   }
               _self.setName = function (tempInput) {
                               _self.name = tempInput;
   }
               _self.getName = function () {
                 return _self.name;
   }
               _self.setAge = function (tempInput) {
                               _self.age = tempInput;
   }
               _self.getAge = function () {
                 return _self.age;
   }
                _self.setDeathAmt = function (tempInput) {
                               _self.deathAmt = tempInput;
   }
               _self.getDeathAmt = function () {
                 return _self.deathAmt;
   }

                _self.setTpdAmt = function (tempInput) {
                               _self.tpdAmt = tempInput;
   }
               _self.getTpdAmt = function () {
                 return _self.tpdAmt;
   }

                _self.setIpAmt = function (tempInput) {
                               _self.ipAmt = tempInput;
   }
               _self.getIpAmt = function () {
                 return _self.ipAmt;
   }

                _self.setWaitingPeriod = function (tempInput) {
                               _self.waitingPeriod = tempInput;
   }
               _self.getWaitingPeriod = function () {
                 return _self.waitingPeriod;
   }

                _self.setBenefitPeriod = function (tempInput) {
                               _self.benefitPeriod = tempInput;
   }
               _self.getBenefitPeriod = function () {
                 return _self.benefitPeriod;
   }
               _self.setAppnumber = function (tempInput) {
                               _self.appnumber = tempInput;
   }
               _self.getAppnumber = function () {
                 return _self.appnumber;
   }

                _self.setIndustryOcc = function (tempInput) {
                               _self.industryOcc = tempInput;
   }
               _self.getIndustryOcc = function () {
                 return _self.industryOcc;
   }

                _self.setGender = function (tempInput) {
                               _self.gender = tempInput;
   }
               _self.getGender = function () {
                 return _self.gender;
   }

                _self.setCountry = function (tempInput) {
                               _self.country = tempInput;
   }
               _self.getCountry = function () {
                 return _self.country;
   }

                _self.setSalary = function (tempInput) {
                               _self.salary = tempInput;
   }
               _self.getSalary = function () {
                 return _self.salary;
   }

                _self.setFifteenHr = function (tempInput) {
                               _self.fifteenHr = tempInput;
   }
               _self.getFifteenHr = function () {
                 return _self.fifteenHr;
   }

                _self.setCompanyId = function (tempInput) {
                               _self.companyId = tempInput;
   }
               _self.getCompanyId = function () {
                 return _self.companyId;
   }

                _self.setClientname = function (tempInput) {
                               _self.clientname = tempInput;
   }
               _self.getClientname = function () {
                 return _self.clientname;
   }

                _self.setDob = function (tempInput) {
                               _self.dob = tempInput;
   }
               _self.getDob = function () {
                 return _self.dob;
   }

                _self.setFirstName = function (tempInput) {
                               _self.firstName = tempInput;
   }
               _self.getFirstName = function () {
                 return _self.firstName;
   }
               _self.setLastName = function (tempInput) {
                               _self.lastName = tempInput;
   }
               _self.getLastName = function () {
                 return _self.lastName;
   }
               _self.setSpecialTerm = function (tempInput) {
                               _self.specialTerm = tempInput;
   }
               _self.getSpecialTerm = function () {
                 return _self.specialTerm;
   }
               _self.setExistingTerm = function (tempInput) {
                               _self.existingTerm = tempInput;
   }
               _self.getExistingTerm = function () {
                 return _self.existingTerm;
   }

                _self.setMemberType = function (tempInput) {
                               _self.memberType = tempInput;
   }
               _self.getMemberType = function () {
                 return _self.memberType;
   }

 });

StateWideApp.service('PersistenceService', function($sessionstorage, fetchAppNumberSvc){
                var thisObject = this;
                thisObject.uploadedFileList = [];
                thisObject.setChangeCoverDetails = function(coverDetails){
                                $sessionstorage.setItem('coverDetails',JSON.stringify(coverDetails));
                };

                thisObject.getChangeCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('coverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('coverDetails'));
                };

                thisObject.setChangeCoverStateDetails = function(coverDetails){
                                $sessionstorage.setItem('coverStateDetails',JSON.stringify(coverDetails));
                };

                thisObject.getChangeCoverStateDetails = function(){
                                return JSON.parse($sessionstorage.getItem('coverStateDetails')) === null ? null : JSON.parse($sessionstorage.getItem('coverStateDetails'));
                };

                thisObject.setNewMemberCoverDetails = function(coverDetails){
                                $sessionstorage.setItem('newMemberCoverDetails',JSON.stringify(coverDetails));
                };

                thisObject.getNewMemberCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemberCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('newMemberCoverDetails'));
                };

                thisObject.setNewMemberCoverStateDetails = function(coverDetails){
                                $sessionstorage.setItem('newMemberCoverStateDetails',JSON.stringify(coverDetails));
                };

                thisObject.getNewMemberCoverStateDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemberCoverStateDetails')) === null ? null : JSON.parse($sessionstorage.getItem('newMemberCoverStateDetails'));
                };

                thisObject.setworkRatingCoverDetails = function(coverDetails){
                                $sessionstorage.setItem('workRatingCoverDetails',JSON.stringify(coverDetails));
                };

                thisObject.getworkRatingCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('workRatingCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('workRatingCoverDetails'));
                };

                thisObject.setworkRatingCoverStateDetails = function(coverDetails){
                                $sessionstorage.setItem('workRatingCoverStateDetails',JSON.stringify(coverDetails));
                };

                thisObject.getworkRatingCoverStateDetails = function(){
                                return JSON.parse($sessionstorage.getItem('workRatingCoverStateDetails')) === null ? null : JSON.parse($sessionstorage.getItem('workRatingCoverStateDetails'));
                };

                thisObject.settransferCoverDetails = function(coverDetails){
                                $sessionstorage.setItem('transferCoverDetails',JSON.stringify(coverDetails));
                };

                thisObject.gettransferCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transferCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transferCoverDetails'));
                };

                thisObject.settransferCoverStateDetails = function(coverDetails){
                                $sessionstorage.setItem('transferCoverStateDetails',JSON.stringify(coverDetails));
                };

                thisObject.gettransferCoverStateDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transferCoverStateDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transferCoverStateDetails'));
                };

                // life event cover
                thisObject.setlifeEventCoverDetails = function(coverDetails){
                                $sessionstorage.setItem('lifeEventCoverDetails',JSON.stringify(coverDetails));
                };

                thisObject.getlifeEventCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('lifeEventCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('lifeEventCoverDetails'));
                };

                thisObject.setlifeEventCoverStateDetails = function(coverDetails){
                                $sessionstorage.setItem('lifeEventCoverStateDetails',JSON.stringify(coverDetails));
                };

                thisObject.getlifeEventCoverStateDetails = function(){
                                /*return JSON.parse($sessionstorage.getItem('lifeEventCoverStateDetails'));*/
                                return JSON.parse($sessionstorage.getItem('lifeEventCoverStateDetails')) === null ? null : JSON.parse($sessionstorage.getItem('lifeEventCoverStateDetails'));
                };

                thisObject.setLifeEventCoverOccDetails = function(lifeEventCoverOccDetails){
                                $sessionstorage.setItem('lifeEventCoverOccDetails',JSON.stringify(lifeEventCoverOccDetails));
                };

                thisObject.getLifeEventCoverOccDetails = function(){
                                return JSON.parse($sessionstorage.getItem('lifeEventCoverOccDetails')) === null ? null : JSON.parse($sessionstorage.getItem('lifeEventCoverOccDetails'));
                };

                thisObject.setLifeEventCoverAuraDetails = function(details){
                                $sessionstorage.setItem('lifeeventCoverAuraDetails',JSON.stringify(details));
                };

                thisObject.getLifeEventCoverAuraDetails = function(){
                                return JSON.parse($sessionstorage.getItem('lifeeventCoverAuraDetails')) === null ? null : JSON.parse($sessionstorage.getItem('lifeeventCoverAuraDetails'));
                };
                // life event cover

                thisObject.setChangeCoverOccDetails = function(changeCoverOccDetails){
                                $sessionstorage.setItem('changeCoverOccDetails',JSON.stringify(changeCoverOccDetails));
                };

                thisObject.getChangeCoverOccDetails = function(){
                                return JSON.parse($sessionstorage.getItem('changeCoverOccDetails')) === null ? null : JSON.parse($sessionstorage.getItem('changeCoverOccDetails'));
                };

                // death Addnlcover details
                thisObject.setDeathAddnlCoverDetails = function(deathAddnlCoverDetails){
                                $sessionstorage.setItem('deathAddnlCoverDetails',JSON.stringify(deathAddnlCoverDetails));
                };

                thisObject.getDeathAddnlCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('deathAddnlCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('deathAddnlCoverDetails'));
                };

                // TPD addnl cover details
                thisObject.setTpdAddnlCoverDetails = function(tpdAddnlCoverDetails){
                                $sessionstorage.setItem('tpdAddnlCoverDetails',JSON.stringify(tpdAddnlCoverDetails));
                };

                thisObject.getTpdAddnlCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('tpdAddnlCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('tpdAddnlCoverDetails'));
    };

                // IP addnl cover details
    thisObject.setIpAddnlCoverDetails = function(ipAddnlCoverDetails){
                                $sessionstorage.setItem('ipAddnlCoverDetails',JSON.stringify(ipAddnlCoverDetails));
                };

                thisObject.getIpAddnlCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('ipAddnlCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('ipAddnlCoverDetails'));
               };

                // tranfer occ details
                thisObject.setTransferCoverOccDetails = function(transferCoverOccDetails){
                                $sessionstorage.setItem('transferCoverOccDetails',JSON.stringify(transferCoverOccDetails));
                };

                thisObject.getTransferCoverOccDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transferCoverOccDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transferCoverOccDetails'));
                };

                // Transfer death addnl cover details
                thisObject.setTransferDeathAddnlDetails = function(transferDeathAddnlDetails){
                                $sessionstorage.setItem('transferDeathAddnlDetails',JSON.stringify(transferDeathAddnlDetails));
                };

                thisObject.getTransferDeathAddnlDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transferDeathAddnlDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transferDeathAddnlDetails'));
                };

                // Transfer TPD addnl Cover details
                thisObject.setTransferTpdAddnlDetails = function(transferTpdAddnlDetails){
                                $sessionstorage.setItem('transferTpdAddnlDetails',JSON.stringify(transferTpdAddnlDetails));
                };

                thisObject.getTransferTpdAddnlDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transferTpdAddnlDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transferTpdAddnlDetails'));
                };

                // Transfer IP addnl cover details
                thisObject.setTransferIpAddnlDetails = function(transferIpAddnlDetails){
                                $sessionstorage.setItem('transferIpAddnlDetails',JSON.stringify(transferIpAddnlDetails));
                };

                thisObject.getTransferIpAddnlDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transferIpAddnlDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transferIpAddnlDetails'));
                };
                thisObject.setWorkRatingCoverOccDetails = function(workRatingCoverOccDetails){
                                $sessionstorage.setItem('workRatingCoverOccDetails',JSON.stringify(workRatingCoverOccDetails));
                };

                thisObject.getWorkRatingCoverOccDetails = function(){
                                return JSON.parse($sessionstorage.getItem('workRatingCoverOccDetails')) === null ? null : JSON.parse($sessionstorage.getItem('workRatingCoverOccDetails'));
                };

                thisObject.setNewMemberCoverOccDetails = function(newMemberCoverOccDetails){
                                $sessionstorage.setItem('newMemberCoverOccDetails',JSON.stringify(newMemberCoverOccDetails));
                };

                thisObject.getNewMemberCoverOccDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemberCoverOccDetails')) === null ? null : JSON.parse($sessionstorage.getItem('newMemberCoverOccDetails'));
                };

                // new member Death addnl cover details
                thisObject.setNewMemberDeathAddnlDetails = function(newMemberDeathAddnlDetails){
                                $sessionstorage.setItem('newMemberDeathAddnlDetails',JSON.stringify(newMemberDeathAddnlDetails));
                };

                thisObject.getNewMemberDeathAddnlDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemberDeathAddnlDetails')) === null ? null : JSON.parse($sessionstorage.getItem('newMemberDeathAddnlDetails'));
                };

                // new member TPD addnl cover details
                thisObject.setNewMemberTpdAddnlDetails = function(newMemberTpdAddnlDetails){
                                $sessionstorage.setItem('newMemberTpdAddnlDetails',JSON.stringify(newMemberTpdAddnlDetails));
                };

                thisObject.getNewMemberTpdAddnlDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemberTpdAddnlDetails')) === null ? null : JSON.parse($sessionstorage.getItem('newMemberTpdAddnlDetails'));
                };

                // new member IP addnl cover details
                thisObject.setNewMemberIpAddnlDetails = function(newMemberIpAddnlDetails){
                                $sessionstorage.setItem('newMemberIpAddnlDetails',JSON.stringify(newMemberIpAddnlDetails));
                };

                thisObject.getNewMemberIpAddnlDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemberIpAddnlDetails')) === null ? null : JSON.parse($sessionstorage.getItem('newMemberIpAddnlDetails'));
                };
                thisObject.setAppNumber = function(appnum){
                                $sessionstorage.setItem('UniqueAppNum',JSON.stringify(appnum));
                };

                thisObject.getAppNumber = function(){
                                return fetchAppNumberSvc.getAppNumber();
                };

                 thisObject.setChangeCoverAuraDetails = function(details){
                                 $sessionstorage.setItem('changeCoverAuraDetails',JSON.stringify(details));
                 };
                
                 thisObject.getChangeCoverAuraDetails = function(){
                                 /*return JSON.parse($sessionstorage.getItem('changeCoverAuraDetails'));*/
                                 return JSON.parse($sessionstorage.getItem('changeCoverAuraDetails')) === null ? null : JSON.parse($sessionstorage.getItem('changeCoverAuraDetails'));
                 };

                thisObject.setTransCoverAuraDetails = function(details){
                                $sessionstorage.setItem('transCoverAuraDetails',JSON.stringify(details));
                };

                thisObject.getTransCoverAuraDetails = function(){
                                return JSON.parse($sessionstorage.getItem('transCoverAuraDetails')) === null ? null : JSON.parse($sessionstorage.getItem('transCoverAuraDetails'));
                };

                thisObject.setNewMemberAuraDetails = function(details){
                                $sessionstorage.setItem('newMemebrAuraDetails',JSON.stringify(details));
                };

                thisObject.getNewMemberAuraDetails = function(){
                                return JSON.parse($sessionstorage.getItem('newMemebrAuraDetails')) === null ? null : $sessionstorage.getItem('newMemebrAuraDetails');
                };

                thisObject.setWorkRatingAuraDetails = function(details){
                                $sessionstorage.setItem('workRatingAuraDetails',JSON.stringify(details));
                };

                thisObject.getWorkRatingAuraDetails = function(){
                                return JSON.parse($sessionstorage.getItem('workRatingAuraDetails')) === null ? null : JSON.parse($sessionstorage.getItem('workRatingAuraDetails'));
                };
                
                // cancel cover details
                thisObject.setCancelCoverDetails = function(details){
                                $sessionstorage.setItem('cancelCoverDetails',JSON.stringify(details));
                };

                thisObject.getCancelCoverDetails = function(){
                                return JSON.parse($sessionstorage.getItem('cancelCoverDetails')) === null ? null : JSON.parse($sessionstorage.getItem('cancelCoverDetails'));
                };

                thisObject.setPDFLocation = function(location){
                                $sessionstorage.setItem('PDFLocation',JSON.stringify(location));
                };

                thisObject.getPDFLocation = function(){
        try {
            if($sessionstorage.getItem('PDFLocation') == 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse($sessionstorage.getItem('PDFLocation'));
            }
        } catch(Error) {
            throw Error;
        }
    };

/*   thisObject.setUploadedFileDetails = function(details){
                                sessionstorage.setItem('UploadedFileDetails',JSON.stringify(details));
                };

                thisObject.getUploadedFileDetails = function(){
                                return JSON.parse($sessionstorage.getItem('UploadedFileDetails'));
                                return Object.keys(sessionstorage.getItem('UploadedFileDetails')).length ? sessionstorage.getItem('UploadedFileDetails') : null;
                };*/


                thisObject.setUploadedFileDetails = function(fileList){
                    thisObject.uploadedFileList = fileList;
                                //sessionStorage.setItem('UploadedFileDetails',JSON.stringify(details));
                };

                thisObject.getUploadedFileDetails = function(){
                    return thisObject.uploadedFileList;
                                //return JSON.parse(sessionStorage.getItem('UploadedFileDetails')) === null ? null : JSON.parse(sessionStorage.getItem('UploadedFileDetails'));
                };


                thisObject.setAppNumToBeRetrieved = function(num){
                                $sessionstorage.setItem('AppNumToBeRetrieved',JSON.stringify(num));
                };

                thisObject.getAppNumToBeRetrieved = function(){
                                return JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved')) === null ? null : JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved'));
                };

                thisObject.setNpsUrl = function(url){
                                $sessionstorage.setItem('NpsLink',JSON.stringify(url));
                };

                thisObject.getNpsUrl = function(){
        try {
            if($sessionstorage.getItem('NpsLink') == 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse($sessionstorage.getItem('NpsLink'));
            }
        } catch(Error) {
            throw Error;
        }
    };

});

StateWideApp.factory('RetrieveAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.retrieveApp = function(url,fundCode,clientRefNo){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'clientRefNo': clientRefNo
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while retrieving saved apps " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('RetrieveAppDetailsService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.retrieveAppDetails = function(url,applicationNumber){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'applicationNumber': applicationNumber
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while retrieving application " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('CheckSavedAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.checkSavedApps = function(url,fundCode,clientRefNo,manageType){
                var defer = $q.defer();
                $http.get(url, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'fundCode': fundCode,
                                'clientRefNo': clientRefNo,
                                'manageType':manageType
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting saved apps " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('CancelSavedAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.cancelSavedApps = function(url,applicationNumber){
                var defer = $q.defer();
                $http.post(url, {}, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'applicationNumber': applicationNumber
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while cancelling app " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('DownloadPDFService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.download = function(url,fileName){
                var defer = $q.defer();
                $http.post(url,{}, {headers:{
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
                                'file_name': fileName
                },responseType: 'arraybuffer',
                transformResponse: function (data) {
                        var pdf;
                        if (data) {
                            pdf = new Blob([data], {
                                type: 'application/pdf'
                            });
                        }
                        return {
                            response: pdf
                        };
                    }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error while getting pdf data " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

/* Change for Accepted Application message */
StateWideApp.factory('CheckAccAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.checkAccApps = function(url,fundCode,clientRefNo,manageTypeCC,dob,firstName,lastName){
                var defer = $q.defer();
                $http.post(url, {}, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
		                	'fundCode': fundCode,
		                    'clientRefNo': clientRefNo,
		                    'manageTypeCC':manageTypeCC,
		                    'dob': dob,
		                    'firstName':firstName,
		                    'lastName': lastName
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error in UW duplicate application " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

/*StateWideApp.factory('FetchUploadFilesService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.fetchFile = function(url,path){
                var defer = $q.defer();
                $http.post(url, {}, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
		                    'path': path
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error in Fetching uploaded file " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);

StateWideApp.factory('DeleteFilesService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.deleteFile = function(url,path){
                var defer = $q.defer();
                $http.post(url, {}, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
		                    'path': path
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error in deleting uploaded file " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
*/
/* Change for Duplicate Application UW message */
/*HostApp.factory('CheckUWDuplicateAppService',['$http','$q','tokenNumService', function($http,$q,tokenNumService){
    var factory = {};
    factory.checkDuplicateApps = function(url,fundCode,clientRefNo,manageTypeCC,dob,firstName,lastName){
                var defer = $q.defer();
                $http.post(url, {}, {headers:{
                                'Content-Type': 'application/json',
                                'Authorization':tokenNumService.getTokenId()
                }, params:{
		                	'fundCode': fundCode,
		                    'clientRefNo': clientRefNo,
		                    'manageTypeCC':manageTypeCC,
		                    'dob': dob,
		                    'firstName':firstName,
		                    'lastName': lastName
                }}).then(function(res){
                                factory = res.data;
                                defer.resolve(res);
                }, function(err){
                                console.log("Error in UW duplicate application " + JSON.stringify(err));
                                defer.reject(err);
                });
                return defer.promise;
    };
    return factory;
}]);
*/