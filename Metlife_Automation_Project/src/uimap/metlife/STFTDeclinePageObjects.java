package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTDeclinePageObjects extends PageObjectLibrary{
		
	public static final By btnDecline = getObjectIdentifaction("stft.decline.close.btn.css");
}
