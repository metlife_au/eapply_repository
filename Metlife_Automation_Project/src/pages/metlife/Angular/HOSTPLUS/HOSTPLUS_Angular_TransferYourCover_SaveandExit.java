package pages.metlife.Angular.HOSTPLUS;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_TransferYourCover_SaveandExit extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_TransferYourCover_SaveandExit TransferCover_HOSTPLUS_SaveAndExit() {
		TransferCoverHOSTPLUSSave();
		return new HOSTPLUS_Angular_TransferYourCover_SaveandExit(scriptHelper);
	}

	public HOSTPLUS_Angular_TransferYourCover_SaveandExit TransferCover_HOSTPLUS_Validation() {
		TransferCoverHOSTPLUS_Validation();

		return new HOSTPLUS_Angular_TransferYourCover_SaveandExit(scriptHelper);
	}

	public HOSTPLUS_Angular_TransferYourCover_SaveandExit(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

public void TransferCoverHOSTPLUSSave() {

		String EmailId = dataTable.getData("HOSTPLUS_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_TransferCover", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_TransferCover", "FifteenHoursWork");
		String Citizen = dataTable.getData("HOSTPLUS_TransferCover", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_TransferCover", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_TransferCover", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_TransferCover", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_TransferCover", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("HOSTPLUS_TransferCover", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_TransferCover", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_TransferCover", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("HOSTPLUS_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("HOSTPLUS_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("HOSTPLUS_TransferCover", "FormerFundNo");
		String DocumentaryEvidence = dataTable.getData("HOSTPLUS_TransferCover", "DocumentaryEvidence");
		String AttachmentSizeLimitTest	= dataTable.getData("HOSTPLUS_TransferCover", "AttachmentSizeLimitTest");
		String AttachmentTestPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachmentTestPath");
		String OverSizeMessage = dataTable.getData("HOSTPLUS_TransferCover", "OverSizeMessage");
		String AttachPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachPath");
		String CostType = dataTable.getData("HOSTPLUS_TransferCover", "CostType");
		String DeathYN = dataTable.getData("HOSTPLUS_TransferCover", "Death");
		String TPDYN = dataTable.getData("HOSTPLUS_TransferCover", "TPD");
		String IPYN = dataTable.getData("HOSTPLUS_TransferCover", "IP");
		String DeathAmount = dataTable.getData("HOSTPLUS_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("HOSTPLUS_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("HOSTPLUS_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("HOSTPLUS_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("HOSTPLUS_TransferCover", "BenefitPeriod");

		try{



				//****Click on Transfer Your Cover Link*****\\
				waitForPageTitle("testing", 6);
				WebDriverWait wait=new WebDriverWait(driver,18);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Transfer your cover']"))).click();
				report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);
				sleep(4000);


					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
				report.updateTestLog("Upload Document Evidence", "Upload Document Evidence continue button clicked", Status.PASS);


				if(driver.getPageSource().contains("You have previously saved application(s).")){
					sleep(1000);
					quickSwitchWindows();
					sleep(2000);
					driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
					report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
					sleep(2000);

				}




				//*****Agree to Duty of disclosure*******\\
				waitForPageTitle("testing", 7);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
				sleep(400);
				report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);




	            //*****Agree to Privacy Statement*******\\


				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);

				sleep(200);

	            //*****Enter the Email Id*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
				report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
				sleep(500);

				//*****Click on the Contact Number Type****\\

					Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
					Contact.selectByVisibleText(TypeofContact);
					report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
					sleep(250);


				//****Enter the Contact Number****\\

					sleep(250);
					wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
					sleep(250);
					report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);



					//***Select the Preferred time of Contact*****\\

					if(TimeofContact.equalsIgnoreCase("Morning")){

						driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

						sleep(250);
						report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
					}
					else{
						driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
						sleep(250);
						report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
					}

					//***********Select the Gender******************\\

					if(Gender.equalsIgnoreCase("Male")){

						driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
						sleep(250);
						report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
					}
					else{
						driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
						sleep(250);
						report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
					}

					//****Contact Details-Continue Button*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
					report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
					sleep(1200);

					//***************************************\\
					//**********OCCUPATION SECTION***********\\
					//****************************************\\

						//*****Select the 15 Hours Question*******\\


					if(FifteenHoursWork.equalsIgnoreCase("Yes")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"))).click();
							sleep(500);
							report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

						}
					else{

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"))).click();
							sleep(500);
							report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

						}

					//*****Resident of Australia****\\

					/*if(Citizen.equalsIgnoreCase("Yes")){
						driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

						}
					else{
						driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

						}*/

					//*****Industry********\\
				    sleep(500);
				    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
				    Industry.selectByVisibleText(IndustryType);
				    sleep(800);
					report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
				//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
					sleep(500);

					//*****Occupation*****\\

					Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
					Occupation.selectByVisibleText(OccupationType);
				    sleep(800);
					report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
					sleep(500);


					//*****Other Occupation *****\\

					if(OccupationType.equalsIgnoreCase("Other")){

						wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);
					}


					//*****Extra Questions*****\\

				    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}
				}



				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



				//******Do you work in hazardous environment*****\\

				if(HazardousEnv.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}

				//******Do you spend more than 10% of your working time outside of an office environment?*******\\

				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}


		          sleep(800);

				    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				        if(OfficeEnvironment.equalsIgnoreCase("Yes")){

					    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

					}

					      //*****Do You Hold a Tertiary Qualification******\\

					        if(TertiaryQual.equalsIgnoreCase("Yes")){

					        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}
					   else{

						   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}
					}

			}
				//*****What is your annual Salary******


				    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
				    sleep(500);
				    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);



				//*****Click on Continue*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);


				//******************************************\\
				//**********PREVIOUS COVER SECTION**********\\
				//*******************************************\\

				//*****What is the name of your previous fund or insurer?*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
				sleep(250);
				report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);

	            //*****Please enter your fund member or insurance policy number*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
				sleep(250);
				report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);

	            //*****Please enter your former fund SPIN number (if known)*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
				sleep(250);
				report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);



				//*****Choose the cost of Insurance Type******\\

				Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
				CostofInsurance.selectByVisibleText(CostType);
				sleep(400);
				report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);


				//****Click on Continue Button******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
				sleep(500);
				report.updateTestLog("Documentary Evidence", "No is selected", Status.PASS);


				//*************************************************\\
				//**************TRANSFER COVER SECTION***************\\
				//**************************************************\\

				//*****Death transfer amount******

				if(DeathYN.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
					sleep(500);
					report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);

				}

				//*****TPD transfer amount******

				if(TPDYN.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
					sleep(500);
					report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);

				}

				//****IP transfer amount*****\\



				if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){


				//*****Select The Waiting Period********\\

					Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
					waitingperiod.selectByVisibleText(WaitingPeriod);
					sleep(500);
					report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);



				//*****Select The Benefit  Period********\\

					Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
					benefitperiod.selectByVisibleText(BenefitPeriod);
					sleep(500);
					report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);


				//****Enter the IP amount*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
					sleep(500);
					report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);

				}

				//****Equivalent units of Hostplus checkbox*****\\
			/*
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='equivalentUnit']/span"))).click();
				report.updateTestLog("Equivalent units of HostPlus scale", "Checkbox is selected", Status.PASS);*/

				//*****Click on calculate Quote*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
				sleep(3000);
				report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);




				//********************************************************************************************\\
				//************Capturing the premiums displayed************************************************\\
				//********************************************************************************************\\

				//*******Capturing the Death Cost********\\

				WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
				Death.click();
				sleep(300);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,-150)", "");
				sleep(500);
				String DeathCost=Death.getText();
				report.updateTestLog("Death Cover amount", CostType+" Cost is "+DeathCost, Status.SCREENSHOT);
				sleep(500);

				//*******Capturing the TPD Cost********\\

				WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
				tpd.click();
				sleep(500);
				String TPDCost=tpd.getText();
				report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);

				//*******Capturing the IP Cost********\\

				WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
				ip.click();
				sleep(500);
				String IPCost=ip.getText();

				report.updateTestLog("IP Cover amount", CostType+" Cost is "+IPCost, Status.SCREENSHOT);

				//*****Scroll to the Bottom of the Page

			    jse.executeScript("window.scrollBy(0,250)", "");

			 /* //******Click on Continue******\\

			 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue '])[2]"))).click();
			 sleep(4500);
			 report.updateTestLog("Continue", "Continue button is selected", Status.PASS); */

		  //****Save & Exit******\\
				sleep(800);
				driver.findElement(By.xpath("(//button[@ng-click='saveQuoteTransfer()'])[1]")).click();
				report.updateTestLog("Save & Exit button", "Save & Exit button is clicked", Status.PASS);

			    //****Fetch the Application No******\\

				sleep(800);
				String AppNo=driver.findElement(By.xpath("//div[@id='tips_text']/strong[2]")).getText();
				report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);

				//******Finish and Close Window******\\

				sleep(500);
			//	quickSwitchWindows();
				driver.findElement(By.xpath("//button[text()='Finish & Close Window ']")).click();
				report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
				sleep(1500);

	        sleep(4000);


			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}

}

public void TransferCoverHOSTPLUS_Validation() {

	String EmailId = dataTable.getData("HOSTPLUS_TransferCover", "EmailId");
	String TypeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TypeofContact");
	String ContactNumber = dataTable.getData("HOSTPLUS_TransferCover", "ContactNumber");
	String TimeofContact = dataTable.getData("HOSTPLUS_TransferCover", "TimeofContact");
	String Gender = dataTable.getData("HOSTPLUS_TransferCover", "Gender");
	String FifteenHoursWork = dataTable.getData("HOSTPLUS_TransferCover", "FifteenHoursWork");
	String Citizen = dataTable.getData("HOSTPLUS_TransferCover", "Citizen");
	String IndustryType = dataTable.getData("HOSTPLUS_TransferCover", "IndustryType");
	String OccupationType = dataTable.getData("HOSTPLUS_TransferCover", "OccupationType");
	String OtherOccupation = dataTable.getData("HOSTPLUS_TransferCover", "OtherOccupation");
	String OfficeEnvironment = dataTable.getData("HOSTPLUS_TransferCover", "OfficeEnvironment");
    String TertiaryQual = dataTable.getData("HOSTPLUS_TransferCover", "TertiaryQual");
	String HazardousEnv = dataTable.getData("HOSTPLUS_TransferCover", "HazardousEnv");
	String OutsideOfcPercent = dataTable.getData("HOSTPLUS_TransferCover", "OutsideOfcPercent");
	String AnnualSalary = dataTable.getData("HOSTPLUS_TransferCover", "AnnualSalary");
	String PreviousInsurer = dataTable.getData("HOSTPLUS_TransferCover", "PreviousInsurer");
	String PolicyNo = dataTable.getData("HOSTPLUS_TransferCover", "PolicyNo");
	String FormerFundNo = dataTable.getData("HOSTPLUS_TransferCover", "FormerFundNo");
	String DocumentaryEvidence = dataTable.getData("HOSTPLUS_TransferCover", "DocumentaryEvidence");
	String AttachmentSizeLimitTest	= dataTable.getData("HOSTPLUS_TransferCover", "AttachmentSizeLimitTest");
	String AttachmentTestPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachmentTestPath");
	String OverSizeMessage = dataTable.getData("HOSTPLUS_TransferCover", "OverSizeMessage");
	String AttachPath = dataTable.getData("HOSTPLUS_TransferCover", "AttachPath");
	String CostType = dataTable.getData("HOSTPLUS_TransferCover", "CostType");
	String DeathYN = dataTable.getData("HOSTPLUS_TransferCover", "Death");
	String TPDYN = dataTable.getData("HOSTPLUS_TransferCover", "TPD");
	String IPYN = dataTable.getData("HOSTPLUS_TransferCover", "IP");
	String DeathAmount = dataTable.getData("HOSTPLUS_TransferCover", "DeathAmount");
	String TPDAmount = dataTable.getData("HOSTPLUS_TransferCover", "TPDAmount");
	String IPAmount = dataTable.getData("HOSTPLUS_TransferCover", "IPAmount");
	String WaitingPeriod = dataTable.getData("HOSTPLUS_TransferCover", "WaitingPeriod");
	String BenefitPeriod = dataTable.getData("HOSTPLUS_TransferCover", "BenefitPeriod");
	String Dodcheckbox = dataTable.getData("HOSTPLUS_TransferCover", "Dodcheckbox");
	String Privacycheckbox = dataTable.getData("HOSTPLUS_TransferCover", "Privacycheckbox");


try{

	WebDriverWait wait=new WebDriverWait(driver,20);
	//*** Retrieving the saved APP**//
	waitForPageTitle("testing", 10);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Saved Applications']"))).click();
	sleep(250);
	report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
    sleep(4000);
    if(driver.getPageSource().contains("The supported file formats are *.pdf, *.txt, *.doc, *.docx,* .xls, *.xlsx, *.tif, *.png, *.jpg and *.jpeg.")){
		sleep(500);
		quickSwitchWindows();
		sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
	report.updateTestLog("Upload Document Evidence", "Upload Document Evidence continue button clicked", Status.PASS);
	}

	if(driver.getPageSource().contains("You have previously saved application(s).")){
		sleep(500);
		quickSwitchWindows();
		sleep(1000);
		driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
		report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
		sleep(2000);

	}

//	String DeathAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='deathCover.amount | currency:undefined:0']"))).getText();
//	String DeathAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(deathCover.units)']"))).getText();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']"))).click();
    	report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);


//*****	 Retrieved Confirmation Page*****//







	 WebElement email=driver.findElement(By.name("coverDetailsTransferEmail"));
	 String Email_validation=email.getAttribute("value");

	 WebElement contacttype=driver.findElement(By.xpath("//select[@ng-model='preferredContactType']"));
	 String contacttype_validation=contacttype.getAttribute("value");

	 WebElement contactnumber=driver.findElement(By.id("preferrednumCCTransId"));
	 String contactnumber_validation=contactnumber.getAttribute("value");

	 WebElement morning=driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*"));
	 WebElement afternoon=driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*"));




	/*
	 WebElement AttachmentnameV=driver.findElement(By.xpath("//*[@class='color1 ng-binding']"));
	 String Attachmentname_validation=AttachmentnameV.getAttribute("value");*/


	    sleep(100);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		sleep(100);

	 if(Dodcheckbox.equalsIgnoreCase("Yes")){
		 WebElement Dod=driver.findElement(By.xpath(".//input[@name='dod']/parent::*"));
		 Dod.isSelected();
			 report.updateTestLog("DOD Check box", "DOD checkbox is selected", Status.PASS);
			}
			else{

				report.updateTestLog("DOD Check box", "DOD checkbox is not selected", Status.FAIL);
			}

	    sleep(100);
		JavascriptExecutor a = (JavascriptExecutor) driver;
		a.executeScript("window.scrollBy(0,300)", "");
		sleep(100);

	 if(Privacycheckbox.equalsIgnoreCase("Yes")){
		 WebElement Privacy=driver.findElement(By.xpath(".//input[@name='privacy']/parent::*"));
		 Privacy.isSelected();
			 report.updateTestLog("Privacy Check box", "Privacy Statement checkbox is selected", Status.PASS);
			}
			else{

				report.updateTestLog("Privacy Check box", "Privacy Statement checkbox is not selected", Status.FAIL);
			}
	  sleep(100);
		JavascriptExecutor b = (JavascriptExecutor) driver;
		b.executeScript("window.scrollBy(0,400)", "");
		sleep(100);

	 if(EmailId.contains(Email_validation)){

			report.updateTestLog("Email ID", EmailId+" is displayed as Expected", Status.PASS);
		}
		else{

			report.updateTestLog("Email ID", EmailId+" is not displayed as expected", Status.FAIL);
		}

	 if(TypeofContact.contains(contacttype_validation)){

			report.updateTestLog("TypeofContact", TypeofContact+" is displayed as Expected", Status.PASS);
		}
		else{

			report.updateTestLog("TypeofContact", TypeofContact+" is not displayed as expected", Status.FAIL);
		}

	 if(ContactNumber.contains(contactnumber_validation)){

			report.updateTestLog("ContactNumber", ContactNumber+" is displayed as Expected", Status.PASS);
		}
		else{

			report.updateTestLog("ContactNumber", ContactNumber+" is not displayed as expected", Status.FAIL);
		}


	 if(TimeofContact.equalsIgnoreCase("Afternoon")){

		 afternoon.isSelected();
			 report.updateTestLog("Time of contact", TimeofContact+" is displayed as Expected", Status.PASS);
			}
			else{
				morning.isSelected();
				report.updateTestLog("Time of contact", TimeofContact+" is not displayed as expected", Status.FAIL);
			}

	 if(Gender.equalsIgnoreCase("Male")){
		 WebElement GenderM=driver.findElement(By.xpath(".//input[@value='Male']/parent::*"));
		 GenderM.isSelected();
			 report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}
			else{
				 WebElement GenderF=driver.findElement(By.xpath(".//input[@value='Male']/parent::*"));
				GenderF.isSelected();
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}

	    sleep(100);
		JavascriptExecutor c = (JavascriptExecutor) driver;
		c.executeScript("window.scrollBy(0,500)", "");
		sleep(100);
	 Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
	    sleep(1000);
	    WebElement inds =Industry.getFirstSelectedOption();
	    String Industry_validation = inds.getText();
	    sleep(800);

	 WebElement occupation=driver.findElement(By.name("occupationTransfer"));
	 String occupation_validation=occupation.getAttribute("value");





	 WebElement salary=driver.findElement(By.id("transferAnnualSal"));
	 String Annualsalary_validation=salary.getAttribute("value");

	//*****Select the 15 Hours Question*******\\

	   sleep(100);
		JavascriptExecutor d = (JavascriptExecutor) driver;
		d.executeScript("window.scrollBy(0,500)", "");
		sleep(100);
		if(FifteenHoursWork.equalsIgnoreCase("Yes")){
			 WebElement FifteenHoursyes=driver.findElement(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"));
			 FifteenHoursyes.isSelected();
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

			}
		else{
			 WebElement FifteenHoursno=driver.findElement(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"));
			 FifteenHoursno.isSelected();
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			}


		/*if(Citizen.equalsIgnoreCase("Yes")){
			 WebElement Citizenyes=driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]"));
			 Citizenyes.isSelected();
			report.updateTestLog("Citizen or PR", Citizen+"is displayed as Expected", Status.PASS);

			}
		else{
			 WebElement CitizenNo=driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]"));
			 CitizenNo.isSelected();
			report.updateTestLog("Citizen or PR", Citizen+"is not displayed as Expected", Status.PASS);

			}*/

		 if(IndustryType.contains(Industry_validation)){



				report.updateTestLog("Industry", IndustryType+" is displayed as Expected", Status.PASS);
			}
			else{

				report.updateTestLog("Industry", IndustryType+" is not displayed as expected", Status.FAIL);
			}


		 if(OccupationType.contains(occupation_validation)){

				report.updateTestLog("Occupation", OccupationType+" is displayed as Expected", Status.PASS);
			}
			else{

				report.updateTestLog("Occupation", OccupationType+" is not displayed as expected", Status.FAIL);
			}


		 if(OtherOccupation.contains("Yes")){
			 WebElement otheroccupation=driver.findElement(By.name("transferotherOccupation"));
			 String otheroccupation_validation=otheroccupation.getAttribute("value");
		 if(OtherOccupation.contains(otheroccupation_validation)){
				report.updateTestLog("OtherOccupation", OtherOccupation+" is displayed as Expected", Status.PASS);
			}
			else{

				report.updateTestLog("OtherOccupation", OtherOccupation+" is not displayed as expected", Status.FAIL);
			}

		 }



		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	WebElement OfficeEnvironmentyes=driver.findElement(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"));
				    	OfficeEnvironmentyes.isSelected();
				    	report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

				}
				else{

					 WebElement OfficeEnvironmentno=driver.findElement(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"));
					 OfficeEnvironmentno.isSelected();
					 report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	WebElement TertiaryQualyes=driver.findElement(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"));
				        	TertiaryQualyes.isSelected();
				        	report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}
				   else{
						 WebElement TertiaryQualno=driver.findElement(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"));
						 TertiaryQualno.isSelected();
						 report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}
				}



				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



				//******Do you work in hazardous environment*****\\

				if(HazardousEnv.equalsIgnoreCase("Yes")){

					 WebElement HazardousEnvYes=driver.findElement(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[1]"));
					 HazardousEnvYes.isSelected();
					 report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}
				else{

					 WebElement HazardousEnvno=driver.findElement(By.xpath("(//input[@name='hazardousTransferQuestion']/parent::*)[2]"));
					 HazardousEnvno.isSelected();
					 report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}

				//******Do you spend more than 10% of your working time outside of an office environment?*******\\

				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

					 WebElement OutsideOfcPercentYes=driver.findElement(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"));
					 OutsideOfcPercentYes.isSelected();
					 report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}
				else{

					 WebElement OutsideOfcPercentNo=driver.findElement(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"));
					 OutsideOfcPercentNo.isSelected();
					 report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}


		          sleep(800);

				    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				        if(OfficeEnvironment.equalsIgnoreCase("Yes")){
				        	WebElement OfficeEnvironmentyes=driver.findElement(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[1]"));
					    	OfficeEnvironmentyes.isSelected();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

					}
					else{
						 WebElement OfficeEnvironmentno=driver.findElement(By.xpath("(//input[@name='withinOfficeTransferQuestion']/parent::*)[2]"));
						 OfficeEnvironmentno.isSelected();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

					}

					      //*****Do You Hold a Tertiary Qualification******\\

					        if(TertiaryQual.equalsIgnoreCase("Yes")){

					        	WebElement TertiaryQualyes=driver.findElement(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[1]"));
					        	TertiaryQualyes.isSelected();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}
					   else{

						   WebElement TertiaryQualno=driver.findElement(By.xpath("(//input[@name='tertiaryTransferQuestion']/parent::*)[2]"));
							 TertiaryQualno.isSelected();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}
					}

			}

if(AnnualSalary.contains(Annualsalary_validation)){

				report.updateTestLog("Occupation", AnnualSalary+" is displayed as Expected", Status.PASS);
			}
			else{

				report.updateTestLog("Occupation", AnnualSalary+" is not displayed as expected", Status.FAIL);
			}
sleep(100);
JavascriptExecutor e = (JavascriptExecutor) driver;
e.executeScript("window.scrollBy(0,900)", "");
sleep(100);
WebElement PreviousInsurerV=driver.findElement(By.name("previousFundName"));
String PreviousInsurer_validation=PreviousInsurerV.getAttribute("value");

WebElement PolicyNoV=driver.findElement(By.name("membershipNumber"));
String PolicyNo_validation=PolicyNoV.getAttribute("value");

WebElement FormerFundNoV=driver.findElement(By.name("spinNumber"));
String FormerFundNo_validation=FormerFundNoV.getAttribute("value");

if(PreviousInsurer.contains(PreviousInsurer_validation)){

	report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is displayed as Expected", Status.PASS);
}
else{

	report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is not displayed as expected", Status.PASS);
}
if(PolicyNo.contains(PolicyNo_validation)){

	report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is displayed as Expected", Status.PASS);
}
else{

	report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is not displayed as expected", Status.PASS);
}
if(FormerFundNo.contains(FormerFundNo_validation)){

	report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is displayed as Expected", Status.PASS);
}
else{

	report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is not displayed as expected", Status.PASS);
}

WebElement CosttypeV=driver.findElement(By.id("coverId"));
String Costtype_validation=CosttypeV.getAttribute("value");

if(CostType.contains(Costtype_validation)){

	report.updateTestLog("Cost of Insurance Type", CostType+" is displayed as Expected", Status.PASS);
}
else{

    report.updateTestLog("Cost of Insurance Type", CostType+" is not displayed as Expected", Status.PASS);
}



sleep(100);
JavascriptExecutor f = (JavascriptExecutor) driver;
f.executeScript("window.scrollBy(0,1000)", "");

//*************************************************\\
//**************TRANSFER COVER SECTION***************\\
//**************************************************\\

//*****Death transfer amount******

if(DeathYN.equalsIgnoreCase("Yes")){
	WebElement DeathamountV=driver.findElement(By.id("dcTransferAmount"));
	String Deathamount_validation=DeathamountV.getAttribute("value");
	if(DeathAmount.contains(Deathamount_validation)){

		report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is displayed as Expected", Status.PASS);
	}
	else{

		report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is not displayed as Expected", Status.PASS);
	}



}

//*****TPD transfer amount******

if(TPDYN.equalsIgnoreCase("Yes")){
	WebElement TPDAmountV=driver.findElement(By.id("tpdTransferAmount"));
	String TPDAmount_validation=TPDAmountV.getAttribute("value");
if(TPDAmount.contains(TPDAmount_validation)){

	report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is displayed as Expected", Status.PASS);
	}
	else{

		report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is not displayed as Expected", Status.PASS);
	}



}

//****IP transfer amount*****\\



if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){

	WebElement WaitingperiodV=driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']"));
	String Waitingperiod_validation=WaitingperiodV.getAttribute("value");

	WebElement BenefitperiodV=driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']"));
	String Benefitperiod_validation=BenefitperiodV.getAttribute("value");

	WebElement IPAmountV=driver.findElement(By.id("ipTransferAmount"));
	String IPAmount_validation=IPAmountV.getAttribute("value");


	if(WaitingPeriod.contains(Waitingperiod_validation)){

		report.updateTestLog("TPD Cover Transfer Amount", WaitingPeriod+" is displayed as Expected", Status.PASS);
		}
		else{

			report.updateTestLog("TPD Cover Transfer Amount", WaitingPeriod+" is not displayed as Expected", Status.PASS);
		}

	if(BenefitPeriod.contains(Benefitperiod_validation)){

		report.updateTestLog("IP Waiting Period", BenefitPeriod+" is displayed as Expected", Status.PASS);
		}
		else{

		report.updateTestLog("IP Waiting Period", BenefitPeriod+" is not displayed as Expected", Status.PASS);
		}


	if(IPAmount.contains(IPAmount_validation)){

		report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is displayed as Expected", Status.PASS);
		}
		else{

			report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is not displayed as Expected", Status.PASS);
		}


}



}catch(Exception e){
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
}
}
