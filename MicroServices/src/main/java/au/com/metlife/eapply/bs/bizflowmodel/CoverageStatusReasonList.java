package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverageStatusReasonList")

@XmlRootElement
public class CoverageStatusReasonList {	
	
	private String SequenceNo ;
	private String DECCategory ;
	private String DECSubCategory ;
	private String Comments ;
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public String getDECCategory() {
		return DECCategory;
	}
	public void setDECCategory(String category) {
		DECCategory = category;
	}
	public String getDECSubCategory() {
		return DECSubCategory;
	}
	public void setDECSubCategory(String subCategory) {
		DECSubCategory = subCategory;
	}
	public String getSequenceNo() {
		return SequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		SequenceNo = sequenceNo;
	}
	
	
	
}
