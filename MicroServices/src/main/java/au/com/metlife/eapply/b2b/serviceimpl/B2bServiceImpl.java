package au.com.metlife.eapply.b2b.serviceimpl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.b2b.model.AuthToken;
import au.com.metlife.eapply.b2b.model.AuthTokenRepository;
import au.com.metlife.eapply.b2b.model.Eappstring;
import au.com.metlife.eapply.b2b.model.EappstringRepository;
import au.com.metlife.eapply.b2b.service.B2bService;

@Service
public class B2bServiceImpl implements B2bService {
	private static final Logger log = LoggerFactory.getLogger(B2bServiceImpl.class);

	 private final EappstringRepository repository;
	 private final AuthTokenRepository authepository;
	 
	 @Autowired
	    public B2bServiceImpl(final EappstringRepository repository,final AuthTokenRepository authepository) {
	        this.repository = repository;
	        this.authepository = authepository;
	    }
	 
	@Override
	@Transactional
	public Eappstring generateToken(String input, String outhToken) {
		log.info("Generation token start");
		Eappstring eappstring = new Eappstring(input,outhToken, "Active", new Date(), "");
		log.info("Inside gen token");
		eappstring = repository.save(eappstring);
		log.info("Generation token finish");
		return eappstring;
	}
	@Override
	@Transactional
	public Eappstring retriveClientData(String token){	
		log.info("Retriving client data start");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -2);		
		Eappstring eappstring =repository.findBySecureToken(token,new Timestamp(calendar.getTimeInMillis()));
		log.info("Retriving client data finish");
		return eappstring;
	}
	
//	@Override
//	@Transactional
//	public Eappstring retriveClientData(String token){	
//		log.info("Retriving client data start");
//		Eappstring eappstring =repository.findBySecureToken(token);
//		log.info("Retriving client data finish");
//		return eappstring;
//	}
//	
	@Override
	@Transactional
	public AuthToken retriveAuthData(String token){	
		log.info("Retriving client data start");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -1);		
		AuthToken authToken =authepository.findByAuthtoken(token, new Timestamp(calendar.getTimeInMillis()));
		log.info("Retriving client data finish");
		return authToken;
	}
	
	@Override
	@Transactional
	public AuthToken saveAuthToken(String outhToken) {
		log.info("Generation token start");		
		AuthToken authToken = new AuthToken(outhToken, new Date());
		log.info("Inside gen token");
		authToken = authepository.save(authToken);
		log.info("Generation token finish");
		return authToken;
	}
}
