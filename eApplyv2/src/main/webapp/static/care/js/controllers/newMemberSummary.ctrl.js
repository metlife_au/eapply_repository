/*Summary Page Controller Starts*/ 
CareSuperApp.controller('newsummary',['$scope','$rootScope', '$location', '$timeout','$window', 'auraInputService','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService', 
                         function($scope,$rootScope,$location, $timeout,$window, auraInputService,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService){
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    $scope.errorOccured = false;
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	
 // added for session expiry
    /*$timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
     
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;           
    };      	
    $window.scrollTo(0, 0);
    
    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
     };
     var ackCheckNMGC;
    $scope.newMemberCoverDetails=PersistenceService.getNewMemberCoverDetails();
    $scope.newOccDetails = PersistenceService.getNewMemberCoverOccDetails();
    $scope.newAuraDetails = PersistenceService.getNewMemberAuraDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    $scope.deathAddnlDetails=PersistenceService.getNewMemberDeathAddnlDetails();
    $scope.tpdAddnlDetails=PersistenceService.getNewMemberTpdAddnlDetails();
    $scope.ipAddnlDetails=PersistenceService.getNewMemberIpAddnlDetails();
    console.log($scope.newMemberCoverDetails);

    $scope.navigateToDecision = function(){
    	ackCheckNMGC = $('#generalConsentLabelNM').hasClass('active');
       	if(ackCheckNMGC){
    		$scope.NMGCackFlag = false;
    		if($scope.newMemberCoverDetails != null && $scope.newOccDetails != null && $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails!= null 
    				&& $scope.ipAddnlDetails != null && $scope.newAuraDetails != null && $scope.personalDetails[0] != null){
    			$rootScope.$broadcast('disablepointer');
    			$scope.newMemberCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.addnlDeathCoverDetails=$scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails=$scope.ipAddnlDetails;
    			$scope.details.occupationDetails=$scope.newOccDetails;
    			var coverDetails = angular.extend($scope.details,$scope.newMemberCoverDetails);
    			var auraDetails = angular.extend(coverDetails, $scope.newAuraDetails);
            	var submitObject = angular.extend(auraDetails ,$scope.personalDetails[0]);
            	auraResponseService.setResponse(submitObject);
        		submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
            		if($scope.newAuraDetails.overallDecision == 'ACC'){
                		$scope.go('/newmemberaccept');
                	} else if($scope.newAuraDetails.overallDecision == 'DCL'){
                		$scope.go('/newmemberdecline');
                	}
            	}, function(err){
            		$scope.errorOccured = true;
            		$window.scrollTo(0, 0);
            	});
            	console.log("submittt@@@@@@@@@@   " + JSON.stringify(submitObject));
            }
        	
    	}else{
    		//$scope.NMackFlag = true;
        	if(ackCheckNMGC){
        		$scope.NMGCackFlag = false;
        	}else{
        		$scope.NMGCackFlag = true;
        	}
        	$scope.scrollToUncheckedElement();
    	}	
    	
    };
    
    $scope.scrollToUncheckedElement = function(){
       		var elements = [ackCheckNMGC];
    		var ids = ['generalConsentLabelNM'];
    	for(var k = 0; k < elements.length; k++){
    		if(!elements[k]){
    			$('html, body').animate({
        	        scrollTop: $("#" + ids[k]).offset().top
        	    }, 1000);
    			break;
    		}
    	}
    };
   /* $scope.saveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};*/
	  
	/*var appNum;
	appNum = PersistenceService.getAppNumber();*/
    /*$scope.saveNewMemberSummary = function(){
    	$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.newMemberCoverDetails != null && $scope.newOccDetails != null && $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails!= null 
				&& $scope.ipAddnlDetails != null && $scope.newAuraDetails != null && $scope.personalDetails[0] != null){
    		$scope.newMemberCoverDetails.lastSavedOn = 'NewMemberSummaryPage';
    		$scope.details={};
			$scope.details.newDeathAddnlCoverDetails=$scope.deathAddnlDetails;
			$scope.details.newTpdAddnlCoverDetails=$scope.tpdAddnlDetails;
			$scope.details.newIpAddnlCoverDetails=$scope.ipAddnlDetails;
			$scope.details.newOccupationDetails=$scope.newOccDetails;
    		var coverDetails = angular.extend($scope.details,$scope.newMemberCoverDetails);
    		var auraDetails = angular.extend(coverDetails,$scope.newAuraDetails);
        	var saveNewMemberSummaryObject = angular.extend(auraDetails,$scope.personalDetails[0]);
        	auraResponseService.setResponse(saveNewMemberSummaryObject)
	        saveEapply.reqObj().then(function(response) {  
	                console.log(response.data)
	        });
    	}
    };*/
    
    /*$scope.checkAckStateNM = function(){
    	$timeout(function(){
    		ackCheckNMDD = $('#DutyOfDisclosureLabelNM').hasClass('active');
        	ackCheckNMPP = $('#privacyPolicyLabelNM').hasClass('active');
        	ackCheckNMGC = $('#generalConsentLabelNM').hasClass('active');
        	
        	if(ackCheckNMDD){
        		$scope.NMDDackFlag = false;
        	}else{
        		$scope.NMDDackFlag = true;
        	}
        	
        	if(ackCheckNMPP){
        		$scope.NMPPackFlag = false;
        	}else{
        		$scope.NMPPackFlag = true;
        	}
        	
        	if(ackCheckNMGC){
        		$scope.NMGCackFlag = false;
        	}else{
        		$scope.NMGCackFlag = true;
        	}
    	}, 10);
    };*/
    
   /* $scope.checkAckStateDDNM = function(){
    	$timeout(function(){
    		ackCheckNMDD = $('#DutyOfDisclosureLabelNM').hasClass('active');
        	
        	if(ackCheckNMDD){
        		$scope.NMDDackFlag = false;
        	}else{
        		$scope.NMDDackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStatePPNM = function(){
    	$timeout(function(){
    		ackCheckNMPP = $('#privacyPolicyLabelNM').hasClass('active');
        	if(ackCheckNMPP){
        		$scope.NMPPackFlag = false;
        	}else{
        		$scope.NMPPackFlag = true;
        	}
    	}, 10);
    };
    */
    $scope.checkAckStateGCNM = function(){
    	$timeout(function(){
    		ackCheckNMGC = $('#generalConsentLabelNM').hasClass('active');        	
        	if(ackCheckNMGC){
        		$scope.NMGCackFlag = false;
        	}else{
        		$scope.NMGCackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStateLE = function(){
    	$timeout(function(){
    		ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');
        	
        	if(ackCheckLE){
        		$scope.LEFlag = false;
        	}else{
        		$scope.LEFlag = true;
        	}
    	}, 10);
    };

    }]); 
   /*Summary Page Controller Ends*/