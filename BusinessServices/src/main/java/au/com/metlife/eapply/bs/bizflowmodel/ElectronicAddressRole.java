package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElectronicAddressRole")

@XmlRootElement
public class ElectronicAddressRole {	
	
	private ElectronicAddress ElectronicAddress;

	public ElectronicAddress getElectronicAddress() {
		return ElectronicAddress;
	}

	public void setElectronicAddress(ElectronicAddress electronicAddress) {
		ElectronicAddress = electronicAddress;
	}


	
	
	
}
