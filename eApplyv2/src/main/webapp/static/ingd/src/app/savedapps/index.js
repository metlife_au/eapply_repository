import angular from 'angular';

// Create the module where our functionality can attach to
let savedappsModule = angular.module('app.savedapps', []);

// Include our UI-Router config settings
import SavedappsConfig from './savedapps.config';
savedappsModule.config(SavedappsConfig);


// Controllers
import savedAppsCtrl from './savedapps.controller';
savedappsModule.controller('savedAppsCtrl', savedAppsCtrl);


export default savedappsModule;
