import angular from 'angular';
import sessionTimeOut from './sessionTimeoutService';

// Create the module where our functionality can attach to
let timeoutModule = angular.module('app.timeout', [sessionTimeOut]);

// Include our UI-Router config settings
import TimeoutConfig from './timeout.config';
timeoutModule.config(TimeoutConfig);


// Controllers
import TimeoutCtrl from './timeout.controller';
timeoutModule.controller('TimeoutCtrl', TimeoutCtrl);


export default timeoutModule;
