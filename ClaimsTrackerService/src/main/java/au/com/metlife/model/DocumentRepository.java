package au.com.metlife.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface  DocumentRepository extends JpaRepository<Document, String>{
	public List<Document> findByPolicyNum(String policyNum);
}
