(function(angular) {
    'use strict';
    /* global channel, inputData, token, moment */
	/**
	 * @ngdoc controller
	 * @name guildApp.controller:landingPageCtrl
	 *
	 * @description
	 * This is landingPageCtrl description
	 */
    angular
	.module('guildApp')
	.controller('landingCtrl', landingCtrl);
    landingCtrl.$inject=['$scope','$rootScope', '$location','$window', 'fetchClientDataSvc', 'fetchTokenNumSvc','fetchDeathCoverSvc','fetchTpdCoverSvc',
                         'fetchIpCoverSvc','fetchNewMemberOfferSvc', 'fetchPersoanlDetailSvc', '$http', 'appNumberService',
                         'auraInputSvc', 'PersistenceService','ngDialog', 'CheckSavedAppService', 'CancelSavedAppService', 'fetchUrlSvc','urls','LifeEvntRenderService','SpecialCvrRenderService', 'fetchAppNumberSvc', 'RetrieveAppDetailsService', 'appData','CheckUWDuplicateAppService', 'extendAppModelGuild', '$q', 'fetchIndustryListGuil','guilUIConstants'];
    
        function landingCtrl($scope,$rootScope, $location,$window, fetchClientDataSvc, fetchTokenNumSvc, fetchDeathCoverSvc, fetchTpdCoverSvc,
       		 fetchIpCoverSvc, fetchNewMemberOfferSvc, fetchPersoanlDetailSvc, $http,appNumberService,auraInputSvc,
    		 PersistenceService, ngDialog, CheckSavedAppService, CancelSavedAppService,fetchUrlSvc,urls,LifeEvntRenderService,SpecialCvrRenderService, fetchAppNumberSvc, RetrieveAppDetailsService, appData,CheckUWDuplicateAppService, extendAppModelGuild, $q, fetchIndustryListGuil,guilUIConstants) {
        	var vm = this;
            var clientRefNum;
            var age;
            var dob;
            var deathUnits = 0,
                tpdUnits = 0,
                memberType;
            var dateJoined;
            vm.renderView = false;
            vm.isEmpty = function(value) {
                return ((value === '' || value === null) || value === '0');
            };

            vm.init = function() {
            	 vm.HeaderNav = {
            			 logoPath : guilUIConstants.path.images + 'logo.png',
            			 logoUrl: guilUIConstants.navUrls.logoUrl
            	};
                var defer = $q.defer();
                vm.urlList = fetchUrlSvc.getUrlList();
                fetchTokenNumSvc.setTokenId(inputData);
                fetchIndustryListGuil.getObj(vm.urlList.quoteUrl);
                vm.deathCover = {
                    amount: '0.0',
                    benefitPeriod: '',
                    benefitType: '1',
                    coverStartDate: null,
                    coverUnit: null,
                    description: null,
                    exclusions: '',
                    indexation: null,
                    loading: '0.0',
                    occRating: 'General',
                    type: '2',
                    units: '0',
                    waitingPeriod: ''
                };
                vm.tpdCover = {
                    amount: '0.0',
                    benefitPeriod: '',
                    benefitType: '2',
                    coverStartDate: null,
                    coverUnit: null,
                    description: null,
                    exclusions: '',
                    indexation: null,
                    loading: '0.0',
                    occRating: 'General',
                    type: '2',
                    units: '0',
                    waitingPeriod: ''
                };
                vm.ipCover = {
                    amount: '0.0',
                    benefitPeriod: '',
                    benefitType: '4',
                    coverStartDate: null,
                    coverUnit: null,
                    description: null,
                    exclusions: '',
                    indexation: null,
                    loading: '0.0',
                    occRating: 'General',
                    type: '2',
                    units: '0',
                    waitingPeriod: ''
                };
                fetchClientDataSvc.requestObj(vm.urlList.clientDataUrl).then(function(response) {
                    var applicantlist = null;
                    vm.partnerRequest = response.data;
                    vm.calcEncrpytURL = vm.partnerRequest.calcEncrpytURL;
                    applicantlist = vm.partnerRequest.applicant[0];
                    memberType = applicantlist.memberType;
                    vm.clientRefNum = applicantlist.clientRefNumber || 0;
                    vm.personalDetails = applicantlist.personalDetails;
                    vm.coverList = applicantlist.existingCovers.cover;
                    vm.fName = applicantlist.personalDetails.firstName;
                    vm.lName = applicantlist.personalDetails.lastName;
                    $rootScope.$emit('getdata',vm.fName);
                    dob = applicantlist.personalDetails.dateOfBirth;
                    vm.maxAge = moment().diff(moment(applicantlist.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years') + 1;
                    //added to check for special offer scenario
                    dateJoined = moment().diff(moment(applicantlist.dateJoined, 'DD-MM-YYYY'), 'days') + 1;
                    vm.newMemberOffer = false;
                    if (moment().diff(moment(applicantlist.welcomeLetterDate, 'DD-MM-YYYY'), 'days') + 1 < 91 && moment(moment(moment(applicantlist.welcomeLetterDate, 'DD-MM-YYYY')).format('MM-DD-YYYY')).isAfter('01-31-2017', 'day') && vm.maxAge < 61 && (applicantlist.memberType === 'Industry' || applicantlist.memberType === 'Corporate')) {
                        vm.newMemberOffer = true;
                    }
                    fetchNewMemberOfferSvc.setNewMemberOfferFlag(vm.newMemberOffer);
                    angular.forEach(vm.coverList, function(value, key) {
                        if (value.benefitType === '1') {
                            vm.deathCover = value;
                            deathUnits = value.units;
                        } else if (value.benefitType === '2') {
                            vm.tpdCover = value;
                            tpdUnits = value.units;
                        } else if (value.benefitType === '4') {
                            vm.ipCover = value;

                        }
                    });
                    fetchDeathCoverSvc.setDeathCover(vm.deathCover);
                    fetchTpdCoverSvc.setTpdCover(vm.tpdCover);
                    fetchIpCoverSvc.setIpCover(vm.ipCover);
                    vm.customDigest();
                    /*if (vm.deathCover.type === '1' && vm.tpdCover.type === '1' && (vm.intvalue(vm.deathCover.units) || vm.intvalue(vm.tpdCover.units))) {
                        vm.convertMaintainDisable = true;
                    } else {
                        vm.convertMaintainDisable = false;
                    }*/

                    /*if (vm.deathCover.type === '2' && vm.tpdCover.type === '2' && (vm.intvalue(vm.deathCover.amount) || vm.intvalue(vm.tpdCover.amount) || vm.intvalue(vm.ipCover.amount))) {
                        vm.cancelEnable = true;
                    } else if (vm.deathCover.type === '1' && vm.tpdCover.type === '1' && (vm.intvalue(vm.deathCover.units) || vm.intvalue(vm.tpdCover.units) || vm.intvalue(vm.ipCover.amount))) {
                        vm.cancelEnable = true;
                    } else {
                        vm.cancelEnable = false;
                    }*/
                    
                    /*if (!vm.intvalue(vm.deathCover.amount) || !vm.intvalue(vm.tpdCover.amount)){
                    	vm.lifeEventDisable = true;
                    }else{
                    	vm.lifeEventDisable = false;
                    }*/
                    
                    /*if (!vm.intvalue(vm.deathCover.amount) && !vm.intvalue(vm.tpdCover.amount) && !vm.intvalue(vm.ipCover.amount)) {
                        vm.lifeEventDisable = true;
                    } else {
                        vm.lifeEventDisable = false;
                    }

                    if (!vm.lifeEventDisable) {
                        if (vm.deathCover.type === '1' && vm.tpdCover.type === '1') {
                        	vm.lifeEventDisable = false; 
                        } else {
                            vm.lifeEventDisable = true;
                        }
                    }*/
                    
                    
                    vm.appContactDetails = applicantlist.contactDetails || {};
                    vm.appContactDetails.homePhone = vm.appContactDetails.homePhone ? vm.appContactDetails.homePhone.replace(/[^0-9]/g, '') : vm.appContactDetails.homePhone;
                    vm.appContactDetails.mobilePhone = vm.appContactDetails.mobilePhone ? vm.appContactDetails.mobilePhone.replace(/[^0-9]/g, '') : vm.appContactDetails.mobilePhone;
                    vm.appContactDetails.workPhone = vm.appContactDetails.workPhone ? vm.appContactDetails.workPhone.replace(/[^0-9]/g, '') : vm.appContactDetails.workPhone;
                    angular.extend(applicantlist.contactDetails, vm.appContactDetails);
                    fetchPersoanlDetailSvc.setMemberDetails(applicantlist);
                    // to render special offer tile
                    /*if (vm.deathCover.type === '1' && vm.tpdCover.type === '1') {
                        SpecialCvrRenderService.requestObj(vm.urlList.specialCvrEligUrl, 'MTAA', vm.maxAge, deathUnits, tpdUnits, dateJoined, vm.clientRefNum, vm.fName, vm.lName, applicantlist.personalDetails.dateOfBirth).then(function(res) {
                            defer.resolve(res);
                            vm.check = res.data;
                            if (vm.newMemberOffer === false && vm.check === true && (memberType.toLowerCase().trim() === 'industry' || memberType.toLowerCase().trim() === 'executive') && (vm.deathCover.units > 0 || vm.deathCover.amount > 0) && vm.tpdCover.type === '1' && vm.maxAge < 65 && vm.ipCover.amount === 0) {
                                vm.renderSpecialCover = true;
                            } else {
                                vm.renderSpecialCover = false;
                            }
                        }, function(err) {
                            console.info('Error while fetching default units ' + JSON.stringify(err));
                            defer.reject(err);
                        });
                    } else {
                        defer.resolve({});
                    }*/

                });
                return defer.promise;
            };

            vm.init().then(function() {
                vm.renderView = true;
            }, function() {
                vm.renderView = true;
            });

            vm.customDigest = function() {
            	if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
            	      $scope.$digest();
                }
            };

            vm.go = function(path, generateAppnum, num) {
                if (generateAppnum) {
                    //generate app number
                    appNumberService.requestObj(vm.urlList.appNumUrl).then(function(response) {
                        fetchAppNumberSvc.setAppNumber(response.data);
                        $location.path(path);
                    });
                } else {
                    if (num){
                    	fetchAppNumberSvc.setAppNumber(num);
                    }
                    $location.path(path);
                }
            };
            vm.tokenid = fetchTokenNumSvc.getTokenId();
            vm.openWindow = function(url) {
                $window.open(url);
            };
            vm.showNewMemberPopUp = function(val) {
                if (val === null || val === '' || val === ' ') {
                    vm.hideTips();
                } else {
                    document.getElementById('mymodalNewMem').style.display = 'block';
                    document.getElementById('mymodalNewMemFade').style.display = 'block';
                    document.getElementById('newMember_text').innerHTML = val;
                }
            };
            vm.hideTips = function() {
                if (document.getElementById('help_div')) {
                    document.getElementById('help_div').style.display = 'none';
                }
            };
            vm.intvalue = function(amount) {
                return parseInt(amount) > 0 ? true : false;
            };
            
            /**
        	 * @ngdoc method
        	 * @name showSavedAppPopup
        	 * @methodOf guildApp.controller:landingPageCtrl
        	 * @description
        	 * This method checks for saved applications for a particular cover and redirects accordingly.
        	 *
        	 */
            
            vm.showSavedAppPopup = function(manageType) {
                CheckSavedAppService.checkSavedApps(vm.urlList.savedAppUrl, 'GUIL', vm.clientRefNum, manageType).then(function(res) {
                    vm.apps = res.data;
                    if (vm.apps.length > 0) {
                        var newScope = $scope.$new();
                        for (var i = 0; i < vm.apps.length; i++) {
                            var tempDate = new Date(vm.apps[i].createdDate);
                            vm.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');

                            if (vm.apps[i].requestType === 'CCOVER') {
                                vm.apps[i].requestType = 'Change Cover';
                            } else if (vm.apps[i].requestType === 'TCOVER') {
                                vm.apps[i].requestType = 'Transfer Cover';
                            } /*else if (vm.apps[i].requestType === 'UWCOVER') {
                                vm.apps[i].requestType = 'Update Work Rating';
                            }*/ else if (vm.apps[i].requestType === 'NCOVER') {
                                vm.apps[i].requestType = 'New Member Cover';
                            } /*else if (vm.apps[i].requestType === 'ICOVER') {
                                vm.apps[i].requestType = 'Life Event Cover';
                            }*/ /*else if (vm.apps[i].requestType === 'SCOVER') {
                                vm.apps[i].requestType = 'Special Cover';
                            }*/ /*else if (vm.apps[i].requestType === 'CANCOVER') {
                                vm.apps[i].requestType = 'Cancel Cover';
                            }*/
                        }
                        // Created a new scope for the modal popup as the parent controller variables are not accessible to the modal
                        newScope.apps = vm.apps;

                        ngDialog.openConfirm({
                            template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title\'>You have previously saved application(s).</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'><div class=\'col-sm-12\'><p> Please continue with the saved application or cancel saved application to start a new application. </p> </div> </div><!-- Row ends --> <div class=\'row d-block d-sm-none\'><div class=\'col-sm-12\'> ' + 
                            '<table width=\'100%\' cellspacing=\'0\' cellpadding=\'0\' border=\'0\' class=\'grid\'><tbody><tr><th class=\'coverbg\'><h5>Application no.</h5></th><th class=\'coverbg \'><h5>Date saved</h5></th>' + 
                            '<th class=\'coverbg \'><h5>Service request type</h5> </th></tr> <tr ng-repeat=\'app in apps\'><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div></div> <div class=\'row d-none d-sm-block\'><div class=\'col-sm-12\'> <table width=\'100%\' cellspacing=\'0\' cellpadding=\'0\' border=\'0\' class=\'grid\'><tbody><tr><th width=\'20%\' class=\'coverbg\'><h5>Application no.</h5></th>' + 
                            '<th width=\'20%\' class=\'coverbg\'><h5>Date saved</h5></th><th width=\'20%\' class=\'coverbg\'><h5>Service request type</h5> </th></tr> <tr ng-repeat=\'app in apps\'><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div> </div></div></br>'+ 
                            '<div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button><button type=\'button\' class=\'btn btn-primary ml10\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Continue</button></div></div></div>',
                            plain: true,
                            className: 'ngdialog-theme-plain custom-width',
                            scope: newScope
                        }).then(function(value) {
                            fetchAppNumberSvc.setAppNumber(vm.apps[0].applicationNumber);
                            PersistenceService.setAppNumToBeRetrieved(vm.apps[0].applicationNumber);
                            PersistenceService.setAppNumber(vm.apps[0].applicationNumber);
                            RetrieveAppDetailsService.retrieveAppDetails(vm.urlList.retrieveAppUrlNew, vm.apps[0].applicationNumber).then(function(res) {
                                console.log(res);
                                appData.setAppData(extendAppModelGuild.extendObj(res.data[0]));
                                switch (vm.apps[0].lastSavedOnPage.toLowerCase()) {
                                    case 'quotepage':
                                        vm.go('/cover/3');
                                        break;
                                    case 'aurapage':
                                        vm.go('/aura');
                                        break;
                                    case 'summarypage':
                                        vm.go('/summary');
                                        break;
                                    case 'transferpage':
                                    	PersistenceService.setTransferCoverDetails(res.data[0]);
                                        vm.go('/quotetransfer/3');
                                        break;
                                    case 'auratransferpage':
                                    	PersistenceService.setTransferCoverDetails(res.data[0]);
                                        vm.go('/auratransfer/3');
                                        break;
                                    case 'summarytransferpage':
                                    	PersistenceService.setTransferCoverDetails(res.data[0]);
                                        vm.go('/transferSummary/3');
                                        break;
                                    case 'quoteupdatepage':
                                        vm.go('/quoteoccchange/3');
                                        break;
                                    case 'auraupdatepage':
                                        vm.go('/auraocc/3');
                                        break;
                                    case 'summaryupdatepage':
                                        vm.go('/workRatingSummary/3');
                                        break;
                                    case 'lifeeventpage':
                                    	PersistenceService.setLifeEventDetails(res.data[0]);
                                        vm.go('/quoteLife/3');
                                        break;
                                    case 'auralifeeventpage':
                                    	PersistenceService.setLifeEventDetails(res.data[0]);
                                        vm.go('/auraLife/3');
                                        break;
                                    case 'summarylifeeventpage':
                                    	PersistenceService.setLifeEventDetails(res.data[0]);
                                        vm.go('/summaryLife/3');
                                        break;
                                    case 'specialquotepage':
                                        vm.go('/quotespecial/3');
                                        break;
                                    case 'specialcoveraurapage':
                                        vm.go('/auraspecial/3');
                                        break;
                                    case 'specialsummarypage':
                                        vm.go('/specialoffersummary/3');
                                        break;
                                    default:
                                        break;
                                }
                            });
                        }, function(value) {
                            if (value === 'oncancel') {
                                CancelSavedAppService.cancelSavedApps(vm.urlList.cancelAppUrl, vm.apps[0].applicationNumber).then(function(result) {
                                    if (vm.newMemberOffer) {
                                        vm.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
                                    } else {
                                        appData.setAppData(extendAppModelGuild.extendObj());
                                        switch (vm.apps[0].lastSavedOnPage.toLowerCase()) {
                                            case 'quotepage':
                                            case 'aurapage':
                                            case 'summarypage':
                                                vm.go('/cover/1', true);
                                                break;
                                            case 'transferpage':
                                            case 'auratransferpage':
                                            case 'summarytransferpage':
                                                vm.go('/quotetransfer/1', true);
                                                break;
                                            case 'quoteupdatepage':
                                            case 'auraupdatepage':
                                            case 'summaryupdatepage':
                                                vm.go('/quoteoccchange/1', true);
                                                break;
                                            case 'lifeeventpage':
                                            case 'auralifeeventpage':
                                            case 'summarylifeeventpage':
                                                vm.go('/quoteLife/1', true);
                                                break;
                                            case 'specialquotepage':
                                            case 'specialcoveraurapage':
                                            case 'specialsummarypage':
                                                vm.go('/quotespecial/1', true);
                                                break;
                                            case 'specialquotepage':
                                                vm.go('/quotespecial/1', true);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }, function(err) {
                                    console.log('Error while cancelling the saved app ' + err);
                                });
                            }
                        });
                    } else {
                        if (vm.newMemberOffer) {
                            vm.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
                        } else {
                            appData.setAppData(extendAppModelGuild.extendObj());
                            switch (manageType.toUpperCase()) {
                                case 'CCOVER':
                                    vm.checkDupApplication('CCOVER');
                                    break;
                                case 'TCOVER':
                                    vm.go('/quotetransfer/1', true);
                                    break;
                                /*case 'UWCOVER':
                                    vm.go('/quoteoccchange/1', true);
                                    break;*/
                                /*case 'ICOVER':
                                    vm.go('/quoteLife/1', true);
                                    break;*/
                                /*case 'SCOVER':
                                    vm.go('/quotespecial/1', true);
                                    break;*/
                                /*case 'CANCOVER':
                                    vm.go('/quoteCancel/1', true);
                                    break;*/
                                default:
                                    break;

                            }
                        }
                    }
                }, function(err) {
                    console.log('Error while fetching saved apps ' + err);
                });
            };
            
            /**
        	 * @ngdoc method
        	 * @name handleBtnKeyPress
        	 * @methodOf guildApp.controller:landingPageCtrl
        	 * @description
        	 * This method gets called on enter press of accessible icons and buttons.
        	 * 
        	 */
            
            vm.handleBtnKeyPress = function(event) {
				  // Check to see if space or enter were pressed
				if (event.key === ' ' || event.key === 'Enter') {
				  // Prevent the default action to stop scrolling when space is pressed
				  event.preventDefault();
				  toggleButton(event.target);
				}
			};
            
			function toggleButton(element) {
					  // Check to see if the button is pressed
				var pressed = (element.getAttribute('aria-pressed') === 'true');
					  // Change aria-pressed to the opposite state
				element.setAttribute('aria-pressed', !pressed);
				var hhtext;
				if(element.id==='increaseCover'){
					hhtext='Increase your cover without the need to provide any medical evidence, if you have experienced any of the below events in your life within the last six months.<ul><li>Marriage</li><li>Birth or adoption of a child</li><li>Taking out or increasing a home mortgage</li><li>Divorce</li></ul>';
					vm.clickToOpen(hhtext);
				}else if(element.id==='fixCover'){
					hhtext='With fixed cover, your cover stays the same, but the amount you pay for the insurance increases as you get older.';
					vm.clickToOpen(hhtext);
				}
				
			}
			/**
        	 * @ngdoc method
        	 * @name clickToOpen
        	 * @methodOf guildApp.controller:landingPageCtrl
        	 * @description
        	 * This method is a generic method which gets called on click of helpfull icons.
        	 * 
        	 */
            vm.clickToOpen = function(hhText, lifeEventDisable, convertMaintainDisable) {
            	 var dialog;
                if (((convertMaintainDisable !== undefined && convertMaintainDisable) || convertMaintainDisable === undefined) && ((lifeEventDisable !== undefined && !lifeEventDisable) || lifeEventDisable === undefined)) {
                   dialog = ngDialog.open({

                        template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Helpful hints</h4><!-- Row starts --><div class=\'row rowcustom\' style=\'margin:0px -35px;\'><div class=\'col-sm-12\'>'+
                        '<p class=\'aligncenter\'></p><div id=\'tips_text\'>' + hhText + '</div><p></p></div></div><!-- Row ends --></div><div class=\'row\'><div class=\'col-sm-4\'></div><div class=\'col-sm-4 col-12\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Close</button></div><div class=\'col-sm-4\'></div></div></div>',
                        className: 'ngdialog-theme-plain',
                        plain: true
                    });
                } else if ((convertMaintainDisable !== undefined && !convertMaintainDisable) || (lifeEventDisable !== undefined && lifeEventDisable)) {
                    dialog = ngDialog.open({

                        template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'></h4><!-- Row starts --><div class=\'row rowcustom\' style=\'margin:0px -35px;\'><div class=\'col-sm-12\'><p class=\'aligncenter\'></p><div id=\'tips_text\'>' + hhText + 
                        '</div><p></p></div></div><!-- Row ends --></div><div class=\'row\'><div class=\'col-sm-4\'></div><div class=\'col-sm-4 col-12\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Close</button></div><div class=\'col-sm-4\'></div></div></div>',
                        className: 'ngdialog-theme-plain',
                        plain: true
                    });
                }
                dialog.closePromise.then(function(data) {
                    console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
                });
            };
            
            /**
        	 * @ngdoc method
        	 * @name checkDupApplication
        	 * @methodOf guildApp.controller:landingPageCtrl
        	 * @description
        	 * This method checks for duplicate applications submitted by member.
        	 * 
        	 */
            
            /* Change for Duplicate Application UW message*/
            vm.checkDupApplication = function(manageTypeCC) {
                CheckUWDuplicateAppService.checkDuplicateApps(vm.urlList.dupAppUrl, 'GUIL', vm.clientRefNum, manageTypeCC, dob, vm.fName, vm.lName).then(function(res) {
                    console.log(res.data);
                    vm.check = res.data;
                    if (vm.check) {
                        ngDialog.openConfirm({
                            template: '<div class=\'ngdialog-contentdup\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title\'>YOU HAVE PREVIOUSLY SUBMITTED APPLICATION(S)</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'>'+
                            '<div class=\'col-sm-15\'><p> Thank you for commencing your application, we note that you have recently lodged an application for additional insurance in the last 30 days that was referred to our underwriting team, please note that your application is currently under assessment and we will be in contact with you as soon as possible with an update.</p> <p>If you would like to initiate a different application to the one that is pending, ' + 
                            'please continue with this application.</p> </div></div></div></br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary mr10px pr20px avoid-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Continue</button></div></div></div>',
                            plain: true,
                            className: 'ngdialog-theme-plain custom-width'
                        }).then(function() {
                            vm.go('/cover/3', true);
                        }, function(e) {
                            if (e === 'oncancel') {
                                return false;
                            }
                        });
                    } else {
                        vm.go('/cover/1', true);
                    }

                }, function(err) {
                    console.log('Error ' + err);
                });

            };
            
            vm.logout = function(path){
              	//window.location = sessionStorage.loginPath;
              	$window.close();
            };

        }
   
})(angular);