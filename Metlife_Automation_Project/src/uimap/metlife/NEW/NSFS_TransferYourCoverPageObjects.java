package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class NSFS_TransferYourCoverPageObjects extends PageObjectLibrary{
		
	public static final By  TransferYourCoverLink = getObjectIdentifaction("NSFS.TYC.Link.xpath");
	public static final By  EmailId = getObjectIdentifaction("NSFS.TYC.Email.id");
	public static final By  ContactTypeClick = getObjectIdentifaction("NSFS.TYC.ContactTypeClick.xpath");
	public static final By  ContactNumber = getObjectIdentifaction("NSFS.TYC.ContactNumber.id");
	public static final By  MorningContact = getObjectIdentifaction("NSFS.TYC.MorningContact.id");
	public static final By  AfterNoonContact = getObjectIdentifaction("NSFS.TYC.AfterNoonContact.id");
	public static final By  ContactDetailsContinue = getObjectIdentifaction("NSFS.TYC.ContactDetailsContinue.xpath");
	public static final By  FifteenHoursYes = getObjectIdentifaction("NSFS.TYC.FifteenHoursYes.id");
	public static final By  FifteenHoursNo = getObjectIdentifaction("NSFS.TYC.FifteenHoursNo.id");
	public static final By  CitizenYes = getObjectIdentifaction("NSFS.TYC.CitizenYes.id");
	public static final By  CitizenNo = getObjectIdentifaction("NSFS.TYC.CitizenNo.id");
	public static final By  ClickIndustry = getObjectIdentifaction("NSFS.TYC.ClickIndustry.xpath");
	public static final By  ClickOccupation = getObjectIdentifaction("NSFS.TYC.ClickOccupation.xpath");
	public static final By  OtherOccupation = getObjectIdentifaction("NSFS.TYC.OtherOccupation.id");
	public static final By  OfficeEnvironmentYes = getObjectIdentifaction("NSFS.TYC.OfficeEnvironmentYes.id");
	public static final By  OfficeEnvironmentNo = getObjectIdentifaction("NSFS.TYC.OfficeEnvironmentNo.id");
	public static final By  TertiaryQualYes = getObjectIdentifaction("NSFS.TYC.TertiaryQualYes.id");
	public static final By  TertiaryQualNo = getObjectIdentifaction("NSFS.TYC.TertiaryQualNo.id");
	public static final By  HazardousEncYes = getObjectIdentifaction("NSFS.TYC.HazardousEncYes.id");
	public static final By  HazardousEncNo = getObjectIdentifaction("NSFS.TYC.HazardousEncNo.id");
	public static final By  OutsideOfcPercentYes = getObjectIdentifaction("NSFS.TYC.OutsideOfcPercentYes.id");
	public static final By  OutsideOfcPercentNo = getObjectIdentifaction("NSFS.TYC.OutsideOfcPercentNo.id");
	public static final By  AnnualSalary = getObjectIdentifaction("NSFS.TYC.AnnualSalary.id");
	public static final By  LoadSalaryLabel = getObjectIdentifaction("NSFS.TYC.LoadSalaryLabel.xpath");
	public static final By  ContinueOccupationDetails = getObjectIdentifaction("NSFS.TYC.ContinueOccupationDetails.xpath");


	public static final By PreviousInsurer = getObjectIdentifaction("NSFS.TYC.PreviousInsurer.id");
	public static final By PolicyNumber = getObjectIdentifaction("NSFS.TYC.PolicyNumber.id");
	public static final By USINumber = getObjectIdentifaction("NSFS.TYC.USINumber.id");
	public static final By DocumentEvidenceYes = getObjectIdentifaction("NSFS.TYC.DocumentEvidenceYes.id");
	public static final By DocumentEvidenceNo = getObjectIdentifaction("NSFS.TYC.DocumentEvidenceNo.id");
	
	
	public static final By TransferChkbox = getObjectIdentifaction("NSFS.TYC.TransferChkbox.id");
	public static final By CostofInsurance = getObjectIdentifaction("NSFS.TYC.CostofInsurance.xpath");
	public static final By ContinueAfterPreviousCover = getObjectIdentifaction("NSFS.TYC.ContinueAfterPreviousCover.xpath");
	public static final By DeathAmount = getObjectIdentifaction("NSFS.TYC.DeathAmount.id");
	public static final By LoadDeathAmount = getObjectIdentifaction("NSFS.TYC.LoadDeathAmount.xpath");
	public static final By TPDAmount = getObjectIdentifaction("NSFS.TYC.TPDAmount.id");
	public static final By LoadTPDAmount = getObjectIdentifaction("NSFS.TYC.LoadTPDAmount.xpath");
	public static final By IPAmount = getObjectIdentifaction("NSFS.TYC.IPAmount.id");
	public static final By LoadIPAmount = getObjectIdentifaction("NSFS.TYC.LoadIPAmount.xpath");
	public static final By Waitingperiod = getObjectIdentifaction("NSFS.TYC.Waitingperiod.xpath");
	public static final By benefitperiod = getObjectIdentifaction("NSFS.TYC.benefitperiod.xpath");
	public static final By CalculateQuote = getObjectIdentifaction("NSFS.TYC.CalculateQuote.xpath");	
	public static final By ContinueAfterTYC = getObjectIdentifaction("NSFS.TYC.ContinueAfterTYC.xpath");
}
