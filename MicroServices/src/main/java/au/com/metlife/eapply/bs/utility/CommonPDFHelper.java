package au.com.metlife.eapply.bs.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Exclusion;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.bs.pdf.CostumPdfAPI;
import au.com.metlife.eapply.bs.pdf.HTMLToString;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureStatement;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonPDFHelper {
	private CommonPDFHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(CommonPDFHelper.class);
	
    public static final String PACKAGE_NAME = "com.metlife.eapply.pdf.helper"; /*$NON-NLS-1$*/
    
    /*Name given to this class.*/
    public static final String CLASS_NAME = "CommonPDFHelper"; /*$NON-NLS-1$*/
    
    
    public static String getDateInString(Date dte) {
        log.info("Get date in string start");
        java.util.Date currDate = dte;
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String theDate="";
        
        if(null != currDate && !"".equalsIgnoreCase(""+currDate)){
        	theDate = formatter.format(currDate);	
        }
        
         log.info("Get date in string finish");
        return theDate;
    }
	
    public static String getTodaysDate() {
    	log.info("Get todays date start");

        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String theDate = formatter.format( currDate);
        log.info("Get todays date finish");
        return theDate;
    }
    public static String getDateExtension() {
        log.info("Get date extension start");
        java.util.Date currDate = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat( "hhmmddMMyyyy");
        String theDate = formatter.format( currDate);
        log.info("Get date extension finish");
        return theDate;
    }
    
    /**
     * Formats amount to $#,###,###.00 e.g. $1,000.00
     * 
     * @param amount
     * @return
     */
    public static String formatAmount(String amount) {
        log.info("Format amount start");
        String amt = amount;
        String formattedStr = amt;
        NumberFormat formatter = null;
        try {
            formatter = new DecimalFormat( "$#,###,###.00");
            if (amt != null && !amt.trim().equalsIgnoreCase( "")) {
                if (amt.contains( "$")) {
                	amt = amt.replace( "$", "").trim();
                }
                if (amt.contains( ",")) {
                	amt = amt.replace( ",", "").trim();
                }
                formattedStr = formatter.format( new BigDecimal( amt).doubleValue());
                if (formattedStr != null
                        && ( formattedStr.substring( 1, ( formattedStr.indexOf( '.')))).equals( "")) {
                    formattedStr = "$0."
                            + formattedStr.substring( ( formattedStr.indexOf( '.')) + 1);
                }
            }
        } catch (Exception e) {
        	log.error("Error in fiormat amount: {}",e);
        } 
        log.info("Format amount finish");
        return formattedStr;
    }
    
    /**
     * Formats amount to $#,###,###.00 e.g. $1,000.00
     * 
     * @param amount
     * @return
     */
    public static String formatIntAmount(String amount) {
         log.info("Format int amount start");
        String amt = amount;
         String formattedStr = amt;
        NumberFormat formatter = null;
        try {
            formatter = new DecimalFormat( "$#,###,###");
            if (amt != null && !amt.trim().equalsIgnoreCase( "")) {
                if (amt.contains( "$")) {
                	amt = amt.replace( "$", "").trim();
                }
                if (amt.contains( ",")) {
                	amt = amt.replace( ",", "").trim();
                }
                formattedStr = formatter.format( new BigDecimal( amt).intValue());
                /*if (formattedStr != null
                        && ( formattedStr.substring( 1, ( formattedStr.indexOf( ".")))).equals( "")) {
                    formattedStr = "$0."
                            + formattedStr.substring( ( formattedStr.indexOf( ".")) + 1);
                }*/
            }
        } catch (Exception e) {
        	log.error("Error in format Int amount {}",e);
        } 
        log.info("Format int amount finish");
        return formattedStr;
    }
    
    
    public static String numberFormat (Double number) {
    	log.info("Number format start");
		String pattern = "###,###.##"; 
		DecimalFormat formatter = new DecimalFormat(pattern); 
		if (number != null) {
			return formatter.format(number);
			
		} else {
			return null;
		}
	}
    
	public static void dispalyRuleDecision(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject, ResponseObject responseObject) throws DocumentException{
		 log.info("Display rule decision start");
		 Insured insured = null;
		
		PdfPTable pdfPEmptyTable = new PdfPTable( 1);
        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPEmptyCell3);
        pdfPEmptyTable.addCell( pdfPEmptyCell3);
        document.add( pdfPEmptyTable);
        
        float[] decFloat = { 0.8f, 0.2f };
        
        PdfPTable pdfDecisionTable = new PdfPTable( decFloat);
        PdfPCell pdfPDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Original decision details", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
        pdfPDecisionCell.setBorderWidth( 1);
        /*pdfPDecisionCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
        /*pdfAPI.disableBordersTop( pdfPDecisionCell);*/
        pdfAPI.disableBorders(pdfPDecisionCell);
        if("INGD".equalsIgnoreCase(pdfObject.getFundId())) {
        pdfPDecisionCell.setBackgroundColor(pdfAPI.getHeader_color());
        }
        else {
        	 pdfPDecisionCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
        }
        pdfPDecisionCell.setPaddingBottom( 6);
        pdfPDecisionCell.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfDecisionTable.addCell( pdfPDecisionCell);
        pdfPDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
        pdfAPI.disableBorders( pdfPDecisionCell);
        pdfDecisionTable.addCell( pdfPDecisionCell);
        document.add( pdfDecisionTable);
        
        pdfPEmptyTable = new PdfPTable( 1);
        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPEmptyCell3);
        pdfPEmptyTable.addCell( pdfPEmptyCell3);
        document.add( pdfPEmptyTable);
		
		float[] f1 = { 0.2f, 0.2f, 0.4f };
        PdfPTable pdfDecisionTable1 = new PdfPTable( f1);
        
        PdfPCell pdfDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Decision type",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfDecisionCell);
        pdfDecisionTable1.addCell( pdfDecisionCell);
        pdfDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Product", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfDecisionCell);
        pdfDecisionTable1.addCell( pdfDecisionCell);
        pdfDecisionCell = new PdfPCell(
                pdfAPI.addParagraph( "Decision", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_MIDDLE);
        pdfAPI.disableBorders( pdfDecisionCell);
        pdfDecisionTable1.addCell( pdfDecisionCell);
        document.add( pdfDecisionTable1);
        
        PdfPTable pdfSingleSpaceTable = new PdfPTable( 1);
        PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        document.add( pdfSingleSpaceTable);
		if (null!=responseObject
				&& null!=responseObject.getClientData()
                && null != responseObject.getClientData().getListInsured()) {
            for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
                insured =  responseObject.getClientData().getListInsured().get( itr);
                	dispalyRuleDecisionDetails( insured.getListRuleDec(), pdfAPI, document,pdfObject);               
            
                	
                	if("Institutional".equalsIgnoreCase(pdfObject.getLob())){
                		/*if(null!=insured.getListGenderAgeBMIDec() && insured.getListGenderAgeBMIDec().size()>0){*/
                		if(null!=insured.getListGenderAgeBMIDec() && !(insured.getListGenderAgeBMIDec().isEmpty())){
                    		dispalyGenderAgeBMIDetails(insured.getListGenderAgeBMIDec(), pdfAPI, document,pdfObject);
                    	}            
                	}else{
                		/*if(null!=insured.getListGenderAgeBMIRangeTable() && insured.getListGenderAgeBMIRangeTable().size()>0){*/
                		if(null!=insured.getListGenderAgeBMIRangeTable() && !(insured.getListGenderAgeBMIRangeTable().isEmpty())){
                    		dispalyGenderAgeBMIDetails( insured.getListGenderAgeBMIRangeTable(), pdfAPI, document,pdfObject);
                    	}                 		
                		/*if(null!=insured.getListGenderAgeCalculatedBMIRangeMetlifeTable() && insured.getListGenderAgeCalculatedBMIRangeMetlifeTable().size()>0){*/
                		if(null!=insured.getListGenderAgeCalculatedBMIRangeMetlifeTable() && !(insured.getListGenderAgeCalculatedBMIRangeMetlifeTable().isEmpty())){
                    		dispalyGenderAgeBMIDetails( insured.getListGenderAgeCalculatedBMIRangeMetlifeTable(), pdfAPI, document,pdfObject);
                    	}
                	}
                	
                	
                
                dispalyDependentRiskDetails(insured.getListDepRiskDec(),pdfAPI, document,pdfObject);
                
            }
            
        }
		

        log.info("Display rule decision finish");
	}
	
	public static void dispalyINGRuleDecision(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject, ResponseObject responseObject) throws DocumentException{
		
		log.info("Display ING rule decision start");
		 Insured insured = null;
		
		PdfPTable pdfPEmptyTable = new PdfPTable( 1);
       PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
       pdfAPI.disableBorders( pdfPEmptyCell3);
       pdfPEmptyTable.addCell( pdfPEmptyCell3);
       document.add( pdfPEmptyTable);
       
       float[] decFloat = { 0.8f, 0.2f };
       
       PdfPTable pdfDecisionTable = new PdfPTable( decFloat);
       PdfPCell pdfPDecisionCell = new PdfPCell(
               pdfAPI.addParagraph("Original Decision Details", FontFactory.HELVETICA, 12, Font.BOLD, MetlifeInstitutionalConstants.COLOR_WHITE));      
       
       /*pdfPDecisionCell.setBorderWidth( 1);*/
       pdfPDecisionCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
       pdfAPI.disableBordersTop( pdfPDecisionCell);
       pdfPDecisionCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
       pdfPDecisionCell.setPaddingBottom( 6);
       pdfPDecisionCell.setVerticalAlignment( Element.ALIGN_CENTER);
       pdfDecisionTable.addCell( pdfPDecisionCell);
       pdfPDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "", MetlifeInstitutionalConstants.COLOR_WHITE, 10));
       pdfAPI.disableBorders( pdfPDecisionCell);
       pdfDecisionTable.addCell( pdfPDecisionCell);
       document.add( pdfDecisionTable);
       
       pdfPEmptyTable = new PdfPTable( 1);
       pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
       pdfAPI.disableBorders( pdfPEmptyCell3);
       pdfPEmptyTable.addCell( pdfPEmptyCell3);
       document.add( pdfPEmptyTable);
		
		float[] f1 = { 0.2f, 0.2f, 0.4f };
       PdfPTable pdfDecisionTable1 = new PdfPTable( f1);
       
       PdfPCell pdfDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "Decision Type", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
       pdfAPI.disableBorders( pdfDecisionCell);
       pdfDecisionTable1.addCell( pdfDecisionCell);
       pdfDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "Product", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
       pdfAPI.disableBorders( pdfDecisionCell);
       pdfDecisionTable1.addCell( pdfDecisionCell);
       pdfDecisionCell = new PdfPCell(
               pdfAPI.addParagraph( "Decision", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
       pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_MIDDLE);
       pdfAPI.disableBorders( pdfDecisionCell);
       pdfDecisionTable1.addCell( pdfDecisionCell);
       document.add( pdfDecisionTable1);
       
       PdfPTable pdfSingleSpaceTable = new PdfPTable( 1);
       PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
       pdfAPI.disableBorders( pdfPSingleSpaceCell);
       pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
       document.add( pdfSingleSpaceTable);
		if (null!=responseObject
				&& null!=responseObject.getClientData()
               && null != responseObject.getClientData().getListInsured()) {
           for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
               insured =  responseObject.getClientData().getListInsured().get( itr);
               	dispalyINGRuleDecisionDetails( insured.getListRuleDec(), pdfAPI, document,pdfObject);               
           
               	
               	if("Institutional".equalsIgnoreCase(pdfObject.getLob())){
               		/*if(null!=insured.getListGenderAgeBMIDec() && insured.getListGenderAgeBMIDec().size()>0){*/
               		if(null!=insured.getListGenderAgeBMIDec() && !(insured.getListGenderAgeBMIDec().isEmpty())){
                   		dispalyINGGenderAgeBMIDetails(insured.getListGenderAgeBMIDec(), pdfAPI, document,pdfObject);
                   	}            
               	}else{
               		/*if(null!=insured.getListGenderAgeBMIRangeTable() && insured.getListGenderAgeBMIRangeTable().size()>0){*/
               		if(null!=insured.getListGenderAgeBMIRangeTable() && !(insured.getListGenderAgeBMIRangeTable().isEmpty())){
               			dispalyINGGenderAgeBMIDetails( insured.getListGenderAgeBMIRangeTable(), pdfAPI, document,pdfObject);
                   	}            	
               	}
               	
               	
               
               dispalyDependentRiskDetails(insured.getListDepRiskDec(),pdfAPI, document,pdfObject);
               
           }
           
       }
		

       log.info("Display ING rule decision finish");
	}
	
	
	
	 public static void dispalyDependentRiskDetails(List dependentRiskDetails,
	            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {
		 log.info("Display dependent risk details start");
	       
	        PdfPTable ruleTable = null;
            PdfPCell ruleCell = null;
            PdfPCell ruleSubCell = null;
            Decision decision = null;
            PdfPTable ruleSubTable = null;
	       /* if (null != dependentRiskDetails && dependentRiskDetails.size()>0) {*/
            if (null != dependentRiskDetails && !(dependentRiskDetails.isEmpty())) {
	            
	            float[] ruleFloat = { 0.12f, 0.68f };
	            ruleTable = new PdfPTable( ruleFloat);
	            ruleCell = new PdfPCell(
	                    pdfAPI.addParagraph( "Dependent risks", new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( ruleCell);
	            ruleTable.addCell( ruleCell);
	            ruleSubCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
	            
	            for (int ruleSubItr = 0; ruleSubItr < dependentRiskDetails.size(); ruleSubItr++) {
	                decision = (Decision) dependentRiskDetails.get( ruleSubItr);
	                float[] ruleSubFloat = { 0.28f, 0.4f };
	                ruleSubTable = new PdfPTable( ruleSubFloat);
	                PdfPCell ruleTableSubCell = null;
	                if (null != decision.getProductName()
	                        && !"".equalsIgnoreCase( decision.getProductName())
	                        && decision.getProductName().length() > 1) {
	                	
	                	 ruleTableSubCell = new PdfPCell(
	                             pdfAPI.addParagraph(decision.getProductName(), new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
	                    
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    StringBuilder strBuff = new StringBuilder();
	                    strBuff.append( decision.getDecision());
	                    
	                    if (null != decision.getTotalDebitsValue()
	                            && decision.getTotalDebitsValue().length() > 0
	                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
	                   	 strBuff.append(MetlifeInstitutionalConstants.LOADING);
	                        strBuff.append(decision.getTotalDebitsValue());
	                        strBuff.append( ", ");
	                        strBuff.append( ");\n");
	                    }
	                    
	                    
	                    if (null != decision.getReasons()
	                            && decision.getReasons().length > 0) {
	                        strBuff.append( "Reason: (");
	                        String[] reason = decision.getReasons();
	                        for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
	                            strBuff.append( reason[reasonItr]);
	                            strBuff.append( ",");
	                        }
	                        strBuff.append( ")");
	                        
	                    }
	                    
	                    ruleTableSubCell = new PdfPCell(
	                            pdfAPI.addParagraph( strBuff.toString(), new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    ruleSubCell.addElement( ruleSubTable);
	                    
	                    
	                } 
	            }
	            
	            /*for (int ruleSubItr = 0; ruleSubItr < dependentRiskDetails.size(); ruleSubItr++) {
	                decision = (Decision) dependentRiskDetails.get( ruleSubItr);
	                float[] ruleSubFloat = { 0.28f, 0.4f };
	                ruleSubTable = new PdfPTable( ruleSubFloat);
	                
	                if (null != decision.getProductName()
	                        && !"".equalsIgnoreCase( decision.getProductName())
	                        && decision.getProductName().length() > 1) {
	                	
	                	 ruleTableSubCell = new PdfPCell(
	                             pdfAPI.addParagraph(decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	                    
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    
	                    strBuff.append( decision.getDecision());
	                    
	                    if (null != decision.getTotalDebitsValue()
	                            && decision.getTotalDebitsValue().length() > 0
	                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
	                   	 strBuff.append( MetlifeInstitutionalConstants.LOADING);
	                        strBuff.append(decision.getTotalDebitsValue());
	                        strBuff.append( ", ");
	                        strBuff.append( ");\n");
	                    }
	                    
	                    
	                    if (null != decision.getReasons()
	                            && decision.getReasons().length > 0) {
	                        strBuff.append( "Reason: (");
	                        String reason[] = decision.getReasons();
	                        for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
	                            strBuff.append( reason[reasonItr]);
	                            strBuff.append( ",");
	                        }
	                        strBuff.append( ")");
	                        
	                    }
	                    
	                    ruleTableSubCell = new PdfPCell(
	                            pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	                    pdfAPI.disableBorders( ruleTableSubCell);
	                    ruleSubTable.addCell( ruleTableSubCell);
	                    ruleSubCell.addElement( ruleSubTable);
	                    strBuff = new StringBuffer();
	                    
	                } 
	            }*/
	            pdfAPI.disableBorders( ruleSubCell);
	            ruleTable.addCell( ruleSubCell);
	            document.add( ruleTable);
	            
	        }
            log.info("Display dependent risk details finish");
	        
	    }

	
	/**
     * Description: display GenderAgeBMI decision details in the PDF Section
     * 
     * @param
     * java.lang.String,java.lang.String,,java.lang.String,,java.lang.String,
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    public static void dispalyGenderAgeBMIDetails(List genderAgeBMIListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {
    	 log.info("Display gender age BMI detail start");
       
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        Decision decision = null;
        PdfPTable ruleSubTable = null;
        Rating rating = null;
        Exclusion exclusion = null;
        PdfPCell ruleSubCell = null;
        if (null != genderAgeBMIListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
            ruleTable = new PdfPTable( ruleFloat);
            ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Gender/Age/BMI", new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
             ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
            
             
             for (int ruleSubItr = 0; ruleSubItr < genderAgeBMIListDecision.size(); ruleSubItr++) {
                 decision = (Decision) genderAgeBMIListDecision.get( ruleSubItr);
                 float[] ruleSubFloat = { 0.28f, 0.4f };
                 ruleSubTable = new PdfPTable( ruleSubFloat);
                 PdfPCell ruleTableSubCell = null;
                 if (null != decision.getProductName()
                         && !"".equalsIgnoreCase( decision.getProductName())
                         && decision.getProductName().length() > 1) {
                 	
                 	 ruleTableSubCell = new PdfPCell(
                              pdfAPI.addParagraph( decision.getProductName(), new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
                 	 
                 	  pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       StringBuilder strBuff = new StringBuilder();
                       strBuff.append( decision.getDecision());
                       
                       
                       /*if(null != decision.getListRating() && decision.getListRating().size()>0){*/
                       if(null != decision.getListRating() && !(decision.getListRating().isEmpty())){
                     	  for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                               rating =  decision.getListRating().get( ratingItr);
                               strBuff.append( MetlifeInstitutionalConstants.LOADING);                              
                               strBuff.append( rating.getValue());
                               strBuff.append( ", ");
                               strBuff.append( ");\n");
                           }
                             
                       }
                       
                       /*if (null != decision.getTotalDebitsValue()
                               && decision.getTotalDebitsValue().length() > 0
                               && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
                      	 strBuff.append( MetlifeInstitutionalConstants.LOADING);
                           strBuff.append(decision.getTotalDebitsValue());
                           strBuff.append( ", ");
                           strBuff.append( ");\n");
                       }
                       */
                       /*if (null != decision.getListExclusion() && decision.getListExclusion().size() > 0) {*/
                       if (null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) {
                       		strBuff.append( MetlifeInstitutionalConstants.EXCLUSION);
                          
                           for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                               exclusion =  decision.getListExclusion().get( itr);
                               strBuff.append( exclusion.getName());
                               strBuff.append(", ");
                           }
                           strBuff.append(")");
                           
                       }
                       ruleTableSubCell = new PdfPCell(
                               pdfAPI.addParagraph( strBuff.toString(), new Font(pdfAPI.getSwsLig(),8,0, pdfAPI.getFont_color())));
                       pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       ruleSubCell.addElement( ruleSubTable);
                       
                 } 
             }
             
           /* for (int ruleSubItr = 0; ruleSubItr < genderAgeBMIListDecision.size(); ruleSubItr++) {
                decision = (Decision) genderAgeBMIListDecision.get( ruleSubItr);
                
                
                float[] ruleSubFloat = { 0.28f, 0.4f };
                ruleSubTable = new PdfPTable( ruleSubFloat);                
                if (null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName())
                        && decision.getProductName().length() > 0) {
                	 ruleTableSubCell = new PdfPCell(
                             pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                } else {
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                }
                pdfAPI.disableBorders( ruleTableSubCell);
                ruleSubTable.addCell( ruleTableSubCell);               
                strBuff.append( decision.getDecision());
                for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                    rating = (Rating) decision.getListRating().get( ratingItr);
                    strBuff.append( "with rating");
                    strBuff.append( rating.getValue());
                    strBuff.append( ",");
                }
                if (null != decision.getListExclusion()) {
                    if (decision.getListExclusion().size() > 0) {
                        strBuff.append( "Exclusion:");
                    }
                    for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                        exclusion = (Exclusion) decision.getListExclusion().get( itr);
                        strBuff.append( exclusion.getText());
                    }
                    
                }
                ruleTableSubCell = new PdfPCell(
                        pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                pdfAPI.disableBorders( ruleTableSubCell);
                ruleSubTable.addCell( ruleTableSubCell);
                ruleSubCell.addElement( ruleSubTable);
                strBuff = new StringBuffer();
            }*/
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }

        log.info("Display gender age BMI detail finish");
        
    }
    
    public static void dispalyINGGenderAgeBMIDetails(List genderAgeBMIListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {
    	 log.info("Display ING gender age BMI detail start");
      
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        Decision decision = null;
        PdfPTable ruleSubTable = null;
        Rating rating = null;
        Exclusion exclusion = null;
        PdfPCell ruleSubCell = null;
        if (null != genderAgeBMIListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
            ruleTable = new PdfPTable( ruleFloat);
            ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Gender/Age/BMI", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
             ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            
             
             for (int ruleSubItr = 0; ruleSubItr < genderAgeBMIListDecision.size(); ruleSubItr++) {
                 decision = (Decision) genderAgeBMIListDecision.get( ruleSubItr);
                 float[] ruleSubFloat = { 0.28f, 0.4f };
                 ruleSubTable = new PdfPTable( ruleSubFloat);
                 PdfPCell ruleTableSubCell = null;
                 if (null != decision.getProductName()
                         && !"".equalsIgnoreCase( decision.getProductName())
                         && decision.getProductName().length() > 1) {
                 	
                 	 ruleTableSubCell = new PdfPCell(
                              pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                 	 
                 	  pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       StringBuilder strBuff = new StringBuilder();
                       strBuff.append( decision.getDecision());
                       
                       
                       /*if(null != decision.getListRating() && decision.getListRating().size()>0){*/
                       if(null != decision.getListRating() && !(decision.getListRating().isEmpty())){
                     	  for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                               rating =  decision.getListRating().get( ratingItr);
                               strBuff.append( MetlifeInstitutionalConstants.LOADING);                              
                               strBuff.append( rating.getValue());
                               strBuff.append( ", ");
                               strBuff.append( ");\n");
                           }
                             
                       }                      
                       /*if (null != decision.getListExclusion() && decision.getListExclusion().size() > 0) {*/
                       if (null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) {
                          
                       		strBuff.append( MetlifeInstitutionalConstants.EXCLUSION);
                          
                           for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                               exclusion =  decision.getListExclusion().get( itr);
                               strBuff.append( exclusion.getName());
                               strBuff.append(", ");
                           }
                           strBuff.append(")");
                           
                       }
                       ruleTableSubCell = new PdfPCell(
                               pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                       pdfAPI.disableBorders( ruleTableSubCell);
                       ruleSubTable.addCell( ruleTableSubCell);
                       ruleSubCell.addElement( ruleSubTable);
                       
                 } 
             }
             
         
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }

        log.info("Display ING gender age BMI detail finish");
        
    }
    
	/**
     * Description: display rule decision details in the PDF Section
     * 
     * @param
     * java.lang.String,java.lang.String,,java.lang.String,,java.lang.String,
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    public static void dispalyRuleDecisionDetails(List ruleListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {
    	log.info("Display rule decision details start");
      
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        PdfPCell ruleSubCell = null;
        Decision decision = null;
        PdfPTable ruleSubTable = null;
       /* Rating rating = null;*/
        Exclusion exclusion = null;
        if (null != ruleListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
             ruleTable = new PdfPTable( ruleFloat);
             ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Rules", new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
            ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
            
            
            
            for (int ruleSubItr = 0; ruleSubItr < ruleListDecision.size(); ruleSubItr++) {
                decision = (Decision) ruleListDecision.get( ruleSubItr);
                float[] ruleSubFloat = { 0.28f, 0.4f };

                if (null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName().trim())
                        && decision.getProductName().length() > 0) {
                	
                	ruleSubTable = new PdfPTable( ruleSubFloat);
                	PdfPCell ruleTableSubCell = null;
                    
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( decision.getProductName(), new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
                    
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    StringBuilder strBuff = new StringBuilder();
                    strBuff.append( decision.getDecision());
                    if (null != decision.getTotalDebitsValue()
                            && decision.getTotalDebitsValue().length() > 0
                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
                   	 strBuff.append( MetlifeInstitutionalConstants.LOADING);
                        strBuff.append(decision.getTotalDebitsValue());
                        strBuff.append( ", ");
                        strBuff.append( ");\n");
                    }
                    
                    /*if (null != decision.getListExclusion() && decision.getListExclusion().size()>0) {
                        if (decision.getListExclusion().size() > 0) {
                       	  strBuff.append( MetlifeInstitutionalConstants.EXCLUSION);
                        }*/
                    if (null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) {
                        strBuff.append( MetlifeInstitutionalConstants.EXCLUSION);
                        for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                           exclusion =  decision.getListExclusion().get( itr);
                            strBuff.append( exclusion.getName());
                            strBuff.append(", ");
                        }
                        strBuff.append(")");
                        
                    }
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph(strBuff.toString(), new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
                    /*ruleTableSubCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);*/
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    ruleSubCell.addElement( ruleSubTable);
                  
                } 
                	
              
              
                
            }
            
           /* for (int ruleSubItr = 0; ruleSubItr < ruleListDecision.size(); ruleSubItr++) {
                decision = (Decision) ruleListDecision.get( ruleSubItr);
                
                if(null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName())
                        && decision.getProductName().length()> 0){
                    float[] ruleSubFloat = { 0.28f, 0.4f };
                    ruleSubTable = new PdfPTable( ruleSubFloat);                                      
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));                    
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                  
                    strBuff.append( decision.getDecision());
                    for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                        rating = (Rating) decision.getListRating().get( ratingItr);
                        strBuff.append( "with rating");
                        strBuff.append( rating.getValue());
                        strBuff.append( ",");
                    }
                    if (null != decision.getListExclusion()) {
                        if (decision.getListExclusion().size() > 0) {
                            strBuff.append( "Exclusion:");
                        }
                        
                        for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                            exclusion = (Exclusion) decision.getListExclusion().get( itr);
                            strBuff.append( exclusion.getText());
                        }
                        
                    }
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                     ruleTableSubCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    ruleSubCell.addElement( ruleSubTable);
                    strBuff = new StringBuffer();
                }
    
                
            }*/
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }

         log.info("Display rule decision details finish");
        
    }
    
    public static void dispalyINGRuleDecisionDetails(List ruleListDecision,
            CostumPdfAPI pdfAPI, Document document, PDFObject pdfObject) throws DocumentException {
    	log.info("Display ING rule decision details start");
        
        PdfPTable ruleTable = null;
        PdfPCell ruleCell = null;
        PdfPCell ruleSubCell = null;
        Decision decision = null;
        PdfPTable ruleSubTable = null;
        /*Rating rating = null;*/
        Exclusion exclusion = null;
        if (null != ruleListDecision) {
            
            float[] ruleFloat = { 0.12f, 0.68f };
             ruleTable = new PdfPTable( ruleFloat);
             ruleCell = new PdfPCell(
                    pdfAPI.addParagraph( "Rules", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( ruleCell);
            ruleTable.addCell( ruleCell);
            ruleSubCell = new PdfPCell(
                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
            
            
            
            for (int ruleSubItr = 0; ruleSubItr < ruleListDecision.size(); ruleSubItr++) {
                decision = (Decision) ruleListDecision.get( ruleSubItr);
                float[] ruleSubFloat = { 0.28f, 0.4f };

                if (null != decision.getProductName()
                        && !"".equalsIgnoreCase( decision.getProductName().trim())
                        && decision.getProductName().length() > 0) {
                	
                	ruleSubTable = new PdfPTable( ruleSubFloat);
                	PdfPCell ruleTableSubCell = null;
                    
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph( decision.getProductName(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    StringBuilder strBuff = new StringBuilder();
                    strBuff.append( decision.getDecision());
                    if (null != decision.getTotalDebitsValue()
                            && decision.getTotalDebitsValue().length() > 0
                            && !decision.getTotalDebitsValue().equalsIgnoreCase( "0.0")) {
                   	 strBuff.append( MetlifeInstitutionalConstants.LOADING);
                        strBuff.append(decision.getTotalDebitsValue());
                        strBuff.append( ", ");
                        strBuff.append( ");\n");
                    }
                    
                    /*if (null != decision.getListExclusion() && decision.getListExclusion().size()>0) {*/
                    if (null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) {
                        /*if (decision.getListExclusion().size() > 0) {
                       	  strBuff.append( MetlifeInstitutionalConstants.EXCLUSION);
                        }*/
                        strBuff.append( MetlifeInstitutionalConstants.EXCLUSION);
                        for (int itr = 0; itr < decision.getListExclusion().size(); itr++) {
                           exclusion =  decision.getListExclusion().get( itr);
                            strBuff.append( exclusion.getName());
                            strBuff.append(", ");
                        }
                        strBuff.append(")");
                        
                    }
                    ruleTableSubCell = new PdfPCell(
                            pdfAPI.addParagraph(strBuff.toString(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
                    /*ruleTableSubCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);*/
                    pdfAPI.disableBorders( ruleTableSubCell);
                    ruleSubTable.addCell( ruleTableSubCell);
                    ruleSubCell.addElement( ruleSubTable);
                  
                }              
              
                
            }            
          
            pdfAPI.disableBorders( ruleSubCell);
            ruleTable.addCell( ruleSubCell);
            document.add( ruleTable);
            
        }
         
         log.info("Display ING rule decision details finish");
    }
    
    public static BigDecimal convertToBigDecimal(String bdValue){
    	log.info("Convert to Big decimal start");
    	BigDecimal returnBD=null;
    	try{
		   if(bdValue != null && !bdValue.trim().equalsIgnoreCase("")){
			   returnBD=new BigDecimal(bdValue);
		   }
    	}catch(Exception e){
    		log.error("Error in convert to big decimal: {}",e);
    	}
    	log.info("Convert to Big decimal finish");
    	return returnBD;
    }
    
    /**
     * Description: display declaration details in the PDF Section
     * 
     * @param com.metlife.eapplication.dto.ApplicationDTO
     *            com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    
    public static void declarationDetails(List disclosureStatementList,
            CostumPdfAPI pdfAPI, Document document) throws DocumentException {
    	log.info("Declaration details start");

        
       
        PdfPTable pdfPDeclarationTable = null;
        DisclosureStatement disclosureStatement = null;
        PdfPCell pdfPDeclarationCell = null;
        PdfPTable pdfPTable = null;
        PdfPTable pdfPTableAck = null;
        PdfPTable pdfPTableStatement = null;
        PdfPCell pdfPCell = null;
        PdfPCell pdfPCell1 = null;
        PdfPCell pdfPCelAck = null;
        
        pdfPTable = new PdfPTable( 1);
        pdfPCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPCell);
        pdfPCell.setPadding( 4);
        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
        pdfPTable.addCell( pdfPCell);
        document.add( pdfPTable);
        
        /*if ( null != disclosureStatementList && disclosureStatementList.size()>0 && !disclosureStatementList.isEmpty()) {*/
        if ( null != disclosureStatementList && !(disclosureStatementList.isEmpty())) {
            pdfPDeclarationTable = new PdfPTable( 1);
            pdfPDeclarationCell = new PdfPCell(
                    pdfAPI.addParagraph( "Declaration", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
            pdfPDeclarationCell.setBorderWidth( 1);
            /*pdfPDeclarationCell.setBorderColor( pdfAPI.getColor( sectionBackground));*/
            pdfAPI.disableBorders( pdfPDeclarationCell);
            pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
            pdfPDeclarationCell.setPaddingBottom( 6);
            pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
            pdfPDeclarationTable.addCell( pdfPDeclarationCell);
            document.add( pdfPDeclarationTable);
            
            for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
                disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
              /*  pdfPTable = new PdfPTable( 1);
                pdfPCell = new PdfPCell(
                        pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, FontFactory.HELVETICA, 8, Font.NORMAL, colorStyle));
                pdfAPI.disableBorders( pdfPCell);
                pdfPCell.setPadding( 4);
                pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTable.addCell( pdfPCell);
                document.add( pdfPTable);*/
                
                float[] f = { .9f, 0.1f };               
                if (null != disclosureStatement
                        && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
                    pdfPTableStatement = new PdfPTable( 1);
                    pdfPCell1 = new PdfPCell(
                            pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfPCell1);
                    pdfPCell1.setPadding( 4);
                    pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
                    pdfPTableStatement.addCell( pdfPCell1);
                    document.add( pdfPTableStatement);
                    
                    pdfPTableAck = new PdfPTable( f);
                    
                   if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
                	   pdfPCelAck = new PdfPCell(
                               pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                   }else if(null!=disclosureStatement.getStatement() && (disclosureStatement.getStatement().contains("I/") || disclosureStatement.getStatement().contains("I /"))){
                    	pdfPCelAck = new PdfPCell(
                                pdfAPI.addParagraph( "I/We acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                        
                    }else{
                    	pdfPCelAck = new PdfPCell(
                                pdfAPI.addParagraph( "I acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                        
                    }                  
                    
                    
                    
                    pdfAPI.disableBorders( pdfPCelAck);
                    pdfPCelAck.setPadding( 4);
                    pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                    pdfPTableAck.addCell( pdfPCelAck);
                    pdfPCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfPCell1);
                    pdfPCell1.setPadding( 4);
                    pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                    pdfPTableAck.addCell( pdfPCell1);
                    document.add( pdfPTableAck);
                }
                
            }
        }

        log.info("Declaration details finish");
        
    }
    
    public static void iNGDdeclarationDetails(List disclosureStatementList,
            CostumPdfAPI pdfAPI, Document document) throws DocumentException {
	log.info("Declaration details start");
    PdfPTable pdfPDeclarationTable = null;
    DisclosureStatement disclosureStatement = null;
    PdfPCell pdfPDeclarationCell = null;
    PdfPTable pdfPTable = null;
    PdfPTable pdfPTableAck = null;
    PdfPTable pdfPTableStatement = null;
    PdfPCell pdfPCell = null;
    PdfPCell pdfPCell1 = null;
    PdfPCell pdfPCelAck = null;
    
    pdfPTable = new PdfPTable( 1);
    pdfPCell = new PdfPCell(
            pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
    pdfAPI.disableBorders( pdfPCell);
    pdfPCell.setPadding( 4);
    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
    pdfPTable.addCell( pdfPCell);
    document.add( pdfPTable);
    if ( null != disclosureStatementList && !(disclosureStatementList.isEmpty())) {
        pdfPDeclarationTable = new PdfPTable( 1);
        pdfPDeclarationCell = new PdfPCell(
                pdfAPI.addParagraph( "Declaration", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
        pdfPDeclarationCell.setBorderWidth( 1);
        pdfAPI.disableBorders( pdfPDeclarationCell);
        pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeader_color());
        pdfPDeclarationCell.setPaddingBottom( 6);
        pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPDeclarationTable.addCell( pdfPDeclarationCell);
        document.add( pdfPDeclarationTable);
        
        for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
            disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
            float[] f = { .9f, 0.1f };               
            if (null != disclosureStatement
                    && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
                pdfPTableStatement = new PdfPTable( 1);
                pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableStatement.addCell( pdfPCell1);
                document.add( pdfPTableStatement);
                
                pdfPTableAck = new PdfPTable( f);
                
               if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
            	   pdfPCelAck = new PdfPCell(
                           pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
               }else if(null!=disclosureStatement.getStatement() && (disclosureStatement.getStatement().contains("I/") || disclosureStatement.getStatement().contains("I /"))){
                	pdfPCelAck = new PdfPCell(
                            pdfAPI.addParagraph( "I/We acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    
                }else{
                	pdfPCelAck = new PdfPCell(
                            pdfAPI.addParagraph( "I acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    
                }                  
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
            }
            
        }
    }

    log.info("Declaration details finish");
    
}
    public static void iNGDgeneraldeclarationDetails(List disclosureStatementList,
            CostumPdfAPI pdfAPI, Document document) throws DocumentException {
	log.info("Declaration details start");
    PdfPTable pdfPDeclarationTable = null;
    DisclosureStatement disclosureStatement = null;
    PdfPCell pdfPDeclarationCell = null;
    PdfPTable pdfPTable = null;
    PdfPTable pdfPTableAck = null;
    PdfPTable pdfPTableStatement = null;
    PdfPCell pdfPCell = null;
    PdfPCell pdfPCell1 = null;
    PdfPCell pdfPCelAck = null;
    
    pdfPTable = new PdfPTable( 1);
    pdfPCell = new PdfPCell(
            pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
    pdfAPI.disableBorders( pdfPCell);
    pdfPCell.setPadding( 4);
    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
    pdfPTable.addCell( pdfPCell);
    document.add( pdfPTable);
    if ( null != disclosureStatementList && !(disclosureStatementList.isEmpty())) {
        pdfPDeclarationTable = new PdfPTable( 1);
        pdfPDeclarationCell = new PdfPCell(
                pdfAPI.addParagraph( "Declaration", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
        pdfPDeclarationCell.setBorderWidth( 1);
        pdfAPI.disableBorders( pdfPDeclarationCell);
        pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeader_color());
        pdfPDeclarationCell.setPaddingBottom( 6);
        pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPDeclarationTable.addCell( pdfPDeclarationCell);
        document.add( pdfPDeclarationTable);
        
        for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
            disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
            float[] f = { .9f, 0.1f };               
            if (null != disclosureStatement
                    && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
                pdfPTableStatement = new PdfPTable( 1);
                pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableStatement.addCell( pdfPCell1);
                document.add( pdfPTableStatement);
                
                pdfPTableAck = new PdfPTable( f);
                
               if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
            	   pdfPCelAck = new PdfPCell(
                           pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
               }else if(null!=disclosureStatement.getStatement() && (disclosureStatement.getStatement().contains("I/") || disclosureStatement.getStatement().contains("I /"))){
                	pdfPCelAck = new PdfPCell(
                            pdfAPI.addParagraph( "I/We acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    
                }else{
                	pdfPCelAck = new PdfPCell(
                            pdfAPI.addParagraph( "I acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    
                }                  
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
            }
            
        }
    }

    log.info("Declaration details finish");
    
}
    
 //Added for INGD
    public static void iNGDtransferevidenceDetails(List disclosureStatementList,
            CostumPdfAPI pdfAPI, Document document) throws DocumentException {
	log.info("Evidence details start");
    PdfPTable pdfPDeclarationTable = null;
    DisclosureStatement disclosureStatement = null;
    PdfPCell pdfPDeclarationCell = null;
    PdfPTable pdfPTable = null;
    PdfPTable pdfPTableAck = null;
    PdfPTable pdfPTableStatement = null;
    PdfPCell pdfPCell = null;
    PdfPCell pdfPCell1 = null;
    PdfPCell pdfPCelAck = null;
    
    pdfPTable = new PdfPTable( 1);
    pdfPCell = new PdfPCell(
            pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
    pdfAPI.disableBorders( pdfPCell);
    pdfPCell.setPadding( 4);
    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
    pdfPTable.addCell( pdfPCell);
    document.add( pdfPTable);
    if ( null != disclosureStatementList && !(disclosureStatementList.isEmpty())) {
        pdfPDeclarationTable = new PdfPTable( 1);
        pdfPDeclarationCell = new PdfPCell(
                pdfAPI.addParagraph( "Evidence of Existing Cover", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
        pdfPDeclarationCell.setBorderWidth( 1);
        pdfAPI.disableBorders( pdfPDeclarationCell);
        pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeader_color());
        pdfPDeclarationCell.setPaddingBottom( 6);
        pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPDeclarationTable.addCell( pdfPDeclarationCell);
        document.add( pdfPDeclarationTable);
        
        for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
            disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
            float[] f = { .9f, 0.1f };               
            if (null != disclosureStatement
                    && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
                pdfPTableStatement = new PdfPTable( 1);
                pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableStatement.addCell( pdfPCell1);
                document.add( pdfPTableStatement);
                
                pdfPTableAck = new PdfPTable( f);
                
               if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
            	   pdfPCelAck = new PdfPCell(
                           pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
               }else if(null!=disclosureStatement.getStatement() && (disclosureStatement.getStatement().contains("I/") || disclosureStatement.getStatement().contains("I /"))){
                	pdfPCelAck = new PdfPCell(
                            pdfAPI.addParagraph( "I/We acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    
                }else{
                	pdfPCelAck = new PdfPCell(
                            pdfAPI.addParagraph( "I acknowledge that my application will not be reviewed by the insurer until I send the supporting documents to the address below.Postal Address: ING REPLY PAID 4307 SYDNEY NSW 2001.", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    
                }                  
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
            }
            
        }
    }

    log.info("Evidence details finish");
    
}
}
