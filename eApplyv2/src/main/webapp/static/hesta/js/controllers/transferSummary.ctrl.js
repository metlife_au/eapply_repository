/*Transfer Summary Page Controller Starts*/ 
CareSuperApp.controller('transferSummary',['$scope', '$location','$timeout','$window', 'auraInputService','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService',
                         function($scope, $location, $timeout,$window, auraInputService,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;           
    };      	
    
    
    $scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}
     };
    
  // added for session expiry
	   /* $timeout(callAtTimeout, 900000); 
	  	function callAtTimeout() {
	  		$location.path("/sessionTimeOut");
	  	}*/
    /* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
     
     var ackCheckTRDD; 
     var ackCheckTRPP;
     var ackCheckTRGC;
   
     
    $scope.transferCoverDetails=PersistenceService.gettransferCoverDetails();
    console.log($scope.transferCoverDetails);
    
    $scope.transferCoverAuraDetails=PersistenceService.getTransCoverAuraDetails();
    console.log($scope.transferCoverAuraDetails);
    
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    $scope.occupationDetails =PersistenceService.getTransferCoverOccDetails();
  	$scope.deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
	$scope.tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
	$scope.ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();
	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();
    
    $scope.submitTransferCover = function(){
    	
    	ackCheckTRDD = $('#DutyOfDisclosureLabelTR').hasClass('active');
    	ackCheckTRPP = $('#privacyPolicyLabelTR').hasClass('active');
    	ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active'); 
    	
    	if(ackCheckTRDD && ackCheckTRPP && ackCheckTRGC){
    		
    		$scope.TRDDackFlag = false;
    		$scope.TRPPackFlag = false;
    		$scope.TRGCackFlag = false;
    		if($scope.transferCoverDetails != null && $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null &&
    		      $scope.occupationDetails != null && $scope.transferCoverAuraDetails != null && $scope.personalDetails[0] != null){
    			$scope.transferCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.occupationDetails =$scope.occupationDetails;
    			$scope.details.addnlDeathCoverDetails=$scope.deathAddnlCvrDetails;
    			$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlCvrDetails;
    			$scope.details.addnlIpCoverDetails=$scope.ipAddnlCvrDetails;
    			$scope.details.transferDocuments = $scope.uploadedFileDetails;
    			var coverObject = angular.extend($scope.details,$scope.transferCoverDetails);
            	var auraObject = angular.extend(coverObject,$scope.transferCoverAuraDetails);
    			var submitObject = angular.extend(auraObject,$scope.personalDetails[0]);
    			auraResponseService.setResponse(submitObject);
    			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
            		if($scope.transferCoverAuraDetails.overallDecision == 'ACC'){
                		$scope.go('/transferAccept');
                	}else if($scope.transferCoverAuraDetails.overallDecision == 'DCL'){
                		$scope.go('/transferDecline');
                	}
            	});
            }
    	}else{
    		if(ackCheckTRDD){
        		$scope.TRDDackFlag = false;
        	}else{
        		$scope.TRDDackFlag = true;
        	}
        	
        	if(ackCheckTRPP){
        		$scope.TRPPackFlag = false;
        	}else{
        		$scope.TRPPackFlag = true;
        	}
        	
        	if(ackCheckTRGC){
        		$scope.TRGCackFlag = false;
        	}else{
        		$scope.TRGCackFlag = true;
        	}
        	$scope.scrollToUncheckedElement();
    	}	
    };
    
    $scope.scrollToUncheckedElement = function(){
		var elements = [ackCheckTRDD, ackCheckTRPP, ackCheckTRGC];
		var ids = ['DutyOfDisclosureLabelTR', 'privacyPolicyLabelTR', 'generalConsentLabelTR'];
    	for(var k = 0; k < elements.length; k++){
    		if(!elements[k]){
    			$('html, body').animate({
        	        scrollTop: $("#" + ids[k]).offset().top
        	    }, 1000);
    			break;
    		}
    	}
    };
  /*  var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveTransferSummary = function(){
    	$scope.transferSummarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.transferCoverDetails != null && $scope.occupationDetails != null &&  $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null && $scope.transferCoverAuraDetails != null&& $scope.personalDetails[0] != null){
    		$scope.transferCoverDetails.lastSavedOn = 'SummaryTransferPage';
    		$scope.details={};
    		$scope.details.occupationDetails =$scope.occupationDetails;
    		$scope.details.addnlDeathCoverDetails =$scope.deathAddnlCvrDetails;
    		$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlCvrDetails;
    		$scope.details.addnlIpCoverDetails=$scope.ipAddnlCvrDetails;
    		var temp = angular.extend($scope.transferCoverDetails,$scope.details);
    		var aura = angular.extend(temp,$scope.transferCoverAuraDetails);
        	var saveTransferSummaryObject = angular.extend(aura, $scope.personalDetails[0]);
        	auraResponseService.setResponse(saveTransferSummaryObject);
	        saveEapply.reqObj().then(function(response) {  
	                console.log(response.data)
	        });
        }
    };
    
    $scope.transferSummarySaveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};*/
	
	/*$scope.checkAckStateTR = function(){
    	$timeout(function(){
    		ackCheckTRDD = $('#DutyOfDisclosureLabelTR').hasClass('active');
        	ackCheckTRPP = $('#privacyPolicyLabelTR').hasClass('active');
        	ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
        	
        	if(ackCheckTRDD){
        		$scope.TRDDackFlag = false;
        	}else{
        		$scope.TRDDackFlag = true;
        	}
        	
        	if(ackCheckTRPP){
        		$scope.TRPPackFlag = false;
        	}else{
        		$scope.TRPPackFlag = true;
        	}
        	
        	if(ackCheckTRGC){
        		$scope.TRGCackFlag = false;
        	}else{
        		$scope.TRGCackFlag = true;
        	}
    	}, 10);
    };*/
	
	$scope.checkAckStateDDTR = function(){
    	$timeout(function(){
    		ackCheckTRDD = $('#DutyOfDisclosureLabelTR').hasClass('active');
        	if(ackCheckTRDD){
        		$scope.TRDDackFlag = false;
        	}else{
        		$scope.TRDDackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStatePPTR = function(){
    	$timeout(function(){
    		ackCheckTRPP = $('#privacyPolicyLabelTR').hasClass('active');
        	if(ackCheckTRPP){
        		$scope.TRPPackFlag = false;
        	}else{
        		$scope.TRPPackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStateGCTR = function(){
    	$timeout(function(){
    		ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
        	if(ackCheckTRGC){
        		$scope.TRGCackFlag = false;
        	}else{
        		$scope.TRGCackFlag = true;
        	}
    	}, 10);
    };
    
    
    }]); 
/*Transfer Summary Page Controller Ends*/