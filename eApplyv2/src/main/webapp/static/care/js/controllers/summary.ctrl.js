/*Summary Page Controller Starts*/ 
CareSuperApp.controller('summary',['$scope','$rootScope','$location','$timeout','$routeParams','$window','auraInputService','PersistenceService', 'submitEapply','auraResponseService','ngDialog', 'persoanlDetailService','saveEapply','RetrieveAppDetailsService','CalculateDeathService','CalculateTPDService','CalculateIPService','urlService',
                         function($scope,$rootScope,$location, $timeout, $routeParams, $window, auraInputService,PersistenceService,submitEapply,auraResponseService,ngDialog, persoanlDetailService,saveEapply, RetrieveAppDetailsService, CalculateDeathService,CalculateTPDService,CalculateIPService,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.errorOccured = false;
    //$scope.claimNo = claimNo
    $rootScope.$broadcast('enablepointer');
    $scope.go = function (path) {
    	var decidedMode;
    	if($routeParams.mode == 3){
    		decidedMode = 3
    	} else if($routeParams.mode == 1 || $routeParams.mode == 2){
    		decidedMode = 2;
    	}
    	$timeout(function(){
    		$location.path(path + decidedMode);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;           
    };
    $window.scrollTo(0, 0);
    
    
    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
     };
     
  // added for session expiry
	   /* $timeout(callAtTimeout, 900000); 
	  	function callAtTimeout() {
	  		$location.path("/sessionTimeOut");
	  	}*/
     
     /*var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });
     */
     var ackCheckCCGC;
     var ackCheckLE;
     
     
    $scope.changeCoverDetails=PersistenceService.getChangeCoverDetails();
    $scope.changeCoverOccDetails = PersistenceService.getChangeCoverOccDetails();
    $scope.auraDetails = PersistenceService.getChangeCoverAuraDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    $scope.deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	$scope.tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
	$scope.ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
	if($scope.changeCoverDetails!= null){
		/*$scope.auraDisabled = $scope.changeCoverDetails.auraDisabled=='true';*/
		$scope.auraDisabled = $scope.changeCoverDetails.auraDisabled;
	}
	if($scope.auraDisabled == true){
		$scope.auraDetails = null;
	}
	
	// Death loading calculation
	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.deathLoading && $scope.auraDetails.deathLoading > 0){
    	var deathReqObject = {
        		"age": $scope.changeCoverDetails.age,
        		"fundCode": "CARE",
        		"gender": $scope.changeCoverOccDetails.gender,
        		"deathOccCategory": $scope.changeCoverDetails.deathOccCategory,
        		"smoker": false,
        		"deathUnitsCost": null,
        		"premiumFrequency": "Weekly",
        		"manageType": "CCOVER",
        		"deathExistingAmount": $scope.changeCoverDetails.existingDeathAmt,
        		"deathCoverType": $scope.deathAddnlDetails.deathCoverType
        	};
    	if($scope.deathAddnlDetails.deathCoverType == 'DcFixed'){
    		deathReqObject['deathFixedAmount'] = parseInt($scope.deathAddnlDetails.deathInputTextValue)- parseInt($scope.changeCoverDetails.existingDeathAmt);
    	} else if($scope.deathAddnlDetails.deathCoverType == 'DcUnitised'){
    		deathReqObject['deathUnits'] = parseInt($scope.deathAddnlDetails.deathInputTextValue)- parseInt($scope.changeCoverDetails.existingDeathUnits);
    	}
    	
    	CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
    	//CalculateDeathService.calculateAmount({}, deathReqObject, function(res){
    		var deathResponse = res.data[0];
    		$scope.deathLoadingCost = deathResponse.cost;
    		$scope.deathAddnlDetails.deathCoverPremium = parseFloat($scope.deathAddnlDetails.deathCoverPremium) + 
    														((parseFloat($scope.auraDetails.deathLoading)/100) * parseFloat($scope.deathLoadingCost));
    		$scope.changeCoverDetails.totalPremium = $scope.deathAddnlDetails.deathCoverPremium+$scope.tpdAddnlDetails.tpdCoverPremium+$scope.ipAddnlDetails.ipCoverPremium;
    		$scope.deathAddnlDetails['deathLoadingCost']=$scope.deathLoadingCost;
    	}, function(err){
    		console.log('Error occured during calculating loading ' + JSON.stringify(err));
    	});
    }

	// TPD loading calculation
	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.tpdLoading && $scope.auraDetails.tpdLoading > 0){
    	var tpdReqObject = {
        		"age": $scope.changeCoverDetails.age,
        		"fundCode": "CARE",
        		"gender": $scope.changeCoverOccDetails.gender,
        		"tpdOccCategory": $scope.changeCoverDetails.tpdOccCategory,
        		"smoker": false,
        		"tpdUnitsCost": null,
        		"premiumFrequency": "Weekly",
        		"manageType": "CCOVER",
        		"tpdExistingAmount": $scope.changeCoverDetails.existingTpdAmt,
        		"tpdCoverType": $scope.tpdAddnlDetails.tpdCoverType
        	};
    	if($scope.tpdAddnlDetails.tpdCoverType == 'TPDFixed'){
    		tpdReqObject['tpdFixedAmount'] = parseInt($scope.tpdAddnlDetails.tpdInputTextValue)- parseInt($scope.changeCoverDetails.existingTpdAmt);
    	} else if($scope.tpdAddnlDetails.tpdCoverType == 'TPDUnitised'){
    		tpdReqObject['tpdUnits'] = parseInt($scope.tpdAddnlDetails.tpdInputTextValue)- parseInt($scope.changeCoverDetails.existingTPDUnits);
    	}
    	
    	CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,tpdReqObject).then(function(res){
    	//CalculateTPDService.calculateAmount({}, tpdReqObject, function(res){
    		var tpdResponse = res.data[0];
    		$scope.tpdLoadingCost = tpdResponse.cost;
    		$scope.tpdAddnlDetails.tpdCoverPremium = parseFloat($scope.tpdAddnlDetails.tpdCoverPremium) + 
    														((parseFloat($scope.auraDetails.tpdLoading)/100) * parseFloat($scope.tpdLoadingCost));
    		$scope.changeCoverDetails.totalPremium = $scope.deathAddnlDetails.deathCoverPremium+$scope.tpdAddnlDetails.tpdCoverPremium+$scope.ipAddnlDetails.ipCoverPremium;
    		$scope.tpdAddnlDetails['tpdLoadingCost']=$scope.tpdLoadingCost;
    	}, function(err){
    		console.log('Error occured during calculating loading ' + JSON.stringify(err));
    	});
    }
	
	// IP loading calculation
	if($scope.auraDetails != null && $scope.auraDetails.overallDecision == 'ACC' && $scope.auraDetails.ipLoading && $scope.auraDetails.ipLoading > 0){
    	var ipReqObject = {
        		"age": $scope.changeCoverDetails.age,
        		"fundCode": "CARE",
        		"gender": $scope.changeCoverOccDetails.gender,
        		"ipOccCategory": $scope.changeCoverDetails.ipOccCategory,
        		"smoker": false,
        		"ipUnitsCost": null,
        		"premiumFrequency": "Weekly",
        		"manageType": "CCOVER",
        		"ipCoverType": "IpFixed",
        		"ipWaitingPeriod": $scope.ipAddnlDetails.waitingPeriod,
        		"ipBenefitPeriod": $scope.ipAddnlDetails.benefitPeriod,
        		/*"ipUnits":parseInt($scope.ipAddnlDetails.ipInputTextValue)- parseInt($scope.changeCoverDetails.existingIPUnits)*/
        		/*"ipFixedAmount":parseInt($scope.ipAddnlDetails.ipInputTextValue)- parseInt($scope.changeCoverDetails.existingIPAmount)*/
        		// Change ipInputTextValue to ipFixedAmt for GD-75
        		"ipFixedAmount":parseInt($scope.ipAddnlDetails.ipFixedAmt)- parseInt($scope.changeCoverDetails.existingIPAmount)
        	};
    	
    	CalculateIPService.calculateAmount($scope.urlList.calculateIpUrl,ipReqObject).then(function(res){
    	//CalculateIPService.calculateAmount({}, ipReqObject, function(res){
    		var ipResponse = res.data[0];
    		$scope.ipLoadingCost = ipResponse.cost;
    		$scope.ipAddnlDetails.ipCoverPremium = parseFloat($scope.ipAddnlDetails.ipCoverPremium) + 
    														((parseFloat($scope.auraDetails.ipLoading)/100) * parseFloat($scope.ipLoadingCost));
    		$scope.changeCoverDetails.totalPremium = $scope.deathAddnlDetails.deathCoverPremium+$scope.tpdAddnlDetails.tpdCoverPremium+$scope.ipAddnlDetails.ipCoverPremium;
    		$scope.ipAddnlDetails['ipLoadingCost']=$scope.ipLoadingCost;
    	}, function(err){
    		console.log('Error occured during calculating loading ' + JSON.stringify(err));
    	});
    }
	
// Exclusion, remove </br> tag from exclusions
  	
	 if($scope.auraDetails != null && $scope.auraDetails.deathExclusions != null)
		{
		var textArray = $scope.auraDetails.deathExclusions.split('<br><br>');
		$scope.auraDetails.deathExclusionsEdited  = textArray[0]+"<p>"+textArray[1]+"</p>";
     }
	 if($scope.auraDetails != null && $scope.auraDetails.tpdExclusions != null)
	   {
		var textArray = $scope.auraDetails.tpdExclusions.split('<br><br>');
		$scope.auraDetails.tpdExclusionsEdited = textArray[0]+"<p>"+textArray[1]+"</p>";
	   }
	 if($scope.auraDetails != null && $scope.auraDetails.ipExclusions != null)
	  {
		var textArray = $scope.auraDetails.ipExclusions.split('<br><br>');
		$scope.auraDetails.ipExclusionsEdited= textArray[0]+"<p>"+textArray[1]+"</p>";
	  }
	 
	
    $scope.navigateToDecision = function(){
	    ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
	
	    if($scope.auraDetails != null){
			 if($scope.auraDetails.specialTerm != null && $scope.auraDetails.specialTerm == true){
				ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');	
		     }
	    }
   
    	if(ackCheckCCGC && (($scope.auraDetails != null && !$scope.auraDetails.specialTerm) || ($scope.auraDetails != null && $scope.auraDetails.specialTerm && ackCheckLE) || $scope.auraDetails==null)){
    		$scope.LEFlag = false;
    		$scope.CCGCackFlag = false;
    		if($scope.changeCoverDetails != null && $scope.changeCoverOccDetails != null && $scope.deathAddnlDetails != null && 
    				$scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null  && $scope.personalDetails[0] != null){
    			$rootScope.$broadcast('disablepointer');
    			$scope.changeCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
    			
    			var temp = angular.extend($scope.details,$scope.changeCoverDetails);
    			var submitObject = null;
    			if($scope.auraDetails != null){
    				var aura = angular.extend(temp,$scope.auraDetails);
    				submitObject = angular.extend(aura, $scope.personalDetails[0]);
    			}else{
    				submitObject = angular.extend(temp, $scope.personalDetails[0]);
    			}			
    			
    			
    			auraResponseService.setResponse(submitObject);
    			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
            		if($scope.auraDetails!=null){
            			if($scope.auraDetails.overallDecision == 'ACC'){
                    		if($scope.auraDetails.specialTerm && !($scope.auraDetails.deathDecision=='DCL' || $scope.auraDetails.tpdDecision=='DCL' || $scope.auraDetails.ipDecision=='DCL')){
                    			$location.path('/changeaspcltermsacc');
                    		}else if($scope.auraDisabled){
                    			$location.path('/changecovernochange');                    			
                    		}else{
                    			if($scope.auraDetails.overallDecision!='RUW' && ($scope.auraDetails.deathDecision=='DCL' || $scope.auraDetails.tpdDecision=='DCL' || $scope.auraDetails.ipDecision=='DCL')){
                    				$location.path('/changemixedaccept');
                    			}else{
                    				$location.path('/changeaccept');
                    			}                    			
                    		}   		
                    	} else if($scope.auraDetails.overallDecision == 'DCL'){
                    		$location.path('/changedecline');
                    	} else if($scope.auraDetails.overallDecision == 'RUW'){
                    		$location.path('/changeunderwriting');
                    	}else {
                    		$location.path('/changemixedaccept');
                    	}
            		}else{
            			/*$location.path('/changeaccept');*/
            			if($scope.auraDisabled){
                			$location.path('/changecovernochange');                    			
                		}else{                    		
                			$location.path('/changeaccept');
                		} 
            		}
            		
            	}, function(err){
            		//$scope.errorOccured = true;
                    $window.scrollTo(0, 0);
                    $rootScope.$broadcast('enablepointer');
                    throw err;
            	});
            }
    	} else{
    		//$scope.ackFlag = true;
        	
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
        	if(ackCheckLE){
        		$scope.LEFlag = false;
        	}else{
        		$scope.LEFlag = true;
        	}
        	$scope.scrollToUncheckedElement();
    	}
    };
    
    $scope.scrollToUncheckedElement = function(){
    	if($scope.auraDetails!=null && $scope.auraDetails.specialTerm){
    		var elements = [ackCheckLE,ackCheckCCGC];
    		var ids = ['lodadingExclusionLabel','generalConsentLabel'];
    	} else{
    		var elements = [ackCheckCCGC];
    		var ids = ['generalConsentLabel'];
    	}
    	/*for(var k = 0; k < elements.length; k++){
    		if(!elements[k]){
    			$('html, body').animate({
        	        scrollTop: $("#" + ids[k]).offset().top
        	    }, 1000);
    			break;
    		}
    	}*/
    };
    
    var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveSummary = function(){
    	$scope.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.changeCoverDetails != null && $scope.changeCoverOccDetails != null && $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null &&  $scope.personalDetails[0] != null){
    		$scope.changeCoverDetails.lastSavedOn = 'SummaryPage';
    		$scope.details={};
    		$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
    		var temp = angular.extend( $scope.details,$scope.changeCoverDetails);
    		var saveSummaryObject = null;
    		if($scope.auraDetails!=null){
    			var aura = angular.extend(temp,$scope.auraDetails);
    			saveSummaryObject = angular.extend(aura, $scope.personalDetails[0]);
    		}else{
    			saveSummaryObject = angular.extend(temp, $scope.personalDetails[0]);
    		}  
    		 
        	auraResponseService.setResponse(saveSummaryObject);
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {  
	                console.log(response.data)
	        });
    	}
        	
    };
    
    $scope.summarySaveAndExitPopUp = function (hhText) {
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary"  ng-dialog-class="ngdialog-theme-plain"  ng-dialog-close-previous="" ng-click="closeThisDialog(\'oncancel\')">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
    /*$scope.checkAckState = function(){
    	$timeout(function(){
    		ackCheckCCDD = $('#DutyOfDisclosureLabel').hasClass('active');
        	ackCheckCCPP = $('#privacyPolicyLabel').hasClass('active');
        	ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
        	
        	if(ackCheckCCDD){
        		$scope.CCDDackFlag = false;
        	}else{
        		$scope.CCDDackFlag = true;
        	}
        	
        	if(ackCheckCCPP){
        		$scope.CCPPackFlag = false;
        	}else{
        		$scope.CCPPackFlag = true;
        	}
        	
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
    	}, 10);
    };*/
	
	/*$scope.checkAckStateDD = function(){
    	$timeout(function(){
    		ackCheckCCDD = $('#DutyOfDisclosureLabel').hasClass('active');
        	
        	if(ackCheckCCDD){
        		$scope.CCDDackFlag = false;
        	}else{
        		$scope.CCDDackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStatePP = function(){
    	$timeout(function(){
    		ackCheckCCPP = $('#privacyPolicyLabel').hasClass('active');
        	if(ackCheckCCPP){
        		$scope.CCPPackFlag = false;
        	}else{
        		$scope.CCPPackFlag = true;
        	}
    	}, 10);
    };*/
    
    $scope.checkAckStateGC = function(){
    	$timeout(function(){
    		ackCheckCCGC = $('#generalConsentLabel').hasClass('active');        	
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
    	}, 10);
    };
    
    $scope.checkAckStateLE = function(){
    	$timeout(function(){
    		ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');
        	
        	if(ackCheckLE){
        		$scope.LEFlag = false;
        	}else{
        		$scope.LEFlag = true;
        	}
    	}, 10);
    };
    
    if($routeParams.mode == 4){
    	var num = PersistenceService.getAppNumToBeRetrieved();
    	
    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
    		var result = res.data[0];
    		var coverDet = {},
    			occDet = {},
    			deathAddDet = {},
    			tpdAddDet = {},
    			ipAddDet = {};
    		
    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.dob = moment(result.personalDetails.dateOfBirth).format('DD/MM/YYYY');
    		coverDet.email = result.email;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		occDet.gender = result.personalDetails.gender;
    		coverDet.existingDeathAmt = parseFloat(result.existingDeathAmt);
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.existingTpdAmt =  parseFloat(result.existingTpdAmt);
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.existingIPAmount =  parseFloat(result.existingIPAmount);
    		//coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium =  parseFloat(result.totalPremium);
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.appNum = result.appNum;
    		coverDet.manageType = "CCOVER";
    		coverDet.partnerCode ="CARE";
    		coverDet.freqCostType = 'Weekly';
    		coverDet.age = result.age;
    		if(result.auraDisabled && result.auraDisabled =="true"){
    			$scope.auraDisabled = true;
    		}else{
    			$scope.auraDisabled = false;
    		}
    	  //coverDet.auraDisabled = result.auraDisabled;
    		$scope.changeCoverDetails = coverDet;
    		
    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		occDet.salary = result.occupationDetails.salary;
    		$scope.changeCoverOccDetails = occDet;
    		
    		deathAddDet.deathFixedAmt =  parseFloat(result.addnlDeathCoverDetails.deathFixedAmt);
    		deathAddDet.deathCoverPremium =  parseFloat(result.addnlDeathCoverDetails.deathCoverPremium);
    		deathAddDet.deathLoadingCost =  parseFloat(result.addnlDeathCoverDetails.deathLoadingCost);
    		$scope.deathAddnlDetails = deathAddDet;
    		
    		tpdAddDet.tpdFixedAmt =  parseFloat(result.addnlTpdCoverDetails.tpdFixedAmt);
    		tpdAddDet.tpdCoverPremium =  parseFloat(result.addnlTpdCoverDetails.tpdCoverPremium);
    		tpdAddDet.tpdLoadingCost =  parseFloat(result.addnlTpdCoverDetails.tpdLoadingCost);
    		$scope.tpdAddnlDetails = tpdAddDet;
    		
    		ipAddDet.ipFixedAmt =  parseFloat(result.addnlIpCoverDetails.ipFixedAmt);
    		ipAddDet.waitingPeriod = result.addnlIpCoverDetails.waitingPeriod;
    		ipAddDet.benefitPeriod = result.addnlIpCoverDetails.benefitPeriod;
    		ipAddDet.ipCoverPremium =  parseFloat(result.addnlIpCoverDetails.ipCoverPremium);
    		ipAddDet.ipLoadingCost =  parseFloat(result.addnlIpCoverDetails.ipLoadingCost);
    		$scope.ipAddnlDetails = ipAddDet;
    		
    	}, function(err){
    		console.log("Encountered an error while fetchimg the app details " + err);
    	});
    }
    
    }]); 
   /*Summary Page Controller Ends*/