package au.com.metlife.eapply.bs.controller;

import java.util.Comparator;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;

public class OccComparator implements Comparator{  

	public int compare(Object o1,Object o2){  
		TblOccMappingRuledata s1=(TblOccMappingRuledata)o1;  
		TblOccMappingRuledata s2=(TblOccMappingRuledata)o2;  
		return s1.getOccupationName().compareTo(s2.getOccupationName());  
}
}