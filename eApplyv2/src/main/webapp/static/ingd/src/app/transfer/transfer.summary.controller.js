class TransferSummaryCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._ngDialog = ngDialog;
    this._EapplyData = EapplyData;
    this.applicantData = {};
    this.transferData = {};
    this.exDthCover = {};
    this.exTpdCover = {};
    this.exIpCover = {};
    this.contactOpts = AppConstants.transfer.contactOpts;
    this.waitingOpts = ['14 Days', '30 Days', '45 Days', '60 Days', '90 Days', '4 Weeks', '8 Weeks', '13 Weeks', 'Not Listed'];
    this.benefitOpts = ['2 Years', '5 Years', 'Age 65', 'Age 67', 'Not Listed'];
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
    this.transferData = this._EapplyData.getTransferData();
  }
  
  goToPath(path){
	 this._$state.go(path); 
  }
  
  previous(){	  
	  this._$state.go('app.transferaura');  
  }
  
  saveAndExitPopUp(hhText) {	  
	  this._ngDialog.openConfirm({
		  template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="confirm()">Finish &amp; Close Window </button></div></div>',
		  plain: true,
	      showClose: false,
	      className: 'ngdialog-theme-plain custom-width'
      }).then(
      () => {
        this._$state.go('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }
  
  save() {
	  
	  this.transferData.lastSavedOn = 'transfersummary';
	  
	  console.log(JSON.stringify(this.transferData));
	  this._CommonAPI.saveEapply(this.urlList.saveEapplyUrlNew, this.transferData).then(
	      (res) => {
	        console.log(res);
	        this.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+this.transferData.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
	      },
	      (err) => {
	        this.errors = err.data.errors;
	      }
	   );
  
  }
  
submit() {
	  
	  const _this = this;
	  
	  _this._$scope.termsForm.$submitted = true;
	  _this._$scope.evidenceForm.$submitted = true;
	  
	  if(_this._$scope.termsForm.$valid && _this._$scope.evidenceForm.$valid){
		  
		  /*_this._$state.go('app.quotedecision');
		   return false;*/
		  
		   console.log(JSON.stringify(_this.transferData));
		  
		  _this._CommonAPI.submitEapply(_this.urlList.submitEapplyUrlNew, _this.transferData).then(
	              (res) => {
	            	  console.log(res);
	            	  _this._EapplyData.setPdfLocation(res.clientPDFLocation);
	            	  if(res.npsTokenUR){
	            		  _this._EapplyData.setNpsUrl(res.npsTokenURL);
	            	  }	            	  
	            	  _this._$state.go('app.transferdecision');
	              }, 
	              (err) => {
	                console.log('Error while submitting transfer cover ' + err);
	              }
	       );
		  
	  }
  }
  
  
}

export default TransferSummaryCtrl;
