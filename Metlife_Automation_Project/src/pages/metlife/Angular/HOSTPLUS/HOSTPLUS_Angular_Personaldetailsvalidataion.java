package pages.metlife.Angular.HOSTPLUS;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ScriptHelper;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
public class HOSTPLUS_Angular_Personaldetailsvalidataion extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_Personaldetailsvalidataion SaveandExit_personaldetails() {
		SaveandExitvalidation();		
		
		
		return new HOSTPLUS_Angular_Personaldetailsvalidataion(scriptHelper);
	}

	public HOSTPLUS_Angular_Personaldetailsvalidataion(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void SaveandExitvalidation() {
	
	String EmailId = dataTable.getData("HOSTPLUS_SaveAndExit", "EmailId");
	String TypeofContact = dataTable.getData("HOSTPLUS_SaveAndExit", "TypeofContact");
	String ContactNumber = dataTable.getData("HOSTPLUS_SaveAndExit", "ContactNumber");
	String TimeofContact = dataTable.getData("HOSTPLUS_SaveAndExit", "TimeofContact");
	String Gender = dataTable.getData("HOSTPLUS_SaveAndExit", "Gender");
	String FifteenHoursWork = dataTable.getData("HOSTPLUS_SaveAndExit", "FifteenHoursWork");
	String RegularIncome = dataTable.getData("HOSTPLUS_SaveAndExit", "RegularIncome");
	String Perform_WithoutRestriction = dataTable.getData("HOSTPLUS_SaveAndExit", "Perform_WithoutRestriction");
	String Work_WithoutLimitation = dataTable.getData("HOSTPLUS_SaveAndExit", "Work_WithoutLimitation");
	String Citizen = dataTable.getData("HOSTPLUS_SaveAndExit", "Citizen");
	String IndustryType = dataTable.getData("HOSTPLUS_SaveAndExit", "IndustryType");
	String OccupationType = dataTable.getData("HOSTPLUS_SaveAndExit", "OccupationType");
	String OtherOccupation = dataTable.getData("HOSTPLUS_SaveAndExit", "OtherOccupation");
	String OfficeEnvironment = dataTable.getData("HOSTPLUS_SaveAndExit", "OfficeEnvironment");
	String TertiaryQual = dataTable.getData("HOSTPLUS_SaveAndExit", "TertiaryQual");
	String HazardousEnv = dataTable.getData("HOSTPLUS_SaveAndExit", "HazardousEnv");
	String OutsideOfcPercent = dataTable.getData("HOSTPLUS_SaveAndExit", "OutsideOfcPercent");
	String AnnualSalary = dataTable.getData("HOSTPLUS_SaveAndExit", "AnnualSalary");
	String CostType=dataTable.getData("HOSTPLUS_SaveAndExit", "CostType");
	String DeathYN=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathYN");
	String AmountType=dataTable.getData("HOSTPLUS_SaveAndExit", "AmountType");
	String DeathAction=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathAction");
	String DeathAmount=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathAmount");
	String DeathUnits=dataTable.getData("HOSTPLUS_SaveAndExit", "DeathUnits");
	String TPDAction=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDAction");
	String TPDAmount=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDAmount");
	String TPDUnits=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDUnits");
	String IPYN=dataTable.getData("HOSTPLUS_SaveAndExit", "IPYN");
	String IPAction=dataTable.getData("HOSTPLUS_SaveAndExit", "IPAction");
	String Insure90Percent=dataTable.getData("HOSTPLUS_SaveAndExit", "Insure90Percent");
	String WaitingPeriod=dataTable.getData("HOSTPLUS_SaveAndExit", "WaitingPeriod");
	String BenefitPeriod=dataTable.getData("HOSTPLUS_SaveAndExit", "BenefitPeriod");
	String IPAmount=dataTable.getData("HOSTPLUS_SaveAndExit", "IPAmount");
	String TPDYN=dataTable.getData("HOSTPLUS_SaveAndExit", "TPDYN");
	String Dodcheckbox=dataTable.getData("HOSTPLUS_SaveAndExit", "Dodcheckbox");
	String Privacycheckbox=dataTable.getData("HOSTPLUS_SaveAndExit", "Privacycheckbox");
	
	
	try{
		
		
		
		//****Click on Change Your Insurance Cover Link*****\\
		waitForPageTitle("testing", 15);
		WebDriverWait wait=new WebDriverWait(driver,18);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Saved Applications']"))).click();		
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(5000);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']"))).click();
    	report.updateTestLog("Saved Application", "Saved Application is Retrieved", Status.PASS);
		sleep(4000);

		
			
			 WebElement email=driver.findElement(By.name("contactEmail"));	 
			 String Email_validation=email.getAttribute("value");	
			 System.out.println(Email_validation);
			 WebElement contacttype=driver.findElement(By.xpath("//select[@ng-model='QPD.contactType']/option[@value='string:1']"));	
			 String contacttype_validation=contacttype.getText();
			 System.out.println(contacttype_validation);
		     
			 WebElement contactnumber=driver.findElement(By.id("preferrednumCCId"));	 
			 String contactnumber_validation=contactnumber.getAttribute("value");
			 
			 WebElement morning=driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]"));	
			 WebElement afternoon=driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]"));	
			 
			 WebElement FifteenHoursyes=driver.findElement(By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*"));	
			 WebElement FifteenHoursno=driver.findElement(By.xpath("//input[@name='fifteenHrsQuestion']/parent::*"));
			 

			 Select Industry=new Select(driver.findElement(By.name("industry")));	 
			
			    sleep(1000);
			    WebElement inds =Industry.getFirstSelectedOption();
			    String Industry_validation = inds.getText();	    
     		    sleep(800); 
 
			 
			 WebElement occupation=driver.findElement(By.name("occupation"));	 
			 String occupation_validation=occupation.getAttribute("value");
			 
		/*	 WebElement otheroccupation=driver.findElement(By.name("otherOccupation"));	 
			 String otheroccupation_validation=otheroccupation.getAttribute("value");	*/
			 		 
			 
			 WebElement salary=driver.findElement(By.name("annualSalary"));	 
			 String Annualsalary_validation=salary.getAttribute("value");
			 
			 
			 WebElement waitingperiod=driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.waitingPeriod']"));
			 String waitingperiod_validation=waitingperiod.getAttribute("value");
			 
			 WebElement benefitperiod=driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.benefitPeriod']"));	 
			 String benefitperiod_validation=benefitperiod.getAttribute("value");
			 //death
//			 WebElement Deathaction=driver.findElement(By.name("coverName"));	 
//			 String Deathaction_validation=Deathaction.getAttribute("value");
			 
			 WebElement Deathamount=driver.findElement(By.id("dollar"));	 
			 String Deathamount_validation=Deathamount.getAttribute("value");
			 
			 //unitised
			 WebElement Deathamoununits=driver.findElement(By.id("nodollar"));	 
			 String Deathamoununits_validation=Deathamoununits.getAttribute("value");
			 
			 ///TPD
//			 WebElement TPDaction=driver.findElement(By.name("tpdCoverName"));	 
//			 String TPDaction_validation=TPDaction.getAttribute("value");
			 
			 WebElement TPDamount=driver.findElement(By.id("dollar1"));	 
			 String TPDamount_validation=TPDamount.getAttribute("value");
			 
			 WebElement TPDunits=driver.findElement(By.id("nodollar1"));	 
			 String TPDunits_validation=TPDunits.getAttribute("value");
			 
			 
			 
			 //IP
//			 WebElement IPaction=driver.findElement(By.name("ipCoverName"));	 
//			 String IPaction_validation=IPaction.getAttribute("value");
			 
			 WebElement IPamount=driver.findElement(By.name("IPRequireCover"));	 
			 String IPamount_validation=IPamount.getAttribute("value");
			 
			  sleep(100);
				JavascriptExecutor a = (JavascriptExecutor) driver;
				a.executeScript("window.scrollBy(0,700)", "");
				sleep(100);	
			 
			 if(Dodcheckbox.equalsIgnoreCase("Yes")){
				 WebElement Dod=driver.findElement(By.xpath(".//input[@name='dod']/parent::*"));
				 Dod.isSelected();
					 report.updateTestLog("DOD Check box", "DOD checkbox is selected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("DOD Check box", "DOD checkbox is not selected", Status.FAIL);
					}
			 
			    sleep(100);
				JavascriptExecutor be = (JavascriptExecutor) driver;
				be.executeScript("window.scrollBy(0,800)", "");
				sleep(100);	
			 
			 if(Privacycheckbox.equalsIgnoreCase("Yes")){
				 WebElement Privacy=driver.findElement(By.xpath(".//input[@name='privacy']/parent::*"));	
				 Privacy.isSelected();
					 report.updateTestLog("Privacy Check box", "Privacy Statement checkbox is selected", Status.PASS);	
					}
					else{
						
						report.updateTestLog("Privacy Check box", "Privacy Statement checkbox is not selected", Status.FAIL);
					}
			  sleep(100);
				JavascriptExecutor b = (JavascriptExecutor) driver;
				b.executeScript("window.scrollBy(0,400)", "");
				sleep(100);	
			 
			 
			 if(EmailId.contains(Email_validation)){
					
					report.updateTestLog("Email ID", EmailId+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Email ID", EmailId+" is not displayed as expected", Status.FAIL);
				}
			 
			 if(TypeofContact.contains(contacttype_validation)){
					
					report.updateTestLog("TypeofContact", TypeofContact+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("TypeofContact", TypeofContact+" is not displayed as expected", Status.FAIL);
				}
			 
			 if(ContactNumber.contains(contactnumber_validation)){
					
					report.updateTestLog("ContactNumber", ContactNumber+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("ContactNumber", ContactNumber+" is not displayed as expected", Status.FAIL);
				}
			 
			
			 if(TimeofContact.equalsIgnoreCase("Afternoon")){
				  		
				 afternoon.isSelected();
					 report.updateTestLog("Time of contact", TimeofContact+" is displayed as Expected", Status.PASS);	
					}
					else{
						morning.isSelected();
						report.updateTestLog("Time of contact", TimeofContact+" is not displayed as expected", Status.FAIL);
					}
				
			 if(FifteenHoursWork.equalsIgnoreCase("Yes")){
			  		
				 FifteenHoursyes.isSelected();
					 report.updateTestLog("FifteenHoursWork question", FifteenHoursWork+" is displayed as Expected", Status.PASS);	
					}
					else{
						FifteenHoursno.isSelected();
						report.updateTestLog("FifteenHoursWork question", FifteenHoursWork+" is not displayed as expected", Status.FAIL);
					}
			
			 sleep(100);
				JavascriptExecutor br = (JavascriptExecutor) driver;
				br.executeScript("window.scrollBy(0,800)", "");
				sleep(100);	
			 
			 if(IndustryType.contains(Industry_validation)){
			
				
				
					report.updateTestLog("Industry", IndustryType+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Industry", IndustryType+" is not displayed as expected", Status.FAIL);
				}
			 
			 
			 if(OccupationType.contains(occupation_validation)){
					
					report.updateTestLog("Occupation", OccupationType+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Occupation", OccupationType+" is not displayed as expected", Status.FAIL);
				}
			 
			 
			/* if(OtherOccupation.contains("Yes")){		 
			 if(OtherOccupation.contains(otheroccupation_validation)){
					report.updateTestLog("OtherOccupation", OtherOccupation+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("OtherOccupation", OtherOccupation+" is not displayed as expected", Status.FAIL);
				}
			 
			 }*/
			 
			 if(AnnualSalary.contains(Annualsalary_validation)){
					
					report.updateTestLog("AnnualSalary Validation:", AnnualSalary+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("AnnualSalary Valdiation:", AnnualSalary+" is not displayed as expected", Status.FAIL);
				}
			 
				//*****Extra Questions*****\\							
				
			    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
			    	 WebElement WithinOfficeyes=driver.findElement(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"));	
					 WebElement WithinOfficeno=driver.findElement(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"));
					 
					
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\	
			    	
			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){	
			    	WithinOfficeyes.isSelected();
					report.updateTestLog("Duties are Undertaken within an Office Environment question validation:", OfficeEnvironment+" is displayed as Expected", Status.PASS);	
				}
				else{
					 WithinOfficeno.isSelected();
					report.updateTestLog("Duties are Undertaken within an Office Environment question validation:", OfficeEnvironment+" is not displayed as expected", Status.FAIL);
				}	 
							
								
			    WebElement TertiaryQualyes=driver.findElement(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"));	
				 WebElement TertiaryQualno=driver.findElement(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"));						
			      //*****Do You Hold a Tertiary Qualification******\\							
										
			        if(TertiaryQual.equalsIgnoreCase("Yes")){							
			       
			        	TertiaryQualyes.isSelected();
						report.updateTestLog("Office Environment question", OfficeEnvironment+" is displayed as Expected", Status.PASS);	
					}
					else{
						TertiaryQualno.isSelected();
						report.updateTestLog("Office Environment question", OfficeEnvironment+" is not displayed as expected", Status.FAIL);
					}			
			}	
													
										
			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
										
				 WebElement HazardousEnvYes=driver.findElement(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[1]"));	
				 WebElement HazardousEnvNo=driver.findElement(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[2]"));
				 
				 
				 WebElement OutsideOfcPercentYes=driver.findElement(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"));	
				 WebElement OutsideOfcPercentNo=driver.findElement(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"));				
										
			//******Do you work in hazardous environment*****\\		
				
			if(HazardousEnv.equalsIgnoreCase("Yes")){							
				HazardousEnvYes.isSelected();
				report.updateTestLog("Hazardous environment question", HazardousEnv+" is displayed as Expected", Status.PASS);	
			}
			else{
				HazardousEnvNo.isSelected();
				report.updateTestLog("Hazardous environment question", HazardousEnv+" is not displayed as expected", Status.FAIL);
				
			}							
										
			//******Do you spend more than 10% of your working time outside of an office environment?*******\\	
			
			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
			
				OutsideOfcPercentYes.isSelected();
				report.updateTestLog("Do you spend more than 10% of your working time outside of an office environment?", OutsideOfcPercent+" is displayed as Expected", Status.PASS);	
			}
			else{
				OutsideOfcPercentNo.isSelected();
				report.updateTestLog("Do you spend more than 10% of your working time outside of an office environment?", OutsideOfcPercent+" is not displayed as expected", Status.FAIL);
				
			}		
			}
			 sleep(800);

			   /* if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
			    	 WebElement WithinOfficeyes=driver.findElement(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"));	
					 WebElement WithinOfficeno=driver.findElement(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"));
					 					
			//***Select if your office Duties are Undertaken within an Office Environment?\\	
			    	
			    	 if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
			    		 WithinOfficeyes.isSelected();
							report.updateTestLog("Office Environment question", OfficeEnvironment+" is displayed as Expected", Status.PASS);	
						}
						else{
							 WithinOfficeno.isSelected();
							report.updateTestLog("Office Environment question", OfficeEnvironment+" is not displayed as expected", Status.FAIL);
						}	   
					 
												
					}							
					else{							
						
						 if(TertiaryQual.equalsIgnoreCase("Yes")){							
							 WebElement TertiaryQualyes=driver.findElement(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"));							
					        	TertiaryQualyes.isSelected();
								report.updateTestLog("Office Environment question", OfficeEnvironment+" is displayed as Expected", Status.PASS);	
							}
							else{
								 WebElement TertiaryQualno=driver.findElement(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"));  
								TertiaryQualno.isSelected();
								report.updateTestLog("Office Environment question", OfficeEnvironment+" is not displayed as expected", Status.FAIL);
							}												
					   
					
							
		}			 
	
*/

			//*******Validating the Death Cover Details********\\
			 
			 sleep(100);
				JavascriptExecutor t = (JavascriptExecutor) driver;
				t.executeScript("window.scrollBy(0,800)", "");
				sleep(100);	
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				
				
		     /*   
				if(DeathAction.contains(Deathaction_validation)){
					
					report.updateTestLog("Death Cover action", DeathAction+" is displayed as Expected", Status.PASS);	
				}
				else{
					
					report.updateTestLog("Death Cover action", DeathAction+" is not displayed as expected", Status.FAIL);
				}*/
				
				
				if(AmountType.equalsIgnoreCase("Fixed")){    
				
		       if(DeathAmount.equalsIgnoreCase(Deathamount_validation)){
					
					report.updateTestLog("Death Cover Amount", DeathAmount+" is displayed as Expected", Status.PASS);	
				}       
				else{
					
					report.updateTestLog("Death Cover Amount", DeathAmount+" is not displayed as expected", Status.FAIL);
				
				}
		       if(!AmountType.equalsIgnoreCase("Fixed")){     
		       
				 if(DeathUnits.equalsIgnoreCase(Deathamoununits_validation)){
						
						report.updateTestLog("Death Cover Units", DeathUnits+" is displayed as Expected", Status.PASS);	
					}       
					else{
						
						report.updateTestLog("Death Cover Units", DeathUnits+" is not displayed as expected", Status.FAIL);
					}
		       }      
		   
		       }
				}
			

				
			//*******Validating the TPD Cover Details********\\
			
			
		if(TPDYN.equalsIgnoreCase("Yes")){
		/*	if(TPDAction.equalsIgnoreCase(TPDaction_validation)){
				
				report.updateTestLog("TPD Cover action", TPDAction+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("TPD Cover action", TPDAction+" is not displayed as expected", Status.FAIL);
			}*/
				if(AmountType.equalsIgnoreCase("Fixed")){       
				
				
		       if(TPDAmount.equalsIgnoreCase(TPDamount_validation)){
					
					report.updateTestLog("TPD Cover Amount", TPDAmount+" is displayed as Expected", Status.PASS);	
				}       
				else{
					
					report.updateTestLog("TPD Cover Amount", TPDAmount+" is not displayed as expected", Status.FAIL);
				}	
				}
		       if(!AmountType.equalsIgnoreCase("Fixed")){     
		       
				 if(TPDUnits.equalsIgnoreCase(TPDunits_validation)){
						
						report.updateTestLog("TPD Cover Units", TPDunits+" is displayed as Expected", Status.PASS);	
					}       
					else{
						
						report.updateTestLog("TPD Cover Units", TPDunits+" is not displayed as expected", Status.FAIL);
					}
		       }
			}
		//*******Validating the IP Cover Details********\\

		if(IPYN.equalsIgnoreCase("Yes")){
			
/*
		if(IPAction.equalsIgnoreCase(IPaction_validation)){
				
				report.updateTestLog("IP Cover action", IPAction+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("IP Cover action", IPAction+" is not displayed as expected", Status.FAIL);
			}*/
			
		if(IPAmount.equalsIgnoreCase(IPamount_validation)){
			
			report.updateTestLog("IP Cover action", IPAmount+" is displayed as Expected", Status.PASS);	
		}
		else{
			
			report.updateTestLog("IP Cover action", IPAmount+" is not displayed as expected", Status.FAIL);
		}
			
			
		}
			
		if(WaitingPeriod.equalsIgnoreCase(waitingperiod_validation)){
			
			 
			   report.updateTestLog("WaitingPeriod", WaitingPeriod+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("WaitingPeriod", WaitingPeriod+" is not displayed as expected", Status.FAIL);
			}

		if(BenefitPeriod.equalsIgnoreCase(benefitperiod_validation)){
				
				 
			   report.updateTestLog("BenefitPeriod", BenefitPeriod+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("BenefitPeriod", BenefitPeriod+" is not displayed as expected", Status.FAIL);
			}
			
				
			
		
	
}catch(Exception e){
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}
}

