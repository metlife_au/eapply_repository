/* Transfer Cover Controller,Progressive and Mandatory validations Starts  */
VicsuperApp.controller('lifeevent',['$scope','$rootScope', '$routeParams','$location','$timeout','$http','$window','persoanlDetailService','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'NewOccupationService','MaxLimitService','auraInputService','PersistenceService','ngDialog','auraResponseService','Upload','urlService','saveEapply','RetrieveAppDetailsService','CalculateService','tokenNumService', '$q','$filter','APP_CONSTANTS',
                                         function($scope,$rootScope, $routeParams,$location,$timeout,$http,$window,persoanlDetailService,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService, NewOccupationService,MaxLimitService, auraInputService,PersistenceService,ngDialog,auraResponseService,Upload,urlService,saveEapply,RetrieveAppDetailsService,CalculateService,tokenNumService, $q,$filter,APP_CONSTANTS){
	
	/* Code for appD starts */
	  var pageTracker = null;
	  if(ADRUM) {
	    pageTracker = new ADRUM.events.VPageView();
	    pageTracker.start();
	  }

	  $scope.$on('$destroy', function() {
	    pageTracker.end();
	    ADRUM.report(pageTracker);
	  });
	  /* Code for appD ends */
	
	
	$scope.urlList = urlService.getUrlList();
    $scope.phoneNumbrLifeEvent = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
    $scope.emailFormatLifeEvent = APP_CONSTANTS.emailFormat;
    $scope.indexation= {
    		death: false,
    		disable: false
    };
    $scope.showhazardousLifeEventQuestion = false;
    $scope.showLifeEventOutsideOffice = false;
    $scope.privacyFlagErr = false;
    $scope.otherOccupationObj = {'lifeEventOtherOccupation': ''};
    $scope.privacyCol = false;
    $scope.contactCol = false;
    $scope.occupationCol = false;
    $scope.lifeEventSectionCol = false;
    $scope.isDeathDisabled = false;
    $scope.isTPDDisabled = false;
    $scope.files = [];
    $scope.selectedFile = null;
    $scope.deathCoverLEDetails = deathCoverService.getDeathCover();
	$scope.tpdCoverLEDetails = tpdCoverService.getTpdCover();
	$scope.ipCoverLEDetails = ipCoverService.getIpCover();
    $scope.preferredContactTransOptions = ["Home phone", "Work phone", "Mobile"];
    $scope.regex = /[0-9]{1,3}/;
    $scope.dcTransCoverAmount = 0.00;
	$scope.dcTransCost = 0.00;
	$scope.tpdTransCoverAmount = 0.00;
	$scope.tpdTransCost = 0.00;
	$scope.ipTransCoverAmount = 0.00;
	$scope.ipTransCost = 0.00;
	$scope.totalTransCost = 0.00;
	$scope.transferAckFlag = false;
	$scope.prevOtherOcc = null;
	$scope.disableGender = false;
	$scope.autoCalc = false;
	$rootScope.$broadcast('enablepointer');
  $scope.fileNotUploadedError = false;
  $scope.fileFormatError = false;
  $scope.corruptedFile = false;
  $scope.corruptedFileMsg = '';
  $scope.deathUnitFixAmt = 0;
  $scope.tpdUnitFixAmt = 0;
  $scope.deathUnitFixAmt2 = 0;
  $scope.tpdUnitFixAmt2 = 0;
  $scope.requireCover = null;
  $scope.TPDRequireCover = null;
  $scope.totalDeathUnits =null;
  $scope.totalTPDUnits = null;
  $scope.ownOccuptionDeath = false;
  $scope.ownOccuptionTpd = false;
  $scope.coverSecFlag = false;
  $scope.tpdTapering = 1.00;
  $scope.tpdTaperAmt = 0.00;
  $scope.hideCoverOnNo = false;
  $scope.continueButtonHit = false;
  $scope.eventList = 
	  [{
      "cde": "MARR",
      "desc": "Marriage or registration of de facto relationship",
      "docDesc": "Marriage or registration of de facto relationship.",
      "evidenceHelpText": "Marriage or registration of de facto relationship."
  			},
  			{
      "cde": "ANNMARR",
      "desc": "1st anniversary of marriage or registration of de facto relationship",
      "docDesc": "1st anniversary of marriage or registration of de facto relationship.",
      "evidenceHelpText": "1st anniversary of marriage or registration of de facto relationship."
  			},
  			{
      "cde": "DIVO",
      "desc": "Divorce or registration of separation from marriage or de facto relationship",
      "docDesc": "Divorce or registration of separation from marriage or de facto relationship.",
      "evidenceHelpText": "Divorce or registration of separation from marriage or de facto relationship:"
  			},
  			{
      "cde": "ANNDIVO",
      "desc": "1st anniversary of divorce or registration of separation from marriage or de factor relationship",
      "docDesc": "1st anniversary of divorce or registation of separation from marriage or de factor relationship.",
      "evidenceHelpText": "1st anniversary of divorce or registation of separation from marriage or de factor relationship."
  			},
  			{
  	  "cde": "BRTH",
  	  "desc": "Birth or adoption of a child",
  	  "docDesc": "Adopting or becoming the natural parent of a child.",
  	  "evidenceHelpText": "A certified copy of the birth certificate or adoption papers."
  		  	},
  		  	{
  	   "cde": "FRST",
  	   "desc": "New mortgage on initial purchase of primary place of residence",
  	   "docDesc": "New mortgage on initial purchase of primary place of residence.",
  	   "evidenceHelpText": "A certified copy of all the following:"
  		  	},
  			{
      "cde": "CUGA",
      "desc": "Increasing existing mortgage by at least $50,000 to renovate / extend primary place of residence",
      "docDesc": "Increasing existing mortgage by at least $50,000 to renovate / extend primary place of residence.",
      "evidenceHelpText": "Increasing existing mortgage by at least $50,000 to renovate / extend primary place of residence."
  			},
  			{
      "cde": "DCSS",
      "desc": "12th birthday of a child",
      "docDesc": "12th birthday of a child.",
      "evidenceHelpText": "12th birthday of a child."
  			}];


    /*Error Flags*/
    $scope.dodFlagErr = null;
    $scope.privacyFlagErr = null;

    var deathLEDBCategory, tpdLEDBCategory, ipLEDBCategory;
    var annualSalForTransUpgradeVal;
    var DCTransMaxAmount, TPDTransMaxAmount, IPTransMaxAmount;
   	var mode3Flag = false;
   	var inputDetails = persoanlDetailService.getMemberDetails();
   	$scope.personalDetails = inputDetails.personalDetails;
   	$scope.gender = $scope.personalDetails.gender;
   	var doj = inputDetails.dateJoined;
   	var fetchAppnum = true;
   	var appNum = PersistenceService.getAppNumber();
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
    var dodCheck;
    var privacyCheck;
    var privacyVal = 0;
    var contactVal = 0;
    var occupationVal = 0;
    var previousSectionVal = 0;
    var transferCoverVal = 0;
    var occupationDetailsLifeEventFormFields = ['fifteenHrsQuestion','areyouperCitzLifeEventQuestion','occRating1a','occRating1b','occgovrating2','annualSalary'];
    var occupationDetailsOtherLifeEventFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation','annualSalary'];
    var contactDetailsLifeEventFormFields = ['contactDetailsLifeEventEmail', 'contactDetailsLifeEventPhone','contactDetailsLifeEventPrefTime','gender'];
    var lifeEventFields = ['event','eventDate','eventAlreadyApplied'];
    var lifeEventFieldsWithChkBox = ['event','eventDate','eventAlreadyApplied'];
    var CoverCalculatorFormFields = ['coverType','requireCover','tpdCoverType','TPDRequireCover'];
    var uploadedFiles = [];
	var transferAckCheck;
	var ackDocument;
	var unitIncrement = 4;
	var amountIncrement = 0.25;
	var incrementLimit = 200000;
	var dcCoverAmount = dcWeeklyCost = tpdCoverAmount = tpdWeeklyCost = ipCoverAmount = ipWeeklyCost = totalCost = 0;
	
	var  standard = 'general';
	var  whitecollor = 'white collar';
	var  ownoccupation = 'own occupation';
	var  professional = 'professional';
	
	$scope.deathCoverAmt=0;
	$scope.tpdCoverAmt=0;
	$scope.deathCost = 0;
	$scope.tpdCost = 0;
	$scope.totalCoverCost = 0;
	$scope.hideTPD = true;
	$scope.contactTypeOptions = ["Home phone", "Work phone", "Mobile"];
	$scope.preferredContactType = '';
	$scope.premFreq = "Weekly";
	 	$scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
	    $scope.benefitPeriodOptions = ['2 Years','5 Years','Age 65'];
 
	    $scope.premiumFrequencyOptions = ['Weekly', 'Monthly', 'Yearly'];
	    
	    
	    
	    switch($scope.ipCoverLEDetails.waitingPeriod.toLowerCase().trim().substr(0,2)){
		
		case "30":
			$scope.ipCoverLEDetails.waitingPeriod = "30 Days";
			break;
		case "60":
			$scope.ipCoverLEDetails.waitingPeriod = "60 Days";
			break;
		case "90":
			$scope.ipCoverLEDetails.waitingPeriod = "90 Days";
			break;
		}
	switch($scope.ipCoverLEDetails.benefitPeriod.toLowerCase().trim().substr(0,1)){
		
		case "2":
			$scope.ipCoverLEDetails.benefitPeriod = "2 Years";
			break;
		case "5":
			$scope.ipCoverLEDetails.benefitPeriod = "5 Years";
			break;
		case "A":
			$scope.ipCoverLEDetails.benefitPeriod = "Age 65";
			break;
		case "a":
			$scope.ipCoverLEDetails.benefitPeriod = "Age 65";
			break;
		}
	    
	    
	    $scope.ipCoverLEDetails.waitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverLEDetails.waitingPeriod) > -1 ? $scope.ipCoverLEDetails.waitingPeriod : '90 Days' || '90 Days';
	    $scope.ipCoverLEDetails.benefitPeriod = $scope.benefitPeriodOptions.indexOf($scope.ipCoverLEDetails.benefitPeriod) > -1 ? $scope.ipCoverLEDetails.benefitPeriod : '2 Years' || '2 Years';

    QuoteService.getList($scope.urlList.quoteUrl,"VICT").then(function(res){
    	$scope.IndustryOptions = res.data;
    }, function(err){
    	//console.log("Error while getting industry options " + JSON.stringify(err));
    });

    if(inputDetails && inputDetails.contactDetails.emailAddress){
		$scope.lifeEventEmail = inputDetails.contactDetails.emailAddress;
	}
	if(inputDetails && inputDetails.contactDetails.prefContactTime){
		if(inputDetails.contactDetails.prefContactTime == "1"){
			$scope.lifeEventTime= "Morning (9am - 12pm)";
		}else{
			$scope.lifeEventTime= "Afternoon (12pm - 6pm)";
		}
	}
	if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
		$scope.gender ='';
	}else{
		$scope.gender = $scope.personalDetails.gender;
	}
	
	if($scope.gender && (($scope.gender).toLowerCase() === 'female' || ($scope.gender).toLowerCase() === 'male' ))
	{
	$scope.disableGender = true;
	}
	else
	{
	$scope.disableGender = false;
	}
	
	
		if($scope.deathCoverLEDetails.type == '1')
			{
			showhide('nodollar1','dollar1');
	          showhide('nodollar','dollar');
			}
		else
			{
			showhide('dollar1','nodollar1');
	          showhide('dollar','nodollar');
			}
	$scope.exitingDeathUnits = $scope.deathCoverLEDetails.units;
	$scope.exitingtpdUnits = $scope.tpdCoverLEDetails.units;
	
	if(inputDetails.contactDetails.prefContact == null || inputDetails.contactDetails.prefContact == "")
	{
		inputDetails.contactDetails.prefContact=1;
	}
	if(inputDetails && inputDetails.contactDetails.prefContact){
		if(inputDetails.contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.lifeEventPhone = inputDetails.contactDetails.mobilePhone;
		}else if(inputDetails.contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home phone";
			$scope.lifeEventPhone = inputDetails.contactDetails.homePhone;
		}else if(inputDetails.contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work phone";
			$scope.lifeEventPhone = inputDetails.contactDetails.workPhone;
		}
   }
	
	var deathoccRating = $scope.deathCoverLEDetails.occRating;
	if (deathoccRating) {
		if(deathoccRating.toLowerCase() == ownoccupation)
		{
		$scope.ownOccuptionDeath = true;
		}
		if (deathoccRating.toLowerCase() == standard){
			deathoccRating = 1;
		}
		else if (deathoccRating.toLowerCase() == whitecollor || deathoccRating.toLowerCase() == ownoccupation){
			deathoccRating = 2;
		}
		else if (deathoccRating.toLowerCase() == professional){
			deathoccRating = 3;
		}
	}
	
	var tpdoccRating = $scope.tpdCoverLEDetails.occRating;
	if (tpdoccRating) {
		if(tpdoccRating.toLowerCase() == ownoccupation)
		{
		$scope.ownOccuptionTpd = true;
		}
		if (tpdoccRating.toLowerCase() == standard){
			tpdoccRating = 1;
		}
		else if (tpdoccRating.toLowerCase() == whitecollor || tpdoccRating.toLowerCase() == ownoccupation){
			tpdoccRating = 2;
		}
		else if (tpdoccRating.toLowerCase() == professional){
			tpdoccRating = 3;
		}
	}
	
	var ipoccRating = $scope.ipCoverLEDetails.occRating;
	if (ipoccRating) {
		if (ipoccRating.toLowerCase() == standard){
			ipoccRating = 1;
		}
		else if (ipoccRating.toLowerCase() == whitecollor || ipoccRating.toLowerCase() == ownoccupation){
			ipoccRating = 2;
		}
		else if (ipoccRating.toLowerCase() == professional){
			ipoccRating = 3;
		}
	}
	
	$scope.inputMsgOccRating = Math.max(deathoccRating,tpdoccRating,ipoccRating);		
	
	
	$scope.lifeEventDeathOccCategory= $scope.inputMsgOccRating || 'General';
	$scope.lifeEventTpdOccCategory = $scope.inputMsgOccRating || 'General';
	$scope.lifeEventIpOccCategory = $scope.inputMsgOccRating || 'General';

	$scope.ageLimit=anb;
	
switch(anb){
    
    case 61 :
    	$scope.tpdTapering = 0.90;
    	break;
    case 62 :
    	$scope.tpdTapering = 0.80;
    	break;
    case 63 :
    	$scope.tpdTapering = 0.70;
    	break;
    case 64 :
    	$scope.tpdTapering = 0.60;
    	break;
    case 65 :
    	$scope.tpdTapering = 0.50;
    	break;
    case 66 :
    	$scope.tpdTapering = 0.40;
    	break;
    case 67 :
    	$scope.tpdTapering = 0.30;
    	break;
    case 68 :
    	$scope.tpdTapering = 0.20;
    	break;
    case 69 :
    	$scope.tpdTapering = 0.20;
    	break;
    
    }
	
var tpdValidAge=64;
if(parseInt($scope.tpdCoverLEDetails.amount) > 0 )
	{
	tpdValidAge = 69;
	$scope.hideTPD = false;
	}
	
	if($scope.ageLimit < 14 || $scope.ageLimit > 69){
		$('#deathsection').removeClass('active');
		$('#tpdsection').removeClass('active');
		$('#ipsection').removeClass('active');
		$("#death").css("display", "none");
		$("#tpd").css("display", "none");
		$scope.isDeathDisabled = true;
	    $scope.isTPDDisabled = true;
	}else if($scope.ageLimit < 14 || $scope.ageLimit > tpdValidAge){
		$('#tpdsection').removeClass('active');
		$('#ipsection').removeClass('active');
		$("#tpd").css("display", "none");
		$scope.isTPDDisabled = true;
	}

	

    $scope.setIndexation = function ($event) {
    	$event.stopPropagation();
    	$event.preventDefault();
    	$scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
    	if(!$scope.indexation.death) {
    		$("#indexation-death").parent().removeClass('active');
    		$("#indexation-disable").parent().removeClass('active');
    	}
    };

    $scope.getOccupations = function(){
      if($scope.otherOccupationObj)
          $scope.otherOccupationObj.lifeEventOtherOccupation = '';
    	if(!$scope.lifeEventIndustry){
    		$scope.lifeEventIndustry = '';
    	}
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.lifeEventIndustry).then(function(res){
    		$scope.OccupationList = res.data;
    		$scope.lifeEventOccupation="";
    		if($scope.toggleThree)
    			{
    			$scope.toggleThree(false);
    			}
    	}, function(err){
    		//console.log("Error while fetching occupation options " + JSON.stringify(err));
    	});
    };
    
    $scope.getOtherOccupationAS = function(entered) {

	    return $http.get('./occupation.json').then(function(response) {
	      $scope.occupationList=[];
        if(response.data.Other) {
	        for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	               obj.name=response.data.Other[key];
                 //if(obj.name.indexOf(entered) > -1) {
                  $scope.occupationList.push(obj.name);
                 //}
	               
	        }
	      }
        return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });
	    
	  };
    
    MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"VICT",inputDetails.memberType,"ICOVER").then(function(res){
		var limits = res.data;
		annualSalForOccUpgradeVal = limits[0].annualSalForUpgradeVal;
	}, function(error){
		console.info('Something went wrong while fetching limits ' + error);
	});
    
    $scope.go = function (path){
  		$location.path(path);
  	};

	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home phone"){
			$scope.lifeEventPhone = inputDetails.contactDetails.homePhone;
		} else if($scope.preferredContactType == "Work phone"){
			$scope.lifeEventPhone = inputDetails.contactDetails.workPhone;
		} else if($scope.preferredContactType == "Mobile"){
			$scope.lifeEventPhone = inputDetails.contactDetails.mobilePhone;
		} else {
      $scope.lifeEventPhone ='';
    }
	};
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };


    if($scope.deathCoverLEDetails.type == '1'){
    	   $scope.deathCoverType = 'DcUnitised';
		} else if($scope.deathCoverLEDetails.type == '2'){
			$scope.deathCoverType = 'DcFixed';
		}
    	if($scope.tpdCoverLEDetails.type == '1'){
    		$scope.tpdCoverType = 'TPDUnitised';
    	} else if($scope.tpdCoverLEDetails.type == '2'){
    		$scope.tpdCoverType = 'TPDFixed';
		}

$scope.citizenshipChange = function(){
    		$scope.hideCoverOnNo = false;
    		if($scope.areyouperCitzLifeEventQuestion == 'No')
    			{
    				$scope.hideCoverOnNo = true;
    				 $scope.toggleThree(false);
    		         $scope.toggleFour(false);
    			}
    		else
    			{
    			if(this.occupationDetailsLifeEventForm.$valid && $scope.continueButtonHit)
    				{
    				$scope.toggleThree(true);
    				}
    			if(this.occupationDetailsLifeEventForm.$valid && this.lifeEvent.$valid && $scope.continueButtonHit)
    				{
    				$scope.toggleFour(true);
    				}
    			}
    	};
    	
$scope.calculateOnChange = function()
	{
    	if(this.contactDetailsLifeEventForm.$valid && this.occupationDetailsLifeEventForm.$valid && this.lifeEvent.$valid
    			&& this.coverCalculatorForm.$valid)
    	{
    		$scope.autoCalc = true;
    		 $scope.calculateUnitsForFixed().then(function(res) 
    			{
    		 		$scope.calculate();
    			 });
    	}
    };

    $scope.validateInputAmounts = function()
    {
    	
    	$scope.tpdErrorFlag = false;
		
		 if(!$scope.isTPDDisabled && $scope.TPDRequireCover != null)
			 {
			 if($scope.requireCover == null || $scope.requireCover === ''){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "You cannot apply for TPD cover without Death Cover." ;
			}
			else if(parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
			}
			
		else{
			$scope.tpdErrorFlag = false;
			$scope.tpdErrorMsg = "";
			}
			}
    	
    	
    	/*$scope.calculateOnChange();*/
    };
    
    
 $scope.calculate = function(){
	   
	 if($scope.isDeathDisabled)
		 {
		 if($scope.deathCoverLEDetails.type == '1'){
	    	   $scope.deathCoverType = 'DcUnitised';
			} else if($scope.deathCoverLEDetails.type == '2'){
				$scope.deathCoverType = 'DcFixed';
			}
		 }
	 if($scope.isTPDDisabled || $scope.hideTPD)
		 {

	    	if($scope.tpdCoverLEDetails.type == '1'){
	    		$scope.tpdCoverType = 'TPDUnitised';
	    	} else if($scope.tpdCoverLEDetails.type == '2'){
	    		$scope.tpdCoverType = 'TPDFixed';
			}
		 }
	 
	 
	   var ruleModel = {
			   "age": anb,
        		"fundCode": "VICT",
        		"gender": $scope.gender,
        		"deathOccCategory": $scope.lifeEventDeathOccCategory,
        		"tpdOccCategory": $scope.lifeEventTpdOccCategory,
        		"ipOccCategory": $scope.lifeEventIpOccCategory,
        		"smoker": false,
        		"deathFixedCost": null,
        		"deathUnitsCost": null,
        		"tpdFixedCost": null,
        		"tpdUnitsCost": null,
        		"ipUnits": parseInt($scope.ipCoverLEDetails.units),
        		"ipUnitsCost": null,
        		"premiumFrequency":$scope.premFreq,
        		"memberType": null,
        		"manageType": "ICOVER",
        		"ipCoverType": "IpUnitised",
        		"ipWaitingPeriod": $scope.ipCoverLEDetails.waitingPeriod,
        		"ipBenefitPeriod": $scope.ipCoverLEDetails.benefitPeriod
        	};

	   if($scope.deathCoverType === 'DcUnitised'){
			ruleModel.deathCoverType = 'DcUnitised';
			if($scope.isDeathDisabled)
				{
				ruleModel.deathUnits = parseInt($scope.exitingDeathUnits);
				$scope.totalDeathUnits  = parseInt($scope.exitingDeathUnits);
				}
			else
				{
				ruleModel.deathUnits = parseInt($scope.exitingDeathUnits) + parseInt($scope.requireCover);
				$scope.totalDeathUnits  = parseInt($scope.exitingDeathUnits) + parseInt($scope.requireCover);
				}
				
		} else if($scope.deathCoverType === 'DcFixed'){
			ruleModel.deathCoverType = 'DcFixed';
			if($scope.isDeathDisabled)
				{
				ruleModel.deathFixedAmount = parseInt($scope.deathCoverLEDetails.amount);
				}
			else
				{
				ruleModel.deathFixedAmount = parseInt($scope.deathCoverLEDetails.amount) + parseInt($scope.requireCover);
				}
		}

 		if($scope.tpdCoverType === 'TPDUnitised'){
			ruleModel.tpdCoverType = 'TPDUnitised';
			
			if($scope.isTPDDisabled || $scope.hideTPD)
				{
				ruleModel.tpdUnits = parseInt($scope.exitingtpdUnits);
				$scope.totalTPDUnits = parseInt($scope.exitingtpdUnits);
				}
			else
				{
			ruleModel.tpdUnits = parseInt($scope.exitingtpdUnits) + parseInt($scope.TPDRequireCover);
			$scope.totalTPDUnits = parseInt($scope.exitingtpdUnits) + parseInt($scope.TPDRequireCover);
				}
				
		} else if($scope.tpdCoverType === 'TPDFixed'){
			ruleModel.tpdCoverType = 'TPDFixed';
			if($scope.isTPDDisabled || $scope.hideTPD)
			{
				ruleModel.tpdFixedAmount = parseInt($scope.tpdCoverLEDetails.amount);
			}
			else
				{
			ruleModel.tpdFixedAmount = parseInt($scope.tpdCoverLEDetails.amount) + parseInt($scope.TPDRequireCover);
				}
		}

 		if($scope.ipCoverLEDetails.amount == undefined || $scope.ipCoverLEDetails.amount == ''){
 			ruleModel.ipFixedAmount = 0;
 		} else{
 			ruleModel.ipFixedAmount = parseInt($scope.ipCoverLEDetails.amount);
 		}
	   
	   CalculateService.calculate(ruleModel, $scope.urlList.calculateUrl).then(function(res){
 			var premium = res.data;
   		for(var i = 0; i < premium.length; i++){
   			if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
   				$scope.deathCoverAmt = premium[i].coverAmount;
   				$scope.deathCost = premium[i].cost;
   			} else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
   				$scope.tpdCoverAmt = premium[i].coverAmount;
   				$scope.tpdCost = premium[i].cost;
   			} else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
   				ipCoverAmount = (premium[i].coverAmount==null?0:premium[i].coverAmount);
   				ipWeeklyCost = (premium[i].cost==null?0:premium[i].cost);
   			}
   		}
   		$scope.totalCoverCost = parseFloat($scope.deathCost) + parseFloat($scope.tpdCost) + parseFloat(ipWeeklyCost);
   		
   		$scope.tpdTaperAmt = Math.ceil(($scope.tpdTapering * ($scope.deathCoverAmt)));
   		
		
		 if(!($scope.isTPDDisabled || $scope.hideTPD) && $scope.TPDRequireCover != null)
			 {
			 if($scope.requireCover == null || $scope.requireCover === ''){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "You cannot apply for TPD cover without Death Cover." ;
			}
			else if(parseInt($scope.tpdCoverAmt) > parseInt($scope.deathCoverAmt) )
				{
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
				}
			else if($scope.tpdCoverType === 'TPDFixed' && parseInt($scope.tpdCoverAmt)  > parseInt($scope.tpdTaperAmt) )
				{
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "The selected benefit exceeds your maximum limit of $"+ $scope.tpdTaperAmt +" , please select a lower extra cover required option" ;
				}
			/*else if(parseInt($scope.tpdCoverAmt) <= parseInt($scope.deathCoverAmt) && parseInt($scope.TPDRequireCover) > parseInt($scope.requireCover)){
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "Your TPD cover amount should not be greater than Death cover amount." ;
			}*/
			
		else{
			$scope.tpdErrorFlag = false;
			$scope.tpdErrorMsg = "";
			}
			}
		 if(parseInt($scope.tpdCoverAmt) > 5000000)
			{
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "The selected benefit exceeds your maximum limit, please select a lower extra cover required option";
			}
   		
 		}, function(err){
 			console.info("Error while calculating life event premium..", JSON.stringify(err));
 		});
	   
   };

  	$scope.continueToNextPage = function(){
  		/*$scope.validateInputAmounts();*/
  		if(this.contactDetailsLifeEventForm.$valid && this.occupationDetailsLifeEventForm.$valid && this.lifeEvent.$valid
  				&& this.coverCalculatorForm.$valid && !$scope.tpdErrorFlag){
  			
  			 $scope.saveDataForPersistence().then(function() {
  		          $scope.go('/lifeeventsummary/1');
  		        }, function(err) {
  		            //console.log(err);
  		        });
  		}
  	};

  	/* Validating event date start
    *  Do not reuse this block
    *  Need to revisit, need to write the logic in small functions
    */
    $scope.isValidEventDate = function(eventDate) {
    	if($scope.eventDate)
    		{
      $scope.eventDate = eventDate.replace(/[^0-9\/]/g, "");
    		}
    	else if($scope.coverSecFlag)
		{
   		 $scope.toggleFour(false);
   		}
    }
    $scope.validateEventDate = function(eventDate, formName, inputNmae) {
    	
    	$scope[formName][inputNmae].$setTouched();
   	 
        if(!eventDate)
          return false;
        var dateObj = new Date();
       /* var sixMonthsOldDate = dateObj.setMonth(dateObj.getMonth() - 6);*/
        var sixtydays = moment().diff(moment($scope.eventDate, 'DD-MM-YYYY'), 'days');
        var userEventDate = new Date($scope.convertDate(eventDate));
        var joiningDate = new Date($scope.convertDate(doj));
        var dateYear = moment($scope.eventDate,"DD/MM/YYYY").year();
        $scope[formName][inputNmae].$setValidity('futureDate', true);
        $scope[formName][inputNmae].$setValidity('notInRange', true);
        $scope[formName][inputNmae].$setValidity('notInRangeWithJoining', true);
        if(!$scope.isValidDate(userEventDate) ) {
          $scope.eventDate = '';
        }else if(userEventDate.withoutTime() > new Date().withoutTime()){
          var isValidDate = userEventDate.withoutTime() < new Date().withoutTime();
      	$scope[formName][inputNmae].$setValidity('futureDate', isValidDate);
      	var d = eventDate.split("/");
          $scope.eventDate = [$scope.datePadding(parseInt(d[0])), $scope.datePadding(parseInt(d[1])), dateYear].join('/');
        } 
        else if(userEventDate.withoutTime() < new Date(joiningDate).withoutTime())
      	  {
      	  var validDate = userEventDate.withoutTime() >= new Date(joiningDate).withoutTime();
            $scope[formName][inputNmae].$setValidity('notInRangeWithJoining', validDate);
            var d = eventDate.split("/");
            
            if(isNaN(dateYear)){
            	$scope[formName][inputNmae].$setValidity('futureDate', false);
            }else{
            	$scope.eventDate = [$scope.datePadding(parseInt(d[0])), $scope.datePadding(parseInt(d[1])), dateYear].join('/');
            }
      	  }
        else {
      	//var validDate = userEventDate.withoutTime() >= new Date(sixMonthsOldDate).withoutTime();
        var valDate = sixtydays<=60;
          $scope[formName][inputNmae].$setValidity('notInRange', valDate);
          var d = eventDate.split("/");
          
          if(isNaN(dateYear)){
          	$scope[formName][inputNmae].$setValidity('futureDate', false);
          }else{
          	$scope.eventDate = [$scope.datePadding(parseInt(d[0])), $scope.datePadding(parseInt(d[1])), dateYear].join('/');
          }
        }
        $scope.collapseCoverOnError(false);
      }
    // Move it to utilities
    Date.prototype.withoutTime = function () {
      var d = new Date(this);
      d.setHours(0, 0, 0, 0);
      return d;
    }

    $scope.isValidDate = function(userEventDate) {
      return userEventDate instanceof Date && isFinite(userEventDate);
    }

    $scope.datePadding = function (s) { return (s < 10) ? '0' + s : s; };
    $scope.convertDate = function(inputFormat) {
      var d = inputFormat.split("/");
      var formatedDate = [$scope.datePadding(parseInt(d[1])), $scope.datePadding(parseInt(d[0])), d[2]].join('/');
      var regEx = /^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$/;
      return regEx.test(formatedDate) ? formatedDate : '';
    }
    /*Validating event date end*/

 /* 	$scope.getLECategoryFromDB = function(fromOccupation){
  		
	  		if(fromOccupation && $scope.lifeEventOccupation!=="Other"){
	    		$scope.otherOccupationObj.lifeEventOtherOccupation = '';
	    	}
	  		if( $scope.prevOtherOcc !== $scope.otherOccupationObj.lifeEventOtherOccupation){
	  		if($scope.lifeEventOccupation != undefined || $scope.otherOccupationObj.lifeEventOtherOccupation != undefined){
	  			if(fromOccupation){
	  				$scope.withinOfficeQuestion = null;
		  		    $scope.tertiaryQuestion = null;
		  		    $scope.hazardousLifeEventQuestion = null;
		  		    $scope.lifeEventOutsideOffice = null;
	  			}
		  		var occName = $scope.lifeEventIndustry + ":" + $scope.lifeEventOccupation;
	  			if($scope.lifeEventOccupation != undefined && ($scope.otherOccupationObj.lifeEventOtherOccupation == null || $scope.otherOccupationObj.lifeEventOtherOccupation == '')){
	  				var occName = $scope.lifeEventIndustry + ":" + $scope.lifeEventOccupation;
		  	    }else if ($scope.otherOccupationObj.lifeEventOtherOccupation != undefined){
		  	    	$scope.prevOtherOcc = $scope.otherOccupationObj.lifeEventOtherOccupation;
		  	    	if(($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.lifeEventOtherOccupation))!== undefined){
		  	    		var occName = $scope.lifeEventIndustry + ":" + $scope.otherOccupationObj.lifeEventOtherOccupation;
		  	    	}else{
		  	    		var occName = $scope.lifeEventIndustry + ":" + $scope.lifeEventOccupation;
		  	    	}
		  	    	
		  	    }
		    	NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "VICT", occName).then(function(res){
		    		if($scope.deathCoverType == 'DcFixed'){
		    			deathLEDBCategory = res.data[0].deathfixedcategeory;
		    		}else if($scope.deathCoverType = 'DcUnitised'){
		    			deathLEDBCategory = res.data[0].deathunitcategeory;
		    		}
		    		if($scope.tpdCoverType == "TPDFixed"){
		    			tpdLEDBCategory = res.data[0].tpdfixedcategeory;
		    		}else if($scope.tpdCoverType == "TPDUnitised"){
		    			tpdLEDBCategory = res.data[0].tpdunitcategeory;
		    		}
		    		ipLEDBCategory = res.data[0].ipfixedcategeory;
		    		$scope.renderOccupationQuestions();
		    	}, function(err){
		    		console.info("Error while getting transfer category from DB " + JSON.stringify(err));
		    	});	    	
	  		 }
  		}
  	};*/
  	$scope.clickToOpen = function (hhText) {

    	var dialog = ngDialog.open({
    		template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
    		className: 'ngdialog-theme-plain',
    		plain: true
    	});
    	dialog.closePromise.then(function (data) {
    		//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
    	});
      };
    $scope.renderOccupationQuestions = function(){
    	var occQustnsRating = 1; 
        var overAllOccRating = 1;
       
    	if (($scope.occRating1a 
    			&& $scope.occRating1a == 'Yes') 
    				&&($scope.occRating2 && $scope.occRating2 == 'Yes') 
    			&& $scope.annualSalary > 100000){
    		occQustnsRating=3;
    	} else if (($scope.occRating1a && $scope.occRating1a == 'Yes') ||
    			($scope.occRating1b && $scope.occRating1b == 'Yes')){
    		occQustnsRating = 2;
    	}   else {
    		occQustnsRating = 1;
    	}   	
        
        if ($scope.inputMsgOccRating && occQustnsRating){
        	 overAllOccRating = Math.max($scope.inputMsgOccRating,occQustnsRating);
        } else if (occQustnsRating){
        	overAllOccRating = occQustnsRating;
        } 
      
        if (overAllOccRating == 3){
    		$scope.lifeEventDeathOccCategory = 'Professional';
    		$scope.lifeEventTpdOccCategory = 'Professional';
        	$scope.lifeEventIpOccCategory  = 'Professional';
        	
        } else if (overAllOccRating == 2) {
    		$scope.lifeEventDeathOccCategory = 'White Collar';
    		$scope.lifeEventTpdOccCategory = 'White Collar';
        	$scope.lifeEventIpOccCategory = 'White Collar';
        } else {
        	$scope.lifeEventDeathOccCategory = 'General';
        	$scope.lifeEventTpdOccCategory = 'General';
        	$scope.lifeEventIpOccCategory = 'General';
        }
        
        if(overAllOccRating == 2 || overAllOccRating == 3)
        	{
        	if($scope.ownOccuptionDeath)
    		{
        		$scope.lifeEventDeathOccCategory = 'Own Occupation';
    		}
        	if ($scope.ownOccuptionTpd)
        	{
    		$scope.lifeEventTpdOccCategory = 'Own Occupation';
        	}
        	if($scope.ipCoverLEDetails.occRating.toLowerCase() == ownoccupation && ($scope.ipCoverLEDetails.benefitPeriod === '5 Years' || $scope.ipCoverLEDetails.benefitPeriod === 'Age 65'))
    		{
    		$scope.lifeEventIpOccCategory  = 'Own Occupation';
    		}
        	}
	  		
	  	$timeout(function(){	
	  		$scope.calculateOnChange();
    		});
  	};

    /* Check if your is allowed to proceed to the next accordion */
      // TBC
      // Need to revisit, need better implementation
      $scope.isCollapsible = function(targetEle, event) {
        if( targetEle == 'collapseprivacy' && !$('#dodCkBoxLblId').hasClass('active')) {
          if($('#dodCkBoxLblId').is(':visible'))
              $scope.dodFlagErr = true;
          event.stopPropagation();
          return false;
        } else if( targetEle == 'collapseOne' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active'))) {
          if($('#privacyCkBoxLblId').is(':visible'))
              $scope.privacyFlagErr = true;
          event.stopPropagation();
          return false;
        }  else if( targetEle == 'collapseTwo' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
          if($("#collapseOne form").is(':visible'))
              $scope.lifeEventFormSubmit($scope.contactDetailsLifeEventForm);
          event.stopPropagation();
          return false;
        }  else if( targetEle == 'collapseThree' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid') || $scope.hideCoverOnNo)) {
          if($("#collapseTwo form").is(':visible'))
              $scope.lifeEventFormSubmit($scope.occupationDetailsLifeEventForm);
          event.stopPropagation();
          return false;
        } else if( targetEle == 'collapseFour' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid') || $("#collapseThree form").hasClass('ng-invalid') || $scope.hideCoverOnNo)) {
            if($("#collapseThree form").is(':visible'))
                $scope.lifeEventFormSubmit($scope.coverCalculatorForm);
            event.stopPropagation();
            return false;
          }
      }
      
      
      $scope.syncRadios = function(val) {
	        var radios = $('label[radio-sync]');
	        var data = $('input[data-sync]');
	        data.filter('[data-sync="' + val + '"]').attr('checked','checked');
	        var innerVal = "0units";
	        if(val == 'Fixed') {    	
	          $scope.deathErrorFlag = false;
	          $scope.deathErrorMsg="";
	          $scope.tpdErrorFlag = false;
	          $scope.tpdErrorMsg ="";	         
	          $scope.deathCoverType = 'DcFixed';
	          $scope.tpdCoverType = 'TPDFixed';
	    	  showhide('dollar1','nodollar1');
	          showhide('dollar','nodollar');
	          $scope.totalDeathUnits =null;
	          $scope.totalTPDUnits = null;
	        } else if(val == 'Unitised') {    	
	          $scope.deathErrorFlag = false;
	          $scope.deathErrorMsg="";
	          $scope.tpdErrorFlag = false;
	          $scope.tpdErrorMsg ="";	          
	          $scope.deathCoverType = 'DcUnitised';
	          $scope.tpdCoverType = 'TPDUnitised';
	    	  showhide('nodollar1','dollar1');
	          showhide('nodollar','dollar');
	          
	        }
	        radios.removeClass('active');
	        radios.filter('[radio-sync="' + val + '"]').addClass('active');
	        radios.filter('[radio-sync="' + innerVal + '"]').addClass('active');
	        $scope.requireCover = 0;
	        $scope.TPDRequireCover = 0;
	        $timeout(function(){
	        $scope.calculateOnChange();
	        });
	      };

    /* TBC */
    // privacy section
    $scope.togglePrivacy = function(checkFlag) {
        $scope.privacyCol = checkFlag;
        if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
    };

    // contact section
    $scope.toggleContact = function(checkFlag) {
        $scope.contactCol = checkFlag;
        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseOne']").click(); /* Can be improved */

    };

    // occupation section
  	$scope.toggleTwo = function(checkFlag) {
        $scope.coltwo = checkFlag;
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };

    // life Event section
  	$scope.toggleThree = function(checkFlag) {
        $scope.colthree = checkFlag;
        if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseThree']").click(); /* Can be improved */
    };
    
 // life Event section
  	$scope.toggleFour = function(checkFlag) {
        $scope.colthree = checkFlag;
        if((checkFlag && $('#collapseFour').hasClass('collapse in')) || (!checkFlag && !$('#collapseFour').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseFour']").click(); /* Can be improved */
    };


  // validation for DOD checkbox
    $scope.checkDodState = function(){
      $timeout(function() {
        $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodCkBoxLblId').hasClass('active') : !$scope.dodFlagErr;
        if($('#dodCkBoxLblId').hasClass('active')) {
          $scope.togglePrivacy(true);
        } else {
          $scope.togglePrivacy(false);
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
      }, 1);
    };

    // validation for Privacy checkbox
    $scope.checkPrivacyState  = function(){
      $timeout(function() {
        $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyCkBoxLblId').hasClass('active') : !$scope.privacyFlagErr;
        if($('#privacyCkBoxLblId').hasClass('active')) {
          $scope.toggleContact(true);
        } else {
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
      }, 1);
    };

    $scope.checkOwnBusinessQuestion = function(){
    	$scope.ownBussinessYesQuestion = null;
    	$scope.ownBussinessNoQuestion = null;
    	if($scope.ownBussinessQuestion == 'Yes'){
    		//occupationDetailsLifeEventFormFields = ['ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation'];
    		occupationDetailsLifeEventFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','annualSalary'];
    		//occupationDetailsOtherLifeEventFormFields = ['ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation'];
    		occupationDetailsOtherLifeEventFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation','annualSalary'];
	    } else if($scope.ownBussinessQuestion == 'No'){
	    	/*occupationDetailsLifeEventFormFields = ['ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation'];
	    	occupationDetailsOtherLifeEventFormFields = ['ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation'];*/
	    	occupationDetailsLifeEventFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','annualSalary'];
    		occupationDetailsOtherLifeEventFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzLifeEventQuestion','lifeEventIndustry','lifeEventOccupation','lifeEventOtherOccupation','annualSalary'];
	    }
    };

    $scope.checkLifeEventFormPreviousMandatoryFields  = function (elementName,formName){
    	var lifeEventFormFields;
    	if(formName == 'contactDetailsLifeEventForm'){
    		lifeEventFormFields = contactDetailsLifeEventFormFields;
    	} else if(formName == 'occupationDetailsLifeEventForm'){
    			lifeEventFormFields = occupationDetailsLifeEventFormFields;
    			 if(elementName == 'occRating1a' && $scope.occRating1a == 'Yes') {
    		    	  $scope.occRating1b = 'No'
    		      }
    		      if(elementName == 'occRating1b' && $scope.occRating1b == 'Yes') {
    		    	  $scope.occRating1a = 'No'
    		      }
    		      if (elementName == 'occRating1a' || elementName == 'occRating1b' || elementName == 'occgovrating2') {
    		    	  $scope.renderOccupationQuestions();
    		      }
    	} else if(formName == 'lifeEvent'){
    			lifeEventFormFields = lifeEventFields;
    	}
    	else if(formName == 'coverCalculatorForm')
    		{
    		lifeEventFormFields = CoverCalculatorFormFields;
    		}
      var inx = lifeEventFormFields.indexOf(elementName);
      if(inx > 0){
        for(var i = 0; i < inx ; i++){
          $scope[formName][lifeEventFormFields[i]].$touched = true;
        }
      }
    };
    $scope.checkAnnualSalary = function() {
	    if(parseInt($scope.annualSalary) > 1000000){
	      $scope.invalidSalAmount = true;
	    } else{
	      $scope.invalidSalAmount = false;
	    }
	    if(!$scope.invalidSalAmount)
	      $scope.renderOccupationQuestions();
	  }
    $scope.lifeEventFormSubmit =  function (form){
      if(form.$name == 'lifeEvent' && $("#transferitrId_div_id").is(":visible")) {
        if($scope.files && $scope.files.length) {
          $scope.fileNotUploadedError = false;
        } else {
          $scope.fileNotUploadedError = true;
          return false;
        }
      }


      if(!form.$valid){
    	  form.$submitted=true;
        if(form.$name == 'contactDetailsLifeEventForm'){
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
      } else if(form.$name == 'occupationDetailsLifeEventForm'){
        $scope.toggleThree(false);
        $scope.toggleFour(false);
      }
	  } else{
		  if(form.$name == 'contactDetailsLifeEventForm'){
    	    $scope.toggleTwo(true);    	    
		  } else if(form.$name == 'occupationDetailsLifeEventForm'){
			  $scope.continueButtonHit = true;
			  if($scope.hideCoverOnNo)
    		  {
    		  return false;
    		  }
			  $scope.toggleThree(true);
		  } else if(form.$name == 'lifeEvent'){
			  if($scope.eventAlreadyApplied != 'Yes'){
				  if(!$scope.invalidSalAmount)
				  {
			 // $scope.continueToNextPage(true);
					  $scope.toggleFour(true);
					  $scope.coverSecFlag = true;
					  if(!$scope.autoCalc)
						  {
						  $scope.calculateUnitsForFixed();
						  }
					  $scope.calculateOnChange();
				  }
			  else
				  {
				  return false;
				  }
		  }
		  }
		  else if(form.$name == 'coverCalculatorForm')
			  {
			  if($scope.eventAlreadyApplied != 'Yes'){
			  $scope.continueToNextPage();
			  }
			  else
				  {
				  return false;
				  }
			  }
       }
    };
$scope.collapseCoverOnError = function (flag){
	if(flag)
		{
	$scope.checkLifeEventFormPreviousMandatoryFields('eventAlreadyApplied','lifeEvent');
		}
	if($scope.eventAlreadyApplied === 'Yes' || $scope.fileNotUploadedError || $scope.fileSizeErrFlag || !this.lifeEvent.$valid)
	{
		$scope.toggleFour(false);
	}
	else if(this.lifeEvent.$valid && $scope.coverSecFlag)
		{
		 $scope.toggleFour(true);
		}
};
    $scope.uploadFiles = function(files, errFiles) {
      $scope.fileSizeErrFlag = false;
    	$scope.fileFormatError = errFiles.length > 0 ? true : false;
      $scope.selectedFile =  files[0];
    };

    $scope.addFilesToStack = function () {
    	 $scope.corruptedFile = false;
 		$scope.corruptedFileMsg = '';
		var fileSize = ($scope.selectedFile.size / 1048576).toFixed(3);
		if(fileSize > 10) {
			$scope.fileSizeErrFlag=true;
			$scope.fileSizeErrorMsg ="File size should not be more than 10MB";
			$scope.selectedFile = null;
			return;
		}else{
			$scope.fileSizeErrFlag=false;
		}
    if(!$scope.files)
      $scope.files = [];
		$scope.files.push($scope.selectedFile);
		//PersistenceService.setUploadedFileDetails($scope.files);
    $scope.fileNotUploadedError = false;
		$scope.selectedFile = null;
		$scope.collapseCoverOnError(false);
	};

  $scope.removeFile = function(index) {
	  $scope.corruptedFile = false;
		$scope.corruptedFileMsg = '';
    $scope.files.splice(index, 1);
    PersistenceService.setUploadedFileDetails($scope.files);
    if($scope.files.length < 1) {
      $scope.fileNotUploadedError = true;
    }
    $scope.collapseCoverOnError(false);
  }

	$scope.submitFiles = function () {
		$scope.corruptedFile = false;
		$scope.corruptedFileMsg = '';
	    $scope.uploadedFiles = $scope.uploadedFiles || [];
	    var defer = $q.defer();
	    if(!$scope.files){
	      $scope.files = [];
	    }
	    if(!$scope.files.length) {
	      defer.resolve({});
	    }
	    var upload;
	    var numOfFiles = $scope.files.length;
	    angular.forEach($scope.files, function(file, index) {
	      if(Upload.isFile(file)) {
	        upload = Upload.http({
	          url: $scope.urlList.fileUploadPostUrl,
	          headers : {
	            'Content-Type': file.name,
	            'Authorization':tokenNumService.getTokenId()
	          },
	          data: file
	        });
	        upload.then(function(res){
	        	numOfFiles--;
	          $scope.uploadedFiles[index] = res.data;
	          if(numOfFiles == 0){
	            PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
	            defer.resolve(res);
	          }
	        }, function(err){
	          //console.log("Error uploading the file " + err);
	        	if (err.status == '403'){
	        		$scope.files.splice(index, 1);
	        		PersistenceService.setUploadedFileDetails($scope.files);
	        	    if($scope.files.length < 1) {
	        	      $scope.fileNotUploadedError = true;
	        	    }
	        	    $scope.collapseCoverOnError(false);
	        		$scope.corruptedFile = true;
	        		$scope.corruptedFileMsg = 'Your document is not a valid document type or corrupted,  Please edit your document and resubmit';
	        	}
	          defer.reject(err);
	        });
	      } else {
	    	  numOfFiles--;
	        if(numOfFiles == 0) {
	          PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
	          defer.resolve({});
	        }
	      }
	    });
	    return defer.promise;
	  };

      $scope.saveDataForPersistence = function(){
        var defer = $q.defer();
	    	var coverObj = {};
	    	var coverStateObj ={};
	    	var lifeEventOccObj={};
	    	var deathAddnlCoverObj={};
	    	var tpdAddnlCoverObj={};
	    	/*var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
	    		return $scope.lifeEventIndustry == obj.key;
	    	});*/
	    	coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
	    	coverObj['firstName'] = $scope.personalDetails.firstName;
	    	coverObj['lastName'] = $scope.personalDetails.lastName;
	    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
	    	coverObj['country'] =persoanlDetailService.getMemberDetails().address.country;
	    	coverObj['email'] = $scope.lifeEventEmail;
	    	coverObj['contactType']=$scope.preferredContactType;
	    	coverObj['contactPhone'] = $scope.lifeEventPhone;
	    	coverObj['contactPrefTime'] = $scope.lifeEventTime;

	    	lifeEventOccObj['gender']=$scope.gender;
	    	lifeEventOccObj['citizenQue'] = $scope.areyouperCitzLifeEventQuestion;
	    	lifeEventOccObj['fifteenHr'] = $scope.fifteenHrsQuestion;
	    	
	    	lifeEventOccObj['occRating1a'] = $scope.occRating1a;
	    	lifeEventOccObj['occRating1b'] = $scope.occRating1b;
	    	lifeEventOccObj['occRating2'] = $scope.occRating2;
	    	
	    	lifeEventOccObj['salary'] = $scope.annualSalary;

	    	coverObj['eventName'] = $scope.event.cde;
	    	coverObj['eventDesc'] = $scope.event.desc;
	    	coverObj['eventDate'] = $scope.eventDate;
	    	coverObj['eventAlreadyApplied'] = $scope.eventAlreadyApplied;
	    	
	    	coverObj['deathOccCategory'] = $scope.lifeEventDeathOccCategory;
	    	coverObj['tpdOccCategory'] = $scope.lifeEventTpdOccCategory;
	    	coverObj['ipOccCategory'] = $scope.lifeEventIpOccCategory;
	    	coverObj['deathAmt'] = parseFloat($scope.deathCoverLEDetails.amount);
	    	coverObj['tpdAmt'] = parseFloat($scope.tpdCoverLEDetails.amount);
	    	coverObj['ipAmt'] = parseFloat($scope.ipCoverLEDetails.amount);
	    	coverObj['deathNewAmt'] = parseFloat($scope.deathCoverAmt);
	    	coverObj['tpdNewAmt'] = parseFloat($scope.tpdCoverAmt);
	    	coverObj['ipNewAmt'] = parseFloat(ipCoverAmount);
	    	/*coverObj['ipLifeUnits'] = $scope.ipCoverLEDetails.units;*/
	    	coverObj['ownOccuptionDeath'] = $scope.ownOccuptionDeath;
	    	coverObj['ownOccuptionTpd'] = $scope.ownOccuptionTpd;
	    	
	    	deathAddnlCoverObj['deathCoverType'] = $scope.deathCoverType;
	    	deathAddnlCoverObj['deathFixedAmt'] = parseFloat($scope.deathCoverAmt);
	    	deathAddnlCoverObj['deathInputTextValue'] = parseFloat($scope.requireCover || 0);
	    	deathAddnlCoverObj['deathCoverPremium'] = parseFloat($scope.deathCost);
	    	
	    	tpdAddnlCoverObj['tpdCoverType'] = $scope.tpdCoverType;
            tpdAddnlCoverObj['tpdFixedAmt'] = parseFloat($scope.tpdCoverAmt);
            tpdAddnlCoverObj['tpdInputTextValue'] = parseFloat($scope.TPDRequireCover || 0);
            tpdAddnlCoverObj['tpdCoverPremium'] = $scope.tpdCost;
	    	
            deathAddnlCoverObj['totalDeathUnits'] = $scope.totalDeathUnits;
            tpdAddnlCoverObj['totalTPDUnits'] = $scope.totalTPDUnits;
	    	if($scope.ipCoverLEDetails.waitingPeriod){
	    		coverObj['waitingPeriod'] = $scope.ipCoverLEDetails.waitingPeriod;
	    	}else{
	    		coverObj['waitingPeriod'] = "90 days";
	    	}				    	
	    	if($scope.ipCoverLEDetails.benefitPeriod){
	    		coverObj['benefitPeriod'] = $scope.ipCoverLEDetails.benefitPeriod;
	    	}else{
	    		coverObj['benefitPeriod'] = "2 Years";
	    	}
	    	
	    	coverObj['appNum'] = appNum;
	    	coverObj['dodCheck'] = $('#dodCkBoxLblId').hasClass('active');
                  coverObj['privacyCheck'] = $('#privacyCkBoxLblId').hasClass('active');
	    	coverObj['lastSavedOn'] = 'LifeEventPage';
	    	coverObj['age'] = anb;
        coverObj['manageType'] = 'ICOVER';
        coverObj['partnerCode'] = 'VICT';

        coverObj['totalPremium'] = parseFloat($scope.totalCoverCost);
        coverObj['deathCoverPremium'] =parseFloat($scope.deathCost);
        coverObj['tpdCoverPremium'] = parseFloat($scope.tpdCost);
        coverObj['ipCoverPremium'] = parseFloat(ipWeeklyCost);

        coverObj['deathLifeCoverType'] = $scope.deathCoverType;
        coverObj['tpdLifeCoverType'] = $scope.tpdCoverType;
        coverObj['ipLifeCoverType'] = 'IpUnitised';
        coverObj['freqCostType'] = $scope.premFreq;

        coverObj['deathLifeUnits'] = $scope.deathUnits;
        coverObj['tpdLifeUnits'] = $scope.tpdUnits;
        /*uploaded transfer documents*/
        $scope.submitFiles().then(function(res) {
		    	PersistenceService.setlifeEventCoverDetails(coverObj);
		    	PersistenceService.setlifeEventCoverStateDetails(coverStateObj);
		    	PersistenceService.setLifeEventCoverOccDetails(lifeEventOccObj);
		    	PersistenceService.setDeathAddnlCoverDetails(deathAddnlCoverObj);
    	    	PersistenceService.setTpdAddnlCoverDetails(tpdAddnlCoverObj);
          defer.resolve(res);
        }, function(err) {
          defer.reject(err);
        });
        return defer.promise;
	    };

	    if($routeParams.mode == 2){
	    	var existingDetails = PersistenceService.getlifeEventCoverDetails();
	    	var occDetails =PersistenceService.getLifeEventCoverOccDetails();
	    	var stateDetails = PersistenceService.getlifeEventCoverStateDetails();
	    	var deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	    	var tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();

	    	$scope.lifeEventEmail = existingDetails.email;
	    	$scope.preferredContactType = existingDetails.contactType;
	    	$scope.lifeEventPhone = existingDetails.contactPhone;
	    	$scope.lifeEventTime = existingDetails.contactPrefTime;

	    	$scope.areyouperCitzLifeEventQuestion = occDetails.citizenQue;
	    	$scope.occRating1a = occDetails.occRating1a;
	    	$scope.occRating1b = occDetails.occRating1b;
	    	$scope.occRating2 = occDetails.occRating2;
	        
	        $scope.fifteenHrsQuestion = occDetails.fifteenHr;
	        $scope.annualSalary = occDetails.salary;
	        
	        $scope.showLifeEventOutsideOffice = stateDetails.showLifeEventOutsideOffice;
        	$scope.showhazardousLifeEventQuestion = stateDetails.showhazardousLifeEventQuestion;
        	$scope.gender = occDetails.gender;
	    //	$scope.event=existingDetails.eventName;
	    	$scope.eventDate=existingDetails.eventDate;
	    	$scope.eventAlreadyApplied=existingDetails.eventAlreadyApplied;
	    	
	    	appNum = existingDetails.appNum;
	    	ackCheck = existingDetails.ackCheck;
	    	dodCheck = existingDetails.dodCheck;
	    	privacyCheck = existingDetails.privacyCheck;

	    	$scope.files = PersistenceService.getUploadedFileDetails();
	    	$scope.uploadedFiles = $scope.files;

	    	var tempEvent = $scope.eventList.filter(function(obj){
	    		return obj.cde == existingDetails.eventName;
	    	});
	    	$scope.event = tempEvent[0];
	    	
	    	$scope.premFreq = existingDetails.freqCostType;
	    	$scope.deathCoverType = deathAddnlDetails.deathCoverType;
	    	$scope.deathCoverAmt = deathAddnlDetails.deathFixedAmt;
	    	$scope.requireCover = deathAddnlDetails.deathInputTextValue;
	    	$scope.deathCost = deathAddnlDetails.deathCoverPremium;
	    	$scope.tpdCoverType = tpdAddnlDetails.tpdCoverType;
	    	$scope.tpdCoverAmt = tpdAddnlDetails.tpdFixedAmt;
	    	$scope.TPDRequireCover = tpdAddnlDetails.tpdInputTextValue;
	    	$scope.tpdCost = tpdAddnlDetails.tpdCoverPremium;
	    	
	    	$scope.ownOccuptionDeath = existingDetails.ownOccuptionDeath;
			$scope.ownOccuptionTpd = existingDetails.ownOccuptionTpd;
	    	
	    	$scope.ipCoverLEDetails.amount = existingDetails.ipAmt;
	    	ipCoverAmount = existingDetails.ipNewAmt;
	    	ipWeeklyCost = existingDetails.ipCoverPremium;
	    	$scope.totalDeathUnits =deathAddnlDetails.totalDeathUnits;
	    	$scope.totalTPDUnits = tpdAddnlDetails.totalTPDUnits;
	    	$scope.renderOccupationQuestions();
	    	
	    	if($scope.deathCoverType === 'DcUnitised')
			{
	    		$timeout(function() {
	    	        showhide('nodollar1','dollar1');
	    	        showhide('nodollar','dollar');
	    	      },500);
			}
	    	else
			{
	    		 $timeout(function() {
	     		 	showhide('dollar1','nodollar1');
	     		 	showhide('dollar','nodollar');
	     	      },500);
			}
	    	
	    	
	    	if(dodCheck){
	    	    $timeout(function(){
	    			$('#dodCkBoxLblId').addClass('active');
			   });
			 }
	    	if(privacyCheck){
	    	    $timeout(function(){
	    			$('#privacyCkBoxLblId').addClass('active');
			   });
			 }

	    	$scope.togglePrivacy(true);
	    	$scope.toggleContact(true);
	    	$scope.toggleTwo(true);
	    	$scope.toggleThree(true);
	    	$scope.toggleFour(true);
	    };

	    if($routeParams.mode == 3){
	    	 mode3Flag = true;
	    	 var num = PersistenceService.getAppNumToBeRetrieved();
	    	 RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
	    		var appDetails = res.data[0];

	    		$scope.lifeEventEmail = appDetails.email;
		    	$scope.preferredContactType = appDetails.contactType;
		    	$scope.lifeEventPhone = appDetails.contactPhone;
		    	$scope.lifeEventTime = appDetails.contactPrefTime;
		    	$scope.files = appDetails.lifeEventDocuments;
		    	$scope.uploadedFiles = $scope.files;

		    	$scope.areyouperCitzLifeEventQuestion = appDetails.occupationDetails.citizenQue;
		    	$scope.occRating1a = appDetails.occupationDetails.occRating1a;
		    	$scope.occRating1b = appDetails.occupationDetails.occRating1b;
		    	$scope.occRating2 = appDetails.occupationDetails.occRating2;
		    	$scope.gender = appDetails.occupationDetails.gender;
		        $scope.fifteenHrsQuestion = appDetails.occupationDetails.fifteenHr;
            	$scope.annualSalary = appDetails.occupationDetails.salary;
		        
		      //  $scope.event=appDetails.eventName;
		    	$scope.eventDate=appDetails.eventDate;
		    	$scope.eventAlreadyApplied=appDetails.eventAlreadyApplied;
		    	
		    	appNum = appDetails.appNum;
		    	ackCheck = appDetails.ackCheck;
		    	dodCheck = appDetails.dodCheck;
		    	privacyCheck = appDetails.privacyCheck;
		    	
		    	$scope.premFreq = appDetails.freqCostType;
		    	$scope.deathCoverType = appDetails.addnlDeathCoverDetails.deathCoverType;
		    	$scope.deathCoverAmt = appDetails.addnlDeathCoverDetails.deathFixedAmt;
		    	$scope.requireCover = appDetails.addnlDeathCoverDetails.deathInputTextValue;
		    	$scope.deathCost = appDetails.addnlDeathCoverDetails.deathCoverPremium;
		    	$scope.tpdCoverType = appDetails.addnlTpdCoverDetails.tpdCoverType;
		    	$scope.tpdCoverAmt = appDetails.addnlTpdCoverDetails.tpdFixedAmt;
		    	$scope.TPDRequireCover = appDetails.addnlTpdCoverDetails.tpdInputTextValue;
		    	$scope.tpdCost = appDetails.addnlTpdCoverDetails.tpdCoverPremium;
		    	
		    	$scope.ownOccuptionDeath = appDetails.ownOccuptionDeath;
				$scope.ownOccuptionTpd = appDetails.ownOccuptionTpd;
		    	
		    	$scope.ipCoverLEDetails.amount = appDetails.existingIPAmount;
		    	ipCoverAmount = appDetails.ipNewAmt;
		    	ipWeeklyCost = appDetails.ipCoverPremium;
		    	
		    	$scope.totalDeathUnits =appDetails.addnlDeathCoverDetails.totalDeathUnits;
		    	$scope.totalTPDUnits = appDetails.addnlTpdCoverDetails.totalTPDUnits;
		    	
		    	//$scope.files = PersistenceService.getUploadedFileDetails();
		    	var tempEvt = $scope.eventList.filter(function(obj){
		    		return obj.cde == appDetails.eventName;
		    	});
		    	$scope.event = tempEvt[0];

		    	$scope.renderOccupationQuestions();
		    	
		    	$('#dodCkBoxLblId').addClass('active');
                $('#privacyCkBoxLblId').addClass('active');
                
                
                if($scope.deathCoverType === 'DcUnitised')
    			{
    	    		$timeout(function() {
    	    	        showhide('nodollar1','dollar1');
    	    	        showhide('nodollar','dollar');
    	    	      },500);
    			}
    	    	else
    			{
    	    		 $timeout(function() {
    	     		 	showhide('dollar1','nodollar1');
    	     		 	showhide('dollar','nodollar');
    	     	      },500);
    			}

		    	$scope.togglePrivacy(true);
		    	$scope.toggleContact(true);
		    	$scope.toggleTwo(true);
		    	$scope.toggleThree(true);
		    	$scope.toggleFour(true);
		    	
		    	$scope.validateEventDate($scope.eventDate,'lifeEvent','eventDate');

	    	 },function(err){
		    		console.info("Error fetching the saved app details " + err);
	    	 });
	    }

	    $scope.goToAura = function(){
	    	if(this.contactDetailsLifeEventForm.$valid && this.occupationDetailsTransferForm.$valid && this.previousCoverForm.$valid && this.coverCalculatorForm.$valid){
	    		$rootScope.$broadcast('disablepointer');
	    		$timeout(function(){
	    			//$scope.saveDataForPersistence();
					  // submit uploaded to server
				    //$scope.submitFiles();
		    	 //$scope.go('/auralifeevent/1');

            $scope.saveDataForPersistence().then(function() {
              $scope.go('/lifeeventsummary/1');
            }, function(err) {
                //console.log(err);
            });
		      }, 10);
	    	}
	    };

	    $scope.saveQuoteLifeEvent = function() {
	    	$scope.saveDataForPersistence().then(function() {
          $scope.quoteLifeEventObject =  PersistenceService.getlifeEventCoverDetails();
          $scope.lifeEventOccDetails =PersistenceService.getLifeEventCoverOccDetails();
          $scope.personalDetails = persoanlDetailService.getMemberDetails();
          
          var lifeEventUploadedFiles = PersistenceService.getUploadedFileDetails();
          var deathAdditionalDetails = PersistenceService.getDeathAddnlCoverDetails();
  		var tpdAdditionalDetails = PersistenceService.getTpdAddnlCoverDetails();
          if($scope.quoteLifeEventObject != null && $scope.lifeEventOccDetails != null && $scope.personalDetails != null){
            $scope.details = {};
            $scope.details.occupationDetails = $scope.lifeEventOccDetails;
            $scope.details.addnlDeathCoverDetails = deathAdditionalDetails;
            $scope.details.addnlTpdCoverDetails = tpdAdditionalDetails;
            //added for uploaded file details
            if(lifeEventUploadedFiles != null) {
              $scope.details.lifeEventDocuments = lifeEventUploadedFiles;
            }
            var temp = angular.extend($scope.quoteLifeEventObject,$scope.details)
            var saveQuoteLifeEventObject = angular.extend(temp,$scope.personalDetails);
            auraResponseService.setResponse(saveQuoteLifeEventObject);
              saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
                //console.log(response.data);
                $scope.lifeEventQuoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
              },function(err){
                //console.log("Something went wrong while saving..."+JSON.stringify(err));
              });
          }
        }, function(err) {
            //console.log(err);
        });
	    	
	    };

	    $scope.calculateUnitsForFixed = function()
	    {
	    	var defer = $q.defer();

	  		var ruleModel = {
	         		"age": anb,
	         		"fundCode": "VICT",
	         		"gender": $scope.gender,
	         		"deathOccCategory": $scope.lifeEventDeathOccCategory,
	         		"tpdOccCategory": $scope.lifeEventTpdOccCategory,
	         		"ipOccCategory": $scope.lifeEventIpOccCategory,
	         		"smoker": false,
	         		"deathFixedCost": null,
	         		"deathUnits": 1,
	         		"deathUnitsCost": null,
	         		"tpdFixedCost": null,
	         		"tpdUnits": 1,
	         		"tpdUnitsCost": null,
	         		"ipUnits": 1,
	         		"ipUnitsCost": null,
	         		"deathCoverType": "DcUnitised",
	        	    "tpdCoverType": "TPDUnitised",
	         		"premiumFrequency": "Weekly",
	         		"memberType": null,
	         		"manageType": "ICOVER",
	         		"ipCoverType": "IpUnitised",
	        		"ipWaitingPeriod": $scope.ipCoverLEDetails.waitingPeriod,
	        		"ipBenefitPeriod": $scope.ipCoverLEDetails.benefitPeriod
	         	};
	  		

	  		CalculateService.calculate(ruleModel, $scope.urlList.calculateUrl).then(function(res){
	  			var premium = res.data;
	  			var deatCoverAmt = 0;
	  			var tpdCoverAmt = 0;
	    		for(var i = 0; i < premium.length; i++){
	    			if(premium[i].coverType == 'DcUnitised'){
	    				deatCoverAmt = premium[i].coverAmount;
	    			} else if(premium[i].coverType == 'TPDUnitised'){
	    				tpdCoverAmt = premium[i].coverAmount;
	    					    			} 
	    		}
	    		 $scope.deathUnitFixAmt = deatCoverAmt;
	    		  $scope.tpdUnitFixAmt = tpdCoverAmt;
	    		  $scope.deathUnitFixAmt2 = parseInt(deatCoverAmt) * 2;
	    		  $scope.tpdUnitFixAmt2 = parseInt(tpdCoverAmt) * 2;
	        
	    		  if(parseInt($scope.deathCoverLEDetails.units) == 0)
	    			  {
	    			  $scope.exitingDeathUnits = Math.ceil($scope.deathCoverLEDetails.amount/$scope.deathUnitFixAmt);
	    			  }
	    		  if(parseInt($scope.tpdCoverLEDetails.units) == 0)
	    			  {
	    			  $scope.exitingtpdUnits =  Math.ceil($scope.tpdCoverLEDetails.amount/$scope.tpdUnitFixAmt);
	    			  }
	    		  defer.resolve(res);
	  		}, function(err){
	  			defer.reject(err);
	  			console.info("Error while calculating life event premium..", JSON.stringify(err));
	  		});
	  	  
	  		return defer.promise;
	    }
	    $scope.isUnitized = function(value){
	        return (value == "1");
	      };	    
	    $scope.lifeEventQuoteSaveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
				    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
    }]);
