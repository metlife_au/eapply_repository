/**
 * 
 */
package au.com.metlife.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import au.com.metlife.model.Document;

/**
 * @author akishore
 *
 */
@Transactional
public interface DocumentDao extends CrudRepository<Document, Long> {
	
	//public Document findBySecurestring( String securestring);	
	public Document findById(Long id);
	//public void saveDocument(Document document);

}
