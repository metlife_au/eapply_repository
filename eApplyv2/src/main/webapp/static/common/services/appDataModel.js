angular.module('appDataModel',[]);
angular.module('appDataModel')
  .service('appData',['$http','$q', function($http,$q) {
    var _this = this;
    _this.appData = {
    }
    _this.changeCoverAuraDetails = {};
    _this.PDFLocation = null;
    _this.NpsLink = null;
    _this.policyNumber = null;
    
    _this.setAppData = function(data) {
     _this.appData = data;
    };

    _this.getAppData = function(){
     return _this.appData;
    };

    _this.setQuoteData = function(data) {
     _this.appData.quotePageDetails = data;
    };

    _this.getQuoteData = function(){
     return _this.appData.quotePageDetails;
    };

    _this.setChngCoverSummaryData = function(data) {
     _this.appData.changeCoverSummary = data;
    };

    _this.getChngCoverSummaryData = function(){
     return _this.appData.changeCoverSummary;
    };

    _this.setChangeCoverAuraDetails = function(data){
      _this.changeCoverAuraDetails = data;
    };

    _this.getChangeCoverAuraDetails = function(){
      return _this.changeCoverAuraDetails;
    };

    _this.setPDFLocation = function(location){
        _this.PDFLocation = location;
    };

    _this.getPDFLocation = function(){
        try {
            if(_this.PDFLocation == 'undefined') {
                throw {message: 'Not found'};
            } else {
                return _this.PDFLocation;
            }
        } catch(Error) {
            throw Error;
        }
    };

    _this.setNpsUrl = function(url){
        _this.NpsLink = url;
    };

    _this.getNpsUrl = function(){
        try {
            if(_this.NpsLink == 'undefined') {
                throw {message: 'Not found'};
            } else {
                return _this.NpsLink;
            }
        } catch(Error) {
            throw Error;
        }
    };

 }]);
