package pages.metlife.Angular.Statewide;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class STATEWIDE_Angular_TransferYourCover extends MasterPage {
	WebDriverUtil driverUtil = null;

	public STATEWIDE_Angular_TransferYourCover TransferCover_STATEWIDE()
			throws Exception {
		TransferCoverSTATEWIDE();
		healthandlifestyledetails();
		confirmation();

		return new STATEWIDE_Angular_TransferYourCover(scriptHelper);
	}

	public STATEWIDE_Angular_TransferYourCover TransferCover_STATEWIDE_Negative() {
		TransferCoverSTATEWIDE_Negativescenario();

		return new STATEWIDE_Angular_TransferYourCover(scriptHelper);
	}

	public STATEWIDE_Angular_TransferYourCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void TransferCoverSTATEWIDE() {

		String EmailId = dataTable
				.getData("STATEWIDE_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_TransferCover",
				"TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_TransferCover",
				"ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_TransferCover",
				"TimeofContact");
		String Gender = dataTable.getData("STATEWIDE_TransferCover", "Gender");
		String FifteenHoursWork = dataTable.getData("STATEWIDE_TransferCover",
				"FifteenHoursWork");
		String Citizen = dataTable
				.getData("STATEWIDE_TransferCover", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_TransferCover",
				"IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_TransferCover",
				"OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_TransferCover",
				"OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_TransferCover",
				"OfficeEnvironment");
		String TertiaryQual = dataTable.getData("STATEWIDE_TransferCover",
				"TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_TransferCover",
				"HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_TransferCover",
				"OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("STATEWIDE_TransferCover",
				"AnnualSalary");
		String PreviousInsurer = dataTable.getData("STATEWIDE_TransferCover",
				"PreviousInsurer");
		String PolicyNo = dataTable.getData("STATEWIDE_TransferCover",
				"PolicyNo");
		String FormerFundNo = dataTable.getData("STATEWIDE_TransferCover",
				"FormerFundNo");
		String DocumentaryEvidence = dataTable.getData(
				"STATEWIDE_TransferCover", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("STATEWIDE_TransferCover",
				"AttachPath");
		String CostType = dataTable.getData("STATEWIDE_TransferCover",
				"CostType");
		String DeathYN = dataTable.getData("STATEWIDE_TransferCover", "Death");
		String TPDYN = dataTable.getData("STATEWIDE_TransferCover", "TPD");
		String IPYN = dataTable.getData("STATEWIDE_TransferCover", "IP");
		String DeathAmount = dataTable.getData("STATEWIDE_TransferCover",
				"DeathAmount");
		String TPDAmount = dataTable.getData("STATEWIDE_TransferCover",
				"TPDAmount");
		String IPAmount = dataTable.getData("STATEWIDE_TransferCover",
				"IPAmount");
		String WaitingPeriod = dataTable.getData("STATEWIDE_TransferCover",
				"WaitingPeriod");
		String BenefitPeriod = dataTable.getData("STATEWIDE_TransferCover",
				"BenefitPeriod");

		try {

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,450)", "");
			Thread.sleep(2000);

			// ****Click on Transfer Your Cover Link*****\\
			WebDriverWait waiting = new WebDriverWait(driver, 25);
			
			Thread.sleep(3000);
			
			waiting.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//h5[contains(text(),'Transfer your cover')]")))
					.click();
			
			WebDriverWait wait = new WebDriverWait(driver, 18);
			report.updateTestLog("Transfer Your Cover",
					"Clicked on Transfer Your Cover Link", Status.PASS);

			sleep(3500);
			// *****Agree to Duty of disclosure*******\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty of Disclosure",
					"Duty of Disclosure label is Checked", Status.PASS);

			// *****Agree to Privacy Statement*******\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@id='privacyCkBoxLblId']/span")))
					.click();
			sleep(350);
			report.updateTestLog("Privacy Statement",
					"Privacy Statement label is Checked", Status.PASS);

			// *****Enter the Email Id*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferEmail"))).sendKeys(
					EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId
					+ " is entered", Status.PASS);
			sleep(500);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(By
					.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "
					+ TypeofContact + " is Selected", Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferPhone"))).sendKeys(
					Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferPhone"))).sendKeys(
					ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "
					+ ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {

				driver.findElement(
						By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span"))
						.click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact
						+ " is preferred time of Contact", Status.PASS);
			} else {
				driver.findElement(
						By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span"))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact
						+ " is preferred time of Contact", Status.PASS);
			}

			// ****Contact Details-Continue Button*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']")))
					.click();
			report.updateTestLog(
					"Continue",
					"Clicked on Continue button after entering contact details",
					Status.PASS);
			sleep(1200);

			// ***************************************\\
			// **********OCCUPATION SECTION***********\\
			// ****************************************\\

			// *****Select the 15 Hours Question*******\\

			if (FifteenHoursWork.equalsIgnoreCase("Yes")) {

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id='collapseTwo']/form/div/div[1]/div[2]/div/div[1]/label/span")))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes",
						Status.PASS);

			} else {

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]")))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No",
						Status.PASS);

			}

			/*
			 * //*****Resident of Australia****\\
			 * 
			 * if(Citizen.equalsIgnoreCase("Yes")){ driver.findElement(By.xpath(
			 * "//*[@id='collapseTwo']/form/div/div[2]/div[2]/div/div[1]/label/span"
			 * )).click(); sleep(250); report.updateTestLog("Citizen or PR",
			 * "Citizen of Australia", Status.PASS);
			 * 
			 * } else{ driver.findElement(By.xpath(
			 * "//*[@id='collapseTwo']/form/div/div[2]/div[2]/div/div[2]/label/span"
			 * )).click(); sleep(250); report.updateTestLog("Citizen or PR",
			 * "Not a citizen of Australia", Status.PASS);
			 * 
			 * }
			 */
			
			
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
					
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}		
			
			
			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Do you work wholly within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}							
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
									
									
									
		//******Do you work in hazardous environment*****\\		
			
		if(HazardousEnv.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}							
									
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\	
		
		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}							
		}							
								    
          sleep(800);
		   
									
		//*****What is your annual Salary******							
									
		
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

			/*
			// *****Industry********\\
			sleep(500);
			Select Industry = new Select(driver.findElement(By
					.name("transferIndustry")));
			Industry.selectByVisibleText(IndustryType);
			sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "
					+ IndustryType, Status.PASS);
			// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your occupation?']"))).click();
			sleep(500);

			// *****Occupation*****\\

			Select Occupation = new Select(driver.findElement(By
					.name("occupationTransfer")));
			Occupation.selectByVisibleText(OccupationType);
			sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "
					+ OccupationType, Status.PASS);
			sleep(500);

			// *****Other Occupation *****\\

			if (OccupationType.equalsIgnoreCase("Other")) {

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.name("transferotherOccupation"))).sendKeys(
						OtherOccupation);
				report.updateTestLog("Other Occupation",
						"Other Occupation entered: " + OtherOccupation,
						Status.PASS);
				sleep(1000);
			}

			// *****Extra Questions*****\\

			if (driver.getPageSource().contains(
					"Do you work wholly within an office environment?")) {

				// ***Select if your office Duties are Undertaken within an
				// Office Environment?\\

				if (OfficeEnvironment.equalsIgnoreCase("Yes")) {

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span")))
							.click();
					report.updateTestLog("Work within Office Environment",
							"Selected Yes", Status.PASS);

				} else {

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span")))
							.click();
					report.updateTestLog("Work within Office Environment",
							"Selected No", Status.PASS);

				}
			}

				if (driver
						.getPageSource()
						.contains(
								"Do you hold a tertiary qualification relevant to your current occupation or are you a member of a professional body relevant to your current occupation or a member of your organisation's senior management team?")) {

					// ***Select if your office Duties are Undertaken within an
					// Office Environment?\\

					if (OfficeEnvironment.equalsIgnoreCase("Yes")) {

						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span")))
								.click();
						report.updateTestLog("Work within Office Environment",
								"Selected Yes", Status.PASS);

					} else {

						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span")))
								.click();
						report.updateTestLog("Work within Office Environment",
								"Selected No", Status.PASS);

					}

					if (driver
							.getPageSource()
							.contains(
									"Are your duties entirely undertaken within an office environment?")) {

						// ***Select if your office Duties are Undertaken within
						// an Office Environment?\\

						if (OfficeEnvironment.equalsIgnoreCase("Yes")) {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											.xpath("//*[@id='collapseTwo']/form/div/div[5]/div[3]/div/div[2]/div/div[1]/label/span")))
									.click();
							report.updateTestLog(
									"Work within Office Environment",
									"Selected Yes", Status.PASS);

						} else {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											.xpath("//*[@id='collapseTwo']/form/div/div[5]/div[3]/div/div[2]/div/div[2]/label/span")))
									.click();
							report.updateTestLog(
									"Work within Office Environment",
									"Selected No", Status.PASS);

						}

						// *****Do You Hold a Tertiary Qualification******\\

						if (TertiaryQual.equalsIgnoreCase("Yes")) {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											.xpath("//*[@id='collapseTwo']/form/div/div[5]/div[4]/div/div[2]/div/div[1]/label/span")))
									.click();
							report.updateTestLog("Tertiary Qualification",
									"Selected Yes", Status.PASS);
						} else {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											.xpath("//*[@id='collapseTwo']/form/div/div[5]/div[4]/div/div[2]/div/div[2]/label/span")))
									.click();
							report.updateTestLog("Tertiary Qualification",
									"Selected No", Status.PASS);
						}
					}

					else if (driver
							.getPageSource()
							.contains(
									"Do you work in a hazardous environment and/or perform any duties of a manual nature?")) {

						// ******Do you work in hazardous environment*****\\

						if (HazardousEnv.equalsIgnoreCase("Yes")) {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											         
											.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span")))
									.click();
							report.updateTestLog("Hazardous Environment",
									"Selected Yes", Status.PASS);
						} else {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span")))
									.click();
							report.updateTestLog("Hazardous Environment",
									"Selected No", Status.PASS);
						}

						// ******Do you spend more than 10% of your working time
						// outside of an office environment?*******\\

						if (OutsideOfcPercent.equalsIgnoreCase("Yes")) {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span")))
									.click();
							report.updateTestLog("10% work outside Ofice",
									"Selected Yes", Status.PASS);
						} else {

							wait.until(
									ExpectedConditions.elementToBeClickable(By
											        
											.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span")))
									.click();
							report.updateTestLog("10% work outside Ofice",
									"Selected No", Status.PASS);
						}
					}

					sleep(800);

					// *****What is your annual Salary******

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.name("annualTransferSalary"))).sendKeys(
							AnnualSalary);
					sleep(500);
					report.updateTestLog("Annual Salary",
							"Annual Salary Entered is: " + AnnualSalary,
							Status.PASS);*/

					// *****Click on Continue*******\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']")))
							.click();
					report.updateTestLog(
							"Continue",
							"Clicked on Continue after entering occupation details",
							Status.PASS);
					sleep(2500);

					// ******************************************\\
					// **********PREVIOUS COVER SECTION**********\\
					// *******************************************\\

					// *****What is the name of your previous fund or
					// insurer?*****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.name("previousFundName"))).sendKeys(
							PreviousInsurer);
					sleep(250);
					report.updateTestLog("Previous Fund or Insurer",
							"Previous Fund or Insurer: " + PreviousInsurer
									+ " is entered", Status.PASS);

					// *****Please enter your fund member or insurance policy
					// number*****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.name("membershipNumber"))).sendKeys(
							PolicyNo);
					sleep(250);
					report.updateTestLog("Previous Insurance Policy No",
							"Insurance Policy No: " + PolicyNo + " is entered",
							Status.PASS);

					// *****Please enter your former fund SPIN number (if
					// known)*****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.name("spinNumber")))
							.sendKeys(FormerFundNo);
					sleep(250);
					report.updateTestLog("SPIN Number", "SPIN Number: "
							+ FormerFundNo + " is entered", Status.PASS);

					// *****Do You want to Attach documentary evidence****\\

					if (DocumentaryEvidence.equalsIgnoreCase("Yes")) {

						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.xpath("//*[@id='collapseThree']/form/div/div[5]/div[2]/div/div[1]/label/span")))
								.click();
						sleep(500);
						report.updateTestLog("Documentary Evidence",
								"Yes is selected", Status.PASS);

						// ****Attach the file******

						driver.findElement(
								By.id("ngf-transferCvrFileUploadAddIdBtn"))
								.sendKeys(AttachPath);
						sleep(1500);

						// *****Let the attachment load*****\\
						driver.findElement(
								By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]"))
								.click();
						sleep(1000);

						// *****Add the Attachment*****\\

						WebElement add = driver.findElement(By
								.xpath("//span[contains(text(),'Add')]"));
						((JavascriptExecutor) driver).executeScript(
								"arguments[0].click();", add);
						sleep(1500);
						report.updateTestLog("Evidence Attachment",
								"File is successfully attached", Status.PASS);

					} else {

						// ****No Attachment *****\\

						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.xpath("//*[@id='collapseThree']/form/div/div[5]/div[2]/div/div[2]/label/span")))
								.click();
						sleep(500);
						report.updateTestLog("Documentary Evidence",
								"No is selected", Status.PASS);

					}

					// *****Choose the cost of Insurance Type******\\

					Select CostofInsurance = new Select(driver.findElement(By
							.id("coverId")));
					CostofInsurance.selectByVisibleText(CostType);
					sleep(400);
					report.updateTestLog("Cost of Insurance Type", CostType
							+ " is selected", Status.PASS);
					
					

					// ***Continue button *****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseThree']/form/div/div[8]/div/a/button")))
							.click();
					sleep(500);
					report.updateTestLog("Documentary Evidence",
							"No is selected", Status.PASS);

					// *************************************************\\
					// **************TRANSFER COVER SECTION***************\\
					// **************************************************\\

					// *****Death transfer amount******

					if (DeathYN.equalsIgnoreCase("Yes")) {
						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.id("dcTransferAmount"))).sendKeys(
								DeathAmount);
						sleep(500);
						report.updateTestLog("Death Cover Transfer Amount",
								DeathAmount + " is entered", Status.PASS);

					}

					// *****TPD transfer amount******

					if (TPDYN.equalsIgnoreCase("Yes")) {

						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.id("tpdTransferAmount"))).sendKeys(
								TPDAmount);
						sleep(500);
						report.updateTestLog("TPD Cover Transfer Amount",
								TPDAmount + " is entered", Status.PASS);

					}

					// ****IP transfer amount*****\\

					if (IPYN.equalsIgnoreCase("Yes")
							&& FifteenHoursWork.equalsIgnoreCase("Yes")) {

						// *****Select The Waiting Period********\\

						Select waitingperiod = new Select(
								driver.findElement(By
										.xpath("//select[@ng-model='waitingPeriodTransPer']")));
						waitingperiod.selectByVisibleText(WaitingPeriod);
						sleep(500);
						report.updateTestLog("IP Waiting Period", WaitingPeriod
								+ " is selected", Status.PASS);

						// *****Select The Benefit Period********\\

						Select benefitperiod = new Select(
								driver.findElement(By
										.xpath("//select[@ng-model='benefitPeriodTransPer']")));
						benefitperiod.selectByVisibleText(BenefitPeriod);
						sleep(500);
						report.updateTestLog("IP Waiting Period", BenefitPeriod
								+ " is selected", Status.PASS);

						// ****Enter the IP amount*****\\

						wait.until(
								ExpectedConditions.elementToBeClickable(By
										.id("ipTransferAmount"))).sendKeys(
								IPAmount);
						sleep(500);
						report.updateTestLog("IP Cover Transfer Amount",
								IPAmount + " is entered", Status.PASS);

					}

					// ****Equivalent units of Statewide checkbox*****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='sc']/div[4]/div[11]/div[2]/div/div[1]/label/span")))
							.click();
					report.updateTestLog("Equivalent value in unitised scale?",
							"Checkbox is selected", Status.PASS);

					// *****Click on calculate Quote*****\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']")))
							.click();
					sleep(2000);
					report.updateTestLog("Calculte Quote",
							"Calculte Quote button is selected", Status.PASS);

					if (driver
							.findElement(
									By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4"))
							.getText().equalsIgnoreCase("$0.00")) {

						driver.findElement(
								By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))
								.click();
						sleep(4000);

					}

					if (driver
							.findElement(
									By.xpath("//div[@class='col-sm-3 col-xs-6 alignright']/h4"))
							.getText().equalsIgnoreCase("$0.00")) {

						driver.findElement(
								By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))
								.click();
						sleep(4000);

					}

					// ********************************************************************************************\\
					// ************Capturing the premiums
					// displayed************************************************\\
					// ********************************************************************************************\\

					// *******Capturing the Death Cost********\\

					WebElement Death = driver
							.findElement(By
									.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[1]"));
					Death.click();
					sleep(300);
					JavascriptExecutor jsee = (JavascriptExecutor) driver;
					jsee.executeScript("window.scrollBy(0,-150)", "");
					sleep(500);
					String DeathCost = Death.getText();
					report.updateTestLog("Death Cover amount", CostType
							+ " Cost is " + DeathCost, Status.SCREENSHOT);
					sleep(500);

					// *******Capturing the TPD Cost********\\

					WebElement tpd = driver
							.findElement(By
									.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[2]"));
					tpd.click();
					sleep(500);
					String TPDCost = tpd.getText();
					report.updateTestLog("TPD Cover amount", CostType+ " Cost is " + TPDCost, Status.SCREENSHOT);
					sleep(500);
					// *******Capturing the IP Cost********\\

					WebElement ip = driver
							.findElement(By
									.xpath("(//div[@class='col-sm-3 alignright col-xs-6']/h4)[3]"));
					ip.click();
					sleep(500);
					String IPCost = ip.getText();

					report.updateTestLog("IP Cover amount", CostType
							+ " Cost is " + IPCost, Status.SCREENSHOT);
					sleep(500);
					// *****Scroll to the Bottom of the Page

					jse.executeScript("window.scrollBy(0,250)", "");

					// ******Click on Continue******\\

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("(//button[@class='btn btn-primary w100p ng-scope'])[2]")))
							.click();
					sleep(2500);
					report.updateTestLog("Continue",
							"Continue button is selected", Status.PASS);
					Thread.sleep(6000);
				
		

		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void healthandlifestyledetails() {

		String RestrictedIllness = dataTable.getData("STATEWIDE_TransferCover",
				"RestrictedIllness");
		String PriorClaim = dataTable.getData("STATEWIDE_TransferCover",
				"PriorClaim");
		String RestrictedFromWork = dataTable.getData(
				"STATEWIDE_TransferCover", "RestrictedFromWork");
		String DiagnosedIllness = dataTable.getData("STATEWIDE_TransferCover",
				"DiagnosedIllness");
		String MedicalTreatment = dataTable.getData("STATEWIDE_TransferCover",
				"MedicalTreatment");
		String DeclinedApplication = dataTable.getData(
				"STATEWIDE_TransferCover", "DeclinedApplication");
		String Fundpremloading = dataTable.getData("STATEWIDE_TransferCover",
				"Fundpremloading");

		WebDriverWait wait = new WebDriverWait(driver, 20);

		try {
			Thread.sleep(3000);
			if (driver.getPageSource().contains("Eligibility Check")) {

				// *******Are you restricted due to illness or injury*********\\

				if (RestrictedIllness.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[2]/div/div[2]/div[1]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Restricted due to illness-Yes", Status.PASS);
					sleep(1000);

				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[2]/div/div[2]/div[2]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Restricted due to illness-No", Status.PASS);
					sleep(1000);

				}

				// *******Are you contemplating or have you ever made a claim
				// for sickness, accident or disability benefits,
				// Workers� Compensation or any other form of compensation due
				// to illness or injury?*********\\

				if (PriorClaim.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[3]/div/div[2]/div[1]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Prior Claim-Yes", Status.PASS);
					sleep(1000);

				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[3]/div/div[2]/div[2]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Prior Claim-No", Status.PASS);
					sleep(1000);

				}

				// *******Have you been restricted from work or unable to
				// perform any of your regular duties for more than
				// seven consecutive days over the past 12 months due to illness
				// or injury (other than for colds or flu)?*********\\

				if (RestrictedFromWork.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[4]/div/div[2]/div[1]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Restricted From Work-Yes", Status.PASS);
					sleep(1000);

				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[4]/div/div[2]/div[2]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Restricted From Work-No", Status.PASS);
					sleep(1000);

				}

				// *****Have you been diagnosed with an illness that in a
				// doctor�s opinion reduces your life expectancy
				// to less than 3 years?

				if (DiagnosedIllness.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[5]/div/div[2]/div[1]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Diagnosed with illness-Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[5]/div/div[2]/div[2]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Diagnosed with illness-No", Status.PASS);
					sleep(1000);
				}

				// *****Are you currently contemplating any medical treatment or
				// advice for any illness or injury for which you
				// have not previously consulted a medical practitioner or an
				// existing illness or injury, which appears to be
				// deteriorating?

				if (MedicalTreatment.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[6]/div/div[2]/div[1]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Medical Treatment-Yes", Status.PASS);
					sleep(1000);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[6]/div/div[2]/div[2]/div/label/span")))
							.click();
					report.updateTestLog("Health and Lifestyle Page",
							"Medical Treatment-No", Status.PASS);
					sleep(1000);
				}

				// ******Have you had an application for Life, TPD, Trauma or
				// Salary Continuance insurance declined by an insurer?*****\\

				if (DeclinedApplication.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[7]/div/div[2]/div[1]/div/label/span")))
							.click();
					sleep(1000);
					report.updateTestLog("Health and Lifestyle Page",
							"Declined Application-Yes", Status.PASS);
					sleep(1000);
				} else {

					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[7]/div/div[2]/div[2]/div/label/span")))
							.click();
					sleep(1000);
					report.updateTestLog("Health and Lifestyle Page",
							"Declined Application-No", Status.PASS);
					sleep(1000);
				}

				// *******Is your cover under the previous fund or individual
				// insurer subject to any premium loadings
				// and/or exclusions, including but not limited to pre-existing
				// conditions exclusions, or restrictions in
				// regards to medical or other conditions?

				if (Fundpremloading.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[6]/div/div[2]/div[1]/div/label/span")))
							.click();
					sleep(200);
					report.updateTestLog("Health and Lifestyle Page",
							"Previous fund with Loading or exclusions-Yes",
							Status.PASS);
					sleep(2000);
				} else {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//*[@id='collapseOne']/div/div[6]/div/div[2]/div[2]/div/label/span")))
							.click();
					sleep(1000);
					report.updateTestLog("Health and Lifestyle Page",
							"Previous fund with Loading or exclusions-No",
							Status.PASS);
					sleep(2000);
				}

				
				WebDriverWait waitt = new WebDriverWait(driver, 45);
				// ******Click on Continue Button********\\

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@ng-click='proceedNext()']")))
						.click();
				report.updateTestLog(
						"Continue",
						"Clicked on Continue button in Health and Lifestyle section",
						Status.PASS);
				sleep(5000);

			}
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void confirmation() {

		WebDriverWait wait = new WebDriverWait(driver, 20);

		try {
			if (driver
					.getPageSource()
					.contains(
							"You must click and read the General Consent before proceeding")) {

				// ******Agree to general Consent*******\\

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id='generalConsentLabelTR']/span")))
						.click();
				report.updateTestLog("General Consent",
						"selected the Checkbox", Status.PASS);
				sleep(500);

				// ******Click on Submit*******\\
				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@ng-click='submitTransferCover()']")))
						.click();
				report.updateTestLog("Submit", "Clicked on Submit button",
						Status.PASS);
				sleep(2500);

				// *******Feedback popup******\\
				
				sleep(5000);
				if (wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//button[text()='No']"))).isDisplayed()) {
					wait.until(
							ExpectedConditions.elementToBeClickable(By
									.xpath("//button[text()='No']"))).click();
					report.updateTestLog("Feedback",
							"Selected No on the Feedback popup", Status.PASS);
					sleep(1500);
				}

			}
			// *****Fetching the Application Status*****\\


			
			String Appstatus = driver.findElement(
					By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(500);
			report.updateTestLog("Decision Page", "Application Status: "
					+ Appstatus, Status.PASS);

			if (!Appstatus.equalsIgnoreCase("TRANSFER OF COVER")) {
				// String
				// Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
				// System.out.println(Justification);
				sleep(5000);

				// *****Fetching the Application Number******\\

				String App = driver
						.findElement(
								By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p"))
						.getText();
				sleep(1000);

				// ******Download the PDF***********\\
				properties = Settings.getInstance();
				String StrBrowseName = properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Chrome")) {
					wait.until(
							ExpectedConditions.presenceOfElementLocated(By
									.xpath("//strong[text()='Download ']")))
							.click();
					sleep(1500);
				}

				// driver.findElement(By.xpath("//a[@class='underline']/strong")).click();

				if (StrBrowseName.equalsIgnoreCase("Firefox")) {
					wait.until(
							ExpectedConditions.presenceOfElementLocated(By
									.xpath("//strong[text()='Download ']")))
							.click();
					sleep(2500);
					Robot roboobj = new Robot();
					roboobj.keyPress(KeyEvent.VK_ENTER);
				}

				if (StrBrowseName.equalsIgnoreCase("InternetExplorer")) {
					Robot roboobj = new Robot();
					wait.until(
							ExpectedConditions.presenceOfElementLocated(By
									.xpath("//strong[text()='Download ']")))
							.click();

					sleep(4000);
					roboobj.keyPress(KeyEvent.VK_TAB);
					roboobj.keyPress(KeyEvent.VK_TAB);
					roboobj.keyPress(KeyEvent.VK_TAB);
					roboobj.keyPress(KeyEvent.VK_TAB);
					roboobj.keyPress(KeyEvent.VK_ENTER);

				}

				report.updateTestLog("PDF File", "PDF File downloaded",
						Status.PASS);

				sleep(1000);

				report.updateTestLog("Decision Page", "Application No: " + App,
						Status.PASS);

			}

			else {
				report.updateTestLog("Application Declined",
						"Application is declined", Status.SCREENSHOT);
			}
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	public void TransferCoverSTATEWIDE_Negativescenario() {
		String EmailId = dataTable
				.getData("STATEWIDE_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_TransferCover",
				"TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_TransferCover",
				"ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_TransferCover",
				"TimeofContact");
		String Gender = dataTable.getData("STATEWIDE_TransferCover", "Gender");
		String FifteenHoursWork = dataTable.getData("STATEWIDE_TransferCover",
				"FifteenHoursWork");
		String Citizen = dataTable
				.getData("STATEWIDE_TransferCover", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_TransferCover",
				"IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_TransferCover",
				"OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_TransferCover",
				"OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_TransferCover",
				"OfficeEnvironment");
		String TertiaryQual = dataTable.getData("STATEWIDE_TransferCover",
				"TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_TransferCover",
				"HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_TransferCover",
				"OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("STATEWIDE_TransferCover",
				"AnnualSalary");
		String PreviousInsurer = dataTable.getData("STATEWIDE_TransferCover",
				"PreviousInsurer");
		String PolicyNo = dataTable.getData("STATEWIDE_TransferCover",
				"PolicyNo");
		String FormerFundNo = dataTable.getData("STATEWIDE_TransferCover",
				"FormerFundNo");
		String DocumentaryEvidence = dataTable.getData(
				"STATEWIDE_TransferCover", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("STATEWIDE_TransferCover",
				"AttachPath");
		String CostType = dataTable.getData("STATEWIDE_TransferCover",
				"CostType");
		String DeathYN = dataTable.getData("STATEWIDE_TransferCover", "Death");
		String TPDYN = dataTable.getData("STATEWIDE_TransferCover", "TPD");
		String IPYN = dataTable.getData("STATEWIDE_TransferCover", "IP");
		String DeathAmount = dataTable.getData("STATEWIDE_TransferCover",
				"DeathAmount");
		String TPDAmount = dataTable.getData("STATEWIDE_TransferCover",
				"TPDAmount");
		String IPAmount = dataTable.getData("STATEWIDE_TransferCover",
				"IPAmount");
		String WaitingPeriod = dataTable.getData("STATEWIDE_TransferCover",
				"WaitingPeriod");
		String BenefitPeriod = dataTable.getData("STATEWIDE_TransferCover",
				"BenefitPeriod");
		
		String TPDAmtGreater = dataTable.getData("STATEWIDE_TransferCover", "TPDAmtGreater");

		try {

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,450)", "");
			Thread.sleep(2000);

			// ****Click on Transfer Your Cover Link*****\\
			WebDriverWait wait = new WebDriverWait(driver, 18);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//h5[contains(text(),'Transfer your cover')]")))
					.click();
			report.updateTestLog("Transfer Your Cover",
					"Clicked on Transfer Your Cover Link", Status.PASS);

			// *****Agree to Duty of disclosure*******\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@id='dodCkBoxLblId']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty of Disclosure",
					"Duty of Disclosure label is Checked", Status.PASS);

			// *****Agree to Privacy Statement*******\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@id='privacyCkBoxLblId']/span")))
					.click();
			sleep(350);
			report.updateTestLog("Privacy Statement",
					"Privacy Statement label is Checked", Status.PASS);

			// *****Enter the Email Id*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferEmail"))).sendKeys(
					EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId
					+ " is entered", Status.PASS);
			sleep(500);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(By
					.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "
					+ TypeofContact + " is Selected", Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferPhone"))).sendKeys(
					Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("coverDetailsTransferPhone"))).sendKeys(
					ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "
					+ ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {

				driver.findElement(
						By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span"))
						.click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact
						+ " is preferred time of Contact", Status.PASS);
			} else {
				driver.findElement(
						By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span"))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact
						+ " is preferred time of Contact", Status.PASS);
			}

			// ****Contact Details-Continue Button*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']")))
					.click();
			report.updateTestLog(
					"Continue",
					"Clicked on Continue button after entering contact details",
					Status.PASS);
			sleep(1200);

			// ***************************************\\
			// **********OCCUPATION SECTION***********\\
			// ****************************************\\

			// *****Select the 15 Hours Question*******\\

			if (FifteenHoursWork.equalsIgnoreCase("Yes")) {

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id='collapseTwo']/form/div/div[1]/div[2]/div/div[1]/label/span")))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes",
						Status.PASS);

			} else {

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]")))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No",
						Status.PASS);

			}

			/*
			 * //*****Resident of Australia****\\
			 * 
			 * if(Citizen.equalsIgnoreCase("Yes")){ driver.findElement(By.xpath(
			 * "//*[@id='collapseTwo']/form/div/div[2]/div[2]/div/div[1]/label/span"
			 * )).click(); sleep(250); report.updateTestLog("Citizen or PR",
			 * "Citizen of Australia", Status.PASS);
			 * 
			 * } else{ driver.findElement(By.xpath(
			 * "//*[@id='collapseTwo']/form/div/div[2]/div[2]/div/div[2]/label/span"
			 * )).click(); sleep(250); report.updateTestLog("Citizen or PR",
			 * "Not a citizen of Australia", Status.PASS);
			 * 
			 * }
			 */
			
			
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("transferIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("occupationTransfer")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
					
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("transferotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}		
			
			
			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Do you work wholly within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}							
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
									
									
									
		//******Do you work in hazardous environment*****\\		
			
		if(HazardousEnv.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[3]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}							
									
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\	
		
		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseTwo']/form/div/div[4]/div[4]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}							
		}							
								    
          sleep(800);
		   
									
		//*****What is your annual Salary******							
									
		
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
			// *****Click on Continue*******\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']")))
					.click();
			report.updateTestLog(
					"Continue",
					"Clicked on Continue after entering occupation details",
					Status.PASS);
			sleep(2500);

			// ******************************************\\
			// **********PREVIOUS COVER SECTION**********\\
			// *******************************************\\

			// *****What is the name of your previous fund or
			// insurer?*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("previousFundName"))).sendKeys(
					PreviousInsurer);
			sleep(250);
			report.updateTestLog("Previous Fund or Insurer",
					"Previous Fund or Insurer: " + PreviousInsurer
							+ " is entered", Status.PASS);

			// *****Please enter your fund member or insurance policy
			// number*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("membershipNumber"))).sendKeys(
					PolicyNo);
			sleep(250);
			report.updateTestLog("Previous Insurance Policy No",
					"Insurance Policy No: " + PolicyNo + " is entered",
					Status.PASS);

			// *****Please enter your former fund SPIN number (if
			// known)*****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.name("spinNumber")))
					.sendKeys(FormerFundNo);
			sleep(250);
			report.updateTestLog("SPIN Number", "SPIN Number: "
					+ FormerFundNo + " is entered", Status.PASS);

			// *****Do You want to Attach documentary evidence****\\

			if (DocumentaryEvidence.equalsIgnoreCase("Yes")) {

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id='collapseThree']/form/div/div[5]/div[2]/div/div[1]/label/span")))
						.click();
				sleep(500);
				report.updateTestLog("Documentary Evidence",
						"Yes is selected", Status.PASS);

				// ****Attach the file******

				driver.findElement(
						By.id("ngf-transferCvrFileUploadAddIdBtn"))
						.sendKeys(AttachPath);
				sleep(1500);

				// *****Let the attachment load*****\\
				driver.findElement(
						By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]"))
						.click();
				sleep(1000);

				// *****Add the Attachment*****\\

				WebElement add = driver.findElement(By
						.xpath("//span[contains(text(),'Add')]"));
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].click();", add);
				sleep(1500);
				report.updateTestLog("Evidence Attachment",
						"File is successfully attached", Status.PASS);

			} else {

				// ****No Attachment *****\\

				wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id='collapseThree']/form/div/div[5]/div[2]/div/div[2]/label/span")))
						.click();
				sleep(500);
				report.updateTestLog("Documentary Evidence",
						"No is selected", Status.PASS);

			}

			// *****Choose the cost of Insurance Type******\\

			Select CostofInsurance = new Select(driver.findElement(By
					.id("coverId")));
			CostofInsurance.selectByVisibleText(CostType);
			sleep(400);
			report.updateTestLog("Cost of Insurance Type", CostType
					+ " is selected", Status.PASS);

			// ***Continue button *****\\

			wait.until(
					ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@id='collapseThree']/form/div/div[8]/div/a/button")))
					.click();
			sleep(500);
			report.updateTestLog("Documentary Evidence",
					"No is selected", Status.PASS);

			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\
			
			//*****Death transfer amount******
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);
			
			}
			
			//*****TPD transfer amount******
			
			if(TPDYN.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);
			
			}
			
			String TPDError=driver.findElement(By.xpath("//*[@ng-if='coverAmtErrFlag']")).getText();
			
			if(TPDError.equalsIgnoreCase(TPDAmtGreater)){
				report.updateTestLog("TPD Error", "Expecetd Error Message: "+TPDAmtGreater, Status.PASS);
			}
			else{
				report.updateTestLog("TPD Error", "Error message is not displayed", Status.FAIL);
			}

			
				
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	}
