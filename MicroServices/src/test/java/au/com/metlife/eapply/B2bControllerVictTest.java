package au.com.metlife.eapply;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import au.com.metlife.eapply.b2b.controller.B2bController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@JsonDeserialize(as = InputStream.class)
public class B2bControllerVictTest {

	@InjectMocks
	private B2bController controller;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	String testData = null;
	String fileName = null;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	// for /getcustomerdata for VicSuper
	@Test
	public void getClientPostDataTest() throws Exception {

		testData = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeU1qSXpOelV6TkN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJek5qYzVaR1ZsWVMwellqQTRMVFJoTURFdFltVTBPQzAyWkROaU5tTTVaVFprTkRraUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5Bam52Mkl4UDJjV3JFbWhfV3FnYTl2Um5FcFF1c3I5N3EwbWNROEh3NVIw\"}";
		mockMvc.perform(post("/getcustomerdata").contentType(MediaType.APPLICATION_JSON).content(testData)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.lineOfBusiness").exists()).andExpect(jsonPath("$.lineOfBusiness").value("Group"))

				.andExpect(jsonPath("$.partnerID").exists()).andExpect(jsonPath("$.partnerID").value("VICT"))

				.andExpect(jsonPath("$.applicant.[0].personalDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").exists())
				.andExpect(jsonPath("$.applicant.[0].personalDetails.firstName").value("TESTVICT"))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[0].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").exists())
				.andExpect(jsonPath("$.applicant.[0].existingCovers.cover.[1].type").value(2))

				.andExpect(jsonPath("$.applicant.[0].address").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").exists())
				.andExpect(jsonPath("$.applicant.[0].address.addressType").value("2"))

				.andExpect(jsonPath("$.applicant.[0].contactDetails").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").exists())
				.andExpect(jsonPath("$.applicant.[0].contactDetails.emailAddress").value("test@test.com"))

				.andExpect(jsonPath("$.applicant.[0].memberType").exists())
				.andExpect(jsonPath("$.applicant.[0].memberType").value("Employee saver"));

	}

	@Test
	public void getPostB2BDataTestVict() throws Exception {

		testData = "<request><adminPartnerID>VICT</adminPartnerID><ageCover>32</ageCover><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>4</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>VICT</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>VICT</partnerID><product></product><campaign_code></campaign_code><tracking_code></tracking_code><promotion_code></promotion_code><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>TestJunit7</clientRefNumber><applicantRole>1</applicantRole><memberType>Employee saver</memberType><memberDivision>Employee saver</memberDivision><siopEndDate>12/12/1986</siopEndDate><segmentCode></segmentCode><personalDetails><firstName>TESTVICT</firstName><lastName>LASTVICT</lastName><dateOfBirth>12/11/1986</dateOfBirth><gender>male</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>1</type><units>10</units><indexation>Y</indexation><amount>515000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>1</type><units>10</units><indexation>N</indexation><amount>515000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>##QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeU1qSXpOall4TUN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJME9HWmxaVGN6T1MxbFlqQm1MVFJoTWpZdE9ESXdOUzAzWmpReFltWXpObU5sTnpVaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5jUjJaZUxuMzF1a3FWWHJMVDZnWlJnRE5YNkVsRjh4NFh6SlZtUDFMZWpn";
		mockMvc.perform(post("/postb2bdata").contentType(MediaType.APPLICATION_XML).content(testData)
				.accept(MediaType.APPLICATION_XML)).andExpect(status().isOk());
	}


	// for /getDefaultUnitsForSpecialCover for VicSuper

	@Test
	public void getDefaultUnitsForSpecialCoverVict() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getDefaultUnitsForSpecialCover?fundCode=VICT&anb=42&deathUnits=6&tpdUnits=6&dateJoined=2588&firstName=TESTVICT&clientrefnum=7&lastName=LASTVICT&dob=01/11/1958")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(content().string("true"))
				;

	}

	// for /getDefaultUnitsForSpecialCover for Invalid FundCode

	@Test
	public void getDefaultUnitsForSpecialCoverInvalidData() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders
				.get("/getDefaultUnitsForSpecialCover?fundCode=adfdf&anb=60&deathUnits=250&tpdUnits=250&dateJoined=2588&firstName=HOST&clientrefnum=7&lastName=HOSTB&dob=01/11/1958")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(content().string("false"))
				;

	}

	// /gethostdata for VicSuper
	@Test
	public void gethostdataVict() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.get("/gethostdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeU1qSXpOelV6TkN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJek5qYzVaR1ZsWVMwellqQTRMVFJoTURFdFltVTBPQzAyWkROaU5tTTVaVFprTkRraUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5Bam52Mkl4UDJjV3JFbWhfV3FnYTl2Um5FcFF1c3I5N3EwbWNROEh3NVIw")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.string("<request><adminPartnerID>VICT</adminPartnerID><ageCover>62</ageCover><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>4</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>VICT</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>VICT</partnerID><product></product><campaign_code></campaign_code><tracking_code></tracking_code><promotion_code></promotion_code><applicant><dateJoined>01/01/2011</dateJoined><clientRefNumber>70</clientRefNumber><applicantRole>1</applicantRole><memberType>Employee saver</memberType><memberDivision>Employee saver</memberDivision><siopEndDate>12/12/1986</siopEndDate><segmentCode></segmentCode><personalDetails><firstName>TESTVICT</firstName><lastName>LASTVICT</lastName><dateOfBirth>12/11/1956</dateOfBirth><gender>male</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><units>0</units><indexation>Y</indexation><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><units>0</units><indexation>N</indexation><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>"))
				;

	}


	// /gethostdata for Invalid TokenId
	@Test
	public void gethostdataInvalidToken() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/gethostdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOsdfsdf")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

	}

	// for /getcustomerdata for Vicsuper
	@Test
	public void getClientData() throws Exception {

		testData = "{\"tokenId\":\"QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeU1qSXpOelV6TkN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJek5qYzVaR1ZsWVMwellqQTRMVFJoTURFdFltVTBPQzAyWkROaU5tTTVaVFprTkRraUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5Bam52Mkl4UDJjV3JFbWhfV3FnYTl2Um5FcFF1c3I5N3EwbWNROEh3NVIw\"}";
		mockMvc.perform(MockMvcRequestBuilders
				.get("/getcustomerdata?tokenid=QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeU1qSXpOelV6TkN3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lJek5qYzVaR1ZsWVMwellqQTRMVFJoTURFdFltVTBPQzAyWkROaU5tTTVaVFprTkRraUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5Bam52Mkl4UDJjV3JFbWhfV3FnYTl2Um5FcFF1c3I5N3EwbWNROEh3NVIw")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}


}
