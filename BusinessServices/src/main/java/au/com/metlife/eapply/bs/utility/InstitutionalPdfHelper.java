package au.com.metlife.eapply.bs.utility;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.Iterator;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.pdf.CostumPdfAPI;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberProductDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.underwriting.response.model.Rule;

public class InstitutionalPdfHelper {
	

public static void displayInstitutionalDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException, MalformedURLException, IOException{
		if("coverDetails".equalsIgnoreCase(pdfObject.getChannel())){			
			if(!("APSD".equalsIgnoreCase(pdfObject.getFundId())|| "HOST".equalsIgnoreCase(pdfObject.getFundId())
					|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())
					|| "FIRS".equalsIgnoreCase(pdfObject.getFundId())
					|| "GUIL".equalsIgnoreCase(pdfObject.getFundId())
					|| "ACCS".equalsIgnoreCase(pdfObject.getFundId())
					|| "REIS".equalsIgnoreCase(pdfObject.getFundId())
					|| "PWCP".equalsIgnoreCase(pdfObject.getFundId())
					|| "PWCS".equalsIgnoreCase(pdfObject.getFundId())
					|| "VICS".equalsIgnoreCase(pdfObject.getFundId())
					|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
					)){
				for(int itr=0;itr<2;itr++){
					if(itr==0){
						displayHeaderNote(pdfAPI, document, pdfObject,pdfType,"Quotation Summary (Advisor Copy)");
					}if(itr==1){
						displayHeaderNote(pdfAPI, document, pdfObject,pdfType,"Quotation Summary (Client Copy)");
					}				
					displayPersonalDetails(pdfAPI, document, pdfObject, pdfType);
					displayCoverSummary(pdfAPI, document, pdfObject, pdfType);					
					displayQuotationDetails(pdfAPI, document, pdfObject, pdfType);
					//document.newPage();
				}
				
			}else{
				displayHeaderNote(pdfAPI, document, pdfObject,pdfType,"Quotation Summary");
				//displayPersonalDetails(pdfAPI, document, pdfObject, pdfType); 
	            if(!pdfObject.isQuickQuoteRender()){
	            	displayPersonalDetails(pdfAPI, document, pdfObject, pdfType); 
	            }
				displayCoverSummary(pdfAPI, document, pdfObject, pdfType);				
				displayQuotationDetails(pdfAPI, document, pdfObject, pdfType);
			}
			
		}else{
			displayHeaderNote(pdfAPI, document, pdfObject,pdfType,"Summary of information provided");
			displayPersonalDetails(pdfAPI, document, pdfObject, pdfType);
			displayCoverSummary(pdfAPI, document, pdfObject, pdfType);		
			if(pdfObject.isLoadingInd()){
        		float[] f = { .9f, 0.1f }; 
        		 PdfPCell pdfPCelAck = null;
        		PdfPTable pdfPTableAck = new PdfPTable( f);    		                    
        		pdfPCelAck = new PdfPCell(
                        pdfAPI.addParagraph( "I acknowledge the loading(s) applied to my premium.", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));                  
                
                pdfAPI.disableBorders( pdfPCelAck);
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                PdfPCell pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
                pdfAPI.disableBorders( pdfPCell1);
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
                
              	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
    	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
    	        
    	        pdfPEmptyTable = new PdfPTable( 1);
    	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
        	}
			if(pdfObject.isExclusionInd()){
        		float[] f = { .9f, 0.1f }; 
        		 PdfPCell pdfPCelAck = null;
        		PdfPTable pdfPTableAck = new PdfPTable( f);    		                    
        		pdfPCelAck = new PdfPCell(
                        pdfAPI.addParagraph( "I acknowledge the applied exclusion(s) applied to my cover.", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));                  
                
                pdfAPI.disableBorders( pdfPCelAck);
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                PdfPCell pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
                pdfAPI.disableBorders( pdfPCell1);
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
                
              	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
    	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
    	        
    	        pdfPEmptyTable = new PdfPTable( 1);
    	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
        	}
		}
		
		PdfPTable pdfSingleSpaceTable = new PdfPTable( 1);
        PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        document.add(pdfSingleSpaceTable);  
        
		if("UW".equalsIgnoreCase(pdfType)){		
			 displayPreExistingTerm(pdfAPI, document, pdfObject);
			 
			 
			 if(null!=pdfObject.getMemberDetails().getResponseObject()){
				 CommonPDFHelper.dispalyRuleDecision(pdfAPI, document, pdfObject, pdfObject.getMemberDetails().getResponseObject());
			 }			
		}
		if(!"coverDetails".equalsIgnoreCase(pdfObject.getChannel())){		
			displayQuotationDetails(pdfAPI, document, pdfObject, pdfType);
		}
		pdfSingleSpaceTable = null;
		pdfPSingleSpaceCell = null;
		
	}
	
	/**
	 * Description: displayPreExistingTerm in the PDF Section
	 * 
	 * @param
	 * java.lang.String,java.lang.String,,java.lang.String,,java.lang.String,
	 * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
	 * @return void
	 * @throws DocumentException
	 * @throws DocumentException,MetlifeException,
	 *             IOException
	 */
	private static void displayPreExistingTerm(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject) throws DocumentException {     
	    
	    PdfPTable pdfPEmptyTable = new PdfPTable( 1);
	    PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    float[] decFloat = { 0.8f, 0.2f };
	    
	    PdfPTable pdfDecisionTable = new PdfPTable( decFloat);
	    PdfPCell pdfPDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "Pre-Existing Terms", pdfObject.getSectionTextColor(), 10));
	    pdfPDecisionCell.setBorderWidth( 1);
	    pdfPDecisionCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	    pdfAPI.disableBordersTop( pdfPDecisionCell);
	    pdfPDecisionCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	    pdfPDecisionCell.setPaddingBottom( 6);
	    pdfPDecisionCell.setVerticalAlignment( Element.ALIGN_CENTER);
	    pdfDecisionTable.addCell( pdfPDecisionCell);
	    pdfPDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "", pdfObject.getSectionTextColor(), 10));
	    pdfAPI.disableBorders( pdfPDecisionCell);
	    pdfDecisionTable.addCell( pdfPDecisionCell);
	    document.add( pdfDecisionTable);
	    
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    float[] f1 = { 0.8f };
	    PdfPTable pdfDecisionTable1 = new PdfPTable( f1);
	    PdfPCell pdfDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "Exclusions:", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
	    pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    document.add( pdfDecisionTable1);
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    pdfDecisionTable1 = new PdfPTable( f1);	 
	     pdfDecisionCell = new PdfPCell(
	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getExistingExclusion(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    document.add( pdfDecisionTable1);
	    
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    pdfDecisionTable1 = new PdfPTable( f1);
	    pdfDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "Loadings:", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
	    pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    document.add( pdfDecisionTable1);
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    pdfDecisionTable1 = new PdfPTable( f1);
	    pdfDecisionCell = new PdfPCell(
	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getExistingLoading(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    
	    document.add( pdfDecisionTable1);   
	    
	    pdfPEmptyTable =null;
	    pdfPEmptyCell3 = null;
	    pdfDecisionTable = null;
	    pdfPDecisionCell = null;
	    pdfDecisionTable1 = null;
	   
	   
	}


	public static void displayQuotationDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		
		PdfPTable pdfPOccupationTable = null;
		DisclosureQuestionAns disclosureQuestionAns = null;
		PdfPCell pdfCell = null;
		
		   	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);
	        
	        pdfPEmptyTable = new PdfPTable( 1);
	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);
	        
	        if(!"UW".equalsIgnoreCase(pdfType)){
	        	 pdfPEmptyTable = new PdfPTable( 1);
	 	        PdfPCell pdfPCell;
	        	 if(pdfObject.isQuickQuoteRender()){
	        		 pdfPCell= new PdfPCell(
		 	                pdfAPI.addParagraph( "Your quotation details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
	        	 }else{
	        		 pdfPCell= new PdfPCell(
	        				 pdfAPI.addParagraph( "Your application details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
	        	 }
	        	 
//	 	        PdfPCell pdfPCell = new PdfPCell(
//	 	                pdfAPI.addParagraph( "Your application details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
	 	        pdfAPI.disableBorders( pdfPCell);
	 	        pdfPCell.setPadding( 4);
	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	        pdfPEmptyTable.addCell( pdfPCell);
	 	        document.add( pdfPEmptyTable);
	        }
	       
	        
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] occupationFloat = { 0.8f, 0.2f };
	        	  pdfPOccupationTable = new PdfPTable( occupationFloat);
	        }else{
	        	  pdfPOccupationTable = new PdfPTable( 1);
	        }
	        
	        PdfPCell pdfPOccupationCell = new PdfPCell(
	                pdfAPI.addParagraph( "Quotation Details", pdfObject.getSectionTextColor(), 10));
	        pdfPOccupationCell.setBorderWidth( 1);
	        pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfAPI.disableBordersTop( pdfPOccupationCell);
	        pdfPOccupationCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfPOccupationCell.setPaddingBottom( 6);
	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPOccupationTable.addCell( pdfPOccupationCell);
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 pdfPOccupationCell = new PdfPCell(
	 	                pdfAPI.addParagraph( "Underwriting Details", pdfObject.getSectionTextColor(), 10));
	 	        pdfPOccupationCell.setBorderWidth( 1);
	 	        pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	 	        pdfAPI.disableBordersTop( pdfPOccupationCell);
	 	        pdfPOccupationCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	 	        pdfPOccupationCell.setPaddingBottom( 6);
	 	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
	 	        pdfPOccupationTable.addCell( pdfPOccupationCell);
	        }
	       
	        document.add( pdfPOccupationTable);
	       
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	 pdfPOccupationTable = new PdfPTable( f1);
	        }        
		
		if(null!=pdfObject.getDisclosureQuestionList()){
			for(int itr =0 ;itr<pdfObject.getDisclosureQuestionList().size();itr++){
				disclosureQuestionAns = (DisclosureQuestionAns)pdfObject.getDisclosureQuestionList().get(itr);
				
				pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( Integer.toString( itr+1), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfCell);
	            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase("Reason for application for additional cover:")){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS".equalsIgnoreCase(pdfObject.getFundId()))){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }
	            //pdfPOccupationTable.addCell( pdfCell);
	            pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), FontFactory.HELVETICA, 8, Font.NORMAL, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfCell);
	            //pdfPOccupationTable.addCell( pdfCell);
	            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase("Reason for application for additional cover:")){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }
	            pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
	            pdfCell.setNoWrap(Boolean.FALSE);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	 pdfCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	 	            pdfCell.disableBorderSide( 1);
	 	            pdfCell.disableBorderSide( 3);
	 	            pdfCell.disableBorderSide( 4);
	            }else{
	            	pdfAPI.disableBorders( pdfCell);
	            }      
	            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
	            //pdfCell.setNoWrap( Boolean.TRUE);
	            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase("Reason for application for additional cover:")){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }
	            //pdfPOccupationTable.addCell( pdfCell);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfCell);
		            //pdfPOccupationTable.addCell( pdfCell);
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase("Reason for application for additional cover:")){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
	            }
	            
			}
		}
		document.add(pdfPOccupationTable);
		pdfPOccupationTable = null;
		disclosureQuestionAns = null;
		pdfCell = null;		
		pdfPEmptyTable = null;
	    pdfPEmptyCell3 = null;
	}

	private static String getOccupationDecision(ResponseObject responseObject){
        StringBuffer strBuff = new StringBuffer();
        if (null != responseObject) {          
            for (int itr = 0; itr < responseObject.getRules().getListRule().size(); itr++) {
                Rule rule = (Rule) responseObject.getRules().getListRule().get( itr);
                if (null != rule
                        && "QG=Occupation Details".equalsIgnoreCase( rule.getAlias())) {
                    if (null != rule.getQuestionTypeId()
                            && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                            && rule.isVisible())
                        if (null != rule
                                && null != rule.getAnswer()
                                && null != rule.getAnswer().getImpId()
                                && "27822446".equalsIgnoreCase( rule.getAnswer().getImpId())
                                && null != rule.getAnswer().getRiskType()
                                && "OCCUPATION".equalsIgnoreCase( rule.getAnswer().getRiskType())) {
                            
                            if (null != rule.getAnswer().getListUWDecisions()) {
                                for (int decItr = 0; decItr < rule.getAnswer().getListUWDecisions().size(); decItr++) {
                                    Decision decision = (Decision) rule.getAnswer().getListUWDecisions().get( decItr);
                                    if (!"ACC".equalsIgnoreCase( decision.getDecision())) {
                                        if (null != decision.getProductName()
                                                && "".equalsIgnoreCase( decision.getProductName())
                                                && decision.getProductName().length() == 0) {
                                            strBuff.append( "All Product:");
                                        } else {
                                            strBuff.append( decision.getProductName());
                                            strBuff.append( ":");
                                        }
                                        strBuff.append( decision.getDecision());
                                        strBuff.append( ",");
                                        for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                                            Rating rating = (Rating) decision.getListRating().get( ratingItr);
                                            strBuff.append( rating.getValue());
                                            strBuff.append( ",");
                                            strBuff.append( rating.getReason());
                                            strBuff.append( ",");
                                        }
                                        if (null != decision.getReasons()
                                                && decision.getReasons().length > 0) {
                                            strBuff.append( "Reason:");
                                            String reason[] = decision.getReasons();
                                            for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
                                                strBuff.append( reason[reasonItr]);
                                            }
                                            
                                        }
                                    }
                                    
                                }
                                
                            }
                            
                        }
                    
                }
            }
        }
        return strBuff.toString();
	}

	public static void displayHeaderNote(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType, String headerText) throws MalformedURLException, IOException, DocumentException{
		 
		Image gif;
		 PdfPTable pdfPTable2 = null;
		 PdfPCell pdfPCell4 = null;
		 PdfPTable pdfSpaceTable1 = null;
		 PdfPCell pdfPSpaceCell1 = null;
		 PdfPTable pdfPreffConDtsTable = null; 
         PdfPCell pdfPreffConDetsCell3 = null;
		float[] tableSize = { .7f, .3f };
	    PdfPTable pdfPTable = new PdfPTable( tableSize);	    
	    PdfPCell pdfPCell = new PdfPCell(
	            pdfAPI.addParagraph(headerText, FontFactory.HELVETICA, 16, Font.BOLD, pdfObject.getColorStyle()));
	    pdfAPI.disableBorders( pdfPCell);
	    pdfPCell.setPadding( 4);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_CENTER);
	    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    pdfPTable.addCell( pdfPCell);
	    pdfPCell = new PdfPCell();
	    if (null != pdfObject.getLogPath()) {
	    	 gif = Image.getInstance(pdfObject.getLogPath());
	    	  gif.setAlignment( Image.ALIGN_RIGHT);	  
	    	  pdfPCell.setFixedHeight(53f);
	    	  pdfPCell.setImage( gif);
	    }	
	    
	    pdfPCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_TOP);
	    
	   /* pdfPCell.setPaddingRight( 4);
        pdfPCell.setPadding( 4);
        pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);*/
        pdfAPI.disableBorders( pdfPCell);
        pdfPTable.addCell( pdfPCell);
        document.add(pdfPTable);
        
        float[] f1 = { 0.2f, 0.8f };		
        pdfPTable = new PdfPTable(f1);
        if(pdfObject.isQuickQuoteRender()){
        	pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_QUOT, pdfObject.getColorStyle(),7));
        }else{
        	pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, pdfObject.getColorStyle(),7));
        }
		//pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, pdfObject.getColorStyle(),7));
		pdfAPI.disableBorders(pdfPCell);
		pdfPCell.setPadding(4);
		pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);		
		pdfPTable.addCell(pdfPCell);	
		pdfPCell = new PdfPCell(pdfAPI.addParagraph(CommonPDFHelper.getTodaysDate(),pdfObject.getColorStyle(),7));
		pdfAPI.disableBorders(pdfPCell);
		pdfPCell.setPadding(4);
		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);		
		pdfPTable.addCell(pdfPCell);
	
		if((!pdfObject.isQuickQuoteRender()) || (pdfObject.isQuickQuoteRender() && pdfObject.getApplicationNumber()!=null &&pdfObject.getApplicationNumber()!="" )){
		pdfPCell = new PdfPCell(pdfAPI.addParagraph("Application Number:", pdfObject.getColorStyle(),7));
		pdfAPI.disableBorders(pdfPCell);
		pdfPCell.setPadding(4);
		pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);		
		pdfPTable.addCell(pdfPCell);	
		pdfPCell = new PdfPCell(pdfAPI.addParagraph(pdfObject.getApplicationNumber(),pdfObject.getColorStyle(),7));
		pdfAPI.disableBorders(pdfPCell);
		pdfPCell.setPadding(4);
		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);		
		pdfPTable.addCell(pdfPCell);
		}
		document.add(pdfPTable);

		pdfPTable = new PdfPTable(1);
		PdfPCell pdfPSpaceCell = new PdfPCell(pdfAPI.addSingleCell());		
		pdfAPI.disableBorders(pdfPSpaceCell);
		pdfPTable.addCell(pdfPSpaceCell);
		document.add(pdfPTable);    
		
		
		if ("RUW".equalsIgnoreCase( pdfObject.getApplicationDecision()) || ("VICS".equalsIgnoreCase(pdfObject.getFundId()) && "UW".equalsIgnoreCase(pdfType))) {			
			
			 if("UW".equalsIgnoreCase(pdfType)){
				 float[] prefConFloat = { .8f, .2f };
				 pdfPTable2 = new PdfPTable( prefConFloat);
			 }else{
				 pdfPTable2 = new PdfPTable( 1);
			 }
                      
            pdfPCell4 = new PdfPCell(
                    pdfAPI.addParagraph( "Preferred Contact Details", pdfObject.getSectionTextColor(), 10));
            pdfPCell4.setBorderWidth( 1);
            pdfPCell4.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
            pdfAPI.disableBordersTop( pdfPCell4);
            pdfPCell4.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
            pdfPCell4.setPaddingBottom( 6);
            pdfPCell4.setVerticalAlignment( Element.ALIGN_CENTER);
            pdfPTable2.addCell( pdfPCell4);
            if("UW".equalsIgnoreCase(pdfType)){
            	pdfPCell4 = new PdfPCell(
                        pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 10));
                pdfAPI.disableBorders( pdfPCell4);
                pdfPTable2.addCell( pdfPCell4);
            }            
            document.add( pdfPTable2);
            
            pdfSpaceTable1 = new PdfPTable( 1);
            pdfPSpaceCell1 = new PdfPCell( pdfAPI.addSingleCell());
            pdfAPI.disableBorders( pdfPSpaceCell1);
            pdfSpaceTable1.addCell( pdfPSpaceCell1);
            document.add( pdfSpaceTable1);            
           
            if("UW".equalsIgnoreCase(pdfType)){
            	 float[] prefConDts = { .2f, .2f, .2f, .2f, .2f };
            	 pdfPreffConDtsTable = new PdfPTable( prefConDts);
            }else{
                float[] prefConDts = { .2f, .3f, .2f, .2f };
                pdfPreffConDtsTable = new PdfPTable( prefConDts);
            }            
            
            pdfPreffConDetsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( "Preferred Contact Method:", pdfObject.getColorStyle(), 7));
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getPrefferedContactType(), FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            
            pdfPreffConDetsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( "Preferred Contact Time:", pdfObject.getColorStyle(), 7));
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            if (null != pdfObject.getMemberDetails() && null != pdfObject.getMemberDetails().getPrefferedContactType() && !pdfObject.getMemberDetails().getPrefferedContactType().contains( "Email")){
            	pdfPreffConDetsCell3 = new PdfPCell(
                        pdfAPI.addParagraph( pdfObject.getMemberDetails().getPrefferedContactTime(), FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
            }                
            else{
            	pdfPreffConDetsCell3 = new PdfPCell(
                        pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
            }
                
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);            
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            if("UW".equalsIgnoreCase(pdfType)){
            	pdfPreffConDetsCell3 = new PdfPCell(
                        pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
                pdfAPI.disableBorders( pdfPreffConDetsCell3);
                pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            }          
            
            document.add( pdfPreffConDtsTable);
            System.out.println("pdfObject.getFundId()>>"+pdfObject.getFundId());
            if("UW".equalsIgnoreCase(pdfType) && "VICS".equalsIgnoreCase(pdfObject.getFundId()) && null!=pdfObject.getMemberDetails().getPrefContactDetails() && pdfObject.getMemberDetails().getPrefContactDetails().length()>0){
	           	
            	System.out.println("pdfObject.getMemberDetails().getClientEmailId()>>"+pdfObject.getMemberDetails().getClientEmailId());
            	float[] prefConDts ={ .2f, .2f, .2f, .2f, .2f };
	           	 pdfPreffConDtsTable = new PdfPTable( prefConDts);
	           	 pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Preferred Contact Details:", pdfObject.getColorStyle(), 7));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( pdfObject.getMemberDetails().getPrefContactDetails(), FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             
	          	 pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Preferred Email Address:", pdfObject.getColorStyle(), 7));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( pdfObject.getMemberDetails().getClientEmailId(), FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             
	             pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph("", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             
	             document.add( pdfPreffConDtsTable);
           } 
            
        }
		pdfPTable = null;
		pdfPCell = null;		
		pdfPTable2 = null;
		pdfPCell4 = null;
		pdfSpaceTable1 = null;
		pdfPSpaceCell1 = null;
		pdfPreffConDtsTable = null; 
        pdfPreffConDetsCell3 = null;
		
	}
	
	public static void displayPersonalDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		
		 PdfPTable pdfPTable = null;
		 PdfPCell pdfPCell2 = null;
		 PdfPTable fundInfoPdfPTable = null;
		 PdfPTable fundPdfPTable = null;
		 PdfPTable bmiPdfPTable = null;
		 PdfPCell fundInfoPdfPCell = null;
		 
		 boolean titleRender = true;
         boolean firstNameRender = true;
         boolean surNameRender = true;
         boolean dobRender = true;
         String exWaitingPeriod = "";
         String exBenefitPeriod = "";
         
         if(pdfObject.isQuickQuoteRender()){
         	if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getTitle() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getTitle())){
	            	titleRender = true;
	            }else{
	            	titleRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getMemberFName() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getMemberFName())){
	            	firstNameRender = true;
	            }else{
	            	firstNameRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getMemberLName() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getMemberLName())){
	            	surNameRender = true;
	            }else{
	            	surNameRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getDob() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getDob())){
	            	dobRender = true;
	            }else{
	            	dobRender = false;
	            }
         }
		 
		 if("UW".equalsIgnoreCase(pdfType)){
			 float[] prefConFloat = { .8f, .2f };
			  pdfPTable = new PdfPTable( prefConFloat);
		 }else{
			  pdfPTable = new PdfPTable( 1);
		 }	 
		 if(titleRender || firstNameRender || surNameRender || dobRender){
	        pdfPCell2 = new PdfPCell(
	                pdfAPI.addParagraph( "Member Details", pdfObject.getSectionTextColor(), 10));
	        pdfPCell2.setBorderWidth( 1);
	        pdfPCell2.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfAPI.disableBordersTop( pdfPCell2);
	        pdfPCell2.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfPCell2.setPaddingBottom( 6);
	        pdfPCell2.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPTable.addCell( pdfPCell2);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	pdfPCell2 = new PdfPCell(
		                pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 10));
		        pdfAPI.disableBorders( pdfPCell2);
		        pdfPTable.addCell( pdfPCell2);
	        }	        
	        document.add( pdfPTable);
		 }
	        if("UW".equalsIgnoreCase(pdfType)){
	        	String localFundid = null;
	        	if(null != pdfObject.getProductName() && "PWCS".equalsIgnoreCase(pdfObject.getProductName())){
	        		localFundid = "PWCP";
	        	}else{
	        		localFundid = pdfObject.getProductName();
	        	}
	        	
	        	float[] fundInfo = { 0.2f, 0.2f, .6f };
		        fundPdfPTable = new PdfPTable( fundInfo);
		        
		        fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph( "Fund:", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		        fundPdfPTable.addCell( fundInfoPdfPCell);		        
		        fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph(localFundid,pdfObject.getColorStyle(), 7));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		        fundPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundPdfPTable.addCell( fundInfoPdfPCell);	
		        
		       document.add( fundPdfPTable);
		        String calculatedBMI = null;
		        if(pdfObject.getMemberDetails().getResponseObject()!=null){
		        	for (Iterator iter = pdfObject.getMemberDetails().getResponseObject().getClientData().getListInsured().iterator(); iter.hasNext();) {
						Insured insured = (Insured) iter.next();
						System.out.println("insured.getBmi()>>"+insured.getBmi());
						calculatedBMI = insured.getBmi();
						
						
					}
		        }
		        
		        if(calculatedBMI!=null){
		        	float[] fundInfo1 = { 0.2f, 0.2f, .6f };
		        	bmiPdfPTable = new PdfPTable( fundInfo1);
			        
			        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph( "Calculated BMI:", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        bmiPdfPTable.addCell( fundInfoPdfPCell);		        
			        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph(calculatedBMI,pdfObject.getColorStyle(), 7));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        bmiPdfPTable.addCell( fundInfoPdfPCell);
			        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        bmiPdfPTable.addCell( fundInfoPdfPCell);
			        document.add( bmiPdfPTable);
		        }		        
		       
	        }
	        
	        if(null!=pdfObject.getMemberDetails().getClientEmailId() && 
	        		("HOST".equalsIgnoreCase(pdfObject.getProductName()) 
	        				|| "PSUP".equalsIgnoreCase(pdfObject.getProductName())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getProductName())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getProductName()))){
	        	
	        	/*String localFundid = null;
	        	if(null != pdfObject.getProductName() && "PWCS".equalsIgnoreCase(pdfObject.getProductName())){
	        		localFundid = "PWCP";
	        	}else{
	        		localFundid = pdfObject.getProductName();
	        	}*/
	        	if("UW".equalsIgnoreCase(pdfType)){
					  float[] fundInfo = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
					  fundInfoPdfPTable = new PdfPTable( fundInfo);
					 
				}else{
					  float[] fundInfo = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
					  fundInfoPdfPTable = new PdfPTable( fundInfo);
				}
	        	/*float[] fundInfo = {  0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f  };
		        fundInfoPdfPTable = new PdfPTable( fundInfo);*/
		        
	        	fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph( "Client Email", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);		        
		        fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph(pdfObject.getMemberDetails().getClientEmailId(),pdfObject.getColorStyle(), 7));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);


		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            
	            fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_RIGHT);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	        }        
	        
	        if(("UW".equalsIgnoreCase(pdfType) && fundInfoPdfPTable!=null) || (fundInfoPdfPTable!=null && null!=pdfObject.getMemberDetails().getClientEmailId() && 
	        		("HOST".equalsIgnoreCase(pdfObject.getProductName()) 
	        				|| "PSUP".equalsIgnoreCase(pdfObject.getProductName())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getProductName())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getProductName())))){
	        	document.add( fundInfoPdfPTable);	
	        }
	        
	        if(("UW".equalsIgnoreCase(pdfType))){
	        
	        	 float[] fundInfo = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
			     fundInfoPdfPTable = new PdfPTable( fundInfo);
					 
				fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph( "Preferred contact number", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph(pdfObject.getMemberDetails().getPrefContactDetails(),pdfObject.getColorStyle(), 7));
			        
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);


		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            
	            fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_RIGHT);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	             document.add( fundInfoPdfPTable);	
	       
	        }
	       
			if("UW".equalsIgnoreCase(pdfType)){
				  float[] f3 = { 0.08f, 0.08f, 0.1f, 0.12f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
				  pdfPTable = new PdfPTable( f3);
				 
			}else{
				  float[] f3 = { 0.04f, 0.08f, 0.09f, 0.12f, 0.06f, 0.1f, 0.12f, 0.1f };
				  pdfPTable = new PdfPTable( f3);
			}    
	            
	            PdfPCell pdfPCell3 = null;
	            
	           
	            
	           
	            
	            if(titleRender){
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Title", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }else{
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }
	            
	            //pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Title", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getTitle(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(firstNameRender){
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "Firstname", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }else{
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }
//	            pdfPCell3 = new PdfPCell(
//	                    pdfAPI.addParagraph( "Firstname", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getMemberFName(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            
	            pdfPTable.addCell( pdfPCell3);
	            	            
	            if(surNameRender){
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Surname", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }else{
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }
//	            pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Surname", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getMemberLName(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(dobRender){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "Date of Birth", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }else{
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            }
	           /* pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Date of Birth", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));*/
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getDob(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPTable.addCell( pdfPCell3);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            pdfPTable.addCell( pdfPCell3);
	            }          
	            
	            document.add( pdfPTable);
	            
	           	            
	            if("UW".equalsIgnoreCase(pdfType)){
					  float[] f4 = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
					  pdfPTable = new PdfPTable( f4);
					 
				}else{
					  float[] f4 = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
					  pdfPTable = new PdfPTable( f4);
				}    
	            
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Date Joined Fund", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getDateJoinedFund(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
	            pdfPTable.addCell( pdfPCell3);
	            
	            pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Gender", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getGender(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(null != pdfObject.getMemberDetails().getClientRefNum() && null != pdfObject.getApplicationNumber() && !pdfObject.getMemberDetails().getClientRefNum().equalsIgnoreCase(pdfObject.getApplicationNumber())){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "Member number", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            // pdfPCell3.setPadding(4);
	 	            // pdfPCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 	            pdfPTable.addCell( pdfPCell3);
	 	            pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getClientRefNum(), pdfObject.getColorStyle(), 7));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            // pdfPCell3.setPadding(4);
	 	            // pdfPCell3.setVerticalAlignment(Element.ALIGN_RIGHT);
	 	            pdfPTable.addCell( pdfPCell3);
	 	            if("UW".equalsIgnoreCase(pdfType)){
	 	            	pdfPCell3 = new PdfPCell(
	 		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 		            pdfAPI.disableBorders( pdfPCell3);
	 		            pdfPTable.addCell( pdfPCell3);
	 	            }	            
	            }
	            
	            document.add( pdfPTable);
	            //////////////////
	            
	            if("VICS".equalsIgnoreCase(pdfObject.getFundId())){
	            	if("UW".equalsIgnoreCase(pdfType)){
						  float[] f4 = { 0.2f, 0.6f, 0.2f };
						  pdfPTable = new PdfPTable( f4);
						 
					}else{
						  float[] f4 = { 0.2f, 0.81f};
						  pdfPTable = new PdfPTable( f4);
					}    
		            
		            pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "Date Joined Company", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            pdfPTable.addCell( pdfPCell3);
		            pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getDateJoinedCompany(), pdfObject.getColorStyle(), 7));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
		            pdfPTable.addCell( pdfPCell3);            
		         
	 	            if("UW".equalsIgnoreCase(pdfType)){
	 	            	pdfPCell3 = new PdfPCell(
	 		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 		            pdfAPI.disableBorders( pdfPCell3);
	 		            pdfPTable.addCell( pdfPCell3);
	 	            } 	            
	 	           document.add( pdfPTable);
	 	           //Amp number
	 	           
	 	          if("UW".equalsIgnoreCase(pdfType)){
					  float[] f4 = { 0.3f, 0.5f, 0.2f };
					  pdfPTable = new PdfPTable( f4);
					 
	 	          }else{
					  float[] f4 = { 0.3f, 0.70f};
					  pdfPTable = new PdfPTable( f4);
	 	          } 
	 	           
	 	           	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "AMP Reference Number", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            pdfPTable.addCell( pdfPCell3);
		            pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getAmpReferenceNumber(), pdfObject.getColorStyle(), 7));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            pdfPTable.addCell( pdfPCell3);
		            
		            if("UW".equalsIgnoreCase(pdfType)){
	 	            	pdfPCell3 = new PdfPCell(
	 		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 		            pdfAPI.disableBorders( pdfPCell3);
	 		            pdfPTable.addCell( pdfPCell3);
	 	            }
		            
		            document.add( pdfPTable);
	 	           
	 	           //address
	 	           if("UW".equalsIgnoreCase(pdfType)){
	 	        	  float[] f4 = { 0.2f, 0.6f, 0.2f };
					  pdfPTable = new PdfPTable( f4);
					 
					}else{
						  float[] f4 = { 0.2f, 0.8f };
						  pdfPTable = new PdfPTable( f4);
					}    
	            
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Address", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getMemberAddress(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
	            pdfPTable.addCell( pdfPCell3);
	            
	         
	            if("UW".equalsIgnoreCase(pdfType)){
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPTable.addCell( pdfPCell3);
	            } 	            
		          document.add( pdfPTable);
	 	           //
	 	           
		          for (int itr = 0; itr < pdfObject.getMemberDetails().getMemberProductList().size(); itr++) {
		 	        	 MemberProductDetails	memberProductDetails = (MemberProductDetails)pdfObject.getMemberDetails().getMemberProductList().get(itr);
		 	        	if("Income Protection".equalsIgnoreCase(memberProductDetails.getProductName()) && (memberProductDetails.getExistingCover()!=null && !"$0".equalsIgnoreCase(memberProductDetails.getExistingCover()))){
		 	        		exWaitingPeriod = memberProductDetails.getExWaitingPeriod();
			 	        	exBenefitPeriod = memberProductDetails.getExBenefitPeriod();
		 	        	}		 	        	 
		 	       }
		          
		        	  if("UW".equalsIgnoreCase(pdfType)){
						  float[] f4 = { 0.25f, 0.18f, 0.25f, 0.12f, 0.2f };
						  pdfPTable = new PdfPTable( f4);
						 
					}else{
						  float[] f4 = { 0.32f, 0.18f, 0.32f, 0.18f };
						  pdfPTable = new PdfPTable( f4);
					} 
		        	  
		        	  pdfPCell3 = new PdfPCell(
			                    pdfAPI.addParagraph( "Current IP Waiting Period", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
			            pdfPTable.addCell( pdfPCell3);
			            pdfPCell3 = new PdfPCell(
			                    pdfAPI.addParagraph( exWaitingPeriod, pdfObject.getColorStyle(), 7));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
			            pdfPTable.addCell( pdfPCell3);
			            
			            pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Current IP Benefit Period", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
			            pdfPTable.addCell( pdfPCell3);
			            pdfPCell3 = new PdfPCell(
			                    pdfAPI.addParagraph( exBenefitPeriod, pdfObject.getColorStyle(), 7));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
			            pdfPTable.addCell( pdfPCell3);            
			            
			           
			            if("UW".equalsIgnoreCase(pdfType)){
			            	pdfPCell3 = new PdfPCell(
				                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
				            pdfAPI.disableBorders( pdfPCell3);
				            pdfPTable.addCell( pdfPCell3);
			            }
			            document.add( pdfPTable);
		               
		            
		            
	            }           
	            
	            ////////////////////////
	           
	           	
	            
	            if(null!=pdfObject.getMemberDetails().getMemberType() &&
	            		("HOST".equalsIgnoreCase(pdfObject.getProductName()) 
	            				||  "PSUP".equalsIgnoreCase(pdfObject.getProductName())
		    					|| "NSFS".equalsIgnoreCase(pdfObject.getProductName())
		    					|| "MTAA".equalsIgnoreCase(pdfObject.getProductName()))&& !pdfObject.isNonValidatedMemberFlow()){				        	   	
		        	/*float[] fundInfo = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
			        fundInfoPdfPTable = new PdfPTable( fundInfo);*/
	            	
	            	if("UW".equalsIgnoreCase(pdfType)){
						  float[] fundInfo = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
						  fundInfoPdfPTable = new PdfPTable( fundInfo);
						 
					}else{
						  float[] fundInfo = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
						  fundInfoPdfPTable = new PdfPTable( fundInfo);
					}
			        
		        	fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph( "Member Type", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        fundInfoPdfPTable.addCell( fundInfoPdfPCell);		        
			        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph(toTitleCase(pdfObject.getMemberDetails().getMemberType()),pdfObject.getColorStyle(), 7));
			        
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
			        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPTable.addCell( fundInfoPdfPCell);	
			        
			        
			        
			        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		            fundInfoPdfPCell = new PdfPCell(
		                    pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		            
		            fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		            fundInfoPdfPCell = new PdfPCell(
		                    pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 7));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_RIGHT);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
			        
			        document.add( fundInfoPdfPTable);
			        
		        }  
		       /* if("UW".equalsIgnoreCase(pdfType) || (null!=pdfObject.getMemberDetails().getMemberType() && "HOST".equalsIgnoreCase(pdfObject.getProductName()))){
		        	document.add( fundInfoPdfPTable);
		        }*/
	            
	         pdfPTable = null;
	   		 pdfPCell2 = null;
	   		 fundInfoPdfPTable = null;
	   		 fundInfoPdfPCell = null;
		
	}
	
	
	public static void displayCoverSummary(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		   PdfPTable pdfPCoverTable = null;
		   PdfPCell pdfPCoverCell = null;
		   PdfPTable pdfPEmptyTable = null;
		   PdfPCell pdfPEmptyCell3 = null;
		   PdfPTable pdfPSummaryDtsTable = null;
		   PdfPCell pdfcoverCostCell1 = null;
		   MemberProductDetails memberProductDetails = null;
		   Boolean corpFund = Boolean.FALSE;		  
		   
		   //costColRq = ConfigurationHelper.getConfigurationValue("CORPINST", pdfObject.getProductName());		   
		   //String hdCorpFunds = ConfigurationHelper.getConfigurationValue("HD_CORP_FUNDS", "HD_CORP_FUNDS");		   
		   if("UW".equalsIgnoreCase(pdfType)){
			   float[] coverSumm = { .8f, .2f };
		       pdfPCoverTable = new PdfPTable( coverSumm);
		   }else{
			   pdfPCoverTable = new PdfPTable(1);
		   }  
	        
	        pdfPCoverCell = new PdfPCell(
	                pdfAPI.addParagraph( "Cover Summary", pdfObject.getSectionTextColor(), 10));
	        pdfPCoverCell.setBorderWidth( 1);
	        pdfPCoverCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfAPI.disableBordersTop( pdfPCoverCell);
	        pdfPCoverCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfPCoverCell.setPaddingBottom( 6);
	        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPCoverTable.addCell( pdfPCoverCell);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	pdfPCoverCell = new PdfPCell(
		                pdfAPI.addParagraph( "Underwriting Details", pdfObject.getSectionTextColor(), 10));
		        pdfPCoverCell.setBorderWidth( 1);
		        pdfPCoverCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
		        pdfAPI.disableBordersTop( pdfPCoverCell);
		        pdfPCoverCell.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
		        pdfPCoverCell.setPaddingBottom( 6);
		        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPCoverTable.addCell( pdfPCoverCell);
	        }	        
	        document.add( pdfPCoverTable);
	        
	        pdfPEmptyTable = new PdfPTable( 1);
	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);	     	        	       
	      
	        if("UW".equalsIgnoreCase(pdfType)){
	        	if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
	        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && null!=pdfObject.getRequestType() 
	        			 && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else{
	        		 float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}	
	        	/*float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
	        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());*/
	        }else{
	        	 /*float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
	        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());*/
	        	if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
	        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId()) 
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else{
	        		 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}	
	        }
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	/* float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
	        	 pdfPSummaryDtsTable = new PdfPTable( f6);*/
	        	
	        	if(("HOST".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f6 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else{
	        		 float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}
	        }else{
	        	 /*float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
	        	 pdfPSummaryDtsTable = new PdfPTable( f6);*/
	        	if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
	        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f6 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else{
	        		float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
		        	pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}
	        }
	        for (int itr = 0; itr < pdfObject.getMemberDetails().getMemberProductList().size(); itr++) {
	        	memberProductDetails = (MemberProductDetails)pdfObject.getMemberDetails().getMemberProductList().get(itr);
	        	displayColumnValue( pdfAPI, document,pdfObject,memberProductDetails, pdfType,pdfPSummaryDtsTable,pdfObject.getProductName());	        	 
	        }
	        
	       
	        	
	        pdfcoverCostCell1 = new PdfPCell(
	                pdfAPI.addParagraph(pdfObject.getMemberDetails().getFrquencyOpted()
	                        + " cost of your requested cover", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));	
	        	
		        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_RIGHT);
		        pdfcoverCostCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
		        if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
		        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
		        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
		        	pdfcoverCostCell1.setColspan(7);
		        }else{
		        	pdfcoverCostCell1.setColspan(6);
		        }
		        
		        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
	     
	        	 pdfcoverCostCell1 = new PdfPCell(
	 	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getTotalPremium(), FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
	 	        pdfcoverCostCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	 	        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_CENTER);
	 	        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
	 	        
	 	       if("UW".equalsIgnoreCase(pdfType)){
		        	pdfcoverCostCell1 = new PdfPCell(
			                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
			        pdfAPI.disableBorders( pdfcoverCostCell1);
			        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        }
	      
	       
	        
	        if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
	        	 
	        	 pdfcoverCostCell1 = new PdfPCell(
	                     pdfAPI.addParagraph( "Additional cover alteration:", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
	        	 pdfcoverCostCell1.setColspan(7);
	        	
	        	 pdfcoverCostCell1.disableBorderSide(pdfcoverCostCell1.BOTTOM);
	        	 pdfcoverCostCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        	 pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
	        	 if("UW".equalsIgnoreCase(pdfType)){
			        	pdfcoverCostCell1 = new PdfPCell(
				                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
				        pdfAPI.disableBorders( pdfcoverCostCell1);
				        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
			        }
	            
	        	pdfcoverCostCell1 = new PdfPCell(
		                pdfAPI.addParagraph(MetlifeInstitutionalConstants.DCL_TEXT_OVERALL+pdfObject.getDeclReasons(), FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()));
		       // pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_RIGHT);
		        pdfcoverCostCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
		        pdfcoverCostCell1.setColspan(7);
		        pdfcoverCostCell1.disableBorderSide(pdfcoverCostCell1.TOP);
		        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        if("UW".equalsIgnoreCase(pdfType)){
		        	pdfcoverCostCell1 = new PdfPCell(
			                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
			        pdfAPI.disableBorders( pdfcoverCostCell1);
			        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        }
	        }
	        document.add( pdfPSummaryDtsTable);
	        
	            pdfPCoverTable = null;
			    pdfPCoverCell = null;
			    pdfPEmptyTable = null;
			    pdfPEmptyCell3 = null;
			    pdfPSummaryDtsTable = null;
			    pdfcoverCostCell1 = null;
			    memberProductDetails = null;
	        
	          
		
	}
	
	private static void displayColumnName(CostumPdfAPI pdfAPI, Document document,float[] f5, PDFObject pdfObject,String pdfType, String productName)
            throws DocumentException {
		 
        PdfPTable pdfPSummaryTable = new PdfPTable( f5);
        PdfPCell pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Cover Type", pdfObject.getSectionTextColor(), 7));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Existing Cover", pdfObject.getSectionTextColor(), 7));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        
        if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
				|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
				|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& null!=pdfObject.getRequestType() 
    			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
        	 pdfPSummaryCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "Transfer Cover", pdfObject.getSectionTextColor(), 7));
             pdfAPI.disableBorders( pdfPSummaryCell3);
             pdfPSummaryCell3.setPadding( 4);
             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
             pdfPSummaryTable.addCell( pdfPSummaryCell3);
        }
        
         if(pdfObject.getMemberDetails().isReplaceCvrOption() && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
        	 pdfPSummaryCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "Additional Cover Required", pdfObject.getSectionTextColor(), 7));
             pdfAPI.disableBorders( pdfPSummaryCell3);
             pdfPSummaryCell3.setPadding( 4);
             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
             pdfPSummaryTable.addCell( pdfPSummaryCell3);
         }        
        else if(!"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
        	 pdfPSummaryCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "Total Cover Required", pdfObject.getSectionTextColor(), 7));
             pdfAPI.disableBorders( pdfPSummaryCell3);
             pdfPSummaryCell3.setPadding( 4);
             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
             pdfPSummaryTable.addCell( pdfPSummaryCell3);
        }else if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
        	pdfPSummaryCell3 = new PdfPCell(
                    pdfAPI.addParagraph( "Total Cover", pdfObject.getSectionTextColor(), 7));
            pdfAPI.disableBorders( pdfPSummaryCell3);
            pdfPSummaryCell3.setPadding( 4);
            pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
            pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
            pdfPSummaryTable.addCell( pdfPSummaryCell3);
        }
       
      
        
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Waiting Period", pdfObject.getSectionTextColor(), 7));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Benefit Period", pdfObject.getSectionTextColor(), 7));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Occupation Rating", pdfObject.getSectionTextColor(), 7));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);  
     
        
	    	 if(pdfObject.getMemberDetails().isReplaceCvrOption()){
	         	pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Estimated Additional " +  pdfObject.getMemberDetails().getFrquencyOpted() + " Cost", pdfObject.getSectionTextColor(), 7));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
	         }else{
	         	pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Estimated Total " +  pdfObject.getMemberDetails().getFrquencyOpted() + " Cost", pdfObject.getSectionTextColor(), 7));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
	         }
	   
        
           
        
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Final Decision After Post Processing", pdfObject.getSectionTextColor(), 7));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        
        document.add( pdfPSummaryTable);
        
         pdfPSummaryTable = null;
         pdfPSummaryCell3 = null;
        
        
        
    }
	
	
	private static void displayColumnValue(CostumPdfAPI pdfAPI, Document document,
            PDFObject pdfObject,MemberProductDetails memberProductDetails , String pdfType, PdfPTable pdfPSummaryDtsTable, String productName)
            throws DocumentException {
		  
        PdfPCell pdfSummaryDtsCell3 = null;   
        PdfPCell pdfPExclusionCell1 = null;
        PdfPCell pdfPEmptyCell1 = null;
        pdfSummaryDtsCell3 = new PdfPCell(
                pdfAPI.addParagraph( memberProductDetails.getProductName(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
        System.out.println("inside the column value>>"+memberProductDetails.getExistingCover());
        
        pdfSummaryDtsCell3 = new PdfPCell(
                pdfAPI.addParagraph(  memberProductDetails.getExistingCover(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
        if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
				|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
				|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& null!=pdfObject.getRequestType() 
    			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && "TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
        	if(null!=memberProductDetails.getTransferCoverAmnt()){
        		pdfSummaryDtsCell3 = new PdfPCell(
                        pdfAPI.addParagraph(  memberProductDetails.getTransferCoverAmnt(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        	}else{
        		pdfSummaryDtsCell3 = new PdfPCell(
                        pdfAPI.addParagraph(  "$0", FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        	}
        	
            pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        }        
       
       if("UWCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
    	   if(memberProductDetails.getNewExistingCover()!=null){
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph(memberProductDetails.getNewExistingCover(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
    	   }else{
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph(memberProductDetails.getExistingCover(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
    	   }

       }else{
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph(memberProductDetails.getProductAmount(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
       }        
        pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
       if("VICS".equalsIgnoreCase(pdfObject.getFundId()) &&  "Income Protection".equalsIgnoreCase(memberProductDetails.getProductName()) && ("$0".equalsIgnoreCase(memberProductDetails.getProductAmount()) || memberProductDetails.getProductAmount()==null)){
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph("", FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
       }else{
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph( memberProductDetails.getWaitingPeriod(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
       }   
        pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
        if("VICS".equalsIgnoreCase(pdfObject.getFundId()) &&  "Income Protection".equalsIgnoreCase(memberProductDetails.getProductName()) && ("$0".equalsIgnoreCase(memberProductDetails.getProductAmount()) || memberProductDetails.getProductAmount()==null)){
        	 pdfSummaryDtsCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        }else{
        	 pdfSummaryDtsCell3 = new PdfPCell(
                     pdfAPI.addParagraph( memberProductDetails.getBenefitPeriod(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        }
        
       
        pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
	        pdfSummaryDtsCell3 = new PdfPCell(
	                pdfAPI.addParagraph( memberProductDetails.getOccupationRating(), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
	        pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
	        pdfSummaryDtsCell3.setPadding( 4);
	        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
	        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);    
       
        	pdfSummaryDtsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( CommonPDFHelper.formatAmount( memberProductDetails.getProductPremiumCost()), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
            pdfSummaryDtsCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
       
        
        
        if("UW".equalsIgnoreCase(pdfType)){    
        	if(pdfObject.getMemberDetails().isClientMatched()){
        		if(pdfObject.getMemberDetails().isShortFormDcl()){
        			/*SRKR:06/09/2016: WR14370 - Update RUW message to RUW due to client match and declined on short form.*/
        			pdfSummaryDtsCell3 = new PdfPCell(
                            pdfAPI.addParagraph("RUW due to client match and declined on short form", FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        		}else{
        			pdfSummaryDtsCell3 = new PdfPCell(
                            pdfAPI.addParagraph("RUW with reason client match", FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        		}
        		 
        	}else{
        		 pdfSummaryDtsCell3 = new PdfPCell(
                         pdfAPI.addParagraph(getCoverUWDetails(memberProductDetails.getProductName(), pdfObject.getMemberDetails().getResponseObject(), memberProductDetails,pdfObject.isDownSaleInd(),pdfObject), FontFactory.HELVETICA, 7, Font.NORMAL, pdfObject.getColorStyle()));
        	}
        	
             pdfAPI.disableBorders( pdfSummaryDtsCell3);
             pdfSummaryDtsCell3.setPadding( 4);
             pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);  
        }      
        
            if (null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length() > 0) {
            	                         
                
                
                if(null != memberProductDetails.getExclusions() && !memberProductDetails.getExclusions().contains("Revised Offer")){
                	pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "Exclusion(s):", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                }else{
                	pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( " ", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));	
                }
                
                
                pdfPExclusionCell1.disableBorderSide( 3);
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                } else{
                	pdfPExclusionCell1.setColspan(7);
                }
                
                             
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }                        
                
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getExclusions(), FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
	              
	                  
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPEmptyCell1 = new PdfPCell(
                             pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                     pdfAPI.disableBorders( pdfPEmptyCell1);
                     pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }             
              
            } 
            if (null!=memberProductDetails.getPrductDecReason() && memberProductDetails.getPrductDecReason().length() > 0) {
            	 
            	             
                pdfPExclusionCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Additional cover alteration:", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                pdfPExclusionCell1.disableBorderSide( 3);
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else{ 
                	pdfPExclusionCell1.setColspan(7);
                }
               
                 
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }          
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getPrductDecReason(), FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else{
                	 pdfPEmptyCell1.setColspan(7);
                }               
                 
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	  pdfPEmptyCell1 = new PdfPCell(
                              pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                      pdfAPI.disableBorders( pdfPEmptyCell1);
                      pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }              
               
            }
            
            if (null!=memberProductDetails.getLoadings() && memberProductDetails.getLoadings().length() > 0) {
            	                         
                pdfPExclusionCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                pdfPExclusionCell1.disableBorderSide( 3);
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else{
                	pdfPExclusionCell1.setColspan(7);
                }
                
                             
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }                        
                
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getLoadings(), FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
	              
	                  
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPEmptyCell1 = new PdfPCell(
                             pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                     pdfAPI.disableBorders( pdfPEmptyCell1);
                     pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }             
              
            }
            
            /**
             * Display the FulAmounts
             */
            if (null!=memberProductDetails.getFulCoverAmount() && memberProductDetails.getFulCoverAmount().length() > 1 && "UW".equalsIgnoreCase(pdfType)) {
                
                pdfPExclusionCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "FUL cover amount:", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                pdfPExclusionCell1.disableBorderSide( 3);
                pdfPExclusionCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
                
                             
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLDITALIC, pdfObject.getColorStyle()));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }                        
                
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getFulCoverAmount(), FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
                if("TCOVER".equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
	              
	                  
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPEmptyCell1 = new PdfPCell(
                             pdfAPI.addSingleCell( pdfAPI.addParagraph( "", FontFactory.HELVETICA, 6, Font.ITALIC, pdfObject.getColorStyle()), false));
                     pdfAPI.disableBorders( pdfPEmptyCell1);
                     pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }             
              
            }
            
            
            
            pdfSummaryDtsCell3 = null;   
            pdfPExclusionCell1 = null;
            pdfPEmptyCell1 = null;
        
    }
	
	 public static String getCoverUWDetails(String coverType,
	            ResponseObject responseObject, MemberProductDetails memberProductDetails,boolean isDownsale, PDFObject pdfObject) {     		
		 if (coverType.equalsIgnoreCase( "Death")) {
	            coverType = "Term-Group";
	        } else if (coverType.contains("Total")) {
	            coverType = "TPD-Group";
	        } else if (coverType.equalsIgnoreCase( "Trauma")) {
	            coverType = "Trauma";
	        } else {
	            coverType = "IP";
	        }
	        StringBuffer strBuff = new StringBuffer();	        
	            if (null != responseObject
	                    && null != responseObject.getClientData()
	                    && null != responseObject.getClientData().getListInsured()) {
	                for (int insuredItr = 0; insuredItr < responseObject.getClientData().getListInsured().size(); insuredItr++) {
	                    Insured insured = (Insured) responseObject.getClientData().getListInsured().get( insuredItr);
	                    if (null != insured) {
	                        for (int overallItr = 0; overallItr < insured.getListOverallDec().size(); overallItr++) {
	                            Decision decision = (Decision) insured.getListOverallDec().get( overallItr);
	                            if (null != decision) {
	                                if (decision.getProductName().contains( coverType)) {
	                                    strBuff.append( decision.getDecision());
	                                    /**
	                                     * PDF display is wrong in postprocessing  - contact bala 
	                                     */
	                                    
	                                   /* if(null!=decision.getListRating() && decision.getListRating().size()>0){
	                                    	for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
		                                        Rating rating = (Rating) decision.getListRating().get( ratingItr);
		                                        if (null != rating) {
		                                            strBuff.append( " with rating: ");
		                                            strBuff.append( rating.getValue());
		                                            strBuff.append( ",");
		                                            strBuff.append( " with reason :");
		                                            strBuff.append( rating.getReason());
		                                        }
		                                        
		                                    }
	                                    }else*/ 
	                                    
	                                    System.out.println("How much decision >> "+decision.getReasons().length);
	                                    
	                                    
	                                    if(null!=decision.getReasons() && decision.getReasons().length>0){	                                    	
	                                    	
	                                    	 if(null!=decision.getTotalDebitsValue() && !"".equalsIgnoreCase(decision.getTotalDebitsValue().trim()) && new BigDecimal(decision.getTotalDebitsValue()).doubleValue() > 0){
	                                    		 	strBuff.append( " with rating: ");
		                                            strBuff.append( decision.getTotalDebitsValue());
		                                            strBuff.append( ",");
	                                    	 }
	                                    	for (int reasonItr = 0; reasonItr <decision.getReasons().length; reasonItr++) {
	                                    		strBuff.append( " with reason: ");
	                                    		strBuff.append( decision.getReasons()[reasonItr]);
	                                    		strBuff.append( ",");
	                                    		
	                                    	}
	                                    	
	                                    }
	                                }
	                            }
	                            
	                        }
	                    }
	                    
	                }
	            }else if("NUW".equalsIgnoreCase(memberProductDetails.getAalLevelStatus())){
	            	strBuff.append("ACC");
	            }
	            //ING downsale
	            if("Term-Group".equalsIgnoreCase(coverType) && isDownsale){
	            	strBuff = new StringBuffer();
	            	 strBuff.append("ACC");
	            }
	            if(pdfObject!=null && pdfObject.getFormLength()!=null && pdfObject.getFormLength().equalsIgnoreCase("Long") &&
	            		pdfObject.getProductName()!=null && pdfObject.getProductName().equalsIgnoreCase("VICS") && strBuff!=null &&  strBuff.toString().length()>0){
	            	strBuff = new StringBuffer();
	            	strBuff.append("RUW");
	            }
	        return strBuff.toString();
	    }
	 
	 	public static String toTitleCase(String input) {
		    StringBuilder titleCase = new StringBuilder();
		    boolean nextTitleCase = true;
		    if(null!=input && input.trim().length()>0){
		    	input = input.toLowerCase();
		        for (char c : input.toCharArray()) {
			        if (Character.isSpaceChar(c)) {
			            nextTitleCase = true;
			        } else if (nextTitleCase) {
			            c = Character.toTitleCase(c);
			            nextTitleCase = false;
			        }

			        titleCase.append(c);
			    }
		    }
		

		    return titleCase.toString();
		}

}
