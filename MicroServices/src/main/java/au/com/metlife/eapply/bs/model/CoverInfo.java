package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CoverInfo implements Serializable {

	/**
	 * Author 175373
	 */
	private static final long serialVersionUID = 1L;
	
	private String benefitType;
	private String coverDecision;
	private String coverReason;
	private String coverExclusion;
	private BigDecimal existingCoverAmount;
	private String fixedUnit;
	private String loading;
	private String policyFee;
	private String occupationRating;
	private String coverOption;
	private String cost;
	private String coverCategory;
	private String deathInputTextValue;
	private String tpdInputTextValue;
	private String ipInputTextValue;
	private String deathFixedAmt;
	private String additionalCoverAmount;
	private String frequencyCostType;
	private int optionalUnit;
	private String existingIpBenefitPeriod;
	private String existingIpWaitingPeriod;
	private String additionalUnit;
	private String additionalIpBenefitPeriod;
	private String additionalIpWaitingPeriod;
	private BigDecimal transferCoverAmount;
	private String totalIpWaitingPeriod;
	private String totalIpBenefitPeriod;
	
	//Added for INGD
	private String existingCoverCategory;
		
	public BigDecimal getTransferCoverAmount() {
		return transferCoverAmount;
	}
	public void setTransferCoverAmount(BigDecimal transferCoverAmount) {
		this.transferCoverAmount = transferCoverAmount;
	}
	public String getTotalIpWaitingPeriod() {
		return totalIpWaitingPeriod;
	}
	public void setTotalIpWaitingPeriod(String totalIpWaitingPeriod) {
		this.totalIpWaitingPeriod = totalIpWaitingPeriod;
	}
	public String getTotalIpBenefitPeriod() {
		return totalIpBenefitPeriod;
	}
	public void setTotalIpBenefitPeriod(String totalIpBenefitPeriod) {
		this.totalIpBenefitPeriod = totalIpBenefitPeriod;
	}
	public String getBenefitType() {
		return benefitType;
	}
	public void setBenefitType(String benefitType) {
		this.benefitType = benefitType;
	}
	public String getCoverDecision() {
		return coverDecision;
	}
	public void setCoverDecision(String coverDecision) {
		this.coverDecision = coverDecision;
	}
	public String getCoverReason() {
		return coverReason;
	}
	public void setCoverReason(String coverReason) {
		this.coverReason = coverReason;
	}
	public String getCoverExclusion() {
		return coverExclusion;
	}
	public void setCoverExclusion(String coverExclusion) {
		this.coverExclusion = coverExclusion;
	}
	public BigDecimal getExistingCoverAmount() {
		return existingCoverAmount;
	}
	public void setExistingCoverAmount(BigDecimal existingCoverAmount) {
		this.existingCoverAmount = existingCoverAmount;
	}
	public String getFixedUnit() {
		return fixedUnit;
	}
	public void setFixedUnit(String fixedUnit) {
		this.fixedUnit = fixedUnit;
	}
	public String getLoading() {
		return loading;
	}
	public void setLoading(String loading) {
		this.loading = loading;
	}
	public String getPolicyFee() {
		return policyFee;
	}
	public void setPolicyFee(String policyFee) {
		this.policyFee = policyFee;
	}
	public String getOccupationRating() {
		return occupationRating;
	}
	public void setOccupationRating(String occupationRating) {
		this.occupationRating = occupationRating;
	}
	public String getCoverOption() {
		return coverOption;
	}
	public void setCoverOption(String coverOption) {
		this.coverOption = coverOption;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getCoverCategory() {
		return coverCategory;
	}
	public void setCoverCategory(String coverCategory) {
		this.coverCategory = coverCategory;
	}
	public String getDeathInputTextValue() {
		return deathInputTextValue;
	}
	public void setDeathInputTextValue(String deathInputTextValue) {
		this.deathInputTextValue = deathInputTextValue;
	}
	public String getTpdInputTextValue() {
		return tpdInputTextValue;
	}
	public void setTpdInputTextValue(String tpdInputTextValue) {
		this.tpdInputTextValue = tpdInputTextValue;
	}
	public String getIpInputTextValue() {
		return ipInputTextValue;
	}
	public void setIpInputTextValue(String ipInputTextValue) {
		this.ipInputTextValue = ipInputTextValue;
	}
	public String getDeathFixedAmt() {
		return deathFixedAmt;
	}
	public void setDeathFixedAmt(String deathFixedAmt) {
		this.deathFixedAmt = deathFixedAmt;
	}
	public String getAdditionalCoverAmount() {
		return additionalCoverAmount;
	}
	public void setAdditionalCoverAmount(String additionalCoverAmount) {
		this.additionalCoverAmount = additionalCoverAmount;
	}
	public String getFrequencyCostType() {
		return frequencyCostType;
	}
	public void setFrequencyCostType(String frequencyCostType) {
		this.frequencyCostType = frequencyCostType;
	}
	public int getOptionalUnit() {
		return optionalUnit;
	}
	public void setOptionalUnit(int optionalUnit) {
		this.optionalUnit = optionalUnit;
	}
	public String getExistingIpBenefitPeriod() {
		return existingIpBenefitPeriod;
	}
	public void setExistingIpBenefitPeriod(String existingIpBenefitPeriod) {
		this.existingIpBenefitPeriod = existingIpBenefitPeriod;
	}
	public String getExistingIpWaitingPeriod() {
		return existingIpWaitingPeriod;
	}
	public void setExistingIpWaitingPeriod(String existingIpWaitingPeriod) {
		this.existingIpWaitingPeriod = existingIpWaitingPeriod;
	}
	public String getAdditionalUnit() {
		return additionalUnit;
	}
	public void setAdditionalUnit(String additionalUnit) {
		this.additionalUnit = additionalUnit;
	}
	public String getAdditionalIpBenefitPeriod() {
		return additionalIpBenefitPeriod;
	}
	public void setAdditionalIpBenefitPeriod(String additionalIpBenefitPeriod) {
		this.additionalIpBenefitPeriod = additionalIpBenefitPeriod;
	}
	public String getAdditionalIpWaitingPeriod() {
		return additionalIpWaitingPeriod;
	}
	public void setAdditionalIpWaitingPeriod(String additionalIpWaitingPeriod) {
		this.additionalIpWaitingPeriod = additionalIpWaitingPeriod;
	}
	public String getExistingCoverCategory() {
		return existingCoverCategory;
	}
	public void setExistingCoverCategory(String existingCoverCategory) {
		this.existingCoverCategory = existingCoverCategory;
	}
	
	
}

