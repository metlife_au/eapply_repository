package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesPaymentPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesPaymentPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesPaymentPage enterColesPayment() {
		fillColesPayment();
		return new ColesPaymentPage(scriptHelper);
	}

	public ColesPaymentPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillColesPayment() {
		try {
			String cardType = dataTable.getData("MetLife_Data", "CardType");
			String cardNumber = dataTable.getData("MetLife_Data","CardNumber");
			String cardName = dataTable.getData("MetLife_Data", "CardName");
			String expiryMonth = dataTable.getData("MetLife_Data","ExpiryMonth");
			String expiryYear = dataTable.getData("MetLife_Data", "ExpiryYear");

			tryAction(waitForClickableElement(ColesPaymentPageObjects.rdCreditCard),
					"Click", "Credit Card");
			
            if(cardType.equalsIgnoreCase("Visa")) {
            	
			tryAction(waitForClickableElement(ColesPaymentPageObjects.rdCCVisa),
					"Click", "Credit Card - VISA");
            }
            else {
            	
            	tryAction(waitForClickableElement(ColesPaymentPageObjects.rdCCMaster),
    					"Click", "Credit Card - Master");
                  }
			tryAction(
					waitForClickableElement(ColesPaymentPageObjects.txtCardNumber),
					"SET", "Card Number", cardNumber);
			
			tryAction(waitForClickableElement(ColesPaymentPageObjects.txtCardNumber),"TAB", "Card Number");
			sleep();
			
			tryAction(waitForClickableElement(ColesPaymentPageObjects.txtCardName),"SET", "Card Name", cardName);
			tryAction(waitForClickableElement(ColesPaymentPageObjects.txtCardName),"TAB", "Card Name");

			sleep(1000);
			tryAction(fluentWaitElement(ColesPaymentPageObjects.selCCExpMonth),"specialDropdown", "Expiry Month", expiryMonth);
			
			sleep(1000);
			tryAction(fluentWaitElement(ColesPaymentPageObjects.selCcExpYr),"specialDropdown", "Expiry Year", expiryYear);

			sleep(2000);
			tryAction(waitForClickableElement(ColesPaymentPageObjects.chkAuthorise),
					"Click", "Authorise Payment");

			tryAction(waitForClickableElement(ColesPaymentPageObjects.btnSubmit),
					"Click", "Submit");
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
