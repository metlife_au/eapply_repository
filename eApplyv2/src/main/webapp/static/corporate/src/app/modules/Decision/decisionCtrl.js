(function(angular){
'use strict';

angular
	.module('CorporateApp')
	.controller('decisionCtrl',decisionCtrl);
	decisionCtrl.$inject=['$rootScope','$location','PersistenceService','DownloadPDFService','ngDialog','$timeout','$window','fetchUrlSvc','appData','inputDataService','fetchCategoryDetailsService'];
function decisionCtrl($rootScope,$location,PersistenceService, DownloadPDFService,ngDialog,$timeout,$window,fetchUrlSvc,appData,inputDataService,fetchCategoryDetailsService){
	var vm= this;
	vm.message= 'Decision Page';
	vm.categoryDetails = appData.getFundCategoryDetails();
	vm.brokernumber = vm.categoryDetails.partner.broker.brokerCnctNum;
	vm.urlList = fetchUrlSvc.getUrlList();
	vm.decision=appData.getSummaryPageAuradecision();
	vm.finaldecision=vm.decision.overallDecision;
	vm.userDetails={};
	vm.userDetails=appData.getAppData();
	vm.specialTerm=vm.decision.specialTerm;
	if(	vm.finaldecision!=='RUW' && vm.finaldecision!=='DCL'){
		
		var auraDecisions = [];
		auraDecisions.push(vm.decision.deathDecision);
		auraDecisions.push(vm.decision.tpdDecision);
		auraDecisions.push(vm.decision.ipDecision);
		
		if(auraDecisions.indexOf('ACC') !== -1 && auraDecisions.indexOf('DCL') !== -1){
			vm.finaldecision='MXD';
		}
	}

	var pdfLocation = PersistenceService.getPDFLocation();
	var npsTokenUrl = PersistenceService.getNpsUrl();
	 $rootScope.$broadcast('enablepointer');
	 if(vm.specialTerm){
	 vm.loading= false;
	 vm.loadingexclusion= false;
	 vm.exclusions = false;
	 if((vm.decision.deathLoading!==undefined && vm.decision.deathLoading!==0) || (vm.decision.tpdLoading!==undefined && vm.decision.tpdLoading!==0) || (vm.decision.ipLoading!==undefined && vm.decision.ipLoading!==0) ){
		vm. loading= true;
	 }
	 else if(((vm.decision.deathLoading!==undefined && vm.decision.deathLoading!==0) || (vm.decision.tpdLoading!==undefined && vm.decision.tpdLoading!==0) || (vm.decision.ipLoading!==undefined && vm.decision.ipLoading!==0))&& (vm.decision.deathExclusions!==null ||vm.decision.tpdExclusions!==null || vm.decision.ipExclusions!==null )){
		 vm.loadingexclusion= true;
	 }
	 else if(vm.decision.deathExclusions!==null ||vm.decision.tpdExclusions!==null || vm.decision.ipExclusions!==null){
		 vm.exclusions= true;
	 }
	 }
	 vm.coverDetails={};
	 vm.coverDetails = inputDataService.getCoverDetails();


	vm.go = function ( path ) {
		$location.path( path );
	};

  	vm.navigateToLandingPage = function (){
  		 var navigateToLandingPagePopup='<div class="ngdialog-content"><div class="modal-body">';
  		navigateToLandingPagePopup +='<!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br>';
  		navigateToLandingPagePopup +='<div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter">';
  		navigateToLandingPagePopup +='<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button>';
  		navigateToLandingPagePopup +='<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
  		navigateToLandingPagePopup +=' </div></div>';

  		ngDialog.openConfirm({
            template:navigateToLandingPagePopup,
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path('/landing');
        }, function(e){
        	if(e==='oncancel'){
        		return false;
        	}
        });
    };
 // NPS survey popup
var surveyPopUp='<div class="ngdialog-content"><div class="modal-header">';
surveyPopUp +='<h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div>';
surveyPopUp +='<div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p>';
surveyPopUp +='<div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>';
surveyPopUp +='<p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter">';
surveyPopUp +='<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button> ';
surveyPopUp +='<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>';
ngDialog.openConfirm({
   		template: surveyPopUp,
 		className: 'ngdialog-theme-plain custom-width',
 		plain: true,
   	}).then(function (value) {
   		if(value==='onYes'){
  			if(npsTokenUrl != null){
  			  var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+npsTokenUrl;
					$window.open(url, '_blank');
					return true;
  			}
  		}
   	},function(value){
   		if(value === 'oncancel'){
   			return false;
   		}
   	});
    vm.downloadPDF = function(){
  		var filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		var a = document.createElement('a');
  	    document.body.appendChild(a);
  	  DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
  			if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
   	           window.navigator.msSaveBlob(res.data.response,filename);
   	       }else{
  	        var fileURL = URL.createObjectURL(res.data.response);
  	        a.href = fileURL;
  	        a.download = filename;
  	        a.click();
   	       }
  		}, function(err){
  			console.log('Error downloading the PDF ' + err);
  		});
  	};
}

})(angular);
