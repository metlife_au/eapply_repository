package au.com.metlife.eapply.bs.controller;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.metlife.eapply.bs.model.RefOccmapping;
import au.com.metlife.eapply.bs.service.RefOccmappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@Component
public class RefOccmappingController {
	private static final Logger log = LoggerFactory.getLogger(RefOccmappingController.class);
	
	private final RefOccmappingService refOccmappingService;
	
	 @Autowired
	    public RefOccmappingController(final RefOccmappingService refOccmappingService) {
	        this.refOccmappingService = refOccmappingService;
	    }
	 
	 @RequestMapping("/getNewOccupationList")
	  public ResponseEntity<List<RefOccmapping>> getNewOccupationList(@RequestParam(value="fundId") String fundId ,@RequestParam(value="occName") String occName) {	
		 log.info("Occupation list retrive start");
		 log.info(">>>fundId {} and >>>>indcc {} ",fundId,occName);
		List<RefOccmapping> refOccmapping =null;
		if(fundId!=null && occName!=null){
			refOccmapping =  refOccmappingService.getNewOccupationName(fundId, occName);
		}
		
	    if(refOccmapping==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }
	    log.info("Occupation list retrive finsih");
	    return new ResponseEntity<>(refOccmapping, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/getUniqueNumber")
	  public ResponseEntity<String> getUniqueNumber() {	
		 log.info("Getting unique number start");
		Calendar cal = Calendar.getInstance(); 
		log.info("Getting unique number finish");
	    return new ResponseEntity<>(Long.toString(cal.getTimeInMillis()), HttpStatus.OK);
		 
	 }
	 
	 

}
