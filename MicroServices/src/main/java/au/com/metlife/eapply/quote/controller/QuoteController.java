package au.com.metlife.eapply.quote.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.quote.model.ConvertModel;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.ProductConfigBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.model.SelectItem;
import au.com.metlife.eapply.quote.service.QuoteService;
import au.com.metlife.eapply.quote.utility.GeneralHelper;
import au.com.metlife.eapply.quote.utility.QuoteConstants;

@RestController
@Component
public class QuoteController {
	private static final Logger log = LoggerFactory.getLogger(QuoteController.class);
	private final QuoteService quoteService;

	@Autowired
    public QuoteController(final QuoteService quoteService) {
        this.quoteService = quoteService;
    }
	
	
	
	 @RequestMapping("/getIndustryList")
	  public ResponseEntity<List<SelectItem>> getIndustryList(@RequestParam(value="fundCode") String fundCode) {	
		 log.info("Get Industry list start");
		 List<SelectItem> industryList=null;
		 List<SelectItem> indList=new ArrayList<>();
		 try{
			 if(fundCode!=null){
				 industryList=quoteService.getIndustryList(fundCode);
					if(industryList!=null){
						for(int iterator=0; iterator<industryList.size();iterator++){
							SelectItem selectItem = industryList.get(iterator);
							if(selectItem!=null){
								if(selectItem.getValue()!=null && !"".equalsIgnoreCase(selectItem.getValue())){
									if(selectItem.getValue().contains("_")){
										String label_ind = selectItem.getValue().replace("_", ",");
										selectItem.setValue(label_ind);
									}
								}
								indList.add(new SelectItem(selectItem.getKey(),selectItem.getValue()));
							}
						}
					}
			 }
		 }catch(Exception e){
			 log.error("Error in get industry list : {}",e);
		 }
	    if(industryList==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
	    }
	    log.info("Get Industry list finish");
	    return new ResponseEntity<>(indList, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/getProductConfig")
	  public ResponseEntity<List<ProductConfigBO>> getProductConfig(@RequestParam(value="fundCode") String fundCode,
			  @RequestParam(value="memberType") String memberType,
			  @RequestParam(value="manageType") String manageType) {	
		 log.info("Get product config start");
		InstProductRuleBO instProductRuleBO=null;
		List<ProductConfigBO> productList=null;
		ProductConfigBO productConfigBO=null;
		 try{
			 if(fundCode!=null){
				 instProductRuleBO=quoteService.getProductConfig(fundCode, memberType, manageType);
				 productConfigBO=new ProductConfigBO();
				 productConfigBO.setProductCode(instProductRuleBO.getProductCode());
				 productConfigBO.setMemberType(instProductRuleBO.getMemberType());
				 productConfigBO.setManageType(instProductRuleBO.getManageType());
				 productConfigBO.setDeathMaxAmount(instProductRuleBO.getDeathMaxAmt());
				 productConfigBO.setTpdMaxAmount(instProductRuleBO.getTpdMaxAmt());
				 productConfigBO.setIpMaxAmount(instProductRuleBO.getIpMaxAmt());
				 productConfigBO.setDeathTpdTransferMaxAmt(instProductRuleBO.getDeathTpdTransferMaxAmt());
				 productConfigBO.setDeathTpdTransferMaxUnits(instProductRuleBO.getDeathTpdTransferMaxUnits());
				 productConfigBO.setIpTransferMaxAmt(instProductRuleBO.getIpTransferMaxAmt());
				 productConfigBO.setAnnualSalForUpgradeVal(instProductRuleBO.getAnnualSalForUpgradeVal());
				 productConfigBO.setAnnualSalMaxLimit(instProductRuleBO.getAnnualSalMaxLmt());
				 productConfigBO.setIpUnitCostMulitiplier(instProductRuleBO.getIpUnitCostMultipFactor());
				 productList=new ArrayList<>();
				 productList.add(productConfigBO);
			 }
		 }catch(Exception e){
			log.error("Error in get product config: {}",e);
		 }
	    if(productList==null){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
	    }
	    log.info("Get product config finish");
	    return new ResponseEntity<>(productList, HttpStatus.OK);
	 }

/*	 @RequestMapping(value = "/getIndustryList",method = RequestMethod.GET)
	  public ResponseEntity<List<SelectItem>> getIndustryList(@RequestParam(value="fundCode") InputStream inputStream) {
		 BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		 String line = null;	
		 String string = "";		
		 List<SelectItem> industryList=null;
			try {
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					log.info("line>>{}",line);	
					if(line!=null && line.trim().length()>0){
						JSONObject jsonObject = new JSONObject(string);
		                log.info("fundCode--->> {}",jsonObject.get("fundCode"));
		                if(jsonObject.get("fundCode")!=null){
		                	industryList=quoteService.getIndustryList(jsonObject.get("fundCode").toString());
		                }
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}	

	    if(industryList==null){
	        return new ResponseEntity<List<SelectItem>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<SelectItem>>(industryList, HttpStatus.OK);
		 
	 }
	*/
	 
	 @RequestMapping(value = "/calculateDeath", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateDeath(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate death start");
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateDeath(ruleModel));
                 }
           } catch (Exception e) {
                 log.error("Error in calculate death: {}",e);
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate death finish");
         return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateTPD", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateTPD(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate TPD start");
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateTpd(ruleModel));
                 }
           } catch (Exception e) {
        	   log.error("Error in calculate TPD : {}",e);
        	  
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT); /*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate TPD finish");
         return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateIP", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateIP(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate IP start");
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateIp(ruleModel));
                 }
           } catch (Exception e) {
                log.error("Error in calculate IP : {}",e);
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate IP finish");
         return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateAllOld", method = RequestMethod.POST)
	  public ResponseEntity<List<QuoteResponse>> calculateAllOld(InputStream inputStream) {	
		 log.info("Calculate All old start");
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;	
			String string = "";			
			RuleModel ruleModel = null;
			List<QuoteResponse> quoteResponseList=null;
			try {
				quoteResponseList=new ArrayList<>();
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					log.info("line>> {}",line);	
					if(line.trim().length()>0){
						JSONObject jsonObject = new JSONObject(string);
		                log.info("age--->>{}",jsonObject.get("age"));
						ruleModel = GeneralHelper.convertToRuleModel(jsonObject);
					}
				}
				if(ruleModel!=null){
					quoteResponseList.add(quoteService.calculateDeath(ruleModel));
					quoteResponseList.add(quoteService.calculateTpd(ruleModel));
					quoteResponseList.add(quoteService.calculateIp(ruleModel));
				}
			} catch (IOException e) {
				log.error("Error in Input and output: {}",e);
			} catch (JSONException e) {
				log.error("Error in Json: {}",e);
			} catch (Exception e) {
				log.error("Error in calculate all old : {}",e);
			}	
		    if(quoteResponseList==null){
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
		    }
            log.info("END-- {}",new  java.util.Date());
		    log.info("Calculate All old finish");
		    return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/calculateAll", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateAll(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate all start");
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateDeath(ruleModel));
                       quoteResponseList.add(quoteService.calculateTpd(ruleModel));
                       quoteResponseList.add(quoteService.calculateIp(ruleModel));
                 }
           } catch (Exception e) {
               log.error("Error in calculate all : {}",e);
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate all finish");
         return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
   }

	 
	 @RequestMapping(value = "/calculateFUL", method = RequestMethod.POST)
     public ResponseEntity<QuoteResponse> calculateFUL(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate FUL Amount start");
           QuoteResponse quoteResponse=null;
           try {
                 if(ruleModel!=null){
                	 quoteResponse =quoteService.calculateFUL(ruleModel);
                 }
           } catch (Exception e) {
               log.error("Error in calculate all:  {}",e);
           }     
         if(quoteResponse==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate all finish");
         return new ResponseEntity<>(quoteResponse, HttpStatus.OK);
   }
	 
	 @RequestMapping(value = "/calculateTransfer", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateTransfer(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate transfer start");
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null){
                       quoteResponseList.add(quoteService.calculateDeath(ruleModel));
                       quoteResponseList.add(quoteService.calculateTpd(ruleModel));
                       quoteResponseList.add(quoteService.calculateIp(ruleModel));
                 }
           } catch (Exception e) {
                 log.error("Error in calculate transfer: {}",e);
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate transfer finish");
         return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/convertCover", method = RequestMethod.POST)
     public ResponseEntity<List<ConvertModel>> convertCover(@RequestBody RuleModel ruleModel) {
		 log.info("Cnovert cover start");
           List<ConvertModel> convertModelList=null;
           try {
        	   convertModelList=new ArrayList<>();
        	   ConvertModel convertModel=new ConvertModel();
                 if(ruleModel!=null){
                	 if(ruleModel.getExDeathCoverType()!=null){
                		 if(ruleModel.getExDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_UNITISED)){
                    		 convertModel.setExCoverType(ruleModel.getExDeathCoverType());
                    		 convertModel.setExCoverAmount(ruleModel.getDeathExistingAmount());
                    		 convertModel.setConvertedAmount(ruleModel.getDeathExistingAmount());
                		 }else if(ruleModel.getExDeathCoverType().equalsIgnoreCase(QuoteConstants.DC_FIXED)){
                    		 convertModel=quoteService.convertDeathFixedToUnits(ruleModel);
                		 }
                	 }
                	 if(ruleModel.getExTpdCoverType()!=null){
                		 if(ruleModel.getExTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_UNITISED)){
                    		 convertModel.setExCoverType(ruleModel.getExTpdCoverType());
                    		 convertModel.setExCoverAmount(ruleModel.getTpdExistingAmount());
                    		 convertModel.setConvertedAmount(ruleModel.getTpdExistingAmount());
                		 }else if(ruleModel.getExTpdCoverType().equalsIgnoreCase(QuoteConstants.TPD_FIXED)){
                    		 convertModel=quoteService.convertTpdFixedToUnits(ruleModel);
                		 }
                	 }
                	 convertModelList.add(convertModel);
                 }
           } catch (Exception e) {
                 log.error("Error in convert cover: {}",e);
           }     
         if(convertModelList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Cnovert cover finish");
         return new ResponseEntity<>(convertModelList, HttpStatus.OK);
	 }
	 
	 //CalculateAAL Logic
	 @RequestMapping(value = "/calculateAAL", method = RequestMethod.POST)
     public ResponseEntity<QuoteResponse> calculateAAL(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate AAL start");
           //List<QuoteResponse> quoteResponseList=null;
           QuoteResponse quoteResponse=null;
           try {
                 //quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null ){
                      // quoteResponseList.add(quoteService.calculateAAL(ruleModel));
                       quoteResponse =quoteService.calculateAAL(ruleModel);
                 }
           } catch (Exception e) {
                 log.error("Error in calculate death: {}",e);
           }     
         if(quoteResponse==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);/*You many decide to return HttpStatus.NOT_FOUND*/
         }
         log.info("Calculate death finish");
         return new ResponseEntity<>(quoteResponse, HttpStatus.OK);
   }
		//Added by 561132 for INGD
	 @RequestMapping(value = "/calculateINGDAll", method = RequestMethod.POST)
     public ResponseEntity<List<QuoteResponse>> calculateINGDAll(@RequestBody RuleModel ruleModel) {
		 log.info("Calculate all start");
           List<QuoteResponse> quoteResponseList=null;
           try {
                 quoteResponseList=new ArrayList<>();
                 if(ruleModel!=null){
                     if(ruleModel.getDeathFixedAmount()!=null) {  
                	 quoteResponseList.add(quoteService.calculateINGDDeath(ruleModel));
                     }
                     if(ruleModel.getTpdFixedAmount()!=null) {  
                	 quoteResponseList.add(quoteService.calculateINGDTpd(ruleModel));
                     }
                     if(ruleModel.getIpFixedAmount()!=null) {  
                     quoteResponseList.add(quoteService.calculateINGDIp(ruleModel));
                     }
                 }
           } catch (Exception e) {
               log.error("Error in calculate all : {}",e);
           }     
         if(quoteResponseList==null){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         }
         log.info("Calculate all finish");
         return new ResponseEntity<>(quoteResponseList, HttpStatus.OK);
   }
	 
	//Added by 561132 for INGD
		 @RequestMapping(value = "/calculateINGDDeathLifeCoverAmount", method = RequestMethod.POST)
	     public ResponseEntity<QuoteResponse> getINGDDeathLifeStageCover(@RequestParam String fundCode,@RequestParam String age) {
			 log.info("Calculate all start");
	           QuoteResponse quote = null;
	           try {
	                 if(fundCode!=null && age!=null){
	                	 quote=  quoteService.calculateLifeStageDeathCoverAmount(fundCode,age);
	                 }
	           } catch (Exception e) {
	               log.error("Error in calculate all : {}",e);
	           }     
	         if(quote==null){
	             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	         }
	         log.info("Calculate all finish");
	         return new ResponseEntity<>(quote, HttpStatus.OK);
	   }
		 
			//Added by 561132 for INGD
		 @RequestMapping(value = "/calculateINGDTPDLifeCoverAmount", method = RequestMethod.POST)
	     public ResponseEntity<QuoteResponse> getINGDTPDLifeStageCover(@RequestParam String fundCode,@RequestParam String age) {
			 log.info("Calculate all start");
	           QuoteResponse quote = null;
	           try {
	                 if(fundCode!=null && age!=null){
	                	 quote=  quoteService.calculateLifeStageTPDCoverAmount(fundCode,age);
	                 }
	           } catch (Exception e) {
	               log.error("Error in calculate all : {}",e);
	           }     
	         if(quote==null){
	             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	         }
	         log.info("Calculate all finish");
	         return new ResponseEntity<>(quote, HttpStatus.OK);
	   }
		 
		 
		//Added by 561132 for INGD
		 @RequestMapping(value = "/getAuraType", method = RequestMethod.POST)
	     public ResponseEntity<String> getAuraType(@RequestBody RuleModel ruleModel) {
			 log.info("Get INGD Aura type start");
			 String auraType = null;
	           try {
	                 if(ruleModel!=null){
	                	 auraType=  quoteService.getAuraType(ruleModel);
	                 }
	           } catch (Exception e) {
	               log.error("Error in INGD Aura Type : {}",e);
	           }     
	         if(auraType==null){
	             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	         }
	         log.info("Get Aura Type Finisg");
	         return new ResponseEntity<>(auraType, HttpStatus.OK);
	   }
}
