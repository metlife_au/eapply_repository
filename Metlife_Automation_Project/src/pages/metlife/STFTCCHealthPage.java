package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.STFTCCHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

public class STFTCCHealthPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTCCHealthPage yourHealth() {
		yourHealthDetails();
		return new STFTCCHealthPage(scriptHelper);
	}

	public STFTCCHealthPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourHealthDetails() {


		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			Thread.sleep(1000);
						
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTCCHealthPageObjects.btnNext)).click().perform();
			Thread.sleep(1000);

			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
