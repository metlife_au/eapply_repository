package au.com.metlife.eapply.bs.model;

public class EmailDetail {
	
	private EmailDecision acc;
	private EmailDecision dcl;
	private EmailDecision ruw;
	private EmailDecision accspl;
	
	
	
	public EmailDecision getAcc() {
		return acc;
	}
	public void setAcc(EmailDecision acc) {
		this.acc = acc;
	}
	public EmailDecision getDcl() {
		return dcl;
	}
	public void setDcl(EmailDecision dcl) {
		this.dcl = dcl;
	}
	public EmailDecision getRuw() {
		return ruw;
	}
	public void setRuw(EmailDecision ruw) {
		this.ruw = ruw;
	}
	public EmailDecision getAccspl() {
		return accspl;
	}
	public void setAccspl(EmailDecision accspl) {
		this.accspl = accspl;
	}
	
	
}
