package au.com.metlife;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ClaimsTrackerServiceApplicationTests {

	 @Autowired
	 private MockMvc mockMvc;
	
	@Test
	public void contextLoads() {
	}
	@Test
	public void retrieveClientData() throws Exception{
		
		
		this.mockMvc.perform(post("/retrieveClaimDetails").content(
				"{\"claimNo\":\"42966\"}"));
		
	}
	
	@Test
	public void testPostAPI1() throws Exception{
		
		this.mockMvc.perform(post("/postb2bdata11").content(
				"{\"securestring\":\"<firstname>anand</firstName>\"}"));
		//.andExpect(status().isCreated());
		
	}
	
	
}
