package au.com.metlife.quote.beans;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;



public class JAXBHelper {
	
	private static final String PACKAGE_NAME = JAXBHelper.class.getPackage().getName();
    private static final String CLASS_NAME = JAXBHelper.class.getName();

	public  static String marshallingOutput(QuoteInput qInput){
		final String METHOD_NAME = "marshallingOutput";
		String outputXML=null;
		try {
			JAXBContext context = JAXBContext.newInstance(QuoteInput.class);
		    javax.xml.bind.Marshaller m = context.createMarshaller();
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    
		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		    m.marshal(qInput, os);
		    
		    
		    byte b[]=os.toByteArray();
		   	    
		    
		    outputXML=new String(b);
		    
		    os.close();
		    //m.m
	    
		 } catch (Exception e) {
		 }
		return outputXML;
	}
	
	public  static String marshallingRequestObj(Request request){
		final String METHOD_NAME = "marshallingRequestObj";
		String outputXML=null;
		try {
			JAXBContext context = JAXBContext.newInstance(Request.class);
		    javax.xml.bind.Marshaller m = context.createMarshaller();
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    
		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		    m.marshal(request, os);
		    
		    
		    byte b[]=os.toByteArray();
		   	    
		    
		    outputXML=new String(b);
		    
		    os.close();
		    //m.m
	    
		 } catch (Exception e) {
		 }
		return outputXML;
	}
	
public  static QuoteOutput unMarshallingInput(String inputXML){
	final String METHOD_NAME = "unMarshallingInput";
	QuoteOutput input=null;
		try {
            JAXBContext jc = JAXBContext.newInstance ("au.com.metlife.quote.beans");            
            Unmarshaller u = jc.createUnmarshaller ();
           //File f = new File ("C:\\Documents and Settings\\rzameer\\Desktop\\eApplicationPropertyConfig.xml");
           JAXBElement element = (JAXBElement) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8"))  );
            input = (QuoteOutput) element.getValue ();
                      
       } catch (Exception e) {
       }
       return input;
	}


	public  static Root unMarshallingOut(String inputXML){
		final String METHOD_NAME = "unMarshallingOut";
		Root input=null;
		try {
            JAXBContext jc = JAXBContext.newInstance ("com.metlife.agent.beans");            
            Unmarshaller u = jc.createUnmarshaller ();
           //File f = new File ("C:\\Documents and Settings\\rzameer\\Desktop\\eApplicationPropertyConfig.xml");
           JAXBElement element = (JAXBElement) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8"))  );
            input = (Root) element.getValue ();
                      
       } catch (Exception e) {
       }
       return input;
	}
	
}
