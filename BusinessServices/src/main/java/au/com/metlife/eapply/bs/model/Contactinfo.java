package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CONTACTINFO database table.
 * 
 */
@Entity
@Table(name = "TBL_A_CONTACTINFO", schema="EAPPDB")
public class Contactinfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_A_CONTACTINFO_SEQ")
	@SequenceGenerator(name="TBL_A_CONTACTINFO_SEQ",sequenceName = "EAPPDB.TBL_A_CONTACTINFO_SEQ", initialValue=1, allocationSize=100)
	private long id;

	@Column(length=50)
	private String addressline1;

	@Column(length=50)
	private String addressline2;

	@Column(length=1)
	private String addupdate;

//	@Column(nullable=false, length=47)
//	private long applicantid;

	@Column(length=1)
	private String contactupdate;

	@Column(length=20)
	private String country;

	@Column(length=500)
	private String homephone;

	@Column(length=500)
	private String mobilephone;

	@Column(length=20)
	private String othercontactnumber;

	@Column(length=7)
	private String othercontacttype;

	@Column(length=4)
	private String postcode;

	@Column(length=500)
	private String preferedcontactdetails;

	@Column(length=20)
	private String preferedcontactnumber;

	@Column(length=50)
	private String preferedcontacttime;

	@Column(length=20)
	private String preferedcontacttype;

	@Column(length=10)
	private String state;

	@Column(length=50)
	private String streetaddress;

	@Column(length=100)
	private String streetname;

	@Column(length=5)
	private String streetnumber;

	@Column(length=50)
	private String streetype;

	@Column(length=50)
	private String suburb;

	@Column(length=5)
	private String unitnumber;

	@Column(length=500)
	private String workphone;
	
//	private Applicant applicant;
//
//	@OneToOne(cascade = {CascadeType.PERSIST})
//	@JoinColumn(name = "applicantid", nullable = false)
//	public Applicant getApplicant() {
//		return applicant;
//	}
//
//	public void setApplicant(Applicant applicant) {
//		this.applicant = applicant;
//	}

	public Contactinfo() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddressline1() {
		return this.addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return this.addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getAddupdate() {
		return this.addupdate;
	}

	public void setAddupdate(String addupdate) {
		this.addupdate = addupdate;
	}

//	public long getApplicantid() {
//		return this.applicantid;
//	}
//
//	public void setApplicantid(long applicantid) {
//		this.applicantid = applicantid;
//	}

	public String getContactupdate() {
		return this.contactupdate;
	}

	public void setContactupdate(String contactupdate) {
		this.contactupdate = contactupdate;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHomephone() {
		return this.homephone;
	}

	public void setHomephone(String homephone) {
		this.homephone = homephone;
	}

	public String getMobilephone() {
		return this.mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public String getOthercontactnumber() {
		return this.othercontactnumber;
	}

	public void setOthercontactnumber(String othercontactnumber) {
		this.othercontactnumber = othercontactnumber;
	}

	public String getOthercontacttype() {
		return this.othercontacttype;
	}

	public void setOthercontacttype(String othercontacttype) {
		this.othercontacttype = othercontacttype;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getPreferedcontactdetails() {
		return this.preferedcontactdetails;
	}

	public void setPreferedcontactdetails(String preferedcontactdetails) {
		this.preferedcontactdetails = preferedcontactdetails;
	}

	public String getPreferedcontactnumber() {
		return this.preferedcontactnumber;
	}

	public void setPreferedcontactnumber(String preferedcontactnumber) {
		this.preferedcontactnumber = preferedcontactnumber;
	}

	public String getPreferedcontacttime() {
		return this.preferedcontacttime;
	}

	public void setPreferedcontacttime(String preferedcontacttime) {
		this.preferedcontacttime = preferedcontacttime;
	}

	public String getPreferedcontacttype() {
		return this.preferedcontacttype;
	}

	public void setPreferedcontacttype(String preferedcontacttype) {
		this.preferedcontacttype = preferedcontacttype;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreetaddress() {
		return this.streetaddress;
	}

	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}

	public String getStreetname() {
		return this.streetname;
	}

	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}

	public String getStreetnumber() {
		return this.streetnumber;
	}

	public void setStreetnumber(String streetnumber) {
		this.streetnumber = streetnumber;
	}

	public String getStreetype() {
		return this.streetype;
	}

	public void setStreetype(String streetype) {
		this.streetype = streetype;
	}

	public String getSuburb() {
		return this.suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getUnitnumber() {
		return this.unitnumber;
	}

	public void setUnitnumber(String unitnumber) {
		this.unitnumber = unitnumber;
	}

	public String getWorkphone() {
		return this.workphone;
	}

	public void setWorkphone(String workphone) {
		this.workphone = workphone;
	}

}