package vicsuper;

/*public class FixCover {

}*/

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import vicsSuper_Objects.FixCoverObjects;
import vicsSuper_Objects.LandingPageObject;

public class FixCover extends MasterPage {
	WebDriverWait wait = new WebDriverWait(driver, 20);
	WebDriverUtil driverUtil = null;
//	JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	public FixCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	String XML;
	String SenarioType;
	String DataSheet;
	String occRating1;
	String occRating2;
	String occRating3;
	String BenefitPeriod;

	String DeathAmtLandingPg;
	String TPDAmtLandingPg;
	String IPAmtLandingPg;
	String DeathUnitLandingPg;
	String TPDUnitLandingPg;
	String IPUnitLandingPg;


	String DeathInsured;
	String TPDInsured;
	String DeathAmount;
	String TPDAmount;
	String TotalAmount;

	// Actions action = new Actions(driver.getWebDriver());

	// FixCoverObjects PerDet= new FixCoverObjects(scriptHelper);

	public FixCover FixYourCover(String DataSheetName, String Type) {

		SenarioType = Type;
		// PerDet.FixCoverObjectsInitialization(AppPage);
		XML = dataTable.getData("VicSuper_Login", "XML");
		DataSheet = DataSheetName;
		if (SenarioType.equalsIgnoreCase("Overall")) {
		} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {
			PremiumRateInputValidation();
		} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			occupationRatingInputValidation();
		} else {
			report.updateTestLog("Error Type", "Invalid Type value-" + SenarioType, Status.FAIL);
		}
		//pdfValidator();
		PersonalDetails();
		return new FixCover(scriptHelper);

	}

	public void PersonalDetails() {
		String DutyOfDisclosure = null;
		String PrivacyStatement = null;
		if (SenarioType.equalsIgnoreCase("Overall")) {
		DutyOfDisclosure = dataTable.getData(DataSheet, "DutyOfDisclosure");
		PrivacyStatement = dataTable.getData(DataSheet, "PrivacyStatement");
		}
		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String Gender = dataTable.getData(DataSheet, "Gender");

		try {

			sleep(5000);

			wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.fixYourCover)).click();

			// Popup Validation
			/*
			 * if (driver.findElement(By.xpath("//button[text()='Continue']")).
			 * isDisplayed() == true) {
			 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
			 * "//button[text()='Continue']"))).click(); sleep(5000); }
			 */
			sleep(5000);

			// -----------------------------------------------------PersonalDetails------------------------------------------------------------------------//
			// *****Agree to Duty of disclosure*******\\

			if (SenarioType.equalsIgnoreCase("Overall")) {
				wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_dutyOfDisclosure));


				String[] ArrayHeaderDD;
				int flag = 1;

				String part2;
				String[] value;
				String HeaderDD = null;

				String[] parts = DutyOfDisclosure.split("---");
				String DDTxt = driver.findElement(By.xpath(".//*[@name='formduty']//p[2]/strong")).getText();
				 if (DDTxt.equalsIgnoreCase(parts[0])) {
					}
					else {
						flag =0;
						report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
					}

				parts = parts[1].split("\\n\\n");

				int j=3;
				for (int a = 0; a < 9; a++) {

						if ( !(a==3) ) {
							HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//p)["+j+"]")).getText();
							j++;

							 if (HeaderDD.equalsIgnoreCase(parts[a])) {
								}
								else {
									flag =0;
									report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
								}


						}
					else {
						String[] parts1 = parts[a].split("\\n");
						int k=1;
						for (int b = 0; b < 5; b++) {

							System.out.println("K="+k);
							if ( !(b==4) ) {
								 HeaderDD = driver.findElement(By.xpath(".//*[@name='formduty']//ul/li["+k+"]")).getText();


							}
							else {
								 HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//strong)[2]")).getText();

							}
							 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

							 }
								else {
									flag =0;
									report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
								}

							k++;

						}

					}


				}



						if (flag==0) {
							report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is not as expected", Status.FAIL);
						}
						else {
							report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is present as expected", Status.PASS);

						}

				}
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_dutyOfDisclosure))
					.click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			// *****Agree to Privacy Statement*******\\
			if (SenarioType.equalsIgnoreCase("Overall")) {
				wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_privacyStatement));

				String[] ArrayHeaderDD;
				int flag = 1;

				String part2;
				String[] value;
				String HeaderDD = null;


				String[] parts = PrivacyStatement.split("---");
				String DDTxt = driver.findElement(By.xpath(".//*[@name='cancelprivacyPolicyForm']//p[2]/strong")).getText();
				 if (DDTxt.equalsIgnoreCase(parts[0])) {

					}
					else {
						flag =0;
						report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
					}

				parts = parts[1].split("\\n\\n\\n");
				int j=3;
					for (int a = 0; a < 2; a++) {

				HeaderDD = driver.findElement(By.xpath("(.//*[@name='cancelprivacyPolicyForm']//p)["+j+"]")).getText();

				 if (HeaderDD.equalsIgnoreCase(parts[a])) {

					}
					else {
						flag =0;
						report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
					}
				j++;
				}

					if (flag==0) {
						report.updateTestLog("Privacy statement", "Privacy statement - Statement is not as expected", Status.FAIL);

					}
					else {
						report.updateTestLog("Privacy statement", "Privacy statement - Statement is present as expected", Status.PASS);
					}

				}

			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_privacyStatement))
					.click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_email)).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_email))
					.sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
			sleep(300);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(FixCoverObjects.obj_PersonalDetails_contactType));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_phNO))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_phNO))
					.sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {
				wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_Morning))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			} else if (TimeofContact.equalsIgnoreCase("Afternoon")) {
				wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_Afternoon))
						.click();
				sleep(500);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for TimeofContact", Status.FAIL);
			}

			/*
			 * //***********Select the Gender******************\\
			 *
			 * if (Gender.equalsIgnoreCase("Male")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * FixCoverObjects.obj_PersonalDetails_Morning)).click();
			 * sleep(250); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS); } else if
			 * (Gender.equalsIgnoreCase("Female")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * FixCoverObjects.obj_PersonalDetails_Afternoon)).click();
			 * sleep(500); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS);
			 *
			 * } else { report.updateTestLog("DataTable",
			 * "Invalid value for TimeofContact", Status.FAIL); }
			 */

			// To click on Continue
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_Continue_ContactDetail)).click();
			sleep(2000);
			report.updateTestLog("Contact Details", "Contact Details - Continue button is clicked", Status.PASS);

			// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//
			if (SenarioType.equalsIgnoreCase("Overall")) {
				cover_Overall();
			} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {
				cover_Premium();
			} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {

			}

			if ((SenarioType.equalsIgnoreCase("Overall")) || (SenarioType.equalsIgnoreCase("OccupationRating"))) {
				wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_Continue_PersonalDetails))
						.click();
				sleep(250);
				report.updateTestLog("PersonalDetails - Continue", "PersonalDetails - Continue button is clicked",
						Status.PASS);
				 confirmation();
			}
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}

	private void confirmation() {
		sleep(3000);
		System.out.println("confirmation");
		String Confirmation_DetailsValidations = dataTable.getData(DataSheet, "Confirmation_DetailsValidations");
		System.out.println(Confirmation_DetailsValidations);
	if (Confirmation_DetailsValidations.equalsIgnoreCase("Yes")
				|| Confirmation_DetailsValidations.equalsIgnoreCase("Occupation")) {
			detailsValidation();
		}

		if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			return;
		} else if (SenarioType.equalsIgnoreCase("Overall")) {
			String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
			String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");
			String GeneralConsent = dataTable.getData(DataSheet, "GeneralConsent");

			try {


					// ******Agree to general Consent*******\\

				String part2;
				String[] value;
				String HeaderDD = null;
				int flag=1;
						wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_Confirmation_TermsConditionsCheckBox));
						String[] parts = GeneralConsent.split("\\n\\n");



						 int j=1;
						for (int a = 0; a < 4; a++) {

							if ( !(a==3) ) {
								HeaderDD = driver.findElement(By.xpath("(.//*[@id='collapse4']/div/div/div/div/p)["+j+"]")).getText();
								j++;

								 if (HeaderDD.equalsIgnoreCase(parts[a])) {

									}
									else {
										flag =0;
										report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
									}

							}
							else {
								System.out.println("<><><><><><>");

								String[] parts1 = parts[a].split("\\n");
								System.out.println("1)"+parts1[0]);
								System.out.println("2)"+parts1[1]);
								System.out.println("3)"+parts1[2]);
								System.out.println("4)"+parts1[3]);
								System.out.println("5)"+parts1[4]);
								System.out.println("6)"+parts1[5]);
								System.out.println("7)"+parts1[6]);
								System.out.println("8)"+parts1[7]);
								System.out.println("9)"+parts1[8]);
								System.out.println("10)"+parts1[9]);


								int k=1;
								for (int b = 0; b < 10; b++) {


										 HeaderDD = driver.findElement(By.xpath(".//*[@id='collapse4']/div/div/div/div/ul/li["+k+"]")).getText();

									 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

									 }
										else {
											flag =0;
											report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
										}

									k++;

								}

							}
						}

						if (flag==0) {
							report.updateTestLog("General Consent", "General Consent - Statement is not as expected", Status.FAIL);

						}
						else {
							report.updateTestLog("General Consent", "General Consent - Statement is present as expected", Status.PASS);
						}

						sleep(2000);

							wait.until(ExpectedConditions
									.elementToBeClickable(FixCoverObjects.obj_Confirmation_TermsConditionsCheckBox)).click();
							report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
							sleep(3000);

					// ****Click on Continue****\\
					sleep(3000);
					wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_Confirmation_Submit))
							.click();
					report.updateTestLog("Submit", "Submit button is Clicked", Status.PASS);
					sleep(5000);



				// *******Feedback popup******\\

				wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_DecisionPopUP_No)).click();
				report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
				sleep(2500);

				if (driver.getPageSource().contains("APPLICATION NUMBER")||driver.getPageSource().contains("Application Number")||driver.getPageSource().contains("Application number")) {

					// *****Fetching the Application Status*****\\

					String Appstatus = driver.findElement(FixCoverObjects.obj_DecisionPopUP_Appstatus).getText();
					sleep(500);


					if (Appstatus.equalsIgnoreCase(DecisionHeader)) {
						report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
					}

					String AppMsg = driver.findElement(FixCoverObjects.obj_DecisionPopUP_AppMsg).getText();
					sleep(500);
					System.out.println("////"+AppMsg+"////");
					if (AppMsg.equals(DecisionMsg)) {
						report.updateTestLog("Decision Message", "Decision Message - "+AppMsg+" is present", Status.PASS);
					}else {
						report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+DecisionMsg+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
					}



					// *****Fetching the Application Number******\\

					String App = driver.findElement(FixCoverObjects.obj_DecisionPopUP_App).getText();
					sleep(1000);
					report.updateTestLog("Decision Page", "Application No: " + App, Status.PASS);

					wait.until(
							ExpectedConditions.presenceOfElementLocated(FixCoverObjects.obj_Confirmation_Download))
							.click();
					sleep(3000);

					report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

					sleep(5000);
				//pdfValidator();
/*
					driver.get("chrome://downloads/");
					try {
						driver.switchTo().alert().accept();
						sleep(3000);
						System.out.println("Alert Handled");
					} catch (Exception e) {
						System.out.println("");
					}
			//		driver.get("chrome://downloads/");
					sleep(3000);


					// Create object of Robot class
					Robot robot = new Robot();
					Thread.sleep(1000);

					// Press TAB

					robot.keyPress(KeyEvent.VK_TAB);
					Thread.sleep(1000L);
					// Release Tab
					robot.keyRelease(KeyEvent.VK_TAB);

					robot.keyPress(KeyEvent.VK_TAB);
					Thread.sleep(1000L);
					// Release Tab
					robot.keyRelease(KeyEvent.VK_TAB);

					robot.keyPress(KeyEvent.VK_ENTER);
					robot.keyRelease(KeyEvent.VK_ENTER);
					sleep(3500);
					String oldTab = driver.getWindowHandle();
					ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
					newTab.remove(oldTab);
					driver.close();
				    sleep(500);
					driver.switchTo().window(newTab.get(0));
					sleep(4500);
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					URL url = new URL(driver.getCurrentUrl());
					System.out.println("url");*/

				}

				else {
					report.updateTestLog("Application Declined", "Application is declined", Status.FAIL);

				}

			} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}

		}

	}

	public void detailsValidation() {

		if (SenarioType.equalsIgnoreCase("Overall")) {
			String Gender = dataTable.getData(DataSheet, "Gender");
			String EmailId = dataTable.getData(DataSheet, "EmailId");
			String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
			String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
			String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
			String Indexation = dataTable.getData(DataSheet, "Indexation");


			String XML = dataTable.getData("VicSuper_Login", "XML");

			String[] part;
			String part2;
			String[] value;


			// To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1];
			value = part2.split("</dateOfBirth>");
			String dob = value[0];

			// First Name
			WebElement objFName = driver.findElement(FixCoverObjects.obj_Confirmation_FirstName);
			fieldValidation("WebElement", objFName,  VicSuper__Login.firstName, "First Name");

			// Last Name
			WebElement objLName = driver.findElement(FixCoverObjects.obj_Confirmation_LastName);
			fieldValidation("WebElement", objLName, VicSuper__Login.lastName, "Last Name");

			// Gender
			WebElement ObjGender = driver.findElement(FixCoverObjects.obj_Confirmation_Gender);
			fieldValidation("WebElement", ObjGender, Gender, "Genderquestion");

			// Phone Number type
			WebElement objcontactType = driver.findElement(FixCoverObjects.obj_Confirmation_ContactType);
			fieldValidation("WebElement", objcontactType, TypeofContact, "contactType");

			// PhoneNumber
			WebElement objContactNo = driver.findElement(FixCoverObjects.obj_Confirmation_ContactNo);
			fieldValidation("WebElement", objContactNo, ContactNumber, "ContactNo");

			// Email
			WebElement objcontactEmail = driver.findElement(FixCoverObjects.obj_Confirmation_Email);
			fieldValidation("WebElement", objcontactEmail, EmailId, "Email");
			js.executeScript("window.scrollBy(0,500)", "");
			// DOB
			WebElement objDOB = driver.findElement(FixCoverObjects.obj_Confirmation_DOB);
			fieldValidation("WebElement", objDOB, dob, "DOB");

			// Morning Afternoon
			WebElement objTime = driver.findElement(FixCoverObjects.obj_Confirmation_Time);
			String eletext = objTime.getText();
			if (eletext.contains(TimeofContact)) {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is displayed",
						Status.PASS);

			} else {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is not displayed",
						Status.FAIL);

			}

			// Indexation
			WebElement objIndexation = driver.findElement(FixCoverObjects.obj_Confirmation_Indexation);
			String eletextIndexation = objIndexation.getText();
			if (eletextIndexation.contains(Indexation)) {
				report.updateTestLog("Indexation", "Indexation" + Indexation + " is displayed", Status.PASS);

			} else {
				report.updateTestLog("Indexation", "Indexation" + Indexation + " is not displayed", Status.FAIL);

			}



			part = XML.split("<amount>");
			String part2Death = part[1];
			String part2Tpd = part[2];
			String part2IP = part[3];

			String[] valueDeath = part2Death.split("</amount>");
			String[] valueTpd = part2Tpd.split("</amount>");
			String[] valueIP = part2IP.split("</amount>");

			String Death = valueDeath[0];
			String TPD = valueTpd[0];
			String IP = valueIP[0];

			double dD = Double.parseDouble(Death);
			int D = (int) dD;
			double dT = Double.parseDouble(TPD);
			int T = (int) dT;
			double dI = Double.parseDouble(IP);
			int I = (int) dI;

			Death = "$" + D;
			TPD = "$" + T;
			IP = "$" + I;
			System.out.println(Death);
			System.out.println(TPD);
			System.out.println(IP);

			// Death Existing Amount

			WebElement deathEXTAmount = driver.findElement(FixCoverObjects.obj_Confirmation_Death_ExistingCover);
			String PagedeathEXTAmount = deathEXTAmount.getText();
			PagedeathEXTAmount = PagedeathEXTAmount.replaceFirst(",", "");
			//fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");
			System.out.println(PagedeathEXTAmount);
			System.out.println(Death);
if (PagedeathEXTAmount.contains(Death)) {
	report.updateTestLog("Death Existing Cover", "Death Existing Cover" + PagedeathEXTAmount + " is displayed",
			Status.PASS);

}
else {
	report.updateTestLog("Death Existing Cover", "Death Existing Cover" + PagedeathEXTAmount + " is not displayed",
			Status.FAIL);
}
			// Death Amount

			WebElement deathAmount = driver.findElement(FixCoverObjects.obj_Confirmation_Death_TransferCover);
			//fieldValidation("Amount Validation", deathAmount, DeathAmount, "Death New Cover");

			String PagedeathAmount = deathAmount.getText();
			PagedeathAmount = PagedeathAmount.replaceFirst(",", "");
			//fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");
			System.out.println(PagedeathAmount);
			System.out.println(Death);
if (PagedeathAmount.contains(Death)) {
	report.updateTestLog("Death New Cover", "Death New Cover" + PagedeathAmount + " is displayed",
			Status.PASS);

}
else {
	report.updateTestLog("Death New Cover", "Death New Cover" + PagedeathAmount + " is not displayed",
			Status.FAIL);
}



			// amountValidator("Death Amount",deathAmount,DeathAmount );

			/*
			 * // Death Weekly Cost WebElement DeathWeeklycost =
			 * driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[1]//following::p"));
			 * fieldValidation("WebElement",DeathWeeklycost,
			 * DeathWeeklyCost,"Death Weekly Cost");
			 */

			// TPD Amount

			// TPD Existing Amount

			WebElement tpdExtAmount = driver.findElement(FixCoverObjects.obj_Confirmation_TPD_ExistingCover);
		//	fieldValidation("Amount Validation", tpdExtAmount, TPD, "TPD Existing Cover");
			String PagetpdExtAmount = tpdExtAmount.getText();
			PagetpdExtAmount = PagetpdExtAmount.replaceFirst(",", "");
			System.out.println(PagetpdExtAmount);
			System.out.println(TPD);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			if (PagetpdExtAmount.contains(TPD)) {
				report.updateTestLog("TPD Existing Cover", "TPD Existing Cover" + PagetpdExtAmount + " is displayed",
						Status.PASS);

			}
			else {
				report.updateTestLog("TPD Existing Cover", "TPD Existing Cover" + PagetpdExtAmount + " is not displayed",
						Status.FAIL);
			}
			// TPD Amount

			WebElement tpdAmount = driver.findElement(FixCoverObjects.obj_Confirmation_TPD_TransferCover);
			String PagetpdAmount = deathEXTAmount.getText();
			PagetpdAmount = PagetpdAmount.replaceFirst(",", "");
			System.out.println(PagetpdAmount);
			System.out.println(TPD);
			//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
			if (PagetpdAmount.contains(TPD)) {
				report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdAmount + " is displayed",
						Status.PASS);

			}
			else {
				report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdAmount + " is not displayed",
						Status.FAIL);
			}
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// TPD Weekly Cost

			/*
			 * WebElement tpdWeeklycost = driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[2]//following::p"));
			 * fieldValidation("WebElement",tpdWeeklycost,
			 * TPDWeeklyCost,"TPD Weekly Cost");
			 */

/*			// IP Amount

			// IP Existing Amount

			WebElement ipAmount = driver.findElement(FixCoverObjects.obj_Confirmation_IP_ExistingCover);
			fieldValidation("Amount Validation", ipAmount, IP, "IP Existing Cover");


			// IP Amount

			WebElement ipExtAmount = driver.findElement(FixCoverObjects.obj_Confirmation_IP_NewCover);
			fieldValidation("Amount Validation", ipExtAmount, IPAmount, "IP New Cover");
			// amountValidator("Death Amount",deathAmount,DeathAmount );




New sum insured
		*/

			// IP Existing Amount
/*
						WebElement ipExtAmount = driver.findElement(FixCoverObjects.obj_Confirmation_IP_ExistingCover);
					//	fieldValidation("Amount Validation", tpdExtAmount, TPD, "TPD Existing Cover");
						String PageipExtAmount = ipExtAmount.getText();
						PageipExtAmount = PageipExtAmount.replaceFirst(",", "");
						System.out.println(PageipExtAmount);
						System.out.println(IP);
						//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
						if (PageipExtAmount.contains(IP)) {
							report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdExtAmount + " is displayed",
									Status.PASS);

						}
						else {
							report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdExtAmount + " is not displayed",
									Status.FAIL);
						}
						// TPD Amount

						WebElement ipAmount = driver.findElement(FixCoverObjects.obj_Confirmation_TPD_TransferCover);
						String PageipAmount = ipAmount.getText();
						PageipAmount = PageipAmount.replaceFirst(",", "");
						System.out.println(PageipAmount);
						System.out.println(TPD);
						//fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");
						if (PageipAmount.contains(IP)) {
							report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdAmount + " is displayed",
									Status.PASS);

						}
						else {
							report.updateTestLog("TPD New Cover", "TPD New Cover" + PagetpdAmount + " is not displayed",
									Status.FAIL);
						}
			*/
			// Waiting Period

			WebElement ipWaitingPeriod = driver.findElement(FixCoverObjects.obj_Confirmation_IP_WaitingPeriod);
			fieldValidation("WebElement", ipWaitingPeriod, "90 Days", "Waiting Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// Benefit Period

			js.executeScript("window.scrollBy(0,500)", "");

			WebElement ipBenefitPeriod = driver.findElement(FixCoverObjects.obj_Confirmation_IP_BenefitPeriod);
			fieldValidation("WebElement", ipBenefitPeriod, "2 Years", "Benefit Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );





		}

		else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			String OutcomeForDeath = dataTable.getData(DataSheet, "OutcomeForDeath");
			String OutcomeForTPD = dataTable.getData(DataSheet, "OutcomeForTPD");
			String OutcomeForIP = dataTable.getData(DataSheet, "OutcomeForIP");

			WebElement OccupationalCategory_Death = driver
					.findElement(FixCoverObjects.obj_Confirmation_Death_OccupationalCategory);
			js.executeScript("arguments[0].scrollIntoView(true);", OccupationalCategory_Death);
			fieldValidation("WebElement", OccupationalCategory_Death, OutcomeForDeath, "OccupationalCategory - Death");

			WebElement OccupationalCategory_tpd = driver
					.findElement(FixCoverObjects.obj_Confirmation_TPD_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_tpd, OutcomeForTPD, "OccupationalCategory - TPD");

			WebElement OccupationalCategory_ip = driver
					.findElement(FixCoverObjects.obj_Confirmation_IP_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_ip, OutcomeForIP, "OccupationalCategory - IP");

		}

	}

	public void occupationRatingInputValidation() {
		System.out.println("starttttttttt-" + DataSheet);
		String ExistingDeath = dataTable.getData(DataSheet, "ExistingDeath");
		String ExistingTPD = dataTable.getData(DataSheet, "ExistingTPD");
		String ExistingIP = dataTable.getData(DataSheet, "ExistingIP");
		System.out.println("enddddddddddddd");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;

		String[] value;

		// To get firstName
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(ExistingDeath))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + ExistingDeath + ".", Status.FAIL);
		}
		if (!(occRating2.equals(ExistingTPD))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + ExistingTPD + ".", Status.FAIL);
		}
		if (!(occRating3.equals(ExistingIP))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + ExistingIP + ".", Status.FAIL);
		}
		System.out.println("///valend");
	}

	public void PremiumRateInputValidation() {

		String DOB = dataTable.getData(DataSheet, "DOB");
		String Rating = dataTable.getData(DataSheet, "Rating");
		String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
		String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;
		String Amount1;
		String Amount2;
		String DateOfBirth;
		String[] value;

		String AmtDeath;
		String AmtTPD;

		// To get DOB
		part = XML.split("<dateOfBirth>");
		DateOfBirth = part[1];
		value = DateOfBirth.split("</dateOfBirth>");
		String date = value[0];
		if (!(date.equals(DOB))) {
			report.updateTestLog("DOB",
					"DOB in XML does not match with the input DOB. Inupt-" + DOB + "/ Output-" + date + ".",
					Status.FAIL);
		}
		// To get occupation
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + Rating + ".", Status.FAIL);
		}
		if (!(occRating2.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + Rating + ".", Status.FAIL);
		}
		if (!(occRating3.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + Rating + ".", Status.FAIL);
		}


		// To get unit Value
				part = XML.split("<amount>");
				Amount1 = part[1];
				Amount2 = part[2];

				value = Amount1.split("</amount>");
				AmtDeath = value[0];
				value = Amount2.split("</amount>");
				AmtTPD = value[0];

				if (!(AmtDeath.equals(DeathAmount))) {
					report.updateTestLog("Amount", "Amount in XML does not match with the input Death. Inupt-"
							+ AmtDeath + "/ Output-" + DeathAmount + ".", Status.FAIL);
				}
				if (!(AmtTPD.equals(TPDAmount))) {
					report.updateTestLog("Amount", "Amount in XML does not match with the input TPD. Inupt-" + AmtDeath
							+ "/ Output-" + TPDAmount + ".", Status.FAIL);
				}





	}

	public void cover_Overall() {
		String Indexation = dataTable.getData(DataSheet, "Indexation");
		String CostType = dataTable.getData(DataSheet, "CostType");


		Select COSTTYPE = new Select(driver.findElement(FixCoverObjects.obj_PersonalDetails_PremiumFrequency));
		COSTTYPE.selectByVisibleText(CostType);
		sleep(1000);
		report.updateTestLog("Cost of Insurance", "Cost frequency selected is: " + CostType, Status.PASS);

		// ******Select cover Type*******\\

		if (Indexation.equalsIgnoreCase("Yes")) {
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_DeathIndexationYes)).click();
			report.updateTestLog("Death", "Indexation applied at lower of CPI and 7.5% per year - Yes is selected",
					Status.PASS);
			sleep(3000);
			WebElement TPDIndexation = driver.findElement(FixCoverObjects.obj_TPDIndexationYes);
			js.executeScript("arguments[0].scrollIntoView(true);", TPDIndexation);

			fieldValidation("WebButton", TPDIndexation, Indexation, "TPD Indexation");

		} else if (Indexation.equalsIgnoreCase("No")) {
			wait.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_DeathIndexationNo)).click();
			report.updateTestLog("Death", "Indexation applied at lower of CPI and 7.5% per year - No is selected",
					Status.PASS);
			sleep(3000);
			WebElement TPDIndexation = driver.findElement(FixCoverObjects.obj_TPDIndexationNo);
			fieldValidation("WebButton", TPDIndexation, Indexation, "TPD Indexation");

		} else {
			report.updateTestLog("DataTable", "Invalid value for Indexation", Status.FAIL);
		}

		DeathInsured = driver.findElement(FixCoverObjects.obj_PersonalDetails_DeathInsured).getText();
		int lengthDeathinsured = DeathInsured.length();
		System.out.println("len"+lengthDeathinsured);
		String Deathinsuredval =DeathInsured.substring(0,lengthDeathinsured-3);
		System.out.println("--->"+Deathinsuredval);
		DeathInsured=Deathinsuredval;

		TPDInsured = driver.findElement(FixCoverObjects.obj_PersonalDetails_TPDInsured).getText();
		int lengthTPDinsured = TPDInsured.length();
		System.out.println("len"+lengthTPDinsured);
		String TPDinsuredval =TPDInsured.substring(0,lengthTPDinsured-3);
		System.out.println("--->"+TPDinsuredval);
		TPDInsured=TPDinsuredval;
		/*int lengthTPDinsured = TPDInsured.length();
		System.out.println("len"+lengthTPDinsured);
		String TPDinsuredval =TPDInsured.substring(1,lengthTPDinsured);
		System.out.println("DeathAmount==="+TPDinsuredval);
		value = Integer.parseInt(TPDinsuredval);
		TPDInsured = "$"+value;*/




		if (CostType.equalsIgnoreCase("Weekly")) {

			DeathAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_DeathWeeklyCost).getText();
			TPDAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_TPDWeeklyCost).getText();
			TotalAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_TotalWeeklyCost).getText();

		} else if (CostType.equalsIgnoreCase("Monthly")) {
			DeathAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_DeathMonthlyCost).getText();
			TPDAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_TPDMonthlyCost).getText();
			TotalAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_TotalMonthlyCost).getText();

		} else if (CostType.equalsIgnoreCase("Yearly")) {
			DeathAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_DeathYearlyCost).getText();
			TPDAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_TPDYearlyCost).getText();
			TotalAmount = driver.findElement(FixCoverObjects.obj_PersonalDetails_TotalYearlyCost).getText();

		} else {
			report.updateTestLog("DataSheet", "Invalid value for Premium Type", Status.FAIL);
		}

		report.updateTestLog("Death Cost", "Death Cost -"+DeathAmount, Status.PASS);
		report.updateTestLog("TPD Cost", "TPD Cost -"+TPDAmount, Status.PASS);


		int lengthDeath = DeathAmount.length();
		System.out.println("len"+lengthDeath);
		String Deathval =DeathAmount.substring(1,lengthDeath);
		System.out.println("DeathAmount==="+Deathval);

		int lengthTPD = TPDAmount.length();
		System.out.println("len"+lengthTPD);
		String TPDval =TPDAmount.substring(1,lengthTPD);
		System.out.println("DeathAmount==="+TPDval);

		int lengthTotal = TotalAmount.length();
		System.out.println("len"+lengthTotal);
		String Totval =TotalAmount.substring(1,lengthTotal);
		System.out.println("DeathAmount==="+Totval);


/*		String[] Amount = DeathAmount.split("$");
		//String Deathval = Amount[1];
		System.out.println("DeathAmount==="+Deathval);
		 Amount = TPDAmount.split("$");
		String TPDval = Amount[1];
		System.out.println("DeathAmount==="+TPDval);
		 Amount = TotalAmount.split("$");
		String Totval = Amount[1];
		System.out.println("DeathAmount==="+Totval);*/

		Double D = Double.parseDouble(Deathval);
		Double T = Double.parseDouble(TPDval);
		Double Total = Double.parseDouble(Totval);

		Double tempTotal = D+T;
		System.out.println("Templ val="+tempTotal);
		if (!(tempTotal==Total)) {
			report.updateTestLog("Yearly Cost", "Yearly Cost is equal to the sum of Death and TPD -"+TotalAmount, Status.PASS);
		}
		else {
			report.updateTestLog("Yearly Cost", "Yearly Cost is not equal to the sum of Death and TPD", Status.FAIL);
		}

	}


	public void cover_Premium() {
		String Age = dataTable.getData(DataSheet, "Age");
			String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
			String DeathCost = dataTable.getData(DataSheet, "DeathCost");
			String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
			String TPDCost = dataTable.getData(DataSheet, "TPDCost");
			// String DeathTPD = dataTable.getData(DataSheet, "DeathTPD");

			String DeathCostPage;
			String TPDCostPage;
			String DeathTPDCostPage;
			Double Total = 0.00;



			// Select Cost Type
			Select COSTTYPE = new Select(driver.findElement(FixCoverObjects.obj_PersonalDetails_PremiumFrequency));
			// DecimalFormat f = new DecimalFormat("##0.00");
			int AgeValue = Integer.parseInt(Age);

				COSTTYPE.selectByVisibleText("Yearly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Yearly is selected", Status.PASS);

				String DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmt = Double.parseDouble(DeathAmount);
				//String DeathAmt1 =  String.format("%.02f", DeathAmt);
				amountValidation(DeathAmountPage, DeathAmt, "Death Amount");


				WebElement Death = driver.findElement(FixCoverObjects.obj_PersonalDetails_DeathCost);
				DeathCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				double DeathCst = Double.parseDouble(DeathCost);
				//String DeathCost1 = String.format("%.02f", DeathCst);

				amountValidation(DeathCostPage, DeathCst, "Death Cost");
				// Total += PageDeathCost;
				if ((AgeValue >= 14) && (AgeValue <= 64)) {

					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmt = Double.parseDouble(TPDAmount);
					//String TPDAmt1 =  String.format("%.02f", TPDAmt);
					amountValidation(DeathAmountPage, TPDAmt, "TPD Amount");


					WebElement TPD = driver.findElement(FixCoverObjects.obj_PersonalDetails_TPDCost);
					TPDCostPage = wait.until(
							ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCst = Double.parseDouble(TPDCost);
					//String TPDCost1 = String.format("%.02f", TPDCst);

					amountValidation(TPDCostPage, TPDCst, "TPD Cost");
					// Total += PageTPDCost;
				} else {
					report.updateTestLog("TPD", "TPD is not applicable since age is >65 or <14", Status.PASS);
				}

				/* DeathTPDCostPage = wait.until(
				 * ExpectedConditions.elementToBeClickable(FixCoverObjects.
				 * obj_PersonalDetails_TotalCostYearly)) .getText(); double
				 * DeathTPDCst = Double.parseDouble(DeathTPD); DeathTPD =
				 * String.format("%.02f", DeathTPDCst);
				 *
				 * amountValidation(DeathTPDCostPage, DeathTPD, "Total Cost");
				 */

				COSTTYPE.selectByVisibleText("Monthly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Monthly is selected", Status.PASS);
				DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_DeathAmount))
						.getText();
				DeathAmt = Double.parseDouble(DeathAmount);
				//DeathAmt1 =  String.format("%.02f", DeathAmt);
				amountValidation(DeathAmountPage, DeathAmt, "Death Amount");


				DeathCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostmonthly = Double.parseDouble(DeathCost);
				DeathCostmonthly = DeathCostmonthly / 12;
				//String DeathCost2 = String.format("%.02f", DeathCostmonthly);

				amountValidation(DeathCostPage, DeathCostmonthly, "Death Cost");
				if ((AgeValue >= 14) && (AgeValue <= 64)) {

					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmt = Double.parseDouble(TPDAmount);
					//String TPDAmt1 =  String.format("%.02f", TPDAmt);
					amountValidation(DeathAmountPage, TPDAmt, "TPD Amount");


					TPDCostPage = wait.until(
							ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostmonthly = Double.parseDouble(TPDCost);
					System.out.println("///"+TPDCostmonthly);
					TPDCostmonthly = TPDCostmonthly / 12;
					System.out.println("///"+TPDCostmonthly);
					//String TPDCost2 = String.format("%.02f", TPDCostmonthly);
					//System.out.println("///"+TPDCost2);

					amountValidation(TPDCostPage, TPDCostmonthly, "TPD Cost");
				}

				COSTTYPE.selectByVisibleText("Weekly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Weekly is selected", Status.PASS);
				DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_DeathAmount))
						.getText();
				DeathAmt = Double.parseDouble(DeathAmount);
				//DeathAmt1 =  String.format("%.02f", DeathAmt);
				amountValidation(DeathAmountPage, DeathAmt, "Death Amount");

				DeathCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostweekly = Double.parseDouble(DeathCost);
				DeathCostweekly = DeathCostweekly / 52;
				//String DeathCost3 = String.format("%.02f", DeathCostweekly);

				amountValidation(DeathCostPage, DeathCostweekly, "Death Cost");

				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmt = Double.parseDouble(TPDAmount);
					//String TPDAmt1 =  String.format("%.02f", TPDAmt);
					amountValidation(DeathAmountPage, TPDAmt, "TPD Amount");

					TPDCostPage = wait.until(
							ExpectedConditions.elementToBeClickable(FixCoverObjects.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostweekly = Double.parseDouble(TPDCost);
					TPDCostweekly = TPDCostweekly / 52;
					//String TPDCost3 = String.format("%.02f", TPDCostweekly);

					amountValidation(TPDCostPage, TPDCostweekly, "TPD Cost");
				}




	}

	public void amountValidation(String DollarAmount, Double newAmount, String Field) {

		String Amount = String.format("%.02f", newAmount);
		System.out.println(DollarAmount);
		System.out.println(Amount);
		String[] part;
		String amount1;
		String[] value;
		String a1;
		String a2;
		String amount;
		Amount = "$" + Amount;
		if (DollarAmount.contains(",")) {

			// To get firstName
			String[] p = DollarAmount.split(",");
			String p1 = p[0];
			String p2 = p[1];
			DollarAmount = p1 + "" + p2;
		}

		if (DollarAmount.equalsIgnoreCase(Amount)) {
			report.updateTestLog(Field, Field + " :" + Amount + " is present", Status.PASS);
		} else {
			Double decAmount = newAmount-0.01;
			Double incAmount = newAmount+0.01;
			String dAmount = String.format("%.02f", decAmount);
			String iAmount = String.format("%.02f", incAmount);


			dAmount = "$" + dAmount;
			iAmount = "$" + iAmount;

			/*if (DollarAmount.contains(",")) {

				// To get firstName
				String[] p = DollarAmount.split(",");
				String p1 = p[0];
				String p2 = p[1];
				DollarAmount = p1 + "" + p2;
			}*/

			if ( (DollarAmount.equalsIgnoreCase(dAmount))||(DollarAmount.equalsIgnoreCase(iAmount)) ) {
				report.updateTestLog(Field, Field + " :" + Amount + " is present with 0.01 corrected value", Status.PASS);
			} else {

				report.updateTestLog(Field,
						Field + " Expected Value:" + Amount + " / Value Present in page:" + DollarAmount, Status.FAIL);
			}

		}

	}


	public void premiumValidation() {
		String DeathYN = dataTable.getData(DataSheet, "DeathYN");
		String TPDYN = dataTable.getData(DataSheet, "TPDYN");
		String IPYN = dataTable.getData(DataSheet, "IPYN");
		String ExpectedDeathcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedDeathcover");
		String ExpectedTPDcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedTPDcover");
		String ExpectedIPcover = dataTable.getData("VicSuper_EApplyPremium", "ExpectedIPcover");
		ExpectedDeathcover = "$" + ExpectedDeathcover;
		ExpectedTPDcover = "$" + ExpectedTPDcover;
		ExpectedIPcover = "$" + ExpectedIPcover;

		// ExpectedDeathcover = amountFormatter(ExpectedDeathcover);
		System.out.println("Formatted amount :" + ExpectedDeathcover);

		if (DeathYN.equalsIgnoreCase("Yes")) {
			WebElement Death = driver.findElement(FixCoverObjects.obj_PersonalDetails_DeathAmount);
			fieldValidation("Amount Validation", Death, ExpectedDeathcover, "Death Cover Premium");
		}

		if (TPDYN.equalsIgnoreCase("Yes")) {
			WebElement tpd = driver.findElement(FixCoverObjects.obj_PersonalDetails_TPDAmount);
			fieldValidation("Amount Validation", tpd, ExpectedTPDcover, "TPD Cover Premium");
		}

		if (IPYN.equalsIgnoreCase("Yes")) {
			WebElement ip = driver.findElement(FixCoverObjects.obj_PersonalDetails_IPAmount);
			fieldValidation("Amount Validation", ip, ExpectedIPcover, "IP Cover Premium");
		}

	}

	public static double round(double value, int numberOfDigitsAfterDecimalPoint) {
		BigDecimal bigDecimal = new BigDecimal(value);
		bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint, BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
	}


	public void pdfValidator() {
		try {
			 Robot robot = new Robot();
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;

		driver.get("chrome://downloads/");

		sleep(3000);

		robot.keyPress(KeyEvent.VK_TAB);
		 robot.keyRelease(KeyEvent.VK_TAB);
		 robot.keyPress(KeyEvent.VK_TAB);
		 robot.keyRelease(KeyEvent.VK_TAB);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 sleep(3000);

			/*String oldTab = driver.getWindowHandle();
			ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			newTab.remove(oldTab);
			driver.close();
		    sleep(500);
			driver.switchTo().window(newTab.get(0));
			sleep(4500);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			URL url = new URL(driver.getCurrentUrl());
			 System.out.println(url);
		 */

		 String parentTab = driver.getWindowHandle();
		 System.out.println("ParentTab"+parentTab);
		  ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		  System.out.println("ChaildTab"+tabs);
		  driver.switchTo().window(tabs.get(2));
		  sleep(1000);
		  String url = driver.getCurrentUrl();
		  System.out.println(url);
		/*  wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).click();
		  robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_2);
			 robot.keyRelease(KeyEvent.VK_2);
			 robot.keyRelease(KeyEvent.VK_CONTROL);*/
			 sleep(5000);


		/*System.out.println("PDFValidator");
		sleep(3000);*/
		//driver.get("file:///C:/Users/572847/Downloads/417f637982fbaf00afb1756829004b81578d95b2da3832fd8f0bcc681ecaa59b416613bef6cddca27fa764a35fbeb3cc3abea6bc16e08f26e827bff973b7323e.pdf");
		//sleep(3000);

		 robot.keyPress(KeyEvent.VK_SHIFT);
		 robot.keyPress(KeyEvent.VK_V);
		 robot.keyRelease(KeyEvent.VK_V);
		 robot.keyPress(KeyEvent.VK_I);
		 robot.keyRelease(KeyEvent.VK_I);
		 robot.keyPress(KeyEvent.VK_C);
		 robot.keyRelease(KeyEvent.VK_C);
		 robot.keyPress(KeyEvent.VK_T);
		 robot.keyRelease(KeyEvent.VK_T);
		 robot.keyRelease(KeyEvent.VK_SHIFT);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 sleep(5000);
		 sleep(5000);
		// url = driver.getCurrentUrl();

		  URL Fileurl = new URL(driver.getCurrentUrl());

			BufferedInputStream fileToParse = new BufferedInputStream(Fileurl.openStream());

			// parse() -- This will parse the stream and populate the
			// COSDocument object.
			// COSDocument object -- This is the in-memory representation of the
			// PDF document

			PDFParser parser = new PDFParser(fileToParse);
			parser.parse();

			// getPDDocument() -- This will get the PD document that was parsed.
			// When you are done with this document you must call close() on it
			// to release resources
			// PDFTextStripper() -- This class will take a pdf document and
			// strip out all of the text and ignore the formatting and such.

			String output = new PDFTextStripper().getText(parser.getPDDocument());
			System.out.println("<><><>"+output+"<><><>");
			parser.getPDDocument().close();










		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
/*	 List<WebElement> allLink = driver.findElements(By.xpath("//*[@id='file-link']"));
	 System.out.println("Size"+allLink.size());
	 for (WebElement currentElement : allLink) {
		 int i =1;
		System.out.println("//"+i);
		String Value = currentElement.getText();
		System.out.println(i+")"+Value);
		i++;
	}
	*/

		/*try {
			pdDoc= PDDocument.load(new File("C:\\Users\\572847\\Downloads\\pdf.pdf"));
			System.out.println("Total PG:"+pdDoc.getNumberOfPages());
			pdfStripper = new PDFTextStripper();
			System.out.println("Value--------"+pdfStripper.getText(pdDoc));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/



	}


}
