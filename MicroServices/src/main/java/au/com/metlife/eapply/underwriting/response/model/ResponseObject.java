package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;

public class ResponseObject implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	private String questionnaireId;
	private LookUp lookup;
	private Rules rules;
	private ClientData clientData;
	private boolean isClientMatched = false;
	private String clientMatchReason;
	
	public boolean isClientMatched() {
		return isClientMatched;
	}
	public void setClientMatched(boolean isClientMatched) {
		this.isClientMatched = isClientMatched;
	}
	public String getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(String questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public Rules getRules() {
		return rules;
	}
	public void setRules(Rules rules) {
		this.rules = rules;
	}
	public ClientData getClientData() {
		return clientData;
	}
	public void setClientData(ClientData clientData) {
		this.clientData = clientData;
	}
	public LookUp getLookup() {
		return lookup;
	}
	public void setLookup(LookUp lookup) {
		this.lookup = lookup;
	}
	public String getClientMatchReason() {
		return clientMatchReason;
	}
	public void setClientMatchReason(String clientMatchReason) {
		this.clientMatchReason = clientMatchReason;
	}
}