package pages.metlife;

import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCPaymentPageObjects;
import uimap.metlife.YBRSummaryPageObjects;

public class YBRSummaryPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	public String appNumber;
	public YBRSummaryPage quoteProtectPayment() {
		quoteProtectPaymentDetails();
		return new YBRSummaryPage(scriptHelper);
	}

	public YBRSummaryPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void quoteProtectPaymentDetails() {
	
		String title = dataTable.getData("MetLife_Data", "Title");
		String lastName = dataTable.getData("MetLife_Data", "LastName");
		String accountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsb = dataTable.getData("MetLife_Data", "BSB");
		String ccType = dataTable.getData("MetLife_Data", "CcType");
		String expiryMonth = dataTable.getData("MetLife_Data", "ExpiryMonth");
		String expiryYear = dataTable.getData("MetLife_Data", "ExpiryYear");
		String cardType = dataTable.getData("MetLife_Data", "CardType");
		String cYSTest = dataTable.getData("MetLife_Data", "CYSTest");
		String StrTCID = dataTable.getData("MetLife_Data", "TC_ID");
		try {
			
			switchToActiveWindow();
			driver.manage().window().maximize();
							
			sleep(1000);
			tryAction(fluentWaitElement(YBRSummaryPageObjects.linkTitle1), "specialDropdown", "Title1", title);
			sleep(1000);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtLastName1), "SET", "LastName_1", lastName);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtLastName1), "TAB", "LastName_1");
			sleep(1000);
			
			sleep(1000);
			tryAction(fluentWaitElement(YBRSummaryPageObjects.linkTitle2), "specialDropdown", "Title2", title);
			sleep(1000);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtLastName2), "SET", "LastName_2", lastName);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtLastName2), "TAB", "LastName_2");
			sleep(1000);
			
			tryAction(waitForClickableElement(YBRSummaryPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			if (cardType.equalsIgnoreCase("Debit")) {
			tryAction(fluentWaitElement(YBRSummaryPageObjects.rddebit), "clkradio", "Debit");
			sleep(500);			
			
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtaccountname), "SET", "AccountName", accountName);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtbsb), "SET", "BSB", bsb);
			tryAction(waitForClickableElement(YBRSummaryPageObjects.txtbsb), "TAB", "BSB");
			
			}
			else
			{
				
					System.out.println("Entered into Credit Card Section");
				 tryAction(fluentWaitElement(YBRSummaryPageObjects.rdcredit), "clkradio", "Credit");
					Thread.sleep(500);
					
					switch(ccType){
					case "Visa":
						System.out.println("Entered into the Visa Switch");
						tryAction(fluentWaitElement(YBRSummaryPageObjects.rdvisa), "clkradio", "Visa");
					    Thread.sleep(500);
						 break;	 
					case "Master":
						System.out.println("Entered into the Master Switch");
					    tryAction(fluentWaitElement(YBRSummaryPageObjects.rdmaster), "clkradio", "Master");
				        Thread.sleep(500);
						 break;
					case "Amex":
						System.out.println("Entered into the Amex Switch");
				        tryAction(fluentWaitElement(YBRSummaryPageObjects.rdamex), "clkradio", "Amex");
			            Thread.sleep(500);
			
							}
					
				tryAction(waitForClickableElement(YBRSummaryPageObjects.txtccnumber), "SET", "CCNumber", accountNumber);
				tryAction(waitForClickableElement(YBRSummaryPageObjects.txtccnumber), "TAB", "CCNumer");
				sleep(1000);
				tryAction(waitForClickableElement(YBRSummaryPageObjects.txtccname), "SET", "CCName", accountName);
				tryAction(waitForClickableElement(YBRSummaryPageObjects.txtccname), "TAB", "CCNumer");
				sleep(1000);
				tryAction(fluentWaitElement(YBRSummaryPageObjects.selCCExpMonth),"specialDropdown", "Expiry Month", expiryMonth);
				sleep(2000);
				tryAction(fluentWaitElement(YBRSummaryPageObjects.selCcExpYr),"specialDropdown", "Expiry Year", expiryYear);
			}
			

			tryAction(waitForClickableElement(YBRSummaryPageObjects.chkauthorise), "Click", "Acknowledge");
							
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(YBRSummaryPageObjects.btnsubmit)).click().perform();
			sleep(4000);			
			if(!StrTCID.contains("CC")){
		
			driver.manage().window().maximize();
			report.updateTestLog("App Submission", "Application submitted sucessfully", Status.SCREENSHOT);
			//String appNumber1=driver.findElement(By.xpath("//*[@id='collapseTwo']/div/div/div/p/strong[1]")).getText();
			String appNumber1=driver.findElement(YBRSummaryPageObjects.textAppnum).getText();
			String appNumber = appNumber1.substring(0, appNumber1.length() - 1); 
			dataTable.putData("MetLife_Data","AppNumber", appNumber);
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(driver.findElement(YBRSummaryPageObjects.btnclosewindow)).click().perform();
			sleep(1000);
			
			}else{
			
				
			List<WebElement> txtAreaOutput = null;
			txtAreaOutput = fluentWaitListElements(ColesCCPaymentPageObjects.txtAreaOutput);
			String StrOutput=fluentWaitElement(txtAreaOutput.get(1)).getText();
				//XML Parsing and reading
					DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource src = new InputSource();
					src.setCharacterStream(new StringReader(StrOutput));
					Document doc = builder.parse(src);
					String appNumber2= doc.getElementsByTagName("insuredID").item(0).getTextContent();
					//System.out.println(appNumber);
					dataTable.putData("MetLife_Data","AppNumber", appNumber2);
					driver.manage().window().maximize();
						
					
			}			
			
			if (cYSTest.equalsIgnoreCase("Yes")) {	
				driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
				driver.manage().window().maximize();
			}	
			driver.manage().window().maximize();
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			/*if (cYSTest.equalsIgnoreCase("Yes")) {	
				driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
				driver.manage().window().maximize();
			}
			*/
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}

	public WebDriverUtil getDriverUtil() {
		return driverUtil;
	}

	public void setDriverUtil(WebDriverUtil driverUtil) {
		this.driverUtil = driverUtil;
	}

	}
