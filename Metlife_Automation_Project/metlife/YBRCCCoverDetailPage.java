package pages.metlife;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.YBRCCCoverDetailPageObjects;
import uimap.metlife.YBRCoverDetailPageObjects;

public class YBRCCCoverDetailPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRCCCoverDetailPage webresponse() {
		webresponseinput();
		return new YBRCCCoverDetailPage(scriptHelper);
	}

	public YBRCCCoverDetailPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void webresponseinput() {
	
		String value = dataTable.getData("MetLife_Data", "InputXML");
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			sleep(2000);
			WebElement elmName = driver.findElement(YBRCCCoverDetailPageObjects.txtareainput);
			elmName.clear();
			elmName.sendKeys(value);
			//tryAction(fluentWaitElement(YBRCCCoverDetailPageObjects.txtareainput), "SET", "XML Input",value);
			sleep(2000);
			tryAction(fluentWaitElement(YBRCCCoverDetailPageObjects.btnNext), "Click", "Apply");
			sleep(4000);
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
