package au.com.metlife.controller;

public class Document {
	private java.lang.String policyNum;	
	private java.lang.String documentlocation;
	private java.lang.String documentCode;
	private java.lang.String documentStatus;
	private java.lang.String fileId;
	private java.lang.String fileName;
	private java.lang.String fileSize;
	private java.lang.String uuId;
	
	public java.lang.String getFileId() {
		return fileId;
	}
	public void setFileId(java.lang.String fileId) {
		this.fileId = fileId;
	}
	public java.lang.String getFileName() {
		return fileName;
	}
	public void setFileName(java.lang.String fileName) {
		this.fileName = fileName;
	}
	public java.lang.String getFileSize() {
		return fileSize;
	}
	public void setFileSize(java.lang.String fileSize) {
		this.fileSize = fileSize;
	}
	public java.lang.String getUuId() {
		return uuId;
	}
	public void setUuId(java.lang.String uuId) {
		this.uuId = uuId;
	}	
	public java.lang.String getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(java.lang.String documentStatus) {
		this.documentStatus = documentStatus;
	}
	public java.lang.String getDocumentlocation() {
		return documentlocation;
	}
	public void setDocumentlocation(java.lang.String documentlocation) {
		this.documentlocation = documentlocation;
	}
	public java.lang.String getDocumentCode() {
		return documentCode;
	}
	public void setDocumentCode(java.lang.String documentCode) {
		this.documentCode = documentCode;
	}
	public java.lang.String getPolicyNum() {
		return policyNum;
	}
	public void setPolicyNum(java.lang.String policyNum) {
		this.policyNum = policyNum;
	}
}
