package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPHealthDetailsPageObjects extends PageObjectLibrary{
		
	
	public static final By chklast3yrsNone = getObjectIdentifaction("YBRIP.health.3yrs.none.chkbox.css");	
	public static final By chkmedicaladviceNone = getObjectIdentifaction("YBRIP.health.diagnosed.none.chkbox.css");
	
	//Exclusion
	public static final By chklastdiagnosedBone = getObjectIdentifaction("YBRIP.health.abturhealth.medicaladvice.bone.checkbox.css");	
	public static final By chklastdiagnosedStrain = getObjectIdentifaction("YBRIP.health.abturhealth.medicaladvice.strain.checkbox.css");
	public static final By chklastdiagnosedAnkle = getObjectIdentifaction("YBRIP.health.abturhealth.medicaladvice.strain.ankles.checkbox.css");
	public static final By drplastdiagnosedImpact = getObjectIdentifaction("YBRIP.health.abturhealth.medicaladvice.strain.ankles.impact.drpdown.css");
	public static final By btnlastdiagnosedSelect = getObjectIdentifaction("YBRIP.health.abturhealth.medicaladvice.strain.ankles.impact.btn.css");
	public static final By rdlastdiagnosedCurrent = getObjectIdentifaction("YBRIP.health.abturhealth.medicaladvice.strain.ankles.impact.radio.css");
	
	// Loadings
	public static final By chklast3yrsLung = getObjectIdentifaction("YBRIP.health.3yrs.lung.chkbox.css");	
	public static final By chklast3yrsAsthma = getObjectIdentifaction("YBRIP.health.3yrs.lung.asthma.chkbox.css");
	public static final By rdlast3yrsAttention = getObjectIdentifaction("YBRIP.health.3yrs.lung.asthma.attention.yes.radio.css");
	public static final By rdlast3yrsCondition = getObjectIdentifaction("YBRIP.health.3yrs.lung.asthma.condition.moderate.radio.css");
	
	
	public static final By chkfamilyhistoryNone = getObjectIdentifaction("YBRIP.health.familyhistory.none.chkbox.css");
	public static final By chklifestyleNo = getObjectIdentifaction("YBRIP.health.lifestyle.no.radio.css");
	
	public static final By txtalchohol = getObjectIdentifaction("YBRIP.health.alchohol.txt.id");
	public static final By btnEnter= getObjectIdentifaction("YBRIP.health.enter.btn.css");
	
	public static final By rdalchoholreduceNo = getObjectIdentifaction("YBRIP.health.alchohol.reduce.no.radio.css");
	public static final By rdhivriskNo = getObjectIdentifaction("YBRIP.health.hiv.risk.no.radio.css");
	
	public static final By rdgeneralNo = getObjectIdentifaction("YBRIP.health.general.no.radio.css");
	public static final By rdinsuranceexclusionNo = getObjectIdentifaction("YBRIP.health.insurancehistory.no.radio.css");
	public static final By btnNext = getObjectIdentifaction("YBRIP.health.next.btn.css");

}
