package pages.metlife;

import java.util.Set;

import org.openqa.selenium.By;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesConfirmationPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesConfirmationPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesConfirmationPage verifyColesPolicyDetail() {
		verifyColesPolicyDetails();
		return new ColesConfirmationPage(scriptHelper);
	}

	public ColesConfirmationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void verifyColesPolicyDetails() {
		try {
			String cYSTest = dataTable.getData("MetLife_Data", "CYSTest");
			//String expectedLoading = dataTable.getData("MetLife_Data", "LoadingValue");
			//assertEqualText(getStringValueFromSessionThreadData("dyn_loadingPercentage"),expectedLoading);
			//driver.findElement(By.id("cvrClosepopupID")).click();
			switchToActiveWindow();
			driver.manage().window().maximize();
			if((waitForClickableElement(ColesConfirmationPageObjects.lnkNo)).isDisplayed()){
			tryAction(waitForClickableElement(ColesConfirmationPageObjects.lnkNo),"Click", "Survey NO");
			}
			if(driver.getPageSource().contains("application number")){
			String appNumber1=driver.findElement(ColesConfirmationPageObjects.txtAppnum).getText();
			String appNumber = appNumber1.substring(0, appNumber1.length() - 1); 
			dataTable.putData("MetLife_Data","AppNumber", appNumber);
			System.out.println(appNumber);
			}
			//tryAction(waitForClickableElement(ColesConfirmationPageObjects.lnkFinish),"Click", "Finish");
			sleep();
			switchToActiveWindow();
			driver.manage().window().maximize();
			//tryAction(waitForClickableElement(ColesConfirmationPageObjects.lnkHere),"Click", "Here");
						    
			if (cYSTest.equalsIgnoreCase("Yes")) {	
				driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
				driver.manage().window().maximize();
			}
			
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
