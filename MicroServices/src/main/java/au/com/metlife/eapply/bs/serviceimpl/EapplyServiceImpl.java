package au.com.metlife.eapply.bs.serviceimpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.transaction.Transactional;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lowagie.text.DocumentException;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.exception.BusinessExceptionConstants;
import au.com.metlife.eapply.bs.exception.MetLifeBSRuntimeException;
import au.com.metlife.eapply.bs.helper.EapplyServiceHelper;
import au.com.metlife.eapply.bs.model.AcceptedAppStatus;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.ApplicantRepository;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.DocumentEappRepository;
import au.com.metlife.eapply.bs.model.DocumentInfo;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplicationRepository;
import au.com.metlife.eapply.bs.model.EapplyOutput;
import au.com.metlife.eapply.bs.model.EmailDetail;
import au.com.metlife.eapply.bs.model.HistoryRepository;
import au.com.metlife.eapply.bs.pdf.GenerateEapplyPDF;
import au.com.metlife.eapply.bs.pdf.PDFbusinessObjectMapperNew;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.bs.utility.BizflowXMLHelper;
import au.com.metlife.eapply.bs.utility.BizflowXMLHelperNew;
import au.com.metlife.eapply.bs.utility.EapplyHelper;
import au.com.metlife.eapply.bs.utility.EmailHelper;
import au.com.metlife.eapply.constants.EtoolkitConstants;
import au.com.metlife.eapply.quote.utility.QuoteHelper;
import au.com.metlife.eapply.quote.utility.SystemProperty;

@Service
public class EapplyServiceImpl implements EapplyService {
	private static final Logger log = LoggerFactory.getLogger(EapplyServiceImpl.class);

	private final EapplicationRepository repository;
	
	private final ApplicantRepository applicantRepository;
	private final HistoryRepository historyRepository;
	private final DocumentEappRepository documentRepository;
	private static final String DEATH = "DEATH";

	@Autowired
    public EapplyServiceImpl(final EapplicationRepository repository, final ApplicantRepository applicantRepository, final HistoryRepository historyRepository,final DocumentEappRepository documentRepository) {
        this.repository = repository;
        this.applicantRepository = applicantRepository;
        this.historyRepository= historyRepository;
        this.documentRepository= documentRepository;
    }
	 
	
	public EapplicationRepository getRepository() {
		return repository;
	}
	public ApplicantRepository getApplicantRepository() {
		return applicantRepository;
	}

	
	
	
	@Override
	@Transactional
	public Eapplication submitEapplication(Eapplication eapplication){
		log.info("Submit eapplication start");
		if(eapplication!=null){
			if(eapplication.getApplicationumber()!=null){
				List<Eapplication> eapplicationList=repository.findByApplicationumber(eapplication.getApplicationumber());
				if(eapplicationList!=null && eapplicationList.size()>1){
					throw new MetLifeBSRuntimeException(BusinessExceptionConstants.ERR_CDE_5100_DUPLICATE_APPNUM, BusinessExceptionConstants.ERR_CDE_5100_MSG);
				}else if(eapplicationList!=null && eapplicationList.size()==1){
					/*Save and retrieve flow*/
					Eapplication exEapplication=eapplicationList.get(0);
					eapplication.setId(exEapplication.getId());
					/*exEapplication.getAuradetail()*/
					
					if(eapplication.getApplicant()!=null && eapplication.getApplicant().size()==1){
						Applicant exApplicant=exEapplication.getApplicant().get(0);
						eapplication.getApplicant().get(0).setId(exApplicant.getId());
						eapplication.getApplicant().get(0).setEapplicationid(exEapplication.getId());
						
						if(exApplicant.getContactinfo()!=null
								&& (eapplication.getApplicant().get(0).getContactinfo()!=null)){
								eapplication.getApplicant().get(0).getContactinfo().setId(exApplicant.getContactinfo().getId());
						}
						/*if(exApplicant.getCovers()!=null && exApplicant.getCovers().size()>0){*/
						if(exApplicant.getCovers()!=null && !(exApplicant.getCovers().isEmpty())){
							for(Cover exCovers: exApplicant.getCovers()){
								if(exCovers!=null && exCovers.getCovercode().equalsIgnoreCase(DEATH)){
									/*if(eapplication.getApplicant().get(0).getCovers()!=null && eapplication.getApplicant().get(0).getCovers().size()>0){*/
									if(eapplication.getApplicant().get(0).getCovers()!=null && !(eapplication.getApplicant().get(0).getCovers().isEmpty())){
										for(Cover newCovers: eapplication.getApplicant().get(0).getCovers()){
											if(newCovers!=null && newCovers.getCovercode().equalsIgnoreCase(DEATH)){
												newCovers.setApplicantid(exApplicant.getId());
												newCovers.setId(exCovers.getId());
												break;
											}
										}
									}
								}else if(exCovers!=null && exCovers.getCovercode().equalsIgnoreCase("TPD")){
									/*if(eapplication.getApplicant().get(0).getCovers()!=null && eapplication.getApplicant().get(0).getCovers().size()>0){*/
									if(eapplication.getApplicant().get(0).getCovers()!=null && !(eapplication.getApplicant().get(0).getCovers().isEmpty())){
										for(Cover newCovers: eapplication.getApplicant().get(0).getCovers()){
											if(newCovers!=null && newCovers.getCovercode().equalsIgnoreCase("TPD")){
												newCovers.setApplicantid(exApplicant.getId());
												newCovers.setId(exCovers.getId());
												break;
											}
										}
									}
								}else if(exCovers!=null && exCovers.getCovercode().equalsIgnoreCase("IP")){
									/*if(eapplication.getApplicant().get(0).getCovers()!=null && eapplication.getApplicant().get(0).getCovers().size()>0){*/
									if(eapplication.getApplicant().get(0).getCovers()!=null && !(eapplication.getApplicant().get(0).getCovers().isEmpty())){
										for(Cover newCovers: eapplication.getApplicant().get(0).getCovers()){
											if(newCovers!=null && newCovers.getCovercode().equalsIgnoreCase("IP")){
												newCovers.setApplicantid(exApplicant.getId());
												newCovers.setId(exCovers.getId());
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			try {
				repository.save(eapplication);
			} catch (Exception e) {
				log.error("Error in submit eapplication: {}",e);
				throw new MetLifeBSRuntimeException(BusinessExceptionConstants.ERR_CDE_5500_DB_DATA_ERROR, BusinessExceptionConstants.ERR_CDE_5500_MSG);
			}
		}
		log.info("Submit eapplication finish");
		return eapplication;
	}
	@Override
	@Transactional
	public Boolean deleteDocument(long eapplicationid) {
		Boolean Status;	
	
		documentRepository.deleteAllByEapplicationID(eapplicationid);		
		Status= Boolean.TRUE;
		return Status;
	}
	
	@Override
	@Transactional
	@Modifying
	public Boolean clientMatch(String firstName, String lastName, String dob) {
		log.info("Client match start");
		Boolean clientMatch = Boolean.FALSE;
		Applicant applicant = new Applicant();
		applicant.setFirstname(firstName);
		/*applicant.setLastname(lastName);*/		
		applicant.setBirthdate(new Timestamp(new Date(dob).getTime()));
		
		List<Applicant> applicantList =  applicantRepository.findByFNamesAndDob(firstName, new Timestamp(new Date(dob).getTime()));
		/*if(applicantList!=null && applicantList.size()>0){*/
		if(applicantList!=null && !(applicantList.isEmpty())){
			clientMatch = Boolean.TRUE;
		}else{
			applicantList =  applicantRepository.findByLNamesAndDob(lastName, new Timestamp(new Date(dob).getTime()));
			/*if(applicantList!=null && applicantList.size()>0){*/
			if(applicantList!=null && !(applicantList.isEmpty())){
				clientMatch = Boolean.TRUE;
			}
		}
		/*Example<Applicant> example = Example.of(applicant);
		List<Applicant> applicantList = applicantRepository.findAll(example);
		if(applicantList!=null && applicantList.size()>0){
			clientMatch = Boolean.TRUE;
		}else{
			applicant = new Applicant();
			applicant.setLastname(lastName);
			applicant.setBirthdate(new Timestamp(new Date(dob).getTime()));
			example = Example.of(applicant);
			applicantList = applicantRepository.findAll(example);
			if(applicantList!=null && applicantList.size()>0){
				clientMatch = Boolean.TRUE;
			}
		}*/
		log.info("Client match finish");
		return clientMatch;
	}


	@Override
	public Eapplication retrieveApplication(String applicationNumber) {
		log.info("Retrive application start");
		List<Eapplication> eapplicationList=null;
		try {
			if(applicationNumber!=null){
				eapplicationList = repository.findByApplicationumber(applicationNumber);
			}
		} catch (Exception e) {
			log.error("Error in retrive application: {}",e);
		}
		/*if(eapplicationList!=null && eapplicationList.size()>0){*/
		if(eapplicationList!=null && !(eapplicationList.isEmpty())){
			log.info("Retrive application finish");
			return eapplicationList.get(0);
		}else{
			log.info("Retrive application finish");
			return null;
		}
	}


	@Override
	public List<Eapplication> retrieveSavedApplications(String fundCode, String clientRefNo,String manageType) {
		log.info("Retrive saved application start");
		List<Eapplication> eapplicationList=null;
		try {
			if(manageType!=null && clientRefNo!=null && fundCode!=null){
				List<Applicant> applicantList =  applicantRepository.findByClientRefNo(clientRefNo);
				eapplicationList=new ArrayList<>();
				for(Applicant applicant:applicantList){
					Eapplication eapplicationTmp=repository.findByIdAndApplicationstatusAndPartnercodeAndRequesttype(Long.valueOf(applicant.getEapplicationid()), "Pending", fundCode,manageType);
					/*Ignore if applicaton is expired , no need to check while clicking on landing page tile*/
					if(!EapplyHelper.determineSavedAppliactionValidatity(eapplicationTmp, fundCode)){
						eapplicationList.add(eapplicationTmp);
					}
				}
			}else if(clientRefNo!=null && fundCode!=null){
				List<Applicant> applicantList =  applicantRepository.findByClientRefNo(clientRefNo);
				eapplicationList=new ArrayList<>();
				for(Applicant applicant:applicantList){
					Eapplication eapplicationTmp=repository.findByIdAndPartnercode(Long.valueOf(applicant.getEapplicationid()),fundCode);
					if(EapplyHelper.determineSavedAppliactionValidatity(eapplicationTmp, fundCode)){
						if(eapplicationTmp.getApplicationstatus()!=null && eapplicationTmp.getApplicationstatus().equalsIgnoreCase("Pending")){
							eapplicationTmp.setApplicationstatus("Expired");
						}
						eapplicationList.add(eapplicationTmp);
					}else{
						eapplicationList.add(eapplicationTmp);
					}
				}
			}
		} catch (Exception e) {
			log.error("Error in retrive saved application: {}",e);
		}
		/*if(eapplicationList!=null && eapplicationList.size()>0){*/
		if(eapplicationList!=null && !(eapplicationList.isEmpty())){
			log.info("Retrive saved application finish");
			return eapplicationList;
		}else{
			log.info("Retrive saved application finish");
			return null;
		}
	}


	@Override
	@Transactional
	public Eapplication expireApplication(String applicationNumber) {
		log.info("Expire application start");
		List<Eapplication> eapplicationList=null;
		Eapplication eapplication=null;
		try {
			if(applicationNumber!=null){
				eapplicationList = repository.findByApplicationumber(applicationNumber);
				/*if(eapplicationList!=null && eapplicationList.size()>0){*/
				if(eapplicationList!=null && !(eapplicationList.isEmpty())){
					eapplication= eapplicationList.get(0);
					eapplication.setApplicationstatus("Expired");
					repository.save(eapplication);
				}
			}
		} catch (Exception e) {
			log.error("Error in expire application: {}",e);
		}
		log.info("Expire application finish");
		return eapplication;
	}


	@Override
	public Boolean checkAppSubmittedInLast24Hrs(String fundCode, String clientRefNo, String manageType) {
		log.info("Check app submit in last 24 hours start");
		 Boolean appsSubmittedinLast24Hrs=Boolean.FALSE;
		try {
			if(manageType!=null && clientRefNo!=null && fundCode!=null){
				List<Applicant> applicantList =  applicantRepository.findByClientRefNo(clientRefNo);
				for(Applicant applicant:applicantList){
					Calendar cal = Calendar.getInstance();
					log.info("Inside in submit in 24 hours {}",cal.getTime()); 
					cal.set(Calendar.DATE, cal.get(Calendar.DATE)-1);
					Eapplication eapplicationTmp=repository.findByIdAndStatusAndLastUpdateDate(Long.valueOf(applicant.getEapplicationid()), "Submitted",cal.getTime());
					if(eapplicationTmp!=null){
						appsSubmittedinLast24Hrs=Boolean.TRUE;
					}
					
				}
			}
		}catch(Exception e){
			log.error("Error in check app submit in last 24 hours: {}",e);
		}
		log.info("Check app submit in last 24 hours finish");
		return appsSubmittedinLast24Hrs;
	}


	@Override
	@Transactional
	public EappHistory createHistory(EappHistory eappHistory) {
		try{
		historyRepository.save(eappHistory);
		}catch(Exception e){
			log.error("Error in create history: {}",e);
			throw new MetLifeBSRuntimeException(BusinessExceptionConstants.ERR_CDE_5500_DB_DATA_ERROR, BusinessExceptionConstants.ERR_CDE_5500_MSG);
		
		}
		return eappHistory;
	}

	@Override
	public Boolean checkDupUWApplication(String fundCode, String clientRefNo, String manageType,String dob, String firstName, String lastName) {
		log.info("check duplicate UW application");
		Boolean dupUWApplication = Boolean.FALSE;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try
		{
			Date date = formatter.parse(dob);
			
			if(fundCode!=null && clientRefNo!=null && manageType!=null && firstName!=null && lastName!=null )
			{
				List<Applicant> applicantList = applicantRepository.findByFLNClntRefDob(firstName, lastName,clientRefNo, new Timestamp(date.getTime()));
				for(Applicant applicant:applicantList)
					{
						Calendar cal = Calendar.getInstance();
						log.info("Inside in fetch in 30 days {}",cal.getTime());
						//Added by 561132 for AEIS Application check in past 24 Hours
						if("AEIS".equalsIgnoreCase(fundCode) || "INGD".equalsIgnoreCase(fundCode)) {
							cal.set(Calendar.HOUR, cal.get(Calendar.HOUR)-24);
							log.info("Inside in fetch in 24 hours {}",cal.getTime());
						}
						else {
							cal.set(Calendar.DATE, cal.get(Calendar.DATE)-30);
						}
					
						Eapplication eapplicationTmp;
						//Commented this code because of this scenario never executes - Purna
						if(manageType.equalsIgnoreCase(fundCode))
						{
							eapplicationTmp=repository.findByIdAndPartnerCodeAndApplicationStatus(new Long(applicant.getEapplicationid()),fundCode,"Submitted");
						}
						else
						{
							eapplicationTmp=repository.findByIdAndAppDecisionAndPartnerCodeAndRequestTypeAndLastUpdateDate(Long.valueOf(applicant.getEapplicationid()), "RUW",fundCode,manageType,cal.getTime());
						}
							if(eapplicationTmp!=null)
							{
								dupUWApplication=Boolean.TRUE;
							}
					}
			}
		}
		catch(Exception e)
		{
			log.error("Error in retriving duplicate application: {}",e);
		}
		
		return dupUWApplication;
	}


	@Override
	public AcceptedAppStatus checkAccApplicationStatus(String fundCode, String clientRefNo, String manageType,
			String dob, String firstName, String lastName) {
		
		log.info("check accepted application");
		AcceptedAppStatus accAppStatus = new AcceptedAppStatus();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try
		{
			Date date = formatter.parse(dob);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR, cal.get(Calendar.HOUR)-24);
			if(fundCode!=null && clientRefNo!=null && manageType!=null && firstName!=null && lastName!=null )
			{
				List<Applicant> applicantList = applicantRepository.findByFLNClntRefDob(firstName, lastName,clientRefNo, new Timestamp(date.getTime()));
				accAppStatus.setDeathOnlyStatus(false);
				accAppStatus.setTpdStatus(false);
				accAppStatus.setIpStatus(false);
				for(Applicant applicant:applicantList)
				{
					Eapplication eapplicationTmp;
					eapplicationTmp=repository.findByIdAndApplicationStatusAndPartnerCodeAndRequestTypeAndLastUpdateDate(Long.valueOf(applicant.getEapplicationid()), "Submitted",fundCode,manageType,cal.getTime());
					
					if(eapplicationTmp!=null)
					{
						 
						 List<Cover>coverList = applicant.getCovers();
						
						for(Cover covers : coverList)
						{
							if(covers.getCovercode().equalsIgnoreCase(DEATH) && covers.getCoverdecision().equalsIgnoreCase("ACC") && (covers.getAdditionalcoveramount()).compareTo(BigDecimal.ZERO) > 0)
							{
								accAppStatus.setDeathOnlyStatus(true);
							}
							else if(covers.getCovercode().equalsIgnoreCase("TPD") && covers.getCoverdecision().equalsIgnoreCase("ACC") && (covers.getAdditionalcoveramount()).compareTo(BigDecimal.ZERO) > 0)
							{
								accAppStatus.setTpdStatus(true);
							}
							else if(covers.getCovercode().equalsIgnoreCase("IP") && covers.getCoverdecision().equalsIgnoreCase("ACC") && (covers.getAdditionalcoveramount()).compareTo(BigDecimal.ZERO) > 0)
							{
								accAppStatus.setIpStatus(true);
							}
						}
					}
					
				}
				
			}
			
		}
		catch(Exception e)
		{
			log.error("Error in retriving accepted application cover status: {}",e);
		}
		
		
		
		return accAppStatus;
	}
	
	@Override
	//MTAA Change
	public EapplyOutput submitEapplyNew(String authId, EappInput eappInput, UriComponentsBuilder ucBuilder) {
		
		 log.info("Submit eapply satrt");
		 Boolean submitStatus = Boolean.FALSE;
		 Eapplication eapplication = null;
		 EappHistory eappHistory = null;
		 String bizFlowXML = null;
		 PDFbusinessObjectMapperNew pdFbusinessObjectMapper = new PDFbusinessObjectMapperNew();
		 Properties property = new Properties();
		 InputStream url = null;
		 List<Document> documentList = null;
		 EapplyOutput eapplyOutput=null;
		 PDFObject pdfObject =null;
		 String clientRefNumber = null;
		 Eapplication prevRecord = null;
		 SystemProperty sys;
		 boolean isSpecialTerm = false;
			
		 try {
			 
			 //1.Convert the eappInput obj from UI into Eapplication Entity object
			 if(null!=eappInput && null!=eappInput.getRequestType() && eappInput.getRequestType().equalsIgnoreCase(MetlifeInstitutionalConstants.NMCOVER)){
				 eapplication = EapplyServiceHelper.convertToEapplication(eappInput, "pending");	
			 }else{
				 eapplication = EapplyServiceHelper.convertToEapplication(eappInput, "Submitted");
				EapplyHelper.setdefaultDecisionNew(eapplication, eappInput);
				 //Work rating maintained
				 if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplication.getRequesttype()) && eappInput.getAppDecision()==null){
					 eappInput.setAppDecision("ACC");
					 eapplication.setAppdecision("ACC");
				 }
				 if(eappInput.getAppDecision()==null){
					 eappInput.setAppDecision(eapplication.getAppdecision());
				 }
			 }
		
			
			 //2.Create pdf object based on the fund name
			 sys = SystemProperty.getInstance();
			 File file = new File( sys.getProperty(eapplication.getPartnercode()));
			 url = new FileInputStream( file);    
			 property.load( url);
			 log.info("getPropertyFile>> {}",sys.getProperty(eapplication.getPartnercode()));
			 //Need to migrate the pdf building as per eappInput
		     pdfObject = pdFbusinessObjectMapper.getPdfOjectsForEapply(eapplication, eappInput, MetlifeInstitutionalConstants.INSTITUTIONAL, sys.getProperty(eapplication.getPartnercode()));
			 pdfObject = EapplyServiceHelper.callpdfGeneration(pdfObject, sys.getProperty(eapplication.getPartnercode()), eappInput, eapplication, sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH));
			 
			 if(eapplication.getApplicant()!=null && !eapplication.getApplicant().isEmpty()){
 				 Applicant applicant=eapplication.getApplicant().get(0);
 				 applicant.setUwpdfloc(pdfObject.getUwPDFLoc());
 				 applicant.setClientpdfloc(pdfObject.getClientPDFLoc());
			 }
		     		
			 //3.update document object into Eapplication entity obj
			 
			 Document document;			 
			 if(eapplication.getDocuments()!=null && !eapplication.getDocuments().isEmpty()){
				 document = new Document();
				 document.setDocLoc(pdfObject.getClientPDFLoc());
				 document.setDocType(MetlifeInstitutionalConstants.CLIENT);
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.getDocuments().add(document);	
				 document = new Document();
				 document.setDocLoc(pdfObject.getUwPDFLoc());
				 document.setDocType("uw");
				 
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.getDocuments().add(document);	
			 }else{
				 documentList = new ArrayList<>();
				 document = new Document();
				 document.setDocLoc(pdfObject.getClientPDFLoc());
				 document.setDocType(MetlifeInstitutionalConstants.CLIENT);
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 documentList.add(document);	
				 document = new Document();
				 document.setDocLoc(pdfObject.getUwPDFLoc());
				 document.setDocType("uw");
				 document.setLastupdatedate(new Timestamp(new Date().getTime()));
				 documentList.add(document);	
				
				 eapplication.setDocuments(documentList);
			 }	
		
			 
			 
			 //4.send Email 
			 if("ACC".equalsIgnoreCase( eapplication.getAppdecision())
             		&& ("true".equalsIgnoreCase(eapplication.getPaindicator() )|| pdfObject.isExclusionInd())){
             	isSpecialTerm = Boolean.TRUE;                        	
             }
			 if(!"INGD".equalsIgnoreCase(eappInput.getPartnerCode())){
			sendMemberAdminEmail(eapplication,eappInput,pdfObject,authId,isSpecialTerm,sys.getProperty(eapplication.getPartnercode()));
			 }
					
			 //5.Send Bizflow xml
			bizFlowXML = BizflowXMLHelperNew.createXMLForBizFlow(eapplication, eappInput,property,null);
			 log.info("bizFlowXML in submiteapply New method ----------->> {}",bizFlowXML);
			 BizflowXMLHelper.sendMetFlowRequestToMQ(bizFlowXML,property);	 			
					 
					
			//6.Delete the previous record if any	
			 prevRecord = retrieveApplication(""+eappInput.getApplicatioNumber());
			 if(prevRecord!=null){
				 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
				 eapplication.setCreatedate(prevRecord.getCreatedate());
			 }else{
				 eapplication.setCreatedate(new Timestamp(new Date().getTime()));
				 eapplication.setLastupdatedate(new Timestamp(new Date().getTime()));
			 }
			 if(prevRecord!=null && prevRecord.getDocuments()!=null && !(prevRecord.getDocuments().isEmpty())){
				 deleteDocument(prevRecord.getId());
			 }
			 		
			 //7. Submit the eapplication and save		
			 submitEapplication(eapplication);
			 
			 //8.create record in history*/
			 eappHistory = new EappHistory();
			 eappHistory.setAppnumber(eapplication.getApplicationumber());
			 eappHistory.setAppdecision(eapplication.getAppdecision());
			 if(!eapplication.getApplicant().isEmpty() && eapplication.getApplicant().get(0)!=null&& eapplication.getApplicant().get(0).getClientrefnumber()!=null) {
				 clientRefNumber = eapplication.getApplicant().get(0).getClientrefnumber();
			 }
			 eappHistory.setClientrefnumber(clientRefNumber);
			 eappHistory.setCreatedate(new Timestamp(new Date().getTime()));
			 eappHistory.setFundid(eappInput.getPartnerCode());
			 eappHistory.setMetadata(bizFlowXML);
			 eappHistory.setPicscaseid("BIZFLOW");
			 createHistory(eappHistory);
			 submitStatus=Boolean.TRUE;
			 eapplyOutput=new EapplyOutput();
			 eapplyOutput.setAppStatus(submitStatus);
			 
			 if(pdfObject!=null){
				 log.info("pdfObject.getClientPDFLoc()>> {}",pdfObject.getClientPDFLoc());
				 eapplyOutput.setClientPDFLocation(EapplyHelper.formEncryptedFilePath(pdfObject.getClientPDFLoc()));
			 }
				
			 //9.Setting the nps token url in eapplyOutput
			 String npsTokenUrl =property.getProperty("nps_tokenURL")+"&fundCode="+eapplication.getPartnercode()+"&custRefNumber="+clientRefNumber+"&TransactionCode="+eapplication.getApplicationumber();
			 log.info("npsTokenUrl>> {}",npsTokenUrl);
			 if(npsTokenUrl != "" && npsTokenUrl!= null){
				 try{
					 eapplyOutput.setNpsTokenURL(QuoteHelper.getNPSToken(npsTokenUrl));
					 log.info("npsToken inside loop>> {}",eapplyOutput.getNpsTokenURL());
				 }catch(Exception e){
					 log.error("Error in NPS token: {}",e);
				 }
			}
				 
		 	} catch (IOException e) {
		 		log.error("Error in input output: {}",e);
		 		 
			} catch (MetLifeBSRuntimeException e) {
				log.error("Error in metlifebs: {}",e);
				 
			}catch (Exception e) {
				log.error("Error in submit eapply::::::: {}",e);
				 
			}  
		
		
		return eapplyOutput;
	}
	
	
	
	
	//MTAA Changes Starts
	public Boolean sendMemberAdminEmail(Eapplication eapplication, EappInput eappInput, PDFObject pdfObject, String authId, Boolean isSpecialTerm, String propFileLoc) {
		log.info("Inside sendMemberAdminEmail Method:");
		Boolean mailSent = false;
		java.util.Properties property = new java.util.Properties();
		File file = null;
		InputStream url = null;
		file = new File( propFileLoc);
		String memberEmail = null;
		String adminEmail = null;
		EmailDetail emaildtl = null;
		Boolean mixedDecision = false;
        try {
			url = new FileInputStream( file);
			property.load( url);
	        String propEntry = null;
	        ObjectMapper objmp = new ObjectMapper();
	        EmailHelper emailHelper = new EmailHelper();
	        
	        if(eappInput.getRequestType()!= null ) {
	        	propEntry = property.getProperty(eappInput.getRequestType());
	        	
	        	emaildtl = objmp.readValue(propEntry,EmailDetail.class);
	        	if(isSpecialTerm) {
		        	memberEmail = emaildtl.getAccspl().getMemberEmail();  
		        	adminEmail = emaildtl.getAccspl().getFundEmail();
		        }else if(eapplication.getAppdecision()!= null && ("ACC").equals(eapplication.getAppdecision())) {
		        	memberEmail = emaildtl.getAcc().getMemberEmail();  
		        	adminEmail = emaildtl.getAcc().getFundEmail();
		        }else if(eapplication.getAppdecision()!= null && ("DCL").equals(eapplication.getAppdecision())) {
		        	memberEmail = emaildtl.getDcl().getMemberEmail();  
		        	adminEmail = emaildtl.getDcl().getFundEmail();
		        }else if(eapplication.getAppdecision()!= null && ("RUW").equals(eapplication.getAppdecision())) {
		        	memberEmail = emaildtl.getRuw().getMemberEmail();  
		        	adminEmail = emaildtl.getRuw().getFundEmail();
		        }
	        }
	        if( eappInput.getResponseObject()!=null){
        		mixedDecision = emailHelper.isMixedDecision(eappInput.getResponseObject());
	        }
		        if(memberEmail != null && ("Y").equalsIgnoreCase( memberEmail)) {
		        	emailHelper.sendEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,null,mixedDecision, eappInput.getPartnerCode());
		        }
		        if(adminEmail!= null && ("Y").equalsIgnoreCase(adminEmail)) {
		        	emailHelper.sendFundAdminEmail(pdfObject, eapplication, eapplication.getAppdecision(), pdfObject.getClientPDFLoc(), MetlifeInstitutionalConstants.INSTITUTIONAL, property, authId,null,mixedDecision, eappInput.getPartnerCode());
		        }	
	        
		} catch (FileNotFoundException e) {
			log.error(EtoolkitConstants.FILENOTFOUND_EXCEPTION+e.getMessage());
		} catch (IOException e) {
			log.error(EtoolkitConstants.IO_EXPECTION+e.getMessage());
		} 
        return mailSent;
        		
	}
	
	@Override
	public EapplyOutput printQuotePageNew(String authId, EappInput eappInput, UriComponentsBuilder ucBuilder) {
		
		 Properties property = new Properties();
		 InputStream url = null;
		 EapplyOutput eapplyOutput=null;
		 PDFObject pdfObject =null;
		 String logoPath = null;
		 String clientPdfLocation = null;
		 SystemProperty sys;
		 try {
			 	Eapplication eapplication = EapplyServiceHelper.convertToEapplication(eappInput,"");	
				 sys = SystemProperty.getInstance();
				 File file = new File( sys.getProperty(eapplication.getPartnercode()));
			     url = new FileInputStream( file);    
			      property.load( url);
			      log.info("getPropertyFile>> {}",sys.getProperty(eapplication.getPartnercode()));
				  pdfObject = PDFbusinessObjectMapperNew.getPdfOjectsForEapply(eapplication, eappInput, MetlifeInstitutionalConstants.INSTITUTIONAL, sys.getProperty(eapplication.getPartnercode()));
				  pdfObject.setChannel("coverDetails");
			      pdfObject.setLob(MetlifeInstitutionalConstants.INSTITUTIONAL);		 
			      pdfObject.setFundId(eapplication.getPartnercode());
				  pdfObject.setSectionBackground(property.getProperty("ClientSectionColor_" + pdfObject.getFundId()));
				  pdfObject.setColorStyle(property.getProperty("ClientColorStyle_" + pdfObject.getFundId()));
				  pdfObject.setSectionTextColor(property.getProperty("ClientSectionTextColor_" + pdfObject.getFundId()));
				  pdfObject.setBorderColor(pdfObject.getSectionBackground());
				  logoPath = property.getProperty("logo_" + pdfObject.getFundId()); 
				  pdfObject.setLogPath(logoPath);
				  if(!pdfObject.isQuickQuoteRender()&& null!=eappInput.getApplicatioNumber())
				  {
				  pdfObject.setApplicationNumber(""+eappInput.getApplicatioNumber());
				  }
				  
				  clientPdfLocation = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+ eapplication.getPartnercode()+"_"+eapplication.getApplicationumber()+".pdf";
				  if(("premium".equalsIgnoreCase(eappInput.getCalculatorFlag()) || "insurancecover".equalsIgnoreCase(eappInput.getCalculatorFlag()))){
					  clientPdfLocation = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+ eapplication.getPartnercode()+"_"+EapplyHelper.getDateExtension()+".pdf";
				  }
				
				  pdfObject.setPdfAttachDir(clientPdfLocation);
				  pdfObject.setClientPDFLoc(clientPdfLocation);
				  GenerateEapplyPDF generateEapplyPDF = new GenerateEapplyPDF();
				  generateEapplyPDF.generatePDF(pdfObject,sys.getProperty(eapplication.getPartnercode()));
				  eapplyOutput=new EapplyOutput();
				 
				  eapplyOutput.setClientPDFLocation(EapplyHelper.formEncryptedFilePath(pdfObject.getClientPDFLoc()));

					
		 	} catch (IOException e) {
		 		log.error("Error in input output of printquote page: {}",e.getMessage());
			} catch (DocumentException e) {
				log.error("Error in document printquote: {}",e.getMessage());
			} catch (MetLifeBSRuntimeException e) {
				log.error("Error in metlifebs printquote : {}",e.getMessage());
			}catch (Exception e) {
				log.error("Error in printing quote page: {} ",e.getMessage());
			}  
		
		
		return eapplyOutput;
	}
	
	@Override
	public DocumentInfo uploadFileNew(String contentType,InputStream inputStream) {
		
		DocumentInfo docInfo = null;
		 log.info(">>>inside {}",contentType);
		 String filepath = null;
		 File targetFile = null;
		 SystemProperty sys = null;
		 try {
			sys = SystemProperty.getInstance();
		} catch (Exception e1) {
			log.error("Error in reading system property: {}",e1);
		}
		 if(sys != null) {
			 filepath = sys.getProperty(MetlifeInstitutionalConstants.PDF_BASE_PATH)+contentType;
		 }
		 targetFile = new File(filepath);
	        try(OutputStream outStream = new FileOutputStream(targetFile)) {
	        	 
	        	byte[] buffer = new byte[1024];
	        	int len = inputStream.read(buffer);
	        	while (len != -1) {
	        		outStream.write(buffer, 0, len);
	        	    len = inputStream.read(buffer);
	        	}	            
	            IOUtils.copy(inputStream,outStream);
	            inputStream.close();
	            
	            docInfo = new DocumentInfo();
	            docInfo.setDocLoc(filepath);
	            docInfo.setName(contentType);
	            docInfo.setStatus(Boolean.TRUE);
	            
	        }  catch (Exception e) {
			    log.error("Error in file uplaod: {}",e);
			} 	
	        
	      return docInfo;
		
	}
	
}
