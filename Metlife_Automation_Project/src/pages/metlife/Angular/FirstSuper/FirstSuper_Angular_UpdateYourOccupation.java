/**
 * 
 */

package pages.metlife.Angular.FirstSuper;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Hemnath
 * 
 */
public class FirstSuper_Angular_UpdateYourOccupation extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public FirstSuper_Angular_UpdateYourOccupation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public FirstSuper_Angular_UpdateYourOccupation UpdateYourOccupation_FirstSuper() {
		updateoccupation();
		HealthandLifestyle();
		Declaration();
		return new FirstSuper_Angular_UpdateYourOccupation(scriptHelper);
	} 
	
	private void updateoccupation() {
		
		
		String EmailId = dataTable.getData("FirstSuper_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("FirstSuper_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("FirstSuper_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("FirstSuper_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("FirstSuper_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("FirstSuper_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("FirstSuper_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("FirstSuper_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("FirstSuper_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("FirstSuper_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("FirstSuper_UpdateOccupation", "TertiaryQual");
		String Eightypercenttimeinoffice = dataTable.getData("FirstSuper_UpdateOccupation", "Eightypercenttimeinoffice");
		
		String AnnualSalary = dataTable.getData("FirstSuper_UpdateOccupation", "AnnualSalary");
		String DutyOfDisclosure = dataTable.getData("FirstSuper_UpdateOccupation", "DutyOfDisclosure");
		String PrivacyStatement = dataTable.getData("FirstSuper_UpdateOccupation", "PrivacyStatement");
		
		try{
			
			//*****Click on Update Occupation Link*******\\
			
			sleep(1500);
			WebDriverWait wait=new WebDriverWait(driver,18);			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Update your occupation ')]"))).click();
			report.updateTestLog("Update your Occupation", "Update occupation link is Selected", Status.PASS);
			

			 sleep(1500);
			
			
			//*****Agree to Duty of disclosure*******\\
				
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is checked", Status.PASS);
			String dod=driver.findElement(By.xpath("(//div[@class=' col-sm-12'])[1]")).getText();
/*			
			if(dod.equalsIgnoreCase(DutyOfDisclosure)){
				report.updateTestLog("Duty Of Disclosure", "Wording is displayed as Expected", Status.PASS);
				
			}
			else{
				report.updateTestLog("Duty Of Disclosure", "Wording is not displayed as Expected", Status.FAIL);
				
			}*/
			sleep(200);
			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			report.updateTestLog("Privacy Statement", "Privacy Statement label is checked", Status.PASS);
		/*	
            String ps=driver.findElement(By.xpath("(//div[@class=' col-sm-12'])[2]")).getText();
			
			if(ps.equalsIgnoreCase(PrivacyStatement)){
				report.updateTestLog("Privacy Statement", "Wording is displayed as Expected", Status.PASS);
			}
			else{
				report.updateTestLog("Privacy Statement", "Wording is not displayed as Expected", Status.FAIL);
			}*/
			sleep(200);

			//*****Enter the Email Id*****\\										
			

			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);
										
			//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);					
										
			//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
										
										
			//***Select the Preferred time of Contact*****\\			
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
						
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}							
										
			//****Contact Details-Continue Button*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);							
										
										
			//***************************************\\							
			//**********OCCUPATION SECTION***********\\							
			//****************************************\\							
										
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
		
			
							
										
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("workRatingIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your occupation?']"))).click();
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("workRatingoccupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
					
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}		
							
										
				
			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(6) > div > div:nth-child(2) > div > div.col-sm-3.pl0 > label > span"))).click();
		    	//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#occupFormId > div > div:nth-child(6) > div > div:nth-child(2) > div > div.col-sm-6.pl0xs > label > span"))).click();
			//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{	
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
			
			//   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			
		}		
		        sleep(500);
		        
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
									
			//******Do you spend at least 80% of all working time in an office*****\\
			if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[1]/label/span"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[2]/label/span"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
			}
			sleep(500);						
			 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){										

				 //***Select if your office Duties are Undertaken within an Office Environment?\\	
	
if(OfficeEnvironment.equalsIgnoreCase("Yes")){							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
	report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
						
  }							
        else{							

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
						
     }							
						
  //*****Do You Hold a Tertiary Qualification******\\							
						
    if(TertiaryQual.equalsIgnoreCase("Yes")){							
   
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[5]"))).click();
		report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
}							
else{							
   
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[5]"))).click();
	report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
}									
		}	
		}
								    
          sleep(800);
		   
		   
        //*****What is your annual Salary******							
			
			
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
									
		
		
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingOccupForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		sleep(3500);

		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	

		private void HealthandLifestyle() {

			String Restrictedillness = dataTable.getData("FirstSuper_UpdateOccupation", "Restrictedillness");
			String PriorClaim = dataTable.getData("FirstSuper_UpdateOccupation", "PriorClaim");
			String ReducedLifeExpectancy = dataTable.getData("FirstSuper_UpdateOccupation", "ReducedLifeExpectancy");
			
			try{			


      if(driver.getPageSource().contains("Eligibility Check")){

	//*******Are you restricted due to illness or injury******\\
	
	WebDriverWait wait=new WebDriverWait(driver,18);
         
	 if(Restrictedillness.equalsIgnoreCase("Yes")){
		
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
   }
    
	 else{
		 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
    }

     //******Are you contemplating, or have you ever made a claim for sickness, accident or disability benefits, Workers� Compensation or any other form of compensation due to illness or injury?***\\

     if(PriorClaim.equalsIgnoreCase("Yes")){
    	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
     }
  
     else{
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
    	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
    	sleep(1000);
        }


//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\

    if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){
	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
    }
   else{
	   
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
}

          //****Click on Continue******\\

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
    sleep(2500);
      }

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

		}
		private void Declaration() {
			String GeneralConsent = dataTable.getData("FirstSuper_UpdateOccupation", "GeneralConsent");	
			String Decision = dataTable.getData("FirstSuper_UpdateOccupation", "Decision");
			try{

        //*****General Consent******\\

				WebDriverWait wait=new WebDriverWait(driver,18);
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelWR']/span"))).click();
   report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
   sleep(500);
   
   String gs=driver.findElement(By.xpath("(//div[@class='col-sm-12'])[1]")).getText();
   
/*	
	if(gs.equalsIgnoreCase(GeneralConsent)){
		report.updateTestLog("General Consent", "Wording is displayed as Expected", Status.PASS);
		
	}
	else{
		report.updateTestLog("General Consent", "Wording is not displayed as Expected", Status.FAIL);
		
	}*/
	sleep(200);

          //******Submit******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitWorkRating()']"))).click();
report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

//*******Feedback Pop-up******\\

	
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
sleep(1000);
/*
String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
System.out.println(Justification);
sleep(400);

if(Justification.equalsIgnoreCase(Decision)){
	report.updateTestLog("Decision", "Wording is displayed as Expected", Status.PASS);
	
}
else{
	report.updateTestLog("Decision", "Wording is not displayed as Expected", Status.FAIL);
	
}*/
sleep(200);

String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);

			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		
		}
			}
	}

		/*
		String EmailId = dataTable.getData("FirstSuper_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("FirstSuper_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("FirstSuper_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("FirstSuper_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("FirstSuper_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("FirstSuper_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("FirstSuper_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("FirstSuper_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("FirstSuper_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("FirstSuper_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("FirstSuper_UpdateOccupation", "TertiaryQual");
		String Eightypercenttimeinoffice = dataTable.getData("FirstSuper_UpdateOccupation", "Eightypercenttimeinoffice");
		
		String AnnualSalary = dataTable.getData("FirstSuper_UpdateOccupation", "AnnualSalary");
		String DutyOfDisclosure = dataTable.getData("FirstSuper_UpdateOccupation", "DutyOfDisclosure");
		String PrivacyStatement = dataTable.getData("FirstSuper_UpdateOccupation", "PrivacyStatement");
		
		try{
			
			//*****Click on Update Occupation Link*******\\
			
			sleep(1500);
			WebDriverWait wait=new WebDriverWait(driver,18);			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Update your occupation ')]"))).click();
			report.updateTestLog("Update your Occupation", "Update occupation link is Selected", Status.PASS);
			

			 sleep(1500);
			
			
			//*****Agree to Duty of disclosure*******\\
				
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is checked", Status.PASS);
			String dod=driver.findElement(By.xpath("(//div[@class=' col-sm-12'])[1]")).getText();
			
			if(dod.equalsIgnoreCase(DutyOfDisclosure)){
				report.updateTestLog("Duty Of Disclosure", "Wording is displayed as Expected", Status.PASS);
				
			}
			else{
				report.updateTestLog("Duty Of Disclosure", "Wording is not displayed as Expected", Status.FAIL);
				
			}
			sleep(200);
			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			report.updateTestLog("Privacy Statement", "Privacy Statement label is checked", Status.PASS);
			
            String ps=driver.findElement(By.xpath("(//div[@class=' col-sm-12'])[2]")).getText();
			
			if(ps.equalsIgnoreCase(PrivacyStatement)){
				report.updateTestLog("Privacy Statement", "Wording is displayed as Expected", Status.PASS);
			}
			else{
				report.updateTestLog("Privacy Statement", "Wording is not displayed as Expected", Status.FAIL);
			}
			sleep(200);

			//*****Enter the Email Id*****\\										
			

			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);
										
			//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);					
										
			//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
										
										
			//***Select the Preferred time of Contact*****\\			
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
						
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}							
										
			//****Contact Details-Continue Button*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);							
										
										
			//***************************************\\							
			//**********OCCUPATION SECTION***********\\							
			//****************************************\\							
										
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			
			//*****Resident of Australia****\\	
			
			if(Citizen.equalsIgnoreCase("Yes")){		
				driver.findElement(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
					
				}	
			else{	
				driver.findElement(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
					
				}	
								
										
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("workRatingIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your occupation?']"))).click();
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("workRatingoccupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
					
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}		
							
										
				
			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[3]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[3]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			
		}		
		        sleep(500);
		        
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you spend at least 80% of all working time in an office environment?")){							
									
			//******Do you spend at least 80% of all working time in an office*****\\
			if(Eightypercenttimeinoffice.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[1]/label/span"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected Yes", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you spend at least 80% of all working time in an office environment?']/../following-sibling::div/div/div[2]/label/span"))).click();
				report.updateTestLog("Eighty Percent Time in Office", "Selected No", Status.PASS);
			}
			sleep(500);						
			 if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){										

				 //***Select if your office Duties are Undertaken within an Office Environment?\\	
	
if(OfficeEnvironment.equalsIgnoreCase("Yes")){							

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[4]"))).click();
	report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
						
  }							
        else{							

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[4]"))).click();
report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
						
     }							
						
  //*****Do You Hold a Tertiary Qualification******\\							
						
    if(TertiaryQual.equalsIgnoreCase("Yes")){							
   
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[5]"))).click();
		report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
}							
else{							
   
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[5]"))).click();
	report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
}									
		}	
		}
								    
          sleep(800);
		   
		   
        //*****What is your annual Salary******							
			
			
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
									
		
		
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingOccupForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		sleep(3500);

		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	

		private void HealthandLifestyle() {

			String Restrictedillness = dataTable.getData("FirstSuper_UpdateOccupation", "Restrictedillness");
			String PriorClaim = dataTable.getData("FirstSuper_UpdateOccupation", "PriorClaim");
			String ReducedLifeExpectancy = dataTable.getData("FirstSuper_UpdateOccupation", "ReducedLifeExpectancy");
			
			try{			


      if(driver.getPageSource().contains("Eligibility Check")){

	//*******Are you restricted due to illness or injury******\\
	
	WebDriverWait wait=new WebDriverWait(driver,18);
         
	 if(Restrictedillness.equalsIgnoreCase("Yes")){
		
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
   }
    
	 else{
		 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
    }

     //******Are you contemplating, or have you ever made a claim for sickness, accident or disability benefits, Workers� Compensation or any other form of compensation due to illness or injury?***\\

     if(PriorClaim.equalsIgnoreCase("Yes")){
    	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
     }
  
     else{
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
    	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
    	sleep(1000);
        }


//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\

    if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){
	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
    }
   else{
	   
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
}

          //****Click on Continue******\\

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
    sleep(2500);
      }

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

		}
		private void Declaration() {
			String GeneralConsent = dataTable.getData("FirstSuper_UpdateOccupation", "GeneralConsent");	
			String Decision = dataTable.getData("FirstSuper_UpdateOccupation", "Decision");
			try{

        //*****General Consent******\\

				WebDriverWait wait=new WebDriverWait(driver,18);
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelWR']/span"))).click();
   report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
   sleep(500);
   
   String gs=driver.findElement(By.xpath("(//div[@class='col-sm-12'])[1]")).getText();
   
	
	if(gs.equalsIgnoreCase(GeneralConsent)){
		report.updateTestLog("General Consent", "Wording is displayed as Expected", Status.PASS);
		
	}
	else{
		report.updateTestLog("General Consent", "Wording is not displayed as Expected", Status.FAIL);
		
	}
	sleep(200);

          //******Submit******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitWorkRating()']"))).click();
report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

//*******Feedback Pop-up******\\

	
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
sleep(1000);

String Justification=driver.findElement(By.xpath("//p[@align='justify']")).getText();
System.out.println(Justification);
sleep(400);

if(Justification.equalsIgnoreCase(Decision)){
	report.updateTestLog("Decision", "Wording is displayed as Expected", Status.PASS);
	
}
else{
	report.updateTestLog("Decision", "Wording is not displayed as Expected", Status.FAIL);
	
}
sleep(200);

String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);

			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		
		}
			}
	}*/

	

	

