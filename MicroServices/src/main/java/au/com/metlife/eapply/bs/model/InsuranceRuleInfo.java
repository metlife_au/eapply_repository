package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class InsuranceRuleInfo implements Serializable{
	 
	private static final long serialVersionUID = 1L;
	
	private Integer retirementAge;
	private BigDecimal inflationRate;
	private BigDecimal realInterestRate;
	private BigDecimal superContribRate;
	private BigDecimal ipBenefitPaidToSA;
	private BigDecimal ipReplaceRatio;
	private BigDecimal funeralCost;
	private BigDecimal annualMedAndNursingCost;
	private Integer dependentSuppAge;
	public Integer getRetirementAge() {
		return retirementAge;
	}
	public void setRetirementAge(Integer retirementAge) {
		this.retirementAge = retirementAge;
	}
	public BigDecimal getInflationRate() {
		return inflationRate;
	}
	public void setInflationRate(BigDecimal inflationRate) {
		this.inflationRate = inflationRate;
	}
	public BigDecimal getRealInterestRate() {
		return realInterestRate;
	}
	public void setRealInterestRate(BigDecimal realInterestRate) {
		this.realInterestRate = realInterestRate;
	}
	public BigDecimal getSuperContribRate() {
		return superContribRate;
	}
	public void setSuperContribRate(BigDecimal superContribRate) {
		this.superContribRate = superContribRate;
	}
	public BigDecimal getIpBenefitPaidToSA() {
		return ipBenefitPaidToSA;
	}
	public void setIpBenefitPaidToSA(BigDecimal ipBenefitPaidToSA) {
		this.ipBenefitPaidToSA = ipBenefitPaidToSA;
	}
	public BigDecimal getIpReplaceRatio() {
		return ipReplaceRatio;
	}
	public void setIpReplaceRatio(BigDecimal ipReplaceRatio) {
		this.ipReplaceRatio = ipReplaceRatio;
	}
	public BigDecimal getFuneralCost() {
		return funeralCost;
	}
	public void setFuneralCost(BigDecimal funeralCost) {
		this.funeralCost = funeralCost;
	}
	public BigDecimal getAnnualMedAndNursingCost() {
		return annualMedAndNursingCost;
	}
	public void setAnnualMedAndNursingCost(BigDecimal annualMedAndNursingCost) {
		this.annualMedAndNursingCost = annualMedAndNursingCost;
	}
	public Integer getDependentSuppAge() {
		return dependentSuppAge;
	}
	public void setDependentSuppAge(Integer dependentSuppAge) {
		this.dependentSuppAge = dependentSuppAge;
	}

}
