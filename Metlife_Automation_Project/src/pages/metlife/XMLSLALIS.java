package pages.metlife;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.description.Parameter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import au.com.metlife.webservices.QuotationServiceStub;



public class XMLSLALIS {
	private Map<String, Object> sessionThreadData = null;

	public String getPayLoad() {
		StringBuffer sb = null;
		String inputString = null;
		sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\"?>" + "<quoteInput>" + "<brandName>COFS</brandName>");
		sb.append("<productName>Life Insurance</productName>");
		sb.append("<paymentFrequency>" + getFequency(getStringValueFromSessionThreadData("PaymentFrequency")) + "</paymentFrequency>");
		sb.append("<insured>");
		sb.append("<surName>" + getStringValueFromSessionThreadData("FirstName") + "</surName>");
		sb.append("<dob>" + getPremiumDOB(Integer.parseInt("-"+getStringValueFromSessionThreadData("DateOfBirth"))) + "</dob>");
		sb.append("<gender>" + getStringValueFromSessionThreadData("Gender") + "</gender>");
		sb.append("<insuredNo>1</insuredNo>");
		sb.append("<smoker>"+true+"</smoker>");
		sb.append("<cover>");
		sb.append("<coverNo>238</coverNo>");
		sb.append("<coverAmount>"+(getStringValueFromSessionThreadData("CoverAmount").replace("$", "")).replace(",", "")+"</coverAmount>");
		sb.append("<loading>");
		sb.append("<loadingType>109</loadingType>");
		sb.append("<percentageLoading>-20</percentageLoading>");
		sb.append("</loading>");
		sb.append("<loading>");
		sb.append("<loadingType>2</loadingType>");
		sb.append("<percentageLoading>0</percentageLoading>");
		sb.append("<loadingAmount/>");
		sb.append("</loading>");
		sb.append("<loading>");
		sb.append("<loadingType>110</loadingType>");
		sb.append("<percentageLoading>0</percentageLoading>");
		//sb.append("<loadingAmount></loadingAmount>");
		sb.append("</loading>");
		sb.append("</cover>");
		sb.append("</insured>");
		sb.append("</quoteInput>");
		inputString=sb.toString();
		return inputString;
	}
	
	protected String getFequency(String strFrequency){
		String convertedFreq = null;
		if ("Annual".equalsIgnoreCase(strFrequency))
			convertedFreq="1";
		else if("monthly".equalsIgnoreCase(strFrequency))
			convertedFreq="12";
		else if("fortnightly".equalsIgnoreCase(strFrequency))
			convertedFreq="26";
		else
			System.out.println("Frequency Does not match");
		
		return convertedFreq;		
	}
	 

	public void storeSessionThreadData(String key, Object value) {
		sessionThreadData.put(key, value);
	}

	public String getStringValueFromSessionThreadData(String key) {
		return (String) sessionThreadData.get(key);
	}

	protected String getPremiumDOB(int intYears) {
		Calendar now = Calendar.getInstance();
		String strDate = null;

		now = Calendar.getInstance();
		now.add(Calendar.YEAR, intYears);
		System.out.println("date before " + intYears + " years : " + (now.get(Calendar.MONTH) + 1) + "-"
				+ now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
		strDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String today = formatter.format(new Date(strDate));
		return today;
	}

	public static void main(String[] args) {
		XMLSLALIS excelapp = new XMLSLALIS();
		excelapp.readExcelNUpdateforPremium();
	}
	
	protected String sendPremiumCalcSoapReq(String inputString) throws Exception {
		String argsPass[] = new String[2];
		argsPass[0] = "https://www.e2e.equery.metlife.com.au/CAW2/services/QuotationService";// webservice
		argsPass[1] = "C:\\client_config\\";
		ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(argsPass[1],null);
		QuotationServiceStub stub1 = null;
		stub1 = new QuotationServiceStub(ctx, argsPass[0]);
		ServiceClient client = stub1._getServiceClient();
		client.engageModule("rampart");
		Options options = client.getOptions();
		options.setTimeOutInMilliSeconds(100000);
		options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, getOutflowConfiguration("syselodg"));
		PWCBClientHandler myCallback = new PWCBClientHandler();
		myCallback.setUTUsername("internal01");
		myCallback.setUTPassword("metlife980");
		options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
		client.setOptions(options);
		QuotationServiceStub.GetQuote oq = new QuotationServiceStub.GetQuote();
		String xml = null;
		System.out.println("*****************************************************************************************");
		System.out.println(inputString);
		System.out.println("*****************************************************************************************");
		xml=inputString;
		oq.setQuoteXML(xml);
		QuotationServiceStub.GetQuoteResponse res = stub1.getQuote(oq);
		//System.out.println(res.get_return());
		//System.out.println(new Date());
		//System.out.println(new Date().getTime());
		return res.get_return();
	}
	
	
	private static Parameter getOutflowConfiguration(String username) {
		OutflowConfiguration ofc = new OutflowConfiguration();
		ofc.setActionItems("UsernameToken");
		ofc.setPasswordType("PasswordText");
		ofc.setUser(username);
		return ofc.getProperty();
	}

	public void readExcelNUpdateforPremium() {

		try {

			FileInputStream file = new FileInputStream(new File("C:\\Users\\sampa\\Desktop\\test.xls"));

			// Get the workbook instance for XLS file
			HSSFWorkbook workbook = new HSSFWorkbook(file);

			// Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheet("MetLife_Data");

			System.out.println("Rows :: " + sheet.getPhysicalNumberOfRows());
			// Iterate through each rows from first sheet

			for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
				this.sessionThreadData = new HashMap<String, Object>();
				storeSessionThreadData(sheet.getRow(0).getCell(4).toString().trim(),sheet.getRow(i).getCell(4).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(5).toString().trim(),sheet.getRow(i).getCell(5).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(6).toString().trim(),sheet.getRow(i).getCell(6).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(7).toString().trim(),sheet.getRow(i).getCell(7).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(8).toString().trim(),sheet.getRow(i).getCell(8).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(9).toString().trim(),sheet.getRow(i).getCell(9).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(10).toString().trim(),sheet.getRow(i).getCell(10).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(11).toString().trim(),sheet.getRow(i).getCell(11).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(12).toString().trim(),sheet.getRow(i).getCell(12).toString().trim());
				storeSessionThreadData(sheet.getRow(0).getCell(13).toString().trim(),sheet.getRow(i).getCell(13).toString().trim());
				//sendPremiumCalcSoapReq(getPayLoad());
				System.out.println("#######################################################################################################################################");
				System.out.println(sendPremiumCalcSoapReq(getPayLoad()));
				System.out.println(getPremium(sendPremiumCalcSoapReq(getPayLoad()),"/quoteOutput/insured/cover/calculatedPremium"));
				System.out.println("#######################################################################################################################################");
				//getPayLoad();
				System.out.println("\n");
			}
			file.close();
			/*FileOutputStream out = new FileOutputStream(new File("C:\\Users\\sampa\\Desktop\\test2.xls"));
			workbook.write(out);
			out.close();*/

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String getPremium(String strXml,String strXPath) throws Exception{
		
		 DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		 Node node = null;
		 String returnValue = null;
	        try {
	            DocumentBuilder builder = domFactory.newDocumentBuilder();
	            Document dDoc = builder.parse(new InputSource(new StringReader(strXml)));
	            XPath xPath = XPathFactory.newInstance().newXPath();
	            returnValue = (String)xPath.evaluate(strXPath, dDoc,XPathConstants.STRING);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        
	        return returnValue;
	}
}
