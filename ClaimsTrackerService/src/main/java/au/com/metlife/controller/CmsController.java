package au.com.metlife.controller;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.model.ClaimOutput;
import au.com.metlife.model.Status;
import au.com.metlife.service.CmsService;
 

@RestController

public class CmsController {
	
	@Autowired 	
     CmsService cmsService;  //Service which will do all data retrieval/manipulation work
	
//	@Autowired 	
//    Status status;  //Service which will do all data retrieval/manipulation work
	
	//@RequestMapping(value = "/API/Quote/", method = RequestMethod.POST)
	@RequestMapping(value = "/method11",method = RequestMethod.POST)	
	public  ResponseEntity <HashMap<String, Object>> getCmsData(InputStream inputStream) {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";	
		HashMap<String, Object> cmsData = null;
			try {
				while ((line = in.readLine()) != null) {
					string += line + "\n";
					System.out.println("line>>"+line);
				}
					
				JSONObject jsonObject = new JSONObject(string);		
				//cmsData = cmsService.refreshCache();				
				System.out.println("fdfd>"+cmsData);
			    if(cmsData==null){
			    	return new ResponseEntity<HashMap<String, Object>>(HttpStatus.NO_CONTENT);			        
			    }	    
			    System.out.println("END--"+new  java.util.Date());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new ResponseEntity<HashMap<String, Object>>(cmsData,HttpStatus.OK);
	    

	}	
	
	@RequestMapping(value = "/method22", method = RequestMethod.POST)
	public  ResponseEntity <List<ClaimOutput>> getQuote(InputStream inputStream) {
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";	
		String claimNumber = null;
		ClaimOutput claimOutput= null;
		List<ClaimOutput> claimOutputList = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
				JSONObject jsonObject = new JSONObject(string);		
				claimNumber= (String)jsonObject.get("claimNo");
				//claimOutputList=cmsService.getClaimStatus(claimNumber);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	    if(claimOutputList==null){
	        return new ResponseEntity<List<ClaimOutput>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    
	    System.out.println("END--"+new  java.util.Date());
	    return new ResponseEntity<List<ClaimOutput>>(claimOutputList, HttpStatus.OK);
	    

	}		
	
	@RequestMapping(value = "/method23", method = RequestMethod.POST)
	public  ResponseEntity <List<ClaimOutput>> getEvents(InputStream inputStream) {
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";	
		String claimNumber = null;
		ClaimOutput claimOutput= null;
		List<ClaimOutput> claimOutputList = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
				JSONObject jsonObject = new JSONObject(string);		
				claimNumber= (String)jsonObject.get("claimNo");
				claimOutputList=cmsService.getClaimEvents(claimNumber);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	    if(claimOutputList==null){
	        return new ResponseEntity<List<ClaimOutput>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    
	    System.out.println("END--"+new  java.util.Date());
	    return new ResponseEntity<List<ClaimOutput>>(claimOutputList, HttpStatus.OK);
	    

	}
	
	@RequestMapping(value = "/method24", method = RequestMethod.POST)
	public  ResponseEntity <List<ClaimOutput>> getRequirements(InputStream inputStream) {
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";	
		String claimNumber = null;
		ClaimOutput claimOutput= null;
		List<ClaimOutput> claimOutputList = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
				JSONObject jsonObject = new JSONObject(string);		
				claimNumber= (String)jsonObject.get("claimNo");
				claimOutputList=cmsService.getClaimRequirements(claimNumber);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	    if(claimOutputList==null){
	        return new ResponseEntity<List<ClaimOutput>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    
	    System.out.println("END--"+new  java.util.Date());
	    return new ResponseEntity<List<ClaimOutput>>(claimOutputList, HttpStatus.OK);	    

	}
	
//	@RequestMapping(value = "/retrieveClaimDetails")
//	public  ResponseEntity <List<ClaimOutput>> retrieveClaimDetails() {
//		
//		String line = null;	
//		String string = "";	
//		String claimNumber = null;		
//		List<ClaimOutput> claimOutputList = null;
////		try {
//			
//				//string += line + "\n";
//				//System.out.println("line>>"+line);
//				//JSONObject jsonObject = new JSONObject(string);		
//				//claimNumber= (String)jsonObject.get("claimNo");
//				claimOutputList=cmsService.getClaimRequirements("42966");
//			
////		} catch (JSONException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}	
//	    if(claimOutputList==null){
//	        return new ResponseEntity<List<ClaimOutput>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//	    }
//	    
//	    System.out.println("END--"+new  java.util.Date());
//	    return new ResponseEntity<List<ClaimOutput>>(claimOutputList, HttpStatus.OK);	    
//
//	}
	
	@RequestMapping(value = "/retrieveClaimDetails")
	public  ResponseEntity <List<ClaimOutput>> retrieveClaimDetails(@RequestParam(value="claimNo") String claimNo,@RequestParam(value="surName") String surName,@RequestParam(value="dob") String dob) {		
				
		List<ClaimOutput> claimOutputList = null;		
		
		SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
		String reformattedStr = null;
		Status status = null;
		try {
		    reformattedStr = myFormat.format(fromUser.parse(dob));
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		
		System.out.println("gpsdate :" + reformattedStr);
		//Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());
		//System.out.println("fromTS1.toString() :"+fromTS1.toString());
		claimOutputList=cmsService.getClaimStatus(claimNo,surName,reformattedStr);			
	
	    if(claimOutputList != null && claimOutputList.size() == 0){
	    	status = new Status();
	    	status.setStatusCode("001");
	    	status.setStatusDesc("Our records don�t match your responses. Please try again, otherwise please contact our call centre for assistance");
	        return new ResponseEntity(status,HttpStatus.OK);//You many decide to return HttpStatus.NOT_FOUND
	    } else if (claimOutputList != null && claimOutputList.size() > 1) {
	    	status = new Status();
	    	status.setStatusCode("002");
	    	status.setStatusDesc("Please contact the call centre");
	    	return new ResponseEntity(status,HttpStatus.OK);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    
	    System.out.println("END--"+new  java.util.Date());
	    return new ResponseEntity<List<ClaimOutput>>(claimOutputList, HttpStatus.OK);	    

	}
	
}