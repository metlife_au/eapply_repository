package pages.metlife.NEW;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_LifeEventPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_LifeEventPage HOSTPLUSLifeEvent() {
		LifeEventHOSTPLUS();
		HealthandLifeStyle();
		confirmation();
		
		return new HOSTPLUS_LifeEventPage(scriptHelper);
	}
	
	public HOSTPLUS_LifeEventPage HOSTPLUSLifeNegativeFlow() {
		LifeEventHOSTPLUSNegativeFlow();
		
		return new HOSTPLUS_LifeEventPage(scriptHelper);
	}

	public HOSTPLUS_LifeEventPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void LifeEventHOSTPLUS() {

		String EmailId = dataTable.getData("HOSTPLUS_LifeEvent", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_LifeEvent", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TimeofContact");		
		String RegularIncome = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_LifeEvent", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_LifeEvent", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_LifeEvent", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_LifeEvent", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_LifeEvent", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_LifeEvent", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_LifeEvent", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_LifeEvent", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent_Date");
		String LifeEvent_PrevApplyin1Year = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent_PrevApplyin1Year");
		String DocumentaryEvidence = dataTable.getData("HOSTPLUS_LifeEvent", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("HOSTPLUS_LifeEvent", "AttachPath");
		String Negative_Scenario=dataTable.getData("HOSTPLUS_LifeEvent", "Negative_Scenario");
		
		try{
			
			
			
			//****Click on Life Event Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Increase your cover due to a life event')]"))).click();
			
			report.updateTestLog("Life Event", "Life Event Link is Clicked", Status.PASS);
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\	
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(200);

			//*****Click on the Contact Number Type****\\			
				
				if(!TypeofContact.equalsIgnoreCase("Mobile")){			
							
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Mobile')]"))).click();
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();			
				report.updateTestLog("Contact Type", "Preferred contact: "+TypeofContact+" is Selected", Status.PASS);
				sleep(500);
				}
				
				
				//****Enter the Contact Number****\\			
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(ContactNumber);
				sleep(500);
				report.updateTestLog("Contact NUmber", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mrngRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("aftrNoonRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				
							
				//****Contact Details-Continue Button*****\\			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueContDetlID']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				sleep(1200);			
							
				
					
					//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
					
					if(RegularIncome.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
							
					}		
					else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
							
					}	
					
					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_yesId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_noId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_yesId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_noId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					
					//*****Resident of Australia****\\	
					
					if(Citizen.equalsIgnoreCase("Yes")){		
						driver.findElement(By.id("valCitizenAus_yes_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						driver.findElement(By.id("valCitizenAus_no_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}	
							
					//*****Industry********\\		
				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='industryId']/span)[1]"))).click();
					sleep(800);				
					driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();		
					sleep(800);		
					report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
					sleep(600);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),' What is your occupation?')]"))).click();
					sleep(800);
					
					//*****Occupation*****\\		
					
					driver.findElement(By.xpath("(//button[@data-id='occupationId']/span)[1]")).click();
					//wait.until(ExpectedConditions.elementToBeClickable(By.id("(//button[@data-id='occupationId']/span)[2]"))).click();	
		            sleep(800);							
					driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();		
					sleep(800);	
					report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
					sleep(500);
							
							
					//*****Other Occupation *****\\	
					
					if(OccupationType.equalsIgnoreCase("Other")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupationId"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);	
					}		
					
					
					//*****Extra Questions*****\\							
					
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
				   
				    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
											
				}							
				else{							

					wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
											
				}							
											
				      //*****Do You Hold a Tertiary Qualification******\\							
											
				        if(TertiaryQual.equalsIgnoreCase("Yes")){							
				       
				        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}							
				   else{							
				       
					   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}							
				}							
											
											
											
				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
											
											
											
				//******Do you work in hazardous environment*****\\		
					
				if(HazardousEnv.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_yes_id__xc_r"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_no_id__xc_"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}							
											
				//******Do you spend 10% time working outside office*******\\	
				
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_yes_id__xc_r"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_no_id__xc_"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}							
				}							
							
				    
                  sleep(800);
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				    	//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
					   
					    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
												
					}							
					else{							

						wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
												
					}							
												
					      //*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){							
					       
					        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}						
				}		
				    
											
				sleep(1000);
				
				//*****Click on Continue*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occuContinueId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				

				//*****Click on Continue*******\\
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occuContinueId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				sleep(800);
				
				//********************************************************************************************************\\
				//****************************LIFE EVENT SECTION***********************************************************\\
				//*********************************************************************************************************\\
				
				
				//*****Please select the specific life event you are applying under to increase your cover*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Please Select your Life event')])[1]"))).click();
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+LifeEvent+"')]"))).click();
				report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);
				sleep(800);
				
             if(Negative_Scenario.equalsIgnoreCase("No")){
            	 
            	//*****What is the date of the life event?*******\\					

					driver.findElement(By.name("lifeeventdateId")).sendKeys(LifeEvent_Date);
					sleep(500);
					driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
					report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
					sleep(800);
			
			
			//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months 
			//within this calendar year?********************\\
			
			if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("lifeevntincrease_yesId__xc_r"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
				sleep(800);	
				
			}
			
			else if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='LifeEvntNoLblID']/span"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","No is Selected", Status.PASS);
				sleep(800);	
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("lifeevntincrease_unsureId__xc_r"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Unsure is Selected", Status.PASS);
				sleep(800);	
			}
			
             }

			   
				//******Do you wish to include documentary evidence of your life event?******\\
				
				if(DocumentaryEvidence.equalsIgnoreCase("Yes")){
					
					
					wait.until(ExpectedConditions.elementToBeClickable(By.id("lifEvnt_documentary_yesId__xc_r"))).click();
					report.updateTestLog("Documentary Evidence","Yes is Selected", Status.PASS);
					sleep(800);	
				
					
					
					//****Attach the file******		
					
					driver.findElement(By.id("LifeEventFileUploadAttach")).sendKeys(AttachPath);
					sleep(1500);
					
					//*****Let the attachment load*****\\	
					
					driver.findElement(By.xpath("//label[contains(text(),'Please include documentary evidence of your event. You are required to upload:')]")).click();
					sleep(1000);
					
					//*****Add the Attachment*****\\
					
					WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
					sleep(1000);
					JavascriptExecutor jse = (JavascriptExecutor) driver;
				    jse.executeScript("window.scrollBy(0,450)", "");
				    sleep(300);
					report.updateTestLog("Evidence Attachment","Attachement is Successful", Status.PASS);
				
				}
				else{
					
					//****No Attachment******\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.id("lifEvnt_documentary_noId__xc_r"))).click();
					report.updateTestLog("Documentary Evidence","No is Selected", Status.PASS);
					sleep(800);	
					
					//**********check box****************\\
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='lifeEvntChckBoxTick']/span"))).click();
					sleep(800);	
					
				}
				
				//*****Click on Continue*******\\
				
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueLifEventId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
				sleep(800);
				

				
				
			 
			sleep(1000);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
	public void HealthandLifeStyle() {

		String Restrictedillness=dataTable.getData("HOSTPLUS_LifeEvent", "Restrictedillness");
		String PriorClaim=dataTable.getData("HOSTPLUS_LifeEvent", "PriorClaim");
		String Diagnosedillness=dataTable.getData("HOSTPLUS_LifeEvent", "Diagnosedillness");
		String DeclinedApplication=dataTable.getData("HOSTPLUS_LifeEvent", "DeclinedApplication");
		
		
		
		
		try{
			if(driver.getPageSource().contains("Eligibility Check")){
				
			WebDriverWait wait=new WebDriverWait(driver,18);
						
  //******Are you restricted, due to illness or injury from carrying out any of the identifiable duties*****\\
			
			
			if(Restrictedillness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-0:0']"))).click();
				report.updateTestLog("Restricted illness", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-0:0']"))).click();
				report.updateTestLog("Restricted illness", "No is Selected", Status.PASS);
			}
			sleep(800);
			
//******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits*****\\
			
			
			if(PriorClaim.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-1:0']"))).click();
				report.updateTestLog("Prior Claim", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-1:0']"))).click();
				report.updateTestLog("Prior Claim", "No is Selected", Status.PASS);
			}
			sleep(800);
			
//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\
			
			
			if(Diagnosedillness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-2:0']"))).click();
				report.updateTestLog("Diagnosed illness", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-2:0']"))).click();
				report.updateTestLog("Diagnosed illness", "No is Selected", Status.PASS);
			}
			sleep(800);
			
//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\
			
			
			if(DeclinedApplication.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-3:0']"))).click();
				report.updateTestLog("Declined Application", "Yes is Selected", Status.PASS);
				
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-1-3:0']"))).click();
				report.updateTestLog("Declined Application", "No is Selected", Status.PASS);
			}
			sleep(800);
			
            //****Click on Continue****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='applyBtnId']/span"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(2000);
			
			
			}			
		}
	
		catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void confirmation() {
		
		WebDriverWait wait=new WebDriverWait(driver,20);	
			
			try{
				if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){
					
				
				

				
			//******Agree to general Consent*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(500);
				
				
			//******Click on Submit*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
				report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
				sleep(1000);			
				
			}
			
			//*******Feedback popup******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1000);
			
			if(driver.getPageSource().contains("Application number")){
				
			
			//*****Fetching the Application Status*****\\
			
			String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(500);
			
			//*****Fetching the Application Number******\\
			
			String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
			sleep(1000);
			
			
			
			//******Download the PDF***********\\
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
				sleep(1500);
	  		}
			
			
			//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
			
			
			
		  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		  			sleep(1500);
		  			Robot roboobj=new Robot();
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  		}
		  		
		  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		  			Robot roboobj=new Robot();
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='underline']/strong"))).click();
		  				
		  			sleep(4000);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  			
		  		}
			
			report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
			
			sleep(1000);
		
			 
			  report.updateTestLog("Confirmation Page", "Application No: "+App,Status.PASS);
			
				
			 report.updateTestLog("Confirmation Page", "Application Status: "+Appstatus,Status.PASS);	
			}

			else{
				 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
			}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
	}

		
	public void LifeEventHOSTPLUSNegativeFlow() {

		String EmailId = dataTable.getData("HOSTPLUS_LifeEvent", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_LifeEvent", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TimeofContact");		
		String RegularIncome = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_LifeEvent", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_LifeEvent", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_LifeEvent", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_LifeEvent", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_LifeEvent", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_LifeEvent", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_LifeEvent", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_LifeEvent", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent_Date");
		
		String Negative_Scenario=dataTable.getData("HOSTPLUS_LifeEvent", "Negative_Scenario");

		
		
		try{
			
			
			
			//****Click on Life Event Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Increase your cover due to a life event')]"))).click();
			
			report.updateTestLog("Transfer Your Cover", "Transfer Your Cover Link is Clicked", Status.PASS);
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\	
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(200);

			//*****Click on the Contact Number Type****\\			
				
				if(!TypeofContact.equalsIgnoreCase("Mobile")){			
							
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Mobile')]"))).click();
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();			
				report.updateTestLog("Contact Type", "Preferred contact: "+TypeofContact+" is Selected", Status.PASS);
				sleep(500);
				}
				
				
				//****Enter the Contact Number****\\			
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(ContactNumber);
				sleep(500);
				report.updateTestLog("Contact NUmber", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mrngRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("aftrNoonRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				
							
				//****Contact Details-Continue Button*****\\			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueContDetlID']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				sleep(1200);			
							
				
					
					//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\		
					
					if(RegularIncome.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
							
					}		
					else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("regIncome_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
							
					}	
					
					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_yesId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("identifiableDuties_noId__xc_r"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_yesId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
							sleep(1000);
						}
						else{
							
							wait.until(ExpectedConditions.elementToBeClickable(By.id("limitation_noId__xc_r"))).click();
							report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
							sleep(1000);
						}
					}
					
					//*****Resident of Australia****\\	
					
					if(Citizen.equalsIgnoreCase("Yes")){		
						driver.findElement(By.id("valCitizenAus_yes_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						driver.findElement(By.id("valCitizenAus_no_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}	
							
					//*****Industry********\\		
				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='industryId']/span)[1]"))).click();
					sleep(800);				
					driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();		
					sleep(800);		
					report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
					sleep(800);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),' What is your occupation?')]"))).click();
					sleep(800);
					
					//*****Occupation*****\\		
					
					driver.findElement(By.xpath("(//button[@data-id='occupationId']/span)[1]")).click();
					//wait.until(ExpectedConditions.elementToBeClickable(By.id("(//button[@data-id='occupationId']/span)[2]"))).click();	
		            sleep(800);							
					driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();		
					sleep(800);	
					report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
					sleep(500);
							
							
					//*****Other Occupation *****\\	
					
					if(OccupationType.equalsIgnoreCase("Other")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupationId"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);	
					}		
					
					
					//*****Extra Questions*****\\							
					
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
				   
				    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
											
				}							
				else{							

					wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
											
				}							
											
				      //*****Do You Hold a Tertiary Qualification******\\							
											
				        if(TertiaryQual.equalsIgnoreCase("Yes")){							
				       
				        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}							
				   else{							
				       
					   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}							
				}							
											
											
											
				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
											
											
											
				//******Do you work in hazardous environment*****\\		
					
				if(HazardousEnv.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_yes_id__xc_r"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_no_id__xc_"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}							
											
				//******Do you spend 10% time working outside office*******\\	
				
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_yes_id__xc_r"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_no_id__xc_"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}							
				}							
							
				    
                  sleep(800);
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				    	//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
					   
					    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
												
					}							
					else{							

						wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
												
					}							
												
					      //*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){							
					       
					        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}						
				}		
				    
											
				sleep(1000);
				
				//*****Click on Continue*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occuContinueId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				

				//*****Click on Continue*******\\
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occuContinueId']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				sleep(800);
				
				//********************************************************************************************************\\
				//****************************LIFE EVENT SECTION***********************************************************\\
				//*********************************************************************************************************\\
				
				
				//*****Please select the specific life event you are applying under to increase your cover*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Please Select your Life event')])[1]"))).click();
				sleep(800);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+LifeEvent+"')]"))).click();
				report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);
				sleep(800);
				
             

			   			
                    if(Negative_Scenario.equalsIgnoreCase("DateError")){
                    	
                    	//*****What is the date of the life event?*******\\		
                    	driver.findElement(By.name("lifeeventdateId")).sendKeys(LifeEvent_Date);
						sleep(1000);
						driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
						report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
						sleep(800);
						
						if(driver.getPageSource().contains("You are not eligible for an increase in cover due to life event. Application must be made within six months of the life event occurring.")){
							String dateerror="You are not eligible for an increase in cover due to life event. Application must be made within six months of the life event occurring.";
							report.updateTestLog("Life Event Date Error Message", dateerror+" is Displayed", Status.PASS);
						}
						
						else{
							report.updateTestLog("Life Event Date Error Message", "Error Message is not Displayed", Status.FAIL);
						}
                    }
						
                    else if(Negative_Scenario.equalsIgnoreCase("PriorEventError")){
                    	

                    	//*****What is the date of the life event?*******\\		
                    	driver.findElement(By.name("lifeeventdateId")).sendKeys(LifeEvent_Date);
						sleep(500);
						driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
						report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
						sleep(800);
                    	
                    	//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months 
        				//within this calendar year?********************\\
                    	
                    	wait.until(ExpectedConditions.elementToBeClickable(By.id("lifeevntincrease_yesId__xc_r"))).click();
    					report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
    					sleep(800);	
    					
    					String Lifeeventerror=driver.findElement(By.id("lifeevntincreaseErrMsg")).getText();
    					sleep(500);
    					if(Lifeeventerror.equalsIgnoreCase("You are not eligible for an increase in cover due to life event. There is a limit of one life event increase within this calendar year.")){
    						report.updateTestLog("Life Event Error Message",Lifeeventerror+" is Displayed", Status.PASS);
    					}
    					
    					else{
							report.updateTestLog("Life Event Error Message", "Error Message is not Displayed", Status.FAIL);
						}
                    }
				
				
				
			
				
				
				
			sleep(1000);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}	
	
	
	}





