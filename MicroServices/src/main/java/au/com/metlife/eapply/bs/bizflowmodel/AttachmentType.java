package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentType")

@XmlRootElement
public class AttachmentType {
	
	private String DocumentURI;
	private String AttachmentTypeCode;
	private String Description;

	public String getAttachmentTypeCode() {
		return AttachmentTypeCode;
	}

	public void setAttachmentTypeCode(String attachmentTypeCode) {
		AttachmentTypeCode = attachmentTypeCode;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getDocumentURI() {
		return DocumentURI;
	}

	public void setDocumentURI(String documentURI) {
		DocumentURI = documentURI;
	}

	

	
	
}
