(function (angular) {
	'use strict';
	/* global channel, inputData, token, moment */
	/**
	 * @ngdoc controller
	 * @name claimTrackermain.controller:loginCtrl
	 *
	 * @description
	 * This is loginCtrl description
	 */
	/*var guildApp=angular.module('guildApp', ['ngRoute','ngResource','vcRecaptcha','googleAnalytics']);*/
	angular.module('guildApp').controller('loginCtrl', ['$scope', '$location',
		function ($scope, $location) {
		$scope.redirect=function(path){
		 $location.path(path);
		};
	}]);
})(angular);
