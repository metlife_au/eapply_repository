package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the REF_OCCMAPPING database table.
 * 
 */
@Entity
@Table(name = "REF_OCCMAPPING", schema="EAPPDB")
public class RefOccmapping implements Serializable {
	private static final long serialVersionUID = 1L;
	
      @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private int id;
	 
	@Column(name="OCCNAME", nullable=false, length=250)
	private String occName;
	
	@Column(name="FUNDID", nullable=false, length=250)
	private String fundId;
	 
	@Column(name="DEATHFIXEDCATEGEORY", length=250)
	private String deathfixedcategeory;
	
	@Column(name="DEATHUNITCATEGEORY", length=250)
	private String deathunitcategeory;

	

	@Column(name="IPFIXEDCATEGEORY", length=250)
	private String ipfixedcategeory;
	
	@Column(name="IPUNITCATEGEORY", length=250)
	private String ipunitcategeory;

	

	@Column(name="SPECIALRISK",  length=250)
	private String specialrisk;

	@Column(name="TPDFIXEDCATEGEORY", length=250)
	private String tpdfixedcategeory;

	@Column(name="TPDUNITCATEGEORY", length=250)
	private String tpdunitcategeory;

	public RefOccmapping() {
	}
	public RefOccmapping(String occname,String fundid, String deathfixedcategeory, String deathunitcategeory, String ipfixedcategeory, String ipunitcategeory,String specialrisk,String tpdfixedcategeory,String tpdunitcategeory) {
		this.fundId = fundid;
		this.occName = occname;
		this.deathfixedcategeory = deathfixedcategeory;
		this.deathunitcategeory = deathunitcategeory;
		this.ipfixedcategeory = ipfixedcategeory;
		this.ipunitcategeory=ipunitcategeory;
		this.specialrisk=specialrisk;
		this.tpdfixedcategeory=tpdfixedcategeory;
		this.tpdunitcategeory=tpdunitcategeory;
		
	}
	public String getDeathfixedcategeory() {
		return this.deathfixedcategeory;
	}

	public void setDeathfixedcategeory(String deathfixedcategeory) {
		this.deathfixedcategeory = deathfixedcategeory;
	}

	public String getDeathunitcategeory() {
		return this.deathunitcategeory;
	}

	public void setDeathunitcategeory(String deathunitcategeory) {
		this.deathunitcategeory = deathunitcategeory;
	}

	public String getFundid() {
		return this.fundId;
	}

	public void setFundid(String fundId) {
		this.fundId = fundId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIpfixedcategeory() {
		return this.ipfixedcategeory;
	}

	public void setIpfixedcategeory(String ipfixedcategeory) {
		this.ipfixedcategeory = ipfixedcategeory;
	}

	public String getIpunitcategeory() {
		return this.ipunitcategeory;
	}

	public void setIpunitcategeory(String ipunitcategeory) {
		this.ipunitcategeory = ipunitcategeory;
	}

	public String getOccname() {
		return this.occName;
	}

	public void setOccname(String occName) {
		this.occName = occName;
	}

	public String getSpecialrisk() {
		return this.specialrisk;
	}

	public void setSpecialrisk(String specialrisk) {
		this.specialrisk = specialrisk;
	}

	public String getTpdfixedcategeory() {
		return this.tpdfixedcategeory;
	}

	public void setTpdfixedcategeory(String tpdfixedcategeory) {
		this.tpdfixedcategeory = tpdfixedcategeory;
	}

	public String getTpdunitcategeory() {
		return this.tpdunitcategeory;
	}

	public void setTpdunitcategeory(String tpdunitcategeory) {
		this.tpdunitcategeory = tpdunitcategeory;
	}

}