package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRQuotePageObjects extends PageObjectLibrary{
		
		public static final By btnCalcQuote = getObjectIdentifaction("ybr.webresponse.quote.btn.css");
		public static final By btnProceed= getObjectIdentifaction("ybr.webresponse.protect.btn.css");

}
