package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPGetQuotePageObjects extends PageObjectLibrary{
		
	public static final By txtFirstName = getObjectIdentifaction("YBRIP.yourquote.firstname.text.id");
//	public static final By txtLastName = getObjectIdentifaction("stft.getquote.lastname.text.id");
	public static final By txtDOB = getObjectIdentifaction("YBRIP.yourquote.dob.text.id");
	public static final By rdMale = getObjectIdentifaction("YBRIP.yourquote.gender.radio.css");
	public static final By rdFemale = getObjectIdentifaction("YBRIP.yourquote.gender.Female.radio.css");
//	public static final By rdSmokedIn12MthsYes = getObjectIdentifaction("stft.getquote.smokedin12months.yes.radio.id");
	public static final By rdSmokedIn12MthsNo = getObjectIdentifaction("YBRIP.yourquote.smoker.radio.css");
	public static final By rdCitizenYes = getObjectIdentifaction("YBRIP.yourquote.citizen.radio.css");
//	public static final By rdCitizenNo = getObjectIdentifaction("stft.getquote.citizen.no.radio.id");
	public static final By selIndustry = getObjectIdentifaction("YBRIP.yourquote.industry.dropdown.css");
	public static final By selOccupation = getObjectIdentifaction("YBRIP.yourquote.occupation.dropdown.css");
	
	public static final By txtAnnualsalary = getObjectIdentifaction("YBRIP.yourquote.annualsalary.text.id");	
	public static final By selemploymentStaus= getObjectIdentifaction("YBRIP.yourquote.employmentstatus.radio.css");	
	
	public static final By rdConsultant= getObjectIdentifaction("YBRIP.yourquote.consultant.radio.css");
	public static final By btnCalculateQuote = getObjectIdentifaction("YBRIP.yourquote.calculatequote.btn.css");
	public static final By btnApply = getObjectIdentifaction("YBRIP.yourquote.apply.btn.css");
}
