package vicsSuper_Objects;

import org.openqa.selenium.By;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class TransferCoverObjects extends ReusableLibrary {

	public TransferCoverObjects(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	// ************************************************************************************************************************
		// -----------------------------------------Personal Details
		// ************************************************************************************************************************
		public static By obj_PersonalDetails_dutyOfDisclosure = By.xpath(".//input[@name='dod']/parent::*");
		public static By obj_PersonalDetails_privacyStatement = By.xpath(".//input[@name='privacy']/parent::*");
		// ContactDetails
		public static By obj_PersonalDetails_email = By.name("coverDetailsTransferEmail");
		public static By obj_PersonalDetails_contactType = By.xpath("//select[@ng-model='preferredContactType']");
		public static By obj_PersonalDetails_phNO = By.id("preferrednumCCTransId");
		public static By obj_PersonalDetails_Morning = By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*");
		public static By obj_PersonalDetails_Afternoon = By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*");
		public static By obj_PersonalDetails_Male = By.xpath("(.//input[@name='gender']/parent::*)[1]");
		public static By obj_PersonalDetails_Female = By.xpath("(.//input[@name='gender']/parent::*)[2]");
		public static By obj_PersonalDetails_ContinueContactDetails = By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']");
		// OccupationDetails
		public static By obj_PersonalDetails_FourteenHrsYes = By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]");
		public static By obj_PersonalDetails_FourteenHrsNo = By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]");
		public static By obj_PersonalDetails_CitizenYes = By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]");
		public static By obj_PersonalDetails_CitizenNo = By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]");
		public static By obj_PersonalDetails_AddQuest1aYes = By.xpath("(//input[@name='occRating1a']/parent::*)[1]");
		public static By obj_PersonalDetails_AddQuest1aNo = By.xpath("(//input[@name='occRating1a']/parent::*)[2]");
		public static By obj_PersonalDetails_AddQuest1bYes = By.xpath("(//input[@name='occRating1b']/parent::*)[1]");
		public static By obj_PersonalDetails_AddQuest1bNo = By.xpath("(//input[@name='occRating1b']/parent::*)[2]");
		public static By obj_PersonalDetails_AddQuest2Yes = By.xpath("(//input[@name='occgovrating2']/parent::*)[1]");
		public static By obj_PersonalDetails_AddQuest2No = By.xpath("(//input[@name='occgovrating2']/parent::*)[2]");
		public static By obj_PersonalDetails_AnnualSal = By.id("transferAnnualSal");
		public static By obj_PersonalDetails_ContinueOccupation = By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']");

		// TransferCover
		public static By obj_PersonalDetails_PreviousFundNane = By.name("previousFundName");
		public static By obj_PersonalDetails_MembershipNo = By.name("membershipNumber");
		public static By obj_PersonalDetails_USINo = By.name("spinNumber");
		public static By obj_PersonalDetails_AttachmentPath = By.id("ngf-transferCvrFileUploadAddIdBtn");
		public static By obj_PersonalDetails_Add = By.xpath("//*[@id='transferCvrFileUploadAddId']");
		public static By obj_PersonalDetails_FileSizeErr = By.xpath("//label[text()='  File size should not be more than 10MB ']");
		public static By obj_PersonalDetails_ContinueTransferCover = By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']");




		// CoverDetails (Indexation Not Handled)
		public static By obj_PersonalDetails_PremiumFrequency = By.id("coverId");
		public static By obj_PersonalDetails_TransferToExistingDeath = By.xpath("(.//input[@name='addtionalDeathTransfer']/parent::*)[1]");
		public static By obj_PersonalDetails_ReplaceExistingDeath = By.xpath("(.//input[@name='addtionalDeathTransfer']/parent::*)[2]");
		public static By obj_PersonalDetails_TransferToExistingTPD = By.xpath("(.//input[@name='addtionalTPDTransfer']/parent::*)[1]");
		public static By obj_PersonalDetails_ReplaceExistingTPD = By.xpath("(.//input[@name='addtionalTPDTransfer']/parent::*)[2]");
		public static By obj_PersonalDetails_DeathCover = By.id("dcTransferAmount");
		public static By obj_PersonalDetails_DeathMAX = By.xpath("//span[@ng-if='maxDeathErrorFlag']");
		public static By obj_PersonalDetails_TPDCover = By.id("tpdTransferAmount");
		public static By obj_PersonalDetails_TPDMAX = By.xpath("//span[@ng-if='maxTpdErrorFlag']");
		public static By obj_PersonalDetails_TPDCoverGeraterThanDeath = By.xpath("//*[@ng-if='maxTotalTPDAmt']");
		public static By obj_PersonalDetails_IPCover = By.id("ipTransferAmount");
		public static By obj_PersonalDetails_IPMax = By.xpath(".//label[text()='The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum']");
		public static By obj_PersonalDetails_WaitingPeriod = By.xpath("//select[@ng-model='waitingPeriodTransPer']");
		public static By obj_PersonalDetails_BenefitPeriod = By.xpath("//select[@ng-model='benefitPeriodTransPer']");





		//Premium Amount

		//*[text()='the member is not a casual or contractor employee ']
			public static By obj_PersonalDetails_DeathAmount = By.xpath("(//*[@id='death']//h4)");
			public static By obj_PersonalDetails_DeathCost = By.xpath("(//*[@id='death']//h4)[2]");
			public static By obj_PersonalDetails_TPDAmount = By.xpath("(//*[@id='tpd']//h4)");
			public static By obj_PersonalDetails_TPDCost = By.xpath("(//*[@id='tpd']//h4)[2]");
			public static By obj_PersonalDetails_IPAmount = By.xpath("(//*[@id='sc']//h4)");
			public static By obj_PersonalDetails_IPCost = By.xpath("(//*[@id='sc']//h4)[2]");
			public static By obj_PersonalDetails_TotalCostYearly = By.xpath("//*[text()=' Total Yearly cost : ']/following-sibling::h4");
			public static By obj_PersonalDetails_TotalCostMonthly = By.xpath("//*[text()=' Total Monthly cost : ']/following-sibling::h4");
			public static By obj_PersonalDetails_TotalCostWeekly = By.xpath("//*[text()=' Total Weekly cost : ']/following-sibling::h4");

		//Footer
		public static By obj_PersonalDetails_Calculate = By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']");
		public static By obj_PersonalDetails_Save = By.xpath("(//button[@ng-click='saveQuoteTransfer()'])[1]");
		public static By obj_PersonalDetails_Continue = By.xpath("//button[@ng-click='goToAura()']");

		public static By obj_SaveReferenceNo = By.xpath("//*[contains(text(), 'Your application reference no')]");
		public static By obj_SaveClose = By.xpath("//button[@ng-click='closeThisDialog()']");
		public static By obj_PersonalDetails_OpenSavedApplication = By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']");
		public static By obj_PersonalDetails_CancelMsg = By.id("decCancelMsg_text");
		public static By obj_PersonalDetails_DecisionContinue = By.xpath("//button[@ng-click='goTo()']");


		// ************************************************************************************************************************
			// -----------------------------------------Aura
			// ************************************************************************************************************************
		public static By obj_Aura_RestrictedIllnessYes = By.xpath("(.//input[@id='option1']/parent::*)[1]");
		public static By obj_Aura_RestrictedIllnessNo = By.xpath("(.//input[@id='option2']/parent::*)[1]");
		public static By obj_Aura_PriorClaimYes = By.xpath("(.//input[@id='option1']/parent::*)[2]");
		public static By obj_Aura_PriorClaimNo = By.xpath("(.//input[@id='option2']/parent::*)[2]");
		public static By obj_Aura_RestrictedFromWorkYes = By.xpath("(.//input[@id='option1']/parent::*)[3]");
		public static By obj_Aura_RestrictedFromWorkNo = By.xpath("(.//input[@id='option2']/parent::*)[3]");
		public static By obj_Aura_DiagnosedIllnessYes = By.xpath("(.//input[@id='option1']/parent::*)[4]");
		public static By obj_Aura_DiagnosedIllnessNo = By.xpath("(.//input[@id='option2']/parent::*)[4] ");
		public static By obj_Aura_MedicalTreatmentYes = By.xpath("(.//input[@id='option1']/parent::*)[5]");
		public static By obj_Aura_MedicalTreatmentNo = By.xpath("(.//input[@id='option2']/parent::*)[5]");
		public static By obj_Aura_DeclinedApplicationYes = By.xpath("(.//input[@id='option1']/parent::*)[6]");
		public static By obj_DeclinedApplicationNo = By.xpath("(.//input[@id='option2']/parent::*)[6]");
		public static By obj_Aura_FundpremloadingYes = By.xpath("(.//input[@id='option1']/parent::*)[7]");
		public static By obj_Aura_loadingdetails = By.xpath("//*[@ng-model='freeTextArea']");
		public static By obj_Aura_Enter = By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']");
		public static By obj_Aura_FundpremloadingNo = By.xpath("(.//input[@id='option2']/parent::*)[7]");
		//Footer
		public static By obj_Aura_Continue = By.xpath("//*[@ng-click='proceedNext()']");
		public static By obj_Aura_Save = By.xpath("//*[@ng-click='saveAuraTransfer()']");
		public static By obj_SaveClose_Aura = By.xpath("//button[@ng-click='preCloseCallback()']");


		// ************************************************************************************************************************
		// -----------------------------------------Confirmation Details
		// ************************************************************************************************************************

		// Personal Details
		public static By obj_Confirmation_FirstName = By.xpath("//*[text()='First name']//following::label");
		public static By obj_Confirmation_LastName = By.xpath("//*[text()='Last name']//following::label");
		public static By obj_Confirmation_Gender = By.xpath("//*[text()='Gender']//following::label");
		public static By obj_Confirmation_ContactType = By.xpath("//*[text()='Preferred contact type']//following::label");
		public static By obj_Confirmation_ContactNo = By.xpath("//*[text()='Preferred contact number ']//following::label");
		public static By obj_Confirmation_Email = By.xpath("//*[text()='Email address']//following::label");
		public static By obj_Confirmation_DOB = By.xpath("//*[text()='Date of birth']//following::label");
		public static By obj_Confirmation_Time = By.xpath("//*[text()='What time of day do you prefer to be contacted?']//following::label");
		public static By obj_Confirmation_HoursWork = By.xpath("//*[text()='Do you work 14 or more hours per week?']//following::label");
		public static By obj_Confirmation_Citizen = By.xpath("//*[contains(text(),'Are you a citizen or permanent resident of Australia')]//following::label");
		public static By obj_Confirmation_Industry = By.xpath("//*[text()='What industry do you work in?']//following::label");
		public static By obj_Confirmation_Occupation = By.xpath("//*[text()='What is your current occupation?']//following::label");
		public static By obj_Confirmation_AddQuest1a = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[1]");
		public static By obj_Confirmation_AddQuest1b = By.xpath("(//*[text()='Are the duties of your regular occupation limited to : ']//following::label)[3]");
		public static By obj_Confirmation_TertiaryQualification = By.xpath("//*[text()='Do you:  ']//following::label");
		public static By obj_Confirmation_AnnualIncome = By.xpath("//*[text()='Annual income']//following::label");

		/*// Cover Details
		public static By obj_Confirmation_Death_ExistingCover = By.xpath("(//*[text()='Existing Cover'])[1]//following::p");
		public static By obj_Confirmation_Death_TransferCover = By.xpath("(//*[text()='Transfer Cover'])[1]//following::p");
		public static By obj_Confirmation_Death_OccupationalCategory = By.xpath("(//*[text()='Occupational Category'])[1]//following::p");
		public static By obj_Confirmation_Death_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[1]//following::p");
		public static By obj_Confirmation_TPD_ExistingCover = By.xpath("(//*[text()='Existing Cover'])[2]//following::p");
		public static By obj_Confirmation_TPD_TransferCover = By.xpath("(//*[text()='Transfer Cover'])[2]//following::p");
		public static By obj_Confirmation_TPD_OccupationalCategory = By.xpath("(//*[text()='Occupational Category'])[2]//following::p");
		public static By obj_Confirmation_TPD_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[2]//following::p");
		public static By obj_Confirmation_IP_ExistingCover = By.xpath("(//*[text()='Existing Cover'])[3]//following::p");
		//public static By obj_Confirmation_IP_NewCover = By.xpath("(//*[text()='Transfer Cover'])[3]//following::p");
		public static By obj_Confirmation_IP_OccupationalCategory = By.xpath("(//*[text()='Occupational Category'])[3]//following::p");
		public static By obj_Confirmation_IP_WaitingPeriod = By.xpath("//*[text()='Waiting Period']//following::p");
		public static By obj_Confirmation_IP_BenefitPeriod = By.xpath("//*[text()='Benefit Period']//following::p");
		public static By obj_Confirmation_IP_WeeklyCost = By.xpath("//*[text()=' Total Weekly cost']//following::h4");
		public static By obj_Confirmation_TotalWeeklyCost = By.xpath("//*[contains(text(), 'Total Weekly cost')]//following::h4");

		*/
		// Cover Details
			public static By obj_Confirmation_Death_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[1]//following::p");
			public static By obj_Confirmation_Death_TransferCover = By.xpath("(//*[text()='Transfer sum insured'])[1]//following::p");
			public static By obj_Confirmation_Death_OccupationalCategory = By.xpath("(//*[text()='Occupational category'])[1]//following::p");
			public static By obj_Confirmation_Death_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[1]//following::p");
			public static By obj_Confirmation_TPD_ExistingCover = By.xpath("(//*[text()='Existing sum insured'])[2]//following::p");
			public static By obj_Confirmation_TPD_TransferCover = By.xpath("(//*[text()='Transfer sum insured'])[2]//following::p");
			public static By obj_Confirmation_TPD_OccupationalCategory = By.xpath("(//*[text()='Occupational category'])[2]//following::p");
			public static By obj_Confirmation_TPD_WeeklyCost = By.xpath("(//*[text()='Weekly cost'])[2]//following::p");
			public static By obj_Confirmation_IP_ExistingCover = By.xpath("(//*[text()='Transfer sum insured'])[3]//following::p");
			//public static By obj_Confirmation_IP_NewCover = By.xpath("(//*[text()='Transfer Cover'])[3]//following::p");
			public static By obj_Confirmation_IP_OccupationalCategory = By.xpath("(//*[text()='Occupational category'])[3]//following::p");
			public static By obj_Confirmation_IP_WaitingPeriod = By.xpath("//*[text()='Waiting period']//following::p");
			public static By obj_Confirmation_IP_BenefitPeriod = By.xpath("//*[text()='Benefit period']//following::p");
			public static By obj_Confirmation_IP_WeeklyCost = By.xpath("//*[text()=' Total Weekly cost']//following::h4");
			public static By obj_Confirmation_TotalWeeklyCost = By.xpath("//*[contains(text(), 'Total Weekly cost')]//following::h4");



	/*	//TransferCover
		public static By obj_Confirmation_Previousfund = By.xpath("//*[contains(text(), 'Name of Previous fund or insurer')]//following::label");
		public static By obj_Confirmation_PolicyNo= By.xpath("//*[contains(text(), 'Fund member or insurance policy number')]//following::label");
		public static By obj_Confirmation_USINo = By.xpath("//*[contains(text(), 'USI Number')]//following::label");
		public static By obj_Confirmation_PremiumPeriod = By.xpath("//*[contains(text(), 'Premium Period')]//following::label");

		*/
			//TransferCover
			public static By obj_Confirmation_Previousfund = By.xpath("//*[contains(text(), 'Name of previous fund or insurer')]//following::label");
			public static By obj_Confirmation_PolicyNo= By.xpath("//*[contains(text(), 'Fund member or insurance policy number')]//following::label");
			public static By obj_Confirmation_USINo = By.xpath("//*[contains(text(), 'USI number')]//following::label");
			public static By obj_Confirmation_PremiumPeriod = By.xpath("//*[contains(text(), 'Premium period')]//following::label");


		public static By obj_Confirmation_TermsConditionsCheckBox = By.xpath(".//input[@id='option2']/parent::*");
		public static By obj_DecisionPopUP_Continue = By.xpath("//*[@ng-click='goTo()']");
		public static By obj_DecisionPopUP_No = By.xpath("//button[text()='No']");
		public static By obj_DecisionPopUP_Appstatus = By.xpath("//a[@class='noarrow']/h4");
		public static By obj_DecisionPopUP_AppMsg = By.xpath("//*[@id='collapseOne']/div/div/div/p");
		public static By obj_DecisionPopUP_App = By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p");
		public static By obj_Confirmation_Download = By.xpath("//strong[text()='Download ']");

		//Footer
		public static By obj_Confirmation_Submit = By.xpath("//*[@ng-click='submitTransferCover()']");
		public static By obj_Confirmation_Save = By.xpath("//*[@ng-click='saveTransferSummary()']");
		public static By obj__Confirmation_SaveClose = By.xpath("//button[@ng-click='preCloseCallback()']");

}
