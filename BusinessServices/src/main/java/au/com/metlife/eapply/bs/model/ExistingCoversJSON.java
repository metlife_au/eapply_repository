package au.com.metlife.eapply.bs.model;

import java.util.List;

public class ExistingCoversJSON {
	
	private List<CoverJSON> cover;

	public List<CoverJSON> getCover() {
		return cover;
	}

	public void setCover(List<CoverJSON> cover) {
		this.cover = cover;
	}
}
