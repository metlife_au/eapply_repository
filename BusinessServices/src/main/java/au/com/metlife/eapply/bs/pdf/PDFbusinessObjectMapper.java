/* --------------------------------------------------------------------------------------------------------------------------- 
 * Description: EappRulesCacheSerlvet class is the backing bean for UC016- Cover Details
 * ---------------------------------------------------------------------------------------------------------------------------
 * Copyright � 2009 Metlife, Inc. 
 * ---------------------------------------------------------------------------------------------------------------------------
 * Author :     Ravi Reddy
 * Created:     17/7/2009
 * ---------------------------------------------------------------------------------------------------------------------------
 * Modification Log:	mm/dd/yyyy
 * ---------------------------------------------------------------------------------------------------------------------------
 * Date				Author					Description         
 * 7/16/2012	   Pushkar Vashishtha       Setting New questions in MemberDetails for YBR PDF
 * 7/17/2012	   Pushkar Vashishtha       Setting IndustryValue instead of IndustryCode
 * {Please place your information at the top of the list}
 * ---------------------------------------------------------------------------------------------------------------------------
 */
package au.com.metlife.eapply.bs.pdf;




import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;



import au.com.metlife.eapply.bs.constants.EapplicationPdfConstants;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
//import com.metlife.common.utility.CoverRankComparator;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;

import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureStatement;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberProductDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.pdf.pdfObject.PersonalDetail;
import au.com.metlife.eapply.bs.utility.CommonPDFHelper;

public class PDFbusinessObjectMapper {
	
	 public final static String PACKAGE_NAME = "com.metlife.eapply.pdf";
	 public final static String CLASS_NAME = "PDFbusinessObjectMapper";
	 
	 
	 @Value("${spring.pdfproperty}")
		private  String pdfproperty;
		
		
	    
	    public String getPdfproperty() {
			return pdfproperty;
		}

		public void setPdfproperty(String pdfproperty) {
			this.pdfproperty = pdfproperty;
		}

	 
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public PDFObject getPdfOjectsForEapply(Eapplication applicationDTO, EapplyInput eapplyInput, String lob, String propertyLoc) throws IOException{
		PDFObject pdfObject = new PDFObject();
		String country = null;	
		MemberProductDetails memberProductDetails = null;
		ArrayList<MemberProductDetails> memberProductDtsList = null;	
		MemberDetails memberDetails = null;
		Applicant applicantDTO = null;
		Cover coversDTO = null;
		Double cost = 0.00;
		StringBuffer alldecreasons = new StringBuffer();
		java.util.Properties property = new java.util.Properties();
		String PdfPropertyFileLoc = null;
        File file = null;
        InputStream url = null;
        Boolean specialCondition = Boolean.FALSE;
        String mixedRUW = null;        
		ArrayList<PersonalDetail> personalDtslist = new ArrayList<PersonalDetail>();
		DisclosureStatement disclosureStatement = null;
		List<DisclosureStatement> disclosureStatementList = new ArrayList<DisclosureStatement>();
		if(null!=applicationDTO){
			file = new File( propertyLoc);
			System.out.println("propertyLoc>>>"+propertyLoc);
	         url = new FileInputStream( file);    
	        property.load( url);
			memberDetails = new MemberDetails();
			System.out.println("url>>>"+url);
			if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob)){
				memberDetails.setBrand("CARE");
			}			
			
			/* if (null != applicationDTO.getFundinfo()
			            && null != applicationDTO.getFundinfo().getExistingLoadings()){
				 memberDetails.setExistingLoading(applicationDTO.getFundinfo().getExistingLoadings());
			 }else{
				 memberDetails.setExistingLoading("None");
			 }
			 if (null != applicationDTO.getFundinfo()
		                && null != applicationDTO.getFundinfo().getExistingExclusion()){
				 memberDetails.setExistingExclusion(applicationDTO.getFundinfo().getExistingExclusion());
			 }else{
				 memberDetails.setExistingExclusion("None");
			 }*/
			 memberDetails.setExistingExclusion("None");
			 if(null!=applicationDTO.getTotalMonthlyPremium()){
				memberDetails.setTotalPremium(CommonPDFHelper.formatAmount(applicationDTO.getTotalMonthlyPremium().toString()));
		     }else{
		    	 memberDetails.setTotalPremium(CommonPDFHelper.formatAmount("0"));
		     }
			 pdfObject.setApplicationDecision(applicationDTO.getAppdecision());
			 
			 pdfObject.setRequestType(applicationDTO.getRequesttype());			 
			// pdfObject.setDecCompany(applicationDTO.getDecCompany()); 
		/*	 pdfObject.setDecPrivacy(applicationDTO.getDecPrivacy());
			 pdfObject.setDecGC(applicationDTO.getDecGC()); 
			 pdfObject.setDecDDS(applicationDTO.getDecDDS());*/ 
			// pdfObject.setDecPrivacyNoAuth(applicationDTO.getDecPrivacyNoAuth());
			 pdfObject.setDecPrivacy(property.getProperty("label_privacy_stmt_pdf"));
			 pdfObject.setDecGC(property.getProperty("label_genral_concent_pdf")); 
			 pdfObject.setDecDDS(property.getProperty("label_duty_disclouser_pdf")); 
			 
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_privacy_stmt_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatementList.add(disclosureStatement);
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_genral_concent_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatementList.add(disclosureStatement);
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatementList.add(disclosureStatement);
			 //END This is a temp fix to stop displaying family question. Code need to change later		
			if(null!=applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0){
				
				for(int itr=0;itr<applicationDTO.getApplicant().size(); itr++) {
					applicantDTO = (Applicant)applicationDTO.getApplicant().get(itr);
					if(null!=applicantDTO && EapplicationPdfConstants.APPLICANT_TYPE_PRIMARY.equalsIgnoreCase(applicantDTO.getApplicanttype())){	
						/*SRKR: 26/04/2016 - Inlcuded the below code to sort the covers in Deat,TPD,Trauma, IP always*/
						//Collections.sort(applicantDTO.getCovers(),new CoverRankComparator());
						if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob) && null!=applicantDTO.getCustomerreferencenumber()){
							memberDetails.setClientEmailId(applicantDTO.getCustomerreferencenumber());
						}
						/*if(null!=applicantDTO.getAuradetails() && "Long".equalsIgnoreCase(applicantDTO.getAuradetails().getFormLength())){
							pdfObject.setFormLength("Long");
						}*/
						pdfObject.setFormLength("Long");
						if(null!=applicantDTO.getAustraliacitizenship() && "yes".equalsIgnoreCase(applicantDTO.getAustraliacitizenship())){
							memberDetails.setAustraliaCitizenship("Yes");
						}else{
							memberDetails.setAustraliaCitizenship("No");
						}
						memberDetails.setOccCategory(eapplyInput.getDeathOccCategory());
						
						memberDetails.setGender(applicantDTO.getGender());
						memberDetails.setMemberFName(applicantDTO.getFirstname());
						memberDetails.setMemberLName(applicantDTO.getLastname());
						memberDetails.setTitle(applicantDTO.getTitle());
						memberDetails.setEmailId(applicantDTO.getEmailid());
						memberDetails.setClientRefNum(applicantDTO.getClientrefnumber());
						memberDetails.setDateJoinedFund(applicantDTO.getDatejoinedfund());
						if("Yes".equalsIgnoreCase(applicationDTO.getClientmatch())){
							memberDetails.setClientMatched(Boolean.TRUE);
						}else{
							memberDetails.setClientMatched(Boolean.FALSE);
						}
						
						//memberDetails.setDateJoinedCompany(dateJoinedCompany);						
						
						
						/*if(null!=applicantDTO.getNominatedadditionalcover() && "Yes".equalsIgnoreCase(applicantDTO.getNominatedadditionalcover())){
							memberDetails.setReplaceCvrOption(Boolean.FALSE);
						}else if(null!=applicantDTO.getNominatedadditionalcover() && "No".equalsIgnoreCase(applicantDTO.getNominatedadditionalcover())){
							memberDetails.setReplaceCvrOption(Boolean.TRUE);
						}*/						
						/*if(null!=applicantDTO.getSmoker() && "Yes".equalsIgnoreCase(applicantDTO.getSmoker())){
							memberDetails.setSmoker("Yes");
						}else if(null!=applicantDTO.getSmoker()){
							memberDetails.setSmoker("No");
						}*/
						// Added by Pushkar
							memberDetails.setMemberOccupation(applicantDTO.getOccupation());
							//memberDetails.setMemberOtherOccupation(memberOtherOccupation);
							memberDetails.setMemberIndustry(applicantDTO.getIndustrytype());
							memberDetails.setEmpStatus(applicantDTO.getPermanentemptype());
							memberDetails.setAnnualSal(applicantDTO.getAnnualsalary());
						
							
							/*if(null!=applicantDTO.getWorkduties()) && applicantDTO.getWorkDutiesManual()){
								memberDetails.setHazardousEnv("Yes");
							}else if(null!=applicantDTO.getWorkDutiesManual() && !applicantDTO.getWorkDutiesManual()){
								memberDetails.setHazardousEnv("No");
							}
							if(null!=applicantDTO.getSpendtimeoutside() && applicantDTO.getSpendTimeOutsideRadioSel()){
								memberDetails.setWorkOutsideOffice("Yes");
							}else if(null!=applicantDTO.getSpendTimeOutsideRadioSel() && !applicantDTO.getSpendTimeOutsideRadioSel()){
								memberDetails.setWorkOutsideOffice("No");
							}*/
						
						/*if(null!=applicantDTO.getSmoker() && "Yes".equalsIgnoreCase(applicantDTO.getSmoker())){
							memberDetails.setSmoker("Yes");
						}else if(null!=applicantDTO.getSmoker()){
							memberDetails.setSmoker("No");
						}*/
						
						StringBuilder addressBuilder = new StringBuilder();			
						if (null != eapplyInput && null!=eapplyInput.getAddress()) {
			                if (null != eapplyInput.getAddress().getCountry()) {
			                    country = eapplyInput.getAddress().getCountry();
			                } /*else {
			                    country = "Australia";
			                }*/	
			                memberDetails.setPrefferedContactNumber(eapplyInput.getContactDetails().getPrefContact());
			                if (null != eapplyInput.getAddress().getLine1()
			                		&& eapplyInput.getAddress().getLine1().trim().length()>0){
			                	addressBuilder.append(eapplyInput.getAddress().getLine1() + " ");
			                }	                
			                	
			                if (null != eapplyInput.getAddress().getLine2()
			                		&& eapplyInput.getAddress().getLine2().trim().length()>0){
			                	addressBuilder.append( eapplyInput.getAddress().getLine2() + ", ");			                	
			                }
			                if (null !=  eapplyInput.getAddress().getSuburb()
			                		&&  eapplyInput.getAddress().getSuburb().trim().length()>0){
			                	memberDetails.setSubrub(eapplyInput.getAddress().getSuburb());
			                	addressBuilder.append( eapplyInput.getAddress().getSuburb() + ", ");			                	
			                }
			                if (null != eapplyInput.getAddress().getState()
			                		&& eapplyInput.getAddress().getState().trim().length()>0){
			                	memberDetails.setState(eapplyInput.getAddress().getState());
			                	addressBuilder.append( eapplyInput.getAddress().getState() + ", ");			                	
			                }
			                if (null != country){
			                	memberDetails.setCountry(country);
			                	addressBuilder.append( country + ", ");			                	
			                }
			                	
			                if (null != eapplyInput.getAddress().getPostCode()){
			                	memberDetails.setPostCode(eapplyInput.getAddress().getPostCode());
			                	addressBuilder.append( eapplyInput.getAddress().getPostCode());
			                }
			            } else {
			            	addressBuilder.append( "");
			            }
						if(null!=applicantDTO.getBirthdate()){
							memberDetails.setDob(CommonPDFHelper.getDateInString(applicantDTO.getBirthdate()));
						}			
						if(null!=eapplyInput.getContactDetails()){
							if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContactTime())){
								memberDetails.setPrefferedContactTime("9am � 12pm");
							}else{
								memberDetails.setPrefferedContactTime("12pm � 6pm");
							}
							if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getMobilePhone());
								memberDetails.setPrefferedContactType("Mobile Phone");
							}else if("2".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getHomePhone());
								memberDetails.setPrefferedContactType("Home Phone");
							}else if("3".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getWorkPhone());
								memberDetails.setPrefferedContactType("Work Phone");
							}else{
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getEmailAddress());
								memberDetails.setPrefferedContactType("Email");
							}
						}						
						
						memberDetails.setMemberAddress(addressBuilder.toString());
						memberDetails.setResponseObject(eapplyInput.getResponseObject());
						if("Transefer".equalsIgnoreCase(applicationDTO.getRequesttype())){
							memberDetails.setTransferCover(Boolean.TRUE);
						}						
						memberProductDtsList = new ArrayList<MemberProductDetails>();
						
						/*if(applicantDTO.getPersonalDtlList()!=null){
				        	personalDtslist = (ArrayList)applicantDTO.getPersonalDtlList();
						} else {
							personalDtslist = new ArrayList<PersonalDetail>();
						}
						if(null!=applicantDTO.getQuetionDtoVector() && applicantDTO.getQuetionDtoVector().size()>0){
							 PdfPropertyFileLoc =getPdfproperty();// ConfigurationHelper.getConfigurationValue( "PdfPropertyFileLoc", "PdfPropertyFileLoc");
					         file = new File( PdfPropertyFileLoc);
					         url = new FileInputStream( file);    
					        property.load( url);
							for(int itrQues=0;itrQues<applicantDTO.getQuetionDtoVector().size();itrQues++){
								QuestionDto questionDto = (QuestionDto)applicantDTO.getQuetionDtoVector().get(itrQues);
								if(null!=questionDto && property.getProperty(questionDto.getQuestionTextID())!=null && property.getProperty(questionDto.getQuestionTextID()).length()>0){
									PersonalDetail personalDetail = new PersonalDetail();
									personalDetail.setLabel(property.getProperty(questionDto.getQuestionTextID()));
									personalDetail.setValue(questionDto.getAnswerText());
									personalDtslist.add(personalDetail);
								}
							}							
						}*/
						memberDetails.setPersonalDts(personalDtslist);												
							if(null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){
								 BigDecimal transferDeathAmtBD=null,transferTpdAmtBD=null;
								for(int covItr=0;covItr<applicantDTO.getCovers().size();covItr++){
									 coversDTO = (Cover)applicantDTO.getCovers().get(covItr);
									
									 if(null!=coversDTO){									
										 
										 memberDetails.setFrquencyOpted(coversDTO.getFrequencycosttype());
										 memberProductDetails = new MemberProductDetails();								
										 if(null!=coversDTO.getCost()){
											
											 
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision()))
													 && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getLoading() 													
													){
												 
												 
													Double additionalCost = 0.0;
													String laodtext = " additional ";
													/*if(null != coversDTO.getAddLoadingCost()){
														additionalCost = coversDTO.getAddLoadingCost();
														laodtext  = " additional ";
													}else if(coversDTO.getExistingCost()!=null){
														additionalCost = new Double(coversDTO.getExistingCost());
														laodtext  = " additional ";
													}*/
													
												cost = new Double(coversDTO.getCost())+(additionalCost * coversDTO.getLoading().doubleValue()/100);
												
												Double double1 = new Double(additionalCost * coversDTO.getLoading().doubleValue()/100);
												
												memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount(cost.toString()));
												
												if(double1 > 0.0) {
													 //StringBuilder loadingAmtTxt = new StringBuilder();
													String loadingAmtTxt="We wish to note that in considering the information disclosed in your personal statement your"+laodtext+"cover is subject to a loading of "+ coversDTO.getLoading().intValue()+"%, an"+laodtext+"premium in the amount of $"+ new BigDecimal(double1).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + " will be applicable if you accept the terms of the cover."; 
													 /**
													  * To Display the oercentage value									  */
													
																						 
											
													 pdfObject.setLoadingInd(Boolean.TRUE);
													 memberProductDetails.setLoadings(loadingAmtTxt);	
													 
												 }
											}else{
												/**
												  * Work Request 6026 - need to display the loading if it wavied of 
												  * need to implement
												  */
												
												/*if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision()))
														 && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())
														 && null!=coversDTO.getCoverOriginalLoading() && coversDTO.getCoverOriginalLoading().length()>0
														 && new BigDecimal(coversDTO.getCoverOriginalLoading()).doubleValue() > 25 
														 ){							
													
													
													if(new BigDecimal(coversDTO.getCoverOriginalLoading()).doubleValue() > 0){
														String loadingWavied = "During your application for additional cover a premium loading of "+ coversDTO.getCoverOriginalLoading() +"% has been assessed. However, this has been waived by your fund.";
														memberProductDetails.setLoadings(loadingWavied);
													}
													
												}*/					
												
												
												 memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount(coversDTO.getCost().toString()));
												 if(coversDTO.getCost()== null){
													 memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount("0"));
												 }
											}										
										 }								
										 memberProductDetails.setProductDecision(coversDTO.getCoverdecision());
										 if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob)){
											 /*SRKR:14/07/20Capture existing unitised cover amount based on new occupation category selected in Update work rating flow
										     * Applicable for NSFS and MTAA in UWCOVER flow.
										     * */
											 /*if(null != coversDTO && null != coversDTO.getNewExistingCoverAmount()){
												 memberProductDetails.setNewExistingCover(CommonPDFHelper.formatAmount(coversDTO.getNewExistingCoverAmount()));
											 }*/
											 
											 if(null != coversDTO && null != coversDTO.getFulamount()){
												 memberProductDetails.setFulCoverAmount(coversDTO.getFulamount());
											 }
											 										 
											 if(coversDTO.getAddunitind()!=null && "1".equalsIgnoreCase(coversDTO.getAddunitind())){
												if(null!=coversDTO.getCoverdecision() && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
													if(new BigDecimal(""+coversDTO.getAdditionalunit()).doubleValue() >0){
													  // memberProductDetails.setProductAmount(coversDTO.getAdditionalunit()+"\n ( "+coversDTO.getFixedunit()+" Units )");
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( "+coversDTO.getAdditionalunit()+" Units )");
													}else{
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
													}
												}else{
													/*if(null != coversDTO.getCoverPercentage()){
														memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n ( "+coversDTO.getAdditionalunit()+" Units  ), " +coversDTO.getCoverPercentage());	
													}else{
														memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n ( "+coversDTO.getAdditionalunit()+" Units )");
													}*/
													memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( "+coversDTO.getAdditionalunit()+" Units )");
												}
											 }else if(null!=coversDTO.getCovercategory() && !memberDetails.isTransferCover() && !coversDTO.getCovercategory().contains("Fixed")){
												 if(!"DCL".equalsIgnoreCase(applicationDTO.getAppdecision()) ){
													
													 /**
													  * IP selection and non-ip section is the same logic. when we need a different format for death, tpd against IP with the decimals 
													  * This can be used and if we have to display the % of IP cover required - Bala16072014
													  */
													 
													 
													 if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
														 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Units )");	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Fixed )"));	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														}else{
															if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Units )");	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Fixed )"));
																 	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }	 
													 }else if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
														 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																// memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n ( Units )");	
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( "+coversDTO.getAdditionalunit()+" Units )");
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 if(coversDTO.getAdditionalcoveramount()!=null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
																 } 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }else{
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n ( Units )");	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 if(coversDTO.getAdditionalcoveramount()!=null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
																 }	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }
													 }else if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision()) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())){													 
														 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Units )");	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Fixed )"));	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }else{
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Units )");	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+"\n  ( "+coversDTO.getCovercategory()+" )"+"\n ( Fixed )"));
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	 
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }
													 }
													 
												 }else{
													 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){														
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
													 }else{
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
														 
													 }
												 }
																				
											 } else if(memberDetails.isTransferCover()){
												 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n ( Transfer of Cover )");													 
											 }else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
												 if(coversDTO.getAdditionalcoveramount()!=null){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
												 }
												 
											 }else{
												 if(coversDTO.getAdditionalcoveramount()!=null){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ( Fixed )");
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
												 }
												 
											 }
											 //memberProductDetails.setAalLevelStatus(coversDTO.getAALevelStatus());
											 memberProductDetails.setProductName(coversDTO.getCovertype());
											 memberProductDetails.setBenefitPeriod(coversDTO.getAdditionalipbenefitperiod());
											 memberProductDetails.setWaitingPeriod(coversDTO.getAdditionalipwaitingperiod());
											 memberProductDetails.setExBenefitPeriod(coversDTO.getExistingipbenefitperiod());
											 memberProductDetails.setExWaitingPeriod(coversDTO.getExistingipwaitingperiod());						 
											
											 //need to revisit again
											 if(coversDTO.getTransfercoveramount()!=null){
												 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(coversDTO.getTransfercoveramount().toString()));
											 }
//											 if(null!=applicationDTO && null!=applicationDTO.getApplicant()){
//												 for(int itr1=0;itr1<applicationDTO.getApplicant().size();itr1++){
//													 Applicant applicantDTO1 = (Applicant)applicationDTO.getApplicant().get(itr1);
//													 if(null!=applicantDTO){
//														 	
//															for(int covItr1=0;covItr1<applicantDTO.getCovers().size();covItr1++){
//																dsd coversDTO = (Cover)applicantDTO.getCovers().get(covItr1);
//															}
//														 List questlist = applicantDTO1.getQuetionDtoVector();
//														 if(null!=questlist){
//															 for(int i=0;i<questlist.size();i++ ){
//																 QuestionDto questionDto = (QuestionDto) questlist.get(i);																		 
//																 if(null!=questionDto){
//																	 String qval = questionDto.getQuestionTextID();
//																	 String aval = questionDto.getAnswerText();
//																	 if("DEATH".equalsIgnoreCase(coversDTO.getCovercode()) && null!=qval && qval.equalsIgnoreCase(QuestionIdConstant.Q16_DEATH_TRANSFER_CVR)){
//																		 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(aval));
//																	 }else if("TPD".equalsIgnoreCase(coversDTO.getCovercode()) && null!=qval && qval.equalsIgnoreCase(QuestionIdConstant.Q17_TPD_TRANSFER_CVR)){
//																		 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(aval));
//																	 }else if("IP".equalsIgnoreCase(coversDTO.getCovercode()) && null!=qval && qval.equalsIgnoreCase(QuestionIdConstant.Q18_IP_TRANSFER_CVR)){
//																		 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(aval));
//																	 }
//																 }										 
//															 }
//															
//															 if(memberProductDetails.getTransferCoverAmnt()==null){
//																 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount("0"));
//															 }
//														 }
//														 								 
//													 }
//												 }
//											 }											
												
											 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
												 
												 if(new BigDecimal(""+coversDTO.getFixedunit()).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+" Units )");
												 }else{
													 if(null != coversDTO.getExistingcoveramount() && coversDTO.getExistingcoveramount().doubleValue() > 0 ){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount()+"\n ( Fixed )"));
															 
													 }else{
														// if(coversDTO.getExistingcoveramount()!=null){
															 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
														// }
													 }
												 }
											 }else{
												 
												 if(new BigDecimal(""+coversDTO.getFixedunit()).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+" Units )");
												 }else{
													 if(null != coversDTO.getExistingcoveramount() && coversDTO.getExistingcoveramount().doubleValue() > 0 ){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ( Fixed )");
															 
													 }else{
														// if(coversDTO.getExistingcoveramount()!=null){
															 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
														// }
													 }
												 }
												 
												/* if(new BigDecimal(""+coversDTO.getFixedunit()).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+" Units )");													
												 }else{													
													 if(coversDTO.getExistingcoveramount()!=null){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));		
													 } 
												 }*/
											 }
											 
										 
											 
											 memberProductDetails.setOccupationRating(coversDTO.getOccupationrating());
											 if(coversDTO.getCoverexclusion()!=null && coversDTO.getCoverexclusion().trim().length()>0){
												 memberProductDetails.setExclusions(HTMLToString.convertHTMLToString(coversDTO.getCoverexclusion()));
											 }											 									 
											 if(null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length()>0){
												 pdfObject.setExclusionInd(Boolean.TRUE);
											 }											 
											 if("DEATH".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_DEATH+coversDTO.getCoverreason());
											 }
											 if("TPD".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 &&  null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_TPD+coversDTO.getCoverreason());
											 }											
											 if("IP".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_IP+coversDTO.getCoverreason());
											 }
											 if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision()) && null!=coversDTO.getCoverreason()){
												 alldecreasons.append(coversDTO.getCoverreason());
											 }
											
										 }
										 memberProductDtsList.add(memberProductDetails);
									 }
									
								}
							}							
						
						memberDetails.setMemberProductList(memberProductDtsList);
												
					}
				}
				
			}		 
			
			// End
		
			if(null!=applicationDTO.getCampaigncode()){
				pdfObject.setCampaignCode(applicationDTO.getCampaigncode());
			}			
			if(null != disclosureStatementList && disclosureStatementList.size()>0){
				pdfObject.setDisclosureStatementList(disclosureStatementList);	
			}	
			pdfObject.setProductName("CARE");	
			//prepare disclosureList
			
			pdfObject.setDisclosureQuestionList(PDFbusinessObjectMapper.prepareQuestionList(eapplyInput, property));
			pdfObject.setMemberDetails(memberDetails);
			pdfObject.setDeclReasons(alldecreasons.toString());
		}
		
		country = null;		
		memberProductDetails = null;
		memberProductDtsList = null;	
		memberDetails = null;
		applicantDTO = null;
		coversDTO = null;
		alldecreasons = null;
		property.clear();
		property=null;		
		PdfPropertyFileLoc = null;
		file =null; 
		if(null!=url){
			url.close();
			url=null;     
		}	
		
		return pdfObject;
	}
	
	public static List<DisclosureQuestionAns> prepareQuestionList(EapplyInput eapplyInput, Properties property){
		List<DisclosureQuestionAns> disclosureQuestionAnsList = new ArrayList<DisclosureQuestionAns>();
		DisclosureQuestionAns disclosureQuestionAns = null;
		
		if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getFirstName()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_first_name"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getPersonalDetails().getFirstName());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
	
		
		if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getLastName()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_last_name"));
			System.out.println("last name----" +disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getPersonalDetails().getLastName());
			System.out.println("last name ans-----" +disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}	
		/*if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getGender()!=null && eapplyInput.getPersonalDetails().getGender()!=""){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_gender"));
			System.out.println("gender----" +disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getPersonalDetails().getGender());
			System.out.println("gender ans-----" +disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}else */
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getGender()!=null && eapplyInput.getOccupationDetails().getGender()!=""){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_gender"));
			System.out.println("gender----" +disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getGender());
			System.out.println("gender ans-----" +disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
		// it is checking with input XML values, will work on this
		/*if(null!=eapplyInput.getContactDetails()){
			if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContactTime())){				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));			
				disclosureQuestionAns.setAnswerText("9am � 12pm");				
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}else{				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));			
				disclosureQuestionAns.setAnswerText("12pm � 6pm");				
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			System.out.println("contact time>>"+eapplyInput.getContactDetails().getPrefContactTime());
			System.out.println("contact Type>>"+eapplyInput.getContactDetails().getPrefContact());
			if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){			
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Mobile Phone");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
		
			}else if("2".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Home Phone");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
			}else if("3".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Work Phone");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}else{				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Email");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactDetails().getEmailAddress());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			
		}*/
		
		
		if(eapplyInput.getEmail()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_email_add"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getEmail());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		
		if((eapplyInput.getDob()) != null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_dob"));		
			disclosureQuestionAns.setAnswerText(eapplyInput.getDob());
			System.out.println("dob-----" +disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);		
		}
		if(eapplyInput.getContactPrefTime()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getContactPrefTime());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		if(eapplyInput.getContactType()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));	
			if("Mobile".equalsIgnoreCase(eapplyInput.getContactType())){		
				disclosureQuestionAns.setAnswerText("Mobile Phone");
			}else if("Home".equalsIgnoreCase(eapplyInput.getContactType())){		
				disclosureQuestionAns.setAnswerText("Home Phone");
			}else if("Work".equalsIgnoreCase(eapplyInput.getContactType())){		
				disclosureQuestionAns.setAnswerText("Work Phone");
			}
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		
		if(eapplyInput.getContactPhone()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getFifteenHr()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_fifteen_hr_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getFifteenHr());
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getCitizenQue()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_citizen_aus"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getCitizenQue());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}	
		
		/*if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getSmoker()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_smoker_que"));
			if("1".equalsIgnoreCase(eapplyInput.getPersonalDetails().getSmoker())){
				disclosureQuestionAns.setAnswerText("Yes"); 
			}else{
				disclosureQuestionAns.setAnswerText("No"); 
			}
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}*/
		
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getSalary()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_annualSalary"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getSalary());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getIndustryName()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_industrycnfrm"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getIndustryName());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getOccupation()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_occupation_que"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccupation());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getWithinOfficeQue()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_askmanual_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getWithinOfficeQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getTertiaryQue()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_ter_qual"));	
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getTertiaryQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getManagementRoleQue()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_work_duty"));	
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getManagementRoleQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		return disclosureQuestionAnsList;
	}

	
	private String prepareTotalCoverLoadingAmt(String totalCoverLoadingAmt, String loadingAmount) {
		Double double1 = null;
		if (isObjectPresent(toDouble(loadingAmount))) {
			double1 = toDouble(loadingAmount);
			if (isObjectPresent(toDouble(totalCoverLoadingAmt))) {
				double1 = toDouble(totalCoverLoadingAmt) + toDouble(loadingAmount);
				return double1.toString();
			} else {
				return loadingAmount;
			}
		}
		return null;
	}
	
	public static Double toDouble(String string) {
		
		if(isValuePresent(string)) {
			return Double.valueOf(string);
		}
		return null;
	}
	
	public static boolean isValuePresent(String string) {
		
		if(isObjectPresent(string) && string.trim().length() > 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isObjectPresent(Object object){
		
		if(null != object) {
			return true;
		}
		return false;
	}

}
