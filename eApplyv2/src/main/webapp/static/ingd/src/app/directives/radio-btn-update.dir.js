export default class CheckboxAndRadioUpdate {
  constructor() {
    this.restrict = 'EA';
    this.replace = false;
    this.scope = false;
    this.require = 'ngModel';
  }

  link(scope, element, attrs, ngModelCtrl) {
    element.on('change', function () {
      scope.$apply(function () {
        ngModelCtrl.$setViewValue(element[0].type.toLowerCase() === 'radio' ? element[0].value : element[0].checked);
      });
    });
  }
}
