package au.com.metlife.eapply.corporate.service;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.corporate.business.CorpRequest;
import au.com.metlife.eapply.corporate.business.CorpResponse;
import au.com.metlife.eapply.corporate.business.DashboardResponse;
import au.com.metlife.eapply.repositories.model.CorporateFund;

public interface CorpService {

	/**
	 * Method to retrieve on-boarded funds 
	 * based on request and return them as list
	 * 
	 * @param corpRequest
	 * 				Type: {@link CorpRequest} - Request details built from HttpRequest
	 */
	CorpResponse getOnboardedFunds(CorpRequest corpRequest);
	/**
	 * Service saves/ updates all the fund and its category information
	 * 
	 * @param corpRequest
	 * 				Type: {@link CorpRequest} - Request details built from HttpRequest
	 */
	CorpResponse saveCorporateFund(CorpRequest corpRequest);

	/**
	 * Service to get fund information and particular fund category information
	 * @param corpRequest
	 * 				Type: {@link CorpRequest} - Request details built from HttpRequest
	 */
	CorpResponse getFundCategoryInformation(CorpRequest corpRequest);
	

	/**
	 * Service to get Member Upload information for particular fund 
	 * @param toDate 
	 * @param fromDate 
	 * @param applicationNumber 
	 * @param lastName 
	 * @param firstName 
	 * @param status 
	 * @param fundCode 
	 * 			
	 */
	List<DashboardResponse> getMemberInformation(String fundCode, String status, String firstName, String lastName, String applicationNumber, Date fromDate, Date toDate);
	
	
	
	/**
	 * @param fundCode
	 * @param status
	 * @param firstName
	 * @param lastName
	 * @param applicationNumber
	 * @return
	 */
	DashboardResponse updateDB(String fundCode, String status, String firstName, String lastName, String applicationNumber);
	
	/**
	 * @param fundCode
	 * @param emailid
	 * @param firstName
	 * @param lastName
	 * @param applicationNumber
	 * @param authid
	 * @return
	 * @throws FileNotFoundException
	 */
	DashboardResponse sendEmail(String fundCode, String emailid, String firstName, String lastName, String applicationNumber,String authid) throws FileNotFoundException;
	/**
	 * @param appNum
	 * @return
	 */
	DashboardResponse saveCorporateApp(Long appNum,String lastName);
	/**
	 * @param appNum
	 * @return
	 */
	DashboardResponse submitCorporateApp(Long appNum,String lastName);
	
	/**
	 * @param applicationNumber
	 * @return
	 */
	CorpResponse expireCorpApp(String applicationNumber);
	
	
	/**
	 * @param fundId
	 * @return
	 */
	CorporateFund getFundDetails(String fundId);
	
	
   /**
 * @param eapplyInput
 * @param authid
 * @return
 */
CorpResponse sendSaveEmail(EapplyInput eapplyInput,String authid);
   
   /**
 * @param eapplyInput
 * @param authid
 * @return
 */
CorpResponse sendFundSaveEmail(EapplyInput eapplyInput,String authid);


CorporateFund getFundCategoryDetails(String fundId,String category);


}
