package au.com.metlife.eapply.b2b.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "AUTHTOKEN", schema="EAPPDB")
public class AuthToken {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="AUTHTOKEN_SEQ")
	@SequenceGenerator(name="AUTHTOKEN_SEQ",sequenceName = "EAPPDB.AUTHTOKEN_SEQ", initialValue=1, allocationSize=100)
	  private long id;	
	  
	  @NotNull
	  @Column(name="authtoken",length=500)
	  private String authtoken;
	  
	public  AuthToken(){
		
	}
	
	  public AuthToken(String authtoken,Date createdate) {
			this.authtoken = authtoken;
			this.createdate = createdate;
	}


	public String getAuthtoken() {
		return authtoken;
	}



	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}



	@Column(name="CREATEDDATE",length=5000)
	  private Date createdate;

	

	public Date getCreatedate() {
		return createdate;
	}

	

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}


}
