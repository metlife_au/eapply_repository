package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ALIHealthDetailsPageObjects;
import uimap.metlife.OAMPSHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

public class ALIHealthDetailsPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public ALIHealthDetailsPage yourHealth() {
		yourHealthDetails();
		return new ALIHealthDetailsPage(scriptHelper);
	}

	public ALIHealthDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourHealthDetails() {

		// exclusion
		String diagnosed = dataTable.getData("MetLife_Data", "Diagnosed");
		String Threeyears    = dataTable.getData("MetLife_Data", "3yrs");
		
		
		String familyHistory = dataTable.getData("MetLife_Data", "FamilyHistory");
		String lifeStyle = dataTable.getData("MetLife_Data", "LifeStyleQuestions");
		String alchohol = dataTable.getData("MetLife_Data", "Alchohol.Quantity");
		String alchoholReduce = dataTable.getData("MetLife_Data", "Alchohol.Reduce");
		String hiv = dataTable.getData("MetLife_Data", "HIV");
		String hivRisk = dataTable.getData("MetLife_Data", "HIV.Risk");
		String general = dataTable.getData("MetLife_Data", "General.Questions");
		String insuranceDecline = dataTable.getData("MetLife_Data", "Exclusion");
		
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String pregnant = dataTable.getData("MetLife_Data", "Pregnant");
	
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
	
			
			if (Threeyears.equalsIgnoreCase("None")) {
			tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.chklast3yrsNone),"Click", "Have you suffered,diagnosed in last 3 years - None");
			Thread.sleep(1000);
			} else if (Threeyears.equalsIgnoreCase("Lung")) {
				
				tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.chklast3yrsLung),"Click", "Lung or breathing conditions");
				Thread.sleep(200);
				tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.chklast3yrsAsthma),"Click", "Asthma");
				Thread.sleep(200);
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdlast3yrsAttention), "clkradio", "Attention");
				Thread.sleep(1000);
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdlast3yrsCondition), "clkradio", "Moderate");
				Thread.sleep(1000);
			}
			if (diagnosed.equalsIgnoreCase("None")) {
			tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.chkmedicaladviceNone),"Click", "Have you suffered from, diagnosed with/sought medical advice - None");
			Thread.sleep(1000);
			}	else if(diagnosed.equalsIgnoreCase("Bone")) {
				
				tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.chklastdiagnosedBone),"Click", "Have you suffered from, diagnosed with/sought medical advice - Bone, joint or limp");
				Thread.sleep(200);
				tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.chklastdiagnosedStrain),"Click", "Have you suffered from, diagnosed with/sought medical advice - Strain");		
				Thread.sleep(200);
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.chklastdiagnosedAnkle), "Click", "Ankle");
				Thread.sleep(200);
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.drplastdiagnosedImpact), "DropDownSelect", "The left ankle","The left ankle");
				Thread.sleep(200);
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.btnlastdiagnosedSelect), "Click", "Select");
				Thread.sleep(1000);
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdlastdiagnosedCurrent), "clkradio", "Current");
				Thread.sleep(1000);
			} 
			
			if (gender.equalsIgnoreCase("Female")) {
		//		tryAction(pregnant.equalsIgnoreCase("Yes")?waitForClickableElement(OAMPSHealthPageObjects.rdPregnantYes):waitForClickableElement(OAMPSHealthPageObjects.rdPregnantNo),"clkradio","Pregnant -" + pregnant);
				String rdPregnantYes= "input[id='radio1-7:0'][name='radio1-7']";
				String rdPregnantNo =  "input[id='radio1-7:1'][name='radio1-7']";	
				tryAction(pregnant.equalsIgnoreCase("Yes")?waitForClickableElement(By.cssSelector(rdPregnantYes)):waitForClickableElement(By.cssSelector(rdPregnantNo)),"clkradio","Pregnant -" + pregnant);
				
				sleep();

			
			String strfamilyHistory = "input[value*='[11120]No']+*";
			String strlifeStyle = "input[id*='radio1-9:1']";
			String stralchoholReduce = "input[id*='radio1-11:1']";
		//	String strhivNo = "input[id*='radio1-12:1']";
		//	String strhivYes = "input[id*='radio1-12:0']";
			String strhivRisk = "input[id*='radio1-12:1']";
			String strgeneral = "input[id*='radio1-13:1']";
		//	String strinsuranceExclNo ="input[id*='radio1-15:1']";
			String strinsuranceClaimNo ="input[id*='radio1-14:1']";
			String strtxtalchohol = "ran1-10";
			String strEnter = "a[id='ran210-Lifestyle_Questions-Lifestyle_Questions-10']";
						
			tryAction(waitForClickableElement(By.cssSelector(strfamilyHistory)), "clkradio", "Family History");
			tryAction(fluentWaitElement(By.cssSelector(strlifeStyle)), "clkradio", "LifeStyle");
			tryAction(fluentWaitElement(By.cssSelector(stralchoholReduce)), "clkradio", "AlchoholReduce");
			tryAction(waitForClickableElement(By.id(strtxtalchohol)), "SET", "AlchoholQuantity", alchohol);
			tryAction(fluentWaitElement(By.cssSelector(strEnter)), "Click", "Apply");

/*			if (hiv.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(By.cssSelector(strhivNo)), "clkradio", "HIV");
			} else {
				tryAction(fluentWaitElement(By.cssSelector(strhivYes)), "clkradio", "HIV");
			}*/
			sleep();
			if (hivRisk.equalsIgnoreCase("No")) {
			tryAction(fluentWaitElement(By.cssSelector(strhivRisk)), "clkradio", "HIVRisk");
			}
			tryAction(fluentWaitElement(By.cssSelector(strgeneral)), "clkradio", "General");
/*			tryAction(fluentWaitElement(By.cssSelector(strinsuranceExclNo)), "clkradio",
					"Insurance Exclusion");*/
			tryAction(fluentWaitElement(By.cssSelector(strinsuranceClaimNo)), "clkradio", "Insurance Details");			
			}
			else { 
			
			if (familyHistory.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.chkfamilyhistoryNone), "clkradio", "Family History" );
			} else {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.chkfamilyhistoryNone), "clkradio", "Family History" );
			}
			
			if (lifeStyle.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.chklifestyleNo), "clkradio", "LifeStyle" );
			} else {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.chklifestyleNo), "clkradio", "LifeStyle" );
			}		
	
			tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.txtalchohol), "SET", "AlchoholQuantity", alchohol);
			tryAction(waitForClickableElement(ALIHealthDetailsPageObjects.txtalchohol), "TAB", "AlchoholQuantity");
			
			tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.btnEnter), "Click", "Apply");
			
			Thread.sleep(1000);
			
			if (alchoholReduce.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdalchoholreduceNo), "clkradio", "AlchoholReduce" );
			} else {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdalchoholreduceNo), "clkradio", "AlchoholReduce" );
			}			
	
		//	Thread.sleep(1000);
			

			Thread.sleep(1000);
			
			if (hivRisk.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdhivriskNo), "clkradio", "HIVRisk" );
			} 		
			
			Thread.sleep(1000);
			
			if (general.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdgeneralNo), "clkradio", "General" );
			} else {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdgeneralNo), "clkradio", "General" );
			}			
		//	Thread.sleep(1000);
			
			if (insuranceDecline.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdinsuranceexclusionNo), "clkradio", "Insurance Decline - No" );
			} else {
				tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.rdinsuranceexclusionNo), "clkradio", "Insurance Decline - No" );
			}		
			}

			tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.btnNext), "Click", "Next");
			//tryAction(fluentWaitElement(ALIHealthDetailsPageObjects.btnNext), "Click", "Next");
			Thread.sleep(4000);

			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
