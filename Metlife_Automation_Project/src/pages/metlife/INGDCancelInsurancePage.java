package pages.metlife;


import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import uimap.metlife.CoverDetailsPageObjects;
import uimap.metlife.MetlifeINGDirPayloadPageObjects;
import uimap.metlife.WebserviceInputStringPageObjects;

public class INGDCancelInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public INGDCancelInsurancePage enterINGDPayload() {
		cancelINGDInsurance();
		return new INGDCancelInsurancePage(scriptHelper);
	}

	public INGDCancelInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void cancelINGDInsurance() {
			
		try {
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCancelCover), "Click", "Proceed To Cancel Cover Page");
			sleep(5000);
			quickSwitchWindows();
			sleep();
			String deathSelCover=dataTable.getData("MetLife_Data", "DeathSelectCover");
			String ipSelCover=dataTable.getData("MetLife_Data", "incomeprotectionselectcover");
			String tpdSelCover = dataTable.getData("MetLife_Data", "TDPSelectCover");
			
			sleep();
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selDeathselectcovertype), "DropDownSelect", "Death Select Cover",deathSelCover);
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selDeathselectcovertype), "TAB", "Death Select Cover");
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selTotalpermdisabilityselectcovertype), "DropDownSelect", "TPD Select Cover",tpdSelCover);
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selTotalpermdisabilityselectcovertype), "TAB", "Death Select Cover");
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selIncomeprotectionselectcovertype), "DropDownSelect", "Income Protection Select Cover",ipSelCover);
			sleep(1000);
			tryAction(fluentWaitElement(CoverDetailsPageObjects.selIncomeprotectionselectcovertype), "TAB", "Income Protection Select Cover");
			sleep(1000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCancelContinue), "Click", "Contiune to Cancel the cover");
			sleep(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCancelContinue), "Click", "Contiune to Cancel the cover");
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}
}
