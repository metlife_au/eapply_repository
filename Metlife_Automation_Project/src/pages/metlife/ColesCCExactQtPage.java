package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCExactQtPageObjects;
import uimap.metlife.ColesCCGetQuotePageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.STFTGetQuotePageObjects;

public class ColesCCExactQtPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public ColesCCExactQtPage enterColesCCExactQuote() {
		CalculateColesCCExactQuote();
		return new ColesCCExactQtPage(scriptHelper);
	}
	
	public ColesCCExactQtPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}
	
	public void CalculateColesCCExactQuote() {

		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
		
			tryAction(fluentWaitElement(ColesCCExactQtPageObjects.btnNext), "Click", "Apply");
			Thread.sleep(1000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}


}
