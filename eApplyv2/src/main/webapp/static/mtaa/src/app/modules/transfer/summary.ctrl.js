(function(angular){
	'use strict';
    /* global channel, moment */  
	angular
	.module('MtaaApp')
	.controller('transferSummary', transferSummary);
	transferSummary.$inject=['$scope','$rootScope','$location','$timeout','$window','auraInputService','getAuraTransferData','deathCoverService','tpdCoverService','submitAura','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService','RetrieveAppDetailsService','saveEapply' ,'$stateParams'];
	function transferSummary($scope, $rootScope,$location, $timeout, $window, auraInputService,getAuraTransferData,deathCoverService,tpdCoverService,submitAura,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService,RetrieveAppDetailsService, saveEapply, $stateParams){
		
		var vm=this;
		vm.urlList = urlService.getUrlList();		
		
		vm.init = function(){
			
			vm.savedData =  PersistenceService.getTransferCoverDetails();
			//vm.totalTransCost = parseFloat(vm.savedData.applicant.cover[0].cost) + parseFloat(vm.savedData.applicant.cover[1].cost) + parseFloat(vm.savedData.applicant.cover[2].cost);
			
			if($stateParams.mode === '3'){
				if(!vm.savedData.responseObject){
					vm.savedData.responseObject = {};
				}
				PersistenceService.setTransCoverAuraDetails(vm.savedData.responseObject);
			}
			
			vm.transferCoverAuraDetails = PersistenceService.getTransCoverAuraDetails();			
		};
		vm.init();
		
		vm.go = function (path) {
			
	    	$timeout(function(){
	    		$location.path(path);
	    	}, 10);
	  	};
		
		vm.saveTransferSummary = function() {
			
			var appNum = PersistenceService.getAppNumber();
			var saveSummaryTransferObject =  PersistenceService.getTransferCoverDetails();
			var transCoverAuraDetails = PersistenceService.getTransCoverAuraDetails();
	    	
	    	if(saveSummaryTransferObject !== null){
	    		
	    		saveSummaryTransferObject.lastSavedOn = 'summarytransferpage';
	    		saveSummaryTransferObject.responseObject = transCoverAuraDetails;
	        	auraResponseService.setResponse(saveSummaryTransferObject);
	        	
	        	console.log(JSON.stringify(saveSummaryTransferObject));	        	
	        	saveEapply.reqObj(vm.urlList.saveEapplyUrlNew).then(function(response) {
		                console.log(response.data);
		                vm.SaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
		        });
	    	}
	  	};
	  	
	  	vm.SaveAndExitPopUp = function (hhText) {
	  		
	  		var templateContent = '<div class="ngdialog-content"><div class="modal-body">';
	  		templateContent +='<h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4>';
	  		templateContent +='<div class="row  rowcustom"><div class="col-sm-12"><p class="aligncenter"></p>';
	  		templateContent +='<div id="tips_text">'+hhText+'</div>  <p></p>  </div></div></div>';
	  		templateContent +='<div class="ngdialog-buttons aligncenter">';
	  		templateContent +='<button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button>';
	  		templateContent +='</div></div>';	  		

			var dialog1 = ngDialog.open({
				    template: templateContent,
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = '/landing';
					       $location.path( url );
					       return true;
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
		
		vm.checkAckStateGCTR = function(){
	    	$timeout(function(){
	    		var ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
	        	if(ackCheckTRGC){
	        		vm.TRGCackFlag = false;
	        	}else{
	        		vm.TRGCackFlag = true;
	        	}
	    	}, 10);
	    };
		
		vm.submitTransferCover = function(){
			var ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
			var submitTransferObject =  PersistenceService.getTransferCoverDetails();
			var transCoverAuraDetails = PersistenceService.getTransCoverAuraDetails();
			submitTransferObject.responseObject = transCoverAuraDetails;
			console.log(JSON.stringify(submitTransferObject));
			
			if(ackCheckTRGC){
				if(submitTransferObject !== null){					
					submitTransferObject.lastSavedOn = '';
					submitTransferObject.appDecision = 'ACC';
					auraResponseService.setResponse(submitTransferObject);
					submitEapply.reqObj(vm.urlList.submitEapplyUrlNew).then(function(response) {
						PersistenceService.setPDFLocation(response.data.clientPDFLocation);
						PersistenceService.setNpsUrl(response.data.npsTokenURL);						
						vm.go('/transferDecision/accept');							
		            });
				}
			}else{
				vm.TRGCackFlag = true;
			}			
		};
		
	}
})(angular);

 /* Transfer Summary Controller,Progressive and Mandatory validations Ends  */
