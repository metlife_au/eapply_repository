/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class HostplusPersonalDetailsPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public HostplusPersonalDetailsPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	
	public static final By txtClientRefId = getObjectIdentifaction("hostplus.personalDetail.memberid.id");
	public static final By txtFirstname = getObjectIdentifaction("hostplus.personalDetail.firstName.id");
	public static final By txtSurname = getObjectIdentifaction("hostplus.personalDetail.lastName.id");
	public static final By txtLastName = getObjectIdentifaction("hostplus.personalDetail.introduction.lastName.id");
	public static final By radMale = getObjectIdentifaction("hostplus.personalDetail.introduction.male.radio.id");
	public static final By radFemale = getObjectIdentifaction("hostplus.personalDetail.introduction.female.radio.id");
	public static final By radCitizenYes = getObjectIdentifaction("hostplus.personalDetail.introduction.citizen.yes.radio.id");
	public static final By radCitizenNo = getObjectIdentifaction("hostplus.personalDetail.introduction.citizen.no.radio.id");
	public static final By radSmokerYes = getObjectIdentifaction("hostplus.personalDetail.introduction.smoker.yes.radio.id");
	public static final By radSmokerNo = getObjectIdentifaction("hostplus.personalDetail.introduction.smoker.no.radio.id");
	public static final By btnIntroduction = getObjectIdentifaction("hostplus.personalDetail.introduction.continue.btn.id");
	public static final By txtDateOfBirth = getObjectIdentifaction("hostplus.personalDetail.DOB.id");
	public static final By btnNewApplication = getObjectIdentifaction("hostplus.personalDetail.btnLogin.id");
	public static final By btnRetrieveApplication = getObjectIdentifaction("hostplus.personalDetail.btnRetrieveApplication.id");
	public static final By btnChangeInsuranceCover = getObjectIdentifaction("hostplus.manageinsurance.changeinsurancecover.link.id");
	public static final By btnTrackPrevApplication = getObjectIdentifaction("hostplus.manageinsurance.trackpreviousapplications.link.id");
	public static final By btnTransferExistingCover = getObjectIdentifaction("hostplus.manageinsurance.transferexistingcover.link.id");
	public static final By btnapplyDeathCover = getObjectIdentifaction("hostplus.manageinsurance.deathcover.link.id");
	public static final By btnapplyTPDCover = getObjectIdentifaction("hostplus.manageinsurance.tpdcover.link.id");
	public static final By btnApplyIPCover = getObjectIdentifaction("hostplus.manageinsurance.ipcover.link.id");
	public static final By btnLifeEvent = getObjectIdentifaction("hostplus.manageinsurance.lifeevent.link.id");
	public static final By btnUpdateOccupation = getObjectIdentifaction("hostplus.manageinsurance.updateoccupation.link.id");
	public static final By btnCancelExistingCover = getObjectIdentifaction("hostplus.manageinsurance.cancelexistingcover.link.id");
	public static final By txtEmailid = getObjectIdentifaction("hostplus.personalDetail.emailid.txt.id");
	public static final By txtContactNumber = getObjectIdentifaction("hostplus.personalDetail.contactnum.txt.id");
	public static final By radMorningTime = getObjectIdentifaction("hostplus.personalDetail.pref.contact.morningtime.radio.css");
	public static final By radAfternoonTime = getObjectIdentifaction("hostplus.personalDetail.pref.contact.afternoontime.radio.css");
	public static final By btnContactDetContinue = getObjectIdentifaction("hostplus.personalDetail.contactdetail.continue.time.btn.id");
	public static final By rad15HrsYes = getObjectIdentifaction("hostplus.manageinsurance.15Hrs.yes.radio.css");
	public static final By rad15HrsNo = getObjectIdentifaction("hostplus.manageinsurance.15Hrs.no.radio.css");
	public static final By radRegIncomeYes = getObjectIdentifaction("hostplus.manageinsurance.regincome.yes.radio.css");
	public static final By radRegIncomeNo = getObjectIdentifaction("hostplus.manageinsurance.regincome.no.radio.css");
	public static final By radDutiesYes = getObjectIdentifaction("hostplus.manageinsurance.duties.yes.radio.css");
	public static final By radDutiesNo = getObjectIdentifaction("hostplus.manageinsurance.duties.no.radio.css");
	public static final By radPrCitizenYes = getObjectIdentifaction("hostplus.manageinsurance.prcitizen.yes.radio.css");
	public static final By radDCFixed = getObjectIdentifaction("hostplus.manageinsurance.dc.fixed.css");
	public static final By radPrCitizenNo = getObjectIdentifaction("hostplus.manageinsurance.prcitizen.no.radio.css");
	public static final By lstIndustry = getObjectIdentifaction("hostplus.manageinsurance.industry.list.css");
	public static final By lstOccupation = getObjectIdentifaction("hostplus.manageinsurance.occupation.list.css");
	public static final By radWorkDutiesYes = getObjectIdentifaction("hostplus.manageinsurance.workduties.yes.radio.css");
	public static final By radWorkDutiesNo = getObjectIdentifaction("hostplus.manageinsurance.workduties.no.radio.css");
	public static final By radTeritaryYes = getObjectIdentifaction("hostplus.manageinsurance.teritery.yes.radio.css");
	public static final By radTeritaryNo = getObjectIdentifaction("hostplus.manageinsurance.teritery.no.radio.css");
	public static final By txtAnnualSalary = getObjectIdentifaction("hostplus.manageinsurance.annualsalary.txt.id");
	public static final By btnOccupationDetContinue = getObjectIdentifaction("hostplus.personalDetail.occupationdetail.continue.time.btn.id");
	public static final By lstDCFunction = getObjectIdentifaction("hostplus.manageinsurance.tpd.function.list.css");
	public static final By radDCUnitised = getObjectIdentifaction("hostplus.manageinsurance.dc.unitised.css");
	public static final By txtDCAmount = getObjectIdentifaction("hostplus.personalDetail.dc.cover.amount.txt.css");
	public static final By lstTPDFunction = getObjectIdentifaction("hostplus.personalDetail.tpd.list.css");
	public static final By lstLifestageFunction = getObjectIdentifaction("hostplus.personalDetail.lifestage.list.css");
	public static final By btnContinuePage = getObjectIdentifaction("hostplus.personalDetail.continue.id");
	public static final By btnHostCalculateQuote = getObjectIdentifaction("hostplus.personalDetail.continue.cover.css");
	public static final By btnHostCalculateQuoteById = getObjectIdentifaction("hostplus.personalDetail.continue.cover.id");
	public static final By btnNonmeberHostplusPopup = getObjectIdentifaction("hostplus.personalDetail.nonmember.popup.btn.css");
	public static final By lstHPHeightUnits =	getObjectIdentifaction("hostplus.personalStatement.height.units.css");
	public static final By lstHPWeightUnits =	getObjectIdentifaction("hostplus.personalStatement.weight.units.css");
	public static final By radHPHealthDeatilsNo =	getObjectIdentifaction("hostplus.personalstatement.life_health_details.radio.no.css");
	public static final By radHPHealthDeatilsYes =	getObjectIdentifaction("hostplus.personalstatement.life_health_details.radio.yes.css");
	
	//public static final By chekboxNOTA =	getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.none.checkbox.css");
	
	public static final By chkLung = getObjectIdentifaction("hostplus.personalStatement.lung.css");
	public static final By chkAsthma = getObjectIdentifaction("hostplus.personalStatement.asthma.css");
	public static final By radAsthmaMild = getObjectIdentifaction("hostplus.personalStatement.asthma.mild.css");
	public static final By radAsthmaModerate = getObjectIdentifaction("hostplus.personalStatement.asthma.moderate.css");
	public static final By radAsthmaSevere = getObjectIdentifaction("hostplus.personalStatement.asthma.severe.css");
	public static final By chkMedAdvNota = getObjectIdentifaction("hostplus.personalStatement.medicaladvise.nota.zero.css");
	public static final By chkBPCholNota = getObjectIdentifaction("hostplus.personalStatement.medicaladvise.nota.first.css");
	public static final By chkBrainBoneNota = getObjectIdentifaction("hostplus.personalStatement.medicaladvise.nota.second.css");
	//added by Samba
	public static final By chekboxNOTA3yrsdiagnosed =	getObjectIdentifaction("hostplus.yourhealth.last3yrs.diagnosed.none.checkbox.css");
	public static final By chekboxNOTA3yrsdiagnosedM =	getObjectIdentifaction("hostplus.yourhealth.last3yrs.diagnosed.none.Male.checkbox.css");
	public static final By chekboxNOTA5yrsdiagnosed =	getObjectIdentifaction("hostplus.yourhealth.last5yrs.diagnosed.none.checkbox.css");
	public static final By chekboxNOTA5yrsdiagnosedM =	getObjectIdentifaction("hostplus.yourhealth.last5yrs.diagnosed.none.Male.checkbox.css");
	public static final By chekboxNOTAdiagnosed =	getObjectIdentifaction("hostplus.yourhealth.suffer.diagnosed.none.checkbox.css");
	public static final By chekboxNOTAdiagnosedM =	getObjectIdentifaction("hostplus.yourhealth.suffer.diagnosed.none.Male.checkbox.css");
	public static final By radPreagNo =	getObjectIdentifaction("hostplus.yourhealth.pregnant.none.radio.css");
	public static final By radRegVisitNo =	getObjectIdentifaction("hostplus.yourhealth.regularvisit.no.radio.css");
	public static final By radRegVisitNoM =	getObjectIdentifaction("hostplus.yourhealth.regularvisit.no.Male.radio.css");
	public static final By btnHealthQuestions =	getObjectIdentifaction("hostplus.yourhealth.healthquestions.btn.id");
	public static final By btnHealthQuestionsF =getObjectIdentifaction("hostplus.yourhealth.healthquestions.Continue.btn.css");//Added for female flow_Deepa
	public static final By radFamHistNo =	getObjectIdentifaction("hostplus.yourhealth.familyhistory.no.radio.xpath");
	public static final By radFamHistNoF =	getObjectIdentifaction("hostplus.yourhealth.familyhistory.no.Female.radio.xpath");
	public static final By btnFamilyHistory =	getObjectIdentifaction("hostplus.yourhealth.familyhistory.btn.css");
	public static final By radTravelPlanNo =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.travelplan.no.radio.css");
	public static final By radTravelPlanYesF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.travelplan.yes.Female.radio.css");
	public static final By radTravelPlanNoF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.travelplan.no.Female.radio.css");
	public static final By radTravelPlanYes =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.travelplan.yes.radio.css");
	public static final By chekboxNOTAhazardous = getObjectIdentifaction("hostplus.yourhealth.lifestyle.hazardous.none.checkbox.css");
	public static final By chekboxNOTAhazardousF = getObjectIdentifaction("hostplus.yourhealth.lifestyle.hazardous.none.Female.checkbox.css");
	public static final By radDrugNo =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.drugs.no.radio.css");
	public static final By radDrugNoF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.drugs.no.female.radio.css");
	public static final By txtAlcoholic =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.alcoholic.text.css");
	public static final By btnAlcoholic =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.alcoholic.btn.css");
	public static final By txtAlcoholicF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.alcoholic.Female.text.css");
	public static final By btnAlcoholicF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.alcoholic.Female.btn.css");
	public static final By radRedAlcoholicNo =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.alcoholic.reduce.no.radio.css");
	public static final By radRedAlcoholicNoF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.alcoholic.reduce.no.female.radio.css");
	public static final By radHIVYes =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.hiv.yes.radio.css");
	public static final By radHIVNo =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.hiv.no.radio.css");
	public static final By radHIVYesF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.hiv.yes.female.radio.css");
	public static final By radHIVNoF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.hiv.no.female.radio.css");
	public static final By radHIVPrevNo =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.hivprev.no.radio.css");
	public static final By radHIVPrevNoF =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.hivprev.no.female.radio.css");
	public static final By btnLifyStyle =	getObjectIdentifaction("hostplus.yourhealth.lifestyle.btn.css");
	public static final By radDiscloseNo =	getObjectIdentifaction("hostplus.yourhealth.general.disclosed.no.radio.css");
	public static final By radDiscloseNoF =	getObjectIdentifaction("hostplus.yourhealth.general.disclosed.no.female.radio.css");
	public static final By btnGeneralQuest = getObjectIdentifaction("hostplus.yourhealth.generalquestion.btn.css");
	public static final By radTraumaNo = getObjectIdentifaction("hostplus.yourhealth.insurancedetails.trauma.no.radio.css");
	public static final By radTraumaNoF = getObjectIdentifaction("hostplus.yourhealth.insurancedetails.trauma.no.female.radio.css");
	public static final By radIllInjuryNo =	getObjectIdentifaction("hostplus.yourhealth.insurancedetails.illinjury.no.radio.css");
	public static final By radIllInjuryNoF =getObjectIdentifaction("hostplus.yourhealth.insurancedetails.illinjury.no.female.radio.css");
	public static final By btnHealthLife =	getObjectIdentifaction("hostplus.yourhealth.healthandlife.btn.css");
	public static final By chekboxSelDuty =	getObjectIdentifaction("hostplus.confirmation.dutycheck.selected.checkbox.css");
	public static final By chekboxSelPrevStmt =	getObjectIdentifaction("hostplus.confirmation.previousstatement.selected.checkbox.css");
	public static final By chekboxSelGC =	getObjectIdentifaction("hostplus.confirmation.generalconsent.selected.checkbox.css");
	public static final By btnconfirm =	getObjectIdentifaction("hostplus.confirmation.finalsubmission.btn.css");
	public static final By lblOutcome=	getObjectIdentifaction("hostplus.decisionpage.outcome.lbl.css");
	public static final By lblAppnum=	getObjectIdentifaction("hostplus.decisionpage.appnum.lbl.css");
	
}
