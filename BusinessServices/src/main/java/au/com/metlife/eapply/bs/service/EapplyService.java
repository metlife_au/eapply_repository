package au.com.metlife.eapply.bs.service;

import java.util.List;

import au.com.metlife.eapply.bs.model.Eapplication;

public interface EapplyService {
	
	Eapplication submitEapplication(Eapplication eapplication) throws Exception;

	Boolean clientMatch(String firstName, String lastName, String dob);
	
	Boolean checkAppSubmittedInLast24Hrs(String fundCode, String clientRefNo,String manageType);

	Eapplication retrieveApplication(String applicationNumber);
	
	Eapplication expireApplication(String applicationNumber);
	
	List<Eapplication> retrieveSavedApplications(String fundCode, String clientRefNo,String manageType);
}
