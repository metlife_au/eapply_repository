package au.com.metlife.eapply.bs.pdf.pdfObject;

import java.io.Serializable;
import java.util.List;

public class PDFObject implements Serializable{
	
	private float logoSize = 0.0f;
	private String colorStyle = null;
	private String pageHeadingBackground = null;
	private String pageHeadingTextColor = null;
	private String sectionBackground = null;
	private String sectionTextColor = null;
	private String tabSectionBackground = null;
	private String tabSectionTextColor = null;
	private String borderColor = null;
	private String fundId = null;
	private String productName = null;
	private String pdfType = null;
	private String pdfFor = null;
	private int fontSize = 7;
	private String pdfAttachDir = null;
	private String logPath = null;
	private boolean freeKidsDiscount = false;
	private boolean promoDiscount = false;
	private String applicationNumber = null;
	private String lob = null;
	private String channel = null;
	private String clientPDFLoc = null;
	private String uwPDFLoc = null;
	private String applicationDecision = null;
	private String campaignCode = null;
	private SpecialConditions specialConditions = null;
	private List<String> partnerAddrList = null;
	private List<DisclosureStatement> disclosureStatementList = null;
	private MemberDetails memberDetails = null;	
	private List questionListForHide = null;
	private List<DisclosureQuestionAns> disclosureQuestionList = null;
	private Boolean accInjuryProdInd = Boolean.FALSE;
	private boolean downSaleInd = Boolean.FALSE;
	private boolean exclusionInd = Boolean.FALSE;
	private boolean loadingInd = Boolean.FALSE;
	private String discountApplied = null;
	//private DiscountDTO discountDTO = null;
	private String declReasons= null;
	private String magazineOffer = null;
	
	private String decCompany = null;
	private String decPrivacy = null;
	private String decGC = null;
	private String decDDS = null;
	private String decPrivacyNoAuth = null;
	private String flyBuysNo = null;
	private String emailAttachDir = null;
	private String policyFee = null;
	
	private String requestType = null;
	
	private boolean nonValidatedMemberFlow=false;
	
	private boolean quickQuoteRender = false;
	
	private String disclosureQuestion = null;	
	
	private String formLength=null;
	
	private String fakeOverallDecision=null;
	
	public String getFakeOverallDecision() {
		return fakeOverallDecision;
	}
	public void setFakeOverallDecision(String fakeOverallDecision) {
		this.fakeOverallDecision = fakeOverallDecision;
	}
	public String getFormLength() {
		return formLength;
	}
	public void setFormLength(String formLength) {
		this.formLength = formLength;
	}	
	public String getDisclosureQuestion() {
		return disclosureQuestion;
	}
	public void setDisclosureQuestion(String disclosureQuestion) {
		this.disclosureQuestion = disclosureQuestion;
	}
	public String getDecDDS() {
		return decDDS;
	}
	public void setDecDDS(String decDDS) {
		this.decDDS = decDDS;
	}
	public String getDecCompany() {
		return decCompany;
	}
	public void setDecCompany(String decCompany) {
		this.decCompany = decCompany;
	}
	public String getDecGC() {
		return decGC;
	}
	public void setDecGC(String decGC) {
		this.decGC = decGC;
	}
	public String getDecPrivacy() {
		return decPrivacy;
	}
	public void setDecPrivacy(String decPrivacy) {
		this.decPrivacy = decPrivacy;
	}
	public String getMagazineOffer() {
		return magazineOffer;
	}
	public void setMagazineOffer(String magazineOffer) {
		this.magazineOffer = magazineOffer;
	}
	public String getDeclReasons() {
		return declReasons;
	}
	public void setDeclReasons(String declReasons) {
		this.declReasons = declReasons;
	}
	public boolean isExclusionInd() {
		return exclusionInd;
	}
	public void setExclusionInd(boolean exclusionInd) {
		this.exclusionInd = exclusionInd;
	}
	public boolean isDownSaleInd() {
		return downSaleInd;
	}
	public void setDownSaleInd(boolean downSaleInd) {
		this.downSaleInd = downSaleInd;
	}
	public Boolean getAccInjuryProdInd() {
		return accInjuryProdInd;
	}
	public void setAccInjuryProdInd(Boolean accInjuryProdInd) {
		this.accInjuryProdInd = accInjuryProdInd;
	}
	public List<DisclosureQuestionAns> getDisclosureQuestionList() {
		return disclosureQuestionList;
	}
	public void setDisclosureQuestionList(
			List<DisclosureQuestionAns> disclosureQuestionList) {
		this.disclosureQuestionList = disclosureQuestionList;
	}
	
	public String getColorStyle() {
		return colorStyle;
	}
	public void setColorStyle(String colorStyle) {
		this.colorStyle = colorStyle;
	}
	public int getFontSize() {
		return fontSize;
	}
	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}
	
	public MemberDetails getMemberDetails() {
		return memberDetails;
	}
	public void setMemberDetails(MemberDetails memberDetails) {
		this.memberDetails = memberDetails;
	}	
	public String getPdfType() {
		return pdfType;
	}
	public void setPdfType(String pdfType) {
		this.pdfType = pdfType;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSectionBackground() {
		return sectionBackground;
	}
	public void setSectionBackground(String sectionBackground) {
		this.sectionBackground = sectionBackground;
	}
	public String getPdfFor() {
		return pdfFor;
	}
	public void setPdfFor(String pdfFor) {
		this.pdfFor = pdfFor;
	}
	public String getPdfAttachDir() {
		return pdfAttachDir;
	}
	public void setPdfAttachDir(String pdfAttachDir) {
		this.pdfAttachDir = pdfAttachDir;
	}
	public String getLogPath() {
		return logPath;
	}
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	public float getLogoSize() {
		return logoSize;
	}
	public void setLogoSize(float logoSize) {
		this.logoSize = logoSize;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public List<String> getPartnerAddrList() {
		return partnerAddrList;
	}
	public void setPartnerAddrList(List<String> partnerAddrList) {
		this.partnerAddrList = partnerAddrList;
	}
	public List<DisclosureStatement> getDisclosureStatementList() {
		return disclosureStatementList;
	}
	public void setDisclosureStatementList(List<DisclosureStatement> disclosureStatementList) {
		this.disclosureStatementList = disclosureStatementList;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	public String getApplicationDecision() {
		return applicationDecision;
	}
	public void setApplicationDecision(String applicationDecision) {
		this.applicationDecision = applicationDecision;
	}
	public String getSectionTextColor() {
		return sectionTextColor;
	}
	public void setSectionTextColor(String sectionTextColor) {
		this.sectionTextColor = sectionTextColor;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	/**
	 * @return the specialConditions
	 */
	public SpecialConditions getSpecialConditions() {
		return specialConditions;
	}
	/**
	 * @param specialConditions the specialConditions to set
	 */
	public void setSpecialConditions(SpecialConditions specialConditions) {
		this.specialConditions = specialConditions;
	}
	public boolean isFreeKidsDiscount() {
		return freeKidsDiscount;
	}
	public void setFreeKidsDiscount(boolean freeKidsDiscount) {
		this.freeKidsDiscount = freeKidsDiscount;
	}
	public String getClientPDFLoc() {
		return clientPDFLoc;
	}
	public void setClientPDFLoc(String clientPDFLoc) {
		this.clientPDFLoc = clientPDFLoc;
	}
	public String getUwPDFLoc() {
		return uwPDFLoc;
	}
	public void setUwPDFLoc(String uwPDFLoc) {
		this.uwPDFLoc = uwPDFLoc;
	}
	public String getCampaignCode() {
		return campaignCode;
	}
	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}
	public boolean isPromoDiscount() {
		return promoDiscount;
	}
	public void setPromoDiscount(boolean promoDiscount) {
		this.promoDiscount = promoDiscount;
	}	
	public String getDiscountApplied() {
		return discountApplied;
	}
	public void setDiscountApplied(String discountApplied) {
		this.discountApplied = discountApplied;
	}
	public List getQuestionListForHide() {
		return questionListForHide;
	}
	public void setQuestionListForHide(List questionListForHide) {
		this.questionListForHide = questionListForHide;
	}
	public String getFundId() {
		return fundId;
	}
	public void setFundId(String fundId) {
		this.fundId = fundId;
	}
	public String getTabSectionBackground() {
		return tabSectionBackground;
	}
	public void setTabSectionBackground(String tabSectionBackground) {
		this.tabSectionBackground = tabSectionBackground;
	}
	public String getTabSectionTextColor() {
		return tabSectionTextColor;
	}
	public void setTabSectionTextColor(String tabSectionTextColor) {
		this.tabSectionTextColor = tabSectionTextColor;
	}
	public String getBorderColor() {
		return borderColor;
	}
	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}
	public boolean isNonValidatedMemberFlow() {
		return nonValidatedMemberFlow;
	}
	public void setNonValidatedMemberFlow(boolean nonValidatedMemberFlow) {
		this.nonValidatedMemberFlow = nonValidatedMemberFlow;
	}
	public boolean isQuickQuoteRender() {
		return quickQuoteRender;
	}
	public void setQuickQuoteRender(boolean quickQuoteRender) {
		this.quickQuoteRender = quickQuoteRender;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public boolean isLoadingInd() {
		return loadingInd;
	}
	public void setLoadingInd(boolean loadingInd) {
		this.loadingInd = loadingInd;
	}
	public String getFlyBuysNo() {
		return flyBuysNo;
	}
	public void setFlyBuysNo(String flyBuysNo) {
		this.flyBuysNo = flyBuysNo;
	}
	public String getEmailAttachDir() {
		return emailAttachDir;
	}
	public void setEmailAttachDir(String emailAttachDir) {
		this.emailAttachDir = emailAttachDir;
	}
	public String getPolicyFee() {
		return policyFee;
	}
	public void setPolicyFee(String policyFee) {
		this.policyFee = policyFee;
	}
	public String getDecPrivacyNoAuth() {
		return decPrivacyNoAuth;
	}
	public void setDecPrivacyNoAuth(String decPrivacyNoAuth) {
		this.decPrivacyNoAuth = decPrivacyNoAuth;
	}
	public String getPageHeadingBackground() {
		return pageHeadingBackground;
	}
	public void setPageHeadingBackground(String pageHeadingBackground) {
		this.pageHeadingBackground = pageHeadingBackground;
	}
	public String getPageHeadingTextColor() {
		return pageHeadingTextColor;
	}
	public void setPageHeadingTextColor(String pageHeadingTextColor) {
		this.pageHeadingTextColor = pageHeadingTextColor;
	}
	
}
