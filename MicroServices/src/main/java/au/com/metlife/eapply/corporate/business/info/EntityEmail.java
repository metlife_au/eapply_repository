package au.com.metlife.eapply.corporate.business.info;

public class EntityEmail {
	
	private String address;
	private boolean isPrimary;

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public boolean isIsPrimary() {
		return isPrimary;
	}
	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	

}
