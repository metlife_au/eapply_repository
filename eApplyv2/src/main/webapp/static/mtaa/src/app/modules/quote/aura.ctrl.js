(function(angular) {
    'use strict';

    /* global channel, inputData, token, moment */
	/**
	 * @ngdoc controller
	 * @name MtaaApp.controller:auraCtrl
	 *
	 * @description
	 * This is auraCtrl description
	 */
    
 angular
	.module('MtaaApp')
	.controller('auraCtrl', auraCtrl);
 auraCtrl.$inject=['$rootScope', '$location', '$timeout','$window','auraTransferInitiateSvc', 'fetchAuraTransferData','auraRespSvc', 'auraPostSvc','auraInputSvc', 'PersistenceService', 'submitAuraSvc','ngDialog','persoanlDetailService','saveEapplyData','clientMatchSvc','submitEapplySvc','fetchUrlSvc', 'fetchPersoanlDetailSvc', 'appData', 'fetchAppNumberSvc', 'fetchIndustryListMtaa', 'fetchIpCoverSvc','$q'];
 function auraCtrl($rootScope, $location, $timeout,$window,auraTransferInitiateSvc,fetchAuraTransferData,auraRespSvc,auraPostSvc,auraInputSvc, PersistenceService, submitAuraSvc, ngDialog,persoanlDetailService,saveEapplyData,clientMatchSvc,submitEapplySvc,fetchUrlSvc, fetchPersoanlDetailSvc, appData, fetchAppNumberSvc, fetchIndustryListMtaa, fetchIpCoverSvc,$q){
  var vm = this;
  vm.urlList = fetchUrlSvc.getUrlList();
  vm.datePattern='/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i';
  vm.ipIncreaseFlag = false;
  vm.waitingPeriodOptions = ['30 Days', '60 Days', '90 Days'];
  vm.benefitPeriodOptions = ['2 Years', '5 Years','Age 65'];
  vm.freeText = {};
  vm.healthQuestionIndex = null;
  vm.isAuraErroneous = false;
  vm.erroneousSections = [];
  vm.go = function (path) {
	  if(vm.heightval && vm.weightVal) {
			vm.submitHeightWeightQuestion(vm.healthQuestionIndex).then(function() {
				$timeout(function(){
					$location.path(path);
				}, 10);
			}, function(err) {
				console.log(err);
			});
		} else {
			$timeout(function(){
				$location.path(path);
			}, 10);
		}
  };

	vm.serachText = '';
  vm.heightDropdownOpt = ['cm', 'Feet'];
  vm.heightDropDown = vm.heightDropdownOpt[0];
  vm.heightOptions= vm.heightOptions === undefined ? 'm.cm' : vm.heightOptions;

  vm.weightDropdownOpt = ['Kilograms', 'Pound', 'Stones'];
  vm.weightDropDown = vm.weightDropdownOpt[0];
  vm.weighOptions=vm.weighOptions === undefined ? 'kg' : vm.weighOptions;
  vm.weightMaxLen = 3;
  vm.oldQuestionId = [];
  vm.noCheck = false;
	vm.navigateToLandingPage = function () {
		ngDialog.openConfirm({
	            template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'>' + 
	            '<div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
    };
    vm.visible = function(idshow) {
    	if(document.getElementById(idshow) != null){
    		document.getElementById(idshow).style.visibility = 'visible';
    	}
    };
    vm.ipCoverDetails = fetchIpCoverSvc.getIpCover();

    vm.quotePageDetails = {}; // will be replaced
    vm.coverDetails = {}; // will be replaced
    vm.deathAddnlDetails = {}; // will be replaced
    vm.tpdAddnlDetails = {}; // will be replaced
    vm.ipAddnlDetails = {}; // will be replaced
    vm.changeCoverOccDetails = {}; // will be replaced
    angular.extend(vm.quotePageDetails, appData.getAppData());
    angular.extend(vm.coverDetails, vm.quotePageDetails);
    angular.extend(vm.deathAddnlDetails, vm.quotePageDetails.applicant.cover[0]);
    angular.extend(vm.tpdAddnlDetails, vm.quotePageDetails.applicant.cover[1]);
    angular.extend(vm.ipAddnlDetails, vm.quotePageDetails.applicant.cover[2]);
    angular.extend(vm.changeCoverOccDetails, vm.quotePageDetails.applicant);
    // Industry name
    vm.quotePageDetails.applicant.occupationCode = fetchIndustryListMtaa.getIndustryName(vm.quotePageDetails.applicant.industryType);
    
    vm.personalDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
    vm.checkBenefitPeriod = function() {
      vm.tempWaitingPeriod = vm.waitingPeriodOptions.indexOf(vm.ipCoverDetails.waitingPeriod) > -1 ? vm.ipAddnlDetails.additionalIpWaitingPeriod : '90 Days' || '90 Days';
      vm.tempBenefitPeriod = vm.benefitPeriodOptions.indexOf(vm.ipCoverDetails.benefitPeriod) > -1 ? vm.ipAddnlDetails.additionalIpBenefitPeriod : '2 Years' || '2 Years';

      if(vm.tempWaitingPeriod === vm.ipAddnlDetails.existingIpWaitingPeriod && vm.tempBenefitPeriod === vm.ipAddnlDetails.existingIpBenefitPeriod){
        vm.ipIncreaseFlag = false;
      } else if((vm.tempBenefitPeriod === 'Age 65' && vm.ipAddnlDetails.existingIpBenefitPeriod === '2 Years') ||
    		  (vm.tempBenefitPeriod === '5 Years' && vm.ipAddnlDetails.existingIpBenefitPeriod === '2 Years')||
    		  (vm.tempBenefitPeriod === 'Age 65' && vm.ipAddnlDetails.existingIpBenefitPeriod === '5 Years')||
          (vm.tempWaitingPeriod === '60 Days' && vm.ipAddnlDetails.existingIpWaitingPeriod === '90 Days') ||
          (vm.tempWaitingPeriod === '30 Days' && vm.ipAddnlDetails.existingIpWaitingPeriod === '90 Days') ||
          (vm.tempWaitingPeriod === '30 Days' && vm.ipAddnlDetails.existingIpWaitingPeriod === '60 Days')){
        vm.ipIncreaseFlag = true;
      } else{
        vm.ipIncreaseFlag = false; 
      }
    };
    vm.checkBenefitPeriod();

  	auraInputSvc.setFund('MTAA');
  	auraInputSvc.setMode('change');
    //setting deafult vaues for testing
  	auraInputSvc.setName(vm.personalDetails.personalDetails.firstName+' '+vm.personalDetails.personalDetails.lastName);
  	//auraInputSvc.setAge(moment().diff(moment(vm.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
  	auraInputSvc.setAge(moment().diff(moment(vm.coverDetails.applicant.birthDate, 'DD-MM-YYYY'), 'years'));
  	auraInputSvc.setAppnumber(parseInt(vm.coverDetails.applicatioNumber));
  	
  	
  		if(parseFloat(vm.deathAddnlDetails.additionalCoverAmount) > parseFloat(vm.coverDetails.applicant.cover[0].existingCoverAmount)){
  			auraInputSvc.setDeathAmt(parseFloat(vm.deathAddnlDetails.additionalCoverAmount));
		}else{
			auraInputSvc.setDeathAmt(0);
		}

		if(parseFloat(vm.tpdAddnlDetails.additionalCoverAmount) > parseFloat(vm.coverDetails.applicant.cover[1].existingCoverAmount)){
			auraInputSvc.setTpdAmt(parseFloat(vm.tpdAddnlDetails.additionalCoverAmount));
		}else{
			auraInputSvc.setTpdAmt(0);
		}
  	
  	if((parseFloat(vm.ipAddnlDetails.additionalUnit)*250) > parseFloat(vm.coverDetails.applicant.cover[2].existingCoverAmount)){
  		auraInputSvc.setIpAmt(parseFloat(vm.ipAddnlDetails.additionalCoverAmount));
  	}else if(((parseFloat(vm.ipAddnlDetails.additionalUnit)*250) <= parseFloat(vm.coverDetails.applicant.cover[2].existingCoverAmount)) && vm.ipIncreaseFlag){
  		auraInputSvc.setIpAmt(parseFloat(vm.ipAddnlDetails.additionalCoverAmount));
  	}else{
  		auraInputSvc.setIpAmt(0);
  	}  	
  
  	auraInputSvc.setWaitingPeriod(vm.ipAddnlDetails.additionalIpWaitingPeriod);
  	auraInputSvc.setBenefitPeriod(vm.ipAddnlDetails.additionalIpBenefitPeriod);
  	auraInputSvc.setMemberType('INDUSTRY OCCUPATION');
  	if(vm.changeCoverOccDetails && vm.changeCoverOccDetails.gender ){
  		auraInputSvc.setGender(vm.changeCoverOccDetails.gender);
  	}else if(vm.personalDetails && vm.personalDetails.gender){
  		auraInputSvc.setGender(vm.personalDetails.gender);
  	}

  	auraInputSvc.setClientname('metaus');
  	auraInputSvc.setIndustryOcc(vm.changeCoverOccDetails.industryType+':'+vm.changeCoverOccDetails.occupation);
  	if(vm.quotePageDetails.applicant.australiaCitizenship==='Yes'){
  		auraInputSvc.setCountry('Australia');
  	}else{
  		auraInputSvc.setCountry(vm.quotePageDetails.applicant.australiaCitizenship);
  	}
  		 // residency que removed from quote page, so hard-coded
  	auraInputSvc.setSalary(vm.changeCoverOccDetails.annualSalary);
  	auraInputSvc.setFifteenHr(vm.changeCoverOccDetails.workHours );
  	auraInputSvc.setLastName(vm.personalDetails.personalDetails.lastName);
  	auraInputSvc.setFirstName(vm.personalDetails.personalDetails.firstName);
  	auraInputSvc.setDob(vm.personalDetails.personalDetails.dateOfBirth);
  	var termFlag = false;
    vm.personalDetails.existingCovers = vm.personalDetails.existingCovers || {}; 
    vm.personalDetails.existingCovers.cover = vm.personalDetails.existingCovers.cover || [];
  	for(var k = 0; k < vm.personalDetails.existingCovers.cover.length; k++){
  		if((vm.personalDetails.existingCovers.cover[k].exclusions && vm.personalDetails.existingCovers.cover[k].exclusions !== '') ||
  				(vm.personalDetails.existingCovers.cover[k].loading && vm.personalDetails.existingCovers.cover[k].loading !== '' &&
  						parseFloat(vm.personalDetails.existingCovers.cover[k].loading) > 0)){
  			termFlag = true;
  			break;
  		}
  	}
  	if(termFlag){
  		auraInputSvc.setExistingTerm(true);
  	} else{
  		auraInputSvc.setExistingTerm(false);
  	}
  	
	 fetchAuraTransferData.requestObj(vm.urlList.auraTransferDataUrl).then(function(response) {
  		vm.auraResponseDataList = response.data.sections;
  		console.log('Aura Response' + JSON.stringify(vm.auraResponseDataList));
  		angular.forEach(vm.auraResponseDataList, function (Object,index) {
  			console.log(vm.auraResponseDataList);
  			vm.questionAlias = Object.sectionName;
  			vm.questionlist = Object.questions;
  			if(vm.questionAlias==='QG=Health Questions'){
  				vm.healthQuestionIndex = index;
  				vm.auraResponseDataList[index].sectionStatus=true;
  			}else{
  				var keepGoing = true;
  				angular.forEach(vm.questionlist, function (Object,index1) {
  					if(keepGoing && Object.branchComplete){
  						vm.auraResponseDataList[index].sectionStatus=true;
  						keepGoing = false;
  					}
  				});
  			}
  		});
  		//console.log(vm.auraResponseDataList)
  		vm.updateHeightWeightValue();
  		vm.updateoldQuestionIdList();

  	});

  	 vm.inchValue  = function(value){
  		console.log(value);
  		vm.heightin = value;
  	 };
  	 vm.meterValue  = function(value,answer, questionObj){
  		if(!angular.isUndefined(value)){
  			vm.heightinMeter = value;
  	  		vm.heightval = value;
  	  		questionObj.error = false;
  		}else{
  			questionObj.error = true;
  		}


  	 };

	vm.heighOptions  = function(value){
  		console.log(value);
  		if(value==='cm'){
  			vm.heightOptions='m.cm';
  		}else{
  			vm.heightval = '';
  			vm.heightOptions='ft.in';
  		}
  	 };
  	vm.weightValue = function(value,answer,questionObj){

  		if(!angular.isUndefined(value)){
  	  		vm.weightVal = value;
  	  		questionObj.error = false;
  		}else if(!angular.isUndefined(value) && value===''){
  			questionObj.error = true;
  		}

  	};
  	vm.weightOptions= function(value,answer){
  		console.log(value);
  		if(value==='Kilograms'){
  			vm.weighOptions='kg';
  			vm.weightDropDown = 'Kilograms';
  			vm.weightMaxLen = 3;
  			vm.weightVal = '';
  		}else if(value==='Pound'){
  			vm.weighOptions='lb';
  			vm.weightDropDown = 'Pound';
  			vm.weightMaxLen = 3;
  			vm.weightVal = '';
  		}else if(value==='Stones'){
  			vm.weighOptions='st.lb';
  			vm.weightDropDown = 'Stones';
  			vm.weightMaxLen = 100;
  			vm.weightVal = '';
  		}
  	};

  	vm.lbsValue = function(value){
  		console.log(value);
  		vm.lbsval = value;
  	};

  	 vm.submitHeightWeightQuestion = function(sectionIndex){
  		var defer = $q.defer();
  		angular.forEach(vm.auraResponseDataList[sectionIndex].questions, function (Object,index) {
  				var serviceCallRequired= false;
  				var questionObj= Object;
  				if(Object.classType==='HeightQuestion'){
  					
      				if(vm.heightOptions==='m.cm'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= vm.heightOptions;
          	   			questionObj.arrAns[1]= '0';
          	   			questionObj.arrAns[2]= vm.heightval;
      				}else if(vm.heightOptions==='ft.in'){
      					console.log(vm.heightOptions);
      					console.log(vm.heightinMeter);
      					console.log(vm.heightin);
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= vm.heightOptions;
          	   			questionObj.arrAns[1]= vm.heightval;
          	   			questionObj.arrAns[2]= vm.heightin;
      				}

      	   			serviceCallRequired= true;
      			}else if(Object.classType==='WeightQuestion'){
      				questionObj= Object;
      				console.log(vm.weightDropDown);
      				console.log(vm.weighOptions);
      				console.log(vm.weightVal);
      				if(vm.weightDropDown==='Stones'){
      					console.log(vm.weightVal);
      					console.log(vm.lbsval);
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'st.lb';
          	   			questionObj.arrAns[1]= vm.weightVal;
          	   			questionObj.arrAns[2]= vm.lbsval;
      				}else if(vm.weightDropDown==='Kilograms'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'kg';
          	   			questionObj.arrAns[1]= vm.weightVal;
      				}else if(vm.weightDropDown==='Pound'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'lb';
          	   			questionObj.arrAns[1]= vm.weightVal;
      				}

      	   			serviceCallRequired= true;
      			}
  				if(serviceCallRequired && questionObj.arrAns.length>1){
  					 vm.auraRes={'questionID':questionObj.questionId,'auraAnswers':questionObj.arrAns};
  					console.log(vm.auraRes);
          			 auraRespSvc.setResponse(vm.auraRes);
          			auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function(response) {
          				vm.updateQuestionList(questionObj.questionId,response.data.changedQuestion,response.data);
          				vm.updateHeightWeightValue();
          				var keepGoing = true;
                  		angular.forEach(vm.auraResponseDataList[sectionIndex].questions, function (Object,index1) {
                  			console.log(Object);
                  			vm.setprogressiveError(vm.auraResponseDataList[sectionIndex].questions[index1]);
                  			if(keepGoing && !Object.branchComplete){
                  				keepGoing = false;
                  			}

                  		});
                  		console.log(keepGoing);
                  		if(keepGoing){
                  			sectionIndex = sectionIndex+1;
                  			console.log(sectionIndex);                      		
                  			vm.auraResponseDataList[sectionIndex].sectionStatus=true;
                  		}
                  		defer.resolve({});
                   	}, function () {
                   		console.log('failed');
                   	    defer.reject({});
                   	});
  				}else{
  					angular.forEach(vm.auraResponseDataList[sectionIndex].questions, function (Object,index1) {
  						vm.setprogressiveError(vm.auraResponseDataList[sectionIndex].questions[index1]);
  					});

  				}

      		//});
  		});
  		return defer.promise;
   	 };
   	 //updating height and weight questions
   	 vm.updateHeightWeightValue = function(){

   		angular.forEach(vm.auraResponseDataList, function (Object,index) {
  			vm.questionAlias = Object.sectionName;
  			vm.questionlist = Object.questions;
  			if(vm.questionAlias==='QG=Health Questions'){
  				vm.healthQuestion = vm.auraResponseDataList[index].questions;
  				 angular.forEach(vm.healthQuestion, function (question) {
 					if(question.classType==='HeightQuestion'){
 						if(question.answerTest.indexOf('Metres')!==-1){

 							vm.heightval=parseInt(question.answerTest.split(',')[0].split('Metres')[0])*100 + parseInt(question.answerTest.split(',')[1].split('cm')[0]);
 							vm.heightDropDown='cm';
 							vm.heightOptions='m.cm';
 						}
 						if(question.answerTest.indexOf('Feet')!==-1){
 							vm.heightDropDown='Feet';
 							vm.heightval=question.answerTest.substring(0,question.answerTest.indexOf('Feet')-1);
 							vm.inches= question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('in')-1);
 							vm.heightOptions='ft.in';
 						}

 					}else if(question.classType==='WeightQuestion'){
 						if(question.answerTest.indexOf('Kilograms')!==-1){
 							console.log(question.answerTest);
 							vm.weightDropDown='Kilograms';
 							vm.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Kilograms')-1);
 							vm.weighOptions='kg';
 						}
 						if(question.answerTest.indexOf('Pound')!==-1){
 							console.log(question.answerTest);
 							vm.weightDropDown='Pound';
 							vm.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Pound')-1);
 							vm.weighOptions='lb';
 						}
 						if(question.answerTest.indexOf('Stones')!==-1){
 							console.log(question.answerTest);
 							vm.weightDropDown='Stones';
 							vm.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Stones')-1);
 							vm.weighOptions='st.lb';
 							vm.lbsval=question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('lbs')-1);
 						}
 					}
 				});
  			}
   		});


   	 };

  	vm.collapseUncollapse = function (index,sectionName){
  		console.log(vm.auraResponseDataList);
  		if(sectionName==='QG=Health Questions'){
  			vm.submitHeightWeightQuestion(index);
  		}else{
  			var keepGoing = true;
      		angular.forEach(vm.auraResponseDataList[index].questions, function (Object,index1) {
      			console.log(Object);
      			vm.setprogressiveError(vm.auraResponseDataList[index].questions[index1]);
      			if(keepGoing && !Object.branchComplete){
      				keepGoing = false;
      			}

      		});
      		console.log(keepGoing);
      		if(keepGoing){
      			index = index+1;
      			console.log(index);
          		vm.auraResponseDataList[index].sectionStatus=true;

      		}
  		}

  		//console.log(vm.auraResponseDataList)
  	};

  	vm.stateChanged = function (qId) {
  		if(vm.answers[qId]){ //If it is checked
  			 console.log('id>>'+qId);
  	   }
  	};
  	vm.updateFreeText = function (answerValue, questionObj){
  		console.log('answer>'+answerValue);
  		questionObj.arrAns = [];
  		questionObj.arrAns[0]=answerValue;
  		questionObj.arrAns[1]='accept';
 		 vm.auraRes={'questionID':questionObj.questionId,'auraAnswers':questionObj.arrAns};
 		auraRespSvc.setResponse(vm.auraRes);
		  auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function(response) {
			  vm.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data);
		  });
  	};
  	vm.updateQuestionList = function(questionId,changedQuestion, questionObj) {
      vm.isAuraErroneous = false;
  		angular.forEach(vm.auraResponseDataList, function (Object,index) {
  			vm.questionlist = Object.questions;
  			angular.forEach(vm.questionlist, function (Obj,index1) {
          if(Obj && changedQuestion) {
    				if(Obj.questionId === changedQuestion.questionId){
    					vm.auraResponseDataList[index].questions[index1]= changedQuestion;
    					 vm.progressive(questionId,changedQuestion,questionObj);
    					console.log(vm.auraResponseDataList[index]);
    				}
          }
  			});
  		});
  	};

  	 vm.updateRadio = function (answerValue, questionObj) {
  		console.log('answer>'+answerValue);
  		if(questionObj.classType === 'DateQuestion') {
  			var pattern =/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  	  		if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}
  		else if(!answerValue && answerValue !== 0) {
  			questionObj.error = true;
  			return false;
  		}

  		questionObj.answerTest = answerValue;

  		questionObj.arrAns[0]=answerValue;
  		 vm.auraRes={'questionID':questionObj.questionId,'auraAnswers':questionObj.arrAns};
  		  auraRespSvc.setResponse(vm.auraRes);

  		  auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function(response) {
  		console.log(response.data.changedQuestion);
  		vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
  			//auraQuestionlist
      	}, function () {
      		console.log('failed');
      	});
  	 };

  	 vm.progressive = function(questionid,changedQuestion,questionObj){
  		angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
  			if(changedQuestion.questionAlias=== Object.sectionName){
  				vm.questionIndex = vm.auraResponseDataList[selectedIndex].questions.indexOf(changedQuestion);
  				angular.forEach(vm.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
  					if(index<=vm.questionIndex){
  						if(index===vm.questionIndex){
  							var keepGoing = true;
  	  						vm.subprogressive(vm.auraResponseDataList[selectedIndex].questions[index],questionid,keepGoing,questionObj);
  						}else{
  							vm.setprogressiveError(vm.auraResponseDataList[selectedIndex].questions[index]);
  						}


  					}

  				});
  			}

  		});
   	 };

   	vm.setprogressiveError =  function(baseQuestionObj){
   		if(baseQuestionObj && baseQuestionObj.questionComplete){
   			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
   				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
   					if(subauraQustion.questionComplete && !subauraQustion.error){
   						vm.setprogressiveError(subauraQustion);
   					}else{
   						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error=true;
              vm.keepGoing = false;
   					}
   				});

   	   		});
   		}else if(baseQuestionObj && baseQuestionObj.classType==='HeightQuestion' && (!angular.isUndefined(vm.heightval) && vm.heightval.length>0)){
   			baseQuestionObj.error = false;
   		}else if(baseQuestionObj && baseQuestionObj.classType==='WeightQuestion' && (!angular.isUndefined(vm.weightVal)  && vm.weightVal.length>0)){
   			baseQuestionObj.error = false;
   		}else if(baseQuestionObj){
   			baseQuestionObj.error = true;
   		}

   	};
   	vm.subprogressive =  function(baseQuestionObj,questionid,keepGoing, updatedQuestionObj){
   		if(baseQuestionObj && baseQuestionObj.questionId<questionid){
   			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
   				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
   					if(!subauraQustion.questionComplete && subauraQustion.questionId<questionid){
   						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error=true;
   					}else if(subauraQustion.questionComplete && subauraQustion.questionId<questionid){
   						vm.subprogressive(subauraQustion,questionid,keepGoing,updatedQuestionObj);
   					}else if(subauraQustion.questionComplete &&  subauraQustion.questionId===questionid){
   						var arrayAnswer = null;
   			   			if(subauraQustion.answerTest.indexOf(',')!==-1){
   			   				arrayAnswer = subauraQustion.answerTest.split(',');
   			   			}else if(!angular.isUndefined(subauraQustion.answerTest)){
   			   				arrayAnswer=[];
   			   				arrayAnswer[0] = subauraQustion.answerTest;
   			   			}
   			   			angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].auraAnswer, function (answer, answerIndex) {
   			   				console.log(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue));
   			   				if(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue)!==-1){
   			   					vm.checkboxProgressive(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex],answerIndex);
   			   				}
   			   			});
   					}
   				});
   	   		});
   		}else if(baseQuestionObj && baseQuestionObj.questionId===questionid && baseQuestionObj.answerTest===updatedQuestionObj.answerTest ){
   			var arrayAnswer = null;
   			if(baseQuestionObj.answerTest.indexOf(',')!==-1){
   				arrayAnswer = baseQuestionObj.answerTest.split(',');
   			}else if(!angular.isUndefined(baseQuestionObj.answerTest)){
   				arrayAnswer=[];
   				arrayAnswer[0] = baseQuestionObj.answerTest;
   			}
   			angular.forEach(baseQuestionObj.auraAnswer, function (answer, answerIndex) {
   				console.log(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue));
   				if(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue)!==-1){
   					vm.checkboxProgressive(baseQuestionObj,answerIndex);
   				}
   			});

   				//answerID
   				//answerValue
   		}

   	};

   	vm.checkboxProgressive = function(baseQuestionObj, answerIndex ){
      if(baseQuestionObj) {
     		angular.forEach(baseQuestionObj.auraAnswer, function (answer, index) {
     			if(index<answerIndex){
     				angular.forEach(baseQuestionObj.auraAnswer[index].childQuestions, function (subauraQustion, subSelectedIndex) {
     					vm.setprogressiveError(subauraQustion);

     				});
     			}
     		});
      }
   	};




  	vm.searchValSubmit = function (answerValue, questionObj,parentQuestionObj){
  		console.log(questionObj.answerValue.answerValue);
  		console.log(parentQuestionObj);
  		questionObj.arrAns=[];
  		questionObj.arrAns[0] = questionObj.answerValue.answerValue;
  		 vm.auraRes={'questionID':parentQuestionObj.questionId,'auraAnswers':questionObj.arrAns};
  		auraRespSvc.setResponse(vm.auraRes);
		  auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function(response) {
			  vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion,response.data);
		  });
  	};

  	vm.checkPopUp = function (answerValue, questionObj,index){
  		console.log(questionObj.answerTest);
  		vm.noCheck = false;
  		var answertestIsempty = (questionObj.answerTest === '');
  		var answertestMatch =(questionObj.answerTest === '' && answerValue.match('None of the above'));
  		var isquestionIdPresent =(vm.oldQuestionId.indexOf(questionObj.questionId) > -1);
  		if(questionObj.answerTest && !answertestIsempty && !(questionObj.answerTest.match('None of the above')) && !answertestMatch && vm.oldQuestionId.indexOf(questionObj.questionId) > -1  && answerValue.match('None of the above'))
  			{
  			if(!isquestionIdPresent)
				{
				vm.oldQuestionId.push(questionObj.questionId);
				}
  			ngDialog.openConfirm({
  		            template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-12\'><p> Clicking on \'None of the above\' will clear your medical disclosures in the above section.</p><br/><p>Do you wish to proceed?</p> </div> </div></div></br>' + 
  		            '<div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary no-arrow\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
  		            plain: true,
  		            className: 'ngdialog-theme-plain custom-width'
  		        }).then(function(){
  		        	if(vm.oldQuestionId.indexOf(questionObj.questionId) > -1)
  		        		{
  		        	vm.oldQuestionId.splice(vm.oldQuestionId.indexOf(questionObj.questionId),1);
  		        		}
  		        	 vm.updateCheckBox(answerValue, questionObj,index);
  		        }, function(e){
  		        		vm.noCheck = true;
  		        		vm.updateCheckBox(answerValue, questionObj,index);
  		        });
  	    
  			
  			}
  		else
  			{
  			if(!isquestionIdPresent)
  				{
  				vm.oldQuestionId.push(questionObj.questionId);
  				}
  			
  			vm.updateCheckBox(answerValue, questionObj,index);
  			}
  	};
  	
  	 vm.updateCheckBox = function (answerValue, questionObj,index){
  		 console.log(questionObj.answerTest);
  		 if(questionObj.arrAns.length===0){
  			var answerArray = questionObj.answerTest.split(',');
  			if(answerArray.length>1){
  				questionObj.arrAns = answerArray;
  			}else{
  				questionObj.arrAns[0] = questionObj.answerTest;
  			}
  		 }


  		 var addtoArray=true;
  		if(answerValue.indexOf('None of the above') !== -1 && !vm.noCheck){
  			questionObj.arrAns=[];
  			questionObj.arrAns[0]= answerValue;
  		}else{
  			for (var i=0;i<questionObj.arrAns.length;i++){
  				//to remove white space
  				questionObj.arrAns[i] = $.trim(questionObj.arrAns[i]);
  				console.log(questionObj.arrAns[i]);
  				 console.log(answerValue);
  				  if(questionObj.arrAns[i]===answerValue ){
                   	questionObj.arrAns.splice(i, 1);
                   	addtoArray=false;
                  }
            }
            if(addtoArray===true){
           	 questionObj.arrAns.splice(index, 0, answerValue);
            }
  		}
         //removing none of the above, if there are other answers
  		if(questionObj.arrAns.length>1){
  			var k;
  			for (k=0;k<questionObj.arrAns.length;k++){
      			if(questionObj.arrAns[k].indexOf('None of the above') !== -1){
      				questionObj.arrAns.splice(k, 1);
      			}

  			}
  		}
         vm.auraRes={'questionID':questionObj.questionId,'auraAnswers':questionObj.arrAns};
         console.log(JSON.stringify(questionObj.arrAns));
         auraRespSvc.setResponse(vm.auraRes);
         auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function(response) {
    			console.log(response.data);
    			vm.updateQuestionList(questionObj.questionId,response.data.changedQuestion,response.data);

       	}, function () {
       		console.log('failed');
           	});
      	 };

      	vm.checkIncompleteQuestion = function (baseQuestionObj, keepGoing){
       		if(baseQuestionObj.questionComplete && keepGoing){
       			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
       				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
       					if(subauraQustion.questionComplete){
       						vm.setprogressiveError(subauraQustion);
       					}else if(!subauraQustion.questionComplete){
       						vm.keepGoing = false;
       					}
       				});

       	   		});
       		}else if(baseQuestionObj.classType==='HeightQuestion' && (!angular.isUndefined(vm.heightval) && vm.heightval.length>0)){
       			vm.keepGoing = true;
       		}else if(baseQuestionObj.classType==='WeightQuestion' && (!angular.isUndefined(vm.weightVal)  && vm.weightVal.length>0)){
       			vm.keepGoing = true;
       		}else{
       			vm.keepGoing = false;
       		}

      	};

      	 vm.proceedToNext = function () {
      		 //check if questions are answered
      		vm.coverDetails.lastSavedOn = '';
          vm.erroneousSections = [];
      		angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
      			vm.keepGoing = true;
            angular.forEach(vm.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
      				if(vm.keepGoing){
      					vm.checkIncompleteQuestion(vm.auraResponseDataList[selectedIndex].questions[index],vm.keepGoing);
      				}
      			});
            if(!vm.keepGoing) {
              vm.erroneousSections.push(Object.sectionName);
            }
      		});
      		vm.isAuraErroneous = vm.erroneousSections.length > 0;
      		if(!vm.isAuraErroneous){
      			vm.submitHeightWeightQuestion(vm.healthQuestionIndex).then(function() {
      			submitAuraSvc.requestObj(vm.urlList.submitAuraUrl).then(function(response) {
      				vm.auraResponses = response.data;
              if(response.status === 200) {
      				if(vm.auraResponses.overallDecision!=='DCL'){
      					clientMatchSvc.reqObj(vm.urlList.clientMatchUrl).then(function(clientMatchResponse) {
      						if(clientMatchResponse.data.matchFound){
      							vm.clientmatchreason = '';
      							vm.auraResponses.clientMatched = clientMatchResponse.data.matchFound;
      							vm.auraResponses.overallDecision='RUW';
      							vm.coverDetails.appDecision ='RUW';
      								vm.information = clientMatchResponse.data.information;
      								angular.forEach(vm.information, function (infoObj,index) {
      									if(index===vm.information.length-1){
      										vm.clientmatchreason = vm.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+', ';
      										vm.clientmatchreason = vm.clientmatchreason.substring(0,vm.clientmatchreason.lastIndexOf(','));
      									}else{
      										vm.clientmatchreason = vm.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+', ';
      									}

      								});
      							vm.auraResponses.clientMatchReason= vm.clientmatchreason;
      							vm.coverDetails.clientMatchReason =  vm.clientmatchreason;
      							vm.coverDetails.clientMatch =  true; 
      							vm.auraResponses.specialTerm = false;
      							appData.setAppData(vm.coverDetails);
      						} 
      						if(vm.auraResponses.overallDecision === 'ACC'){
      	      					vm.coverDetails.appDecision ='ACC';
      	  		    			appData.setAppData(vm.coverDetails);
      	      				}

          					appData.setChangeCoverAuraDetails(vm.auraResponses);
                    //vm.setHeightWeight();
          					$location.path('/summary');
          				});
      				}else if(vm.auraResponses.overallDecision === 'DCL'){
      					if(vm.coverDetails != null && vm.changeCoverOccDetails != null && vm.deathAddnlDetails != null &&
      		    				vm.tpdAddnlDetails != null && vm.ipAddnlDetails != null && vm.auraResponses != null && vm.personalDetails != null){
      		    			vm.coverDetails.lastSavedOn = '';
      		    			vm.details={};
      		    			vm.details.applicant = vm.changeCoverOccDetails;
      		    			vm.coverDetails.appDecision ='DCL';
      		    			vm.coverDetails.applicant.cover[0].coverReason = vm.auraResponses.deathAuraResons;
      		    			vm.coverDetails.applicant.cover[1].coverReason = vm.auraResponses.tpdAuraResons;
      		    			vm.coverDetails.applicant.cover[2].coverReason = vm.auraResponses.ipAuraResons;
      		    			vm.coverDetails.applicant.cover[0].coverDecision = vm.auraResponses.deathDecision;
      		    			vm.coverDetails.applicant.cover[1].coverDecision = vm.auraResponses.tpdDecision;
      		    			vm.coverDetails.applicant.cover[2].coverDecision = vm.auraResponses.ipDecision;
      		    			vm.coverDetails.applicant.cover[0].coverExclusion = vm.auraResponses.deathExclusions;
      		    			vm.coverDetails.applicant.cover[1].coverExclusion = vm.auraResponses.tpdExclusions;
      		    			vm.coverDetails.applicant.cover[2].coverExclusion = vm.auraResponses.ipExclusions;
      		    			vm.coverDetails.applicant.cover[0].loading = vm.auraResponses.deathLoading;
      		    			vm.coverDetails.applicant.cover[1].loading = vm.auraResponses.tpdLoading;
      		    			vm.coverDetails.applicant.cover[2].loading = vm.auraResponses.ipLoading;
      		    			
      		    			var temp = angular.extend(vm.details,vm.coverDetails);
      		    			var aura = angular.extend(temp,vm.auraResponses);
      		    			var submitObject = angular.extend(aura, vm.personalDetails);
      		    			
      		    			appData.setAppData(vm.coverDetails);
      		    			auraRespSvc.setResponse(submitObject);
      		    			
      		    			console.log(JSON.stringify(submitObject));
      		    			
      		    			submitEapplySvc.submitObj(vm.urlList.submitEapplyUrlNew).then(function(response) {
      		            		appData.setPDFLocation(response.data.clientPDFLocation);
      		            		appData.setNpsUrl(response.data.npsTokenURL);
      		            		$location.path('/changedecline');
      		            	}, function(err){
      		            		console.log('Error saving the cover ' + err);
      		            	});
      		            }
      				}
              }
         		 });
      			}, function(err) {
					console.log(err);
				});
  			}
          console.log(vm.erroneousSections);
          if(vm.erroneousSections.length){
        	  vm.formatErrorMsg();
          }
      	 };

         vm.formatErrorMsg = function() {
          for( var v=0; v < vm.erroneousSections.length; v++) {
            vm.erroneousSections[v] = vm.erroneousSections[v].replace('QG=', '');
          }
         };

      	// Save
      	var appNum;
        appNum = fetchAppNumberSvc.getAppNumber();
      	 vm.saveAura = function (){
      		if(vm.coverDetails != null && vm.changeCoverOccDetails != null &&  vm.deathAddnlDetails != null && vm.tpdAddnlDetails != null && vm.ipAddnlDetails != null &&  vm.personalDetails != null){
				if(vm.heightval && vm.weightVal) {
					vm.submitHeightWeightQuestion(vm.healthQuestionIndex).then(function() {
						vm.saveAuraDetails();
					}, function(err) {
						console.log(err);
					});
				} else {
					vm.saveAuraDetails();
				}
      		}
         };
          
       vm.saveAuraDetails = function() {
			vm.coverDetails.lastSavedOn = 'AuraPage';
			vm.details={};
    		vm.details.applicant={};
    		vm.details.applicant.cover=[];
    			vm.details.applicant.cover[0] = vm.deathAddnlDetails;
    			vm.details.applicant.cover[1]= vm.tpdAddnlDetails;
    			vm.details.applicant.cover[2] = vm.ipAddnlDetails;
    			vm.details.applicant = vm.changeCoverOccDetails;
			var saveAuraObject = angular.extend(vm.details,vm.coverDetails);
			auraRespSvc.setResponse(saveAuraObject);
			saveEapplyData.reqObj(vm.urlList.saveEapplyUrlNew, saveAuraObject).then(function(response) {
				console.log(response.data);
				vm.auraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ appNum +'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
			});
		 };
       vm.auraSaveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
				    template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Application saved </h4><!-- Row starts --><div class=\'row  rowcustom\'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>'+hhText+
				    '</div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = '/landing';
					       $location.path( url );
					       return true;
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};

	   	 vm.updateoldQuestionIdList = function(){

	    		angular.forEach(vm.auraResponseDataList, function (Object,index) {
	   			vm.questionAlias = Object.sectionName;
	   			vm.questionlist = Object.questions;
	   			vm.healthQuestion = vm.auraResponseDataList[index].questions;
	   				 angular.forEach(vm.healthQuestion, function (question) {
	   					 var isanswerEmpty=(question.answerTest === '');
	   					 var isquestionidPresent = (vm.oldQuestionId.indexOf(question.questionId) > -1);
	  					if(question.classType==='CheckBoxQuestion'){
	  						if(question.answerTest && !isanswerEmpty && !(question.answerTest.match('None of the above')) ){

	  							if(!isquestionidPresent)
	  							{
	  							vm.oldQuestionId.push(question.questionId);
	  							}
	  						}

	  					}
	  				});
	    		});


	    	 };
      	vm.clickToOpen = function (hhText) {
      		console.log(hhText);
			var dialog = ngDialog.open({
					template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Helpful hints</h4><!-- Row starts --><div class=\'row rowcustom\' style=\'margin:0px -35px;\'><div class=\'col-sm-12\'><p class=\'aligncenter\'></p><div id=\'tips_text\'>'+hhText+
					'</div><p></p></div></div><!-- Row ends --></div><div class=\'row\'><div class=\'col-sm-4\'></div><div class=\'col-sm-4 col-12\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\' style=\'width: 100%;font-weight: normal !important; font-size: 18px !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;\'>Close</button></div><div class=\'col-sm-4\'></div></div></div>',
					className: 'ngdialog-theme-plain',
					plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
 }
   /*Aura Page Controller Ends*/
})(angular);