/**
 * 
 */
package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

/**
 * @author 599063
 *
 */
public class FundCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -71431848609262560L;
	
	private String corpFundId;
	private String id;
	private String category;
	private String deathReqd;
	private String tpdReqd;
	private String ipReqd;
	private String GlPolicyNum;
	private DeathCover deathCover;
	private TPDCover tpdCover;
	private IPCover ipCover;
	private String createdDate;
	
	public String getCorpFundId() {
		return corpFundId;
	}
	public void setCorpFundId(String corpFundId) {
		this.corpFundId = corpFundId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDeathReqd() {
		return deathReqd;
	}
	public void setDeathReqd(String deathReqd) {
		this.deathReqd = deathReqd;
	}
	public String getTpdReqd() {
		return tpdReqd;
	}
	public void setTpdReqd(String tpdReqd) {
		this.tpdReqd = tpdReqd;
	}
	public String getIpReqd() {
		return ipReqd;
	}
	public void setIpReqd(String ipReqd) {
		this.ipReqd = ipReqd;
	}
	public String getGlPolicyNum() {
		return GlPolicyNum;
	}
	public void setGlPolicyNum(String glPolicyNum) {
		GlPolicyNum = glPolicyNum;
	}
	public DeathCover getDeathCover() {
		return deathCover;
	}
	public void setDeathCover(DeathCover deathCover) {
		this.deathCover = deathCover;
	}
	public TPDCover getTpdCover() {
		return tpdCover;
	}
	public void setTpdCover(TPDCover tpdCover) {
		this.tpdCover = tpdCover;
	}
	public IPCover getIpCover() {
		return ipCover;
	}
	public void setIpCover(IPCover ipCover) {
		this.ipCover = ipCover;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "FundCategory [corpFundId=" + corpFundId + ", id=" + id + ", category=" + category + ", deathReqd="
				+ deathReqd + ", tpdReqd=" + tpdReqd + ", ipReqd=" + ipReqd + ", GlPolicyNum=" + GlPolicyNum
				+ ", deathCover=" + deathCover + ", tpdCover=" + tpdCover + ", ipCover=" + ipCover + ", createdDate="
				+ createdDate + "]";
	}
	
	

	
	
	
}
