/*aura occ upgrade controller*/    
CareSuperApp.controller('auraocc',['$scope', '$location','$timeout','$window','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','ngDialog','urlService',
                                     function($scope, $location,$timeout,$window,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,ngDialog,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	  //$location.path(path);
  	};
  	
 	$scope.proceedNext = function() {
 		$scope.keepGoing = true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true;
  				$scope.keepGoing = false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  				//$scope.go('/workRatingSummary');
  			}
  		});
  		if($scope.keepGoing){
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) { 
  				PersistenceService.setWorkRatingAuraDetails(response.data);
  				//console.log(response.data);
  				$scope.go('/workRatingSummary');
  	  		});
  		}
  		
  		
  	}
 	
 	$scope.coverDetails=PersistenceService.getworkRatingCoverDetails();
 	$scope.personalDetails = persoanlDetailService.getMemberDetails();
 	$scope.workRatingOccDetails=PersistenceService.getWorkRatingCoverOccDetails();
 	
   /* $scope.saveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
	var appNum;
    appNum = PersistenceService.getAppNumber();
 	$scope.saveUpdateAura = function() {
 		$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
 		if($scope.coverDetails != null && $scope.workRatingOccDetails != null && $scope.personalDetails[0] != null){
    		$scope.coverDetails.lastSavedOn = 'AuraUpdatePage';
    		// $scope.auraDetails
    		$scope.details={};
			$scope.details.workRatingOccDetails = $scope.workRatingOccDetails;
    		var temp = angular.extend($scope.coverDetails,$scope.details);
        	var saveUpdateAuraObject = angular.extend(temp, $scope.personalDetails[0]);
        	auraResponseService.setResponse(saveUpdateAuraObject)
	        saveEapply.reqObj().then(function(response) {  
	                console.log(response.data)
	        });
    	}
 	}*/
  	
	$scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
	 
	// added for session expiry
   /* $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
	
	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
  	
	 console.log($scope.coverDetails);
	
	/*$scope.coverDetails = PersistenceService.getChangeCoverDetails();
	console.log($scope.coverDetails);*/
	
	
  	auraInputService.setFund('CARE')
  	auraInputService.setMode('WorkRating') 
  	
  	/////////////////////////
  	/*auraInputService.setAge( moment().diff($scope.coverDetails.dob, 'years')) ; 		
  	auraInputService.setAppnumber($scope.coverDetails.appNum);
  	auraInputService.setDeathAmt($scope.coverDetails.deathLabelAmt)
  	auraInputService.setTpdAmt($scope.coverDetails.tpdLabelAmt)
  	auraInputService.setIpAmt($scope.coverDetails.ipLabelAmt)
  	auraInputService.setWaitingPeriod($scope.coverDetails.waitingPeriod)
  	auraInputService.setBenefitPeriod($scope.coverDetails.benefitPeriod) 
  	
  	auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years') 
  	
  	auraInputService.setGender($scope.coverDetails.gender)
  	auraInputService.setIndustryOcc($scope.coverDetails.industryCode+":"+$scope.coverDetails.occupation)
  	if($scope.coverDetails.citizenQue=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.coverDetails.citizenQue)
  	}
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary($scope.coverDetails.salary)
  	auraInputService.setFifteenHr($scope.coverDetails.fifteenHr) 	
  	auraInputService.setName($scope.coverDetails.name)*/
  	//////////////////////
  	
  	//setting deafult vaues for testing
  	auraInputService.setName($scope.coverDetails.name);
  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ; 	
  //auraInputService.setAge(moment().diff($scope.coverDetails.dob, 'years'));
  	//below values has to be set from quote page, as of now taking from existing insurance
  	auraInputService.setAppnumber($scope.coverDetails.appNum);
  	auraInputService.setDeathAmt(parseInt($scope.coverDetails.deathAmt));
  	auraInputService.setTpdAmt(parseInt($scope.coverDetails.tpdAmt));
  	auraInputService.setIpAmt(parseInt($scope.coverDetails.ipAmt));
  	auraInputService.setWaitingPeriod($scope.coverDetails.waitingPeriod);
  	auraInputService.setBenefitPeriod($scope.coverDetails.benefitPeriod); 
  	if($scope.workRatingOccDetails && $scope.workRatingOccDetails.gender ){
  		auraInputService.setGender($scope.workRatingOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
  //auraInputService.setGender($scope.coverDetails.gender);
  	auraInputService.setIndustryOcc($scope.workRatingOccDetails.industryCode+":"+$scope.workRatingOccDetails.occupation);
  	auraInputService.setCountry('Australia');
  	auraInputService.setSalary($scope.workRatingOccDetails.salary);
  	auraInputService.setFifteenHr($scope.workRatingOccDetails.fifteenHr);
  	auraInputService.setClientname('metaus')
  	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
  	auraInputService.setExistingTerm(false);
  	if($scope.personalDetails[0].memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	}
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
  		$scope.auraResponseDataList = response.data.questions;
  		console.log($scope.auraResponseDataList)
  		angular.forEach($scope.auraResponseDataList, function(Object) {
			$scope.sectionname = Object.questionAlias.substring(3);            			
			 
			});
  	});  	 
  	 
  	 $scope.updateRadio = function (answerValue, questionObj){  		
   		
   		questionObj.arrAns[0]=answerValue;
   		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
   		  auraResponseService.setResponse($scope.auraRes)  		  
   		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {  			 
   			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
   			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
   	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 	  			
   	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
   	  				//branch complete false for previous questions  	  				
   	  				$scope.auraResponseDataList[selectedIndex].error=true;  	  				
   	  			}else if(Object.questionComplete){
   	  				$scope.auraResponseDataList[selectedIndex].error=false;
   	  			}
   	  			
   	  		});
   			  
       	}, function () {
       		//console.log('failed');
       	});
   	 };
  	
  	
}]); 