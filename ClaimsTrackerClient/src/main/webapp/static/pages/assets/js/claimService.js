/* Angular JS services
@ created  Purnachandra k
*/
claimApp.factory('claimFactory', ['$http', function($http) {

    //var urlBase = 'http://localhost:8084/claims/retrieveClaimTestDetails';
	var urlBase = 'https://www.e2e.eapplication.metlife.com.au/services/ClaimServices/';
	var captcheUrl = 'http://ausydetkd03:8084//claims/login';
    var factObj = {};   
    var claimData = {};
    var noOutStanding = true;
    var noCompleted = true;
    var claimId = '';
    var partnerName='';
    var claimStatus = '';
    var uniqueId='';
    var activitiesData ='';
    var labelProperties = '';
    
    var captcheConfig = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
                'Access-Control-Allow-Origin':'http://ausydetkd03:8084'
            }
        }  
    var headerConfig = {
            headers : {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin':'http://ausydetkd03:8084'
            }
        }  
    
    factObj.getClaim = function (user) {  
    	 var data = { claimId: user.claimNo,
    	 	   surName: user.lName,
    		   dob: user.dob};    	
    	 //data = JSON.parse(data);
       return $http.post(urlBase,data,headerConfig);
    };
    
    factObj.getClaimActivities = function (uniqueId) {  
   	// var data = { claimId: uniqueId};    	
   	 //data = JSON.parse(data);
      return $http.get("https://www.e2e.eapplication.metlife.com.au/services/ClaimServices/"+uniqueId);
   };
    
    factObj.getClaimData = function(){    	
        return claimData;
    };
    
    factObj.setClaimData = function(data){
    	claimData = data;
        return claimData;
    };    
    
    factObj.getLabelProperties = function(){    	
        return labelProperties;
    };
    
    factObj.setLabelProperties = function(data){
    	labelProperties = data;
        return labelProperties;
    };
    factObj.getActivitiesData = function(){    	
        return activitiesData;
    };
    
    factObj.setActivitiesData = function(data){
    	activitiesData = data;
        return activitiesData;
    };
    factObj.filterClaimData = function(claimActivities){
    	noOutStanding = true;
    	noCompleted = true;
    	//claimActivities = JSON.parse(claimActivities);
	    for(var i=0; i < claimActivities.length; i++){
	        var activity =  claimActivities[i];
	        if (activity.name == 'Information Requested'  && activity.requirement != null && activity.requirementReceviedDate != null) {           		
	        		noOutStanding = false;
	        		//alert('hii1')
	        		break;           		
	        }	       
	    }
	    for(var i=0; i < claimActivities.length; i++){
	        var activity =  claimActivities[i];
	        if (activity.name == 'Information Requested'  && activity.requirement != null && activity.requirementReceviedDate == null) {           		
	        		noCompleted = false;
	        		//alert('hii2')
	        		break;           		
	        }	       
	    }
    };
    
    factObj.getNoOutStanding = function() {    	
    	return noOutStanding;
    };
    factObj.getNoCompleted = function() {    	
    	return noCompleted;
    };
    
    factObj.verifyCaptche = function (data) {  
    	
        return $http.post(captcheUrl, data,captcheConfig);
     };
     
     factObj.getClaimId = function() {    	
     	return claimId;
     };
     factObj.setClaimId = function(data){
    	 claimId = data;
         return claimId;
     }; 
     
     factObj.getClaimStatus = function() {    	
      	return claimStatus;
      };
      factObj.setClaimStatus = function(data){
    	  claimStatus = data;
    	  if (data.toUpperCase() === "Open".toUpperCase() || data.toUpperCase() === "Admitted".toUpperCase()
    			  || data.toUpperCase() === "Pending".toUpperCase() || data.toUpperCase() === "Re-open".toUpperCase()) {
    		  claimStatus = 'Under Assessment'; 
    	  } else if(data.toUpperCase() === "Closed".toUpperCase() || data.toUpperCase() === "Paid".toUpperCase()
    			  || data.toUpperCase() === "Declined".toUpperCase()) {
    		  claimStatus = 'Decision Complete';
    	  }else if(data.toUpperCase() === "Not Proceeded With".toUpperCase()) {
    		  claimStatus = 'Inactive'; 
    	  }    	  
          return claimStatus;
      }; 
      
      factObj.getPartnerName = function() {    	
       	return partnerName;
       };
       factObj.setPartnerName = function(data){
    	   partnerName = data;
           return partnerName;
       };
       factObj.getUniqueId = function(){    	
           return uniqueId;
       };
       
       factObj.setUniqueId = function(data){
    	   uniqueId = data;
           return uniqueId;
       };
    
    return factObj;
}]);

claimApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
       var fd = new FormData();
       fd.append('file', file);
    
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
    
       .success(function(){
    	//   alert("successfully uploaded");
       })
    
       .error(function(err){
    	//   alert("error while uploading" + JSON.stringify(err));
       });
    }
 }]);

claimApp.service('propertyService',['$http', function($http){
	
	this.getProperties = function(){
		var configObject = {};
		$http.get('./claimTracker_labels.properties').then(function (response) {
	       	var configData = response.data;
	       	configData = configData.split('\n');
	       	var arr = [];
	       	var key;
	       	var newArr = [];       	
	       	
	       	//var obj = {};
	       	for(var i = 0; i < configData.length; i++){
	       		var temp = configData[i].split('=');
	       		newArr = newArr.concat(temp);
	       	}
	       	for(var i = 0; i < newArr.length; i++){
	       		if(i % 2 == 0){
	       			key = newArr[i];
	       		} else if(i % 2 == 1){
	       			var value = newArr[i];
	       			configObject[key] = value;
	       		}
	       	}
	     }, function(err){
	    	 console.log("Error fetching properties " + JSON.stringify(err));
	     });
		return configObject;
	}
}]);