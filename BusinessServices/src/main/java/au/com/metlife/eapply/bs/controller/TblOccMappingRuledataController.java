package au.com.metlife.eapply.bs.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;
import au.com.metlife.eapply.bs.service.TblOccMappingRuledataService;

@RestController
@Component
/*@PropertySource(value = "application.properties")*/
public class TblOccMappingRuledataController {
	
	
	private final TblOccMappingRuledataService tblOccMappingRuledataService;
	
	 @Autowired
	    public TblOccMappingRuledataController(final TblOccMappingRuledataService tblOccMappingRuledataService) {
	        this.tblOccMappingRuledataService = tblOccMappingRuledataService;
	    }
	 
	 @RequestMapping("/getOccupationName")
	  public ResponseEntity<List<TblOccMappingRuledata>> getOccupationName(@RequestParam(value="fundId") String fundId ,@RequestParam(value="induCode") String induCode) {		
		System.out.println(">>>fundId"+fundId+">>>>indcc--"+induCode);
		List<TblOccMappingRuledata> tblOccMappingRuledata =null;
		if(fundId!=null && induCode!=null){
			tblOccMappingRuledata =  tblOccMappingRuledataService.getOccupationList(fundId, induCode);
		}
		
	    if(tblOccMappingRuledata==null){
	        return new ResponseEntity<List<TblOccMappingRuledata>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	    }
	    return new ResponseEntity<List<TblOccMappingRuledata>>(tblOccMappingRuledata, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/generateApplicationNo")
	  public ResponseEntity<String> generateApplicationNo() {		
		java.lang.String retVal = null;
		try {
			Random random = new Random();
			retVal = "" + (long) Math.round(random.nextFloat() * Math.pow(10, 8));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>(retVal, HttpStatus.OK);
	 }
	 
	 

}
