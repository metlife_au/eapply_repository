package vicsSuper_Objects;

import org.openqa.selenium.By;

public class LandingPageObject {

	// Your current cover

		public static final By deathAmount = By.xpath("//*[text()='Sum insured']//following::p[1]");
		public static final By deathUnit = By.xpath("//*[text()='Sum insured']//following::p[2]");


		public static final By applynowDeath = By
				.xpath("//div[@ng-if='!intvalue(deathCover.amount) && renderView']//button[text()='Apply now ']");
		public static final By applynowTPD = By
				.xpath("//div[@ng-if='!intvalue(tpdCover.amount) && renderView']//button[text()='Apply now ']");
		public static final By applynowIP = By
				.xpath("//div[@ng-if='!intvalue(ipCover.amount) && renderView']//button[text()='Apply now ']");

		// Manage your insurance
		public static final By changeYourInsurence = By.xpath("//h4[text()='Change your insurance cover']");
		public static final By savesApplication = By.xpath("//h4[text()='Saved applications']");
		public static final By newMember = By.xpath("//h4[text()='Special offer for new VicSuper EmployeeSaver members']");
		public static final By savedApplications = By.xpath("//h4[text()='Saved applications']");
		public static final By transferYourCover = By.xpath("//h4[text()='Transfer your cover']");
		public static final By lifeEvent = By.xpath("//h4[text()='Increase your cover due to a life event']");
		public static final By fixYourCover = By.xpath("//h4[text()='Change from unit based to fixed cover']");
		public static final By updateOccupation = By.xpath("//h4[contains(text(),'Update your occupation category')]");
		public static final By cancelCover = By.xpath("//h4[contains(text(),'Cancel your cover')]");


}





