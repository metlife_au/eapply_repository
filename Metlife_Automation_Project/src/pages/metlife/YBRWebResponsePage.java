package pages.metlife;

import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.YBRWebResponsePageObjects;

public class YBRWebResponsePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRWebResponsePage webresponse_newapp() {
		webresponse_newapplication();
		return new YBRWebResponsePage(scriptHelper);
	}

	public YBRWebResponsePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void webresponse_newapplication() {
	
	
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			sleep(4000);
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(YBRWebResponsePageObjects.btnNewapp)).click().perform();
			//tryAction(fluentWaitElement(STFTHealthPageObjects.btnNext), "Click", "Apply");
			sleep(1000);
			
			driver.switchTo().alert().accept();
			sleep(1000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
