<!DOCTYPE html>

<html lang="en" ng-app="claimTrackermain">
	<head>
		    
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <meta name="description" content="">
	    
	    <style>
		html{
		display : none ;
		visibility : hidden ;
		}
		</style>
	    
	    <link rel="icon" id="favIconId" ng-if="fundName" href="../../eapply/static/pages/{{fundName}}/img/favicon.ico">
		<title ng-bind="$root.title"> </title>

		<link ng-href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

		
		
		<!-- Vendor files concatenated and minified CSS -->
		<link id='vendorImportId'  href="../../eapply/static/pages/dist/css/vendor-imports.min.css" type="text/css" rel="stylesheet">
		
		<!-- SASS files concatenated and minified CSS -->
		<link id='mainMinId' ng-if="fundName" ng-href="../../eapply/static/pages/{{fundName}}/css/main.min.css" type="text/css" rel="stylesheet">
		
		<!-- user defined css  -->
		<link id ="customId" ng-if="fundName" ng-href="../../eapply/static/pages/{{fundName}}/css/custom.css" type="text/css" rel="stylesheet">
		
		
		<script>
		if( self == top ) {
		document.documentElement.style.display = 'block' ;
		document.documentElement.style.visibility = 'visible' ;
		} else {
		top.location = self.location ;
		}
		</script>
				
		
		<script src="../../eapply/static/pages/assets/js/angular.min.js"></script>
		<script src="../../eapply/static/pages/assets/js/angular-route.js"></script>
		<script src="../../eapply/static/pages/assets/js/angular-resource.min.js"></script>
		
		<script src="../../eapply/static/pages/assets/js/angular-recaptcha.min.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/jquery-1.11.0.min.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/dist/js/output.min.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/assets/js/script.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimcontroller.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimService.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/assets/js/claimDirectives.js" type="text/javascript" ></script>
		<script src="../../eapply/static/pages/dist/js/moment.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/dist/js/fine-uploader.min.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/dist/js/ng-file-upload.js" type="text/javascript"></script>
		<script src="../../eapply/static/pages/dist/js/ng-file-upload-shim.js" type="text/javascript"></script>
		<!-- reCaptcha -->
		<!-- 		<script src='https://www.google.com/recaptcha/api.js'></script> -->
		<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
		<script src="../../eapply/static/common/services/google-analytics-service.js"></script>
		
	</head>

	<body class="login-page" id="caresuper">

	<!-- Global header module starts here -->
		<div  ng-controller="mainPage">
  			<div ng-include src="$root.header.url"></div>
		
		
		
	<!-- end Global header module -->
<div ng-if="headerSection">
<section class="header-secondary">
			<div class="container">
				<div class="row">
					<div class="col col-sm-12" >
						<div class="row"  >
							<div class="title col col-sm-12 col-lg-6" ng-if="claimFactory.getClaimStatus() !=  'Decision Complete' ">
								Claim Status: {{claimFactory.getClaimStatus()}}
							</div>
							<div class="title col col-sm-12 col-lg-6" ng-if="claimFactory.getClaimStatus() == 'Decision Complete' ">
								Claim Status: {{claimFactory.getClaimStatus()}} - The assessment of your claim has been completed. For further information please contact {{claimFactory.getClaimData().fundName}}
							</div>
							<div class="member-info col col-sm-12 col-lg-6">
								Claim Number: {{claimFactory.getClaimId()}} 
							</div>
							<div class="member-info col col-sm-2" ">
								<a href=""  class="btn btn-info btn-lg" ng-click="logout()">Log out </a>
 							</div>
						</div>
					</div>					
			</div>
			</div>			
		</section>	
		
		</div>
		
		<div ng-if="!headerSection && contactHeader">
		<section class="header-secondary">
			<div class="container">
				<div class="row">
					<div class="col col-sm-12" >
						<div class="row"  >						
							<div class="title col col-sm-12 col-lg-6">
								
							</div>
							<div class="member-info col col-sm-12 col-lg-6">
								
							</div>						
							<div  class="member-info col col-sm-2" ">
								<a class="btn btn-info btn-lg" ng-click="back()">Back </a>						
							</div>
					</div>					
			</div>
			</div>			
		</section>	
		</div>
		
		
		
 <div ng-view></div>
	
<div ng-include src="$root.footer.url"></div>
</div>

<!-- Google Analytics code snippet -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60148568-10', 'auto');

</script>	
		
	</body>
</html>
