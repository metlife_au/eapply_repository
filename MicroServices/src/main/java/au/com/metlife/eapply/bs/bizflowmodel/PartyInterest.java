package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyInterest")
@XmlRootElement
public class PartyInterest {
	
	private String RoleCode;
	private InterestedPartyReferences InterestedPartyReferences;
	public InterestedPartyReferences getInterestedPartyReferences() {
		return InterestedPartyReferences;
	}
	public void setInterestedPartyReferences(
			InterestedPartyReferences interestedPartyReferences) {
		InterestedPartyReferences = interestedPartyReferences;
	}
	public String getRoleCode() {
		return RoleCode;
	}
	public void setRoleCode(String roleCode) {
		RoleCode = roleCode;
	}
	
	
	
	
	
}
