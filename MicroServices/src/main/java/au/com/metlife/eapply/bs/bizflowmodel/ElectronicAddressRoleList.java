package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElectronicAddressRoleList")

@XmlRootElement
public class ElectronicAddressRoleList {	
	
	private ElectronicAddressRole ElectronicAddressRole;

	public ElectronicAddressRole getElectronicAddressRole() {
		return ElectronicAddressRole;
	}

	public void setElectronicAddressRole(ElectronicAddressRole electronicAddressRole) {
		ElectronicAddressRole = electronicAddressRole;
	}

	
	
	
}
