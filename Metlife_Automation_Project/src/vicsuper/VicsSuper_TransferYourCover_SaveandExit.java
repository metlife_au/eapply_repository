package vicsuper;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import supportlibraries.ScriptHelper;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class VicsSuper_TransferYourCover_SaveandExit extends MasterPage {
	WebDriverUtil driverUtil = null;

	public VicsSuper_TransferYourCover_SaveandExit TransferCover_Vicsuper_SaveAndExit() {
		TransferCoverSave();
		return new VicsSuper_TransferYourCover_SaveandExit(scriptHelper);
	}
	
	public VicsSuper_TransferYourCover_SaveandExit TransferCover_Vicsuper_Validation() {
		TransferCoverHOSTPLUS_Validation();
		
		return new VicsSuper_TransferYourCover_SaveandExit(scriptHelper);
	}

	public VicsSuper_TransferYourCover_SaveandExit(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	JavascriptExecutor jse = (JavascriptExecutor) driver;
public void TransferCoverSave() {
		String EmailId = dataTable.getData("VicsSuper_TransferCover", "EmailId");
		String TypeofContact = dataTable.getData("VicsSuper_TransferCover", "TypeofContact");
		String ContactNumber = dataTable.getData("VicsSuper_TransferCover", "ContactNumber");
		String TimeofContact = dataTable.getData("VicsSuper_TransferCover", "TimeofContact");
		String Gender = dataTable.getData("VicsSuper_TransferCover", "Gender");
		String FifteenHoursWork = dataTable.getData("VicsSuper_TransferCover", "FifteenHoursWork");
		String Citizen = dataTable.getData("VicsSuper_TransferCover", "Citizen");
		String EducationalInstitution = dataTable.getData("VicsSuper_TransferCover", "EducationalInstitution");
		String EducationalDuties = dataTable.getData("VicsSuper_TransferCover", "EducationalDuties");
	    String TertiaryQual = dataTable.getData("VicsSuper_TransferCover", "TertiaryQual");
		String AnnualSalary = dataTable.getData("VicsSuper_TransferCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("VicsSuper_TransferCover", "PreviousInsurer");
		String PolicyNo = dataTable.getData("VicsSuper_TransferCover", "PolicyNo");
		String FormerFundNo = dataTable.getData("VicsSuper_TransferCover", "FormerFundNo");
		String AttachmentSizeLimitTest	= dataTable.getData("VicsSuper_TransferCover", "AttachmentSizeLimitTest");
		String AttachmentTestPath = dataTable.getData("VicsSuper_TransferCover", "AttachmentTestPath");
		String OverSizeMessage = dataTable.getData("VicsSuper_TransferCover", "OverSizeMessage");
		String AttachPath = dataTable.getData("VicsSuper_TransferCover", "AttachPath");
		String CostType = dataTable.getData("VicsSuper_TransferCover", "CostType");
		String DeathYN = dataTable.getData("VicsSuper_TransferCover", "Death");
		String TPDYN = dataTable.getData("VicsSuper_TransferCover", "TPD");
		String IPYN = dataTable.getData("VicsSuper_TransferCover", "IP");
		String DeathAmount = dataTable.getData("VicsSuper_TransferCover", "DeathAmount");
		String TPDAmount = dataTable.getData("VicsSuper_TransferCover", "TPDAmount");
		String IPAmount = dataTable.getData("VicsSuper_TransferCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("VicsSuper_TransferCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("VicsSuper_TransferCover", "BenefitPeriod");		
		
		try{
			
			
			
			//****Click on Transfer Your Cover Link*****\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Transfer your cover']"))).click();
			report.updateTestLog("Transfer Your Cover", "Clicked on Transfer Your Cover Link", Status.PASS);
			
			
			if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(2000);
		
			}
		
			//*****Agree to Duty of disclosure*******\\
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
			sleep(400);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);
		
			

			
            //*****Agree to Privacy Statement*******\\
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);
			
			sleep(200);
			
            //*****Enter the Email Id*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("coverDetailsTransferEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\			
				
				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
				Contact.selectByVisibleText(TypeofContact);			
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
								
				
			//****Enter the Contact Number****\\	
				
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCTransId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				
			
				//***Select the Preferred time of Contact*****\\			
				
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();
					
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
/*//***********Select the Gender******************\\	
				
				if(Gender.equalsIgnoreCase("Male")){			
					
					driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}	*/
				
				//****Contact Details-Continue Button*****\\			
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(coverDetailsTransferForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);			
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
					//*****Select the 15 Hours Question*******\\		
					
				
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
				
				//*****Resident of Australia****\\	
				
				if(Citizen.equalsIgnoreCase("Yes")){		
					driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
						
					}	
				else{	
					driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
						
					}	
						

				

				// *****Are the duties of your regular occupation limited to
				// either:*****\\

				if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

					// ***Select duties which are undertaken within an Office
					// Environment?\\
					if (EducationalInstitution.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]")))
								.click(); // Dhipika
						report.updateTestLog("Educational institution", "Selected No", Status.PASS);
						sleep(1000);
					}

					// *****Educational duties performed within a school or other
					// educational institution******\\
					if (EducationalDuties.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]")))
								.click(); // Dhipika
						report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]")))
								.click(); // Dhipika
						report.updateTestLog("Educational duties", "Selected No", Status.PASS);
						sleep(1000);
					}

				} else {
					report.updateTestLog("Additional Questions",
							"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

				}

				// Do you hold a tertiary qualification or work in a management role
				if (driver.getPageSource().contains("Do you:")) {

					// *****Do You Hold a Tertiary Qualification******\\
					if (TertiaryQual.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[1]"))).click(); // Dhipika
						report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
						sleep(1000);
					} else {
						wait.until(ExpectedConditions
								.elementToBeClickable(By.xpath("(//input[@name='occgovrating2']/parent::*)[2]"))).click(); // Dhipika
						report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
						sleep(1000);
					}

				} else {
					report.updateTestLog("Additional Questions",
							"Do you hold a tertiary qualification or work in a management role is not present",
							Status.FAIL);
				}
				
						
			//*****What is your annual Salary******							
										
			
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("transferAnnualSal"))).sendKeys(AnnualSalary);
			    sleep(500);		
			    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
										
			
			
			//*****Click on Continue*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(occupationDetailsTransferForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		
			
			//******************************************\\
			//**********PREVIOUS COVER SECTION**********\\
			//*******************************************\\
			
			//*****What is the name of your previous fund or insurer?*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("previousFundName"))).sendKeys(PreviousInsurer);
			sleep(250);
			report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is entered", Status.PASS);
			
            //*****Please enter your fund member or insurance policy number*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("membershipNumber"))).sendKeys(PolicyNo);
			sleep(250);
			report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is entered", Status.PASS);
			
            //*****Please enter your former fund SPIN number (if known)*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("spinNumber"))).sendKeys(FormerFundNo);
			sleep(250);
			report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is entered", Status.PASS);

			//*****Attach documentary evidence****\\
			
		
				System.out.println("Yes");
				
				if(AttachmentSizeLimitTest.equalsIgnoreCase("Yes")){
					
					 //Click Browse
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
					 sleep(2500);
					 System.out.println("look");
					StringSelection stringSelection = new StringSelection(AttachmentTestPath);
					   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
					
					   //native key strokes for CTRL, V and ENTER keys
			            Robot robot = new Robot();
				
			            robot.keyPress(KeyEvent.VK_CONTROL);
			            robot.keyPress(KeyEvent.VK_V);
			            robot.keyRelease(KeyEvent.VK_V);
			            robot.keyRelease(KeyEvent.VK_CONTROL);
			            robot.keyPress(KeyEvent.VK_ENTER);
			            robot.keyRelease(KeyEvent.VK_ENTER);
			            sleep(1000);
				          //Click Add
						 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddId']")).click();
						 sleep(2500);
						
					
					
				/*	System.out.println("Yes");
					properties = Settings.getInstance();	
					String StrBrowseName=properties.getProperty("currentBrowser");
					System.out.println(StrBrowseName);
					//if (StrBrowseName.equalsIgnoreCase("Firefox")){
						 StringSelection sel = new StringSelection(AttachmentTestPath);
						 
						   // Copy to clipboard  
						 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
						 
						 //Click
						 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
						 
						 // Create object of Robot class
						 Robot robot = new Robot();
						 Thread.sleep(1000);
						      
						  // Press Enter
						 robot.keyPress(KeyEvent.VK_ENTER);
						 
						// Release Enter
						 robot.keyRelease(KeyEvent.VK_ENTER);
						 
						  // Press CTRL+V
						 robot.keyPress(KeyEvent.VK_CONTROL);
						 robot.keyPress(KeyEvent.VK_V);
						 
						// Release CTRL+V
						 robot.keyRelease(KeyEvent.VK_CONTROL);
						 robot.keyRelease(KeyEvent.VK_V);
						 Thread.sleep(1000);
						        
						        // Press Enter 
						 robot.keyPress(KeyEvent.VK_ENTER);
						 robot.keyRelease(KeyEvent.VK_ENTER);
						 
						sleep(500);*/
					//}
						
					/*else{
						driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachmentTestPath);
						
					}*/
					
				/*	//*****Let the attachment load*****\\		
					driver.findElement(By.xpath("//label[contains(text(),'Display the cost of my insurance cover as')]")).click();
					sleep(1000);
					
					//*****Add the Attachment*****\\
					
					WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
					add.click();
					//((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
					sleep(1500);*/
					
					String OversizeMessage=driver.findElement(By.xpath("//label[text()='  File size should not be more than 10MB ']")).getText();
					sleep(250);
					String SizeLimit=OversizeMessage.trim();
					
					if(OverSizeMessage.equalsIgnoreCase(SizeLimit)){
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is Displayed", Status.PASS);
					}
					else{
						report.updateTestLog("Attachment Limit Exceeded", OversizeMessage+" is not Displayed", Status.FAIL);
					}
					

					
				}
				
				
				//****Attach the file******	
				sleep(500);
				
				/*properties = Settings.getInstance();	
				String StrBrowseName=properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Firefox")){
					StringSelection sel = new StringSelection(AttachPath);
					 
					   // Copy to clipboard  
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
					 
					 //Click
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
					 
					 // Create object of Robot class
					 Robot robot = new Robot();
					 Thread.sleep(1000);
					      
					  // Press Enter
					 robot.keyPress(KeyEvent.VK_ENTER);
					 
					// Release Enter
					 robot.keyRelease(KeyEvent.VK_ENTER);
					 
					  // Press CTRL+V
					 robot.keyPress(KeyEvent.VK_CONTROL);
					 robot.keyPress(KeyEvent.VK_V);
					 
					// Release CTRL+V
					 robot.keyRelease(KeyEvent.VK_CONTROL);
					 robot.keyRelease(KeyEvent.VK_V);
					 Thread.sleep(1000);
					        
					        // Press Enter 
					 robot.keyPress(KeyEvent.VK_ENTER);
					 robot.keyRelease(KeyEvent.VK_ENTER);
					 
					sleep(500);
				
			}
				
			else{
				driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
				
			}	
				*/
				
				
				
				//Click Browse
				 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddIdBtn']")).click();
				 sleep(2500);
				 System.out.println("look1");
				 System.out.println(AttachPath);
				StringSelection stringSelection1 = new StringSelection(AttachPath);
				   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection1, null);
				
				   //native key strokes for CTRL, V and ENTER keys
		            Robot robot = new Robot();
			
		            robot.keyPress(KeyEvent.VK_CONTROL);
		            robot.keyPress(KeyEvent.VK_V);
		            robot.keyRelease(KeyEvent.VK_V);
		            robot.keyRelease(KeyEvent.VK_CONTROL);
		            robot.keyPress(KeyEvent.VK_ENTER);
		            robot.keyRelease(KeyEvent.VK_ENTER);
		            sleep(1000);
		            //Click Add
					 driver.findElement(By.xpath("//*[@id='transferCvrFileUploadAddId']")).click();
					 sleep(2500);
					 
					 System.out.println("Done");
			//*****Choose the cost of Insurance Type******\\
			
			Select CostofInsurance=new Select(driver.findElement(By.id("coverId")));
			CostofInsurance.selectByVisibleText(CostType);
			sleep(400);
			report.updateTestLog("Cost of Insurance Type", CostType+" is selected", Status.PASS);
			
			
			//****Click on Continue Button******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(previousCoverForm);']"))).click();
			sleep(500);
			report.updateTestLog("Continue", "Continue button is clicked", Status.PASS);
			
			//*************************************************\\
			//**************TRANSFER COVER SECTION***************\\
			//**************************************************\\
			
			//*****Death transfer amount******
			
			if(DeathYN.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.id("dcTransferAmount"))).sendKeys(DeathAmount);
				sleep(500);
				report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is entered", Status.PASS);
			
			}
			
			//*****TPD transfer amount******
			
			if(TPDYN.equalsIgnoreCase("Yes")){
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("tpdTransferAmount"))).sendKeys(TPDAmount);
				sleep(500);
				report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is entered", Status.PASS);
			
			}
			
			//****IP transfer amount*****\\
			
			
			
			if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){
				

			//*****Select The Waiting Period********\\
			
				Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']")));
				waitingperiod.selectByVisibleText(WaitingPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", WaitingPeriod+" is selected", Status.PASS);
			
			
			
			//*****Select The Benefit  Period********\\
			
				Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']")));
				benefitperiod.selectByVisibleText(BenefitPeriod);
				sleep(500);
				report.updateTestLog("IP Waiting Period", BenefitPeriod+" is selected", Status.PASS);

				
			//****Enter the IP amount*****\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("ipTransferAmount"))).sendKeys(IPAmount);
				sleep(500);
				report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is entered", Status.PASS);
				
			}
			
			//****Equivalent units of Hostplus checkbox*****\\
			
			
			//*****Click on calculate Quote*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='CoverDetailsTransferFormSubmit(TranscoverCalculatorForm)']"))).click();
			sleep(3000);
			report.updateTestLog("Calculte Quote", "Calculte Quote button is selected", Status.PASS);
			

			

			//********************************************************************************************\\
			//************Capturing the premiums displayed************************************************\\
			//********************************************************************************************\\
			
//*******Capturing the Death Cost********\\
			
			WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
			Death.click();
			sleep(300);
		//	JavascriptExecutor jse = (JavascriptExecutor) driver.getWebDriver () ;
			jse.executeScript("window.scrollBy(0,-150)", "");
			sleep(500);
			String DeathCost=Death.getText();	
			report.updateTestLog("Death Cover amount", CostType+" Cost is "+DeathCost, Status.SCREENSHOT);
			sleep(500);
			
			//*******Capturing the TPD Cost********\\
			
			WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
			tpd.click();
			sleep(500);
			String TPDCost=tpd.getText();
			report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);report.updateTestLog("TPD Cover amount", CostType+" Cost is "+TPDCost, Status.SCREENSHOT);
			
//*******Capturing the IP Cost********\\
			
			WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
			ip.click();
			sleep(500);
			String IPCost=ip.getText();
			
			report.updateTestLog("IP Cover amount", CostType+" Cost is "+IPCost, Status.SCREENSHOT);
			
			//*****Scroll to the Bottom of the Page
			
		    jse.executeScript("window.scrollBy(0,250)", "");	
			
		  //****Save & Exit******\\
				sleep(800);				
				driver.findElement(By.xpath("(//button[@ng-click='saveQuoteTransfer()'])[1]")).click();
				report.updateTestLog("Save & Exit button", "Save & Exit button is clicked", Status.PASS);
				
			    //****Fetch the Application No******\\

				sleep(800);
				String AppNo=driver.findElement(By.xpath("//div[@id='tips_text']/strong[2]")).getText();
				report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);
		
				//******Finish and Close Window******\\
				
				sleep(500);
			//	quickSwitchWindows();
				driver.findElement(By.xpath("//button[text()='Finish & Close Window ']")).click();
				report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
				sleep(1500); 	 
    				 
	        sleep(4000);

			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

/*private void healthandlifestyledetails() {
	
	String RestrictedIllness = dataTable.getData("VicsSuper_TransferCover", "RestrictedIllness");
	String PriorClaim = dataTable.getData("VicsSuper_TransferCover", "PriorClaim");
	String RestrictedFromWork = dataTable.getData("VicsSuper_TransferCover", "RestrictedFromWork");
	String DiagnosedIllness = dataTable.getData("VicsSuper_TransferCover", "DiagnosedIllness");
	String MedicalTreatment = dataTable.getData("VicsSuper_TransferCover", "MedicalTreatment");
	String DeclinedApplication = dataTable.getData("VicsSuper_TransferCover", "DeclinedApplication");
	String Fundpremloading = dataTable.getData("VicsSuper_TransferCover", "Fundpremloading");
	String Exclusiontext = dataTable.getData("VicsSuper_TransferCover", "Exclusion");
	WebDriverWait wait=new WebDriverWait(driver,20);
		
		try{
			
			if(driver.getPageSource().contains("Eligibility Check")){
			
		//*******Are you restricted due to illness or injury*********\\
			
			if(RestrictedIllness.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[1]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
				sleep(2000);
			
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[1]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
				sleep(2000);
				
			}
			
        //*******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits,
			//Workers� Compensation or any other form of compensation due to illness or injury?*********\\
			
			if(PriorClaim.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[2]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Prior Claim-Yes", Status.PASS);
				sleep(2000);
			
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[2]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Prior Claim-No", Status.PASS);
				sleep(2000);
				
			}	
			
            //*******Have you been restricted from work or unable to perform any of your regular duties for more than 
			//seven consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?*********\\
			
			if(RestrictedFromWork.equalsIgnoreCase("Yes")){		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[3]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-Yes", Status.PASS);
				sleep(2000);
			
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[3]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Restricted From Work-No", Status.PASS);
				sleep(2000);
				
			}	
			
			
		//*****Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy 
	    //to less than 3 years?
			
			if(DiagnosedIllness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[4]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-Yes", Status.PASS);
				sleep(2000);
			}
			else{			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[4] "))).click();
				report.updateTestLog("Health and Lifestyle Page", "Diagnosed with illness-No", Status.PASS);
				sleep(2000);
			}
			
			
			//*****Are you currently contemplating any medical treatment or advice for any illness or injury for which you
			//have not previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating? 
			
			
			if(MedicalTreatment.equalsIgnoreCase("Yes")){				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[5]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-Yes", Status.PASS);
				sleep(2000);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[5]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Medical Treatment-No", Status.PASS);
				sleep(2000);
			}
	
			//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\
			
			if(DeclinedApplication.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[6]"))).click();
				report.updateTestLog("Health and Lifestyle Page", "Declined Application-Yes", Status.PASS);
				sleep(2000);
			}
			else{				
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[6]"))).click();
				sleep(1000);
				report.updateTestLog("Health and Lifestyle Page", "Declined Application-No", Status.PASS);
				sleep(2000);
			}
			
			//*******Is your cover under the previous fund or individual insurer subject to any premium loadings 
			//and/or exclusions, including but not limited to pre-existing conditions exclusions, or restrictions in 
			//regards to medical or other conditions?
			
			if(Fundpremloading.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[7]"))).click();
				sleep(2000);
				report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-Yes", Status.PASS);
				sleep(500);
				  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']"))).sendKeys(Exclusiontext);
				    sleep(1500);	
				    driver.findElement(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']")).click();				  
				
				sleep(1000);
			}
			else{				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[7]"))).click();					
				report.updateTestLog("Health and Lifestyle Page", "Previous fund with Loading or exclusions-No", Status.PASS);
				sleep(2000);
			}
			
		//******Click on Continue Button********\\
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
		     sleep(4000);
		    									
			
			
			}
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}*/

/*private void confirmation() {
	
	//String SaveExit_Confirm=dataTable.getData("ChangeCover_SaveExit", "SaveExit_Confirmation");
	sleep(5000);
	WebDriverWait wait=new WebDriverWait(driver,20);	
		
		try{
			if(driver.getPageSource().contains("I acknowledge and consent to the above.")){
				//******Agree to general Consent*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='option2']/parent::*"))).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(500);
				
		//******Click on Save*******\\
			//if(SaveExit_Confirm.trim().length()>0){
				
				System.out.println("Inside save - confirm");
				sleep(7000);
				driver.findElement(By.xpath("//*[@ng-click='saveTransferSummary()']")).click();
				sleep(7000);
				 report.updateTestLog("Save and Exit", "Save and Exit button is selected", Status.PASS); 
				 report.updateTestLog("Save and Exit", "Fund saved successfully", Status.PASS);
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Finish & Close Window ']"))).click();
				 sleep(2000);	
				 if(driver.getPageSource().contains("Your application reference no")){
						
						WebElement WErefNo = driver.findElement(By.xpath("//*[contains(text(), 'Your application reference no')]"));
						String refNo = WErefNo.getText();

						String[] part = refNo.split("Your application reference no. is ");
						String SE = part[1]; 
						
						String[] part1 = SE.split("\\n\\n");
						String ref = part1[0]; 

					report.updateTestLog("Reference No", "Reference No:"+ref, Status.PASS);							
					driver.findElement(By.xpath("//button[@ng-click='preCloseCallback()']")).click();
					report.updateTestLog("Finish and close", "Finish and close button is clicked", Status.PASS);
					sleep(7000);
						return;
								}
					else {
						System.out.println( "ref no not present");
						report.updateTestLog("Saved And Exit", "Application Saved pop-up does not appear", Status.FAIL);					
					}	
			//}
			else{
				sleep(7000);
				System.out.println("continue - confirm");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
				report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
				sleep(5000);
				
			}
			sleep(5000);				
		}
		
		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
}*/


public void TransferCoverHOSTPLUS_Validation() {

	
	String EmailId = dataTable.getData("VicsSuper_TransferCover", "EmailId");
	String TypeofContact = dataTable.getData("VicsSuper_TransferCover", "TypeofContact");
	String ContactNumber = dataTable.getData("VicsSuper_TransferCover", "ContactNumber");
	String TimeofContact = dataTable.getData("VicsSuper_TransferCover", "TimeofContact");
	String Gender = dataTable.getData("VicsSuper_TransferCover", "Gender");
	String FifteenHoursWork = dataTable.getData("VicsSuper_TransferCover", "FifteenHoursWork");
	String Citizen = dataTable.getData("VicsSuper_TransferCover", "Citizen");
	String EducationalInstitution = dataTable.getData("VicsSuper_TransferCover", "EducationalInstitution");
	String EducationalDuties = dataTable.getData("VicsSuper_TransferCover", "EducationalDuties");
    String TertiaryQual = dataTable.getData("VicsSuper_TransferCover", "TertiaryQual");
	String AnnualSalary = dataTable.getData("VicsSuper_TransferCover", "AnnualSalary");
	String PreviousInsurer = dataTable.getData("VicsSuper_TransferCover", "PreviousInsurer");
	String PolicyNo = dataTable.getData("VicsSuper_TransferCover", "PolicyNo");
	String FormerFundNo = dataTable.getData("VicsSuper_TransferCover", "FormerFundNo");
	String AttachmentSizeLimitTest	= dataTable.getData("VicsSuper_TransferCover", "AttachmentSizeLimitTest");
	String AttachmentTestPath = dataTable.getData("VicsSuper_TransferCover", "AttachmentTestPath");
	String OverSizeMessage = dataTable.getData("VicsSuper_TransferCover", "OverSizeMessage");
	String AttachPath = dataTable.getData("VicsSuper_TransferCover", "AttachPath");
	String CostType = dataTable.getData("VicsSuper_TransferCover", "CostType");
	String DeathYN = dataTable.getData("VicsSuper_TransferCover", "Death");
	String TPDYN = dataTable.getData("VicsSuper_TransferCover", "TPD");
	String IPYN = dataTable.getData("VicsSuper_TransferCover", "IP");
	String DeathAmount = dataTable.getData("VicsSuper_TransferCover", "DeathAmount");
	String TPDAmount = dataTable.getData("VicsSuper_TransferCover", "TPDAmount");
	String IPAmount = dataTable.getData("VicsSuper_TransferCover", "IPAmount");
	String WaitingPeriod = dataTable.getData("VicsSuper_TransferCover", "WaitingPeriod");
	String BenefitPeriod = dataTable.getData("VicsSuper_TransferCover", "BenefitPeriod");
	String Dodcheckbox = dataTable.getData("VicsSuper_TransferCover", "Dodcheckbox");
	String Privacycheckbox = dataTable.getData("VicsSuper_TransferCover", "Privacycheckbox");

	
try{
	
	WebDriverWait wait=new WebDriverWait(driver,20);	
	//*** Retrieving the saved APP**//
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Saved Applications']"))).click();
	sleep(250);
	report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
    sleep(500);
//	String DeathAmountDisplayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-bind='deathCover.amount | currency:undefined:0']"))).getText();
//	String DeathAmount_Type_Displayed=wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-if='isEmpty(deathCover.units)']"))).getText();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[contains(@class,'ng-scope')]//td[contains(text(),'Pending')]/preceding-sibling::td/a[@class='ng-binding']"))).click();
    	report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);					

	
//*****	 Retrieved Confirmation Page*****//
	
	
	
	
	 

	
	 WebElement email=driver.findElement(By.name("coverDetailsTransferEmail"));	 
	 String Email_validation=email.getAttribute("value");	
	 
	 WebElement contacttype=driver.findElement(By.xpath("//select[@ng-model='preferredContactType']"));	 
	 String contacttype_validation=contacttype.getAttribute("value");
     
	 WebElement contactnumber=driver.findElement(By.id("preferrednumCCTransId"));	 
	 String contactnumber_validation=contactnumber.getAttribute("value");
	 
	 WebElement morning=driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*"));	
	 WebElement afternoon=driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*"));	
	 
	
	
			
	/*
	 WebElement AttachmentnameV=driver.findElement(By.xpath("//*[@class='color1 ng-binding']"));	
	 String Attachmentname_validation=AttachmentnameV.getAttribute("value");*/
	 
	 JavascriptExecutor jse = (JavascriptExecutor) driver;
	    sleep(100);
	//	JavascriptExecutor jse = (JavascriptExecutor) driver.getWebDriver ();
		jse.executeScript("window.scrollBy(0,700)", "");
		sleep(100);	
	 
	 if(Dodcheckbox.equalsIgnoreCase("Yes")){
		 WebElement Dod=driver.findElement(By.xpath(".//input[@name='dod']/parent::*"));
		 Dod.isSelected();
			 report.updateTestLog("DOD Check box", "DOD checkbox is selected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("DOD Check box", "DOD checkbox is not selected", Status.FAIL);
			}
	 JavascriptExecutor a = (JavascriptExecutor) driver;
	    sleep(100);
	//	JavascriptExecutor a = (JavascriptExecutor) driver.getWebDriver ();
		a.executeScript("window.scrollBy(0,300)", "");
		sleep(100);	
	 
	 if(Privacycheckbox.equalsIgnoreCase("Yes")){
		 WebElement Privacy=driver.findElement(By.xpath(".//input[@name='privacy']/parent::*"));	
		 Privacy.isSelected();
			 report.updateTestLog("Privacy Check box", "Privacy Statement checkbox is selected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("Privacy Check box", "Privacy Statement checkbox is not selected", Status.FAIL);
			}
	 JavascriptExecutor b = (JavascriptExecutor) driver;
	  sleep(100);
	//	JavascriptExecutor b = (JavascriptExecutor) driver.getWebDriver ();
		b.executeScript("window.scrollBy(0,400)", "");
		sleep(100);	
	 
	 if(EmailId.contains(Email_validation)){
			
			report.updateTestLog("Email ID", EmailId+" is displayed as Expected", Status.PASS);	
		}
		else{
			
			report.updateTestLog("Email ID", EmailId+" is not displayed as expected", Status.FAIL);
		}
	 
	 if(TypeofContact.contains(contacttype_validation)){
			
			report.updateTestLog("TypeofContact", TypeofContact+" is displayed as Expected", Status.PASS);	
		}
		else{
			
			report.updateTestLog("TypeofContact", TypeofContact+" is not displayed as expected", Status.FAIL);
		}
	 
	 if(ContactNumber.contains(contactnumber_validation)){
			
			report.updateTestLog("ContactNumber", ContactNumber+" is displayed as Expected", Status.PASS);	
		}
		else{
			
			report.updateTestLog("ContactNumber", ContactNumber+" is not displayed as expected", Status.FAIL);
		}
	 
	
	 if(TimeofContact.equalsIgnoreCase("Afternoon")){
		  		
		 afternoon.isSelected();
			 report.updateTestLog("Time of contact", TimeofContact+" is displayed as Expected", Status.PASS);	
			}
			else{
				morning.isSelected();
				report.updateTestLog("Time of contact", TimeofContact+" is not displayed as expected", Status.FAIL);
			}
	 
	 if(Gender.equalsIgnoreCase("Male")){		
		 WebElement GenderM=driver.findElement(By.xpath(".//input[@value='Male']/parent::*"));		
		 GenderM.isSelected();
			 report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);	
			}
			else{
				 WebElement GenderF=driver.findElement(By.xpath(".//input[@value='Male']/parent::*"));
				GenderF.isSelected();
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			} 
	 JavascriptExecutor c = (JavascriptExecutor) driver;
	    sleep(100);
	//	JavascriptExecutor c = (JavascriptExecutor) driver.getWebDriver ();
		c.executeScript("window.scrollBy(0,500)", "");
		sleep(100);	
		 
	 /*WebElement salary=driver.findElement(By.id("transferAnnualSal"));	 
	 String Annualsalary_validation=salary.getAttribute("value");*/
	 
	//*****Select the 15 Hours Question*******\\		
		JavascriptExecutor d = (JavascriptExecutor) driver;	
	   sleep(100);
	//	JavascriptExecutor d = (JavascriptExecutor) driver.getWebDriver ();
		d.executeScript("window.scrollBy(0,500)", "");
		sleep(100);	
		if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
			 WebElement FifteenHoursyes=driver.findElement(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[1]"));	
			 FifteenHoursyes.isSelected();
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
					
			}		
		else{		
			 WebElement FifteenHoursno=driver.findElement(By.xpath("(//input[@name='fifteenHrsTransferQuestion']/parent::*)[2]"));			
			 FifteenHoursno.isSelected();
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
					
			}			
	
		
		if(Citizen.equalsIgnoreCase("Yes")){		
			 WebElement Citizenyes=driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[1]"));	
			 Citizenyes.isSelected();
			report.updateTestLog("Citizen or PR", Citizen+"is displayed as Expected", Status.PASS);
				
			}	
		else{	
			 WebElement CitizenNo=driver.findElement(By.xpath("(//input[@name='areyouperCitzTransferQuestion']/parent::*)[2]"));
			 CitizenNo.isSelected();
			report.updateTestLog("Citizen or PR", Citizen+"is not displayed as Expected", Status.PASS);
				
			}	
				 
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
    	
	    if(EducationalInstitution.equalsIgnoreCase("Yes")){							
	   
	    	//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
	    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[1]"))).click();	//Dhipika
			report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
			sleep(1000);					
	}							
	else{							

		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='occRating1a']/parent::*)[2]"))).click();	//Dhipika
		report.updateTestLog("Educational institution", "Selected No", Status.PASS);
		sleep(1000);						
	}							
								
	      //*****EducationalDuties******\\							
								
	        if(EducationalDuties.equalsIgnoreCase("Yes")){							
	       
	        	//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
	        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[1]"))).click();	//Dhipika
	        	report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
				sleep(1000);
	}							
	   else{							
	       
		  // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
		   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='occRating1b']/parent::*)[2]"))).click();	//Dhipika
		   report.updateTestLog("Educational duties", "Selected No", Status.PASS);
			sleep(1000);
	}
												
	//*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){			       

					        	WebElement TertiaryQualyes=driver.findElement(By.xpath("(//input[@name='occgovrating2']/parent::*)[1]"));      
					        	TertiaryQualyes.isSelected();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   WebElement TertiaryQualno=driver.findElement(By.xpath("(//input[@name='occgovrating2']/parent::*)[2]"));   
							 TertiaryQualno.isSelected();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}							
								
	//Current annual salary?
	WebElement objannualSalCCId = driver.findElement(By.id("transferAnnualSal"));
	fieldValidation("WebEdit",objannualSalCCId,AnnualSalary,"AnnualSalary");		
							

/*if(AnnualSalary.contains(Annualsalary_validation)){
				
				report.updateTestLog("Occupation", AnnualSalary+" is displayed as Expected", Status.PASS);	
			}
			else{
				
				report.updateTestLog("Occupation", AnnualSalary+" is not displayed as expected", Status.FAIL);
			}*/
sleep(100);
JavascriptExecutor e = (JavascriptExecutor) driver;
//JavascriptExecutor e = (JavascriptExecutor) driver.getWebDriver ();
e.executeScript("window.scrollBy(0,900)", "");
sleep(100);	
WebElement PreviousInsurerV=driver.findElement(By.name("previousFundName"));	
String PreviousInsurer_validation=PreviousInsurerV.getAttribute("value");

WebElement PolicyNoV=driver.findElement(By.name("membershipNumber"));	
String PolicyNo_validation=PolicyNoV.getAttribute("value");

WebElement FormerFundNoV=driver.findElement(By.name("spinNumber"));	
String FormerFundNo_validation=FormerFundNoV.getAttribute("value");

if(PreviousInsurer.contains(PreviousInsurer_validation)){
	
	report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is displayed as Expected", Status.PASS);
}
else{
	
	report.updateTestLog("Previous Fund or Insurer", "Previous Fund or Insurer: "+PreviousInsurer+" is not displayed as expected", Status.PASS);
}
if(PolicyNo.contains(PolicyNo_validation)){
	
	report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is displayed as Expected", Status.PASS);
}
else{
	
	report.updateTestLog("Previous Insurance Policy No", "Insurance Policy No: "+PolicyNo+" is not displayed as expected", Status.PASS);
}
if(FormerFundNo.contains(FormerFundNo_validation)){
	
	report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is displayed as Expected", Status.PASS);
}
else{
	
	report.updateTestLog("SPIN Number", "SPIN Number: "+FormerFundNo+" is not displayed as expected", Status.PASS);
}

WebElement CosttypeV=driver.findElement(By.id("coverId"));	
String Costtype_validation=CosttypeV.getAttribute("value");

if(CostType.contains(Costtype_validation)){
	
	report.updateTestLog("Cost of Insurance Type", CostType+" is displayed as Expected", Status.PASS);
}
else{
	
    report.updateTestLog("Cost of Insurance Type", CostType+" is not displayed as Expected", Status.PASS);
}



sleep(100);
JavascriptExecutor f = (JavascriptExecutor) driver;
//JavascriptExecutor f = (JavascriptExecutor) driver.getWebDriver ();
f.executeScript("window.scrollBy(0,1000)", "");

//*************************************************\\
//**************TRANSFER COVER SECTION***************\\
//**************************************************\\

//*****Death transfer amount******

if(DeathYN.equalsIgnoreCase("Yes")){
	WebElement DeathamountV=driver.findElement(By.id("dcTransferAmount"));	
	String Deathamount_validation=DeathamountV.getAttribute("value");
	if(DeathAmount.contains(Deathamount_validation)){
		
		report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is displayed as Expected", Status.PASS);
	}
	else{
		
		report.updateTestLog("Death Cover Transfer Amount", DeathAmount+" is not displayed as Expected", Status.PASS);
	}



}

//*****TPD transfer amount******

if(TPDYN.equalsIgnoreCase("Yes")){
	WebElement TPDAmountV=driver.findElement(By.id("tpdTransferAmount"));	
	String TPDAmount_validation=TPDAmountV.getAttribute("value");	
if(TPDAmount.contains(TPDAmount_validation)){
		
	report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is displayed as Expected", Status.PASS);
	}
	else{
		
		report.updateTestLog("TPD Cover Transfer Amount", TPDAmount+" is not displayed as Expected", Status.PASS);
	}

	

}

//****IP transfer amount*****\\



if(IPYN.equalsIgnoreCase("Yes")&&FifteenHoursWork.equalsIgnoreCase("Yes")){
	
	WebElement WaitingperiodV=driver.findElement(By.xpath("//select[@ng-model='waitingPeriodTransPer']"));	
	String Waitingperiod_validation=WaitingperiodV.getAttribute("value");

	WebElement BenefitperiodV=driver.findElement(By.xpath("//select[@ng-model='benefitPeriodTransPer']"));	
	String Benefitperiod_validation=BenefitperiodV.getAttribute("value");

	WebElement IPAmountV=driver.findElement(By.id("ipTransferAmount"));	
	String IPAmount_validation=IPAmountV.getAttribute("value");
	
	
	if(WaitingPeriod.contains(Waitingperiod_validation)){
		
		report.updateTestLog("TPD Cover Transfer Amount", WaitingPeriod+" is displayed as Expected", Status.PASS);
		}
		else{
			
			report.updateTestLog("TPD Cover Transfer Amount", WaitingPeriod+" is not displayed as Expected", Status.PASS);
		}
	
	if(BenefitPeriod.contains(Benefitperiod_validation)){
		
		report.updateTestLog("IP Waiting Period", BenefitPeriod+" is displayed as Expected", Status.PASS);
		}
		else{
			
		report.updateTestLog("IP Waiting Period", BenefitPeriod+" is not displayed as Expected", Status.PASS);
		}
	
	
	if(IPAmount.contains(IPAmount_validation)){
		
		report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is displayed as Expected", Status.PASS);
		}
		else{
			
			report.updateTestLog("IP Cover Transfer Amount", IPAmount+" is not displayed as Expected", Status.PASS);
		}

	}
}catch(Exception e){
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
}
}
