package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesCCConfirmPageObjects extends PageObjectLibrary {
	public static final By selTitle = getObjectIdentifaction("coles.cc.confirm.title.dropdown.css");
	public static final By txtLastName = getObjectIdentifaction("coles.cc.confirm.lastname.text.id");
	public static final By chkkDeclarationAgree = getObjectIdentifaction("coles.cc.confirm.declaration.checkbox.css");
	public static final By lnkNext = getObjectIdentifaction("coles.cc.confirm.next.link.css");
}
