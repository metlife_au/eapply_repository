package au.com.metlife.eapply.ct.serviceimpl;


import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.ct.dto.CTHistoryDto;
import au.com.metlife.eapply.ct.model.CTHistoryBO;
import au.com.metlife.eapply.ct.model.ClaimsTrackerHistory;
import au.com.metlife.eapply.ct.model.LoginAttemptRepository;
import au.com.metlife.eapply.ct.service.LoginAttemptsService;
@Service
public class LoginAttemptsServiceImpl implements LoginAttemptsService{
	
	 private final LoginAttemptRepository repository;
	 private final ClaimsTrackerHistory ctRepository;
	 
		private static final Logger log = LoggerFactory.getLogger(LoginAttemptRepository.class);

		 
		 @Autowired
		    public LoginAttemptsServiceImpl(final LoginAttemptRepository repository,final ClaimsTrackerHistory ctRepository) {
		        this.repository = repository;
		        this.ctRepository = ctRepository;
		    }

	@Override
	public au.com.metlife.eapply.ct.dto.LoginAttempts retrieveLoginAttempts(au.com.metlife.eapply.ct.dto.LoginAttempts loginAttempts) {
		log.info("Retrive login attempts start");
		au.com.metlife.eapply.ct.model.LoginAttempts loginBo= repository.findByIpAddress(loginAttempts.getIpAddress());
		if (null != loginBo) {
			loginAttempts.setAttempts(loginBo.getAttempts());
			loginAttempts.setIpLocked(loginBo.getIpLocked());
			loginAttempts.setLastLogin(loginBo.getLastLogin());
		}
		log.info("Retrive login attempts finish");
		return loginAttempts;
	}

	@Override
	public void saveOrUpdateLoginAttempts(au.com.metlife.eapply.ct.dto.LoginAttempts loginAttempts) {
		log.info("Save or update login attempts start");
		au.com.metlife.eapply.ct.model.LoginAttempts loginBo = null;
		loginBo = repository.findByIpAddress(loginAttempts.getIpAddress());
		if (null == loginBo) {
			loginBo = new au.com.metlife.eapply.ct.model.LoginAttempts();
			loginBo.setAttempts(loginAttempts.getAttempts());
			loginBo.setIpLocked(loginAttempts.getIpLocked());
			loginBo.setIpAddress(loginAttempts.getIpAddress());
			loginBo.setLastLogin(new Date());
		} else {
			loginBo.setAttempts(loginAttempts.getAttempts());
			loginBo.setIpLocked(loginAttempts.getIpLocked()); 
			loginBo.setLastLogin(new Date());
		}
		log.info("Save or update login attempts finish");
		repository.save(loginBo);	
	}

	@Override
	public void saveCTHistory(CTHistoryDto history) {
		log.info("Save ct history start");
		CTHistoryBO historyBO = new CTHistoryBO();
		historyBO.setClaimNum(history.getClaimNum());
		historyBO.setFundName(history.getFundName());
		historyBO.setActivityName(history.getActivityName());
		historyBO.setStatus(history.getStatus());
		historyBO.setAccessedDate(new Date());
		ctRepository.save(historyBO);
		log.info("Save ct history finish");
		
	}

}
