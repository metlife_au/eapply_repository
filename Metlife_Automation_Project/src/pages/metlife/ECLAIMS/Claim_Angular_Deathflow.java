package pages.metlife.ECLAIMS;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;

public class Claim_Angular_Deathflow extends MasterPage {
	WebDriverUtil driverUtil = null;

	public Claim_Angular_Deathflow E_Deathclaims() {
		Eclaims_Deathflow();
		Eclaims_Reviewclaims();
		return new Claim_Angular_Deathflow(scriptHelper);
	}
	
	public Claim_Angular_claimtype_life E_Deathclaims_nagativescenario() {
		Eclaims_Death_negativescenario();
			
		return new Claim_Angular_claimtype_life(scriptHelper);
	}
	
	public Claim_Angular_Deathflow(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

 public void Eclaims_Deathflow(){
	    String ClaimType = dataTable.getData("Eclaims_death", "ClaimType");
		String Creditcard = dataTable.getData("Eclaims_death", "Creditcard");
		String Title = dataTable.getData("Eclaims_death", "Title");
		String DOB = dataTable.getData("Eclaims_death", "DOB");
		String Name = dataTable.getData("Eclaims_death", "Name");
		String Surname = dataTable.getData("Eclaims_death", "Surname");
		String Address=dataTable.getData("Eclaims_death", "Address1");
		String Addresss=dataTable.getData("Eclaims_death", "Address2");
		String suburb=dataTable.getData("Eclaims_death", "suburb");	
		String state = dataTable.getData("Eclaims_death", "State");
		String postcode = dataTable.getData("Eclaims_death", "Postcode");
		String Relationship = dataTable.getData("Eclaims_death", "Relationship");
		String givenTitle = dataTable.getData("Eclaims_death", "givenTitle");
		String Givenname = dataTable.getData("Eclaims_death", "Givenname");
	    String Givensurname = dataTable.getData("Eclaims_death", "Givensurname");
	    String PreferredPhoneNO = dataTable.getData("Eclaims_death", "PreferredPhoneNO");
		String Otherphoneno = dataTable.getData("Eclaims_death", "Otherphoneno");
		String Email = dataTable.getData("Eclaims_death", "Email");
		String Preferredmode = dataTable.getData("Eclaims_death", "Preferredmode");
		String Contacttime = dataTable.getData("Eclaims_death", "Contacttime");
	    String DateofDeath = dataTable.getData("Eclaims_death", "DateofDeath");	    
	    String CauseofDeath = dataTable.getData("Eclaims_death", "CauseofDeath");
		String Deathcause = dataTable.getData("Eclaims_death", "Deathcause");
		String Medicalpractitionername = dataTable.getData("Eclaims_death", "Medicalpractitionername");
	    String Medicalparctionerphn = dataTable.getData("Eclaims_death", "Medicalpractitionerphoneno");
	    String practitioneradd = dataTable.getData("Eclaims_death", "practitioneradd");
	  	String practitioneraddress = dataTable.getData("Eclaims_death", "practitioneraddress");
	  	String Deathcertificate = dataTable.getData("Eclaims_death", "Deathcertificate");
		String Attachpath = dataTable.getData("Eclaims_death", "Attachpath");
	 	String Month = dataTable.getData("Eclaims_death", "Month");
		String Year = dataTable.getData("Eclaims_death", "Year");
	  	String Addresssame=dataTable.getData("Eclaims_death", "Addresssame");
        String Product=dataTable.getData("Eclaims_death", "Product");
		String Otherdocs=dataTable.getData("Eclaims_death", "Otherdocument");
        		

	    
		
		try{				
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			
			//****Click on claim type*****\\
			
				
			 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-DEATH']/parent::*"))).click();
			 sleep(500);
			 report.updateTestLog("Death claim", "Death claim is Selected", Status.PASS);
		
			
			//****Enter creditcard number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
			report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",  Creditcard+" is entered" , Status.PASS);
	
			
			
            //****Select title*****\\
			
			Select titletype=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));		
			titletype.selectByVisibleText(Title);				
			report.updateTestLog("Enter Title:", Title+" is selected ", Status.PASS);
	
			
			//****Enter DOB*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2"))).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2"))).sendKeys(DOB);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Given name ']"))).click();
			
			report.updateTestLog("Enter Date of birth :", DOB+" is entered", Status.PASS);
			
			
			//****Enter Name*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0"))).sendKeys(Name);
			report.updateTestLog("Enter Given name:", Name+" is entered", Status.PASS);
			
			
			//****Enter surname*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1"))).sendKeys(Surname);
			report.updateTestLog("Enter Surname name:", Surname+" is entered", Status.PASS);
			
			
			//****Enter Address1*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0"))).sendKeys(Address);
			report.updateTestLog("Enter Given Address1:", Address+" is entered", Status.PASS);
			
			//****Enter Address2*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1"))).sendKeys(Addresss);
			report.updateTestLog("Enter Given Address2:", Addresss+" is entered", Status.PASS);
			
			
			//****Enter Suburb*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0"))).sendKeys(suburb);
			report.updateTestLog("Enter Suburb:", suburb+" is entered", Status.PASS);			
						
			
			//****Select State*****\\
			
			Select Stateselect=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));		
			Stateselect.selectByVisibleText(state);			
			report.updateTestLog("Select State:", state+" is selected ", Status.PASS);
		    sleep(500);
			
			//****Enter Postal code*****\\
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2"))).sendKeys(postcode);
			sleep(500);			
			report.updateTestLog("Enter Post code:", postcode+" is entered", Status.PASS);
			sleep(300);
			
						
	                          	//*****	Claimant details****\\
			
			
			
				//****Relationship to insured******\\
			
			Select Relation=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ400_0")));		
			Relation.selectByVisibleText(Relationship);			
			report.updateTestLog("Select Relationship to insured:", Relationship+" is selected ", Status.PASS);		
			
			
				//****Select Title*****\\
			
			Select titl=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ401_2")));		
			titl.selectByVisibleText(givenTitle);			
			report.updateTestLog("Select Title:", Title+" is selected ", Status.PASS);
			sleep(500);
						
			//****Enter Given name*****\\
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ403_0"))).sendKeys(Givenname);
			report.updateTestLog("Enter Given name:", Givenname+" is entered", Status.PASS);
			sleep(500);
			
			//****Enter surname*****\\
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ404_1"))).sendKeys(Givensurname);
			report.updateTestLog("Enter Surname name:", Givensurname+" is entered", Status.PASS);
			sleep(500);
			
			
			//****Is your address the same as the primary account holder?*****\\
			if(Addresssame.equalsIgnoreCase("Yes")){
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-Yes']/parent::*"))).click();
			sleep(500);
			report.updateTestLog("Address same is selected", "Selected Yes", Status.PASS);
			sleep(2000);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-No']/parent::*"))).click();
				sleep(500);
				report.updateTestLog("Address same as primary accont hoder", "Selected No", Status.PASS);
				sleep(500);
				
				//****Enter Address1*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ409_1"))).sendKeys(Address);
				report.updateTestLog("Enter Given Address1:", Address+" is entered", Status.PASS);
				
				//****Enter Address2*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ410_2"))).sendKeys(Addresss);
				report.updateTestLog("Enter Given Address2:", Addresss+" is entered", Status.PASS);
				
				
				//****Enter Suburb*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ411_0"))).sendKeys(suburb);
				report.updateTestLog("Enter Suburb:", suburb+" is entered", Status.PASS);			
							
				
				//****Select State*****\\
				
				Select Stateprimaryselect=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ412_1")));		
				Stateprimaryselect.selectByVisibleText(state);			
				report.updateTestLog("Select State:", state+" is selected ", Status.PASS);
			    sleep(500);
				
				//****Enter Postal code*****\\
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ413_2"))).sendKeys(postcode);
				sleep(500);			
				report.updateTestLog("Enter Post code:", postcode+" is entered", Status.PASS);
				sleep(300);
			}
			
			//****Enter Emailaddress*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ402_0"))).sendKeys(Email);
			report.updateTestLog("Enter Email:", Email+" is entered", Status.PASS);
			
			
			//****Enter Peferred phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ415_1"))).sendKeys(PreferredPhoneNO);
			sleep(500);
			report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO+" is entered", Status.PASS);
			sleep(500);
			
			//****Enter other phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ414_2"))).sendKeys(Otherphoneno);
			sleep(500);
			report.updateTestLog("Enter otherphonenumber:", Otherphoneno+" is entered", Status.PASS);
			sleep(500);
			
			
			
			
			//****Preferred mode of contact (Phone / Email / Letter)?*****\\
			
			
			if(Preferredmode.equalsIgnoreCase("Phone")){
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*"))).click();
		    sleep(500);
		    report.updateTestLog("Preferred Mode of Contact ", Preferredmode+" is Selected ", Status.PASS);	
					
				
			Select mode=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ417_1")));		
			mode.selectByVisibleText(Contacttime);			
			report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):", Contacttime+" is selected ", Status.PASS);
			sleep(500);
			}
			if(Preferredmode.equalsIgnoreCase("Email")){
			//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*"))).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[3]/div/div/div[3]/div[2]/form/ng-form/div[4]/ng-form/div[5]/ng-form/div[7]/ng-form/div[1]/div/div/div/div/label[2]"))).click();
				
			    report.updateTestLog("Preferred Mode of Contact ", Preferredmode+" is Selected ", Status.PASS);	
			}
				if(Preferredmode.equalsIgnoreCase("Letter")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*"))).click();
			    sleep(500);
			    report.updateTestLog("Preferred Mode of Contact ", Preferredmode+" is Selected ", Status.PASS);	
			}
		
				
			
									
			//***************Death Details***************//
			
				
			//****Enter Date of death******\\	
		   
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ500_0"))).sendKeys(DateofDeath);
			sleep(500);			
			report.updateTestLog("Enter Date of birth :", DateofDeath+" is entered", Status.PASS);
		

		
 	

			//****Cause of death*****\\
			sleep(500);
			Select deathcause=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ501_0")));				
			deathcause.selectByVisibleText(CauseofDeath);	
			sleep(500);
			report.updateTestLog("Death cause:", CauseofDeath+" is selected ", Status.PASS);
			
			//****Description of death cause******\\
			
			sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input-datalist_CQ502_1"))).sendKeys(Deathcause);
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Cause of Death ']"))).click();		
			report.updateTestLog("Description of death cause*:", Deathcause+" is entered", Status.PASS);		
	
			
			
			//****Proceed button******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary w100p']"))).click();
			sleep(500);
			report.updateTestLog("Proceed Button", "clicked", Status.PASS);	
			
	
			
	                              		//***Additional Details*****\\
			
			
			//****Medical practitioner name*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10048_0"))).sendKeys(Medicalpractitionername);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner name:", Medicalpractitionername+" is entered", Status.PASS);			
			
			
			//****Medical practitioner phone number******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10049_1"))).sendKeys(Medicalparctionerphn);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner phone number :", Medicalparctionerphn+" is entered", Status.PASS);		
			
			
			//****Medical practitioner address 1******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10050_0"))).sendKeys(practitioneradd);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner address 1:", practitioneradd+" is entered", Status.PASS);		
			
			
			//****Medical practitioner address 2******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_input_CQ10051_1"))).sendKeys(practitioneraddress);
			sleep(500);
			report.updateTestLog("Enter Medical practitioner address 2:", practitioneraddress+" is entered", Status.PASS);		

			
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,800)", "");
			sleep(500);
			
			//****Attachpath******\\
			
			if(Deathcertificate.equalsIgnoreCase("Yes")){		

				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("vm.additionalDetailsForm_upload_CQ900_1"))).sendKeys(Attachpath);		
				report.updateTestLog("Document attachment", "Death Certificate is attached", Status.PASS);
				sleep(1000);						
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']"))).click();
				sleep(2000);	

			}
			if(Otherdocs.equalsIgnoreCase("Yes")){
	        	sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.additionalDetailsForm_upload_CQ909_4"))).sendKeys(Attachpath);		
				report.updateTestLog("Document attachment", "Other document is attached", Status.PASS);
				sleep(2000);		
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='attach_action_spanid']"))).click();
				sleep(2000);
				report.updateTestLog("Attached file", "Attach Button is Selected", Status.PASS);	
				
			}
			if(Deathcertificate.equalsIgnoreCase("No")&&Otherdocs.equalsIgnoreCase("No")){
			
              //***Agree button**//
	
			wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
			sleep(500);
			report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);
	
			}
			JavascriptExecutor jsse = (JavascriptExecutor) driver;
			jsse.executeScript("window.scrollBy(0,100)", "");
			sleep(500);
			//****Next button******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
				sleep(500);
				report.updateTestLog("Next Button:", "Clicked next button", Status.PASS);			
		sleep(1000);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
 
}
 
		public void Eclaims_Reviewclaims(){
			
			try{
			//********Field validation*******//
			WebDriverWait wait=new WebDriverWait(driver,18);
			
			    String ClaimType = dataTable.getData("Eclaims_death", "ClaimType");
				String Creditcard = dataTable.getData("Eclaims_death", "Creditcard");
				String Title = dataTable.getData("Eclaims_death", "Title");
				String DOB = dataTable.getData("Eclaims_death", "DOB");
				String Name = dataTable.getData("Eclaims_death", "Name");
				String Surname = dataTable.getData("Eclaims_death", "Surname");
				String Address=dataTable.getData("Eclaims_death", "Address1");
				String Addresss=dataTable.getData("Eclaims_death", "Address2");
				String suburb=dataTable.getData("Eclaims_death", "suburb");	
				String state = dataTable.getData("Eclaims_death", "State");
				String postcode = dataTable.getData("Eclaims_death", "Postcode");
				String Relationship = dataTable.getData("Eclaims_death", "Relationship");
				String givenTitle = dataTable.getData("Eclaims_death", "givenTitle");
				String Givenname = dataTable.getData("Eclaims_death", "Givenname");
			    String Givensurname = dataTable.getData("Eclaims_death", "Givensurname");
			    String PreferredPhoneNO = dataTable.getData("Eclaims_death", "PreferredPhoneNO");
				String Otherphoneno = dataTable.getData("Eclaims_death", "Otherphoneno");
				String Email = dataTable.getData("Eclaims_death", "Email");
				String Preferredmode = dataTable.getData("Eclaims_death", "Preferredmode");
				String Contacttime = dataTable.getData("Eclaims_death", "Contacttime");
			    String DateofDeath = dataTable.getData("Eclaims_death", "DateofDeath");	    
			    String CauseofDeath = dataTable.getData("Eclaims_death", "CauseofDeath");
				String Deathcause = dataTable.getData("Eclaims_death", "Deathcause");
				String Medicalpractitionername = dataTable.getData("Eclaims_death", "Medicalpractitionername");
			    String Medicalparctionerphn = dataTable.getData("Eclaims_death", "Medicalpractitionerphoneno");
			    String practitioneradd = dataTable.getData("Eclaims_death", "practitioneradd");
			  	String practitioneraddress = dataTable.getData("Eclaims_death", "practitioneraddress");
			  	String Deathcertificate = dataTable.getData("Eclaims_death", "Deathcertificate");
				String Attachpath = dataTable.getData("Eclaims_death", "Attachpath");
			 	String Month = dataTable.getData("Eclaims_death", "Month");
				String Year = dataTable.getData("Eclaims_death", "Year");
			  	String Addresssame=dataTable.getData("Eclaims_death", "Addresssame");
		        String Product=dataTable.getData("Eclaims_death", "Product");	
		        String Otherdocs=dataTable.getData("Eclaims_death", "Otherdocument");
		
			WebElement product=driver.findElement(By.xpath("//label[text()='Product ']/following-sibling::label"));
			WebElement claimtype=driver.findElement(By.xpath("//label[text()='Claim Type ']/following-sibling::label"));
			WebElement creditcard=driver.findElement(By.xpath("//label[text()='Credit card / facility no. (Please enter the last 4 digits) ']/following-sibling::label"));
			WebElement title=driver.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[1]"));
			WebElement Dob=driver.findElement(By.xpath("//label[text()='Date of birth (dd/mm/yyyy) ']/following-sibling::label"));
			WebElement GivenName=driver.findElement(By.xpath("(//label[text()='Given name ']/following-sibling::label)[1]"));
			WebElement SurName=driver.findElement(By.xpath("(//label[text()='Surname ']/following-sibling::label)[1]"));
			WebElement Address1=driver.findElement(By.xpath("(//label[text()='Address1 ']/following-sibling::label)[1]"));
			WebElement Address2=driver.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[1]"));
			WebElement Suburb=driver.findElement(By.xpath("(//label[text()='Suburb ']/following-sibling::label)[1]"));
			WebElement State=driver.findElement(By.xpath("(//label[text()='State ']/following-sibling::label)[1]"));
			WebElement postcodes=driver.findElement(By.xpath("(//label[text()='Post Code ']/following-sibling::label)[1]"));
			WebElement Relationships=driver.findElement(By.xpath("//label[text()='Relationship to insured person ']/following-sibling::label"));
			WebElement Titles=driver.findElement(By.xpath("(//label[text()='Title ']/following-sibling::label)[2]"));
			WebElement claimantGivenname=driver.findElement(By.xpath("(//label[text()='Given name ']/following-sibling::label)[2]"));
			WebElement claimantsurname=driver.findElement(By.xpath("(//label[text()='Surname ']/following-sibling::label)[2]"));
			WebElement Adresssame=driver.findElement(By.xpath("//label[text()='Is your address the same as the primary account holder? ']/following-sibling::label"));
			WebElement claimantaddress1=driver.findElement(By.xpath("(//label[text()='Address1 ']/following-sibling::label)[2]"));
			WebElement claimantaddress2=driver.findElement(By.xpath("(//label[text()='Address 2 ']/following-sibling::label)[2]"));
			WebElement claimantsuburb=driver.findElement(By.xpath("(//label[text()='Suburb ']/following-sibling::label)[2]"));
			WebElement claimantstate=driver.findElement(By.xpath("(//label[text()='State ']/following-sibling::label)[2]"));
			WebElement claimantpostalcode=driver.findElement(By.xpath("(//label[text()='Post Code ']/following-sibling::label)[2]"));
			WebElement Preferredphoneno=driver.findElement(By.xpath("//label[text()='Preferred phone no ']/following-sibling::label"));
			WebElement Otherphonenos=driver.findElement(By.xpath("//label[text()='Other phone no ']/following-sibling::label"));
			WebElement Emailaddress=driver.findElement(By.xpath("//label[text()='Email address ']/following-sibling::label"));

			WebElement Dateofdeath=driver.findElement(By.xpath("//label[text()='Date of death ']/following-sibling::label"));
			WebElement causeofdeath=driver.findElement(By.xpath("//label[text()='Cause of death ']/following-sibling::label"));
			WebElement descofdeath=driver.findElement(By.xpath("//label[text()='Description of death cause ']/following-sibling::label"));
			WebElement practitionername=driver.findElement(By.xpath("//label[text()='Medical practitioner name ']/following-sibling::label"));
			WebElement practitionerphoneno=driver.findElement(By.xpath("//label[text()='Medical practitioner phone number ']/following-sibling::label"));
			WebElement practitioneraddress1=driver.findElement(By.xpath("//label[text()='Medical practitioner address 1 ']/following-sibling::label"));
			WebElement practitioneraddress2=driver.findElement(By.xpath("//label[text()='Medical practitioner address 2 ']/following-sibling::label"));
	
			String product_validation=product.getText();
			String claimtype_validation=claimtype.getText();
			String creditcard_validation=creditcard.getText();
			String title_validation=title.getText();
			String Dob_validation=Dob.getText();
			String GivenName_validation=GivenName.getText();
			String SurName_validation=SurName.getText();
			String Address1_validation=Address1.getText();
			String Address2_validation=Address2.getText();
			String Suburb_validation=Suburb.getText();
			String State_validation=State.getText();
			String postcode_validation=postcodes.getText();
			String Relationship_validation=Relationships.getText();
			String Title_validation=Titles.getText();
			String claimantGivenname_validation=claimantGivenname.getText();
			String claimantsurname_validation=claimantsurname.getText();
			String Adresssame_validation=Adresssame.getText();
			String claimantaddress1_validation=claimantaddress1.getText();
			String claimantaddress2_validation=claimantaddress2.getText();
			String claimantsuburb_validation=claimantsuburb.getText();
			String claimantstate_validation=claimantstate.getText();
			String claimantpostalcode_validation=claimantpostalcode.getText();
			String Preferredphoneno_validation=Preferredphoneno.getText();
			String Otherphoneno_validation=Otherphonenos.getText();
			String Emailaddress_validation=Emailaddress.getText();
			String Dateofdeath_validation=Dateofdeath.getText();
			String causeofdeath_validation=causeofdeath.getText();
			String descofdeath_validation=descofdeath.getText();
			String practitionername_validation=practitionername.getText();
			String practitionerphoneno_validation=practitionerphoneno.getText();
			String practitioneraddress1_validation=practitioneraddress1.getText();
			String practitioneraddress2_validation=practitioneraddress2.getText();
			
			//****Product*****\\			 
			 if(Product.contains(product_validation)){    	
	    		
	 			report.updateTestLog("Product validation:", Product+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Product validation:", Product+" is not displayed as expected", Status.FAIL);
	 		}		 
			 
			//****Validate claim type*****\\			 
			 if(ClaimType.contains(claimtype_validation)){    	
	    		
	 			report.updateTestLog("Claim Type validation:", ClaimType+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Claim Type validation:", ClaimType+" is not displayed as expected", Status.FAIL);
	 		}		 
		
			
			//****Enter creditcard number*****\\
			
			 JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
			 
			 if(Creditcard.contains(creditcard_validation)){    	
		    		
		 			report.updateTestLog("Credit card validation:", Creditcard+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Credit card validation:", Creditcard+" is not displayed as expected", Status.FAIL);
		 		}		 
			
			
           //****Select title*****\\
			
			 if(Title.contains(title_validation)){    	
		    		
		 			report.updateTestLog("Title validation:", Title+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Title validation:", Title+" is not displayed as expected", Status.FAIL);
		 		}
	
			
			//****Enter DOB*****\\
			 
			 if(DOB.contains(Dob_validation)){    	
		    		
		 			report.updateTestLog("DOB validation:", DOB+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("DOB validation:", DOB+" is not displayed as expected", Status.FAIL);
		 		}
			
			 JavascriptExecutor jj = (JavascriptExecutor) driver;
				jj.executeScript("window.scrollBy(0,100)", "");
				sleep(500);
			//****Enter Name*****\\
			
			if(Name.contains(GivenName_validation)){    	
	    		
	 			report.updateTestLog("Name validation:", Name+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Name validation:", Name+" is not displayed as expected", Status.FAIL);
	 		}			
			
			//****Enter surname*****\\
			
			
			if(Surname.contains(SurName_validation)){    	
	    		
	 			report.updateTestLog("Surname validation:", Surname+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Surname validation:", Surname+" is not displayed as expected", Status.FAIL);
	 		}			
			
			//****Enter Address1*****\\
			
			if(Address.contains(Address1_validation)){    	
	    		
	 			report.updateTestLog("Address 1 validation:", Address+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Address 1 validation:", Address+" is not displayed as expected", Status.FAIL);
	 		}

			
			//****Enter Address2*****\\
			JavascriptExecutor exe = (JavascriptExecutor) driver;
			exe.executeScript("window.scrollBy(0,100)", "");
			sleep(500);
			
			if(Addresss.contains(Address2_validation)){    	
	    		
	 			report.updateTestLog("Address 2 validation:", Addresss+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Address 2 validation:", Addresss+" is not displayed as expected", Status.FAIL);
	 		}
			
			
			//****Enter Suburb*****\\
			
			if(suburb.contains(Suburb_validation)){    	
	    		
	 			report.updateTestLog("Suburb validation:", suburb+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Suburb validation:", suburb+" is not displayed as expected", Status.FAIL);
	 		}
			JavascriptExecutor exs = (JavascriptExecutor) driver;
			exs.executeScript("window.scrollBy(0,100)", "");
			sleep(500);
			
			//****Select State*****\\
			
			if(state.contains(State_validation)){    	
	    		
	 			report.updateTestLog("State validation:", state+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("State validation:", state+" is not displayed as expected", Status.FAIL);
	 		}

			
			//****Enter Postal code*****\\
		    
		    if(postcode.contains(postcode_validation)){    	
	    		
	 			report.updateTestLog("Post code validation:", postcode+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Post code validation:", postcode+" is not displayed as expected", Status.FAIL);
	 		}
		 
		    JavascriptExecutor jss = (JavascriptExecutor) driver;
			jss.executeScript("window.scrollBy(0,400)", "");
			sleep(500);
	    	//*****	Claimant details****\\
			
		 
					//****Relationship to insured******\\
		    
		    if(Relationship.contains(Relationship_validation)){    	
	    		
	 			report.updateTestLog("Relationship to insured validation:", Relationship+" is displayed as Expected", Status.PASS);	
	 		}
	 		else{	 		
	 			report.updateTestLog("Relationship to insured validation:", Relationship+" is not displayed as expected", Status.FAIL);
	 		}			

				
					//****Select Title*****\\
			 if(Title.contains(Title_validation)){    	
			    		
			 			report.updateTestLog("Claimant Title validation:", Title+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Claimant Title validation:", Title+" is not displayed as expected", Status.FAIL);
			 		}
							
				//****Enter Given name*****\\
				 if(Givenname.contains(claimantGivenname_validation)){    	
			    		
			 			report.updateTestLog("Claimant Given name validation:", Givenname+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Claimant Given name validation:", Givenname+" is not displayed as expected", Status.FAIL);
			 		}
			
				 JavascriptExecutor je = (JavascriptExecutor) driver;
					je.executeScript("window.scrollBy(0,300)", "");
					sleep(500);
				
				//****Enter surname*****\\
				
				 if(Givensurname.contains(claimantsurname_validation)){    	
			    		
			 			report.updateTestLog("Surname name validation:", Givensurname+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Surname name validation:", Givensurname+" is not displayed as expected", Status.FAIL);
			 		}
				
				
				
				//****Is your address the same as the primary account holder?*****\\
				
				 
				if(Addresssame.equalsIgnoreCase("Yes")){

				if(Addresssame.contains(Adresssame_validation)){    	
		
		 			report.updateTestLog("Address same as primary account holder validation:", Addresssame+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Address same as primary account holder validation:", Addresssame+" is not displayed as expected", Status.FAIL);
		 		}
				
				}
				else{
					
					if(Addresssame.contains(Adresssame_validation)){    	
			    		
			 			report.updateTestLog("Address same as primary account holder validation:", Addresssame+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Address same as primary account holder validation:", Addresssame+" is not displayed as expected", Status.FAIL);
			 		}
		
					
					//****Enter Address1*****\\
					
					if(Address.contains(claimantaddress1_validation)){    	
			    		
			 			report.updateTestLog("Address1 validation:", Address+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Address1 validation:", Address+" is not displayed as expected", Status.FAIL);
			 		}

					  JavascriptExecutor jee = (JavascriptExecutor) driver;
						jee.executeScript("window.scrollBy(0,400)", "");
						sleep(500);
					//****Enter Address2*****\\
					
					if(Addresss.contains(claimantaddress2_validation)){    	
			    		
			 			report.updateTestLog("Address2 validation:", Addresss+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Address2 validation:", Addresss+" is not displayed as expected", Status.FAIL);
			 		}
					
					
					//****Enter Suburb*****\\
					
					if(suburb.contains(claimantsuburb_validation)){    	
			    		
			 			report.updateTestLog("Suburb validation:", suburb+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Suburb validation:", suburb+" is not displayed as expected", Status.FAIL);
			 		}
					
					//****Select State*****\\
					
					if(state.contains(claimantstate_validation)){    	
			    		
			 			report.updateTestLog("state validation:", state+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("state validation:", state+" is not displayed as expected", Status.FAIL);
			 		}				

					
					//****Enter Postal code*****\\
				    
				    if(postcode.contains(claimantpostalcode_validation)){    	
			    		
			 			report.updateTestLog("Post code validation:", postcode+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Post code validation:", postcode+" is not displayed as expected", Status.FAIL);
			 		}

				}
				
				//****Enter Emailaddress*****\\
				
				if(Email.contains(Emailaddress_validation)){    	
		    		
		 			report.updateTestLog("Email validation:", Email+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Email validation:", Email+" is not displayed as expected", Status.FAIL);
		 		}
	
				
				
				//****Enter Peferred phone number*****\\
				
				if(PreferredPhoneNO.contains(Preferredphoneno_validation)){    	
		    		
		 			report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("PreferredPhoneno validation:", PreferredPhoneNO+" is not displayed as expected", Status.FAIL);
		 		}

				
				//****Enter other phone number*****\\
				
				if(Otherphoneno.contains(Otherphoneno_validation)){    	
		    		
		 			report.updateTestLog("Otherphoneno validation:", Otherphoneno+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Otherphoneno validation:", Otherphoneno+" is not displayed as expected", Status.FAIL);
		 		}				
				
				
				
				//****Preferred mode of contact (Phone / Email / Letter)?*****\\
				
				
				if(Preferredmode.equalsIgnoreCase("Phone")){
					WebElement Modeofcontact=driver.findElement(By.xpath("//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
					WebElement timeofcontact=driver.findElement(By.xpath("//label[text()='Preferred contact time: AM / PM ']/following-sibling::label"));
					String Modeofcontact_validation=Modeofcontact.getText();
				    String timeofcontact_validation=timeofcontact.getText();
					if(Preferredmode.contains(Modeofcontact_validation)){    	
			    		
			 			report.updateTestLog("Preferred Mode of Contact validation:", Preferredmode+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Preferred Mode of Contact validation:", Preferredmode+" is not displayed as expected", Status.FAIL);
			 		}	
	
			    if(Contacttime.contains(timeofcontact_validation)){   	
			    
		 			report.updateTestLog("Time of contact validation:", Contacttime+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Time of contact validation:", Contacttime+" is not displayed as expected", Status.FAIL);
		 		}		
		
				}
				if(Preferredmode.equalsIgnoreCase("Email")){
					WebElement Modeofcontact=driver.findElement(By.xpath("//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
					String Modeofcontact_validation=Modeofcontact.getText();
					if(Preferredmode.contains(Modeofcontact_validation)){    	
			    		
			 			report.updateTestLog("Preferred mode of contact validation:", Preferredmode+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Preferred mode of contact validation:", Preferredmode+" is not displayed as expected", Status.FAIL);
			 		}
				
				}else
				{
					WebElement Modeofcontact=driver.findElement(By.xpath("//label[text()='Preferred mode of contact (Phone / Email / Letter)? ']/following-sibling::label"));
					String Modeofcontact_validation=Modeofcontact.getText();
					if(Preferredmode.contains(Modeofcontact_validation)){  	
						
			 			report.updateTestLog("Preferredmode of contact validation:", Preferredmode+" is displayed as Expected", Status.PASS);	
			 		}
			 		else{	 		
			 			report.updateTestLog("Preferredmode of contact validation:", Preferredmode+" is not displayed as expected", Status.FAIL);
			 		}
			
				}

			
				//***************Death Details***************//
				
				
				//****Enter Date of death******\\	
			
				
				if(DateofDeath.contains(Dateofdeath_validation)){    	
		    		
		 			report.updateTestLog("Date of birth validation:", DateofDeath+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Date of birth validation:", DateofDeath+" is not displayed as expected", Status.FAIL);
		 		}

				//****Cause of death*****\\
				
				if(CauseofDeath.contains(causeofdeath_validation)){    	
		    		
		 			report.updateTestLog("Death cause validation:", CauseofDeath+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Death cause validation:", CauseofDeath+" is not displayed as expected", Status.FAIL);
		 		}
				
				 JavascriptExecutor ja = (JavascriptExecutor) driver;
				 ja.executeScript("window.scrollBy(0,300)", "");
					sleep(500);
				//****Description of death cause******\\
				
				if(Deathcause.contains(descofdeath_validation)){    	
		    		
		 			report.updateTestLog("Description of death cause validation:", Deathcause+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Description of death cause validation:", Deathcause+" is not displayed as expected", Status.FAIL);
		 		}			
				 JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("window.scrollBy(0,150)", "");
					sleep(500);
			
          		//***Additional Details*****\\


				//****Medical practitioner name*****\\
				if(Medicalpractitionername.contains(practitionername_validation)){    	
		    		
		 			report.updateTestLog("Medicalpractitionername validation:", Medicalpractitionername+" is displayed as Expected", Status.PASS);	
		 		}
		 		else{	 		
		 			report.updateTestLog("Medicalpractitionername validation:", Medicalpractitionername+" is not displayed as expected", Status.FAIL);
		 		}		


			//****Medical practitioner phone number******\\
		if(Medicalparctionerphn.contains(practitionerphoneno_validation)){    	
	
			report.updateTestLog("Medical practitioner phone number validation:", Medicalparctionerphn+" is displayed as Expected", Status.PASS);	
		}
		else{	 		
			report.updateTestLog("Medical practitioner phone number validation:", Medicalparctionerphn+" is not displayed as expected", Status.FAIL);
		}

		JavascriptExecutor jb = (JavascriptExecutor) driver;
		 jb.executeScript("window.scrollBy(0,300)", "");
			sleep(500);

		//****Medical practitioner address 1******\\

		if(practitioneradd.contains(practitioneraddress1_validation)){    	
			
			report.updateTestLog("Medical practitioner address 1 validation:", practitioneradd+" is displayed as Expected", Status.PASS);	
		}
			else{	 		
			report.updateTestLog("Medical practitioner address 1 validation:", practitioneradd+" is not displayed as expected", Status.FAIL);
				}


			//****Medical practitioner address 2******\\

		if(practitioneraddress.contains(practitioneraddress2_validation)){    	
	
			report.updateTestLog("Enter Medical practitioner address 2 validation:", practitioneraddress+" is displayed as Expected", Status.PASS);	
		}
	else{	 		
			report.updateTestLog("Enter Medical practitioner address 2 validation:", practitioneraddress+" is not displayed as expected", Status.FAIL);
		}

		JavascriptExecutor jc = (JavascriptExecutor) driver;
		 jc.executeScript("window.scrollBy(0,600)", "");
			sleep(500);
			
			if(Deathcertificate.equalsIgnoreCase("Yes")){
			WebElement Deathcert=driver.findElement(By.xpath("//table//td[text()='Certified death certificate']/./following::td"));
			String Deathcert_validation=Deathcert.getText();
			if(Deathcertificate.contains(Deathcert_validation)){				
				
				report.updateTestLog("Death Certificate name validation:", Deathcertificate+" is displayed as Expected", Status.PASS);	

			}
			}
			if(Otherdocs.equalsIgnoreCase("Yes")){
			WebElement otherdocumentname=driver.findElement(By.xpath("//table//td[text()='Other']/./following::td"));
			String otherdocumentname_validation=otherdocumentname.getText();
			if(Otherdocs.contains(otherdocumentname_validation)){
				
				report.updateTestLog("Other Document name validation:", Otherdocs+" is displayed as Expected", Status.PASS);	
			}
			}
			
			//****Review Claim Details******\\
			
				//***Agree button**//
				
				wait.until(ExpectedConditions.elementToBeClickable(By.id("others"))).click();
				sleep(500);
				report.updateTestLog("Consent checkbox", "Consent checkbox Clicked", Status.PASS);
			
			
			//***Submit button**//
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-sm-3 col-xs-12']/button[@type='submit']"))).click();
			sleep(500);
			report.updateTestLog("Acknowledgement checkbox", "Checkbox is clicked", Status.PASS);	
			sleep(2000);
			
			
			
			
			//******Download the PDF***********\\
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
				sleep(2000);
			}	
	if (StrBrowseName.equalsIgnoreCase("Firefox")){
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
		sleep(1500);
		Robot roboobj=new Robot();
		roboobj.keyPress(KeyEvent.VK_ENTER);	
	}
	
	if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		Robot roboobj=new Robot();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='/eclaims/static/cardAssure/images/img_pdf_icon.gif']"))).click();
		
		sleep(1500);	
	}

	report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS); 

	sleep(3000);
		
	JavascriptExecutor jf = (JavascriptExecutor) driver;
	 jf.executeScript("window.scrollBy(0,600)", "");
		sleep(500);

			
	//***Finish button**//
	
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='col-xs-6 col-sm-2 right-col']/button[@type='submit']"))).click();
	sleep(500);
	report.updateTestLog("Finish button:", "Finish button is clicked", Status.PASS);	

		

 }catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
 
 }


public void Eclaims_Death_negativescenario(){
	
	try{
		    String ClaimType = dataTable.getData("Eclaims_death", "ClaimType");
			String Creditcard = dataTable.getData("Eclaims_death", "Creditcard");
			String Title = dataTable.getData("Eclaims_death", "Title");
			String DOB = dataTable.getData("Eclaims_death", "DOB");
			String Name = dataTable.getData("Eclaims_death", "Name");
			String Surname = dataTable.getData("Eclaims_death", "Surname");
			String Address=dataTable.getData("Eclaims_death", "Address1");
			String Addresss=dataTable.getData("Eclaims_death", "Address2");
			String suburb=dataTable.getData("Eclaims_death", "suburb");	
			String state = dataTable.getData("Eclaims_death", "State");
			String postcode = dataTable.getData("Eclaims_death", "Postcode");
			String Relationship = dataTable.getData("Eclaims_death", "Relationship");
			String givenTitle = dataTable.getData("Eclaims_death", "givenTitle");
			String Givenname = dataTable.getData("Eclaims_death", "Givenname");
		    String Givensurname = dataTable.getData("Eclaims_death", "Givensurname");
		    String PreferredPhoneNO = dataTable.getData("Eclaims_death", "PreferredPhoneNO");
			String Otherphoneno = dataTable.getData("Eclaims_death", "Otherphoneno");
			String Email = dataTable.getData("Eclaims_death", "Email");
			String Preferredmode = dataTable.getData("Eclaims_death", "Preferredmode");
			String Contacttime = dataTable.getData("Eclaims_death", "Contacttime");
		    String DateofDeath = dataTable.getData("Eclaims_death", "DateofDeath");	    
		    String CauseofDeath = dataTable.getData("Eclaims_death", "CauseofDeath");
			String Deathcause = dataTable.getData("Eclaims_death", "Deathcause");
			String Addresssame=dataTable.getData("Eclaims_death", "Addresssame");
			String Doberror = dataTable.getData("Eclaims_death", "Doberror");
			String emailerror = dataTable.getData("Eclaims_death", "emailerror");
			String phonenoerror = dataTable.getData("Eclaims_death", "phonenoerror");
		//	String weddingdateerror = dataTable.getData("Eclaims_death", "weddingdateerror");
			String Otherphonenoerror = dataTable.getData("Eclaims_death", "Otherphonenoerror");
			String Deathdateerror = dataTable.getData("Eclaims_death", "Deathdateerror");	
			String Postcodeerror = dataTable.getData("Eclaims_death", "Postcodeerror");
		
			WebDriverWait wait=new WebDriverWait(driver,18);
			
			//****Click on claim type*****\\
			
				
			 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='claimType-DEATH']/parent::*"))).click();
			 sleep(500);
			 report.updateTestLog("Death claim", "Death claim is Selected", Status.PASS);
		
			
			//****Enter creditcard number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("ccard_lastfourdigits"))).sendKeys(Creditcard);
			report.updateTestLog("Credit card / facility no.(Please enter the last 4 digits):",  Creditcard+" is entered" , Status.PASS);
	
			
			
            //****Select title*****\\
			
			Select titletype=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ200_1")));		
			titletype.selectByVisibleText(Title);				
			report.updateTestLog("Enter Title:", Title+" is selected ", Status.PASS);
	
			
			//****Enter DOB*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2"))).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ203_2"))).sendKeys(DOB);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Given name ']"))).click();
			
			report.updateTestLog("Enter Date of birth :", DOB+" is entered", Status.PASS);
			
			
			//****Enter Name*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ201_0"))).sendKeys(Name);
			report.updateTestLog("Enter Given name:", Name+" is entered", Status.PASS);
			
			
			//****Enter surname*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ202_1"))).sendKeys(Surname);
			report.updateTestLog("Enter Surname name:", Surname+" is entered", Status.PASS);
			
			
			//****Enter Address1*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ205_0"))).sendKeys(Address);
			report.updateTestLog("Enter Given Address1:", Address+" is entered", Status.PASS);
			
			//****Enter Address2*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ206_1"))).sendKeys(Addresss);
			report.updateTestLog("Enter Given Address2:", Addresss+" is entered", Status.PASS);
			
			
			//****Enter Suburb*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ207_0"))).sendKeys(suburb);
			report.updateTestLog("Enter Suburb:", suburb+" is entered", Status.PASS);			
						
			
			//****Select State*****\\
			
			Select Stateselect=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ208_1")));		
			Stateselect.selectByVisibleText(state);			
			report.updateTestLog("Select State:", state+" is selected ", Status.PASS);
		    sleep(500);
			
			//****Enter Postal code*****\\
		    wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ209_2"))).sendKeys(postcode);
			sleep(500);			
			report.updateTestLog("Enter Post code:", postcode+" is entered", Status.PASS);
			sleep(300);
			
						
	                          	//*****	Claimant details****\\
			
			
			
				//****Relationship to insured******\\
			
			Select Relation=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ400_0")));		
			Relation.selectByVisibleText(Relationship);			
			report.updateTestLog("Select Relationship to insured:", Relationship+" is selected ", Status.PASS);		
			
			
				//****Select Title*****\\
			
			Select titl=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ401_2")));		
			titl.selectByVisibleText(givenTitle);			
			report.updateTestLog("Select Title:", Title+" is selected ", Status.PASS);
			sleep(500);
						
			//****Enter Given name*****\\
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ403_0"))).sendKeys(Givenname);
			report.updateTestLog("Enter Given name:", Givenname+" is entered", Status.PASS);
			sleep(500);
			
			//****Enter surname*****\\
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ404_1"))).sendKeys(Givensurname);
			report.updateTestLog("Enter Surname name:", Givensurname+" is entered", Status.PASS);
			sleep(500);
			
			
			//****Is your address the same as the primary account holder?*****\\
			if(Addresssame.equalsIgnoreCase("Yes")){
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-Yes']/parent::*"))).click();
			sleep(500);
			report.updateTestLog("Address same is selected", "Selected Yes", Status.PASS);
			sleep(2000);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='CQ408-No']/parent::*"))).click();
				sleep(500);
				report.updateTestLog("Address same as primary accont hoder", "Selected No", Status.PASS);
				sleep(500);
				
				//****Enter Address1*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ409_1"))).sendKeys(Address);
				report.updateTestLog("Enter Given Address1:", Address+" is entered", Status.PASS);
				
				//****Enter Address2*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ410_2"))).sendKeys(Addresss);
				report.updateTestLog("Enter Given Address2:", Addresss+" is entered", Status.PASS);
				
				
				//****Enter Suburb*****\\
				wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ411_0"))).sendKeys(suburb);
				report.updateTestLog("Enter Suburb:", suburb+" is entered", Status.PASS);			
							
				
				//****Select State*****\\
				
				Select Stateprimaryselect=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ412_1")));		
				Stateprimaryselect.selectByVisibleText(state);			
				report.updateTestLog("Select State:", state+" is selected ", Status.PASS);
			    sleep(500);
				
				//****Enter Postal code*****\\
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ413_2"))).sendKeys(postcode);
				sleep(500);			
				report.updateTestLog("Enter Post code:", postcode+" is entered", Status.PASS);
				sleep(300);
			}
			
			//****Enter Emailaddress*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ402_0"))).sendKeys(Email);
			report.updateTestLog("Enter Email:", Email+" is entered", Status.PASS);
			
			
			//****Enter Peferred phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ415_1"))).sendKeys(PreferredPhoneNO);
			sleep(500);
			report.updateTestLog("Enter PreferredPhoneNO:", PreferredPhoneNO+" is entered", Status.PASS);
			sleep(500);
			
			//****Enter other phone number*****\\
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ414_2"))).sendKeys(Otherphoneno);
			sleep(500);
			report.updateTestLog("Enter otherphonenumber:", Otherphoneno+" is entered", Status.PASS);
			sleep(500);
			
			
			
			
			//****Preferred mode of contact (Phone / Email / Letter)?*****\\
			
			
			if(Preferredmode.equalsIgnoreCase("Phone")){
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Phone']/parent::*"))).click();
		    sleep(500);
		    report.updateTestLog("Preferred Mode of Contact ", Preferredmode+" is Selected ", Status.PASS);	
					
				
			Select mode=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ417_1")));		
			mode.selectByVisibleText(Contacttime);			
			report.updateTestLog("Preferred mode of contact (Phone / Email / Letter):", Contacttime+" is selected ", Status.PASS);
			sleep(500);
			}
			if(Preferredmode.equalsIgnoreCase("Email")){
			//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Email']/parent::*"))).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[3]/div/div/div[3]/div[2]/form/ng-form/div[4]/ng-form/div[5]/ng-form/div[7]/ng-form/div[1]/div/div/div/div/label[2]"))).click();
				
			    report.updateTestLog("Preferred Mode of Contact ", Preferredmode+" is Selected ", Status.PASS);	
			}
				if(Preferredmode.equalsIgnoreCase("Letter")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@value='Letter']/parent::*"))).click();
			    sleep(500);
			    report.updateTestLog("Preferred Mode of Contact ", Preferredmode+" is Selected ", Status.PASS);	
			}
		
				
			
									
			//***************Death Details***************//
			
				
			//****Enter Date of death******\\	
		   
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input_CQ500_0"))).sendKeys(DateofDeath);
			sleep(500);			
			report.updateTestLog("Enter Date of birth :", DateofDeath+" is entered", Status.PASS);
		

		
 	

			//****Cause of death*****\\
			sleep(500);
			Select deathcause=new Select(driver.findElement(By.name("vm.personalDetailsForm_custom-select_CQ501_0")));				
			deathcause.selectByVisibleText(CauseofDeath);	
			sleep(500);
			report.updateTestLog("Death cause:", CauseofDeath+" is selected ", Status.PASS);
			
			//****Description of death cause******\\
			
			sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("vm.personalDetailsForm_input-datalist_CQ502_1"))).sendKeys(Deathcause);
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Cause of Death ']"))).click();		
			report.updateTestLog("Description of death cause*:", Deathcause+" is entered", Status.PASS);		
	
			
			
			//****Proceed button******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary w100p']"))).click();
			sleep(500);
			report.updateTestLog("Proceed Button", "clicked", Status.PASS);
			
			
			WebElement Doberrormessage = driver.findElement(
					By.xpath("(//div[@class='error-message ng-binding ng-scope'])[1]"));
			WebElement emailerrormessage = driver.findElement(
					By.xpath("(//div[@class='error-message ng-binding ng-scope'])[2]"));
			WebElement postcodeerrormessage = driver.findElement(
					By.xpath("(//div[@class='ng-scope'])[2]"));
			WebElement phonenoerrormessage = driver.findElement(
					By.xpath("(//div[@class='error-message ng-binding ng-scope'])[3]"));
			WebElement otherphonenoerrormessage = driver.findElement(
					By.xpath("(//div[@class='error-message ng-binding ng-scope'])[4]"));
			WebElement Deathcermessage = driver.findElement(
					By.xpath("(//div[@class='error-message ng-binding ng-scope'])[5]"));
			String Doberror_validation = Doberrormessage.getText();
			String emailerror_validation = emailerrormessage.getText();
			String phonenoerror_validation = phonenoerrormessage.getText();
			String Deathdateerror_validation = Deathcermessage.getText();
			String otherphonenoerror_validation  = otherphonenoerrormessage.getText();
			String postcodeerror_validation  = postcodeerrormessage.getText();
			
			if (Doberror.contains(Doberror_validation)) {

				report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is displayed as Expected",
						Status.PASS);

			} else {
				report.updateTestLog("Future Date of Birth Error message validation:", Doberror + " is not displayed as expected",
						Status.FAIL);
			}
			
			if (Postcodeerror.contains(postcodeerror_validation)) {

				report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is displayed as Expected",
						Status.PASS);

			} else {
				report.updateTestLog("Post Code Error message validation:", Postcodeerror + " is not displayed as expected",
						Status.FAIL);
			}
			
			if (emailerror.contains(emailerror_validation)) {

				report.updateTestLog("Email address Error validation:", emailerror + " is displayed as Expected",
						Status.PASS);

			} else {
				report.updateTestLog("Email address Error validation:", emailerror + " is not displayed as expected",
						Status.FAIL);
			}
			if (phonenoerror.contains(phonenoerror_validation)) {

				report.updateTestLog("Phone number Error message validation:", phonenoerror + " is displayed as Expected",
						Status.PASS);

			} else {
				report.updateTestLog("Phone number Error message validation:", phonenoerror + " is not displayed as expected",
						Status.FAIL);
			}
			if (Otherphonenoerror.contains(otherphonenoerror_validation)) {

				report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is displayed as Expected",
						Status.PASS);

			} else {
				report.updateTestLog("Other Phone number Error message validation:", Otherphonenoerror + " is not displayed as expected",
						Status.FAIL);
			}
			if (Deathdateerror.contains(Deathdateerror_validation)) {

				report.updateTestLog("Future Date of Death Error message validation:", Deathdateerror + " is displayed as Expected",
						Status.PASS);

			} else {
				report.updateTestLog("Future Date of Death Error message validation:", Deathdateerror + " is not displayed as expected",
						Status.FAIL);
			}
		
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}
}



