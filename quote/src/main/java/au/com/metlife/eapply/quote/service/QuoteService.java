package au.com.metlife.eapply.quote.service;

import java.util.List;

import au.com.metlife.eapply.quote.model.ConvertModel;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.model.SelectItem;

public interface QuoteService {
	
	QuoteResponse calculateDeath(RuleModel ruleModel);
	
	QuoteResponse calculateTpd(RuleModel ruleModel);
	
	QuoteResponse calculateIp(RuleModel ruleModel);
	
	List<SelectItem> getIndustryList(String fundCode);
	
	InstProductRuleBO getProductConfig(String fundCode,String memberType,String manageType);
	
	ConvertModel convertDeathFixedToUnits(RuleModel ruleModel);
	
	ConvertModel convertTpdFixedToUnits(RuleModel ruleModel);
}
