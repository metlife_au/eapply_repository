class QuoteDecisionCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $scope, $state, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this._$state = $state;
    this._CommonAPI = CommonAPI;
    this._ngDialog = ngDialog;
    this._$scope = $scope;
    this._EapplyData = EapplyData;
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
    this.changeCoverData = this._EapplyData.getChangeCoverData();
    
    this.enableLoadingSection = false;
    this.enableExclusionSection = false;
    this.deathLoading = false;
    this.tpdLoading = false;
    this.ipLoading = false;
    this.deathExclusion = false;
    this.tpdExclusion = false;
    this.ipExclusion = false;
    
    this.pdfLocation = this._EapplyData.getPdfLocation();
    this.npsURL = this._EapplyData.getNpsUrl();
    
    this.finalDecision = this.changeCoverData.applicant.overallCoverDecision;
    if(this.finalDecision == null || this.finalDecision == '' || this.finalDecision ==undefined){
    	this.finalDecision = 'ACC';
    }
    
    if(this.finalDecision == 'ACC'){    	
    	var auraDecisions = [];
		auraDecisions.push(this.changeCoverData.applicant.cover[0].coverDecision);
		auraDecisions.push(this.changeCoverData.applicant.cover[1].coverDecision);
		auraDecisions.push(this.changeCoverData.applicant.cover[2].coverDecision);
		
		if(auraDecisions.indexOf('ACC') !== -1 && auraDecisions.indexOf('DCL') !== -1){
			this.finalDecision='MXD';
		}
		
		if(this.finalDecision === 'MXD'){
			var acceptedCovers = [];
			var declinedCovers = [];
			
			if(this.changeCoverData.applicant.cover[0].coverDecision === 'ACC'){
				acceptedCovers.push('Death');
			}else{
				declinedCovers.push('Death');
			}
			
			if(this.changeCoverData.applicant.cover[1].coverDecision === 'ACC'){
				acceptedCovers.push('Total Permanent Disablement');
			}else{
				declinedCovers.push('Total Permanent Disablement');
			}
			
			if(this.changeCoverData.applicant.cover[2].coverDecision === 'ACC'){
				acceptedCovers.push('Income Protection');
			}else{
				declinedCovers.push('Income Protection');
			}
			this.mixedAcceptedCovers =  acceptedCovers.join(' and ');
			this.mixedDeclinedCovers =  declinedCovers.join(' and ');
		}
	}
    
    if(this.changeCoverData.applicant.overallCoverDecision === 'ACC'){
    	this.checkLoading();
    	this.checkExclusion();
    }  
    
    this.openNPSPopup();    
    
    this._$scope.$on('$locationChangeStart', function(evnt, next, current){
    	var currentURL = current.split("/");
    	if(currentURL[currentURL.length-1] == 'quotedecision'){
    		evnt.preventDefault();
    	}    	
    });
  }
  
  checkLoading(){
	  
	  if(this.changeCoverData.applicant.cover[0].loading > 0){
	  		this.deathLoading = true;	  		
	  	}
	  	
	  	if(this.changeCoverData.applicant.cover[1].loading > 0){
	  		this.tpdLoading = true;	  		
	  	}
	  	
	  	if(this.changeCoverData.applicant.cover[2].loading > 0){
	  		this.ipLoading = true;	  		
	  	}
	  	
	  	if(this.deathLoading || this.tpdLoading || this.ipLoading){
	  		this.enableLoadingSection = true;
	  	} 
  }
  
  checkExclusion(){
	  if(this.changeCoverData.applicant.cover[0].coverExclusion != null){
		  this.deathExclusion = true;
	  }	
	  
	  if(this.changeCoverData.applicant.cover[1].coverExclusion != null){
		  this.tpdExclusion = true;
	  }
	  
	  if(this.changeCoverData.applicant.cover[2].coverExclusion != null){
		  this.ipExclusion = true;
	  }
	  
	  if(this.deathExclusion || this.tpdExclusion || this.ipExclusion){
	  		this.enableExclusionSection = true;
	  } 
  }
  
  downloadPDF(){
	  
	  const _this = this;	  
	  var filename = _this.pdfLocation.substring(_this.pdfLocation.lastIndexOf('/')+1);
	  var a = document.createElement('a');
	  document.body.appendChild(a);
	  
	  _this._CommonAPI.downloadPDF(_this.urlList.downloadUrl, _this.pdfLocation).then(
			  (res) => {
				  if(navigator.appVersion.toString().indexOf('.NET') > 0) {
					  window.navigator.msSaveBlob(res.response,filename);
				  }else{
					  var fileURL = URL.createObjectURL(res.response);
					  a.href = fileURL;
					  a.download = filename;
					  a.click();
				  }
			  }, 
              (err) => {
                console.log('Error while Downloading the PDF' + err);
              }
	  );	  	
  }
  
  openNPSPopup(){
	  var templateContent = '<div class="ngdialog-content"><div class="modal-header">';
	    templateContent += '<h4 id="myModalLabel" class="modal-title aligncenter"> We value your feedback</h4></div>';
	    templateContent += '<div class="modal-body"><div class="row rowcustom"><div class="col-sm-12">';
	    templateContent += '<p class="aligncenter">  </p>';
	    templateContent += '<div id="tips_text"><p>Please take 2 minutes to complete our online survey to tell us about your experience</p></div>';
	    templateContent += '<p></p></div></div></div>';
	    templateContent += '<div class="ngdialog-buttons aligncenter">';
	    templateContent += '<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm(\'onYes\')">Yes</button>';
	    templateContent += '<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button>';
	    templateContent += ' </div></div>';
	    	
	    this._ngDialog.openConfirm({
	   		template: templateContent,
	 		className: 'ngdialog-theme-plain custom-width',
	 		plain: true,
	 		showClose: false
	   	}).then(
	   	      (value) => {
	   	    	if(value==='onYes'){
	   	  			if(npsTokenUrl != null){
	   	  			  var url = 'http://c001.cloudrecording.com.au/METLIFE_EAPPLY/index.php?tk='+this.npsURL;
	   						$window.open(url, '_blank');
	   						return true;
	   	  			}
	   	  		}
	   	      }, 
	   	      (event) => {
	   	        if(event === 'oncancel') {
	   	          return false;
	   	        }
	   	      }
	   	    );
  }
}

export default QuoteDecisionCtrl;
