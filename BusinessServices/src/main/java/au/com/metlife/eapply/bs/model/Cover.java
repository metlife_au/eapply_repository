package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the COVERS database table.
 * 
 */
@Entity
@Table(name = "TBL_A_COVERS", schema="EAPPDB")
public class Cover implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length=50)
	private String aalevel;

	@Column(precision=22, scale=2)
	private BigDecimal addcovamounttpddec;

	@Column(precision=22, scale=2)
	private BigDecimal additionalcoveramount;

	@Column(length=20)
	private String additionalipbenefitperiod;

	@Column(length=20)
	private String additionalipwaitingperiod;

	private int additionalunit;

	@Column(length=1)
	private String addunitind;

	@Column(nullable=false, length=47)
	private long applicantid;

	@Column(length=20)
	private String cost;

	@Column(length=20)
	private String coveramount;

	@Column(length=100)
	private String covercategory;

	@Column(length=20)
	private String covercode;

	@Column(length=3)
	private String coverdecision;

	@Lob
	private String coverexclusion;

	@Column(length=50)
	private String coveroption;

	@Column(length=5000)
	private String coverreason;

	@Column(length=50)
	private String covertype;

	private Timestamp createdate;

	@Column(precision=22, scale=2)
	private BigDecimal defaultcoveramount;

	@Column(name="EXCL_CODE", length=3000)
	private String exclCode;

	@Column(length=20)
	private String existingcost;

	@Column(precision=22, scale=2)
	private BigDecimal existingcoveramount;

	@Column(length=20)
	private String existingipbenefitperiod;

	@Column(length=20)
	private String existingipwaitingperiod;

	private int fixedunit;

	@Column(length=20)
	private String frequencycosttype;

	@Column(length=50)
	private String fulamount;
	
	@Column(precision=22, scale=2)
	private BigDecimal transfercoveramount;

	public BigDecimal getTransfercoveramount() {
		return transfercoveramount;
	}

	public void setTransfercoveramount(BigDecimal transfercoveramount) {
		this.transfercoveramount = transfercoveramount;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_A_COVERS_SEQ")
	@SequenceGenerator(name="TBL_A_COVERS_SEQ",sequenceName = "EAPPDB.TBL_A_COVERS_SEQ", initialValue=1, allocationSize=100)
	private long id;

	private Timestamp lastupdatedate;

	@Column(length=6)
	private String levelofcommission;

	@Column(precision=5, scale=2)
	private BigDecimal loading;

	@Column(length=50)
	private String occupationrating;

	@Column(precision=22, scale=2)
	private BigDecimal optionalcoveramount;

	private int optionalunit;

	@Column(precision=22, scale=2)
	private BigDecimal policyfee;

	@Column(precision=22, scale=2)
	private BigDecimal stampduty;

	@Column(precision=22, scale=2)
	private BigDecimal totalcovamounttpddec;

	@Column(length=20)
	private String totalcovcosttpddec;

	@Column(precision=22, scale=2)
	private BigDecimal totalcoveramount;

	@Column(length=20)
	private String totalipbenefitperiod;

	@Column(length=20)
	private String totalipwaitingperiod;

	@Column(length=1)
	private String tpdtopupind;

	@Column(length=1)
	private String underwritten;

	public Cover() {
	}

	public String getAalevel() {
		return this.aalevel;
	}

	public void setAalevel(String aalevel) {
		this.aalevel = aalevel;
	}

	public BigDecimal getAddcovamounttpddec() {
		return this.addcovamounttpddec;
	}

	public void setAddcovamounttpddec(BigDecimal addcovamounttpddec) {
		this.addcovamounttpddec = addcovamounttpddec;
	}

	public BigDecimal getAdditionalcoveramount() {
		return this.additionalcoveramount;
	}

	public void setAdditionalcoveramount(BigDecimal additionalcoveramount) {
		this.additionalcoveramount = additionalcoveramount;
	}

	public String getAdditionalipbenefitperiod() {
		return this.additionalipbenefitperiod;
	}

	public void setAdditionalipbenefitperiod(String additionalipbenefitperiod) {
		this.additionalipbenefitperiod = additionalipbenefitperiod;
	}

	public String getAdditionalipwaitingperiod() {
		return this.additionalipwaitingperiod;
	}

	public void setAdditionalipwaitingperiod(String additionalipwaitingperiod) {
		this.additionalipwaitingperiod = additionalipwaitingperiod;
	}

	public int getAdditionalunit() {
		return this.additionalunit;
	}

	public void setAdditionalunit(int additionalunit) {
		this.additionalunit = additionalunit;
	}

	public String getAddunitind() {
		return this.addunitind;
	}

	public void setAddunitind(String addunitind) {
		this.addunitind = addunitind;
	}

	public long getApplicantid() {
		return this.applicantid;
	}

	public void setApplicantid(long applicantid) {
		this.applicantid = applicantid;
	}

	public String getCost() {
		return this.cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCoveramount() {
		return this.coveramount;
	}

	public void setCoveramount(String coveramount) {
		this.coveramount = coveramount;
	}

	public String getCovercategory() {
		return this.covercategory;
	}

	public void setCovercategory(String covercategory) {
		this.covercategory = covercategory;
	}

	public String getCovercode() {
		return this.covercode;
	}

	public void setCovercode(String covercode) {
		this.covercode = covercode;
	}

	public String getCoverdecision() {
		return this.coverdecision;
	}

	public void setCoverdecision(String coverdecision) {
		this.coverdecision = coverdecision;
	}

	public String getCoverexclusion() {
		return this.coverexclusion;
	}

	public void setCoverexclusion(String coverexclusion) {
		this.coverexclusion = coverexclusion;
	}

	public String getCoveroption() {
		return this.coveroption;
	}

	public void setCoveroption(String coveroption) {
		this.coveroption = coveroption;
	}

	public String getCoverreason() {
		return this.coverreason;
	}

	public void setCoverreason(String coverreason) {
		this.coverreason = coverreason;
	}

	public String getCovertype() {
		return this.covertype;
	}

	public void setCovertype(String covertype) {
		this.covertype = covertype;
	}

	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public BigDecimal getDefaultcoveramount() {
		return this.defaultcoveramount;
	}

	public void setDefaultcoveramount(BigDecimal defaultcoveramount) {
		this.defaultcoveramount = defaultcoveramount;
	}

	public String getExclCode() {
		return this.exclCode;
	}

	public void setExclCode(String exclCode) {
		this.exclCode = exclCode;
	}

	public String getExistingcost() {
		return this.existingcost;
	}

	public void setExistingcost(String existingcost) {
		this.existingcost = existingcost;
	}

	public BigDecimal getExistingcoveramount() {
		return this.existingcoveramount;
	}

	public void setExistingcoveramount(BigDecimal existingcoveramount) {
		this.existingcoveramount = existingcoveramount;
	}

	public String getExistingipbenefitperiod() {
		return this.existingipbenefitperiod;
	}

	public void setExistingipbenefitperiod(String existingipbenefitperiod) {
		this.existingipbenefitperiod = existingipbenefitperiod;
	}

	public String getExistingipwaitingperiod() {
		return this.existingipwaitingperiod;
	}

	public void setExistingipwaitingperiod(String existingipwaitingperiod) {
		this.existingipwaitingperiod = existingipwaitingperiod;
	}

	public int getFixedunit() {
		return this.fixedunit;
	}

	public void setFixedunit(int fixedunit) {
		this.fixedunit = fixedunit;
	}

	public String getFrequencycosttype() {
		return this.frequencycosttype;
	}

	public void setFrequencycosttype(String frequencycosttype) {
		this.frequencycosttype = frequencycosttype;
	}

	public String getFulamount() {
		return this.fulamount;
	}

	public void setFulamount(String fulamount) {
		this.fulamount = fulamount;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getLastupdatedate() {
		return this.lastupdatedate;
	}

	public void setLastupdatedate(Timestamp lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	public String getLevelofcommission() {
		return this.levelofcommission;
	}

	public void setLevelofcommission(String levelofcommission) {
		this.levelofcommission = levelofcommission;
	}

	public BigDecimal getLoading() {
		return this.loading;
	}

	public void setLoading(BigDecimal loading) {
		this.loading = loading;
	}

	public String getOccupationrating() {
		return this.occupationrating;
	}

	public void setOccupationrating(String occupationrating) {
		this.occupationrating = occupationrating;
	}

	public BigDecimal getOptionalcoveramount() {
		return this.optionalcoveramount;
	}

	public void setOptionalcoveramount(BigDecimal optionalcoveramount) {
		this.optionalcoveramount = optionalcoveramount;
	}

	public int getOptionalunit() {
		return this.optionalunit;
	}

	public void setOptionalunit(int optionalunit) {
		this.optionalunit = optionalunit;
	}

	public BigDecimal getPolicyfee() {
		return this.policyfee;
	}

	public void setPolicyfee(BigDecimal policyfee) {
		this.policyfee = policyfee;
	}

	public BigDecimal getStampduty() {
		return this.stampduty;
	}

	public void setStampduty(BigDecimal stampduty) {
		this.stampduty = stampduty;
	}

	public BigDecimal getTotalcovamounttpddec() {
		return this.totalcovamounttpddec;
	}

	public void setTotalcovamounttpddec(BigDecimal totalcovamounttpddec) {
		this.totalcovamounttpddec = totalcovamounttpddec;
	}

	public String getTotalcovcosttpddec() {
		return this.totalcovcosttpddec;
	}

	public void setTotalcovcosttpddec(String totalcovcosttpddec) {
		this.totalcovcosttpddec = totalcovcosttpddec;
	}

	public BigDecimal getTotalcoveramount() {
		return this.totalcoveramount;
	}

	public void setTotalcoveramount(BigDecimal totalcoveramount) {
		this.totalcoveramount = totalcoveramount;
	}

	public String getTotalipbenefitperiod() {
		return this.totalipbenefitperiod;
	}

	public void setTotalipbenefitperiod(String totalipbenefitperiod) {
		this.totalipbenefitperiod = totalipbenefitperiod;
	}

	public String getTotalipwaitingperiod() {
		return this.totalipwaitingperiod;
	}

	public void setTotalipwaitingperiod(String totalipwaitingperiod) {
		this.totalipwaitingperiod = totalipwaitingperiod;
	}

	public String getTpdtopupind() {
		return this.tpdtopupind;
	}

	public void setTpdtopupind(String tpdtopupind) {
		this.tpdtopupind = tpdtopupind;
	}

	public String getUnderwritten() {
		return this.underwritten;
	}

	public void setUnderwritten(String underwritten) {
		this.underwritten = underwritten;
	}

}