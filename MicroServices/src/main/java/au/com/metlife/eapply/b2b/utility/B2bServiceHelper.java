package au.com.metlife.eapply.b2b.utility;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.b2b.xsd.beans.Applicant;
import au.com.metlife.eapply.b2b.xsd.beans.Cover;
import au.com.metlife.eapply.b2b.xsd.beans.Request;

public class B2bServiceHelper {
	private static final Logger log = LoggerFactory.getLogger(B2bServiceHelper.class);
	
	public static final String XMLSCHEMA="http://www.w3.org/2001/XMLSchema"; 
	
	public static final int ERROR_MIN_LENGTH = 1;


	public  static Request unMarshallingMInput(String inputXML){
		log.info("Unmarshalling input start");
		Request input=null;
		try {
			JAXBContext jc = JAXBContext.newInstance (new Class[] {Request.class});            
            Unmarshaller u = jc.createUnmarshaller ();
      
             input = (Request) u.unmarshal (new ByteArrayInputStream(inputXML.getBytes("UTF-8")));
       } catch (Exception e) {
          log.error("Error in unmarshalling input {}",e);
       }
		log.info("Unmarshalling input finish");
       return input;
	}
	
	/**
	 * @param xmlContent
	 * @param xsdFileFullPath
	 * @return
	 */
	public  String validateXMLString(String xmlContent,String xsdpath){
		log.info("validate xml string start");
	    String validationMesage="valid";
		Validator validator=null;
        try {
          SchemaFactory factory =SchemaFactory.newInstance(XMLSCHEMA);

             /*2. Compile the schema.
             Here the schema is loaded from a java.io.File, but you could use
             a java.net.URL or a javax.xml.transform.Source instead.*/
    	    log.info("xsdpath>> {}",xsdpath);
            File schemaLocation = new File(xsdpath);
            Schema schema = factory.newSchema(schemaLocation);

            /*3. Get a validator from the schema.*/             
            validator = schema.newValidator();

            validator.setErrorHandler(new XMLErrorHandler());

            String errors;

             /*4. Parse the document you want to check.*/
            StringReader reader = new StringReader(xmlContent);
              Source source = new StreamSource(reader);
              validator.validate(source);
              errors=((XMLErrorHandler)validator.getErrorHandler()).getErrors();
              if(errors!=null  && errors.trim().length()>ERROR_MIN_LENGTH){
            	  validationMesage="invalid -{\n"+errors+"\n}";
              }
        }
        catch (Exception ex) {        	
            validationMesage="invalid -{\n 400: Bad xml data passed.";
        }
        log.info("validate xml string finish");
		return validationMesage;

	}
	
	
	public static Boolean validDateIPDeathTPDCover(Request request, String fundid){
		Boolean status = Boolean.TRUE;
		Applicant applicant = request.getPolicy().getApplicant().get(0);
		String deathType = null;
		String tpdType = null;
		Cover [] covers;
		
		if(applicant.getExistingCovers()!=null && applicant.getExistingCovers().getCover()!=null && applicant.getExistingCovers().getCover().length>0){
			covers = applicant.getExistingCovers().getCover();
			for (int i = 0; i < covers.length; i++) {
				/*if("4".equalsIgnoreCase(covers[i].getBenefitType())){
					if(fundid.equalsIgnoreCase("CARE") && covers[i].getType()!=null 
							&& "1".equalsIgnoreCase(covers[i].getType())){
						status = Boolean.FALSE;
					}
				}else*/ if("1".equalsIgnoreCase(covers[i].getBenefitType())){
					deathType = covers[i].getType();
				}else if("2".equalsIgnoreCase(covers[i].getBenefitType())){
					tpdType = covers[i].getType();
				}
			}
			//Added by 561132 to block the application if Mixed type's are given for death and Tpd
			if((fundid.equalsIgnoreCase("CARE") || "AEIS".equalsIgnoreCase(fundid)) && deathType!=null && tpdType!=null && !deathType.equalsIgnoreCase(tpdType)){
				status = Boolean.FALSE;
			}
		}
		return status;
	}

}
