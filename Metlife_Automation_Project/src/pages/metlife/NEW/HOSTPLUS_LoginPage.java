/**
 * 
 */
package pages.metlife.NEW;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Hemnath
 * 
 */
public class HOSTPLUS_LoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HOSTPLUS_LoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public HOSTPLUS_LoginPage enterHOSTPLUSlogin() {
		enterlogindetails();
		return new HOSTPLUS_LoginPage(scriptHelper);
	} 
	
	private void enterlogindetails() {
		
		String XML = dataTable.getData("HOSTPLUS_Login", "XML");
		
		try{
			
			
			
			//******Input the XML String******\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(XML);
			report.updateTestLog("XML String", "XML String is entered", Status.PASS);
			
			
			//****Proceed to Next page button is Clicked*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'PROCEED TO NEXT PAGE')]"))).click();
			report.updateTestLog("Proceed To Next Page", "Proceed To Next Page button is Clicked", Status.PASS);
			
			
			//****Web response button click*****\\
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'New Application')]"))).click();
			report.updateTestLog("RWD Application", "RWD Application button is Clicked", Status.PASS);
			sleep(1500);
			
			quickSwitchWindows();
			
			
			
			sleep(3000);
			report.updateTestLog("HOSTPLUS Login", "Sucessfully Logged into the application", Status.PASS);
			
		
		
	
	
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
