(function(angular){
	'use strict';
    /* global channel, moment */
	angular
		.module('MtaaApp')
		.controller('quoteoccupdate', quoteoccupdate);
	quoteoccupdate.$inject=['$scope','$rootScope','$stateParams','$location','$timeout','urlService','persoanlDetailService','mtaaUIConstants','$q','QuoteService','OccupationService','NewOccupationService','deathCoverService','tpdCoverService','ipCoverService','fetchAppNumberSvc','appData','ngDialog','auraResponseService','saveEapply','RetrieveAppDetailsService','MaxLimitService','CalculateService','DownloadPDFService','printQuotePage','$filter','fetchOccupationSvc','auraRespSvc','printPageSvc'];
	function quoteoccupdate($scope, $rootScope, $stateParams, $location,  $timeout, urlService, persoanlDetailService, mtaaUIConstants, $q, QuoteService, OccupationService, NewOccupationService,deathCoverService,tpdCoverService,ipCoverService,fetchAppNumberSvc,appData,ngDialog,auraResponseService,saveEapply,RetrieveAppDetailsService,MaxLimitService,CalculateService,DownloadPDFService,printQuotePage,$filter,fetchOccupationSvc,auraRespSvc,printPageSvc){
		var vm = this;
		vm.OPD = {
        		requestType: 'UWCOVER',
                partnerCode: 'MTAA',
                lastSavedOn: 'Quotepage',
                auraDisabled: true,
                clientRefNumber:null,
                totalMonthlyPremium:null,
                applicatioNumber:null,
                ackCheck:false,
                privacyCheck:false,
                dodCheck:false,
                fulCheck:false,
                appDecision:'ACC',
                clientMatchReason:null,
                clientMatch:false,
                deathExclusions:null,
                tpdExclusions:null,
                ipExclusions:'',
                applicant: {
            		firstName:null,
            		lastName:null,
            		birthDate:null,
            		emailId:null,
            		contactType:null,
            		contactNumber:null,
            		smoker:null,
            		title:null,
            		age:null,
            		memberType:null,
            		gender:null,
            		customerReferenceNumber:null,
            		clientRefNumber:null,
            		australiaCitizenship:null,
            		dateJoinedFund:'',
            		workHours:null,
            		industryType:null,
            		occupationCode:null,
            		occupation:null,
            		spendTimeOutside:null,
            		annualSalary:null,
            		occupationDuties:null,
            		otherOccupation:'',
            		workDuties:null,
            		ownBusinessQue:null,
            		ownBusinessYesQue:null,
            		ownBusinessNoQue:null,
            		tertiaryQue:null,
            		previousTpdBenefit:'',
            		cover:[{
            			benefitType:'1',
            			coverDecision:null,
            			coverReason:null,
            			existingCoverAmount:null,
            			fixedUnit:null,
            			occupationRating:null, 
            			cost:null,
            			coverCategory:null,
            			additionalCoverAmount:null,
            			additionalUnit:'',
            			frequencyCostType:null
            		},{
            			benefitType:'2',
            			coverDecision:null,
            			coverReason:null,
            			existingCoverAmount:null,
            			fixedUnit:null,
            			occupationRating:null,
            			cost:null, 
            			coverCategory:null,
            			tpdInputTextValue:'',
            			additionalCoverAmount:null,
            			frequencyCostType:null
            		},{
            			benefitType:'4',
            			coverDecision:null,
            			coverReason:null,
            			existingCoverAmount:null,
            			optionalUnit:null,
            			existingIpBenefitPeriod:null,
            			existingIpWaitingPeriod:null,
            			fixedUnit:null,
            			occupationRating:null,
            			cost:null, 
            			coverCategory:'IpUnitised',
            			additionalCoverAmount:null,
            			additionalUnit:null,
            			additionalIpBenefitPeriod:null,
            			additionalIpWaitingPeriod:null,
            			totalIpWaitingPeriod:null,
            			totalIpBenefitPeriod:null,
            			frequencyCostType:null
            		}],
            		contactDetails:{
            		preferedContactTime:null,
            		preferedContacType:null,
            		mobilePhone: null,
            		workPhone: null,
            		homePhone: null,
            		preferedContactNumber:null,
            		country:null,
            		addressLine1:null,
            		addressLine2:null,
            		postCode:null,
            		state:null,
            		suburb:null
            		}
            }
        };
		vm.constants = {
				image: mtaaUIConstants.path.images,
				phoneNumber: mtaaUIConstants.onboardDetails.phoneNumber,
				emailFormat: mtaaUIConstants.onboardDetails.emailFormat,
				postCodeFormat: mtaaUIConstants.onboardDetails.postCodeFormat,
				contactTypeOption: mtaaUIConstants.onboardDetails.contactTypeOption,
				titleOption: mtaaUIConstants.onboardDetails.titleOption,
				addressTypeOption: mtaaUIConstants.onboardDetails.addressTypeOption,
				stateOptions: mtaaUIConstants.onboardDetails.stateOptions
		};
		$('#PersonalDetailsSection').collapse('hide');
		$('#collapseprivacy').collapse('hide');
		$('#OccupationSection').collapse('hide');
		var workRatingOccupationFormFields;
		var workRatingOthrOccupationFormFields;
		 vm.annualSalForOccUpgradeVal = null;
		    var fetchAppnum = true;
			var appNum;
			var dcWeeklyCost, tpdWeeklyCost, ipWeeklyCost, totalCost;
			var dcCoverAmount, tpdCoverAmount, ipCoverAmount;
			var deathCoverType,tpdCoverType,ipCoverType;
			var deathUpgrade = false,
				tpdUpgrade = false,
				ipUpgrade = false;
			var checkAppNum = false;
			workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'annualSalary'];
			workRatingOthrOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'otherOccupation', 'annualSalary'];
			vm.init = function(){
			
			var defer = $q.defer();
			vm.urlList = urlService.getUrlList();
			vm.inputDetails = persoanlDetailService.getMemberDetails();
			vm.deathCoverTransDetails = deathCoverService.getDeathCover();
		    vm.tpdCoverTransDetails = tpdCoverService.getTpdCover();
			vm.ipCoverTransDetails = ipCoverService.getIpCover();
			
			vm.waitingPeriodOptions = ['30 Days', '60 Days', '90 Days'];
			vm.benefitPeriodOptions = ['2 Years', '5 Years','Age 65']; 
			vm.OPD.applicant.cover[0].frequencyCostType = 'Monthly';
            vm.OPD.applicant.cover[1].frequencyCostType = 'Monthly';
            vm.OPD.applicant.cover[2].frequencyCostType = 'Monthly';

            vm.OPD.applicant.cover[0].coverDecision  = 'ACC';
            vm.OPD.applicant.cover[1].coverDecision  = 'ACC';
            vm.OPD.applicant.cover[2].coverDecision  = 'ACC';
            
			vm.OPD.applicant.cover[2].existingIpWaitingPeriod = vm.waitingPeriodOptions.indexOf(vm.ipCoverTransDetails.waitingPeriod) > -1 ? vm.ipCoverTransDetails.waitingPeriod : '90 Days' || '90 Days';
            vm.OPD.applicant.cover[2].existingIpBenefitPeriod= vm.benefitPeriodOptions.indexOf(vm.ipCoverTransDetails.benefitPeriod) > -1 ? vm.ipCoverTransDetails.benefitPeriod : '2 Years' || '2 Years';
            /*vm.OPD.applicant.cover[2].additionalIpWaitingPeriod = vm.OPD.applicant.cover[2].additionalIpWaitingPeriod;
            vm.OPD.applicant.cover[2].additionalIpBenefitPeriod= vm.OPD.applicant.cover[2].additionalIpBenefitPeriod;*/
			 if(vm.deathCoverTransDetails.type === '1'){
				 vm.OPD.applicant.cover[0].coverCategory='DcUnitised';
				deathCoverType = 'DcUnitised';
				 vm.OPD.applicant.cover[0].additionalUnit = vm.deathCoverTransDetails.units;
                 vm.OPD.applicant.cover[0].existingCoverAmount = vm.deathCoverTransDetails.amount || 0;
                 vm.OPD.applicant.cover[0].fixedUnit = vm.deathCoverTransDetails.units || 0;
			 }else if(vm.deathCoverTransDetails.type === '2'){
				deathCoverType = 'DcFixed';
				 vm.OPD.applicant.cover[0].coverCategory='DcFixed';
				 vm.OPD.applicant.cover[0].additionalUnit = vm.deathCoverTransDetails.amount;
                 vm.OPD.applicant.cover[0].existingCoverAmount = vm.deathCoverTransDetails.amount || 0;
			 }

			if(vm.tpdCoverTransDetails.type === '1'){
				tpdCoverType = 'TPDUnitised';
				vm.OPD.applicant.cover[1].coverCategory  = 'TPDUnitised';
				vm.OPD.applicant.cover[1].additionalUnit = vm.tpdCoverTransDetails.units;
                vm.OPD.applicant.cover[1].existingCoverAmount = vm.tpdCoverTransDetails.amount || 0;
                vm.OPD.applicant.cover[1].fixedUnit = vm.tpdCoverTransDetails.units || 0;
			}else if(vm.tpdCoverTransDetails.type === '2'){
				tpdCoverType = 'TPDFixed';
				vm.OPD.applicant.cover[1].coverCategory = 'TPDFixed';
                vm.OPD.applicant.cover[1].additionalUnit = vm.tpdCoverTransDetails.amount;
                vm.OPD.applicant.cover[1].existingCoverAmount = vm.tpdCoverTransDetails.amount || 0;
			}

			if (vm.ipCoverTransDetails.type === '1') {
            	vm.OPD.applicant.cover[2].additionalUnit = vm.ipCoverTransDetails.units;
            	vm.OPD.applicant.cover[2].fixedUnit = vm.ipCoverTransDetails.units || 0;
            	vm.OPD.applicant.cover[2].existingCoverAmount = vm.ipCoverTransDetails.amount || 0;

            } else if (vm.ipCoverTransDetails.type === '2') {
            	vm.OPD.applicant.cover[2].existingCoverAmount = vm.ipCoverTransDetails.amount;
            }
			
			$rootScope.$broadcast('enablepointer');

			/*Error Flags*/
			vm.dodFlagErr = null;
			vm.privacyFlagErr = null;

			var inputDetails = persoanlDetailService.getMemberDetails();
			vm.personalDetails = inputDetails.personalDetails;
			vm.contactDetails = vm.inputDetails.contactDetails || {};

		    QuoteService.getList(vm.urlList.quoteUrl,'MTAA').then(function(res){
		    	vm.IndustryOptions = res.data;
		    	defer.resolve(res);
		    }, function(err){
		    	console.log('Error while fetching industry options ' + JSON.stringify(err));
		    	defer.reject(err);
		    });
			
		    MaxLimitService.getMaxLimits(vm.urlList.maxLimitUrl,'MTAA',inputDetails.memberType,'UWCOVER').then(function(res){
				var limits = res.data;
				vm.annualSalForOccUpgradeVal = limits[0].annualSalForUpgradeVal;
				defer.resolve(res);
			}, function(error){
				console.info('Something went wrong while fetching limits ' + error);
				defer.reject(error);
			});
		    
			return defer.promise;
		};
		
		vm.init().then(function() {
			angular.extend(vm.OPD.applicant, appData.getAppData().applicant);
            vm.OPD.dodCheck=appData.getAppData().dodCheck;
            vm.OPD.privacyCheck=appData.getAppData().privacyCheck;
            vm.OPD.clientRefNumber = appData.getAppData().clientRefNumber;
            vm.OPD.applicatioNumber = appData.getAppData().applicatioNumber;
            vm.OPD.applicant.age=parseInt(moment().diff(moment(vm.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) ;
			$scope.$watch( 'vm.formduty.$valid', function(newVal, oldVal) {
            	if(!newVal) {
					$('#PersonalDetailsSection').collapse('hide');
					$('#collapseprivacy').collapse('hide');
					$('#OccupationSection').collapse('hide');
				} else {
					$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
				}
            });
            
            $scope.$watch('vm.occUpgradePrivacyPolicyForm.$valid', function(newVal, oldVal) {
            	if(!newVal) {
            		$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
				} else {
					$('#PersonalDetailsSection').collapse('show');
					$('#OccupationSection').collapse('hide');
				}
            	});
            
            $scope.$watch('vm.workRatingContactForm.$valid', function(newVal, oldVal) {
            	if(!newVal) {
					$('#OccupationSection').collapse('hide');
				} else {
					$('#OccupationSection').collapse('show');
				}
            });
            
            if($stateParams.mode === '2' || $stateParams.mode === '3'){
        		$timeout(function() {
        			$('#PersonalDetailsSection').collapse('show');
                    $('#collapseprivacy').collapse('show');
                    $('#OccupationSection').collapse('show');
				}, 2000);
        		
        	}else{
        		$('#PersonalDetailsSection').collapse('hide');
                $('#collapseprivacy').collapse('hide');
                $('#OccupationSection').collapse('hide');
                vm.OPD.auraDisabled = false;
        	}
            
            if (vm.OPD.applicant.industryType) {
                fetchOccupationSvc.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.OPD.applicant.industryType).then(function(res) {
                    vm.OccupationList = res.data;
                    vm.getOccCategoryFromDB();
                }, function(err) {
                    console.info('Error while fetching occupations ' + JSON.stringify(err));
                });
            }
            if (vm.OPD.applicant.contactDetails.preferedContacType === null || vm.OPD.applicant.contactDetails.preferedContacType === undefined) {
	           	 vm.OPD.applicant.contactDetails.preferedContactNumber = '';
	           }
			
		});
		vm.getOccupations = function() {
            vm.OPD.applicant.occupation = '';
            vm.OPD.applicant.otherOccupation = '';
            vm.OPD.applicant.industryType = vm.OPD.applicant.industryType === '' ? null : vm.OPD.applicant.industryType;
            fetchOccupationSvc.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.OPD.applicant.industryType).then(function(res) {
                vm.OccupationList = res.data;
            }, function(err) {
                console.info('Error while fetching occupations ' + JSON.stringify(err));
            });
        };
	    
		vm.coltwo = false;
	  	vm.isCollapsible = function(targetEle, event) {
	  		if( targetEle === 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
	  			if($('#dodLabel').is(':visible')){
	  				vm.dodFlagErr = true;
	  			}
	  			event.stopPropagation();
	  			return false;
	  		} else if( targetEle === 'PersonalDetailsSection' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
	  			if($('#privacyLabel').is(':visible')){
	  				vm.privacyFlagErr = true;
	  			}
	  			event.stopPropagation();
	  			return false;
	  		}  else if( targetEle === 'OccupationSection' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $('#collapseOne form').hasClass('ng-invalid'))) {
	  			if($('#PersonalDetailsSection form').is(':visible')){
	  				vm.workRatingFormSubmit(vm.workRatingContactForm);
	  			}
	  			event.stopPropagation();
	  			return false;
	  		}
	  	};

	  	vm.toggleTwo = function(checkFlag) {
	        vm.coltwo = checkFlag;
	        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in'))){
	        	return false;
	        }
	        $('a[data-target=\'#collapseTwo\']').click(); /* Can be improved */
	    };

	    vm.privacyCol = false;
	    var dodCheck;
	    var privacyCheck;
	    var dodVal = 0;
	    var privacyVal = 0;
	    var contactVal = 0;
	    var occupationVal = 0;
	    vm.togglePrivacy = function(flag) {
	        vm.privacyCol = flag;
	        $('a[data-target=\'#collapseprivacy\']').click();

	    };
	    vm.toggleContact = function(checkFlag) {
	        vm.contactCol = checkFlag;
	        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in'))){
	        	return false;
	        }
	        $('a[data-target=\'#collapseOne\']').click(); /* Can be improved */

	    };
	    vm.checkDodState = function(){
	    	vm.OPD.dodCheck = !vm.OPD.dodCheck;
	    	if(vm.OPD.dodCheck){
                vm.dodFlagErr = false;
	        }else{
	                vm.dodFlagErr = true;
	        }
	    };

	    vm.checkPrivacyState  = function(){
	    	vm.OPD.privacyCheck =  !vm.OPD.privacyCheck;
	    	if(vm.OPD.privacyCheck){
                vm.privacyFlagErr = false;
         }else{
                vm.privacyFlagErr = true;
         }
	    };
	    vm.navigateToLandingPage = function (){
	    	ngDialog.openConfirm({
	            template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'>' + 
	            '<div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
	    };


	    vm.changePrefContactTime=function(value){
        	vm.OPD.applicant.contactDetails.preferedContactTime=value;
        };
        
        vm.changeGender=function(value){
        	vm.OPD.applicant.gender = value;
        };
        vm.changePrefContactType = function(value) {
            if (vm.OPD.applicant.contactDetails.preferedContacType === '1') {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.mobilePhone;
            } else if (vm.OPD.applicant.contactDetails.preferedContacType === '2') {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.homePhone;
            } else if (vm.OPD.applicant.contactDetails.preferedContacType === '3') {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.workPhone;
            } else {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = '';
            }
        };
		
	    var anb = parseInt(moment().diff(moment(vm.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

	   //Progressive validation
	    var workRatingFormFields = ['contactEmail', 'contactPhone','contactPrefTime','gender'];



	    vm.getOccCategoryFromDB = function(fromOccupation){
	    	if (fromOccupation && vm.OPD.applicant.occupation !== 'Other') {
                vm.OPD.applicant.otherOccupation = '';
            }
		    	if(vm.OPD.applicant.occupation !== undefined){
		    		if(fromOccupation)
		    			{
		    			vm.OPD.applicant.occupationDuties= null;
                        vm.OPD.applicant.tertiaryQue = null;
                        vm.OPD.applicant.workDuties = null;
                        vm.OPD.applicant.spendTimeOutside = null;
		    			}
		    		
			    	var occName = vm.OPD.applicant.industryType + ':' + vm.OPD.applicant.occupation;
		    		
			    	NewOccupationService.getOccupation(vm.urlList.newOccupationUrl, 'MTAA', occName).then(function(res){
			    		if(vm.OPD.applicant.cover[0].coverCategory === 'DcFixed'){
			    			vm.deathOccDBCategory = res.data[0].deathfixedcategeory;
			    		}else if(vm.OPD.applicant.cover[0].coverCategory === 'DcUnitised'){
			    			vm.deathOccDBCategory = res.data[0].deathunitcategeory;
			    		}
			    		if(vm.OPD.applicant.cover[1].coverCategory === 'TPDFixed'){
			    			vm.tpdOccDBCategory = res.data[0].tpdfixedcategeory;
			    		}else if(vm.OPD.applicant.cover[1].coverCategory === 'TPDUnitised'){
			    			vm.tpdOccDBCategory = res.data[0].tpdunitcategeory;
			    		}
			    		vm.ipOccDBCategory = res.data[0].ipfixedcategeory;
			    		vm.renderOccupationQuestions();
			    	}, function(err){
			    		console.info('Error while getting transfer category from DB ' + JSON.stringify(err));
			    	});
		    	}
	    	
	    };

	    vm.renderOccupationQuestions = function(){
		  		if(vm.OccupationList){
		  			var selectedOcc = vm.OccupationList.filter(function(obj){
			  			return obj.occupationName === vm.OPD.applicant.occupation;
			  		});
			  		var selectedOccObj = selectedOcc[0];

			  		if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
			  			vm.showWithinOfficeQuestion = true;
                        vm.showTertiaryQuestion = true;
                        vm.showHazardousQuestion = false;
                        vm.showOutsideOfficeQuestion = false;
                        if (vm.OPD.applicant.ownBusinessQue === 'Yes') {
                            workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
                        } else if (vm.OPD.applicant.ownBusinessQue === 'No') {
                            workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
                        }
			  			//workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
			  			if(vm.OPD.applicant.occupationDuties === 'Yes' && vm.OPD.applicant.tertiaryQue === 'Yes' && vm.OPD.applicant.annualSalary && parseFloat(vm.OPD.applicant.annualSalary) >= parseFloat(vm.annualSalForOccUpgradeVal)){
			  				vm.OPD.applicant.cover[0].occupationRating = 'Professional';
			  				vm.OPD.applicant.cover[1].occupationRating = 'Professional';
			  				vm.OPD.applicant.cover[2].occupationRating = 'Professional';
		  		    	} else{
		  		    		vm.OPD.applicant.cover[0].occupationRating = vm.deathOccDBCategory;
		  		    		vm.OPD.applicant.cover[1].occupationRating = vm.tpdOccDBCategory;
		  		    		vm.OPD.applicant.cover[2].occupationRating = vm.ipOccDBCategory;
		  		    	}
			  		} else if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'true'){
			  			vm.showWithinOfficeQuestion = false;
			  		    vm.showTertiaryQuestion = false;
			  		    vm.showHazardousQuestion = true;
			  		    vm.showOutsideOfficeQuestion = true;
			  		  if (vm.OPD.applicant.ownBusinessQue === 'Yes') {
                          workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation','hazardousQuestion', 'outsideOffice', 'annualSalary'];
                      } else if (vm.OPD.applicant.ownBusinessQue === 'No') {
                          workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion','outsideOffice','annualSalary'];
                      }
			  		    //workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','outsideOffice','tertiaryQuestion','annualSalary'];
			  		    if(vm.OPD.applicant.workDuties === 'No' && vm.OPD.applicant.spendTimeOutside === 'No'){
			  		    	vm.showWithinOfficeQuestion = true;
			  	  		    vm.showTertiaryQuestion = true;
			  	  		if (vm.OPD.applicant.ownBusinessQue === 'Yes') {
	                          workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
	                      } else if (vm.OPD.applicant.ownBusinessQue === 'No') {
	                          workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
	                      }
				  	  	    //workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
				  	  	vm.OPD.applicant.cover[0].occupationRating = 'Non Manual';
                        vm.OPD.applicant.cover[1].occupationRating = 'Non Manual';
                        vm.OPD.applicant.cover[2].occupationRating = 'Non Manual';
                        if (vm.OPD.applicant.occupationDuties === 'Yes' && vm.OPD.applicant.tertiaryQue === 'Yes' && vm.OPD.applicant.annualSalary && parseFloat(vm.OPD.applicant.annualSalary) >= parseFloat(vm.annualSalForOccUpgradeVal)) {
                        	vm.OPD.applicant.cover[0].occupationRating = 'Professional';
                        	vm.OPD.applicant.cover[1].occupationRating = 'Professional';
                        	vm.OPD.applicant.cover[2].occupationRating = 'Professional';
                        } else {
                        	vm.OPD.applicant.cover[0].occupationRating = 'Non Manual';
                        	vm.OPD.applicant.cover[1].occupationRating = 'Non Manual';
                        	vm.OPD.applicant.cover[2].occupationRating = 'Non Manual';
                        }
			  		    } else{
			  		    	vm.showWithinOfficeQuestion = false;
			  	  		    vm.showTertiaryQuestion = false;
				  	  		if (vm.OPD.applicant.ownBusinessQue === 'Yes') {
	                            workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
	                        } else if (vm.OPD.applicant.ownBusinessQue === 'No') {
	                        	workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
	                        }
				  	  		//workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
					  	  	vm.OPD.applicant.cover[0].occupationRating = vm.deathOccDBCategory;
	                        vm.OPD.applicant.cover[1].occupationRating = vm.tpdOccDBCategory;
	                        vm.OPD.applicant.cover[2].occupationRating = vm.ipOccDBCategory;
			  		    }
			  		} else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'true'){
			  			vm.showWithinOfficeQuestion = false;
			  		    vm.showTertiaryQuestion = false;
			  		    vm.showHazardousOccQuestion = true;
			  		    vm.showOutsideOfficeOccQuestion = true;
			  		  if (vm.OPD.applicant.ownBusinessQue === 'Yes') {
                          workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                      } else if (vm.OPD.applicant.ownBusinessQue === 'No') {
                          workRatingOccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                      }
			  		   // workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzQuestion','industry','occupation','hazardousQuestion','outsideOffice','annualSalary'];
			  		  if (vm.OPD.applicant.workDuties === 'No' && vm.OPD.applicant.spendTimeOutside === 'No') {
                      	vm.OPD.applicant.cover[0].occupationRating = 'Non Manual';
                      	vm.OPD.applicant.cover[1].occupationRating = 'Non Manual';
                      	vm.OPD.applicant.cover[2].occupationRating = 'Non Manual';
                      } else {
                      	vm.OPD.applicant.cover[0].occupationRating = 'General';
                      	vm.OPD.applicant.cover[1].occupationRating = 'General';
                          vm.OPD.applicant.cover[2].occupationRating = 'General';
                      }
			  		} else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
			  			vm.showWithinOfficeQuestion = false;
			  		    vm.showTertiaryQuestion = false;
			  		    vm.showHazardousOccQuestion = false;
			  		    vm.showOutsideOfficeOccQuestion = false;
			  		    workRatingOccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
				  		  vm.OPD.applicant.cover[0].occupationRating = vm.deathOccDBCategory;
	                      vm.OPD.applicant.cover[1].occupationRating = vm.tpdOccDBCategory;
	                      vm.OPD.applicant.cover[2].occupationRating = vm.ipOccDBCategory;
			  		}
		  		}
		  	 else{
		  		vm.showWithinOfficeQuestion = false;
	  		    vm.showTertiaryQuestion = false;
	  		    vm.showHazardousOccQuestion = false;
	  		    vm.showOutsideOfficeOccQuestion = false;
	  		    workRatingOccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
	  		  if (vm.deathOccDBCategory === 'Non Manual') {
              	vm.OPD.applicant.cover[0].occupationRating= 'General';
              } else if (vm.deathOccDBCategory === 'Professional') {
              	vm.OPD.applicant.cover[0].occupationRating = 'Professional';
              } else {
              	vm.OPD.applicant.cover[0].occupationRating = 'General';
              }

              if (vm.tpdOccDBCategory === 'Non Manual') {
              	vm.OPD.applicant.cover[1].occupationRating = 'General';
              } else if (vm.tpdOccDBCategory === 'Professional') {
              	vm.OPD.applicant.cover[1].occupationRating = 'Professional';
              } else {
              	vm.OPD.applicant.cover[1].occupationRating = 'General';
              }

              if (vm.ipOccDBCategory === 'Non Manual') {
              	vm.OPD.applicant.cover[2].occupationRating = 'General';
              } else if (vm.ipOccDBCategory === 'Professional') {
              	vm.OPD.applicant.cover[2].occupationRating = 'Professional';
              } else {
              	vm.OPD.applicant.cover[2].occupationRating = 'General';
              }
		  	}
	  	};


	     vm.checkPreviousMandatoryFields  = function (workRatingElementName,workRatingFormName){
	      	var WorkRatingformFields;
	      	if(workRatingFormName === 'workRatingContactForm'){
	      		WorkRatingformFields = workRatingFormFields;
	    	}else if(workRatingFormName === 'workRatingOccupForm'){
	    		if(vm.OPD.applicant.occupation !== undefined && vm.OPD.applicant.occupation === 'Other'){
	    			WorkRatingformFields = workRatingOthrOccupationFormFields;
	    		} else{
	    			WorkRatingformFields = workRatingOccupationFormFields;
	    		}
	    	}
	        var inx = WorkRatingformFields.indexOf(workRatingElementName);
	        if(inx > 0){
	          for(var i = 0; i < inx ; i++){
	            vm[workRatingFormName][WorkRatingformFields[i]].$touched = true;
	          }
	        }
	      };

	   
	     vm.workRatingFormSubmit =  function (form){
	        if(!form.$valid){
	      	  form.$submitted=true;
	      	  if(form.$name === 'vm.workRatingContactForm'){
	      		vm.toggleTwo(false);
	      	  }
	  	    }else{
	  		  if(form.$name === 'vm.workRatingContactForm'){
	  			if(!checkAppNum)
				{
				if(fetchAppnum){
	    			fetchAppnum = false;
	    			vm.OPD.applicatioNumber = appData.getAppData().applicatioNumber;
	    		}
				}
	      	    vm.toggleTwo(true);
	  		  }else if(form.$name === 'vm.workRatingOccupForm'){
	  			
	  			    vm.goToAura();
	  			    $rootScope.$broadcast('disablepointer');
	   		  }
	       }
	     };

	     vm.invalidSalAmount = false;
		    vm.checkValidSalary = function(){
		    	vm.renderOccupationQuestions();
		    	$timeout(function() {
					vm.invalidSalAmount = false;
					if(parseInt(vm.OPD.applicant.annualSalary) === 0){
						vm.invalidSalAmount = true;
					}
				},100);
		    };

	     vm.auraDisabled = false;
	     
	     vm.continueWorkRatingPage = function(print){
	    	 vm.renderOccupationQuestions();
	    	 vm.validCategories = ['non manual', 'general', 'professional'];
	    	 var occDeathRating = vm.deathCoverTransDetails.occRating||'';
	    	 occDeathRating=occDeathRating.toLowerCase();
	    	 var occTpdRating = vm.tpdCoverTransDetails.occRating||'';
	    	 occTpdRating=occTpdRating.toLowerCase();
	    	 
	    	 
	    	 
	    	 if(deathCoverType === 'DcFixed'){
	    		 occDeathRating = vm.validCategories.indexOf(occDeathRating) > -1 ? occDeathRating : '';
	 		}else if(deathCoverType === 'DcUnitised'){
	 			occDeathRating = vm.validCategories.indexOf(occDeathRating) > -1 ? occDeathRating : 'general';
	 		}
	 		if(tpdCoverType === 'TPDFixed'){
	 			occTpdRating = vm.validCategories.indexOf(occTpdRating) > -1 ? occTpdRating : '';
	 		}else if(tpdCoverType === 'TPDUnitised'){
	 			occTpdRating = vm.validCategories.indexOf(occTpdRating) > -1 ? occTpdRating : 'general';
	 		}
	    	 
	    	 
	    	 if(vm.OPD.applicant.cover[0].occupationRating === 'Professional' && (occDeathRating === 'general' || occDeathRating === 'general')){
	    		 deathUpgrade = true;
	    	 } else if(vm.OPD.applicant.cover[0].occupationRating === 'General' && occDeathRating === 'non manual'){
	    		 deathUpgrade = true;
	    	 }
	    	 
	    	 if(vm.OPD.applicant.cover[1].occupationRating === 'Professional' && (occTpdRating === 'general' || occTpdRating === 'non manual')){
	    		 tpdUpgrade = true;
	    	 } else if(vm.OPD.applicant.cover[1].occupationRating === 'General' && occTpdRating === 'non manual'){
	    		 tpdUpgrade = true;
	    	 }
	    	 var occIpRating = vm.ipCoverTransDetails.occRating||'General';
	    	 occIpRating=occIpRating.toLowerCase();
	    	 occIpRating = vm.validCategories.indexOf(occIpRating) > -1 ? occIpRating : '';
	    	 if(vm.OPD.applicant.cover[2].occupationRating === 'Professional' && (occIpRating === 'general' || occIpRating === 'non manual')){
	    		 ipUpgrade = true;
	    	 } else if(vm.OPD.applicant.cover[2].occupationRating === 'General' && (occIpRating === 'non manual' || occIpRating === '')){
	    		 ipUpgrade = true;
	    	 }
	    	 var ruleModel = {
	         		'age': anb,
	         		'fundCode': 'MTAA',
	         		'gender': vm.OPD.applicant.gender,
	         		'deathOccCategory': vm.OPD.applicant.cover[0].occupationRating,
	         		'tpdOccCategory': vm.OPD.applicant.cover[1].occupationRating,
	         		'ipOccCategory': vm.OPD.applicant.cover[2].occupationRating,
	         		'smoker': false,
	         		'deathFixedCost': null,
	         		'deathUnitsCost': null,
	         		'tpdFixedCost': null,
	         		'tpdUnitsCost': null,
	         		'ipUnits': parseInt(vm.OPD.applicant.cover[2].additionalUnit),
	         		'ipFixedAmount':  parseInt(vm.OPD.applicant.cover[2].additionalcoveramount),
	         		'ipUnitsCost': null,
	         		'premiumFrequency': vm.OPD.applicant.cover[0].frequencyCostType,
	         		'memberType': null,
	         		'manageType': 'UWCOVER',
	         		'ipCoverType': 'IpUnitised',
	         		'ipWaitingPeriod': vm.OPD.applicant.cover[2].existingIpWaitingPeriod,
	         		'ipBenefitPeriod': vm.OPD.applicant.cover[2].existingIpBenefitPeriod
	         	};
	    	 if(vm.deathCoverTransDetails.type === '1'){
	 			ruleModel.deathCoverType = 'DcUnitised';
	 			ruleModel.deathUnits = parseInt(vm.deathCoverTransDetails.units);
	 		} else if(vm.deathCoverTransDetails.type === '2'){
	 			ruleModel.deathCoverType = 'DcFixed';
	 			ruleModel.deathFixedAmount =  parseInt(vm.deathCoverTransDetails.amount);
	 		}

	 		if(vm.tpdCoverTransDetails.type === '1'){
	 			ruleModel.tpdCoverType = 'TPDUnitised';
	 			ruleModel.tpdUnits = parseInt(vm.tpdCoverTransDetails.units);
	 		} else if(vm.tpdCoverTransDetails.type === '2'){
	 			ruleModel.tpdCoverType = 'TPDFixed';
	 			ruleModel.tpdFixedAmount =  parseInt(vm.tpdCoverTransDetails.amount);
	 		}

	 		CalculateService.calculate(ruleModel,vm.urlList.calculateUrl).then(function(res){
	 			var premium = res.data;
				for(var i = 0; i < premium.length; i++){
	    			if(premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised'){
	    				vm.OPD.applicant.cover[0].cost = premium[i].cost;
	    				vm.OPD.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
	    			} else if(premium[i].coverType === 'TPDFixed' || premium[i].coverType === 'TPDUnitised'){
	    				vm.OPD.applicant.cover[1].cost = premium[i].cost;
	    				vm.OPD.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
	    			} else if(premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised'){
	    				vm.OPD.applicant.cover[2].cost = premium[i].cost;
	    				vm.OPD.applicant.cover[2].additionalCoverAmount = premium[i].coverAmount;
	    			}
	    		}
				vm.OPD.totalMonthlyPremium = parseFloat(vm.OPD.applicant.cover[0].cost) + parseFloat(vm.OPD.applicant.cover[1].cost) + parseFloat(vm.OPD.applicant.cover[2].cost);

				if(!print){
					 var selectedIndustry;
					if(deathUpgrade || tpdUpgrade || ipUpgrade){
		  				vm.OPD.auraDisabled = false;
		  				if (vm.formduty.$valid && vm.occUpgradePrivacyPolicyForm.$valid && vm.workRatingContactForm.$valid && vm.workRatingOccupForm.$valid) {
		                    selectedIndustry = vm.IndustryOptions.filter(function(obj) {
		                        return vm.OPD.applicant.industryType === obj.key;
		                    });
		                    vm.OPD.applicant.occupationCode = selectedIndustry[0].value;
		                    vm.OPD.applicant.industryType = selectedIndustry[0].key;
		                    vm.OPD.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
		                    appData.setAppData(vm.OPD);
		      	  		$location.path('/auraocc/1');
		  				}
		      		}else{
		      			vm.OPD.auraDisabled = true;
		      			if (vm.formduty.$valid && vm.occUpgradePrivacyPolicyForm.$valid && vm.workRatingContactForm.$valid && vm.workRatingOccupForm.$valid) {
		                    selectedIndustry = vm.IndustryOptions.filter(function(obj) {
		                        return vm.OPD.applicant.industryType === obj.key;
		                    });
		                    vm.OPD.applicant.occupationCode = selectedIndustry[0].value;
		                    vm.OPD.applicant.industryType = selectedIndustry[0].key;
		                    vm.OPD.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
		                    appData.setAppData(vm.OPD);
		      			$location.path('/workRatingSummary/1');
		      			}
		      		}
				}
	 		}, function(err){
	 			console.info('Error while calculating premium ' + JSON.stringify(err));
	 		});
	     };

		

	    vm.goToAura = function (){
	    	if(this.workRatingContactForm.$valid && this.workRatingOccupForm.$valid){
		    	vm.continueWorkRatingPage(false);
	    	}
	    };

	    vm.saveWorkRating = function (){
	    		var savedObject =  vm.OPD;
	    		
	    		if(savedObject != null && !vm.invalidSalAmount){
	    			
	    			savedObject.lastSavedOn = 'QuoteUpdatePage';
	    			auraResponseService.setResponse(savedObject);
	                  
	                  console.log(JSON.stringify(savedObject));
	                  
	                  saveEapply.reqObj(vm.urlList.saveEapplyUrlNew).then(function(response) {
	                    console.log(response.data);
	                    vm.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+vm.OPD.applicatioNumber+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
	                  },function(err) {
	                    console.log('Something went wrong while saving...'+JSON.stringify(err));
	                  });
	    		}
	    		
        	
	    };

	    vm.saveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
				    template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Application saved </h4><!-- Row starts --><div class=\'row  rowcustom\'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>'+hhText+
				    '</div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog=\'secondDialogId\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-dialog-close-previous=\'\' ng-click=\'preCloseCallback()\'>Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = '/landing';
					       $location.path( url );
					       return true;
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
	  vm.clickToOpen = function (hhText) {

			var dialog = ngDialog.open({
					template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Helpful hints</h4><!-- Row starts --><div class=\'row rowcustom\' style=\'margin:0px -35px;\'><div class=\'col-sm-12\'><p class=\'aligncenter\'></p><div id=\'tips_text\'>'+hhText+
					'</div><p></p></div></div><!-- Row ends --></div><div class=\'row\'><div class=\'col-sm-4\'></div><div class=\'col-sm-4 col-12\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Close</button></div><div class=\'col-sm-4\'></div></div></div>',
					className: 'ngdialog-theme-plain',
					plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};

	    if($stateParams.mode === 2){
	    	
	    	if(!vm.OPD || vm.OPD===null || vm.OPD===undefined) {
	    		$location.path('/quoteoccchange/1');
	    		return false;
	    	}

	    	angular.extend(vm.OPD.applicant, appData.getAppData().applicant);
            vm.OPD.dodCheck=appData.getAppData().dodCheck;
            vm.OPD.privacyCheck=appData.getAppData().privacyCheck;
            vm.OPD.clientRefNumber = appData.getAppData().clientRefNumber;
            vm.OPD.applicatioNumber = appData.getAppData().applicatioNumber;
            vm.OPD.applicant.contactDetails.preferedContacType = vm.contactDetails.prefContact || '1';
            if (vm.OPD.applicant.contactDetails.preferedContacType === '1') {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.mobilePhone;
            } else if ( vm.OPD.applicant.contactDetails.preferedContacType === '2') {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.homePhone;
            } else if ( vm.OPD.applicant.contactDetails.preferedContacType === '3') {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.workPhone;
            } else {
            	vm.OPD.applicant.contactDetails.preferedContactNumber = '';
            }
	    	
			$('#dodLabel').addClass('active');
			$('#privacyLabel').addClass('active');

	    	OccupationService.getOccupationList(vm.urlList.occupationUrl,'MTAA',vm.OPD.applicant.industryType).then(function(res){
	        	vm.OccupationList = res.data;
	        	var temp = vm.OccupationList.filter(function(obj){
	        		return obj.occupationName === vm.OPD.applicant.occupation;
	        	});
	        	vm.OPD.applicant.occupation = temp[0].occupationName;
	        	vm.getOccCategoryFromDB();
	        }, function(err){
	        	console.log('Error while fetching occupation options ' + JSON.stringify(err));
	        });
	    	vm.togglePrivacy(true);
			vm.toggleContact(true);
	    	vm.toggleTwo(true);
	    }

	    if($stateParams.mode === 3){
	        	RetrieveAppDetailsService.retrieveAppDetails(vm.urlList.retrieveAppUrlNew,parseInt(fetchAppNumberSvc.getAppNumber())).then(function(res){
	        		var result = res.data[0];
	        		angular.extend(vm.OPD.applicant, res.data[0].applicant);
	            	checkAppNum=true;
	            	OccupationService.getOccupationList(vm.urlList.occupationUrl,'MTAA',vm.OPD.applicant.industryType).then(function(response){
	                	vm.OccupationList = response.data;
	                	var temp = vm.OccupationList.filter(function(obj){
	                		return obj.occupationName === result.applicant.occupation;
	                	});
	                	vm.OPD.applicant.occupation = temp[0].occupationName;
	                	vm.getOccCategoryFromDB();
	                }, function(err){
	                	console.log('Error while fetching occupation options ' + JSON.stringify(err));
	                });

	        	},function(err){
	        		console.log('Something went wrong while retrieving'+ JSON.stringify(err));
	        	});
	        }

	    vm.generatePDF = function(){
	    	vm.continueWorkRatingPage(true);
	    	$rootScope.$broadcast('disablepointer');
            var selectedIndustry = vm.IndustryOptions.filter(function(obj) {
                return vm.OPD.applicant.industryType === obj.key;
            });
            vm.OPD.applicant.occupationCode = selectedIndustry[0].value;
            vm.OPD.applicant.occupationCode  = selectedIndustry[0].value;
            vm.OPD.applicant.industryType = selectedIndustry[0].key;
            vm.OPD.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
            vm.OPD = angular.extend(vm.OPD, vm.inputDetails);
            auraRespSvc.setResponse(vm.OPD);
            printPageSvc.reqObj(vm.urlList.printQuotePageNew).then(function(response) {
                appData.setPDFLocation(response.data.clientPDFLocation);
                vm.downloadPDF();
            }, function(err) {
                $rootScope.$broadcast('enablepointer');
                console.info('Something went wrong while generating pdf...' + JSON.stringify(err));
            });
	    };

	    vm.downloadPDF = function(){
		    var pdfLocation =null;
		    var filename = null;
		   	var a = null;
	    	pdfLocation = appData.getPDFLocation();
	    	console.log(pdfLocation+'pdfLocation');
	    	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  		a = document.createElement('a');
	  	    document.body.appendChild(a);
	  	    DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
					if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
	   	            window.navigator.msSaveBlob(res.data.response,filename);
	   	        }else{
		  	        var fileURL = URL.createObjectURL(res.data.response);
		  	        a.href = fileURL;
		  	        a.download = filename;
		  	        a.click();
	   	        }
	  		}, function(err){
	  			console.log('Error downloading the PDF ' + err);
	  		});
	  	};


	}
})(angular);
