angular.module('Metlife.corp.directives', [])
  .directive('loading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (value) {
          if (value) {
            element.removeClass('ng-hide');
          } else {
            element.addClass('ng-hide');
          }
        });
      }
    };
  }])
  .directive('alphaOnly', [function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          var transformedInput = text.replace(/[^a-zA-Z ]/g, '');
          //console.log(transformedInput);
          if (transformedInput !== text) {
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
          }
          return transformedInput;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('nameOnly', [function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          var transformedInput = text.replace(/[^a-zA-Z '-]/g, '');
          //console.log(transformedInput);
          if (transformedInput !== text) {
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
          }
          return transformedInput;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('checkboxAndRadioUpdate', ['$http', function ($http) {
    return {
      replace: false,
      require: 'ngModel',
      scope: false,
      link: function (scope, element, attrs, ngModelCtrl) {
        element.on('change', function () {
          scope.$apply(function () {
            ngModelCtrl.$setViewValue(element[0].type.toLowerCase() === 'radio' ? element[0].value : element[0].checked);
          });
        });
      }
    };
  }])
  .directive('numbersOnly', [function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = text.replace(/[^0-9]/g, '');

            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('numbersWithSlashOnly', [function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          if (text) {
            var transformedInput = text.replace(/[^0-9/]/g, '');

            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          return undefined;
        }
        ngModelCtrl.$parsers.push(fromUser);
      }
    };
  }])
  .directive('limitTo', [function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elem, attrs, ngModelCtrl) {
        angular.element(elem).on('keypress', function (e) {
          var maxlength = Number(attrs.limitTo);
          function fromUser(text) {
              if (text.length > maxlength) {
                var transformedInput = text.substring(0, maxlength);
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
                return transformedInput;
              } 
              return text;
          }
          ngModelCtrl.$parsers.push(fromUser);
        });
      }
    };
  }])
  .directive('datepicker', function () {
    return {
      require: 'ngModel',
      link: function (scope, el, attr, ngModel) {
        $(el).datepicker({
          dateFormat: 'dd/mm/yy',
          onSelect: function (dateText) {
            scope.$apply(function () {
              ngModel.$setViewValue(dateText);
            });
          },
          changeMonth: true,
          autoSize: true,
          changeYear: true,
          maxDate: '+0D'
        });
      }
    };
  })

  .directive('datePickerDob', function (APP_CONSTANTS) {
    return {
      require: 'ngModel',
      link: function (scope, el, attr, ngModel) {
        $(el).datepicker({
					showOn: 'both',
					buttonImage: APP_CONSTANTS.path.images + '/cal-icon.png',
					buttonImageOnly: true,
          dateFormat: 'dd/mm/yy',
          onSelect: function (dateText) {
            scope.$apply(function () {
              ngModel.$setViewValue(dateText);
            });
          },
          changeMonth: true,
          changeYear: true,
          autoSize: true,
					minDate: '-120Y',
					maxDate: '+0D'
        });
      }
    };
  })
  .directive('numbersWithDecimals', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ngModelCtrl) {

        if (!ngModelCtrl) {
          return;
        }

        ngModelCtrl.$parsers.push(function (val) {

          if (angular.isUndefined(val)) {
            val = '';
          }

          var clean = val.replace(/[^0-9\.]/g, '');
          var negativeCheck = clean.split('-');
          var decimalCheck = clean.split('.');
          if (!angular.isUndefined(negativeCheck[1])) {
            negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
            clean = negativeCheck[0] + '-' + negativeCheck[1];
            if (negativeCheck[0].length > 0) {
              clean = negativeCheck[0];
            }

          }

          if (!angular.isUndefined(decimalCheck[1])) {
            decimalCheck[1] = decimalCheck[1].slice(0, 1);
            clean = decimalCheck[0] + '.' + decimalCheck[1];
          }

          if (val !== clean) {
            ngModelCtrl.$setViewValue(clean);
            ngModelCtrl.$render();
          }
          return clean;
        });

        element.bind('keypress', function (event) {
          if (event.keyCode === 32) {
            event.preventDefault();
          }
        });
      }
    };
  })
  .directive('format', ['$filter', function ($filter) {
    return {
      require: '?ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ctrl) {
        if (!ctrl) {return;}
        elem.bind('focus', function (event) {
          var newVal = $(this).val().replace(/,/g, '');
          $(this).val(newVal);
        });
        elem.bind('blur', function(event) {
          var newVal = $(this).val().replace(/,/g, '');
          if(newVal) {
            $(this).val($filter(attrs.format)(newVal));
          } else {
            elem.val(newVal);
          }  
        });
        ctrl.$formatters.unshift(function (a) {
          return $filter(attrs.format)(ctrl.$modelValue);
        });
        ctrl.$parsers.unshift(function (viewValue) {
          var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
          if(plainNumber) {
            elem.val($filter(attrs.format)(plainNumber));
          } else {
            elem.val(plainNumber);
          }  
          return plainNumber;
        });
      }
    };
}])
  .directive('phoneOnly', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attrs, modelCtrl) {

        modelCtrl.$parsers.push(function (inputValue) {
          var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;

          if (transformedInput !== inputValue) {
            modelCtrl.$setViewValue(transformedInput);
            modelCtrl.$render();
          }

          return transformedInput;
        });

        $(element).on('keyup', function () {
          var TempVal = $(this).val().replace(/[,.]/g, '');
          var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        });
      }
    };
  });