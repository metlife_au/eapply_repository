package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class VOWCoverDetailPageObjects extends PageObjectLibrary{
		
	public static final By txtareainput = getObjectIdentifaction("vow.coverdetails.textarea1.id");
	public static final By btnNext = getObjectIdentifaction("vow.coverdetails.next.btn.css");

}
