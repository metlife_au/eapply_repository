package au.com.metlife.eapply.b2b.model;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AuthTokenRepository extends JpaRepository<AuthToken, String>{
	@Query("SELECT t FROM AuthToken t where t.authtoken = ?1 AND t.createdate > ?2")
	public AuthToken findByAuthtoken(String authtoken,Timestamp createdate);	
}

