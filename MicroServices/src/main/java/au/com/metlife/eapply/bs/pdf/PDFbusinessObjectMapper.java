/* --------------------------------------------------------------------------------------------------------------------------- 
 * Description: EappRulesCacheSerlvet class is the backing bean for UC016- Cover Details
 * ---------------------------------------------------------------------------------------------------------------------------
 * Copyright @ 2009 Metlife, Inc. 
 * ---------------------------------------------------------------------------------------------------------------------------
 * Author :     Ravi Reddy
 * Created:     17/7/2009
 * ---------------------------------------------------------------------------------------------------------------------------
 * Modification Log:	mm/dd/yyyy
 * ---------------------------------------------------------------------------------------------------------------------------
 * Date				Author					Description         
 * 7/16/2012	   Pushkar Vashishtha       Setting New questions in MemberDetails for YBR PDF
 * 7/17/2012	   Pushkar Vashishtha       Setting IndustryValue instead of IndustryCode
 * {Please place your information at the top of the list}
 * ---------------------------------------------------------------------------------------------------------------------------
 */
package au.com.metlife.eapply.bs.pdf;




import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.constants.EapplicationPdfConstants;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
/*import com.metlife.common.utility.CoverRankComparator;*/
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureStatement;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberProductDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.pdf.pdfObject.PersonalDetail;
import au.com.metlife.eapply.bs.pdf.pdfObject.TransferPreviousQuestionAns;
import au.com.metlife.eapply.bs.utility.CommonPDFHelper;
import au.com.metlife.eapply.helper.CorporateHelper;
import au.com.metlife.eapply.quote.utility.QuoteConstants;

public class PDFbusinessObjectMapper {
	private static final Logger log = LoggerFactory.getLogger(PDFbusinessObjectMapper.class);
	
	 public static final String PACKAGE_NAME = "com.metlife.eapply.pdf";
	 public static final String CLASS_NAME = "PDFbusinessObjectMapper";
	 


	 
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public PDFObject getPdfOjectsForEapply(Eapplication applicationDTO, EapplyInput eapplyInput, String lob, String propertyLoc) throws IOException{
		log.info("Pdf object mapping start");
		PDFObject pdfObject = new PDFObject();
		String country = null;	
		MemberProductDetails memberProductDetails = null;
		ArrayList<MemberProductDetails> memberProductDtsList = null;	
		MemberDetails memberDetails = null;
		Applicant applicantDTO = null;
		Cover coversDTO = null;
		Double cost = null;
		StringBuilder alldecreasons = new StringBuilder();
		java.util.Properties property = new java.util.Properties();
		/*String PdfPropertyFileLoc = null;*/
        File file = null;
        InputStream url = null;
        /*Boolean specialCondition = Boolean.FALSE;*/
       /* String mixedRUW = null;   */     
		ArrayList<PersonalDetail> personalDtslist = new ArrayList<>();
		DisclosureStatement disclosureStatement = null;
		List<DisclosureStatement> disclosureStatementList = new ArrayList<>();
		List<DisclosureStatement> disclosureStatementListGc = new ArrayList<>();
		
		if(null!=applicationDTO){
			file = new File( propertyLoc);
			log.info("propertyLoc>>> {}",propertyLoc);
	         url = new FileInputStream( file);    
	        property.load( url);
			memberDetails = new MemberDetails();
			log.info("url>>> {}",url);
			if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob)){
				memberDetails.setBrand(applicationDTO.getPartnercode());
			}			
			
			//One more condition needs to be added 
			if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.isQuickQuoteRender()) {
				pdfObject.setQuickQuoteRender(true);
			}
			/*if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType())
					&& "true".equalsIgnoreCase(eapplyInput.getAuraDisabled()) && !eapplyInput.isConvertCheck())
			{
				pdfObject.setDisableGenConsent(true);
			}*/
			/* if (null != applicationDTO.getFundinfo()
			            && null != applicationDTO.getFundinfo().getExistingLoadings()){
				 memberDetails.setExistingLoading(applicationDTO.getFundinfo().getExistingLoadings());
			 }else{
				 memberDetails.setExistingLoading("None");
			 }
			 if (null != applicationDTO.getFundinfo()
		                && null != applicationDTO.getFundinfo().getExistingExclusion()){
				 memberDetails.setExistingExclusion(applicationDTO.getFundinfo().getExistingExclusion());
			 }else{
				 memberDetails.setExistingExclusion("None");
			 }*/
			 memberDetails.setExistingExclusion("None");
			 if(null!=applicationDTO.getTotalMonthlyPremium()){
				memberDetails.setTotalPremium(CommonPDFHelper.formatAmount(applicationDTO.getTotalMonthlyPremium().toString()));
		     }else{
		    	 memberDetails.setTotalPremium(CommonPDFHelper.formatAmount("0"));
		     }
			 pdfObject.setApplicationDecision(applicationDTO.getAppdecision());
			 
			 pdfObject.setRequestType(applicationDTO.getRequesttype());			 
			 /*pdfObject.setDecCompany(applicationDTO.getDecCompany()); */
		/*	 pdfObject.setDecPrivacy(applicationDTO.getDecPrivacy());
			 pdfObject.setDecGC(applicationDTO.getDecGC()); 
			 pdfObject.setDecDDS(applicationDTO.getDecDDS());*/ 
			 /*pdfObject.setDecPrivacyNoAuth(applicationDTO.getDecPrivacyNoAuth());*/
			 if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) && !"INGD".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
			 pdfObject.setDecPrivacy(property.getProperty("label_privacy_stmt_pdf"));
			 }
			 pdfObject.setDecGC(property.getProperty("label_genral_concent_pdf"));
			 pdfObject.setDecDDS(property.getProperty("label_duty_disclouser_pdf")); 
		      if(!CorporateHelper.isNullOrEmpty(eapplyInput.getDeathFul()) && "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
		    	  pdfObject.setFulDeathAmount(eapplyInput.getDeathFul());
		      }
		       if(!CorporateHelper.isNullOrEmpty(eapplyInput.getTpdFul()) &&  "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
		    	  pdfObject.setFulTPDAmount(eapplyInput.getTpdFul());
		      }
		       if(!CorporateHelper.isNullOrEmpty(eapplyInput.getIpFul()) &&  "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
		    	  pdfObject.setFulIPAmount(eapplyInput.getIpFul());
		      }
		    
			 

			 /*if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
				 disclosureStatement = new DisclosureStatement();
				 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_pdf"));
				 disclosureStatement.setAnswer("true");
				 disclosureStatementList.add(disclosureStatement);
				 disclosureStatement = new DisclosureStatement();
				 disclosureStatement.setStatement(property.getProperty("label_privacy_stmt_pdf"));
				 disclosureStatement.setAnswer("true");
				 disclosureStatementList.add(disclosureStatement);
				 disclosureStatement = new DisclosureStatement();
				 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_tccover_pdf"));
				 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_uwcover_pdf"));
				 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "NCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_nonmembr_pdf"));
				 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_iccover_pdf"));
				 }
				 else{
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_pdf"));
				 }
				 disclosureStatement.setAnswer("true");
				 disclosureStatement.setAckText(property.getProperty("label_genrl_ack_text"));
				 disclosureStatementListGc.add(disclosureStatement);
			 }else{
				 disclosureStatement = new DisclosureStatement();
				 disclosureStatement.setStatement(property.getProperty("label_privacy_stmt_pdf"));
				 disclosureStatement.setAnswer("true");
				 disclosureStatementList.add(disclosureStatement);
				 disclosureStatement = new DisclosureStatement();
				 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_tccover_pdf"));
				 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_uwcover_pdf"));
				 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "NCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_nonmembr_pdf"));
				 }else{
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_pdf"));
				 }
				 disclosureStatement.setAnswer("true");
				 disclosureStatementList.add(disclosureStatement);
				 disclosureStatement = new DisclosureStatement();
				 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_pdf"));
				 disclosureStatement.setAnswer("true");
				 disclosureStatementList.add(disclosureStatement);
			 }*/

			 disclosureStatement = new DisclosureStatement();
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_tccover_pdf"));
			 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_uwcover_pdf"));
			 }else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "NCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_nonmembr_pdf"));
			 }
			 //changes for VicSuper special cover
			 else if("SCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()) && !(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()))) {
				 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_scover_pdf"));
			 }
			//Added for Life event general consent Hostplus
			 else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "ICOVER".equalsIgnoreCase(applicationDTO.getRequesttype())
					 && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode())){
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_iccover_pdf"));
			 }
			 else{
				 disclosureStatement.setStatement(property.getProperty("label_genral_concent_pdf"));
				 if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) && MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType())
							&& "true".equalsIgnoreCase(eapplyInput.getAuraDisabled()) && !eapplyInput.isConvertCheck())
				 {
					 disclosureStatement.setStatement(property.getProperty("label_genral_concent_nonaura_pdf"));
				 }
			 }
			 disclosureStatement.setAnswer("true");
			 disclosureStatement.setAckText(property.getProperty("label_genrl_ack_text"));
			 
			 
			 /*POH-540 - General consent not in the Acknowledgement page for Cancel Cover but its in the PDF generated. - 169682*/
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
					&&  null!=applicationDTO.getAppdecision() && !"".equalsIgnoreCase(applicationDTO.getAppdecision()) && !"DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
				 disclosureStatementListGc.add(disclosureStatement);
			 }
			 else if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
					 && "HOST".equalsIgnoreCase(applicationDTO.getPartnercode()))
			 {
				 StringBuilder sb = new StringBuilder();
				 if(null!=eapplyInput.getCoverCancellation())
				 {
					 sb = sb.append(eapplyInput.getCoverCancellation().isDeath() ? EapplicationPdfConstants.DEATH : "")
							 .append(eapplyInput.getCoverCancellation().isDeath() && eapplyInput.getCoverCancellation().isTpd() && eapplyInput.getCoverCancellation().isSalContinuance() ? " , " : eapplyInput.getCoverCancellation().isDeath() && eapplyInput.getCoverCancellation().isTpd() && !eapplyInput.getCoverCancellation().isSalContinuance() ? MetlifeInstitutionalConstants.AND_STR : "")
							 .append(eapplyInput.getCoverCancellation().isTpd() ? EapplicationPdfConstants.TPD : eapplyInput.getCoverCancellation().isDeath() && !eapplyInput.getCoverCancellation().isTpd() && eapplyInput.getCoverCancellation().isSalContinuance() ? MetlifeInstitutionalConstants.AND_STR : "")
							 .append(eapplyInput.getCoverCancellation().isTpd() && eapplyInput.getCoverCancellation().isSalContinuance() ? MetlifeInstitutionalConstants.AND_STR : "")
							 .append(eapplyInput.getCoverCancellation().isSalContinuance() ? EapplicationPdfConstants.IP : "");
				 }
				 
				 
				 if(property.getProperty(MetlifeInstitutionalConstants.LABEL_GENERAL_ACK)!=null && property.getProperty(MetlifeInstitutionalConstants.LABEL_GENERAL_ACK).contains("<%CancelCovers%>"))
				 {
					 disclosureStatement.setStatement("");
					 disclosureStatement.setAnswer("true");
					 disclosureStatement.setAckText(property.getProperty(MetlifeInstitutionalConstants.LABEL_GENERAL_ACK).replace("<%CancelCovers%>", sb.toString()));
					 disclosureStatementListGc.add(disclosureStatement);
				 }
			 
			 }
			 /*POH-540 - General consent not in the Acknowledgement page for Cancel Cover but its in the PDF generated.- 169682 */
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_duty_disclouser_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatement.setAckText(property.getProperty("label_dod_ack_text"));
			 /*POH-586 - Duty of Disclosure section not to be displayed for Cancel Cover PDF generated. - 169682 */
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatementList.add(disclosureStatement);
			 }
			 /*POH-586 - Duty of Disclosure section not to be displayed for Cancel Cover PDF generated. - 169682*/
			 /*disclosureStatementList.add(disclosureStatement);*/
			 if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) && !"INGD".equalsIgnoreCase(eapplyInput.getPartnerCode()) ) {
			 disclosureStatement = new DisclosureStatement();
			 disclosureStatement.setStatement(property.getProperty("label_privacy_stmt_pdf"));
			 disclosureStatement.setAnswer("true");
			 disclosureStatement.setAckText(property.getProperty("label_privacy_ack_text"));
			 /*POH-586 - Privacy Statement section not to be displayed for Cancel Cover PDF generated.- 169682 */
			 if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
				 disclosureStatementList.add(disclosureStatement);
			 }
			 }
			 /*POH-586 - Privacy Statement section not to be displayed for Cancel Cover PDF generated. - 169682*/
			/* disclosureStatementList.add(disclosureStatement);*/

			 /*END This is a temp fix to stop displaying family question. Code need to change later		*/
			/*if(null!=applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0){*/
			 if(null!=applicationDTO.getApplicant() && !(applicationDTO.getApplicant().isEmpty())){
				
				for(int itr=0;itr<applicationDTO.getApplicant().size(); itr++) {
					applicantDTO = applicationDTO.getApplicant().get(itr);
					if(null!=applicantDTO && EapplicationPdfConstants.APPLICANT_TYPE_PRIMARY.equalsIgnoreCase(applicantDTO.getApplicanttype())){	
						/*SRKR: 26/04/2016 - Inlcuded the below code to sort the covers in Deat,TPD,Trauma, IP always*/
						/*Collections.sort(applicantDTO.getCovers(),new CoverRankComparator());*/
						/*if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob) && null!=applicantDTO.getCustomerreferencenumber()){
							memberDetails.setClientEmailId(applicantDTO.getCustomerreferencenumber());
						}*/
						if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob) && null!=applicantDTO.getEmailid()){
							memberDetails.setClientEmailId(applicantDTO.getEmailid());
						}
						/*if(null!=applicantDTO.getAuradetails() && "Long".equalsIgnoreCase(applicantDTO.getAuradetails().getFormLength())){
							pdfObject.setFormLength("Long");
						}*/
						pdfObject.setFormLength("Long");
						if(null!=applicantDTO.getAustraliacitizenship() && "yes".equalsIgnoreCase(applicantDTO.getAustraliacitizenship())){
							memberDetails.setAustraliaCitizenship("Yes");
						}else{
							memberDetails.setAustraliaCitizenship("No");
						}
						memberDetails.setOccCategory(eapplyInput.getDeathOccCategory());
						
						memberDetails.setGender(applicantDTO.getGender());
						memberDetails.setMemberFName(applicantDTO.getFirstname());
						memberDetails.setMemberLName(applicantDTO.getLastname());
						memberDetails.setTitle(applicantDTO.getTitle());
						/*memberDetails.setEmailId(applicantDTO.getEmailid());*/
						/*Added for getting fund admin email from input XML*/
						/*if(null!=eapplyInput && null!=eapplyInput.getContactDetails() && null!=eapplyInput.getContactDetails().getEmailAddress()){
							memberDetails.setEmailId(eapplyInput.getContactDetails().getEmailAddress());
						}*/	
						if(null!=eapplyInput && null!=eapplyInput.getContactDetails() && null!=eapplyInput.getContactDetails().getFundEmailAddress()){
							memberDetails.setEmailId(eapplyInput.getContactDetails().getFundEmailAddress());
						}
						memberDetails.setClientRefNum(applicantDTO.getClientrefnumber());
						if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
						memberDetails.setDateJoinedFund(applicantDTO.getDatejoinedfund());
						}
						if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
							memberDetails.setClientRefNum(eapplyInput.getClientRefNumber());
						}
						memberDetails.setClientMatched(applicationDTO.getClientmatch());
						/*if("Yes".equalsIgnoreCase(applicationDTO.getClientmatch())){
							memberDetails.setClientMatched(applicationDTO.getClientmatch());
						}else{
							memberDetails.setClientMatched(Boolean.FALSE);
						}*/
						
						/*memberDetails.setDateJoinedCompany(dateJoinedCompany);*/						
						
						
						/*if(null!=applicantDTO.getNominatedadditionalcover() && "Yes".equalsIgnoreCase(applicantDTO.getNominatedadditionalcover())){
							memberDetails.setReplaceCvrOption(Boolean.FALSE);
						}else if(null!=applicantDTO.getNominatedadditionalcover() && "No".equalsIgnoreCase(applicantDTO.getNominatedadditionalcover())){
							memberDetails.setReplaceCvrOption(Boolean.TRUE);
						}*/						
						/*if(null!=applicantDTO.getSmoker() && "Yes".equalsIgnoreCase(applicantDTO.getSmoker())){
							memberDetails.setSmoker("Yes");
						}else if(null!=applicantDTO.getSmoker()){
							memberDetails.setSmoker("No");
						}*/
						 /*Added by Pushkar*/
							memberDetails.setMemberOccupation(applicantDTO.getOccupation());
							/*memberDetails.setMemberOtherOccupation(memberOtherOccupation);*/
							memberDetails.setMemberIndustry(applicantDTO.getIndustrytype());
							memberDetails.setEmpStatus(applicantDTO.getPermanentemptype());
							memberDetails.setAnnualSal(applicantDTO.getAnnualsalary());
							if(null!=applicantDTO.getMembertype() && !"".equalsIgnoreCase(applicantDTO.getMembertype())){
								memberDetails.setMemberType(applicantDTO.getMembertype());
							}
						
							
							/*if(null!=applicantDTO.getWorkduties()) && applicantDTO.getWorkDutiesManual()){
								memberDetails.setHazardousEnv("Yes");
							}else if(null!=applicantDTO.getWorkDutiesManual() && !applicantDTO.getWorkDutiesManual()){
								memberDetails.setHazardousEnv("No");
							}
							if(null!=applicantDTO.getSpendtimeoutside() && applicantDTO.getSpendTimeOutsideRadioSel()){
								memberDetails.setWorkOutsideOffice("Yes");
							}else if(null!=applicantDTO.getSpendTimeOutsideRadioSel() && !applicantDTO.getSpendTimeOutsideRadioSel()){
								memberDetails.setWorkOutsideOffice("No");
							}*/
						
						/*if(null!=applicantDTO.getSmoker() && "Yes".equalsIgnoreCase(applicantDTO.getSmoker())){
							memberDetails.setSmoker("Yes");
						}else if(null!=applicantDTO.getSmoker()){
							memberDetails.setSmoker("No");
						}*/
						
						StringBuilder addressBuilder = new StringBuilder();			
						if (null != eapplyInput && null!=eapplyInput.getAddress()) {
			                if (null != eapplyInput.getAddress().getCountry()) {
			                    country = eapplyInput.getAddress().getCountry();
			                } /*else {
			                    country = "Australia";
			                }*/	
			                memberDetails.setPrefferedContactNumber(eapplyInput.getContactDetails().getPrefContact());
			                if (null != eapplyInput.getAddress().getLine1()
			                		&& eapplyInput.getAddress().getLine1().trim().length()>0){
			                	addressBuilder.append(eapplyInput.getAddress().getLine1() + " ");
			                }	                
			                	
			                if (null != eapplyInput.getAddress().getLine2()
			                		&& eapplyInput.getAddress().getLine2().trim().length()>0){
			                	addressBuilder.append( eapplyInput.getAddress().getLine2() + ", ");			                	
			                }
			                if (null !=  eapplyInput.getAddress().getSuburb()
			                		&&  eapplyInput.getAddress().getSuburb().trim().length()>0){
			                	memberDetails.setSubrub(eapplyInput.getAddress().getSuburb());
			                	addressBuilder.append( eapplyInput.getAddress().getSuburb() + ", ");			                	
			                }
			                if (null != eapplyInput.getAddress().getState()
			                		&& eapplyInput.getAddress().getState().trim().length()>0){
			                	memberDetails.setState(eapplyInput.getAddress().getState());
			                	addressBuilder.append( eapplyInput.getAddress().getState() + ", ");			                	
			                }
			                if (null != country){
			                	memberDetails.setCountry(country);
			                	addressBuilder.append( country + ", ");			                	
			                }
			                	
			                if (null != eapplyInput.getAddress().getPostCode()){
			                	memberDetails.setPostCode(eapplyInput.getAddress().getPostCode());
			                	addressBuilder.append( eapplyInput.getAddress().getPostCode());
			                }
			            } else {
			            	addressBuilder.append( "");
			            }
						if(null!=eapplyInput.getDob()){
							memberDetails.setDob(eapplyInput.getDob());
						}			
						if(null!=eapplyInput.getContactDetails()){
							/*if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContactTime())){
								memberDetails.setPrefferedContactTime("9am - 12pm");
							}else{
								memberDetails.setPrefferedContactTime("12pm - 6pm");
							}*/
							memberDetails.setPrefferedContactTime(eapplyInput.getContactPrefTime());
							
							/*if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getMobilePhone());
								memberDetails.setPrefferedContactType("Mobile Phone");
							}else if("2".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getHomePhone());
								memberDetails.setPrefferedContactType("Home Phone");
							}else if("3".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getWorkPhone());
								memberDetails.setPrefferedContactType("Work Phone");
							}else{
								memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getEmailAddress());
								memberDetails.setPrefferedContactType("Email");
							}*/
								if(null!=eapplyInput.getContactType() && MetlifeInstitutionalConstants.MOBILE_PH.equalsIgnoreCase(eapplyInput.getContactType())){		
									memberDetails.setPrefContactDetails(eapplyInput.getContactPhone());
									memberDetails.setPrefferedContactType("Mobile Phone");
								}else if(null!=eapplyInput.getContactType() && ("Home".equalsIgnoreCase(eapplyInput.getContactType()) || "Home phone".equalsIgnoreCase(eapplyInput.getContactType()))){	
									memberDetails.setPrefContactDetails(eapplyInput.getContactPhone());
									memberDetails.setPrefferedContactType("Home Phone");
								}else if(null!=eapplyInput.getContactType() && ("Work".equalsIgnoreCase(eapplyInput.getContactType()) || "Work phone".equalsIgnoreCase(eapplyInput.getContactType()))){	
									memberDetails.setPrefContactDetails(eapplyInput.getContactPhone());
									memberDetails.setPrefferedContactType("Work Phone");
								}/*else{
									memberDetails.setPrefContactDetails(eapplyInput.getContactDetails().getEmailAddress());
									memberDetails.setPrefferedContactType("Email");
								}*/
						}						
						
						if(eapplyInput.getIndexationDeath()!= null && eapplyInput.getIndexationTpd()!= null){	
							if(eapplyInput.getIndexationDeath().equalsIgnoreCase("true") && eapplyInput.getIndexationTpd().equalsIgnoreCase("true")){
								memberDetails.setIndexationFlag("Yes");
							}else{
								memberDetails.setIndexationFlag("No");
							}
							if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode()) 
									&& (eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType() != null && eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType().equalsIgnoreCase(MetlifeInstitutionalConstants.DC_UNITISED)))
							{
								memberDetails.setIndexationFlag("NA");
							}
						}
						
						/**Vicssuper changes starts- purna**/
						if(eapplyInput.isIpDisclaimer()){
								memberDetails.setIpDisclaimer("Yes");
							}else{
								memberDetails.setIpDisclaimer("No");
							}		
												
						/**Vicssuper changes ends- purna**/
						if(eapplyInput.getUnitisedCovers()!= null){	
							if(eapplyInput.getUnitisedCovers().equalsIgnoreCase("true")){
								memberDetails.setUnitisedCvrsFlag("Yes");
							}else{
								memberDetails.setUnitisedCvrsFlag("No");
							}			
						}
						if(eapplyInput.getCancelreason()!= null){
							memberDetails.setCancelreason(eapplyInput.getCancelreason());
						}
						if(eapplyInput.getCancelotherreason()!= null){
							memberDetails.setCancelotherreason("Other - "+eapplyInput.getCancelotherreason());
						}
						
						memberDetails.setMemberAddress(addressBuilder.toString());
						memberDetails.setResponseObject(eapplyInput.getResponseObject());
						if("Transefer".equalsIgnoreCase(applicationDTO.getRequesttype())){
							memberDetails.setTransferCover(Boolean.TRUE);
						}						
						memberProductDtsList = new ArrayList<>();
						
						/*if(applicantDTO.getPersonalDtlList()!=null){
				        	personalDtslist = (ArrayList)applicantDTO.getPersonalDtlList();
						} else {
							personalDtslist = new ArrayList<PersonalDetail>();
						}
						if(null!=applicantDTO.getQuetionDtoVector() && applicantDTO.getQuetionDtoVector().size()>0){
							 PdfPropertyFileLoc =getPdfproperty(); ConfigurationHelper.getConfigurationValue( "PdfPropertyFileLoc", "PdfPropertyFileLoc");
					         file = new File( PdfPropertyFileLoc);
					         url = new FileInputStream( file);    
					        property.load( url);
							for(int itrQues=0;itrQues<applicantDTO.getQuetionDtoVector().size();itrQues++){
								QuestionDto questionDto = (QuestionDto)applicantDTO.getQuetionDtoVector().get(itrQues);
								if(null!=questionDto && property.getProperty(questionDto.getQuestionTextID())!=null && property.getProperty(questionDto.getQuestionTextID()).length()>0){
									PersonalDetail personalDetail = new PersonalDetail();
									personalDetail.setLabel(property.getProperty(questionDto.getQuestionTextID()));
									personalDetail.setValue(questionDto.getAnswerText());
									personalDtslist.add(personalDetail);
								}
							}							
						}*/
						memberDetails.setPersonalDts(personalDtslist);												
							/*if(null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
						if(null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
								
								 /*BigDecimal transferDeathAmtBD=null;*/
								 /*BigDecimal transferTpdAmtBD=null;*/
								for(int covItr=0;covItr<applicantDTO.getCovers().size();covItr++){
									 coversDTO = applicantDTO.getCovers().get(covItr);
									 if(("AEIS".equalsIgnoreCase(eapplyInput.getPartnerCode())||"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode()) || "INGD".equalsIgnoreCase(eapplyInput.getPartnerCode())) && ("Salary Continuance".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Income Protection");
										}
									 if(("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) && ("Total & Permanent Disablement".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Total & Permanent Disability");
										}
									 
									 if(("INGD".equalsIgnoreCase(eapplyInput.getPartnerCode())) && ("Total & Permanent Disablement".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Total Permanent Disablement");
										}
									 /**Added for vicsuper by Purna - starts**/
									 if((MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) && ("Salary Continuance".equalsIgnoreCase(coversDTO.getCovertype()))) {
										 coversDTO.setCovertype("Income Protection");
										}
									 /**Added for vicsuper by Purna - ends**/
									 if(null!=coversDTO){	
										 
										 memberDetails.setFrquencyOpted(coversDTO.getFrequencycosttype());
										 memberProductDetails = new MemberProductDetails();								
										 if(null!=coversDTO.getCost()){
											
											 log.info(">>>loading test>> {}",coversDTO.getLoading());
											 log.info(">>11>getPolicyfee test>> {}",coversDTO.getPolicyfee());
											 log.info(">>Application Decision>> {}",pdfObject.getApplicationDecision());
											 log.info(">>Cover Decision>> {}",coversDTO.getCoverdecision());
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision()))
													 && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getLoading() 													
													){
												 
												 log.info(">>>getPolicyfee test>> {}",coversDTO.getPolicyfee());
													Double additionalCost = 0.0;
													String laodtext = " additional ";
													/*if(null != coversDTO.getAddLoadingCost()){
														additionalCost = coversDTO.getAddLoadingCost();
														laodtext  = " additional ";
													}else if(coversDTO.getExistingCost()!=null){
														additionalCost = new Double(coversDTO.getExistingCost());
														laodtext  = " additional ";
													}*/
													if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
             									cost = new Double(coversDTO.getCost())+(additionalCost * coversDTO.getLoading().doubleValue()/100);
											   /* Double double1 = new Double(additionalCost * coversDTO.getLoading().doubleValue()/100);*/
											    memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount(cost.toString()));
													}
													if(!"CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
												if(coversDTO.getPolicyfee()!=null && coversDTO.getPolicyfee().compareTo(BigDecimal.ZERO)>0) {
													 /*StringBuilder loadingAmtTxt = new StringBuilder();*/
													String loadingAmtTxt="We wish to note that in considering the information disclosed in your personal statement your"+laodtext+"cover is subject to a loading of "+ coversDTO.getLoading().intValue()+"%, an"+laodtext+"premium in the amount of $"+ coversDTO.getPolicyfee().toString() + " will be applicable if you accept the terms of the cover."; 
													 /**
													  * To Display the oercentage value									  */
													
																						 
											
													 pdfObject.setLoadingInd(Boolean.TRUE);
													 memberProductDetails.setLoadings(loadingAmtTxt);	
													 
												 }
													}
													else {
														String loadingAmtTxt="We wish to note that in considering the information disclosed in your personal statement your"+laodtext+"cover is subject to a loading of "+ coversDTO.getLoading().intValue()+"%.This loading will be applicable if you accept the terms of the cover."; 
														 /**
														  * To Display the oercentage value									  */
														
																							 
												
														 pdfObject.setLoadingInd(Boolean.TRUE);
														 memberProductDetails.setLoadings(loadingAmtTxt);	
													}
											}else{
												/**
												  * Work Request 6026 - need to display the loading if it wavied of 
												  * need to implement
												  */
												
												/*if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision()))
														 && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())
														 && null!=coversDTO.getCoverOriginalLoading() && coversDTO.getCoverOriginalLoading().length()>0
														 && new BigDecimal(coversDTO.getCoverOriginalLoading()).doubleValue() > 25 
														 ){							
													
													
													if(new BigDecimal(coversDTO.getCoverOriginalLoading()).doubleValue() > 0){
														String loadingWavied = "During your application for additional cover a premium loading of "+ coversDTO.getCoverOriginalLoading() +"% has been assessed. However, this has been waived by your fund.";
														memberProductDetails.setLoadings(loadingWavied);
													}
													
												}*/					
												
												if(!"null".equalsIgnoreCase(coversDTO.getCost())) {
												 memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount(coversDTO.getCost()));
												}
												 if(coversDTO.getCost()== null){
													 memberProductDetails.setProductPremiumCost(CommonPDFHelper.formatAmount("0"));
												 }
											}										
										 }								
										 memberProductDetails.setProductDecision(coversDTO.getCoverdecision());
										 if(EapplicationPdfConstants.LOB_INSTITUTIONAL.equalsIgnoreCase(lob)){
											 /*SRKR:14/07/20Capture existing unitised cover amount based on new occupation category selected in Update work rating flow
										     * Applicable for NSFS and MTAA in UWCOVER flow.
										     * */
											 /*if(null != coversDTO && null != coversDTO.getNewExistingCoverAmount()){
												 memberProductDetails.setNewExistingCover(CommonPDFHelper.formatAmount(coversDTO.getNewExistingCoverAmount()));
											 }*/
											 
											 if(null != coversDTO && null != coversDTO.getFulamount()){
												 memberProductDetails.setFulCoverAmount(coversDTO.getFulamount());
											 }
											 										 
											 if(coversDTO.getAddunitind()!=null && "1".equalsIgnoreCase(coversDTO.getAddunitind())){
												if(null!=coversDTO.getCoverdecision() && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
													//code refactored since nullcheck should be at start
													if(new BigDecimal(Integer.toString(coversDTO.getAdditionalunit())).doubleValue() >0 && coversDTO.getAdditionalcoveramount()!=null){
													   /*memberProductDetails.setProductAmount(coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getFixedunit()+MetlifeInstitutionalConstants.UNITS);*/
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
													}else{
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
													}
												}else{
													/*if(null != coversDTO.getCoverPercentage()){
														memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+" Units  ), " +coversDTO.getCoverPercentage());	
													}else{
														memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
													}*/
													memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
												}
												if (("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode()) || /*Added for vics -purna*/MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()))
														&& QuoteConstants.MTYPE_CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
													//Refined this logic  as part sonarqube modifications  - Vicsuper By Purna
													setProductAmnt(coversDTO, memberProductDetails);
												}
												if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()) && !("IP".equalsIgnoreCase(coversDTO.getCovercode()))
														&& QuoteConstants.MTYPE_ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
												{
													memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getCoveramount()+MetlifeInstitutionalConstants.UNITS);
												}
											 }else if(null!=coversDTO.getCovercategory() && !memberDetails.isTransferCover() && !coversDTO.getCovercategory().contains("Fixed")){
												 if(!"DCL".equalsIgnoreCase(applicationDTO.getAppdecision()) ){
													
													 /**
													  * IP selection and non-ip section is the same logic. when we need a different format for death, tpd against IP with the decimals 
													  * This can be used and if we have to display the % of IP cover required - Bala16072014
													  */
													 
													 
													 if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
														 /*if("IP".equalsIgnoreCase(coversDTO.getCovercode())){*/
														//code refactored since nullcheck should be at start
														 if(coversDTO.getAdditionalcoveramount() == null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
														 else if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_FIXED));	 
															 }else{
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString()));
															 }
														/*}else{
															if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_FIXED));
																 	 
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }	*/ 
													 }else if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
														 if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
															//code refactored since nullcheck should be at start
															 if(coversDTO.getAdditionalcoveramount() == null){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
															 }
															 else if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 /*memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASH_N_UNITS);	*/
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 if(coversDTO.getAdditionalcoveramount()!=null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
																 } 
															 }else{
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString()));	
																
															 }
														 }else{
															//code refactored since nullcheck should be at start
															 if(coversDTO.getAdditionalcoveramount() == null){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
															 }
															 else if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 if(coversDTO.getAdditionalcoveramount()!=null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
																 }	 
															 }else{
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString()));	
															 }
														 }
													 }else if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision()) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision())){													 
														 /*if("IP".equalsIgnoreCase(coversDTO.getCovercode())){*/
														 //code refactored since nullcheck should be at start
														 if(coversDTO.getAdditionalcoveramount() == null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
														 else if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_FIXED));	 
															 }else{
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString()));	
																 
															 }
														 /*}else{
															 if(null != coversDTO.getAddunitind() && "1".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_UNITS);	 
															 }else if(null != coversDTO.getAddunitind() && "0".equalsIgnoreCase(coversDTO.getAddunitind()) && null != coversDTO.getTotalcoveramount() && new BigDecimal(""+coversDTO.getTotalcoveramount()).doubleValue()>0){
																 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount()+MetlifeInstitutionalConstants.SLASHBRACKET+coversDTO.getCovercategory()+" )"+MetlifeInstitutionalConstants.SLASH_N_FIXED));
															 }else{
																 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount().toString());	 
																 if(coversDTO.getAdditionalcoveramount() == null){
																	 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
																 }
															 }
														 }*/
													 }
													 
												 }else{
													 /*if("IP".equalsIgnoreCase(coversDTO.getCovercode())){														
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
													 }else{*/
														 if(coversDTO.getAdditionalcoveramount()!=null){
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
														 }else{
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
														 }
														 
													 /*}*/
												 }
												
												//added for Transfer cover as unitized 
											if(null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
														 && ((QuoteConstants.DC_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlDeathCoverDetails().getDeathCoverType())) 
																 || (QuoteConstants.TPD_UNITISED.equalsIgnoreCase(eapplyInput.getAddnlTpdCoverDetails().getTpdTransferCoverType()))) 
														 && QuoteConstants.PARTNER_HOST.equalsIgnoreCase(applicationDTO.getPartnercode()))
												 {
													 
													 memberProductDetails.setProductAmount((null!=coversDTO.getCovercode() && "Death".equalsIgnoreCase(coversDTO.getCovercode()))?
															 CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+eapplyInput.getAddnlDeathCoverDetails().getDeathTransferUnits()+MetlifeInstitutionalConstants.UNITS:
																 CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+eapplyInput.getAddnlTpdCoverDetails().getTpdTransferUnits()+MetlifeInstitutionalConstants.UNITS);
													 

													 if("Death".equalsIgnoreCase(coversDTO.getCovercode()))
													 {
														 pdfObject.setDeathRoundMsg(((coversDTO.getAdditionalcoveramount()).compareTo(coversDTO.getExistingcoveramount().add(coversDTO.getTransfercoveramount()))) > 0 );
													 }
													 if("TPD".equalsIgnoreCase(coversDTO.getCovercode()))
													 {
														 pdfObject.setTpdRoundMsg(((coversDTO.getAdditionalcoveramount()).compareTo(coversDTO.getExistingcoveramount().add(coversDTO.getTransfercoveramount()))) > 0 );
													 }
													 
												 
												 }
											if (("CARE".equalsIgnoreCase(applicationDTO.getPartnercode()) || /*Added for vics -purna*/MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())) && null!=applicationDTO.getRequesttype()){
												//Refined this logic as part of sonarqube issue
												if(!(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())))
												 {
													iproundUpMsg(applicationDTO.getRequesttype(), coversDTO, memberProductDetails, pdfObject);
												 }
												else if (!"".equalsIgnoreCase(applicationDTO.getRequesttype()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "IP".equalsIgnoreCase(coversDTO.getCovercode())){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
												}
												
											} 
												 
											 } else if(memberDetails.isTransferCover()){
												 memberProductDetails.setProductAmount(coversDTO.getAdditionalcoveramount()+"\n ( Transfer of Cover )");													 
											 }else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
												 if(coversDTO.getAdditionalcoveramount()!=null){
													 if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){	//Added for Vicsuper by purna 													 
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ("+eapplyInput.getAddnlIpCoverDetails().getIpInputTextValue()+MetlifeInstitutionalConstants.UNITS);
														 if(QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
														 {
															 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n ("+(String.valueOf(((coversDTO.getAdditionalcoveramount()).intValue())/500))+MetlifeInstitutionalConstants.UNITS);
														 }
													 } else {													 
														 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
													 }
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
												 }
												 
												 
											 }else{
												 if(coversDTO.getAdditionalcoveramount()!=null){
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
												 }else{
													 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount("0"));
												 }
												 
											 }
											 /*memberProductDetails.setAalLevelStatus(coversDTO.getAALevelStatus());*/
											 memberProductDetails.setProductName(coversDTO.getCovertype());
											 if("IP".equalsIgnoreCase(coversDTO.getCovercode()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
													 && !("CARE".equalsIgnoreCase(eapplyInput.getPartnerCode())))
											 {
												 memberProductDetails.setBenefitPeriod(coversDTO.getTotalipbenefitperiod());
												 memberProductDetails.setWaitingPeriod(coversDTO.getTotalipwaitingperiod());
												 memberProductDetails.setExBenefitPeriod(coversDTO.getExistingipbenefitperiod());
												 memberProductDetails.setExWaitingPeriod(coversDTO.getExistingipwaitingperiod());
											 }
											 else
											 {
												 memberProductDetails.setBenefitPeriod(coversDTO.getAdditionalipbenefitperiod());
												 memberProductDetails.setWaitingPeriod(coversDTO.getAdditionalipwaitingperiod());
												 memberProductDetails.setExBenefitPeriod(coversDTO.getExistingipbenefitperiod());
												 memberProductDetails.setExWaitingPeriod(coversDTO.getExistingipwaitingperiod());
											 }
											 						 
											
											 /*need to revisit again*/
											 if(coversDTO.getTransfercoveramount()!=null){
												 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(coversDTO.getTransfercoveramount().toString()));
												/* if("IP".equalsIgnoreCase(coversDTO.getCovercode()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
														 && (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()))
														 && (MetlifeInstitutionalConstants.NO.equalsIgnoreCase(eapplyInput.getOccupationDetails().getFifteenHr())
																 || ((coversDTO.getTransfercoveramount()).intValue()==0 && (coversDTO.getAdditionalcoveramount()).intValue()>0)
																 || ((coversDTO.getAdditionalcoveramount()).intValue() == 0 && (coversDTO.getExistingcoveramount()).intValue() == 0)))
												 {
													 memberProductDetails.setTransferCoverAmnt("");
												 }*/
												 
													 if((MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())) 
															 && (("IP".equalsIgnoreCase(coversDTO.getCovercode())
															 && (MetlifeInstitutionalConstants.NO.equalsIgnoreCase(eapplyInput.getOccupationDetails().getFifteenHr())
																	 || ((coversDTO.getTransfercoveramount()).intValue()==0 && (coversDTO.getAdditionalcoveramount()).intValue()>0)
																	 || ((coversDTO.getAdditionalcoveramount()).intValue() == 0 && (coversDTO.getExistingcoveramount()).intValue() == 0)))
															 || ("Death".equalsIgnoreCase(coversDTO.getCovercode()) &&  (((coversDTO.getTransfercoveramount()).intValue()==0 && (coversDTO.getAdditionalcoveramount()).intValue()>0)
																	 || ((coversDTO.getAdditionalcoveramount()).intValue() == 0 && (coversDTO.getExistingcoveramount()).intValue() == 0)))
															 || ("TPD".equalsIgnoreCase(coversDTO.getCovercode()) &&  (((coversDTO.getTransfercoveramount()).intValue()==0 && (coversDTO.getAdditionalcoveramount()).intValue()>0)
																	 || ((coversDTO.getAdditionalcoveramount()).intValue() == 0 && (coversDTO.getExistingcoveramount()).intValue() == 0)))))
													 {
														 memberProductDetails.setTransferCoverAmnt("");
													 }
												 
											 }
/*											 if(null!=applicationDTO && null!=applicationDTO.getApplicant()){
												 for(int itr1=0;itr1<applicationDTO.getApplicant().size();itr1++){
													 Applicant applicantDTO1 = (Applicant)applicationDTO.getApplicant().get(itr1);
													 if(null!=applicantDTO){
														 	
															for(int covItr1=0;covItr1<applicantDTO.getCovers().size();covItr1++){
																dsd coversDTO = (Cover)applicantDTO.getCovers().get(covItr1);
														}
														 List questlist = applicantDTO1.getQuetionDtoVector();
														 if(null!=questlist){
															 for(int i=0;i<questlist.size();i++ ){
																 QuestionDto questionDto = (QuestionDto) questlist.get(i);																		 
																 if(null!=questionDto){
																	 String qval = questionDto.getQuestionTextID();
																	 String aval = questionDto.getAnswerText();
																	 if("DEATH".equalsIgnoreCase(coversDTO.getCovercode()) && null!=qval && qval.equalsIgnoreCase(QuestionIdConstant.Q16_DEATH_TRANSFER_CVR)){
																		 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(aval));
																	 }else if("TPD".equalsIgnoreCase(coversDTO.getCovercode()) && null!=qval && qval.equalsIgnoreCase(QuestionIdConstant.Q17_TPD_TRANSFER_CVR)){
																		 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(aval));
																	 }else if("IP".equalsIgnoreCase(coversDTO.getCovercode()) && null!=qval && qval.equalsIgnoreCase(QuestionIdConstant.Q18_IP_TRANSFER_CVR)){
																		 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount(aval));
																	 }
																 }										 
															 }
															
															 if(memberProductDetails.getTransferCoverAmnt()==null){
																 memberProductDetails.setTransferCoverAmnt(CommonPDFHelper.formatIntAmount("0"));
															 }
														 }
														 								 
													 }
												 }
											 }											
*/												
											 /*if("IP".equalsIgnoreCase(coversDTO.getCovercode())){*/
												 
											 if(new BigDecimal(Integer.toString(coversDTO.getFixedunit())).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+MetlifeInstitutionalConstants.UNITS);
												 }else{
													 if(null != coversDTO.getExistingcoveramount() && coversDTO.getExistingcoveramount().doubleValue() > 0 ){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
															 
													 }else{
														 /*if(coversDTO.getExistingcoveramount()!=null){*/
														 //Below code check is added when the IP tag is removed in the xml
														 /*if(coversDTO.getExistingcoveramount()==null){*/
															 coversDTO.setExistingcoveramount(BigDecimal.ZERO);
															 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
														 /*}*/
															// memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
														/* }*/
													 }
												 }
											 /*}else{
												 
												 if(new BigDecimal(""+coversDTO.getFixedunit()).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+MetlifeInstitutionalConstants.UNITS);
												 }else{
													 if(null != coversDTO.getExistingcoveramount() && coversDTO.getExistingcoveramount().doubleValue() > 0 ){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_N_FIXED);
															 
													 }else{
														 if(coversDTO.getExistingcoveramount()!=null){
														//Below code check is added when the death & TPD tag is removed in the xml
														 if(coversDTO.getExistingcoveramount()==null){
															 coversDTO.setExistingcoveramount(BigDecimal.ZERO);
															 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
														 }
															// memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));
														 }
													 }
												 }
												 
												 if(new BigDecimal(""+coversDTO.getFixedunit()).doubleValue()>0){
													 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString())+"\n ("+coversDTO.getFixedunit()+MetlifeInstitutionalConstants.UNITS);													
												 }else{													
													 if(coversDTO.getExistingcoveramount()!=null){
														 memberProductDetails.setExistingCover(CommonPDFHelper.formatIntAmount(coversDTO.getExistingcoveramount().toString()));		
													 } 
												 }
											 }*/
											 
										 
											 
											 memberProductDetails.setOccupationRating(coversDTO.getOccupationrating());
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision())) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision()) && coversDTO.getCoverexclusion()!=null && coversDTO.getCoverexclusion().trim().length()>0){
												 memberProductDetails.setExclusions(HTMLToString.convertHTMLToString(coversDTO.getCoverexclusion()));
											 }											 									 
											 if(("ACC".equalsIgnoreCase(pdfObject.getApplicationDecision())) && "ACC".equalsIgnoreCase(coversDTO.getCoverdecision()) && null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length()>0){
												 pdfObject.setExclusionInd(Boolean.TRUE);
											 }											 
											 if("DEATH".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_DEATH+coversDTO.getCoverreason());
												 pdfObject.setFulDeathAmount("0");
											 }
											 if("TPD".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 &&  null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_TPD+coversDTO.getCoverreason());
											 }		
											 
											 if("CORP".equalsIgnoreCase(applicationDTO.getPartnercode()) && "TPD".equalsIgnoreCase(coversDTO.getCovercode()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_TPD+coversDTO.getCoverreason());
												 pdfObject.setFulTPDAmount("0");
											 }
											 if("IP".equalsIgnoreCase(coversDTO.getCovertype()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_IP+coversDTO.getCoverreason());
											 }
											 
											 if("CORP".equalsIgnoreCase(applicationDTO.getPartnercode()) && "IP".equalsIgnoreCase(coversDTO.getCovercode()) && "DCL".equalsIgnoreCase(coversDTO.getCoverdecision())
													 && null!=coversDTO.getCoverreason()
													 && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())
													 && !"RUW".equalsIgnoreCase(pdfObject.getApplicationDecision())){
												 memberProductDetails.setPrductDecReason(MetlifeInstitutionalConstants.DCL_TEXT_IP+coversDTO.getCoverreason());
												 pdfObject.setFulIPAmount("0");
											 }
											 if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision()) && null!=coversDTO.getCoverreason()){
												 alldecreasons.append(coversDTO.getCoverreason());
												 if("CORP".equalsIgnoreCase(applicationDTO.getPartnercode())) {
													 pdfObject.setFulIPAmount("0"); 
													 pdfObject.setFulTPDAmount("0");
													 pdfObject.setFulDeathAmount("0");
												 }
											 }
											
										 }
										 if("INGD".equalsIgnoreCase(applicationDTO.getPartnercode()) && "DEATH".equalsIgnoreCase(coversDTO.getCovertype())){
											 
										 if("2".equalsIgnoreCase(eapplyInput.getExistingCovers().getCover().get(0).getType())){
											 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Tailored"));											 										 
									     }else{											 
												 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Automatic"));									
										 
										 }
										 if("DcTailored".equalsIgnoreCase(coversDTO.getCovercategory())){
											 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_TAILORED);
											 }else{
												 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_AUTOMATIC);
											 }
										 }
										 if("INGD".equalsIgnoreCase(applicationDTO.getPartnercode()) && "Total Permanent Disablement".equalsIgnoreCase(coversDTO.getCovertype())){
										 if("2".equalsIgnoreCase(eapplyInput.getExistingCovers().getCover().get(1).getType())){
											 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Tailored"));
											
										 }else{											 
											 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Automatic"));									
											 
									     }
										 if("TPDTailored".equalsIgnoreCase(coversDTO.getCovercategory())){
											 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_TAILORED);
											 }else{
												 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_AUTOMATIC);
											 }
										 }
										 if("INGD".equalsIgnoreCase(applicationDTO.getPartnercode()) && "Income Protection".equalsIgnoreCase(coversDTO.getCovertype())){
										 if("2".equalsIgnoreCase(eapplyInput.getExistingCovers().getCover().get(2).getType())){
											 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Tailored"));
											 if("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
												 //boolean seventyfiveflag=false;
												 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_75PERCENT_SALARY+MetlifeInstitutionalConstants.SLASH_N_FIXED);
												 /*if(seventyfiveflag==true){
													 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_75PERCENT_SALARY+MetlifeInstitutionalConstants.SLASH_N_FIXED);
												 }else{
													 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_85PERCENT_SALARY+MetlifeInstitutionalConstants.SLASH_N_FIXED);
												 }
												 */
											 }else{
												 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount()+MetlifeInstitutionalConstants.SLASH_N_FIXED);
											 }
											 
										 }
										 }/*else{
											 if("Income Protection".equalsIgnoreCase(coversDTO.getCovertype())){
												 memberProductDetails.setExistingCover(memberProductDetails.getExistingCover().replace("Fixed", "Automatic"));
												 memberProductDetails.setProductAmount(memberProductDetails.getProductAmount().replaceAll("Fixed", "Automatic"));
											 }
										 }*/
										 
										 if((null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
												 && ((coversDTO.getExistingcoveramount()).compareTo(BigDecimal.ZERO) <=0 ))
										 {
											 memberProductDetails = new MemberProductDetails();
										 
										 }
										 else
										 {
										 memberProductDtsList.add(memberProductDetails);
										 }
									 }
									
								}
							}							
						
						memberDetails.setMemberProductList(memberProductDtsList);
												
					}
				}
				
			}		 
			
			 /*End*/
		
			if(null!=applicationDTO.getCampaigncode()){
				pdfObject.setCampaignCode(applicationDTO.getCampaigncode());
			}			
			/*if(null != disclosureStatementList && disclosureStatementList.size()>0){*/
			if(null != disclosureStatementList && !(disclosureStatementList.isEmpty())){
				pdfObject.setDisclosureStatementList(disclosureStatementList);	

			}
			/*if(null != disclosureStatementListGc && disclosureStatementListGc.size()>0){*/
			if(null != disclosureStatementListGc && !(disclosureStatementListGc.isEmpty())){
				pdfObject.setDisclosureStatementListGc(disclosureStatementListGc);	
			}
			
			//pdfObject.setProductName("CARE");
			
			


			}	
			/*if(null != disclosureStatementListGc && disclosureStatementListGc.size()>0){*/
		if(null != disclosureStatementListGc && !(disclosureStatementListGc.isEmpty())){
				pdfObject.setDisclosureStatementListGc(disclosureStatementListGc);	
			}

		   if(!CorporateHelper.isNullOrEmpty(eapplyInput.getCorpFundCode()) &&  "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
					    	  pdfObject.setProductName(eapplyInput.getCorpFundCode());
					      }
		   else {
			   pdfObject.setProductName(applicationDTO.getPartnercode());
		   }

			/*prepare disclosureList*/
			if(pdfObject.isQuickQuoteRender()) {
				pdfObject.setDisclosureQuestionList(PDFbusinessObjectMapper.prepareQuestionListPremCalc(eapplyInput, property));
			}else {
				pdfObject.setDisclosureQuestionList(PDFbusinessObjectMapper.prepareQuestionList(eapplyInput, property));
			}
			if(/*pdfObject.getProductName() != null && pdfObject.getProductName() !="" && (pdfObject.getProductName().equalsIgnoreCase("HOST") || pdfObject.getProductName().equalsIgnoreCase("FIRS") || pdfObject.getProductName().equalsIgnoreCase("SFPS")) &&*/
					null!=applicationDTO.getRequesttype() && !"".equalsIgnoreCase(applicationDTO.getRequesttype()) && "TCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
				pdfObject.setTransferPrevQuestionList(PDFbusinessObjectMapper.prepareTransferPrevQuestionList(eapplyInput, property));
			}
			pdfObject.setMemberDetails(memberDetails);
			pdfObject.setDeclReasons(alldecreasons.toString());
		
		
		property.clear();
		if(null!=url){
			url.close();
		}	
		log.info("Pdf object mapping finish");
		return pdfObject;
	}
	
	
	public static List<DisclosureQuestionAns> prepareQuestionListPremCalc(EapplyInput eapplyInput, Properties property){
		log.info("Prepare question list for Premium calculator start");
		List<DisclosureQuestionAns> disclosureQuestionAnsList = new ArrayList<>();
		DisclosureQuestionAns disclosureQuestionAns = null;
		
		//DOB
		if((eapplyInput.getDob()) != null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_dob"));		
			disclosureQuestionAns.setAnswerText(eapplyInput.getDob());
			log.info("dob----- {}" ,disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);		
		}
		
		//Gender
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getGender()!=null && eapplyInput.getOccupationDetails().getGender()!=""){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_gender"));
			log.info("gender---- {}" ,disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getGender());
			log.info("gender ans----- {}" ,disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
		
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getFifteenHr()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_fifteen_hr_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getFifteenHr());
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}
		
		
	    if(eapplyInput.getOccupationDetails() != null && (eapplyInput.getOccupationDetails().getOccRating1a() != null)){
	   			disclosureQuestionAns = new DisclosureQuestionAns();
	   			disclosureQuestionAns.setQuestiontext(property.getProperty("label_occrating1a"));	  			
	   	       	disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccRating1a());   	        		
	   			disclosureQuestionAnsList.add(disclosureQuestionAns);			
	   	}
     	
     	/**Vicsuper Code changes ends - Made by Purna**/
     	 
		if(eapplyInput.getOccupationDetails() != null && (eapplyInput.getOccupationDetails().getIndustryName()!= null 
				|| eapplyInput.getOccupationDetails().getOccRating1b() != null)){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_occrating1b"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccRating1b());
				disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		
		if(eapplyInput.getOccupationDetails() != null && (eapplyInput.getOccupationDetails().getTertiaryQue()!= null
				|| eapplyInput.getOccupationDetails().getOccRating2() != null)){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_ter_qual"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccRating2());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
		
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getSalary()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_annualSalary"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getSalary());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		log.info("Prepare question list for Premium calculator  End");
		return disclosureQuestionAnsList;
	}
	
	
	public static List<DisclosureQuestionAns> prepareQuestionList(EapplyInput eapplyInput, Properties property){
		log.info("Prepare question list start");
		String upLoadFileName = "";
		List<DisclosureQuestionAns> disclosureQuestionAnsList = new ArrayList<>();
		DisclosureQuestionAns disclosureQuestionAns = null;
		
		if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getFirstName()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_first_name"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getPersonalDetails().getFirstName());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
	
		
		if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getLastName()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_last_name"));
			log.info("last name---- {}",disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getPersonalDetails().getLastName());
			log.info("last name ans----- {}",disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}	
		/*if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getGender()!=null && eapplyInput.getPersonalDetails().getGender()!=""){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_gender"));
			log.info("gender----" +disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getPersonalDetails().getGender());
			log.info("gender ans-----{}" ,disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}else */
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getGender()!=null && eapplyInput.getOccupationDetails().getGender()!=""){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_gender"));
			log.info("gender---- {}" ,disclosureQuestionAns.getQuestiontext());
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getGender());
			log.info("gender ans----- {}" ,disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
		/* it is checking with input XML values, will work on this*/
		/*if(null!=eapplyInput.getContactDetails()){
			if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContactTime())){				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));			
				disclosureQuestionAns.setAnswerText("9am - 12pm");				
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}else{				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));			
				disclosureQuestionAns.setAnswerText("12pm - 6pm");				
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			log.info("contact time>> {}",eapplyInput.getContactDetails().getPrefContactTime());
			log.info("contact Type>> {}",eapplyInput.getContactDetails().getPrefContact());
			if("1".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){			
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Mobile Phone");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
		
			}else if("2".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Home Phone");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
			}else if("3".equalsIgnoreCase(eapplyInput.getContactDetails().getPrefContact())){				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Work Phone");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}else{				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));
				disclosureQuestionAns.setAnswerText("Email");
				disclosureQuestionAnsList.add(disclosureQuestionAns);
				
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getContactDetails().getEmailAddress());
				disclosureQuestionAnsList.add(disclosureQuestionAns);
			}
			
		}*/
		
		
		if(eapplyInput.getEmail()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_email_add"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getEmail());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		
		if((eapplyInput.getDob()) != null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_dob"));		
			disclosureQuestionAns.setAnswerText(eapplyInput.getDob());
			log.info("dob----- {}" ,disclosureQuestionAns.getAnswerText());
			disclosureQuestionAnsList.add(disclosureQuestionAns);		
		}
		if(eapplyInput.getContactPrefTime()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_time"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getContactPrefTime());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		if(eapplyInput.getContactType()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_contact_type"));	
			///Added for Corporate need to refractor in the front end and remove
			if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				if("1".equalsIgnoreCase(eapplyInput.getContactType())) {
					eapplyInput.setContactType(MetlifeInstitutionalConstants.MOBILE_PH);
				}
				else if("2".equalsIgnoreCase(eapplyInput.getContactType())) {
					eapplyInput.setContactType("Home");
				}
				else if("3".equalsIgnoreCase(eapplyInput.getContactType())){
					eapplyInput.setContactType("Work");
				}
			}
			if(MetlifeInstitutionalConstants.MOBILE_PH.equalsIgnoreCase(eapplyInput.getContactType())){		
				disclosureQuestionAns.setAnswerText("Mobile Phone");
			}else if("Home".equalsIgnoreCase(eapplyInput.getContactType()) || "Home phone".equalsIgnoreCase(eapplyInput.getContactType())){		
				disclosureQuestionAns.setAnswerText("Home Phone");
			}else if("Work".equalsIgnoreCase(eapplyInput.getContactType()) || "Work phone".equalsIgnoreCase(eapplyInput.getContactType())){		
				disclosureQuestionAns.setAnswerText("Work Phone");
			}
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		
		if(eapplyInput.getContactPhone()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_preferred_no"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getContactPhone());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		// For HOST, Change cover
			if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getOwnBussinessQues()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_que"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOwnBussinessQues());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			
			if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getOwnBussinessYesQues()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_yes_que"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOwnBussinessYesQues());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
			
			if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getOwnBussinessNoQues()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_own_business_no_que"));
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOwnBussinessNoQues());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			}
		// For HOST, Change cover
		
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getFifteenHr()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_fifteen_hr_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getFifteenHr());
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getCitizenQue()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_citizen_aus"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getCitizenQue());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}	
		if(eapplyInput.getPartnerCode() != null && eapplyInput.getPartnerCode() !="" && (eapplyInput.getPartnerCode().equalsIgnoreCase("SFPS") && "CCOVER".equalsIgnoreCase(eapplyInput.getManageType()))
				&& (eapplyInput.getOccupationDetails()!=null && eapplyInput.getOccupationDetails().getSmokerQuestion()!=null)) {
			//if(eapplyInput.getPersonalDetails()!=null && eapplyInput.getPersonalDetails().getSmoker()!=null){
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_smoker_que"));
				/*if("1".equalsIgnoreCase(eapplyInput.getPersonalDetails().getSmoker())){
					disclosureQuestionAns.setAnswerText("Yes"); 
				}else{
					disclosureQuestionAns.setAnswerText("No"); 
				}*/
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getSmokerQuestion());
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
			
		}
		if(eapplyInput.getPartnerCode() != null && eapplyInput.getPartnerCode() !="" && ("AEIS".equalsIgnoreCase(eapplyInput.getPartnerCode()))
				&& eapplyInput.getSmoker()!=null) {
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_smoker_que"));
				if("Yes".equalsIgnoreCase(eapplyInput.getSmoker())){
					disclosureQuestionAns.setAnswerText("Yes"); 
				}else{
					disclosureQuestionAns.setAnswerText("No"); 
				}
				disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getSalary()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_annualSalary"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getSalary());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		
		/**Vicsuper Code changes starts - Made by Purna**/
		if ((MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				||   MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				|| MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplyInput.getManageType())
				|| MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplyInput.getManageType()))
     			 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
	     		if(eapplyInput.getOccupationDetails() != null && (eapplyInput.getOccupationDetails().getOccRating1a() != null/**vicsuper changes - Purna**/)){
	   			disclosureQuestionAns = new DisclosureQuestionAns();
	   			disclosureQuestionAns.setQuestiontext(property.getProperty("label_occrating1a"));	  			
	   	       	disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccRating1a());   	        		
	   			disclosureQuestionAnsList.add(disclosureQuestionAns);			
	   		}
     	 }
     	/**Vicsuper Code changes ends - Made by Purna**/
     	 
		if(eapplyInput.getOccupationDetails() != null && (eapplyInput.getOccupationDetails().getIndustryName()!= null 
				|| eapplyInput.getOccupationDetails().getOccRating1b() != null/**vicsuper changes - Purna**/)){
			disclosureQuestionAns = new DisclosureQuestionAns();
			if(!MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())){
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_industrycnfrm"));
			}else{
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_occrating1b"));
			}
			/**Vicsuper Code changes starts - Made by Purna**/
			if ((MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) ||  MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType()) 
					||   MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())
					|| MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplyInput.getManageType())
					|| MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplyInput.getManageType()))
	       			 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
	       		disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccRating1b());	
	       	 } else {
	       		disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getIndustryName());		 
	       	 }				
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if("CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())&& eapplyInput.getIndustryName()!=null) {
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_industrycnfrm"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getIndustryName());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		//Added for VicSuper Change Cover module
		if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())
						&& MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType())
						&& !eapplyInput.isConvertCheck()
						&& eapplyInput.getIndustryName()!=null) 
			{
				disclosureQuestionAns = new DisclosureQuestionAns();
				disclosureQuestionAns.setQuestiontext(property.getProperty("label_industrycnfrm"));			
				disclosureQuestionAns.setAnswerText(eapplyInput.getIndustryName());			
				disclosureQuestionAnsList.add(disclosureQuestionAns);			
			}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getOccupation()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_occupation_que"));
			if(eapplyInput.getOccupationDetails().getOccupation().equalsIgnoreCase("Other")){
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOtherOccupation());
			}else{
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccupation());
			}
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccupation());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		
		if(eapplyInput.getClientRefNumber() != null && "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_employeenumber"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getClientRefNumber());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		
		if(eapplyInput.getAddress() != null && "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_address"));		
			StringBuilder addressBuilder = new StringBuilder();			
                if (null != eapplyInput.getAddress().getLine1()
                		&& eapplyInput.getAddress().getLine1().trim().length()>0){
                	addressBuilder.append(eapplyInput.getAddress().getLine1() + " ,");
                }	                
                if (null != eapplyInput.getAddress().getLine2()
                		&& eapplyInput.getAddress().getLine2().trim().length()>0){
                	addressBuilder.append( eapplyInput.getAddress().getLine2() + ", ");			                	
                }
                if (null !=  eapplyInput.getAddress().getSuburb()
                		&&  eapplyInput.getAddress().getSuburb().trim().length()>0){
                	addressBuilder.append( eapplyInput.getAddress().getSuburb() + ", ");			                	
                }
                if (null != eapplyInput.getAddress().getState()
                		&& eapplyInput.getAddress().getState().trim().length()>0){
                	addressBuilder.append( eapplyInput.getAddress().getState() + ", ");			                	
                }
                if(null != eapplyInput.getAddress().getState() && "OTH".equalsIgnoreCase(eapplyInput.getAddress().getState())) {
                	addressBuilder.append( "Overseas" + ", ");			                	
                }
                else {
                	addressBuilder.append( "Australia" + ", ");		
                }              	
                	
                if (null != eapplyInput.getAddress().getPostCode()){
                	addressBuilder.append( eapplyInput.getAddress().getPostCode());
                }
			disclosureQuestionAns.setAnswerText(addressBuilder.toString());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		/*For WR-17442 Added other occupation inside PDF - @auther:Prasanna Durga*/
		/*if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getOccupation()!= null 
				&& eapplyInput.getOccupationDetails().getOccupation()!= "" 
					&& eapplyInput.getOccupationDetails().getOccupation().equalsIgnoreCase("Other")){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext("If Other, please enter your occupation");
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOtherOccupation());	
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}*/
		
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getWithinOfficeQue()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_askmanual_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getWithinOfficeQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getOccupationDetails() != null && (eapplyInput.getOccupationDetails().getTertiaryQue()!= null
				|| eapplyInput.getOccupationDetails().getOccRating2() != null/**vicsuper changes - Purna**/)){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_ter_qual"));
			//Made changes for vicsuper - Purna 
			if ((MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(eapplyInput.getManageType()) ||  MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(eapplyInput.getManageType())
					||   MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(eapplyInput.getManageType())
					|| MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(eapplyInput.getManageType())
					|| MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(eapplyInput.getManageType()))
	       			 && MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())) {
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getOccRating2());	
			} else{
				disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getTertiaryQue());
			}			 
			disclosureQuestionAnsList.add(disclosureQuestionAns);
			
		}
		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getManagementRoleQue()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_work_duty"));	
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getManagementRoleQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}

		if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getHazardousQue()!= null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_sec_hazardous_que"));	
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getHazardousQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getPreviousTpdBenefit() != null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_previous_tpd_benefit_que"));	
			disclosureQuestionAns.setAnswerText(eapplyInput.getPreviousTpdBenefit()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(eapplyInput.getTerminalIllClaimQue() != null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_previous_terminal_ill_que"));	
			disclosureQuestionAns.setAnswerText(eapplyInput.getTerminalIllClaimQue()); 
			disclosureQuestionAnsList.add(disclosureQuestionAns);			
		}
		if(null!=eapplyInput && null!=eapplyInput.getEventDesc() && !"".equalsIgnoreCase(eapplyInput.getEventDesc())){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_name"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getEventDesc());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
    		
    	}
		if(null!=eapplyInput && (eapplyInput.isFulCheck()||!eapplyInput.isFulCheck()) && "CORP".equalsIgnoreCase(eapplyInput.getPartnerCode())){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("labe_FUL"));	
			if(eapplyInput.isFulCheck()) {
			disclosureQuestionAns.setAnswerText(property.getProperty("label_FULyes"));	
			}
			if(!eapplyInput.isFulCheck()) {
			disclosureQuestionAns.setAnswerText(property.getProperty("label_FULno"));	
			}
			disclosureQuestionAnsList.add(disclosureQuestionAns);
    		
    	}
    	if(null!=eapplyInput && null!=eapplyInput.getEventDate() && !"".equalsIgnoreCase(eapplyInput.getEventDate())){
    		disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_date"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getEventDate());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
    	}
    	if(null!=eapplyInput && null!=eapplyInput.getEventAlreadyApplied() && !"".equalsIgnoreCase(eapplyInput.getEventAlreadyApplied()) && null!=property.getProperty("label_lifeevent_alreadyApplied")){
    		disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_lifeevent_alreadyApplied"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getEventAlreadyApplied());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
    	}
    	if(!(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())))
		{
    	if(eapplyInput.getDocumentName()!=null && eapplyInput.getLifeEventDocuments()!=null){
    		disclosureQuestionAns = new DisclosureQuestionAns();
    		disclosureQuestionAns.setQuestiontext(property.getProperty("label_document_evidence_life"));
    		disclosureQuestionAns.setAnswerText(eapplyInput.getDocumentName());
    		disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
		//Added for document sending address inclusion in PDF start
    	if(null!=eapplyInput && null!=eapplyInput.getEventDesc() && !"".equalsIgnoreCase(eapplyInput.getEventDesc()))
    	{
		if(eapplyInput.getDocumentAck2()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_document_no_evidence"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getDocumentAck2());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
		if(eapplyInput.getDocumentAddress()!=null)
		{
			disclosureQuestionAns=new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_document_address"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getDocumentAddress());
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
    	}
		}
    	if(eapplyInput.getLifeEventDocuments()!=null && eapplyInput.getDocumentName()!=null && "yes".equalsIgnoreCase(eapplyInput.getDocumentName())){
    		disclosureQuestionAns = new DisclosureQuestionAns();
    		disclosureQuestionAns.setQuestiontext(property.getProperty("label_summary_documents"));
			for(int i=0;i<eapplyInput.getLifeEventDocuments().size();i++){
				String filename= eapplyInput.getLifeEventDocuments().get(i).getName();						
				/*System.out.println("filename >>> "+filename);*/
				log.info("filename >>> {} ",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			disclosureQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
    	if(MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode()) && eapplyInput.getLifeEventDocuments()!=null && !eapplyInput.getLifeEventDocuments().isEmpty()){
    		disclosureQuestionAns = new DisclosureQuestionAns();
    		disclosureQuestionAns.setQuestiontext(property.getProperty("label_host_summary_documents"));
			for(int i=0;i<eapplyInput.getLifeEventDocuments().size();i++){
				String filename= eapplyInput.getLifeEventDocuments().get(i).getName();						
				/*System.out.println("filename >>> "+filename);*/
				log.info("filename >>> {} ",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			disclosureQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		}
    	if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())
    			&& eapplyInput.getLifeEventDocuments()!=null)
    	{

    		disclosureQuestionAns = new DisclosureQuestionAns();
    		disclosureQuestionAns.setQuestiontext(property.getProperty("label_summary_documents"));
			for(int i=0;i<eapplyInput.getLifeEventDocuments().size();i++){
				String filename= eapplyInput.getLifeEventDocuments().get(i).getName();						
				/*System.out.println("filename >>> "+filename);*/
				log.info("filename >>> {} ",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			disclosureQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			disclosureQuestionAnsList.add(disclosureQuestionAns);
		
    	}
    	
    	if(null!=eapplyInput && null!=eapplyInput.getRsnFrAdnlCvr() && !"".equalsIgnoreCase(eapplyInput.getRsnFrAdnlCvr())){
    		disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_reason_addnl_cover"));			
			disclosureQuestionAns.setAnswerText(eapplyInput.getRsnFrAdnlCvr());			
			disclosureQuestionAnsList.add(disclosureQuestionAns);
    	}

    	/*if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getSmokerQuestion()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_smoker_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getSmokerQuestion());
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}*/
    	
    	if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getHoursPerWeekQuestion()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_thirty_five_hrs_week_que"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getHoursPerWeekQuestion());
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}
    	
    	if(eapplyInput.getOccupationDetails() != null && eapplyInput.getOccupationDetails().getPermanentlyEmployedQuestion()!=null){
			disclosureQuestionAns = new DisclosureQuestionAns();
			disclosureQuestionAns.setQuestiontext(property.getProperty("label_permanent_empl"));
			disclosureQuestionAns.setAnswerText(eapplyInput.getOccupationDetails().getPermanentlyEmployedQuestion());
			disclosureQuestionAnsList.add(disclosureQuestionAns);				
		}
    	
		log.info("Prepare question list finish");
		return disclosureQuestionAnsList;
	}

	public static List<TransferPreviousQuestionAns> prepareTransferPrevQuestionList(EapplyInput eapplyInput, Properties property){
		log.info("Prepare Transfer Previous question list start");
		String upLoadFileName = "";
		List<TransferPreviousQuestionAns> transferPreviousCoverQuestionAnsList = new ArrayList<>();
		TransferPreviousQuestionAns transferPrevQuestionAns = null;
		
		if(eapplyInput.getPreviousFundName()!=null){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_nam_previ_insurer"));
			transferPrevQuestionAns.setAnswerText(eapplyInput.getPreviousFundName());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		if(eapplyInput.getMembershipNumber()!=null){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_fundmem_inspoli_no"));
			transferPrevQuestionAns.setAnswerText(eapplyInput.getMembershipNumber());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		if(eapplyInput.getSpinNumber()!=null){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_spin_no"));
			transferPrevQuestionAns.setAnswerText(eapplyInput.getSpinNumber());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		if(null!=eapplyInput.getPartnerCode() && !"".equalsIgnoreCase(eapplyInput.getPartnerCode())&& !"CARE".equalsIgnoreCase(eapplyInput.getPartnerCode())
				&& eapplyInput.getFreqCostType()!=null){
				transferPrevQuestionAns = new TransferPreviousQuestionAns();
				transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_prem_freq"));
				transferPrevQuestionAns.setAnswerText(eapplyInput.getFreqCostType());
				transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		if(!(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())))
		{
		if(eapplyInput.getDocumentName()!=null){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_document_evidence"));
			transferPrevQuestionAns.setAnswerText(eapplyInput.getDocumentName());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		//Added for document sending address inclusion in PDF start
		if(eapplyInput.getDocumentAck2()!=null){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_document_no_evidence"));
			transferPrevQuestionAns.setAnswerText(eapplyInput.getDocumentAck2());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		if(eapplyInput.getDocumentAddress()!=null)
		{
			transferPrevQuestionAns=new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_document_address"));
			transferPrevQuestionAns.setAnswerText(eapplyInput.getDocumentAddress());
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		}
				//change end
		if(eapplyInput.getTransferDocuments()!=null && eapplyInput.getDocumentName()!=null && "yes".equalsIgnoreCase(eapplyInput.getDocumentName())){
			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_documents"));
			for(int i=0;i<eapplyInput.getTransferDocuments().size();i++){
				String filename= eapplyInput.getTransferDocuments().get(i).getName();						
				/*System.out.println("filename >>> "+filename);*/
				log.info("filename >>> {}",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			transferPrevQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		}
		
		if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(eapplyInput.getPartnerCode())
				&& eapplyInput.getTransferDocuments()!=null)
		{

			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_summary_documents"));
			for(int i=0;i<eapplyInput.getTransferDocuments().size();i++){
				String filename= eapplyInput.getTransferDocuments().get(i).getName();						
				/*System.out.println("filename >>> "+filename);*/
				log.info("filename >>> {}",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			transferPrevQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		
		}
		
		if(MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(eapplyInput.getPartnerCode())
				&& eapplyInput.getTransferDocuments()!=null && !eapplyInput.getTransferDocuments().isEmpty())
		{

			transferPrevQuestionAns = new TransferPreviousQuestionAns();
			transferPrevQuestionAns.setQuestiontext(property.getProperty("label_host_summary_documents"));
			for(int i=0;i<eapplyInput.getTransferDocuments().size();i++){
				String filename= eapplyInput.getTransferDocuments().get(i).getName();						
				/*System.out.println("filename >>> "+filename);*/
				log.info("filename >>> {}",filename);
				upLoadFileName = upLoadFileName.concat(filename+"<br>");
			}
			transferPrevQuestionAns.setAnswerText(HTMLToString.convertHTMLToString(upLoadFileName));
			transferPreviousCoverQuestionAnsList.add(transferPrevQuestionAns);
		
		}
		
		log.info("Prepare Transfer Previous question list finish");
		return transferPreviousCoverQuestionAnsList;
	}
	
	private String prepareTotalCoverLoadingAmt(String totalCoverLoadingAmt, String loadingAmount) {
		log.info("Prepare total cover loading amount start");
		Double double1 = null;
		if (isObjectPresent(toDouble(loadingAmount))) {
			if (isObjectPresent(toDouble(totalCoverLoadingAmt))) {
				double1 = toDouble(totalCoverLoadingAmt) + toDouble(loadingAmount);
				return double1.toString();
			} else {
				return loadingAmount;
			}
		}
		log.info("Prepare total cover loading amount finish");
		return null;
	}
	
	public static Double toDouble(String string) {
		
		if(isValuePresent(string)) {
			return Double.valueOf(string);
		}
		return null;
	}
	
	public static boolean isValuePresent(String string) {
		
		/*if(isObjectPresent(string) && string.trim().length() > 0) {
			return true;
		}
		return false;*/
		return (isObjectPresent(string) && string.trim().length() > 0);
	}
	
	public static boolean isObjectPresent(Object object){
		/*
		if(null != object) {
			return true;
		}
		return false;*/
		return (null != object);
	}
	
	private static void setProductAmnt(Cover coversDTO,MemberProductDetails memberProductDetails){
		if (null!=coversDTO.getAdditionalcoveramount() && coversDTO.getAdditionalunit() >0)
		 {
			//Do not merge the if clause
			if(coversDTO.getAdditionalunit() < (coversDTO.getAdditionalcoveramount()).intValue())
			{
			 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+"\n("+ coversDTO.getAdditionalunit() +" Units)");
			}
			else
			{
			memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString()));
			}
		 }
	}
	
	private static void iproundUpMsg(String requestType,Cover coversDTO,MemberProductDetails memberProductDetails,PDFObject pdfObject){
		if (!"".equalsIgnoreCase(requestType) && QuoteConstants.MTYPE_TCOVER.equalsIgnoreCase(requestType) && "IP".equalsIgnoreCase(coversDTO.getCovercode())){
			 memberProductDetails.setProductAmount(CommonPDFHelper.formatIntAmount(coversDTO.getAdditionalcoveramount().toString())+MetlifeInstitutionalConstants.SLASH_BRACKET+coversDTO.getAdditionalunit()+MetlifeInstitutionalConstants.UNITS);
			 pdfObject.setIpRoundMsg(((coversDTO.getAdditionalcoveramount()).compareTo(coversDTO.getTransfercoveramount())) > 0 );													
		}
	}

}
