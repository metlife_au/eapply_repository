package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class Mortgage1300QuoteObjects extends PageObjectLibrary {
	
	public static final By rdpolicyType = getObjectIdentifaction("1300.yourquote.policytype.joint.radio.css");
	public static final By txtfirstname = getObjectIdentifaction("1300.yourquote.firstname.text.id");
	public static final By txtfirstlname = getObjectIdentifaction("1300.yourquote.firstname.left.text.id");
	public static final By txtfirstrname = getObjectIdentifaction("1300.yourquote.firstname.right.text.id");
	
	public static final By txtdob = getObjectIdentifaction("1300.yourquote.dob.text.id");
	public static final By txtdobl = getObjectIdentifaction("1300.yourquote.dob.left.text.id");
	public static final By txtdobr = getObjectIdentifaction("1300.yourquote.dob.right.text.id");
	
	public static final By rdGender = getObjectIdentifaction("1300.yourquote.gender.male.radio.css");
	public static final By rdGenderl = getObjectIdentifaction("1300.yourquote.gender.male.left.radio.css");
	public static final By rdGenderr = getObjectIdentifaction("1300.yourquote.gender.male.right.radio.css");
	
	public static final By rdnoTobacco = getObjectIdentifaction("1300.yourquote.12mnthstobacco.no.radio.css");
	public static final By rdnoTobaccol = getObjectIdentifaction("1300.yourquote.12mnthstobacco.no.left.radio.css");
	public static final By rdnoTobaccor = getObjectIdentifaction("1300.yourquote.12mnthstobacco.no.right.radio.css");
	
	public static final By rdyesloan = getObjectIdentifaction("1300.yourquote.12mnthsloan.yes.radio.css");
	public static final By rdyesloanl = getObjectIdentifaction("1300.yourquote.12mnthsloan.yes.left.radio.css");
	public static final By rdyesloanr = getObjectIdentifaction("1300.yourquote.12mnthsloan.yes.right.radio.css");
	
	public static final By rdyesCitizen = getObjectIdentifaction("1300.yourquote.citizen.yes.radio.css");
	public static final By rdyesCitizenl = getObjectIdentifaction("1300.yourquote.citizen.yes.left.radio.css");
	public static final By rdyesCitizenr = getObjectIdentifaction("1300.yourquote.citizen.yes.right.radio.css");
	
	public static final By drpLoan = getObjectIdentifaction("1300.yourquote.loan.dropdown.css");
	public static final By drpLoanjoint = getObjectIdentifaction("1300.yourquote.loan.joint.dropdown.css");
	
	public static final By txtloanAmount = getObjectIdentifaction("1300.yourquote.loanamount.text.id");
	public static final By txtloanAmountjoint = getObjectIdentifaction("1300.yourquote.loanamount.joint.text.id");
	
	public static final By txtrepaymentAmount = getObjectIdentifaction("1300.yourquote.repaymentamount.text.id");
	public static final By txtrepaymentAmountjoint = getObjectIdentifaction("1300.yourquote.repaymentamount.joint.text.id");
	
	public static final By chkloanCover = getObjectIdentifaction("1300.yourquote.loancover.checkbox.css");
	public static final By chkloanCoverl = getObjectIdentifaction("1300.yourquote.loancover.left.checkbox.css");
	public static final By chkloanCoverr = getObjectIdentifaction("1300.yourquote.loancover.right.checkbox.css");
	
	public static final By drploanCover = getObjectIdentifaction("1300.yourquote.loancover.dropdown.css");
	public static final By drploanCoverl = getObjectIdentifaction("1300.yourquote.loancover.left.dropdown.css");
	public static final By drploanCoverr = getObjectIdentifaction("1300.yourquote.loancover.right.dropdown.css");
	
	public static final By chkRepayment = getObjectIdentifaction("1300.yourquote.repaymentcover.checkbox.id");
	public static final By drpRepayment = getObjectIdentifaction("1300.yourquote.repaymentcover.dropdown.css");
	
	public static final By rdMonthly = getObjectIdentifaction("1300.yourquote.monthly.radio.css");
	
	public static final By btnCalculateQuote = getObjectIdentifaction("1300.yourquote.calculatequote.btn.css");
	public static final By btnApply = getObjectIdentifaction("1300.yourquote.apply.btn.css");
	
}