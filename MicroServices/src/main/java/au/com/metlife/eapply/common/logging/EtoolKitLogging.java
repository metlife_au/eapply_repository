/**
 * 
 */
package au.com.metlife.eapply.common.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import au.com.metlife.eapply.EtoolKitException;

/**
 * @author 599063
 *
 */

@Aspect
@Component
public class EtoolKitLogging {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EtoolKitLogging.class);
	
	@Before("execution(* au.com.metlife.eapply.corporate.serviceImpl.*.*(..))")
	public void beforeServiceExecution(JoinPoint joinPoint){
		if(LOGGER.isDebugEnabled())
		{
		LOGGER.debug(String.format("Entering the service method: %s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
		}
	}
	
	@After("execution(* au.com.metlife.eapply.corporate.serviceImpl.*.*(..))")
	public void afterServiceExecution(JoinPoint joinPoint){
		if(LOGGER.isDebugEnabled())
		{
		LOGGER.debug(String.format("Exiting the service method: %s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
		}
	}
	
	@AfterThrowing(pointcut="execution(* au.com.metlife.eapply.corporate.serviceImpl.*.*(..))", throwing="error")
	public void afterServiceException(JoinPoint joinPoint, Throwable error){
		if(LOGGER.isDebugEnabled())
		{
		LOGGER.debug(String.format("Throwing exception from the service method: %s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
		LOGGER.debug(String.format("Exception details: %s:%s", error , processExceptionMessage(error)));
		}
	}
	
	@Before("execution(* au.com.metlife.eapply.helper.*.*(..))")
	public void beforeHelperExecution(JoinPoint joinPoint){
		if(LOGGER.isDebugEnabled())
		{
		LOGGER.debug(String.format("Entering the helper method: %s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
		}
	}
	
	@After("execution(* au.com.metlife.eapply.helper.*.*(..))")
	public void afterHelperExecution(JoinPoint joinPoint){
		if(LOGGER.isDebugEnabled())
		{
		LOGGER.debug(String.format("Exiting the helper method: %s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
		}
	}
	
	@AfterThrowing(pointcut="execution(* au.com.metlife.eapply.helper.*.*(..))", throwing="error")
	public void afterHelperException(JoinPoint joinPoint, Throwable error){
		if(LOGGER.isDebugEnabled())
		{
		LOGGER.debug(String.format("Throwing exception from the helper method: %s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
		LOGGER.debug(String.format("Exception details: %s:%s", error , processExceptionMessage(error)));
		}
	}
	
	private String processExceptionMessage(Throwable error){
		
		if(error instanceof EtoolKitException){
			return ((EtoolKitException)error).getErrorMsg();
		}
		else {
			return error.getMessage();
		}
		
		
	}

}
