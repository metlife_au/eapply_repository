package au.com.metlife.test;

import java.util.List;

public class ClaimTestObj implements java.io.Serializable{

		private String claimId;
		private String surName;
		private String dob;
		private String status;
		private String statusDate;
		
		private List<ClaimTestActivity> claimActivities;
		
				
		
		public List<ClaimTestActivity> getClaimActivities() {
			return claimActivities;
		}
		public void setClaimActivities(List<ClaimTestActivity> claimActivities) {
			this.claimActivities = claimActivities;
		}
		public String getClaimId() {
			return claimId;
		}
		public void setClaimId(String claimId) {
			this.claimId = claimId;
		}
		public String getSurName() {
			return surName;
		}
		public void setSurName(String surName) {
			this.surName = surName;
		}
		public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			this.dob = dob;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getStatusDate() {
			return statusDate;
		}
		public void setStatusDate(String statusDate) {
			this.statusDate = statusDate;
		}
		
}
