package au.com.metlife.eapply.underwriting.utils;

import java.io.Serializable;
import java.util.HashMap;

import org.drools.KnowledgeBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.underwriting.constants.ApplicationConstants;

@Component
@Scope("singleton")
public class AuraRulesCacheProvider implements Serializable{
	private static final Logger log = LoggerFactory.getLogger(AuraRulesCacheProvider.class);
	static final String CLASS_NAME = "AuraRulesCacheProvider";
	
	private java.util.HashMap ruleMap;
	SystemProperty sys;
	private String filePath;
    
    public String getFilePath() {
           return filePath;
    }

    public void setFilePath(String filePath) {
           this.filePath = filePath;
    }


	public java.util.HashMap getRuleMap() {
		return ruleMap;
	}

	public AuraRulesCacheProvider(){
		log.info("Aura rule cache provider start");
		try {
			sys=SystemProperty.getInstance();
			this.filePath = sys.getProperty("ruleFilePath");
	        log.info("ruleFilePath>> {}",filePath);
			loadProperties();
		} catch (Exception e) {
			log.error("Error in aura rule cache provider: {}",e);
		}
		log.info("Aura rule cache provider finish");
	}
		
	public void loadProperties()	{
		log.info("Load properties start");
		try{		
			ruleMap = getDroolsSet();
			if(ruleMap.isEmpty()){
				throw(new Exception("Drools not initialized properly. Terminating process."));
			}
		}catch(Exception e){
			log.error("Error in load properties: {}",e);
		}
		log.info("Load properties finish");
	}
	
	private HashMap getDroolsSet()	{
		log.info("Get drools set start");
		HashMap obj_props = new HashMap();
		/*String RULE_FILE_LOC = "E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";*/
		try {
			obj_props.put(ApplicationConstants.AURA_QUESTION_FILTER_RULE,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.AURA_QUESTION_FILTER_RULE));
			log.info(">>>>Catching completed---AURA_QUESTION_FILTER_RULE");
			obj_props.put(ApplicationConstants.AURA_QUESTION_ID_BY_PROD,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.AURA_QUESTION_ID_BY_PROD));
			log.info(">>>>Catching completed---AURA_QUESTION_ID_BY_PROD");
			obj_props.put(ApplicationConstants.POST_PROCESS_MASTER,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_MASTER));
			log.info(">>>>Catching completed---POST_PROCESS_MASTER");
			obj_props.put(ApplicationConstants.POST_PROCESS_1,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_1));
			log.info(">>>>Catching completed---POST_PROCESS_1");
			obj_props.put(ApplicationConstants.POST_PROCESS_2,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_2));
			log.info(">>>>Catching completed---POST_PROCESS_2");
			obj_props.put(ApplicationConstants.POST_PROCESS_3,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_3));
			log.info(">>>>Catching completed---POST_PROCESS_3");
			obj_props.put(ApplicationConstants.POST_PROCESS_4,AuraDroolsHelper.getRuleSession(getFilePath(),ApplicationConstants.POST_PROCESS_4));
			log.info(">>>>Catching completed---POST_PROCESS_4");
		} catch (Exception e) {
			log.error("Error in get drools set: {}",e);
		}
		log.info("Get drools set finish");
		return obj_props;
	}

	
	public KnowledgeBase getDroolsFromCache(String arg_property)	{	
		return (KnowledgeBase)ruleMap.get(arg_property);
	}
	

	

}
