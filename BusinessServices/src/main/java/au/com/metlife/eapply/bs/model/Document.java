package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the "DOCUMENT" database table.
 * 
 */
@Entity
@Table(name = "TBL_A_DOCUMENT", schema="EAPPDB")
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="DOC_LOC", length=500)
	private String docLoc;

	@Column(name="DOC_STATUS", length=10)
	private String docStatus;

	@Column(name="DOC_TYPE", length=100)
	private String docType;

	@Column(nullable=false, length=47)
	private long eapplicationid;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_A_DOCUMENT_SEQ")
	@SequenceGenerator(name="TBL_A_DOCUMENT_SEQ",sequenceName = "EAPPDB.TBL_A_DOCUMENT_SEQ", initialValue=1, allocationSize=100)
	private long id;

	@Column(nullable=false)
	private Timestamp lastupdatedate;

	public Document() {
	}

	public String getDocLoc() {
		return this.docLoc;
	}

	public void setDocLoc(String docLoc) {
		this.docLoc = docLoc;
	}

	public String getDocStatus() {
		return this.docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getDocType() {
		return this.docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public long getEapplicationid() {
		return this.eapplicationid;
	}

	public void setEapplicationid(long eapplicationid) {
		this.eapplicationid = eapplicationid;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getLastupdatedate() {
		return this.lastupdatedate;
	}

	public void setLastupdatedate(Timestamp lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

}