package pages.metlife.Angular.Statewide;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class STATEWIDE_Angular_ConvertandMaintain extends MasterPage {
	WebDriverUtil driverUtil = null;

	public STATEWIDE_Angular_ConvertandMaintain ConvertandMaintain_STATEWIDE() {
		ConvertandMaintainSTATEWIDE();
		
		confirmation();
		
		return new STATEWIDE_Angular_ConvertandMaintain(scriptHelper);
	}
	
	

	public STATEWIDE_Angular_ConvertandMaintain(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ConvertandMaintainSTATEWIDE() {

		String EmailId = dataTable.getData("STATEWIDE_ConvertandMaintain", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_ConvertandMaintain", "TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_ConvertandMaintain", "ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_ConvertandMaintain", "TimeofContact");
		String Gender = dataTable.getData("STATEWIDE_ConvertandMaintain", "Gender");		
		String FifteenHoursWork = dataTable.getData("STATEWIDE_ConvertandMaintain", "FifteenHoursWork");
		String RegularIncome=dataTable.getData("STATEWIDE_ConvertandMaintain", "RegularIncome");
		String Perform_WithoutRestriction=dataTable.getData("STATEWIDE_ConvertandMaintain", "Perform_WithoutRestriction");
		String Work_WithoutLimitation=dataTable.getData("STATEWIDE_ConvertandMaintain", "Work_WithoutLimitation");	
		String Citizen = dataTable.getData("STATEWIDE_ConvertandMaintain", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_ConvertandMaintain", "IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_ConvertandMaintain", "OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_ConvertandMaintain", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_ConvertandMaintain", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("STATEWIDE_ConvertandMaintain", "TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_ConvertandMaintain", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_ConvertandMaintain", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("STATEWIDE_ConvertandMaintain", "AnnualSalary");		
		String CostType = dataTable.getData("STATEWIDE_ConvertandMaintain", "CostType");			
		String WorkLimitation = dataTable.getData("STATEWIDE_ConvertandMaintain", "Work_WithoutLimitation");
		String Smokerquestion = dataTable.getData("STATEWIDE_ConvertandMaintain", "Smoker");
		String Permanentlyempyd = dataTable.getData("STATEWIDE_ConvertandMaintain", "Permanentlyempyd");
		
		try{
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,450)", "");
			
			Thread.sleep(2000);
		
			
			//****Click on Convert and Maintain Link*****\\
			WebDriverWait waiting=new WebDriverWait(driver,35);
		
			waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Convert and maintain')]"))).click();
			report.updateTestLog("Convert and Maintain", "Clicked on Convert and Maintain Link Link", Status.PASS);
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is Checked", Status.PASS);

			
            //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement label is Checked", Status.PASS);
		
            //*****Enter the Email Id*****\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\			
				
				Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
				Contact.selectByVisibleText(TypeofContact);			
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
								
				
			//****Enter the Contact Number****\\	
				
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span")).click();
					
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
//****Contact Details-Continue Button*****\\			
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(formOne);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);	
				
				
				//***************************************\\							
				//**********OCCUPATION SECTION***********\\							
				//****************************************\\	
				
				
				
											
				//*****Select the Smoker Question*******\\		
				
				if(Smokerquestion.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[1]/div[2]/div/div[1]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[1]/div[2]/div/div[2]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
				//*****Select the 15 Hours Question*******\\		
				
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[2]/div[2]/div/div[1]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);							
					}		
				else{		
																					  
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[2]/div[2]/div/div[2]/label/span"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
				
				//*****Select the industry type*******\\	
				
				Select Industry=new Select(driver.findElement(By.xpath("//select[@ng-model='industry']")));		
				Industry.selectByVisibleText(IndustryType);			
				report.updateTestLog("Industry Type", "Preferred Industry Type: "+IndustryType+" is Selected", Status.PASS);
				sleep(250);
					
				//*****Select the occupation type*******\\	
				
				Select Occupation=new Select(driver.findElement(By.xpath("//select[@ng-model='workRatingoccupation']")));		
				Occupation.selectByVisibleText(OccupationType);			
				report.updateTestLog("Occupation Type", "Preferred Industry Type: "+OccupationType+" is Selected", Status.PASS);
				sleep(250);	
			
				//*****If other occupation type*******\\	
				
				if(OccupationType.equalsIgnoreCase("Other")){		
					
					wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);	
				}								
			    

	//*****Extra Questions*****\\							
				
			    if(driver.getPageSource().contains("Do you work wholly within an office environment?")){							
			    
										
			//***Select if your office Duties are Undertaken within an Office Environment?\\	
			    	
			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
			   
			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
										
			}							
			else{							

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
										
			}							
										
			      //*****Do You Hold a Tertiary Qualification******\\							
										
			        if(TertiaryQual.equalsIgnoreCase("Yes")){							
			       
			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div/div[2]/div/div[1]/label/span"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}							
			   else{							
			       
				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div/div[2]/div/div[2]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			}							
					
			    
			      //*****What is your annual Salary******							
			    	
			    	
			        wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary);
			        sleep(500);		
			        report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);	
			    
			    //yuvi
			    
				if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("35 hours questions", "Selected Yes", Status.PASS);
						
					}	
				else{	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("35 hours questions", "Selected No", Status.PASS);
						
					}
			  
			  
			  
			  //*****Permanently employed***\\	
				
				if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Permanently emplyd question", "Selected Yes", Status.PASS);
						
					}	
				else{															
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Permanently emplyd question", "Selected No", Status.PASS);
						
					}
			  
			  
			//*****Resident of Australia****\\	
				
					if(Citizen.equalsIgnoreCase("Yes")){		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[1]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[2]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}
					
					
					
			    }
			    //yuvi
										
										
										
			// if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
										
			    else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){			
										
			//******Do you work in hazardous environment*****\\		
				
			if(HazardousEnv.equalsIgnoreCase("Yes")){							
																			   
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			}							
			else{							

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			}							
										
			//******Do you spend more than 10% of your working time outside of an office environment?*******\\	

			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("10% work outside Ofice", "Selected Yes", Status.PASS);
			}							
			else{							

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("10% work outside Ofice", "Selected No", Status.PASS);
			}	

			//*****What is your annual Salary******							


			wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary);
			sleep(500);		
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);	

			//yuvi

				if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("35 hours questions", "Selected Yes", Status.PASS);
						
					}	
				else{	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("35 hours questions", "Selected No", Status.PASS);
						
					}

			//*****Permanently employed***\\	
				
				if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[1]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Permanently emplyd question", "Selected Yes", Status.PASS);
						
					}	
				else{	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[9]/div[2]/div/div[2]/label/span"))).click();
					sleep(250);
					report.updateTestLog("Permanently emplyd question", "Selected No", Status.PASS);
						
					}

			//*****Resident of Australia****\\	
				
					if(Citizen.equalsIgnoreCase("Yes")){		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[1]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[10]/div[2]/div/div[2]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}
					}		
					
			    else{
				//*****What is your annual Salary******							
			     

				wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalCCId"))).sendKeys(AnnualSalary);
				sleep(500);		
				report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);	
				
			//35 hours question
				sleep(250);
					if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div[2]/div/div[1]/label/span"))).click();
						sleep(250);                                                 
						report.updateTestLog("35 hours questions", "Selected Yes", Status.PASS);
							
						}	
					else{	                                                         
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[6]/div[2]/div/div[2]/label/span"))).click();
						sleep(250);
						report.updateTestLog("35 hours questions", "Selected No", Status.PASS);
							
						}
				  
				  
				  
				  //*****Permanently employed***\\	
					sleep(250);
					if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){		         
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[7]/div[2]/div/div[1]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Permanently emplyd question", "Selected Yes", Status.PASS);
							
						}	
					else{	 														
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[7]/div[2]/div/div[2]/label/span"))).click();
						sleep(250);
						report.updateTestLog("Permanently emplyd question", "Selected No", Status.PASS);
							
						}
				  
				  
				//*****Resident of Australia****\\	
					sleep(250);
					
						if(Citizen.equalsIgnoreCase("Yes")){		                     
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[1]/label/span"))).click();
							sleep(250);
							report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
								
							}	
						else{	
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[8]/div[2]/div/div[2]/label/span"))).click();
							sleep(250);
							report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
								
							}

			    }
			  sleep(800);
						  
							
							//******Click on Cost of  Insurance as*******\\		
							sleep(1000);			
							Select COSTTYPE=new Select(driver.findElement(By.name("premFreq")));		
							COSTTYPE.selectByVisibleText(CostType);	
							sleep(250);
							report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
							sleep(800);
			
							
							//*****Click on Continue*******\\
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(occupationForm);']"))).click();
							report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);						
				
						
							
							
                           //*****Click on  CONVERT AND MAINTAIN TO FIXED COVER Continue*******\\
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-if='totalWeeklyCost > 0 && (!deathDecOrCancelFlag  && !tpdDecOrCancelFlag && !ipDecOrCancelFlag)']"))).click();
							report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);					
				
													
							
	
							
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void confirmation() {
		
		WebDriverWait wait=new WebDriverWait(driver,20);	
			
			try{
				if(driver.getPageSource().contains("I acknowledge I have read, understood and consent to the above.")){			
				
				

				
			//******Agree to acknowledge*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span"))).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(500);
				
				
			//******Click on Submit*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapse4']/div/div[3]/div[2]/button"))).click();
				report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
				sleep(1000);			
				
			}
			
			//*******Feedback popup******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1500);
			
	
				if(driver.getPageSource().contains("Application number")){	
			
			//*****Fetching the Application Status*****\\
			
			String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(500);
			
			//*****Fetching the Application Number******\\
			
			String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
			sleep(1000);
			
			
			
			//******Download the PDF***********\\
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
				sleep(1500);
	  		}	
			
			
			
		  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
		  			sleep(1500);
		  			Robot roboobj=new Robot();
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  		}
		  		
		  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		  			Robot roboobj=new Robot();
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
		  				
		  			sleep(4000);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  			
		  		}
			
			report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
			
			sleep(1000);
		
			 
			  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);
			
				
			 report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);	
			}

			else{
				 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
			}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
	}


	}

