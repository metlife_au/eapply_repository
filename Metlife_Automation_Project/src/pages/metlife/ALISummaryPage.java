package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ALISummaryPageObjects;


public class ALISummaryPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public ALISummaryPage yourSummary() {
		yourSummaryDetails();
		return new ALISummaryPage(scriptHelper);
	}

	public ALISummaryPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourSummaryDetails() {

		String ackexclusion = dataTable.getData("MetLife_Data", "AckExclusion");
		
		try {
			
			if (ackexclusion.equalsIgnoreCase("exclusion")) {
				tryAction(fluentWaitElement(ALISummaryPageObjects.chkexclusion), "Click", "Exclusion" );
			} 
			
			if (ackexclusion.equalsIgnoreCase("loading")) {
				tryAction(fluentWaitElement(ALISummaryPageObjects.chkloading), "Click", "Loading" );				
			}
		
			if (ackexclusion.equalsIgnoreCase("loading&exclusion")) {
				tryAction(fluentWaitElement(ALISummaryPageObjects.chkloading), "Click", "Loading" );	
				Thread.sleep(1000);
				tryAction(fluentWaitElement(ALISummaryPageObjects.chkexclusion), "Click", "Exclusion" );
			}
			
			Thread.sleep(1000);
			
			//Clicking Next on Summary page

			tryAction(fluentWaitElement(ALISummaryPageObjects.btnsummaryNext), "Click", "Apply");
			Thread.sleep(1000);

						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
