package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIConfirmationsPageObjects extends PageObjectLibrary{
		

	public static final By linkTitle = getObjectIdentifaction("ALI.confirmation.title.drpdown.css");
	public static final By txtlastName = getObjectIdentifaction("ALI.confirmation.lastname.text.id");
	public static final By txtContact = getObjectIdentifaction("ALI.confirmation.contact.text.id");
	public static final By txtcontactOther = getObjectIdentifaction("ALI.confirmation.contactother.text.id");
	public static final By txtemail = getObjectIdentifaction("ALI.confirmation.email.text.id");
//	public static final By drpcontactTime = getObjectIdentifaction("ALI.confirmation.contacttime.dropdown.css");
	public static final By txtemailconfirm = getObjectIdentifaction("ALI.confirmation.confirm.email.text.id");
	public static final By txtaddress1 = getObjectIdentifaction("ALI.confirmation.address1.text.id");
	public static final By txtaddress2 = getObjectIdentifaction("ALI.confirmation.address2.text.id");

	public static final By txtSuburb = getObjectIdentifaction("ALI.confirmation.suburb.text.id");
	public static final By drpState = getObjectIdentifaction("ALI.confirmation.state.dropdown.css");

	
	public static final By txtPostcode = getObjectIdentifaction("ALI.confirmation.postcode.txt.id");
	public static final By chkacknowledge = getObjectIdentifaction("ALI.confirmation.acknowledge.chkbox.css");
	public static final By btnNext = getObjectIdentifaction("ALI.confirmation.next.btn.css");
}
