(function(angular){
'use strict';

angular.module('CorporateApp', ['ui.router', 'corp.ui.constants', 'Metlife.corp.uicomponents', 'Metlife.corp.directives','restAPI','ngDialog','appDataModel','sessionTimeOut', 'eApplyInterceptor'])
  .run(function (beforeUnload, $rootScope) {
    // Must invoke the service at least once
    $rootScope.$on('onBeforeUnload', function (e, confirmation) {
      confirmation.message = 'All data willl be lost.';
      e.preventDefault();
    });
    $rootScope.$on('onUnload', function (e) {
        console.log('leaving page'); // Use 'Preserve Log' option in Console
    });
  });

})(angular);
