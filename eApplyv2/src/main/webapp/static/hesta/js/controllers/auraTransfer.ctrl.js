/*aura transfer controller*/    
CareSuperApp.controller('auratransfer',['$scope','$timeout', '$location','$window','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService','submitAura','persoanlDetailService','ngDialog','urlService',
                                     function($scope,$timeout, $location,$window,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,ngDialog,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {   
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	   //$location.path( path );
  	};
  	
 // added for session expiry
    /*$timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
     
  	$scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
  	
  	$scope.coverDetails=PersistenceService.gettransferCoverDetails();
  	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.transferOccDetails =PersistenceService.getTransferCoverOccDetails();
  	$scope.deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
	$scope.tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
	$scope.ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();
    console.log($scope.coverDetails); 	
    	
 // 	console.log(auraInputService); 	
  	auraInputService.setFund('CARE');
  	auraInputService.setMode('TransferCover'); 	
  	//setting values from quote page
    auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ; 	
 //	auraInputService.setAge(moment().diff($scope.coverDetails.dob, 'years')) ; 
  	auraInputService.setName($scope.coverDetails.name);
  	auraInputService.setAppnumber($scope.coverDetails.appNum);
  	
  	if($scope.transferOccDetails && $scope.transferOccDetails.gender ){
  		auraInputService.setGender($scope.transferOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
  	auraInputService.setCountry('Australia');
  	auraInputService.setFifteenHr($scope.transferOccDetails.fifteenHr);
  	auraInputService.setDeathAmt($scope.deathAddnlCvrDetails.deathTransferCovrAmt);
  	auraInputService.setTpdAmt($scope.tpdAddnlCvrDetails.tpdTransferCovrAmt);
  	auraInputService.setIpAmt($scope.ipAddnlCvrDetails.ipTransferCovrAmt);
  	auraInputService.setWaitingPeriod($scope.ipAddnlCvrDetails.addnlTransferWaitingPeriod);
  	auraInputService.setBenefitPeriod($scope.ipAddnlCvrDetails.addnlTransferBenefitPeriod) ;
  	auraInputService.setIndustryOcc($scope.transferOccDetails.industryCode+":"+$scope.transferOccDetails.occupation);
  	auraInputService.setSalary($scope.transferOccDetails.salary);
  	auraInputService.setClientname('metaus')
  	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
  	auraInputService.setExistingTerm(false);
  	if($scope.personalDetails[0].memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	}
  	/*auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')
  	auraInputService.setGender($scope.coverDetails.gender)
  	auraInputService.setIndustryOcc($scope.coverDetails.industryCode+":"+$scope.coverDetails.occupation)
  	if($scope.transferOccDetails.citizenQue=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.transferOccDetails.citizenQue)
  	}  	
  	auraInputService.setSalary($scope.transferOccDetails.salary)
  	auraInputService.setFifteenHr($scope.transferOccDetails.fifteenHr) 
  	auraInputService.setName($scope.coverDetails.name)*/
  	///////////////   	
  	
  	 //setting deafult vaues for testing
	/*auraInputService.setName('CARE')
  	auraInputService.setAge(50)
  	//below values has to be set from quote page, as of now taking from existing insurance
  	auraInputService.setAppnumber(14782223482354338)
  	auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')   
  	auraInputService.setGender('Male')
  	auraInputService.setIndustryOcc('026:Transfer')
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary('100000')
  	auraInputService.setFifteenHr('Yes')*/
  	 //end deafult vaues for testing
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
  		$scope.auraResponseDataList = response.data.questions;  		
  		angular.forEach($scope.auraResponseDataList, function(Object) {
			$scope.sectionname = Object.questionAlias.substring(3);            			
			 
			});
  	});
  	
  	 $scope.updateRadio = function (answerValue, questionObj){   
  		console.log($scope.auraResponseDataList) 	
  		
  		questionObj.arrAns[0]=answerValue;
  		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
  		  auraResponseService.setResponse($scope.auraRes)  		  
  		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {  			 
  			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
  			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
  	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 	  			
  	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
  	  				//branch complete false for previous questions  	  				
  	  				$scope.auraResponseDataList[selectedIndex].error=true;  	  				
  	  			}else if(Object.questionComplete){
  	  				$scope.auraResponseDataList[selectedIndex].error=false;
  	  			}
  	  			
  	  		});
  			  
      	}, function () {
      		//console.log('failed');
      	});
  	 };
  	 
  	$scope.proceedNext = function() {
  		$scope.keepGoing= true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true; 
  				$scope.keepGoing= false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  				//$scope.go('/transferSummary');
  			}
  		});
  		if($scope.keepGoing){
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) { 
  				PersistenceService.setTransCoverAuraDetails(response.data);
  				//console.log(response.data);
  				$scope.go('/transferSummary');
  	  		});
  		}
  	}
  	
  	/*$scope.transferAuraSaveAndExitPopUp = function (hhText) {
      	
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
	
 	var appNum;
    appNum = PersistenceService.getAppNumber();
  	$scope.saveAuraTransfer = function() {
  		$scope.transferAuraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
  		if($scope.coverDetails != null && $scope.transferOccDetails != null && $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null && $scope.personalDetails[0] != null){
    		$scope.coverDetails.lastSavedOn = 'AuraTransferPage';
    		//$scope.auraDetails
    		$scope.details = {};
	    	$scope.details.deathAddnlTransferDetails = $scope.deathAddnlCvrDetails;
	    	$scope.details.tpdAddnlTransferDetails = $scope.tpdAddnlCvrDetails;
	    	$scope.details.ipAddnlTransferCvrDetails = $scope.ipAddnlCvrDetails;
	    	$scope.details.transferOccDetails = $scope.transferOccDetails;
    		var temp = angular.extend($scope.coverDetails, $scope.details);
        	var saveAuraTransferObject = angular.extend(temp, $scope.personalDetails[0]);
        	auraResponseService.setResponse(saveAuraTransferObject)
	        saveEapply.reqObj().then(function(response) {  
	                console.log(response.data)
	        });
    	}
  	};
  	*/
  	$scope.collapseOne = true;
  	$scope.toggleOne = function() {
        $scope.collapseOne = !$scope.collapseOne;          
    };
  	$scope.collapseTwo = false;
  	$scope.toggleTwo = function() {
        $scope.collapseTwo = !$scope.collapseTwo;            
    };
    $scope.collapseThree = false;
  	$scope.toggleThree = function() {
        $scope.collapseThree = !$scope.collapseThree;            
    };
    $scope.collapseFour = false;
  	$scope.toggleFour = function() {
        $scope.collapseFour = !$scope.collapseFour;            
    };
    $scope.collapseFive = false;
  	$scope.toggleFive = function() {
        $scope.collapseFive = !$scope.collapseFive;            
    };
}]); 
////