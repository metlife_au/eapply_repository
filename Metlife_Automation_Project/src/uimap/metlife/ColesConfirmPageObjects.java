package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesConfirmPageObjects extends PageObjectLibrary {
	public static final By selTitle = getObjectIdentifaction("coles.confirm.title.dropdown.css");
	public static final By linkTitleMr = getObjectIdentifaction("coles.confirm.title.mr.linktext");
	public static final By linkTitleMrs = getObjectIdentifaction("coles.confirm.title.mrs.linktext");
	public static final By linkTitleMs = getObjectIdentifaction("coles.confirm.title.ms.linktext");
	public static final By linkTitleMiss = getObjectIdentifaction("coles.confirm.title.miss.linktext");
	public static final By linkTitleDr = getObjectIdentifaction("coles.confirm.title.dr.linktext");
	public static final By txtFirtsName = getObjectIdentifaction("coles.confirm.firstname.text.id");
	public static final By txtLastName = getObjectIdentifaction("coles.confirm.lastname.text.id");
	public static final By txtPrefContactNum = getObjectIdentifaction("coles.confirm.prefcontactnumber.text.id");
	public static final By selPrefContactNum = getObjectIdentifaction("coles.confirm.prefcontactnumber.dropdown.id");
	public static final By txtOtherContactNum = getObjectIdentifaction("coles.confirm.othercontactnumber.text.id");
	public static final By selOtherContactNum = getObjectIdentifaction("coles.confirm.othercontactnumber.dropdown.id");
	public static final By txtEmail = getObjectIdentifaction("coles.confirm.email.text.id");
	public static final By txtConfirmEmail = getObjectIdentifaction("coles.confirm.confirmemail.text.id");
	public static final By txtUnitNo = getObjectIdentifaction("coles.confirm.unitno.text.id");
	public static final By txtStreetNo = getObjectIdentifaction("coles.confirm.streetno.text.id");
	public static final By txtStreetName = getObjectIdentifaction("coles.confirm.streetname.text.id");
	public static final By txtSuburb = getObjectIdentifaction("coles.confirm.suburb.text.id");
	public static final By selState = getObjectIdentifaction("coles.confirm.state.dropdown.id");
	public static final By txtPostCode = getObjectIdentifaction("coles.confirm.postcode.text.id");
	public static final By chkkDeclarationAgree = getObjectIdentifaction("coles.confirm.declaration.checkbox.id");
	public static final By lnkNext = getObjectIdentifaction("coles.confirm.next.link.id");
}
