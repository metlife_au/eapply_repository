// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var premCalcApp = angular.module('premCalcApp',['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'restAPI', 'appDataModel','ui.bootstrap','eApplyInterceptor']);
// configure our routes
premCalcApp.config(function($routeProvider,$httpProvider,$templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('eapplyrouter');
    $templateRequestProvider.httpOptions({_isTemplate: true});
$routeProvider
.when('/', {
    templateUrl : 'static/vicsuper/PremiumCalc.html',
    controller  : 'premCalc',
			reloadOnSearch: false,
    resolve: {
     urls:function(fetchUrlSvc){
       var path = 'CareSuperUrls.properties';
       return fetchUrlSvc.getUrls(path);
     }
    }
    })
	    .when('/premCalc', {
        templateUrl : 'static/vicsuper/PremiumCalc.html',
        controller  : 'premCalc',
  			reloadOnSearch: false,
        resolve: {
         urls:function(fetchUrlSvc){
           var path = 'CareSuperUrls.properties';
           return fetchUrlSvc.getUrls(path);
         }
        }
	    })
	    
	    
})