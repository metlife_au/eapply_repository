package au.com.metlife.eapply.bs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;
import au.com.metlife.eapply.bs.service.TblOccMappingRuledataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@Component
public class TblOccMappingRuledataController {
	private static final Logger log = LoggerFactory.getLogger(TblOccMappingRuledataController.class);
	
	
	private final TblOccMappingRuledataService tblOccMappingRuledataService;
	
	 @Autowired
	    public TblOccMappingRuledataController(final TblOccMappingRuledataService tblOccMappingRuledataService) {
	        this.tblOccMappingRuledataService = tblOccMappingRuledataService;
	    }
	 
	 @RequestMapping("/getOccupationName")
	  public ResponseEntity<List<TblOccMappingRuledata>> getOccupationName(@RequestParam(value="fundId") String fundId ,@RequestParam(value="induCode") String induCode) {
		 log.info("Getting occupation name start");
		 log.info(">>>fundId {} and >>>>indcc {} ",fundId,induCode);
		List<TblOccMappingRuledata> tblOccMappingRuledata =new ArrayList();
		if(fundId!=null && induCode!=null){
			tblOccMappingRuledata =  tblOccMappingRuledataService.getOccupationList(fundId, induCode);
		}
		Collections.sort(tblOccMappingRuledata,new OccComparator()); 
      
		for(int i=0;i<tblOccMappingRuledata.size();i++){
			
			if(tblOccMappingRuledata.get(i).getOccupationName() !=null && tblOccMappingRuledata.get(i).getOccupationName().equalsIgnoreCase("Other")){
				List<TblOccMappingRuledata> occOther=new ArrayList<>();
				occOther.add(tblOccMappingRuledata.get(i));
				tblOccMappingRuledata.removeAll(occOther);
				tblOccMappingRuledata.addAll(occOther);
				
			}
		}
		if(tblOccMappingRuledata.isEmpty()){
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }
		log.info("Getting occupation name finish");
	    return new ResponseEntity<>(tblOccMappingRuledata, HttpStatus.OK);
		 
	 }
	 
	 @RequestMapping("/generateApplicationNo")
	  public ResponseEntity<String> generateApplicationNo() {	
		 log.info("Generate application number start");
		java.lang.String retVal = null;
		try {
			Random random = new Random();
			retVal = Long.toString(Math.round(random.nextFloat() * Math.pow(10, 8)));
		} catch (Exception e) {
			log.error("Error in generating application number {}",e);
		}
		log.info("Generate application number finish");
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	 }
	 
	 

}
