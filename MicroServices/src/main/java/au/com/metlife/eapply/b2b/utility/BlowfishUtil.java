
package au.com.metlife.eapply.b2b.utility;

import au.com.metlife.eapply.blowfish.BinConverter;
import au.com.metlife.eapply.blowfish.BlowfishECB;
import au.com.metlife.eapply.blowfish.StringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlowfishUtil{
	private BlowfishUtil(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(BlowfishUtil.class);

	public static String decrypt(String key,String hexaEncStrign){
		log.info("Decryption process start");
		byte[] msgBuf=StringConverter.hexToByte(hexaEncStrign);
		byte[] keyByte=key.getBytes();
		BlowfishECB bfe = new BlowfishECB(keyByte, 0, keyByte.length);
		bfe.decrypt(msgBuf, 0, msgBuf, 0, msgBuf.length);
		log.info("Decryption process finish");
		return new String(msgBuf);
	}

	

	

	public static String  encrypt(String key,String plainString ){
		log.info("Encryption process start");
		int nRest;
		int nMsgSize;
		byte[] testKey;
		byte[] tempBuf;
		byte[] msgBuf;
		BlowfishECB bfe;
		testKey=key.getBytes();
		bfe = new BlowfishECB(testKey, 0, testKey.length);

		tempBuf=plainString.getBytes();
		nMsgSize=tempBuf.length;
		nRest = nMsgSize & 7;

		if (nRest != 0)	{
			msgBuf = new byte[(nMsgSize & (~7))+ 8 ];

			System.arraycopy(tempBuf, 0, msgBuf, 0, nMsgSize);

			for (int count = nMsgSize; count < msgBuf.length; count++){
				msgBuf[count] = ' ';
			}
		}
		else{
			msgBuf = new byte[nMsgSize];
			System.arraycopy(tempBuf, 0, msgBuf, 0, nMsgSize);
		}

		bfe.encrypt(msgBuf, 0, msgBuf, 0, msgBuf.length);
		log.info("Encryption process finish");
		return BinConverter.bytesToHexStr(msgBuf);
	}


}
