package au.com.metlife.eapply.b2b.utility;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;


public class AesHelper {
	private AesHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(AesHelper.class);
	 /* 128 bit key*/
	public static final String key = "Bar12345Bar12345";
	/*16 bytes IV*/
	public static final String initVector = "RandomInitVector"; 
	
	 public static String encrypt(String value) {
		 log.info("Encryption process start");
	        try {
	            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(MetlifeInstitutionalConstants.UTF_8));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(MetlifeInstitutionalConstants.UTF_8), "AES");

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

	            byte[] encrypted = cipher.doFinal(value.getBytes());
	            log.info("encrypted string: {}", Base64.encode(encrypted));

	            return Arrays.toString(Base64.encode(encrypted));
	        } catch (Exception ex) {
	          log.error("Error in Encryption {}", ex);
	        }
	        log.info("Encryption process finish");
	        return null;
	    }

	    public static String decrypt(String key, String initVector, String encrypted) {
	    	log.info("Decryption process finish");
	        try {
	            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(MetlifeInstitutionalConstants.UTF_8));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(MetlifeInstitutionalConstants.UTF_8), "AES");

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

	            byte[] original = cipher.doFinal(Base64.decode(encrypted.getBytes()));

	            return new String(original);
	        } catch (Exception ex) {
	        	log.error("Error in Decryption {}", ex);
	        }
	        log.info("Decryption process finish");
	        return null;
	    }
}
