package au.com.metlife.eapply;

import au.com.metlife.eapply.constants.AppErrorCodes;

public class EtoolKitException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 411929288552875211L;
	
	private final Throwable exception;
	
	private final int errorCode;
	
	private final String errorMsg;
	
	private final AppErrorCodes category;
	
	private final String element;

	public EtoolKitException(){
		this.errorCode = 0;
		this.errorMsg = "Error occured";
		this.exception = new Exception();
		this.category = AppErrorCodes.DEFAULT;
		this.element = "";
	}
	
	public EtoolKitException(int errorCode, String errorMessage){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.category = AppErrorCodes.DEFAULT;
		this.element = "";
		this.exception = new Exception();
	}
	
	public EtoolKitException(int errorCode, String errorMessage, Throwable cause){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.exception = cause;
		this.category = AppErrorCodes.DEFAULT;
		this.element = "";
	}
	
	public EtoolKitException(int errorCode, String errorMessage, AppErrorCodes category){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.category = category;
		this.element = "";
		this.exception = new Exception();
	}
	
	public EtoolKitException(int errorCode, String errorMessage, AppErrorCodes category, String element){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.category = category;
		this.element = element;
		this.exception = new Exception();
	}
	
	public EtoolKitException(int errorCode, String errorMessage, AppErrorCodes category, Throwable cause){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.category = category;
		this.exception = cause;
		this.element = "";
	}
	
	public EtoolKitException(int errorCode, String errorMessage, AppErrorCodes category, String element, Throwable cause){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.category = category;
		this.exception = cause;
		this.element = element;
	}

	public Throwable getException() {
		return exception;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public AppErrorCodes getCategory() {
		return category;
	}

	public String getElement() {
		return element;
	}
	
	

}
