package au.com.metlife.eapply.ct.dto;


import java.util.Date;

public class LoginAttempts {
	
	private String ipAddress;	
	private Integer attempts;
	private Character ipLocked;
	private Date lastLogin;
	private Boolean unlock = Boolean.FALSE;
		
	
		public Boolean getUnlock() {
		return unlock;
	}
	public void setUnlock(Boolean unlock) {
		this.unlock = unlock;
	}
		public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
		public Character getIpLocked() {
		return ipLocked;
	}
	public void setIpLocked(Character ipLocked) {
		this.ipLocked = ipLocked;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getAttempts() {
		return attempts;
	}
	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}

}
