package au.com.metlife.eapply.calculator.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExpensesVO implements Serializable{
public static final String WEEKLY="Weekly";
	
	public static final String ANNUALLY="Annually";
	
	public static final String ANNUAL="Annual";
	
	public static final String QUARTERLY="Quarterly";
	
	public static final String MONTHLY="MONTHLY";
	
	public static final String FORTNIGHTLY="FORTNIGHTLY";
	
	private String expenseType=null;
	
	private BigDecimal expenseAmount=null;
	
	private BigDecimal oldExpenseAmount=null;
	
	private String expenseFreq=null;
	
	private String oldExpenseFreq=null;
	
	private BigDecimal weeklyAmount=null;
	
	private BigDecimal monthlyAmount=null;
	
	private BigDecimal yearlyAmount=null;
	
	private BigDecimal quarterlyAmount=null;
	
	private BigDecimal fortnightlyAmount=null;
	
	private boolean acceptValues=false;
	
	
	public boolean isAcceptValues() {
		return acceptValues;
	}



	public void setAcceptValues(boolean acceptValues) {
		this.acceptValues = acceptValues;
	}



	public ExpensesVO(String expenseType) {
		super();
		this.expenseType = expenseType;
	}
	
	

	public ExpensesVO(String expenseType, String expenseFreq) {
		super();
		this.expenseType = expenseType;
		this.expenseFreq = expenseFreq;
	}

	public BigDecimal getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(BigDecimal expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getExpenseFreq() {
		return expenseFreq;
	}

	public void setExpenseFreq(String expenseFreq) {
		this.expenseFreq = expenseFreq;
	}

	public String getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}

	public BigDecimal getFortnightlyAmount() {
		if(expenseFreq.equalsIgnoreCase(FORTNIGHTLY)){
			fortnightlyAmount=this.expenseAmount;
		}else if(expenseFreq.equalsIgnoreCase(ANNUALLY)){
			fortnightlyAmount=this.expenseAmount.divide(new BigDecimal(26),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(MONTHLY)){
			fortnightlyAmount=(this.expenseAmount.multiply(new BigDecimal(52))).divide(new BigDecimal(26),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(WEEKLY)){
			fortnightlyAmount=(this.expenseAmount.multiply(new BigDecimal(2)));
		}else if(expenseFreq.equalsIgnoreCase(QUARTERLY)){
			fortnightlyAmount=(this.expenseAmount.multiply(new BigDecimal(3))).divide(new BigDecimal(26),2,BigDecimal.ROUND_HALF_UP);
		}
		return fortnightlyAmount;
	}

	public void setFortnightlyAmount(BigDecimal fortnightlyAmount) {
		this.fortnightlyAmount = fortnightlyAmount;
	}

	public BigDecimal getMonthlyAmount() {
		if(expenseFreq.equalsIgnoreCase(MONTHLY)){
			monthlyAmount=this.expenseAmount;
		}else if(expenseFreq.equalsIgnoreCase(ANNUALLY)){
			monthlyAmount=this.expenseAmount.divide(new BigDecimal(12),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(QUARTERLY)){
			monthlyAmount=this.expenseAmount.divide(new BigDecimal(4),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(WEEKLY)){
			monthlyAmount=(this.expenseAmount.multiply(new BigDecimal(52))).divide(new BigDecimal(12),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(FORTNIGHTLY)){
			monthlyAmount=(this.expenseAmount.multiply(new BigDecimal(26))).divide(new BigDecimal(12),2,BigDecimal.ROUND_HALF_UP);
		}
		return monthlyAmount;
	}

	public void setMonthlyAmount(BigDecimal monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public BigDecimal getQuarterlyAmount() {
		if(expenseFreq.equalsIgnoreCase(QUARTERLY)){
			quarterlyAmount=this.expenseAmount;
		}else if(expenseFreq.equalsIgnoreCase(ANNUALLY)){
			quarterlyAmount=this.expenseAmount.divide(new BigDecimal(3),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(MONTHLY)){
			quarterlyAmount=(this.expenseAmount.multiply(new BigDecimal(4)));
		}else if(expenseFreq.equalsIgnoreCase(WEEKLY)){
			quarterlyAmount=(this.expenseAmount.multiply(new BigDecimal(52))).divide(new BigDecimal(3),2,BigDecimal.ROUND_HALF_UP);
		}else if(expenseFreq.equalsIgnoreCase(FORTNIGHTLY)){
			quarterlyAmount=(this.expenseAmount.multiply(new BigDecimal(26))).divide(new BigDecimal(3),2,BigDecimal.ROUND_HALF_UP);
		}
		return quarterlyAmount;
	}

	public void setQuarterlyAmount(BigDecimal quarterlyAmount) {
		this.quarterlyAmount = quarterlyAmount;
	}

	public BigDecimal getWeeklyAmount() {
		if(this.expenseAmount!=null && this.expenseFreq!=null){
			if(expenseFreq.equalsIgnoreCase(WEEKLY)){
				weeklyAmount=this.expenseAmount;
			}else if(expenseFreq.equalsIgnoreCase(ANNUALLY)){
				weeklyAmount=this.expenseAmount.divide(new BigDecimal(52),2,BigDecimal.ROUND_HALF_UP);
			}else if(expenseFreq.equalsIgnoreCase(QUARTERLY)){
				weeklyAmount=(this.expenseAmount.multiply(new BigDecimal(4))).divide(new BigDecimal(52),2,BigDecimal.ROUND_HALF_UP);
			}else if(expenseFreq.equalsIgnoreCase(MONTHLY)){
				weeklyAmount=(this.expenseAmount.multiply(new BigDecimal(12))).divide(new BigDecimal(52),2,BigDecimal.ROUND_HALF_UP);
			}else if(expenseFreq.equalsIgnoreCase(FORTNIGHTLY)){
				weeklyAmount=(this.expenseAmount.multiply(new BigDecimal(26))).divide(new BigDecimal(52),2,BigDecimal.ROUND_HALF_UP);
			}
		}
		return weeklyAmount;
	}

	public void setWeeklyAmount(BigDecimal weeklyAmount) {
		this.weeklyAmount = weeklyAmount;
	}

	public BigDecimal getYearlyAmount() {
		if(this.expenseAmount!=null && this.expenseFreq!=null){
			if(expenseFreq.equalsIgnoreCase(ANNUALLY)){
				yearlyAmount=this.expenseAmount;
			}else if(expenseFreq.equalsIgnoreCase(WEEKLY)){
				yearlyAmount=this.expenseAmount.multiply(new BigDecimal(52));
			}else if(expenseFreq.equalsIgnoreCase(QUARTERLY)){
				yearlyAmount=(this.expenseAmount.multiply(new BigDecimal(4)));
			}else if(expenseFreq.equalsIgnoreCase(MONTHLY)){
				yearlyAmount=(this.expenseAmount.multiply(new BigDecimal(12)));
			}else if(expenseFreq.equalsIgnoreCase(FORTNIGHTLY)){
				yearlyAmount=(this.expenseAmount.multiply(new BigDecimal(26)));
			}
		}
		return yearlyAmount;
	}

	public void setYearlyAmount(BigDecimal yearlyAmount) {
		this.yearlyAmount = yearlyAmount;
	}



	public BigDecimal getOldExpenseAmount() {
		return oldExpenseAmount;
	}



	public void setOldExpenseAmount(BigDecimal oldExpenseAmount) {
		this.oldExpenseAmount = oldExpenseAmount;
	}



	public String getOldExpenseFreq() {
		return oldExpenseFreq;
	}



	public void setOldExpenseFreq(String oldExpenseFreq) {
		this.oldExpenseFreq = oldExpenseFreq;
	}
	
}
