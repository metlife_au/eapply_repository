/**
 * 
 */
package pages.metlife;

import java.util.Properties;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.EServiceLoginPageObjects;
import uimap.metlife.EServiceRaiseRequestPageObjects;
import uimap.metlife.EServiceSearchPolicy;
import uimap.metlife.EViewLoginPageObjects;


/**
 * @author sampath
 * 
 */
public class EServiceRaiseRequest extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public EServiceRaiseRequest(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public EServiceRaiseRequest RaiseRequest() {
		RaiseRequestDet();
		return new EServiceRaiseRequest(scriptHelper);
	} 
	
	private void RaiseRequestDet() {
		
		String strPaymethod = dataTable.getData("MetLife_Data", "PaymentMethod");
		String strCCType = dataTable.getData("MetLife_Data", "CCType");
		String strCCnumber = dataTable.getData("MetLife_Data", "CCNUMBER");
		String strCCExpMonth = dataTable.getData("MetLife_Data", "CCExpMonth");
		String strCCExpYear = dataTable.getData("MetLife_Data", "CCExpYear");
	try{
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.linRraiserqst),"Click","Navigate to RaiseRequest");
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			driver.manage().window().maximize();
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.chkUpdatecredit),"Click","update credit card details");
			sleep(2000);
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.drpPaymeth),"DropDownSelect","payment menthod Select",strPaymethod);
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.drpCCtype),"DropDownSelect","card type Select",strCCType);
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.txtCCnum1),"SET","First 4 digits",strCCnumber.substring(0, 4));
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.txtCCnum2),"SET","Second 4 digits",strCCnumber.substring(4, 8));
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.txtCCnum3),"SET","Third 4 digits",strCCnumber.substring(8, 12));
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.txtCCnum4),"SET","fourth 4 digits",strCCnumber.substring(12, 16));
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.txtCCExpmonth),"SET","CC Exp month",strCCExpMonth);
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.txtCCExpyear),"SET","CC Exp year",strCCExpYear);
			sleep(1000);
			tryAction(waitForClickableElement(EServiceRaiseRequestPageObjects.linkDone),"Click","Click on done on card details update");
			sleep();
		//	switchToMainFrame();
			}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
