package allocator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openqa.selenium.Platform;

import com.cognizant.framework.ExcelDataAccess;
import com.cognizant.framework.FrameworkParameters;
import com.cognizant.framework.IterationOptions;
import com.cognizant.framework.Settings;
import com.cognizant.framework.selenium.Browser;
import com.cognizant.framework.selenium.ResultSummaryManager;
import com.cognizant.framework.selenium.SeleniumTestParameters;

import supportlibraries.PageObjectLoader;


/**
 * Class to manage the batch execution of test scripts within the framework
 * @author Cognizant
 */
public class Allocator
{
	private FrameworkParameters frameworkParameters = FrameworkParameters.getInstance();
	private Properties properties;
	private ResultSummaryManager resultSummaryManager = new ResultSummaryManager();


	/**
	 * The entry point of the test batch execution
	 * @param args Command line arguments to the Allocator (Not applicable)
	 */
	public static void main(String[] args)
	{
		Allocator allocator = new Allocator();
		allocator.driveBatchExecution();
	}

	private void driveBatchExecution()
	{
		resultSummaryManager.setRelativePath();

		/*Work around for eclipse project as well as standalone project - starts*/
		 String relativePath = new File(System.getProperty("user.dir")).getAbsolutePath();
		 if (relativePath.contains("allocator")) {
		 relativePath = new File(System.getProperty("user.dir")).getParent();
		}
		 if (relativePath.contains("src")) {
			 relativePath = new File(System.getProperty("user.dir")).getParent();
			}
		 frameworkParameters.setRelativePath(relativePath);
		 /*Work around for eclipse project as well as standalone project - ends*/

		properties = Settings.getInstance();

		/*Work around for driver path everywhere - starts*/
		properties.setProperty("ChromeDriverPath", relativePath+"/drivers/chromedriver.exe");
		properties.setProperty("InternetExplorerDriverPath", relativePath+"/drivers/IEDriverServer.exe");
		properties.setProperty("OperaDriverPath", relativePath+"/drivers/operadriver-1.5.jar");
		properties.setProperty("FirefoxDriverPath", relativePath+"/drivers/geckodriver.exe");
		/*Work around for driver path everywhere - ends*/

		if(properties.getProperty("ExternalisePageObjects").equalsIgnoreCase("true")){
			PageObjectLoader.loadProperties(relativePath+"/src/uimap/Page_Objects.properties");
		}

		resultSummaryManager.initializeTestBatch(properties.getProperty("RunConfiguration"));

		int nThreads = Integer.parseInt(properties.getProperty("NumberOfThreads"));
		resultSummaryManager.initializeSummaryReport(nThreads);

		resultSummaryManager.setupErrorLog();

		executeTestBatch(nThreads);

		resultSummaryManager.wrapUp(false);
		resultSummaryManager.launchResultSummary();
	}

	private void executeTestBatch(int nThreads)
	{
		List<SeleniumTestParameters> testInstancesToRun =
							getRunInfo(frameworkParameters.getRunConfiguration());
		ExecutorService parallelExecutor = Executors.newFixedThreadPool(nThreads);

		for (int currentTestInstance = 0; currentTestInstance < testInstancesToRun.size() ; currentTestInstance++ ) {
			ParallelRunner testRunner =
					new ParallelRunner(testInstancesToRun.get(currentTestInstance),
																resultSummaryManager);
			parallelExecutor.execute(testRunner);

			if(frameworkParameters.getStopExecution()) {
				break;
			}
		}

		parallelExecutor.shutdown();
		while(!parallelExecutor.isTerminated()) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private List<SeleniumTestParameters> getRunInfo(String sheetName)
	{
		ExcelDataAccess runManagerAccess = new ExcelDataAccess(frameworkParameters.getRelativePath(), "Run Manager");
		runManagerAccess.setDatasheetName(sheetName);

		int nTestInstances = runManagerAccess.getLastRowNum();
		List<SeleniumTestParameters> testInstancesToRun = new ArrayList<SeleniumTestParameters>();
		for (int currentTestInstance = 1; currentTestInstance <= nTestInstances; currentTestInstance++) {
			String executeFlag = runManagerAccess.getValue(currentTestInstance, "Execute");
			if (executeFlag.equalsIgnoreCase("Yes")) {
				String currentScenario = runManagerAccess.getValue(currentTestInstance, "TestScenario");
				String currentTestcase = runManagerAccess.getValue(currentTestInstance, "TestCase");
				SeleniumTestParameters testParameters =
						new SeleniumTestParameters(currentScenario, currentTestcase);
				testParameters.setCurrentTestDescription(runManagerAccess.getValue(currentTestInstance, "Description"));

				String iterationMode = runManagerAccess.getValue(currentTestInstance, "IterationMode");
				if (!iterationMode.equals("")) {
					testParameters.setIterationMode(IterationOptions.valueOf(iterationMode));
				} else {
					testParameters.setIterationMode(IterationOptions.RunAllIterations);
				}


				String startIteration = runManagerAccess.getValue(currentTestInstance, "StartIteration");
				if (!startIteration.equals("")) {
					testParameters.setStartIteration(Integer.parseInt(startIteration));
				}
				String endIteration = runManagerAccess.getValue(currentTestInstance, "EndIteration");
				if (!endIteration.equals("")) {
					testParameters.setEndIteration(Integer.parseInt(endIteration));
				}

				String browser = runManagerAccess.getValue(currentTestInstance, "Browser");
				if (!browser.equals("")) {
					testParameters.setBrowser(Browser.valueOf(browser));
					properties.setProperty("currentBrowser", browser);
				} else {
					testParameters.setBrowser(Browser.valueOf(properties.getProperty("DefaultBrowser")));
				}
				String browserVersion = runManagerAccess.getValue(currentTestInstance, "BrowserVersion");
				if (!browserVersion.equals("")) {
					testParameters.setBrowserVersion(browserVersion);
				}
				String platform = runManagerAccess.getValue(currentTestInstance, "Platform");
				if (!platform.equals("")) {
					testParameters.setPlatform(Platform.valueOf(platform));
				} else {
					testParameters.setPlatform(Platform.valueOf(properties.getProperty("DefaultPlatform")));
				}

				testInstancesToRun.add(testParameters);
			}
		}

		return testInstancesToRun;
	}
}