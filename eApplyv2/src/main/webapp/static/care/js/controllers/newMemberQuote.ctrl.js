/*New Member Change Cover Controller ,Progressive and Mandatory Validations Starts   */
CareSuperApp.controller('newmemberquote', ['$scope', '$rootScope', '$routeParams', '$location', '$http', '$timeout', '$window', 'QuoteService', 'OccupationService', 'persoanlDetailService', 'deathCoverService',
	'tpdCoverService', 'ipCoverService', 'CalculateService', 'auraInputService', 'CalculateDeathService',
	'CalculateTPDService', 'PersistenceService', 'ngDialog', 'auraResponseService', 'urlService', '$filter', 'APP_CONSTANTS',
	function ($scope, $rootScope, $routeParams, $location, $http, $timeout, $window, QuoteService, OccupationService, persoanlDetailService, deathCoverService, tpdCoverService,
		ipCoverService, CalculateService, auraInputService, CalculateDeathService, CalculateTPDService,
		PersistenceService, ngDialog, auraResponseService, urlService, $filter, APP_CONSTANTS) {
		
	/* Code for appD starts */
	  var pageTracker = null;
	  if(ADRUM) {
	    pageTracker = new ADRUM.events.VPageView();
	    pageTracker.start();
	  }

	  $scope.$on('$destroy', function() {
	    pageTracker.end();
	    ADRUM.report(pageTracker);
	  });
	  /* Code for appD ends */
	
	  	$scope.urlList = urlService.getUrlList();
		$scope.emailFormat = APP_CONSTANTS.emailFormat;
		$scope.phoneNo = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
		$scope.eligibilityViolated = false;
		$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
		$scope.preferredContactType = '';
		$rootScope.$broadcast('enablepointer');
		$scope.modelOptions = {updateOn: 'blur'};
		/* $scope.coltwo = false;
		 $scope.toggleTwo = function() {
				 $scope.coltwo = !$scope.coltwo;
				 $("a[data-target='#collapseTwo']").click();
		 };
		 $scope.colthree = false;
		 $scope.toggleThree = function() {
				 $scope.colthree = true;
				 $("a[data-target='#collapseThree']").click()
		 };
		 
		 $scope.collapse = false;
			 $scope.toggle = function() {
						 $scope.collapse = !$scope.collapse;           
				};*/
		$scope.customDigest = function() {
			if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
					$scope.$digest();
			}
		};
		// model values are not in sync, need to refactor this
	/*	$scope.$watch('newDeathrequireCover', function() {
			if(newDynamicFlag) {
				$scope.validateDeathTPDAmount();
			}
		}, true);

		$scope.$watch('newTPDRequireCover', function() {
			if(newDynamicFlag) {
				$scope.validateDeathTPDAmount();
			}
		}, true);*/
		
		

		$scope.isCollapsible = function (targetEle, event) {
			if (targetEle == 'collapseprivacy' && !$('#dodLabel').hasClass('active')) {
				event.stopPropagation();
				return false;
			} else if (targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
				event.stopPropagation();
				return false;
			} else if (targetEle == 'collapseTwo' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
				event.stopPropagation();
				return false;
			} else if (targetEle == 'collapseThree' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
				event.stopPropagation();
				return false;
			}
		}

		$scope.toggleTwo = function (checkFlag) {
			$scope.coltwo = checkFlag;
			if ((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
				return false;
			$("a[data-target='#collapseTwo']").click(); /* Can be improved */
		};

		$scope.toggleThree = function (checkFlag) {
			$scope.colthree = checkFlag;
			if ((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
				return false;
			$("a[data-target='#collapseThree']").click(); /* Can be improved */
		};

		$scope.privacyCol = false;
		var dodCheck;
		var privacyCheck;

		/* TBC */
		$scope.togglePrivacy = function (checkFlag) {
			$scope.privacyCol = checkFlag;
			if ((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
				return false;
			$("a[data-target='#collapseprivacy']").click(); /* Can be improved */
		};

		$scope.toggleContact = function (checkFlag) {
			$scope.contactCol = checkFlag;
			if ((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
				return false;
			$("a[data-target='#collapseOne']").click(); /* Can be improved */

		};
		$scope.checkDodState = function () {
			$timeout(function () {
				$scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
				if ($('#dodLabel').hasClass('active')) {
					$scope.togglePrivacy(true);
				} else {
					$scope.togglePrivacy(false);
					$scope.toggleContact(false);
					$scope.toggleTwo(false);
					$scope.toggleThree(false);
				}
			}, 1);
		};

		$scope.checkPrivacyState = function () {
			$timeout(function () {
				$scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
				if ($('#privacyLabel').hasClass('active')) {
					$scope.toggleContact(true);
				} else {
					$scope.toggleContact(false);
					$scope.toggleTwo(false);
					$scope.toggleThree(false);
				}
			}, 1);
		};

		QuoteService.getList($scope.urlList.quoteUrl, "CARE").then(function (res) {
			$scope.IndustryOptions = res.data;
		}, function (err) {
			console.log("Error while fetching industry options " + JSON.stringify(err));
		});
		$scope.isDeathDisabled = false;
		$scope.isTPDDisabled = false;
		$scope.isIPDisabled = false;
		$scope.otherOccupationObj = { 'newOtherOccupation': '' };

		$scope.showHelp = function (msg) {
			$scope.modalShown = !$scope.modalShown;
			$scope.tipMsg = msg;
		};

		$scope.invalidSalAmount = false;
		$scope.checkIPCover = function () {
			//commented code for WR 19056 start and code commented 'eligibilityViolated' check in line no 1007 & 1132
			/*if ($scope.newMemberDeathCoverDetails.amount > (7 * parseInt($scope.newAnnualSalary))) {
				$scope.eligibilityViolated = true;
			} else {
				$scope.eligibilityViolated = false;
			}*/
			
			//commented code for WR 19056 end 
			if (parseInt($scope.newAnnualSalary) == 0) {
				$scope.invalidSalAmount = true;
			} else {
				$scope.invalidSalAmount = false;
			}
			if (parseInt($scope.newAnnualSalary) < 16000 && $scope.workTimeAndSalary == "No") {
				$('#ipsection').removeClass('active');
				$("#sc").css("display", "none");
				$scope.isIPDisabled = true;
			} else {
    		/*if(($scope.terminalIllnessClaim == undefined || $scope.terminalIllnessClaim == "No") && 
    				($scope.tpdClaim == undefined || $scope.tpdClaim == "No")){*/
				$('#ipsection').addClass('active');
				$("#sc").css("display", "block");
				$scope.isIPDisabled = false;
				/*	}*/
			}
			$scope.setOccupationCategory();
		};

		$scope.indexation = {
			death: false,
			tpd: false
		}
		$scope.setNewIndexation = function ($event) {
			$event.stopPropagation();
			$event.preventDefault();
			$scope.indexation.death = $scope.indexation.tpd = !$scope.indexation.death;
			if (!$scope.indexation.death) {
				$("#newchangecvr-index-dth").parent().removeClass('active');
				$("#newchangeCvr-indextpd-tpd").parent().removeClass('active');
			}
		};

		$scope.syncRadios = function (val) {
			$scope.deathWarningFlag = false;
			$scope.tpdWarningFlag = false;
			$scope.newDeathrequireCover = "";
			$scope.newTPDRequireCover = "";
			$scope.coverAmountFlag = false;
			$scope.coverAmountErrorMsg = "";
			var radios = $('label[radio-sync]');
			var data = $('input[data-sync]');
			data.filter('[data-sync="' + val + '"]').attr('checked', 'checked');
			if (val == 'Fixed') {
				showhide('dollar1', 'nodollar1');
				showhide('dollar', 'nodollar');
				$scope.newDeathcoverType = 'DcFixed';
				$scope.newTpdCoverType = 'TPDFixed';
			} else if (val == 'Unitised') {
				showhide('nodollar1', 'dollar1');
				showhide('nodollar', 'dollar');
				$scope.newDeathcoverType = 'DcUnitised';
				$scope.newTpdCoverType = 'TPDUnitised';
			}
			radios.removeClass('active');
			radios.filter('[radio-sync="' + val + '"]').addClass('active');
			$scope.calculateNewQuoteOnChange();
		};

		$scope.navigateToLandingPage = function () {
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
			ngDialog.openConfirm({
				template: '<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
				plain: true,
				className: 'ngdialog-theme-plain custom-width'
			}).then(function () {
				$location.path("/landing");
			}, function (e) {
				if (e == 'oncancel') {
					return false;
				}
			});
		};

		$scope.getOccupations = function () {
			if (!$scope.newIndustry) {
				$scope.newIndustry = '';
			}
			OccupationService.getOccupationList($scope.urlList.occupationUrl, "CARE", $scope.newIndustry).then(function (res) {
				//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.newIndustry}, function(occupationList){
				$scope.OccupationList = res.data;
				$scope.newOccupation = "";
				$scope.calculateNewQuoteOnChange();
			}, function (err) {
				console.log("Error while fetching occupation options " + JSON.stringify(err));
			});
		};

    /*$scope.getOtherOccupationAS = function(entered) {
	    return $http.get('./occupation.json').then(function(response) {
	      $scope.occupationList=[];
        if(response.data.Other) {
	        for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	               obj.name=response.data.Other[key];
                  $scope.occupationList.push(obj.name);
	               
	        }
	      }
        return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });
	    
	  };*/

		$scope.newMemberDeathCoverDetails = deathCoverService.getDeathCover();
		$scope.newMemberTpdCoverDetails = tpdCoverService.getTpdCover();
		$scope.newMemberIpCoverDetails = ipCoverService.getIpCover();
		if ($scope.newMemberDeathCoverDetails.indexation == '1' && $scope.newMemberTpdCoverDetails.indexation == '1') {
			$scope.indexation.death = $scope.indexation.tpd = true;
			$("#newchangecvr-index-dth").parent().addClass('active');
			$("#newchangeCvr-indextpd-tpd").parent().addClass('active');
		} else {
			$scope.indexation.death = $scope.indexation.tpd = false;
			$("#newchangecvr-index-dth").parent().removeClass('active');
			$("#newchangeCvr-indextpd-tpd").parent().removeClass('active');
		}

		if ($scope.newMemberDeathCoverDetails
			&& $scope.newMemberDeathCoverDetails.type) {
			if ($scope.newMemberDeathCoverDetails.type == "1") {
				$scope.newDeathcoverType = "DcUnitised";
				showhide('nodollar1', 'dollar1');
				showhide('nodollar', 'dollar');
			} else if ($scope.newMemberDeathCoverDetails.type == "2") {
				$scope.newDeathcoverType = "DcFixed";
			}
		}

		if ($scope.newMemberTpdCoverDetails
			&& $scope.newMemberTpdCoverDetails.type) {
			if ($scope.newMemberTpdCoverDetails.type == "1") {
				$scope.newTpdCoverType = "TPDUnitised";
				showhide('nodollar1', 'dollar1');
				showhide('nodollar', 'dollar');
			} else if ($scope.newMemberTpdCoverDetails.type == "2") {
				$scope.newTpdCoverType = "TPDFixed";
			}
		}

		/*if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.type){
			 $scope.newIPRequireCover = $scope.newMemberIpCoverDetails.units;
		}*/

		if ($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.waitingPeriod && $scope.newMemberIpCoverDetails.waitingPeriod != '') {
			$scope.waitingPeriodAddnl = $scope.newMemberIpCoverDetails.waitingPeriod;
		} else {
			$scope.waitingPeriodAddnl = '30 Days';
		}

		if ($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.benefitPeriod && $scope.newMemberIpCoverDetails.benefitPeriod != '') {
			$scope.benefitPeriodAddnl = $scope.newMemberIpCoverDetails.benefitPeriod;
		} else {
			$scope.benefitPeriodAddnl = '2 Years';
		}

		$scope.setOccupationCategory = function () {
			if ($scope.newWithinOfficeQuestion == "Yes") {
				$scope.newDeathOccupationCategory = "Office";
				if ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes") {
					if (parseInt($scope.newAnnualSalary) > 100000) {
						$scope.newDeathOccupationCategory = "Professional";
					}
				} else if ($scope.newTertiaryQuestion == "No" && $scope.newManagementRole == "No") {
					$scope.newDeathOccupationCategory = "Office";
				}
			} else if ($scope.newWithinOfficeQuestion == "No") {
				$scope.newDeathOccupationCategory = "General";
			}

			if ($scope.newWithinOfficeQuestion == "Yes") {
				$scope.newTpdOccupationCategory = "Office";
				if ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes") {
					if (parseInt($scope.newAnnualSalary) > 100000) {
						$scope.newTpdOccupationCategory = "Professional";
					}
				} else if ($scope.newTertiaryQuestion == "No" && $scope.newManagementRole == "No") {
					$scope.newTpdOccupationCategory = "Office";
				}
			} else if ($scope.newWithinOfficeQuestion == "No") {
				$scope.newTpdOccupationCategory = "General";
			}

			if ($scope.newWithinOfficeQuestion == "Yes") {
				$scope.newIpOccupationCategory = "Office";
				if ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes") {
					if (parseInt($scope.newAnnualSalary) > 100000) {
						$scope.newIpOccupationCategory = "Professional";
					}
				} else if ($scope.newTertiaryQuestion == "No" && $scope.newManagementRole == "No") {
					$scope.newIpOccupationCategory = "Office";
				}
			} else if ($scope.newWithinOfficeQuestion == "No") {
				$scope.newIpOccupationCategory = "General";
			}
			$scope.checkIpCoverValue();
			$scope.deathTpdcheckboxState = $('#increaseCoverAmountLabel').hasClass('active');
			if ($scope.deathTpdcheckboxState) {
				$scope.increaseCoverAmount();
			} else if(newDynamicFlag || (!newDynamicFlag && ($scope.newDeathrequireCover != undefined && $scope.newDeathrequireCover != "") && ($scope.newTPDRequireCover != undefined && $scope.newTPDRequireCover != ""))) {
				if($scope.newDeathcoverType == "DcUnitised"){
					$scope.convertNewDeathUnitsToAmount();
				}
				if($scope.newTpdCoverType == "TPDUnitised"){
					$scope.convertNewTPDUnitsToAmount();
				}else{
					$scope.validateDeathTPDAmount();
				}
			}
			
			//	$scope.calculateNewQuoteOnChange();
		};
		$scope.newWaitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
		$scope.newBenefitPeriodOptions = ['2 Years', '5 Years'];
		$scope.dcNewCoverAmount = 0.00;
		$scope.dcNewWeeklyCost = 0.00;
		$scope.tpdNewCoverAmount = 0.00;
		$scope.tpdNewWeeklyCost = 0.00;
		$scope.ipNewCoverAmount = 0.00;
		$scope.ipNewWeeklyCost = 0.00;
		$scope.totalNewWeeklyCost = 0.00;
		var newDynamicFlag = false;
		var newDeathAmount, newTPDAmount;
		var fetchAppnum = true;
		var appNum;
		var deathCallFlag = true;
		var tpdCallFlag = true;
		var IPAmount;
		var inputDetails = persoanlDetailService.getMemberDetails();
		$scope.newPersonalDetails = inputDetails[0].personalDetails;

		if (inputDetails[0] && inputDetails[0].contactDetails.emailAddress) {
			$scope.newContactEmail = inputDetails[0].contactDetails.emailAddress;
		}
		if (inputDetails[0] && inputDetails[0].contactDetails.fundEmailAddress) {
			$scope.newContactfundEmail = inputDetails[0].contactDetails.fundEmailAddress;
		}
		if (inputDetails[0] && inputDetails[0].contactDetails.prefContactTime) {
			if (inputDetails[0].contactDetails.prefContactTime == "1") {
				$scope.newContactPrefTime = "Morning (9am - 12pm)";
			} else {
				$scope.newContactPrefTime = "Afternoon (12pm - 6pm)";
			}
		}
		if (inputDetails[0] && inputDetails[0].contactDetails.prefContact) {
			if (inputDetails[0].contactDetails.prefContact == "1") {
				$scope.preferredContactType = "Mobile";
				$scope.newContactPhone = inputDetails[0].contactDetails.mobilePhone;
			} else if (inputDetails[0].contactDetails.prefContact == "2") {
				$scope.preferredContactType = "Home";
				$scope.newContactPhone = inputDetails[0].contactDetails.homePhone;
			} else if (inputDetails[0].contactDetails.prefContact == "3") {
				$scope.preferredContactType = "Work";
				$scope.newContactPhone = inputDetails[0].contactDetails.workPhone;
			}
		}
		$scope.changePrefContactType = function () {
			if ($scope.preferredContactType == "Home") {
				$scope.newContactPhone = inputDetails[0].contactDetails.homePhone;
			} else if ($scope.preferredContactType == "Work") {
				$scope.newContactPhone = inputDetails[0].contactDetails.workPhone;
			} else if ($scope.preferredContactType == "Mobile") {
				$scope.newContactPhone = inputDetails[0].contactDetails.mobilePhone;
			}
		};
		var anb = parseInt(moment().diff(moment($scope.newPersonalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

		$scope.calculateQuote = function () {
			if ($scope.newDeathrequireCover == undefined || $scope.newDeathrequireCover == "") {
				if($scope.newDeathcoverType == "DcUnitised"){
					$scope.newDeathrequireCover = $scope.newMemberDeathCoverDetails.units;
				}
				if($scope.newDeathcoverType == "DcFixed"){
					$scope.newDeathrequireCover = $scope.newMemberDeathCoverDetails.amount;
				}
			}
			if ($scope.newTPDRequireCover == undefined || $scope.newTPDRequireCover == "") {
				if($scope.newTpdCoverType == "TPDUnitised"){
					$scope.newTPDRequireCover = $scope.newMemberTpdCoverDetails.units;
				}
				if($scope.newTpdCoverType == "TPDFixed"){
					$scope.newTPDRequireCover = $scope.newMemberTpdCoverDetails.amount;
				}
			}
			if ($scope.newIPRequireCover == undefined || $scope.newIPRequireCover == "") {
				$scope.newIPRequireCover = $scope.newMemberIpCoverDetails.units;
			}
			var ruleModel = {
				"age": anb,
				"fundCode": "CARE",
				"gender": $scope.gender,
				"deathOccCategory": $scope.newDeathOccupationCategory,
				"tpdOccCategory": $scope.newTpdOccupationCategory,
				"ipOccCategory": $scope.newIpOccupationCategory,
				"smoker": false,
				"deathUnits": parseInt($scope.newDeathrequireCover),
				"deathFixedAmount": parseInt($scope.newDeathrequireCover),
				"deathFixedCost": null,
				"deathUnitsCost": null,
				"tpdUnits": parseInt($scope.newTPDRequireCover),
				"tpdFixedAmount": parseInt($scope.newTPDRequireCover),
				"tpdFixedCost": null,
				"tpdUnitsCost": null,
				"ipUnits": parseInt($scope.newIPRequireCover),
				"ipFixedAmount": parseInt($scope.newIPRequireCover),
				"ipFixedCost": null,
				"ipUnitsCost": null,
				"premiumFrequency": "Weekly",
				"memberType": null,
				"manageType": "NCOVER",
				"deathCoverType": $scope.newDeathcoverType,
				"tpdCoverType": $scope.newTpdCoverType,
				"ipCoverType": "IpUnitised",
				"ipWaitingPeriod": $scope.waitingPeriodAddnl,
				"ipBenefitPeriod": $scope.benefitPeriodAddnl,
				"deathExistingAmount":parseInt($scope.newMemberDeathCoverDetails.amount || 0),
                "tpdExistingAmount":parseInt($scope.newMemberTpdCoverDetails.amount || 0)
			};
			CalculateService.calculate(ruleModel, $scope.urlList.calculateUrl).then(function (res) {
				//CalculateService.calculate({}, ruleModel, function(res){
				$timeout(function() {
					var newPremium = res.data;
					newDynamicFlag = true;
					for (var i = 0; i < newPremium.length; i++) {
						if (newPremium[i].coverType == 'DcFixed' || newPremium[i].coverType == 'DcUnitised') {
							$scope.dcNewCoverAmount = newPremium[i].coverAmount;
							$scope.dcNewWeeklyCost = newPremium[i].cost;
						} else if (newPremium[i].coverType == 'TPDFixed' || newPremium[i].coverType == 'TPDUnitised') {
							$scope.tpdNewCoverAmount = newPremium[i].coverAmount;
							$scope.tpdNewWeeklyCost = newPremium[i].cost;
						} else if (newPremium[i].coverType == 'IpFixed' || newPremium[i].coverType == 'IpUnitised') {
							$scope.ipNewCoverAmount = newPremium[i].coverAmount;
							$scope.ipNewWeeklyCost = newPremium[i].cost;
						}
					}
					$scope.totalNewWeeklyCost = $scope.dcNewWeeklyCost + $scope.tpdNewWeeklyCost + $scope.ipNewWeeklyCost;
					if (fetchAppnum) {
						fetchAppnum = false;
						appNum = PersistenceService.getAppNumber();
					}
					$scope.customDigest();
				});		
			}, function (err) {
				console.log("Error calculating new member premium " + JSON.stringify(err));
			});
		};

		$scope.coverAmountFlag = false;
		$scope.coverAmountErrorMsg = "";
		$scope.deathWarningFlag = false;
		$scope.deathErrorFlag = false;
		$scope.tpdErrorFlag = false;
		$scope.deathErrorMsg = "";
		$scope.tpdErrorMsg = "";
		$scope.tpdLimitMsg = "";
		$scope.tpdWarningFlag = false;
		$scope.validateDeathTPDAmount = function () {
			// Death max validation
			if ($scope.newDeathcoverType == "DcFixed") {
				var deathAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
				if (parseInt($scope.newDeathrequireCover) < parseInt($scope.newMemberDeathCoverDetails.amount)) {
					$scope.deathWarningFlag = false;
					$scope.deathLimitMsg = "";
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
				} else if ((parseInt($scope.newDeathrequireCover) > deathAllowableLimit) && (deathAllowableLimit < 750000)) {
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = true;
					$scope.newDeathrequireCover = deathAllowableLimit;
					$scope.deathLimitMsg = "Death amount cannot be greter than 7 times your annual salary";
				} else if ((parseInt($scope.newDeathrequireCover) > 750000) && (deathAllowableLimit > 750000)) {
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = true;
					$scope.newDeathrequireCover = 750000;
					$scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
				} else {
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = false;
					$scope.deathLimitMsg = "";
				}
			} else if ($scope.newDeathcoverType == "DcUnitised") {
				var deathAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
				if ((parseInt($scope.newDeathrequireCover) * newDeathAmount) < parseInt($scope.newMemberDeathCoverDetails.amount)) {
					$scope.deathWarningFlag = false;
					$scope.deathLimitMsg = "";
					$scope.deathErrorFlag = true;
					$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
				} else if ((parseInt($scope.newDeathrequireCover) * newDeathAmount > deathAllowableLimit) && (deathAllowableLimit < 750000)) {
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = true;
					$scope.newDeathrequireCover = Math.ceil(deathAllowableLimit / newDeathAmount);
					$scope.deathLimitMsg = "Death amount cannot be greter than 7 times your annual salary";
				} else if ((parseInt($scope.newDeathrequireCover) * newDeathAmount > 750000) && (deathAllowableLimit > 750000)) {
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = true;
					$scope.newDeathrequireCover = Math.ceil(750000 / newDeathAmount);
					$scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
				} else {
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = false;
					$scope.deathLimitMsg = "";
				}
			}

			// TPD max validation
			if ($scope.newTpdCoverType == "TPDFixed") {
				var tpdAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
				if ($scope.newTPDRequireCover != null && ($scope.newDeathrequireCover == null || $scope.newDeathrequireCover == "")) {
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.coverAmountFlag = true;
					$scope.coverAmountErrorMsg = "You cannot apply for TPD cover without Death Cover.";
				} else if (parseInt($scope.newTPDRequireCover) < parseInt($scope.newMemberTpdCoverDetails.amount)) {
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
					$scope.tpdWarningFlag = false;
					$scope.tpdLimitMsg = "";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
				} else if ((parseInt($scope.newTPDRequireCover) > tpdAllowableLimit) && (tpdAllowableLimit < 750000)) {
					if(anb >30 && parseInt($scope.newDeathrequireCover) < parseInt($scope.newTPDRequireCover)){
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
						$scope.coverAmountFlag = true;
						$scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
					}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = true;
					$scope.newTPDRequireCover = tpdAllowableLimit;
					$scope.tpdLimitMsg = "TPD amount cannot be greter than 7 times your annual salary";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
					}
				} else if ((parseInt($scope.newTPDRequireCover) > 750000) && (tpdAllowableLimit > 750000)) {
					if(anb >30 && parseInt($scope.newDeathrequireCover) < parseInt($scope.newTPDRequireCover)){
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
						$scope.coverAmountFlag = true;
						$scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
					}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = true;
					$scope.newTPDRequireCover = 750000;
					$scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
					}
				} else if (parseInt($scope.newDeathrequireCover) < parseInt($scope.newTPDRequireCover)) {
					if (anb < 30 && (parseInt($scope.newDeathrequireCover) < (parseInt($scope.newMemberDeathCoverDetails.amount) + (4 * newDeathAmount)))) {
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
						$scope.coverAmountFlag = false;
						$scope.coverAmountErrorMsg = "";
					} else {
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
						$scope.coverAmountFlag = true;
						$scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
					}
				} else {
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = false;
					$scope.tpdErrorFlag = false;
					$scope.tpdLimitMsg = "";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
				}

			} else if ($scope.newTpdCoverType == "TPDUnitised") {
				var tpdAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
				if ($scope.newTPDRequireCover != null && ($scope.newDeathrequireCover == null || $scope.newDeathrequireCover == "")) {
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.coverAmountFlag = true;
					$scope.coverAmountErrorMsg = "You cannot apply for TPD cover without Death Cover.";
				} else if ((parseInt($scope.newTPDRequireCover) * newTPDAmount) < parseInt($scope.newMemberTpdCoverDetails.amount)) {
					$scope.tpdErrorFlag = true;
					$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
					$scope.tpdWarningFlag = false;
					$scope.tpdLimitMsg = "";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
				} else if ((parseInt($scope.newTPDRequireCover) * newTPDAmount > tpdAllowableLimit) && (tpdAllowableLimit < 750000)) {
					if(anb >30 && parseInt($scope.newDeathrequireCover) < parseInt($scope.newTPDRequireCover)){
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
						$scope.coverAmountFlag = true;
						$scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
					}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = true;
					$scope.newTPDRequireCover = Math.ceil(tpdAllowableLimit / newTPDAmount);
					$scope.tpdLimitMsg = "TPD amount cannot be greter than 7 times your annual salary";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
					}
				} else if ((parseInt($scope.newTPDRequireCover) * newTPDAmount > 750000) && (tpdAllowableLimit > 750000)) {
					if (anb > 30 && parseInt($scope.newDeathrequireCover) < parseInt($scope.newTPDRequireCover)){
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.coverAmountFlag = true;
						$scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
					}else{
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = true;
					$scope.newTPDRequireCover = Math.ceil(750000 / newTPDAmount);
					$scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
					}
				} else if (parseInt($scope.newDeathrequireCover) < parseInt($scope.newTPDRequireCover)) {
					if (anb < 30 && ((parseInt($scope.newDeathrequireCover) * newDeathAmount) < (parseInt($scope.newMemberDeathCoverDetails.amount) + (4 * newDeathAmount)))) {
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.coverAmountFlag = false;
						$scope.coverAmountErrorMsg = "";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
					} else {
						$scope.tpdErrorFlag = false;
						$scope.tpdErrorMsg = "";
						$scope.coverAmountFlag = true;
						$scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
						$scope.tpdWarningFlag = false;
						$scope.tpdLimitMsg = "";
					}
				} else {
					$scope.tpdErrorFlag = false;
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = false;
					$scope.tpdLimitMsg = "";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
				}

			}
			$scope.calculateNewQuoteOnChange();
		};

		$scope.calculateNewQuoteOnChange = function () {
			if (newDynamicFlag && !$scope.coverAmountFlag && !$scope.deathErrorFlag && !$scope.tpdErrorFlag) {
				$scope.calculateQuote();
			}
		};
		$scope.isNewDeathcoverTypeDisabled = false;
		$scope.isNewDeathrequireCoverDisabled = false;
		$scope.isNewTpdCoverTypeDisabled = false;
		$scope.isNewTPDRequireCoverDisabled = false;
		$scope.deathTpdcheckboxState = false;
		$scope.increaseCoverAmount = function () {
			$timeout(function () {
				$scope.deathTpdcheckboxState = $('#increaseCoverAmountLabel').hasClass('active');
				if ($scope.deathTpdcheckboxState == true) {
					$scope.isNewDeathcoverTypeDisabled = true;
					$scope.isDeathIndexDisabled = true;
					$scope.isNewDeathrequireCoverDisabled = true;
					$scope.isNewTpdCoverTypeDisabled = true;
					$scope.isTPDIndexDisabled = true;
					$scope.isNewTPDRequireCoverDisabled = true;
					$scope.deathErrorFlag = false;
					$scope.deathErrorMsg = "";
					$scope.deathWarningFlag = false;
					$scope.deathLimitMsg = "";
					$scope.tpdErrorMsg = "";
					$scope.tpdWarningFlag = false;
					$scope.tpdErrorFlag = false;
					$scope.tpdLimitMsg = "";
					$scope.coverAmountFlag = false;
					$scope.coverAmountErrorMsg = "";
					//$scope.newDeathcoverType = "DcFixed";
					//$scope.newTpdCoverType = "TPDFixed";
					if ($scope.newDeathcoverType == "DcFixed" && $scope.newTpdCoverType == "TPDFixed") {
						$scope.newDeathrequireCover = 7 * ($scope.newAnnualSalary);
						$scope.newTPDRequireCover = 7 * ($scope.newAnnualSalary);
						if (parseInt($scope.newDeathrequireCover) > 750000) {
							$scope.newDeathrequireCover = 750000;
							$scope.deathWarningFlag = true;
							$scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
						}
						if (parseInt($scope.newTPDRequireCover) > 750000) {
							$scope.newTPDRequireCover = 750000;
							$scope.tpdWarningFlag = true;
							$scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
						}
					} else if ($scope.newDeathcoverType == "DcUnitised" && $scope.newTpdCoverType == "TPDUnitised") {
						var sevenTimesSalary = 7 * ($scope.newAnnualSalary);
						var deathReqObject = {
							"age": anb,
							"fundCode": "CARE",
							"gender": $scope.gender,
							"deathOccCategory": $scope.newDeathOccupationCategory,
							"smoker": false,
							"deathUnits": 1,
							"deathUnitsCost": null,
							"premiumFrequency": "Weekly",
							"memberType": null,
							"manageType": "NCOVER",
							"deathCoverType": "DcUnitised"
						};
						var TPDReqObject = {
							"age": anb,
							"fundCode": "CARE",
							"gender": $scope.gender,
							"tpdOccCategory": $scope.newTpdOccupationCategory,
							"smoker": false,
							"tpdUnits": 1,
							"tpdUnitsCost": null,
							"premiumFrequency": "Weekly",
							"memberType": null,
							"manageType": "NCOVER",
							"tpdCoverType": "TPDUnitised",
						};

						CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl, deathReqObject).then(function (res) {
							var unitDeathAmount = res.data[0].coverAmount;
							$scope.deathErrorFlag = false;
							$scope.deathErrorMsg = "";
							$scope.deathWarningFlag = false;
							$scope.deathLimitMsg = "";
							$scope.tpdErrorMsg = "";
							$scope.tpdWarningFlag = false;
							$scope.tpdErrorFlag = false;
							$scope.tpdLimitMsg = "";
							$scope.coverAmountFlag = false;
							$scope.coverAmountErrorMsg = "";
							$scope.newDeathrequireCover = Math.ceil(sevenTimesSalary / unitDeathAmount);
							$scope.dcNewCoverAmount = $scope.newDeathrequireCover * unitDeathAmount;
							if (sevenTimesSalary > 750000) {
								$scope.newDeathrequireCover = Math.ceil(750000 / unitDeathAmount);
								$scope.dcNewCoverAmount = $scope.newDeathrequireCover * unitDeathAmount;
								$scope.deathWarningFlag = true;
								$scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
							}
							$scope.customDigest();
						}, function (err) {
							console.log('Error while fetching death amount ' + err);
						});

						CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl, TPDReqObject).then(function (res) {
							var unitTpdAmount = res.data[0].coverAmount;
							$scope.newTPDRequireCover = Math.ceil(sevenTimesSalary / unitTpdAmount);
							$scope.tpdNewCoverAmount = $scope.newTPDRequireCover * unitTpdAmount;
							if (sevenTimesSalary > 750000) {
								$scope.newTPDRequireCover = Math.ceil(750000 / unitTpdAmount);
								$scope.tpdNewCoverAmount = $scope.newTPDRequireCover * unitTpdAmount;
								$scope.tpdWarningFlag = true;
								$scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
							}
							$scope.customDigest();
						}, function (err) {
							console.log('Error while fetching TPD amount ' + err);
						});
					}
				} else if ($scope.deathTpdcheckboxState == false) {
					$scope.isNewDeathcoverTypeDisabled = false;
					$scope.isNewDeathrequireCoverDisabled = false;
					$scope.isNewTpdCoverTypeDisabled = false;
					$scope.isNewTPDRequireCoverDisabled = false;
					$scope.deathWarningFlag = false;
					$scope.tpdWarningFlag = false;
					$scope.newDeathrequireCover = '';
					$scope.newTPDRequireCover = '';
					$scope.deathLimitMsg = '';
					$scope.tpdLimitMsg = '';
				}

			});
		};

		$scope.isUnitEmpty = function (value) {
			return ((value == "" || value == null) || value == "0");
		};


		$scope.IPWarningFlag = false;

		$scope.validateIPUnitOnChange = function () {
			//$scope.newIpOccupationCategory = newOccupationCategory[0].ipfixedcategeory;
			var ipMaxLimit;
			//var newCat = newOccupationCategory[0].ipfixedcategeory;
			var ipMaxLimitDependOcc;
			if ($scope.newIpOccupationCategory == "Professional") {
				ipMaxLimitDependOcc = 24;
			} else if ($scope.newIpOccupationCategory == "Office") {
				ipMaxLimitDependOcc = 17;
			} else if ($scope.newIpOccupationCategory == "General") {
				ipMaxLimitDependOcc = 12;
			}

			// Checking for IP max
			if (IPAmount < ipMaxLimitDependOcc) {
				ipMaxLimit = IPAmount;
			} else {
				ipMaxLimit = ipMaxLimitDependOcc;
			}


			if (parseInt($scope.newIPRequireCover) > ipMaxLimit) {
				$scope.newIPRequireCover = ipMaxLimit;
				$scope.IPWarningFlag = true;
			} else {
				$scope.newIPRequireCover = $scope.newIPRequireCover;
			}


			$scope.calculateNewQuoteOnChange();
		};

		/*$scope.changeIPValue = function(){
			
				$scope.IPWarningFlag = false;
				$timeout(function(){
					ipCheckboxState = $('#ipsalarycheck').prop('checked');
					if(ipCheckboxState == true){
							if(parseInt($scope.newAnnualSalary) < 423530){
								IPAmount  = Math.ceil((0.85 * ($scope.newAnnualSalary/12)) / 425);
								$scope.validateIPUnit();
							} else if(parseInt($scope.newAnnualSalary) > 423530 && parseInt($scope.newAnnualSalary) < 623530){
								IPAmount = Math.ceil((Math.round(((parseInt($scope.newAnnualSalary) - 423530)/12)*0.6) + 30000)/425);
								$scope.validateIPUnit();
							} else if(parseInt($scope.newAnnualSalary) > 623530){
								IPAmount = 95;
								$scope.validateIPUnit();
							}
						//	$scope.validateIPUnit();
							$scope.checkIPCover();
					} else if(ipCheckboxState == false){
						$scope.newIPRequireCover = '';
						$scope.IPWarningFlag = false;
					}
				
				}, 10);
			};*/

		$scope.checkIpCoverValue = function () {
			$scope.IPWarningFlag = false;
			if (parseInt($scope.newAnnualSalary) < 423530) {
				IPAmount = Math.ceil((0.85 * ($scope.newAnnualSalary / 12)) / 425);
				$scope.validateIPUnitOnChange();
			} else if (parseInt($scope.newAnnualSalary) > 423530 && parseInt($scope.newAnnualSalary) < 623530) {
				IPAmount = Math.ceil((Math.round(((parseInt($scope.newAnnualSalary) - 423530) / 12) * 0.6) + 30000) / 425);
				$scope.validateIPUnitOnChange();
			} else if (parseInt($scope.newAnnualSalary) > 623530) {
				IPAmount = 95;
				$scope.validateIPUnitOnChange();
			}
			//	$scope.calculateNewQuoteOnChange();
		};


		// Progressive validation
		var newMemberContactFormFields = ['newContactEmail', 'newContactPhone', 'newContactPrefTime'];

		while ($scope.newPersonalDetails) {
			if ($scope.newPersonalDetails.gender == null || $scope.newPersonalDetails.gender == "") {
				$scope.genderFlag = false;
				$scope.gender = '';
				var newMemberOccupationFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry', 'newOccupation', 'newWithinOfficeQuestion', 'newTertiaryQuestion', 'newManagementRole', 'gender', 'newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
				var newMemberOccupationOtherFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry', 'newOccupation', 'newOtherOccupation', 'newWithinOfficeQuestion', 'newTertiaryQuestion', 'newManagementRole', 'gender', 'newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
			} else {
				$scope.genderFlag = true;
				$scope.gender = $scope.newPersonalDetails.gender;
				var newMemberOccupationFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry', 'newOccupation', 'newWithinOfficeQuestion', 'newTertiaryQuestion', 'newManagementRole', 'newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
				var newMemberOccupationOtherFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry', 'newOccupation', 'newOtherOccupation', 'newWithinOfficeQuestion', 'newTertiaryQuestion', 'newManagementRole', 'newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
			}
			break;
		}
		/* var newMemberOccupationFormFields = ['workTimeAndSalary','areyouperCitzNewMemberQuestion','newIndustry','newOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary','tpdClaim','terminalIllnessClaim'];
		 var newMemberOccupationOtherFormFields = ['workTimeAndSalary','areyouperCitzNewMemberQuestion','newIndustry','newOccupation','newOtherOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary','tpdClaim','terminalIllnessClaim'];*/
		var newMembercoverCalculatorFormFields = ['newDeathcoverType', 'newDeathrequireCover', 'newTpdCoverType', 'newTPDRequireCover'];

		$scope.checkNewMemberPreviousMandatoryFields = function (elementName, formName) {
			var newFormFields;
			if (formName == 'newMemberContactForm') {
				newFormFields = newMemberContactFormFields;
			} else if (formName == 'newMemberOccupationForm') {
				if ($scope.newOccupation != undefined && $scope.newOccupation == 'Other') {
					newFormFields = newMemberOccupationOtherFormFields;
				} else {
					newFormFields = newMemberOccupationFormFields;
				}
			} else if (formName == 'newMembercoverCalculatorForm') {
				newFormFields = newMembercoverCalculatorFormFields;
			}
			var inx = newFormFields.indexOf(elementName);
			if (inx > 0) {
				for (var i = 0; i < inx; i++) {
					$scope[formName][newFormFields[i]].$touched = true;
				}
			}
		};

		$scope.convertNewDeathUnitsToAmount = function () {
			if ($scope.newDeathcoverType == "DcUnitised" && (parseInt($scope.newDeathrequireCover) >= parseInt($scope.newMemberDeathCoverDetails.units))) {
				var deathReqObject = {
					"age": anb,
					"fundCode": "CARE",
					"gender": $scope.gender,
					"deathOccCategory": $scope.newDeathOccupationCategory,
					"smoker": false,
					"deathUnits": 1,
					"deathUnitsCost": null,
					"premiumFrequency": "Weekly",
					"memberType": null,
					"manageType": "NCOVER",
					"deathCoverType": "DcUnitised"
				};
				if (deathCallFlag) {
					CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl, deathReqObject).then(function (res) {
						//CalculateDeathService.calculateAmount({}, deathReqObject, function(res){
						deathCallFlag = false;
						newDeathAmount = res.data[0].coverAmount;
						$scope.validateDeathTPDAmount();
					}, function (err) {
						console.log('Error while fetching death amount ' + err);
					});
				} else {
					$scope.validateDeathTPDAmount();
				}
			} else if ($scope.newDeathcoverType == "DcFixed") {
				$scope.validateDeathTPDAmount();
			} else {
				$scope.deathWarningFlag = false;
				$scope.deathLimitMsg = "";
				$scope.deathErrorFlag = true;
				$scope.deathErrorMsg = "You cannot reduce your Death Only cover. Please re-enter your cover amount.";
			}

		};

		$scope.convertNewTPDUnitsToAmount = function () {
			if ($scope.newTpdCoverType == "TPDUnitised" && (parseInt($scope.newTPDRequireCover)) >= parseInt($scope.newMemberTpdCoverDetails.units)) {
				var TPDReqObject = {
					"age": anb,
					"fundCode": "CARE",
					"gender": $scope.gender,
					"tpdOccCategory": $scope.newTpdOccupationCategory,
					"smoker": false,
					"tpdUnits": 1,
					"tpdUnitsCost": null,
					"premiumFrequency": "Weekly",
					"memberType": null,
					"manageType": "NCOVER",
					"tpdCoverType": "TPDUnitised",
				};
				if (tpdCallFlag) {
					CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl, TPDReqObject).then(function (res) {
						//CalculateTPDService.calculateAmount({}, TPDReqObject, function(res){
						tpdCallFlag = false;
						newTPDAmount = res.data[0].coverAmount;
						$scope.validateDeathTPDAmount();
					}, function (err) {
						console.log('Error while fetching TPD amount ' + err);
					});
				} else {
					$scope.validateDeathTPDAmount();
				}
			} else if ($scope.newTpdCoverType == "TPDFixed") {
				$scope.validateDeathTPDAmount();
			} else {
				$scope.tpdErrorFlag = true;
				$scope.tpdErrorMsg = "You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
				$scope.tpdWarningFlag = false;
				$scope.tpdLimitMsg = "";
				$scope.coverAmountFlag = false;
				$scope.coverAmountErrorMsg = "";
			}
		};
		//temp to test aura integration
		$scope.gotoAura = function () {
			$location.path('/auraspecialoffer/1');
		};
		// validate fields "on continue"
		$scope.newMemberFormSubmit = function (form) {
			if (!form.$valid) {
				form.$submitted = true;
				if (form.$name == 'newMemberContactForm') {
					$scope.toggleTwo(false);
					$scope.toggleThree(false);
				} else if (form.$name == 'newMemberOccupationForm') {
					$scope.toggleThree(false);
				}
			} else {
				if (form.$name == 'newMemberContactForm') {
					$scope.toggleTwo(true);
				} else if (form.$name == 'newMemberOccupationForm') {
					/*if (!$scope.eligibilityViolated) {*/
						$scope.toggleThree(true);
					/*}*/
				} else if (form.$name == 'newMembercoverCalculatorForm') {
					if (!$scope.coverAmountFlag && !$scope.deathErrorFlag && !$scope.tpdErrorFlag) {
						$scope.calculateQuote();
					}
				}
			}
		};

		$scope.newAckFlag = false;
		var newAckCheck;

		$scope.checkNewAckState = function () {
			$timeout(function () {
				newAckCheck = $('#newTermsLabel').hasClass('active');
				if (newAckCheck) {
					$scope.newAckFlag = false;
				} else {
					$scope.newAckFlag = true;
				}
			}, 10);
		};


		$scope.saveNewDataForPersistence = function () {
			var newCoverObj = {};
			var newCoverStateObj = {};
			var newOccCoverObj = {};
			var newDeathAddnlObj = {};
			var newTpdAddnlObj = {};
			var newIpAddnlObj = {};

			var selectedIndustry = $scope.IndustryOptions.filter(function (obj) {
				return $scope.newIndustry == obj.key;
			});

			newCoverObj['name'] = $scope.newPersonalDetails.firstName + " " + $scope.newPersonalDetails.lastName;
			newCoverObj['dob'] = $scope.newPersonalDetails.dateOfBirth;
			newCoverObj['email'] = $scope.newContactEmail;
			newOccCoverObj['gender'] = $scope.gender;
			newCoverObj['contactType'] = $scope.preferredContactType;
			newCoverObj['contactPhone'] = $scope.newContactPhone;
			newCoverObj['contactPrefTime'] = $scope.newContactPrefTime;

			newOccCoverObj['fifteenHr'] = $scope.workTimeAndSalary;
			/*newOccCoverObj['citizenQue'] = $scope.areyouperCitzNewMemberQuestion;*/
			newOccCoverObj['industryName'] = selectedIndustry[0].value;
			newOccCoverObj['industryCode'] = selectedIndustry[0].key;
			newOccCoverObj['occupation'] = $scope.newOccupation;
			newOccCoverObj['withinOfficeQue'] = $scope.newWithinOfficeQuestion;
			newOccCoverObj['tertiaryQue'] = $scope.newTertiaryQuestion;
			newOccCoverObj['managementRoleQue'] = $scope.newManagementRole;
			newOccCoverObj['salary'] = $scope.newAnnualSalary;
			newOccCoverObj['otherOccupation'] = $scope.otherOccupationObj.newOtherOccupation;

			/*newCoverObj['previousTpdClaimQue'] = $scope.tpdClaim;
			newCoverObj['terminalIllClaimQue'] = $scope.terminalIllnessClaim;*/

			newCoverObj['newExistingDeathAmt'] = $scope.newMemberDeathCoverDetails.amount;
			newCoverObj['newExistingDeathUnits'] = $scope.newMemberDeathCoverDetails.units;
			newCoverObj['deathOccCategory'] = $scope.newDeathOccupationCategory;
			newCoverObj['tpdOccCategory'] = $scope.newTpdOccupationCategory;
			newCoverObj['newExistingIPUnits'] = $scope.newMemberIpCoverDetails.units;
			newCoverObj['ipOccCategory'] = $scope.newIpOccupationCategory;
			newCoverObj['newExistingTpdAmt'] = $scope.newMemberTpdCoverDetails.amount;
			newCoverObj['newExistingTPDUnits'] = $scope.newMemberTpdCoverDetails.units;
			//newCoverObj['ipcheckbox'] = ipCheckboxState;

			newDeathAddnlObj['newDeathCoverType'] = $scope.newDeathcoverType;
			newDeathAddnlObj['newDeathLabelAmt'] = $scope.dcNewCoverAmount;
			newDeathAddnlObj['newDeathUpdatedCover'] = $scope.newDeathrequireCover;
			newDeathAddnlObj['newDeathCoverPremium'] = $scope.dcNewWeeklyCost;

			newTpdAddnlObj['newTpdCoverType'] = $scope.newTpdCoverType;
			newTpdAddnlObj['newTpdLabelAmt'] = $scope.tpdNewCoverAmount;
			newTpdAddnlObj['newTpdUpdatedCover'] = $scope.newTPDRequireCover;
			newTpdAddnlObj['newTpdCoverPremium'] = $scope.tpdNewWeeklyCost;

			newIpAddnlObj['newWaitingPeriod'] = $scope.waitingPeriodAddnl;
			newIpAddnlObj['newBenefitPeriod'] = $scope.benefitPeriodAddnl;
			newIpAddnlObj['newIpLabelAmt'] = $scope.ipNewCoverAmount;
			newIpAddnlObj['newIpUpdatedCover'] = $scope.newIPRequireCover;
			newIpAddnlObj['newIpCoverPremium'] = $scope.ipNewWeeklyCost;
			newIpAddnlObj['newIpCoverType'] = 'IpUnitised';

			newCoverObj['totalPremium'] = $scope.totalNewWeeklyCost;

			newCoverObj['newDeathTpdcheckboxState'] = $scope.deathTpdcheckboxState;
			//newCoverObj['newOccupationCategory'] = newOccupationCategory;
			newCoverObj['appNum'] = appNum;
			newCoverObj['newAckCheck'] = newAckCheck;
			newCoverObj['dodCheck'] = $('#dodLabel').hasClass('active');
			newCoverObj['privacyCheck'] = $('#privacyLabel').hasClass('active');
			newCoverObj['age'] = anb;
			newCoverObj['manageType'] = 'NCOVER';
			newCoverObj['partnerCode'] = 'CARE';
			newCoverObj['freqCostType'] = 'Weekly';

			newCoverObj['lastSavedOn'] = 'NewMemberQuotepage';

			newCoverStateObj['isNewDeathcoverTypeDisabled'] = $scope.isNewDeathcoverTypeDisabled;
			newCoverStateObj['isNewDeathrequireCoverDisabled'] = $scope.isNewDeathrequireCoverDisabled;
			newCoverStateObj['isNewTpdCoverTypeDisabled'] = $scope.isNewTpdCoverTypeDisabled;
			newCoverStateObj['isNewTPDRequireCoverDisabled'] = $scope.isNewTPDRequireCoverDisabled;
			newCoverObj['indexationDeath'] = $scope.indexation.death;
			newCoverObj['indexationTpd'] = $scope.indexation.tpd;
			newCoverStateObj['newDynamicFlag'] = newDynamicFlag;
			/*newCoverStateObj['expandOrCollapseFlag'] = expandOrCollapseFlag;*/
			newCoverStateObj['isDeathDisabled'] = $scope.isDeathDisabled;
			newCoverStateObj['isTPDDisabled'] = $scope.isTPDDisabled;
			newCoverStateObj['isIPDisabled'] = $scope.isIPDisabled;


			PersistenceService.setNewMemberCoverDetails(newCoverObj);
			PersistenceService.setNewMemberCoverStateDetails(newCoverStateObj);
			PersistenceService.setNewMemberCoverOccDetails(newOccCoverObj);
			PersistenceService.setNewMemberDeathAddnlDetails(newDeathAddnlObj);
			PersistenceService.setNewMemberTpdAddnlDetails(newTpdAddnlObj);
			PersistenceService.setNewMemberIpAddnlDetails(newIpAddnlObj);

		};

		$scope.goToAuraPage = function () {
			if (this.newMemberContactForm.$valid && this.newMemberOccupationForm.$valid && this.newMembercoverCalculatorForm.$valid /*&& !$scope.eligibilityViolated*/
				&& !$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.coverAmountFlag) {
				$rootScope.$broadcast('disablepointer');
				$timeout(function () {
					newAckCheck = $('#newTermsLabel').hasClass('active');
					if (newAckCheck) {
						$scope.saveNewDataForPersistence();
						$scope.newAckFlag = false;
						$scope.gotoAura();
					} else {
						$scope.newAckFlag = true;
					}
				}, 10);
			}
		};

		if ($routeParams.mode == 2) {
			var newExistingDetails = PersistenceService.getNewMemberCoverDetails();
			var newStateDetails = PersistenceService.getNewMemberCoverStateDetails();
			var newOccDetails = PersistenceService.getNewMemberCoverOccDetails();
			var deathAddnlDetails = PersistenceService.getNewMemberDeathAddnlDetails();
			var tpdAddnlDetails = PersistenceService.getNewMemberTpdAddnlDetails();
			var ipAddnlDetails = PersistenceService.getNewMemberIpAddnlDetails();

			if (!newExistingDetails || !newStateDetails || !newOccDetails || !deathAddnlDetails || !tpdAddnlDetails || !ipAddnlDetails) {
				$location.path("/newmemberquote/1");
				return false;
			}
			$scope.newContactEmail = newExistingDetails.email;
			$scope.preferredContactType = newExistingDetails.contactType;
			$scope.newContactPhone = newExistingDetails.contactPhone;
			$scope.newContactPrefTime = newExistingDetails.contactPrefTime;

			$scope.workTimeAndSalary = newOccDetails.fifteenHr;
			/*$scope.areyouperCitzNewMemberQuestion = newOccDetails.citizenQue;*/
			$scope.newIndustry = newOccDetails.industryCode;
			//$scope.occupation = newExistingDetails.occupation;
			$scope.newWithinOfficeQuestion = newOccDetails.withinOfficeQue;
			$scope.newTertiaryQuestion = newOccDetails.tertiaryQue;
			$scope.newManagementRole = newOccDetails.managementRoleQue;
			$scope.gender = newOccDetails.gender;
			$scope.newAnnualSalary = newOccDetails.salary;
			$scope.otherOccupationObj.newOtherOccupation = newOccDetails.otherOccupation;

			/*$scope.tpdClaim = newExistingDetails.previousTpdClaimQue;
			$scope.terminalIllnessClaim = newExistingDetails.terminalIllClaimQue;*/

			$scope.newMemberDeathCoverDetails.amount = newExistingDetails.newExistingDeathAmt;
			$scope.newMemberDeathCoverDetails.units = newExistingDetails.newExistingDeathUnits;
			$scope.newMemberTpdCoverDetails.amount = newExistingDetails.newExistingTpdAmt;
			$scope.newMemberTpdCoverDetails.units = newExistingDetails.newExistingTPDUnits;
			$scope.newMemberIpCoverDetails.units = newExistingDetails.newExistingIPUnits;
			$scope.newIpOccupationCategory = newExistingDetails.ipOccCategory;
			$scope.newDeathOccupationCategory = newExistingDetails.deathOccCategory;
			$scope.newTpdOccupationCategory = newExistingDetails.tpdOccCategory;

			//ipCheckboxState = newExistingDetails.ipcheckbox;
			$scope.newDeathcoverType = deathAddnlDetails.newDeathCoverType;
			$scope.dcNewCoverAmount = deathAddnlDetails.newDeathLabelAmt;
			$scope.newDeathrequireCover = deathAddnlDetails.newDeathUpdatedCover;
			$scope.dcNewWeeklyCost = deathAddnlDetails.newDeathCoverPremium;

			$scope.newTpdCoverType = tpdAddnlDetails.newTpdCoverType;
			$scope.tpdNewCoverAmount = tpdAddnlDetails.newTpdLabelAmt;
			$scope.newTPDRequireCover = tpdAddnlDetails.newTpdUpdatedCover;
			$scope.tpdNewWeeklyCost = tpdAddnlDetails.newTpdCoverPremium;

			$scope.waitingPeriodAddnl = ipAddnlDetails.newWaitingPeriod;
			$scope.benefitPeriodAddnl = ipAddnlDetails.newBenefitPeriod;
			$scope.ipNewCoverAmount = ipAddnlDetails.newIpLabelAmt;
			$scope.newIPRequireCover = ipAddnlDetails.newIpUpdatedCover;
			$scope.ipNewWeeklyCost = ipAddnlDetails.newIpCoverPremium;

			$scope.totalNewWeeklyCost = newExistingDetails.totalPremium;
			//newOccupationCategory = newExistingDetails.newOccupationCategory;
			appNum = newExistingDetails.appNum;
			newAckCheck = newExistingDetails.newAckCheck;
			dodCheck = newExistingDetails.dodCheck;
			privacyCheck = newExistingDetails.privacyCheck;
			$scope.deathTpdcheckboxState = newExistingDetails.newDeathTpdcheckboxState;


			$scope.isNewDeathcoverTypeDisabled = newStateDetails.isNewDeathcoverTypeDisabled;
			$scope.isNewDeathrequireCoverDisabled = newStateDetails.isNewDeathrequireCoverDisabled;
			$scope.isNewTpdCoverTypeDisabled = newStateDetails.isNewTpdCoverTypeDisabled;
			$scope.isNewTPDRequireCoverDisabled = newStateDetails.isNewTPDRequireCoverDisabled;
			$scope.indexation.death = newExistingDetails.indexationDeath;
			$scope.indexation.tpd = newExistingDetails.indexationTpd;
			/*expandOrCollapseFlag = newStateDetails.expandOrCollapseFlag;*/
			newDynamicFlag = newStateDetails.newDynamicFlag;
			$scope.isDeathDisabled = newStateDetails.isDeathDisabled;
			$scope.isTPDDisabled = newStateDetails.isTPDDisabled;
			$scope.isIPDisabled = newStateDetails.isIPDisabled;

			/*if($scope.terminalIllnessClaim == 'No' && $scope.tpdClaim == "Yes"){
				expandOrCollapseFlag = true;	
						$scope.toggleThreeDeath();
			} else if($scope.terminalIllnessClaim == 'No' && $scope.tpdClaim == "No"){
				expandOrCollapseFlag = true;	*/

			/* }*/
			  if($scope.newDeathcoverType == "DcUnitised"){
                  showhide('nodollar1','dollar1');
                        showhide('nodollar','dollar');
            } else if($scope.newDeathcoverType == "DcFixed"){
                  showhide('dollar1','nodollar1');
                  showhide('dollar','nodollar');
            }
			OccupationService.getOccupationList($scope.urlList.occupationUrl, "CARE", $scope.newIndustry).then(function (res) {
				//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.newIndustry}, function(occupationList){
				$scope.OccupationList = res.data;
				var temp = $scope.OccupationList.filter(function (obj) {
					return obj.occupationName == newOccDetails.occupation;
				});
				$scope.newOccupation = temp[0].occupationName;
			}, function (err) {
				console.log("Error while fetching occupation options " + JSON.stringify(err));
			});
			/*if(ipCheckboxState){
				$('#ipsalarycheck').parent().addClass('active');
			}*/
			if ($scope.deathTpdcheckboxState) {
				$('#increaseCoverAmount').parent().addClass('active');
			}
			if ($scope.indexation.death) {
				$('#newchangecvr-index-dth').parent().addClass('active');
			}

			if ($scope.indexation.tpd) {
				$('#newchangeCvr-indextpd-tpd').parent().addClass('active');
			}
			if (newAckCheck) {
				$timeout(function () {
					$('#newTermsLabel').addClass('active');
				});
			}
			if (dodCheck) {
				$timeout(function () {
					$('#dodLabel').addClass('active');
				});
			}
			if (privacyCheck) {
				$timeout(function () {
					$('#privacyLabel').addClass('active');
				});
			}
			$scope.toggleTwo(true);
			$scope.toggleThree(true);
			$scope.togglePrivacy(true);
			$scope.toggleContact(true);
		}
		$('.selectpicker').selectpicker();


	}]);


/*New Member Change Cover Controller ,Progressive and Mandatory Validations Ends   */

CareSuperApp.directive('phoneOnly', function () {
	return {
		require: 'ngModel',
		link: function (scope, element, attrs, modelCtrl) {

			modelCtrl.$parsers.push(function (inputValue) {
				var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;

				if (transformedInput != inputValue) {
					modelCtrl.$setViewValue(transformedInput);
					modelCtrl.$render();
				}

				return transformedInput;
			});

			$(element).on("keyup", function () {
				var TempVal = $(this).val().replace(" ", "");
				var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
				if (!regex.test(TempVal)) {
					// element.controller('ngModel').$setValidity('required', false);
					// element.controller('ngModel').$touched = true;
				}

			})
		}
	};
});