package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import au.com.metlife.eapply.bs.controller.TblOccMappingRuledataController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@JsonDeserialize(as = InputStream.class)
public class TblOccMappingRuledataControllerTest {

	@InjectMocks
	private TblOccMappingRuledataController tblOccMappingRuledataController;
	
	private MockMvc mockMvc;



	@Autowired
	private WebApplicationContext context;
	
	String testData = null;
	String fileName =null;
	
	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void getOccupationNameTest001() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "001"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
				.andExpect(jsonPath("$[1].occupationName", is("Office desk based duties - Degree or tertiary qualified")))
				.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
				.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Not degree or tertiary qualified")))
				.andExpect(jsonPath("$[4].occupationName", is("Working outside of office environment - Handling merchandise")))
				.andExpect(jsonPath("$[5].occupationName", is("Working outside of office environment - Not handling merchandise")))
				.andExpect(jsonPath("$[6].occupationName", is("Other")));
	}
	
	@Test
	public void getOccupationNameTest002() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "002"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Abattoir Worker - Manual")))
					.andExpect(jsonPath("$[1].occupationName", is("Abattoir Worker - Non Manual")))
					.andExpect(jsonPath("$[2].occupationName", is("Agriculture Pilot")))
					.andExpect(jsonPath("$[3].occupationName", is("Agronomist / Agricultural Scientist")))
					.andExpect(jsonPath("$[4].occupationName", is("Animal Handler/Trainer/Breeder - Large animals")))
					.andExpect(jsonPath("$[5].occupationName", is("Animal Handler/Trainer/Breeder - Small animals")))
					.andExpect(jsonPath("$[6].occupationName", is("Arborist Tree Surgeon - Above 15 metres")));
		
	}
	
	@Test
	public void getOccupationNameTest003() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "003"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Actor/Actress/Entertainer")))
					.andExpect(jsonPath("$[1].occupationName", is("Amusement Parlour/Centre - Other")))
					.andExpect(jsonPath("$[2].occupationName", is("Amusement Parlour/Centre - Owner/Manager/Supervisor")))
					.andExpect(jsonPath("$[3].occupationName", is("Artist - Commercial not working from home")))
					.andExpect(jsonPath("$[4].occupationName", is("Artist - Freelance or working from home")))
					.andExpect(jsonPath("$[5].occupationName", is("Author/Writer")))
					.andExpect(jsonPath("$[6].occupationName", is("Business Analyst/Consultant")));
	}
	
	@Test
	public void getOccupationNameTest004() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "004"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Air Traffic Controller")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Cabin Crew")))
					.andExpect(jsonPath("$[3].occupationName", is("Flight Engineer")))
					.andExpect(jsonPath("$[4].occupationName", is("Maintenance/Baggage Handlers/Truck Drivers/Mechanic")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
	}
	
	@Test
	public void getOccupationNameTest005() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "005"))
		        	.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Air Conditioning Engineer/Installer/Technician - Manual")))
					.andExpect(jsonPath("$[1].occupationName", is("Air Conditioning Engineer/Installer/Technician - Non Manual")))
					.andExpect(jsonPath("$[2].occupationName", is("Architect - Manual")))
					.andExpect(jsonPath("$[3].occupationName", is("Architect - Non Manual")))
					.andExpect(jsonPath("$[4].occupationName", is("Asbestos Worker")))
					.andExpect(jsonPath("$[5].occupationName", is("Asphalt Layer/Road Construction/Maintenance")))
					.andExpect(jsonPath("$[6].occupationName", is("Blaster/Explosive Handler")));
	}
	
	@Test
	public void getOccupationNameTest006() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "006"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Child Care Worker - Not registered")))
					.andExpect(jsonPath("$[2].occupationName", is("Child Care Worker - Registered")))
					.andExpect(jsonPath("$[3].occupationName", is("Employment Agent")))
					.andExpect(jsonPath("$[4].occupationName", is("Headmaster/Principal")))
					.andExpect(jsonPath("$[5].occupationName", is("Janitor")))
					.andExpect(jsonPath("$[6].occupationName", is("Lecturer - Other")));
	}
	
	@Test
	public void getOccupationNameTest007() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "007"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Accountant - CPA")))
					.andExpect(jsonPath("$[1].occupationName", is("Accountant/Bookkeeper - Other")))
					.andExpect(jsonPath("$[2].occupationName", is("Actuary")))
					.andExpect(jsonPath("$[3].occupationName", is("Armed Guards/Armoured Car Drivers")))
					.andExpect(jsonPath("$[4].occupationName", is("Auditor - Other")))
					.andExpect(jsonPath("$[5].occupationName", is("Auditor - Qualified")))
					.andExpect(jsonPath("$[6].occupationName", is("Broker - Commodity/Finance/Stock")));
	}
	
	@Test
	public void getOccupationNameTest008() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "008"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Diver - Abalone/Professional/Salvage")))
					.andExpect(jsonPath("$[2].occupationName", is("Dockyard/Shipyard Worker/Labourer")))
					.andExpect(jsonPath("$[3].occupationName", is("Fisherman/woman")))
					.andExpect(jsonPath("$[4].occupationName", is("Marine Biologist - Manual duties (diving)")))
					.andExpect(jsonPath("$[5].occupationName", is("Marine Biologist - Manual duties (no diving)")))
					.andExpect(jsonPath("$[6].occupationName", is("Marine Biologist - Office only")));
	}
	
	@Test
	public void getOccupationNameTest009() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "009"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Cemetery/Crematorium - Grave digger")))
					.andExpect(jsonPath("$[2].occupationName", is("Cemetery/Crematorium - Manager - Office Only")))
					.andExpect(jsonPath("$[3].occupationName", is("Cemetery/Crematorium - Stonemason")))
					.andExpect(jsonPath("$[4].occupationName", is("Child Care Worker - Not Registered")))
					.andExpect(jsonPath("$[5].occupationName", is("Child Care Worker - Registered")))
					.andExpect(jsonPath("$[6].occupationName", is("Community Care Worker")));
	}
	
	@Test
	public void getOccupationNameTest010() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "010"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Bar Attendant")))
					.andExpect(jsonPath("$[1].occupationName", is("Bar Manager/Publican")))
					.andExpect(jsonPath("$[2].occupationName", is("Bouncer/Security Worker")));
					
	}
	
	@Test
	public void getOccupationNameTest011() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "011"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Analyst/Consultant/Programmer")))
					.andExpect(jsonPath("$[1].occupationName", is("Keyboard/Systems Operator")))
					.andExpect(jsonPath("$[2].occupationName", is("Maintenance Engineer (Software)")))
					.andExpect(jsonPath("$[3].occupationName", is("Maintenance Engineer/ Technician (Hardware)")))
					.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[6].occupationName", is("Sales Representative")));
	}
	
	@Test
	public void getOccupationNameTest012() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "012"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Attorney/Barrister/Lawyer/Solicitor")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Coroner")))
					.andExpect(jsonPath("$[3].occupationName", is("Magistrate/Judge/Legal Arbitrator")))
					.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[6].occupationName", is("Patent Attorney")));
	}
	
	@Test
	public void getOccupationNameTest013() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "013"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Dressmaker/Seamstress - Not working from home")))
					.andExpect(jsonPath("$[3].occupationName", is("Dressmaker/Seamstress- Working from home")))
					.andExpect(jsonPath("$[4].occupationName", is("Factory/Process Worker")))
					.andExpect(jsonPath("$[5].occupationName", is("Inspector Quality Control")))
					.andExpect(jsonPath("$[6].occupationName", is("Machine Operator")));
	}
	
	@Test
	public void getOccupationNameTest014() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "014"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Actor/Director/Producer")))
					.andExpect(jsonPath("$[1].occupationName", is("Announcer/Newsreader/Producer")))
					.andExpect(jsonPath("$[2].occupationName", is("Antenna/ Lines person working Above 15 mts")))
					.andExpect(jsonPath("$[3].occupationName", is("Antenna/ Lines person working Below 15 mts")))
					.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[5].occupationName", is("Cable Television Installer")))
					.andExpect(jsonPath("$[6].occupationName", is("Camera Operator/Engineer/Sound Recorder")));
	}
	
	@Test
	public void getOccupationNameTest015() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "015"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Acupuncturist/Alternate Health Care/Naturopath")))
					.andExpect(jsonPath("$[1].occupationName", is("Ambulance Driver/Paramedic")))
					.andExpect(jsonPath("$[2].occupationName", is("Audiologist/Audiometrist")))
					.andExpect(jsonPath("$[3].occupationName", is("Auxilary Nurse/Aide/Assistant")))
					.andExpect(jsonPath("$[4].occupationName", is("Biochemist/Chemist/Pharmacist")))
					.andExpect(jsonPath("$[5].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[6].occupationName", is("Chemist/Pharmacist - Owner")));
	}
	
	@Test
	public void getOccupationNameTest016() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "016"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Blaster/Explosive Handler")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Engineer/Surveyor/Geologist - Non Manual")))
					.andExpect(jsonPath("$[3].occupationName", is("Machinery or plant operator")))
					.andExpect(jsonPath("$[4].occupationName", is("Nuclear Plant - Engineer/Operator/Technician")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
	}
	
	@Test
	public void getOccupationNameTest017() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "017"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
					.andExpect(jsonPath("$[1].occupationName", is("Auto Electrician/Technician/Mechanic")))
					.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[3].occupationName", is("Car Detailer/Spray painter/Panel beater")))
					.andExpect(jsonPath("$[4].occupationName", is("Car Salesperson")))
					.andExpect(jsonPath("$[5].occupationName", is("Demolisher/Salvage/Wrecker")))
					.andExpect(jsonPath("$[6].occupationName", is("Garage/Service Station Worker - Cashier - No manual duties")));
	}
	
	@Test
	public void getOccupationNameTest018() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "018"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Armed Guards / Armoured Car Drivers")))
					.andExpect(jsonPath("$[1].occupationName", is("Army/Navy/Air Force - All ranks & Personnel")))
					.andExpect(jsonPath("$[2].occupationName", is("Bodyguard")))
					.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[4].occupationName", is("Crowd Control / Bouncer")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
	}
	
	@Test
	public void getOccupationNameTest019() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "019"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Agents Representative")))
					.andExpect(jsonPath("$[1].occupationName", is("Auctioneer")))
					.andExpect(jsonPath("$[2].occupationName", is("Building Estimator / Property Valuer")))
					.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[4].occupationName", is("Buyers Advocate / Vendors Advocate")))
					.andExpect(jsonPath("$[5].occupationName", is("Land Broker")))
					.andExpect(jsonPath("$[6].occupationName", is("Licensed Real Estate Agent")));
	}
	
	@Test
	public void getOccupationNameTest020() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "020"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Bakery/Butcher/Delicatessen/Fishmonger")))
					.andExpect(jsonPath("$[1].occupationName", is("Barista")))
					.andExpect(jsonPath("$[2].occupationName", is("Beautician/Hairdresser/Manicurist - Mobile worker or from home")))
					.andExpect(jsonPath("$[3].occupationName", is("Beautician/Hairdresser/Manicurist - Shopfront")))
					.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")));
	}
	
	@Test
	public void getOccupationNameTest021() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "021"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Lab Assistant/Surveyor/Technician")))
					.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[4].occupationName", is("Scientist working in office or Lab only")))
					.andExpect(jsonPath("$[5].occupationName", is("Scientist working in remote places")))
					.andExpect(jsonPath("$[6].occupationName", is("Scientist working with animals/chemicals/in field/overseas")));
	}
	
	@Test
	public void getOccupationNameTest022() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "022"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Golf Professional (Teaching And Pro Shop only)")))
					.andExpect(jsonPath("$[2].occupationName", is("Jockey/Trotting Driver")))
					.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[5].occupationName", is("Personal Trainer")))
					.andExpect(jsonPath("$[6].occupationName", is("Scuba Diving Instructor")));
	}
	
	@Test
	public void getOccupationNameTest023() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "023"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Bus/Coach/Tram - 200km distance or greater")))
					.andExpect(jsonPath("$[1].occupationName", is("Bus/Coach/Tram - Local")))
					.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[3].occupationName", is("Courier - Bicycle/Motor bike")))
					.andExpect(jsonPath("$[4].occupationName", is("Courier - Car/Van")))
					.andExpect(jsonPath("$[5].occupationName", is("Driving Instructor/Chauffeur/Funeral car driver")))
					.andExpect(jsonPath("$[6].occupationName", is("Ferry Crew")));
	}
	
	@Test
	public void getOccupationNameTest024() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "024"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Student")))
					.andExpect(jsonPath("$[1].occupationName", is("Unemployed")));
	}
	
	@Test
	public void getOccupationNameTest025() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "HOST").param("induCode", "025"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Home Maker")));
					
	}
	
	//Care
	
	@Test
	public void getOccupationNameCareTest001() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "001"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
				.andExpect(jsonPath("$[1].occupationName", is("Office desk based duties - Degree or tertiary qualified")))
				.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
				.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Not degree or tertiary qualified")))
				.andExpect(jsonPath("$[4].occupationName", is("Working outside of office environment - Handling merchandise")))
				.andExpect(jsonPath("$[5].occupationName", is("Working outside of office environment - Not handling merchandise")))
				.andExpect(jsonPath("$[6].occupationName", is("Other")));
	}
	
	@Test
	public void getOccupationNameCareTest002() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "002"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Abattoir Worker - Manual")))
					.andExpect(jsonPath("$[1].occupationName", is("Abattoir Worker - Non Manual")))
					.andExpect(jsonPath("$[2].occupationName", is("Agriculture Pilot")))
					.andExpect(jsonPath("$[3].occupationName", is("Agronomist / Agricultural Scientist")))
					.andExpect(jsonPath("$[4].occupationName", is("Animal Handler/Trainer/Breeder - Large animals")))
					.andExpect(jsonPath("$[5].occupationName", is("Animal Handler/Trainer/Breeder - Small animals")))
					.andExpect(jsonPath("$[6].occupationName", is("Arborist Tree Surgeon - Above 15 metres")));
		
	}
	
	@Test
	public void getOccupationNameCareTest003() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "003"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Actor/Actress/Entertainer")))
					.andExpect(jsonPath("$[1].occupationName", is("Amusement Parlour/Centre - Other")))
					.andExpect(jsonPath("$[2].occupationName", is("Amusement Parlour/Centre - Owner/Manager/Supervisor")))
					.andExpect(jsonPath("$[3].occupationName", is("Artist - Commercial not working from home")))
					.andExpect(jsonPath("$[4].occupationName", is("Artist - Freelance or working from home")))
					.andExpect(jsonPath("$[5].occupationName", is("Author/Writer")))
					.andExpect(jsonPath("$[6].occupationName", is("Business Analyst/Consultant")));
	}
	
	@Test
	public void getOccupationNameCareTest004() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "004"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Air Traffic Controller")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Cabin Crew")))
					.andExpect(jsonPath("$[3].occupationName", is("Flight Engineer")))
					.andExpect(jsonPath("$[4].occupationName", is("Maintenance/Baggage Handlers/Truck Drivers/Mechanic")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
	}
	
	@Test
	public void getOccupationNameCareTest005() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "005"))
		        	.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Air Conditioning Engineer/Installer/Technician - Manual")))
					.andExpect(jsonPath("$[1].occupationName", is("Air Conditioning Engineer/Installer/Technician - Non Manual")))
					.andExpect(jsonPath("$[2].occupationName", is("Architect - Manual")))
					.andExpect(jsonPath("$[3].occupationName", is("Architect - Non Manual")))
					.andExpect(jsonPath("$[4].occupationName", is("Asbestos Worker")))
					.andExpect(jsonPath("$[5].occupationName", is("Asphalt Layer/Road Construction/Maintenance")))
					.andExpect(jsonPath("$[6].occupationName", is("Blaster/Explosive Handler")));
	}
	
	@Test
	public void getOccupationNameCareTest006() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "006"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Child Care Worker - Not registered")))
					.andExpect(jsonPath("$[2].occupationName", is("Child Care Worker - Registered")))
					.andExpect(jsonPath("$[3].occupationName", is("Employment Agent")))
					.andExpect(jsonPath("$[4].occupationName", is("Headmaster/Principal")))
					.andExpect(jsonPath("$[5].occupationName", is("Janitor")))
					.andExpect(jsonPath("$[6].occupationName", is("Lecturer - Other")));
	}
	
	@Test
	public void getOccupationNameCareTest007() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "007"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Accountant - CPA")))
					.andExpect(jsonPath("$[1].occupationName", is("Accountant/Bookkeeper - Other")))
					.andExpect(jsonPath("$[2].occupationName", is("Actuary")))
					.andExpect(jsonPath("$[3].occupationName", is("Armed Guards/Armoured Car Drivers")))
					.andExpect(jsonPath("$[4].occupationName", is("Auditor - Other")))
					.andExpect(jsonPath("$[5].occupationName", is("Auditor - Qualified")))
					.andExpect(jsonPath("$[6].occupationName", is("Broker - Commodity/Finance/Stock")));
	}
	
	@Test
	public void getOccupationNameCareTest008() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "008"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Diver - Abalone/Professional/Salvage")))
					.andExpect(jsonPath("$[2].occupationName", is("Dockyard/Shipyard Worker/Labourer")))
					.andExpect(jsonPath("$[3].occupationName", is("Fisherman/woman")))
					.andExpect(jsonPath("$[4].occupationName", is("Marine Biologist - Manual duties (diving)")))
					.andExpect(jsonPath("$[5].occupationName", is("Marine Biologist - Manual duties (no diving)")))
					.andExpect(jsonPath("$[6].occupationName", is("Marine Biologist - Office only")));
	}
	
	@Test
	public void getOccupationNameCareTest009() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "009"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Cemetery/Crematorium - Grave digger")))
					.andExpect(jsonPath("$[2].occupationName", is("Cemetery/Crematorium - Manager - Office Only")))
					.andExpect(jsonPath("$[3].occupationName", is("Cemetery/Crematorium - Stonemason")))
					.andExpect(jsonPath("$[4].occupationName", is("Child Care Worker - Not Registered")))
					.andExpect(jsonPath("$[5].occupationName", is("Child Care Worker - Registered")))
					.andExpect(jsonPath("$[6].occupationName", is("Community Care Worker")));
	}
	
	@Test
	public void getOccupationNameCareTest010() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "010"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Bar Attendant")))
					.andExpect(jsonPath("$[1].occupationName", is("Bar Manager/Publican")))
					.andExpect(jsonPath("$[2].occupationName", is("Bouncer/Security Worker")));
					
	}
	
	@Test
	public void getOccupationNameCareTest011() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "011"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Analyst/Consultant/Programmer")))
					.andExpect(jsonPath("$[1].occupationName", is("Keyboard/Systems Operator")))
					.andExpect(jsonPath("$[2].occupationName", is("Maintenance Engineer (Software)")))
					.andExpect(jsonPath("$[3].occupationName", is("Maintenance Engineer/ Technician (Hardware)")))
					.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[6].occupationName", is("Sales Representative")));
	}
	
	@Test
	public void getOccupationNameCareTest012() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "012"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Attorney/Barrister/Lawyer/Solicitor")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Coroner")))
					.andExpect(jsonPath("$[3].occupationName", is("Magistrate/Judge/Legal Arbitrator")))
					.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[6].occupationName", is("Patent Attorney")));
	}
	
	@Test
	public void getOccupationNameCareTest013() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "013"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Dressmaker/Seamstress - Not working from home")))
					.andExpect(jsonPath("$[3].occupationName", is("Dressmaker/Seamstress- Working from home")))
					.andExpect(jsonPath("$[4].occupationName", is("Factory/Process Worker")))
					.andExpect(jsonPath("$[5].occupationName", is("Inspector Quality Control")))
					.andExpect(jsonPath("$[6].occupationName", is("Machine Operator")));
	}
	
	@Test
	public void getOccupationNameCareTest014() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "014"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Actor/Director/Producer")))
					.andExpect(jsonPath("$[1].occupationName", is("Announcer/Newsreader/Producer")))
					.andExpect(jsonPath("$[2].occupationName", is("Antenna/ Lines person working Above 15 mts")))
					.andExpect(jsonPath("$[3].occupationName", is("Antenna/ Lines person working Below 15 mts")))
					.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[5].occupationName", is("Cable Television Installer")))
					.andExpect(jsonPath("$[6].occupationName", is("Camera Operator/Engineer/Sound Recorder")));
	}
	
	@Test
	public void getOccupationNameCareTest015() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "015"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Acupuncturist/Alternate Health Care/Naturopath")))
					.andExpect(jsonPath("$[1].occupationName", is("Ambulance Driver/Paramedic")))
					.andExpect(jsonPath("$[2].occupationName", is("Audiologist/Audiometrist")))
					.andExpect(jsonPath("$[3].occupationName", is("Auxilary Nurse/Aide/Assistant")))
					.andExpect(jsonPath("$[4].occupationName", is("Biochemist/Chemist/Pharmacist")))
					.andExpect(jsonPath("$[5].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[6].occupationName", is("Chemist/Pharmacist - Owner")));
	}
	
	@Test
	public void getOccupationNameCareTest016() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "016"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Blaster/Explosive Handler")))
					.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[2].occupationName", is("Engineer/Surveyor/Geologist - Non Manual")))
					.andExpect(jsonPath("$[3].occupationName", is("Machinery or plant operator")))
					.andExpect(jsonPath("$[4].occupationName", is("Nuclear Plant - Engineer/Operator/Technician")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
	}
	
	@Test
	public void getOccupationNameCareTest017() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "017"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
					.andExpect(jsonPath("$[1].occupationName", is("Auto Electrician/Technician/Mechanic")))
					.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[3].occupationName", is("Car Detailer/Spray painter/Panel beater")))
					.andExpect(jsonPath("$[4].occupationName", is("Car Salesperson")))
					.andExpect(jsonPath("$[5].occupationName", is("Demolisher/Salvage/Wrecker")))
					.andExpect(jsonPath("$[6].occupationName", is("Garage/Service Station Worker - Cashier - No manual duties")));
	}
	
	@Test
	public void getOccupationNameCareTest018() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "018"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Armed Guards / Armoured Car Drivers")))
					.andExpect(jsonPath("$[1].occupationName", is("Army/Navy/Air Force - All ranks & Personnel")))
					.andExpect(jsonPath("$[2].occupationName", is("Bodyguard")))
					.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[4].occupationName", is("Crowd Control / Bouncer")))
					.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
	}
	
	@Test
	public void getOccupationNameCareTest019() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "019"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Agents Representative")))
					.andExpect(jsonPath("$[1].occupationName", is("Auctioneer")))
					.andExpect(jsonPath("$[2].occupationName", is("Building Estimator / Property Valuer")))
					.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[4].occupationName", is("Buyers Advocate / Vendors Advocate")))
					.andExpect(jsonPath("$[5].occupationName", is("Land Broker")))
					.andExpect(jsonPath("$[6].occupationName", is("Licensed Real Estate Agent")));
	}
	
	@Test
	public void getOccupationNameCareTest020() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "020"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Bakery/Butcher/Delicatessen/Fishmonger")))
					.andExpect(jsonPath("$[1].occupationName", is("Barista")))
					.andExpect(jsonPath("$[2].occupationName", is("Beautician/Hairdresser/Manicurist - Mobile worker or from home")))
					.andExpect(jsonPath("$[3].occupationName", is("Beautician/Hairdresser/Manicurist - Shopfront")))
					.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")));
	}
	
	@Test
	public void getOccupationNameCareTest021() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "021"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Lab Assistant/Surveyor/Technician")))
					.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[4].occupationName", is("Scientist working in office or Lab only")))
					.andExpect(jsonPath("$[5].occupationName", is("Scientist working in remote places")))
					.andExpect(jsonPath("$[6].occupationName", is("Scientist working with animals/chemicals/in field/overseas")));
	}
	
	@Test
	public void getOccupationNameCareTest022() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "022"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Golf Professional (Teaching And Pro Shop only)")))
					.andExpect(jsonPath("$[2].occupationName", is("Jockey/Trotting Driver")))
					.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Other employees")))
					.andExpect(jsonPath("$[5].occupationName", is("Personal Trainer")))
					.andExpect(jsonPath("$[6].occupationName", is("Scuba Diving Instructor")));
	}
	
	@Test
	public void getOccupationNameCareTest023() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "023"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Bus/Coach/Tram - 200km distance or greater")))
					.andExpect(jsonPath("$[1].occupationName", is("Bus/Coach/Tram - Local")))
					.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[3].occupationName", is("Courier - Bicycle/Motor bike")))
					.andExpect(jsonPath("$[4].occupationName", is("Courier - Car/Van")))
					.andExpect(jsonPath("$[5].occupationName", is("Driving Instructor/Chauffeur/Funeral car driver")))
					.andExpect(jsonPath("$[6].occupationName", is("Ferry Crew")));
	}
	
	@Test
	public void getOccupationNameCareTest024() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "024"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Student")))
					.andExpect(jsonPath("$[1].occupationName", is("Unemployed")));
	}
	
	@Test
	public void getOccupationNameCareTest025() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CARE").param("induCode", "025"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Home Maker")));
					
	}
	
	//Aeis
	
		@Test
		public void getOccupationNameAeisTest001() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "001"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Office desk based duties - Degree or tertiary qualified")))
					.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Not degree or tertiary qualified")))
					.andExpect(jsonPath("$[4].occupationName", is("Working outside of office environment - Handling merchandise")))
					.andExpect(jsonPath("$[5].occupationName", is("Working outside of office environment - Not handling merchandise")))
					.andExpect(jsonPath("$[6].occupationName", is("Other")));
		}
		
		@Test
		public void getOccupationNameAeisTest002() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "002"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Abattoir Worker - Manual")))
						.andExpect(jsonPath("$[1].occupationName", is("Abattoir Worker - Non Manual")))
						.andExpect(jsonPath("$[2].occupationName", is("Agriculture Pilot")))
						.andExpect(jsonPath("$[3].occupationName", is("Agronomist / Agricultural Scientist")))
						.andExpect(jsonPath("$[4].occupationName", is("Animal Handler/Trainer/Breeder - Large animals")))
						.andExpect(jsonPath("$[5].occupationName", is("Animal Handler/Trainer/Breeder - Small animals")))
						.andExpect(jsonPath("$[6].occupationName", is("Arborist Tree Surgeon - Above 15 metres")));
			
		}
		
		@Test
		public void getOccupationNameAeisTest003() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "003"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Actor/Actress/Entertainer")))
						.andExpect(jsonPath("$[1].occupationName", is("Amusement Parlour/Centre - Other")))
						.andExpect(jsonPath("$[2].occupationName", is("Amusement Parlour/Centre - Owner/Manager/Supervisor")))
						.andExpect(jsonPath("$[3].occupationName", is("Artist - Commercial not working from home")))
						.andExpect(jsonPath("$[4].occupationName", is("Artist - Freelance or working from home")))
						.andExpect(jsonPath("$[5].occupationName", is("Author/Writer")))
						.andExpect(jsonPath("$[6].occupationName", is("Business Analyst/Consultant")));
		}
		
		@Test
		public void getOccupationNameAeisTest004() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "004"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Air Traffic Controller")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Cabin Crew")))
						.andExpect(jsonPath("$[3].occupationName", is("Flight Engineer")))
						.andExpect(jsonPath("$[4].occupationName", is("Maintenance/Baggage Handlers/Truck Drivers/Mechanic")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
		}
		
		@Test
		public void getOccupationNameAeisTest005() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "005"))
			        	.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Air Conditioning Engineer/Installer/Technician - Manual")))
						.andExpect(jsonPath("$[1].occupationName", is("Air Conditioning Engineer/Installer/Technician - Non Manual")))
						.andExpect(jsonPath("$[2].occupationName", is("Architect - Manual")))
						.andExpect(jsonPath("$[3].occupationName", is("Architect - Non Manual")))
						.andExpect(jsonPath("$[4].occupationName", is("Asbestos Worker")))
						.andExpect(jsonPath("$[5].occupationName", is("Asphalt Layer/Road Construction/Maintenance")))
						.andExpect(jsonPath("$[6].occupationName", is("Blaster/Explosive Handler")));
		}
		
		@Test
		public void getOccupationNameAeisTest006() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "006"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Child Care Worker - Not registered")))
						.andExpect(jsonPath("$[2].occupationName", is("Child Care Worker - Registered")))
						.andExpect(jsonPath("$[3].occupationName", is("Employment Agent")))
						.andExpect(jsonPath("$[4].occupationName", is("Headmaster/Principal")))
						.andExpect(jsonPath("$[5].occupationName", is("Janitor")))
						.andExpect(jsonPath("$[6].occupationName", is("Lecturer - Other")));
		}
		
		@Test
		public void getOccupationNameAeisTest007() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "007"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Accountant - CPA")))
						.andExpect(jsonPath("$[1].occupationName", is("Accountant/Bookkeeper - Other")))
						.andExpect(jsonPath("$[2].occupationName", is("Actuary")))
						.andExpect(jsonPath("$[3].occupationName", is("Armed Guards/Armoured Car Drivers")))
						.andExpect(jsonPath("$[4].occupationName", is("Auditor - Other")))
						.andExpect(jsonPath("$[5].occupationName", is("Auditor - Qualified")))
						.andExpect(jsonPath("$[6].occupationName", is("Broker - Commodity/Finance/Stock")));
		}
		
		@Test
		public void getOccupationNameAeisTest008() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "008"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Diver - Abalone/Professional/Salvage")))
						.andExpect(jsonPath("$[2].occupationName", is("Dockyard/Shipyard Worker/Labourer")))
						.andExpect(jsonPath("$[3].occupationName", is("Fisherman/woman")))
						.andExpect(jsonPath("$[4].occupationName", is("Marine Biologist - Manual duties (diving)")))
						.andExpect(jsonPath("$[5].occupationName", is("Marine Biologist - Manual duties (no diving)")))
						.andExpect(jsonPath("$[6].occupationName", is("Marine Biologist - Office only")));
		}
		
		@Test
		public void getOccupationNameAeisTest009() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "009"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Cemetery/Crematorium - Grave digger")))
						.andExpect(jsonPath("$[2].occupationName", is("Cemetery/Crematorium - Manager - Office Only")))
						.andExpect(jsonPath("$[3].occupationName", is("Cemetery/Crematorium - Stonemason")))
						.andExpect(jsonPath("$[4].occupationName", is("Child Care Worker - Not Registered")))
						.andExpect(jsonPath("$[5].occupationName", is("Child Care Worker - Registered")))
						.andExpect(jsonPath("$[6].occupationName", is("Community Care Worker")));
		}
		
		@Test
		public void getOccupationNameAeisTest010() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "010"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Bar Attendant")))
						.andExpect(jsonPath("$[1].occupationName", is("Bar Manager/Publican")))
						.andExpect(jsonPath("$[2].occupationName", is("Bouncer/Security Worker")));
						
		}
		
		@Test
		public void getOccupationNameAeisTest011() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "011"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Analyst/Consultant/Programmer")))
						.andExpect(jsonPath("$[1].occupationName", is("Keyboard/Systems Operator")))
						.andExpect(jsonPath("$[2].occupationName", is("Maintenance Engineer (Software)")))
						.andExpect(jsonPath("$[3].occupationName", is("Maintenance Engineer/ Technician (Hardware)")))
						.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[6].occupationName", is("Sales Representative")));
		}
		
		@Test
		public void getOccupationNameAeisTest012() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "012"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Attorney/Barrister/Lawyer/Solicitor")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Coroner")))
						.andExpect(jsonPath("$[3].occupationName", is("Magistrate/Judge/Legal Arbitrator")))
						.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[6].occupationName", is("Patent Attorney")));
		}
		
		@Test
		public void getOccupationNameAeisTest013() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "013"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Dressmaker/Seamstress - Not working from home")))
						.andExpect(jsonPath("$[3].occupationName", is("Dressmaker/Seamstress- Working from home")))
						.andExpect(jsonPath("$[4].occupationName", is("Factory/Process Worker")))
						.andExpect(jsonPath("$[5].occupationName", is("Inspector Quality Control")))
						.andExpect(jsonPath("$[6].occupationName", is("Machine Operator")));
		}
		
		@Test
		public void getOccupationNameAeisTest014() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "014"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Actor/Director/Producer")))
						.andExpect(jsonPath("$[1].occupationName", is("Announcer/Newsreader/Producer")))
						.andExpect(jsonPath("$[2].occupationName", is("Antenna/ Lines person working Above 15 mts")))
						.andExpect(jsonPath("$[3].occupationName", is("Antenna/ Lines person working Below 15 mts")))
						.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[5].occupationName", is("Cable Television Installer")))
						.andExpect(jsonPath("$[6].occupationName", is("Camera Operator/Engineer/Sound Recorder")));
		}
		
		@Test
		public void getOccupationNameAeisTest015() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "015"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Acupuncturist/Alternate Health Care/Naturopath")))
						.andExpect(jsonPath("$[1].occupationName", is("Ambulance Driver/Paramedic")))
						.andExpect(jsonPath("$[2].occupationName", is("Audiologist/Audiometrist")))
						.andExpect(jsonPath("$[3].occupationName", is("Auxilary Nurse/Aide/Assistant")))
						.andExpect(jsonPath("$[4].occupationName", is("Biochemist/Chemist/Pharmacist")))
						.andExpect(jsonPath("$[5].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[6].occupationName", is("Chemist/Pharmacist - Owner")));
		}
		
		@Test
		public void getOccupationNameAeisTest016() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "016"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Blaster/Explosive Handler")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Engineer/Surveyor/Geologist - Non Manual")))
						.andExpect(jsonPath("$[3].occupationName", is("Machinery or plant operator")))
						.andExpect(jsonPath("$[4].occupationName", is("Nuclear Plant - Engineer/Operator/Technician")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
		}
		
		@Test
		public void getOccupationNameAeisTest017() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "017"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
						.andExpect(jsonPath("$[1].occupationName", is("Auto Electrician/Technician/Mechanic")))
						.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[3].occupationName", is("Car Detailer/Spray painter/Panel beater")))
						.andExpect(jsonPath("$[4].occupationName", is("Car Salesperson")))
						.andExpect(jsonPath("$[5].occupationName", is("Demolisher/Salvage/Wrecker")))
						.andExpect(jsonPath("$[6].occupationName", is("Garage/Service Station Worker - Cashier - No manual duties")));
		}
		
		@Test
		public void getOccupationNameAeisTest018() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "018"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Armed Guards / Armoured Car Drivers")))
						.andExpect(jsonPath("$[1].occupationName", is("Army/Navy/Air Force - All ranks & Personnel")))
						.andExpect(jsonPath("$[2].occupationName", is("Bodyguard")))
						.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[4].occupationName", is("Crowd Control / Bouncer")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
		}
		
		@Test
		public void getOccupationNameAeisTest019() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "019"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Agents Representative")))
						.andExpect(jsonPath("$[1].occupationName", is("Auctioneer")))
						.andExpect(jsonPath("$[2].occupationName", is("Building Estimator / Property Valuer")))
						.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[4].occupationName", is("Buyers Advocate / Vendors Advocate")))
						.andExpect(jsonPath("$[5].occupationName", is("Land Broker")))
						.andExpect(jsonPath("$[6].occupationName", is("Licensed Real Estate Agent")));
		}
		
		@Test
		public void getOccupationNameAeisTest020() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "020"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Bakery/Butcher/Delicatessen/Fishmonger")))
						.andExpect(jsonPath("$[1].occupationName", is("Barista")))
						.andExpect(jsonPath("$[2].occupationName", is("Beautician/Hairdresser/Manicurist - Mobile worker or from home")))
						.andExpect(jsonPath("$[3].occupationName", is("Beautician/Hairdresser/Manicurist - Shopfront")))
						.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")));
		}
		
		@Test
		public void getOccupationNameAeisTest021() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "021"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Lab Assistant/Surveyor/Technician")))
						.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[4].occupationName", is("Scientist working in office or Lab only")))
						.andExpect(jsonPath("$[5].occupationName", is("Scientist working in remote places")))
						.andExpect(jsonPath("$[6].occupationName", is("Scientist working with animals/chemicals/in field/overseas")));
		}
		
		@Test
		public void getOccupationNameAeisTest022() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "022"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Golf Professional (Teaching And Pro Shop only)")))
						.andExpect(jsonPath("$[2].occupationName", is("Jockey/Trotting Driver")))
						.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[5].occupationName", is("Personal Trainer")))
						.andExpect(jsonPath("$[6].occupationName", is("Scuba Diving Instructor")));
		}
		
		@Test
		public void getOccupationNameAeisTest023() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "023"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Bus/Coach/Tram - 200km distance or greater")))
						.andExpect(jsonPath("$[1].occupationName", is("Bus/Coach/Tram - Local")))
						.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[3].occupationName", is("Courier - Bicycle/Motor bike")))
						.andExpect(jsonPath("$[4].occupationName", is("Courier - Car/Van")))
						.andExpect(jsonPath("$[5].occupationName", is("Driving Instructor/Chauffeur/Funeral car driver")))
						.andExpect(jsonPath("$[6].occupationName", is("Ferry Crew")));
		}
		
		@Test
		public void getOccupationNameAeisTest024() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "024"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Student")))
						.andExpect(jsonPath("$[1].occupationName", is("Unemployed")));
		}
		
		@Test
		public void getOccupationNameAeisTest025() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "AEIS").param("induCode", "025"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Home Maker")));
						
		}
		
//Corp CORP
		
		@Test
		public void getOccupationNameCorpTest001() throws Exception {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "001"))
					.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
					.andExpect(jsonPath("$[1].occupationName", is("Office desk based duties - Degree or tertiary qualified")))
					.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
					.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Not degree or tertiary qualified")))
					.andExpect(jsonPath("$[4].occupationName", is("Working outside of office environment - Handling merchandise")))
					.andExpect(jsonPath("$[5].occupationName", is("Working outside of office environment - Not handling merchandise")))
					.andExpect(jsonPath("$[6].occupationName", is("Other")));
		}
		
		@Test
		public void getOccupationNameCorpTest002() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "002"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Abattoir Worker - Manual")))
						.andExpect(jsonPath("$[1].occupationName", is("Abattoir Worker - Non Manual")))
						.andExpect(jsonPath("$[2].occupationName", is("Agriculture Pilot")))
						.andExpect(jsonPath("$[3].occupationName", is("Agronomist / Agricultural Scientist")))
						.andExpect(jsonPath("$[4].occupationName", is("Animal Handler/Trainer/Breeder - Large animals")))
						.andExpect(jsonPath("$[5].occupationName", is("Animal Handler/Trainer/Breeder - Small animals")))
						.andExpect(jsonPath("$[6].occupationName", is("Arborist Tree Surgeon - Above 15 metres")));
			
		}
		
		@Test
		public void getOccupationNameCorpTest003() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "003"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Actor/Actress/Entertainer")))
						.andExpect(jsonPath("$[1].occupationName", is("Amusement Parlour/Centre - Other")))
						.andExpect(jsonPath("$[2].occupationName", is("Amusement Parlour/Centre - Owner/Manager/Supervisor")))
						.andExpect(jsonPath("$[3].occupationName", is("Artist - Commercial not working from home")))
						.andExpect(jsonPath("$[4].occupationName", is("Artist - Freelance or working from home")))
						.andExpect(jsonPath("$[5].occupationName", is("Author/Writer")))
						.andExpect(jsonPath("$[6].occupationName", is("Business Analyst/Consultant")));
		}
		
		@Test
		public void getOccupationNameCorpTest004() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "004"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Air Traffic Controller")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Cabin Crew")))
						.andExpect(jsonPath("$[3].occupationName", is("Flight Engineer")))
						.andExpect(jsonPath("$[4].occupationName", is("Maintenance/Baggage Handlers/Truck Drivers/Mechanic")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
		}
		
		@Test
		public void getOccupationNameCorpTest005() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "005"))
			        	.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Air Conditioning Engineer/Installer/Technician - Manual")))
						.andExpect(jsonPath("$[1].occupationName", is("Air Conditioning Engineer/Installer/Technician - Non Manual")))
						.andExpect(jsonPath("$[2].occupationName", is("Architect - Manual")))
						.andExpect(jsonPath("$[3].occupationName", is("Architect - Non Manual")))
						.andExpect(jsonPath("$[4].occupationName", is("Asbestos Worker")))
						.andExpect(jsonPath("$[5].occupationName", is("Asphalt Layer/Road Construction/Maintenance")))
						.andExpect(jsonPath("$[6].occupationName", is("Blaster/Explosive Handler")));
		}
		
		@Test
		public void getOccupationNameCorpTest006() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "006"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Child Care Worker - Not registered")))
						.andExpect(jsonPath("$[2].occupationName", is("Child Care Worker - Registered")))
						.andExpect(jsonPath("$[3].occupationName", is("Employment Agent")))
						.andExpect(jsonPath("$[4].occupationName", is("Headmaster/Principal")))
						.andExpect(jsonPath("$[5].occupationName", is("Janitor")))
						.andExpect(jsonPath("$[6].occupationName", is("Lecturer - Other")));
		}
		
		@Test
		public void getOccupationNameCorpTest007() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "007"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Accountant - CPA")))
						.andExpect(jsonPath("$[1].occupationName", is("Accountant/Bookkeeper - Other")))
						.andExpect(jsonPath("$[2].occupationName", is("Actuary")))
						.andExpect(jsonPath("$[3].occupationName", is("Armed Guards/Armoured Car Drivers")))
						.andExpect(jsonPath("$[4].occupationName", is("Auditor - Other")))
						.andExpect(jsonPath("$[5].occupationName", is("Auditor - Qualified")))
						.andExpect(jsonPath("$[6].occupationName", is("Broker - Commodity/Finance/Stock")));
		}
		
		@Test
		public void getOccupationNameCorpTest008() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "008"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Diver - Abalone/Professional/Salvage")))
						.andExpect(jsonPath("$[2].occupationName", is("Dockyard/Shipyard Worker/Labourer")))
						.andExpect(jsonPath("$[3].occupationName", is("Fisherman/woman")))
						.andExpect(jsonPath("$[4].occupationName", is("Marine Biologist - Manual duties (diving)")))
						.andExpect(jsonPath("$[5].occupationName", is("Marine Biologist - Manual duties (no diving)")))
						.andExpect(jsonPath("$[6].occupationName", is("Marine Biologist - Office only")));
		}
		
		@Test
		public void getOccupationNameCorpTest009() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "009"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Cemetery/Crematorium - Grave digger")))
						.andExpect(jsonPath("$[2].occupationName", is("Cemetery/Crematorium - Manager - Office Only")))
						.andExpect(jsonPath("$[3].occupationName", is("Cemetery/Crematorium - Stonemason")))
						.andExpect(jsonPath("$[4].occupationName", is("Child Care Worker - Not Registered")))
						.andExpect(jsonPath("$[5].occupationName", is("Child Care Worker - Registered")))
						.andExpect(jsonPath("$[6].occupationName", is("Community Care Worker")));
		}
		
		@Test
		public void getOccupationNameCorpTest010() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "010"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Bar Attendant")))
						.andExpect(jsonPath("$[1].occupationName", is("Bar Manager/Publican")))
						.andExpect(jsonPath("$[2].occupationName", is("Bouncer/Security Worker")));
						
		}
		
		@Test
		public void getOccupationNameCorpTest011() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "011"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Analyst/Consultant/Programmer")))
						.andExpect(jsonPath("$[1].occupationName", is("Keyboard/Systems Operator")))
						.andExpect(jsonPath("$[2].occupationName", is("Maintenance Engineer (Software)")))
						.andExpect(jsonPath("$[3].occupationName", is("Maintenance Engineer/ Technician (Hardware)")))
						.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[6].occupationName", is("Sales Representative")));
		}
		
		@Test
		public void getOccupationNameCorpTest012() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "012"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Attorney/Barrister/Lawyer/Solicitor")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Coroner")))
						.andExpect(jsonPath("$[3].occupationName", is("Magistrate/Judge/Legal Arbitrator")))
						.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[6].occupationName", is("Patent Attorney")));
		}
		
		@Test
		public void getOccupationNameCorpTest013() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "013"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Dressmaker/Seamstress - Not working from home")))
						.andExpect(jsonPath("$[3].occupationName", is("Dressmaker/Seamstress- Working from home")))
						.andExpect(jsonPath("$[4].occupationName", is("Factory/Process Worker")))
						.andExpect(jsonPath("$[5].occupationName", is("Inspector Quality Control")))
						.andExpect(jsonPath("$[6].occupationName", is("Machine Operator")));
		}
		
		@Test
		public void getOccupationNameCorpTest014() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "014"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Actor/Director/Producer")))
						.andExpect(jsonPath("$[1].occupationName", is("Announcer/Newsreader/Producer")))
						.andExpect(jsonPath("$[2].occupationName", is("Antenna/ Lines person working Above 15 mts")))
						.andExpect(jsonPath("$[3].occupationName", is("Antenna/ Lines person working Below 15 mts")))
						.andExpect(jsonPath("$[4].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[5].occupationName", is("Cable Television Installer")))
						.andExpect(jsonPath("$[6].occupationName", is("Camera Operator/Engineer/Sound Recorder")));
		}
		
		@Test
		public void getOccupationNameCorpTest015() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "015"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Acupuncturist/Alternate Health Care/Naturopath")))
						.andExpect(jsonPath("$[1].occupationName", is("Ambulance Driver/Paramedic")))
						.andExpect(jsonPath("$[2].occupationName", is("Audiologist/Audiometrist")))
						.andExpect(jsonPath("$[3].occupationName", is("Auxilary Nurse/Aide/Assistant")))
						.andExpect(jsonPath("$[4].occupationName", is("Biochemist/Chemist/Pharmacist")))
						.andExpect(jsonPath("$[5].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[6].occupationName", is("Chemist/Pharmacist - Owner")));
		}
		
		@Test
		public void getOccupationNameCorpTest016() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "016"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Blaster/Explosive Handler")))
						.andExpect(jsonPath("$[1].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[2].occupationName", is("Engineer/Surveyor/Geologist - Non Manual")))
						.andExpect(jsonPath("$[3].occupationName", is("Machinery or plant operator")))
						.andExpect(jsonPath("$[4].occupationName", is("Nuclear Plant - Engineer/Operator/Technician")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
		}
		
		@Test
		public void getOccupationNameCorpTest017() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "017"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Assembly/Production Line")))
						.andExpect(jsonPath("$[1].occupationName", is("Auto Electrician/Technician/Mechanic")))
						.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[3].occupationName", is("Car Detailer/Spray painter/Panel beater")))
						.andExpect(jsonPath("$[4].occupationName", is("Car Salesperson")))
						.andExpect(jsonPath("$[5].occupationName", is("Demolisher/Salvage/Wrecker")))
						.andExpect(jsonPath("$[6].occupationName", is("Garage/Service Station Worker - Cashier - No manual duties")));
		}
		
		@Test
		public void getOccupationNameCorpTest018() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "018"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Armed Guards / Armoured Car Drivers")))
						.andExpect(jsonPath("$[1].occupationName", is("Army/Navy/Air Force - All ranks & Personnel")))
						.andExpect(jsonPath("$[2].occupationName", is("Bodyguard")))
						.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[4].occupationName", is("Crowd Control / Bouncer")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[6].occupationName", is("Office desk based duties - Other employees")));
		}
		
		@Test
		public void getOccupationNameCorpTest019() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "019"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Agents Representative")))
						.andExpect(jsonPath("$[1].occupationName", is("Auctioneer")))
						.andExpect(jsonPath("$[2].occupationName", is("Building Estimator / Property Valuer")))
						.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[4].occupationName", is("Buyers Advocate / Vendors Advocate")))
						.andExpect(jsonPath("$[5].occupationName", is("Land Broker")))
						.andExpect(jsonPath("$[6].occupationName", is("Licensed Real Estate Agent")));
		}
		
		@Test
		public void getOccupationNameCorpTest020() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "020"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Bakery/Butcher/Delicatessen/Fishmonger")))
						.andExpect(jsonPath("$[1].occupationName", is("Beautician/Hairdresser/Manicurist - Mobile worker or from home")))
						.andExpect(jsonPath("$[2].occupationName", is("Beautician/Hairdresser/Manicurist - Shopfront")))
						.andExpect(jsonPath("$[3].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[5].occupationName", is("Cashier/Person Handling Heavy stock eg:grocer/hardware")))
						.andExpect(jsonPath("$[6].occupationName", is("Cashier/Person Handling Light stock eg:books/clothes/shoes")));
		}
		
		@Test
		public void getOccupationNameCorpTest021() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "021"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Lab Assistant/Surveyor/Technician")))
						.andExpect(jsonPath("$[2].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[3].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[4].occupationName", is("Scientist working in office or Lab only")))
						.andExpect(jsonPath("$[5].occupationName", is("Scientist working in remote places")))
						.andExpect(jsonPath("$[6].occupationName", is("Scientist working with animals/chemicals/in field/overseas")));
		}
		
		@Test
		public void getOccupationNameCorpTest022() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "022"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[1].occupationName", is("Diving Instructor")))
						.andExpect(jsonPath("$[2].occupationName", is("Golf Professional (Teaching And Pro Shop only)")))
						.andExpect(jsonPath("$[3].occupationName", is("Jockey/Trotting Driver")))
						.andExpect(jsonPath("$[4].occupationName", is("Office desk based duties - Executive or manager")))
						.andExpect(jsonPath("$[5].occupationName", is("Office desk based duties - Other employees")))
						.andExpect(jsonPath("$[6].occupationName", is("Personal Trainer")));
		}
		
		@Test
		public void getOccupationNameCorpTest023() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "023"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Bus/Coach/Tram - 200km distance or greater")))
						.andExpect(jsonPath("$[1].occupationName", is("Bus/Coach/Tram - Local")))
						.andExpect(jsonPath("$[2].occupationName", is("Business Analyst/Consultant")))
						.andExpect(jsonPath("$[3].occupationName", is("Courier - Bicycle/Motor bike")))
						.andExpect(jsonPath("$[4].occupationName", is("Courier - Car/Van")))
						.andExpect(jsonPath("$[5].occupationName", is("Driving Instructor/Chauffeur/Funeral car driver")))
						.andExpect(jsonPath("$[6].occupationName", is("Ferry Crew")));
		}
		
		@Test
		public void getOccupationNameCorpTest024() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "024"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Student")))
						.andExpect(jsonPath("$[1].occupationName", is("Unemployed")));
		}
		
		@Test
		public void getOccupationNameCorpTest025() throws Exception {
				mockMvc.perform(
						MockMvcRequestBuilders.get("/getOccupationName").param("fundId", "CORP").param("induCode", "025"))
						.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
						.andExpect(jsonPath("$[0].occupationName", is("Home Maker")));
						
		}
}
