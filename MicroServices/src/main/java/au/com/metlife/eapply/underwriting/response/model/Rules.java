package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Rules implements Serializable{
	private String applicationComplete;
	private String applicationSubmitted;
	private String PercentComplete;
	
	private List<Rule> listRule;

	public String getApplicationComplete() {
		return applicationComplete;
	}

	public void setApplicationComplete(String applicationComplete) {
		this.applicationComplete = applicationComplete;
	}

	public String getApplicationSubmitted() {
		return applicationSubmitted;
	}

	public void setApplicationSubmitted(String applicationSubmitted) {
		this.applicationSubmitted = applicationSubmitted;
	}

	public String getPercentComplete() {
		return PercentComplete;
	}

	public void setPercentComplete(String percentComplete) {
		PercentComplete = percentComplete;
	}

	public List<Rule> getListRule() {
		
		if (null == listRule) {
			listRule = new ArrayList<>();
		}
		return listRule;
	}	
}