package au.com.metlife.pics.webserivce;

public class MetlifeElodgeConstants {
	
	//Name given to this application type as underwriting.
	public final static String APPLICATION_UW = "UW";
	//Name given to this application type as claims.
	public final static String APPLICATION_CLAIMS = "CLAIMS";
	//Name given to status.
	public final static String STATUS = "ACTIVE";
//	Name given to death cover.
	public final static String DEATH_COVER = "DEATH";
//	Name given to tpd cover.
	public final static String TPD_COVER = "TPD";
//	Name given to IP cover.
	public final static String IP_COVER = "IP";
//	Name given to Trauma cover.
	public final static String TRAUMA_COVER = "TRAUMA";
	
	public final static String TERMINAL_ILLNESS_COVER = "TIC";
	
	public final static String SUBJECT_UW = "Confirmation of receipt of New Underwriting Case";
	public final static String SUBJECT_CLAIMS = "Confirmation of receipt of New Claim";
	public final static String SUBJECT_EXIST_UW = "Confirmation of receipt of additional Underwriting information";
	public final static String SUBJECT_EXIST_CLAIMS = "Confirmation of receipt of additional information for existing claim";
	public final static String PHONE_NUM = "PHONENUMBER1";
	public final static String APP_NUM = "APPLICATIONNUMBER";
	
	public final static String PH_NUM = "1300 555 625";
	
	public final static String FROM_SENDER = "metlife.service@au.metlife.com";
	
	public final static String CLIENT_CONFIG = "C:\\client_config\\";
	
	public final static String RETRIEVE_UW_WSDL = "https://awweb.e2e.au.metlifeasia.com/CAW2/services/EAPPSearch";
	public final static String CREATE_UPDATE_UW_WSDL = "https://awweb.e2e.au.metlifeasia.com/CAW2/services/PicsLodgement";
	
	
	public final static String ENGAGE_MODULE = "rampart";
	public final static String PICS_USERNAME = "internal01";
	public final static String PICS_PASSWORD= "metlife890"; 
	public final static String PICS_SUCCESS_CODE= "200";
	public final static String CLAIMS_APP_TYPE= "CLAIMS";
	public final static String CLAIMS_FILE_NAME= "_eLodgeNewCLAIM.pdf";
	public final static String UW_FILE_NAME= "_eLodgeNewUW.pdf";
	public final static String ADDITIONAL_CLAIMS_FILE_NAME= "_eLodgeAdditionalCLAIM.pdf";
	public final static String ADDITIONAL_UW_FILE_NAME= "_eLodgeAdditionalUW.pdf";
	public final static String UW_CONFIRM_MSG = "Confirmation of Online Lodgement-New Underwriting Case Submission";
	public final static String CLAIMS_CONFIRM_MSG= "Confirmation of Online Lodgement-New Claim Submission";
	public final static String UW_EXIST_CONFIRM_MSG= "Confirmation of Online Lodgement-additional information for existing underwriting case";
	public final static String CL_EXIST_CONFIRM_MSG= "Confirmation of Online Lodgement-additional information for existing claim";
	 public static final String ELODGEMENT_PDF_ATTACHMENT = "ELODGEMENT_PDF_ATTACHMENT";
    public static final String ELODGEMENT_CLAIMS_PDF_ATTACHMENT = "ElodgementClaimsPdfAttachment";
    public static final String ELODGEMENT_UW_PDF_ATTACHMENT = "ElodgementPdfAttachment";
    public static final String PDF_PROPERTY_FILE_LOC = "PdfPropertyFileLoc";
    public static final String WEB_SER_CONFIG = "ElodgementWSConfig";
    public static final String PICS_CRAETE_CONFIG = "PICSCreateConfig";
    public static final String PICS_SEARCH_CONFIG = "PICSSearchConfig";
    
    public static final String SECURITY_MANAGER = "Security Manager";
	public static final String AUTH_HOSTNAME ="authenticationHostname";
	public static final String AUTH_PORTNUMBER="authenticationPortnumber";
	public static final String AUTH_MGRRDN="authenticationManagerRdn";
	public static final String AUTH_PSWD="authenticationManagerPassword";
	public static final String AUTH_BASEDN="loginBaseDn";
	public static final String METLIFE_AUTH_BASEDN="loginBaseDnCustom";
	public static final String MET_ROLENAME="metrolename";
	public static final String LDAP_GIVENNAME="givenname";
	public static final String LDAP_SN="sn";	
	public static final String UID="uid";
	public static final String EQULAS="=";	
	public static final String BACK_SLASH ="BACK_SLASH";
	public static final String FILE_UPLOAD_PATH ="FILE_UPLOAD_PATH";
	public static final String FILECOUNTER = "FILECOUNTER";
	public static final String UPLOADFULLPATH = "UPLOADFULLPATH";
	public static final String EAPPUW_FUND_ADMIN = "EAPPUW FUND ADMINISTRATOR";
	public static final String EAPPUW_FUND_OWNER = "EAPPUW FUND OWNER";
	public static final String EAPPCLAIMS_FUND_ADMIN = "EAPPCLAIMS FUND ADMINISTRATOR";
	public static final String EAPPCLAIMS_FUND_OWNER = "EAPPCLAIMS FUND OWNER";
	
	public static final String FUNDID_ADMIN = "FUNDID_ADMIN";
	public static final String FUNDADMINISTRATORID = "FUNDADMINISTRATORID";
	
	public static final String METLIFE_INTERNAL = "METLIFE INTERNAL";
	public static final String NOT_ENTERED = "<Not Entered>";
	public static final String CURRENCY = "$";
	public static final String TITLE = "TITLE";
	public static final String FIRST_NAME = "FIRSTNAME";
	public static final String LAST_NAME = "LASTNAME";
	public static final String ZERO="0";
	
	
}

