claimApp.directive('myDatePicker',function(){
	   return {
	     restrict: 'A',	
	     require: 'ngModel',
	     link: function($scope, elem, $attr,ngModel){
	    	 $( elem ).datepicker({
	    			dateFormat: "dd/mm/yy",
	    			onSelect:function (date) {	    				
	    				//console.log(ngModel);    					    				 
     					$scope.$apply(function() {
     						ngModel.$setViewValue(date);         					
       				 	});
	    			},
	    			changeMonth: true,
	    			changeYear: true,
	    			minDate: "-100Y",
	    			maxDate: "+0D",
	    			yearRange: "-100:+0",
	    			showOn: "both",
	    			buttonText: "<i class='fa fa-calendar'></i>"
	    	    });
	     }
	   }
	 });	 
	
claimApp.directive('ngFiles', ['$parse', function ($parse) {

    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    };

    return {
        link: fn_link
    }
} ]);

claimApp.directive('docUploadType',function(){
	   return {
	     restrict: 'A',	
	     scope: {
	         component: "=component"
	       },
	     //require: 'ngModel',
	     link: function($scope, elem, $attr,ngModel){
	    	 alert(elem);
	    	// scope.$watch('onChange', function(nVal) { elm.val(nVal); });            
	    	 elem.on('change', function (event) {
	            	alert(elem);
	               // var currentValue = elm.val();
	               // if( scope.onChange !== currentValue ) {
	                   // scope.$apply(function() {
	                       // scope.onChange = currentValue;
	                    //});
	                //}
	            });
	     }
	   }
	 });
claimApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);                   
                });
            });
        }
    };
}]);

claimApp.directive('alphaNumeric', function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, element, attr, ngModelCtrl) {
            var Func = function(val) {
            
                if (val || val === '') {
                    var transformedInput = val.replace(/[^a-zA-Z0-9]/g, '');

                    if (transformedInput !== val) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                   return transformedInput;
                }
                return ''; //validation fix 
            };
            ngModelCtrl.$parsers.push(Func);
            ngModelCtrl.$formatters.push(Func);
        }
    };
});