/**
 * 
 */
package pages.metlife;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import supportlibraries.ScriptHelper;
import uimap.metlife.MetFlowLoginPageObjects;


/**
 * @author sampath
 * 
 */
public class MetflowLoginDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public MetflowLoginDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public MetflowLoginDetailsPage loginToMetflow() {
		enterLoginDetails();
		
		return new MetflowLoginDetailsPage(scriptHelper);
	} 
	
	private void enterLoginDetails() {
		
/*		final Properties properties;
		
		properties = Settings.getInstance();
		
		storeSessionThreadData("dyn_currentBrowser",properties.getProperty("currentBrowser"));
		System.out.println("The Current Browser is :: "+getStringValueFromSessionThreadData("dyn_currentBrowser"));*/
		
		String strLoginName = dataTable.getData("MetLife_Data", "login.name");
		String strLoginPassword = dataTable.getData("MetLife_Data", "login.password");
//		storeSessionThreadData("dyn_CompleteName",strFirstName.trim()+strLastName.trim());
		
		try{
			switchToFrame(waitForClickableElement(MetFlowLoginPageObjects.frameMainFrame));
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.txtLogin),"SET","Login User Name is ",strLoginName);
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.txtPassword),"SET","Login User Name is ",strLoginPassword);
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.btnSubmit),"Click","Sign In ");
		waitForPageTitle("Testing", 8);
			try{
		         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		       }
		 catch (org.openqa.selenium.UnhandledAlertException e) {                
		         Alert alert = driver.switchTo().alert(); 
		         String alertText = alert.getText().trim();
		         System.out.println("Alert data: "+ alertText);
		         alert.accept();
		         }

		/*	switchToMainFrame();
			sleep();
			switchToFrame(waitForClickableElement(MetFlowLoginPageObjects.frameDashboard));		
			tryAction(waitForClickableElement(MetFlowLoginPageObjects.linkCaseSearch),"Click","Case Search ");
		
			driver.findElement(By.xpath("//*[@id='top_6']/div/a/span")).click();
			sleep();
			switchToMainFrame();*/
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void isAlertPresent() {
		// TODO Auto-generated method stub
		
	}
}
