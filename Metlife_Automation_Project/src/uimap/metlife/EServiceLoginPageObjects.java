/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class EServiceLoginPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public EServiceLoginPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	

	
	public static final By txtUserID = getObjectIdentifaction("eService.login.userid.txt.css");
	public static final By txtPassword = getObjectIdentifaction("eService.login.password.txt.css");
	public static final By btnLogin= getObjectIdentifaction("eService.login.login.btn.css");

}
