(function(angular){
'use strict';

angular.module('CorporateApp').config(['$stateProvider', '$urlRouterProvider', '$httpProvider','APP_CONSTANTS', '$templateRequestProvider', function ($stateProvider, $urlRouterProvider,$httpProvider, APP_CONSTANTS, $templateRequestProvider) {
  $httpProvider.defaults.withCredentials = true;
  $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});
	$urlRouterProvider.otherwise('/login');
  $stateProvider
    .state('login', {
      url: '/login',
      controller: 'loginCtrl',
      resolve: {
        	urls:function(fetchUrlSvc){
          var path = 'CareSuperUrls.properties';
          return fetchUrlSvc.getUrls(path);
        }
      },
      controllerAs: 'vm',
      templateUrl: APP_CONSTANTS.path.modules + 'login/login.html'
    })
    .state('cover', {
      url: '/cover',
      controller: 'coverCtrl',
      controllerAs: 'vm',
      templateUrl: APP_CONSTANTS.path.modules + 'cover/cover.html'
    })
    .state('summary', {
      url: '/summary',
      controller: 'summaryCtrl',
      controllerAs: 'vm',
      templateUrl: APP_CONSTANTS.path.modules + 'summary/summary.html'
    })
    .state('onboard', {
      url: '/onboard/:fundcode',
      templateUrl: APP_CONSTANTS.path.modules + 'onboard/onboard.html',
      resolve: {
          	urls:function(fetchUrlSvc){
            var path = 'CareSuperUrls.properties';
            return fetchUrlSvc.getUrls(path);
          }
      },
      controller: 'onboardCtrl',
      controllerAs: 'vm'
    })
  .state('aura', {
      url: '/aura',
      templateUrl: APP_CONSTANTS.path.modules + 'aura/aura.html',
      controller: 'auraCtrl',
      resolve: {
          	urls:function(fetchUrlSvc){
            var path = 'CareSuperUrls.properties';
            return fetchUrlSvc.getUrls(path);
          }
      },
      controllerAs: 'vm'
    })
  .state('decision', {
      url: '/decision',
      templateUrl: APP_CONSTANTS.path.modules + 'Decision/decision.html',
      controller: 'decisionCtrl',
      resolve: {
          	urls:function(fetchUrlSvc){
            var path = 'CareSuperUrls.properties';
            return fetchUrlSvc.getUrls(path);
          }
      },
      controllerAs: 'vm'
    })
    .state('dashboard', { 
        url: '/dashboard/:fundcode', 
        templateUrl: APP_CONSTANTS.path.modules + 'dashboard/dashboard.html', 
        controller: 'dashboardCtrl', 
        resolve: { 
              urls:function(fetchUrlSvc){ 
              var path = 'CareSuperUrls.properties'; 
              return fetchUrlSvc.getUrls(path); 
            } 
        }, 
        controllerAs: 'vm' 
    })
    .state('sessionTimeOut', {
    	url:'/sessionTimeOut',
    	templateUrl : APP_CONSTANTS.path.modules + 'timeout/timeout.html',
      controller  : 'timeOutController',
      controllerAs: 'vm'
    });
  
}]);

})(angular);
