package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

public class IPCover implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -4411197543333293217L;
	
	private String ipMinAge;
	private String ipMaxAge;
	private String ipMinSumIns;
	private String ipMaxSumIns;
	private String ipSumInsType;
	private String ipFormula;
	private String ipAal;
	private String ipWp;
	private String ipBp;
	private String ipSciPolicyNum;
	public String getIpMinAge() {
		return ipMinAge;
	}
	public void setIpMinAge(String ipMinAge) {
		this.ipMinAge = ipMinAge;
	}
	public String getIpMaxAge() {
		return ipMaxAge;
	}
	public void setIpMaxAge(String ipMaxAge) {
		this.ipMaxAge = ipMaxAge;
	}
	public String getIpMinSumIns() {
		return ipMinSumIns;
	}
	public void setIpMinSumIns(String ipMinSumIns) {
		this.ipMinSumIns = ipMinSumIns;
	}
	public String getIpMaxSumIns() {
		return ipMaxSumIns;
	}
	public void setIpMaxSumIns(String ipMaxSumIns) {
		this.ipMaxSumIns = ipMaxSumIns;
	}
	public String getIpSumInsType() {
		return ipSumInsType;
	}
	public void setIpSumInsType(String ipSumInsType) {
		this.ipSumInsType = ipSumInsType;
	}
	public String getIpFormula() {
		return ipFormula;
	}
	public void setIpFormula(String ipFormula) {
		this.ipFormula = ipFormula;
	}
	public String getIpAal() {
		return ipAal;
	}
	public void setIpAal(String ipAal) {
		this.ipAal = ipAal;
	}
	public String getIpWp() {
		return ipWp;
	}
	public void setIpWp(String ipWp) {
		this.ipWp = ipWp;
	}
	public String getIpBp() {
		return ipBp;
	}
	public void setIpBp(String ipBp) {
		this.ipBp = ipBp;
	}
	public String getIpSciPolicyNum() {
		return ipSciPolicyNum;
	}
	public void setIpSciPolicyNum(String ipSciPolicyNum) {
		this.ipSciPolicyNum = ipSciPolicyNum;
	}
	@Override
	public String toString() {
		return "IPCover [ipMinAge=" + ipMinAge + ", ipMaxAge=" + ipMaxAge + ", ipMinSumIns=" + ipMinSumIns
				+ ", ipMaxSumIns=" + ipMaxSumIns + ", ipSumInsType=" + ipSumInsType + ", ipFormula=" + ipFormula
				+ ", ipAal=" + ipAal + ", ipWp=" + ipWp + ", ipBp=" + ipBp + ", ipSciPolicyNum=" + ipSciPolicyNum + "]";
	}
	
	

}
