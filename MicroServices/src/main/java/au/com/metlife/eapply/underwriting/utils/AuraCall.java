package au.com.metlife.eapply.underwriting.utils;

import java.io.Serializable;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rgatp.aura.wsapi.AuraService;
import com.rgatp.aura.wsapi.AuraServiceException;
import com.rgatp.aura.wsapi.AuraServiceProxyLocator;
import com.rgatp.aura.wsapi.Questionnaire;
import com.rgatp.aura.wsapi.Response;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.quote.utility.SystemProperty;



public class AuraCall implements Serializable{
	private static final Logger log = LoggerFactory.getLogger(AuraCall.class);
	/*private static String CLASS_NAME = "au.com.metlife.aura.utils.AuraCall";*/
	 private String _serviceUrl  = null;
	 private AuraService _auraService = null;
	 private Questionnaire _questionnaire = null; /*only used for testing ... TestQuestionCreator.createQuestionnaire()*/
	/* private Exception _lastException     = null;
	 private String    _lastExceptionCmd  = null;
	 private Date      _lastExceptionDate = null;*/

	 SystemProperty sys;
	 
	 

	 public AuraCall(){
		 try {
			 sys = SystemProperty.getInstance();
			/*hardcoded to E2E*/
			 _serviceUrl=sys.getProperty("auraUrl");//"https://www.e2e.underwriting.metlife.com.au/MetAus/services/AuraService";
			
		 }catch(Exception e){
             log.error("Error in getting system instance {}",e);
		 }
	 }


	  public AuraCall(String port,String add){
	 		 try {
	 			 log.info("Aura call start");
	 			/*hardcoded to E2E*/
	 			sys = SystemProperty.getInstance();
	 			_serviceUrl=sys.getProperty("auraUrl");//"https://www.e2e.underwriting.metlife.com.au/MetAus/services/AuraService";
	 			
	 			log.info("Aura call finish");
	 		
	 		 }catch(Exception e){
                    log.error("Error in getting system instance {}",e);
	 		 }
	 }
	  public Questionnaire processResponse(String clientName, String questionnaireId, Response[] responses){
		  log.info("Process response start");
		  _auraService = null;
	        /* locate our service*/
	        URL url = null;
	        AuraServiceProxyLocator serviceLocator = new AuraServiceProxyLocator();
	        serviceLocator.setMaintainSession(false);/* maintain a stateful session!*/
	        try {
	        	 url = new URL(_serviceUrl);
		         _auraService = serviceLocator.getAuraService(url);
		         _questionnaire=  _auraService.processResponses(clientName,questionnaireId,responses);
	        }catch (Exception e) {
	        	log.error("Error in process response : {}",e);
	            throw new AuraServiceException(MetlifeInstitutionalConstants.ERROR_IN_WEBSERVICE + e);
			}
	        log.info("Process response finish");
	        return _questionnaire;
	  }

	 /**
	     * Establish a new "stateful" session with a web-service
	     * identified by the supplied serviceUrl.
	     *
	     * @throws AuraServiceException a problem was encountered in
	     * establishing the new session.
	     */
	    public Questionnaire establishNewSessionToAURA(String inputXml){
	    	log.info("Establish new session to aura start");
	    	/*set initial state*/
	        _auraService = null;
	      /*locate our service*/
	        URL url = null;
	        AuraServiceProxyLocator serviceLocator = new AuraServiceProxyLocator();
	        serviceLocator.setMaintainSession(true); /* maintain a stateful session!*/
	        try {
	            url = new URL(_serviceUrl);
	            _auraService = serviceLocator.getAuraService(url);
	          /* ping our service to see if all is good*/
	            _auraService.testQuestionnaireEncoding(null);
	            _questionnaire = _auraService.initiateQuestionnaire(inputXml);
	            log.info("Establish new session to aura finish");
	            return _questionnaire;
	        }
	        catch (Exception e) {	 
	        	log.error("Error in establish new session to aura : {}",e);
	            throw new AuraServiceException(MetlifeInstitutionalConstants.ERROR_IN_WEBSERVICE + e);
	        /*return null;*/
	        }
            
	    }


	    /**
	     * Process the command -- init {inputXmlFile}
	     */





	    /**
	     * Process the command -- process
	     */
	    public Questionnaire process(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	log.info("Process start");
	    	
	    	System.out.println("Client Name : "+qu.getClientName());
	    	System.out.println("QuestionnaireId : "+qu.getQuestionnaireId());
	    	System.out.println("obtainResponses : "+qu.obtainResponses());

	        _questionnaire = _auraService.processResponses(qu.getClientName(),
	        		qu.getQuestionnaireId(),
	        		qu.obtainResponses());
        
	     /* qu.getQuestions()[0].obtainResponses()*/
	        log.info("Process finish");
	        return _questionnaire;

	    }


	    public String cancle(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	return _auraService.cancelQuestionnaire(qu.getClientName(),qu.getQuestionnaireId());
	    }

	    public String saveExit(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	log.info("Save and exit start");
	    	String returnStrign="";
	    	try{
	    	returnStrign= _auraService.saveAndExitQuestionnaire(qu.getClientName(),qu.getQuestionnaireId());
	    	}catch(Exception e){
	    		log.error("Error in save and exit : {}",e);
	    		}
	    	log.info("Save and exit finish");
	    	return returnStrign;
	    }

	    public String submit(Questionnaire qu) throws RemoteException, AuraServiceException{
	    	
	    	_auraService = getAuraService();	    	
	    	return _auraService.submitQuestionnaire(qu.getClientName(),qu.getQuestionnaireId());
	    }


	 
		public Questionnaire get_questionnaire() {
			return _questionnaire;
		}

		public void set_questionnaire(Questionnaire _questionnaire) {
			this._questionnaire = _questionnaire;
		}

		 /**
	     * Establish a new "stateful" session with a web-service
	     * identified by the supplied serviceUrl.
	     *
	     * @throws AuraServiceException a problem was encountered in
	     * establishing the new session.
	     */
	    public AuraService getAuraService(){
	    	log.info("Establish new session to aura start");
	    	/*set initial state*/
	        _auraService = null;
	      /*locate our service*/
	        URL url = null;
	        AuraServiceProxyLocator serviceLocator = new AuraServiceProxyLocator();
	        serviceLocator.setMaintainSession(true); /* maintain a stateful session!*/
	        try {
	            url = new URL(_serviceUrl);
	            _auraService = serviceLocator.getAuraService(url);
	          /* ping our service to see if all is good*/
	            _auraService.testQuestionnaireEncoding(null);
	          
	            return _auraService;
	        }
	        catch (Exception e) {	 
	        	log.error("Error in establish new session to aura: {}",e);
	            throw new AuraServiceException(MetlifeInstitutionalConstants.ERROR_IN_WEBSERVICE + e);
	        /*return null;*/
	        }
            
	    }
}
