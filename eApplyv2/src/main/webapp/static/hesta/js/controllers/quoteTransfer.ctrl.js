/* Transfer Cover Controller,Progressive and Mandatory validations Starts  */
CareSuperApp.controller('quotetransfer',['$scope', '$routeParams','$location','$timeout','$window','persoanlDetailService','MaxLimitService','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'TransferCalculateService', 'NewOccupationService','auraInputService','PersistenceService','ngDialog','auraResponseService','Upload','urlService', 
                                         function($scope, $routeParams,$location,$timeout,$window,persoanlDetailService,MaxLimitService,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService, TransferCalculateService, NewOccupationService, auraInputService,PersistenceService,ngDialog,auraResponseService,Upload,urlService){
	$scope.urlList = urlService.getUrlList();
    $scope.phoneNumbrTrans = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
    $scope.emailFormatTrans = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{2,}$/;
    // ($scope.deathCoverTransDetails.amount=='0')
    $scope.indexation= {
    		death: false,
    		disable: false
    }
    
    $scope.setIndexation = function ($event) {
    	$event.stopPropagation();
    	$event.preventDefault();
    	$scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
    	if(!$scope.indexation.death) {
    		$("#indexation-death").parent().removeClass('active');
    		$("#indexation-disable").parent().removeClass('active');
    	}
    }
    QuoteService.getList($scope.urlList.quoteUrl,"CARE").then(function(res){
    	$scope.IndustryOptions = res.data;
    }, function(err){
    	console.log("Error while getting industry options " + JSON.stringify(err));
    });
    
    //Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
  	$scope.isEmpty = function(value){
  		return ((value == "" || value == null) || value == "0");
  	};
    
    $scope.getOccupations = function(){
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"METL",$scope.transferIndustry).then(function(res){
    		$scope.OccupationList = res.data;
        	$scope.occupationTransfer = '';
    	}, function(err){
    		console.log("Error while fetching occupation options " + JSON.stringify(err));
    	});
    };
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
    	/*console.log(auraInputService)
  		auraInputService.setName($scope.personalDetails.firstName+" "+$scope.personalDetails.lastName)
  		auraInputService.setIndustryOcc($scope.transferIndustry + ":" + $scope.occupationTransfer)
  		auraInputService.setWaitingPeriod($scope.waitingPeriodTransAddnl);
  		auraInputService.setBenefitPeriod($scope.benefitPeriodTransAddnl);
  		auraInputService.setGender($scope.personalDetails.gender);	  		
  		auraInputService.setAge( moment().diff($scope.personalDetails.dateOfBirth, 'years')) ; 		
  		auraInputService.setCountry(persoanlDetailService.getMemberDetails()[0].address.country);
  		auraInputService.setDeathAmt($scope.dcTransCoverAmount)
  		auraInputService.setTpdAmt($scope.tpdTransCoverAmount)
  		auraInputService.setIpAmt($scope.ipTransCoverAmount) 	
  		auraInputService.setFifteenHr($scope.fifteenHrsTransferQuestion)
  		auraInputService.setSalary($scope.annualTransferSalary)	*/
  		$location.path( path );
  	};
  	
 // Added to get user details and Maximum limits from Rulesheet
   	var DCTransMaxAmount, TPDTransMaxAmount, IPTransMaxAmount;
  	/*getclientData.requestObj().then(function(response){    	
    	$scope.partnerRequest = response.data;
    	$scope.applicantlist = $scope.partnerRequest.applicant;   
    	
    	angular.forEach($scope.applicantlist, function (applicant) {        		
    		$scope.personalDetails = applicant.personalDetails 
    		if(applicant.personalDetails.gender==null || applicant.personalDetails.gender==""){
    			$scope.genderFlag =  false;
			}else{
				$scope.genderFlag =  true;
			}
    	});
    	var memberType = response.data.applicant[0].memberType;
    	MaxLimitService.getMaxLimits({fundCode:"CARE", memberType: memberType, manageType:"TCOVER"}, function(res){
        	var limits = res;
        	DCTransMaxAmount = limits[0].deathTpdTransferMaxAmt;
        	TPDTransMaxAmount = limits[0].deathTpdTransferMaxAmt;
        	IPTransMaxAmount = limits[0].ipTransferMaxAmt;
    	}, function(error){
        	console.log('Something went wrong while fetching limits ' + error);
        });
    });*/
   	
   	var inputDetails = persoanlDetailService.getMemberDetails();
   	$scope.personalDetails = inputDetails[0].personalDetails;
   	
		if(inputDetails[0] && inputDetails[0].contactDetails.emailAddress){
			$scope.transferEmail = inputDetails[0].contactDetails.emailAddress;
		}
		if(inputDetails[0] && inputDetails[0].contactDetails.prefContactTime){
			if(inputDetails[0].contactDetails.prefContactTime == "1"){
				$scope.TransferTime= "Morning (9am - 12pm)";
			}else{
				$scope.TransferTime= "Afternoon (12pm - 6pm)";
			}
		}
		if(inputDetails[0] && inputDetails[0].contactDetails.prefContact){
			if(inputDetails[0].contactDetails.prefContact == "1"){
				$scope.preferredContactType= "Mobile";
				$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
				$scope.transferPhone = inputDetails[0].contactDetails.mobilePhone;
			}else if(inputDetails[0].contactDetails.prefContact == "2"){
				$scope.preferredContactType= "Home";
				$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
				$scope.transferPhone = inputDetails[0].contactDetails.homePhone;
			}else if(inputDetails[0].contactDetails.prefContact == "3"){
				$scope.preferredContactType= "Work";
				$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
				$scope.transferPhone = inputDetails[0].contactDetails.workPhone;
			}
	   }
		$scope.changePrefContactType = function(){
			if($scope.preferredContactType == "Home"){
				$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
				$scope.transferPhone = inputDetails[0].contactDetails.homePhone;
			}else if($scope.preferredContactType == "Work"){
				$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
				$scope.transferPhone = inputDetails[0].contactDetails.workPhone;
			}else if($scope.preferredContactType == "Mobile"){
				$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
				$scope.transferPhone = inputDetails[0].contactDetails.mobilePhone;
			}
		}
   	console.log($scope.personalDetails);
   	var fetchAppnum = true;
   	var appNum;
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    $scope.otherOccupationObj = {'transferotherOccupation': ''};
    
  	$scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
  	

  //maximum validation
  	$scope.validateDeathMaxAmount = function(){
  		$scope.coverAmtErrFlag = false;
  		 if(parseInt($scope.deathCoverTransDetails.amount) > 2000000){
  			$scope.TransDeathRequireCover = 0;
  			$scope.maxDeathErrorFlag = true;
  		 } else if(parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount) > 2000000){
  			$scope.TransDeathRequireCover = 2000000 - parseInt($scope.deathCoverTransDetails.amount);
 	  		$scope.maxDeathErrorFlag = true;
  		 } else{
 	  		$scope.maxDeathErrorFlag = false;  
  	  	}
  		$scope.autoCalculate();
  	 };
  	 
  	$scope.validateTpdMaxAmount = function(){
  		 if($scope.TransTPDRequireCover != null && $scope.TransDeathRequireCover == null ){
  			$scope.coverAmtErrFlag = true;
  			$scope.coverAmtErrMsg="You cannot apply for TPD cover without Death Cover.";
 	    } else if(parseInt($scope.tpdCoverTransDetails.amount) > 2000000){
  			$scope.TransTPDRequireCover = 0;
  		    $scope.maxTpdErrorFlag = true;
	    } else if(parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount) > 2000000){
	    	$scope.TransTPDRequireCover = 2000000 - $scope.tpdCoverTransDetails.amount;
	    	$scope.maxTpdErrorFlag = true;
	    }
	    /*else if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
	    	$scope.coverAmtErrFlag = true;
	    	$scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.Please re-enter.";
	    }*/
	    else{
	    	$scope.coverAmtErrFlag = false;
 	  		$scope.maxTpdErrorFlag = false; 
 	  	}
  		$scope.autoCalculate();
  	};
  	
  	$scope.validateIpMaxAmount = function(){
  		$scope.coverAmtErrFlag = false;
  		var maxAllowableTransfer = parseInt(0.85 * (parseInt($scope.annualTransferSalary)/12));
  		if(parseInt($scope.ipCoverTransDetails.amount) > 10000){
  			$scope.TransIPRequireCover = 0;
 	 	    $scope.maxIpErrorFlag = true;
		} else if(parseInt($scope.ipCoverTransDetails.amount) < 10000 && maxAllowableTransfer > 10000 && $scope.TransIPRequireCover > 10000){
			$scope.TransIPRequireCover = 10000;
			$scope.maxIpErrorFlag = true;
		} else if(parseInt($scope.ipCoverTransDetails.amount) < 10000 && maxAllowableTransfer < 10000 && $scope.TransIPRequireCover > maxAllowableTransfer){
			$scope.TransIPRequireCover = maxAllowableTransfer;
			$scope.maxIpErrorFlag = true;
		} else{
			$scope.maxIpErrorFlag = false;
 		}
  		$scope.autoCalculate();
   	 };
  	
  	var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
  
  	$scope.calculateTransfer = function(){
  		if(typeof($scope.TransDeathRequireCover) == "undefined" || $scope.TransDeathRequireCover==''){
  			$scope.TransDeathRequireCover = 0;
  		}
  		if(typeof($scope.TransTPDRequireCover) == "undefined" || $scope.TransTPDRequireCover==''){
  			$scope.TransTPDRequireCover = 0;
  		}
  		if(typeof($scope.TransIPRequireCover) == "undefined" || $scope.TransIPRequireCover==''){
  			$scope.TransIPRequireCover = 0;
  		}
  		if(($scope.deathCoverTransDetails && $scope.deathCoverTransDetails.amount && typeof($scope.deathCoverTransDetails.amount) == "undefined") || $scope.deathCoverTransDetails.amount==''){
  			$scope.deathCoverTransDetails.amount = 0;
  		}
  		if(($scope.tpdCoverTransDetails && $scope.tpdCoverTransDetails.amount && typeof($scope.tpdCoverTransDetails.amount) == "undefined") || $scope.tpdCoverTransDetails.amount==''){
  			$scope.tpdCoverTransDetails.amount = 0;
  		}
  		if(($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.amount && typeof($scope.ipCoverTransDetails.amount) == "undefined") || $scope.ipCoverTransDetails.amount==''){
  			$scope.ipCoverTransDetails.amount = 0;
  		}
  		var ruleModel = {
        		"age": anb,
        		"fundCode": "METl",
        		"gender": $scope.gender,
        		"deathOccCategory": $scope.transferDeathOccupationCategory,
        		"tpdOccCategory": $scope.transferTpdOccupationCategory,
        		"ipOccCategory": $scope.transferIpOccupationCategory,
        		"manageType": "TCOVER",
        		"deathCoverType": "DcFixed",
        		"tpdCoverType": "TpdFixed",
        		"ipCoverType": "IpUnitised",
        		"premiumFrequency":"Weekly",
        		"ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
        		"ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
        		"deathTransferAmount": parseInt($scope.TransDeathRequireCover),
        		"tpdTransferAmount": parseInt($scope.TransTPDRequireCover),
        		"ipTransferAmount": parseInt($scope.TransIPRequireCover),
        		"deathExistingAmount": parseInt($scope.deathCoverTransDetails.amount),
        		"tpdExistingAmount": parseInt($scope.tpdCoverTransDetails.amount),
        		"ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
        	};
  		
  		TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
  		//TransferCalculateService.calculate({}, ruleModel, function(res){
  			var premium = res.data;
  			autoCalculate = true;
  			for(var i = 0; i < premium.length; i++){
    			if(premium[i].coverType == 'DcFixed'){
    				$scope.dcTransCoverAmount = premium[i].coverAmount;
    				$scope.dcTransCost = premium[i].cost;
    				if($scope.dcTransCost == null){
    	  				$scope.dcTransCost = 0.00;
    	  			}
    			} else if(premium[i].coverType == 'TpdFixed'){
    				$scope.tpdTransCoverAmount = premium[i].coverAmount;
    				$scope.tpdTransCost = premium[i].cost;
    				if($scope.tpdTransCost == null){
    	  				$scope.tpdTransCost = 0.00;
    	  			}
    			} else if(premium[i].coverType == 'IpUnitised'){
    				$scope.ipTransCoverAmount = premium[i].coverAmount;
    				$scope.ipTransCost = premium[i].cost;
    				if($scope.ipTransCost == null){
    	  				$scope.ipTransCost = 0.00;
    	  			}
    			}
			}
  			
  			$scope.totalTransCost = parseFloat($scope.dcTransCost) + parseFloat($scope.tpdTransCost) + parseFloat($scope.ipTransCost);
  			if(fetchAppnum){
    			fetchAppnum = false;
    			appNum = PersistenceService.getAppNumber();
    		}
  		}, function(err){
  			console.log("Error while calculating transfer premium " + JSON.stringify(err));
  		});
  	};
  	$scope.coverAmtErrFlag= false;
  	var autoCalculate = false;
  	$scope.autoCalculate = function(){
  		if(autoCalculate && !$scope.coverAmtErrFlag){
  			$scope.calculateTransfer();
  		}
  	};
  	var occupationCategory;
  	$scope.getTransferOccupationCategory = function(){
  		var name = $scope.transferIndustry + ":" + $scope.occupationTransfer;
  		NewOccupationService.getOccupation($scope.urlList.newOccupationUrl,"METL",name).then(function(res){
  		//NewOccupationService.getOccupation({fundId:"CARE", occName:name},function(res){
  			occupationCategory = res.data;
  			$scope.transferDeathOccupationCategory = occupationCategory[0].deathfixedcategeory;
  			$scope.transferTpdOccupationCategory = occupationCategory[0].tpdfixedcategeory;
  			$scope.transferIpOccupationCategory = occupationCategory[0].ipfixedcategeory;
  			
  			if($scope.transferDeathOccupationCategory != "Professional" && $scope.withinOfficeTransferQuestion == "Yes"){
  				$scope.transferDeathOccupationCategory = "Office";
  				if(parseInt($scope.annualTransferSalary) > 100000 && ($scope.tertiaryTransferQuestion == "Yes" || $scope.managementTransferRole == "Yes")){
  					$scope.transferDeathOccupationCategory = "Professional";
  				}
  			}
  			if($scope.transferTpdOccupationCategory != "Professional" && $scope.withinOfficeTransferQuestion == "Yes"){
  				$scope.transferTpdOccupationCategory = "Office";
  				if(parseInt($scope.annualTransferSalary) > 100000 && ($scope.tertiaryTransferQuestion == "Yes" || $scope.managementTransferRole == "Yes")){
  					$scope.transferTpdOccupationCategory = "Professional";
  				}
  			}
  			if($scope.transferIpOccupationCategory != "Professional" && $scope.withinOfficeTransferQuestion == "Yes"){
  				$scope.transferIpOccupationCategory = "Office";
  				if(parseInt($scope.annualTransferSalary) > 100000 && ($scope.tertiaryTransferQuestion == "Yes" || $scope.managementTransferRole == "Yes")){
  					$scope.transferIpOccupationCategory = "Professional";
  				}
  			}
  			$scope.autoCalculate();
  		}, function(e){
  			console.log("Error while fetching new occupation " + JSON.stringify(e));
  		});
  	};
  	
  	$scope.setCategory = function(){
  		var deathCat, tpdCat, ipCat;
  		
  		if(occupationCategory){
  			deathCat = occupationCategory[0].deathfixedcategeory;
  			tpdCat = occupationCategory[0].tpdfixedcategeory;
  			ipCat = occupationCategory[0].ipfixedcategeory;
  		}
  		$scope.transferDeathOccupationCategory = deathCat;
  		$scope.transferTpdOccupationCategory = tpdCat;
  		$scope.transferIpOccupationCategory = ipCat;
  		
  		if(deathCat != "Professional" && $scope.withinOfficeTransferQuestion == "Yes"){
			$scope.transferDeathOccupationCategory = "Office";
			if(parseInt($scope.annualTransferSalary) > 100000 && ($scope.tertiaryTransferQuestion == "Yes" || $scope.managementTransferRole == "Yes")){
				$scope.transferDeathOccupationCategory = "Professional";
			}
		}
		if(tpdCat != "Professional" && $scope.withinOfficeTransferQuestion == "Yes"){
			$scope.transferTpdOccupationCategory = "Office";
			if(parseInt($scope.annualTransferSalary) > 100000 && ($scope.tertiaryTransferQuestion == "Yes" || $scope.managementTransferRole == "Yes")){
				$scope.transferTpdOccupationCategory = "Professional";
			}
		}
		if(ipCat != "Professional" && $scope.withinOfficeTransferQuestion == "Yes"){
			$scope.transferIpOccupationCategory = "Office";
			if(parseInt($scope.annualTransferSalary) > 100000 && ($scope.tertiaryTransferQuestion == "Yes" || $scope.managementTransferRole == "Yes")){
				$scope.transferIpOccupationCategory = "Professional";
			}
		}
		$scope.autoCalculate();
  	};
  	
  	$scope.coltwo = false;
  	$scope.toggleTwo = function() {
        $scope.coltwo = !$scope.coltwo;  
        $("a[data-target='#collapseTwo']").click();
    };
  	$scope.colthree = false;
  	$scope.toggleThree = function() {
        $scope.colthree = !$scope.colthree;  
        $("a[data-target='#collapseThree']").click();
    };
    $scope.colfour = false;
  	$scope.toggleFour = function() {
        $scope.colfour = !$scope.colfour; 
        $("a[data-target='#collapseFour']").click();
    };
    
    // commenting 2 questions as per (CSO-348 UAT - Please remove two questions from Transfer your cover option)

   /* var toggleFlag = false;
    $scope.toggleSection = function(){
	   if(toggleFlag && ($scope.tpdClaimTrans == 'Yes' || $scope.terminalIllnessClaimTrans == 'Yes')){
		   	$scope.colfour = false; 
	        $("a[data-target='#collapseFour']").click();
	   } else if(toggleFlag && $scope.tpdClaimTrans == 'No' && $scope.terminalIllnessClaimTrans == 'No'){
		   $scope.colfour = true; 
	        $("a[data-target='#collapseFour']").click();
	   }
   };*/
   
   
    while($scope.personalDetails){
		if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
			$scope.genderFlag =  false;
			$scope.gender = '';
			var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion'/*,'areyouperCitzTransferQuestion'*/,'transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','gender','annualTransferSalary'];
		    var occupationDetailsOtherTransferFormFields = ['fifteenHrsTransferQuestion'/*,'areyouperCitzTransferQuestion'*/,'transferIndustry','occupationTransfer','transferotherOccupation','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','gender','annualTransferSalary'];
		    
		} else{
			$scope.genderFlag =  true;
			$scope.gender = $scope.personalDetails.gender;
			var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion'/*,'areyouperCitzTransferQuestion'*/,'transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];
		    var occupationDetailsOtherTransferFormFields = ['fifteenHrsTransferQuestion'/*,'areyouperCitzTransferQuestion'*/,'transferIndustry','occupationTransfer','transferotherOccupation','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];
		    
		}
		break;
	}
    
    var coverDetailsTransferFormFields = ['coverDetailsTransferEmail', 'coverDetailsTransferPhone','coverDetailsTransferPrefTime'];
    /*var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];*/
    /*var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];
    var occupationDetailsOtherTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','transferIndustry','occupationTransfer','transferotherOccupation','withinOfficeTransferQuestion','tertiaryTransferQuestion','managementTransferRole','annualTransferSalary'];*/
    /*var previousCoverFormFields = ['gender','previousFundName','membershipNumber','spinNumber','documentName'];*/
    var previousCoverFormFields = ['previousFundName','membershipNumber',/*'spinNumber',*/'documentName'/*,'tpdClaimTrans','terminalIllnessClaimTrans'*/];
    var previousCoverFormFieldsWithChkBox = ['previousFundName','membershipNumber',/*'spinNumber',*/'documentName','ackDocument2'/*,'tpdClaimTrans','terminalIllnessClaimTrans'*/];
    var TranscoverCalculatorFormFields = ['TransDeathRequireCover','TransTPDRequireCover','TransIPRequireCover'];
    $scope.checkCoverDetailsTransferFormPreviousMandatoryFields  = function (elementName,formName){
    	var transferFormFields;
    	if(formName == 'coverDetailsTransferForm'){
    		transferFormFields = coverDetailsTransferFormFields;
    	} else if(formName == 'occupationDetailsTransferForm'){
    		//transferFormFields = occupationDetailsTransferFormFields;
    		if($scope.occupationTransfer != undefined && $scope.occupationTransfer == 'Other'){
    			transferFormFields = occupationDetailsOtherTransferFormFields;
    		} else{
    			transferFormFields = occupationDetailsTransferFormFields;
    		}
    	} else if(formName == 'previousCoverForm'){
    		if($scope.documentName != undefined && $scope.documentName =='No'){
    			transferFormFields = previousCoverFormFieldsWithChkBox;
    		} else{
    			transferFormFields = previousCoverFormFields;
    		}
    		//transferFormFields = previousCoverFormFields;
    	} 
    	/*else if(formName == 'TranscoverCalculatorForm'){
    		transferFormFields = TranscoverCalculatorFormFields;
    	}*/
      var inx = transferFormFields.indexOf(elementName);
      //console.log(elementName, inx);
      if(inx > 0){
        for(var i = 0; i < inx ; i++){
          $scope[formName][transferFormFields[i]].$touched = true;
        }
      }
    };

    $scope.CoverDetailsTransferFormSubmit =  function (form){
      if(!form.$valid){
    	  form.$submitted=true; 
	  } else{
		  if(form.$name == 'coverDetailsTransferForm')
			  {
    	    $scope.toggleTwo();
			  }
		  else if(form.$name == 'occupationDetailsTransferForm')
			  {
			  $scope.toggleThree();
			  }
		  else if(form.$name == 'previousCoverForm'){
			 // if($scope.tpdClaimTrans == 'No' && $scope.terminalIllnessClaimTrans == 'No'){
				  $scope.toggleFour();
			//	  toggleFlag = true;
			//  }
		  }else if(form.$name == 'TranscoverCalculatorForm'){
			  if(!$scope.coverAmtErrFlag){
				  $scope.calculateTransfer(); 
			  }
		  }
       }
      //console.log("Form Validation");
    };
    
  /*  $scope.industryTransferOptions = [{
    	options: 'option1',
    	industryName: 'Industry 1'
    }, {
    	options: 'option2',
    	industryName: 'Industry 2'
    }
    ];*/
    
    $scope.occupationTransferOptions = [{
    	options: 'option1',
    	occupationName: 'Occupation 1'
      }, {
    	  options: 'option2',
    	  occupationName: 'Occupation 2'
      }
    ];
    
   
    $scope.files = [];
    $scope.selectedFile = null;
    $scope.uploadFiles = function(files, errFiles) {
    	$scope.selectedFile =  files[0] ;
    };
    $scope.addFilesToStack = function () {
		var fileSize = ($scope.selectedFile.size / 1048576).toFixed(3);
		if(fileSize > 10) {
			$scope.fileSizeErrFlag=true;
			$scope.fileSizeErrorMsg ="File size should not be more than 10MB";
			$scope.selectedFile = null;
			return;
		}else{
			$scope.fileSizeErrFlag=false;
		}
		$scope.files.push($scope.selectedFile);
		$scope.selectedFile = null;
	}
   
   
	var uploadedFiles = [];
	$scope.submitFiles = function () {
		if(!$scope.files){
			$scope.files = [];
		}
		var upload;
		var numOfFiles = $scope.files.length;
        angular.forEach($scope.files, function(file) {
	    	upload = Upload.http({
	    		url: 'http://localhost\:8087/fileUpload',
	    		headers : {
	    			'Content-Type': file.name
	    		},
	    		data: file
			});
	    	upload.then(function(res){
	    		uploadedFiles.push(res.data);
	    		numOfFiles--;
	    		if(numOfFiles == 0){
	    			PersistenceService.setUploadedFileDetails(uploadedFiles);
	    		}
	    	}, function(err){
	    		console.log("Error uploading the file " + err);
	    	});
        });
	};

    
    $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
	$scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
	$scope.ipCoverTransDetails = ipCoverService.getIpCover();
	
	$scope.waitingPeriodTransOptions = ["14 Days", "30 Days", "45 Days", "60 Days", "90 Days", "Not Listed"];
    $scope.benefitPeriodTransOptions = ["2 Years", "3 Years", "5 Years", "Age 60", "Age 65", "Not Listed"];
    
    $scope.waitingPeriodTransAdlnOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodTransAdlnOptions = ['2 Years', '5 Years'];
    
    $scope.preferredContactTransOptions = ['Mobile','Office','Home'];
    
    $scope.regex = /[0-9]{1,3}/;
    $scope.dcTransCoverAmount = 0.00;
	$scope.dcTransCost = 0.00;
	$scope.tpdTransCoverAmount = 0.00;
	$scope.tpdTransCost = 0.00;
	$scope.ipTransCoverAmount = 0.00;
	$scope.ipTransCost = 0.00;
	$scope.totalTransCost = 0.00;
	
	if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.waitingPeriod && $scope.ipCoverTransDetails.waitingPeriod != ''){
			$scope.waitingPeriodTransPer = $scope.ipCoverTransDetails.waitingPeriod;
			$scope.waitingPeriodTransAddnl = $scope.ipCoverTransDetails.waitingPeriod;
	} else {
		$scope.waitingPeriodTransPer = '30 Days';
		$scope.waitingPeriodTransAddnl = '30 Days';
	}
	
	if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitPeriod && $scope.ipCoverTransDetails.benefitPeriod != ''){
			$scope.benefitPeriodTransPer = $scope.ipCoverTransDetails.benefitPeriod;
			$scope.benefitPeriodTransAddnl = $scope.ipCoverTransDetails.benefitPeriod;
	} else{
		$scope.benefitPeriodTransPer = '2 Years';
		$scope.benefitPeriodTransAddnl = '2 Years';
	}
	
	 $scope.changeWaitingPeriod = function() {
		 if(($scope.waitingPeriodTransPer == '14 Days') || ($scope.waitingPeriodTransPer == '30 Days')){
				$scope.waitingPeriodTransAddnl = '30 Days';
		 }else  if(($scope.waitingPeriodTransPer == '45 Days') || ($scope.waitingPeriodTransPer == '60 Days')){
				$scope.waitingPeriodTransAddnl = '60 Days';
		 }else if($scope.waitingPeriodTransPer == '90 Days'){
			    $scope.waitingPeriodTransAddnl = '90 Days';
		 } else if($scope.waitingPeriodTransPer == 'Not Listed'){
			 $scope.waitingPeriodTransAddnl = '90 Days';
 		 }
		 $scope.autoCalculate();
      };
      
      $scope.clickToOpen = function (hhText) {
        	
  		var dialog = ngDialog.open({
  			/*template: '<p>'+hhText+'</p>' +
  				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
  				template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hint</h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="">Close</button></div></div>',
  				className: 'ngdialog-theme-plain',
  				plain: true
  		});
  		dialog.closePromise.then(function (data) {
  			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
  		});
  	};
      
      $scope.changeBenefitPeriod = function() {
    		 if(($scope.benefitPeriodTransPer == '2 Years') || ($scope.benefitPeriodTransPer == '3 Years')){
					$scope.benefitPeriodTransAddnl = '2 Years';
    		 }else if($scope.benefitPeriodTransPer == '5 Years'){
					$scope.benefitPeriodTransAddnl = '5 Years';
	    		 } else if($scope.benefitPeriodTransPer == 'Not Listed'){
	    			 $scope.benefitPeriodTransAddnl = '2 Years';
	    		 } else if($scope.benefitPeriodTransPer == 'Age 65'){
	    			 $scope.benefitPeriodTransAddnl = '5 Years';
	    		 } else if($scope.benefitPeriodTransPer == 'Age 60'){
	    			 if(anb < 57){
	    				 $scope.benefitPeriodTransAddnl = '5 Years';
	    			 } else{
	    				 $scope.benefitPeriodTransAddnl = '2 Years';
	    			 }
	    		 } 
    		 		$scope.autoCalculate();
		      };
		      
		      $scope.transferAckFlag = false;
			  var transferAckCheck;
			  
			  $scope.checkTransferAckState = function(){
			    	$timeout(function(){
			    		transferAckCheck = $('#transferTermsLabel').hasClass('active');
			    		if(transferAckCheck){
			    			$scope.transferAckFlag = false;
			    		} else{
			    			$scope.transferAckFlag = true;
			    		}
			    	}, 10);
			    };
			    var ackDocument;
		      $scope.saveDataForPersistence = function(){
			    	var coverObj = {};
			    	var transferOccObj={};
			    	var transferDeathAddnlObj={};
			    	var transferTpdAddnlObj={};
			    	var transferIpAddnlObj={};
			    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
			    		return $scope.transferIndustry == obj.key;
			    	});
			    	
			    	
						    	coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
						    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
						    	coverObj['country'] =persoanlDetailService.getMemberDetails()[0].address.country;
						    	coverObj['email'] = $scope.transferEmail;
						    	transferOccObj['gender'] = $scope.gender;
						    	coverObj['contactType']=$scope.preferredContactType;
						    	coverObj['contactPhone'] = $scope.transferPhone;
						    	coverObj['contactPrefTime'] = $scope.TransferTime;
						    	
						    	transferOccObj['fifteenHr'] = $scope.fifteenHrsTransferQuestion;
						    	/*transferOccObj['citizenQue'] = $scope.areyouperCitzTransferQuestion;*/
						    	transferOccObj['industryName'] = selectedIndustry[0].value;
						    	transferOccObj['industryCode'] = selectedIndustry[0].key;
						    	transferOccObj['occupation'] = $scope.occupationTransfer;
						    	transferOccObj['withinOfficeQue']= $scope.withinOfficeTransferQuestion;
						    	transferOccObj['tertiaryQue']= $scope.tertiaryTransferQuestion;
						    	transferOccObj['managementRoleQue']= $scope.managementTransferRole;
						    	transferOccObj['salary'] = $scope.annualTransferSalary;
						    	transferOccObj['otherOccupation'] = $scope.otherOccupationObj.transferotherOccupation;
						    	
						    	coverObj['previousFundName'] = $scope.previousFundName;
						    	coverObj['membershipNumber'] = $scope.membershipNumber;
						    	coverObj['spinNumber'] = $scope.spinNumber;
						    	coverObj['documentName'] = $scope.documentName;
						    //	coverObj['uploadedDocument'] = $scope.selectedFile.name;
						    	ackDocument = $('#acknowledgeDocAdressCheck').hasClass('active');
						    	if(ackDocument){
						    		coverObj['documentAddress'] = ackDocument;
						    	}
						    //	coverObj['documentAddressCheckbox'] =acknowledgeDocAdressCheckState; 
						    	/*coverObj['previousTpdClaimQue'] = $scope.tpdClaimTrans;
						    	coverObj['terminalIllClaimQue'] = $scope.terminalIllnessClaimTrans;*/
						    	
						    	coverObj['transferDeathExistingAmt'] = $scope.deathCoverTransDetails.amount;
						    	coverObj['transferTpdExistingAmt'] = $scope.tpdCoverTransDetails.amount;
						    	coverObj['deathOccCategory'] = $scope.transferDeathOccupationCategory;
						    	coverObj['tpdOccCategory'] = $scope.transferTpdOccupationCategory;
						    	coverObj['ipOccCategory'] = $scope.transferIpOccupationCategory;
						    	coverObj['transferIpExistingAmt'] = $scope.ipCoverTransDetails.amount;
						    	coverObj['transferIpWaitingPeriod'] = $scope.waitingPeriodTransPer;
						    	coverObj['transferIpBenefitPeriod'] = $scope.benefitPeriodTransPer;
						    	
						    	transferDeathAddnlObj['deathTransferAmt'] = $scope.TransDeathRequireCover;
						    	transferDeathAddnlObj['deathTransferCovrAmt'] = $scope.dcTransCoverAmount;
						    	transferDeathAddnlObj['deathTransferWeeklyCost'] = $scope.dcTransCost;
						    	transferDeathAddnlObj['deathTransferCoverType'] = 'DcFixed';
						    	
						    	transferTpdAddnlObj['tpdTransferAmt'] = $scope.TransTPDRequireCover;
						    	transferTpdAddnlObj['tpdTransferCovrAmt'] = $scope.tpdTransCoverAmount;
						    	transferTpdAddnlObj['tpdTransferWeeklyCost'] = $scope.tpdTransCost;
						    	transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDFixed';
						    	
						    	
						    	transferIpAddnlObj['ipTransferAmt'] = $scope.TransIPRequireCover;
						    	transferIpAddnlObj['ipTransferCovrAmt'] = $scope.ipTransCoverAmount;
						    	transferIpAddnlObj['ipTransferWeeklyCost'] = $scope.ipTransCost;
						    	transferIpAddnlObj['addnlTransferWaitingPeriod'] = $scope.waitingPeriodTransAddnl;
						    	transferIpAddnlObj['addnlTransferBenefitPeriod'] = $scope.benefitPeriodTransAddnl;
						    	transferIpAddnlObj['ipTransferCoverType'] = 'IpFixed';
						    	
						    	coverObj['totalPremium']=$scope.totalTransCost;
						    	coverObj['autoCalculateFlag']=autoCalculate;
						    	coverObj['appNum'] = appNum;
						    	coverObj['transferAckCheck'] = transferAckCheck;
						    	coverObj['lastSavedOn'] = 'Transferpage';
						    	coverObj['manageType'] = 'TCOVER';
				                coverObj['partnerCode'] = 'METL';
				                coverObj['freqCostType'] = 'Weekly';
				                coverObj['age'] = anb;
						    	
						    	PersistenceService.settransferCoverDetails(coverObj);
						    	PersistenceService.setTransferCoverOccDetails(transferOccObj);
						    	PersistenceService.setTransferDeathAddnlDetails(transferDeathAddnlObj);
						    	PersistenceService.setTransferTpdAddnlDetails(transferTpdAddnlObj);
						    	PersistenceService.setTransferIpAddnlDetails(transferIpAddnlObj);
						
			    };
			    
			    if($routeParams.mode == 2){
			    	var existingDetails = PersistenceService.gettransferCoverDetails();
			    	var occDetails =PersistenceService.getTransferCoverOccDetails();
			    	var deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
			    	var tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
			    	var ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();
			    	
			    	$scope.transferEmail = existingDetails.email;
			    	$scope.preferredContactType = existingDetails.contactType;
			    	$scope.transferPhone = existingDetails.contactPhone;
			    	$scope.TransferTime = existingDetails.contactPrefTime;
			    	
			    	$scope.fifteenHrsTransferQuestion = occDetails.fifteenHr;
			    	/*$scope.areyouperCitzTransferQuestion = occDetails.citizenQue;*/
			    	$scope.transferIndustry = occDetails.industryCode;
			    	//$scope.occupationTransfer = occDetails.occupation;
			    	$scope.withinOfficeTransferQuestion = occDetails.withinOfficeQue;
			    	$scope.tertiaryTransferQuestion = occDetails.tertiaryQue;
			    	$scope.managementTransferRole = occDetails.managementRoleQue;
			    	$scope.annualTransferSalary = occDetails.salary;
			    	$scope.gender = occDetails.gender;
			    	$scope.otherOccupationObj.transferotherOccupation = occDetails.otherOccupation;
			    	
			    	$scope.previousFundName=existingDetails.previousFundName;
			    	$scope.membershipNumber=existingDetails.membershipNumber;
			    	$scope.spinNumber=existingDetails.spinNumber;
			    	$scope.documentName=existingDetails.documentName;
			    //	$scope.selectedFile.name=existingDetails.uploadedDocument;
			    	ackDocument=existingDetails.documentAddress;
			    //	$scope.acknowledgeDocAdressCheckState=existingDetails.documentAddressCheckbox;
			    	/*$scope.tpdClaimTrans=existingDetails.previousTpdClaimQue;
			    	$scope.terminalIllnessClaimTrans=existingDetails.terminalIllClaimQue;*/
			    	
			    	
			    	$scope.deathCoverTransDetails.amount=existingDetails.transferDeathExistingAmt;
			    	$scope.transferDeathOccupationCategory=existingDetails.deathOccCategory;
			    	$scope.transferTpdOccupationCategory=existingDetails.tpdOccCategory;
			    	$scope.transferIpOccupationCategory=existingDetails.ipOccCategory;
			    	$scope.tpdCoverTransDetails.amount=existingDetails.transferTpdExistingAmt;
			    	$scope.ipCoverTransDetails.amount=existingDetails.transferIpExistingAmt;
			    	$scope.waitingPeriodTransPer=existingDetails.transferIpWaitingPeriod;
			    	$scope.benefitPeriodTransPer=existingDetails.transferIpBenefitPeriod;
			    	
			    	$scope.TransDeathRequireCover=deathAddnlCvrDetails.deathTransferAmt;
			    	$scope.dcTransCoverAmount=deathAddnlCvrDetails.deathTransferCovrAmt;
			    	$scope.dcTransCost=deathAddnlCvrDetails.deathTransferWeeklyCost;
			    	
			    	$scope.TransTPDRequireCover=tpdAddnlCvrDetails.tpdTransferAmt;
			    	$scope.tpdTransCoverAmount=tpdAddnlCvrDetails.tpdTransferCovrAmt;
			    	$scope.tpdTransCost=tpdAddnlCvrDetails.tpdTransferWeeklyCost;
			    	
			    	
			    	$scope.TransIPRequireCover=ipAddnlCvrDetails.ipTransferAmt;
			    	$scope.ipTransCoverAmount=ipAddnlCvrDetails.ipTransferCovrAmt;
			    	$scope.ipTransCost=ipAddnlCvrDetails.ipTransferWeeklyCost;
			    	$scope.waitingPeriodTransAddnl=ipAddnlCvrDetails.addnlTransferWaitingPeriod;
			    	$scope.benefitPeriodTransAddnl=ipAddnlCvrDetails.addnlTransferBenefitPeriod;
			    	
			    	$scope.totalTransCost=existingDetails.totalPremium;
			    	autoCalculate=existingDetails.autoCalculateFlag;
			    	appNum = existingDetails.appNum;
			    	transferAckCheck = existingDetails.transferAckCheck;
			    	
			    	$scope.files = PersistenceService.getUploadedFileDetails();
			    	
			    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"METL",$scope.transferIndustry).then(function(res){
			    	//OccupationService.getOccupationList({fundId:"CARE", induCode:$scope.transferIndustry}, function(occupationList){
			        	$scope.OccupationList = res.data;
			        	var temp = $scope.OccupationList.filter(function(obj){
			        		return obj.occupationName == occDetails.occupation;
			        	});
			        	$scope.occupationTransfer = temp[0].occupationName;
			        }, function(err){
			        	console.log("Error while getting occupatio list " + JSON.stringify(err));
			        });
			    	
			    	if(transferAckCheck){
			    	    $timeout(function(){
			    			$('#transferTermsLabel').addClass('active');
			    			   });
			    			 }
			    	/*if(acknowledgeDocAdressCheckState){
			    		$('#acknowledgeDocAdressCheck').parent().addClass('active');
			    	}*/
			    	
			    	if(ackDocument){
			    		$timeout(function(){
			    		$('#acknowledgeDocAdressCheck').addClass('active');
			    		});
			       	}
			    	$scope.toggleTwo();
			    	$scope.toggleThree();
			    	$scope.toggleFour();
			    	
			    };
			    
			    $scope.goToAura = function(){
			    	if(this.coverDetailsTransferForm.$valid && this.occupationDetailsTransferForm.$valid && this.previousCoverForm.$valid && this.TranscoverCalculatorForm.$valid){
			    	
			    	$timeout(function(){
		    		transferAckCheck = $('#transferTermsLabel').hasClass('active');
		    		if(transferAckCheck){
		    			$scope.transferAckFlag = false;
		    			$scope.saveDataForPersistence();
						  // submit uploaded to server
						  $scope.submitFiles();
			    	$scope.go('/auratransfer/1');
			    	
		    		}else{
		    				$scope.transferAckFlag = true;
		    		}
			      }, 10);
			     }
			    };
			    
			    $scope.invalidSalAmount = false;
			    $scope.checkValidSalary = function(){
			    	if(parseInt($scope.annualTransferSalary) == 0){
			  			$scope.invalidSalAmount = true;
			  		} else{
			  			$scope.invalidSalAmount = false;
			  		}
			    };
			  /*  $scope.saveQuoteTransfer = function(){
			    	$scope.transferQuoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
			    	$scope.saveDataForPersistence();
			    	$scope.quoteTransferObject =  PersistenceService.gettransferCoverDetails();
			    	$scope.transferOccDetails =PersistenceService.getTransferCoverOccDetails();
			    	$scope.deathAddnlTransferCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
			    	$scope.tpdAddnlTransferCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
			    	$scope.ipAddnlTransferCvrDetails=PersistenceService.getTransferIpAddnlDetails();
		    	    $scope.personalDetails = persoanlDetailService.getMemberDetails();
		    	    if($scope.quoteTransferObject != null && $scope.transferOccDetails != null && $scope.deathAddnlTransferCvrDetails != null && $scope.tpdAddnlTransferCvrDetails != null &&
		    	    		$scope.ipAddnlTransferCvrDetails != null && $scope.personalDetails[0] != null){
		    	    	$scope.details = {};
		    	    	$scope.details.deathAddnlTransferDetails = $scope.deathAddnlTransferCvrDetails;
		    	    	$scope.details.tpdAddnlTransferDetails = $scope.tpdAddnlTransferCvrDetails;
		    	    	$scope.details.ipAddnlTransferCvrDetails = $scope.ipAddnlTransferCvrDetails;
		    	    	$scope.details.transferOccDetails = $scope.transferOccDetails;
		    	    	var temp = angular.extend($scope.quoteTransferObject,$scope.details)
		        	    var saveQuoteTransferObject = angular.extend(temp,$scope.personalDetails[0]);
		    	    	auraResponseService.setResponse(saveQuoteTransferObject)
		    	        saveEapply.reqObj().then(function(response) {  
		    	                console.log(response.data)
		    	        });
		    	    }
			    };*/
			    
			    /*$scope.transferQuoteSaveAndExitPopUp = function (hhText) {
			      	
					var dialog1 = ngDialog.open({
						    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
							className: 'ngdialog-theme-plain custom-width',
							preCloseCallback: function(value) {
							       var url = "/landing"
							       $location.path( url );
							       return true
							},
							plain: true
					});
					dialog1.closePromise.then(function (data) {
						console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
					});
				};*/
				
		      
    }]); 
    
 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */

CareSuperApp.directive('phoneOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
            
            $(element).on("keyup", function() {
            	var TempVal=$(this).val().replace(" ","");
             	var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            	if( !regex.test(TempVal)) {
            		// element.controller('ngModel').$setValidity('required', false);
            		// element.controller('ngModel').$touched = true;
            	}
            	
            })
        }
    };
});
  