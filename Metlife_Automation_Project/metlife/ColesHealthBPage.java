package pages.metlife;

import java.util.List;

import org.openqa.selenium.WebElement;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthBPageObjects;
import uimap.metlife.PersonalStatementPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesHealthBPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesHealthBPage enterColesHealthBPage() {
		fillColesHealthB();
		return new ColesHealthBPage(scriptHelper);
	}
	
	public ColesHealthBPage enterLoadinfColesHealthBPage() {
		fillLoadingColesHealthB();
		return new ColesHealthBPage(scriptHelper);
	}
	
	
	public ColesHealthBPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillColesHealthB() {
		try {
			String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
			String strSmokeMoreThan30 = dataTable.getData("MetLife_Data", "MoreThan30");
			String strSmokeMoreThan50 = dataTable.getData("MetLife_Data", "MoreThan50");
			String strCoverAmt = dataTable.getData("MetLife_Data", "CoverAmount");
			

			if(strSmoke.equalsIgnoreCase("yes"))
			{
				if(strSmokeMoreThan30.equalsIgnoreCase("yes"))
				{
					tryAction(waitForClickableElement(ColesHealthBPageObjects.radMoreThan30Yes),"Click", "Do you currently smoke more than 30 cigarettes (or equivalent) on average per day? "+strSmokeMoreThan30);
					tryAction(strSmokeMoreThan50.equalsIgnoreCase("yes")?waitForClickableElement(ColesHealthBPageObjects.radMoreThan50Yes):waitForClickableElement(ColesHealthBPageObjects.radMoreThan50No),"Click", "Do you currently smoke more than 50 cigarettes (or equivalent) on average per day? "+strSmokeMoreThan50);
				}
				else{
					tryAction(waitForClickableElement(ColesHealthBPageObjects.radMoreThan30No),"Click", "Do you currently smoke more than 30 cigarettes (or equivalent) on average per day? "+strSmokeMoreThan30);
				}
			}
			
			tryAction(waitForClickableElement(ColesHealthBPageObjects.chkSufferedFrom),"Click", "Have you suffered,diagnosed in last 5 years - None");
						
			
			tryAction(
					waitForClickableElement(ColesHealthBPageObjects.rdAbtUrStyleNo),
					"Click", "Used Drugs in last 5 years - No");
			fillInInsuranceDetails();
			
			tryAction(
					waitForClickableElement(ColesHealthBPageObjects.rdAbtMedicalHistoryNo),
					"Click", "Any Medical Advise - No");
			
			tryAction(
					waitForClickableElement(ColesHealthBPageObjects.rdAbtInsuranceHistoryNo),
					"Click", "Any Insurance History - No");
			tryAction(waitForClickableElement(ColesHealthBPageObjects.lnkNext),"MoveToElm","Next");
			sleep();
			tryAction(waitForClickableElement(ColesHealthBPageObjects.lnkNext),"Click", "Next");
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	public void fillLoadingColesHealthB() {
		try {
			
			String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
			String strSmokeMoreThan30 = dataTable.getData("MetLife_Data", "MoreThan30");
			String strSmokeMoreThan50 = dataTable.getData("MetLife_Data", "MoreThan50");
			String strHealth5Years = dataTable.getData("MetLife_Data", "healthb.5years");
			
			

			if(strSmoke.equalsIgnoreCase("yes"))
			{
				if(strSmokeMoreThan30.equalsIgnoreCase("yes"))
				{
					tryAction(waitForClickableElement(ColesHealthBPageObjects.radMoreThan30Yes),"Click", "Do you currently smoke more than 30 cigarettes (or equivalent) on average per day? "+strSmokeMoreThan30);
					tryAction(strSmokeMoreThan50.equalsIgnoreCase("yes")?waitForClickableElement(ColesHealthBPageObjects.radMoreThan50Yes):waitForClickableElement(ColesHealthBPageObjects.radMoreThan50No),"Click", "Do you currently smoke more than 50 cigarettes (or equivalent) on average per day? "+strSmokeMoreThan50);
				}
				else{
					tryAction(waitForClickableElement(ColesHealthBPageObjects.radMoreThan30No),"Click", "Do you currently smoke more than 30 cigarettes (or equivalent) on average per day? "+strSmokeMoreThan30);
				}
			}
			sleep();
			if(strHealth5Years.equalsIgnoreCase("none"))
			{
				tryAction(
						waitForClickableElement(ColesHealthBPageObjects.chkSufferedFrom),
						"Click", "Have you suffered,diagnosed in last 5 years - None");
			}else if(strHealth5Years.equalsIgnoreCase("lung")){
				tryAction(waitForClickableElement(ColesHealthBPageObjects.chkSufferLung),"Click", "Lung or Breating Conditions");
				tryAction(waitForClickableElement(ColesHealthBPageObjects.chkSufferAsthma),"Click", "Asthma");
				tryAction(waitForClickableElement(ColesHealthBPageObjects.radMedicalAttentionYes),"Click", "Have you had an episode or symptoms requiring medical attention in the last 12 months? - Yes");
				tryAction(waitForClickableElement(ColesHealthBPageObjects.radAsthmaModerate),"Click", "Moderate");
			}else{
				tryAction(
						waitForClickableElement(ColesHealthBPageObjects.chkSufferedFrom),
						"Click", "Have you suffered,diagnosed in last 5 years - None");
			}
			
			fillInInsuranceDetails();
			
			tryAction(
					waitForClickableElement(ColesHealthBPageObjects.rdAbtMedicalHistoryNo),
					"Click", "Any Medical Advise - No");
			sleep(500);
			tryAction(
					waitForClickableElement(ColesHealthBPageObjects.rdAbtInsuranceHistoryNo),
					"Click", "Any Insurance History - No");

			tryAction(waitForClickableElement(ColesHealthBPageObjects.lnkNext),"MoveToElm","Next");
			sleep();
			tryAction(waitForClickableElement(ColesHealthBPageObjects.lnkNext),"Click", "Next");
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInInsuranceDetails() {
		
		
		try{
			String strflyBusMem = dataTable.getData("MetLife_Data", "FlyBuysMemeber");
			String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
			List<WebElement> radLifeStyleDetails = null;
			radLifeStyleDetails = fluentWaitListElements(ColesHealthBPageObjects.radListAbtUrLifeStyleNo);
			tryAction(fluentWaitElement(radLifeStyleDetails.get(0)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you - No?");
			sleep();
			// added by Sandeep
			//if(!strflyBusMem.equalsIgnoreCase("No")){
			tryAction(fluentWaitElement(ColesHealthBPageObjects.txtAlcoholQntity),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			tryAction(fluentWaitElement(ColesHealthBPageObjects.btnEnter),"Click"," Enter");
			sleep();
			tryAction(fluentWaitElement(ColesHealthBPageObjects.radLifestyle2),"Click","Have you ever been advised by a health professional to reduce your alcohol consumption? - No");
			sleep();
			tryAction(fluentWaitElement(ColesHealthBPageObjects.radLifestyle3),"Click","Are you in a high risk category for contracting HIV?");
			sleep();
			tryAction(fluentWaitElement(ColesHealthBPageObjects.radLifestyle4),"Click","Are you  waiting on an HIV test result?");
			//}
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
