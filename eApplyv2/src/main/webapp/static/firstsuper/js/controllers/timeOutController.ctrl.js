/*Landing Page Controller Starts*/    
FirstSuperApp.controller('timeOutController',['$scope', '$location',  function($scope, $location){
   
	// since onpopstate event is not detected by IE
	if (navigator.appVersion.toString().indexOf('.NET') > 0){
		window.onhashchange = function (e) { window.history.forward(1); }
	} else{
		window.onpopstate = function (e) { window.history.forward(1); }
	}
}]); 
/*Landing Page Controller Ends*/