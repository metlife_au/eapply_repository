package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;

public class Exclusion implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String text;
	private String[] reason;
	

	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getReason() {
		return reason;
	}
	public void setReason(String[] reason) {
		this.reason = reason;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
