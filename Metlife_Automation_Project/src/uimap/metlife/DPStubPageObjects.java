package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class DPStubPageObjects extends PageObjectLibrary {
	public static final By drpdownProduct = getObjectIdentifaction("test.stub.landingpage.product.dropdown.css");
	public static final By drpdownbrand = getObjectIdentifaction("test.stub.landingpage.brand.dropdown.css");
	public static final By drpdowntrackingcode = getObjectIdentifaction("test.stub.landingpage.trackingcode.dropdown.css");
	public static final By btnApply = getObjectIdentifaction("test.stub.landingpage.apply.btn.css");
}
