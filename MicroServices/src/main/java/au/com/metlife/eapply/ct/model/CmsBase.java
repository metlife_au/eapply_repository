package au.com.metlife.eapply.ct.model;

import java.io.Serializable;

/*import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.springframework.beans.factory.annotation.Autowired;
*/
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/*
import au.com.metlife.webservices.CMSSearchStub;
import au.com.metlife.webservices.CMSSearchStub.ToolTips;
import au.com.metlife.webservices.PWCBClientHandler;
*/
@Component
@Scope("singleton")

public class CmsBase implements Serializable{

/*	private HashMap<String, Object> cachedData;
	private static ConfigurationContext ctx =null;	
	private static String  clientConfig = null;
	public final static String FUND_INFO = "FundInfo";
	public static final String FUND_ID_LIST = "FundIdList";
	public static final String AURA_TOOL_TIPS = "AuraToolTips";
	public final static String ALL_FUND_INFO = "MetLifeFundList";
public CmsBase() {
		
	cachedData = loadCmsData();
		
	}

public HashMap<String, Object> getCmsData(){

	return cachedData;
}

public HashMap<String, Object> loadCmsData(){
	String argsPass[] = new String[2];
	String cmsWebServiceURL="https://www.e2e.equery.metlife.com.au/CAW2/services/CMSSearch";
	argsPass[0] =cmsWebServiceURL;
	CMSSearchStub stub1 = null;
	ServiceClient client = null;
	HashMap<String, Object> cachedDataMap= new HashMap<String, Object>(); 
	CMSSearchStub.FundCMSInfo[] cmsFundInfo=null;
	try{
		
				stub1 = new CMSSearchStub(getInstance(), argsPass[0]);
				log.info("stub1>> {}",stub1);
				client = stub1._getServiceClient();
				
				client.engageModule("rampart");
				Options options = client.getOptions();
				options.setTimeOutInMilliSeconds(50000);
				options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY,
				getOutflowConfiguration("syselodg"));
				PWCBClientHandler myCallback = new PWCBClientHandler();
				myCallback.setUTUsername("syselodg");
				myCallback.setUTPassword("metlife890");
				options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
				client.setOptions(options);
		
				HashMap<String, Object> toolTipMap=new HashMap<String, Object>();
				CMSSearchStub.GetStyleforAllFundsResponse cmsAllFundsRes = stub1.getStyleforAllFunds();
				CMSSearchStub.GetAURAToolTipsResponse auraCMSToolTips=stub1.getAURAToolTips();
				
				
				CMSSearchStub.FundCMSReturnObject cmsFund=cmsAllFundsRes.get_return();
				CMSSearchStub.AuraToolTips auraCMSToolTipMess=auraCMSToolTips.get_return();
				ToolTips[] tempTooltips=auraCMSToolTipMess.getTips();
				for(int i=0;i<tempTooltips.length;i++)
				{
					
					toolTipMap.put(tempTooltips[i].getKey(),tempTooltips[i].getValue());
			}
				
				
				String list[] = cmsFund.getFundList();
				if(cmsFund != null){
				cmsFundInfo = cmsFund.getAllFundscmsContent(); 
				/* Start of Only for testing will be removed */
				
/*				for(int k=0;k<cmsFundInfo.length;k++){
					CMSSearchStub.FundCMSInfo obj =	(CMSSearchStub.FundCMSInfo) cmsFundInfo[k];
					log.info(obj.getFundId());
				}
*/				
				/* End of Only for testing will be removed */			
//				cachedDataMap.put(FUND_ID_LIST, list);						
//				cachedDataMap.put(ALL_FUND_INFO, cmsFundInfo);
//				cachedDataMap.put(AURA_TOOL_TIPS, toolTipMap);
//			
//				
//				}
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}finally{
//				try {
//					//Stub Clean
//					if(stub1!=null){
//						stub1.cleanup();
//					}
//					if(client!=null){
//						client.cleanup();
//						client.cleanupTransport();
//					}
//					
//					
//				} catch (AxisFault e) {
//					 e.printStackTrace();
//				}
//			}
//			/*End of CMS Integration*/
//	return cachedDataMap;
//	}	
//
//public static synchronized ConfigurationContext getInstance() throws Exception
//{
//	if(ctx ==  null)
//	{
//		clientConfig = "C:\\client_config\\";
//		ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(clientConfig, null);
//	}
//	return ctx;
//}
//private static Parameter getOutflowConfiguration(String username) {
//		OutflowConfiguration ofc = new OutflowConfiguration();
//		ofc.setActionItems("UsernameToken Timestamp");
//		ofc.setPasswordType("PasswordText");
//		ofc.setUser(username);
//		// ofc.setPasswordCallbackClass("au.com.metlife.webservices.PWCBClientHandler");
//
//		return ofc.getProperty();
//	}*/
}
