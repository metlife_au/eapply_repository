package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSPaymentsPageObjects extends PageObjectLibrary{
		
	public static final By rddebit = getObjectIdentifaction("OAMPS.payment.bank.radio.css");
//	public static final By rdcredit = getObjectIdentifaction("stft.payment.credit.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("OAMPS.payment.bank.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("OAMPS.payment.bank.accountnumber.text.id");
	public static final By txtbsbcode = getObjectIdentifaction("OAMPS.payment.bank.bsbcode.text.id");	
	public static final By txtbsbnumber = getObjectIdentifaction("OAMPS.payment.bank.bsbnumber.text.id");	
	public static final By chkacknowledge = getObjectIdentifaction("OAMPS.payment.agree.chkbox.css");
	public static final By btnsubmit = getObjectIdentifaction("OAMPS.payment.submit.btn.css");
}
