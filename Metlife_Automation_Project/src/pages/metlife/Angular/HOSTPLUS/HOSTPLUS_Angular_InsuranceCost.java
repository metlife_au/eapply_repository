package pages.metlife.Angular.HOSTPLUS;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_InsuranceCost extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_InsuranceCost InsuranceCost_HOSTPLUS() {
		InsuranceCost();
		return new HOSTPLUS_Angular_InsuranceCost(scriptHelper);
	}
	public HOSTPLUS_Angular_InsuranceCost(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void InsuranceCost() {

		String dateofbirth = dataTable.getData("InsuranceCost_Data", "DOB");
		String Gender = dataTable.getData("InsuranceCost_Data", "Gender");
		String FifteenPlusQuestion=dataTable.getData("InsuranceCost_Data", "Morethan_15HoursWork");
		String workindustry=dataTable.getData("InsuranceCost_Data", "WorkIndustry");
		String occupationtype=dataTable.getData("InsuranceCost_Data", "OccupationType");
		String OtherOccupation=dataTable.getData("InsuranceCost_Data", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("InsuranceCost_Data", "OfficeEnvironment");
	    String TertiaryQual = dataTable.getData("InsuranceCost_Data", "TertiaryQual");
		String HazardousEnv = dataTable.getData("InsuranceCost_Data", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("InsuranceCost_Data", "OutsideOfcPercent");
		String annualsalary=dataTable.getData("InsuranceCost_Data", "AnnualSalary");
		String costfrequncy=dataTable.getData("InsuranceCost_Data", "CostFrequency");
		String costtype=dataTable.getData("InsuranceCost_Data", "CostType");
		String deathamount=dataTable.getData("InsuranceCost_Data", "DeathAmount");
		String DeathYN=dataTable.getData("InsuranceCost_Data", "DeathYN");
		String TPDYN=dataTable.getData("InsuranceCost_Data", "TPDYN");
		String IPYN=dataTable.getData("InsuranceCost_Data", "IPYN");
		String tpdamount=dataTable.getData("InsuranceCost_Data", "TPDAmount");
		String deathunit=dataTable.getData("InsuranceCost_Data", "DeathUnit");
		String tpdunit=dataTable.getData("InsuranceCost_Data", "TPDUnit");
		String waitingperiod=dataTable.getData("InsuranceCost_Data", "WaitingPeriod");
		String benefitperiod=dataTable.getData("InsuranceCost_Data", "BenefitPeriod");
		String coverageunit=dataTable.getData("InsuranceCost_Data", "CoverageUnit");
		String expecteddeathcover=dataTable.getData("InsuranceCost_Data", "ExpectedDeathCover");
		String expectedtpdcover=dataTable.getData("InsuranceCost_Data", "ExpectedTPDCover");
		String expectedipcover=dataTable.getData("InsuranceCost_Data", "ExpectedIpCover");
		String Year=dataTable.getData("InsuranceCost_Data", "Year");



		try{

			//****Click on Transfer Your Cover Link*****\\
			WebDriverWait wait=new WebDriverWait(driver,18);

			//****Enter the DOB*****\\
			sleep(2500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("dob"))).click();
			//*****month********\\
		    sleep(1000);
		    Select monthselect=new Select(driver.findElement(By.className("ui-datepicker-month")));
		    monthselect.selectByVisibleText("Jan");
		    sleep(1500);
			report.updateTestLog("Month", "Month is: Jan", Status.PASS);

			//*****Year********\\
			sleep(3500);
		//    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@data-handler='selectYear']"))).click();

		//    sleep(3500);
		//	driver.findElement(By.xpath("//select[@data-handler='selectYear']/option[@value='1957']")).click();
			sleep(1000);
		    Select yearselect=new Select(driver.findElement(By.xpath("//select[@data-handler='selectYear']")));
		    yearselect.selectByValue(Year);
		    sleep(1500);
			report.updateTestLog("Year", "Year Selected is: "+Year, Status.PASS);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table/tbody/tr//a[text()='1'][@class='ui-state-default']"))).click();
			sleep(500);
			/*wait.until(ExpectedConditions.elementToBeClickable(By.id("dobId"))).sendKeys(dateofbirth);
			report.updateTestLog("Date Of Birth", dateofbirth+" is entered", Status.PASS);*/

			//****Select the Gender*****\\
			sleep(2500);
			if(Gender.equalsIgnoreCase("Male")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Male']/parent::*"))).click();
				report.updateTestLog("Gender", "Male is Selected", Status.PASS);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Female']/parent::*"))).click();
				report.updateTestLog("Gender", "Female is Selected", Status.PASS);
			}

		   //*****Do you work 15 or more hours per week?******\\

			if(FifteenPlusQuestion.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workHours' ]/parent::*)[1]"))).click();
				report.updateTestLog("Do you work 15 or more hours a week?", "Yes is Selected", Status.PASS);
			}

			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workHours' ]/parent::*)[2]"))).click();
				report.updateTestLog("Do you work 15 or more hours a week?", "No is Selected", Status.PASS);
			}



			//*****Industry********\\
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("industry")));
		    Industry.selectByVisibleText(workindustry);
		    sleep(500);
			report.updateTestLog("Industry", "Industry Selected is: "+workindustry, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();


			//*****Occupation*****\\

			Select Occupation=new Select(driver.findElement(By.name("occupation")));
			Occupation.selectByVisibleText(occupationtype);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+occupationtype, Status.PASS);
			sleep(1000);


			//*****Other Occupation *****\\

			/*if(OccupationType.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}*/



			//*****Other Occupation *****\\

			if(occupationtype.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);
			}
			sleep(5000);

			//*****Extra Questions*****\\

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='spendTimeInside' ]/parent::*)[1]"))).click();
		    	sleep(300);
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='spendTimeInside' ]/parent::*)[2]"))).click();
			sleep(300);
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

		}
			sleep(800);
		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQue' ]/parent::*)[1]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					sleep(300);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQue' ]/parent::*)[2]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				sleep(300);
		}
		}



		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



		//******Do you work in hazardous environment*****\\

		if(HazardousEnv.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workDuties' ]/parent::*)[1]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='workDuties' ]/parent::*)[2]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			sleep(300);
		}
		sleep(800);
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.id("(//input[@name='spendTimeInside_1' ]/parent::*)[1]"))).click();
			report.updateTestLog("10% work outside Office", "Selected Yes", Status.PASS);
			sleep(300);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.id("(//input[@name='spendTimeInside_1' ]/parent::*)[2]"))).click();
			report.updateTestLog("10% work outside Office", "Selected No", Status.PASS);
			sleep(300);
		}

		if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			 if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='spendTimeInside' ]/parent::*)[1]"))).click();
			    	sleep(300);
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
				sleep(300);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='spendTimeInside' ]/parent::*)[2]"))).click();
				sleep(300);
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

			}
				sleep(800);
			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQue' ]/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
						sleep(300);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQue' ]/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					sleep(300);
			}
		}
		}

          sleep(800);







        //*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(annualsalary);
		    sleep(500);
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+annualsalary, Status.PASS);

		/*//*****Display the cost of my insurance cover as****\\

		    sleep(1500);
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("premFreq"))).click();
		    sleep(500);
		    report.updateTestLog("Insurance Cost Frequency", costfrequncy+" is Selected", Status.PASS);*/


		    sleep(500);
		    Select insurnacecover=new Select(driver.findElement(By.name("premFreq")));
		    insurnacecover.selectByVisibleText(costfrequncy);
		    sleep(500);
			report.updateTestLog("Insurance Cost Frequency", "Insurance Cost Frequency Selected is: "+costfrequncy, Status.PASS);


		  //*****Select the Cost Type*****\\
		    if(DeathYN.equalsIgnoreCase("Yes")){
		    if(costtype.equalsIgnoreCase("Fixed")){

		    	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='deathcoverCategory' ]/parent::*)[1]"))).click();
		    	  sleep(2000);
				  report.updateTestLog("Cost Type", costtype+" is Selected", Status.PASS);
				  sleep(500);

				  //****Enter the Death Amount*****\\

				  wait.until(ExpectedConditions.elementToBeClickable(By.name("deathCoverAmount"))).sendKeys(deathamount);
		    	  sleep(2000);
		    	  driver.findElement(By.xpath("(//label[text()=' Cover Amount'])[1]")).click();
		    	  sleep(2000);
				  report.updateTestLog("Death Cover Amount", deathamount+" is entered", Status.PASS);
				  sleep(500);

               //****Enter the TPD Amount*****\\


				  wait.until(ExpectedConditions.elementToBeClickable(By.name("tpdCoverAmount"))).sendKeys(tpdamount);
		    	  sleep(2000);
		    	  driver.findElement(By.xpath("(//label[text()=' Cover Amount'])[1]")).click();
		    	  sleep(2000);
				  report.updateTestLog("TPD Cover Amount", tpdamount+" is entered", Status.PASS);
				  sleep(200);


		    }

		    else{
		    	sleep(500);
		    	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='deathcoverCategory' ]/parent::*)[2]"))).click();
		    	  sleep(2000);
				   report.updateTestLog("Cost Type", costtype+" is Selected", Status.PASS);
				   sleep(500);

					  //****Enter the Death Units*****\\

					  wait.until(ExpectedConditions.elementToBeClickable(By.name("deathCoverUnit"))).sendKeys(deathunit);
			    	  sleep(2000);
			    	  driver.findElement(By.xpath("(//label[text()=' Cover Amount'])[2]")).click();
			    	  sleep(2000);
					  report.updateTestLog("Death Cover Units", deathunit+" units is entered", Status.PASS);
					  sleep(200);

	               //****Enter the TPD Units*****\\

					  sleep(500);
					  wait.until(ExpectedConditions.elementToBeClickable(By.name("tpdCoverUnit"))).sendKeys(tpdunit);
			    	  sleep(2000);
			    	  driver.findElement(By.xpath("(//label[text()=' Cover Amount'])[2]")).click();
			    	  sleep(2000);
					  report.updateTestLog("TPD Cover Units", tpdunit+" units is entered", Status.PASS);
					  sleep(200);

		    }
		    }



		    //*****Enter the IP Cover Details*******\
		    sleep(800);
		  if(IPYN.equalsIgnoreCase("Yes")){

		    //******Select the Waiting Period******\\

			  sleep(500);
			    Select waiting=new Select(driver.findElement(By.name("ipWaitingPeriod")));
			    waiting.selectByVisibleText(waitingperiod);
			    sleep(500);
				report.updateTestLog("IP Waiting Period", waitingperiod+"  is Selected", Status.PASS);


		    //*****Select the Benefit Period******\\

				 sleep(500);
				    Select benefit=new Select(driver.findElement(By.name("ipBenefitPeriod")));
				    benefit.selectByVisibleText(benefitperiod);
				    sleep(500);
					report.updateTestLog("IP Waiting Period",benefitperiod+"  is Selected", Status.PASS);


		    //*****Select the IP Cover Amount Required*****\\

	//	    wait.until(ExpectedConditions.elementToBeClickable(By.id("ipRequredcvrId"))).click();
		    sleep(1000);
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("ipCoverAmount"))).click();
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("ipCoverAmount"))).sendKeys(coverageunit);

		    sleep(2000);
		    driver.findElement(By.xpath("(//label[text()=' Cover Amount'])[3]")).click();
		    sleep(2000);
		    report.updateTestLog("IP Cover Amount",coverageunit+"  is entered", Status.PASS);
		  }
		    sleep(1000);

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='disclaimer']/parent::*"))).click();
		    sleep(400);
		    report.updateTestLog("Acknowledgment checkbox ","Acknowledgment checkbox  is Clicked", Status.PASS);
            sleep(3000);

		    //******Click on Calculate Quote*******\\

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='p-3 offset-4 col-8 offset-md-7 col-md-5 offset-sm-6 col-sm-6 ng-scope']"))).click();
		    sleep(400);
		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
            sleep(3000);
/*
          String Totalcost=driver.findElement(By.xpath("(//h4[@class='coverval']/span)[2]")).getText();
          if(Totalcost.equalsIgnoreCase("$0.00")){
        	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calcQuoteId']/span"))).click();
  		    sleep(400);
  		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
           sleep(3000);
          }

          if(Totalcost.equalsIgnoreCase("$0.00")){
        	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='calcQuoteId']/span"))).click();
  		    sleep(400);
  		    report.updateTestLog("Calculate Quote","Calculate Quote button is Clicked", Status.PASS);
           sleep(3000);
          }

 */
			///****Fetch the Death cover and TPD cover amounts****\\\



				sleep(500);

				if(DeathYN.equalsIgnoreCase("Yes")){
	            WebElement DeathCover=driver.findElement(By.xpath("(//h5[@class='coverval ng-binding'])[2]"));
	            DeathCover.click();
	            sleep(500);
				String DeathCoveramount=DeathCover.getText();


				if(DeathCoveramount.equalsIgnoreCase(expecteddeathcover)){

			//		System.out.println("Death cover is as expected ");
			//		System.out.println("Death cover amount displayed "+DeathCoveramount);
			//		System.out.println("Death cover amount expected "+expecteddeathcover);
					report.updateTestLog("Death Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expecteddeathcover+" Actual Premium: "+DeathCoveramount, Status.PASS);
				}
				else{

//					System.out.println("Death cover is not as expected ");
//					System.out.println("Death cover amount displayed "+DeathCoveramount);
//					System.out.println("Death cover amount expected "+expecteddeathcover);
					report.updateTestLog("Death Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expecteddeathcover+" Actual Premium: "+DeathCoveramount, Status.FAIL);
				}
				}

				sleep(1000);
				if(TPDYN.equalsIgnoreCase("Yes")){

				WebElement TPDCover=driver.findElement(By.xpath("(//h5[@class='coverval ng-binding'])[4]"));
				TPDCover.click();
				sleep(500);
				String TPDCoveramount=TPDCover.getText();
				if(TPDCoveramount.equalsIgnoreCase(expectedtpdcover)){
//					System.out.println("TPD cover is as expected ");
//					System.out.println("TPD cover amount displayed "+TPDCoveramount);
//					System.out.println("TPD cover amount expected "+expectedtpdcover);
					report.updateTestLog("TPD Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedtpdcover+" Actual Premium: "+TPDCoveramount, Status.PASS);

				}
				else{
//					System.out.println("TPD cover is not as expected ");
//					System.out.println("TPD cover amount displayed "+TPDCoveramount);
//					System.out.println("TPD cover amount expected "+expectedtpdcover);
					report.updateTestLog("TPD Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedtpdcover+" Actual Premium: "+TPDCoveramount, Status.FAIL);
				}
				}
				 sleep(1000);
				if(IPYN.equalsIgnoreCase("Yes")){
				WebElement IPCover=driver.findElement(By.xpath("(//h5[@class='coverval ng-binding'])[6]"));
				IPCover.click();
				sleep(500);
				String IpCoverAmount=IPCover.getText();

				if(IpCoverAmount.equalsIgnoreCase(expectedipcover)){
//					System.out.println("Ip cover is as expected ");
//					System.out.println("Ip cover amount displayed "+IpCoverAmount);
//					System.out.println("Ip cover amount expected "+expectedipcover);
					report.updateTestLog("IP Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedipcover+" Actual Premium: "+IpCoverAmount, Status.PASS);

				}
				else{
//					System.out.println("Ip cover is not as expected ");
//					System.out.println("Ip cover amount displayed "+IpCoverAmount);
//					System.out.println("Ip cover amount expected "+expectedipcover);
					report.updateTestLog("IP Cover Premium", costfrequncy+" cost: "+"Expected Premium: "+expectedipcover+" Actual Premium: "+IpCoverAmount, Status.FAIL);
				}
				}
          sleep(1000);


			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}


}
