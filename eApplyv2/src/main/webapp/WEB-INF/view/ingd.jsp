<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="../../eapply/static/ingd/src/assets/images/favicon.ico">
    <link rel="stylesheet" href="../../eapply/resources/css/bootstrap-4.min.css">
    <link rel="stylesheet" href="../../eapply/resources/css/ngDialog.css">
    <link rel="stylesheet" href="../../eapply/static/ingd/src/assets/styles/ngDialog-theme-plain.css">

    <link rel="stylesheet" href="../../eapply/static/ingd/src/assets/styles/custom-style.css">
    <script src="../../eapply/resources/js/lib/jQuery/jquery.3.2.1.min.js"></script>
    <script src="../../eapply/resources/js/lib/bootstrap/bootstrap-4.0.0-beta.bundle.min.js"></script>
<title>INGD</title>
</head>
<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var baseUrl = '<%= domainName%>';
</script>

<body class="tooltip-demo">
    
    <!-- THIS IS WHERE WE WILL INJECT OUR CONTENT ============================== -->
    <div ui-view autoscroll="true"></div>
    
    <!-- THIS IS PLACE LOADER OVER THE CONTENT ============================== -->
    <div id="body-mask" data-loading>
        <div class="body-mask-loader"></div>
    </div>
    <!-- Application source file -->
    <script src="../../eapply/static/ingd/build/main.js"></script>
    <script charset='UTF-8'>
            window['adrum-start-time'] = new Date().getTime();
            window['adrum-config'] = {
              userEventInfo: {
                  "PageView": function(context) {
                      return {
                          userPageName: "ING",
                          userData: {
                            first_name: 'ING First Name',
                            last_name: 'ING Last Name'
                          }
                      }
                  },
                  "VPageView": function(context) {
                      return {
                          userData: {
                            app_number: '1234567890',
                            first_name: 'ING First Name',
                            last_name: 'ING Last Name'
                          }
                      }
                  }
              }
            };
            (function(config){
                config.appKey = 'EUM-AAB-AUA';
                config.adrumExtUrlHttp = 'http://cdn.appdynamics.com';
                config.adrumExtUrlHttps = 'https://cdn.appdynamics.com';
                //config.beaconUrlHttp = 'https://uat.eum.metlife.com.au';
                //config.beaconUrlHttps = 'https://uat.eum.metlife.com.au';
                config.xd = {enable : false};
            })(window['adrum-config'] || (window['adrum-config'] = {}));
            if ('https:' === document.location.protocol) {
                document.write(unescape('%3Cscript')
             + " src='https://cdn.appdynamics.com/adrum/adrum-4.4.0.117.js' "
             + " type='text/javascript' charset='UTF-8'"
             + unescape('%3E%3C/script%3E'));
            } else {
                document.write(unescape('%3Cscript')
             + " src='http://cdn.appdynamics.com/adrum/adrum-4.4.0.117.js' "
             + " type='text/javascript' charset='UTF-8'"
             + unescape('%3E%3C/script%3E'));
            }
            </script>
  </body>
</html>