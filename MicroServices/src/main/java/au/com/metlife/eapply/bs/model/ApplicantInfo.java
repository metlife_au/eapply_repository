package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;

public class ApplicantInfo implements Serializable {

	/**
	 * Author 175373
	 */
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String birthDate;
	private String emailId;
	private String contactType;
	private String contactNumber;
	private String smoker;
	private String title;
	private String age;
	private String memberType;
	private String gender;
	private String customerReferenceNumber;
	private String clientRefNumber;
	private String australiaCitizenship;
	private String dateJoinedFund;
	private String workHours;
	private String industryType;
	private String occupationCode;
	private String occupation;
	private String spendTimeOutside;
	private String annualSalary;
	private String occupationDuties;
	private String otherOccupation;
	private String workDuties;
	private String ownBusinessQue;
	private String ownBusinessYesQue;
	private String ownBusinessNoQue;
	private String tertiaryQue;
	private String permanenTemplQue;
	private String thirtyFiveHrsWrkQue;
	private String overallCoverDecision;
	private String previousInsurerName;
	private String fundMemPolicyNumber;
	private String spinNumber;
	private String previousTpdClaimQue;
	private String previousTerminalIllQue;
	private String documentEvidence;
	private String lifeEvent;
	private String lifeEventDate;
	private String eventAlreadyApplied;
	private String cancelReason;
	private String cancelOtherReason;
	private String previousTpdBenefit;
	private String relation;
	private String indexationDeath;
	private String indexationTpd;
	private String unitisedCovers;
	private String documentAddress;
	private ContactDetails contactDetails;
	
	private List<CoverInfo> cover;
	
	private String eventDesc;//Life Event Description
	private String insuredSalary;//IP checkbox
	
	//Added for insurance cover
	private String spouseSalary;
	private String mortgageAmount;
	private String totalExpense;
	private String totalDebts;
	private String retirementAge;
	private String inflationRate;
	private String realInterestRate;
	private String superContribRate;
	private String ipBenefitPaidToSA;
	private String ipReplaceRatio;
	private String funeralCost;
	private String annualMedAndNursingCost;
	private String dependentSuppAge;
	private String livingWithWife;
	private String dependentChild;
	private String salaryFrequency;
	private String spouseSalaryFrequency;
	private String totalExpenseFrequency;
	private String mortgageFrequency;
	private boolean showDebtsDetails = false;
	private boolean showExpenseDetails = false;
	private List<DisclosureQuestionAns> expenses;
	private List<DisclosureQuestionAns> debts;
	private List<DisclosureQuestionAns> childListPDF;
	private boolean nursingCosts;
	private boolean tpdWithoutIP;
	
	
	public boolean isNursingCosts() {
		return nursingCosts;
	}
	public void setNursingCosts(boolean nursingCosts) {
		this.nursingCosts = nursingCosts;
	}
	public boolean isTpdWithoutIP() {
		return tpdWithoutIP;
	}
	public void setTpdWithoutIP(boolean tpdWithoutIP) {
		this.tpdWithoutIP = tpdWithoutIP;
	}
	public List<DisclosureQuestionAns> getChildListPDF() {
		return childListPDF;
	}
	public void setChildListPDF(List<DisclosureQuestionAns> childListPDF) {
		this.childListPDF = childListPDF;
	}
	public List<DisclosureQuestionAns> getExpenses() {
		return expenses;
	}
	public void setExpenses(List<DisclosureQuestionAns> expenses) {
		this.expenses = expenses;
	}
	public List<DisclosureQuestionAns> getDebts() {
		return debts;
	}
	public void setDebts(List<DisclosureQuestionAns> debts) {
		this.debts = debts;
	}
	public boolean isShowDebtsDetails() {
		return showDebtsDetails;
	}
	public void setShowDebtsDetails(boolean showDebtsDetails) {
		this.showDebtsDetails = showDebtsDetails;
	}
	public boolean isShowExpenseDetails() {
		return showExpenseDetails;
	}
	public void setShowExpenseDetails(boolean showExpenseDetails) {
		this.showExpenseDetails = showExpenseDetails;
	}
	public String getSalaryFrequency() {
		return salaryFrequency;
	}
	public void setSalaryFrequency(String salaryFrequency) {
		this.salaryFrequency = salaryFrequency;
	}
	public String getSpouseSalaryFrequency() {
		return spouseSalaryFrequency;
	}
	public void setSpouseSalaryFrequency(String spouseSalaryFrequency) {
		this.spouseSalaryFrequency = spouseSalaryFrequency;
	}
	public String getTotalExpenseFrequency() {
		return totalExpenseFrequency;
	}
	public void setTotalExpenseFrequency(String totalExpenseFrequency) {
		this.totalExpenseFrequency = totalExpenseFrequency;
	}
	public String getMortgageFrequency() {
		return mortgageFrequency;
	}
	public void setMortgageFrequency(String mortgageFrequency) {
		this.mortgageFrequency = mortgageFrequency;
	}
	public String getDependentChild() {
		return dependentChild;
	}
	public void setDependentChild(String dependentChild) {
		this.dependentChild = dependentChild;
	}
	public String getLivingWithWife() {
		return livingWithWife;
	}
	public void setLivingWithWife(String livingWithWife) {
		this.livingWithWife = livingWithWife;
	}
	public String getSpouseSalary() {
		return spouseSalary;
	}
	public void setSpouseSalary(String spouseSalary) {
		this.spouseSalary = spouseSalary;
	}
	public String getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(String mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
	public String getTotalExpense() {
		return totalExpense;
	}
	public void setTotalExpense(String totalExpense) {
		this.totalExpense = totalExpense;
	}
	public String getTotalDebts() {
		return totalDebts;
	}
	public void setTotalDebts(String totalDebts) {
		this.totalDebts = totalDebts;
	}
	public String getRetirementAge() {
		return retirementAge;
	}
	public void setRetirementAge(String retirementAge) {
		this.retirementAge = retirementAge;
	}
	public String getInflationRate() {
		return inflationRate;
	}
	public void setInflationRate(String inflationRate) {
		this.inflationRate = inflationRate;
	}
	public String getRealInterestRate() {
		return realInterestRate;
	}
	public void setRealInterestRate(String realInterestRate) {
		this.realInterestRate = realInterestRate;
	}
	public String getSuperContribRate() {
		return superContribRate;
	}
	public void setSuperContribRate(String superContribRate) {
		this.superContribRate = superContribRate;
	}
	public String getIpBenefitPaidToSA() {
		return ipBenefitPaidToSA;
	}
	public void setIpBenefitPaidToSA(String ipBenefitPaidToSA) {
		this.ipBenefitPaidToSA = ipBenefitPaidToSA;
	}
	public String getIpReplaceRatio() {
		return ipReplaceRatio;
	}
	public void setIpReplaceRatio(String ipReplaceRatio) {
		this.ipReplaceRatio = ipReplaceRatio;
	}
	public String getFuneralCost() {
		return funeralCost;
	}
	public void setFuneralCost(String funeralCost) {
		this.funeralCost = funeralCost;
	}
	public String getAnnualMedAndNursingCost() {
		return annualMedAndNursingCost;
	}
	public void setAnnualMedAndNursingCost(String annualMedAndNursingCost) {
		this.annualMedAndNursingCost = annualMedAndNursingCost;
	}
	public String getDependentSuppAge() {
		return dependentSuppAge;
	}
	public void setDependentSuppAge(String dependentSuppAge) {
		this.dependentSuppAge = dependentSuppAge;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContactType() {
		return contactType;
	}
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getSmoker() {
		return smoker;
	}
	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCustomerReferenceNumber() {
		return customerReferenceNumber;
	}
	public void setCustomerReferenceNumber(String customerReferenceNumber) {
		this.customerReferenceNumber = customerReferenceNumber;
	}
	public String getClientRefNumber() {
		return clientRefNumber;
	}
	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}
	public String getAustraliaCitizenship() {
		return australiaCitizenship;
	}
	public void setAustraliaCitizenship(String australiaCitizenship) {
		this.australiaCitizenship = australiaCitizenship;
	}
	public String getDateJoinedFund() {
		return dateJoinedFund;
	}
	public void setDateJoinedFund(String dateJoinedFund) {
		this.dateJoinedFund = dateJoinedFund;
	}
	public String getWorkHours() {
		return workHours;
	}
	public void setWorkHours(String workHours) {
		this.workHours = workHours;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getSpendTimeOutside() {
		return spendTimeOutside;
	}
	public void setSpendTimeOutside(String spendTimeOutside) {
		this.spendTimeOutside = spendTimeOutside;
	}
	public String getAnnualSalary() {
		return annualSalary;
	}
	public void setAnnualSalary(String annualSalary) {
		this.annualSalary = annualSalary;
	}
	public String getOccupationDuties() {
		return occupationDuties;
	}
	public void setOccupationDuties(String occupationDuties) {
		this.occupationDuties = occupationDuties;
	}
	public String getOtherOccupation() {
		return otherOccupation;
	}
	public void setOtherOccupation(String otherOccupation) {
		this.otherOccupation = otherOccupation;
	}
	public String getWorkDuties() {
		return workDuties;
	}
	public void setWorkDuties(String workDuties) {
		this.workDuties = workDuties;
	}
	public String getOwnBusinessQue() {
		return ownBusinessQue;
	}
	public void setOwnBusinessQue(String ownBusinessQue) {
		this.ownBusinessQue = ownBusinessQue;
	}
	
	public String getTertiaryQue() {
		return tertiaryQue;
	}
	public void setTertiaryQue(String tertiaryQue) {
		this.tertiaryQue = tertiaryQue;
	}
	public String getPermanenTemplQue() {
		return permanenTemplQue;
	}
	public void setPermanenTemplQue(String permanenTemplQue) {
		this.permanenTemplQue = permanenTemplQue;
	}
	public String getThirtyFiveHrsWrkQue() {
		return thirtyFiveHrsWrkQue;
	}
	public void setThirtyFiveHrsWrkQue(String thirtyFiveHrsWrkQue) {
		this.thirtyFiveHrsWrkQue = thirtyFiveHrsWrkQue;
	}
	public String getOverallCoverDecision() {
		return overallCoverDecision;
	}
	public void setOverallCoverDecision(String overallCoverDecision) {
		this.overallCoverDecision = overallCoverDecision;
	}
	public String getPreviousInsurerName() {
		return previousInsurerName;
	}
	public void setPreviousInsurerName(String previousInsurerName) {
		this.previousInsurerName = previousInsurerName;
	}
	public String getFundMemPolicyNumber() {
		return fundMemPolicyNumber;
	}
	public void setFundMemPolicyNumber(String fundMemPolicyNumber) {
		this.fundMemPolicyNumber = fundMemPolicyNumber;
	}
	public String getSpinNumber() {
		return spinNumber;
	}
	public void setSpinNumber(String spinNumber) {
		this.spinNumber = spinNumber;
	}
	public String getPreviousTpdClaimQue() {
		return previousTpdClaimQue;
	}
	public void setPreviousTpdClaimQue(String previousTpdClaimQue) {
		this.previousTpdClaimQue = previousTpdClaimQue;
	}
	public String getPreviousTerminalIllQue() {
		return previousTerminalIllQue;
	}
	public void setPreviousTerminalIllQue(String previousTerminalIllQue) {
		this.previousTerminalIllQue = previousTerminalIllQue;
	}
	public String getDocumentEvidence() {
		return documentEvidence;
	}
	public void setDocumentEvidence(String documentEvidence) {
		this.documentEvidence = documentEvidence;
	}
	public String getLifeEvent() {
		return lifeEvent;
	}
	public void setLifeEvent(String lifeEvent) {
		this.lifeEvent = lifeEvent;
	}
	public String getLifeEventDate() {
		return lifeEventDate;
	}
	public void setLifeEventDate(String lifeEventDate) {
		this.lifeEventDate = lifeEventDate;
	}
	public String getEventAlreadyApplied() {
		return eventAlreadyApplied;
	}
	public void setEventAlreadyApplied(String eventAlreadyApplied) {
		this.eventAlreadyApplied = eventAlreadyApplied;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getCancelOtherReason() {
		return cancelOtherReason;
	}
	public void setCancelOtherReason(String cancelOtherReason) {
		this.cancelOtherReason = cancelOtherReason;
	}
	public String getPreviousTpdBenefit() {
		return previousTpdBenefit;
	}
	public void setPreviousTpdBenefit(String previousTpdBenefit) {
		this.previousTpdBenefit = previousTpdBenefit;
	}
	
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getIndexationDeath() {
		return indexationDeath;
	}
	public void setIndexationDeath(String indexationDeath) {
		this.indexationDeath = indexationDeath;
	}
	public String getIndexationTpd() {
		return indexationTpd;
	}
	public void setIndexationTpd(String indexationTpd) {
		this.indexationTpd = indexationTpd;
	}
	public String getUnitisedCovers() {
		return unitisedCovers;
	}
	public void setUnitisedCovers(String unitisedCovers) {
		this.unitisedCovers = unitisedCovers;
	}
	
	public List<CoverInfo> getCover() {
		return cover;
	}
	public void setCover(List<CoverInfo> cover) {
		this.cover = cover;
	}

	public ContactDetails getContactDetails() {
		return contactDetails;
	}
	public void setContactDetails(ContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}
	public String getOwnBusinessYesQue() {
		return ownBusinessYesQue;
	}
	public void setOwnBusinessYesQue(String ownBusinessYesQue) {
		this.ownBusinessYesQue = ownBusinessYesQue;
	}
	public String getOwnBusinessNoQue() {
		return ownBusinessNoQue;
	}
	public void setOwnBusinessNoQue(String ownBusinessNoQue) {
		this.ownBusinessNoQue = ownBusinessNoQue;
	}
	public String getEventDesc() {
		return eventDesc;
	}
	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}
	public String getInsuredSalary() {
		return insuredSalary;
	}
	public void setInsuredSalary(String insuredSalary) {
		this.insuredSalary = insuredSalary;
	}
	public String getDocumentAddress() {
		return documentAddress;
	}
	public void setDocumentAddress(String documentAddress) {
		this.documentAddress = documentAddress;
	}
	
}
