(function (angular) {
	'use strict';
	/* global channel, inputData, token, moment */
	/**
	 * @ngdoc controller
	 * @name claimTrackermain.controller:loginCtrl
	 *
	 * @description
	 * This is loginCtrl description
	 */
	/*var mtaaApp=angular.module('MtaaApp', ['ngRoute','ngResource','vcRecaptcha','googleAnalytics']);*/
	angular.module('MtaaApp').controller('loginCtrl', ['$scope', '$location',
		function ($scope, $location) {
		$scope.redirect=function(path){
		 $location.path(path);
		};
	}]);
})(angular);
