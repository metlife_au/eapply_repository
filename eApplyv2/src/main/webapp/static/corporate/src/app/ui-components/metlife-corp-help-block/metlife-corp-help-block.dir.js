(function(angular){
'use strict';

angular
    .module('Metlife.corp.uicomponents')
    .directive('metlifeCorpHelpBlock', metlifeCorpHelpBlock);

function metlifeCorpHelpBlock(APP_CONSTANTS) {
    var directive = {
        restrict: 'EA',
        templateUrl: APP_CONSTANTS.path.uiComponents + '/metlife-corp-help-block/metlife-corp-help-block.tmpl.html',
        controller: helpBlockCtrl,
        controllerAs: 'vm',
        bindToController: true
    };

    return directive;
}

helpBlockCtrl.$inject = ['$scope', 'APP_CONSTANTS'];

function helpBlockCtrl($scope, APP_CONSTANTS) {
    var vm = this;
    vm.constants = {
        helpContact : '1300 555 625'
    };
}

})(angular);