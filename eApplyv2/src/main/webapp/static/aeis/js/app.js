// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var AeisApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'angular.vertilize', 'restAPI', 'appDataModel','ui.bootstrap', 'eApplyInterceptor','ngSanitize']);
// configure our routes
AeisApp.config(function($routeProvider,$httpProvider, IdleProvider, KeepaliveProvider,$templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});

    $routeProvider
	    .when('/', {
        templateUrl : 'static/aeis/landingpage.html',
        controller  : 'login',
  			reloadOnSearch: false,
        resolve: {
         urls:function(fetchUrlSvc){
           var path = 'CareSuperUrls.properties';
           console.log(path);
           return fetchUrlSvc.getUrls(path);
         }
        }
	    })
	    .when('/landing', {
	        templateUrl : 'static/aeis/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(fetchUrlSvc){
             var path = 'CareSuperUrls.properties';
             console.log(path);
	    		   return fetchUrlSvc.getUrls(path);
	    	   }
	        }
	    })
	    .when('/quote', {
	        templateUrl : 'static/aeis/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    .when('/aura', {
	        templateUrl : 'static/aeis/aura.html',
	        controller  : 'aura'
	    })
	    .when('/summary', {
	        templateUrl : 'static/aeis/confirmation.html',
	        controller  : 'summary'
	    })	    
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/aeis/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })	    
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/aeis/timeout.html',
	        controller  : 'timeOutController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/aeis/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/aeis/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/aeis/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/aeis/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/aeis/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	    .when('/quotecancel/:mode', {
	        templateUrl : 'static/aeis/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/aeis/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/aeis/cancel_decision.html',
	        controller  : 'cancelController'
	    })

});
AeisApp.constant('APP_CONSTANTS', function () {
	return {
		'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	}
});

AeisApp.run(function($rootScope, $window) {
	$rootScope.logout = function() {
      $window.close();
    }
});
