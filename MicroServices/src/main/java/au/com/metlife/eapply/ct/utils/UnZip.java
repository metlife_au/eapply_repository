package au.com.metlife.eapply.ct.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnZip {
	private static final Logger log = LoggerFactory.getLogger(UnZip.class);
    
    /** The ZipFile that is used to read an archive */
    protected ZipFile zippy;
    /** The buffer for reading/writing the ZipFile data */
    protected byte[] b;  
    /** Construct an UnZip object. Just allocate the buffer */
    public UnZip() {
        b = new byte[8092];
    }
    /** Cache of paths we've mkdir()ed. */
    protected SortedSet dirsMade;
    /** For a given Zip file, process each entry. */
    public void unZip(String fileName,String opDir) {
        dirsMade = new TreeSet();
        try {
            zippy = new ZipFile(fileName);
            Enumeration all = zippy.entries();
            while (all.hasMoreElements()) {
                getFile((ZipEntry)all.nextElement(),opDir);
            }
        } catch (IOException err) {
            /*System.err.println("IO Error: " + err);*/
        	log.error("IO Error: {}",err);
            return;
        }
    }

    protected boolean warnedMkDir = false;
    /** Process one file from the zip, given its name.
     * Either print the name, or create the file on disk.
     */
    protected void getFile(ZipEntry e,String opDir) throws IOException {
    	//Change as per Sonar fix "Try with Resource Added" start
    	/*FileOutputStream os = null;
        InputStream  is = null;*/
    	
    	String zipName = e.getName();
		if (zipName.startsWith("/")) {
			if (!warnedMkDir)
				/*System.out.println("Ignoring absolute paths");*/
				log.info("Ignoring absolute paths");
			warnedMkDir = true;
			zipName = zipName.substring(1);
		}
		// if a directory, just return. We mkdir for every file,
		// since some widely-used Zip creators don't put out
		// any directory entries, or put them in the wrong place.
		if (zipName.endsWith("/")) {
			return;
		}
		// Else must be a file; open the file for output
		// Get the directory part.
		int ix = zipName.lastIndexOf('/');
		if (ix > 0) {
			String dirName = opDir+zipName.substring(0, ix);
			if (!dirsMade.contains(dirName)) {
				File d = new File(dirName);
				// If it already exists as a dir, don't do anything
				if (!(d.exists() && d.isDirectory())) {
					// Try to create the directory, warn if it fails
					if (!d.mkdirs()) {
						/*System.err.println(*/
						log.error("Warning: unable to mkdir {}",dirName);
					}
					dirsMade.add(dirName);
				}
			}
		}
    	try(FileOutputStream os = new FileOutputStream(opDir+zipName);
    	        InputStream  is = zippy.getInputStream(e);) {	
    		/*String zipName = e.getName();

    		if (zipName.startsWith("/")) {
    			if (!warnedMkDir)
    				System.out.println("Ignoring absolute paths");
    			warnedMkDir = true;
    			zipName = zipName.substring(1);
    		}
    		// if a directory, just return. We mkdir for every file,
    		// since some widely-used Zip creators don't put out
    		// any directory entries, or put them in the wrong place.
    		if (zipName.endsWith("/")) {
    			return;
    		}
    		// Else must be a file; open the file for output
    		// Get the directory part.
    		int ix = zipName.lastIndexOf('/');
    		if (ix > 0) {
    			String dirName = opDir+zipName.substring(0, ix);
    			if (!dirsMade.contains(dirName)) {
    				File d = new File(dirName);
    				// If it already exists as a dir, don't do anything
    				if (!(d.exists() && d.isDirectory())) {
    					// Try to create the directory, warn if it fails
    					if (!d.mkdirs()) {
    						System.err.println(
    								"Warning: unable to mkdir " + dirName);
    					}
    					dirsMade.add(dirName);
    				}
    			}
    		}
    		
    		os = new FileOutputStream(opDir+zipName);
    	    is = zippy.getInputStream(e);*/
    		int n = 0;
    		while ((n = is.read(b)) >0) {
    			os.write(b, 0, n);
    		}    		
    		
    		//is.close();
    		//os.close();
    	}catch(Exception ex){
    		 log.error("Error in upload file {}",ex);
    		
      }
    	 /* finally{
    		  if(os!= null){
        		  os.close();        		  
        	  }
    		  if(is!= null){
        		  is.close();        		  
        	  }
    	  }*/
        	  
    	//Change as per Sonar fix "Try with Resource Added" ends	  
    

        	

    }
    
    /*public static void main(String[] argv) {
    	UnZip obj = new UnZip();
    	obj.unZip("X:/BAU/Documents/Testing.zip", "X:/cms/test/");
    	obj=null;
    }*/

}
