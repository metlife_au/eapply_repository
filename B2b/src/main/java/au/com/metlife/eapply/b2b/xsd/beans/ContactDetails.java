package au.com.metlife.eapply.b2b.xsd.beans;

public class ContactDetails {

	String emailAddress;
	String mobilePhone;
	String workPhone;
	String homePhone;
	String prefContact;
	String prefContactTime;
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getPrefContact() {
		return prefContact;
	}
	public void setPrefContact(String prefContact) {
		this.prefContact = prefContact;
	}
	public String getPrefContactTime() {
		return prefContactTime;
	}
	public void setPrefContactTime(String prefContactTime) {
		this.prefContactTime = prefContactTime;
	}
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
}
