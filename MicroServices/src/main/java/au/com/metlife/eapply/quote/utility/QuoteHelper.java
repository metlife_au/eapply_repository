package au.com.metlife.eapply.quote.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;


public class QuoteHelper {
	private QuoteHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(QuoteHelper.class);
	
	public static BigDecimal calculateIPUntCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		log.info("Calculate IP unit cost start");
		BigDecimal ipCostBD=null;
		/*BigDecimal ipUnts=null;
		BigDecimal ipUnitsCvrAmount;*/
		try{			 			 			
			/*ipUnts=new BigDecimal(ruleModel.getIpUnits());
			log.info("ipUnts--->{}",ipUnts);
			ipUnitsCvrAmount=ipUnts.multiply(new BigDecimal(instProductRuleBO.getIpUnitCostMultipFactor())).setScale(0, BigDecimal.ROUND_CEILING);
			ruleModel.setIpFixedAmount(ipUnitsCvrAmount);
			log.info("ipUnitsCvrAmount--->{}",ipUnitsCvrAmount);*/
			String ipCost=calculateIPFixedCost(ruleModel, instProductRuleBO, quoteResponse, ruleInfoMap, ruleFilePath);
			if(ipCost!=null){
				ipCostBD=new BigDecimal(ipCost);
				quoteResponse.setCoverAmount(ruleModel.getIpFixedAmount());
				quoteResponse.setCost(ipCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			log.info("ipCost---> {}",ipCost);		
		}
		catch(Exception e){
			log.error("Error in calculate IP unit cost : {}",e);
		}
		log.info("Calculate IP unit cost finish");
		return ipCostBD;
	}
	
	
	public static String calculateIPFixedCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath) {
		log.info("Calculate IP fixed cost start");
		String ruleNameAsPerFund;
		double ipYearlyCost=0;
		double ipWeeklyCost=0;
		String ipCost =null;
		BigDecimal ipThresholdLimit=null;
		BigDecimal ipAddnlUnits=null;
		BigDecimal ipThresholdLimitInUnits=null;
		/*String ipAddnlTxtFldVal=null;*/
		BigDecimal ipFixedAmt = null;
		double ipUnitsCvrAmount=0;
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleModel.isSmoker()){
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
			}else{
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
			}

			if(ruleModel.getMemberType()!=null){
				coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
			}
			
			//Guild changes
			if(QuoteConstants.PARTNER_GUIL.equalsIgnoreCase(ruleModel.getFundCode())) {
				coverDetailsRuleBO.setUnitsed(ruleModel.isUnitsed());
				coverDetailsRuleBO.setIpUnits(ruleModel.getIpUnits());
			}

			if(ruleModel.getIpBenefitPeriod()!=null){
				if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_2);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_5_YEARS)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_5);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_60)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_60);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_65)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_65);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_67)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_TO_AGE_67)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
				}
			}
			
			if(ruleModel.getIpWaitingPeriod()!=null ){
				if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_30_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_30);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_60_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_60);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_90_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_90);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_1_YEAR)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_1_YEAR);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_3_YEAR)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_3_YEARS);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_45_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_45);
				}
			}	
			/*Check if IP Unitised or fixed radio buttons are rendered, if not consider the ip as fixed*/
			if(instProductRuleBO!=null && ruleInfoMap!=null && quoteResponse!=null && ruleModel != null && ruleModel.getIpFixedAmount()!= null){
				ipThresholdLimit=getIpThresholdLimit(ruleModel, instProductRuleBO, quoteResponse, ruleInfoMap,ruleFilePath);
				if(ruleModel.getIpOccCategory()!=null && ruleModel.getIpCoverType().equalsIgnoreCase("IpUnitised") 
						&& ruleModel.getIpUnits() > 0 && ruleModel.getFundCode()!=null){
					ipAddnlUnits=(new BigDecimal(ruleModel.getIpUnits()));
					/*Convert the threshold limit to units.*/
					/*If additional IP cover is in unitised, then convert the units into fixed amount
					 * based on occupation*/
					if(ipThresholdLimit!=null && ruleModel.getIpOccCategory()!=null && ruleModel.getFundCode().equalsIgnoreCase("SFPS")){
						if(ruleModel.getIpOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_WHITECOLLAR)){
//							ipUnitsCvrAmount=new BigDecimal(coverQuoteDetailsVO.getIpAddnlTxtFldVal()).multiply(new BigDecimal(instProductRuleBO.getSfpsIpUnitCostWhiteColMultFac())).doubleValue();
							/*Convert ipThreshold limit into units*/
							ipThresholdLimitInUnits=ipThresholdLimit.divide(BigDecimal.valueOf(instProductRuleBO.getSfpsIpUnitCostWhiteColMultFac()),2,BigDecimal.ROUND_HALF_UP);
						}else if(ruleModel.getIpOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
//							ipUnitsCvrAmount=new BigDecimal(coverQuoteDetailsVO.getIpAddnlTxtFldVal()).multiply(new BigDecimal(instProductRuleBO.getSfpsIpUnitCostProfMultFac())).doubleValue();
							ipThresholdLimitInUnits=ipThresholdLimit.divide(BigDecimal.valueOf(instProductRuleBO.getSfpsIpUnitCostProfMultFac()),2,BigDecimal.ROUND_HALF_UP);
						}else if(ruleModel.getIpOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
//							ipUnitsCvrAmount=new BigDecimal(coverQuoteDetailsVO.getIpAddnlTxtFldVal()).multiply(new BigDecimal(instProductRuleBO.getSfpsIpUnitCostStandardMultFac())).doubleValue();
							ipThresholdLimitInUnits=ipThresholdLimit.divide(BigDecimal.valueOf(instProductRuleBO.getSfpsIpUnitCostStandardMultFac()),2,BigDecimal.ROUND_HALF_UP);
						}
					}
				}
			}
			/*Check if ip additional units are more than the thresold limit, in that case restrict it to threshold limit*/
			BigDecimal ipUnts=null;
			coverDetailsRuleBO.setIPMaxUnitsAllowed(0.0);
			if(ipThresholdLimitInUnits!=null && ipAddnlUnits!=null){
					ipThresholdLimitInUnits=ipThresholdLimitInUnits.setScale(0, BigDecimal.ROUND_CEILING);
					if(ipAddnlUnits.compareTo(ipThresholdLimitInUnits)>0){
						ipUnts=ipThresholdLimitInUnits;
					}else{
						ipUnts=ipAddnlUnits;
					}
			}else if(ipAddnlUnits!=null){
				ipUnts=ipAddnlUnits;
			}
			if(ipUnts!=null && ruleModel.getIpOccCategory()!=null &&  QuoteConstants.PARTNER_SFPS.equalsIgnoreCase(ruleModel.getFundCode())){
				ipUnts=ipUnts.setScale(0, BigDecimal.ROUND_CEILING);
				if(ruleModel.getIpOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_WHITECOLLAR)){
					ipUnitsCvrAmount=ipUnts.multiply(BigDecimal.valueOf(instProductRuleBO.getSfpsIpUnitCostWhiteColMultFac())).doubleValue();
				}else if(ruleModel.getIpOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
					ipUnitsCvrAmount=ipUnts.multiply(BigDecimal.valueOf(instProductRuleBO.getSfpsIpUnitCostProfMultFac())).doubleValue();
				}else if(ruleModel.getIpOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
					ipUnitsCvrAmount=ipUnts.multiply(BigDecimal.valueOf(instProductRuleBO.getSfpsIpUnitCostStandardMultFac())).doubleValue();
				}
				ruleModel.setIpFixedAmount(BigDecimal.valueOf(ipUnitsCvrAmount));
				coverDetailsRuleBO.setIPMaxUnitsAllowed(ipUnts.doubleValue());
			}
			
			if((ruleModel.getFundCode().equalsIgnoreCase("CARE") || 
					QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode())) && ruleModel.getManageType().equalsIgnoreCase(QuoteConstants.MTYPE_TCOVER)
					/*&& ruleModel.getIpExistingAmount().compareTo(ruleModel.getIpTransferAmount()) < 0*/){
				ipFixedAmt=createIPMultiplesByFactor(ruleModel.getIpFixedAmount(), BigDecimal.valueOf(instProductRuleBO.getIpUnitCostMultipFactor()).stripTrailingZeros());
				/*if(ipFixedAmt.intValue()>40000)
				{
					ipFixedAmt = new BigDecimal("40000");
				}*/
			}
			else
			{
				ipFixedAmt=createMultiplesByFactor(ruleModel.getIpFixedAmount(), instProductRuleBO.getIpMultipleFactor());
			}
				ruleModel.setIpFixedAmount(ipFixedAmt);
				coverDetailsRuleBO.setCoverAmount(ruleModel.getIpFixedAmount().doubleValue());
			
			
			

				if(ruleModel.getFundCode()!=null){
					ruleNameAsPerFund=ruleModel.getFundCode()+QuoteConstants.RULE_NAME_INST_IP_FXD_COST;
					if(QuoteConstants.PARTNER_HOST.equalsIgnoreCase(ruleModel.getFundCode()) || QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(ruleModel.getFundCode())){
						coverDetailsRuleBO.setInsuranceCategoryStr(QuoteConstants.INS_CATEGORY_WHITECOLLAR);
					}else{
						coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getIpOccCategory());
					}
					if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
						coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
					}
					coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
					coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
					log.info("coverDetailsRuleBO.getYearlyCost()----> {}",coverDetailsRuleBO.getYearlyCost());
					if(!QuoteConstants.PARTNER_SFPS.equalsIgnoreCase(ruleModel.getFundCode()) && !QuoteConstants.PARTNER_GUIL.equalsIgnoreCase(ruleModel.getFundCode())){
						if(QuoteConstants.PARTNER_HOST.equalsIgnoreCase(ruleModel.getFundCode()) || QuoteConstants.PARTNER_MTAA.equalsIgnoreCase(ruleModel.getFundCode())
								||QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(ruleModel.getFundCode())){
							coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.IP_COVER, ruleModel.getIpOccCategory(), coverDetailsRuleBO,ruleModel.getFundCode(),ruleFilePath);	
						}else{
							 if(!QuoteConstants.PARTNER_FIRS.equalsIgnoreCase(ruleModel.getFundCode())){
							coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.IP_COVER, ruleModel.getIpOccCategory(), coverDetailsRuleBO,ruleModel.getFundCode(),ruleFilePath);	
						}
						}
					}
					/*Yearly cost of IP for SFPS*/
					if(ruleModel.getFundCode().equalsIgnoreCase(QuoteConstants.PARTNER_SFPS)){
					//	coverDetailsRuleBO.setIPMaxUnitsAllowed(ipUnts.doubleValue());
						//if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)){
						if((QuoteConstants.BENEFIT_PERIOD_2_YEARS).equalsIgnoreCase(ruleModel.getIpBenefitPeriod())){
							ipYearlyCost=coverDetailsRuleBO.getTwoYearRate();
						//}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_65)){
						}else if((QuoteConstants.BENEFIT_PERIOD_AGE_65).equalsIgnoreCase(ruleModel.getIpBenefitPeriod())){
							ipYearlyCost=coverDetailsRuleBO.getAge60Rate()+coverDetailsRuleBO.getTwoYearRate();
						}
					}else{ //for other partners
						ipYearlyCost=coverDetailsRuleBO.getYearlyCost();
					}
					ipCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, ipYearlyCost, ruleModel);
					if (ruleModel.getFundCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL) && ruleModel.isUnitsed() ) {
						ipWeeklyCost = coverDetailsRuleBO.getWeeklyCost();
						ipCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_WEEKLY, ipWeeklyCost, ruleModel);
					}
				}
			log.info("IP cost in coverDetailsHelper>>> {}",ipCost);
		}catch(Exception e){
			log.error("Error in calculate IP fixed cost: {}",e);
			throw e;
		}
		log.info("Calculate IP fixed cost finish");
		return ipCost;
	}
	
	public static BigDecimal getIpThresholdLimit(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		log.info("Calculate IP ThresholdLimit start");
		/*BigDecimal monthlySal=null;*/ 
		BigDecimal monthlyBenefit=null;
		/*BigDecimal maxIpCvrLimit=null;*/
		BigDecimal thresholdLimit=null;
		/*BigDecimal ipExCvrAmtBd=null;*/
		try{
			
			if(ruleModel.getFundCode()!=null && ruleModel.getAnnualSalary()!=null && !ruleModel.getAnnualSalary().equalsIgnoreCase("")){
				//revisit the below 2 lines
				/*if(coverQuoteDetailsVO.getIpExCvrAmt()!=null && !coverQuoteDetailsVO.getIpExCvrAmt().equalsIgnoreCase("")){
					ipExCvrAmtBd=new BigDecimal(coverQuoteDetailsVO.getIpExCvrAmt());
				}*/
				/*Invoke business rule to get the IP Monthly benefit*/
                CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
                coverDetailsRuleBO.setAnnualSal(new BigDecimal(ruleModel.getAnnualSalary()).doubleValue());
                String ruleNameIPThresholdLmt = QuoteConstants.RULE_NAME_INST_IP_THRESHOLD_LMT;
                coverDetailsRuleBO.setRuleFileName(ruleNameIPThresholdLmt);
				coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());	
                
                
                if(ruleModel.getIpBenefitPeriod()!=null &&
                		ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_2);	
				}else{
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_65);
				}
                if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameIPThresholdLmt)!=null){
                      coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameIPThresholdLmt));
                }
                coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO, ruleFilePath);
//				monthlySal=new BigDecimal(coverQuoteDetailsVO.getAnnualSal()).divide(new BigDecimal(InstCoverDetailsConstant.DIVISOR_12),2,BigDecimal.ROUND_HALF_UP);
				/*Get monthy benefit of montly salary*/
                monthlyBenefit=BigDecimal.valueOf(coverDetailsRuleBO.getIpMonthlyBenefit()).setScale(2, BigDecimal.ROUND_HALF_UP);
                /*In case of SFPS directly assign the monthly benefit to threshold limit*/
                if(ruleModel.getFundCode().equalsIgnoreCase("SFPS")){
                	thresholdLimit=monthlyBenefit;
                }
                if(null!= thresholdLimit ){
                thresholdLimit=thresholdLimit.setScale(0,BigDecimal.ROUND_FLOOR);
                }
			}
		}catch(Exception e){
			log.error("Error in calculate IP threshold limit: {}",e);
		}
		log.info("Calculate IP ThresholdLimit finish");
		return thresholdLimit;
	}
	
	public static String calculateTPDFixedCost(RuleModel ruleModel,Map ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse,InstProductRuleBO instProductRuleBO){
		log.info("Calculate TPD fixed cost start");
		BigDecimal tpdAddnlCvrBd=null;
		String ruleNameAsPerFund;
		double tpdYearlyCost;
		String tpdCost=null;
		try{
			log.info("inside calculateTPDFixedCost");
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			tpdAddnlCvrBd = ruleModel.getTpdFixedAmount();
			//Commented condition -Added if condition for vicsuper -Purnachandra K
			//if (!QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode())) {
			/*Added the condition as per the WR 19289*/
			if(QuoteConstants.PARTNER_CARE.equalsIgnoreCase(ruleModel.getFundCode()) && (null != ruleModel.getTpdFixedAmount() && ruleModel.getTpdFixedAmount().compareTo(ruleModel.getTpdExistingAmount()) == 0)){
				tpdAddnlCvrBd = ruleModel.getTpdFixedAmount();
			}
			else if(QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode()))
			{
				tpdAddnlCvrBd = tpdAddnlCvrBd;
			}
			else if(QuoteConstants.PARTNER_GUIL.equalsIgnoreCase(ruleModel.getFundCode()))
			{
				tpdAddnlCvrBd = ruleModel.getTpdFixedAmount();
			}
			else{
				tpdAddnlCvrBd=createMultiplesByFactor(tpdAddnlCvrBd, instProductRuleBO.getTpdMultipleFactor());
			}
			//}
			ruleModel.setTpdFixedAmount(tpdAddnlCvrBd);

			
			/*boolean isDeathAndTpdApplied=(coverQuoteDetailsVO.isBothDeathAndTpdApplied() && deathAndTpdCoverTypeSame);*/
	
			
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_FXD_COST;
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleModel.isSmoker()){
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
			}else{
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
			}
			
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			
			/*In case of AUSW/NSFS first we need to calculate based on white collar,
			 * then call other rule to convert the cost based on the respective 
			 * occupation category. calculateCoverCostByOccup(...) will do this*/
			if(QuoteConstants.PARTNER_AUSW.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_NSFS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_HOST.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())){
				coverDetailsRuleBO.setInsuranceCategoryStr(QuoteConstants.INS_CATEGORY_WHITECOLLAR);
			}else{
				coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			}
			
			coverDetailsRuleBO.setCoverAmount(ruleModel.getTpdFixedAmount().doubleValue());
			coverDetailsRuleBO.setDeathAndTpdApplied(true);
			if(ruleModel.getMemberType()!=null){
				coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
			}
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);
			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_TPD, ruleModel.getTpdOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			tpdYearlyCost=coverDetailsRuleBO.getYearlyCost();
			tpdCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, tpdYearlyCost, ruleModel);
			if(tpdCost!=null){
				quoteResponse.setCost(new BigDecimal(tpdCost).setScale(2, BigDecimal.ROUND_HALF_UP));
				quoteResponse.setCoverAmount(ruleModel.getTpdFixedAmount());
			}
			
		}
		catch(Exception e){
			log.error("Error in calculate TPD fixed cost : {}",e);
		}
		log.info("Calculate TPD fixed cost finish");
		return tpdCost;
	}
	
	
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @return
	 * @throws Exception
	 */
	public static String computeTpdUnitCost(RuleModel ruleModel, InstProductRuleBO instProductRuleBO) {
		log.info("Compute tpd unit cost start");
		double tpdWeeklyCost=0.0;
		String tpdCost;
		/*SRKR: FOR FIRS super we need to calculate units cost based on occupation category*/
		if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_FIRS)){
			if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_LOW_RISK)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFactProff())).doubleValue();
			}
		}else if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && 
				(instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_HOST)
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_VICS) //Added for vicsuper
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_SFPS)
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL)
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_ACCS))){
			if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD) || ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_GENERAL)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_WHITECOLLAR)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFactProff())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_OWN_OCCUPATION)){//Added for vicsuper
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultFacOwnWhiteCollar())).doubleValue();
			}
		}
		/*MTAA Changes Start*/
		else if(null != instProductRuleBO && null != instProductRuleBO.getProductCode()
				&& QuoteConstants.PARTNER_MTAA.equalsIgnoreCase(instProductRuleBO.getProductCode())){
			if(ruleModel.getAge()> 26 && instProductRuleBO.getTpdUnitCostGrtThan27() != 0){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostGrtThan27())).doubleValue();
			}else{
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultipFactor())).doubleValue();
			}			
			
		}
		/*MTAA Change End*/
		else if(null != instProductRuleBO){
			tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getTpdUnitCostMultipFactor())).doubleValue();
		}
		tpdCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_WEEKLY, tpdWeeklyCost, ruleModel);
		log.info("Compute tpd unit cost finish");
		return tpdCost;
	}
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @param quoteResponse
	 * @param ruleInfoMap
	 * @param ruleFilePath
	 */
	public static void calculateTpdUnitisedAmtAndCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		log.info("Calculate Tpd unitised amount and cost start");
		String tpdCost=null;
		BigDecimal tpdCostBD=null;
		log.info("inside calculateTpdUnitisedAmtAndCost");
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			String ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT;
			coverDetailsRuleBO.setUnits(ruleModel.getTpdUnits());
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
		
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				log.info("TPD amount--- {}" ,coverDetailsRuleBO.getCoverAmount());
				quoteResponse.setCoverAmount(new BigDecimal(Double.toString(coverDetailsRuleBO.getCoverAmount())).setScale(0, BigDecimal.ROUND_HALF_UP));
			}
			/*Calculate TPD cost*/			
			tpdCost = computeTpdUnitCost(ruleModel, instProductRuleBO);
			if(tpdCost != null){
				tpdCostBD=new BigDecimal(tpdCost);
				quoteResponse.setCost(tpdCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
				log.info("??tpdCost? {}",tpdCost);
			}
		}catch(Exception e){
			log.error("Error in calculate Tpd unitised amount and cost : {}",e);
		}
		log.info("Calculate Tpd unitised amount and cost finish");
	}
	
	
	/**
	 * @param ruleInfoMap
	 * @param productType
	 * @param insuranceCategory
	 * @param coverRuleBO
	 * @param inputIdentifier
	 * @param ruleFileLocation
	 * @return
	 * @throws Exception
	 */
	private static CoverDetailsRuleBO calculateCoverCostByOccup(Map ruleInfoMap,int productType, String insuranceCategory,CoverDetailsRuleBO coverRuleBO,String inputIdentifier,String ruleFileLocation) {
		log.info("Calculate cover cost by occupation start");
		CoverDetailsRuleBO coverDetailsRuleBO = new CoverDetailsRuleBO();
		String ruleNameAsPerFund=null;
		try{
			if(ruleInfoMap!=null && insuranceCategory!=null && inputIdentifier!=null){
				coverDetailsRuleBO.setCoverType(productType);
				coverDetailsRuleBO.setInsuranceCategoryStr(insuranceCategory);
				coverDetailsRuleBO.setDeathCostperUnit(coverRuleBO.getDeathCostperUnit());
				coverDetailsRuleBO.setTpdCostperUnit(coverRuleBO.getTpdCostperUnit());
				coverDetailsRuleBO.setCalcWhiteCollarAmount(coverRuleBO.getYearlyCost());
				if(null == coverRuleBO.getLevelOfCommision()){
					coverRuleBO.setLevelOfCommision(Double.valueOf(0));
				}else{
					coverDetailsRuleBO.setLevelOfCommision(coverRuleBO.getLevelOfCommision());
				}
				coverDetailsRuleBO.setFundCode(inputIdentifier);
				coverDetailsRuleBO.setDeathAndTpdApplied(coverRuleBO.isDeathAndTpdApplied());
				ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_NORM_ALL_CVR_COST;
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
				if (ruleInfoMap.get(ruleNameAsPerFund) != null) {
					coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(ruleNameAsPerFund));
				}
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);
			}
		}catch(Exception e){
			log.error("Error in calculate cover cost by occupation : {}",e);
		}
		log.info("Calculate cover cost by occupation finish");
		return coverDetailsRuleBO;
	}	
	
	/**
	 * @param ruleModel
	 * @param ruleInfoMap
	 * @param ruleFileLocation
	 * @param quoteResponse
	 * @return
	 * @throws Exception
	 */
	public static String calculateDeathFixedCost(InstProductRuleBO instProductRuleBO,RuleModel ruleModel,Map ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse) {
		log.info("Calculate death fixed cost start");
		
		BigDecimal deathAddnlCvrBd=null;
		BigDecimal remainingDeath=null;
		String ruleNameAsPerFund;
		double dcYearlyCost;
		String dcCost = null;
		Double deathCost=null;
		Double dcExcessCost=null;
		CoverDetailsRuleBO coverDetailsRuleBO=null;
		try{
			coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			
			//Commented this condition - Added if condition for vicsuper -Purnachandra K
			//deathAddnlCvrBd = ruleModel.getDeathFixedAmount();
			//if (!QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode())) {
			/*Added the condition as per the WR 19289*/
			if(QuoteConstants.PARTNER_CARE.equalsIgnoreCase(ruleModel.getFundCode()) && (null != ruleModel.getDeathFixedAmount() && ruleModel.getDeathFixedAmount().compareTo(ruleModel.getDeathExistingAmount()) == 0)){
				deathAddnlCvrBd = ruleModel.getDeathFixedAmount();
			}
			else if(QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode()))
			{
				deathAddnlCvrBd = ruleModel.getDeathFixedAmount();
			}
			else if(QuoteConstants.PARTNER_GUIL.equalsIgnoreCase(ruleModel.getFundCode()))
			{
				deathAddnlCvrBd = ruleModel.getDeathFixedAmount();
			} 
			else{
				deathAddnlCvrBd=createMultiplesByFactor(ruleModel.getDeathFixedAmount(), instProductRuleBO.getDeathMultipleFactor());
			}
			//}
				
			ruleModel.setDeathFixedAmount(deathAddnlCvrBd);
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST;
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleModel.isSmoker()){
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
			}else{
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
			}
			if(ruleModel.getMemberType()!=null){
				coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
			}
			
			/*if(coverQuoteDetailsVO.getLevelOfCommision()!=null && !coverQuoteDetailsVO.getLevelOfCommision().equalsIgnoreCase("")){
				coverDetailsRuleBO.setLevelOfCommision(Double.parseDouble(coverQuoteDetailsVO.getLevelOfCommision()));
     		}*/
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			/*In case of AUSW/NSFS first we need to calculate based on white collar,
			 * then call other rule to convert the cost based on the respective 
			 * occupation category. calculateCoverCostByOccup(...) will do this*/
			if(QuoteConstants.PARTNER_AUSW.equalsIgnoreCase(coverDetailsRuleBO.getFundCode()) 
					|| QuoteConstants.PARTNER_NSFS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_HOST.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())){
				coverDetailsRuleBO.setInsuranceCategoryStr(QuoteConstants.INS_CATEGORY_WHITECOLLAR);
			}else{
				coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			}
			if(QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())){
				if(log.isDebugEnabled())
				{
				log.info(String.valueOf(coverDetailsRuleBO.getYearlyCost()));
				}
				remainingDeath=occupationrelateddeathcost(ruleModel);
		
			if(remainingDeath!=null) {
				coverDetailsRuleBO.setCoverAmount(remainingDeath.doubleValue());
				coverDetailsRuleBO.setDeathAndTpdApplied(Boolean.FALSE);
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);	
				coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
				dcExcessCost=coverDetailsRuleBO.getYearlyCost();
				CoverDetailsRuleBO aeisdeathCoverBo= new CoverDetailsRuleBO();
				aeisdeathCoverBo.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
				aeisdeathCoverBo.setFundCode(ruleModel.getFundCode());
				aeisdeathCoverBo.setAge(ruleModel.getAge());
				if(ruleModel.isSmoker()){
					aeisdeathCoverBo.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
				}else{
					aeisdeathCoverBo.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
				}
				if(ruleModel.getMemberType()!=null){
					aeisdeathCoverBo.setMemberType(ruleModel.getMemberType());
				}
				if(ruleModel.getGender()!=null){
					if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
						aeisdeathCoverBo.setGender(CoverDetailsRuleBO.GENDER_MALE);
					}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
						aeisdeathCoverBo.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
					}
				}
				aeisdeathCoverBo.setInsuranceCategoryStr(QuoteConstants.INS_CATEGORY_WHITECOLLAR);
				if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
					aeisdeathCoverBo.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
				}
				aeisdeathCoverBo.setCoverAmount((ruleModel.getDeathFixedAmount().subtract(remainingDeath)).doubleValue());
		
		
				aeisdeathCoverBo.setRuleFileName(ruleNameAsPerFund);
				aeisdeathCoverBo = DroolsHelper.invokeUC16BusinessRules(aeisdeathCoverBo,ruleFileLocation);	
				if(log.isDebugEnabled())
				{
				log.info(String.valueOf(aeisdeathCoverBo.getYearlyCost()));
				}
				aeisdeathCoverBo.setDeathAndTpdApplied(Boolean.TRUE);
				aeisdeathCoverBo=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), aeisdeathCoverBo, ruleModel.getFundCode(),ruleFileLocation);
				deathCost=aeisdeathCoverBo.getYearlyCost();
				if(deathCost!=null && dcExcessCost!=null){
					dcYearlyCost = (new BigDecimal(deathCost).add(new BigDecimal(dcExcessCost))).doubleValue();
					 dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, dcYearlyCost, ruleModel);
				}
			}
			else {
				coverDetailsRuleBO.setCoverAmount(ruleModel.getDeathFixedAmount().doubleValue());
				if(ruleModel.getDeathFixedAmount().doubleValue()!=0 && ruleModel.getTpdFixedAmount().doubleValue()!=0) {
					coverDetailsRuleBO.setDeathAndTpdApplied(Boolean.TRUE);
				}
				else {
					coverDetailsRuleBO.setDeathAndTpdApplied(Boolean.FALSE);
				}
				/*SRKR In case of INGD we need pass the flag if both death and tpd are applied. If both death and tpd are applied
				 * then we have different set of rates*/
	    		/*coverDetailsRuleBO.setDeathAndTpdApplied(bothDeathTPDApplied);*/
				
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);	

				coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
				dcYearlyCost=coverDetailsRuleBO.getYearlyCost();
				log.info("coverDetailsRuleBO.getYearlyCost()?>> {}",coverDetailsRuleBO.getYearlyCost());
				dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, dcYearlyCost, ruleModel);
			}
			}
			else {
			coverDetailsRuleBO.setCoverAmount(ruleModel.getDeathFixedAmount().doubleValue());
			
			/*SRKR In case of INGD we need pass the flag if both death and tpd are applied. If both death and tpd are applied
			 * then we have different set of rates*/
    		/*coverDetailsRuleBO.setDeathAndTpdApplied(bothDeathTPDApplied);*/
			
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);	

			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			dcYearlyCost=coverDetailsRuleBO.getYearlyCost();
			log.info("coverDetailsRuleBO.getYearlyCost()?>> {}",coverDetailsRuleBO.getYearlyCost());
			dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, dcYearlyCost, ruleModel);
			}

			if(dcCost != null){
			quoteResponse.setCost(new BigDecimal(dcCost).setScale(2, BigDecimal.ROUND_HALF_UP));
			quoteResponse.setCoverAmount(ruleModel.getDeathFixedAmount());
			}
		}catch(Exception e){
			log.error("Error in calculate death fixed cost : {}",e);
			throw e;
		}
		log.info("Calculate death fixed cost finish");
		return dcCost;
	}
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @param quoteResponse
	 * @param ruleInfoMap
	 * @param ruleFilePath
	 * @return
	 */
	public static RuleModel calculateDcUnitisedAmtAndCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		log.info("Calculate Dc Unitised Amount and cost start");
		String dcCost=null;
		BigDecimal deathCostBD=null;
		log.info("inside calculateDcUnitisedAmtAndCost");
		/*String RULE_FILE_LOC="E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";*/
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			/*PrimeSuperFundRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue(QuoteConstants.RULE_FILE_LOC, QuoteConstants.RULE_FILE_LOC);*/
			coverDetailsRuleBO.setDeathUnits(ruleModel.getDeathUnits());
			coverDetailsRuleBO.setTpdUnits(ruleModel.getTpdUnits());

			if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT));
			}
			
				coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
				log.info("InsuranceCategoryStr Death Calc------{}" ,coverDetailsRuleBO.getInsuranceCategoryStr());
			
			
			coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				log.info("Death Amount----{}" ,coverDetailsRuleBO.getCoverAmount());
				/*ruleModel.setDeathFixedAmount(new BigDecimal("" +coverDetailsRuleBO.getCoverAmount()).setScale(2, BigDecimal.ROUND_HALF_UP));*/
				quoteResponse.setCoverAmount(new BigDecimal(Double.toString(coverDetailsRuleBO.getCoverAmount())).setScale(0, BigDecimal.ROUND_HALF_UP));
			}

			
			dcCost = computeDcUnitsCost(ruleModel, instProductRuleBO);
			if(dcCost != null){
				deathCostBD=new BigDecimal(dcCost);
				/*ruleModel.setDeathUnitsCost(deathCostBD);*/
				quoteResponse.setCost(deathCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
				log.info("??dcCost? {}",dcCost);
			}
			log.info("deathCostBD : {}",deathCostBD);
		}catch(Exception e){
			log.error("Error in calculate unit amount {}",e);
		}
		log.info("Calculate Dc Unitised Amount and cost finish");
		return ruleModel;
	}
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @return
	 * @throws Exception
	 */
	private static String computeDcUnitsCost(RuleModel ruleModel, InstProductRuleBO instProductRuleBO) {
		log.info("Compute Dc Units cost start");
		double dcWeeklyCost=0.0;
		String dcCost;
		BigDecimal dcAddnlUnitsBD=null;
		BigDecimal tpdAddnlUnitsBD=null;
		BigDecimal dcOnlyUnitsBD=null;
		/*SRKR: FOR FIRS super we need to calculate units cost based on occupation category*/
		if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_FIRS)){
			if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_LOW_RISK)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFactProff())).doubleValue();
			}
		}else if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null 
				&& (instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_HOST) 
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_VICS) //Added for vicsuper
						||instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_SFPS)
						||instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL)
						||instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_ACCS))){
			if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD) || ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_GENERAL)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_WHITECOLLAR)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFactProff())).doubleValue();
			}
			//Added for VicSuper Death Own Occupation
		else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_OWN_OCCUPATION)){
			dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultFacOwnWhiteCollar())).doubleValue();
		}
			
		}
		/*MTAA Changes Start*/
		else if(null != instProductRuleBO && null != instProductRuleBO.getProductCode()
				&& QuoteConstants.PARTNER_MTAA.equalsIgnoreCase(instProductRuleBO.getProductCode())){
			if(ruleModel.getAge()> 26 && instProductRuleBO.getDeathUnitCostGrtThan27() != 0){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostGrtThan27())).doubleValue();
			}else{
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultipFactor())).doubleValue();
			}			
			
		}
		/*MTAA Change End*/
		else if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && (
/*				instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL)
				|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_ACCS)|| */
				instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_REIS))){/*SRKR: For Guild and child care calculating unitised cost is different*/
			/*SRKR Guild and child care Units cost calculation.
			 * Assume 25 units Dth, 20 TPD
			 * Death:
			 * $0.50 x 20 units Death = $10 
			 * $1 x (25 Dth - 20 TPD) = $5
			 * Death:  $10 + $5 = $15 per week
			 * TPD:  $0.50 x 20 TPD = $10 per week*/
			
			/*SRKR:WR8820: 13/08/2014 REIS Units cost calculation.
			 * Assume 25 units Dth, 20 TPD
			 * Death:
			 * $0.55 x 20 units Death = $11 
			 * $0.75 x (25 Dth - 20 TPD) = $3.75
			 * Death:  $11 + $3.75 = $14.75 per week
			 * TPD:  $0.50 x 20 TPD = $10 per week*/			
			dcAddnlUnitsBD=new BigDecimal(ruleModel.getDeathUnits());
			tpdAddnlUnitsBD=new BigDecimal(ruleModel.getTpdUnits());
	
			if(dcAddnlUnitsBD!=null && tpdAddnlUnitsBD!=null){
				dcOnlyUnitsBD=dcAddnlUnitsBD.subtract(tpdAddnlUnitsBD);
				/*e.g death 10 tpd 4 units
				 * then (10-4)*1 + 4 * 0.5
				 * dcWeeklcost=*/
				dcWeeklyCost=(dcOnlyUnitsBD.multiply(BigDecimal.valueOf(instProductRuleBO.getDeathOnlyMultipFactor()))).add(tpdAddnlUnitsBD.multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultipFactor()))).doubleValue();
			}else if(dcAddnlUnitsBD!=null){
				dcOnlyUnitsBD=dcAddnlUnitsBD;
				dcWeeklyCost=(dcOnlyUnitsBD.multiply(BigDecimal.valueOf(instProductRuleBO.getDeathOnlyMultipFactor()))).doubleValue();
			}
			
		}else if(null != instProductRuleBO){
			dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(BigDecimal.valueOf(instProductRuleBO.getDeathUnitCostMultipFactor())).doubleValue();
		}
		dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_WEEKLY, dcWeeklyCost, ruleModel);
		log.info("Compute Dc Units cost finish");
		return dcCost;
	}

	/**
	 * @param presentCostType
	 * @param presentCoverCost
	 * @param ruleModel
	 * @return
	 * @throws Exception
	 */
	public static String calculateAddnlCostByFreq(String presentCostType,double presentCoverCost,RuleModel ruleModel){
		log.info("Calculate addnl cost by freq start");
		double addnlCost=0;
		try{
			if(ruleModel!=null && ruleModel.getPremiumFrequency()!=null && presentCostType!=null){
				if(presentCostType.equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
					if(QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode()))
					{
						if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
							addnlCost=((presentCoverCost / QuoteConstants.perWeek) * QuoteConstants.perYear) / QuoteConstants.perMonth;
						} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
							addnlCost=presentCoverCost;
						}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
							addnlCost=(presentCoverCost / QuoteConstants.perWeek) * QuoteConstants.perYear;
						}
					}
					else
					{
					if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
						addnlCost=(presentCoverCost*52)/12;
					} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
						addnlCost=presentCoverCost;
					}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
						addnlCost=presentCoverCost*52;
					}
					}
				}else if(presentCostType.equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
					
					if(QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode()))
					{
						if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
							addnlCost=presentCoverCost;
						} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
							addnlCost=((presentCoverCost * QuoteConstants.perMonth) / QuoteConstants.perYear) * QuoteConstants.perWeek;
						}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
							addnlCost=presentCoverCost * QuoteConstants.perMonth;
						}
					}
					else
					{
					if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
						addnlCost=presentCoverCost;
					} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
						addnlCost=(presentCoverCost*12)/52;
					}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
						addnlCost=presentCoverCost*12;
					}
					}
				}else if(presentCostType.equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
					if(QuoteConstants.PARTNER_VICS.equalsIgnoreCase(ruleModel.getFundCode()))
					{

						if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
							addnlCost=presentCoverCost / QuoteConstants.perMonth;
						} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
							addnlCost=(presentCoverCost  / QuoteConstants.perYear) * QuoteConstants.perWeek;
						}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
							addnlCost=presentCoverCost;
						}
						
					}
					else
					{
					if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
						addnlCost=presentCoverCost/12;
					} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
						addnlCost=presentCoverCost/52;
					}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
						addnlCost=presentCoverCost;
					}
					}
				}
			}
		}catch(Exception e){
			log.error("Error in calculate addnl cost by freq: {} ",e);
		}
		log.info("Calculate addnl cost by freq finish");
		return Double.toString(addnlCost);
	}
	
	/**
	 * @param lodgeFundId
	 * @param memberType
	 * @param managetype
	 * @param ruleInfoMap
	 * @param ruleFilePath
	 * @return
	 * @throws Exception
	 */
	public static InstProductRuleBO invokeInstProductBusinessRule(String lodgeFundId,String memberType,String managetype,Map ruleInfoMap,String ruleFilePath){
		log.info("Invoke Inst Product BusinessRule start");
		InstProductRuleBO instProductRuleBO = new InstProductRuleBO();
		try{
			if(lodgeFundId!=null){
				/*String RULE_FILE_LOC="E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";*/
				instProductRuleBO.setProductCode(lodgeFundId);
				instProductRuleBO.setMemberType(memberType);
				instProductRuleBO.setManageType(managetype);
				if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID)!=null){
					instProductRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID));
				}
				instProductRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID);
				instProductRuleBO = DroolsHelper.invokeInstProductBusinessRule(instProductRuleBO,ruleFilePath);
			}
		}catch(Exception e){
			log.error("Error in Invoke Inst Product BusinessRule: {} ",e);
		}
		log.info("Invoke Inst Product BusinessRule finish");
		return instProductRuleBO;
	}
	

	
	/**
	 * @param obj
	 * @return
	 */
	public static boolean isNull(Object obj){
		if(obj == null){
			return false;
		}else{
			/*if(obj.toString().equalsIgnoreCase("") || obj.toString().equalsIgnoreCase("null")){
				return false;
			}else{
				return true;
			}*/
			return !(obj.toString().equalsIgnoreCase("") || obj.toString().equalsIgnoreCase("null"));
		}
	}
	public static BigDecimal createMultiplesByFactor(BigDecimal cvrAmountBD,BigDecimal factor){
		log.info("Create multiples by factor start");
		BigDecimal cvrAmtBD = cvrAmountBD;
		try{
			BigDecimal quotient=null;
			if(cvrAmtBD!=null && null!=factor){
				quotient=cvrAmtBD.divide(factor);
				if(quotient.scale()>0){
					quotient=quotient.setScale(0, BigDecimal.ROUND_UP);
					cvrAmtBD=quotient.multiply(factor);
					cvrAmtBD=cvrAmtBD.setScale(0, BigDecimal.ROUND_HALF_UP);
				}
			}
			
		}catch(Exception e){
			log.error("Error in create multiples by factor:{}",e);
		}
		log.info("Create multiples by factor finish");
		return cvrAmtBD;
	}
	
	public static BigDecimal createIPMultiplesByFactor(BigDecimal cvrAmountBD,BigDecimal factor){
		log.info("Create multiples by factor start");
		BigDecimal cvrAmtBD = cvrAmountBD;
		try{
			BigDecimal quotient=null;
			BigDecimal adder = new BigDecimal("1");
			if(cvrAmtBD!=null && null!=factor){
				quotient = cvrAmtBD.remainder(factor);
				if(quotient.compareTo(BigDecimal.ZERO)>0)
				{
					cvrAmtBD = ((cvrAmtBD.divide(factor, 0, RoundingMode.FLOOR)).add(adder)).multiply(factor);
				}
			}
			
		}catch(Exception e){
			log.error("Error in create multiples by factor : {}",e);
		}
		log.info("Create multiples by factor finish");
		return cvrAmtBD;
	}
	
	public static BigDecimal convertDeathFixedToUnits(RuleModel ruleModel,Map ruleInfoMap,String ruleFilePath){
		log.info("Convert Death Fixed To Units start");
		BigDecimal convertedUnits=null;
		BigDecimal oneUnitBD=null;
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			/*PrimeSuperFundRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue(QuoteConstants.RULE_FILE_LOC, QuoteConstants.RULE_FILE_LOC);*/
			coverDetailsRuleBO.setDeathUnits(1);
			coverDetailsRuleBO.setTpdUnits(0);

			if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT));
			}
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			log.info("InsuranceCategoryStr Death Calc------{}" ,coverDetailsRuleBO.getInsuranceCategoryStr());
			
			coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				log.info("Death Amount----{}" ,coverDetailsRuleBO.getCoverAmount());
				oneUnitBD=new BigDecimal(Double.toString(coverDetailsRuleBO.getCoverAmount())).setScale(0, BigDecimal.ROUND_HALF_UP);
				if(ruleModel.getDeathExistingAmount()!=null && oneUnitBD!=null){
					convertedUnits=ruleModel.getDeathExistingAmount().divide(oneUnitBD,0,BigDecimal.ROUND_UP);
				}
			}
			
		}catch(Exception e){
			log.error("Error in convert Death Fixed To Units :{}",e);
		}
		log.info("Convert Death Fixed To Units finish");
		return convertedUnits;
	}
	
	public static BigDecimal convertTpdFixedToUnits(RuleModel ruleModel,Map ruleInfoMap,String ruleFilePath){
		log.info("Convert Tpd Fixed To Units start");
		BigDecimal convertedUnits=null;
		BigDecimal oneUnitBD=null;
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			String ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT;
			coverDetailsRuleBO.setUnits(1);
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				log.info("TPD Amount----{}" ,coverDetailsRuleBO.getCoverAmount());
				oneUnitBD=new BigDecimal(Double.toString(coverDetailsRuleBO.getCoverAmount())).setScale(0, BigDecimal.ROUND_HALF_UP);
				if(ruleModel.getTpdExistingAmount()!=null && oneUnitBD!=null){
					convertedUnits=ruleModel.getTpdExistingAmount().divide(oneUnitBD,0,BigDecimal.ROUND_UP);
				}
			}
			
		}catch(Exception e){
			log.error("Error in convert Tpd Fixed To Units: {} ",e);
		}
		log.info("Convert Tpd Fixed To Units finish");
		return convertedUnits;
	}
	
public static String getNPSToken(String urlStr) throws IOException, JSONException {
	  log.info("Get NPS token start");
	  URL url = new URL(urlStr);
	  JSONObject jsObject = null;
	  StringBuilder responseBuff = null;
	  String token = null;
	  HttpURLConnection conn =
	      (HttpURLConnection) url.openConnection();

	  if (conn.getResponseCode() != 200) {
	    throw new IOException(conn.getResponseMessage());
	  }

	 /*Buffer the result into a string*/
	  BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  responseBuff = new StringBuilder();
	  String line;
	  while ((line = rd.readLine()) != null) {
		  log.info("line>> {}",line);
	   responseBuff.append(line);
	  }
	  rd.close();		 
	
	  jsObject = new JSONObject(responseBuff.toString());
	  token= (String)jsObject.get("message");
	  log.info("token>> {}",token);			
	  conn.disconnect();	
	  log.info("Get NPS token finish");
	  return token;
	}

public static BigDecimal occupationrelateddeathcost (RuleModel ruleModel) {
	BigDecimal tpdAddnlCvrBd=null;
	BigDecimal dcAddnlCvrBd=null;
	BigDecimal remainingDeath=null;
	/*boolean deathAndTpdCoverTypeSame=true;*/
	boolean isDeathAndTpdApplied=false;
	if(ruleModel!=null && ruleModel.getDeathFixedAmount()!=null){
		dcAddnlCvrBd=ruleModel.getDeathFixedAmount();
	}
	
	if(ruleModel!=null && ruleModel.getTpdFixedAmount()!=null){
		tpdAddnlCvrBd=ruleModel.getTpdFixedAmount();
	}
			log.info(">dcAddnlCvrBd> {}",dcAddnlCvrBd);
			log.info(">tpdAddnlCvrBd> {}",tpdAddnlCvrBd);
		
		if(dcAddnlCvrBd!=null && tpdAddnlCvrBd!=null && dcAddnlCvrBd.compareTo(BigDecimal.ZERO)>0 && tpdAddnlCvrBd.compareTo(BigDecimal.ZERO)>0){
			isDeathAndTpdApplied=true;
		}
		if(isDeathAndTpdApplied && dcAddnlCvrBd.compareTo(tpdAddnlCvrBd)>0){
			remainingDeath=dcAddnlCvrBd.subtract(tpdAddnlCvrBd);
		}
	return remainingDeath;
	
}
public static QuoteResponse calculateFulAmount(InstProductRuleBO instProductRuleBO,RuleModel ruleModel,Map ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse) {
	log.info("Calculate death fixed cost start");
	/*String fulCost = null;*/
	String ruleNameAsPerFund;
	CoverDetailsRuleBO FULRuleBO=new CoverDetailsRuleBO();
	ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_FUL_LMT;
	FULRuleBO.setFundCode(ruleModel.getFundCode());
	//For Death Only 
	if(null!=ruleModel.getDeathFixedAmount() && ruleModel.getDeathFixedAmount().doubleValue()!=0){
		FULRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
		FULRuleBO.setTotalCoverAmt(ruleModel.getDeathFixedAmount().doubleValue());
		FULRuleBO.setRevisedCoverAmt(((ruleModel.getDeathFixedAmount()).multiply(new BigDecimal("1.3"))).doubleValue());		
		
		// CoverDetailsRuleBO.DEATH_TPD, CoverDetailsRuleBO.DEATH_ONLY, CoverDetailsRuleBO.IP_COVER
		
		
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			FULRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		}
		FULRuleBO.setRuleFileName(ruleNameAsPerFund);
		FULRuleBO=DroolsHelper.invokeUC16BusinessRules(FULRuleBO,ruleFileLocation);
		if(null != FULRuleBO &&  FULRuleBO.isBandChange() && FULRuleBO.isAalStatus()){
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_BAND_LMT;
			FULRuleBO.setRevisedCoverAmt(FULRuleBO.getMaxThresholdCvrAmt()-FULRuleBO.getTotalCoverAmt());
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				FULRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			FULRuleBO=DroolsHelper.invokeUC16BusinessRules(FULRuleBO,ruleFileLocation);
			quoteResponse.setFulDeathAmount(roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());
			quoteResponse.setDcAalStatus(Boolean.FALSE);
		}else if(null != FULRuleBO && FULRuleBO.isAalStatus() ){
			quoteResponse.setFulDeathAmount(roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());
			quoteResponse.setDcAalStatus(Boolean.FALSE);
		}else if(null != FULRuleBO && !FULRuleBO.isAalStatus() ){
			quoteResponse.setFulDeathAmount(roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());
			//coverQuoteDetailsVO.setDcAalStatus(Boolean.TRUE);
		}
		log.info("coverQuoteDetailsVO.getFulDeathAmount----- {}" ,quoteResponse.getFulDeathAmount());
		
		// Comparing FUL amount with max amount 
		if(quoteResponse.getFulDeathAmount() != null){
			BigDecimal fullDeathAmt=new BigDecimal(quoteResponse.getFulDeathAmount());
			if(ruleModel.getDeathMaxAmount()!= null && fullDeathAmt.compareTo(ruleModel.getDeathMaxAmount())>0){
				String amount = null;
				
				quoteResponse.setFulDeathAmount((ruleModel.getDeathMaxAmount()).toString());
				
				amount = (roundOfAmount(ruleModel.getDeathMaxAmount(), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());
				
				log.info("death amount----- {}" ,amount);
				log.info("FulDeathAmount-----{}" ,quoteResponse.getFulDeathAmount());
				
			}
			
		}
	}/*else{
		coverQuoteDetailsVO.setDcAalStatus(Boolean.TRUE);
	}*/
	
	
	
	
	//For TPD Only
	if(null!=ruleModel.getTpdFixedAmount() && ruleModel.getTpdFixedAmount().doubleValue()!=0){
		FULRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
		FULRuleBO.setTotalCoverAmt(ruleModel.getTpdFixedAmount().doubleValue());
		FULRuleBO.setRevisedCoverAmt(((ruleModel.getTpdFixedAmount()).multiply(new BigDecimal("1.3"))).doubleValue());		
		
		
		// CoverDetailsRuleBO.DEATH_TPD, CoverDetailsRuleBO.DEATH_ONLY, CoverDetailsRuleBO.IP_COVER
		
		ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_FUL_LMT;
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			FULRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		}
		FULRuleBO=DroolsHelper.invokeUC16BusinessRules(FULRuleBO,ruleFileLocation);
		
		
		if(null != FULRuleBO &&  FULRuleBO.isBandChange() && FULRuleBO.isAalStatus()){
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_BAND_LMT;
			FULRuleBO.setRevisedCoverAmt(FULRuleBO.getMaxThresholdCvrAmt()-FULRuleBO.getTotalCoverAmt());
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				FULRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			FULRuleBO=DroolsHelper.invokeUC16BusinessRules(FULRuleBO,ruleFileLocation);
			quoteResponse.setFulTPDAmount(roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());
			quoteResponse.setTpdAalStatus(Boolean.FALSE);
		}else if(null != FULRuleBO && FULRuleBO.isAalStatus()){
			quoteResponse.setFulTPDAmount(roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());	
			quoteResponse.setTpdAalStatus(Boolean.FALSE);
		}else if(null != FULRuleBO && !FULRuleBO.isAalStatus() ){
			quoteResponse.setFulTPDAmount(roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), MetlifeInstitutionalConstants.MAX_THRESHOLD).toString());
			//coverQuoteDetailsVO.setTpdAalStatus(Boolean.TRUE);
		}
		
//		 Comparing FUL amount with max amount 
		if(quoteResponse.getFulTPDAmount() != null){
			BigDecimal fullTPDAmt=new BigDecimal(quoteResponse.getFulTPDAmount());
			if(ruleModel.getTpdMaxAmount()!= null && fullTPDAmt.compareTo(ruleModel.getTpdMaxAmount())>0){
				String tpdamount = null;
				BigDecimal tpdamountReturned = null;
				quoteResponse.setFulTPDAmount(ruleModel.getTpdMaxAmount().toString());
				tpdamountReturned = roundOfAmount(ruleModel.getTpdMaxAmount(), MetlifeInstitutionalConstants.MAX_THRESHOLD);
				if(null!=tpdamountReturned)
				{
				tpdamount = tpdamountReturned.toString();
				}
				log.info("TPD amount----- {}",tpdamount);
			}
			
		}
		
	}/*else{
		coverQuoteDetailsVO.setTpdAalStatus(Boolean.TRUE);
	}*/
	
	
	/*if(null != FULRuleBO){
		if(FULRuleBO.getOriginalScale() >= FULRuleBO.getRevisedScale()){
			coverQuoteDetailsVO.setFulTPDAmount(""+FULRuleBO.getRevisedCoverAmt());
		}else{
			coverQuoteDetailsVO.setFulTPDAmount(""+FULRuleBO.getMaxThresholdCvrAmt());
		}	
	}*/
	
	
	//FOr IP Only
	if(null!=ruleModel.getIpFixedAmount() && ruleModel.getIpFixedAmount().doubleValue()!=0){
		FULRuleBO.setCoverType(CoverDetailsRuleBO.IP_COVER);
		FULRuleBO.setTotalCoverAmt(ruleModel.getIpFixedAmount().doubleValue());
		FULRuleBO.setRevisedCoverAmt(((ruleModel.getIpFixedAmount()).multiply(new BigDecimal("1.3"))).doubleValue());		
		
		if(ruleModel.getIpWaitingPeriod()!=null ){
			if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_30_DAYS)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_30);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_60_DAYS)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_60);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_90_DAYS)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_90);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_1_YEAR)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_1_YEAR);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_3_YEAR)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_3_YEARS);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_45_DAYS)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_45);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_820_DAYS)){
				FULRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_820);
			}
		}
		
		if(ruleModel.getIpBenefitPeriod()!=null){
			if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)){
				FULRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_2);
			}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_5_YEARS)){
				FULRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_5);
			}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_60)){
				FULRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_60);
			}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_65)){
				FULRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_65);
			}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_67)){
				FULRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
			}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_TO_AGE_67)){
				FULRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
			}
		}
		

		
		// CoverDetailsRuleBO.DEATH_TPD, CoverDetailsRuleBO.DEATH_ONLY, CoverDetailsRuleBO.IP_COVER
		
		log.info("FULRuleBO.setWaitingPeriod>> {}",FULRuleBO.getWaitingPeriod());
		log.info("FULRuleBO.setBenefitPeriod>> {}",FULRuleBO.getBenefitPeriod());
		
		log.info("FULRuleBO.setRevisedCoverAmt>> {}",FULRuleBO.getRevisedCoverAmt());
		
		log.info("FULRuleBO.setTotalCoverAmt>> {}",FULRuleBO.getTotalCoverAmt());
		log.info("FULRuleBO.setMaxThresholdCvrAmt>> {}",FULRuleBO.getMaxThresholdCvrAmt());
	
		ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_FUL_LMT;
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			FULRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		}
		FULRuleBO=DroolsHelper.invokeUC16BusinessRules(FULRuleBO,ruleFileLocation);
		BigDecimal maxThresholdCvrAmt = roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), "100");
		if(null != FULRuleBO &&  FULRuleBO.isBandChange() && FULRuleBO.isAalStatus() && null != maxThresholdCvrAmt){
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_BAND_LMT;
			FULRuleBO.setRevisedCoverAmt(FULRuleBO.getMaxThresholdCvrAmt()-FULRuleBO.getTotalCoverAmt());			
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				FULRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			FULRuleBO=DroolsHelper.invokeUC16BusinessRules(FULRuleBO,ruleFileLocation);
			BigDecimal maxThresholdCvrAmtAfterBand = roundOfAmount(BigDecimal.valueOf(FULRuleBO.getMaxThresholdCvrAmt()), "100");
			quoteResponse.setIpFulAmount(maxThresholdCvrAmtAfterBand.toString());
			quoteResponse.setIpAalStatus(Boolean.FALSE);
		}else if(null != FULRuleBO && FULRuleBO.isAalStatus() && null != maxThresholdCvrAmt){
			quoteResponse.setIpFulAmount(maxThresholdCvrAmt.toString());		
			quoteResponse.setIpAalStatus(Boolean.FALSE);
		}else if(null != FULRuleBO && !FULRuleBO.isAalStatus() && null != maxThresholdCvrAmt){
			quoteResponse.setIpFulAmount(maxThresholdCvrAmt.toString());
			//coverQuoteDetailsVO.setIpAalStatus(Boolean.TRUE);
		}
		
//		 Comparing FUL amount with max amount 
		if(quoteResponse.getIpFulAmount() != null){
			BigDecimal fullIPAmt=new BigDecimal(quoteResponse.getIpFulAmount());
			if(ruleModel.getIpMaxAmount()!= null && fullIPAmt.compareTo(ruleModel.getIpMaxAmount())>0){
				String ipamount = null;
				BigDecimal ipamountReturned = null;
				quoteResponse.setIpFulAmount(ruleModel.getIpMaxAmount().toString());	
				ipamountReturned = roundOfAmount(ruleModel.getIpMaxAmount(),  "100");
				if(null!= ipamountReturned){
					ipamount = ipamountReturned.toString();
				}
				log.info("ipamount----- {}" ,ipamount);
				log.info("FulDeathAmount-----{}" ,quoteResponse.getIpFulAmount());
				
			}
			
		}
	}/*else{
		coverQuoteDetailsVO.setIpAalStatus(Boolean.TRUE);
	}*/
	
	
/*if(!coverQuoteDetailsVO.isDcAalStatus() || !coverQuoteDetailsVO.isTpdAalStatus() || !coverQuoteDetailsVO.isIpAalStatus()){
	coverQuoteDetailsVO.setOverAllAalStatus(Boolean.FALSE);
}else{
	coverQuoteDetailsVO.setOverAllAalStatus(Boolean.TRUE);
}*/
	
	
	return quoteResponse;
}
public static BigDecimal roundOfAmount(BigDecimal cvrAmountBD, String roundOfFactor){

	BigDecimal cvrAmtBD = cvrAmountBD;
	try{
		BigDecimal multiple=new BigDecimal(roundOfFactor);
		BigDecimal quotient=null;
		if(cvrAmtBD!=null){
			quotient=cvrAmtBD.divide(multiple);
			if(quotient.scale()>0){
				quotient=quotient.setScale(0, BigDecimal.ROUND_UP);
				cvrAmtBD=quotient.multiply(multiple);					

			}
		}
		
	}catch(Exception e){
		log.error("Error in rounding amount : {}",e.getMessage());
	}
	return cvrAmtBD;

}

public static QuoteResponse calculateAAL(RuleModel ruleModel,Map ruleInfoMap,
		String ruleFileLocation,QuoteResponse quoteResponse) {
	log.info("Calculate death fixed cost start");
	/*String fulCost = null;*/
	String ruleNameAsPerFund;
	
	ruleNameAsPerFund=QuoteConstants.RULE_NAME_SFPS_AAL_STATUS_FIXED;
	//FULRuleBO.setFundCode(ruleModel.getFundCode());
	//For Death Only 
	
	
	
	int deathunits=ruleModel.getDeathUnits();
	int tpdunits=ruleModel.getTpdUnits();
	int ipunits=ruleModel.getIpUnits();
	
	
	if(tpdunits> 0){
		CoverDetailsRuleBO aaLRuleBO=new CoverDetailsRuleBO();
		aaLRuleBO.setFundCode(ruleModel.getFundCode());
		aaLRuleBO.setAge(ruleModel.getAge());
		aaLRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD); 
		aaLRuleBO.setUserAALunits(tpdunits);
		aaLRuleBO.setInsuranceCategoryStr("Professional");
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			
			aaLRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		} 
		aaLRuleBO = DroolsHelper.invokeUC16BusinessRules(aaLRuleBO,ruleFileLocation); 
		
		if("NUW".equalsIgnoreCase(aaLRuleBO.getAALevelStatus())){
			quoteResponse.setTpdAalStatus(true);
		}
		else{
			quoteResponse.setTpdAalStatus(false);
		}
		
	}
	if(deathunits > 0){
		CoverDetailsRuleBO aaLRuleBO=new CoverDetailsRuleBO();
		aaLRuleBO.setFundCode(ruleModel.getFundCode());
		aaLRuleBO.setAge(ruleModel.getAge());
		aaLRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
		aaLRuleBO.setUserAALunits(deathunits);
		aaLRuleBO.setInsuranceCategoryStr("Professional");
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			aaLRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		} 
		aaLRuleBO = DroolsHelper.invokeUC16BusinessRules(aaLRuleBO,ruleFileLocation); 
		if("NUW".equalsIgnoreCase(aaLRuleBO.getAALevelStatus())){
			quoteResponse.setDcAalStatus(true);
		}
		else{
			quoteResponse.setDcAalStatus(false);
		}
	}
	if(ipunits > 0){
		CoverDetailsRuleBO aaLRuleBO=new CoverDetailsRuleBO();
		aaLRuleBO.setFundCode(ruleModel.getFundCode());
		aaLRuleBO.setAge(ruleModel.getAge());
		aaLRuleBO.setCoverType(CoverDetailsRuleBO.IP_COVER); 
		aaLRuleBO.setUserAALunits(ipunits);
		aaLRuleBO.setInsuranceCategoryStr("Professional");
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			aaLRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		} 
		aaLRuleBO = DroolsHelper.invokeUC16BusinessRules(aaLRuleBO,ruleFileLocation); 
		if("NUW".equalsIgnoreCase(aaLRuleBO.getAALevelStatus())){
			quoteResponse.setIpAalStatus(true);
		}
		else{
			quoteResponse.setIpAalStatus(false);
		}
	}
	
	
	

		
	
	
/*if(!coverQuoteDetailsVO.isDcAalStatus() || !coverQuoteDetailsVO.isTpdAalStatus() || !coverQuoteDetailsVO.isIpAalStatus()){
	coverQuoteDetailsVO.setOverAllAalStatus(Boolean.FALSE);
}else{
	coverQuoteDetailsVO.setOverAllAalStatus(Boolean.TRUE);
}*/
	
	
	return quoteResponse;
}



public static String calculateINGDIPFixedCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,Map ruleInfoMap,String ruleFilePath,QuoteResponse quoteResponse) {
	log.info("Calculate IP fixed cost start");
	String ruleNameAsPerFund;
	double ipYearlyCost=0;
	String ipCost =null;
	BigDecimal ipCostBD=null;
	BigDecimal ipFixedAmt = null;
	try{
		CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
		coverDetailsRuleBO.setAge(ruleModel.getAge());
		if(ruleModel.getGender()!=null){
			if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
				coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
			}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
				coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
			}
		}
		if(ruleModel.getIpBenefitPeriod()!=null){
			if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)){
				coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_2);
			}
			else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_67)){
				coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
			}
		}
		if(ruleModel.getIpWaitingPeriod()!=null ){
			if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_30_DAYS)){
				coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_30);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_60_DAYS)){
				coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_60);
			}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_90_DAYS)){
				coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_90);
			}
		}	
			ipFixedAmt=createMultiplesByFactor(ruleModel.getIpFixedAmount(), instProductRuleBO.getIpMultipleFactor());
			ruleModel.setIpFixedAmount(ipFixedAmt);
			coverDetailsRuleBO.setCoverAmount(ruleModel.getIpFixedAmount().doubleValue());
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getIpOccCategory());
			if(ruleModel.getFundCode()!=null){
				ruleNameAsPerFund=ruleModel.getFundCode()+QuoteConstants.RULE_NAME_INST_IP_FXD_COST;
				if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
					coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
				}
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
				log.info("coverDetailsRuleBO.getYearlyCost()----> {}",coverDetailsRuleBO.getYearlyCost());
					coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.IP_COVER, ruleModel.getIpOccCategory(), coverDetailsRuleBO,ruleModel.getFundCode(),ruleFilePath);	
					ipYearlyCost=coverDetailsRuleBO.getYearlyCost();
					ipCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, ipYearlyCost, ruleModel);
			}
			
			if(instProductRuleBO.getIpMultiPolicyDiscount()!=0.0 && 
					ruleModel.getDeathFixedAmount()!=null && ruleModel.getDeathFixedAmount().compareTo(BigDecimal.ZERO)>0 &&
							ruleModel.getTpdFixedAmount()!=null && ruleModel.getTpdFixedAmount().compareTo(BigDecimal.ZERO)>0){
				BigDecimal ipCostBd=null;
				BigDecimal discountFactor=null;
				/*Apply the discount on IP Premium*/
				discountFactor= BigDecimal.valueOf(instProductRuleBO.getIpMultiPolicyDiscount());
				ipCostBd=new BigDecimal(ipCost);
				coverDetailsRuleBO.setIpMultiplyPolicyDiscount((ipCostBd.multiply(discountFactor)).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			
			if(coverDetailsRuleBO.getIpMultiplyPolicyDiscount()!=null && ipCost!=null){
				ipCost=new BigDecimal(ipCost).subtract(coverDetailsRuleBO.getIpMultiplyPolicyDiscount()).toString();
			}
			String ipcost=new BigDecimal(ipCost).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
			
			coverDetailsRuleBO=calculateIpStampDuty(coverDetailsRuleBO, ruleModel, ruleInfoMap,ruleFilePath,ipcost);
			if(coverDetailsRuleBO.getIpStampDutyRate()!=null  && ipCost!=null && ruleModel.getIpFixedAmount().compareTo(BigDecimal.ZERO)!=0){
				ipCost=new BigDecimal(ipCost).add(coverDetailsRuleBO.getIpStampDutyRate()).toString();
			}
			if(ipCost!=null) {
				ipCostBD=new BigDecimal(ipCost);
				quoteResponse.setCoverAmount(ruleModel.getIpFixedAmount());
				quoteResponse.setCost(ipCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
			}
				log.info("IP cost in coverDetailsHelper>>> {}",ipCost);
				}catch(Exception e){
				log.error("Error in calculate IP fixed cost: {}",e);
				throw e;
	}
	log.info("Calculate IP fixed cost finish",ipCost);
	return ipCost;
}

/**
 * There is no stamp duty payable for Death only benefit or for Death & TPD benefit
 * where Death and TPD benefit amount are the same.
 * 
 * The premium rates for TPD only benefit does not include stamp duty
 * 
 * There is additional stamp duty payable for TPD stand alone benefit or for any amount 
 * of TPD cover in excess of the Death cover (when applying for both Death & TPD)
 * 
 * Stamp duty is calculated based on the covered person�s state 
 * 
 * calculate stamp duty and include in the premium payable after the base premium has
 * been loaded with occupational and health loading
 * 
 * @param coverQuoteDetailsVO
 * @param ruleModel
 * @param ruleInfoMap
 * @return
 */
public static CoverDetailsRuleBO calculateIpStampDuty(CoverDetailsRuleBO coverQuoteDetailsVO,RuleModel ruleModel, Map ruleInfoMap,String ruleFilePath,String ipCost){
	try{
		if(coverQuoteDetailsVO!=null && ruleModel!=null && ruleInfoMap!=null){
		
			if(/*coverQuoteDetailsVO.isIpFilled() && coverQuoteDetailsVO.getIpAddnlCvrAmt()!=null &&*/ ipCost!=null && !ipCost.equalsIgnoreCase("")){
				/*Get the stamp duty rate for the IP amount*/
				CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
				coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());	
				coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.IP_COVER);
				coverDetailsRuleBO.setStateCode(ruleModel.getStateCode());
				coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_STAMP_DUTY);
				if(ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_STAMP_DUTY)!=null){
					coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_STAMP_DUTY));
				}
				coverDetailsRuleBO.setRuleFileName(coverDetailsRuleBO.getRuleFileName());
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
				if(ruleModel.getIpCoverStartDate()!=null && !ruleModel.getIpCoverStartDate().equalsIgnoreCase("")){
					SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
					coverDetailsRuleBO.setTpdCoverStartDate(sdf.parse(ruleModel.getIpCoverStartDate()));
					if(coverDetailsRuleBO.getTpdCoverStartDate().before(new Date("08/01/2013"))){
						coverDetailsRuleBO.setBeforeAug2013(true);
					}else{
						coverDetailsRuleBO.setBeforeAug2013(false);
					}
					if(ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_STAMP_DUTY)!=null){
						coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_STAMP_DUTY));
					}
					coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
					log.info(">coverDetailsRuleBO.getIpCoverStartDate>"+coverDetailsRuleBO.getTpdCoverStartDate());
				}					
				
				
				
				if(coverDetailsRuleBO.getStampDutyRate()!=0){
					coverQuoteDetailsVO.setIpStampDutyRate((new BigDecimal(ipCost).multiply(new BigDecimal(coverDetailsRuleBO.getStampDutyRate()))).setScale(2, BigDecimal.ROUND_HALF_UP));
					log.info(">ip>>>coverDetailsRuleBO.getStampDutyRate()>"+coverDetailsRuleBO.getStampDutyRate());
				}
			}else{
				coverQuoteDetailsVO.setIpStampDutyRate(null);
			}
		}
		
	}catch(Exception e){
		log.error("Error in calculating IP Stamp Duty"+e);
	}
	log.info("Finished Calculating calculateIpStampDuty");
	return coverQuoteDetailsVO;
}
public static CoverDetailsRuleBO calculateIpMultiPolicyDiscountForLoadingAmt(CoverDetailsRuleBO coverQuoteDetailsVO, InstProductRuleBO instProductRuleBO, String ipAddCost){
	BigDecimal ipCostBd=null;
	BigDecimal discountFactor=null;
	try{
		if(coverQuoteDetailsVO!=null && instProductRuleBO!=null){
				if(instProductRuleBO.getIpMultiPolicyDiscount()!=0.0){
					/*Apply the discount on IP Premium*/
					discountFactor= BigDecimal.valueOf(instProductRuleBO.getIpMultiPolicyDiscount());
					ipCostBd=new BigDecimal(ipAddCost);
					coverQuoteDetailsVO.setIpMultiplyPolicyDiscount((ipCostBd.multiply(discountFactor)).setScale(2, BigDecimal.ROUND_HALF_UP));
				}else{
					coverQuoteDetailsVO.setIpMultiplyPolicyDiscount(null);
				}
		}
	}catch(Exception e){
		log.error("Error in Calculating IP Policy Discount");
	}
	return coverQuoteDetailsVO;
}	
public static String calculateINGDTPDFixedCost(RuleModel ruleModel,Map ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse,InstProductRuleBO instProductRuleBO){
	log.info("Calculate TPD fixed cost start");
	BigDecimal tpdAddnlCvrBd=null;
	String ruleNameAsPerFund;
	double tpdYearlyCost;
	String tpdCost=null;
	String tpdStampDutyCost=null;
	try{
		log.info("inside calculateTPDFixedCost");
		CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
		coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
		coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
		coverDetailsRuleBO.setAge(ruleModel.getAge());
		
		ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_FXD_COST;
		if(ruleModel.getGender()!=null){
			if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
				coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
			}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
				coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
			}
		}
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		}
		coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
		coverDetailsRuleBO.setCoverAmount(ruleModel.getTpdFixedAmount().doubleValue());
		coverDetailsRuleBO.setDeathAndTpdApplied(false);
		if(ruleModel.getMemberType()!=null){
			coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
		}
		coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
		BigDecimal tpdStampDutyAmt=calculateExTpdStampDutyAmount(coverDetailsRuleBO,ruleModel);
		
		if(tpdStampDutyAmt!=null) {
			tpdAddnlCvrBd = tpdStampDutyAmt;
			tpdAddnlCvrBd=createMultiplesByFactor(tpdAddnlCvrBd, instProductRuleBO.getTpdMultipleFactor());
			coverDetailsRuleBO.setCoverAmount(tpdAddnlCvrBd.doubleValue());
			coverDetailsRuleBO.setDeathAndTpdApplied(false);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);
			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_TPD, ruleModel.getTpdOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			tpdYearlyCost=coverDetailsRuleBO.getYearlyCost();
			tpdStampDutyCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, tpdYearlyCost, ruleModel);
			coverDetailsRuleBO=calculateTpdStampDuty(coverDetailsRuleBO, ruleModel, ruleInfoMap,tpdStampDutyCost,ruleFileLocation);
			if(coverDetailsRuleBO.getTpdStampDutyRate()!=null  && tpdStampDutyCost!=null)
			{
				tpdStampDutyCost=new BigDecimal(tpdStampDutyCost).add(coverDetailsRuleBO.getTpdStampDutyRate()).toString();
			}
		if(tpdStampDutyCost!=null) {
			CoverDetailsRuleBO ingdTPDCover= new CoverDetailsRuleBO();
			ingdTPDCover.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			ingdTPDCover.setFundCode(ruleModel.getFundCode());
			ingdTPDCover.setAge(ruleModel.getAge());
			tpdAddnlCvrBd = ruleModel.getTpdFixedAmount();
			tpdAddnlCvrBd=createMultiplesByFactor(tpdAddnlCvrBd, instProductRuleBO.getTpdMultipleFactor());
			ingdTPDCover.setCoverAmount(tpdAddnlCvrBd.doubleValue());
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_FXD_COST;
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					ingdTPDCover.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					ingdTPDCover.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				ingdTPDCover.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			ingdTPDCover.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			ingdTPDCover.setCoverAmount(ruleModel.getTpdFixedAmount().subtract(tpdStampDutyAmt).doubleValue());
			ingdTPDCover.setDeathAndTpdApplied(true);
			if(ruleModel.getMemberType()!=null){
				ingdTPDCover.setMemberType(ruleModel.getMemberType());
			}
			ingdTPDCover.setRuleFileName(ruleNameAsPerFund);
			ingdTPDCover.setDeathAndTpdApplied(true);
			ingdTPDCover = DroolsHelper.invokeUC16BusinessRules(ingdTPDCover,ruleFileLocation);
			ingdTPDCover=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_TPD, ruleModel.getTpdOccCategory(), ingdTPDCover, ruleModel.getFundCode(),ruleFileLocation);
			Double tpdOtherYearlyCost=ingdTPDCover.getYearlyCost();
			String tpdOtherCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, tpdOtherYearlyCost, ruleModel);
			if(tpdOtherCost!=null && tpdStampDutyCost!=null){
				quoteResponse.setCost(new BigDecimal(tpdOtherCost).add(new BigDecimal(tpdStampDutyCost)).setScale(2, BigDecimal.ROUND_HALF_UP));
				quoteResponse.setCoverAmount(ruleModel.getTpdFixedAmount());
			}
		}
		}
		else {
			tpdAddnlCvrBd = ruleModel.getTpdFixedAmount();
			tpdAddnlCvrBd=createMultiplesByFactor(tpdAddnlCvrBd, instProductRuleBO.getTpdMultipleFactor());
		ruleModel.setTpdFixedAmount(tpdAddnlCvrBd);
		coverDetailsRuleBO.setDeathAndTpdApplied(true);
		coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);
		coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_TPD, ruleModel.getTpdOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
		tpdYearlyCost=coverDetailsRuleBO.getYearlyCost();
		tpdCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, tpdYearlyCost, ruleModel);
		if(tpdCost!=null){
			quoteResponse.setCost(new BigDecimal(tpdCost).setScale(2, BigDecimal.ROUND_HALF_UP));
			quoteResponse.setCoverAmount(ruleModel.getTpdFixedAmount());
		}
		}
	}
	catch(Exception e){
		log.error("Error in calculate INGD TPD fixed cost : {}",e);
	}
	log.info("Calculate INGD TPD fixed cost finish");
	return tpdCost;
}

public static BigDecimal calculateExTpdStampDutyAmount(CoverDetailsRuleBO coverQuoteDetailsVO,RuleModel ruleModel ){
	BigDecimal dcExAmtBD=null;
	BigDecimal tpdExlAmtBD=null;
	BigDecimal tpdStampDutyAmt=null;
	try{
		if(coverQuoteDetailsVO!=null){
			
			if(ruleModel.getDeathFixedAmount()!=null && !ruleModel.getDeathFixedAmount().equals(BigDecimal.ZERO)){
				dcExAmtBD=ruleModel.getDeathFixedAmount();
			}
			if(null==ruleModel.getDeathFixedAmount()) {
				dcExAmtBD=BigDecimal.valueOf(0);
			}
			
			if(ruleModel.getTpdFixedAmount()!=null && !ruleModel.getTpdFixedAmount().equals(BigDecimal.ZERO)){
				tpdExlAmtBD=ruleModel.getTpdFixedAmount();
			}
				if(dcExAmtBD!=null && tpdExlAmtBD!=null && (dcExAmtBD.compareTo(tpdExlAmtBD)!=0) && (tpdExlAmtBD.compareTo(dcExAmtBD)>0)){
					tpdStampDutyAmt=tpdExlAmtBD.subtract(dcExAmtBD);
			if(ruleModel.getStateCode()!=null && ruleModel.getStateCode().equalsIgnoreCase("VIC")
					&& ruleModel.getFundCode()!=null && ruleModel.getFundCode().equalsIgnoreCase("INGD")
					&& tpdExlAmtBD.compareTo(dcExAmtBD) <=0){
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				String vicStartDate="30/06/2014";
				Date vicsCoverStartDate=null;
				Date tpdCoverStartDate=null;
					vicsCoverStartDate=sdf.parse(vicStartDate);
					if(ruleModel.getTpdCoverStartDate()!=null && !ruleModel.getTpdCoverStartDate().equalsIgnoreCase("")){
						tpdCoverStartDate=sdf.parse(ruleModel.getTpdCoverStartDate());
						if(tpdCoverStartDate.after(vicsCoverStartDate)){
							tpdStampDutyAmt=tpdExlAmtBD;
						}
					else{
						tpdStampDutyAmt=tpdExlAmtBD;
					}
				}
				
			}
				}
		}
	}
			catch(Exception e){
		log.error("Error in Calculating TPD Stamp Amount"+e);
	}
	log.info("Calculate TPD Stamp Duty End");
	return tpdStampDutyAmt;
}
public static CoverDetailsRuleBO calculateTpdStampDuty(CoverDetailsRuleBO coverQuoteDetailsVO,RuleModel ruleModel, Map ruleInfoMap,String tpdCost ,String ruleFilePath){
	try{
		if(coverQuoteDetailsVO!=null && ruleModel!=null && ruleInfoMap!=null){
			/*Check if both death and tpd are applied, if applied then check if both are same*/
			if(tpdCost!=null){
				/*Get the stamp duty rate for the remaining tpd amount*/
				CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
				coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());	
				coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
				coverDetailsRuleBO.setStateCode(ruleModel.getStateCode());
				coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_STAMP_DUTY);
				if(ruleModel.getTpdCoverStartDate()!=null && !ruleModel.getTpdCoverStartDate().equalsIgnoreCase("")){
					SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
					coverDetailsRuleBO.setTpdCoverStartDate(sdf.parse(ruleModel.getTpdCoverStartDate()));
					if(coverDetailsRuleBO.getTpdCoverStartDate().before(new Date("08/01/2013"))){
						coverDetailsRuleBO.setBeforeAug2013(true);
					}else{
						coverDetailsRuleBO.setBeforeAug2013(false);
					}
					log.info(">coverDetailsRuleBO.getTpdCoverStartDate>"+coverDetailsRuleBO.getTpdCoverStartDate());
				}
				coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_STAMP_DUTY);
				if(ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_STAMP_DUTY)!=null){
					coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_STAMP_DUTY));
				}
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);	
				log.info(">>fundDetailsVO.getStampDutyRate()>>>"+coverDetailsRuleBO.getStampDutyRate());
				if(coverDetailsRuleBO.getStampDutyRate()!=0){
					coverQuoteDetailsVO.setTpdStampDutyRate(new BigDecimal(tpdCost).multiply(new BigDecimal(coverDetailsRuleBO.getStampDutyRate())).setScale(2, BigDecimal.ROUND_HALF_UP));
				}
			}else{
				coverQuoteDetailsVO.setTpdStampDutyRate(null);
			}
			
		}
		
	}catch(Exception e){
		log.error("Error in Calculating TPD Stamp Duty"+e);
	}
	log.info("End of Calculation of Stamp Duty");
	return coverQuoteDetailsVO;
}
/**
 * @param ruleModel
 * @param ruleInfoMap
 * @param ruleFileLocation
 * @param quoteResponse
 * @return
 * @throws Exception
 */
public static String calculateINGDDeathFixedCost(InstProductRuleBO instProductRuleBO,RuleModel ruleModel,Map ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse) {
	log.info("Calculate death fixed cost start");
	
	BigDecimal deathAddnlCvrBd=null;
	BigDecimal remainingDeath=null;
	String ruleNameAsPerFund;
	double dcYearlyCost;
	String dcCost = null;
	Double deathCost=null;
	Double dcExcessCost=null;
	CoverDetailsRuleBO coverDetailsRuleBO=null;
	try{
		coverDetailsRuleBO=new CoverDetailsRuleBO();
		coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
		coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
		coverDetailsRuleBO.setAge(ruleModel.getAge());
		deathAddnlCvrBd=createMultiplesByFactor(ruleModel.getDeathFixedAmount(), instProductRuleBO.getDeathMultipleFactor());
		ruleModel.setDeathFixedAmount(deathAddnlCvrBd);
		ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST;
		if(ruleModel.getGender()!=null){
			if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
				coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
			}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
				coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
			}
		}
		if(ruleModel.isSmoker()){
			coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
		}else{
			coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
		}
		if(ruleModel.getMemberType()!=null){
			coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
		}
		if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
			coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
		}
		coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			remainingDeath=occupationrelateddeathcost(ruleModel);
		if(remainingDeath!=null) {
			coverDetailsRuleBO.setCoverAmount(remainingDeath.doubleValue());
			coverDetailsRuleBO.setDeathAndTpdApplied(Boolean.FALSE);
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);	
			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			dcExcessCost=coverDetailsRuleBO.getYearlyCost();
			CoverDetailsRuleBO ingddeathCoverBo= new CoverDetailsRuleBO();
			ingddeathCoverBo.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			ingddeathCoverBo.setFundCode(ruleModel.getFundCode());
			ingddeathCoverBo.setAge(ruleModel.getAge());
			if(ruleModel.getMemberType()!=null){
				ingddeathCoverBo.setMemberType(ruleModel.getMemberType());
			}
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					ingddeathCoverBo.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					ingddeathCoverBo.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			ingddeathCoverBo.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				ingddeathCoverBo.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			ingddeathCoverBo.setCoverAmount((ruleModel.getDeathFixedAmount().subtract(remainingDeath)).doubleValue());
	
	
			ingddeathCoverBo.setRuleFileName(ruleNameAsPerFund);
			ingddeathCoverBo.setDeathAndTpdApplied(Boolean.TRUE);
			ingddeathCoverBo = DroolsHelper.invokeUC16BusinessRules(ingddeathCoverBo,ruleFileLocation);	
			if(log.isDebugEnabled())
			{
			log.info(String.valueOf(ingddeathCoverBo.getYearlyCost()));
			}
			ingddeathCoverBo.setDeathAndTpdApplied(Boolean.TRUE);
			ingddeathCoverBo=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), ingddeathCoverBo, ruleModel.getFundCode(),ruleFileLocation);
			deathCost=ingddeathCoverBo.getYearlyCost();
			if(deathCost!=null && dcExcessCost!=null){
				dcYearlyCost = (new BigDecimal(deathCost).add(new BigDecimal(dcExcessCost))).doubleValue();
				 dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, dcYearlyCost, ruleModel);
			}
		}
		else {
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			coverDetailsRuleBO.setCoverAmount(ruleModel.getDeathFixedAmount().doubleValue());
			if(ruleModel.getTpdFixedAmount()!=null) {
			coverDetailsRuleBO.setDeathAndTpdApplied(Boolean.TRUE);
			}
			else {
				coverDetailsRuleBO.setDeathAndTpdApplied(Boolean.FALSE);
			}
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);	

			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			dcYearlyCost=coverDetailsRuleBO.getYearlyCost();
			log.info("coverDetailsRuleBO.getYearlyCost()?>> {}",coverDetailsRuleBO.getYearlyCost());
			dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, dcYearlyCost, ruleModel);
		}
		if(dcCost != null){
		quoteResponse.setCost(new BigDecimal(dcCost).setScale(2, BigDecimal.ROUND_HALF_UP));
		quoteResponse.setCoverAmount(ruleModel.getDeathFixedAmount());
		}
	}catch(Exception e){
		log.error("Error in calculate death fixed cost : {}",e);
		throw e;
	}
	log.info("Calculate death fixed cost finish");
	return dcCost;
}

//Added by 561132 for Getting INGD Life Stage Death cover Amount Value
	public static void calculateDeathLifeStageCoverAmount(String fundCode,String age, Map ruleInfoMap, String filePath,
			QuoteResponse quoteResponse) {
		CoverDetailsRuleBO coverDetailsRuleBO = null;
		String ruleNameAsPerFund;
		try {
			coverDetailsRuleBO = new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(fundCode);
			coverDetailsRuleBO.setAge(Integer.valueOf(age));
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			ruleNameAsPerFund = QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT;
			if (ruleInfoMap != null && ruleInfoMap.get(ruleNameAsPerFund) != null) {
				coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(ruleNameAsPerFund));
			}
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO, filePath);
			if (coverDetailsRuleBO != null) {
				quoteResponse.setCoverAmount(BigDecimal.valueOf(coverDetailsRuleBO.getCoverAmount()));
			}
		} catch (Exception e) {
			log.error("Error in calculate death cover amount for INGD : {}", e);
			throw e;
		}
	}

	//Added by 561132 for Getting INGD Life Stage TPD cover Amount Value
		public static void calculateTPDLifeStageCoverAmount(String fundCode,String age, Map ruleInfoMap, String filePath,
				QuoteResponse quoteResponse) {
			CoverDetailsRuleBO coverDetailsRuleBO = null;
			String ruleNameAsPerFund;
			try {
				coverDetailsRuleBO = new CoverDetailsRuleBO();
				coverDetailsRuleBO.setFundCode(fundCode);
				coverDetailsRuleBO.setAge(Integer.valueOf(age));
				coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
				ruleNameAsPerFund = QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT;
				if (ruleInfoMap != null && ruleInfoMap.get(ruleNameAsPerFund) != null) {
					coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(ruleNameAsPerFund));
				}
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO, filePath);
				if (coverDetailsRuleBO != null) {
					quoteResponse.setCoverAmount(BigDecimal.valueOf(coverDetailsRuleBO.getCoverAmount()));
				}
			} catch (Exception e) {
				log.error("Error in calculate tpd cover amount for INGD : {}", e);
				throw e;
			}
		}
		
		//Added by 561132 for INGD Aura Type
		
		public static boolean getShortAuraStatus(RuleModel ruleModel) {
			boolean shortAura=false;
			
			if(ruleModel.getAge()<55 && (ruleModel.getDeathFixedAmount().compareTo(BigDecimal.valueOf(750000))<=0 || ruleModel.getTpdFixedAmount().compareTo(BigDecimal.valueOf(750000))<=0)) 
			{
				shortAura=true;
			}
			if(ruleModel.getAge()<64 && ruleModel.getAge()>54 &&(ruleModel.getDeathFixedAmount().compareTo(BigDecimal.valueOf(500000))<=0 || ruleModel.getTpdFixedAmount().compareTo(BigDecimal.valueOf(500000))<=0)) 
			{
				shortAura=true;
			}
			if(ruleModel.getAge()<64 && (ruleModel.getIpFixedAmount().compareTo(BigDecimal.valueOf(10000))<=0) && ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)) {
				shortAura=true;
			}
			return shortAura;
			
		}
		
		//Added by 561132 for INGD Aura Type
		
				public static boolean getLongAuraStatus(RuleModel ruleModel) {
					boolean longAura=false;
					
					if((ruleModel.getAge()>63 && ruleModel.getDeathFixedAmount().compareTo(BigDecimal.ZERO)!=0) || (ruleModel.getAge()>64 && ruleModel.getTpdFixedAmount().compareTo(BigDecimal.ZERO)!=0)) 
					{
						longAura=true;
					}
					if(ruleModel.getAge()<55 &&(ruleModel.getDeathFixedAmount().compareTo(BigDecimal.valueOf(750000))>0 || ruleModel.getTpdFixedAmount().compareTo(BigDecimal.valueOf(750000))>0)) 
					{
						longAura=true;
					}
					if(ruleModel.getAge()>54 && ruleModel.getAge()<64 && ruleModel.getDeathFixedAmount().compareTo(BigDecimal.valueOf(500000))>0 ) 
					{
						longAura=true;
					}
					if(ruleModel.getAge()>54 && ruleModel.getAge()<65 && ruleModel.getTpdFixedAmount().compareTo(BigDecimal.valueOf(500000))>0 ) 
					{
						longAura=true;
					}
					if(ruleModel.getAge()<64 && ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_67)) {
						longAura=true;
					}
					
					if(ruleModel.getAge()<64 && (ruleModel.getIpFixedAmount().compareTo(BigDecimal.valueOf(10000))>0 && ruleModel.getIpFixedAmount().compareTo(BigDecimal.valueOf(30000))<=0 )&& ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)) {
						longAura=true;
					}
					return longAura;
					
				}
	
}
