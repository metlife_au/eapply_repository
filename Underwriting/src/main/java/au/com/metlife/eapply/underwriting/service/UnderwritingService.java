package au.com.metlife.eapply.underwriting.service;

import au.com.metlife.eapply.underwriting.model.AuraDecision;
import au.com.metlife.eapply.underwriting.model.AuraQuestion;
import au.com.metlife.eapply.underwriting.model.AuraQuestionnaire;
import au.com.metlife.eapply.underwriting.model.AuraResponse;
import au.com.metlife.eapply.underwriting.model.Quote;

public interface UnderwritingService {
	Quote getQuote(Quote input);
	
	AuraQuestionnaire getAllAuraQuestions(String inputXml);	
	AuraQuestion updateAuraQuestion(AuraResponse res);
	AuraDecision submitAuraSession(String clientname, String auraSessionId);
}
