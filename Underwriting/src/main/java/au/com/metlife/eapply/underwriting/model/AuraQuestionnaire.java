package au.com.metlife.eapply.underwriting.model;

import java.io.Serializable;
import java.util.List;

public class AuraQuestionnaire implements Serializable{
	private String sessionID;
	private List<AuraQuestion> questions;	
	private List<AuraSection> sections;	
	public List<AuraSection> getSections() {
		return sections;
	}
	public void setSections(List<AuraSection> sections) {
		this.sections = sections;
	}
	public List<AuraQuestion> getQuestions() {
		return questions;
	}
	public void setQuestions(List<AuraQuestion> questions) {
		this.questions = questions;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

}
