/**
 * 
 */
package pages.metlife;

import java.util.Properties;
import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.EServiceLoginPageObjects;
import uimap.metlife.EViewLoginPageObjects;


/**
 * @author sampath
 * 
 */
public class EServiceLoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public EServiceLoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public EServiceLoginPage LoginToeService() {
		enterLoginDetails();
		return new EServiceLoginPage(scriptHelper);
	} 
	
	private void enterLoginDetails() {
		
		String strUserID = dataTable.getData("MetLife_Data", "UserId");
		String strLoginPassword = dataTable.getData("MetLife_Data", "Password");
		
		try{
			tryAction(waitForClickableElement(EServiceLoginPageObjects.txtUserID),"SET","Login User Name is ",strUserID);
			driver.findElement((EServiceLoginPageObjects.txtPassword)).sendKeys(strLoginPassword);
			report.updateTestLog("Password", "Password Entry", Status.PASS);
			tryAction(waitForClickableElement(EServiceLoginPageObjects.btnLogin),"Click","Login");
			sleep();
		//	switchToMainFrame();
			}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
