package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTCCGetQuotePageObjects extends PageObjectLibrary{
		
	public static final By selIndustry = getObjectIdentifaction("stft.cc.getquote.Industry.dropdown.css");
	public static final By selOccupation = getObjectIdentifaction("stft.cc.getquote.Occupation.dropdown.css");
	
	public static final By txtLifeCoverAmt = getObjectIdentifaction("stft.cc.getquote.coveramount.text.id");	
	public static final By txtTotalCoverAmt = getObjectIdentifaction("stft.cc.getquote.txtTotalCoverAmt.text.id");	
	public static final By btnCalculateQuote = getObjectIdentifaction("stft.cc.getquote.calculatequote.button.css");
	public static final By  btnApply = getObjectIdentifaction("stft.cc.getquote.apply.button.css");
}
