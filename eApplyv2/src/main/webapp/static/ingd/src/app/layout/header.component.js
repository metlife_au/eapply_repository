class AppHeaderCtrl {
  constructor(AppConstants, $window, $rootScope, $scope, $state, ngDialog, EapplyData) {
    'ngInject';
    
    this.appName = AppConstants.appName;
    this._$window = $window;
    this._$rootScope = $rootScope;
    this._$scope = $scope;
    this._$state = $state;
    this._ngDialog = ngDialog;
    this._EapplyData = EapplyData;
    this.screen = $state.current.name.replace('app.', '');
    this._$rootScope.$on('$stateChangeStart', (event, newState) => {
      this.screen = newState.name.replace('app.', '');
    });
    
  }

  changeState(path) {
    this._$state.go(path);
  }

  home() {
    const that = this;
    this._ngDialog.openConfirm({
      template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row"><div class="col-12"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer"><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button></div></div>',
      plain: true,
      showClose: false,
      className: 'ngdialog-theme-plain custom-width'
    }).then(
      () => {
        that._EapplyData.setCancelData(null);
        that.changeState('app.landing');
      }, 
      (event) => {
        if(event === 'oncancel') {
          return false;
        }
      }
    );
  }

  logout() {
    this._$window.close();
  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  controllerAs: '$ctrl',
  templateUrl: 'layout/header.html'
};

export default AppHeader;
