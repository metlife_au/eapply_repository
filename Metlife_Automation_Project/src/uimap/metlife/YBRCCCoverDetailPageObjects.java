package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRCCCoverDetailPageObjects extends PageObjectLibrary{
		
	public static final By txtareainput = getObjectIdentifaction("ybrcc.coverdetails.textarea.css");
	public static final By btnNext = getObjectIdentifaction("ybrcc.coverdetails.next.btn.css");

}
