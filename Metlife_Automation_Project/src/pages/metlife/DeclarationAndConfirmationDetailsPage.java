/**
 * 
 */
package pages.metlife;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import com.cognizant.framework.Status;
import supportlibraries.ScriptHelper;
import uimap.metlife.DeclarationAndConfirmationPageObjects;

/**
 * @author sampath
 * 
 */
public class DeclarationAndConfirmationDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public DeclarationAndConfirmationDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public DeclarationAndConfirmationDetailsPage enterDeclarationAndConfirmationDetails() {
		fillInDeclarationAndConfirmationDetails();
		return new DeclarationAndConfirmationDetailsPage(scriptHelper);
	}
	
	public DeclarationAndConfirmationDetailsPage validateConclusionMessage() {
		validateApplicationMessage();
		return new DeclarationAndConfirmationDetailsPage(scriptHelper);
	}

	private void fillInDeclarationAndConfirmationDetails() {

		
		String strContactMethod = dataTable.getData("MetLife_Data", "personal.ContactMethod");
		String strContactTime = dataTable.getData("MetLife_Data", "personal.ContactTime");
		String strContactDetail = dataTable.getData("MetLife_Data", "personal.ContactDetails");
		String strContactMailId = dataTable.getData("MetLife_Data", "personal.emailAddress");
		String strTCName = dataTable.getData("MetLife_Data", "TC_ID");
		String strDeathExclu = dataTable.getData("MetLife_Data", "death_exclusion");
		String strTPDExclu = dataTable.getData("MetLife_Data", "tpd_exclusion");
		String strIPExclu = dataTable.getData("MetLife_Data", "ip_exclusion");
		String strDeathLoad = dataTable.getData("MetLife_Data", "death_loading");
		String strTPDLoad = dataTable.getData("MetLife_Data", "tpd_loading");
		String strIPLoad = dataTable.getData("MetLife_Data", "ip_loading");
		String strConclusionMsg = dataTable.getData("MetLife_Data", "applicationMessage");
		try{
			if(isElementPresent(DeclarationAndConfirmationPageObjects.chkExclusionsOfDeclaration))
			{
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkExclusionsOfDeclaration),"Click","I acknowledge the applied exclusion(s) applied to my cover.");
				storeSessionThreadData("dyn_death_exclusion",getAttribute(driver.findElement(DeclarationAndConfirmationPageObjects.hidDeathExclu), "value"));
				storeSessionThreadData("dyn_tpd_exclusion",getAttribute(driver.findElement(DeclarationAndConfirmationPageObjects.hidTPDExclu), "value"));
				storeSessionThreadData("dyn_ip_exclusion",getAttribute(driver.findElement(DeclarationAndConfirmationPageObjects.hidIPExclu), "value"));
				assertCSVStrings(strDeathExclu,getStringValueFromSessionThreadData("dyn_death_exclusion"));
				assertCSVStrings(strTPDExclu,getStringValueFromSessionThreadData("dyn_tpd_exclusion"));
				assertCSVStrings(strIPExclu,getStringValueFromSessionThreadData("dyn_ip_exclusion"));
			}
			if(isElementPresent(DeclarationAndConfirmationPageObjects.chkLoadingsOfDeclaration))
			{
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkLoadingsOfDeclaration),"Click","I acknowledge the loading(s) applied to my premium.");
				storeSessionThreadData("dyn_death_loading",getAttribute(driver.findElement(DeclarationAndConfirmationPageObjects.hidDeathLoad), "value"));
				storeSessionThreadData("dyn_tpd_loading",getAttribute(driver.findElement(DeclarationAndConfirmationPageObjects.hidTPDLoad), "value"));
				storeSessionThreadData("dyn_ip_loading",getAttribute(driver.findElement(DeclarationAndConfirmationPageObjects.hidIPLoad), "value"));
				System.out.println("##########################################################################################################################################");
				System.out.println(getStringValueFromSessionThreadData("dyn_death_loading"));
				System.out.println(getStringValueFromSessionThreadData("dyn_tpd_loading"));
				System.out.println(getStringValueFromSessionThreadData("dyn_ip_loading"));
				System.out.println("##########################################################################################################################################");
				assertEqualText(strDeathLoad,getStringValueFromSessionThreadData("dyn_death_loading"));
				assertEqualText(strTPDLoad,getStringValueFromSessionThreadData("dyn_tpd_loading"));
				assertEqualText(strIPLoad,getStringValueFromSessionThreadData("dyn_ip_loading"));
			}
			if(!strTCName.contains("VICS")&&!strTCName.contains("INGD")){
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkDutyOfDeclaration),"Click","You must click and read the Duty of Disclosure before proceeding");
			sleep();
			}
			if(!strTCName.contains("PWC")&&!strTCName.contains("INGD")){
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkPrivacyStatement),"Click","You must click and read the Privacy Statement before proceeding");
				sleep();
			}
			if(strTCName.contains("INGD")){
				if(strTCName.contains("Transfer")){
					tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkEvidence),"Click","Evidence");
					}
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkIngdGeneralConsent),"Click","By ticking this box I acknowledge and confirm");
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.lstPreferredContactMethod),"DropDownSelect","Preferred Contact Method",strContactMethod);
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.lstPreferredContactTime),"DropDownSelect","Preferred Contact Time",strContactTime);
				Thread.sleep(1000);
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.txtDetails),"SET","Contact Detail",strContactDetail);
				Thread.sleep(1000);
			}
			else{
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.chkGeneralConsent),"Click","You must click and read the General Consent before proceeding");
			}
			if(!(strTCName.contains("PWC")||strTCName.contains("PSUP")||strTCName.contains("INGD"))){
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.lstPreferredContactMethod),"DropDownSelect","Preferred Contact Method",strContactMethod);
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.lstPreferredContactTime),"DropDownSelect","Preferred Contact Time",strContactTime);
				Thread.sleep(1000);
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.txtDetails),"SET","Contact Detail",strContactDetail);
				Thread.sleep(1000);
				tryAction(fluentWaitElements(DeclarationAndConfirmationPageObjects.txtEmailAddress),"SET","Contact Email Id",strContactMailId);
				Thread.sleep(1000);
			}
			
			if(strTCName.contains("PWC")){
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.btnPwcContinue),"MoveToElm","Continue");
				sleep();
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.btnPwcContinue),"Click","Continue");
			}else{
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.btnContinue),"MoveToElm","Continue");
				sleep();
				tryAction(waitForClickableElement(DeclarationAndConfirmationPageObjects.btnContinue),"Click","Continue");
				if(strTCName.contains("INGD")&& strTCName.contains("Transfer")) {
					String strConclusionMsg1 = dataTable.getData("MetLife_Data", "applicationMessage");
					System.out.println("Entered into 2nd Loop");
					List<WebElement> StrDec=fluentWaitListElements(DeclarationAndConfirmationPageObjects.txtDesMsg);
					String StrDecMsg=StrDec.get(0).getText();
					System.out.println(StrDecMsg);
					if(StrDecMsg.contains(strConclusionMsg1)){
						report.updateTestLog("Decision Validation", "Valid Message is displayed", Status.PASS );
					}
				}else {
				assertContainsText(fluentWaitElements(DeclarationAndConfirmationPageObjects.finalSuccessMsg),strConclusionMsg);
				      }
		}
		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}

	private void validateApplicationMessage(){
		
		String strConclusionMsg = dataTable.getData("MetLife_Data", "applicationMessage");
		String strTCName= dataTable.getData("MetLife_Data", "TC_ID");
		
		try{
			
			if(strTCName.contains("OAMPS")||strTCName.contains("ALI")||strTCName.contains("YBRIP")){	
				
			//	String message = "table > tbody > tr > td.titleCont > h1.title";
				String message = "h1.title";
				assertContainsText(fluentWaitElement(By.cssSelector(message)), strConclusionMsg);
			}
			else {
				
				assertContainsText(fluentWaitElements(DeclarationAndConfirmationPageObjects.finalSuccessMsg),strConclusionMsg);
			}
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action in validateApplicationMessage " + e.getMessage(), Status.FAIL);
		}
		
	}


}
