package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSSummaryPageObjects extends PageObjectLibrary{
	public static final By btnsummaryNext = getObjectIdentifaction("OAMPS.summary.next.btn.css");
	public static final By chkexclusion = getObjectIdentifaction("OAMPS.summary.exclusion.chkbox.css");	
	public static final By chkloading = getObjectIdentifaction("OAMPS.summary.loading.chkbox.css");	
}
