/**
 * 
 */
package pages.metlife.Angular.Statewide;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

/**
 * @author Hemnath
 * 
 */
public class STATEWIDE_Angular_UpdateYourOccupation extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public STATEWIDE_Angular_UpdateYourOccupation(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public STATEWIDE_Angular_UpdateYourOccupation UpdateYourOccupation_STATEWIDE() {
		updateoccupation();
		HealthandLifestyle();
		Declaration();
		return new STATEWIDE_Angular_UpdateYourOccupation(scriptHelper);
	} 
	
	private void updateoccupation() {
		
		String EmailId = dataTable.getData("STATEWIDE_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("STATEWIDE_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("STATEWIDE_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("STATEWIDE_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("STATEWIDE_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("STATEWIDE_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("STATEWIDE_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("STATEWIDE_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("STATEWIDE_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("STATEWIDE_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("STATEWIDE_UpdateOccupation", "TertiaryQual");
		String HazardousEnv = dataTable.getData("STATEWIDE_UpdateOccupation", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("STATEWIDE_UpdateOccupation", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("STATEWIDE_UpdateOccupation", "AnnualSalary");
		
		
		try{
			
			//*****Click on Update Occupation Link*******\\
			sleep(1500);
			WebDriverWait waiting=new WebDriverWait(driver,35);		
			waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Update your occupation ')]"))).click();
			report.updateTestLog("Update your Occupation", "Update occupation link is Selected", Status.PASS);
			
		
			 sleep(1500);
			
			 WebDriverWait wait=new WebDriverWait(driver,18);
			//*****Agree to Duty of disclosure*******\\
				
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dodLabel']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "Duty of Disclosure label is checked", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacyLabel']/span"))).click();
			report.updateTestLog("Privacy Statement", "Privacy Statement label is checked", Status.PASS);

			//*****Enter the Email Id*****\\										
			

			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);
										
			//*****Click on the Contact Number Type****\\			
			
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);					
										
			//****Enter the Contact Number****\\	
			
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("wrkRatingPreferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
										
										
										
			//***Select the Preferred time of Contact*****\\	
			
			if(TimeofContact.equalsIgnoreCase("Morning")){			
				
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[1]/label/span")).click();
				
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}			
			else{			
				driver.findElement(By.xpath("//*[@id='collapseOne']/form/div/div[4]/div[2]/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
			}		
									
										
			//****Contact Details-Continue Button*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingContactForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);							
										
										
			//***************************************\\							
			//**********OCCUPATION SECTION***********\\							
			//****************************************\\							
										
			//*****Select the 15 Hours Question*******\\		
			
			if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
						
				}		
			else{		
						
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-6 pl0xs']/label/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
						
				}	
			
			/*not present in statewide*/
			//*****Resident of Australia****\\	
		/*	
			if(Citizen.equalsIgnoreCase("Yes")){		
				driver.findElement(By.xpath("//*[@id='occupFormId']/div/div[1]/div[2]/div/div[1]/label/span")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
					
				}	
			else{	
				driver.findElement(By.xpath("//*[@id='occupFormId']/div/div[1]/div[2]/div/div[2]/label/span")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
					
				}	
					*/			
										
			//*****Industry********\\		
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("workRatingIndustry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(800);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='What is your occupation?']"))).click();
			sleep(500);
			
			//*****Occupation*****\\		
			
			Select Occupation=new Select(driver.findElement(By.name("workRatingoccupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			sleep(500);
					
					
			//*****Other Occupation *****\\	
			
			if(OccupationType.equalsIgnoreCase("Other")){		
					
				wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingotherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1000);	
			}									
										
				

			//*****Extra Questions*****\\							
			
		    if(driver.getPageSource().contains("Do you work wholly within an office environment?")){							
		    
									
		//***Select if your office Duties are Undertaken within an Office Environment?\\	
		    	
		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
		   
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[1]/label/span"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
									
		}							
		else{							

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
									
		}							
									
		      //*****Do You Hold a Tertiary Qualification******\\							
									
		        if(TertiaryQual.equalsIgnoreCase("Yes")){							
		       
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
		}							
		   else{							
		       
			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
		}							
		}							
									
									
									
		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
									
									
									
		//******Do you work in hazardous environment*****\\		
			
		if(HazardousEnv.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[4]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
		}							
									
		//******Do you spend more than 10% of your working time outside of an office environment?*******\\	
		
		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[1]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
		}							
		else{							
		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='occupFormId']/div/div[5]/div/div[2]/div/div[2]/label/span"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
		}							
		}							
								    
          sleep(800);
		   
        //*****What is your annual Salary******							
			
			
		    wait.until(ExpectedConditions.elementToBeClickable(By.name("workRatingAnnualSal"))).sendKeys(AnnualSalary);
		    sleep(500);		
		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
									
		
		
		//*****Click on Continue*******\\
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='workRatingFormSubmit(workRatingOccupForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		sleep(2500);

		
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	

		private void HealthandLifestyle() {

			String Restrictedillness = dataTable.getData("STATEWIDE_UpdateOccupation", "Restrictedillness");
			String PriorClaim = dataTable.getData("STATEWIDE_UpdateOccupation", "PriorClaim");
			String ReducedLifeExpectancy = dataTable.getData("STATEWIDE_UpdateOccupation", "ReducedLifeExpectancy");
			
			try{			


      if(driver.getPageSource().contains("Eligibility Check")){

	//*******Are you restricted due to illness or injury******\\
	
	WebDriverWait wait=new WebDriverWait(driver,25);
         
	 if(Restrictedillness.equalsIgnoreCase("Yes")){
		
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
   }
    
	 else{
		 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[1]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
    }

     //******Are you contemplating, or have you ever made a claim for sickness, accident or disability benefits, Workers� Compensation or any other form of compensation due to illness or injury?***\\

     if(PriorClaim.equalsIgnoreCase("Yes")){
    	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[2]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
     }
  
     else{
    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[2]"))).click();
    	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
    	sleep(1000);
        }


//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\

    if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){
	 
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 pl0 col-xs-12 ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-Yes", Status.PASS);
	sleep(1000);
    }
   else{
	   
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='col-sm-3 col-xs-12 pl0xs ng-scope']/label/span)[3]"))).click();
	report.updateTestLog("Health and Lifestyle Page", "Restricted due to illness-No", Status.PASS);
	sleep(1000);
}

          //****Click on Continue******\\

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue button in Health and Lifestyle section", Status.PASS);
    sleep(4000);
      }

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

		}
	
		
		
	/////////////////*************Confirmation PAge*********************////////
		
		private void Declaration() {
			
			try{

				
				
								//*****General Consent check box******\\

				WebDriverWait wait=new WebDriverWait(driver,18);
   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabelWR']/span"))).click();
   report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);

   										//******Submit******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitWorkRating()']"))).click();
report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

Thread.sleep(6000);	

//*******Feedback Pop-up******\\
Thread.sleep(6000);	
if(driver.getPageSource().contains("We value your feedback")){		
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
sleep(8000); 
}
sleep(1000);
String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);
sleep(800);
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		
		}
			}
	}