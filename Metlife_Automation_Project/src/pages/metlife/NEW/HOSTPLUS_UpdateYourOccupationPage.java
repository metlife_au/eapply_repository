/**
 * 
 */
package pages.metlife.NEW;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.HOSTPLUS_UpdateYourOccupationPageObjects;

/**
 * @author Hemnath
 * 
 */
public class HOSTPLUS_UpdateYourOccupationPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public HOSTPLUS_UpdateYourOccupationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public HOSTPLUS_UpdateYourOccupationPage enterHOSTPLUSupdateoccupation() {
		updateoccupation();
		return new HOSTPLUS_UpdateYourOccupationPage(scriptHelper);
	} 
	
	private void updateoccupation() {
		
		String EmailId = dataTable.getData("HOSTPLUS_UpdateOccupation", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_UpdateOccupation", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_UpdateOccupation", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_UpdateOccupation", "TimeofContact");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_UpdateOccupation", "FifteenHoursWork");
		String Citizen = dataTable.getData("HOSTPLUS_UpdateOccupation", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_UpdateOccupation", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_UpdateOccupation", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_UpdateOccupation", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_UpdateOccupation", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_UpdateOccupation", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_UpdateOccupation", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_UpdateOccupation", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_UpdateOccupation", "AnnualSalary");
		String Restrictedillness = dataTable.getData("HOSTPLUS_UpdateOccupation", "Restrictedillness");
		String PriorClaim = dataTable.getData("HOSTPLUS_UpdateOccupation", "PriorClaim");
		String ReducedLifeExpectancy = dataTable.getData("HOSTPLUS_UpdateOccupation", "ReducedLifeExpectancy");
		
		try{
			
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.UpdateOccupationLink), "Click", "Update Your Occupation");
			
			

			 sleep(1500);
				
				
				if(driver.getPageSource().contains("Cancel Application")){
					quickSwitchWindows();
					sleep(1000);
					driver.findElement(By.xpath("(//span[contains(text(),'Cancel Application')])[1]")).click();
					report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
					sleep(1000);
			
				}
			
			
			//*****Agree to Duty of disclosure*******\\
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Duty of Disclosure", "selected the Checkbox", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			report.updateTestLog("Privacy Statement", "selected the Checkbox", Status.PASS);

			//*****Enter the Email Id*****\\										
			
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.Email), "SET", "Email Id",EmailId);							
										
										
			//*****Click on the Contact Number Type****\\							
			if(!TypeofContact.equalsIgnoreCase("Mobile")){							
										
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ContactTypeClick), "Click", "Preferred Type of Contact: "+TypeofContact);							
										
			//*****Choose the Type of Contact*****\							
										
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();							
										
			}							
										
			//****Enter the Contact Number****\\							
			waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));							
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);							
										
										
										
			//***Select the Preferred time of Contact*****\\							
			if(TimeofContact.equalsIgnoreCase("Morning")){							
										
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.MorningContact), "Click", "Preferred Contact Time is Morning");							
			}							
			else{							
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.AfterNoonContact), "Click", "Preferred Contact Time is AfterNoon");							
			}							
										
			//****Contact Details-Continue Button*****\\							
			tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ContactDetailsContinue), "Click", "Continue After entering Contact Details");							
			sleep(1200);							
										
										
			//***************************************\\							
			//**********OCCUPATION SECTION***********\\							
			//****************************************\\							
										
				//*****Select the 15 Hours Question*******\\						
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){						
										
				tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.FifteenHoursYes), "Click", "15 Hours a Week-Yes");						
										
				}						
				else{						
										
				tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");						
										
				}						
										
				//*****Select if Resident of Australia****\\						
				if(Citizen.equalsIgnoreCase("Yes")){						
										
					tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.CitizenYes), "Click", "Australian Citizen-Yes");					
										
					}					
				else{						
					tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.CitizenNo), "Click", "Australian Citizen-No");					
										
					}					
										
				//*****Click on Industry********\\						
										
				tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ClickIndustry), "Click", "Industry");						
				sleep(800);						
										
				//*****Choose the Industry Type*****\\						
				driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();						
				sleep(800);						
										
				//*****Click On  Occupation*****\\						
										
		        tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ClickOccupation), "Click", "Occupation");								
		        sleep(800);								
		        								
		        //****Choose the Occupation Type*****\\								
										
				driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();						
				sleep(1000);						
										
										
				//*****Other Occupation *****\\						
				if(OccupationType.equalsIgnoreCase("Other")){						
										
					tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OtherOccupation), "SET", "Other Occupation",OtherOccupation);					
					sleep(1000);					
				}						
										
				
               //*****Extra Questions*****\\						
				
    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){						
   						
				
//***Select if your office Duties are Undertaken within an Office Environment?\\							
    if(OfficeEnvironment.equalsIgnoreCase("Yes")){						
     tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");					
				
}						
else{						
				
       tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");					
				
}						
				
      //*****Do You Hold a Tertiary Qualification******\\							
				
        if(TertiaryQual.equalsIgnoreCase("Yes")){						
       tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");					
}						
   else{						
       tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");					
}						
}							
					
				
				
else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
				
				
				
//******Do you work in hazardous environment*****\\							
if(HazardousEnv.equalsIgnoreCase("Yes")){						
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.HazardousEncYes), "Click", "Hazardous Environment-Yes");					
}						
else{						
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.HazardousEncNo), "Click", "Hazardous Environment-No");					
}						
				
//******Do you spend 20% time working outside office*******\\							
if(OutsideOfcPercent.equalsIgnoreCase("Yes")){						
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OutsideOfcPercentYes), "Click", "Work 20% Outside Office-Yes");					
}						
else{						
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OutsideOfcPercentNo), "Click", "Work 20% Outside Office-No");					
}						
}							
		
	
    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){						
   						
				
//***Select if your office Duties are Undertaken within an Office Environment?\\							
    if(OfficeEnvironment.equalsIgnoreCase("Yes")){						
     tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OfficeEnvironmentYes), "Click", "Work within Office Environment-Yes");					
				
}						
else{						
				
       tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.OfficeEnvironmentNo), "Click", "Work within Office Environment-No");					
				
}						
				
      //*****Do You Hold a Tertiary Qualification******\\							
				
        if(TertiaryQual.equalsIgnoreCase("Yes")){						
       tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.TertiaryQualYes), "Click", "Tertiary Qualification-Yes");					
}						
   else{						
       tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.TertiaryQualNo), "Click", "Tertiary Qualification-No");					
}						
}		
				
//*****What is your annual Salary******							
				
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);							
sleep(500);							
				
//***Load the Salary Amount****\\							
waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.LoadSalaryLabel).click();							
sleep(500);							
				
				
//*****Continue button after entering occupation details*******\\								
				
				
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ContinueOccupationDetails), "Click", "Continue After entering Occupation Details");							
sleep(4500);	



	




//******************************Health and Lifestyle section*****************************\\

//*******Are you restricted due to illness or injury, from carrying out any of the identifiable duties of your current and normal occupation on a full time basis
//(even if you are not currently working on a full time basis). Full time basis is considered to be at least 
//35 hours per week.

if(driver.getPageSource().contains("Eligibility Check")){
if(Restrictedillness.equalsIgnoreCase("Yes")){
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.RestrictedillnessYes), "Click", "Restrictedillness-Yes");
}
else{
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.RestrictedillnessNo), "Click", "Restrictedillness-No");	
}

//******Are you contemplating, or have you ever made a claim for sickness, accident or disability benefits, Workers� Compensation or any other form of compensation due to illness or injury?***\\

if(PriorClaim.equalsIgnoreCase("Yes")){
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.PriorClaimYes), "Click", "PriorClaim-Yes");
}
else{
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.PriorClaimNo), "Click", "PriorClaim-No");	
}


//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\

if(ReducedLifeExpectancy.equalsIgnoreCase("Yes")){
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ReducedLifeExpectancyYes), "Click", "ReducedLifeExpectancy-Yes");
}
else{
tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ReducedLifeExpectancyNo), "Click", "ReducedLifeExpectancy-No");	
}

//****Click on Continue******\\

tryAction(waitForClickableElement(HOSTPLUS_UpdateYourOccupationPageObjects.ContinueAftHealth), "Click", "Continue After entering Health and Lifestyle Details");	
}

//**************DECLARATION PAGE*****************\\

//*****General Consent******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='gc_chckbox_id__xc_c']/span"))).click();
report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);

//******Submit******\\

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='submitId']/span"))).click();
report.updateTestLog("Submit", "Clicked on Submit Button", Status.PASS);

//*******Feedback Pop-up******\\

	
wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
sleep(1000);

String Appstatus=driver.findElement(By.xpath("//h4[@class='panel-title']")).getText();
report.updateTestLog("Application status", "Application status is: "+Appstatus, Status.PASS);

			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
