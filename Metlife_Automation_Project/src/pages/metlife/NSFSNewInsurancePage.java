package pages.metlife;


import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.FundInputStringPageObjects;
import uimap.metlife.WebserviceInputStringPageObjects;

public class NSFSNewInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public NSFSNewInsurancePage enterINGDPayload() {
		fillAndCalculateNSFSQuote();
		return new NSFSNewInsurancePage(scriptHelper);
	}

	public NSFSNewInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		//sleep(1000);
		//switchToActiveWindow();
	}

	public void fillAndCalculateNSFSQuote() {
		String inputPayload = null;
		inputPayload = createInputPayload();
		System.out.println(inputPayload);
		try{
		tryAction(fluentWaitElement(WebserviceInputStringPageObjects.txtInputString),"SET", "Input Payload",inputPayload);
		tryAction(fluentWaitElement(WebserviceInputStringPageObjects.btnPlainText),"Click", "ProceedToNextPage");
		tryAction(fluentWaitElements(FundInputStringPageObjects.btnNFSFNewApplication),"Click", "New Application");
		sleep(5000);
		quickSwitchWindows();

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		/*String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String coveramt = dataTable.getData("MetLife_Data", "CoverAmount");
		String flybuysmemeber = dataTable.getData("MetLife_Data", "FlyBuysMemeber");
		String joinflybuys = dataTable.getData("MetLife_Data", "JoinFlyBuys");
		String phoneNumber = dataTable.getData("MetLife_Data", "PreferredNumber");
		String strState = dataTable.getData("MetLife_Data", "State");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCoverAmt = dataTable.getData("MetLife_Data", "CoverAmount");
		String strPaymentFreq = dataTable.getData("MetLife_Data", "PaymentFrequency");
		System.out.println("The Premium Date is :: " + getPremiumDOB(-3));
		try {
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.btnNewApplication), "Click", "New Application");
			switchToActiveWindow();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			Thread.sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "SET", "Phone Number", phoneNumber);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "TAB", "Phone Number");
			Thread.sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtDOB), "SET", "Date of birth", RandomDateOfBirth());
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFemale), "clkradio", "Gender -" + gender);
			}
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsYes), "Click",
						"Have you Smoked in 12 months - Yes");

			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsNo), "Click",
						"Have you Smoked in 12 months - No");
			}
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.btnState), "specialDropdown", "State", strState);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selCoverAmt), "specialDropdown", "Cover Amount",
					strCoverAmt);

			Thread.sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selFrequency), "specialDropdown", "Payment Frequency",
					strPaymentFreq);
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysYes), "Click",
					"Are you a flybuys member - Yes");
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysNo), "Click",
					"Are you a flybuys member - No");
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysJoinNo), "Click",
					"Would you like to join flybuys - No");
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ColesGetQuotePageObjects.lnkCalculateQuoteId)).click().perform();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.lnkApply), "Click", "Apply");

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}*/
	}

	public String createInputPayload()
	{
		String adminPartnerID=dataTable.getData("MetLife_Data", "adminPartnerID");
		String transRefGUID=dataTable.getData("MetLife_Data", "transRefGUID");
		String transType=dataTable.getData("MetLife_Data", "transType");
		String transDate=dataTable.getData("MetLife_Data", "transDate");
		String transTime=dataTable.getData("MetLife_Data", "transTime");
		String fundID=dataTable.getData("MetLife_Data", "fundID");
		String fundEmail=dataTable.getData("MetLife_Data", "fundEmail");
		String lineOfBusiness=dataTable.getData("MetLife_Data", "lineOfBusiness");
		String partnerID=dataTable.getData("MetLife_Data", "partnerID");
		String applicant2_dateJoined=dataTable.getData("MetLife_Data", "applicant2_dateJoined");
		String applicant2_memberType=dataTable.getData("MetLife_Data", "applicant2_memberType");
		String applicant2_applicantRole=dataTable.getData("MetLife_Data", "applicant2_applicantRole");
		String applicant2_clientRefNumber=dataTable.getData("MetLife_Data", "applicant2_clientRefNumber");
		String applicant2_personalDetails2_firstName=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_firstName");
		String applicant2_personalDetails2_lastName=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_lastName");
		String applicant2_personalDetails2_dateOfBirth=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_dateOfBirth");
		String applicant2_personalDetails2_gender=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_gender");
		String applicant2_personalDetails2_applicantSubType=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_applicantSubType");
		String applicant2_personalDetails2_smoker=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_smoker");
		String applicant2_personalDetails2_title=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_title");
		String applicant2_personalDetails2_priority=dataTable.getData("MetLife_Data", "applicant2_personalDetails2_priority");
		String applicant2_cover1_occRating=dataTable.getData("MetLife_Data", "applicant2_cover1_occRating");
		String applicant2_cover1_benefitType=dataTable.getData("MetLife_Data", "applicant2_cover1_benefitType");
		String applicant2_cover1_type=dataTable.getData("MetLife_Data", "applicant2_cover1_type");
		String applicant2_cover1_amount=dataTable.getData("MetLife_Data", "applicant2_cover1_amount");
		String applicant2_cover1_loading=dataTable.getData("MetLife_Data", "applicant2_cover1_loading");
		String applicant2_cover1_exclusions=dataTable.getData("MetLife_Data", "applicant2_cover1_exclusions");
		String applicant2_cover2_occRating=dataTable.getData("MetLife_Data", "applicant2_cover2_occRating");
		String applicant2_cover2_benefitType=dataTable.getData("MetLife_Data", "applicant2_cover2_benefitType");
		String applicant2_cover2_type=dataTable.getData("MetLife_Data", "applicant2_cover2_type");
		String applicant2_cover2_amount=dataTable.getData("MetLife_Data", "applicant2_cover2_amount");
		String applicant2_cover2_loading=dataTable.getData("MetLife_Data", "applicant2_cover2_loading");
		String applicant2_cover2_exclusions=dataTable.getData("MetLife_Data", "applicant2_cover2_exclusions");
		String applicant2_cover3_occRating=dataTable.getData("MetLife_Data", "applicant2_cover3_occRating");
		String applicant2_cover3_benefitType=dataTable.getData("MetLife_Data", "applicant2_cover3_benefitType");
		String applicant2_cover3_type=dataTable.getData("MetLife_Data", "applicant2_cover3_type");
		String applicant2_cover3_amount=dataTable.getData("MetLife_Data", "applicant2_cover3_amount");
		String applicant2_cover3_loading=dataTable.getData("MetLife_Data", "applicant2_cover3_loading");
		String applicant2_cover3_exclusions=dataTable.getData("MetLife_Data", "applicant2_cover3_exclusions");
		String applicant2_cover3_waitingPeriod=dataTable.getData("MetLife_Data", "applicant2_cover3_waitingPeriod");
		String applicant2_cover3_benefitPeriod=dataTable.getData("MetLife_Data", "applicant2_cover3_benefitPeriod");
		String applicant2_address_addressType=dataTable.getData("MetLife_Data", "applicant2_address_addressType");
		String applicant2_address_line1=dataTable.getData("MetLife_Data", "applicant2_address_line1");
		String applicant2_address_line2=dataTable.getData("MetLife_Data", "applicant2_address_line2");
		String applicant2_address_suburb=dataTable.getData("MetLife_Data", "applicant2_address_suburb");
		String applicant2_address_state=dataTable.getData("MetLife_Data", "applicant2_address_state");
		String applicant2_address_postCode=dataTable.getData("MetLife_Data", "applicant2_address_postCode");
		String applicant2_address_country=dataTable.getData("MetLife_Data", "applicant2_address_country");
		String applicant2_contact_emailAddress=dataTable.getData("MetLife_Data", "applicant2_contact_emailAddress");
		String applicant2_contact_mobilePhone=dataTable.getData("MetLife_Data", "applicant2_contact_mobilePhone");
		String applicant2_contact_homePhone=dataTable.getData("MetLife_Data", "applicant2_contact_homePhone");
		String applicant2_contact_workPhone=dataTable.getData("MetLife_Data", "applicant2_contact_workPhone");
		String applicant2_contact_prefContact=dataTable.getData("MetLife_Data", "applicant2_contact_prefContact");
		String applicant2_contact_prefContactTime=dataTable.getData("MetLife_Data", "applicant2_contact_prefContactTime");

		
		StringBuilder inputStringPayload = null;
		inputStringPayload = new StringBuilder();
		inputStringPayload.append("<request>");
		inputStringPayload.append("<adminPartnerID>");
		inputStringPayload.append(adminPartnerID);
		inputStringPayload.append("</adminPartnerID>");
		inputStringPayload.append("<transRefGUID>");
		inputStringPayload.append(transRefGUID);
		inputStringPayload.append("</transRefGUID>");
		inputStringPayload.append("<transType>");
		inputStringPayload.append(transType);
		inputStringPayload.append("</transType>");
		inputStringPayload.append("<transDate>");
		inputStringPayload.append(transDate);
		inputStringPayload.append("</transDate>");
		inputStringPayload.append("<transTime>");
		inputStringPayload.append(transTime);
		inputStringPayload.append("</transTime>");
		inputStringPayload.append("<fundID>");
		inputStringPayload.append(fundID);
		inputStringPayload.append("</fundID>");
		inputStringPayload.append("<fundEmail>");
		inputStringPayload.append(fundEmail);
		inputStringPayload.append("</fundEmail>");
		inputStringPayload.append("<policy>");
		inputStringPayload.append("<lineOfBusiness>");
		inputStringPayload.append(lineOfBusiness);
		inputStringPayload.append("</lineOfBusiness>");
		inputStringPayload.append("<partnerID>");
		inputStringPayload.append(partnerID);
		inputStringPayload.append("</partnerID>");
		inputStringPayload.append("<applicant>");
		inputStringPayload.append("<dateJoined>");
		inputStringPayload.append(applicant2_dateJoined);
		inputStringPayload.append("</dateJoined>");
		inputStringPayload.append("<clientRefNumber>");
		inputStringPayload.append(getRandomAlphaNumeric(8));
		inputStringPayload.append("</clientRefNumber>");
		inputStringPayload.append("<applicantRole>");
		inputStringPayload.append(applicant2_applicantRole);
		inputStringPayload.append("</applicantRole>");
		inputStringPayload.append("<memberType>");
		inputStringPayload.append(applicant2_memberType);
		inputStringPayload.append("</memberType>");
		inputStringPayload.append("<personalDetails>");
		inputStringPayload.append("<firstName>");
		inputStringPayload.append(applicant2_personalDetails2_firstName);
		inputStringPayload.append(getMonthNameMMM());
		inputStringPayload.append(getRandomText(7));
		inputStringPayload.append(getWeekDayNameEEE());
		inputStringPayload.append("</firstName>");
		inputStringPayload.append("<lastName>");
		inputStringPayload.append(applicant2_personalDetails2_lastName);
		inputStringPayload.append(getMonthNameMMM());
		inputStringPayload.append(getRandomText(7));
		inputStringPayload.append(getWeekDayNameEEE());
		inputStringPayload.append("</lastName>");
		inputStringPayload.append("<dateOfBirth>");
		inputStringPayload.append(getPremiumDOB(Integer.parseInt("-"+applicant2_personalDetails2_dateOfBirth)));
		inputStringPayload.append("</dateOfBirth>");
		inputStringPayload.append("<gender>");
		inputStringPayload.append(applicant2_personalDetails2_gender);
		inputStringPayload.append("</gender>");
		inputStringPayload.append("<applicantSubType/>");
		inputStringPayload.append("<smoker>");
		inputStringPayload.append(applicant2_personalDetails2_smoker);
		inputStringPayload.append("</smoker>");
		inputStringPayload.append("<title>");
		inputStringPayload.append(applicant2_personalDetails2_title);
		inputStringPayload.append("</title>");
		inputStringPayload.append("<priority>");
		inputStringPayload.append(applicant2_personalDetails2_priority);
		inputStringPayload.append("</priority>");
		inputStringPayload.append("</personalDetails>");
		inputStringPayload.append("<existingCovers>");
		inputStringPayload.append("<cover>");
		inputStringPayload.append("<occRating>");
		inputStringPayload.append(applicant2_cover1_occRating);
		inputStringPayload.append("</occRating>");
		inputStringPayload.append("<benefitType>");
		inputStringPayload.append(applicant2_cover1_benefitType);
		inputStringPayload.append("</benefitType>");
		inputStringPayload.append("<type>");
		inputStringPayload.append(applicant2_cover1_type);
		inputStringPayload.append("</type>");
		inputStringPayload.append("<amount>");
		inputStringPayload.append(applicant2_cover1_amount);
		inputStringPayload.append("</amount>");
		inputStringPayload.append("<loading>");
		inputStringPayload.append(applicant2_cover1_loading);
		inputStringPayload.append("</loading>");
		inputStringPayload.append("<exclusions>");
		inputStringPayload.append(applicant2_cover1_exclusions);
		inputStringPayload.append("</exclusions>");
		inputStringPayload.append("</cover>");
		inputStringPayload.append("<cover>");
		inputStringPayload.append("<occRating>");
		inputStringPayload.append(applicant2_cover2_occRating);
		inputStringPayload.append("</occRating>");
		inputStringPayload.append("<benefitType>");
		inputStringPayload.append(applicant2_cover2_benefitType);
		inputStringPayload.append("</benefitType>");
		inputStringPayload.append("<type>");
		inputStringPayload.append(applicant2_cover2_type);
		inputStringPayload.append("</type>");
		inputStringPayload.append("<amount>");
		inputStringPayload.append(applicant2_cover2_amount);
		inputStringPayload.append("</amount>");
		inputStringPayload.append("<loading>");
		inputStringPayload.append(applicant2_cover2_loading);
		inputStringPayload.append("</loading>");
		inputStringPayload.append("<exclusions>");
		inputStringPayload.append(applicant2_cover2_exclusions);
		inputStringPayload.append("</exclusions>");
		inputStringPayload.append("</cover>");
		inputStringPayload.append("<cover>");
		inputStringPayload.append("<occRating>");
		inputStringPayload.append(applicant2_cover3_occRating);
		inputStringPayload.append("</occRating>");
		inputStringPayload.append("<benefitType>");
		inputStringPayload.append(applicant2_cover3_benefitType);
		inputStringPayload.append("</benefitType>");
		inputStringPayload.append("<type>");
		inputStringPayload.append(applicant2_cover3_type);
		inputStringPayload.append("</type>");
		inputStringPayload.append("<amount>");
		inputStringPayload.append(applicant2_cover3_amount);
		inputStringPayload.append("</amount>");
		inputStringPayload.append("<loading>");
		inputStringPayload.append(applicant2_cover3_loading);
		inputStringPayload.append("</loading>");
		inputStringPayload.append("<exclusions>");
		inputStringPayload.append(applicant2_cover3_exclusions);
		inputStringPayload.append("</exclusions>");
		inputStringPayload.append("<waitingPeriod>");
		inputStringPayload.append(applicant2_cover3_waitingPeriod);
		inputStringPayload.append("</waitingPeriod>");
		inputStringPayload.append("<benefitPeriod>");
		inputStringPayload.append(applicant2_cover3_benefitPeriod);
		inputStringPayload.append("</benefitPeriod>");
		inputStringPayload.append("</cover>");
		inputStringPayload.append("</existingCovers>");
		inputStringPayload.append("<address>");
		inputStringPayload.append("<addressType>");
		inputStringPayload.append(applicant2_address_addressType);
		inputStringPayload.append("</addressType>");
		inputStringPayload.append("<line1>");
		inputStringPayload.append(applicant2_address_line1);
		inputStringPayload.append("</line1>");
		inputStringPayload.append("<line2/>");
		inputStringPayload.append("<suburb>");
		inputStringPayload.append(applicant2_address_suburb);
		inputStringPayload.append("</suburb>");
		inputStringPayload.append("<state>");
		inputStringPayload.append(applicant2_address_state);
		inputStringPayload.append("</state>");
		inputStringPayload.append("<postCode>");
		inputStringPayload.append(applicant2_address_postCode);
		inputStringPayload.append("</postCode>");
		inputStringPayload.append("<country>");
		inputStringPayload.append(applicant2_address_country);
		inputStringPayload.append("</country>");
		inputStringPayload.append("</address>");
		inputStringPayload.append("<contactDetails>");
		inputStringPayload.append("<emailAddress>");
		inputStringPayload.append(applicant2_contact_emailAddress);
		inputStringPayload.append("</emailAddress>");
		inputStringPayload.append("<mobilePhone>");
		inputStringPayload.append(applicant2_contact_mobilePhone);
		inputStringPayload.append("</mobilePhone>");
		inputStringPayload.append("<homePhone>");
		inputStringPayload.append(applicant2_contact_homePhone);
		inputStringPayload.append("</homePhone>");
		inputStringPayload.append("<workPhone>");
		inputStringPayload.append(applicant2_contact_workPhone);
		inputStringPayload.append("</workPhone>");
		inputStringPayload.append("<prefContact>");
		inputStringPayload.append(applicant2_contact_prefContact);
		inputStringPayload.append("</prefContact>");
		inputStringPayload.append("<prefContactTime>");
		inputStringPayload.append(applicant2_contact_prefContactTime);
		inputStringPayload.append("</prefContactTime>");
		inputStringPayload.append("</contactDetails>");
		inputStringPayload.append("</applicant>");
		inputStringPayload.append("</policy>");
		inputStringPayload.append("</request>");
		return inputStringPayload.toString();
	}
}
