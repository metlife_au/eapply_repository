package au.com.metlife.model;

import java.io.Serializable;
import java.util.Date;

public class ClaimOutput  implements Serializable{
	
	private String fundname;
	
	private String fundownerId;
	private String policyNo;
	private String claimId ;
	private String claimNo;
	
	private String productType;
	private String firstName;
	private String surName ;
	private Date dob;
	
	private String gender;
	private String reference;
	private String scheme ;
	
	private String waitperiod;
	private String benefitperiod;
	private String statusCode ;
	private String statusDescription;
	private Date datesubmitted;
	private Date disabilitydate ;
	
	private String claimTypeDescription;
	private String suminsured;
	private String totalBenPaid;
	private Date dateLastPaid;
	private String admittedDate;
	private Double salaryInsured;
	private Double superContribution;
	private Double monthlyBenefit;

	private String additionalInformation;
	private String mailComment;
	private Date startdate;
	private String description;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdditionalInformation() {
		return additionalInformation;
	}
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	public String getMailComment() {
		return mailComment;
	}
	public void setMailComment(String mailComment) {
		this.mailComment = mailComment;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public String getAdmittedDate() {
		return admittedDate;
	}
	public void setAdmittedDate(String admittedDate) {
		this.admittedDate = admittedDate;
	}
	public Double getSalaryInsured() {
		return salaryInsured;
	}
	public void setSalaryInsured(Double salaryInsured) {
		this.salaryInsured = salaryInsured;
	}
	public Double getSuperContribution() {
		return superContribution;
	}
	public void setSuperContribution(Double superContribution) {
		this.superContribution = superContribution;
	}
	public Double getMonthlyBenefit() {
		return monthlyBenefit;
	}
	public void setMonthlyBenefit(Double monthlyBenefit) {
		this.monthlyBenefit = monthlyBenefit;
	}
	private Date lastupdateDate;
	
	private String claimCauseDescription;
	private String claimAccessor;
	
	public String getClaimCauseDescription() {
		return claimCauseDescription;
	}
	public void setClaimCauseDescription(String claimCauseDescription) {
		this.claimCauseDescription = claimCauseDescription;
	}
	public String getClaimAccessor() {
		return claimAccessor;
	}
	public void setClaimAccessor(String claimAccessor) {
		this.claimAccessor = claimAccessor;
	}
	public String getFundname() {
		return fundname;
	}
	public void setFundname(String fundname) {
		this.fundname = fundname;
	}
	public String getFundownerId() {
		return fundownerId;
	}
	public void setFundownerId(String fundownerId) {
		this.fundownerId = fundownerId;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public String getClaimNo() {
		return claimNo;
	}
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getScheme() {
		return scheme;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public String getWaitperiod() {
		return waitperiod;
	}
	public void setWaitperiod(String waitperiod) {
		this.waitperiod = waitperiod;
	}
	public String getBenefitperiod() {
		return benefitperiod;
	}
	public void setBenefitperiod(String benefitperiod) {
		this.benefitperiod = benefitperiod;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public Date getDatesubmitted() {
		return datesubmitted;
	}
	public void setDatesubmitted(Date datesubmitted) {
		this.datesubmitted = datesubmitted;
	}
	public Date getDisabilitydate() {
		return disabilitydate;
	}
	public void setDisabilitydate(Date disabilitydate) {
		this.disabilitydate = disabilitydate;
	}
	public String getClaimTypeDescription() {
		return claimTypeDescription;
	}
	public void setClaimTypeDescription(String claimTypeDescription) {
		this.claimTypeDescription = claimTypeDescription;
	}
	public String getSuminsured() {
		return suminsured;
	}
	public void setSuminsured(String suminsured) {
		this.suminsured = suminsured;
	}
	public String getTotalBenPaid() {
		return totalBenPaid;
	}
	public void setTotalBenPaid(String totalBenPaid) {
		this.totalBenPaid = totalBenPaid;
	}
	public Date getDateLastPaid() {
		return dateLastPaid;
	}
	public void setDateLastPaid(Date dateLastPaid) {
		this.dateLastPaid = dateLastPaid;
	}
	
	public Date getLastupdateDate() {
		return lastupdateDate;
	}
	public void setLastupdateDate(Date lastupdateDate) {
		this.lastupdateDate = lastupdateDate;
	}
	


	
	

}
