package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FUNDINFO database table.
 * 
 */
@Entity
@Table(name = "FUNDINFO", schema="EAPPDB")
public class Fundinfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FUNDINFO_SEQ")
	@SequenceGenerator(name="FUNDINFO_SEQ",sequenceName = "EAPPDB.FUNDINFO_SEQ", initialValue=1, allocationSize=100)
	private long id;

	@Column(nullable=false, length=47)
	private long eapplicationid;

	@Column(length=50)
	private String fixedratingcategory;

	@Column(nullable=false, length=30)
	private String fundid;

	@Column(length=50)
	private String fundname;

	@Column(length=50)
	private String ipratingcategory;

	@Column(length=50)
	private String unitratingcategory;

	public Fundinfo() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getEapplicationid() {
		return this.eapplicationid;
	}

	public void setEapplicationid(long eapplicationid) {
		this.eapplicationid = eapplicationid;
	}

	public String getFixedratingcategory() {
		return this.fixedratingcategory;
	}

	public void setFixedratingcategory(String fixedratingcategory) {
		this.fixedratingcategory = fixedratingcategory;
	}

	public String getFundid() {
		return this.fundid;
	}

	public void setFundid(String fundid) {
		this.fundid = fundid;
	}

	public String getFundname() {
		return this.fundname;
	}

	public void setFundname(String fundname) {
		this.fundname = fundname;
	}

	public String getIpratingcategory() {
		return this.ipratingcategory;
	}

	public void setIpratingcategory(String ipratingcategory) {
		this.ipratingcategory = ipratingcategory;
	}

	public String getUnitratingcategory() {
		return this.unitratingcategory;
	}

	public void setUnitratingcategory(String unitratingcategory) {
		this.unitratingcategory = unitratingcategory;
	}

}