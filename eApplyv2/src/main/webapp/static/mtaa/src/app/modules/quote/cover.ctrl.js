(function(angular) {
    'use strict';
    /* global channel, inputData, token, moment */
    /**
     * @ngdoc controller
     * @name MtaaApp.controller:changeCoverCtrl
     *
     * @description
     * This is changeCoverCtrl description
     */
    angular
       .module('MtaaApp')
       .controller('coverCtrl', coverCtrl);
    coverCtrl.$inject = ['$scope','$rootScope', '$location', '$http', '$stateParams','$window', 'ngDialog', 'fetchUrlSvc', 'fetchPersoanlDetailSvc', 'fetchQuoteSvc', 'fetchOccupationSvc', 'fetchDeathCoverSvc', 'fetchTpdCoverSvc', 'fetchIpCoverSvc', 'MaxLimitService', 'NewOccupationService', 'CalculateService', '$q', 'appData', 'fetchAppNumberSvc', 'saveEapplyData', 'printPageSvc', 'DownloadPDFSvc', 'auraRespSvc', 'propertyService', '$filter','mtaaUIConstants','$timeout'];
        function coverCtrl($scope,$rootScope, $location, $http, $stateParams , $window, ngDialog, fetchUrlSvc, fetchPersoanlDetailSvc, fetchQuoteSvc, fetchOccupationSvc, fetchDeathCoverSvc, fetchTpdCoverSvc, fetchIpCoverSvc, MaxLimitService, NewOccupationService, CalculateService, $q, appData, fetchAppNumberSvc, saveEapplyData, printPageSvc, DownloadPDFSvc, auraRespSvc, propertyService, $filter,mtaaUIConstants,$timeout) {
             var vm = this;
             vm.ipCoverMax = false;
           vm.QPD = {
                    requestType: 'CCOVER',
                partnerCode: 'MTAA',
                lastSavedOn: 'Quotepage',
                auraDisabled: true,
                clientRefNumber:null,
                totalMonthlyPremium:null,
                applicatioNumber:null,
                ackCheck:false,
                privacyCheck:false,
                dodCheck:false,
                fulCheck:false,
                appDecision:'ACC',
                clientMatchReason:null,
                clientMatch:false,
                deathExclusions:null,
                tpdExclusions:null,
                ipExclusions:'',
                applicant: {
                    firstName:null,
                    lastName:null,
                    birthDate:null,
                    emailId:null,
                    contactType:null,
                    contactNumber:null,
                    smoker:null,
                    title:null,
                    age:null,
                    memberType:null,
                    gender:null,
                    customerReferenceNumber:null,
                    clientRefNumber:null,
                    australiaCitizenship:null,
                    dateJoinedFund:'',
                    workHours:null,
                    industryType:null,
                    occupationCode:null,
                    occupation:null,
                    spendTimeOutside:null,
                    annualSalary:null,
                    occupationDuties:null,
                    otherOccupation:'',
                    workDuties:null,
                    ownBusinessQue:null,
                    ownBusinessYesQue:null,
                    ownBusinessNoQue:null,
                    tertiaryQue:null,
                    previousTpdBenefit:'',
                    cover:[{
                           benefitType:'1',
                           coverDecision:null,
                           coverReason:null,
                           existingCoverAmount:null,
                           fixedUnit:null,
                           transferCoverAmount:null,
                           occupationRating:null, 
                           cost:null,
                           coverCategory:null,
                           additionalCoverAmount:null,
                           additionalUnit:'',
                           frequencyCostType:null
                    },{
                           benefitType:'2',
                           coverDecision:null,
                           coverReason:null,
                           existingCoverAmount:null,
                           fixedUnit:null,
                           transferCoverAmount:null,
                           occupationRating:null,
                           cost:null, 
                           coverCategory:null,
                           tpdInputTextValue:'',
                           additionalCoverAmount:null,
                           frequencyCostType:null
                    },{
                           benefitType:'4',
                           coverDecision:null,
                           coverReason:null,
                           existingCoverAmount:null,
                           transferCoverAmount:null ,
                           ipInputTextValue:null,
                           optionalUnit:null,
                           existingIpBenefitPeriod:null,
                           existingIpWaitingPeriod:null,
                           fixedUnit:null,
                           occupationRating:null,
                           cost:null, 
                           coverCategory:'IpUnitised',
                           additionalCoverAmount:null,
                           additionalUnit:null,
                           additionalIpBenefitPeriod:null,
                           additionalIpWaitingPeriod:null,
                           totalIpWaitingPeriod:null,
                           totalIpBenefitPeriod:null,
                           frequencyCostType:null
                    }],
                    contactDetails:{
                    preferedContactTime:null,
                    preferedContacType:null,
                    mobilePhone: null,
                    workPhone: null,
                    homePhone: null,
                    preferedContactNumber:null,
                    country:null,
                    addressLine1:null,
                    addressLine2:null,
                    postCode:null,
                    state:null,
                    suburb:null
                    }
            }
        };
           vm.showhide=function(idshow,idhide) {
                    if(document.getElementById(idshow)){
                           document.getElementById(idshow).style.display = 'block';
                    }
                    if(document.getElementById(idhide)){
                           document.getElementById(idhide).style.display = 'none';
                    }
                    
             };
           vm.constants = {
                           image: mtaaUIConstants.path.images,
                           phoneNumber: mtaaUIConstants.onboardDetails.phoneNumber,
                           emailFormat: mtaaUIConstants.onboardDetails.emailFormat,
                           postCodeFormat: mtaaUIConstants.onboardDetails.postCodeFormat,
                           waitingPeriodOptions: mtaaUIConstants.onboardDetails.waitingPeriodOptions,
                           benefitPeriodOptions: mtaaUIConstants.onboardDetails.benefitPeriodOptions,
                           contactTypeOption: mtaaUIConstants.onboardDetails.contactTypeOption,
                           titleOption: mtaaUIConstants.onboardDetails.titleOption,
                           addressTypeOption: mtaaUIConstants.onboardDetails.addressTypeOption,
                          stateOptions: mtaaUIConstants.onboardDetails.stateOptions
                    };
            $('#collapseOne').collapse('hide');
                     $('#collapseprivacy').collapse('hide');
                     $('#collapseTwo').collapse('hide');
                     $('#collapseThree').collapse('hide');
            vm.modelOptions = {
                updateOn: 'blur'
            };
            vm.phoneNumbr = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
            vm.emailFormat = /^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z]+\.[a-zA-Z.]{2,}$/;
            vm.regex = /[0-9]{1,3}/;
            vm.deathCvrRegex = /^\d{1,8}(,\d{1,8}){0,9}?$/;
            vm.isDCCoverRequiredDisabled = false;
            vm.isDCCoverTypeDisabled = false;
            vm.isTPDCoverRequiredDisabled = false;
            vm.isTPDCoverTypeDisabled = false;

            vm.isWaitingPeriodDisabled = false;
            vm.isBenefitPeriodDisabled = false;
            vm.isIPCoverRequiredDisabled = false;

            vm.isIpSalaryCheckboxDisabled = false;
            vm.ipWarningFlag = false;
            vm.ackFlag = false;
            vm.modalShown = false;
            vm.invalidSalAmount = false;
            vm.dcIncreaseFlag = false;
            vm.tpdIncreaseFlag = false;
            vm.ipIncreaseFlag = false;
            
            vm.disclaimerFlag = true;
            vm.IndustryOptions = null;
            vm.OccupationList = null;

            vm.showWithinOfficeQuestion = false;
            vm.showTertiaryQuestion = false;
            vm.showHazardousQuestion = false;
            vm.showOutsideOfficeQuestion = false;
            vm.urlList = fetchUrlSvc.getUrlList();
            
            vm.ipWarning = false;
            vm.QPD.applicant.insuredSalary = false;
            vm.eligibleFrStIp = false;

            vm.isDeathDisabled = false;
            vm.isTPDDisabled = false;
            vm.isIPDisabled = false;
            vm.prevOtherOcc = null;

            /*Error Flags*/
            vm.dodFlagErr = null;
            vm.privacyFlagErr = null;
            vm.deathErrorFlag = false;
            vm.tpdErrorFlag = false;
            vm.ipErrorFlag = false;
            vm.eligibleFrStIpFlag = false;

            vm.premiumFrequencyOptions = ['Monthly', 'Yearly', 'Weekly'];
            vm.contactTypeOptions = [{
                text: 'Home',
                value: '2'
            }, {
                text: 'Work',
                value: '3'
            }, {
                text: 'Mobile',
                value: '1'
            }];

            vm.FormOneFields = ['contactEmail', 'contactPhone', 'contactPrefTime', 'gender'];
            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'annualSalary'];
            vm.OccupationOtherFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'otherOccupation', 'annualSalary'];
            vm.CoverCalculatorFormFields = ['coverName', 'coverType', 'requireCover', 'tpdCoverName', 'tpdCoverType', 'TPDRequireCover'];

            vm.personalDetails = {};
            vm.contactDetails = {};
            vm.isEmpty = function(value) {
                return ((value === '' || value === null) || value === '0');
            };
            vm.deathUnitsForFixed = 0;
            vm.tpdUnitsForFixed = 0;
            vm.deathUnitsForFixedOld = 0;
            vm.tpdUnitsForFixedOld = 0;
            vm.init = function() {
            	vm.ipCoverMax=true;
                var defer = $q.defer();
                vm.deathCoverDetails = fetchDeathCoverSvc.getDeathCover();
                vm.tpdCoverDetails = fetchTpdCoverSvc.getTpdCover();
                vm.ipCoverDetails = fetchIpCoverSvc.getIpCover();

                vm.QPD.applicant.cover[0].occupationRating = vm.deathCoverDetails.occRating || 'General';
                vm.QPD.applicant.cover[1].occupationRating = vm.tpdCoverDetails.occRating || 'General';
                vm.QPD.applicant.cover[2].occupationRating = vm.ipCoverDetails.occRating || 'General';

                vm.waitingPeriodOptions = ['30 Days', '60 Days', '90 Days'];
                vm.benefitPeriodOptions = ['2 Years', '5 Years','Age 65']; 

                vm.QPD.applicant.cover[2].existingIpWaitingPeriod = vm.waitingPeriodOptions.indexOf(vm.ipCoverDetails.waitingPeriod) > -1 ? vm.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
                vm.QPD.applicant.cover[2].existingIpBenefitPeriod= vm.benefitPeriodOptions.indexOf(vm.ipCoverDetails.benefitPeriod) > -1 ? vm.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';

                
                if (vm.deathCoverDetails.type === '1') {
                    vm.QPD.applicant.cover[0].coverCategory = 'DcUnitised';
                    vm.QPD.applicant.cover[0].additionalUnit = vm.deathCoverDetails.units || 0;
                    vm.QPD.applicant.cover[0].additionalCoverAmount = parseInt(vm.deathCoverDetails.amount) || 0;
                    vm.QPD.applicant.cover[0].existingCoverAmount = parseInt(vm.deathCoverDetails.amount) || 0;
                    vm.QPD.applicant.cover[0].fixedUnit = vm.deathCoverDetails.units || 0;
                    $timeout(function() {
                        vm.showhide('nodollar1', 'dollar1');
                        vm.showhide('nodollar', 'dollar');
                      });

                } else if (vm.deathCoverDetails.type === '2') {
                    vm.QPD.applicant.cover[0].coverCategory = 'DcFixed';
                    vm.QPD.applicant.cover[0].additionalUnit = vm.deathCoverDetails.units || 0;
                    vm.QPD.applicant.cover[0].additionalCoverAmount = Math.ceil(parseInt(vm.deathCoverDetails.amount)/1000)*1000 || 0;
                    vm.QPD.applicant.cover[0].existingCoverAmount = parseInt(vm.deathCoverDetails.amount) || 0;
                    vm.isDCCoverTypeDisabled = false;
                    $timeout(function() {
                        vm.showhide('dollar1', 'nodollar1');
                        vm.showhide('dollar', 'nodollar');
                    });

                }
                
                //if (vm.ipCoverDetails.type === '1') {
                    vm.QPD.applicant.cover[2].additionalUnit = vm.ipCoverDetails.units;
                    $timeout(function() {
                        vm.showhide('nodollar1', 'dollar1');
                        vm.showhide('nodollar', 'dollar');
                        });

                //}
                
                if (vm.tpdCoverDetails.type === '1') {
                    vm.QPD.applicant.cover[1].coverCategory  = 'TPDUnitised';
                    vm.QPD.applicant.cover[1].additionalUnit = vm.tpdCoverDetails.units || 0;
                    vm.QPD.applicant.cover[1].additionalCoverAmount = parseInt(vm.tpdCoverDetails.amount) || 0;
                    vm.QPD.applicant.cover[1].existingCoverAmount = parseInt(vm.tpdCoverDetails.amount) || 0;
                    vm.QPD.applicant.cover[1].fixedUnit = vm.tpdCoverDetails.units || 0;
                    
                    $timeout(function() {
                        vm.showhide('nodollar1','dollar1');
                        vm.showhide('nodollar','dollar');
                      });
                    

                } else if (vm.tpdCoverDetails.type === '2') {
                    vm.QPD.applicant.cover[1].coverCategory = 'TPDFixed';
                    vm.QPD.applicant.cover[1].additionalUnit = vm.tpdCoverDetails.units || 0;
                    vm.QPD.applicant.cover[1].additionalCoverAmount = Math.ceil(parseInt(vm.tpdCoverDetails.amount)/1000)*1000 || 0;
                    vm.QPD.applicant.cover[1].existingCoverAmount = parseInt(vm.tpdCoverDetails.amount) || 0;
                    vm.isTPDCoverTypeDisabled = false;
                    
                    $timeout(function() {
                      vm.showhide('dollar1','nodollar1');
                        vm.showhide('dollar','nodollar');
                    });
                    
                }

                vm.QPD.applicant.cover[2].existingCoverAmount = parseInt(vm.ipCoverDetails.amount) || 0;
                vm.QPD.applicant.cover[2].fixedUnit = vm.ipCoverDetails.units;
                vm.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

                vm.personalDetails = vm.inputDetails.personalDetails || {};
                vm.contactDetails = vm.inputDetails.contactDetails || {};

                fetchQuoteSvc.getList(vm.urlList.quoteUrl, 'MTAA').then(function(res) {
                    vm.IndustryOptions = res.data;
                }, function(err) {
                    console.info('Error while fetching industry list ' + JSON.stringify(err));
                });

                vm.QPD.applicant.age = parseInt(moment().diff(moment(vm.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

                if (vm.deathCoverDetails && vm.deathCoverDetails.benefitType && vm.deathCoverDetails.benefitType === '1') {
                    if (vm.QPD.applicant.age <= 15|| vm.QPD.applicant.age > 70) {
                        $('#deathsection').removeClass('active');
                        $('#death').css('display', 'none');
                        vm.isDeathDisabled = true;
                        vm.QPD.applicant.cover[0].additionalUnit = 0;
                    }
                }
                if (vm.tpdCoverDetails && vm.tpdCoverDetails.benefitType && vm.tpdCoverDetails.benefitType === '2') {
                    if (vm.QPD.applicant.age <= 15 || vm.QPD.applicant.age > 65) {
                        $('#tpdsection').removeClass('active');
                        $('#tpd').css('display', 'none');
                        vm.isTPDDisabled = true;
                        vm.QPD.applicant.cover[1].additionalCoverAmount = 0;
                    }
                }
                if (vm.ipCoverDetails && vm.ipCoverDetails.benefitType && vm.ipCoverDetails.benefitType === '4') {
                    if (vm.QPD.applicant.age < 16 || vm.QPD.applicant.age > 65) {
                        $('#ipsection').removeClass('active');
                        $('#sc').css('display', 'none');
                        vm.isIPDisabled = true;
                        vm.QPD.applicant.cover[2].additionalUnit = 0;
                    }
                }
                /*if(vm.QPD.applicant.age>65){
                	console.log('greater than 65 years');
                	vm.isTPDDisabled = true;
                	vm.isIPDisabled = true;
                }*/
                MaxLimitService.getMaxLimits(vm.urlList.maxLimitUrl, 'MTAA', vm.inputDetails.memberType, 'CCOVER').then(function(res) {
                    var limits = res.data;
                    vm.annualSalForUpgradeVal = limits[0].annualSalForUpgradeVal;
                    vm.DCMaxAmount = limits[0].deathMaxAmount;
                    vm.TPDMaxAmount = limits[0].tpdMaxAmount;
                    vm.IPMaxAmount = limits[0].ipMaxAmount;
                    defer.resolve(res);
                }, function(error) {
                    console.info('Something went wrong while fetching limits ' + error);
                    defer.reject(error);
                });
                return defer.promise;
            };
            vm.init().then(function() {
            	vm.QPD.applicant.cover[2].additionalIpWaitingPeriod = vm.ipCoverDetails.waitingPeriod;
                vm.QPD.applicant.cover[2].additionalIpBenefitPeriod = vm.ipCoverDetails.benefitPeriod;
                angular.extend(vm.QPD.applicant, appData.getAppData().applicant);
                vm.QPD.dodCheck=appData.getAppData().dodCheck;
                vm.QPD.privacyCheck=appData.getAppData().privacyCheck;
                vm.QPD.clientRefNumber = appData.getAppData().clientRefNumber;
                vm.QPD.applicatioNumber = appData.getAppData().applicatioNumber;
                vm.QPD.applicant.birthDate = moment(vm.QPD.applicant.birthDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
                if (vm.QPD.applicant.workHours === 'No') {
                    vm.disableIpCover();
                }
                if (vm.QPD.applicant.industryType) {
                    fetchOccupationSvc.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.QPD.applicant.industryType).then(function(res) {
                        vm.OccupationList = res.data;
                        vm.getCategoryFromDB();
                    }, function(err) {
                        console.info('Error while fetching occupations ' + JSON.stringify(err));
                    });
                }
                vm.QPD.applicant.insuredSalary = (vm.QPD.applicant.insuredSalary === 'true' || vm.QPD.applicant.insuredSalary === true) ? true : false;
                
                	var radios = $('label[radio-sync]');
                    var data = $('input[data-sync]');
                    data.filter('[data-sync=\'\' + val + \'\']').attr('checked', 'checked');
                    if (vm.QPD.applicant.cover[0].coverCategory==='DcFixed') {
                        vm.deathErrorFlag = false;
                        vm.QPD.deathErrorMsg = '';
                        vm.tpdErrorFlag = false;
                        vm.QPD.tpdErrorMsg = '';
                        vm.showhide('dollar1', 'nodollar1');
                        vm.showhide('dollar', 'nodollar');
                        vm.QPD.applicant.cover[0].coverCategory  = 'DcFixed';
                        vm.QPD.applicant.cover[1].coverCategory  = 'TPDFixed';
                    } else if (vm.QPD.applicant.cover[0].coverCategory==='DcUnitised') {
                        vm.deathErrorFlag = false;
                        vm.QPD.deathErrorMsg = '';
                        vm.tpdErrorFlag = false;
                        vm.QPD.tpdErrorMsg = '';
                        vm.showhide('nodollar1', 'dollar1');
                        vm.showhide('nodollar', 'dollar');
                        vm.QPD.applicant.cover[0].coverCategory  = 'DcUnitised';
                        vm.QPD.applicant.cover[1].coverCategory  = 'TPDUnitised';
                    }
                    radios.removeClass('active');
                    radios.filter('[radio-sync=\'\' + val + \'\']').addClass('active');
                	
                
                if (vm.QPD.applicant.insuredSalary) {
                    vm.insureNinetyPercentIp();
                    $('#ipsalarycheck').parent().addClass('active');
                    $('#ipsalarycheck').attr('checked', 'checked');
                }
                $scope.$watch('vm.formduty.$valid', function(newVal, oldVal) {
                    if(newVal) {
                                  $('#collapseprivacy').collapse('show');
                                  $('#collapseOne').collapse('hide');
                                  $('#collapseTwo').collapse('hide');
                                  $('#collapseThree').collapse('hide');
                           }else{
                                  $('#collapseprivacy').collapse('hide');
                                  $('#collapseOne').collapse('hide');
                                  $('#collapseTwo').collapse('hide');
                                  $('#collapseThree').collapse('hide');
                           } 
                });
                
                $scope.$watch('vm.cancelprivacyPolicyForm.$valid', function(newVal, oldVal) {
                    if(newVal) {
                           $('#collapseOne').collapse('show');
                                  $('#collapseTwo').collapse('hide');
                                  $('#collapseThree').collapse('hide');
                           }else{
                                  $('#collapseOne').collapse('hide');
                                  $('#collapseTwo').collapse('hide');
                                  $('#collapseThree').collapse('hide');
                           } 
                    });

                $scope.$watch('vm.formOne.$valid', function(newVal, oldVal) {
                    if(!newVal) {
                                  $('#collapseTwo').collapse('hide');
                                  $('#collapseThree').collapse('hide');
                           } else {
                                  $('#collapseTwo').collapse('show');
                                  $('#collapseThree').collapse('hide');
                           }
                });

                $scope.$watch('vm.occupationForm.$valid', function(newVal, oldVal) {
                    if(!newVal) {
                                  $('#collapseThree').collapse('hide');
                           } else {
                                  $('#collapseThree').collapse('show');
                           }
                    });
                
                if($stateParams.mode === '2' || $stateParams.mode === '3'){
            		$timeout(function() {
            			$('#collapseOne').collapse('show');
                        $('#collapseprivacy').collapse('show');
                        $('#collapseTwo').collapse('show');
                        $('#collapseThree').collapse('show');
    				}, 2000);
            		if(vm.QPD.applicant.cover[0].frequencyCostType != null){
            			vm.frequencyCostType =  vm.QPD.applicant.cover[0].frequencyCostType;
            		}else{
            			vm.frequencyCostType = 'Monthly';
            		}
            		vm.checkBenefitPeriod();
            		
            	}else{
            		$('#collapseOne').collapse('hide');
                    $('#collapseprivacy').collapse('hide');
                    $('#collapseTwo').collapse('hide');
                    $('#collapseThree').collapse('hide');
                    vm.QPD.auraDisabled = false;
                    vm.validateDeathTpdIpAmounts();
                    vm.frequencyCostType = 'Monthly';
                    vm.QPD.applicant.cover[0].frequencyCostType = 'Monthly';
                    vm.QPD.applicant.cover[1].frequencyCostType = 'Monthly';
                    vm.QPD.applicant.cover[2].frequencyCostType = 'Monthly';
            	}
               
                if (vm.QPD.applicant.contactDetails.preferedContacType === null || vm.QPD.applicant.contactDetails.preferedContacType === undefined) {
                	 vm.QPD.applicant.contactDetails.preferedContactNumber = '';
                }
                //vm.toggleIPCondition();
                
            }, function() {

            });
            vm.checkCoverRequired = function(){
            	vm.ipCoverMax = false;
            	vm.validateDeathTpdIpAmounts();
            };
            vm.changePrefContactTime=function(value){
            vm.QPD.applicant.contactDetails.preferedContactTime=value;
            };
            vm.changeGender=function(value){
            vm.QPD.applicant.gender = value;
            };
            vm.changePrefContactType = function(value) {
                if (vm.QPD.applicant.contactDetails.preferedContacType === '1') {
                    vm.QPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.mobilePhone;
                } else if (vm.QPD.applicant.contactDetails.preferedContacType === '2') {
                    vm.QPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.homePhone;
                } else if (vm.QPD.applicant.contactDetails.preferedContacType === '3') {
                    vm.QPD.applicant.contactDetails.preferedContactNumber = vm.contactDetails.workPhone;
                } else {
                    vm.QPD.applicant.contactDetails.preferedContactNumber = '';
                }
            };
            vm.isCollapsible = function(targetEle, event) {
                var dodLabelCheck = $('#dodLabel').hasClass('active');
                var privacyLabelCheck = $('#privacyLabel').hasClass('active');
                if (targetEle === 'collapseprivacy' && !dodLabelCheck) {
                    if ($('#dodLabel').is(':visible')){
                           vm.dodFlagErr = true;
                    }
                       
                    event.stopPropagation();
                    return false;
                } else if (targetEle === 'collapseOne' && (!dodLabelCheck || !privacyLabelCheck)) {
                    if ($('#privacyLabel').is(':visible')){
                          vm.privacyFlagErr = true;
                    }
                    event.stopPropagation();
                    return false;
                } else if (targetEle === 'collapseTwo' && (!dodLabelCheck || !privacyLabelCheck || $('#collapseOne form').hasClass('ng-invalid'))) {
                    if ($('#collapseOne form').is(':visible')){
                          vm.onFormContinue(vm.formOne);
                    }
                    event.stopPropagation();
                    return false;
                } else if (targetEle === 'collapseThree' && (!dodLabelCheck || !privacyLabelCheck || $('#collapseOne form').hasClass('ng-invalid') || $('#collapseTwo form').hasClass('ng-invalid'))) {
                    if ($('#collapseTwo form').is(':visible')){
                           vm.onFormContinue(vm.occupationForm);
                    }
                    event.stopPropagation();
                    return false;
                }
            };

            vm.toggleOccupation = function(checkFlag) {
                if ((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in'))){
                     return false;
                }
                $('a[data-target=\'#collapseTwo\']').click();  //Can be improved 
            };

            vm.toggleCoverCalc = function(checkFlag) {
                if ((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in'))){
                    return false;
                }
                $('a[data-target=\'#collapseThree\']').click();  //Can be improved 
            };

            vm.togglePrivacy = function(checkFlag) {
                if ((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in'))){
                    return false;
                }
                $('a[data-target=\'#collapseprivacy\']').click();  //Can be improved 
            };

            vm.toggleContact = function(checkFlag) {
                if ((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in'))){
                    return false;
                }
                $('a[data-target=\'#collapseOne\']').click();  //Can be improved 

            };
            vm.checkDodState = function() {
            vm.QPD.dodCheck = !vm.QPD.dodCheck;
            if(vm.QPD.dodCheck){
                    vm.dodFlagErr = false;
            }else{
                    vm.dodFlagErr = true;
            }
            
            };

            vm.checkPrivacyState = function() {
                    vm.QPD.privacyCheck =  !vm.QPD.privacyCheck;
                    if(vm.QPD.privacyCheck){
                           vm.privacyFlagErr = false;
                    }else{
                           vm.privacyFlagErr = true;
                    }
            };
            vm.clickToOpen = function(hhText) {

                var dialog = ngDialog.open({
                    template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Helpful hints</h4><!-- Row starts --><div class=\'row rowcustom\' style=\'margin:0px -35px;\'><div class=\'col-sm-12\'><p class=\'aligncenter\'></p><div id=\'tips_text\'>' + hhText +
                    '</div><p></p></div></div><!-- Row ends --></div><div class=\'row\'><div class=\'col-sm-4\'></div><div class=\'col-sm-4 col-xs-12\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Close</button></div><div class=\'col-sm-4\'></div></div></div>',
                    className: 'ngdialog-theme-plain',
                    plain: true
                });
                dialog.closePromise.then(function(data) {
                    console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
                });
            };

            vm.checkPreviousMandatoryFields = function(elementName, formName) {
                var formFields;
                if (formName === 'formOne') {
                    formFields = vm.FormOneFields;
                } else if (formName === 'occupationForm') {
                    if (vm.QPD.applicant.occupation !== undefined && vm.QPD.applicant.occupation === 'Other') {
                        formFields = vm.OccupationOtherFormFields;
                    } else {
                        formFields = vm.OccupationFormFields;
                    }
                } else if (formName === 'coverCalculatorForm') {
                    formFields = vm.CoverCalculatorFormFields;
                }
                var inx = formFields.indexOf(elementName);
                if (inx > 0) {
                    for (var i = 0; i < inx; i++) {
                        if (vm[formName][formFields[i]]){
                           vm[formName][formFields[i]].$touched = true;
                        }
                           
                    }
                }
            };

            vm.disableIpCover = function() {
                vm.QPD.applicant.cover[2].coverOption = 'No change';
                vm.QPD.applicant.insuredSalary = false;
                /*if (vm.ipCoverDetails.type === '1') {*/
                    vm.QPD.applicant.cover[2].additionalUnit = vm.ipCoverDetails.units;
                /*}*/
                vm.isIPCoverNameDisabled = true;
                vm.isWaitingPeriodDisabled = true;
                vm.isBenefitPeriodDisabled = true;
                vm.isIPCoverRequiredDisabled = true;
                vm.isIpSalaryCheckboxDisabled = true;
                vm.ipWarning = true;
                $('#ipsalarycheck').removeAttr('checked');
                $('#ipsalarychecklabel').removeClass('active');
            };
            vm.toggleIPCondition = function() {
                if (vm.isIPDisabled){
                     return false;
                }
                   
                if (vm.QPD.applicant.workHours === 'No') {
                    vm.disableIpCover();
                    /*vm.QPD.applicant.cover[2].additionalUnit = '';
                    angular.element(document.querySelector('.cover-units-ip')).html(' ');*/
                } else {
                    vm.QPD.applicant.cover[2].coverOption = '';
                    angular.element(document.querySelector('.cover-units-ip')).html('units');
                    /*if (vm.ipCoverDetails.type === '1') {*/
                          vm.QPD.applicant.cover[2].additionalUnit = vm.ipCoverDetails.units;
                   /* }*/
                    vm.isIPCoverNameDisabled = false;
                    vm.isWaitingPeriodDisabled = false;
                    vm.isBenefitPeriodDisabled = false;
                    vm.isIPCoverRequiredDisabled = false;
                    vm.isIpSalaryCheckboxDisabled = false;
                    vm.ipWarning = false;
                }
                vm.renderOccupationQuestions();
                vm.QPD.applicant.occupationDuties = null;
                vm.QPD.applicant.tertiaryQue = null;
                vm.QPD.applicant.workDuties = null;
                vm.QPD.applicant.spendTimeOutside = null;
            };

            vm.checkAnnualSalary = function() {
                if (parseInt(vm.QPD.applicant.annualSalary) === 0) {
                    vm.invalidSalAmount = true;
                } else {
                    vm.invalidSalAmount = false;
                }
                if (!vm.invalidSalAmount){
                	 //vm.checkIpCover();
                     vm.renderOccupationQuestions();
                }
                   

                if (vm.QPD.applicant.insuredSalary) {
                    vm.insureNinetyPercentIp();
                }
            };
            vm.checkInsuredIp = function(){
            vm.QPD.applicant.insuredSalary = !vm.QPD.applicant.insuredSalary;
            if(!vm.QPD.applicant.insuredSalary){
            	vm.QPD.applicant.cover[2].additionalUnit = 0;
            	vm.QPD.applicant.cover[2].additionalCoverAmount = 0;
            	vm.QPD.applicant.cover[2].cost = 0;
            	vm.isIPCoverRequiredDisabled = false;
                vm.ipWarningFlag = false;
            }else{
            	vm.insureNinetyPercentIp();
            }
             
            };
            vm.insureNinetyPercentIp = function() {
                vm.ipWarningFlag = false;
                     //$('#ipsalarycheck').prop('checked');
                    if (vm.QPD.applicant.insuredSalary === true) {
                        vm.QPD.applicant.cover[2].additionalUnit = Math.round(parseFloat((0.85 * (vm.QPD.applicant.annualSalary / 12)).toFixed(2)) / 100) * 100;
                        if (vm.QPD.applicant.cover[2].additionalUnit > vm.IPMaxAmount) {
                            vm.QPD.applicant.cover[2].additionalUnit = Math.round(parseFloat(vm.IPMaxAmount/250));
                            vm.ipWarningFlag = true;
                        } else {
                          vm.QPD.applicant.cover[2].additionalUnit = parseInt(vm.QPD.applicant.cover[2].additionalUnit/250);
                            vm.ipWarningFlag = false;
                        }
                        vm.isIPCoverRequiredDisabled = true;
                    } else {
                          /*if (vm.ipCoverDetails.type === '1') {*/
                          vm.QPD.applicant.cover[2].additionalUnit = vm.ipCoverDetails.units;
                        /*}*/
                        vm.isIPCoverRequiredDisabled = false;
                        vm.ipWarningFlag = false;
                    }
                    vm.validateDeathTpdIpAmounts();
            };
             vm.checkOwnBusinessQuestion = function() {
                vm.QPD.applicant.ownBusinessYesQue = null;
                vm.QPD.applicant.ownBusinessNoQue = null;
                if (vm.QPD.applicant.ownBusinessQue === 'Yes') {
                    vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'annualSalary'];
                    vm.OccupationOtherFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'otherOccupation', 'annualSalary'];
                } else if (vm.QPD.applicant.ownBusinessQue === 'No') {
                    vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'annualSalary'];
                    vm.OccupationOtherFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'otherOccupation', 'annualSalary'];
                }
           };

            vm.getOccupations = function() {
                vm.QPD.applicant.occupation = '';
                vm.QPD.applicant.otherOccupation = '';
                vm.QPD.applicant.industryType = vm.QPD.applicant.industryType === '' ? null : vm.QPD.applicant.industryType;
                fetchOccupationSvc.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.QPD.applicant.industryType).then(function(res) {
                    vm.OccupationList = res.data;
                }, function(err) {
                    console.info('Error while fetching occupations ' + JSON.stringify(err));
                });
            };
            vm.getOtherOccupationAS = function(entered) {

                return $http.get('./occupation.json').then(function(response) {
                    vm.occupationList = [];
                    if (response.data.Other) {
                        for (var key in response.data.Other) {
                          if (Object.hasOwnProperty(key)) {
                                  var obj = {};
                                 obj.id = key;
                                 obj.name = response.data.Other[key];
                                 vm.occupationList.push(obj.name);
                          }
                           

                        }
                    }
                    return $filter('filter')(vm.occupationList, entered);
                }, function(err) {
                    console.info('Error while fetching occupations ' + JSON.stringify(err));
                });

            };
            vm.syncRadios = function(val) {
                var radios = $('label[radio-sync]');
                var data = $('input[data-sync]');
                data.filter('[data-sync=\'\' + val + \'\']').attr('checked', 'checked');
                if (val === 'Fixed') {
                    vm.deathErrorFlag = false;
                    vm.QPD.deathErrorMsg = '';
                    vm.tpdErrorFlag = false;
                    vm.QPD.tpdErrorMsg = '';
                    vm.showhide('dollar1', 'nodollar1');
                    vm.showhide('dollar', 'nodollar');
                    vm.QPD.applicant.cover[0].coverCategory  = 'DcFixed';
                    vm.QPD.applicant.cover[1].coverCategory  = 'TPDFixed';
                } else if (val === 'Unitised') {
                    vm.deathErrorFlag = false;
                    vm.QPD.deathErrorMsg = '';
                    vm.tpdErrorFlag = false;
                    vm.QPD.tpdErrorMsg = '';
                    vm.showhide('nodollar1', 'dollar1');
                    vm.showhide('nodollar', 'dollar');
                    vm.QPD.applicant.cover[0].coverCategory  = 'DcUnitised';
                    vm.QPD.applicant.cover[1].coverCategory  = 'TPDUnitised';
                }
                radios.removeClass('active');
                radios.filter('[radio-sync=\'\' + val + \'\']').addClass('active');
                /*vm.QPD.applicant.cover[0].additionalUnit = null;
                vm.QPD.applicant.cover[1].additionalUnit = null;*/
                if(val==='Unitised'){
                    vm.QPD.applicant.cover[0].additionalUnit = vm.deathCoverDetails.units;
                    vm.QPD.applicant.cover[1].additionalUnit = vm.tpdCoverDetails.units;
                    vm.QPD.applicant.cover[0].additionalCoverAmount = vm.deathCoverDetails.amount || 0;
                    vm.QPD.applicant.cover[1].additionalCoverAmount = vm.tpdCoverDetails.amount || 0;
                }else{
                    vm.QPD.applicant.cover[0].additionalCoverAmount = Math.ceil(vm.deathCoverDetails.amount/1000)*1000 || 0;
                    vm.QPD.applicant.cover[1].additionalCoverAmount = Math.ceil(vm.tpdCoverDetails.amount/1000)*1000 || 0;
                }
                
                if (vm.deathCoverDetails.type === '2' && vm.tpdCoverDetails.type === '2') {
                    if(val==='Unitised'){
                          vm.QPD.applicant.cover[0].additionalUnit = vm.deathUnitsForFixed;
                          vm.QPD.applicant.cover[1].additionalUnit = vm.tpdUnitsForFixed;
                          vm.QPD.applicant.cover[0].additionalCoverAmount = vm.deathCoverDetails.amount || 0;
                          vm.QPD.applicant.cover[1].additionalCoverAmount = vm.tpdCoverDetails.amount || 0;
                    }else{
                          vm.QPD.applicant.cover[0].additionalCoverAmount = Math.ceil(vm.deathCoverDetails.amount/1000)*1000 || 0;
                          vm.QPD.applicant.cover[1].additionalCoverAmount = Math.ceil(vm.tpdCoverDetails.amount/1000)*1000 || 0;
                    }
                    vm.deathUnitsForFixedOld = vm.deathUnitsForFixed;
                    vm.tpdUnitsForFixedOld = vm.tpdUnitsForFixed;
                }
                vm.validateDeathTpdIpAmounts();
            };
            vm.navigateToLandingPage = function() {
                ngDialog.openConfirm({
                    template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'>' + 
                    '<button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
                    plain: true,
                    className: 'ngdialog-theme-plain custom-width'
                }).then(function() {
                    $location.path('/landing');
                }, function(e) {
                    if (e === 'oncancel') {
                        return false;
                    }
                });
            };

            function matchOccupation(occupation, occArray){
                for (var i=0; i < occArray.length; i++) {
                    if (occArray[i].occupationName === occupation) {
                        return occArray[i];
                    }
                }
            }
            
            vm.getCategoryFromDB = function(fromSelect) {
            var occName;
                if (fromSelect && vm.QPD.applicant.occupation !== 'Other') {
                    vm.QPD.applicant.otherOccupation = '';
                }
                if (vm.prevOtherOcc !== vm.QPD.applicant.otherOccupation) {
                    if (vm.QPD.applicant.occupation !== undefined || vm.QPD.applicant.otherOccupation !== undefined) {
                        if (fromSelect) {
                            vm.QPD.applicant.occupationDuties= null;
                            vm.QPD.applicant.tertiaryQue = null;
                            vm.QPD.applicant.workDuties = null;
                            vm.QPD.applicant.spendTimeOutside = null;
                        }
                        if (vm.QPD.applicant.occupation !== undefined && (vm.QPD.applicant.otherOccupation === null || vm.QPD.applicant.otherOccupation === '')) {
                             occName = vm.QPD.applicant.industryType + ':' + vm.QPD.applicant.occupation;
                        } else if (vm.QPD.applicant.otherOccupation !== undefined) {
                            vm.prevOtherOcc = vm.QPD.applicant.otherOccupation;
                            if ((matchOccupation(vm.QPD.applicant.otherOccupation,vm.OccupationList)) !== undefined) {
                                 occName = vm.QPD.applicant.industryType + ':' + vm.QPD.applicant.otherOccupation;
                            } else {
                                 occName = vm.QPD.applicant.industryType + ':' + vm.QPD.applicant.occupation;
                            }

                        }

                        NewOccupationService.getOccupation(vm.urlList.newOccupationUrl, 'MTAA', occName).then(function(res) {
                            if (vm.QPD.applicant.cover[0].coverCategory === 'DcFixed') {
                                vm.deathDBCategory = res.data[0].deathfixedcategeory;
                            } else if (vm.QPD.applicant.cover[0].coverCategory === 'DcUnitised') {
                                vm.deathDBCategory = res.data[0].deathunitcategeory;
                            }
                            if (vm.QPD.applicant.cover[1].coverCategory === 'TPDFixed') {
                                vm.tpdDBCategory = res.data[0].tpdfixedcategeory;
                            } else if (vm.QPD.applicant.cover[1].coverCategory === 'TPDUnitised') {
                                vm.tpdDBCategory = res.data[0].tpdunitcategeory;
                            }
                            vm.ipDBCategory = res.data[0].ipfixedcategeory;
                            vm.eligibleForStIp();
                            vm.renderOccupationQuestions();
                        }, function(err) {
                            console.info('Error while getting category from DB ' + JSON.stringify(err));
                        });
                    }
                }
            };

            vm.eligibleForStIp = function() {
                var occ;
                if (vm.QPD.applicant.occupation !== 'Other') {
                    occ = vm.QPD.applicant.occupation;
                } else {
                    occ = vm.QPD.applicant.otherOccupation;
                }
                var occSearch = true;
                propertyService.getOccList().then(function(response) {
                    vm.decOccList = response.data;
                    angular.forEach(vm.decOccList, function(value, key) {
                        if (occSearch) {
                            if (angular.lowercase(key) === angular.lowercase(occ)) {
                                if (angular.lowercase(value) === 'ip') {
                                    vm.eligibleFrStIp = true;
                                    occSearch = false;
                                }
                            } else {
                                vm.eligibleFrStIp = false;
                            }
                        }
                    });
                    if (vm.eligibleFrStIp && vm.QPD.applicant.cover[2].additionalIpBenefitPeriod === 'Age 65') {
                        vm.eligibleFrStIpFlag = true;
                    } else {
                        vm.eligibleFrStIpFlag = false;
                    }
                });
            };

            vm.calculateOnChange = function() {
            		 vm.QPD.applicant.cover[0].frequencyCostType = vm.frequencyCostType;
                     vm.QPD.applicant.cover[1].frequencyCostType = vm.frequencyCostType;
                     vm.QPD.applicant.cover[2].frequencyCostType = vm.frequencyCostType;
           
                if (this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
                    //if (!vm.deathErrorFlag && !vm.tpdErrorFlag && !vm.ipErrorFlag && !vm.eligibleFrStIpFlag) {
                        vm.calculate();
                    //}
                }
            };
            
            vm.renderOccupationQuestions = function() {
                if (vm.QPD.applicant.occupation || vm.QPD.applicant.otherOccupation) {
                    var selectedOcc = vm.OccupationList.filter(function(obj) {
                        if (vm.QPD.applicant.occupation && vm.QPD.applicant.otherOccupation === '') {
                            return obj.occupationName === vm.QPD.applicant.occupation;
                        } else if (vm.QPD.applicant.otherOccupation && ((matchOccupation(vm.QPD.applicant.otherOccupation,vm.OccupationList)) !== undefined)) {
                            return obj.occupationName === vm.QPD.applicant.otherOccupation;
                        } else {
                            return obj.occupationName === vm.QPD.applicant.occupation;
                        }
                    });
                    var selectedOccObj = selectedOcc[0];

                    if (selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'false') {
                        vm.showWithinOfficeQuestion = true;
                        vm.showTertiaryQuestion = true;
                        vm.showHazardousQuestion = false;
                        vm.showOutsideOfficeQuestion = false;
                        if (vm.QPD.applicant.ownBusinessQue === 'Yes') {
                            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
                        } else if (vm.QPD.applicant.ownBusinessQue === 'No') {
                            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
                        }

                        if (vm.QPD.applicant.occupationDuties === 'Yes' && vm.QPD.applicant.tertiaryQue === 'Yes' && vm.QPD.applicant.annualSalary && parseFloat(vm.QPD.applicant.annualSalary) >= parseFloat(vm.annualSalForUpgradeVal)) {
                            vm.QPD.applicant.cover[0].occupationRating = 'Professional';
                            vm.QPD.applicant.cover[1].occupationRating = 'Professional';
                            vm.QPD.applicant.cover[2].occupationRating = 'Professional';
                        } else {
                            vm.QPD.applicant.cover[0].occupationRating = vm.deathDBCategory;
                            vm.QPD.applicant.cover[1].occupationRating = vm.tpdDBCategory;
                            vm.QPD.applicant.cover[2].occupationRating = vm.ipDBCategory;
                        }
                    } else if (selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'true') {
                        vm.showWithinOfficeQuestion = false;
                        vm.showTertiaryQuestion = false;
                        vm.showHazardousQuestion = true;
                        vm.showOutsideOfficeQuestion = true;
                        if (vm.QPD.applicant.ownBusinessQue === 'Yes') {
                            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                        } else if (vm.QPD.applicant.ownBusinessQue === 'No') {
                            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                        }
                        if (vm.QPD.applicant.workDuties === 'No' && vm.QPD.applicant.spendTimeOutside === 'No') {
                            vm.showWithinOfficeQuestion = true;
                            vm.showTertiaryQuestion = true;
                            if (vm.QPD.applicant.ownBusinessQue === 'Yes') {
                                vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
                            } else if (vm.QPD.applicant.ownBusinessQue === 'No') {
                                vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'withinOfficeQuestion', 'tertiaryQuestion', 'annualSalary'];
                            }

                            vm.QPD.applicant.cover[0].occupationRating = 'Non Manual';
                            vm.QPD.applicant.cover[1].occupationRating = 'Non Manual';
                            vm.QPD.applicant.cover[2].occupationRating = 'Non Manual';
                            if (vm.QPD.applicant.occupationDuties === 'Yes' && vm.QPD.applicant.tertiaryQue === 'Yes' && vm.QPD.applicant.annualSalary && parseFloat(vm.QPD.applicant.annualSalary) >= parseFloat(vm.annualSalForUpgradeVal)) {
                                 vm.QPD.applicant.cover[0].occupationRating = 'Professional';
                                 vm.QPD.applicant.cover[1].occupationRating = 'Professional';
                                 vm.QPD.applicant.cover[2].occupationRating = 'Professional';
                            } else {
                                 vm.QPD.applicant.cover[0].occupationRating = 'Non Manual';
                                 vm.QPD.applicant.cover[1].occupationRating = 'Non Manual';
                                 vm.QPD.applicant.cover[2].occupationRating = 'Non Manual';
                            }
                        } else {
                            vm.showWithinOfficeQuestion = false;
                            vm.showTertiaryQuestion = false;
                            if (vm.QPD.applicant.ownBusinessQue === 'Yes') {
                                vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                            } else if (vm.QPD.applicant.ownBusinessQue === 'No') {
                                vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                            }

                            vm.QPD.applicant.cover[0].occupationRating = vm.deathDBCategory;
                            vm.QPD.applicant.cover[1].occupationRating = vm.tpdDBCategory;
                            vm.QPD.applicant.cover[2].occupationRating = vm.ipDBCategory;
                        }
                    } else if (selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'true') {
                        vm.showWithinOfficeQuestion = false;
                        vm.showTertiaryQuestion = false;
                        vm.showHazardousQuestion = true;
                        vm.showOutsideOfficeQuestion = true;
                        if (vm.QPD.applicant.ownBusinessQue === 'Yes') {
                            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessYesQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                        } else if (vm.QPD.applicant.ownBusinessQue === 'No') {
                            vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'ownBussinessNoQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'hazardousQuestion', 'outsideOffice', 'annualSalary'];
                        }

                        if (vm.QPD.applicant.workDuties === 'No' && vm.QPD.applicant.spendTimeOutside === 'No') {
                          vm.QPD.applicant.cover[0].occupationRating = 'Non Manual';
                          vm.QPD.applicant.cover[1].occupationRating = 'Non Manual';
                          vm.QPD.applicant.cover[2].occupationRating = 'Non Manual';
                        } else {
                          vm.QPD.applicant.cover[0].occupationRating = 'General';
                          vm.QPD.applicant.cover[1].occupationRating = 'General';
                            vm.QPD.applicant.cover[2].occupationRating = 'General';
                        }
                    } else if (selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'false') {
                        vm.showWithinOfficeQuestion = false;
                        vm.showTertiaryQuestion = false;
                        vm.showHazardousQuestion = false;
                        vm.showOutsideOfficeQuestion = false;
                        vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'annualSalary'];

                        vm.QPD.applicant.cover[0].occupationRating = vm.deathDBCategory;
                        vm.QPD.applicant.cover[1].occupationRating = vm.tpdDBCategory;
                        vm.QPD.applicant.cover[2].occupationRating = vm.ipDBCategory;
                    }
                }
                
        
                else {
                    vm.showWithinOfficeQuestion = false;
                    vm.showTertiaryQuestion = false;
                    vm.showHazardousQuestion = false;
                    vm.showOutsideOfficeQuestion = false;
                    vm.OccupationFormFields = ['fifteenHrsQuestion', 'ownBussinessQuestion', 'areyouperCitzQuestion', 'industry', 'occupation', 'annualSalary'];

                    if (vm.deathDBCategory === 'Non Manual') {
                          vm.QPD.applicant.cover[0].occupationRating= 'General';
                    } else if (vm.deathDBCategory === 'Professional') {
                          vm.QPD.applicant.cover[0].occupationRating = 'Professional';
                    } else {
                          vm.QPD.applicant.cover[0].occupationRating = 'General';
                    }

                    if (vm.tpdDBCategory === 'Non Manual') {
                          vm.QPD.applicant.cover[1].occupationRating = 'General';
                    } else if (vm.tpdDBCategory === 'Professional') {
                          vm.QPD.applicant.cover[1].occupationRating = 'Professional';
                    } else {
                          vm.QPD.applicant.cover[1].occupationRating = 'General';
                    }

                    if (vm.ipDBCategory === 'Non Manual') {
                          vm.QPD.applicant.cover[2].occupationRating = 'General';
                    } else if (vm.ipDBCategory === 'Professional') {
                          vm.QPD.applicant.cover[2].occupationRating = 'Professional';
                    } else {
                          vm.QPD.applicant.cover[2].occupationRating = 'General';
                    }
                }


                var _this = this;
                $timeout(function(){
                    if(_this.formOne.$valid && _this.occupationForm.$valid && _this.coverCalculatorForm.$valid){
                      vm.calculate();
                    }
                  });

            };

            // validate fields 'on continue'
            vm.onFormContinue = function(form) {
                if (!form.$valid) {
                    form.$submitted = true;
                    if (form.$name === 'formOne') {
                        vm.toggleOccupation(false);
                        vm.toggleCoverCalc(false);
                    } else if (form.$name === 'occupationForm') {
                        vm.toggleCoverCalc(false);
                    }
                } else {
                    if (form.$name === 'formOne') {
                        vm.toggleOccupation(true);
                    } else if (form.$name === 'occupationForm') {
                        vm.toggleCoverCalc(true);
                    } else if (form.$name === 'coverCalculatorForm') {
                        if (!vm.deathErrorFlag && !vm.tpdErrorFlag && !vm.ipErrorFlag && !vm.eligibleFrStIpFlag) {
                            vm.calculate();
                        }
                    }
                }
            };

            vm.calculate = function() {

                if (vm.deathCoverDetails.type === '2' && vm.tpdCoverDetails.type === '2'  && vm.ipCoverDetails.type === '2') {
                    vm.calculateForUnits();
                }

                var ruleModel = {
                    'age': vm.QPD.applicant.age,
                    'fundCode': 'MTAA',
                    'gender': vm.QPD.applicant.gender,
                    'deathOccCategory': vm.QPD.applicant.cover[0].occupationRating,
                    'tpdOccCategory': vm.QPD.applicant.cover[1].occupationRating,
                    'ipOccCategory': vm.QPD.applicant.cover[2].occupationRating,
                    'smoker': false,
                    'deathUnits': parseInt(vm.QPD.applicant.cover[0].additionalUnit),
                    'deathFixedAmount': parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount),
                    'deathFixedCost': null,
                    'deathUnitsCost': null,
                    'tpdUnits': parseInt(vm.QPD.applicant.cover[1].additionalUnit),
                    'tpdFixedAmount': parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount),
                    'tpdFixedCost': null,
                    'tpdUnitsCost': null,
                    'ipUnits': parseInt(vm.QPD.applicant.cover[2].additionalUnit),
                    'ipFixedAmount': parseInt(vm.QPD.applicant.cover[2].additionalCoverAmount),
                    'ipFixedCost': null,
                    'ipUnitsCost': null,
                    'premiumFrequency': vm.QPD.applicant.cover[0].frequencyCostType,
                    'memberType': null,
                    'manageType': 'CCOVER',
                    'deathCoverType': vm.QPD.applicant.cover[0].coverCategory,
                    'tpdCoverType': vm.QPD.applicant.cover[1].coverCategory ,
                    'ipCoverType': 'IpUnitised',
                    'ipWaitingPeriod': vm.QPD.applicant.cover[2].existingIpWaitingPeriod,
                    'ipBenefitPeriod': vm.QPD.applicant.cover[2].existingIpBenefitPeriod
                };
                CalculateService.calculate(ruleModel, vm.urlList.calculateUrl).then(function(res) {
                    var premium = res.data;
                    vm.dynamicFlag = true;
                    for (var i = 0; i < premium.length; i++) {
                        if (premium[i].coverType === 'DcFixed') {
                            vm.QPD.applicant.cover[0].additionalCoverAmount = Math.ceil(parseInt(premium[i].coverAmount)/1000)*1000 || 0;
                            vm.QPD.applicant.cover[0].cost = premium[i].cost || 0;
                        } else if (premium[i].coverType === 'TPDFixed') {
                            vm.QPD.applicant.cover[1].additionalCoverAmount = Math.ceil(parseInt(premium[i].coverAmount)/1000)*1000 || 0;
                            vm.QPD.applicant.cover[1].cost = premium[i].cost || 0;
                        } else if (premium[i].coverType === 'IpFixed') {
                            vm.QPD.applicant.cover[2].additionalCoverAmount = Math.ceil(parseInt(premium[i].coverAmount)/1000)*1000 || 0;
                            vm.QPD.applicant.cover[2].cost = premium[i].cost || 0;
                        }
                        if (premium[i].coverType === 'DcUnitised') {
                            vm.QPD.applicant.cover[0].additionalCoverAmount = parseInt(premium[i].coverAmount) || 0;
                            vm.QPD.applicant.cover[0].cost= premium[i].cost || 0;
                        } else if (premium[i].coverType === 'TPDUnitised') {
                            vm.QPD.applicant.cover[1].additionalCoverAmount = parseInt(premium[i].coverAmount) || 0;
                            vm.QPD.applicant.cover[1].cost = premium[i].cost || 0;
                        } else if (premium[i].coverType === 'IpUnitised') {
                            vm.QPD.applicant.cover[2].additionalUnit = parseInt((premium[i].coverAmount/250));
                            vm.QPD.applicant.cover[2].additionalCoverAmount = parseInt(premium[i].coverAmount) || 0;
                            vm.QPD.applicant.cover[2].cost = premium[i].cost || 0;
                        }
                    }
                    vm.QPD.totalMonthlyPremium = parseFloat(vm.QPD.applicant.cover[0].cost) + parseFloat(vm.QPD.applicant.cover[1].cost) + parseFloat(vm.QPD.applicant.cover[2].cost);
                    if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) > parseInt(vm.DCMaxAmount)) {
                        vm.deathErrorFlag = true;
                        vm.deathErrorMsg = 'Your total death cover exceeds eligibility limit, maximum allowed for this product is  ' + vm.DCMaxAmount + '. Please re-enter your cover.';
                    }else{
                    	vm.deathErrorFlag = false;
                    }
                    
                    if (vm.canValidateDeathTpd() && ((vm.QPD.applicant.cover[1].coverCategory  === 'TPDUnitised' && parseInt(vm.QPD.applicant.cover[1].additionalUnit) > parseInt(vm.QPD.applicant.cover[0].additionalUnit)) || (vm.QPD.applicant.cover[1].coverCategory  === 'TPDFixed' && parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) > parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount)))) {
                        vm.tpdErrorFlag = true;
                        vm.tpdErrorMsg = 'TPD amount should not be greater than your Death amount.Please re-enter.';
                    }else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) > parseInt(vm.TPDMaxAmount)) {
                        vm.tpdErrorFlag = true;
                        vm.tpdErrorMsg = 'Your total TPD cover exceeds eligibility limit, maximum allowed for this product is ' + vm.TPDMaxAmount + '. Please re-enter your cover.';
                    }else{
                    	vm.tpdErrorFlag = false;
                    }
                }, function(err) {
                    console.info('Something went wrong while calculating...' + JSON.stringify(err));
                });
            };

            vm.checkBenefitPeriod = function() { 
            	vm.QPD.applicant.cover[2].existingIpBenefitPeriod = vm.QPD.applicant.cover[2].additionalIpBenefitPeriod;
            	vm.QPD.applicant.cover[2].existingIpWaitingPeriod = vm.QPD.applicant.cover[2].additionalIpWaitingPeriod;
                vm.tempWaitingPeriod = vm.waitingPeriodOptions.indexOf(vm.ipCoverDetails.waitingPeriod) > -1 ? vm.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
                vm.tempBenefitPeriod = vm.benefitPeriodOptions.indexOf(vm.ipCoverDetails.benefitPeriod) > -1 ? vm.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';

                if (vm.tempWaitingPeriod === vm.QPD.applicant.cover[2].existingIpWaitingPeriod && vm.tempBenefitPeriod === vm.QPD.applicant.cover[2].existingIpBenefitPeriod) {
                    vm.ipIncreaseFlag = false;
                    vm.disclaimerFlag = true;
                } else if (
                	//fix to trigger Aura
                	(vm.tempBenefitPeriod === '5 Years' && vm.QPD.applicant.cover[2].existingIpBenefitPeriod === 'Age 65') ||
                	(vm.tempBenefitPeriod === '2 Years' && vm.QPD.applicant.cover[2].existingIpBenefitPeriod === 'Age 65') ||
                	(vm.tempBenefitPeriod === '2 Years' && vm.QPD.applicant.cover[2].existingIpBenefitPeriod === '5 Years') ||
                    (vm.tempWaitingPeriod === '90 Days' && vm.QPD.applicant.cover[2].existingIpWaitingPeriod === '60 Days') ||
                    (vm.tempWaitingPeriod === '90 Days' && vm.QPD.applicant.cover[2].existingIpWaitingPeriod === '30 Days') ||
                    (vm.tempWaitingPeriod === '60 Days' && vm.QPD.applicant.cover[2].existingIpWaitingPeriod === '30 Days')) {
                    vm.ipIncreaseFlag = true;
                    vm.disclaimerFlag = true;
                    vm.QPD.auraDisabled = false;
                } else {
                    vm.ipIncreaseFlag = false;
                    vm.disclaimerFlag = false;
                }
                
                vm.eligibleForStIp();
                vm.validateDeathTpdIpAmounts();
                /*vm.QPD.applicant.cover[2].existingIpWaitingPeriod = vm.tempWaitingPeriod;
                vm.QPD.applicant.cover[2].existingIpBenefitPeriod = vm.tempBenefitPeriod;*/
                vm.QPD.applicant.cover[2].existingIpBenefitPeriod = vm.ipCoverDetails.benefitPeriod;
            	vm.QPD.applicant.cover[2].existingIpWaitingPeriod = vm.ipCoverDetails.waitingPeriod;
            };

            /*vm.checkIpCover = function() { old function
            	var insuredSalary = 0.85 * (parseInt(vm.QPD.applicant.annualSalary)/12);
            	if(parseInt(vm.QPD.applicant.cover[2].additionalUnit)*250 > insuredSalary){
            		vm.IPAmount = Math.round(parseFloat(insuredSalary/250));
            	}
            	if (parseInt(vm.QPD.applicant.cover[2].additionalUnit) > vm.IPAmount && !vm.QPD.applicant.insuredSalary) {
                    vm.QPD.applicant.cover[2].additionalUnit = vm.IPAmount;
                    vm.ipErrorFlag = false;
                    vm.ipErrorMsg = '';
                    vm.ipWarningFlag = true;
                }else{
                	if(vm.QPD.applicant.cover[2].additionalUnit === null){
                		vm.QPD.applicant.cover[2].additionalUnit = 0;
                	}
                }
            };*/
            
            vm.checkIpCover = function() {
            	
            	var ipCoverMaxAmount = 25000;
            	var insuredSalaryAmount = 0.85 * (parseInt(vm.QPD.applicant.annualSalary)/12);
            	
            	if(ipCoverMaxAmount > insuredSalaryAmount){
            		vm.IPAmount = parseInt(insuredSalaryAmount/250);
            	}else{
            		vm.IPAmount = parseInt(ipCoverMaxAmount/250);
            	}
            		
            	if (parseInt(vm.QPD.applicant.cover[2].additionalUnit) > vm.IPAmount && !vm.QPD.applicant.insuredSalary) {
                    vm.QPD.applicant.cover[2].additionalUnit = vm.IPAmount;
                    vm.ipErrorFlag = false;
                    vm.ipErrorMsg = '';
                    vm.ipWarningFlag = true;
                }else{
                	if(vm.QPD.applicant.cover[2].additionalUnit === null){
                		vm.QPD.applicant.cover[2].additionalUnit = 0;
                	}
                }
            };

            vm.validateDeathTpdIpAmounts = function() {
            	vm.QPD.auraDisabled = true;
                vm.deathErrorFlag = false;
                vm.tpdErrorFlag = false;
                vm.ipErrorFlag = false;
                vm.deathDecOrCancelFlag = false;
                vm.tpdDecOrCancelFlag = false;
                vm.ipDecOrCancelFlag = false;
                vm.ipWarningFlag = false;
                if(vm.ipCoverMax === false){
                	vm.checkIpCover();
                }
                // error check
                
                if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) > parseInt(vm.DCMaxAmount)) {
                    vm.deathErrorFlag = true;
                    vm.deathErrorMsg = 'Your total death cover exceeds eligibility limit, maximum allowed for this product is  ' + vm.DCMaxAmount + '. Please re-enter your cover.';
                }else{
                	vm.deathErrorFlag = false;
                }
                
                if (vm.canValidateDeathTpd() && ((vm.QPD.applicant.cover[1].coverCategory  === 'TPDUnitised' && parseInt(vm.QPD.applicant.cover[1].additionalUnit) > parseInt(vm.QPD.applicant.cover[0].additionalUnit)) || (vm.QPD.applicant.cover[1].coverCategory  === 'TPDFixed' && parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) > parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount)))) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'TPD amount should not be greater than your Death amount.Please re-enter.';
                }else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) > parseInt(vm.TPDMaxAmount)) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'Your total TPD cover exceeds eligibility limit, maximum allowed for this product is ' + vm.TPDMaxAmount + '. Please re-enter your cover.';
                }else{
                	vm.tpdErrorFlag = false;
                }

                
                //cover name check
                //if (!vm.deathErrorFlag && !vm.tpdErrorFlag && !vm.ipErrorFlag) {
                    // Death cover
                    var eXDcAmount = parseInt(vm.deathCoverDetails.amount);
                    if (vm.QPD.applicant.cover[0].coverCategory === 'DcFixed') {
                        if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) < eXDcAmount) {
                            vm.QPD.applicant.cover[0].coverOption = 'Decrease your cover';
                            vm.deathDecOrCancelFlag = true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) > eXDcAmount) {
                          vm.QPD.applicant.cover[0].coverOption = 'Increase your cover';
                            vm.QPD.auraDisabled = false;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) === eXDcAmount) {
                          vm.QPD.applicant.cover[0].coverOption = 'No change';
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) === 0 && parseInt(vm.deathCoverDetails.amount) !== 0) {
                          vm.QPD.applicant.cover[0].coverOption = 'Cancel your cover';
                            vm.deathDecOrCancelFlag = true;
                        }
                    } else if (vm.QPD.applicant.cover[0].coverCategory  === 'DcUnitised') {
                        if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) < parseInt(vm.deathCoverDetails.units)) {
                          vm.QPD.applicant.cover[0].coverOption = 'Decrease your cover';
                            vm.deathDecOrCancelFlag = true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) > parseInt(vm.deathCoverDetails.units)) {
                          vm.QPD.applicant.cover[0].coverOption = 'Increase your cover';
                            vm.QPD.auraDisabled = false;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) === parseInt(vm.deathCoverDetails.units)) {
                          vm.QPD.applicant.cover[0].coverOption = 'No change';
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) === 0 && parseInt(vm.deathCoverDetails.units !== 0)) {
                          vm.QPD.applicant.cover[0].coverOption = 'Cancel your cover';
                            vm.deathDecOrCancelFlag = true;
                        }
                    }


                    // Tpd cover
                    var eXTpdAmount = parseInt(vm.tpdCoverDetails.amount);
                    if (vm.QPD.applicant.cover[1].coverCategory === 'TPDFixed') {
                        if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) < eXTpdAmount) {
                            vm.QPD.applicant.cover[1].coverOption = 'Decrease your cover';
                            vm.tpdDecOrCancelFlag = true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) > eXTpdAmount) {
                          vm.QPD.applicant.cover[1].coverOption = 'Increase your cover';
                            vm.QPD.auraDisabled = false;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) === eXTpdAmount) {
                          vm.QPD.applicant.cover[1].coverOption = 'No change';
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) === 0 && eXTpdAmount !== 0) {
                          vm.QPD.applicant.cover[1].coverOption = 'Cancel your cover';
                            vm.tpdDecOrCancelFlag = true;
                        }
                    } else if (vm.QPD.applicant.cover[1].coverCategory  === 'TPDUnitised') {
                        if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) < parseInt(vm.tpdCoverDetails.units)) {
                          vm.QPD.applicant.cover[1].coverOption = 'Decrease your cover';
                            vm.tpdDecOrCancelFlag = true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) > parseInt(vm.tpdCoverDetails.units)) {
                          vm.QPD.applicant.cover[1].coverOption = 'Increase your cover';
                            vm.QPD.auraDisabled = false;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) === parseInt(vm.tpdCoverDetails.units)) {
                          vm.QPD.applicant.cover[1].coverOption = 'No change';
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) === 0 && parseInt(vm.tpdCoverDetails.units !== 0)) {
                          vm.QPD.applicant.cover[1].coverOption = 'Cancel your cover';
                            vm.tpdDecOrCancelFlag = true;
                        }
                    }
                    if ((vm.deathCoverDetails.type === '2' && vm.tpdCoverDetails.type === '2') && (vm.QPD.applicant.cover[0].coverCategory === 'DcUnitised' && vm.QPD.applicant.cover[1].coverCategory === 'TPDUnitised')) {
                        vm.QPD.auraDisabled = true;
                        vm.deathDecOrCancelFlag = false;
                        vm.tpdDecOrCancelFlag = false;
                        if (vm.QPD.applicant.cover[0].coverCategory === 'DcUnitised') {
                            if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) < parseInt(vm.deathUnitsForFixed)) {
                                 vm.QPD.applicant.cover[0].coverOption = 'Decrease your cover';
                                vm.deathDecOrCancelFlag = true;
                            } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) > parseInt(vm.deathUnitsForFixed)) {
                                 vm.QPD.applicant.cover[0].coverOption = 'Increase your cover';
                                vm.QPD.auraDisabled = false;
                            } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) === parseInt(vm.deathUnitsForFixed)) {
                                 vm.QPD.applicant.cover[0].coverOption = 'No change';
                            } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) === 0 && parseInt(vm.deathUnitsForFixed !== 0)) {
                                 vm.QPD.applicant.cover[0].coverOption = 'Cancel your cover';
                                vm.deathDecOrCancelFlag = true;
                            }
                        }

                        if (vm.QPD.applicant.cover[1].coverCategory === 'TPDUnitised') {
                            if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) < parseInt(vm.tpdUnitsForFixed)) {
                                 vm.QPD.applicant.cover[1].coverOption = 'Decrease your cover';
                               vm.tpdDecOrCancelFlag = true;
                            } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) > parseInt(vm.tpdUnitsForFixed)) {
                                 vm.QPD.applicant.cover[1].coverOption = 'Increase your cover';
                                vm.QPD.auraDisabled = false;
                            } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) === parseInt(vm.tpdUnitsForFixed)) {
                                 vm.QPD.applicant.cover[1].coverOption = 'No change';
                            } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) === 0 && parseInt(vm.tpdUnitsForFixed !== 0)) {
                                 vm.QPD.applicant.cover[1].coverOption = 'Cancel your cover';
                                vm.tpdDecOrCancelFlag = true;
                            }
                        }

                    }

                    var eXIpAmount;
                    // Ip cover
                    /*if (vm.ipCoverDetails.type === '1') {*/
                     eXIpAmount = parseInt(vm.ipCoverDetails.units);
                    /*}*/
                    if (parseInt(vm.QPD.applicant.cover[2].additionalUnit) < eXIpAmount) {
                          vm.QPD.applicant.cover[2].coverOption = 'Decrease your cover';
                        vm.ipDecOrCancelFlag = true;
                    } else if (parseInt(vm.QPD.applicant.cover[2].additionalUnit) > eXIpAmount) {
                          vm.QPD.applicant.cover[2].coverOption = 'Increase your cover';
                        vm.QPD.auraDisabled = false;
                    } else if (parseInt(vm.QPD.applicant.cover[2].additionalUnit) === eXIpAmount) {
                          vm.QPD.applicant.cover[2].coverOption = 'No change';
                    } else if (parseInt(vm.QPD.applicant.cover[2].additionalUnit) === 0 && eXIpAmount !== 0) {
                          vm.QPD.applicant.cover[2].coverOption = 'Cancel your cover';
                        vm.ipDecOrCancelFlag = true;
                    }

                    if (vm.ipIncreaseFlag) {
                        vm.QPD.auraDisabled = false;
                    }
                    // ends here
                    
                    	 vm.calculateOnChange();
                   
                       
                //}
            };

            vm.canValidateDeathTpd = function() {


                if ((vm.deathCoverDetails.type === '2' && vm.tpdCoverDetails.type === '2') && (vm.QPD.applicant.cover[0].coverCategory === 'DcUnitised' && vm.QPD.applicant.cover[1].coverCategory === 'TPDUnitised')) {

                    if (vm.QPD.applicant.cover[0].coverCategory === 'DcUnitised') {
                        if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) < parseInt(vm.deathUnitsForFixed)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) > parseInt(vm.deathUnitsForFixed)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) === 0 && parseInt(vm.deathUnitsForFixed !== 0)) {
                            return true;
                        }
                    }

                    // Tpd cover

                    if (vm.QPD.applicant.cover[1].coverCategory === 'TPDUnitised') {
                        if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) < parseInt(vm.tpdUnitsForFixed)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) > parseInt(vm.tpdUnitsForFixed)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) === 0 && parseInt(vm.tpdUnitsForFixed !== 0)) {
                            return true;
                        }
                    }
                } else {

                    // Death cover
                    var eXDcAmount = Math.ceil(parseInt(vm.deathCoverDetails.amount) / 1000) * 1000;
                    if (vm.QPD.applicant.cover[0].coverCategory === 'DcFixed') {
                        if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) < eXDcAmount) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) > eXDcAmount) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount) === 0 && parseInt(vm.deathCoverDetails.amount) !== 0) {
                            return true;
                        }
                    } else if (vm.QPD.applicant.cover[0].coverCategory === 'DcUnitised') {
                        if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) < parseInt(vm.deathCoverDetails.units)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) > parseInt(vm.deathCoverDetails.units)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[0].additionalUnit) === 0 && parseInt(vm.deathCoverDetails.units !== 0)) {
                            return true;
                        }
                    }

                    // Tpd cover
                    var eXTpdAmount = Math.ceil(parseInt(vm.tpdCoverDetails.amount) / 1000) * 1000;
                    if (vm.QPD.applicant.cover[1].coverCategory === 'TPDFixed') {
                        if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) < eXTpdAmount) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) > eXTpdAmount) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount) === 0 && eXTpdAmount !== 0) {
                            return true;
                        }
                    } else if (vm.QPD.applicant.cover[1].coverCategory === 'TPDUnitised') {
                        if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) < parseInt(vm.tpdCoverDetails.units)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) > parseInt(vm.tpdCoverDetails.units)) {
                            return true;
                        } else if (parseInt(vm.QPD.applicant.cover[1].additionalUnit) === 0 && parseInt(vm.tpdCoverDetails.units !== 0)) {
                            return true;
                        }
                    }
                }
            };


            vm.quoteSaveAndExitPopUp = function(hhText) {
                var dialog1 = ngDialog.open({
                    template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'>Application saved</h4></div><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom \'> '+ 
                    ' <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>' + hhText + 
                    '</div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Finish &amp; Close Window </button></div></div>',
                    className: 'ngdialog-theme-plain custom-width',
                    preCloseCallback: function(value) {
                        var url = '/landing';
                        $location.path(url);
                        return true;
                    },
                    plain: true
                });
                dialog1.closePromise.then(function(data) {
                    console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
                });
            };

            vm.saveQuote = function() {
                vm.QPD.lastSavedOn = 'Quotepage';
                $rootScope.$broadcast('disablepointer');
                var selectedIndustry = vm.IndustryOptions.filter(function(obj) {
                    return vm.QPD.applicant.industryType === obj.key;
                });
                vm.QPD.applicant.occupationCode = selectedIndustry[0].value;//Doubt
                vm.QPD.applicant.industryType = selectedIndustry[0].key;
                vm.QPD.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
                vm.validateDeathTpdIpAmounts();
                saveEapplyData.reqObj(vm.urlList.saveEapplyUrlNew, vm.QPD).then(function(response) {
                    console.log(response.data);
                    vm.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>' + vm.QPD.applicatioNumber + '</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR>');
                });
            };

            vm.goToAura = function() {
            	this.ipCoverMax = true;
                this.formOne.$submitted = true;
                this.occupationForm.$submitted = true;
                this.coverCalculatorForm.$submitted = true;
                if($stateParams.mode !== '2' && $stateParams.mode !== '3'){
                	vm.validateDeathTpdIpAmounts();
                }
                
                if (this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
                    if (!vm.deathErrorFlag && !vm.tpdErrorFlag && !vm.ipErrorFlag && !vm.eligibleFrStIpFlag) {
                        if (vm.deathDecOrCancelFlag || vm.tpdDecOrCancelFlag || vm.ipDecOrCancelFlag) {
                            vm.decOrCancelCovers();
                        } else {
                            vm.goTo();
                        }
                    } else {
                        return false;
                    }
                }
            };

            vm.goTo = function() {
                if (!vm.QPD.auraDisabled) {
                    $timeout(function() {
                        vm.continueToNextPage('/aura');
                     }, 10);
                } else {
                    $timeout(function() {
                        vm.continueToNextPage('/summary');
                    }, 10);
                }
            };
            vm.continueToNextPage = function(path) {
                if (this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
                    var selectedIndustry = vm.IndustryOptions.filter(function(obj) {
                        return vm.QPD.applicant.industryType === obj.key;
                    });
                    vm.QPD.applicant.occupationCode = selectedIndustry[0].value;
                    vm.QPD.applicant.industryType = selectedIndustry[0].key;
                    vm.QPD.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
                    appData.setAppData(vm.QPD);
                    $location.path(path);
                }
            };

             vm.generatePDF = function() {
                $rootScope.$broadcast('disablepointer');
                var selectedIndustry = vm.IndustryOptions.filter(function(obj) {
                    return vm.QPD.applicant.industryType === obj.key;
                });
                vm.QPD.applicant.occupationCode = selectedIndustry[0].value;
                vm.QPD.applicant.occupationCode  = selectedIndustry[0].value;
                vm.QPD.applicant.industryType = selectedIndustry[0].key;
                vm.QPD.applicatioNumber = parseInt(fetchAppNumberSvc.getAppNumber());
                vm.QPD = angular.extend(vm.QPD, vm.inputDetails);
                auraRespSvc.setResponse(vm.QPD);
                printPageSvc.reqObj(vm.urlList.printQuotePageNew).then(function(response) {
                    appData.setPDFLocation(response.data.clientPDFLocation);
                    vm.downloadPDF();
                }, function(err) {
                    $rootScope.$broadcast('enablepointer');
                    console.info('Something went wrong while generating pdf...' + JSON.stringify(err));
                });
            };

             vm.downloadPDF = function() {
                var pdfLocation = null;
                var filename = null;
                var a = null;
                pdfLocation = appData.getPDFLocation();
                console.log(pdfLocation + 'pdfLocation');
                filename = pdfLocation.substring(pdfLocation.lastIndexOf('/') + 1);
                a = document.createElement('a');
                document.body.appendChild(a);
                DownloadPDFSvc.download(vm.urlList.downloadUrl, pdfLocation).then(function(res) {
                    if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
                        window.navigator.msSaveBlob(res.data.response, filename);
                    } else {
                        var fileURL = URL.createObjectURL(res.data.response);
                        a.href = fileURL;
                        a.download = filename;
                        a.click();
                    }
                    $rootScope.$broadcast('enablepointer');
                }, function(err) {
                    console.log('Error downloading the PDF ' + err);
                    $rootScope.$broadcast('enablepointer');
                });
            };

            // added for Decrease or Cancel cover popup 'on continue' after calculate quote
            vm.showDecreaseOrCancelPopUp = function(val) {
                if (this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
                    if (!vm.deathErrorFlag && !vm.tpdErrorFlag && !vm.ipErrorFlag && !vm.eligibleFrStIpFlag) {
                        if (val === null || val === '' || val === ' ') {
                            vm.hideTips();
                        } else {
                            vm.ackFlag = false;
                            document.getElementById('mymodalDecCancel').style.display = 'block';
                            document.getElementById('mymodalDecCancelFade').style.display = 'block';
                            document.getElementById('decCancelMsg_text').innerHTML = val;

                        }
                    } else {
                        return false;
                    }
                }
            };
            vm.hideTips = function() {
                if (document.getElementById('help_div')) {
                    document.getElementById('help_div').style.display = 'none';
                }
            };
            
            vm.isUnitized = function(value){
    	    	return (value === '1');
    	    };

            vm.decOrCancelCovers = function() {
                if (vm.deathDecOrCancelFlag === true && vm.tpdDecOrCancelFlag === true && vm.ipDecOrCancelFlag === true) {
                    vm.decCancelCover = 'Death, TPD & IP';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else if (vm.deathDecOrCancelFlag === true && vm.tpdDecOrCancelFlag === true) {
                    vm.decCancelCover = 'Death & TPD';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else if (vm.deathDecOrCancelFlag && vm.ipDecOrCancelFlag) {
                    vm.decCancelCover = 'Death & IP';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else if (vm.tpdDecOrCancelFlag === true && vm.ipDecOrCancelFlag === true) {
                    vm.decCancelCover = 'TPD & IP';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else if (vm.deathDecOrCancelFlag === true) {
                    vm.decCancelCover = 'Death';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else if (vm.tpdDecOrCancelFlag === true) {
                    vm.decCancelCover = 'TPD';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else if (vm.ipDecOrCancelFlag === true) {
                    vm.decCancelCover = 'IP';
                    vm.decCancelMsg = 'You are requesting to cancel/decrease your ' + vm.decCancelCover + ' cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?';
                } else {
                    vm.decCancelMsg = 'You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.';
                }
                vm.showDecreaseOrCancelPopUp(vm.decCancelMsg);
            };

            vm.calculateForUnits = function() {
                var ruleModel = {
                    'age': vm.QPD.applicant.age,
                    'fundCode': 'MTAA',
                    'gender': vm.QPD.applicant.gender,
                    'deathOccCategory': vm.QPD.applicant.cover[0].occupationRating,
                    'tpdOccCategory': vm.QPD.applicant.cover[1].occupationRating,
                    'ipOccCategory': vm.QPD.applicant.cover[2].occupationRating,
                    'smoker': false,
                    'deathUnits': 1,
                    'deathFixedAmount': parseInt(vm.QPD.applicant.cover[0].additionalCoverAmount),
                    'deathFixedCost': null,
                    'deathUnitsCost': null,
                    'tpdUnits': 1,
                    'tpdFixedAmount': parseInt(vm.QPD.applicant.cover[1].additionalCoverAmount),
                    'tpdFixedCost': null,
                    'tpdUnitsCost': null,
                    'ipUnits': parseInt(vm.QPD.applicant.cover[2].additionalUnit),
                    'ipFixedAmount': parseInt(vm.QPD.applicant.cover[2].additionalUnit),
                    'ipFixedCost': null,
                    'ipUnitsCost': null,
                    'premiumFrequency': vm.QPD.applicant.cover[0].frequencyCostType,
                    'memberType': null,
                    'manageType': 'CCOVER',
                    'deathCoverType': 'DcUnitised',
                    'tpdCoverType': 'TPDUnitised',
                    'ipCoverType': 'IpUnitised',
                    'ipWaitingPeriod': vm.QPD.applicant.cover[2].existingIpWaitingPeriod,
                    'ipBenefitPeriod': vm.QPD.applicant.cover[2].existingIpBenefitPeriod
                };
                CalculateService.calculate(ruleModel, vm.urlList.calculateUrl).then(function(res) {
                    var coverAmtPerUnit = 0;
                    var premium = res.data;

                    for (var i = 0; i < premium.length; i++) {
                        if (premium[i].coverType === 'DcUnitised') {
                            coverAmtPerUnit = premium[i].coverAmount || 0;
                            if (coverAmtPerUnit > 0) {
                                vm.deathUnitsForFixed = Math.ceil(vm.deathCoverDetails.amount / coverAmtPerUnit);
                            } else {
                                vm.deathUnitsForFixed = 0;
                            }
                        } else if (premium[i].coverType === 'TPDUnitised') {
                            coverAmtPerUnit = premium[i].coverAmount || 0;
                            if (coverAmtPerUnit > 0) {
                                vm.tpdUnitsForFixed = Math.ceil(vm.tpdCoverDetails.amount / coverAmtPerUnit);
                            } else {
                                vm.tpdUnitsForFixed = 0;
                            }
                        }
                    }
                    if (vm.QPD.applicant.cover[0].additionalUnit === vm.deathUnitsForFixedOld) {
                          var deathOld=!vm.deathUnitsForFixedOld;
                        if (deathOld === vm.deathUnitsForFixed) {
                            vm.QPD.applicant.cover[0].additionalUnit = vm.deathUnitsForFixed;
                        }
                    }
                    if (vm.QPD.applicant.cover[1].additionalUnit === vm.tpdUnitsForFixedOld) {
                          var tpdOld=!vm.tpdUnitsForFixedOld;
                        if (tpdOld === vm.tpdUnitsForFixed) {
                           vm.QPD.applicant.cover[1].additionalUnit = vm.tpdUnitsForFixed;
                        }
                    }
                }, function(err) {
                    console.info('Something went wrong while calculating...' + JSON.stringify(err));
                });
            };

        }
    
})(angular);
