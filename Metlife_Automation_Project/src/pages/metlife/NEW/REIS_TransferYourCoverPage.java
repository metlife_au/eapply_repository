package pages.metlife.NEW;


import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.REIS_TransferYourCoverPageObjects;


public class REIS_TransferYourCoverPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public REIS_TransferYourCoverPage REIStransfercover() {
		TransferCoverREIS();
		HealthandLifestyle();
		DeclarationandConfirmation();
		
		return new REIS_TransferYourCoverPage(scriptHelper);
	}

	public REIS_TransferYourCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void TransferCoverREIS() {

		String FifteenHours = dataTable.getData("REIS_TransferYourCover", "FifteenHours");
		
		String IndustryType = dataTable.getData("REIS_TransferYourCover", "IndustryType");		
		String OccupationType = dataTable.getData("REIS_TransferYourCover", "OccupationType");
		String AnnualSalary = dataTable.getData("REIS_TransferYourCover", "AnnualSalary");
		String PreviousInsurer = dataTable.getData("REIS_TransferYourCover", "PreviousInsurer");
		String PolicyNumber = dataTable.getData("REIS_TransferYourCover", "PolicyNumber");
		String SPINNumber = dataTable.getData("REIS_TransferYourCover", "SPINNumber");
		String PremiumFrequency = dataTable.getData("REIS_TransferYourCover", "PremiumFrequency");
		String DeathYN = dataTable.getData("REIS_TransferYourCover", "DeathYN");
		String TPDYN = dataTable.getData("REIS_TransferYourCover", "TPDYN");
		String IPYN = dataTable.getData("REIS_TransferYourCover", "IPYN");
		String DeathAmount = dataTable.getData("REIS_TransferYourCover", "DeathAmount");
		String TPDAmount = dataTable.getData("REIS_TransferYourCover", "TPDAmount");
		String IPAmount = dataTable.getData("REIS_TransferYourCover", "IPAmount");
		String WaitingPeriod = dataTable.getData("REIS_TransferYourCover", "WaitingPeriod");
		String BenefitPeriod = dataTable.getData("REIS_TransferYourCover", "BenefitPeriod");

		
		try{
			
	          
			//****Click on Transfer Your Cover Link*****\\
			
	tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.TransferYourCoverLink), "Click", "Tranfer Your Cover Link");
		
		//*****Enter Fifteen hours question*****\\
			
			if(FifteenHours.equalsIgnoreCase("Yes")){
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.FifteenHoursYes), "Click", "Work more than 15 Hours-Yes");
				
			}
			else{						
				
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.FifteenHoursNo), "Click", "15 Hours a Week-No");			
							
				}			
							
							
				//*****Click on Industry********\\			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.Industry), "DropDownSelect", "Industry",IndustryType);			
				sleep(800);			
							
							
				//*****Click On  Occupation*****\\			
							
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.Occupation), "DropDownSelect", "Occupation",OccupationType);						
	        						
	          						
	            //*****Annual Salary*****\\						
				sleep(500);			
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.AnnualSalary), "SET", "Annual Salary",AnnualSalary);						
	        	
	            //****What is the name of your previous fund or insurer?*********\\
	           
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PreviousInsurer), "SET", "Previous Insurer",PreviousInsurer);						
	            
	            //*****please enter your fund member or insurance policy number******\\
	           
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PolicyNumber), "SET", "Policy Number",PolicyNumber);						
	            
	            //*****Please enter your former fund SPIN Number******\\
	           
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.SPINNumber), "SET", "SPIN Number",SPINNumber);
	            
	            //*****Display My cost of insurance cover as*****\\
	            
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PremiumFrequency), "DropDownSelect", "Premium Frequency",PremiumFrequency);			
	          						
				//****Enter the Death Cover amount*****\\		
	            if(DeathYN.equalsIgnoreCase("Yes")){
	            						
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.DeathAmount), "SET", "Transfer Death Amount",DeathAmount);
	            sleep(300);
	            }						
	            //*****Enter the TPD Cover amount*****\\						
	            if(TPDYN.equalsIgnoreCase("Yes")){						
	          						
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.TPDAmount), "SET", "Transfer TPD Amount",TPDAmount);
	            sleep(300);
	            						
	            }					
	            //******Enter the IP Cover amount******						
	            if(IPYN.equalsIgnoreCase("Yes")&&FifteenHours.equalsIgnoreCase("Yes")){					
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.IPAmount), "SET", "Transfer IP Amount",IPAmount);						
	            						
	           						
	            //*****Waiting period******\\						
	            						
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.WaitingPeriod), "DropDownSelect", "Waiting Period",WaitingPeriod);						
	            						
	           						
	            //******Benefit period******\\						
	            						
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.BenefitPeriod), "DropDownSelect", "Benefit Period",BenefitPeriod);						
	            }						
	           						
	            //*****Calculate Quote******\\						
	            						
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.CalculateQuote), "Click", "Calculate Quote");						
	            						
	         						
							
	            //****Fetching the Death Cost*****\\						
	            sleep(1500);						
	            String DeathPremium=waitForClickableElement(REIS_TransferYourCoverPageObjects.FetchDeathAmt).getText();						
	            report.updateTestLog("Death Cost", "Estimated Death prmeium is: "+DeathPremium , Status.SCREENSHOT);						
	            						
	          						
	            //*****Fetching the TPD cost******\\						
	            						
	            String TPDPremium=waitForClickableElement(REIS_TransferYourCoverPageObjects.FetchTPDAmt).getText();						
	            report.updateTestLog("Death Cost", "Estimated TPD prmeium is: "+TPDPremium , Status.SCREENSHOT);						
	            						
	           						
	            //****Fetching the IP cost*****\\						
	            						
	            String IPPremium=waitForClickableElement(REIS_TransferYourCoverPageObjects.FetchIPAmt).getText();						
	            report.updateTestLog("Death Cost", "Estimated IP prmeium is: "+IPPremium , Status.SCREENSHOT);						
	            						
	           						
	           					
	           						
	            //*****Continue Button*****						
	            						
	            tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.ContinueafterCoverdetails), "Click", "Apply");						
	            sleep(2000);						
							
							
				report.updateTestLog("Cover Details Page", "Sucessfully automated cover details section", Status.PASS);			
							
							
							
				}catch(Exception e){			
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);		
				}			
			}				
							
	private void HealthandLifestyle() {						
							
		String Restrictedillness = dataTable.getData("REIS_TransferYourCover", "Restrictedillness");
		String PriorClaim = dataTable.getData("REIS_TransferYourCover", "PriorClaim");
		String RestrictedFromWork = dataTable.getData("REIS_TransferYourCover", "RestrictedFromWork");					
		String Diagnosedillness = dataTable.getData("REIS_TransferYourCover", "Diagnosedillness");	
		String MedicalTreatment = dataTable.getData("REIS_TransferYourCover", "MedicalTreatment");	
		String DeclinedApplication = dataTable.getData("REIS_TransferYourCover", "DeclinedApplication");
		String PremFundLoading = dataTable.getData("REIS_TransferYourCover", "PremFundLoading");
		String PremLoadingDetails = dataTable.getData("REIS_TransferYourCover", "PremLoadingDetails");
							
							
							
			try{				
							
				//******Are you restricted, due to injury or illness, from carrying out the identifiable duties 			
				//of your current and normal occupation on a full-time basis (even if you are not currently working			
				//on a full-time basis)? Full-time basis is considered to be at least 30 hours per week.			
							
				if(Restrictedillness.equalsIgnoreCase("Yes")){			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.RestrictedillnessYes), "Click", "Restrictedillness-Yes");			
				}			
				else{			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.RestrictedillnessNo), "Click", "Restrictedillness-No");			
				}		
				
				//******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits, Workers� Compensation
				//or any other form of compensation due to illness or injury?
				
				if(PriorClaim.equalsIgnoreCase("Yes")){			
					
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PriorClaimYes), "Click", "PriorClaim-Yes");			
				}			
				else{			
								
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PriorClaimNo), "Click", "PriorClaim-No");			
					}	
							
				//*****Have you been restricted from work or unable to perform any of your regular duties for more than seven 
				//consecutive days over the past 12 months due to illness or injury (other than for colds or flu)?		
							
				if(RestrictedFromWork.equalsIgnoreCase("Yes")){			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.RestrictedFromWorkYes), "Click", "RestrictedFromWork-Yes");			
					}		
				else{			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.RestrictedFromWorkNo), "Click", "RestrictedFromWork-No");			
					}		
							
							
							
				//******Have you been diagnosed with an illness that reduces your life expectancy to less than 12 months			
				//from the date of this application?			
							
				if(Diagnosedillness.equalsIgnoreCase("Yes")){			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.DiagnosedillnessYes), "Click", "Diagnosedillness-Yes");			
						}	
					else{		
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.DiagnosedillnessNo), "Click", "Diagnosedillness-No");			
						}	
				
				//*****Are you currently contemplating any medical treatment or advice for any illness or injury for which you have not
				//previously consulted a medical practitioner or an existing illness or injury, which appears to be deteriorating?		
							
				if(MedicalTreatment.equalsIgnoreCase("Yes")){			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.MedicalTreatmentYes), "Click", "MedicalTreatment-Yes");			
						}	
					else{		
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.MedicalTreatmentNo), "Click", "MedicalTreatment-No");			
						}	
				
				//*******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\
				
				if(DeclinedApplication.equalsIgnoreCase("Yes")){			
					
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.DeclinedApplicationYes), "Click", "DeclinedApplication-Yes");			
							}	
					else{		
								
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.DeclinedApplicationNo), "Click", "DeclinedApplication-No");			
							}	
							
							
				//*****Is your cover under the former insurer subject to any premium loadings and/or exclusions, including but not 			
				//limited to pre-existing condition exclusions, or restrictions in regards to medical or other conditions?			
							
							
				if(PremFundLoading.equalsIgnoreCase("Yes")){			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PremFundLoadingYes), "Click", "PremFundLoading-Yes");
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PremLoadDetails), "SET", "Premium Loading details",PremLoadingDetails);
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.EnterPremLoading), "Click", "Enter Loading");
				
						}	
					else{		
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PremFundLoadingNo), "Click", "PremFundLoading-No");			
						}	
							
				//******Click on Submit Button******\\			
							
				tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.SubmitHealthPage), "Click", "Continue");
				sleep(1500);
							
							
				report.updateTestLog("Health and lifestyle Page", "Sucessfully automated Health and Lifestyle section", Status.PASS);			
							
							
							
			}catch(Exception e){				
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);			
			}				
		}					
							
	private void DeclarationandConfirmation() {						
		
		String Attachment = dataTable.getData("REIS_TransferYourCover", "Attachment");
		String AttachPath = dataTable.getData("REIS_TransferYourCover", "AttachPath");					
		String PreferredContact = dataTable.getData("REIS_TransferYourCover", "PreferredContact");					
		String PreferredTime = dataTable.getData("REIS_TransferYourCover", "PreferredTime");					
		String ContactNumber = dataTable.getData("REIS_TransferYourCover", "ContactNumber");	
		String EmailAddress = dataTable.getData("REIS_TransferYourCover", "EmailAddress");
							
							
							
			try{				
							
				if(driver.getPageSource().contains("Transfer Cover - Declaration and Confirmation of Cover")){
							
		//*****Evidence of existing cover*****\\	
					
		if(Attachment.equalsIgnoreCase("Yes")){
					
			//****Attach the file******		
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,550)", ""); 
		sleep(500);
		driver.findElement(By.id("extraDocFileUploadAttach")).sendKeys(AttachPath);
		sleep(1000);
					
		//*****Let the attachment load*****\\	
		driver.findElement(By.xpath("//h2[contains(text(),'Evidence of Existing Cover')]")).click();
		sleep(500);
					
		//*****Add the Attachment*****\\
					
		WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
		sleep(500);
		}
		else{
							
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.EvidenceofCoverchkbox), "Click", "Evidence of Cover");					
		}				
		//******Declaration Checkbox*******\\					
							
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.Declarationchkbox), "Click", "Declaration checkbox");					
							
		//******Privacy Statement *******\\					
		
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PrivacyStatement), "Click", "Privacy Statement");					
				
		//******General Consent*******\\					
				
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.GeneralConsent), "Click", "General Consent");					
		//******Preferred Contact*****\\					
							
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PreferredContact), "DropDownSelect", "Preferred Contact",PreferredContact);					
							
		//*****Preferred Time*****\\					
							
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.PreferredTime), "DropDownSelect", "Preferred Time",PreferredTime);					
							
		//********Contact Number******\\					
							
		waitForClickableElement(REIS_TransferYourCoverPageObjects.ContactNumber).sendKeys(Keys.chord(Keys.CONTROL, "a"));					
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.ContactNumber), "SET", "Contact Number",ContactNumber);
		
		//*****Email Address*****\\
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.EmailAddress), "SET", "Email Address",EmailAddress);
							
		//*****Submit*****\\					
							
		tryAction(waitForClickableElement(REIS_TransferYourCoverPageObjects.SubmitDeclaration), "Click", "Submit");					
		sleep(2000);	
				}
		
		//*******Feedback Pop-up******\\
        WebDriverWait wait=new WebDriverWait(driver,18);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'No')]"))).click();
		report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
		sleep(1000);
		
		if(driver.getPageSource().contains("REFERRED TO ADMINISTRATOR")){
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//font[@color='#800080']"))).click();
			 report.updateTestLog("PDF", "PDF File Downloaded",Status.SCREENSHOT);
			sleep(1000);
		}
		else{
			 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
		}
						
							
							
							
							
							
		}catch(Exception e){					
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);				
		}					
	}						
	}						
							
