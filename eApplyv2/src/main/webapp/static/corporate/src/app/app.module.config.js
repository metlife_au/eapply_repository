(function (angular) {
  'use strict';
  angular
    .module('corp.ui.constants', [])
    .constant('APP_CONSTANTS', (function () {
      var basePath = 'static/corporate/src/';
      return {
        'path': {
          'base': basePath,
          'modules': basePath + 'app/modules/',
          'uiComponents': basePath + 'app/ui-components/',
          'images': basePath + '/assets/images/'
        },
        'navUrls': {
					'logoUrl': 'https://www.metlife.com.au/',
          'about': 'https://www.metlife.com.au/about',
          'contactUs': 'https://www.metlife.com.au/support/contact-us',
          'privacyUrl': 'https://www.metlife.com.au/privacy',
          'legalNoticeUrl':'https://www.metlife.com.au/legal-notices/'
        },
        'onboardDetails': {
					'phoneNumber': /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/,
					'dateFormat': /^(?:0?[1-9]|[12][0-9]|3[01])\/(?:0?[1-9]|1[0-2])\/(?:(?:19|20)[0-9]{2})$/,
          'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          'postCodeFormat': /^d{4}$/,
          'waitingPeriodOptions': ['30 Days','60 Days','90 Days', '180 Days'],
          'benefitPeriodOptions': ['2 Years', '5 Years', 'Age 65'],
          'titleOption':[{
      	    text: 'Mr',
      	    value: 'Mr'
      	  },{
      	    text: 'Mrs',
      	    value: 'Mrs'
      	  },{
      	    text: 'Ms',
      	    value: 'Ms'
      	  },{
      	    text: 'Dr',
      	    value: 'Dr'
      	  },{
      		 text: 'Miss',
          	 value: 'Miss'
          },{
      		 text: 'Sir',
          	 value: 'Sir'
          },{
      		 text: 'Madam',
          	 value: 'Madam'
          }
      	  ],
          'contactTypeOption':[{
        	    text: 'Home',
        	    value: '2'
        	  },{
        	    text: 'Work',
        	    value: '3'
        	  },{
        	    text: 'Mobile',
        	    value: '1'
          }],
    	  'addressTypeOption':[{
      	    text: 'Home',
      	    value: '2'
      	  },{
      	    text: 'Work',
      	    value: '3'
      	  }],
      	  'stateOptions':[{key:'NSW', value: 'NSW'}, {key:'VIC', value: 'VIC'}, {key:'QLD', value: 'QLD'}, {key:'SA', value: 'SA'}, {key:'WA', value: 'WA'}, {key:'ACT', value: 'ACT'}, {key:'TAS', value: 'TAS'}, {key:'NT', value: 'NT'}, {key:'OTH', value: 'OTHER'}]
        }
      };
    })());
})(angular);