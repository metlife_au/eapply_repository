package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class NSFS_UpdateYourOccupationPageObjects extends PageObjectLibrary{
		
	public static final By UpdateOccupationLink = getObjectIdentifaction("NSFS.UpdateOccupation.LInk.xpath");
	
	public static final By  EmailId = getObjectIdentifaction("NSFS.UpdateOccupation.Email.id");	
	public static final By  ContactTypeClick = getObjectIdentifaction("NSFS.UpdateOccupation.ContactTypeClick.xpath");
	public static final By  ContactNumber = getObjectIdentifaction("NSFS.UpdateOccupation.ContactNumber.id");
	public static final By  MorningContact = getObjectIdentifaction("NSFS.UpdateOccupation.MorningContact.id");
	public static final By  AfterNoonContact = getObjectIdentifaction("NSFS.UpdateOccupation.AfterNoonContact.id");
	public static final By  ContactDetailsContinue = getObjectIdentifaction("NSFS.UpdateOccupation.ContactDetailsContinue.xpath");
	
	public static final By  CitizenYes = getObjectIdentifaction("NSFS.UpdateOccupation.CitizenYes.id");
	public static final By  CitizenNo = getObjectIdentifaction("NSFS.UpdateOccupation.CitizenNo.id");
	public static final By  ClickIndustry = getObjectIdentifaction("NSFS.UpdateOccupation.ClickIndustry.xpath");
	public static final By  ClickOccupation = getObjectIdentifaction("NSFS.UpdateOccupation.ClickOccupation.xpath");
	public static final By  OtherOccupation = getObjectIdentifaction("NSFS.UpdateOccupation.OtherOccupation.id");
	public static final By  OfficeEnvironmentYes = getObjectIdentifaction("NSFS.UpdateOccupation.OfficeEnvironmentYes.id");
	public static final By  OfficeEnvironmentNo = getObjectIdentifaction("NSFS.UpdateOccupation.OfficeEnvironmentNo.id");
	public static final By  TertiaryQualYes = getObjectIdentifaction("NSFS.UpdateOccupation.TertiaryQualYes.id");
	public static final By  TertiaryQualNo = getObjectIdentifaction("NSFS.UpdateOccupation.TertiaryQualNo.id");
	public static final By  HazardousEncYes = getObjectIdentifaction("NSFS.UpdateOccupation.HazardousEncYes.id");
	public static final By  HazardousEncNo = getObjectIdentifaction("NSFS.UpdateOccupation.HazardousEncNo.id");
	public static final By  OutsideOfcPercentYes = getObjectIdentifaction("NSFS.UpdateOccupation.OutsideOfcPercentYes.id");
	public static final By  OutsideOfcPercentNo = getObjectIdentifaction("NSFS.UpdateOccupation.OutsideOfcPercentNo.id");
	public static final By  AnnualSalary = getObjectIdentifaction("NSFS.UpdateOccupation.AnnualSalary.id");
	public static final By  LoadSalaryLabel = getObjectIdentifaction("NSFS.UpdateOccupation.LoadSalaryLabel.xpath");
	public static final By  ContinueOccupationDetails = getObjectIdentifaction("NSFS.UpdateOccupation.ContinueOccupationDetails.xpath");

}
