package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesConfirmPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesConfirmPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	

	public ColesConfirmPage enterColesPersonalDetails() {
		fillPersonalDetails();
		return new ColesConfirmPage(scriptHelper);
	} 

	public ColesConfirmPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillPersonalDetails() {
		String title = dataTable.getData("MetLife_Data", "Title");
		String lastName = dataTable.getData("MetLife_Data", "LastName");
		String contactnumber = dataTable.getData("MetLife_Data",
				"PreferredNumber");
		String email = dataTable.getData("MetLife_Data", "Email");
		String unitno = dataTable.getData("MetLife_Data", "UnitNo");
		String streetno = dataTable.getData("MetLife_Data", "StreetNo");
		String streetname = dataTable.getData("MetLife_Data", "StreetName");
		String suburb = dataTable.getData("MetLife_Data", "Suburb");
		String state = dataTable.getData("MetLife_Data", "State");
		String postcode = dataTable.getData("MetLife_Data", "PostCode");
		try {
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(ColesConfirmPageObjects.selTitle),"specialDropdown", "Title", "Mr");

			tryAction(waitForClickableElement(ColesConfirmPageObjects.txtLastName),
					"SET", "Last Name", lastName);

	/*		tryAction(
					waitForClickableElement(ColesConfirmPageObjects.txtPrefContactNum),
					"SET", "Preferred Contact Number", contactnumber);*/

			tryAction(waitForClickableElement(ColesConfirmPageObjects.txtEmail),
					"SET", "Email", email);

			tryAction(
					waitForClickableElement(ColesConfirmPageObjects.txtConfirmEmail),
					"SET", "Confirm Email", email);

			tryAction(waitForClickableElement(ColesConfirmPageObjects.txtUnitNo),
					"SET", "Unit No", unitno);

			tryAction(waitForClickableElement(ColesConfirmPageObjects.txtStreetNo),
					"SET", "Street No", streetno);

			tryAction(
					waitForClickableElement(ColesConfirmPageObjects.txtStreetName),
					"SET", "Street Name", streetname);

			tryAction(waitForClickableElement(ColesConfirmPageObjects.txtSuburb),
					"SET", "Suburb", suburb);

			/*tryAction(waitForClickableElement(ColesConfirmPageObjects.selState),
					"DropDownSelect", "State", state);*/

			tryAction(waitForClickableElement(ColesConfirmPageObjects.txtPostCode),
					"SET", "Postcode", postcode);
			tryAction(
					waitForClickableElement(ColesConfirmPageObjects.chkkDeclarationAgree),
					"Click", "Agree Declaration");
			tryAction(waitForClickableElement(ColesConfirmPageObjects.lnkNext),"MoveToElm","Next");
			sleep();
			tryAction(waitForClickableElement(ColesConfirmPageObjects.lnkNext),"Click", "Next");
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

}
