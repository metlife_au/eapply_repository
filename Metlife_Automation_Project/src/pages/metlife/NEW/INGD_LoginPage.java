/**
 * 
 */
package pages.metlife.NEW;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.INGD_LoginPageObjects;

/**
 * @author Hemnath
 * 
 */
public class INGD_LoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public INGD_LoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public INGD_LoginPage enterINGDlogin() {
		enterlogindetails();
		return new INGD_LoginPage(scriptHelper);
	} 
	
	private void enterlogindetails() {
		String InputString = dataTable.getData("INGD_Login", "InputString");
		
	
		try{
			//***Enter the INput String XML*******\\
			
			//WebDriverWait wait=new WebDriverWait(driver,18);
			//wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(InputString);
		
			tryAction(fluentWaitElements(INGD_LoginPageObjects.INGDXMLInput), "SET", "Input XML Strinf",InputString);
			
			
		//****Click on Proceed to Next button******\\
			
			tryAction(waitForClickableElement(INGD_LoginPageObjects.INGDNextButton), "Click", "Next Button");
			sleep(1500);
			
		//*****Click on New Application******\\
			
			tryAction(waitForClickableElement(INGD_LoginPageObjects.INGDNewApplication), "Click", "New Application");
			sleep(2500);
			
			//*****Switch Window and Maximize*****
			
			quickSwitchWindows();
			sleep(2500);
			
			
			report.updateTestLog("INGD Login", "Sucessfully Logged into the application", Status.PASS);
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
