(function(angular){
'use strict';

angular
    .module('Metlife.guil.uicomponents',['guil.ui.constants'])
    .directive('metlifeGuilHelpBlock', metlifeGuilHelpBlock);

function metlifeGuilHelpBlock(guilUIConstants) {
    var directive = {
        restrict: 'EA',
        templateUrl: guilUIConstants.path.uiComponents + '/metlife-corp-help-block/metlife-corp-help-block.tmpl.html',
        controller: helpBlockCtrl,
        controllerAs: 'helpBlock',
        bindToController: true
    };

    return directive;
}

helpBlockCtrl.$inject = ['$scope', 'guilUIConstants'];

function helpBlockCtrl($scope, guilUIConstants) {
    var helpBlock = this;
    helpBlock.constants = {
        helpContact : '1300 361 477'
    };
}

})(angular);