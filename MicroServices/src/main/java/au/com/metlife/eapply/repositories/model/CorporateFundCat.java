package au.com.metlife.eapply.repositories.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/** 
 *
 *	CREATE TABLE "EAPPDB"."TBL_CORP_FUND_CATG" (
 *			"FUNDCODE" VARCHAR(50) NOT NULL, 
 *			"CATEGORY" INTEGER NOT NULL, 
 *			"DEATH_REQUIRED" CHAR(2) NOT NULL, 
 *			"TPD_REQUIRED" CHAR(2) NOT NULL, 
 *			"IP_REQUIRED" CHAR(2) NOT NULL, 
 *			"DEATH_MIN_AGE" INTEGER, 
 *			"DEATH_MAX_AGE" INTEGER, 
 *			"DEATH_MIN_SUM_INSURED" VARCHAR(500), 
 *			"DEATH_MAX_SUM_INSURED" VARCHAR(500), 
 *			"DEATH_AAL" VARCHAR(500), 
 * 			"GL_POLICYNUMBER" VARCHAR(500), 
 * 			"TPD_MIN_AGE" INTEGER, 
 *			"TPD_MAX_AGE" INTEGER, 
 *			"TPD_MIN_SUM_INSURED" VARCHAR(500), 
 *			"TPD_MAX_SUM_INSURED" VARCHAR(500), 
 *			"TPD_AAL" VARCHAR(500), 
 *			"IP_MIN_AGE" INTEGER, 
 *			"IP_MAX_AGE" INTEGER, 
 *			"IP_MIN_SUM_INSURED" VARCHAR(500), 
 *			"IP_MAX_SUM_INSURED" VARCHAR(500), 
 *			"IP_SUM_INSUREDTYPE" VARCHAR(500), 
 *			"IP_FORMULA" VARCHAR(500), 
 *			"IP_AAL" VARCHAR(500), 
 *			"IP_WP" VARCHAR(50), 
 *			"IP_BP" VARCHAR(50), 
 *			"IP_SCIPOLICYNUM" VARCHAR(500), 
 *			"MOD_USR" VARCHAR(500), 
 *			"MOD_DAT" TIMESTAMP, 
 *			"CREATED_DATE" TIMESTAMP NOT NULL, 
 * 			"ID" Integer
 *		)
 *		ORGANIZE BY ROW
 * 		DATA CAPTURE NONE 
 *		IN "EAPPIDX"
 *		COMPRESS NO;
 *	
 *		ALTER TABLE "EAPPDB"."TBL_CORP_FUND_CATG" ADD CONSTRAINT "CC1511754792187" PRIMARY KEY ("ID");
 *		
 *		COMMENT ON TABLE "EAPPDB"."TBL_CORP_FUND_CATG" IS 'For Corporate Category';
 *
 *
 */
@Entity
@Table(name="TBL_CORP_FUND_CATG", schema="eappdb")
public class CorporateFundCat {
	
		@Id
		@Column(name="ID")
		@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_CORP_FUND_CAT_SEQ")
		@SequenceGenerator(name="TBL_CORP_FUND_CAT_SEQ",sequenceName = "EAPPDB.TBL_CORP_FUND_CAT_SEQ", initialValue=1000, allocationSize=100)
		private Integer id;
	
		@ManyToOne
		@JoinColumn(name="CORP_FUND_ID",nullable = false)
		private CorporateFund corporateFundId;
		
		@Column(name="CATEGORY")
		private Integer category;
		
		@Column(name="DEATH_REQUIRED")
		private Character deathRequired;
		
		@Column(name="TPD_REQUIRED")
		private Character tpdRequired;
		
		@Column(name="IP_REQUIRED")
		private Character ipRequired;
		
		@Column(name="DEATH_MIN_AGE")
		private Integer deathMinAge;
		
		@Column(name="DEATH_MAX_AGE")
		private Integer deathMaxAge;
		
		@Column(name="DEATH_MIN_SUM_INSURED")
		private String deathMinSumIns;
		
		@Column(name="DEATH_MAX_SUM_INSURED")
		private String deathMaxSumIns;
		
		@Column(name="DEATH_AAL")
		private String deathAal;
		
		@Column(name="GL_POLICYNUMBER")
		private String glPolicyNum;
		
		@Column(name="TPD_MIN_AGE")
		private Integer tpdMinAge;
		
		@Column(name="TPD_MAX_AGE")
		private Integer tpdMaxAge;
		
		@Column(name="TPD_MIN_SUM_INSURED")
		private String tpdMinSumIns;
		
		@Column(name="TPD_MAX_SUM_INSURED")
		private String tpdMaxSumIns;
		
		@Column(name="TPD_AAL")
		private String tpdAal;
		
		@Column(name="IP_MIN_AGE")
		private Integer ipMinAge;
		
		@Column(name="IP_MAX_AGE")
		private Integer ipMaxAge;
		
		@Column(name="IP_MIN_SUM_INSURED")
		private String ipMinSumIns;
		
		@Column(name="IP_MAX_SUM_INSURED")
		private String ipMaxSumIns;
		
		@Column(name="IP_SUM_INSUREDTYPE")
		private String ipSumInsType;
		
		@Column(name="IP_FORMULA")
		private String ipFormula;
		
		@Column(name="IP_AAL")
		private String ipAal;
		
		@Column(name="IP_WP")
		private String ipWp;
		
		@Column(name="IP_BP")
		private String ipBp;
		
		@Column(name="IP_SCIPOLICYNUM")
		private String ipSciPolicyNum;
		
		@Column(name="MOD_DAT")
		private Timestamp lastUpdtDate;
		
		@Column(name="MOD_USR")
		private String lastUpdtUser;
		
		@Column(name="CREATED_DATE")
		private Timestamp createdDate;
		
		

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
		
		public CorporateFund getCorporateFundId() {
			return corporateFundId;
		}

		public void setCorporateFundId(CorporateFund corporateFundId) {
			this.corporateFundId = corporateFundId;
		}

		public Integer getCategory() {
			return category;
		}

		

		public void setCategory(Integer category) {
			this.category = category;
		}

		public Character getDeathRequired() {
			return deathRequired;
		}

		public void setDeathRequired(Character deathRequired) {
			this.deathRequired = deathRequired;
		}

		public Character getTpdRequired() {
			return tpdRequired;
		}

		public void setTpdRequired(Character tpdRequired) {
			this.tpdRequired = tpdRequired;
		}

		public Character getIpRequired() {
			return ipRequired;
		}

		public void setIpRequired(Character ipRequired) {
			this.ipRequired = ipRequired;
		}

		public Integer getDeathMinAge() {
			return deathMinAge;
		}

		public void setDeathMinAge(Integer deathMinAge) {
			this.deathMinAge = deathMinAge;
		}

		public Integer getDeathMaxAge() {
			return deathMaxAge;
		}

		public void setDeathMaxAge(Integer deathMaxAge) {
			this.deathMaxAge = deathMaxAge;
		}

		public String getDeathMinSumIns() {
			return deathMinSumIns;
		}

		public void setDeathMinSumIns(String deathMinSumIns) {
			this.deathMinSumIns = deathMinSumIns;
		}

		public String getDeathMaxSumIns() {
			return deathMaxSumIns;
		}

		public void setDeathMaxSumIns(String deathMaxSumIns) {
			this.deathMaxSumIns = deathMaxSumIns;
		}

		public String getDeathAal() {
			return deathAal;
		}

		public void setDeathAal(String deathAal) {
			this.deathAal = deathAal;
		}

		public String getGlPolicyNum() {
			return glPolicyNum;
		}

		public void setGlPolicyNum(String glPolicyNum) {
			this.glPolicyNum = glPolicyNum;
		}

		public Integer getTpdMinAge() {
			return tpdMinAge;
		}

		public void setTpdMinAge(Integer tpdMinAge) {
			this.tpdMinAge = tpdMinAge;
		}

		public Integer getTpdMaxAge() {
			return tpdMaxAge;
		}

		public void setTpdMaxAge(Integer tpdMaxAge) {
			this.tpdMaxAge = tpdMaxAge;
		}

		public String getTpdMinSumIns() {
			return tpdMinSumIns;
		}

		public void setTpdMinSumIns(String tpdMinSumIns) {
			this.tpdMinSumIns = tpdMinSumIns;
		}

		public String getTpdMaxSumIns() {
			return tpdMaxSumIns;
		}

		public void setTpdMaxSumIns(String tpdMaxSumIns) {
			this.tpdMaxSumIns = tpdMaxSumIns;
		}

		public String getTpdAal() {
			return tpdAal;
		}

		public void setTpdAal(String tpdAal) {
			this.tpdAal = tpdAal;
		}

		public Integer getIpMinAge() {
			return ipMinAge;
		}

		public void setIpMinAge(Integer ipMinAge) {
			this.ipMinAge = ipMinAge;
		}

		public Integer getIpMaxAge() {
			return ipMaxAge;
		}

		public void setIpMaxAge(Integer ipMaxAge) {
			this.ipMaxAge = ipMaxAge;
		}

		public String getIpMinSumIns() {
			return ipMinSumIns;
		}

		public void setIpMinSumIns(String ipMinSumIns) {
			this.ipMinSumIns = ipMinSumIns;
		}

		public String getIpMaxSumIns() {
			return ipMaxSumIns;
		}

		public void setIpMaxSumIns(String ipMaxSumIns) {
			this.ipMaxSumIns = ipMaxSumIns;
		}

		public String getIpSumInsType() {
			return ipSumInsType;
		}

		public void setIpSumInsType(String ipSumInsType) {
			this.ipSumInsType = ipSumInsType;
		}

		public String getIpFormula() {
			return ipFormula;
		}

		public void setIpFormula(String ipFormula) {
			this.ipFormula = ipFormula;
		}

		public String getIpAal() {
			return ipAal;
		}

		public void setIpAal(String ipAal) {
			this.ipAal = ipAal;
		}

		public String getIpWp() {
			return ipWp;
		}

		public void setIpWp(String ipWp) {
			this.ipWp = ipWp;
		}

		public String getIpBp() {
			return ipBp;
		}

		public void setIpBp(String ipBp) {
			this.ipBp = ipBp;
		}

		public String getIpSciPolicyNum() {
			return ipSciPolicyNum;
		}

		public void setIpSciPolicyNum(String ipSciPolicyNum) {
			this.ipSciPolicyNum = ipSciPolicyNum;
		}

		public Timestamp getLastUpdtDate() {
			return lastUpdtDate;
		}

		public void setLastUpdtDate(Timestamp lastUpdtDate) {
			this.lastUpdtDate = lastUpdtDate;
		}

		public String getLastUpdtUser() {
			return lastUpdtUser;
		}

		public void setLastUpdtUser(String lastUpdtUser) {
			this.lastUpdtUser = lastUpdtUser;
		}

		public Timestamp getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(Timestamp createdDate) {
			this.createdDate = createdDate;
		}

		@Override
		public String toString() {
			return "CorporateFundCat [id=" + id + ", corporateFundId=" + corporateFundId + ", category=" + category
					+ ", deathRequired=" + deathRequired + ", tpdRequired=" + tpdRequired + ", ipRequired=" + ipRequired
					+ ", deathMinAge=" + deathMinAge + ", deathMaxAge=" + deathMaxAge + ", deathMinSumIns="
					+ deathMinSumIns + ", deathMaxSumIns=" + deathMaxSumIns + ", deathAal=" + deathAal
					+ ", glPolicyNum=" + glPolicyNum + ", tpdMinAge=" + tpdMinAge + ", tpdMaxAge=" + tpdMaxAge
					+ ", tpdMinSumIns=" + tpdMinSumIns + ", tpdMaxSumIns=" + tpdMaxSumIns + ", tpdAal=" + tpdAal
					+ ", ipMinAge=" + ipMinAge + ", ipMaxAge=" + ipMaxAge + ", ipMinSumIns=" + ipMinSumIns
					+ ", ipMaxSumIns=" + ipMaxSumIns + ", ipSumInsType=" + ipSumInsType + ", ipFormula=" + ipFormula
					+ ", ipAal=" + ipAal + ", ipWp=" + ipWp + ", ipBp=" + ipBp + ", ipSciPolicyNum=" + ipSciPolicyNum
					+ ", lastUpdtDate=" + lastUpdtDate + ", lastUpdtUser=" + lastUpdtUser + ", createdDate="
					+ createdDate + "]";
		}

		
		
		
		
	
}
