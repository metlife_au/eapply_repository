package pages.metlife;

import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.STFTConfirmationPageObjects;

public class STFTConfirmationPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTConfirmationPage yourConfirmation() {
		yourConfirmationDetails();
		return new STFTConfirmationPage(scriptHelper);
	}

	public STFTConfirmationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourConfirmationDetails() {

		String title = dataTable.getData("MetLife_Data", "Title");
		String contact = dataTable.getData("MetLife_Data", "ContactNumber");
		String contactOther = dataTable.getData("MetLife_Data", "ContactOther");
		String email = dataTable.getData("MetLife_Data", "Email");
		String streetNumber = dataTable.getData("MetLife_Data", "Street.Number");
		String streetName = dataTable.getData("MetLife_Data", "Street.Name");
		String suburb = dataTable.getData("MetLife_Data", "Suburb");
		String state = dataTable.getData("MetLife_Data", "State");
		String postcode = dataTable.getData("MetLife_Data", "Postcode");
	
		try {
			
			
			tryAction(fluentWaitElement(STFTConfirmationPageObjects.rdowner), "clkradio", "Owner");
			
			tryAction(fluentWaitElement(STFTConfirmationPageObjects.linkTitle), "specialDropdown", "Title", title);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtContact), "SET", "Contact", contact);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtContact), "TAB", "Contact");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtcontactOther), "SET", "Contact", contactOther);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtcontactOther), "TAB", "Contact");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtemail), "SET", "Email", email);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtemail), "TAB", "Email");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtemailconfirm), "SET", "Conafirm Email", email);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtemailconfirm), "TAB", "Confirm Email");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtstreetNum), "SET", "Street Number", streetNumber);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtstreetNum), "TAB", "Street Number");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtstreetName), "SET", "Street Name", streetName);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtstreetName), "TAB", "Street Name");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtSuburb), "SET", "Suburb", suburb);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtSuburb), "TAB", "Suburb");
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtSuburb), "SET", "Suburb", suburb);
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtSuburb), "TAB", "Suburb");
			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(STFTConfirmationPageObjects.btnState), "specialDropdown", "State", state);
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.txtPostcode), "SET", "Post Code", postcode);
			
			tryAction(waitForClickableElement(STFTConfirmationPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTConfirmationPageObjects.btnNext)).click().perform();
			Thread.sleep(1000);
			
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
