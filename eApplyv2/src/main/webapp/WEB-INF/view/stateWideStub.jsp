<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style> html{display : none ; } </style>
<script> if( self == top ) { document.documentElement.style.display = 'block' ; } else { document.write(''); }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="static/statewide/images/favicon.ico"/>
<title>Statewide Super</title>
<link rel="stylesheet" href="https://www.eapplication.metlife.com.au/eApplication/xAdvisorWeb/CMS/Group/SSFS/css/eservicestyle.css" media="screen"  />

<script type="text/javascript">
	function formSubmit(){
		var errorStatus = false;
		if(document.getElementById('input_string_id').value == "" || document.getElementById('input_string_id').value == null ){
			document.getElementById('errorMessage1').innerHTML = "Input String is a mandatory field";
			errorStatus = true;
		}else{
		document.getElementById('HDInputString').value=document.getElementById('input_string_id').value;
		}
		if(!errorStatus){
			document.instform.submit(); 
		}
		
		
	}
function openLegalNoticenew(value)
{
//alert(value);
var sUrl = '/eApplication/faces/xAdvisorWeb/bundles/institutional/login/LegalNotice.jsp?fundID=NSFS';
window.open(sUrl,"Window1","menubar=no,width=1000,height=1200,toolbar=no,resizable=yes,scrollbars=yes");

//alert(value);
// write to window
//Window1.document.writeln(value); 
}
</script>
</head>

<body>
<div class="outerWrap"> 
  <!-- Top navigation container starts -->
  <div class="topNavCont">
    <div class="cornerTopLeft"></div>
    <div class="topNavContent">
      <ul class="topNav">
        <li class="cornerLeft"></li>
        <li> <a href="http://www.statewide.com.au/about_us" target="_new" title="About us"> About us </a> </li>
        <li>|</li>
        <li> <a href="http://www.statewide.com.au/contact_us" target="_new" title="Contact us"> Contact us </a> </li>
        <li class="cornerRight"></li>
      </ul>
    </div>
    <div class="cornerTopRight"></div>
    <!-- Top navigation container ends --> 
  </div>
  
  <!-- Main Container starts -->
  <div class="mainCont"> 
    <!-- Logo container starts -->
    <div class="logoCont">
      <div class="logoLeft"> <a href="#"> <img src="https://www.eapplication.metlife.com.au/eApplication/xAdvisorWeb/CMS/Group/SSFS/images/logo.png" alt="" title="StateWide Super Fund" /></a> </div>
      <div class="logoRight none"> </div>
    </div>
    <!-- Logo container ends --> 
    
    <!-- Content container starts -->
    <div class="contentCont">

      <div class="leftCol">
        <div class="titleCont">
          <h1 class="title">Login</h1>
        </div>
        <h2 class="subTitle">Please enter the following information.</h2>
        <div class="blueLine"></div>
       <form name="instform" id="instform"  action="/eapply/sfpsWebStub">
       <input type="hidden" name=HDInputString id="HDInputString" value=""/>
	   <input type="hidden" name="emailID" id="emailID" value="ravi.reddy@cognizant.com"/>
	   <input type="hidden" name="request_type" id="request_type" value="newBusiness"/>
	   <input type="hidden" name="fund_id" id="fund_id" value="SFPS"/>
	   <input type="hidden" name="stubRespPage_id" id="stubRespPage_id" value="stateWideStubResp.jsp"/>
          <!-- Forms started -->
         			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td valign="top" style="padding-left:5px; padding-right:5px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						style="height: 5px">
						<tr> 
							<td>
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tbody>
									
										<tr class="td_alt_white">
											<td width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><label class="label">Input String</label></td>
											<td><div id="errorMessage1" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:red;font-size:13px;"> </div></td>
										</tr>
										<tr>
											<td colspan="2" width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><textarea class="medium" rows="5" cols="90" id="input_string_id" name="inputString"></textarea></td>
										</tr>
										<tr>
											<td>
											<textarea rows="5" cols="90" ><request><adminPartnerID>SFPS</adminPartnerID><transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID><transType>2</transType><transDate>01/02/2011</transDate><transTime>02:14:04</transTime><fundID>SFPS</fundID><fundEmail>test@test.com</fundEmail><policy><lineOfBusiness>Group</lineOfBusiness><partnerID>SFPS</partnerID><policyNumber>123456</policyNumber><applicant><dateJoined>01/01/2011</dateJoined><welcomeLetterDate>01/01/2010</welcomeLetterDate><clientRefNumber>7</clientRefNumber><applicantRole>1</applicantRole><memberType>Personal</memberType><personalDetails><firstName>SFPS</firstName><lastName>SFPSB</lastName><dateOfBirth>01/11/1958</dateOfBirth><gender>Female</gender><applicantSubType></applicantSubType><smoker>2</smoker><title>Ms</title><priority>1</priority></personalDetails><existingCovers><cover><occRating>Professional</occRating><benefitType>1</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>500000.0</amount><loading>5.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>2</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>500000.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions></cover><cover><occRating>Professional</occRating><benefitType>4</benefitType><type>2</type><cpiOptin>2</cpiOptin><units>0</units><amount>0.0</amount><loading>0.0</loading><exclusions>TEST | another test</exclusions><waitingPeriod></waitingPeriod><benefitPeriod></benefitPeriod></cover></existingCovers><address><addressType>2</addressType><line1>2 Park stt</line1><line2></line2><suburb>Sydney</suburb><state>NSW</state><postCode>2000</postCode><country>Australia</country></address><contactDetails><emailAddress>test@test.com</emailAddress><mobilePhone>0410225656</mobilePhone><homePhone>0292661272</homePhone><workPhone>02926685478</workPhone><prefContact>1</prefContact><prefContactTime>1</prefContactTime></contactDetails></applicant></policy></request>
											</textarea>
											</td>
										</tr>								
									</tbody>
								</table>
								<br />

							</td>
						</tr>

						

					</table> 
					</td>
				</tr>
			</table>
          <div class="btnHolder fright">
             <a href="#" class="submit" onclick="formSubmit();"><span>PROCEED TO NEXT PAGE</span></a>
          </div>
          <!-- Forms ends -->
        </form>

      </div>
     <div class="rightCol"> 
        <!-- Rounded edge box starts -->        
        <div class="boxCont">
          <div class="popupCont">
          	<div class="popupContRight">
                <h2 class="subHead">Customer service</h2>
			    <div class="greyWhite"></div>
			    <span class="icoPhone"></span>
			    <h2 class="tel">1300 65 18 65</h2>
			    <div class="greyWhite"></div>
			    <span class="icoTime"></span>
			    <h3 class="date">All days </h3>
			   <h3 class="time">8:30am to 5.30pm</h3>
			  <p class="greyText">Timings are in AEST</p>
           </div>
          </div>
          <div class="bottomCont">
            <div></div>
          </div>
        </div>
        <!-- Rounded edge box ends --> 
      </div>
      <!-- Content container ends --> 
    </div>
    <!-- Main Container ends --> 
      <div class="footerCont">
	    <ul class="footer">
	      <li><a href="http://www.statewide.com.au/page/view_by_id/15" target="_new" title="Privacy statement">Privacy statement</a></li>
	       <li>|</li> 
	     <li><a href="http://www.statewide.com.au/page/view_by_id/15" target="_new" title="Terms of use">Legal notice</a></li> 
	    </ul>
	    <p class="footeTxt">&#169; Statewide Superannuation Pty Ltd ABN 62 008 099 223 AFSL 243171.</p>
	  </div>
  </div>

  
  <!-- Bottom container starts -->
  <div class="footerCurve">
    <div class="cornerBottomLeft"></div>
    <div class="BottomMid">&nbsp;</div>
    <div class="cornerBottomRight"></div>
    <!-- Bottom container ends --> 
  </div>
</div>
</body>
</html>
