/*Decision Page Controller Starts*/   
CareSuperApp.controller('decision',['$scope', '$location',  function($scope, $location){
    $scope.message = 'Look! I am an about page.';
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	//claimNumService.setClaimNumber($scope.claimNo);
  	  $location.path( path );
  	};
  	
  	$scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
  	
}]); 
/*Decision Page Controller Ends*/