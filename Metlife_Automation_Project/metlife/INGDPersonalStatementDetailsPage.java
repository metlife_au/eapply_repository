/**
 * 
 */
package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ScriptHelper;
import uimap.metlife.PersonalStatementPageObjects;

import com.cognizant.framework.Status;

/**
 * @author sampath
 * 
 */
public class INGDPersonalStatementDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public INGDPersonalStatementDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public INGDPersonalStatementDetailsPage enterINGDPersonalStatementDetails() {
		fillInINGDPersonalStatementDetails();
		return new INGDPersonalStatementDetailsPage(scriptHelper);
	}
	
	public INGDPersonalStatementDetailsPage enterINGDPersonalStatementDets() {
		fillInINGDPersonalStatementDets();
		return new INGDPersonalStatementDetailsPage(scriptHelper);
	}
	
	
	private void fillInInsuranceDetails() {
		
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strReceivedClaim = dataTable.getData("MetLife_Data", "personal.receivedClaim");
		
		try{

			List<WebElement> radInsuranceDetailsNo = null;
			radInsuranceDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsNo);
			List<WebElement> radInsuranceDetailsYes = null;
			radInsuranceDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsYes);
			tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(0)):fluentWaitElement(radInsuranceDetailsNo.get(0)),"Click","Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined? - "+strInsuTPD);
			tryAction(strReceivedClaim.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(1)):fluentWaitElement(radInsuranceDetailsNo.get(1)),"Click","Are you contemplating or have you ever made a claim for or received sickness? - "+strReceivedClaim);
			if(radInsuranceDetailsYes.size()==3)
				tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(2)):fluentWaitElement(radInsuranceDetailsNo.get(2)),"Click","Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund? - " +strInsuTPD);
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInLifeStyleDetails() {
		
		String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
		String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
		String strAlcoholConsu = dataTable.getData("MetLife_Data", "personal.alcohlConsumption");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strAbtHIV = dataTable.getData("MetLife_Data", "personal.HIV");
		
		try{
			
			List<WebElement> radLifeStyleDetailsNo = null;
			radLifeStyleDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsNo);
			List<WebElement> radLifeStyleDetailsYes = null;
			radLifeStyleDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsYes);
	
			tryAction(strTravelPlan.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(0)):fluentWaitElement(radLifeStyleDetailsNo.get(0)),"Click","Do you have firm plans to travel or reside in another country other than NZ?");
			tryAction(strDrugs.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(1)):fluentWaitElement(radLifeStyleDetailsNo.get(1)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you? - "+strDrugs);
			tryAction(strAlcoholConsu.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(2)):fluentWaitElement(radLifeStyleDetailsNo.get(2)),"Click","Have you ever been advised by a health professional to reduce your alcohol consumption? - "+strAlcoholConsu);
			tryAction(strAbtHIV.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(3)):fluentWaitElement(radLifeStyleDetailsNo.get(3)),"Click","Do you have HIV (Human Immunodeficiency Virus) that causes AIDS? - "+strAbtHIV);
			if(strAbtHIV.equalsIgnoreCase("no")){
				String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetailsYes.get(3).getAttribute("name")+"_1_0'][value='No']";
				//Click On No for HIV
				tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
			}
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkSportsNota),"Click","Do you regularly engage in or intend to engage in any of the following hazardous activities?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtAlcoholicDrinksConsumption),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnAlcoholicEnter),"CLICK","Enter");
			
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}

	private void fillInINGDPersonalStatementDetails() {
		
		String strLungDese = dataTable.getData("MetLife_Data", "personal.lung");
		String strLungAsthma = dataTable.getData("MetLife_Data", "personal.asthma");
		String strLungCondition = dataTable.getData("MetLife_Data", "personal.condition");
		String strPregnent = dataTable.getData("MetLife_Data", "personal.prebnent");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strDisclosed = dataTable.getData("MetLife_Data", "personal.disclosed");
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strAsthmaWorsened = dataTable.getData("MetLife_Data", "personal.Asthamaworesen");
		String strHeight = dataTable.getData("MetLife_Data", "personal.height");
		String strHeightUnits = dataTable.getData("MetLife_Data", "personal.heightunits");
		String strWeight = dataTable.getData("MetLife_Data", "personal.weight");
		String strWeightUnits = dataTable.getData("MetLife_Data", "personal.weightunits");
		String strSmoked = dataTable.getData("MetLife_Data", "personal.smoked");
		String strGender = dataTable.getData("MetLife_Data", "applicant2_personalDetails2_gender");
		String strRegVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
		String str3YrDiagnosis = dataTable.getData("MetLife_Data", "last3YearsDiagnosied");
		//String str5YrDiagnosis = dataTable.getData("MetLife_Data", "last5yearsDiagnosis");
		
		String[] array3YrDiagnosis = str3YrDiagnosis.split(",");
		//String[] array5YrDiagnosis = str5YrDiagnosis.split(",");
		
		
		try{
			
			if(strGender.equalsIgnoreCase("female")){
				List<WebElement> radHealthDetailsNo = null;
				radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
				List<WebElement> radHealthDetailsYes = null;
				radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsYes);
				tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
				tryAction(strPregnent.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Are you currently pregnant? - "+strPregnent);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(2)):fluentWaitElement(radHealthDetailsNo.get(2)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}else{
				System.out.println("hitest");
				List<WebElement> radHealthDetailsNo = null;
				radHealthDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsNo);
				List<WebElement> radHealthDetailsYes = null;
				radHealthDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radHealthDeatilsYes);
				tryAction(strSmoked.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(0)):fluentWaitElement(radHealthDetailsNo.get(0)),"Click","Have you smoked in the past 12 months? "+strSmoked);
				tryAction(strRegVisit.equalsIgnoreCase("yes")?fluentWaitElement(radHealthDetailsYes.get(1)):fluentWaitElement(radHealthDetailsNo.get(1)),"Click","Do you have a usual doctor or medical centre you regularly visit? - "+strRegVisit);
			}
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtHeight),"SET","Height",strHeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstHeightUnits),"DropDownSelect","Height Units",strHeightUnits);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.txtWeight),"SET","Weight",strWeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstWeightUnits),"DropDownSelect","Weight Units",strWeightUnits);
			System.out.println("length"+array3YrDiagnosis.length);
			for(int i=0;i<array3YrDiagnosis.length;i++){
				System.out.println("array3YrDiagnosis[i] : "+array3YrDiagnosis[i]);
				if(array3YrDiagnosis[i].equalsIgnoreCase("Lung")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
					tryAction(fluentWaitElements(PersonalStatementPageObjects.chkAsthma),"chkCheck","Asthma");
					if(strLungCondition.equalsIgnoreCase("mild")){
						tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition - "+strLungCondition);
					}else if(strLungCondition.equalsIgnoreCase("Moderate")){
						tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaModerate),"Click","Mild Asthma Condition - "+strLungCondition);
					}else if(strLungCondition.equalsIgnoreCase("Severe")){
						tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaSevere),"Click","Mild Asthma Condition - "+strLungCondition);
					}else{
						tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition - "+strLungCondition);
					}
					sleep();
					String strNextId = null;
					strNextId = getAttribute(waitForClickableElement(PersonalStatementPageObjects.radAsthmaMild), "name");
					String strRadioId =null;
					strRadioId = org.apache.commons.lang.StringUtils.substring(strNextId, 0, 15)+"1";
					tryAction(strAsthmaWorsened.equalsIgnoreCase("yes")?waitForClickableElement(getObject("input[type='radio'][name='"+strRadioId+"'][value='Yes']")):waitForClickableElement(getObject("input[type='radio'][name='"+strRadioId+"'][value='No']")),"Click","Is your asthma worsened by your occupation? "+strAsthmaWorsened );
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("none")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.notaLungSTFt),"chkCheck","Selected Values is None Of the Above ");
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("copd")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxCOPD),"chkCheck","Selected Values is Emphysema/COPD ");
				}
			}			
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBPCholNota),"Click","In the last 5 years have you suffered from");
			
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.radPregnentNo),"Click","Are you currently pregnant?");
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.radVisitDocNo),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBrainBoneNota),"Click","Have you ever suffered from, been diagnosed with or sought medical advice or treatment for?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistNo),"Click","Family History");
			
			fillInLifeStyleDetails();

			//General Details
			tryAction(strDisclosed.equalsIgnoreCase("yes")?waitForClickableElement(PersonalStatementPageObjects.radSufferFromAnyConditionYes):waitForClickableElement(PersonalStatementPageObjects.radSufferFromAnyConditionNo),"CLICK","do you presently suffer from any condition? - "+strDisclosed);
			fillInInsuranceDetails();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinuePersonalStatement),"Click","Calculate Quote");
			sleep(2000);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
	
	
	private void fillInINGDPersonalStatementDets() {

		String strOffWork = dataTable.getData("MetLife_Data", "shortform.offwork");
		String str3YrDiagnosis = dataTable.getData("MetLife_Data", "shortform.healthcheckboxes");
		//String str5YrDiagnosis = dataTable.getData("MetLife_Data", "last5yearsDiagnosis");
		
		String[] array3YrDiagnosis = str3YrDiagnosis.split(",");
		//String[] array5YrDiagnosis = str5YrDiagnosis.split(",");
		
		
		try{
			
			for(int i=0;i<array3YrDiagnosis.length;i++){
				System.out.println("array3YrDiagnosis[i] : "+array3YrDiagnosis[i]);
				if(array3YrDiagnosis[i].equalsIgnoreCase("Kidney")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxKidney),"chkCheck","Kidney or liver condition");
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxDialysis),"chkCheck","Dialysis");
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("none")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxIngdShortformNOTA),"chkCheck","Selected Values is None Of the Above ");
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("digestive")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxDigestive),"chkCheck","digestive condition");
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxOesophagus),"chkCheck","Barrett's Oesophagus");
					
				}else if(array3YrDiagnosis[i].equalsIgnoreCase("thyroid")){
					tryAction(waitForClickableElement(PersonalStatementPageObjects.chkboxThyroid),"chkCheck","Selected Values is Thyroid condition ");
					
				}
			}			
			
			//tryAction(waitForClickableElement(PersonalStatementPageObjects.radVisitDocNo),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			tryAction(strOffWork.equalsIgnoreCase("yes")?waitForClickableElement(PersonalStatementPageObjects.radOffWorkYes):waitForClickableElement(PersonalStatementPageObjects.radOffWorkNo),"Click","Are you currently off work due to injury, accident or illness; or restricted from being able to perform your full and normal duties on a full-time basis (for at least 30 hours per week)? Your actual employment can be on a full-time, part-time or casual basis. - "+strOffWork);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnIngdShortformContinue),"Click","Continue");
			sleep(2000);
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
	
	
	
	
	
}
