package au.com.metlife.eapply.ct.webserivce;


import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

import au.com.metlife.eapply.ct.exception.MetlifeException;
import au.com.metlife.webservices.EAPPSearchStub;
import au.com.metlife.webservices.PWCBClientHandler;
import au.com.metlife.webservices.PicsLodgementStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ELodgementServiceInitializer extends MetlifeException{
	private static final Logger log = LoggerFactory.getLogger(ELodgementServiceInitializer.class);

	
	/*private static final String PACKAGE_NAME = ELodgementServiceInitializer.class
	.getPackage().getName();

	private static final String CLASS_NAME = ELodgementServiceInitializer.class.getName();
	private static final String PICS_SERVICE_PATH = MetlifeElodgeConstants.CREATE_UPDATE_UW_WSDL ;*/
	
	/*static MetLifeProperty property = new MetLifeProperty();*/
	ConfigurationContext ctx= null;
	
	
	
	public  PicsLodgementStub doInitPICS(String userName,String password,String clientConfig,String picsUrl) throws MetlifeException{
		log.info("Do init pics start");
        PicsLodgementStub picsStub = null;
		/*EAPPSearchStub stub = null;
*/		String wsConfigPath = null;
		try {
		ServiceClient client ;
		
		
		/*java.util.Properties props = new java.util.Properties();               */
		wsConfigPath = clientConfig;/*(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.WEB_SER_CONFIG, MetlifeElodgeConstants.WEB_SER_CONFIG);	        */
        String servicePath = picsUrl;/*(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.PICS_SEARCH_CONFIG, MetlifeElodgeConstants.PICS_SEARCH_CONFIG);*/
        
			ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(wsConfigPath, null);	

			picsStub = new PicsLodgementStub(ctx, servicePath);

			client = picsStub._getServiceClient();

			client.engageModule(MetlifeElodgeConstants.ENGAGE_MODULE);
		
		Options options = client.getOptions();
		options.setTimeOutInMilliSeconds(100000);

		options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY,
				ELodgementOutflowConfiguration.getOutflowConfiguration(userName));

		PWCBClientHandler myCallback = new PWCBClientHandler();
		myCallback.setUTUsername(userName);
		myCallback.setUTPassword(password);

		options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);

		client.setOptions(options);
		
		} catch (Exception e) {
			MetlifeException exe = new MetlifeException();
			exe.setErrorCode("10024");
			throw new MetlifeException();
		}

		log.info("Do init pics finish");
		return picsStub;

	}
	

	public  EAPPSearchStub doInitEAPP(String userName,String password) throws MetlifeException{
		log.info("Do init eapp start");
		EAPPSearchStub stub = null;
		String wsConfigPath = null;
		            
		
		try {
	        wsConfigPath = "/var/WebSphere/props/eApplication/client_config";/*(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.WEB_SER_CONFIG, MetlifeElodgeConstants.WEB_SER_CONFIG);	        */
	        String servicePath = "http://10.173.91.95/webtop/PICSWS/services/PicsLodgement";/*(String)ConfigurationHelper.getConfigurationValue(MetlifeElodgeConstants.PICS_SEARCH_CONFIG, MetlifeElodgeConstants.PICS_SEARCH_CONFIG);*/

	        log.info("11> {} and <22> {}",wsConfigPath,servicePath);
	        ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(wsConfigPath, null);      		
			stub = new EAPPSearchStub(ctx, servicePath);
			ServiceClient client = stub._getServiceClient();
			log.info("client ");
			client.engageModule(MetlifeElodgeConstants.ENGAGE_MODULE);
			Options options = client.getOptions();
			options.setTimeOutInMilliSeconds(100000);			
			options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY,
					ELodgementOutflowConfiguration.getOutflowConfiguration(userName));			
			PWCBClientHandler myCallback = new PWCBClientHandler();
			
			log.info("call beack handler userName {} and password {}",userName,password);
			myCallback.setUTUsername(userName);
			myCallback.setUTPassword(password);
			options.setProperty(WSHandlerConstants.PW_CALLBACK_REF, myCallback);
			client.setOptions(options);
			
		} catch (Exception e) {
			MetlifeException exe = new MetlifeException();
			exe.setErrorCode("10024");
			throw new MetlifeException();
		}
		log.info("Do init eapp finish");
		return stub;

	}

	
	
	
}

