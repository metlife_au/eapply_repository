package au.com.metlife.eapply.bs.pdf;

/**
 * <p>
 * Customized API for generating PDF using the iText API.
 * </p>
 * 
 * @author Puneet Malode
 */

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.events.FieldPositioningEvents;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.quote.utility.SystemProperty;


public class CostumPdfAPI {
	private static final Logger log = LoggerFactory.getLogger(CostumPdfAPI.class);
    public static final String PACKAGE_NAME = "com.metlife.elodge.pdfgenerator";
    
    public static final String CLASS_NAME = "CostumPdfAPI";
    
    private java.util.Properties property = new java.util.Properties();
    
    private Font font;
    
    private Color color;
    
    private int fontSize;
    
    private float borderWidth;
    
    private String fundCode;
    private String fontStyle;
    private  BaseFont swsMed;
    private  BaseFont swsLig;
    private  BaseFont swsThn;
    private Color font_color;
    private Color heading_color;
    private Color heading_color_fill;
    private Color header_color;
    private Color table_fill_color;
    private Color background_color;
    private Color border_color;
    
    
    private  FieldPositioningEvents fpe = new FieldPositioningEvents();
    SystemProperty sys;
  
  /*  @Value("${spring.propertyFile}")
	private  String pdfproperty;
	
	
    
    public String getPdfproperty() {
		return pdfproperty;
	}

	public void setPdfproperty(String pdfproperty) {
		this.pdfproperty = pdfproperty;
	}*/
	
	public CostumPdfAPI(String pdfBasePath) {
         log.info("Custom pdf api start");
     
        
        try {
        	log.info("pdfpropertyfileloc>> {}",pdfBasePath);
            String PdfPropertyFileLoc = pdfBasePath;/*ConfigurationHelper.getConfigurationValue( "PdfPropertyFileLoc", "PdfPropertyFileLoc");*/
            File file = new File( PdfPropertyFileLoc);
            InputStream url = new FileInputStream( file);
            property.load( url);
        } catch (IOException e) {
          log.error("Error in input output : {}",e);
        }
        color = Color.decode( property.getProperty( "white"));
        fontSize = 9;
        font = FontFactory.getFont( FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, new Color(
                0, 0, 0));
        fontStyle = FontFactory.HELVETICA_BOLD;
        borderWidth = 0.9f;
        log.info("Custom pdf api finish");
    }

	public CostumPdfAPI() {
		log.info("Custom pdf api start");
        
        
        try {
        	sys = SystemProperty.getInstance();
        	log.info("pdfpropertyfileloc>> {}", sys.getProperty("propertyFile"));
            String PdfPropertyFileLoc = sys.getProperty("propertyFile");/*ConfigurationHelper.getConfigurationValue( "PdfPropertyFileLoc", "PdfPropertyFileLoc");*/
            File file = new File( PdfPropertyFileLoc);
            InputStream url = new FileInputStream( file);
            property.load( url);
        } catch (IOException e) {
        	log.error("Error in input output: {}",e);
        } catch (Exception e) {
        	log.error("Error in custom pdf: {} ",e);
		}
        color = Color.decode( property.getProperty( "white"));
        fontSize = 9;
        font = FontFactory.getFont( FontFactory.HELVETICA_BOLD, 9, Font.NORMAL, new Color(
                0, 0, 0));
        fontStyle = FontFactory.HELVETICA_BOLD;
        borderWidth = 0.9f;
        log.info("Custom pdf api finish");
    }
    
    public Color getColor() {

        return this.color;
    }
    
    public Color getColor(String color) {

        return Color.decode( property.getProperty( color));
    }
    
    public void setColor(String color) {

        this.color = Color.decode( property.getProperty( color));
    }
    
    public void setColor(Color color) {

        this.color = color;
    }
    
    public Font getFont() {

        return font;
    }
    
    public Font getFont(String fontStyle, int fontSize, String type,
            String fontColor) {

        this.fontStyle = fontStyle + "-" + type;
        return FontFactory.getFont( this.fontStyle, fontSize, getColor( fontColor));
    }
    
    public Font getFont(String fontStyle, int fontSize, String type) {

        this.fontStyle = fontStyle + "-" + type;
        return FontFactory.getFont( this.fontStyle, fontSize, this.color);
    }
    
    public Font getFont(String fontStyle, int fontSize) {

        return FontFactory.getFont( fontStyle, fontSize, this.color);
    }
    
    public Font getFont(Color color) {

        return FontFactory.getFont( this.fontStyle, this.fontSize, color);
    }
    
    public Font getFont(Color color, int fontSize) {

        return FontFactory.getFont( this.fontStyle, fontSize, color);
    }
    
    public Font getFont(String fontStyle) {

        return FontFactory.getFont( fontStyle, this.fontSize, this.color);
    }
    
    public void setFont(String fontStyle, int fontSize, String type,
            String fontColor) {

        this.fontStyle = fontStyle;
        this.fontSize = fontSize;
        this.color = this.getColor( fontColor);
        this.fontStyle = fontStyle + "-" + type;
        this.font = FontFactory.getFont( this.fontStyle, fontSize, getColor( fontColor));
    }
    
    public void setFont(Font font) {

        this.font = font;
    }
    
    public void setFont(String fontStyle, int fontSize, String color) {

        this.fontStyle = fontStyle;
        this.fontSize = fontSize;
        this.color = getColor( color);
        
        this.font = FontFactory.getFont( this.fontStyle, fontSize, this.color);
    }
    
    public void setFont(String fontStyle, int fontSize, Color color) {

        this.fontStyle = fontStyle;
        this.fontSize = fontSize;
        this.color = color;
        
        this.font = FontFactory.getFont( this.fontStyle, fontSize, this.color);
    }
    
    public void setFont(String fontStyle, int fontSize) {

        this.fontStyle = fontStyle;
        this.fontSize = fontSize;
        
        this.font = FontFactory.getFont( fontStyle, fontSize, this.color);
    }
    
    public void setFont(String fontStyle) {

        this.fontStyle = fontStyle;
        this.font = FontFactory.getFont( fontStyle, this.fontSize, this.color);
    }
    
    public float getborderWidth() {

        return borderWidth;
    }
    
    public void setborderWidth(float borderWidth) {

        this.borderWidth = borderWidth;
    }
    
    public PdfPTable addBreakDateTable() {
         log.info("Adding break date table start");
        float[] borderWidth = { .25f, .1f, .3f, 0.1f, .25f };
        PdfPTable tableBreakDate = new PdfPTable( borderWidth);
        tableBreakDate.setLockedWidth( true);
        tableBreakDate.setTotalWidth( 100f);
        PdfPCell pdfCC1 = new PdfPCell();
        disableBorders( pdfCC1);
        /*cellPadding(pdfCC1,50f);*/
        tableBreakDate.addCell( pdfCC1);
        
        PdfPCell pdfCC2 = new PdfPCell( addParagraph( "/"));
        disableBorders( pdfCC2);
        /*cellPadding(pdfCC2,50f);*/
        tableBreakDate.addCell( pdfCC2);
        tableBreakDate.addCell( pdfCC1);
        tableBreakDate.addCell( pdfCC2);
        tableBreakDate.addCell( pdfCC1);
        log.info("Adding break date table finish");
        return tableBreakDate;
    }
    
    public String addDateSeparator(int space) {
    	log.info("Adding date separator start");
        String spaces = " ";
        for (int i = 0; i < space; i++) {
            spaces = spaces.concat( "\t");
        }
        spaces = spaces + "/" + spaces + "/" + spaces;
        log.info("Adding date separator finish");
        return spaces;
    }
    
    public String addDateSeparator() {
    	 log.info("Adding date separator start");
        String spaces = " ";
        for (int i = 0; i < 10; i++) {
            spaces = spaces.concat( "\t");
        }
        spaces = spaces + "/" + spaces + "/" + spaces;
        log.info("Adding date separator finish");
        return spaces;
    }
    
    public PdfPTable addIfYes() {
    	 log.info("Added if yes start");
        float[] borderWidth2 = { 1f };
        PdfPTable tableIfYes = new PdfPTable( borderWidth2);
        PdfPCell pdfCC1 = new PdfPCell(
                addParagraph( "\t\t\tIf \"YES\", please provide details."));
        disableBorders( pdfCC1);
        tableIfYes.addCell( pdfCC1);
        PdfPCell pdfCC2 = new PdfPCell();
        pdfCC2.setFixedHeight( 15f);
        pdfCC2.setBorderColor( color);
        pdfCC2.setBorderWidth( borderWidth);
        tableIfYes.addCell( pdfCC2);
        tableIfYes.addCell( pdfCC2);
        log.info("Added if yes finish");
        return tableIfYes;
    }
    
    public PdfPTable addYesNo() {
    	log.info("Added yes no start");
        float[] width2 = { 0.25f, 0.25f, 0.25f, 0.25f };
        PdfPTable tableYesNo = new PdfPTable( width2);
        tableYesNo.setLockedWidth( true);
        tableYesNo.setTotalWidth( 100f);
        PdfPCell c835 = new PdfPCell( new Paragraph( " Yes", font));
        disableBorders( c835);
        c835.setFixedHeight( fontSize + 4f);
        
        tableYesNo.addCell( c835);
        
        PdfPCell c865 = new PdfPCell();
        c865.setFixedHeight( fontSize);
        c865.setBorderColor( this.color);
        c865.setBorderWidth( this.borderWidth);
        tableYesNo.addCell( c865);
        
        PdfPCell c845 = new PdfPCell( new Paragraph( " No", font));
        disableBorders( c845);
        c845.setFixedHeight( fontSize + 4f);
        tableYesNo.addCell( c845);
        
        tableYesNo.addCell( c865);
        log.info("Added yes no finish");
        return tableYesNo;
    }
    
    public PdfPTable addSingleCell(PdfWriter writer, String name) {
    	log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_START);
        writer.setPageEvent( fpe);
        PdfPTable pdfPTable = new PdfPTable( 1);
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.setBorderColor( this.color);
        pdfPCell.setBorderWidth( this.borderWidth);
        try {
            pdfPCell.setCellEvent( new FieldPositioningEvents( writer, name));
        } catch (IOException e) {
        	log.error("Error in input output: {}",e);
        } catch (DocumentException e) {
        	log.error("Error in document: {}",e);
        }
        pdfPTable.addCell( pdfPCell);
        log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_FINISH);
        return pdfPTable;
    }
    
    public PdfPCell addSingleCell() {
    	log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_START);
        PdfPTable pdfPTable = new PdfPTable( 1);
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.setBorderColor( this.color);
        pdfPCell.setBorderWidth( this.borderWidth);
        
        pdfPTable.addCell( pdfPCell);
        log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_FINISH);
        return pdfPCell;
    }
    
    public PdfPTable addSingleCell(String cellContent, boolean cellBorder) {
    	log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_START);
        PdfPTable pdfPTable = new PdfPTable( 1);
        PdfPCell pdfPCell = new PdfPCell( addParagraph( cellContent));
        if (!cellBorder) {
            this.disableBorders( pdfPCell);
        }
        pdfPCell.setBorderColor( this.color);
        pdfPCell.setBorderWidth( this.borderWidth);
        
        pdfPTable.addCell( pdfPCell);
        log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_FINISH);
        return pdfPTable;
    }
    
    public PdfPTable addSingleCell(Paragraph cellContent, boolean cellBorder) {
    	log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_START);
        PdfPTable pdfPTable = new PdfPTable( 1);
        PdfPCell pdfPCell = new PdfPCell( cellContent);
        if (!cellBorder) {
            this.disableBorders( pdfPCell);
        }
        pdfPCell.setBorderColor( this.color);
        pdfPCell.setBorderWidth( this.borderWidth);
        pdfPTable.addCell( pdfPCell);
        log.info(MetlifeInstitutionalConstants.ADD_SINGLE_CELL_FINISH);
        return pdfPTable;
    }
    
    public PdfPTable addBullet(String content, String backgroundColor,
            String fontColor) {
    	log.info("Add bullet start");

        PdfPTable table = new PdfPTable( 1);
        table.setTotalWidth( fontSize + 6f);
        table.setLockedWidth( true);
        
        PdfPCell cell1 = new PdfPCell(
                addParagraph( content, fontColor, fontSize));
        cell1.setHorizontalAlignment( Element.ALIGN_CENTER);
        
        cell1.setBorderColor( color);
        
        if (!backgroundColor.equalsIgnoreCase( fontColor)) {
            cell1.setBackgroundColor( this.getColor( backgroundColor));
        }
        cell1.setFixedHeight( fontSize + 5f);
        cell1.setHorizontalAlignment( Element.ALIGN_LEFT);
        cell1.setVerticalAlignment( Element.ALIGN_TOP);
        table.addCell( cell1);
        log.info("Add bullet finish");
        return table;
        
    }
    
    public Chunk addBulletchunk() {
        
        return new Chunk( "\u2022",
                FontFactory.getFont( this.fontStyle, this.fontSize, this.color));
        
    }
    
    public Paragraph addParagraph(String str) {

        return new Paragraph( str, font);
    }
    
    public Paragraph addParagraph(String str, String color) {

        return new Paragraph( str, this.getFont( this.getColor( color)));
    }
    
    public Paragraph addParagraph(String str, String color, int fontSize) {

        return new Paragraph( str,
                this.getFont( this.getColor( color), fontSize));
    }
    
    public Paragraph addParagraph(String str, int fontSize) {

        return new Paragraph( str, this.getFont( color, fontSize));
    }
    
    public Paragraph addParagraph(String str, Font font) {

        return new Paragraph( str, font);
    }
    
    public Paragraph addParagraph(String str, String font, int size,
            int pattern, String fontColor) {

        return new Paragraph(
                str,
                FontFactory.getFont( font, size, pattern, this.getColor( fontColor)));
    }
    
    public Paragraph addParagraph(Date dte, String color) {
    	log.info("Add paragraph start");
        SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy");
        String dateString = formatter.format( dte);
        log.info("Add paragraph finish");
        return new Paragraph( dateString, font);
    }
    
    public PdfPTable addDateTable(String separator, PdfWriter writer,
            String name) {
    	log.info("Add date table start");
        PdfPTable nest2 = new PdfPTable( 8);
        for (int i = 0; i < 8; i++) {
            
            if (i == 2 || i == 5) {
                
                PdfPCell pdfDC2 = new PdfPCell(
                        addSingleCell( " " + separator, true));
                
                disableBorders( pdfDC2);
                pdfDC2.setBorderColor( color);
                pdfDC2.setBorderWidth( borderWidth);
                cellPadding( pdfDC2, 1f);
                pdfDC2.setHorizontalAlignment( Element.ALIGN_CENTER);
                nest2.addCell( pdfDC2);
                
            } else {
                PdfPCell pdfDC1 = new PdfPCell(
                        addSingleCell( writer, name + i));
                try {
                    pdfDC1.setCellEvent( new FieldPositioningEvents( writer,
                            name));
                } catch (IOException e) {
                	log.error("Error in Input output: {}",e);
                } catch (DocumentException e) {
                	log.error("Error in document: {}",e);
                }
                pdfDC1.setBorderWidth( borderWidth);
                disableBorders( pdfDC1);
                pdfDC1.setBorderColor( color);
                cellPadding( pdfDC1, 1f);
                nest2.addCell( pdfDC1);
            }
            
        }
        log.info("Add date table finish");
        return nest2;
    }
    
    public PdfPTable addDateTable(String separator) {
    	log.info("Add date table start");
        PdfPTable nest2 = new PdfPTable( 8);
        
        PdfPCell pdfDC1 = new PdfPCell( addSingleCell( "", true));
        pdfDC1.setBorderWidth( borderWidth);
        disableBorders( pdfDC1);
        pdfDC1.setBorderColor( color);
        cellPadding( pdfDC1, 1f);
        nest2.addCell( pdfDC1);
        nest2.addCell( pdfDC1);
        PdfPCell pdfDC2 = new PdfPCell( addSingleCell( " " + separator, true));
        disableBorders( pdfDC2);
        pdfDC2.setBorderColor( color);
        pdfDC2.setBorderWidth( borderWidth);
        cellPadding( pdfDC2, 1f);
        pdfDC2.setHorizontalAlignment( Element.ALIGN_CENTER);
        nest2.addCell( pdfDC2);
        nest2.addCell( pdfDC1);
        nest2.addCell( pdfDC1);
        nest2.addCell( pdfDC2);
        nest2.addCell( pdfDC1);
        nest2.addCell( pdfDC1);
        log.info("Add date table finish");
        return nest2;
    }
    
    /*comments*/
    public PdfPTable addDateTableFromTo(String separator) {
    	log.info("Add date table from to start");
        PdfPTable nest2 = new PdfPTable( 4);
        
        PdfPCell pdfDC1 = new PdfPCell( addDateTable( "/"));
        disableBorders( pdfDC1);
        nest2.addCell( pdfDC1);
        PdfPCell pdfPCell2 = new PdfPCell( addParagraph( "To"));
        disableBorders( pdfPCell2);
        pdfPCell2.setHorizontalAlignment( Element.ALIGN_CENTER);
        nest2.addCell( pdfPCell2);
        
        nest2.addCell( pdfDC1);
        PdfPCell pdfPCell3 = new PdfPCell();
        disableBorders( pdfPCell3);
        nest2.addCell( pdfPCell3);
        log.info("Add date table from to finish");
        return nest2;
    }
    
    /*comments*/
    public PdfPTable addYearTable() {
    	log.info("Add year table start");
        PdfPTable pdfPTable = new PdfPTable( 2);
        
        PdfPCell pdfDC1 = new PdfPCell();
        pdfDC1.setBorderWidth( borderWidth);
        pdfDC1.setBorderColor( color);
        cellPadding( pdfDC1, 1f);
        pdfPTable.addCell( pdfDC1);
        
        PdfPCell pdfDC2 = new PdfPCell( addParagraph( "Years"));
        disableBorders( pdfDC2);
        pdfDC2.setBorderColor( color);
        pdfDC2.setBorderWidth( borderWidth);
        cellPadding( pdfDC2, 1f);
        
        pdfPTable.addCell( pdfDC2);
        log.info("Add year table finish");
        return pdfPTable;
    }
    
    public PdfPTable setTopic(String str) {
        log.info("Setting topic start");
        PdfPTable tab1 = new PdfPTable( 1);
        PdfPCell c1 = new PdfPCell( new Paragraph( str, font));
        c1.setBackgroundColor( color);
        disableBorders( c1);
        tab1.addCell( c1);
        log.info("Setting topic finish");
        return tab1;
    }
    
    public void disableBorders(PdfPCell cell) {
        log.info("Disable broders start");
        cell.disableBorderSide( PdfPCell.LEFT);
        cell.disableBorderSide( PdfPCell.RIGHT);
        cell.disableBorderSide( PdfPCell.TOP);
        cell.disableBorderSide( PdfPCell.BOTTOM);
        log.info("Disable broders finish");
    }
    
    public void disableBordersTop(PdfPCell cell) {
    	log.info("Disable broders top start");
        cell.disableBorderSide( PdfPCell.LEFT);
        cell.disableBorderSide( PdfPCell.RIGHT);
        cell.disableBorderSide( PdfPCell.BOTTOM);
        log.info("Disable broders finish");
    }
    
    public void cellPadding(PdfPCell cell, float padding) {
    	log.info("Cell padding start");
        cell.setPaddingBottom( padding);
        cell.setPaddingLeft( padding);
        cell.setPaddingRight( padding);
        cell.setPaddingTop( padding);
        log.info("Cell padding finish");
    }
    
    public int getFontSize() {

        return fontSize;
    }
    
    public void setFontSize(int fontSize) {

        this.fontSize = fontSize;
    }
    
    public String getProperty(String key) {
        return property.getProperty( key);
    }
    
    public void finish() {

        property.clear();
        property = null;
        fpe = null;
        fontStyle = null;
        font = null;
        color = null;
        
    }

	public BaseFont getSwsMed() {
		return swsMed;
	}

	public void setSwsMed(BaseFont swsMed) {
		this.swsMed = swsMed;
	}

	public BaseFont getSwsLig() {
		return swsLig;
	}

	public void setSwsLig(BaseFont swsLig) {
		this.swsLig = swsLig;
	}

	public BaseFont getSwsThn() {
		return swsThn;
	}

	public void setSwsThn(BaseFont swsThn) {
		this.swsThn = swsThn;
	}

	public Color getFont_color() {
		return font_color;
	}

	public void setFont_color(Color font_color) {
		this.font_color = font_color;
	}

	public Color getHeading_color() {
		return heading_color;
	}

	public void setHeading_color(Color heading_color) {
		this.heading_color = heading_color;
	}

	public Color getHeading_color_fill() {
		return heading_color_fill;
	}

	public void setHeading_color_fill(Color heading_color_fill) {
		this.heading_color_fill = heading_color_fill;
	}

	public Color getHeader_color() {
		return header_color;
	}

	public void setHeader_color(Color header_color) {
		this.header_color = header_color;
	}

	public Color getTable_fill_color() {
		return table_fill_color;
	}

	public void setTable_fill_color(Color table_fill_color) {
		this.table_fill_color = table_fill_color;
	}

	public Color getBackground_color() {
		return background_color;
	}

	public void setBackground_color(Color background_color) {
		this.background_color = background_color;
	}

	public Color getBorder_color() {
		return border_color;
	}

	public void setBorder_color(Color border_color) {
		this.border_color = border_color;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
}
