function LandingConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.landing', {
    url: '/',
    controller: 'LandingCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'landing/landing.html',
    title: 'Landing',
    resolve: {
      urls: function(CommonAPI, AppConstants) {
        return !CommonAPI.getUrlList() && CommonAPI.fetchApiUrls(AppConstants.urlFilePath);
      }
    }
  });

};

export default LandingConfig;
