package au.com.metlife.eapply.bs.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;
import au.com.metlife.eapply.bs.model.TblOccMappingRuledataRepository;
import au.com.metlife.eapply.bs.service.TblOccMappingRuledataService;

@Service
public class TblOccMappingRuledataServiceImpl implements TblOccMappingRuledataService {
	private static final Logger log = LoggerFactory.getLogger(TblOccMappingRuledataServiceImpl.class);

	 private final TblOccMappingRuledataRepository repository;

	 @Autowired
	    public TblOccMappingRuledataServiceImpl(final TblOccMappingRuledataRepository repository) {
	        this.repository = repository;
	    }
	 
	
	public TblOccMappingRuledataRepository getRepository() {
		return repository;
	}


	@Override
	@Transactional
	public List<TblOccMappingRuledata> getOccupationList(String fundCode, String induCode) {
		log.info("Get occupation list start");
		List<TblOccMappingRuledata> tblOccMappingRuledata=repository.findByFundCodeAndIndustryCode(fundCode,induCode);
		log.info("Get occupation list finish");
		return tblOccMappingRuledata;
	}
}
