package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileAttachment")

@XmlRootElement
public class FileAttachment {
	
	private AttachmentType AttachmentType;

	public AttachmentType getAttachmentType() {
		return AttachmentType;
	}

	public void setAttachmentType(AttachmentType attachmentType) {
		AttachmentType = attachmentType;
	}

	

	
	
}
