(function (angular) {
  'use strict';

  angular
    .module('Metlife.guil.uicomponents', ['guil.ui.constants'])
    .directive('metlifeGuilFooter', metlifeGuilFooter);

  function metlifeGuilFooter(guilUIConstants) {
    var directive = {
      restrict: 'EA',
      templateUrl: guilUIConstants.path.uiComponents + 'metlife-guil-footer/metlife-guil-footer.tmpl.html',
      controller: footerCtrl,
      controllerAs: 'footer',
      bindToController: true
    };

    return directive;
  }

  footerCtrl.$inject = ['$scope', 'guilUIConstants'];

  function footerCtrl($scope, guilUIConstants) {
    var footer = this;
    footer.constants = {
      privacyUrl: guilUIConstants.navUrls.privacyUrl
    };
  }

})(angular);