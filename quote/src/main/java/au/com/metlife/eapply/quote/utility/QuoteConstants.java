package au.com.metlife.eapply.quote.utility;

public interface QuoteConstants {

	public static final String PREM_FREQ_MONTHLY = "Monthly";

	public static final String PREM_FREQ_YEARLY = "Yearly";

	public static final String PREM_FREQ_WEEKLY = "Weekly";
	
	public static final String RULE_NAME_CARE_INST_IP_FXD_COST="CARE_INST_IP_FXD_COST.xls";
	
	public static final String RULE_NAME_INST_IP_FXD_COST="_INST_IP_FXD_COST.xls";
	
	public static final String RULE_NAME_INST_TPD_FXD_COST="INST_TPD_FXD_COST.xls";

	public static final String RULE_NAME_INST_DEATH_UNT_CVR_AMT = "INST_DEATH_UNT_CVR_AMT.xls";

	public static final String RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID = "Inst_Product_Rule_Set_By_ProdId.xls";

	public static final String RULE_NAME_INST_IP_THRESHOLD_LMT = "INST_IP_THRESHOLD_LMT.xls";

	public static final String RULE_NAME_INST_INDUSTRY_MAPPING = "INST_INDUSTRY_MAPPING.xls";

	public static final String RULE_NAME_INST_INDUSTRY_OCCUPATION_MAPPING = "INST_INDUSTRY_OCCUPATION_MAPPING.xls";

	public static final String RULE_NAME_INST_DEATH_FXD_COST = "INST_DEATH_FXD_COST.xls";

	public static final String RULE_NAME_INST_NORM_ALL_CVR_COST = "INST_NORM_ALL_CVR_COST.xls";

	public static final String RULE_NAME_INST_TPD_UNT_CVR_AMT = "INST_TPD_UNT_CVR_AMT.xls";

	public static final String INS_CATEGORY_STANDARD = "Standard";

	public static final String INS_CATEGORY_PROFESSIONAL = "Professional";

	public static final String INS_CATEGORY_NA = "N/A";

	public static final String INS_CATEGORY_WHITECOLLAR = "White Collar";

	public static final String INS_CATEGORY_GENERAL = "General";

	public static final String INS_CATEGORY_APPROVED = "Approved";

	public static final String INS_CATEGORY_OWN_APPROVED = "Own Approved";

	public static final String INS_CATEGORY_LIGT_BLUE_COLLAR = "Light Blue Collar";

	public static final String INS_CATEGORY_NON_MANUAL = "Non Manual";

	public static final String INS_CATEGORY_HEAVY_BLUE_COLLAR = "Heavy Blue Collar";

	public static final String INS_CATEGORY_HEAVY_BLUE_COLLAR_SKILLED = "Heavy Blue Collar Skilled";

	public static final String INS_CATEGORY_HEAVY_BLUE_COLLAR_UNSKILLED = "Heavy Blue Collar Unskilled";

	public static final String INS_CATEGORY_SPECIAL = "Special";

	public static final String INS_CATEGORY_LOW_RISK = "Low Risk";

	public static final String INS_CATEGORY_BASIC = "Basic";

	public static final String INS_CATEGORY_STANDARD_PLUS = "Standard Plus";

	public static final String PARTNER_PSUP = "PSUP";

	public static final String PARTNER_PSUP_N = "PSUP_N";

	public static final String PARTNER_PWCP = "PWCP";

	public static final String PARTNER_PWCP_OLD = "PWCP_OLD";

	public static final String PARTNER_SFPS = "SFPS";

	public static final String PARTNER_AUSW = "AUSW";

	public static final String PARTNER_AUSW_E = "AUSW_E";

	public static final String PARTNER_REIS = "REIS";

	public static final String PARTNER_ACCS = "ACCS";

	public static final String PARTNER_PLAN = "PLAN";

	public static final String PARTNER_CSSF = "CSSF";

	public static final String PARTNER_JJSF = "JJSF";

	public static final String PARTNER_MTAA = "MTAA";

	public static final String PARTNER_NSFS = "NSFS";

	public static final String PARTNER_AEIS = "AEIS";

	public static final String PARTNER_ASSF = "ASSF";

	public static final String PARTNER_INGD = "INGD";

	public static final String PARTNER_APSD = "APSD";

	public static final String PARTNER_GEAU = "GEAU";

	public static final String PARTNER_A102 = "A102";

	public static final String PARTNER_CORP = "CORP";

	public static final String PARTNER_A103 = "A103";

	public static final String PARTNER_A104 = "A104";

	public static final String PARTNER_INGS = "INGS";

	public static final String PARTNER_PWCS = "PWCS";

	public static final String PARTNER_FIRS = "FIRS";

	public static final String PARTNER_HOST = "HOST";

	public static final String PARTNER_GUIL = "GUIL";

	public static final String PARTNER_VICS = "VICS";

	public static final String DC_UNITISED = "DcUnitised";

	public static final String DC_FIXED = "DcFixed";

	public static final String TPD_UNITISED = "TpdUnitised";

	public static final String TPD_FIXED = "TpdFixed";

	public static final String IP_UNITISED = "IpUnitised";

	public static final String IP_FIXED = "IpFixed";

	public static final String GENDER_MALE = "Male";

	public static final String GENDER_FEMALE = "Female";
	
	public static final String BENEFIT_PERIOD_5_YEARS="5 Years";
	
	public static final String BENEFIT_PERIOD_2_YEARS="2 Years";
	
	public static final String BENEFIT_PERIOD_AGE_60="Age 60";
	
	public static final String BENEFIT_PERIOD_AGE_65="Age 65";
	
	public static final String BENEFIT_PERIOD_TO_AGE_65="To Age 65";
	
	public static final String BENEFIT_PERIOD_AGE_67="Age 67";
	
	public static final String BENEFIT_PERIOD_TO_AGE_67="To Age 67";
	
	public static final String WAITING_PERIOD_14_DAYS="14 Days";
	
	public static final String WAITING_PERIOD_30_DAYS="30 Days";
	
	public static final String WAITING_PERIOD_60_DAYS="60 Days";
	
	public static final String WAITING_PERIOD_820_DAYS="820 Days";
	
	public static final String WAITING_PERIOD_90_DAYS="90 Days";
	
	public static final String WAITING_PERIOD_1_YEAR="1 Year";
	
	public static final String WAITING_PERIOD_3_YEAR="3 Years";
	
	public static final String WAITING_PERIOD_45_DAYS="45 Days";
	
	public static final String WAITING_PERIOD_4_WEEKS="4 Weeks";
	
	public static final String WAITING_PERIOD_8_WEEKS="8 Weeks";
	
	public static final String WAITING_PERIOD_13_WEEKS="13 Weeks";
	
    public static final String MTYPE_ICOVER="ICOVER";
    
    public static final String MTYPE_CCOVER="CCOVER";
    
    public static final String MTYPE_SCOVER="SCOVER";
    
    public static final String MTYPE_TCOVER="TCOVER";
    
    public static final String MTYPE_UWCOVER="UWCOVER";
    
    public static final String MTYPE_CANCCVR="CANCCVR";

}
