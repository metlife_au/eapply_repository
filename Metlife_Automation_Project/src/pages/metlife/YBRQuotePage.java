package pages.metlife;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.YBRQuotePageObjects;

public class YBRQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public YBRQuotePage quoteProtect() {
		quoteProtectNow();
		return new YBRQuotePage(scriptHelper);
	}

	public YBRQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void quoteProtectNow() {
	
	
		try {
			
			driver.manage().window().maximize();
			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			sleep(2000);
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(YBRQuotePageObjects.btnCalcQuote)).click().perform();
			sleep(6000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,150)", "");
			report.updateTestLog("Capturing the Auto poplation screenshot", "Values are Autopopuated", Status.SCREENSHOT);
			JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			jse1.executeScript("window.scrollBy(0,450)", "");
			report.updateTestLog("Capturing the Auto poplation screenshot", "Values are Autopopuated", Status.SCREENSHOT);
		    tryAction(waitForClickableElement(YBRQuotePageObjects.btnProceed), "Click", "Apply");
			driver.manage().window().maximize();
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
