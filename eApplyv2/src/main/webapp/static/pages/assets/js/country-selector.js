(function countrySelector() {
    $('.country_selector_container').click(function () {
        if ($('.hidden-xs').is(':visible')) {
            var top = $('.country_selector_container').height();
            $('.country_list_container').css('top', top + 'px');
        }

        $('.country_list_container').slideToggle(500).scrollTop(0);
        return false;
    });

    $('.country_group').click(function () {
        var selectedCountry = jQuery('.country_selector_container .selected');
        selectedCountry.find('.country_flag').replaceWith(jQuery(this).find('.country_flag').clone());
        selectedCountry.find('.country_name').replaceWith(jQuery(this).find('.country_name').clone());
        if (jQuery(this).attr('data-redirect') !== "" && jQuery(this).attr('data-redirect')) {
            jQuery(".country_list_container").slideUp(500);
            window.location.href = jQuery(this).attr('data-redirect');
        } else {
        //    alert("Missing URL for " + jQuery(this).find('span.country_name').text());
        }
    });
    // Close country selector when not accessing
    $(document).on("click tap", function (e) {
        var container = jQuery(".country_list_container");
        if (!container.is(e.target)
            && container.has(e.target).length === 0) {
            container.slideUp(500);
        }
    });

    /***** End Country Selector *************************************************************/
})();





 



