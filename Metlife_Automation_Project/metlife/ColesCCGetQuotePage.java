package pages.metlife;

import org.openqa.selenium.interactions.Actions;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCGetQuotePageObjects;

public class ColesCCGetQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public ColesCCGetQuotePage enterColesCCCalculateQuote() {
		fillAndCalculateColesCCQuote();
		return new ColesCCGetQuotePage(scriptHelper);
	}
	
	

	public ColesCCGetQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}

	public void fillAndCalculateColesCCQuote() {
		
		String coveramt = dataTable.getData("MetLife_Data", "CoverAmount");
		String flybuysmemeber = dataTable.getData("MetLife_Data", "FlyBuysMemeber");
		String joinflybuys = dataTable.getData("MetLife_Data", "JoinFlyBuys");
		
		try {
			
			driver.manage().window().maximize();
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			
			tryAction(fluentWaitElement(ColesCCGetQuotePageObjects.txtColesLname), "SET", "Cover Amount", getRandomText(5));
			tryAction(fluentWaitElement(ColesCCGetQuotePageObjects.selCoverAmt), "DropDownSelect", "Cover Amount", coveramt);
			sleep(1000);
			
			if (flybuysmemeber.equalsIgnoreCase("Yes")) {
				tryAction(waitForClickableElement(ColesCCGetQuotePageObjects.rdFlyBuysYes), "clkradio", "FlyBuys Member - Yes");
			} else {
				tryAction(waitForClickableElement(ColesCCGetQuotePageObjects.rdFlyBuysNo), "clkradio", "FlyBuys Member - No");
			}
			
			sleep(200);
			
			if (joinflybuys.equalsIgnoreCase("Yes")) {
				tryAction(waitForClickableElement(ColesCCGetQuotePageObjects.rdFlyBuysJoinYes), "clkradio", "FlyBuys Member Join - Yes");
			} else {
				tryAction(waitForClickableElement(ColesCCGetQuotePageObjects.rdFlyBuysJoinNo), "clkradio", "FlyBuys Member Join - No");
			}
			
			sleep(200);
		
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ColesCCGetQuotePageObjects.lnkCalculateQuote)).click().perform();
			
			sleep(1000);
			
			tryAction(waitForClickableElement(ColesCCGetQuotePageObjects.lnkApply), "Click", "Apply");

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

}
