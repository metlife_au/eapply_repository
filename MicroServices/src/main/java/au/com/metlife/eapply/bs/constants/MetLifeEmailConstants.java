package au.com.metlife.eapply.bs.constants;
/* -------------------------------------------------------------------------------------------------    
 * Description:   This class is used for defining constants for email component
 * -------------------------------------------------------------------------------------------------
 * Copyright @ 2009 MetLife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :    khirod.panda
 * Created:     April 22,2009
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * Date(MM/DD/YYYY)                 Author                                    Description         
 *          
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
public class MetLifeEmailConstants {



    private MetLifeEmailConstants()
    {
    	//Do nothing as written for future use
    }

    public static final String TEMPLATE_CODE_UC10_1 = "TestMailTemplate.txt";
    public static final String PROP_MAIL_SMTP_HOST = "mail.smtp.host";
    public static final String PROP_MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String PROP_MAIL_SMTP_STARTTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String MSG_CONTENT_TYPE = "text/plain";
    public static final String METLF_EMAIL_DEV_TEST_USERID = "MetLifetest.mpk";
    public static final String EMAIL_ERR_1 = "MetLifeEmailServicException:To Address not Specified";
    public static final String EMAIL_ERR_5 = "MetLifeEmailServicException:Eamil Template not Specified";
    public static final String EMAIL_ERR_2 = "MetLifeEmailServicException:Max to 10 senders Allowed";
    public static final String EMAIL_ERR_3 = "MetLifeEmailServicException:To Address Deined";
    public static final String EMAIL_RESR_1 = "METLIFE";
    public static final String EMAIL_TRUE = "true";
    public static final String EMAIL_FALSE = "false";
    public static final String EMAIL_ERR_4 = "MetLifeEmailServicException:Subject not Specified";
    public static final String EMAIL_EXCEP_1 = "MetLifeEmailServicException:Unable to Read from MetLifeEmailConfig";
    public static final String EMAIL_EXCEP_2 = "MetLifeEmailServicException:Unable to Read Environment from MetLifeEmailConfig";
    public static final String EMAIL_EXCEP_3 = "MetLifeEmailServicException:Unable to SMTP Host name from MetLifeEmailConfig";
    public static final String EMAIL_EXCEP_4 = "MetLifeEmailServicException:Unable to Read Email User ID from MetLifeEmailConfig";
    public static final String EMAIL_EXCEP_5 = "MetLifeEmailServicException:Unable to Read Email Password from MetLifeEmailConfig";
    public static final String METLF_EMAIL_INFO_COMMON = "METLIFE_EMAIL_INFO_COMMON";
    public static final String METLF_ENV = "MetLifeEnvironment";
    public static final String METLF_EMAIL_TEMP_ROOT = "MetLifeEmailTemplateRoot";
    public static final String METLF_EMAIL_ATTACH_ROOT = "MetLifeEmailAttachmentRoot";
    public static final String METLF_ENV_DEV = "DEV";
    public static final String METLF_ENV_PROD = "PROD";
    public static final String DEV_METLF_EMAIL_INFO = "DEV_METLF_EMAIL_INFO";
    public static final String DEV_METLF_EMAIL_UID = "DEV_METLF_EMAIL_UID";
    public static final String DEV_METLF_EMAIL_PW = "DEV_METLF_EMAIL_PWD";
    public static final String DEV_SMTP_HOST_NAME = "DEV_SMTP_HOST_NAME";
    public static final String PROD_METLF_EMAIL_INFO = "PROD_METLF_EMAIL_INFO";
    public static final String PROD_METLF_EMAIL_UID = "PROD_METLF_EMAIL_UID";
    public static final String PROD_METLF_EMAIL_PW = "PROD_METLF_EMAIL_PWD";
    public static final String PROD_SMTP_HOST_NAME = "PROD_SMTP_HOST_NAME";
    public static final String METLF_EMAIL_INFO = "METLF_EMAIL_INFO";
    public static final String SMTP_HOST_NAME = "SMTP_HOST_NAME";
    public static final String SMTP_HOST_NAME_VAL = "SMTP_HOST_NAME_VAL";
    public static final String RECIPIENT = "RECIPIENT";
    public static final String METLF_EMAIL_ADDRESSES = "METLF_EMAIL_ADDRESSES";
    public static final String METLF_EMAIL_CONFIG_ROOT_FOLDER = "Email_1";
    public static final String PDF_LOC = "PDF_LOC";
    public static final String DEV_SENDER_MAIL_ID = "SENDER_MAIL_ID";
    public static final String PROD_SENDER_MAIL_ID = "SENDER_MAIL_ID";
    public static final String NEWUNDERWRITING = "NEWUNDERWRITING";
    public static final String SEARCHUNDERWRITING = "SEARCHUNDERWRITING";
    public static final String NEWCLAIM = "NEWCLAIM";
    public static final String SEARCHCLAIM = "SEARCHCLAIM";
    public static final String NEWUNDERWRITING_TEMPLETNAME = "NewUnderwritingeMailTemp.txt";
    public static final String SUBMIT_TEMPLETNAME = "SubmitApplicationMailTemp.txt";    
    public static final String SUBMIT_INDV_TEMPLETNAME = "SubmitIndvApplicationMailTemp.txt";
    public static final String SEARCHUNDERWRITING_TEMPLETNAME = "ExistingUnderwritingeMailTemp.txt";
    public static final String NEWCLAIM_TEMPLETNAME = "NewClaimeMailTemp.txt";
    public static final String SEARCHCLAIM_TEMPLETNAME = "ExistingClaimeMailTemp.txt";
    public static final String NULL = "<null>";
    public static final String LINE_SEPERATOR = "line.separator";
    public static final String EMAIL_DEV = "DEV";
    public static final String EMAIL_PROD = "PROD";
    public static final String HOST_NAME = "smtp_host_name"; 
    public static final String EMAIL_NOT_SENT_CODE = "EMAIL_NOT_SENT_CODE";
    public static final String CLAIMS = "CLAIMS";
    public static final String INSTITUTIONAL = "INSTITUTIONAL";
    public static final String ELODGEMENT_PDF_ATTACHMENT = "ELODGEMENT_PDF_ATTACHMENT";
    public static final String ELODGEMENT_CLAIMS_PDF_ATTACHMENT = "ElodgementClaimsPdfAttachment";
    public static final String ELODGEMENT_UW_PDF_ATTACHMENT = "ElodgementPdfAttachment";
    public static final String FILE_NAME_FOR_UW= "_eLodgeNewUW.pdf"; 
    public static final String FILE_NAME_FOR_CLAIMS= "_eLodgeNewCLAIM.pdf";
    public static final String ADDITIONAL_FILE_NAME_FOR_UW= "_eLodgeAdditionalUW.pdf"; 
    public static final String ADDITIONAL_FILE_NAME_FOR_CLAIMS= "_eLodgeAdditionalCLAIM.pdf";
    public static final String EMPTY= "";
    public static final String EMAIL_ID_PW = "EMAIL_ID_PASSWORD";
    public static final String EMAIL_ID_USERNAME = "EMAIL_ID_USERNAME";
    public static final String FILE_UPLOAD_PATH ="FILE_UPLOAD_PATH";	
	public static final String UPLOADFULLPATH = "UPLOADFULLPATH"; 
	public static final String SEND_FORGOT_PW_TEMPLETNAME = "SendForgotPasswordTemp.txt";
	public static final String SEND_CONFIRM_PW_TEMPLETNAME = "SendConfirmPasswordTemp.txt";
	public static final String SEND_SAVE_CONFIRM_PW_TEMPLETNAME = "SendSaveConfirmPasswordTemp.txt";

}
