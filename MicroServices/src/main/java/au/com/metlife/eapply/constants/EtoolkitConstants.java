package au.com.metlife.eapply.constants;

public class EtoolkitConstants {

	private EtoolkitConstants() {
	}

	public static final String FUND_CODE = "fundCode";
	public static final String FUND_NAME = "fundName";
	public static final String FUND_CATEGORY = "category";
	public static final String MODE_EDIT = "isEdit";
	public static final String USERID = "uid";
	public static final String STATUS_PENDING = "Pending";
	public static final String STATUS_SUBMITTED = "Submitted";
	public static final String STATUS_ACTIVE = "Active";
	public static final String STATUS_ALL = "All";
	public static final String STATUS_EMPTY = "";
	public static final String STATUS_EXPIRED = "Expired";
	public static final int ERR_DATABASE_NOT_AVAILABLE = 9501;
	public static final int ERR_DATA_ACCESS = 9503;
	public static final int ERR_BAD_INPUT_DATA = 9400;
	public static final int ERR_RESOURCE_NOT_FOUND = 9404;
	public static final int ERR_FILEFORMAT = 9505;
	public static final int ERR_UNCATEGORIZED = 9500;

	public static final String ETOOLKITEXCEPTION = "EToolKitException::";
	public static final String DATAACCESS_RESOURCE_FAILURE_EXCEPTION = "DataAccessResourceFailureException::";
	public static final String DATAACCESS_EXCEPTION = "DataAccessException::";
	public static final String NUMBER_FORMAT_EXCEPTION = "NumberFormatException::";
	public static final String RSQL_PARSER_EXCEPTION = "RSQLParserException::";
	public static final String IO_EXPECTION = "IOEXCEPTION::";
	public static final String URI_SYNTAX_EXPECTION = "URISyntaxException::";

	public static final String ERRMSG_DATABASE_NOT_AVAILABLE = "Data not accessible: Database may be down or connection lost";
	public static final String ERRMSG_INVALID_REQUEST = "Invalid Input Request Data Payload";
	public static final String ERRMSG_INVALID_FUNDCODE = "Invalid Fund Code";
	public static final String ERRMSG_DATA_ACCESS = "Data access error: %s";
	public static final String ERRMSG_INVALID_INPUT = "Invalid Input Code";

	// Mail Properties for Dashboard Start
	public static final String DASHBOARDMAIL_SUBJECT = "EMAIL_SUBJECT";
	public static final String DASHBOARD_MAIL_FUNDCODE = "FUNDNAME";
	public static final String FROM_ADDRESS = "fromsenderEmail";
	public static final String SMTP_HOST = "EMAIL_SMTP_HOST";
	public static final String SMTP_PORT = "EMAIL_SMTP_PORT";
	public static final String DASHBOARD_MAIL_APPLICATIONNUMBER = "APPLICATIONNUMBER";
	public static final String MEMBER_FIRSTNAME = "FIRSTNAME";
	public static final String MEMBER_LASTNAME = "LASTNAME";
	public static final String MEMBER_TOTAL_DEATHCOVER = "TOTALDEATHCOVER";
	public static final String MEMBER_ELIGIBLE_DEATHCOVER = "ELIGIBLEDEATHCOVER";
	public static final String MEMBER_TOTAL_TPDCOVER = "TOTALTPDCOVER";
	public static final String MEMBER_ELIGIBLE_TPDCOVER = "ELIGIBLETPDCOVER";
	public static final String MEMBER_TOTAL_IPCOVER = "TOTALIPCOVER";
	public static final String MEMBER_ELIGIBLE_IPCOVER = "ELIGIBLEIPCOVER";
	public static final String DASHBOARDMAIL_PROP_SUBJECT = "email_subj_dashboardEmail";
	public static final String MAIL_FUNDCODE = "<%FUNDNAME%>";
	public static final String MAIL_BROKERNUMBER = "<%BROKERNUMBER%>";
	public static final String DASHBOARD__PROP_MAIL_BODY = "email_dody_dashboardEmail";
	public static final String URl_INPUT_IDN = "<%INP_IDN%>";
	public static final String URL_INPUT_DATA = "<%INP_DATA%>";
	public static final String URL_S_DATA = "<%S_DATA%>";
	public static final String PROP_EMAIL_HOST = "email_host";
	public static final String PROP_EMAIL_PORT = "email_port";
	public static final String APPLICATION_URL = "application_url";
	public static final String URL = "URL";
	public static final String BODY_URL="<%URL%>";
	public static final String DASHBOARDBODY = "email_dody_dashboardEmail";
	public static final String DASHBOARD_FUNDCODE = "CORP";
	public static final String DASHBOARD_TOTAL_DEATH_COVER="<%TOTALDEATHCOVER%>";
	public static final String DASHBOARD_ELIGIBLE_DEATH_COVER="<%ELIGIBLEDEATHCOVER%>";
	public static final String DASHBOARD_TOTAL_TPD_COVER="<%TOTALTPDCOVER%>";
	public static final String DASHBOARD_ELIGIBLE_TPD_COVER="<%ELIGIBLETPDCOVER%>";
	public static final String DASHBOARD_TOTAL_IP_COVER="<%TOTALIPCOVER%>";
	public static final String DASHBOARD_ELIGIBLE_IP_COVER="<%ELIGIBLEIPCOVER%>";
	public static final String SAVE_EMAIL="save_body_content_client";
	public static final String SAVE_EMAIL_SUBJECT="email_subject_clientsave";
	public static final String SAVE_EMAIL_BODY_BROKER="save_broker_content";
	public static final String SAVE_FROM_SENDER="fromsenderemailSave";
	public static final String BROKER_NUMBER = " <%BrokerNumber%>";
	public static final String EVIEW_APPLICATION_URL = "application_eview_url";
	public static final String EVIEW_SOURCE = "eview";
	// Mail Properties for Dashboard End
	//MTAA Change Starts
	public static final String JSON_PARSE_EXCEPTION = "JsonParseException::";
	public static final String JSON_MAPPING_EXCEPTION = "JsonMappingException::";
	public static final String FILENOTFOUND_EXCEPTION = "JsonMappingException::";
	//MTTA Change ends
}
