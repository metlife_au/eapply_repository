package au.com.metlife.eapply.calculator.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SuperAnnuationVO implements Serializable {
	
private BigDecimal minSuperPerc;
	
	private BigDecimal actualSuperPerc;
	
	private BigDecimal superContribAmt;
	
	private BigDecimal superNPVAmt;

	public BigDecimal getActualSuperPerc() {
		return actualSuperPerc;
	}

	public void setActualSuperPerc(BigDecimal actualSuperPerc) {
		this.actualSuperPerc = actualSuperPerc;
	}

	public BigDecimal getMinSuperPerc() {
		return minSuperPerc;
	}

	public void setMinSuperPerc(BigDecimal minSuperPerc) {
		this.minSuperPerc = minSuperPerc;
	}

	public BigDecimal getSuperContribAmt() {
		return superContribAmt;
	}

	public void setSuperContribAmt(BigDecimal superContribAmt) {
		this.superContribAmt = superContribAmt;
	}

	public BigDecimal getSuperNPVAmt() {
		return superNPVAmt;
	}

	public void setSuperNPVAmt(BigDecimal superNPVAmt) {
		this.superNPVAmt = superNPVAmt;
	}

}
