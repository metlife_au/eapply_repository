(function(angular){
	'use strict';
    /* global channel, moment, Date */
	angular
		.module('MtaaApp')
		.controller('quoteLife', quoteLife);
	quoteLife.$inject=['$scope','$rootScope','$stateParams','$location','$http','$timeout','$window','urlService','persoanlDetailService','PersistenceService','mtaaUIConstants','$q','QuoteService','OccupationService','NewOccupationService','deathCoverService','tpdCoverService','ipCoverService','Upload','saveEapply','tokenNumService', 'auraResponseService','CalculateService','ngDialog','DownloadPDFService','printQuotePage'];
	function quoteLife($scope, $rootScope, $stateParams, $location, $http, $timeout, $window, urlService, persoanlDetailService, PersistenceService, mtaaUIConstants, $q, QuoteService, OccupationService, NewOccupationService, deathCoverService, tpdCoverService, ipCoverService, Upload, saveEapply, tokenNumService, auraResponseService, CalculateService, ngDialog, DownloadPDFService, printQuotePage){
		var vm = this;
		var fetchAppnum = true;
		var appNum;
		vm.TPD = {
				'requestType':'ICOVER',
				'partnerCode':'MTAA',
				'lastSavedOn':'lifeeventpage',
				'applicant': {
					'cover':[{},{},{}],
					'contactDetails':{}
				}
		};
		
		vm.constants = {
				image: mtaaUIConstants.path.images,
				phoneNumber: mtaaUIConstants.onboardDetails.phoneNumber,
				emailFormat: mtaaUIConstants.onboardDetails.emailFormat,
				postCodeFormat: mtaaUIConstants.onboardDetails.postCodeFormat,
				contactTypeOption: mtaaUIConstants.onboardDetails.contactTypeOption,
				titleOption: mtaaUIConstants.onboardDetails.titleOption,
				addressTypeOption: mtaaUIConstants.onboardDetails.addressTypeOption,
				stateOptions: mtaaUIConstants.onboardDetails.stateOptions
		};
		
		vm.eventList = [{
	    	'cde': 'MARR',
	    	'desc': 'Marriage',
	    	'docDesc': 'A marriage that is recognised as valid under the Marriage Act 1961(Cth).'
	    },
	    {
	    	'cde': 'BRTH',
	    	'desc': 'Birth or adoption of a child',
	    	'docDesc': 'Adopting or becoming the natural parent of a child.'
	    },
	    {
	    	'cde': 'FRST',
	    	'desc': 'Taking out or increasing a home mortgage',
	    	'docDesc': 'Obtaining either a new mortgage or increasing an existing mortgage on your residence.'
	    },
	    {
	    	'cde': 'DIVO',
	    	'desc': 'Divorce',
	    	'docDesc': 'Divorcing from a spouse.'
	    }];
		
		vm.init = function(){
			
			var defer = $q.defer();
			vm.urlList = urlService.getUrlList();
			vm.inputDetails = persoanlDetailService.getMemberDetails();
			vm.userAge = parseInt(moment().diff(moment(vm.inputDetails.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
			vm.ageLimit=vm.userAge;
			
			//setting value to JSON
			vm.TPD.applicant.memberType = vm.inputDetails.memberType;
			vm.TPD.applicant.firstName = vm.inputDetails.personalDetails.firstName;
			vm.TPD.applicant.lastName = vm.inputDetails.personalDetails.lastName;
			vm.TPD.applicant.gender = vm.inputDetails.personalDetails.gender;
			vm.TPD.applicant.birthDate =  vm.inputDetails.personalDetails.dateOfBirth;
			vm.TPD.applicant.dateJoinedFund = vm.inputDetails.dateJoined;
			vm.TPD.applicant.emailId = vm.inputDetails.contactDetails.emailAddress;
			
			//address
			vm.TPD.applicant.contactDetails.preferedContactTime = vm.inputDetails.contactDetails.prefContactTime;
			vm.TPD.applicant.contactDetails.preferedContacType = vm.inputDetails.contactDetails.prefContact;
			
			//documentAddress
			vm.TPD.applicant.documentAddress = 'Postal address\n\nMTAA Super\nLocked Bag 5134\nParramatta\nNSW 2124';
			
			vm.TPD.applicant.contactDetails.addressLine1 = vm.inputDetails.address.line1;
			vm.TPD.applicant.contactDetails.addressLine2 = vm.inputDetails.address.line2;
			vm.TPD.applicant.contactDetails.postCode = vm.inputDetails.address.postCode;
			vm.TPD.applicant.contactDetails.country = vm.inputDetails.address.country;
			vm.TPD.applicant.contactDetails.state = vm.inputDetails.address.state;
			vm.TPD.applicant.contactDetails.suburb = vm.inputDetails.address.suburb;
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
			
			vm.TPD.clientRefNumber = vm.inputDetails.clientRefNumber;
			vm.TPD.applicant.clientRefNumber = parseInt(vm.inputDetails.clientRefNumber);
			vm.doj = vm.inputDetails.dateJoined;
			vm.TPD.applicant.customerReferenceNumber = vm.inputDetails.contactDetails.fundEmailAddress;
			
			vm.deathCoverTransDetails = deathCoverService.getDeathCover();
			vm.tpdCoverTransDetails = tpdCoverService.getTpdCover();
			vm.ipCoverTransDetails = ipCoverService.getIpCover();
			
			if(vm.deathCoverTransDetails.amount){
				vm.TPD.applicant.cover[0].existingCoverAmount = vm.deathCoverTransDetails.amount;
		    }
			if(vm.deathCoverTransDetails.units){
				vm.TPD.applicant.cover[0].fixedUnit = vm.deathCoverTransDetails.units;
		    }
			if(vm.tpdCoverTransDetails.amount){
				vm.TPD.applicant.cover[1].existingCoverAmount = vm.tpdCoverTransDetails.amount;
		    }
			if(vm.tpdCoverTransDetails.units){
				vm.TPD.applicant.cover[1].fixedUnit = vm.tpdCoverTransDetails.units;
		    }
			if(vm.ipCoverTransDetails.amount){
				vm.TPD.applicant.cover[2].existingCoverAmount = vm.ipCoverTransDetails.amount;
				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				vm.TPD.applicant.cover[2].fixedUnit = vm.ipCoverTransDetails.units;
				
				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
				vm.TPD.applicant.cover[2].totalIpWaitingPeriod = vm.ipCoverTransDetails.waitingPeriod;
				vm.TPD.applicant.cover[2].totalIpBenefitPeriod = vm.ipCoverTransDetails.benefitPeriod;
				
		    }
			
			//benefit type
			vm.TPD.applicant.cover[0].benefitType = vm.deathCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[1].benefitType = vm.tpdCoverTransDetails.benefitType;
			vm.TPD.applicant.cover[2].benefitType = vm.ipCoverTransDetails.benefitType;
			
			vm.TPD.applicant.cover[0].frequencyCostType = 'Monthly';
			vm.TPD.applicant.cover[1].frequencyCostType = 'Monthly';
			vm.TPD.applicant.cover[2].frequencyCostType = 'Monthly';
			
			//coverDecision - set to ACC since no AURA
			vm.TPD.applicant.cover[0].coverDecision = 'ACC';
			vm.TPD.applicant.cover[1].coverDecision = 'ACC';
			vm.TPD.applicant.cover[2].coverDecision = 'ACC';
			
			QuoteService.getList(vm.urlList.quoteUrl, 'MTAA').then(function(res) {
				vm.IndustryOptions = res.data;
				defer.resolve(res);
			},function(err) {
				console.log('Error while getting industry options '+ JSON.stringify(err));
				defer.reject(err);
			});	
			
			return defer.promise;
		};
		
		vm.init();
		
		vm.init().then(function() {
			
			if($stateParams.mode === '3' || $stateParams.mode === '2'){
				vm.TPD = PersistenceService.getLifeEventDetails();
				
				vm.TPD.applicant.industryType = vm.TPD.applicant.industryType === '' ? null : vm.TPD.applicant.industryType;
	            OccupationService.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.TPD.applicant.industryType).then(function(res) {
	                vm.OccupationList = res.data;
	                vm.renderOccupationQuestions();
	            }, function(err) {
	                console.info('Error while fetching occupations ' + JSON.stringify(err));
	            });
	            
	            var tempEvent = vm.eventList.filter(function(obj){
		    		return obj.cde === vm.TPD.applicant.lifeEvent;
		    	});
	            
	            vm.lifeEventSelected = tempEvent[0];
	            
	            vm.files = vm.TPD.lifeEventDocuments;
	            vm.uploadedFiles = vm.files;
	            PersistenceService.setUploadedFileDetails(vm.files);
	            
	            $timeout(function() {
					$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('show');
					$('#OccupationSection').collapse('show');
					$('#LifeEventSection').collapse('show');
					
					if(vm.TPD.applicant.documentEvidence === 'No'){
						vm.TPD.ackDocument = true;
		            	$('#acknowledgeDocAdressCheck').addClass('active');
		            }else{
		            	vm.TPD.ackDocument = false;
		            	$('#acknowledgeDocAdressCheck').removeClass('active');
		            }
					
				}, 1000);
				
			}else{
				$('#collapseprivacy').collapse('hide');
				$('#PersonalDetailsSection').collapse('hide');
				$('#OccupationSection').collapse('hide');
				$('#LifeEventSection').collapse('hide');
			}
			
		});
		
		vm.changePrefContactType = function(){
			
			if(vm.TPD.applicant.contactDetails.preferedContacType === '1'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.mobilePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '2'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.homePhone;
			}else if(vm.TPD.applicant.contactDetails.preferedContacType === '3'){
				vm.TPD.applicant.contactDetails.preferedContactNumber = vm.inputDetails.contactDetails.workPhone;
			}else{
				vm.TPD.applicant.contactDetails.preferedContactNumber = '';
			}
		};
		
		vm.getOccupations = function(){
	    	vm.TPD.applicant.occupation = '';
            vm.TPD.applicant.otherOccupation = '';
            vm.TPD.applicant.industryType = vm.TPD.applicant.industryType === '' ? null : vm.TPD.applicant.industryType;
            OccupationService.getOccupationList(vm.urlList.occupationUrl, 'MTAA', vm.TPD.applicant.industryType).then(function(res) {
                vm.OccupationList = res.data;
                vm.TPD.applicant.occupation = '';
            }, function(err) {
                console.info('Error while fetching occupations ' + JSON.stringify(err));
            });
	    };
	    
	    vm.invalidSalAmount = false;
		vm.checkValidSalary = function(){
			
			vm.renderOccupationQuestions();
			
			$timeout(function() {
				vm.invalidSalAmount = false;
				if(parseInt(vm.TPD.applicant.annualSalary) === 0){
					vm.invalidSalAmount = true;
				}
			},100);
		};
	    
	    vm.getTransCategoryFromDB = function(fromOccupation){
			  
			  if(vm.otherOccupationObj){
				  vm.otherOccupationObj.transferotherOccupation = '';
			  }
			  
			  if(vm.TPD.applicant.occupation !== undefined){
				  vm.renderOccupationQuestions();
			  } 
		  };
		
		vm.renderOccupationQuestions = function(){
			  
			  //var occupationDetailsTransferFormFields;
			/*vm.TPD.applicant.cover[0].occupationRating = vm.deathCoverTransDetails.occRating === '' ? 'General' : vm.deathCoverTransDetails.occRating;
			vm.TPD.applicant.cover[1].occupationRating = vm.tpdCoverTransDetails.occRating === '' ? vm.deathCoverTransDetails.occRating : vm.tpdCoverTransDetails.occRating;
			vm.TPD.applicant.cover[2].occupationRating = vm.ipCoverTransDetails.occRating === '' ? vm.deathCoverTransDetails.occRating : vm.ipCoverTransDetails.occRating;
			*/
			  if(vm.OccupationList){
				  var selectedOcc = vm.OccupationList.filter(function(obj){
					  return obj.occupationName === vm.TPD.applicant.occupation;
		          });
		          var selectedOccObj = selectedOcc[0];
		          vm.occupUpgradeNotEligible = false;
		          if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
		              vm.showWithinOfficeTransferQuestion = true;
		        	  vm.showTertiaryTransferQuestion = true;
		              vm.showHazardousTransferQuestion = false;
		              vm.showOutsideOffice = false;
		              
		              vm.TPD.applicant.spendTimeOutside = '';
		              vm.TPD.applicant.workDuties = '';
		          
		          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'true' && selectedOccObj.manualFlag.toLowerCase() === 'true'){
		              vm.showWithinOfficeTransferQuestion = false;
		              vm.showTertiaryTransferQuestion = false;
		              vm.showHazardousTransferQuestion = true;
		              vm.showOutsideOffice = true;
		              
		              vm.TPD.applicant.occupationDuties = '';
		              vm.TPD.applicant.tertiaryQue = '';
		              
		          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'true'){	        	  
		              vm.showTertiaryTransferQuestion = false;
		              vm.showHazardousTransferQuestion = true;
		              vm.showOutsideOffice = true;
		              
		              vm.TPD.applicant.tertiaryQue = '';
		              
		          }else if(selectedOccObj.professionalFlag.toLowerCase() === 'false' && selectedOccObj.manualFlag.toLowerCase() === 'false'){
		        	  vm.showWithinOfficeTransferQuestion = false;
		              vm.showTertiaryTransferQuestion = false;
		              vm.showHazardousTransferQuestion = false;
		              vm.showOutsideOffice = false;
		              
		              vm.TPD.applicant.occupationDuties = '';
		              vm.TPD.applicant.tertiaryQue = '';
		              vm.TPD.applicant.workDuties = '';
		              vm.TPD.applicant.spendTimeOutside = '';
		          }
		      }else{
		    	  vm.showWithinOfficeTransferQuestion = false;
		          vm.showTertiaryTransferQuestion = false;
		          vm.showHazardousTransferQuestion = false;
		          vm.showOutsideOffice = false;
		          
		          vm.TPD.applicant.occupationDuties = '';
	              vm.TPD.applicant.tertiaryQue = '';
	              vm.TPD.applicant.workDuties = '';
	              vm.TPD.applicant.spendTimeOutside = '';
		      }
			  
			  vm.nonManualFlag = false;
			  vm.professionalFlag = false;			  
			  
			  if(vm.TPD.applicant.workDuties === 'No' && vm.TPD.applicant.spendTimeOutside === 'No'){
				  vm.TPD.applicant.cover[0].occupationRating = 'Non Manual';
		          vm.TPD.applicant.cover[1].occupationRating = 'Non Manual';
		          vm.TPD.applicant.cover[2].occupationRating = 'Non Manual';
		          vm.nonManualFlag = true;
		      }
			  
			  if(parseInt(vm.TPD.applicant.annualSalary) > 120000 && vm.TPD.applicant.occupationDuties === 'Yes' && vm.TPD.applicant.tertiaryQue=== 'Yes'){
				  vm.TPD.applicant.cover[0].occupationRating = 'Professional';
		          vm.TPD.applicant.cover[1].occupationRating = 'Professional';
		          vm.TPD.applicant.cover[2].occupationRating = 'Professional';
		          vm.professionalFlag = true;
			  }
			  
			  if(!vm.nonManualFlag && !vm.professionalFlag){
				  vm.TPD.applicant.cover[0].occupationRating = 'General';
		          vm.TPD.applicant.cover[1].occupationRating = 'General';
		          vm.TPD.applicant.cover[2].occupationRating = 'General';
			  }
			  
		    };
		
		vm.checkDodState = function() {
			vm.TPD.dodCheck = !vm.TPD.dodCheck;
        };

        vm.checkPrivacyState = function() {
        	vm.TPD.privacyCheck =  !vm.TPD.privacyCheck;
        };
        
        vm.fetchAppnum = function(){
        	
        	if(fetchAppnum){
        		fetchAppnum = false;
        		appNum = PersistenceService.getAppNumber();
        		vm.TPD.applicatioNumber = appNum;
        	}        	
        };
        
        vm.saveDataForPersistence = function(){
        	
        	vm.fetchAppnum();
        	
            var defer = $q.defer();
            
            var selectedIndustry = vm.IndustryOptions.filter(function(obj){
                return vm.TPD.applicant.industryType === obj.key;
            });
            
            vm.TPD.applicant.occupationCode = selectedIndustry[0].value;            
            vm.TPD.applicant.lifeEvent = vm.lifeEventSelected.cde;
            vm.TPD.applicant.eventDesc = vm.lifeEventSelected.desc;
            
             vm.submitFiles().then(function(res) {
            	vm.TPD.lifeEventDocuments = PersistenceService.getUploadedFileDetails();
            	console.log(vm.TPD.lifeEventDocuments);
            	PersistenceService.setLifeEventDetails(vm.TPD);
            	defer.resolve(res);
             }, function(err) {
            	 defer.reject(err);
            });
            
            return defer.promise;
          
        };  
        
        
        vm.saveQuoteLifeEvent = function(){
        	
        	vm.checkForFileUpload();
        	
        	if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && vm.lifeEvent.$valid){
        		
        		if(!vm.fileNotUploadedError && !vm.invalidSalAmount && vm.TPD.applicant.eventAlreadyApplied !== 'Yes'){
        			
        			vm.saveDataForPersistence().then(function() {
        				
        				var savedObject =  PersistenceService.getLifeEventDetails();
        				
        				savedObject.lastSavedOn = 'lifeeventpage';
        				
        				auraResponseService.setResponse(savedObject);
        				
        				console.log(JSON.stringify(savedObject));
        				
        				saveEapply.reqObj(vm.urlList.saveEapplyUrlNew).then(function(response) {
        					console.log(response.data);
        					vm.showSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
        				},function(err) {
        					console.log('Something went wrong while saving...'+JSON.stringify(err));
	                    });
        			});
        		}
        	}
        };
        
        vm.continueToNextPage = function(){
        	if(vm.deathCoverTransDetails.type === '1' && vm.tpdCoverTransDetails.type === '1'){
        		vm.calculateForUnits();
        	}else{        		
        		vm.calculatePerUnitAmount();
        	}
        };
        
        vm.calculateForFixed = function(){
        	
        	var deathFixedAmount = parseInt(vm.deathPerUnitAmount*vm.TPD.applicant.cover[0].additionalUnit) + parseInt(vm.TPD.applicant.cover[0].existingCoverAmount);
        	var tpdFixedAmount = parseInt(vm.tpdPerUnitAmount*vm.TPD.applicant.cover[1].additionalUnit) + parseInt(vm.TPD.applicant.cover[1].existingCoverAmount);

      		var ruleModel = {
             		'age': vm.userAge,
             		'fundCode': 'MTAA',
             		'gender': vm.TPD.applicant.gender,
             		'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
             		'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
             		'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
             		'smoker': false,
             		'deathFixedCost': null,
             		'deathUnitsCost': null,
             		'tpdFixedCost': null,
             		'tpdUnitsCost': null,
             		'ipUnits': null,
             		'ipUnitsCost': null,
             		'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
             		'memberType': null,
             		'manageType': 'ICOVER',
             		'ipCoverType': 'IpFixed',
             		'deathCoverType':'DcFixed',
             		'tpdCoverType':'TPDFixed',
             		'ipWaitingPeriod': vm.TPD.applicant.cover[2].additionalIpWaitingPeriod,
             		'ipBenefitPeriod': vm.TPD.applicant.cover[2].additionalIpBenefitPeriod,
             		'deathFixedAmount' :parseInt(deathFixedAmount),
             		'tpdFixedAmount': parseInt(tpdFixedAmount),
             		'ipFixedAmount' : parseInt(vm.TPD.applicant.cover[2].existingCoverAmount)
             	};
      		
      		
      		console.log(JSON.stringify(ruleModel));

      		CalculateService.calculate(ruleModel, vm.urlList.calculateUrl).then(function(res){
      			var premium = res.data;
        		for(var i = 0; i < premium.length; i++){
        			if(premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised'){
        				vm.TPD.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
        				vm.TPD.applicant.cover[0].cost = premium[i].cost;
        				vm.TPD.applicant.cover[0].coverCategory = premium[i].coverType;
        			} else if(premium[i].coverType === 'TPDFixed' || premium[i].coverType === 'TPDUnitised'){
        				vm.TPD.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
        				if(vm.TPD.applicant.cover[1].additionalCoverAmount >5000000){
        					vm.TPD.applicant.cover[1].additionalCoverAmount= 5000000;
        				} 
        				vm.TPD.applicant.cover[1].cost = premium[i].cost;
        				vm.TPD.applicant.cover[1].coverCategory = premium[i].coverType;
        			} else if(premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised'){
        				vm.TPD.applicant.cover[2].additionalCoverAmount = (premium[i].coverAmount==null?0:premium[i].coverAmount);
        				vm.TPD.applicant.cover[2].cost = (premium[i].cost==null?0:premium[i].cost);
        				vm.TPD.applicant.cover[2].coverCategory = premium[i].coverType;
        			}
        		}
        		
        		vm.TPD.totalMonthlyPremium = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost) + parseFloat(vm.TPD.applicant.cover[2].cost);        		
        		console.log(vm.TPD.totalMonthlyPremium);
        		
        		vm.proceedToAura();
        		
            
      		}, function(err){
      			console.info('Error while calculating life event premium..', JSON.stringify(err));
      		});
        	
        };
        
        vm.calculatePerUnitAmount = function(){

      		var ruleModel = {
             		'age': vm.userAge,
             		'fundCode': 'MTAA',
             		'gender': vm.TPD.applicant.gender,
             		'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
             		'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
             		'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
             		'smoker': false,
             		'deathUnits':1,
             		'deathFixedCost': null,
             		'deathUnitsCost': null,
             		'tpdUnits': 1,
             		'tpdFixedCost': null,
             		'tpdUnitsCost': null,
             		'ipUnits': null,
             		'ipUnitsCost': null,
             		'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
             		'memberType': null,
             		'manageType': 'ICOVER',
             		'deathCoverType':'DcUnitised',
             		'tpdCoverType':'TPDUnitised',
             		'ipCoverType':'IpUnitised',
             		'ipWaitingPeriod': vm.TPD.applicant.cover[2].additionalIpWaitingPeriod,
             		'ipBenefitPeriod': vm.TPD.applicant.cover[2].additionalIpBenefitPeriod		
           };
      		
      		CalculateService.calculate(ruleModel, vm.urlList.calculateUrl).then(function(res){
      			var premium = res.data;
        		for(var i = 0; i < premium.length; i++){
        			if(premium[i].coverType === 'DcUnitised'){
        				vm.deathPerUnitAmount = premium[i].coverAmount;
        			} else if(premium[i].coverType === 'TPDUnitised'){
        				vm.tpdPerUnitAmount = premium[i].coverAmount;
        			}
        		}
        		vm.calculateForFixed();
      		}, function(err){
      			console.info('Error while calculating life event premium..', JSON.stringify(err));
      		});
      	
        };
        
        vm.calculateForUnits = function(){

      		var ruleModel = {
             		'age': vm.userAge,
             		'fundCode': 'MTAA',
             		'gender': vm.TPD.applicant.gender,
             		'deathOccCategory': vm.TPD.applicant.cover[0].occupationRating,
             		'tpdOccCategory': vm.TPD.applicant.cover[1].occupationRating,
             		'ipOccCategory': vm.TPD.applicant.cover[2].occupationRating,
             		'smoker': false,
             		'deathFixedCost': null,
             		'deathUnitsCost': null,
             		'tpdFixedCost': null,
             		'tpdUnitsCost': null,
             		'ipUnits': null,
             		'ipUnitsCost': null,
             		'premiumFrequency': vm.TPD.applicant.cover[0].frequencyCostType,
             		'memberType': null,
             		'manageType': 'ICOVER',
             		'ipCoverType': 'IpFixed',
             		'ipWaitingPeriod': vm.TPD.applicant.cover[2].additionalIpWaitingPeriod,
             		'ipBenefitPeriod': vm.TPD.applicant.cover[2].additionalIpBenefitPeriod
             	};
      		
      		ruleModel.deathCoverType = 'DcUnitised';
      		ruleModel.tpdCoverType = 'TPDUnitised';
      		
      		if(vm.deathCoverTransDetails.units === undefined || vm.deathCoverTransDetails.units === ''){
      			ruleModel.deathUnits = 0;
      		}else{
      			ruleModel.deathUnits = parseInt(vm.deathCoverTransDetails.units) + parseInt(vm.TPD.applicant.cover[0].additionalUnit);
      		}
      		
      		if(vm.tpdCoverTransDetails.units === undefined || vm.tpdCoverTransDetails.units === ''){
      			ruleModel.tpdUnits = 0;
      		}else{
      			ruleModel.tpdUnits = parseInt(vm.tpdCoverTransDetails.units) + parseInt(vm.TPD.applicant.cover[1].additionalUnit);
      		} 
      		
      		ruleModel.ipUnits = 0;
      		ruleModel.ipFixedAmount = 0;

      		/*if(vm.ipCoverTransDetails.amount === undefined || vm.ipCoverTransDetails.amount === ''){
      			ruleModel.ipFixedAmount = 0;
      		} else{
      			ruleModel.ipFixedAmount = parseInt(vm.ipCoverTransDetails.amount);
      		}*/
      		
      		console.log(JSON.stringify(ruleModel));

      		CalculateService.calculate(ruleModel, vm.urlList.calculateUrl).then(function(res){
      			var premium = res.data;
        		for(var i = 0; i < premium.length; i++){
        			if(premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised'){
        				vm.TPD.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
        				vm.TPD.applicant.cover[0].cost = premium[i].cost;
        				vm.TPD.applicant.cover[0].coverCategory = premium[i].coverType;
        			} else if(premium[i].coverType === 'TPDFixed' || premium[i].coverType === 'TPDUnitised'){
        				vm.TPD.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
        				if(vm.TPD.applicant.cover[1].additionalCoverAmount >5000000){
        					vm.TPD.applicant.cover[0].additionalCoverAmount= 5000000;
        				} 
        				vm.TPD.applicant.cover[1].cost = premium[i].cost;
        				vm.TPD.applicant.cover[1].coverCategory = premium[i].coverType;
        			} else if(premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised'){
        				vm.TPD.applicant.cover[2].additionalCoverAmount = (premium[i].coverAmount==null?0:premium[i].coverAmount);
        				vm.TPD.applicant.cover[2].cost = (premium[i].cost==null?0:premium[i].cost);
        				vm.TPD.applicant.cover[2].coverCategory = premium[i].coverType;
        			}
        		}
        		
        		vm.TPD.totalMonthlyPremium = parseFloat(vm.TPD.applicant.cover[0].cost) + parseFloat(vm.TPD.applicant.cover[1].cost) + parseFloat(vm.TPD.applicant.cover[2].cost);        		
        		console.log(vm.TPD.totalMonthlyPremium);
        		
        		vm.proceedToAura();
        		
            
      		}, function(err){
      			console.info('Error while calculating life event premium..', JSON.stringify(err));
      		});
      	
        };
        
        vm.proceedToAura = function(){
        	
        	vm.checkForFileUpload();
    		 
    		 if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && vm.lifeEvent.$valid){
    			 
    			 if(!vm.fileNotUploadedError && !vm.invalidSalAmount && vm.TPD.applicant.eventAlreadyApplied !== 'Yes'){
    				 $timeout(function(){
    					 vm.saveDataForPersistence().then(function() {
    						$location.path('/summaryLife/1');
    					 }, function(err) {});
    				}, 1000);
    	
    	        }else{
    	        	return false;
    	        }
    	    }
    	  
        };
        
        vm.generatePDF = function(){
        	
        	vm.checkForFileUpload();
        	
        	if(vm.coverDetailsTransferForm.$valid && vm.occupationDetailsTransferForm.$valid && vm.lifeEvent.$valid){
        		
        		if(!vm.fileNotUploadedError && !vm.invalidSalAmount){
        			
        			$timeout(function(){
        				
        				vm.saveDataForPersistence().then(function() {
        			    	
        			    	var quoteObject =  PersistenceService.getLifeEventDetails();
        			    	
        			    	if(quoteObject != null){
        			    		
        			    		auraResponseService.setResponse(quoteObject);
        			    		
        			    		printQuotePage.reqObj(vm.urlList.printQuotePageNew).then(function(response){
        			    			PersistenceService.setPDFLocation(response.data.clientPDFLocation);
        			    			vm.downloadPDF();
        			    		},function(err){
        			    			console.log('Something went wrong while saving...'+JSON.stringify(err));
        			    		});	              
        			    	}
        			    	
        		    	});
        				
        			}, 1000);
        			
        		}else{
    	        	return false;
    	        }
    	    }
	    };
	    
	    vm.downloadPDF = function(){
	    	var pdfLocation =null;
	    	var filename = null;
	    	var a = null;
	    	pdfLocation = PersistenceService.getPDFLocation();
	    	console.log(pdfLocation+'pdfLocation');
	    	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	    	a = document.createElement('a');
	    	document.body.appendChild(a);
	    	DownloadPDFService.download(vm.urlList.downloadUrl,pdfLocation).then(function(res){
	    		
	    		if (navigator.appVersion.toString().indexOf('.NET') > 0) {
	    			window.navigator.msSaveBlob(res.data.response,filename);
	    		}else{
	    			var fileURL = URL.createObjectURL(res.data.response);
	    			a.href = fileURL;
	    			a.download = filename;
	    			a.click();
	    		}
	    	}, function(err){
	    		console.log('Error downloading the PDF ' + err);
	    	});
	    };
        
        vm.showSaveAndExitPopUp = function (hhText) {

	          var dialog1 = ngDialog.open({
	                template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
	              className: 'ngdialog-theme-plain custom-width',
	              preCloseCallback: function(value) {
	                     var url = '/landing';
	                     $location.path( url );
	                     return true;
	              },
	              plain: true
	          });
	          dialog1.closePromise.then(function (data) {
	            console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
	          });
	        
	    };
        
        //file upload codes
        
        vm.files = [];
	    vm.selectedFile = null;
	    vm.uploadFiles = function(files, errFiles) {

	    	vm.fileSizeErrFlag = false;
	    	vm.fileFormatError = errFiles.length > 0 ? true : false;
	    	vm.selectedFile =  files[0];
	    	
	    };
	    
	    vm.checkForFileUpload = function(){
	    	  if(vm.files.length > 0){
	    		  vm.fileNotUploadedError = false;
	    	  }else{
	    		  if(vm.TPD.applicant.documentEvidence === 'Yes'){
	    			  vm.fileNotUploadedError = true;            
	    		  }else{
	    			  vm.fileNotUploadedError = false;
	    		  }
	    	  }  
	      };
        
        vm.addFilesToStack = function () {
		      var fileSize = (vm.selectedFile.size / 1048576).toFixed(3);
		      if(fileSize > 10) {
		    	  vm.fileSizeErrFlag=true;
		    	  vm.fileSizeErrorMsg ='File size should not be more than 10MB';
		    	  vm.selectedFile = null;
		        return;
		      }else{
		    	  vm.fileSizeErrFlag=false;
		      }
		      if(!vm.files){
		    	  vm.files = [];
		      }		    	 
		      vm.files.push(vm.selectedFile);
		      PersistenceService.setUploadedFileDetails(vm.files);
		      vm.fileNotUploadedError = false;
		      vm.selectedFile = null;
		    
	    };
        
        vm.removeFile = function(index) {
	    	vm.files.splice(index, 1);
	    	PersistenceService.setUploadedFileDetails(vm.files);
	    	if(vm.files.length < 1) {
	    		vm.fileNotUploadedError = true;
	    	}
	  
	    };
	    
        vm.submitFiles = function () {
	    	
	    	vm.uploadedFiles = vm.uploadedFiles || [];
	    	var defer = $q.defer();
	        
	    	if(!vm.files){
	          vm.files = [];
	        }
	    	
	        if(!vm.files.length) {
	          defer.resolve({});
	        }
	        
	        var upload;
	        var numOfFiles = vm.files.length;
	        angular.forEach(vm.files, function(file, index) {
	        	if(Upload.isFile(file)) {
	        		upload = Upload.http({
	        			url: vm.urlList.fileUploadUrlNew,
	        			headers : {
	        				'Content-Type': file.name,
	        				'Authorization':tokenNumService.getTokenId()
	        			},
	        			data: file
	        		});
	        		
	        		upload.then(function(res){
	        			numOfFiles--;
	        			vm.uploadedFiles[index] = res.data;
	        			if(numOfFiles === 0){
	        				PersistenceService.setUploadedFileDetails(vm.uploadedFiles);
	        				defer.resolve(res);
	        			}
	        		}, function(err){
	        			console.log('Error uploading the file ' + err);
	        			defer.reject(err);
	        		});
	        	}else{
	        		numOfFiles--;
	        		if(numOfFiles === 0) {
	        			PersistenceService.setUploadedFileDetails(vm.uploadedFiles);
	        			defer.resolve({});
	        		}
	        	}
	        });
	        return defer.promise;
	      
	    };
        
        
        vm.coverDetailsTransferFormFields = ['contactEmail', 'contactType', 'contactPhone', 'contactPrefTime', 'gender'];
	    vm.occupationDetailsTransferFormFields = ['fifteenHrsQuestion','ownBusinessQue','ownBusinessYesQue','ownBusinessNoQue','areyouperCitzQuestion','industry','occupation','otherOccupation','hazardousQuestion','outsideOffice','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
	    vm.lifeEventFormFields = ['event','eventDate','eventAlreadyApplied','deathadditionalUnit', 'TPDadditionalUnit', 'documentName'];
	    
        vm.checkPreviousMandatoryFields  = function (elementName,formName){
        	
        	var transferFormFields = [];
	    	
	    	if(formName === 'coverDetailsTransferForm'){
	    		transferFormFields = vm.coverDetailsTransferFormFields;
	    	}else if(formName === 'occupationDetailsTransferForm'){
	    		transferFormFields = vm.occupationDetailsTransferFormFields;
	        }else if(formName === 'lifeEvent'){	        	
	        	transferFormFields = vm.lifeEventFormFields;	        	
	        }
	    	
	        var inx = transferFormFields.indexOf(elementName);	        
	        
	        if(inx > 0){
	        	for(var i = 0; i < inx ; i++){
	        		 if (vm[formName][transferFormFields[i]]){
	        			 vm[formName][transferFormFields[i]].$touched = true;
	        		 }                        
	        	}
	        }
	      
        };
        
        
        vm.isCollapsible = function(targetEle, event) {
	    	
	    	var formdutyValid = vm.formduty.$valid;
	    	var transferPrivacyPolicyFormValid = vm.transferPrivacyPolicyForm.$valid;
	    	var coverDetailsTransferFormValid = vm.coverDetailsTransferForm.$valid;
	    	var occupationDetailsTransferFormValid = vm.occupationDetailsTransferForm.$valid;
	    	var stopPropagation = false;
	    	
	    	
	    	if(targetEle === 'collapseprivacy' && !formdutyValid){
	    		vm.formduty.$submitted = true;
	    		stopPropagation = true;
	        }else if(targetEle === 'PersonalDetailsSection' && (!formdutyValid || !transferPrivacyPolicyFormValid)) {
	        	vm.transferPrivacyPolicyForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'OccupationSection' && (!formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid)) {
	        	vm.coverDetailsTransferForm.$submitted = true;
	        	stopPropagation = true;
	        }else if(targetEle === 'LifeEventSection' && (vm.invalidSalAmount || !formdutyValid || !transferPrivacyPolicyFormValid || !coverDetailsTransferFormValid || !occupationDetailsTransferFormValid)){
	        	vm.occupationDetailsTransferForm.$submitted = true;
	        	stopPropagation = true;
	        }
	        
	        if(stopPropagation){
	        	event.stopPropagation();
	        	return false;
	        }
	        
	    };
	    
	    vm.clickToOpen = function (hhText) {
	    	
	    	var templateContent = '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4>';
	    	templateContent += '<div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p>';
	    	templateContent += '<div id="tips_text">'+hhText+'</div><p></p></div></div></div>';
	    	templateContent += '<div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12">';
	    	templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>';
	    	
	    	

			var dialog = ngDialog.open({
				template: templateContent,
				className: 'ngdialog-theme-plain',
				plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		  
	  };
		
		//watchers for form valid and invalid
        
        $scope.$watch('vm.TPD.dodCheck' , function(newVal, oldVal) {
        	$timeout(function() {
	        	if(vm.TPD.dodCheck) {
		        	$('#collapseprivacy').collapse('show');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
					$('#LifeEventSection').collapse('hide');
	        	}else{
	        		$('#collapseprivacy').collapse('hide');
					$('#PersonalDetailsSection').collapse('hide');
					$('#OccupationSection').collapse('hide');
					$('#LifeEventSection').collapse('hide');
	        	}
        	},10);
        });
        
        $scope.$watch('vm.TPD.privacyCheck', function(newVal, oldVal) {
        	$timeout(function() {
	        	if(vm.TPD.privacyCheck) {
	        		$('#PersonalDetailsSection').collapse('show');
	        		$('#OccupationSection').collapse('hide');
	        		$('#LifeEventSection').collapse('hide');
	        	}else{
	        		$('#PersonalDetailsSection').collapse('hide');
	        		$('#OccupationSection').collapse('hide');
	        		$('#LifeEventSection').collapse('hide');
	        	}
        	},10);
       });
        
        $scope.$watch('vm.coverDetailsTransferForm.$valid', function(newVal, oldVal) {
        	  if(newVal) {
        		  $('#OccupationSection').collapse('show');
        		  $('#LifeEventSection').collapse('hide');
        	  }
          });
          
          $scope.$watch('vm.coverDetailsTransferForm.$invalid', function(newVal, oldVal) {
        	  if(newVal) {
        		  $('#OccupationSection').collapse('hide');
        		  $('#LifeEventSection').collapse('hide');        		  
        	  }
          });
          
          $scope.$watch('vm.occupationDetailsTransferForm.$valid', function(newVal, oldVal) {
       	   $timeout(function() {
   	      	  if(newVal && !vm.invalidSalAmount) {
   	      		  $('#LifeEventSection').collapse('show');   	      		  
   	      	  }
       	   },100);
           });
           
           $scope.$watch('vm.occupationDetailsTransferForm.$invalid', function(newVal, oldVal) {
         	  if(newVal) {
         		  $('#LifeEventSection').collapse('hide');
         	  }
           });
           
           $scope.$watch('vm.invalidSalAmount', function(newVal, oldVal) {
           	if(newVal) {
           		$timeout(function() {
           			$('#LifeEventSection').collapse('hide');
           		},100);
           	 }else if(vm.occupationDetailsTransferForm.$valid){
           		 $('#LifeEventSection').collapse('show');
           	 }
           });
           
           //date methods
           
           vm.isValidEventDate = function() {
           	vm.TPD.applicant.lifeEventDate = vm.TPD.applicant.lifeEventDate.replace(/[^0-9\/]/g, '');          
           };
           
           vm.datePadding = function (s) {
           	return (s < 10) ? '0' + s : s;
           };
           
           vm.convertDate = function(inputFormat){
           	
           	var d = inputFormat.split('/');
               var formatedDate = [vm.datePadding(parseInt(d[1])), vm.datePadding(parseInt(d[0])), d[2]].join('/');
               var regEx = /^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$/;
               return regEx.test(formatedDate) ? formatedDate : '';          
           };
           
           Date.prototype.withoutTime = function () {
           	
           	var d = new Date(this);
               d.setHours(0, 0, 0, 0);
               return d;          
           };
           
           vm.isValidDate = function(userEventDate) {
        	   return userEventDate instanceof Date && isFinite(userEventDate);
           };
           
           vm.validateEventDate = function(eventDate, formName, inputName) {
           	
           	vm[formName][inputName].$setTouched();
           	
           	if(!eventDate){
           		return false;
           	}
           	var validDate,d;
           	var dateObj = new Date();
           	var sixtyDaysOldDate = dateObj.setDate(dateObj.getDate() - 60);
           	var userEventDate = new Date(vm.convertDate(eventDate));
           	var joiningDate = new Date(vm.convertDate(vm.doj));
           	var dateYear = moment(vm.TPD.applicant.lifeEventDate,'DD/MM/YYYY').year();
           	
           	vm[formName][inputName].$setValidity('futureDate', true);
               vm[formName][inputName].$setValidity('notInRange', true);
               vm[formName][inputName].$setValidity('notInRangeWithJoining', true);
               
               
               if(!vm.isValidDate(userEventDate)) {
               	
               	vm.TPD.applicant.lifeEventDate = '';
               	
               }else if(userEventDate.withoutTime() > new Date().withoutTime()){

               	
               	var isValidDate = userEventDate.withoutTime() < new Date().withoutTime();
               	vm[formName][inputName].$setValidity('futureDate', isValidDate);
               	d = eventDate.split('/');
                vm.TPD.applicant.lifeEventDate = [vm.datePadding(parseInt(d[0])), vm.datePadding(parseInt(d[1])), dateYear].join('/');
               
               }else if(userEventDate.withoutTime() < new Date(joiningDate).withoutTime()){
               	
               	validDate = userEventDate.withoutTime() >= new Date(joiningDate).withoutTime();
               	vm[formName][inputName].$setValidity('notInRangeWithJoining', validDate);
               	d = eventDate.split('/');
               	
               	if(isNaN(dateYear)){
               		vm[formName][inputName].$setValidity('futureDate', false);
               	}else{
               		vm.TPD.applicant.lifeEventDate = [vm.datePadding(parseInt(d[0])), vm.datePadding(parseInt(d[1])), dateYear].join('/');
               	}            	  
               
               }else {
               	
               	validDate = userEventDate.withoutTime() >= new Date(sixtyDaysOldDate).withoutTime();
                   vm[formName][inputName].$setValidity('notInRange', validDate);
                   d = eventDate.split('/');
                   
                   if(isNaN(dateYear)){
                   	vm[formName][inputName].$setValidity('futureDate', false);
                   }else{
                   	vm.TPD.applicant.lifeEventDate = [vm.datePadding(parseInt(d[0])), vm.datePadding(parseInt(d[1])), dateYear].join('/');
                   }
                 }            
           };
		
	}
})(angular);

 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */
