/* ------------------------------------------------------------------------------------------------- 
 * Description:   IndvQuoteBean class is the backing bean for UC021-Get Quote. 
 * -------------------------------------------------------------------------------------------------
 * Copyright@ 2013 Metlife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :     Cognizant
 * Created:     15/05/2011
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -------------------------------------------------------------------------------------------------
 * Date				Author					Description   
 * 01/07/2013	Ravi Reddy			Updated for OAMPS CR007
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
package au.com.metlife.eapply.bs.pdf;


import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import au.com.metlife.eapply.bs.constants.EapplicationPdfConstants;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.utility.CommonPDFHelper;
import au.com.metlife.eapply.bs.utility.InstitutionalPdfHelper;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.Rule;
import au.com.metlife.eapply.underwriting.response.model.RuleAnswer;
import au.com.metlife.eapply.underwriting.response.model.UnitValues;

public class GenerateEapplyPDF {
	private static final Logger log = LoggerFactory.getLogger(GenerateEapplyPDF.class);

    public static final String PACKAGE_NAME = "com.metlife.eapply.pdf"; /*$NON-NLS-1$*/
    
     /*Name given to this class.*/
    public static final String CLASS_NAME = "GenerateEapplyPDF"; /*$NON-NLS-1$*/
    

    SystemProperty sys;
        
    /*
     * String primaryDecision = null; String secondaryDecision = null;
     */
    /**
     * Description: Generates the Confirmation of eapplication Case Submission
     * PDF
     * 
     * @param com.metlife.eapplication.dto.ApplicationDTO,java.util.Vector
     * @return java.lang.String
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    public String generatePDF(PDFObject pdfObject, String pdfBasePath)
            throws DocumentException, IOException {
    	log.info("Generate pdf start");

       
       
       /* MemberDetails memberKid = null;*/
        
        /*MemberDetails policyOwnerMember = null;*/
       /* DisclosureStatement disclosureStatement = null;*/
        
        CostumPdfAPI pdfAPI = new CostumPdfAPI(pdfBasePath);
        pdfAPI.setFont( MetlifeInstitutionalConstants.HELVETICA_BOLD, 9, MetlifeInstitutionalConstants.COLOR_GRAY);
        
        Document document = new Document( PageSize.A4);
        document.setMargins( 0, 0, 30, 30);
        
        PdfWriter writer = null;               
   
        BaseFont swsMed = null;
		BaseFont swsLig = null;
		BaseFont swsThn = null;
		try {
            
        	if(null != pdfObject.getPdfType() && "EMAIL".equalsIgnoreCase(pdfObject.getPdfType())){
        		writer = PdfWriter.getInstance( document, new FileOutputStream(
                		pdfObject.getEmailAttachDir()));
        	}else{
        		writer = PdfWriter.getInstance( document, new FileOutputStream(
                		pdfObject.getPdfAttachDir()));	
        	}
        	if(MetlifeInstitutionalConstants.CLIENT.equalsIgnoreCase(pdfObject.getClientPdfType()))
        	{
        	writer.setEncryption(pdfObject.getDob().getBytes(), MetlifeInstitutionalConstants.FUND_VICS.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
        	}
        	
        	
        	sys = SystemProperty.getInstance();
    		String swissMUrl = sys.getProperty("swissmediumfont");
    		String swissLUrl = sys.getProperty("swisslightfont");
    		String swissTUrl = sys.getProperty("swissthinfont");
    		log.info("swissMUrl: {}",swissMUrl);
    		 swsMed = BaseFont.createFont(swissMUrl, BaseFont.CP1252,
    	            BaseFont.EMBEDDED);
    		 swsLig = BaseFont.createFont(swissLUrl, BaseFont.CP1252,
    	            BaseFont.EMBEDDED);
    		 swsThn = BaseFont.createFont(swissTUrl, BaseFont.CP1252,
    	            BaseFont.EMBEDDED);
        } catch (FileNotFoundException e) {
        	log.error("Error in file: {}",e);
        } catch (DocumentException e) {
        	log.error("Error in document: {}",e);
        } catch (Exception e) {
        	log.error("Error in generate pdf: {}",e);
		}
        pdfAPI.setSwsMed(swsMed);
        pdfAPI.setSwsLig(swsLig);
        pdfAPI.setSwsThn(swsThn);
        pdfAPI.setFont_color(new Color(0,0,0));
        if(null!=pdfObject.getFundId() && "AEIS".equalsIgnoreCase(pdfObject.getFundId())) {
            pdfAPI.setHeading_color(new Color(0,0,0));
        }
        else {
            pdfAPI.setHeading_color(new Color(255,255,255));
        }

        if(null!=pdfObject.getFundId() && "HOST".equalsIgnoreCase(pdfObject.getFundId())){
        	pdfAPI.setHeading_color_fill(new Color(0, 59, 93));
        }
        else if(null!=pdfObject.getFundId() && "AEIS".equalsIgnoreCase(pdfObject.getFundId())){
        	pdfAPI.setHeading_color_fill(new Color(157, 156, 156));
        }
        else if(null!=pdfObject.getFundId() && "INGD".equalsIgnoreCase(pdfObject.getFundId())){
        	pdfAPI.setHeading_color_fill(new Color(	255, 98, 0));
        }
        else{
            pdfAPI.setHeading_color_fill(new Color(0,102,161));
        }
        if(null!=pdfObject.getFundId() && ("HOST".equalsIgnoreCase(pdfObject.getFundId()) || "MTAA".equalsIgnoreCase(pdfObject.getFundId()) || "GUIL".equalsIgnoreCase(pdfObject.getFundId()) ||
        		/**Vicsuper changes by purna**/MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()))){
        	pdfAPI.setHeader_color(new Color(0,0,0));
        }
        else if(null!=pdfObject.getFundId() && "AEIS".equalsIgnoreCase(pdfObject.getFundId())){
        	pdfAPI.setHeader_color(new Color( 31, 135, 73));
        }
        else if(null!=pdfObject.getFundId() && "INGD".equalsIgnoreCase(pdfObject.getFundId())){
        	pdfAPI.setHeader_color(new Color(168,168,168));
        }
        else{
        	pdfAPI.setHeader_color(new Color(181, 191, 0));
        }
        if(null!=pdfObject.getFundId() && "HOST".equalsIgnoreCase(pdfObject.getFundId())){
            pdfAPI.setTable_fill_color(new Color(0, 59, 93));
        }
        else if(null!=pdfObject.getFundId() && "AEIS".equalsIgnoreCase(pdfObject.getFundId())) {
        	 pdfAPI.setTable_fill_color(new Color(157, 156, 156));
        }
        else if(null!=pdfObject.getFundId() && "INGD".equalsIgnoreCase(pdfObject.getFundId())) {
        	pdfAPI.setFundCode("INGD");
       	 pdfAPI.setTable_fill_color(new Color( 240, 240, 240));
       }
       
        else{
        	pdfAPI.setTable_fill_color(new Color(60,60,60));	
        }
        pdfAPI.setBackground_color(new Color(225,227,228));
        pdfAPI.setBorder_color(new Color(225,227,228));
        
        EventHelper eventHelper = new EventHelper(pdfObject.getMemberDetails().getBrand(),pdfObject.getRequestType(), pdfBasePath);
       if(null!=writer)
       {
        writer.setPageEvent( eventHelper);
       }
        /*
         * HeaderFooter footer = new HeaderFooter(new Phrase("page "), true);
         * footer.setBorder(0); footer.setAlignment(Element.ALIGN_RIGHT);
         * document.setFooter(footer);
         */
        document.open();
        

        try {  
        	
		           if("Institutional".equalsIgnoreCase(pdfObject.getLob())){
		            	InstitutionalPdfHelper.displayInstitutionalDetails(pdfAPI, document, pdfObject, pdfObject.getPdfType());
		            }
		           if(!"coverDetails".equalsIgnoreCase(pdfObject.getChannel())){
		        	if("INGD".equalsIgnoreCase(pdfObject.getFundId())) {
		        		displauAuraQA( pdfAPI, document, pdfObject.getMemberDetails(), pdfObject.getQuestionListForHide(), pdfObject.getPdfType()); 
		        		 CommonPDFHelper.iNGDtransferevidenceDetails( pdfObject.getDisclosureStatementListTe(), pdfAPI, document);
			        	   CommonPDFHelper.iNGDgeneraldeclarationDetails( pdfObject.getDisclosureStatementListGc(), pdfAPI, document);  
	        	}
		        	else {
		        		displauAuraQA( pdfAPI, document, pdfObject.getMemberDetails(), pdfObject.getQuestionListForHide(), pdfObject.getPdfType());
		        		
		        			CommonPDFHelper.declarationDetails( pdfObject.getDisclosureStatementListGc(), pdfAPI, document);	
	        	}
		        	}
		        	   
		           
		           
        }
		          catch (Exception e) {
        	log.error("Error in generate pdf : {}",e);
        } finally {
            document.close();
            pdfAPI.finish();
            pdfAPI = null;
            document = null;
            eventHelper = null; 
           /* memberKid = null;*/
            if(null!=writer)
            {
            writer.close();   
            }
            
        }       
        log.info("Generate pdf finish");
        return pdfObject.getPdfAttachDir();
    } 
       
    /**
     * Description: display Aura question Answer details in the PDF Section
     * 
     * @param
     * java.lang.String,com.metlife.aura.integration.businessobjects.ResponseObject
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     */
    public void displauAuraQA(CostumPdfAPI pdfAPI, Document document,
            MemberDetails memberDetails,  List questionHideList, String pdfType) throws DocumentException {
    	log.info("Display aura question start");
        
        java.util.List applicationData = null;
        List<Rule> insuranceList = null;
        List<Rule> lifestyleList = null;
        List<Rule> healthList = null;
        List<Rule> familyList = null;
        List<Rule> occupationList = null;
        List<Rule> personalList = null;
        List<Rule> generalQuestionList = null;
        List<Rule> kidsQuestionList = null;
        List<Rule> defaultQuestionList = null;
        List<Rule> transferQuestionList = null;
        List<Rule> pregnancyList = null;
        List<Rule> medicalHistList = null;
        List<Rule> workRatingList = null;
        List<Rule> lifeEventList = null;
        List<Rule> employmentList = null;
        List<Rule> eligibilityQuestionList = null;
        Rule rule = null;
        String insuranceHisGrpName = null;
        String lifeStyleGrpName = null;
        String  familyHistGrpName = null;
        String  healthStyleGrpName = null;
        String  persDtsGrpName = null;
        String workRatingGrpName = null;
        String lifeEventGrpName = null;
        String pregnancyGrpName = null;
        String medicalGrpName = null;
        String memberFullName = "";
        String employmentGrpName = null;
        String eligibilityCheckGrpName = null;
        if(null!=memberDetails.getResponseObject()){
        	float[] auraQA  ;
        	if("UW".equalsIgnoreCase(pdfType)){
        		float[] f1 = { 0.05f, 0.6f, 0.15f, .2f };
        		auraQA = f1;
        	}else{
        		float [] f1 = { 0.05f, 0.7f, 0.25f };  
        		auraQA = f1;
        	}
            applicationData = memberDetails.getResponseObject().getRules().getListRule();
            
            for (int itr = 0; itr < applicationData.size(); itr++) {
                rule = (Rule) applicationData.get( itr);
                int insIndex = 0;
                
                if (null != rule
                        && (EapplicationPdfConstants.QG_INSURANCE_DETAILS.equalsIgnoreCase(rule.getAlias())
                        || EapplicationPdfConstants.QG_INSURANCE_HISTORY.equalsIgnoreCase(rule.getAlias())
                        || rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_INSURANCE_HISTORY))                        
                ) {
                	if(rule.getAlias().contains("DP")){                		
                		insuranceHisGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		insuranceHisGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	}                	
                    insIndex = insIndex + 1;
                    if (insuranceList == null) {
                        insuranceList = new ArrayList<>( 1);
                    }
                    insuranceList.add( rule);
                    
                }
                if (null != rule
                		 && (rule.getAlias().contains(EapplicationPdfConstants.QG_LIFESTYLE_QUESTIONS)
                			|| rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_LIFESTYLE_QUESTIONS))                       
                        ) {
                	if(rule.getAlias().contains("DP")){                		
                		lifeStyleGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		lifeStyleGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	} 
                    insIndex = insIndex + 1;
                    if (lifestyleList == null) {
                        lifestyleList = new ArrayList<>( 1);
                    }
                    lifestyleList.add( rule);
                    
                }
                if (null != rule
                		 &&( rule.getAlias().contains(EapplicationPdfConstants.QG_HEALTH_QUESTIONS)
                		 || rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_HEALTH_QUESTIONS))        
                        ) {
                	
                	if(rule.getAlias().contains("DP")){                		
                		healthStyleGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		healthStyleGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	} 
                    insIndex = insIndex + 1;
                    if (healthList == null) {
                        healthList = new ArrayList<>( 1);
                    }
                    healthList.add( rule);
                    
                }
                if (null != rule
                		&& (rule.getAlias().contains(EapplicationPdfConstants.QG_FAMILY_HISTORY)
                		|| rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_FAMILY_HISTORY))        
                		) {
                	
                	if(rule.getAlias().contains("DP")){                		
                		familyHistGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		familyHistGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	} 
                    insIndex = insIndex + 1;
                    if (familyList == null) {
                        familyList = new ArrayList<>( 1);
                    }
                    familyList.add( rule);
                    
                }
                
                if (null != rule
                		&& (rule.getAlias().contains(EapplicationPdfConstants.QG_EMPLOYMENT_DETAILS)
                		|| rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_EMPLOYMENT_DETAILS))        
                		) {
                	
                	if(rule.getAlias().contains("DP")){                		
                		employmentGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		employmentGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	} 
                    insIndex = insIndex + 1;
                    if (employmentList == null) {
                    	employmentList = new ArrayList<>( 1);
                    }
                    employmentList.add( rule);
                    
                }
                
                if (null != rule
                		&&( rule.getAlias().contains(EapplicationPdfConstants.QG_OCCUPATION_DETAILS)
                		|| rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_OCCUPATION_DETAILS))        
                        ) {
                	               	
                    insIndex = insIndex + 1;
                    if (null != rule.getQuestionTypeId()
                            && !rule.getQuestionTypeId().equalsIgnoreCase( "6")
                            && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                            && rule.isVisible()) {
                        if (occupationList == null) {
                            occupationList = new ArrayList<>( 1);
                        }
                        occupationList.add( rule);
                    }
                    
                }
                            
                if (null != rule 
                		&& (rule.getAlias().contains(EapplicationPdfConstants.QG_PERSONAL_DETAILS)
                        || rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_PERSONAL_DETAILS))        
                        && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                        && rule.isVisible()) {
                	
                	if(rule.getAlias().contains("DP")){                		
                		persDtsGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		persDtsGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	}
                    insIndex = insIndex + 1;
                    if (personalList == null) {
                        personalList = new ArrayList<>( 1);
                    }
                    personalList.add( rule);
                    
                }
                if (null != rule
                		&& rule.getAlias().contains(EapplicationPdfConstants.QG_GENERAL_QUESTIONS)
                        /*&& QG_GENERAL_QUESTIONS.equalsIgnoreCase( rule.getAlias())*/
                        ) {
                    insIndex = insIndex + 1;
                    if (generalQuestionList == null) {
                        generalQuestionList = new ArrayList<>( 1);
                    }
                    generalQuestionList.add( rule);
                    
                }
                if (null != rule
                		&& (rule.getAlias().contains(EapplicationPdfConstants.QG_TRANSFER_QUESTIONS)
                		|| rule.getAlias().contains(EapplicationPdfConstants.QG_TRANSFER_DETAILS))		
                        /*&& QG_TRANSFER_QUESTIONS.equalsIgnoreCase( rule.getAlias())*/
                		) {
                    insIndex = insIndex + 1;
                    if (transferQuestionList == null) {
                    	transferQuestionList = new ArrayList<>( 1);
                    }
                    transferQuestionList.add( rule);
                    
                }
                
                if (null != rule
                		&& rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_MEDICAL_QUESTIONS)                      
                		) {
                	
                	if(rule.getAlias().contains("DP")){                		
                    	medicalGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		medicalGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	}
                    insIndex = insIndex + 1;
                    if (medicalHistList == null) {
                    	medicalHistList = new ArrayList<>( 1);
                    }
                    medicalHistList.add( rule);
                    
                }
                
                if (null != rule
                		&& rule.getAlias().contains(EapplicationPdfConstants.QG_KIDS_COVER)
                        /*&& QG_KIDS_COVER.equalsIgnoreCase( rule.getAlias())*/
                        ) {
                    insIndex = insIndex + 1;
                    if (kidsQuestionList == null) {
                        kidsQuestionList = new ArrayList<>( 1);
                    }
                    kidsQuestionList.add( rule);
                }
                
                if (null != rule
                		&& rule.getAlias().contains(EapplicationPdfConstants.QG_KIDS_COVER_OPTION)
                        /*&& QG_KIDS_COVER.equalsIgnoreCase( rule.getAlias())*/
                        ) {
                    insIndex = insIndex + 1;
                    if (kidsQuestionList == null) {
                        kidsQuestionList = new ArrayList<>( 1);
                    }
                    kidsQuestionList.add( rule);
                }
                
                
                if (null != rule
                		/*&& rule.getAlias().contains(QG_DEFAULT)*/
                        && EapplicationPdfConstants.QG_DEFAULT.equalsIgnoreCase( rule.getAlias())
                        ) {
                    insIndex = insIndex + 1;
                    if (defaultQuestionList == null) {
                        defaultQuestionList = new ArrayList<>( 1);
                    }
                    defaultQuestionList.add( rule);
                }
                
                if (null != rule
                        && (EapplicationPdfConstants.QG_PREGNANCY.equalsIgnoreCase( rule.getAlias())
                        || rule.getAlias().contains(EapplicationPdfConstants.QG_YOUR_PREGNANCY))        
                        ) {
                    insIndex = insIndex + 1;         
                    
                    if (pregnancyList == null) {
                    	pregnancyList = new ArrayList<>( 1);
                    }
                    if(rule.getAlias().contains("DP")){                		
                    	pregnancyGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		pregnancyGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	}
                    pregnancyList.add( rule);
                }
                
                if (null != rule &&                		
                       rule.getAlias().contains(EapplicationPdfConstants.QG_WORK_RATING_DETAILS)       
                        && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                        && rule.isVisible()) {
                	
                	if(rule.getAlias().contains("DP")){                		
                		workRatingGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                	}else{
                		workRatingGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                	}
                    insIndex = insIndex + 1;
                    if (workRatingList == null) {
                    	workRatingList = new ArrayList<>( 1);
                    }
                    workRatingList.add( rule);
                    
                }
                
                if (null != rule &&                		
                        rule.getAlias().contains(EapplicationPdfConstants.QG_LIFE_EVENT_DETAILS)       
                         && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                         && rule.isVisible()) {
                 	
                 	if(rule.getAlias().contains("DP")){                		
                 		lifeEventGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                 	}else{
                 		lifeEventGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                 	}
                     insIndex = insIndex + 1;
                     if (lifeEventList == null) {
                    	 lifeEventList = new ArrayList<>( 1);
                     }
                     lifeEventList.add( rule);
                     
                 }
                
                if (null != rule
                		&& rule.getAlias().contains(EapplicationPdfConstants.QG_ELIGIBILITY_CHECK)
                       && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                         && rule.isVisible()) {
                	
                	if(rule.getAlias().contains("DP")){                		
                		eligibilityCheckGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().indexOf('|'));
                 	}else{
                 		eligibilityCheckGrpName = rule.getAlias().substring(rule.getAlias().indexOf('=') + 1, rule.getAlias().length());
                 	}
                	
                    insIndex = insIndex + 1;
                    if (eligibilityQuestionList == null) {
                    	eligibilityQuestionList = new ArrayList<>( 1);
                    }
                    eligibilityQuestionList.add( rule);
                    
                }
                
                
            }
            if (null != memberDetails.getTitle()) {
            	memberFullName = memberDetails.getTitle()+". ";
            }
            if (null != memberDetails.getMemberFName()) {
            	memberFullName = memberFullName + memberDetails.getMemberFName().trim();
            }
            if (null != memberDetails.getMemberLName()) {
            	memberFullName = memberFullName+" "+memberDetails.getMemberLName().trim();
            }            
            if (personalList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( personalList);
               
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	displayAuraSection( personalList, pdfAPI, auraQA,pdfType,document, persDtsGrpName);	
                }else{
                	displayAuraSection( personalList, pdfAPI, auraQA,pdfType,document, persDtsGrpName+" - "
                    		+memberFullName);
                }
                
                
            }
            if (healthList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( healthList);
                
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	displayHealthAndLifestyle( healthList, pdfAPI,auraQA,pdfType, document, healthStyleGrpName, questionHideList);
                }else{
                	displayHealthAndLifestyle( healthList, pdfAPI,auraQA,pdfType, document, healthStyleGrpName+" - "
                    		+memberFullName, questionHideList);
                }
                
                
            }
            if (familyList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( familyList);
                
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	displayHealthAndLifestyle( familyList, pdfAPI, auraQA,pdfType,document, familyHistGrpName,questionHideList);
                }else{
                	displayHealthAndLifestyle( familyList, pdfAPI, auraQA,pdfType,document, familyHistGrpName+" - "
                    		+memberFullName,questionHideList);
                }
                
                
            }
            if (lifestyleList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( lifestyleList);
               
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	displayHealthAndLifestyle( lifestyleList,pdfAPI,auraQA,pdfType, document, lifeStyleGrpName, questionHideList);
                }else{
                	displayHealthAndLifestyle( lifestyleList,pdfAPI,auraQA,pdfType, document, lifeStyleGrpName+" - "
                    		+memberFullName, questionHideList);
                }
                
                
                
            }
            if (pregnancyList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( pregnancyList);             
                
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	 displayAuraSection( pregnancyList, pdfAPI,auraQA,pdfType, document, pregnancyGrpName);  
                }else{
                	 displayAuraSection( pregnancyList, pdfAPI,auraQA,pdfType, document, pregnancyGrpName+" - "
                     		+memberFullName);  
                }
                
                displayAuraSection( pregnancyList, pdfAPI,auraQA,pdfType, document, pregnancyGrpName+" - "
                		+memberFullName);               
            }
            
            if (employmentList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( employmentList);
                displayAuraSection( employmentList, pdfAPI,auraQA,pdfType, document, employmentGrpName+" - "
                		+memberFullName);
            }            
            
            if (insuranceList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( insuranceList);
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	displayHealthAndLifestyle( insuranceList,pdfAPI,auraQA,pdfType, document, insuranceHisGrpName, questionHideList); 
                }else{
                	 displayHealthAndLifestyle( insuranceList,pdfAPI,auraQA,pdfType, document, insuranceHisGrpName+" - "
                     		+memberFullName, questionHideList); 
                }
                
                              
               /* displayAuraSection( insuranceList, pdfAPI,auraQA,pdfType, document, insuranceHisGrpName+" - "
                		+memberFullName);*/
            }
            if (generalQuestionList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( generalQuestionList);
                
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	displayAuraSection( generalQuestionList, pdfAPI,auraQA,pdfType, document, "General Questions");
                }else{
                	  displayAuraSection( generalQuestionList, pdfAPI,auraQA,pdfType, document, "General Questions - "
                      		+memberFullName);
                }
                
              
            }
            if (kidsQuestionList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( kidsQuestionList);
                displayHealthAndLifestyle( kidsQuestionList, pdfAPI,auraQA,pdfType, document, "Kids Cover Option - "
                		+memberFullName, questionHideList);
            }
            if (defaultQuestionList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( defaultQuestionList);
              
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
	  				displayAuraSection( defaultQuestionList, pdfAPI,auraQA,pdfType, document, "Default");
                }else{
                	  displayAuraSection( defaultQuestionList, pdfAPI,auraQA,pdfType, document, "Default - "
                      		+memberFullName);
                }

              
            }   
            if (transferQuestionList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( transferQuestionList);
                displayAuraSection( transferQuestionList, pdfAPI,auraQA,pdfType, document, "Transfer Questions - "
                		+memberFullName);
            } 
            if (medicalHistList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( medicalHistList);
               
                if(null!= memberDetails.getBrand() && "COFS".equalsIgnoreCase(memberDetails.getBrand())){
                	 displayAuraSection( medicalHistList, pdfAPI,auraQA,pdfType, document, medicalGrpName);
                }else{
                	 displayAuraSection( medicalHistList, pdfAPI,auraQA,pdfType, document, medicalGrpName+" - "
                     		+memberFullName);
                }
                
               
            }
            if (workRatingList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( workRatingList);
                displayAuraSection( workRatingList, pdfAPI,auraQA,pdfType, document, workRatingGrpName+" - "
                		+memberFullName);
            }
            if (lifeEventList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( lifeEventList);
                displayAuraSection( lifeEventList, pdfAPI,auraQA,pdfType, document, lifeEventGrpName+" - "
                		+memberFullName);
            }
            if (eligibilityQuestionList != null) {
                createAuraSpaces( pdfAPI, document);
                Collections.reverse( eligibilityQuestionList);
                displayAuraSection( eligibilityQuestionList, pdfAPI,auraQA,pdfType, document, eligibilityCheckGrpName+" - "
                		+memberFullName);
            }

            
            createAuraSpaces( pdfAPI, document);
            document.newPage();      
           
        }
         log.info("Display aura question finish");
        
        
    }
    
    /**
     * Description: display Aura details in the PDF Section
     * 
     * @param java.util.Vector
     *            com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    
    public void displayAuraSection(List decisionList, CostumPdfAPI pdfAPI, float [] auraQA,String pdfType,
            Document document, String sectionType ) throws DocumentException {
    	log.info("Display aura section start");

        createSectionHeader(sectionType,pdfType, pdfAPI, document);
        int index = 0;       
        PdfPTable pdfPTable = null;
        Rule rule = null;
        for (int insIndex = 0; insIndex < decisionList.size(); insIndex++) {
            rule = (Rule) decisionList.get( insIndex);
            index = index + 1;            
            if (!rule.getQuestionTypeId().equalsIgnoreCase( "40")
                    && rule.isVisible()) {
                pdfPTable = new PdfPTable( auraQA);                
                document.add( createAuraQuestionCell(pdfPTable, pdfAPI, document, rule, index,pdfType));
                if (null != rule.getAnswer().getListRule()) {
                    
                	if("UW".equalsIgnoreCase(pdfType)){
                		double subItr = 0.05;
                		 double negativeFactor = 0.0;
                		 createSubAuraDetails( rule.getAnswer().getListRule(), pdfAPI, document, subItr,negativeFactor,pdfType);
                	}else{
                		double subItr = 0.05;
                		createSubAuraDetails( rule.getAnswer().getListRule(), pdfAPI, document, subItr,0.00,pdfType);                		
                	}
                	
                    
                }
            }
            
        }        
        addLineSpace( pdfAPI, document,pdfType);
        log.info("Display aura section finish"); 
        
    }
    
    private String getQuestionDecision(Rule rule){
    	log.info("Get question decision start");
    	StringBuilder strBuff = new StringBuilder();
    	Decision decision = null;
    	Rating rating = null;
    	String[] reason = null;
        if (null != rule.getAnswer().getListUWDecisions()) {
            for (int decItr = 0; decItr < rule.getAnswer().getListUWDecisions().size(); decItr++) {
                 decision =  rule.getAnswer().getListUWDecisions().get( decItr);
                if (null != decision
                        && ( !"ACC".equalsIgnoreCase( decision.getDecision())
                                /*|| ( null != decision.getListExclusion() && decision.getListExclusion().size() > 0) || ( null != decision.getListRating() && decision.getListRating().size() > 0))) {*/
                        		|| ( null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) || ( null != decision.getListRating() && !(decision.getListRating().isEmpty())))) {
                    if (null != decision.getProductName()
                            && "".equalsIgnoreCase( decision.getProductName())
                            && decision.getProductName().length() == 0) {
                        strBuff.append( "All Product:");
                    } else {
                        strBuff.append( decision.getProductName());
                        strBuff.append( ":");
                    }
                    strBuff.append( decision.getDecision());
                    strBuff.append( ",");
                    for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                        rating = decision.getListRating().get( ratingItr);
                        strBuff.append( rating.getValue());
                        strBuff.append( ",");
                        strBuff.append( rating.getReason());
                        strBuff.append( ",");
                    }
                    if (null != decision.getReasons()
                            && decision.getReasons().length > 0) {
                        strBuff.append( "Reason:");
                        reason = decision.getReasons();
                        for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
                            strBuff.append( reason[reasonItr]);
                        }
                        
                    }
                }
                
            }
            
        }
    	log.info("Get question decision finish");
    	return strBuff.toString();
    }
    
    private PdfPTable createDecisionCellForUW(PdfPTable pdfPTable, CostumPdfAPI pdfAPI, String value){
    	log.info("Create decision cell for UW start");
    	PdfPCell pdfCell = new PdfPCell(
                 pdfAPI.addParagraph( value, new Font( pdfAPI.getSwsLig(), 8,0, pdfAPI.getFont_color())));
    	pdfCell.setBorderColor( pdfAPI.getBorder_color());
    	pdfCell.setBorder( PdfPCell.LEFT);        
        /* pdfAPI.disableBorders( pdfCell);*/
         pdfPTable.addCell( pdfCell);
         log.info("Create decision cell for UW finish");
         return pdfPTable;
    }
    
    private void displayHealthAndLifestyle(List decisionList,
            CostumPdfAPI pdfAPI, float [] auraQA, String pdfType, Document document, String type,
            List questionHideList) throws DocumentException {
    	log.info("Display health and life style start");

       
        createSectionHeader(type, pdfType, pdfAPI, document);        
        int index = 0;
        Rule rule = null;
        Rule rle = null;
        RuleAnswer ruleAnswer = null;
        PdfPTable pdfInsuranceDetailsTable = null;
        RuleAnswer ruleUnselectedAnswer = null;
        Boolean isQuestionToHide = null;
        PdfPTable pdfUnselectedTable = null;
        PdfPTable pdfselectedTable = null;
        RuleAnswer obj = null;
        PdfPCell pdfCell = null;
        for (int insIndex = 0; insIndex < decisionList.size(); insIndex++) {
            rule = (Rule) decisionList.get( insIndex);
            if(null!=rule && rule.getValue().contains("Have you smoked in the past 3 years?")){
            	rule.setVisible(Boolean.TRUE);
            }
            if (!rule.getQuestionTypeId().equalsIgnoreCase( "40")
                    && rule.isVisible()) {
                index = index + 1;
                if (!rule.getQuestionTypeId().equalsIgnoreCase( "14")) {                	
                	
                    pdfInsuranceDetailsTable = new PdfPTable( auraQA);
                    document.add( createAuraQuestionCell(pdfInsuranceDetailsTable, pdfAPI, document, rule, index,pdfType));                   
                    
                    if (null != rule.getAnswer().getListRule()) {
                    	double subItr;
                        double negativeFactor = 0.0;
                    	if("UW".equalsIgnoreCase(pdfType)){
                    		 subItr = 0.1;
                             negativeFactor = 0.0;
                    	}else{
                    		subItr = 0.05;
                    	}   
                    	
                    	
                    	 /*specific to this question of aura*/
                    	if(rule.getValue().contains("Do you have a usual doctor or medical centre ")){
	                    	if("UW".equalsIgnoreCase(pdfType)){
	                    		 negativeFactor = 0.03;
	                    		 Collections.reverse(rule.getAnswer().getListRule());
	                    	}
	                    		createSubAuraDetails(rule.getAnswer().getListRule(), pdfAPI, document, subItr, negativeFactor, pdfType);
	                    }else{
	                    	 for (int itr = 0; itr < rule.getAnswer().getListRule().size(); itr++) {
	                             rle =  rule.getAnswer().getListRule().get( itr);
	                             if (!rle.getQuestionTypeId().equalsIgnoreCase( "14") && !rle.getQuestionTypeId().equalsIgnoreCase( "40")
	                                     && rle.isVisible()) {
	                                 createSubHealthAndLifestyleDetails( rule.getAnswer().getListRule(), pdfAPI, document, subItr,negativeFactor, questionHideList,pdfType);
	                             } else if (rle.getQuestionTypeId().equalsIgnoreCase( "14") && !rle.getQuestionTypeId().equalsIgnoreCase( "40")
	                                     && rle.isVisible()) {
	                            	 if("UW".equalsIgnoreCase(pdfType)){
	                            		 subItr = 0.15;
	                            	 }
	                                 displayRuleAnswer( rule.getAnswer().getListRule(), pdfAPI, document, subItr,negativeFactor, questionHideList,pdfType);
	                             /*}else  if(null!=rle.getAnswer().getListRule() && rle.getAnswer().getListRule().size()>0){*/
	                             }else  if(null!=rle.getAnswer().getListRule() && !(rle.getAnswer().getListRule().isEmpty())){
	                         		displayDoctorsQuestion(rle.getAnswer().getListRule(), pdfAPI, document, subItr, negativeFactor, questionHideList, pdfType);                            	
	                             }
	                             
	                         }
	                    }
                        
                       
                         /*createSubHealthAndLifestyleDetails(rule.getAnswer().getListRule(),
                         pdfAPI, document, subItr);*/
                    }
                } else if (rule.getQuestionTypeId().equalsIgnoreCase( "14")) {
                	float[] f2;
                	if("UW".equalsIgnoreCase(pdfType)){
                		  float[] rulefloat = { 0.05f, 0.75f, .2f };
                		  f2= rulefloat;
                	}else{
                		 float[] rulefloat = { 0.05f, 0.95f };
                		 f2 = rulefloat;
                	}          
                  
                    pdfInsuranceDetailsTable = new PdfPTable( f2);
                   pdfCell = new PdfPCell(
                            pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK
                                    + index, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfCell);
                    pdfInsuranceDetailsTable.addCell( pdfCell);
                    pdfCell = new PdfPCell(
                            pdfAPI.addParagraph( filterString( rule.getValue()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfCell);
                    pdfInsuranceDetailsTable.addCell( pdfCell);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfCell = new PdfPCell(
                                 pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK,new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
                     	pdfCell.setBorder( PdfPCell.LEFT); 
                         pdfInsuranceDetailsTable.addCell( pdfCell);
                    }                   
                    
                    document.add( pdfInsuranceDetailsTable);
                    for (int itr = 0; itr < rule.getListSelUnselAnswers().size(); itr++) {
                        obj = rule.getListSelUnselAnswers().get( itr);
                        if (obj.isAnswered()) {
                            ruleAnswer = obj;
                            isQuestionToHide = checkQuestionsForHide( ruleAnswer.getValue(), questionHideList);
                            if (null != ruleAnswer && !isQuestionToHide) {
                            	 float[] f4;
                            	if("UW".equalsIgnoreCase(pdfType)){
                            		float[] selectedAns = { 0.05f, 0.05f, 0.50f, 0.20f, 0.2f };
                            		f4= selectedAns;
                            	}else{
                            		  float[] selectedAns = { 0.05f, 0.05f, 0.65f, 0.25f };
                            		  f4 = selectedAns;
                            	}                        
                                pdfselectedTable = new PdfPTable( f4);                                
                                document.add( getSelectedQuestionTables(ruleAnswer, pdfAPI, document, pdfType, pdfselectedTable));
                                if (null != ruleAnswer.getListRule()
                                        /*&& ruleAnswer.getListRule().size() > 0) {*/
                                		&& !(ruleAnswer.getListRule().isEmpty())) {
                                	
                                	 double subItr = 0.15;
                                     double negativeFactor = 0.0;                        
                                    displayRuleAnswer( ruleAnswer.getListRule(), pdfAPI, document, subItr,negativeFactor, questionHideList,pdfType);
                                }
                            }
                            
                        } else if (!obj.isAnswered()) {
                             ruleUnselectedAnswer =  obj;
                             isQuestionToHide = checkQuestionsForHide( ruleUnselectedAnswer.getValue(), questionHideList);
                          
                            if (null != ruleUnselectedAnswer
                                    && !isQuestionToHide) {     
                            	float[] f3;
                            	if("UW".equalsIgnoreCase(pdfType)){
	                           		 float[] ruleQA = { 0.05f, 0.05f, 0.50f, 0.20f, 0.2f };
	                           		 f3= ruleQA;
	                           	}else{
	                           		float[] ruleQA = { 0.05f, 0.05f, 0.65f, 0.25f }; 
	                           		f3= ruleQA;
	                           	} 
                            	pdfUnselectedTable = new PdfPTable(f3);    	
                                document.add( getUnselectedQuestionTables(ruleUnselectedAnswer, pdfAPI, document, pdfType,pdfUnselectedTable));
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        addLineSpace( pdfAPI, document,pdfType);
        
        log.info("Display health and life style finish");
    }
    
    private PdfPTable getUnselectedQuestionTables(RuleAnswer ruleUnselectedAnswer, CostumPdfAPI pdfAPI,Document document,String pdfType,PdfPTable pdfUnselectedTable){ 	  
    	log.info("Get unselected question tables start");
    	PdfPCell pdfCell = new PdfPCell(
                 pdfAPI.addParagraph( "", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
         pdfAPI.disableBorders( pdfCell);
         pdfUnselectedTable.addCell( pdfCell);
         pdfCell = new PdfPCell(
                 pdfAPI.addParagraph( "-",new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
         pdfAPI.disableBorders( pdfCell);
         pdfUnselectedTable.addCell( pdfCell);
         pdfCell = new PdfPCell(
                 pdfAPI.addParagraph( filterString( ruleUnselectedAnswer.getValue()), new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
         pdfAPI.disableBorders( pdfCell);
         pdfUnselectedTable.addCell( pdfCell);
         pdfCell = new PdfPCell(
                 pdfAPI.addParagraph( "No", new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
         pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
         pdfAPI.disableBorders( pdfCell);
         pdfUnselectedTable.addCell( pdfCell);
         if("UW".equalsIgnoreCase(pdfType)){
        	 pdfCell = new PdfPCell(
                     pdfAPI.addParagraph( "", new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
             pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
             pdfCell.setBorderColor( pdfAPI.getBorder_color());
         		pdfCell.setBorder( PdfPCell.LEFT); 
             pdfUnselectedTable.addCell( pdfCell);  
         }    
         log.info("Get unselected question tables finish");
         return pdfUnselectedTable;
    }
    
    private PdfPTable getSelectedQuestionTables(RuleAnswer ruleAnswer, CostumPdfAPI pdfAPI,Document document,String pdfType,PdfPTable pdfselectedTable){ 	  
    	log.info("Get selected question tables start");
    	PdfPTable pdfselectedTab = pdfselectedTable;
    	PdfPCell pdfCell = new PdfPCell(
                pdfAPI.addParagraph( "", new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfCell);
        pdfselectedTab.addCell( pdfCell);
        pdfCell = new PdfPCell(
                pdfAPI.addParagraph( "-", new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfCell);
        pdfselectedTab.addCell( pdfCell);
        pdfCell = new PdfPCell(
                pdfAPI.addParagraph( filterString( ruleAnswer.getValue()), new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfCell);
        pdfselectedTab.addCell( pdfCell);
        pdfCell = new PdfPCell(
                pdfAPI.addParagraph( "Yes", new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
        pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
        pdfAPI.disableBorders( pdfCell);
        pdfselectedTab.addCell( pdfCell);
         if("UW".equalsIgnoreCase(pdfType)){
        	
        	 pdfselectedTab = createDecisionCellForUW(pdfselectedTab, pdfAPI, getRuleAnswerDecision(ruleAnswer));
           
         }   
         log.info("Get selected question tables finish");
         return pdfselectedTab;
    }
    
    private String getRuleAnswerDecision(RuleAnswer ruleAnswer){
    	log.info("Get rule answer decision start");
    	StringBuilder strBuff = new StringBuilder();
    	Rating rating = null;
    	String [] reason = null;
    	 Decision decision = null;
    	if (null != ruleAnswer.getListUWDecisions()) {
            for (int decItr = 0; decItr < ruleAnswer.getListUWDecisions().size(); decItr++) {
                decision =  ruleAnswer.getListUWDecisions().get( decItr);
                if (null != decision
                        && ( !"ACC".equalsIgnoreCase( decision.getDecision())
                                /*|| ( null != decision.getListExclusion() && decision.getListExclusion().size() > 0) || ( null != decision.getListRating() && decision.getListRating().size() > 0))) {*/
                        		|| ( null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) || ( null != decision.getListRating() && !(decision.getListRating().isEmpty())))
                        && (null != decision.getProductName()
                                && !"".equalsIgnoreCase( decision.getProductName())
                                && decision.getProductName().length() > 1)) {              			
                		
                			strBuff.append( decision.getProductName());
                			strBuff.append( ":");
                        
	                        strBuff.append( decision.getDecision());
	                        strBuff.append( ",");
	                        for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
	                            rating = decision.getListRating().get( ratingItr);
	                            strBuff.append( rating.getValue());
	                            strBuff.append( ",");
	                            strBuff.append( rating.getReason());
	                            strBuff.append( ",");
	                        }
	                        if (null != decision.getReasons()
	                                && decision.getReasons().length > 0) {
	                            strBuff.append( "Reason:");
	                            reason = decision.getReasons();
	                            for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
	                                strBuff.append( reason[reasonItr]);
	                            }
	                            
	                        }
                }
            }
            
        }
    	 log.info("Get rule answer decision finish");
    	return strBuff.toString();
    }
    
    private void displayRuleAnswer(List ruleList, CostumPdfAPI pdfAPI,
            Document document, double subItr, double negativeFactor, List questionListForHide, String pdfType)
            throws DocumentException {
    	log.info("Display rule answer start");
    	double subIter = subItr;
    	double negativeFact = negativeFactor;
    	float[] f4 ;
    	 Rule rule = null;
    	 RuleAnswer obj = null;
    	 RuleAnswer ruleAnswer = null;
         Boolean isQuestionToHide = null;
         RuleAnswer ruleUnselectedAnswer = null;
         PdfPTable pdfUnselectedTable = null;
    	if("UW".equalsIgnoreCase(pdfType)){
    		 float[] auraQA = { (float) subIter,(float) ( 0.65 - negativeFact), 0.2f };
    		f4 = auraQA;
    	}else{
    		float[] auraQA = { (float) subIter, 0.85f };
    		f4 = auraQA;
    	}
        if (null != ruleList) {
            for (int subRuleListItr = 0; subRuleListItr < ruleList.size(); subRuleListItr++) {
                rule = (Rule) ruleList.get( subRuleListItr);
                if (!rule.getQuestionTypeId().equalsIgnoreCase( "40") && rule.isVisible()) {                    
                    PdfPTable pdfselectedTable = new PdfPTable( f4);
                    PdfPCell pdfCell = new PdfPCell(
                            pdfAPI.addParagraph( "", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfCell);
                    pdfselectedTable.addCell( pdfCell);
                    pdfCell = new PdfPCell(
                            pdfAPI.addParagraph( filterString( rule.getValue()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfCell);
                    pdfselectedTable.addCell( pdfCell);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	pdfCell = new PdfPCell(
                                pdfAPI.addParagraph( "", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    	pdfCell.setBorderColor( pdfAPI.getBorder_color());
                    	pdfCell.setBorder( PdfPCell.LEFT); 
                        pdfselectedTable.addCell( pdfCell);
                    }                    
                    document.add( pdfselectedTable);
                    if (null != rule) {
                        for (int itr = 0; itr < rule.getListSelUnselAnswers().size(); itr++) {
                            obj = rule.getListSelUnselAnswers().get( itr);
                            if (obj.isAnswered()) {
                                ruleAnswer = obj;
                                isQuestionToHide = checkQuestionsForHide( ruleAnswer.getValue(), questionListForHide);
                                if (null != ruleAnswer && !isQuestionToHide) {
                                	float[] f1;
                                	if("UW".equalsIgnoreCase(pdfType)){
                                		 float[] ruleAnsFloat = { (float) subIter, 0.05f,(float) ( 0.55 - negativeFact),0.05f, 0.2f };
                                		 f1 = ruleAnsFloat;
                                	}else{
                                		float[] ruleAnsFloat = { (float) subIter, 0.05f,0.65f, 0.25f };
                                		f1 = ruleAnsFloat;
                                	}                         
                                    pdfselectedTable = new PdfPTable( f1);                                                                       
                                    document.add( getSelectedQuestionTables(ruleAnswer, pdfAPI, document, pdfType, pdfselectedTable));                                 
                                    if (null != ruleAnswer.getListRule()
                                            /*&& ruleAnswer.getListRule().size() > 0) {*/
                                    		&& !(ruleAnswer.getListRule().isEmpty())) {
                                    	subIter = (float) ( subIter + 0.01);
                                        
                                       if("UW".equalsIgnoreCase(pdfType)){
                                    	   negativeFact = (float) negativeFact + 0.01;
                                       }                               
                                        displayRuleAnswer( ruleAnswer.getListRule(), pdfAPI, document, subIter,negativeFact, questionListForHide,pdfType);
                                    }
                                }
                            } else if (!obj.isAnswered()
                                    && rule.getQuestionTypeId().equalsIgnoreCase( "14")) {
                                
                                ruleUnselectedAnswer =  obj;
                                isQuestionToHide = checkQuestionsForHide( ruleUnselectedAnswer.getValue(), questionListForHide);
                                if (null != ruleUnselectedAnswer
                                        && !isQuestionToHide) {
                                	
                                	float[] f3;
                                	if("UW".equalsIgnoreCase(pdfType)){
    	                           		 float[] ruleQA = { (float) subIter, 0.05f,(float) ( 0.55 - negativeFact), 0.05f, 0.2f };
    	                           		 f3= ruleQA;
    	                           	}else{
    	                           		float[] ruleQA ={ (float) subIter, 0.05f,0.65f, 0.25f };
    	                           		f3= ruleQA;
    	                           	} 
                                	pdfUnselectedTable = new PdfPTable(f3); 
                                    document.add( getUnselectedQuestionTables(ruleUnselectedAnswer, pdfAPI, document, pdfType, pdfUnselectedTable));
                                    
                                }
                            }
                            
                        }
                    }
                    
                }else if(null!=rule.getListSelUnselAnswers()&& !rule.getQuestionTypeId().equalsIgnoreCase( "40")){
                	 for (int itr = 0; itr < rule.getListSelUnselAnswers().size(); itr++) {
                		 obj = rule.getListSelUnselAnswers().get( itr);
                         if (obj instanceof RuleAnswer) {
                             ruleAnswer = obj;
                             displayRuleAnswer( ruleAnswer.getListRule(), pdfAPI, document, subIter,negativeFact, questionListForHide,pdfType);
                         }
                	 }
                	
                }
                
            }
        }
        log.info("Display rule answer finish");
    }
    
    private void createSubHealthAndLifestyleDetails(List subQuestions,
            CostumPdfAPI pdfAPI, Document document, double subItr,double negativeFactor,
            List questionListForHide,String pdfType) throws DocumentException {
    	log.info("Create sub health and lifestyle details start");
        
       double subIter = subItr;
       double negativeFact = negativeFactor;
        Rule subTransferObject = null;
        PdfPTable pdfSubDetailsTable = null;
        Rule rle = null;
        float[] f1;
        if("UW".equalsIgnoreCase(pdfType)){            	
        	float[] auraQAfloat = { (float) subIter,(float) ( 0.65 - negativeFact), 0.05f, .2f };           	
        	f1 = auraQAfloat;
        }else{
        	float[] auraQAfloat = { (float) subIter, 0.25f, 0.25f };
        	f1 = auraQAfloat;
        }
        
        if (null != subQuestions) {   
        	
            for (int subQuesItr = 0; subQuesItr < subQuestions.size(); subQuesItr++) {
                subTransferObject = (Rule) subQuestions.get( subQuesItr);               
                if (!subTransferObject.getQuestionTypeId().equalsIgnoreCase( "40")
                        && subTransferObject.isVisible()) {
                    pdfSubDetailsTable = new PdfPTable( f1);                    
                    document.add( createAuraQuestionCell(pdfSubDetailsTable, pdfAPI, document, subTransferObject, 30, pdfType));
                    if (null != subTransferObject.getAnswer().getListRule()) {
                        
                    	subIter = (float) ( subIter + 0.01);
                        
                       if("UW".equalsIgnoreCase(pdfType)){
                    	   negativeFact = (float) negativeFact + 0.01;
                       }                       
                        
                        for (int itr = 0; itr < subTransferObject.getAnswer().getListRule().size(); itr++) {
                            rle =  subTransferObject.getAnswer().getListRule().get( itr);                         
                            if (!rle.getQuestionTypeId().equalsIgnoreCase( "14")) {
                                createSubHealthAndLifestyleDetails( subTransferObject.getAnswer().getListRule(), pdfAPI, document, subIter,negativeFact, questionListForHide,pdfType);
                            } else if (rle.getQuestionTypeId().equalsIgnoreCase( "14")) {
                                displayRuleAnswer( subTransferObject.getAnswer().getListRule(), pdfAPI, document, subIter + 0.05,negativeFact, questionListForHide,pdfType);
                            }
                        }                        
                        
                    }
                }else if (null != subTransferObject.getAnswer().getListRule()) {
                	Collections.reverse( subTransferObject.getAnswer().getListRule());
                	subIter = (float) ( subIter + 0.01);
                	negativeFact = (float) negativeFact + 0.01;
                    for (int itr = 0; itr < subTransferObject.getAnswer().getListRule().size(); itr++) {
                        rle =  subTransferObject.getAnswer().getListRule().get( itr);                   
                        if (!rle.getQuestionTypeId().equalsIgnoreCase( "14")) {                        	
                        	  pdfSubDetailsTable = new PdfPTable( f1);                    
                              document.add( createAuraQuestionCell(pdfSubDetailsTable, pdfAPI, document, rle, 30, pdfType));                                                     	
                        	/*if(null!=rle.getAnswer().getListRule() && rle.getAnswer().getListRule().size()>0){*/
                              if(null!=rle.getAnswer().getListRule() && !(rle.getAnswer().getListRule().isEmpty())){
                        		displayDoctorsQuestion(rle.getAnswer().getListRule(), pdfAPI, document, subIter, negativeFact, questionListForHide, pdfType);
                            	/*createSubHealthAndLifestyleDetails( rle.getAnswer().getListRule(), pdfAPI, document, subItr, negativeFactor, questionListForHide,pdfType);*/
                            }
                        	
                          
                        } else if (rle.getQuestionTypeId().equalsIgnoreCase( "14")) {
                            displayRuleAnswer( subTransferObject.getAnswer().getListRule(), pdfAPI, document, subIter + 0.05, negativeFact, questionListForHide,pdfType);
                             /*displayRuleAnswer(subTransferObject.getAnswer().getListRule(),pdfAPI,document,subItr);*/
                        }
                    }
                     /*createSubHealthAndLifestyleDetails(subTransferObject.getAnswer().getListRule(),pdfAPI,document,subItr,negativeFactor);*/
                    
                }
                
            }
        }

        log.info("Create sub health and lifestyle details finsih");
        
    }
    
    
    private void displayDoctorsQuestion(List subQuestions,
            CostumPdfAPI pdfAPI, Document document, double subItr,double negativeFactor,
            List questionListForHide,String pdfType)throws DocumentException {
    	 log.info("Display doctor question start");
    	 double negativeFact = negativeFactor;
    	 double subIter = subItr;
    	 PdfPTable pdfSubDetailsTable = null;
    	  Rule rle = null;
    	  float[] f1;
          if("UW".equalsIgnoreCase(pdfType)){            	
          	float[] auraQAfloat = { (float) subIter,(float) ( 0.65 - negativeFact), 0.05f, .2f };           	
          	f1 = auraQAfloat;
          }else{
          	float[] auraQAfloat = { (float) subIter, 0.25f, 0.25f };
          	f1 = auraQAfloat;
          }   
          Collections.reverse(subQuestions);
          if("UW".equalsIgnoreCase(pdfType)){
        	  Collections.reverse(subQuestions);
          }
    	for(int ruleItr=0;ruleItr<subQuestions.size();ruleItr++){
			 rle = (Rule) subQuestions.get( ruleItr); 
			 if(null!=rle && !rle.getQuestionTypeId().equalsIgnoreCase( "40")){
				 subIter = (float) ( subIter + 0.01);
		           if("UW".equalsIgnoreCase(pdfType)){
		        	   negativeFact = (float) negativeFact + 0.01;
		           } 
		           pdfSubDetailsTable = new PdfPTable( f1);     
		           document.add( createAuraQuestionCell(pdfSubDetailsTable, pdfAPI, document, rle, 30, pdfType));
		           /*if(rle.getAnswer().getListRule().size()>0){  */
		           if(!(rle.getAnswer().getListRule().isEmpty())){  
		        	   displayDoctorsQuestion(rle.getAnswer().getListRule(), pdfAPI, document, subIter, negativeFact, questionListForHide, pdfType);
		           }
			 }
			
          
		}
    	log.info("Display doctor question finish");
    }
    
    /**
     * Description: display Aura spaces Answer details in the PDF Section
     * 
     * @param
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     */
    public void createAuraSpaces(CostumPdfAPI pdfAPI, Document document)
            throws DocumentException {
        log.info("Create aura space start");
       
       
        
        float[] spaceFloat = { 0.8f, 0.2f };
        
        PdfPTable pdfSingleSpaceTable = new PdfPTable( spaceFloat);
        PdfPCell pdfPSingleSpaceCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK,  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        pdfPSingleSpaceCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK,  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        document.add( pdfSingleSpaceTable);
        
        PdfPTable pdfDoubleSpaceTable = new PdfPTable( spaceFloat);
        PdfPCell pdfPDoubleSpaceCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK,  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPDoubleSpaceCell);
        pdfDoubleSpaceTable.addCell( pdfPDoubleSpaceCell);
        pdfPDoubleSpaceCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPDoubleSpaceCell);
        pdfDoubleSpaceTable.addCell( pdfPDoubleSpaceCell);
        document.add( pdfDoubleSpaceTable);
        
        PdfPTable pdfDoubleSpaceTable1 = new PdfPTable( spaceFloat);
        pdfPDoubleSpaceCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK,  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPDoubleSpaceCell);
        pdfDoubleSpaceTable1.addCell( pdfPDoubleSpaceCell);
        pdfPDoubleSpaceCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK,  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPDoubleSpaceCell);
        pdfDoubleSpaceTable1.addCell( pdfPDoubleSpaceCell);
        document.add( pdfDoubleSpaceTable1);
        
        log.info("Create aura space finish");
        
    }
    
    /**
     * Description: filtering string
     * 
     * @param java.lang.String
     * @return java.lang.String
     */
    public String filterString(String value) {
    	 log.info("Filter string start");
        String output = value;
       
        if (output.contains( "]")) {
            int index = output.lastIndexOf( ']');
            output = output.substring( index + 1);
        }
        log.info("Filter string finish");
        return output;
    }
    
    /**
     * Description: display sub Aura details in the PDF Section
     * 
     * @param java.util.Vector
     *            com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    
    public void createSubAuraDetails(List subQuestions, CostumPdfAPI pdfAPI,
            Document document, double subItr,double negativeFactor,String pdfType) throws DocumentException {
    	log.info("Craete sub aura details start");

        double subIter = subItr;
        double negativeFact = negativeFactor;
       
        Rule subTransferObject = null;
        PdfPTable pdfSubDetailsTable = null;
        float[] f1;
        if (null != subQuestions) {
            Collections.reverse( subQuestions);
            if("UW".equalsIgnoreCase(pdfType)){   
            	 Collections.reverse( subQuestions);
            	float[] auraQAfloat = { (float) subIter,(float) ( 0.55 - negativeFact), 0.2f, .2f };          	
            	f1 = auraQAfloat;
            }else{
            	float[] auraQAfloat = { (float) subIter, 0.25f, 0.25f };
            	f1 = auraQAfloat;
            }
            for (int subQuesItr = 0; subQuesItr < subQuestions.size(); subQuesItr++) {
                subTransferObject = (Rule) subQuestions.get( subQuesItr);                
                if (!subTransferObject.getQuestionTypeId().equalsIgnoreCase( "40")
                        && subTransferObject.isVisible()) {
                    pdfSubDetailsTable = new PdfPTable( f1);                   
                    document.add( createAuraQuestionCell(pdfSubDetailsTable, pdfAPI, document, subTransferObject, 30, pdfType));
                    if (null != subTransferObject.getAnswer().getListRule()) {
                        
                    	subIter = (float) ( subIter + 0.01);
                        
                        if("UW".equalsIgnoreCase(pdfType)){
                        	negativeFact = (float) negativeFact + 0.01;
                        	 createSubAuraDetails( subTransferObject.getAnswer().getListRule(), pdfAPI, document, subIter,negativeFact,pdfType);
                        }else{
                        	createSubAuraDetails( subTransferObject.getAnswer().getListRule(), pdfAPI, document, subIter,0.00,pdfType);
                        }
                       
                        
                        
                        
                    }
                }
                
            }
        }
        log.info("Craete sub aura details finish");
        
    }
    
    /**
     * Description: Adding line space in the PDF
     * 
     * @param
     * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    public void addLineSpace(CostumPdfAPI pdfAPI, Document document, String pdfType)
            throws DocumentException {
          log.info("Add line space start");
       
       
        PdfPTable pdfSingleSpaceTable = null;
        if("UW".equalsIgnoreCase(pdfType)){
        	float [] f1 ={.8f,.2f};
             pdfSingleSpaceTable = new PdfPTable(f1);
        }else{        	
             pdfSingleSpaceTable = new PdfPTable(1);
        }
        
        PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        if("UW".equalsIgnoreCase(pdfType)){
        	 pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addParagraph( "",  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
             pdfPSingleSpaceCell.setBorderColor( pdfAPI.getBorder_color());
             pdfPSingleSpaceCell.setBorder(PdfPCell.LEFT);
             pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        }
       
        document.add( pdfSingleSpaceTable);
        log.info("Add line space finish");
        
    }
    
    /**
     * Description: display declaration details in the PDF Section
     * 
     * @param com.metlife.eapplication.dto.ApplicationDTO
     *            com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException 
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    
   
    
       
    private void createSectionHeader(String text, String pdfType, CostumPdfAPI pdfAPI,Document document) throws DocumentException{
    	log.info("Create section header start");
    	PdfPTable pdfPTable = null;
    	if("UW".equalsIgnoreCase(pdfType)){
    		float [] f1={0.8f, 0.2f};
    		pdfPTable = new PdfPTable(f1);
    	}else{
    		pdfPTable = new PdfPTable( 1);
    	}    	
        PdfPCell pdfInsuranceCell = new PdfPCell(
                pdfAPI.addParagraph( text,  new Font( pdfAPI.getSwsMed(), 10,0,  pdfAPI.getHeading_color())));
        pdfInsuranceCell.setBorderWidth( 1);
        pdfInsuranceCell.setBorderColor( pdfAPI.getBorder_color());
        pdfAPI.disableBorders( pdfInsuranceCell);
        if(pdfAPI.getFundCode()!=null && "INGD".equalsIgnoreCase(pdfAPI.getFundCode())) {
            pdfInsuranceCell.setBackgroundColor(pdfAPI.getHeader_color());
        }
        else {
            pdfInsuranceCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
        }
        pdfInsuranceCell.setPaddingBottom( 6);
        pdfInsuranceCell.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPTable.addCell( pdfInsuranceCell);
        if("UW".equalsIgnoreCase(pdfType)){
        	 pdfPTable = createEmptyCellForUW(pdfAPI, pdfPTable);
        }  
       
        document.add( pdfPTable);
        log.info("Create section header finish");
    }
    
    private PdfPTable createEmptyCellForUW(CostumPdfAPI pdfAPI,PdfPTable pdfPTable){
    	log.info("Create empty cell for UW start");
    	PdfPCell pdfInsuranceCell = new PdfPCell(
                 pdfAPI.addParagraph( "", new Font( pdfAPI.getSwsLig(), 10,0,  pdfAPI.getHeading_color())));
         pdfInsuranceCell.setBorderWidth( 1);
         pdfInsuranceCell.setBorderColor( pdfAPI.getBorder_color());
         pdfAPI.disableBorders( pdfInsuranceCell);
         if(pdfAPI.getFundCode()!=null && "INGD".equalsIgnoreCase(pdfAPI.getFundCode())) {
             pdfInsuranceCell.setBackgroundColor(pdfAPI.getHeader_color());
         }
         else {
         pdfInsuranceCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
         }
         pdfInsuranceCell.setPaddingBottom( 6);
         pdfInsuranceCell.setVerticalAlignment( Element.ALIGN_CENTER);
         pdfPTable.addCell( pdfInsuranceCell);
         log.info("Create empty cell for UW finish");
         return pdfPTable;
    }
    
    private PdfPTable createAuraQuestionCell(PdfPTable pdfPTable,CostumPdfAPI pdfAPI,Document document, Rule rule, int questionIndex, String pdfType ){
    	log.info("Create aura question cell start");
    	PdfPTable pdfPTab = pdfPTable;
    	 String unitAnswer = null;
    	 PdfPCell pdfCell = null;
    	 if(questionIndex==30){
    		 pdfCell = new PdfPCell(
    	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
    	 }else{
    		 pdfCell = new PdfPCell(
    	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK
    	                        + questionIndex,  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
    	 }   
        pdfAPI.disableBorders( pdfCell);
        pdfPTab.addCell( pdfCell);
        pdfCell = new PdfPCell(
                pdfAPI.addParagraph( filterString( rule.getValue()),  new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));        
        pdfAPI.disableBorders( pdfCell);
        pdfPTab.addCell( pdfCell);
        unitAnswer = getUnitsAnswer(rule).toString();
        if(null!=unitAnswer && unitAnswer.length()>0){
        	 pdfCell = new PdfPCell(
                     pdfAPI.addParagraph(unitAnswer,new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
        }else{
        	 pdfCell = new PdfPCell(
                     pdfAPI.addParagraph( filterString( rule.getAnswer().getValue()),  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
        }
        pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
        pdfAPI.disableBorders( pdfCell);
        pdfPTab.addCell( pdfCell);  
        if("UW".equalsIgnoreCase(pdfType)){
        	pdfPTab = createDecisionCellForUW(pdfPTab, pdfAPI, getQuestionDecision(rule));
        }
   	 log.info("Create aura question cell finsh");
        return pdfPTab;
    }
    
    private StringBuilder getUnitsAnswer(Rule rule){
    	log.info("Get unit answer start");
    	StringBuilder unitVal = new StringBuilder();
    	StringBuilder firstUnitVal = new StringBuilder();
    	StringBuilder secondUnitVal = new StringBuilder();
         UnitValues unitValues = null;
         if (null != rule.getListUnitValues()) {
             for (int unitListItr = 0; unitListItr < rule.getListUnitValues().size(); unitListItr++) {
                 unitValues = rule.getListUnitValues().get( unitListItr);
                 
                 if (unitValues.getPosition().equalsIgnoreCase( "2")) {
                     secondUnitVal.append( unitValues.getValue());
                     secondUnitVal.append( " ");
                     secondUnitVal.append( unitValues.getUnit());
                 }
                 if (unitValues.getPosition().equalsIgnoreCase( "1")) {
                     firstUnitVal.append( " ");
                     firstUnitVal.append( unitValues.getValue());
                     firstUnitVal.append( " ");
                     firstUnitVal.append( unitValues.getUnit());
                 }
                 
             }
             if (secondUnitVal.length() > 0 || firstUnitVal.length() > 0) {
                 unitVal.append( firstUnitVal);
                 unitVal.append( " ");
                 unitVal.append( secondUnitVal);
             }
         }
          log.info("Get unit answer finish");
         return unitVal;
    }
    public static Boolean checkQuestionsForHide(String question,
    		List questionHideVec) {
    	log.info("Check quetsion for hide start");

        Boolean isQuestionToHide = Boolean.FALSE;
        if (null != questionHideVec) {
            for (int itr = 0; itr < questionHideVec.size(); itr++) {
                String questionText = (String) questionHideVec.get( itr);
                if (question.contains( questionText)) {
                    isQuestionToHide = Boolean.TRUE;
                    break;
                }              

            }
        }
        log.info("Check quetsion for hide finish");
        return isQuestionToHide;
    }
    

    /**
     * Description: display declaration details in the PDF Section
     * 
     * @param com.metlife.eapplication.dto.ApplicationDTO
     *            com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
     * @return void
     * @throws DocumentException,MetlifeException,
     *             IOException
     */
    
   /* public void declarationDetails(List disclosureStatementList,
            CostumPdfAPI pdfAPI, Document document) throws DocumentException {
    	log.info("Declaration details start");

       
        PdfPTable pdfPDeclarationTable = null;
        DisclosureStatement disclosureStatement = null;
        PdfPCell pdfPDeclarationCell = null;
        PdfPTable pdfPTable = null;
        PdfPTable pdfPTableAck = null;
        PdfPTable pdfPTableStatement = null;
        PdfPCell pdfPCell = null;
        PdfPCell pdfPCell1 = null;
        PdfPCell pdfPCelAck = null;
        
        pdfPTable = new PdfPTable( 1);
        pdfPCell = new PdfPCell(
                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
        pdfAPI.disableBorders( pdfPCell);
        pdfPCell.setPadding( 4);
        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
        pdfPTable.addCell( pdfPCell);
        document.add( pdfPTable);
        
        if ( null != disclosureStatementList && disclosureStatementList.size()>0 && !disclosureStatementList.isEmpty()) {
            
            pdfPDeclarationTable = new PdfPTable( 1);
            pdfPDeclarationCell = new PdfPCell(
                    pdfAPI.addParagraph( "Declaration", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
            pdfPDeclarationCell.setBorderWidth( 1);
            pdfPDeclarationCell.setBorderColor( pdfAPI.getColor( sectionBackground));
            pdfAPI.disableBorders( pdfPDeclarationCell);
            pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
            pdfPDeclarationCell.setPaddingBottom( 6);
            pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
            pdfPDeclarationTable.addCell( pdfPDeclarationCell);
            document.add( pdfPDeclarationTable);
            
            for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
                disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
                pdfPTable = new PdfPTable( 1);
                pdfPCell = new PdfPCell(
                        pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, FontFactory.HELVETICA, 8, Font.NORMAL, colorStyle));
                pdfAPI.disableBorders( pdfPCell);
                pdfPCell.setPadding( 4);
                pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTable.addCell( pdfPCell);
                document.add( pdfPTable);
                
                float[] f = { .9f, 0.1f };               
                if (null != disclosureStatement
                        && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
                    pdfPTableStatement = new PdfPTable( 1);
                    pdfPCell1 = new PdfPCell(
                            pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfPCell1);
                    pdfPCell1.setPadding( 4);
                    pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
                    pdfPTableStatement.addCell( pdfPCell1);
                    document.add( pdfPTableStatement);
                    
                    pdfPTableAck = new PdfPTable( f);
                    
                   if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
                	   pdfPCelAck = new PdfPCell(
                               pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                   }else if(null!=disclosureStatement.getStatement() && (disclosureStatement.getStatement().contains("I/") || disclosureStatement.getStatement().contains("I /"))){
                    	pdfPCelAck = new PdfPCell(
                                pdfAPI.addParagraph( "I/We acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                        
                    }else{
                    	pdfPCelAck = new PdfPCell(
                                pdfAPI.addParagraph( "I acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
                        
                    }                  
                    
                    
                    
                    pdfAPI.disableBorders( pdfPCelAck);
                    pdfPCelAck.setPadding( 4);
                    pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                    pdfPTableAck.addCell( pdfPCelAck);
                    pdfPCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
                    pdfAPI.disableBorders( pdfPCell1);
                    pdfPCell1.setPadding( 4);
                    pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                    pdfPTableAck.addCell( pdfPCell1);
                    document.add( pdfPTableAck);
                }
                
            }
        }
        pdfPDeclarationTable = null;
        disclosureStatement = null;
        pdfPDeclarationCell = null;
        pdfPTable = null;
        pdfPTableAck = null;
        pdfPTableStatement = null;
        pdfPCell = null;
        pdfPCell1 = null;
        pdfPCelAck = null;
        log.info("Declaration details finish");
        
    }*/

}
