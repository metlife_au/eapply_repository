package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.PersonalStatementPageObjects;

import org.openqa.selenium.By;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesHealthAPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesHealthAPage enterColesHealthADetails() {
		fillColesHealthA();
		return new ColesHealthAPage(scriptHelper);
	} 

	public ColesHealthAPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillColesHealthA() {
		String height = dataTable.getData("MetLife_Data", "Height");
		String heightType = dataTable.getData("MetLife_Data", "HeightType");
		String weight = dataTable.getData("MetLife_Data", "Weight");
		String weightType = dataTable.getData("MetLife_Data", "WeightType");
		String strCitizen = dataTable.getData("MetLife_Data", "Citizen");
		String strHealthA = dataTable.getData("MetLife_Data", "healtha.last5yrs");
		String strHBPInvestigation = dataTable.getData("MetLife_Data", "healtha.bpinvestigation"); 
		String strTreatmentRequired = dataTable.getData("MetLife_Data", "healtha.treatment");
		String strAdviceByDoctor = dataTable.getData("MetLife_Data", "healtha.advisedbydoctor");
		String strBPReadingTaken = dataTable.getData("MetLife_Data", "healtha.12Monthsago");
		String strTCID = dataTable.getData("MetLife_Data", "TC_ID");
		String strEmail = dataTable.getData("MetLife_Data", "Email");
		String StrURL=dataTable.getData("MetLife_Data", "_url");
		try {
			if(strCitizen.equalsIgnoreCase("No"))
			{
				tryAction(fluentWaitElement(ColesHealthAPageObjects.rdAusCitizenNo),"Click", "Are you Australian Citizen - No");
				tryAction(fluentWaitElement(ColesHealthAPageObjects.rd457Yes),"Click", "Do you hold 457 - Yes");
			}else{
				tryAction(fluentWaitElement(ColesHealthAPageObjects.rdAusCitizenYes),"Click", "Are you Australian Citizen - Yes");
			}
			tryAction(waitForClickableElement(ColesHealthAPageObjects.txtHeight),"SET", "Height", height);
			
			tryAction(waitForClickableElement(ColesHealthAPageObjects.selHeight),"click", "Height Type");
			tryAction(waitForClickableElement(ColesHealthAPageObjects.linkHeightCm),"click", "cm");
			
			tryAction(waitForClickableElement(ColesHealthAPageObjects.txtWeight),"SET", "Weight", weight);
			//tryAction(waitForClickableElement(ColesHealthAPageObjects.selWeight),"DropDownSelect", "Weight Type", weightType);
			
			if(strHealthA.equalsIgnoreCase("High Blood Pressure")){
				tryAction(waitForClickableElement(ColesHealthAPageObjects.chkBoxHBP),"Click", "Have you suffered,diagnosed in last 5 years - "+strHealthA);
				String strHBP = null;
				String strHBPId = null;
				strHBP = getAttribute(waitForClickableElement(ColesHealthAPageObjects.chkBoxHBP), "for");
				strHBPId ="radio"+org.apache.commons.lang.StringUtils.substring(strHBP, 5, 10)+"_0_0"; 
				System.out.println("strHBPId is : "+strHBPId);
				//tryAction(waitForClickableElement(ColesHealthAPageObjects.chkBoxHBP),"Click", "Are you awaiting further investigations such as blood or urine tests, ECG, echocardiogram or chest x-ray? - "+strHealthA);
				sleep();
				tryAction(strHBPInvestigation.equalsIgnoreCase("Yes")?waitForClickableElement(getObject("input[type='radio'][name='"+strHBPId+"'][value='Yes']+*")):waitForClickableElement(getObject("input[type='radio'][name='radio1-1-4_0_0'][value='No']+*")),"Click", "Are you awaiting further investigations such as blood or urine tests, ECG, echocardiogram or chest x-ray? - "+strHealthA);
				if(strHBPInvestigation.equalsIgnoreCase("no")){
					tryAction(waitForClickableElement(ColesHealthAPageObjects.drpdownHBPTreated),"click", "How is your high blood pressure being treated?");
					if(strTreatmentRequired.equalsIgnoreCase("requiremedication")){
						tryAction(waitForClickableElement(ColesHealthAPageObjects.linkTextRequire),"click", "Requiring medication and/or lifestyle changes as advised by your doctor?");
						String strMedication=null;
						strMedication = strHBPId+"_1_0_1_0";
						//tryAction(strAdviceByDoctor.equalsIgnoreCase("Yes")?waitForClickableElement(getObject("input[type='radio'][name='"+strMedication+"'][value='Yes']+*")):waitForClickableElement(getObject("input[type='radio'][name='"+strMedication+"'][value='No']+*")),"Click", "Are you awaiting further investigations such as blood or urine tests, ECG, echocardiogram or chest x-ray? - "+strMedication);
						tryAction(waitForClickableElement(ColesHealthAPageObjects.btnSelect),"Click", "Select");
						if(strAdviceByDoctor.equalsIgnoreCase("Yes")){
							tryAction(waitForClickableElement(getObject("input[type='radio'][name='"+strMedication+"'][value='Yes']+*")),"Click", "Are you taking your medication or modified your lifestyle as advised by your doctor? - "+strMedication);
							if(strBPReadingTaken.equalsIgnoreCase("Yes")){
								tryAction(waitForClickableElement(ColesHealthAPageObjects.radMoreThan12Months),"Click", "When was your last blood pressure reading taken? - More Than 12 Months ago");
							}else{
								tryAction(waitForClickableElement(ColesHealthAPageObjects.radLessThan12Months),"Click", "When was your last blood pressure reading taken? - Less Than 12 Months ago");
							}
							
						}else{
							tryAction(waitForClickableElement(getObject("input[type='radio'][name='"+strMedication+"'][value='No']+*")),"Click", "Are you taking your medication or modified your lifestyle as advised by your doctor? - "+strMedication);
						}
						
						
					}else{
						tryAction(waitForClickableElement(ColesHealthAPageObjects.linkTextNotRequire),"click", "Not requiring medication or lifestyle changes as advised by your doctor");
					}
				}
				
			}else{
			
			tryAction(waitForClickableElement(ColesHealthAPageObjects.chlLast5YrsNone),"Click", "Have you suffered,diagnosed in last 5 years - None");
			}
			sleep(2000);
			if(strTCID.contains("SecondpageSave")){
				tryAction(waitForClickableElement(ColesHealthAPageObjects.linktextSave),"Click", "Save and Email on Page2");
				sleep(2000);
				tryAction(waitForClickableElement(ColesHealthAPageObjects.txtSaveEmail),"SET", "Save Email", strEmail);
				sleep(1000);
				tryAction(waitForClickableElement(ColesHealthAPageObjects.linkConfirm),"Click", "Confirming Save");
				String SaveRefNumber=waitForClickableElement(ColesHealthAPageObjects.txtsGetRefNumber).getText();
				System.out.println(SaveRefNumber);
				//tryAction(waitForClickableElement(ColesHealthAPageObjects.linkSavepopclose),"Click", "Save Popup Close");
				//switchToMainFrame();
				//switchToActiveWindow();
				//driver.manage().window().maximize();
				//String Strtitle = driver.getTitle();
				//System.out.println("Hi");
				//System.out.println(Strtitle);
				//System.out.println("Hi2");
				//driver.quit();
				driver.get(StrURL);
				tryAction(waitForClickableElement(ColesHealthAPageObjects.linktextRetrieve),"Click", "Retrieve Link");
						}
			else{
			tryAction(waitForClickableElement(ColesHealthAPageObjects.lnkNextId),"MoveToElm","Next");
			sleep();
			tryAction(waitForClickableElement(ColesHealthAPageObjects.lnkNextId),"Click", "Next");
			}
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
