// create the module and name it claimApp
        // also include ngRoute for all our routing needs
    var claimApp = angular.module('claimTrackermain', ['ngRoute','ngResource','vcRecaptcha','googleAnalytics']);
    var count;
    var disableFields = false;
	var datesBetweenErrflag=false;
    // configure our routes
   claimApp.config(function($routeProvider,$httpProvider) {
	   $httpProvider.defaults.withCredentials = true;
        $routeProvider   
        
        	//.when('/', {
          .when('/:id/:fundid', {    
          
        	  templateUrl : 'static/pages/home.html',
              controller  : 'login',
              resolve: {
                  properties: function(propertyService){
                      return propertyService.getProperties();                      
                  }
              },
             reloadOnSearch: false,
             title: 'Claims Tracker - Login'
            }) 
//            .when('/', {     
//          
//                templateUrl : 'static/pages/home.html',
//                controller  : 'login',
//                resolve: {
//                    properties: function(propertyService){
//                        return propertyService.getProperties();
//                    }
//                },
//                
//               reloadOnSearch: false,
//               title: 'Claims Tracker - Login'
//            })
//           
            
           /* .when('/landing', {
                templateUrl : 'care/landingpage.html',
                controller  : 'landing'
            })*/
            .when('/timeline', {
                templateUrl : 'static/pages/timeline.html',
                controller  : 'timeline',
                	title: 'Claims Tracker-Timeline'
            })
            .when('/todo', {
                templateUrl : 'static/pages/to-dos.html',
                controller  : 'todo',
                title: 'Claims Tracker-To-do'
            })
             .when('/completed', {
                templateUrl : 'static/pages/completed.html',
                controller  : 'completed',
                	title: 'Claims Tracker-Completed'
            })
            .when('/contact', {
                templateUrl : 'static/pages/contact.html',
                controller  : 'contact',
                	title: 'Claims Tracker-Contact'
            })
            .when('/completedEmpty', {
                templateUrl : 'static/pages/completed-empty.html',
                controller  : 'completedEmpty',
                	 title: 'Claims Tracker-CompletedEmpty'
            })
            .when('/todoEmpty', {
                templateUrl : 'static/pages/to-dos-empty.html',
                controller  : 'todoEmpty',
                title: 'Claims Tracker-TodoEmpty'
            })
            .when('/upload', {
                templateUrl : 'static/pages/upload.html',
                controller  : 'upload',
                	title: 'Claims Tracker-Upload'
            })
            .when('/main', {
           
                templateUrl : 'static/main.html',
               controller  : 'mainPage'
           }) 
           .when('/error', {
           
                templateUrl : 'static/pages/error.html'               
           }) 
           .otherwise({
        	   redirectTo: '/'
           });
    });
   
   claimApp.run([
	    'gaTracker', '$rootScope', function(gaTracker, $rootScope) {
	      // subscribe to events
	      $rootScope.$on('$routeChangeSuccess', function() {
	      	gaTracker.sendPageview(sessionStorage.getItem('UniqueAppNum') || '');
	      });
	    }
	  ]);
   
   claimApp.run(function($rootScope, $route, $location){
	   //Bind the `$locationChangeSuccess` event on the rootScope, so that we dont need to 
	   //bind in induvidual controllers.

	   $rootScope.$on('$locationChangeSuccess', function(currentRoute) {
	        $rootScope.actualLocation = $location.path();
	        $rootScope.title = $route.current.title;	        
	    });        

	   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
	        if($rootScope.actualLocation === newLocation) {
	          // alert('Why did you use history back?'+oldLocation);
	           $location.path( oldLocation ); 
	        }
	    });
	});
   
   claimApp.controller('mainPage',['$scope', '$rootScope', '$location','claimFactory','propertyService','SessionTimeoutService','$routeParams',  function($scope,$rootScope, $location,claimFactory,propertyService,SessionTimeoutService,$routeParams){
	   //	alert('hi');
	          SessionTimeoutService.removeListeners();	      
	          $rootScope.fundName = claimFactory.getPartnerName();	
	          if ($rootScope.fundName  != null) {
	              $rootScope.header = {name: "header.html", url: "static/pages/"+$rootScope.fundName+"/header.html"};
		          $rootScope.footer = {name: "footer.html", url: "static/pages/"+$rootScope.fundName+"/footer.html"};
		          //$rootScope.disclaimer = {name: "disclaimer.html", url: "static/pages/"+$rootScope.fundName+"/disclaimer.html"};
	          }
	         // alert(claimFactory.getPartnerName());
	          var disableFields = false;
	          $scope.message = 'Look! I am an about page.';
	          $rootScope.headerSection=false;	
	          $rootScope.contactHeader=false;
	          $scope.claimData = claimFactory.getClaimData();
	          $scope.claimFactory = claimFactory;	
	         //$scope.configObject = propertyService.getProperties();
	   	    // claimFactory.setLabelProperties($scope.configObject);
	          $scope.go = function ( path ) {  
		          	//   alert('Hi1');
		           	  $location.path( path );
		           	};
		         $scope.logout= function () {
			     	  //  Session.clear();
		        	 claimFactory.setDisableContcHdr(true);
		        	 claimFactory.manageHistory('logout is succesful','logout');
		        	var partnerName = claimFactory.getPartnerName();
		        	 sessionStorage.clear();
			     	   $scope.go('/ct/'+partnerName);
			     	};
			     	
			     	$scope.back= function () {
				     	  //  Session.clear();
			     	 claimFactory.setDisableContcHdr(true);
			     	 claimFactory.manageHistory('back to login page','back');
			     	var partnerName = claimFactory.getPartnerName();
			     	 sessionStorage.clear();
				     	   $scope.go('/ct/'+partnerName);
				     	};
			    
	}]); 
  
 claimApp.controller('login',['$scope','$rootScope', '$location','$http','claimFactory','vcRecaptchaService', 'propertyService','$routeParams','properties','$timeout','$window','SessionTimeoutService',  function($scope,$rootScope, $location, $http,claimFactory,vcRecaptchaService, propertyService,$routeParams,properties,$timeout, $window,SessionTimeoutService){
	 SessionTimeoutService.removeListeners();
	 var partnerNotAvailable = false;
	 var claimRes = claimFactory.retrieveFunds();
	 claimRes.success(function(data, status, headers, config) {
  	   $scope.names = data;
  	   for (var i=0;i<data.length;i++){
  		   if(JSON.stringify(data[i].PartnerName) == '"'+$routeParams.fundid+'"'){
  			 partnerNotAvailable=false;
  				break;     
  		   } else {
  			 partnerNotAvailable=true; 
  		   }
  	   }
  	  if(partnerNotAvailable){
  		$location.path('/error');
	  }
  	if ($rootScope.fundName  != null) {
        $rootScope.header = {name: "header.html", url: "static/pages/"+$rootScope.fundName+"/header.html"};
        $rootScope.footer = {name: "footer.html", url: "static/pages/"+$rootScope.fundName+"/footer.html"};
        $rootScope.disclaimer = {name: "disclaimer.html", url: "static/pages/"+$rootScope.fundName+"/disclaimer.html"};
    }
	 claimFactory.setPartnerName($rootScope.fundName);
     }),
     claimRes.error(function(data, status, headers, config) {	
    	 $location.path('/error');
     });
			
	 if ($routeParams.fundid == 'Metlife') {
		 $rootScope.fundName = 'MetLife';
	 } else {
		 $rootScope.fundName = $routeParams.fundid;
	 }
	 
	// alert($rootScope.fundName);
	 
	 var partner ='';	 
	   var  loginresponse;
	   var retrieveResponse;
	   $rootScope.timerFlag = false;
	   $scope.configObject = properties.data;
	   claimFactory.setLabelProperties(properties.data);
	   //$rootScope.partnerName='Metlife';
	   $.get("https://ipinfo.io", function(response) {
		   claimFactory.setUserIp(response.ip);			
	    }, "jsonp");
	   
	  // $scope.partnerName = "Metlife";
	//   if ( $routeParams.fundid !== undefined) {
		//   alert('login'+partner);
		  // partner = $routeParams.fundid;
		   //$scope.appName = $routeParams.fundid;
		  // $('#customId').attr('href',"static/pages/"+partner+"/dist/css/custom.css");
		  // 	$('#favIconId').attr('href',"static/pages/"+partner+"/assets/img/favicon-m.ico");
		  // 	$('#vendorImportId').attr('href',"static/"+partner+"/pages/dist/css/vendor-imports.min.css");
		  // 	$('#mainMinId').attr('href',"static/pages/"+partner+"/dist/css/main.min.css");
		  // 	$('#partnerId').attr('src',"static/pages/CareSuper/assets/img/MetLife.png");
//		   $('#favIconId').attr('href',"static/pages/"+partner+"/img/favicon.ico");
//		   $('#partnerId').attr('src',"static/pages/"+partner+"/img/header_logo.png");
//		   $('#partnerId').attr('alt',partner+" logo");
//		   $('#footerId').attr('src',"static/pages/"+partner+"/img/footer_logo.png");
//		   $('#footerId').attr('data-src',"static/pages/"+partner+"/img/footer_logo.png");
//		   $('#footerId').attr('alt',partner);
		   //$('.fundName').val(partner);
//		   $scope.partnerName=partner;
//		   if(partner.toLowerCase() == 'caresuper'){
//			   $('body').attr('id', 'caresuper');
//		   }
	//   } 
	   //claimFactory.setPartnerName($scope.partnerName);
	   //$scope.configObject = claimFactory.getLabelProperties();
	   
	   	
	   
       $scope.message = '';  
       $scope.captacheMessage = '';
       $rootScope.headerSection=false;
       $rootScope.contactHeader=false;
       $scope.data = {}; 
      // $scope.errorMsg=''  
       $scope.regex = /[0-9]{1,3}/;
       $scope.datePattern= /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/\-]\d{4}$/;
      // $scope.data = {};  
       //alert($scope.configObject.captchePublicKey);
       $scope.user = {claimNo:'',lName:'',dob:'',publicKey:$scope.configObject.captchePublicKey};  
       $scope.check = "This is for check"       
   	   $scope.errorMsg=''   		
      // $scope.configObject = propertyService.getProperties();
       $scope.user.publicKey = $scope.configObject.captchePublicKey;       
     // added for session expiry
      /* $timeout(callAtTimeout, 600000); 
     	function callAtTimeout() {
     		$location.path("/");
     	}*/
       /*var timer;
	    angular.element($window).bind('mouseover', function(){
	    	timer = $timeout(function(){
	    		sessionStorage.clear();
	    		localStorage.clear();
	    		$location.path("/");
	    	}, 600000);
	    }).bind('mouseout', function(){
	    	$timeout.cancel(timer);
	    });*/
       
       $scope.go = function (form,user,path) {    	   
    	  	  
    		$scope.captacheMessage = '';
    		$scope.datesBetweenErrflag=false;
       //alert('Hi12 '+vcRecaptchaService.getResponse($scope.recaptchaId));
    	   if(!form.$valid){             
    		 //alert(vcRecaptchaService.getResponse($scope.recaptchaId));
    		 if(vcRecaptchaService.getResponse($scope.recaptchaId) === ""){ //if string is empty
 			   	$scope.captacheMessage = $scope.configObject.robotErr;
    		 } 
    	   } else {
    		//   alert('captche :'+vcRecaptchaService.getResponse($scope.recaptchaId));
    		   if(vcRecaptchaService.getResponse($scope.recaptchaId) === ""){ //if string is empty
    			   	$scope.captacheMessage = 'captche failed,please try again';
             		 form.$submitted=true;
             		 form.$valid = false;
         		 }else {
             		//alert(vcRecaptchaService.getResponse()) 
         			$rootScope.timerFlag = true;
		          	 var data = $.param({
		             		 secret : $scope.configObject.captcheServerkey,
		             		 response:vcRecaptchaService.getResponse($scope.recaptchaId),
		             		 remoteip:$scope.configObject.ipaddress
		             		 
		         		 });      

		//	alert('data '+data);
		//	claimFactory.verifyCaptche(data,$scope.configObject.captcheUrl).then(function(response) {
				
			//	if (response.status == '200') {
		          	 retrieveResponse = claimFactory.retrieveLoginAttempts(claimFactory.getUserIp());		            	
		          	retrieveResponse.success(function(data, status, headers, config) {					
		      			
		                    if (status == '200' && data != null && '' != data) { 
		                  	  //alert('lock :'+data.ipLocked);
		                    	if (data.ipLocked == 'Y'){
		                    		// var NowMoment = moment().format('YYYY-M-D HH:mm:ss');
		                    		// var NowMoment1 = moment(data.lastLogin).format('YYYY-M-D HH:mm:ss');
		                    		//var currentDate = moment().toObject();
		                    		//var lastLoginDate = moment(data.lastLogin).toObject();
		                    		//[2013, 1, 4, 14, 40, 16, 154]
		                    		//alert(moment([currentDate.years, currentDate.months, currentDate.date,currentDate.hours,currentDate.minutes,currentDate.seconds],))
		                    		
		                  		//  alert('It is locked'+ moment(data.lastLogin).duration().minutes()+' '+moment().duration().minutes());
		                    	//	alert('4'+data.unlock);
		                    		if (data.unlock == true) {
		                    			claimFactory.setLoginAttempts(0);
		                    			loginresponse= claimFactory.manageLoginAttempts(claimFactory.getUserIp(),-1);
		                    		}else {
		                    			$scope.data='';
		                  		  	    $scope.message = "You have made 5 unsuccessful attempts to log into Claims Tracker. Please confirm your details and try again after 30 minutes, or contact MetLife if you need further assistance. ";		                  		
		                  		  	     return;
		                    		}
		                  	  } else {
		                  		 // alert('1 '+data.attempts);
		                  		  if (null != data.attempts) {
		                  			  claimFactory.setLoginAttempts(data.attempts);
		                  		  } else {
		                  			 claimFactory.setLoginAttempts(0); 
		                  		  }
		                  	  }
		                  		  
		                  		var response = claimFactory.getClaim(user,$scope.configObject.claimServiceURL,claimFactory.getPartnerName());	
		    					//claimFactory.getClaim(user,$scope.configObject.claimServiceURL)
		    		          	response.success(function(data, status, headers, config) {						
		    					
		    						//alert( response);
		    			           	  $scope.data =  data;	
		    			           	 // alert('$scope.data '+$scope.data); 
		    			               if ($scope.data != null && '' != $scope.data) { 
		    			            	  // alert('H1i'+form.$valid); 
		    			            
		    			            	   var toDate= moment().format('DD/MM/YYYY');
		    			            	   var fromDate = $scope.data.statusDate;
		    			            	   var dt1 = fromDate.split('/'),
		    			                     dt2 = toDate.split('/'),
		    			                     one = new Date(dt1[2], dt1[1], dt1[0]),
		    			                     two = new Date(dt2[2], dt2[1], dt2[0]);
		    			                 
		    			            	   var millisecondsPerDay = 1000 * 60 * 60 * 24;
		    			            	   var millisBetween = two.getTime() - one.getTime();
		    			            	   var days = millisBetween / millisecondsPerDay;
		    			               
		    			            	   // var months = parseInt(moment().diff(moment([year, month, day]), 'months', true));
		    			            	   claimFactory.setClaimStatus($scope.data.status);
		    			            	  // alert(claimFactory.getClaimStatus());
		    			            	   	if ((claimFactory.getClaimStatus() =='Decision Complete') && days > 365) {
		    			            	   // Write the logic here to show error if status date exceeds 12 months
		    			            		$scope.datesBetweenErrflag=true;
		    			            		//$scope.datesBetweenMsg = "It has been 12 months since your claim has closed, therefore you cannot access the claims tracker. To contact a member of staff you can call us on 1300 555 625 or email ";
		    			            		$scope.datesBetweenMsg = "This claim has been closed for longer than 12 months, if you require access to the information please contact the call centre";
		    			            		//$scope.emailLink = 'auservices@metlife.com';	
		    			            		
		    			            	   } else if((claimFactory.getClaimStatus() =='Decision Complete' && days <= 365) || (claimFactory.getClaimStatus() != 'Decision Complete') ) {
		    			            		   $scope.datesBetweenErrflag=false;
		    			            	   		$scope.datesBetweenMsg = "";          
		    			            	   		claimFactory.setClaimData($scope.data); 
		    			            	   		//claimFactory.filterClaimData($scope.data);
		    			            	   		claimFactory.setUniqueId($scope.data.uniqueId);
		    			            	   		//alert(claimFactory.getUniqueId());
		    			            	   		claimFactory.setClaimId($scope.data.claimId);
		    			            	   		//claimFactory.setClaimStatus($scope.data.status);
		    			            	   	   claimFactory.getClaimActivities($scope.data.uniqueId,$scope.configObject.claimServiceURL).then(function(response) {  
		    			            	  	   $scope.claimData = response.data;			            	  	   
		    			            	  	   claimFactory.setActivitiesData($scope.claimData);
		    			            	  	   claimFactory.filterClaimData($scope.claimData);
		    			            	  	   claimFactory.setDisableContcHdr(false);
		    			            	  	   $location.path( path ); 
		    			            	  	 //alert('3'+claimFactory.getLoginAttempts());
		    			            	  	   if(claimFactory.getLoginAttempts() != 0){
		    			            	  	     loginresponse= claimFactory.manageLoginAttempts(claimFactory.getUserIp(),-1);	
		    			            	  	   }
		    			            	  	 claimFactory.manageHistory($scope.configObject.loginSuccess,$scope.configObject.loginStatus);
		    				        					 
		    			            	  	   //alert('hi');
		    			            	  	   },function (error) {			            	  		
		    			            	  		if( error.status == '500'){
		    			            	  			    $scope.data = ''
		    					            	        form.$submitted=true;
		    					            	  		form.$valid = false; 
		    					        			    $scope.message = "Internal Server Error,Please contact adminstrator";
		    					        		} else  {
		    					        			if (error.status == '404' && error.data.errors == undefined) {
		    					        				 $scope.data = ''
		    							            	 form.$submitted=true;
		    							            	 form.$valid = false; 
		    					        				 $scope.message = "Resource not found,Please contact adminstrator";
		    					        			} else if(error.status == '404'  && error.data.errors != 'undefined' &&  error.data.errors.length > 0){
		    					        				 claimFactory.setActivitiesData(null);
		    					        				 claimFactory.filterClaimData(null);
		    					        				 claimFactory.setDisableContcHdr(false);
		    					        				 $location.path( path ); 
		    					        				 claimFactory.manageHistory($scope.configObject.loginSuccess,$scope.configObject.loginStatus);
		    					        				// alert('2'+claimFactory.getLoginAttempts());
		    					        				 if(claimFactory.getLoginAttempts() != 0){
		    		    			            	  	     loginresponse= claimFactory.manageLoginAttempts(claimFactory.getUserIp(),-1);	
		    		    			            	  	 }
		    					        			}
		    					        		}
		    			            	      }); 
		    			            	   		      
		    			            	   }
		    			   			  	     				             
		    			           	   } else {
		    			           		   $scope.data = ''
		    			           		   form.$submitted=true;
		    			           		   form.$valid = false;
		    			           		   $scope.message = "Our records don't match your responses.  Please try again, otherwise please contact our call centre for assistance ";
		    			           	   }			           	    
		    			            });
		    		          	response.error(function(data, status, headers, config)  {
		    		          			$scope.data = ''
		    			            	form.$submitted=true;
		    			        		form.$valid = false; 
		    			        		if (status == '404') {
		    			        			 $scope.message = "Our records don't match your responses.  Please try again, otherwise please contact our call centre for assistance ";
		    			        			// alert('1'+claimFactory.getLoginAttempts());
		    			        			 loginresponse= claimFactory.manageLoginAttempts(claimFactory.getUserIp(),claimFactory.getLoginAttempts());			        				
		    			        					 
		    			        		} else if(status == '500'){
		    			        			$scope.message = "Internal Server Error,Please contact adminstrator";
		    			        		} else  {
		    			        			$scope.message = "Resource not found,Please contact adminstrator";
		    			        		}
		    			        		
		    			            });	                  		  
		                  	  
		                    }
		      			
		      		});
		          	retrieveResponse.error(function(data, status, headers, config) {
		      			
		      		});
		           
		          	if (loginresponse != undefined) {
		          	    loginresponse.success(function(data, status, headers, config) {       				 
		       		    }),
		       			 loginresponse.error(function(data, status, headers, config) {		       				 
		       		    })	
         		    }
					
//				} else {
//					 $scope.captacheMessage = 'captche server verification failed,please try again';
//            		 form.$submitted=true;
//            		 form.$valid = false;
//				}								
//			},function (error) {
//				$scope.captacheMessage = 'captche server verification failed,please try again';
//            	form.$submitted=true;
//        		form.$valid = false;        		
//            });
       }					  
        	     	    	   
     };
   }  
 // added for disabling the timline, to-do's and complet     };
   $scope.needHelp = function(response) {
	   //disableFields = true;	   
   }
   
   $scope.onCapctheCreate = function(widgetId) {	 
	//   alert('widgetId :'+widgetId);
	   $scope.recaptchaId = widgetId;
   }
   $scope.onCapctheSuccess = function() {
	   $scope.captacheMessage = '';
   }
   $scope.onCapctheExpire = function() {
	 //  alert('onCapctheExpire');
   }
}]); 
   
 claimApp.controller('timeline',['$scope', '$rootScope','$window','$http','$location','claimFactory','$timeout','SessionTimeoutService',  function($scope,$rootScope, $window, $http, $location,claimFactory,$timeout,SessionTimeoutService){
	// alert('timeline');
	 SessionTimeoutService.removeListeners();
     SessionTimeoutService.setup();
    $scope.$on('$destroy', function() {    
    		if(!$scope.$$phase){
    			$scope.$apply();
    		}
    	});
	   $rootScope.headerSection=true;
	   $scope.claimData = claimFactory.getClaimData();
	   $scope.partnerName = claimFactory.getPartnerName();
	   $scope.uniqueId = claimFactory.getClaimId();   
	   $scope.claimData = claimFactory.getActivitiesData();	 
	   claimFactory.manageHistory('',claimFactory.getLabelProperties().timelineStatus);

	   //back to top for help
	      	$scope.scrollToTop = function(){
	       	$window.scrollTo(0,0);
	      	}
	      	 // added for session expiry
	       /* $timeout(callAtTimeout, 600000); 
	      	function callAtTimeout() {
	      		$location.path("/");
	      	}*/
	      /*	var timer;
		    angular.element($window).bind('mouseover', function(){
		    	timer = $timeout(function(){
		    		sessionStorage.clear();
		    		localStorage.clear();
		    		$location.path("/");
		    	}, 100000);
		    }).bind('mouseout', function(){
		    	$timeout.cancel(timer);
		    });*/
	      	
	      /*	var timeoutID;
			 
			function setup() {
				//angular.element($window).bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', resetTimer);
				this.addEventListener("mousemove", resetTimer, false);
			    this.addEventListener("click", resetTimer, false);
			    this.addEventListener("mouseup", resetTimer, false);
			    this.addEventListener("mousedown", resetTimer, false);
			    this.addEventListener("keydown", resetTimer, false);
			    this.addEventListener("keypress", resetTimer, false);
			    this.addEventListener("keyup", resetTimer, false);
			    this.addEventListener("submit", resetTimer, false);
			    this.addEventListener("change", resetTimer, false);
			    this.addEventListener("mouseenter", resetTimer, false);
			    this.addEventListener("scroll", resetTimer, false);
			    this.addEventListener("resize", resetTimer, false);
			    this.addEventListener("dblclick", resetTimer, false);
			    startTimer();
			}
			setup();
			 
			function startTimer() {
			    timeoutID = window.setTimeout(goInactive, 600000);
			}
			 
			function resetTimer(e) {
			    window.clearTimeout(timeoutID);
			    goActive();
			}
			 
			function goInactive() {				
				claimFactory.setDisableContcHdr(true);
				claimFactory.manageHistory('session is expired','logout');
				$location.path("/");
	            $scope.$apply();
			}
			 
			function goActive() {   
			    startTimer();
			}*/
   }]); 
   
   claimApp.controller('todo',['$scope','$rootScope','$window', '$location','claimFactory','$timeout','SessionTimeoutService',  function($scope,$rootScope,$window, $location,claimFactory,$timeout,SessionTimeoutService){
	   SessionTimeoutService.removeListeners();
	     SessionTimeoutService.setup();
	    $scope.$on('$destroy', function() {    
	    		if(!$scope.$$phase){
	    			$scope.$apply();
	    		}
	    	});
	   $scope.message = 'Look! I am an about page.';
       $rootScope.headerSection=true;
       //$scope.claimNo = claimNo
       $scope.claimData = claimFactory.getClaimData();
       $scope.noOutStanding = claimFactory.getNoOutStanding(); 
       $scope.activityData = claimFactory.getActivitiesData();
       claimFactory.manageHistory('',claimFactory.getLabelProperties().todoStatus);
       $scope.scrollToTop = function(){
        	$window.scrollTo(0,0);
       }
       // added for session expiry
       /*$timeout(callAtTimeout, 600000); 
     	function callAtTimeout() {
     		$location.path("/");
     	}*/
      /* var timer;
	    angular.element($window).bind('mouseover', function(){
	    	timer = $timeout(function(){
	    		sessionStorage.clear();
	    		localStorage.clear();
	    		$location.path("/");
	    	}, 600000);
	    }).bind('mouseout', function(){
	    	$timeout.cancel(timer);
	    });*/
     /*  var timeoutID;
		 
		function setup() {
			//angular.element($window).bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', resetTimer);
			this.addEventListener("mousemove", resetTimer, false);
		    this.addEventListener("click", resetTimer, false);
		    this.addEventListener("mouseup", resetTimer, false);
		    this.addEventListener("mousedown", resetTimer, false);
		    this.addEventListener("keydown", resetTimer, false);
		    this.addEventListener("keypress", resetTimer, false);
		    this.addEventListener("keyup", resetTimer, false);
		    this.addEventListener("submit", resetTimer, false);
		    this.addEventListener("change", resetTimer, false);
		    this.addEventListener("mouseenter", resetTimer, false);
		    this.addEventListener("scroll", resetTimer, false);
		    this.addEventListener("resize", resetTimer, false);
		    this.addEventListener("dblclick", resetTimer, false);
		    startTimer();
		}
		setup();
		 
		function startTimer() {
		    timeoutID = window.setTimeout(goInactive, 600000);
		}
		 
		function resetTimer(e) {
		    window.clearTimeout(timeoutID);
		    goActive();
		}
		 
		function goInactive() {
			claimFactory.setDisableContcHdr(true);
			claimFactory.manageHistory('session is expired','logout');
			$location.path("/");
           $scope.$apply();
		}
		 
		function goActive() {   
		    startTimer();
		}*/
     	
   }]); 
   
   claimApp.controller('completed',['$scope', '$rootScope','$window','$location','claimFactory','$timeout','SessionTimeoutService',  function($scope,$rootScope,$window, $location,claimFactory,$timeout,SessionTimeoutService){
	   SessionTimeoutService.removeListeners();
	     SessionTimeoutService.setup();
	    $scope.$on('$destroy', function() {    
	    		if(!$scope.$$phase){
	    			$scope.$apply();
	    		}
	    	});
	   $scope.message = 'Look! I am an about page.';
       $rootScope.headerSection=true;
       //$scope.claimNo = claimNo
       
       $scope.claimData = claimFactory.getClaimData();
       $scope.noCompleted = claimFactory.getNoCompleted();   
       $scope.activityData = claimFactory.getActivitiesData();
       claimFactory.manageHistory('',claimFactory.getLabelProperties().completedStatus);
       $scope.scrollToTop = function(){
       	$window.scrollTo(0,0);
       }
       // added for session expiry
      /* $timeout(callAtTimeout, 600000); 
     	function callAtTimeout() {
     		$location.path("/");
     	}*/
       /*var timer;
	    angular.element($window).bind('mouseover', function(){
	    	timer = $timeout(function(){
	    		sessionStorage.clear();
	    		localStorage.clear();
	    		$location.path("/");
	    	}, 600000);
	    }).bind('mouseout', function(){
	    	$timeout.cancel(timer);
	    });*/
      /* var timeoutID;
		 
		function setup() {
			//angular.element($window).bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', resetTimer);
			this.addEventListener("mousemove", resetTimer, false);
		    this.addEventListener("click", resetTimer, false);
		    this.addEventListener("mouseup", resetTimer, false);
		    this.addEventListener("mousedown", resetTimer, false);
		    this.addEventListener("keydown", resetTimer, false);
		    this.addEventListener("keypress", resetTimer, false);
		    this.addEventListener("keyup", resetTimer, false);
		    this.addEventListener("submit", resetTimer, false);
		    this.addEventListener("change", resetTimer, false);
		    this.addEventListener("mouseenter", resetTimer, false);
		    this.addEventListener("scroll", resetTimer, false);
		    this.addEventListener("resize", resetTimer, false);
		    this.addEventListener("dblclick", resetTimer, false);
		    startTimer();
		}
		setup();
		 
		function startTimer() {
		    timeoutID = window.setTimeout(goInactive, 600000);
		}
		 
		function resetTimer(e) {
		    window.clearTimeout(timeoutID);
		    goActive();
		}
		 
		function goInactive() {
			claimFactory.setDisableContcHdr(true);
			claimFactory.manageHistory('session is expired','logout');
			$location.path("/");
           $scope.$apply();
		}
		 
		function goActive() {   
		    startTimer();
		}*/
   }]); 
   
   claimApp.controller('contact',['$scope', '$rootScope','$location', '$window','$http','propertyService','claimFactory','$timeout','SessionTimeoutService', function($scope,$rootScope, $location, $window, $http, propertyService,claimFactory,$timeout,SessionTimeoutService){
	   $rootScope.contactHeader=true;
	   SessionTimeoutService.removeListeners();
	   if (claimFactory.getDisableContcHdr() == false){
	       SessionTimeoutService.setup();
	       $scope.$on('$destroy', function() {    
	    		if(!$scope.$$phase){
	    			$scope.$apply();
	    		}
	    	});
      }
	   
	   $scope.message = 'Look! I am an about page.';       
       claimFactory.manageHistory('',claimFactory.getLabelProperties().contactStatus);
       $('.accordionHeader').on("click", function () {
    	   	   		var acc_con = $(this).next('.accordionContent');
    	   	   		if (acc_con.is(':visible')) {
    	   	   			$(this).removeClass('acc-open');
    	   	   			acc_con.slideUp(300);
    	   	   			$scope.acc_con = false;
    	   	   			$scope.$apply();
    	   	   		} else {
    	  	   			$(this).addClass('acc-open');
    	   	   			$(this).addClass('.accordionContent');
    	   	   			acc_con.slideDown(300);
    	   	   			 $scope.acc_con = true;
    	   	   			 $scope.$apply();
    	   	   		}
    	   	   	});
   
       $scope.configObject = claimFactory.getLabelProperties();
       if (null != claimFactory.getClaimData()) {
       	$scope.claimAssesor =  claimFactory.getClaimData().claimAssesor;
       	$scope.claimAssesorPhone =  claimFactory.getClaimData().claimAssesorPhone;
       }
       if($scope.claimAssesorPhone == null || $scope.claimAssesorPhone == '') {
    	   $scope.claimAssesorPhone='1300 555 625';
       }
       $scope.scrollToTop = function(){
        	$window.scrollTo(0,0);
   	   }
       
       
	     	
//       if($rootScope.timerFlag==true){
//    	   SessionTimeoutService.stopWatch();
//   		}
       // added for session expiry
       /*$timeout(callAtTimeout, 600000); 
     	function callAtTimeout() {
     		$location.path("/");
     	}*/
      /* var timer;
	    angular.element($window).bind('mouseover', function(){
	    	timer = $timeout(function(){
	    		sessionStorage.clear();
	    		localStorage.clear();
	    		$location.path("/");
	    	}, 600000);
	    }).bind('mouseout', function(){
	    	$timeout.cancel(timer);
	    });*/
      /* var timeoutID;
		 
		function setup() {
			//angular.element($window).bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', resetTimer);
			this.addEventListener("mousemove", resetTimer, false);
		    this.addEventListener("click", resetTimer, false);
		    this.addEventListener("mouseup", resetTimer, false);
		    this.addEventListener("mousedown", resetTimer, false);
		    this.addEventListener("keydown", resetTimer, false);
		    this.addEventListener("keypress", resetTimer, false);
		    this.addEventListener("keyup", resetTimer, false);
		    this.addEventListener("submit", resetTimer, false);
		    this.addEventListener("change", resetTimer, false);
		    this.addEventListener("mouseenter", resetTimer, false);
		    this.addEventListener("scroll", resetTimer, false);
		    this.addEventListener("resize", resetTimer, false);
		    this.addEventListener("dblclick", resetTimer, false);
		    startTimer();
		}
		
		function startTimer() {
		    timeoutID = window.setTimeout(goInactive, 600000);
		}
		 
		function resetTimer(e) {
		    window.clearTimeout(timeoutID);
		    goActive();
		}
		 
		function goInactive() {
			claimFactory.setDisableContcHdr(true);
			claimFactory.manageHistory('session is expired','logout');
			$location.path("/");
           $scope.$apply();
		}
		 
		function goActive() {   
		    startTimer();
		}*/
     /// added for disabling the timline, to-do's and completed sections
		//alert('header :'+claimFactory.getDisableContcHdr());
          		
                  if(claimFactory.getDisableContcHdr()==true){
                	 // alert("disableFields----" +disableFields);
                	  $rootScope.headerSection=false;
                	  
        	          $('#disableTimeline').click(function(e) {
        	              $(this)
        	                 .css('cursor', 'default')
        	                 .css('text-decoration', 'none')
        	              
        	              return false;
        	          });
        	          $('#disabletodoField').click(function(e) {
        	              $(this)
        	                 .css('cursor', 'default')
        	                 .css('text-decoration', 'none')
                   
        	              return false;
        	          });
        	          $('#disableCompleted').click(function(e) {
        	              $(this)
        	                 .css('cursor', 'default')
        	                 .css('text-decoration', 'none')
        	          
        	              return false;
        	          });
                  }else{
                	//  alert("disableFields----" +disableFields);
                	  $rootScope.headerSection=true;
                  }
        
   }]);
   claimApp.controller('completedEmpty',['$scope','$rootScope', '$location',  function($scope, $rootScope,$location){
       $scope.message = 'Look! I am an about page.';
       $rootScope.headerSection=true;
       //$scope.claimNo = claimNo
       $scope.go = function ( path ) {        	
     	  $location.path( path );
     	};
     	
   }]);
   

   claimApp.controller('admin',['$scope','$rootScope', '$location','fileUpload','claimFactory','propertyService',  function($scope,$rootScope,$location,fileUpload,claimFactory,propertyService){
       $scope.message = '';    
       $scope.errmessage ='';
       $scope.partnerUrl ='';
       $scope.partner = {selectedPartner:'',partnerName:'',selectedFileType:''};       
       //claimFactory.setLabelProperties(propertyService.getProperties().data);
       var claimRes = claimFactory.retrieveFunds();      
       claimRes.success(function(data, status, headers, config) {
    	   $scope.names = data;
    	   $scope.fileNames = 
    	       [ {fileId: 'css', fileName: 'css'},
    	         {fileId: 'img', fileName: 'img'},
    	         {fileId: 'disclaimer', fileName: 'disclaimer'}
    	       ];
    	   
       }),
       claimRes.error(function(data, status, headers, config) {	
    	   
       });	   

       $scope.change = function() {
    	   $scope.message = '';    
           $scope.errmessage ='';
           $scope.partnerUrl='';
       };
       //$scope.claimNo = claimNo
    //  alert('admin');
       $scope.go = function (form,partner) {  
    	   if (form.$valid) {    	   
    	   var file =$scope.myFile;
    	   var filename = $scope.myFile.name;
    	   var extn = filename.split(".").pop();
    	   var withOutextn = filename.slice(0, -4);
    	   if ("ZIP".toUpperCase() != extn.toUpperCase()) {
    		   form.$valid = false;
    		   form.$submitted = true;
    		   $scope.errmessage ='Please upload the file in zip format';
    		   $scope.message='';
    		   return;
    	   }
    	  
    	   var partnerName =partner.selectedPartner;
    	   var newPartner = false;
    	   var filepath = partner.selectedFileType;    	  
    	   if (partner.selectedPartner == 'NewPartner') {
    		   if (withOutextn != partner.partnerName){
    			   form.$valid = false;
        		   form.$submitted = true;
        		   $scope.errmessage ='Please make sure that folder name and fundname should be same';
        		   $scope.message='';        		   
        		   return;
    		   }
    		   newPartner = true;
    		   partnerName = partner.partnerName;
    	   } 
    	   var fileUploadRes = fileUpload.uploadFileToUrl(file,partnerName,newPartner,filepath);
    	   fileUploadRes.success(function(data, status, headers, config){
    		   if (null != data && data != '') {
    			  // alert(data.msg);
    			 //  if (data.msg != undefined) {
    			   if (data.success == true) {    			     
    			     if (newPartner == true) {
    			     var claimUpdateRes =claimFactory.updateFunds(partnerName);
    			     claimUpdateRes.success(function(data, status, headers, config) {
    			    	 $scope.message ="The "+partnerName+" fund has been uploaded successfully";
    	    			  $scope.errmessage=''; 
    			    	   
    			       }),
    			       claimUpdateRes.error(function(data, status, headers, config) {	
    			    	   
    			       });	
    			    } else {
    			    	$scope.message ="The "+filepath+" file has been uploaded successfully under fund "+partnerName;
  	    			    $scope.errmessage='';
    			    }
    			   } else {
    				  $scope.errmessage ="The file has not been uploaded succesfully"; 
    				  $scope.message = '';
    			   }
//    			  } else {
//    				  $scope.errmessage =data; 
//    				  $scope.message = ''; 
//    			  }
    		   }
    		   if (newPartner == true) {
    			   $scope.partnerUrl = "https://www.e2e.eapplication.metlife.com.au/eapply/claims#/ct/"+partnerName;
    		   } else {
    			   $scope.partnerUrl ='';
    		   }
    		   $(".loader-mask").hide();
           })
          fileUploadRes .error(function(data, status, headers, config){
        	 // alert('Service not available');
        	  $scope.errmessage ='Service not available,Please contact adminstrator';        	  
			  $scope.message = '';
        	  $(".loader-mask").hide();
           });
     	};
     	
       }
     	
   }]);   
   
  
   function process(date){
	   var parts = date.split("/");
	   return new Date(parts[2], parts[1] - 1, parts[0]);
	}
   
   claimApp.controller('upload',['$scope','$rootScope','$window','$http', '$location','fileUpload', 'propertyService','claimFactory','$timeout','$compile', 'SessionTimeoutService',  function($scope, $rootScope,$window, $http,$location,fileUpload, propertyService,claimFactory,$timeout,$compile, SessionTimeoutService){
      
	   SessionTimeoutService.removeListeners();
	     SessionTimeoutService.setup();
	    $scope.$on('$destroy', function() {    
	    		if(!$scope.$$phase){
	    			$scope.$apply();
	    		}
	    	});
	    
	   $scope.message = 'Look! I am an about page.';
       //alert(claimFactory.getClaimId());
       $rootScope.headerSection=true;
       $scope.urls = claimFactory.getLabelProperties();   
       claimFactory.manageHistory('',claimFactory.getLabelProperties().uploadStatus);
       //$scope.claimNo = claimNo
       $scope.go = function ( path ) {        	
     	  $location.path( path );
     	};
     	 // added for session expiry
     	/*var timeoutID;
		 
		function setup() {
			//angular.element($window).bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', resetTimer);
			this.addEventListener("mousemove", resetTimer, false);
		    this.addEventListener("click", resetTimer, false);
		    this.addEventListener("mouseup", resetTimer, false);
		    this.addEventListener("mousedown", resetTimer, false);
		    this.addEventListener("keydown", resetTimer, false);
		    this.addEventListener("keypress", resetTimer, false);
		    this.addEventListener("keyup", resetTimer, false);
		    this.addEventListener("submit", resetTimer, false);
		    this.addEventListener("change", resetTimer, false);
		    this.addEventListener("mouseenter", resetTimer, false);
		    this.addEventListener("scroll", resetTimer, false);
		    this.addEventListener("resize", resetTimer, false);
		    this.addEventListener("dblclick", resetTimer, false);
		    startTimer();
		}
		setup();
		 
		function startTimer() {
		    timeoutID = window.setTimeout(goInactive, 600000);
		}
		 
		function resetTimer(e) {
		    window.clearTimeout(timeoutID);
		    goActive();
		}
		 
		function goInactive() {				
			claimFactory.setDisableContcHdr(true);
			claimFactory.manageHistory('session is expired','logout');
			$location.path("/");
            $scope.$apply();
		}
		 
		function goActive() {   
		    startTimer();
		}*/

         
           $scope.scrollToTop = function(){
             	$window.scrollTo(0,0);
         	}
           var docList = new Array();           
           var ids = new Array();  
           angular.element(document).ready(function () {              
               var manualUploader = new qq.FineUploader({
			        element: document.getElementById('fine-uploader-manual-trigger'),
			        template: 'qq-template-manual-trigger',
			        request: {
			            endpoint: $scope.urls.urlUploadFile			            
			        },
			        display: {
			            fileSizeOnSubmit: true
			        },
			        messages: {
					    typeError: 'The document you are trying to upload does not meet the required file type or size requirements, please save it as a different file type or reduce the size and try to upload it again.',
					    //sizeError:'test',
					    onLeave:'Files are still uploading, are you sure you want to leave this page and cancel the upload?'
					},
			        thumbnails: {
			            placeholders: {
			                waitingPath: $location.protocol() + "://" + $location.host() + ":" + $location.port()+$scope.urls.urlWaitingPath,
			                notAvailablePath: $location.protocol() + "://" + $location.host() + ":" + $location.port()+$scope.urls.urlNotAvailablePath
			            }
			        },
			        params: { 
			        	dropDownVal:  'poorna'
			        },
			        session : {
			            endpoint : $scope.urls.urlEndpoint2+claimFactory.getClaimId(),
			            refreshOnRequest:true,
//			            params :{
//			            	claimNo:  claimFactory.getClaimId()
//			            },
			            thumbnails: {
				            placeholders: {
				            	waitingPath: $location.protocol() + "://" + $location.host() + ":" + $location.port()+$scope.urls.urlWaitingPath,
				                notAvailablePath: $location.protocol() + "://" + $location.host() + ":" + $location.port()+$scope.urls.urlNotAvailablePath
				            }
				        },
			          },
			          resume: {
			              enabled: true
			          },
			        autoUpload: false,
			        debug: true,

					validation: {
						allowedExtensions: ['pdf','txt','doc','docx','xls','xlsx','tif','jpeg', 'jpg', 'png'],
						stopOnFirstInvalidFile: true,
						messages: {
							sizeError:'test1'
						},
						
						// itemLimit: 3,
						 sizeLimit: 26214400 // 50 kB = 50 * 1024 bytes 
					},
					
					
					callbacks: {
						_formatSize: function(bytes) {							
				            var i = -1;
				            do {
				                bytes = bytes / 1024;
				                i++;
				            } while (bytes > 1023);				           
				           // this._options.messages.sizeError="{file} is too large, maximum file size is {sizeLimit}.";
				            //alert('bytes :'+bytes);
				            return Math.max(bytes, 0.1).toFixed(2) + this._options.text.sizeSymbols[i];
				        },
					
			            onSubmit: function(id, fileName) {
			            	//alert($('.qq-file-id-'+id).find("span.qq-upload-size-selector.qq-upload-size").val());			            	
			            	currentId = id;
			            	var name = fileName.substr(0, fileName.lastIndexOf('.')) || fileName;			            	
			            	ids.push(currentId);			            	
			            },
			            onSubmitted: function(id, fileName) {
			            	//alert('onSubmitted');			            	
			            	var idval = '.qq-file-id-'+id;
		            		$(idval).find("span.qq-upload-size-selector.qq-upload-size").html(this._options.callbacks._formatSize(this.getSize(id)));			            	
			            },
			            onCancel: function(id, fileName) {
			            	
			            },
			            onUpload: function(id, fileName) {
			          
			            	//var  qqEle = '.qq-file-id-'+id+' select :selected';
			            	//console.log($('.qq-file-id-'+id).children('span.qq-document-type').children().val());
			            	if($('.qq-file-id-'+id).children('span.qq-document-type').children().val() != '0'){
								var  qqEle = '.qq-file-id-'+id+' select :selected';
								this.setParams({description:$(qqEle).text(),claimStatus:claimFactory.getClaimStatus()});
								//console.log('qqEle1 :'+qqEle);
							} else{
							
							}			            	
			            },
			            onProgress: function(id, fileName, loaded, total) {
			            	var idval = '.qq-file-id-'+id;
			            	if (loaded < total) {
			            	    progress = '"' + fileName + '" uploading...  ' + Math.round(loaded /
			            	    		total * 100) + '%';
			            	    $(idval).find("span.qq-upload-size-selector.qq-upload-size").html(progress);
			            	  }
			            	  else {
			            		  $(idval).find("span.qq-upload-size-selector.qq-upload-size").html(this._options.callbacks._formatSize(this.getSize(id)));	
			            	  }


			                //alert('onProgress'+loaded);
			               // var idval = '.qq-file-id-'+id;
		            		//$(idval).find("span.qq-upload-size-selector.qq-upload-size").html(this._options.callbacks._formatSize(this.getSize(id)));	

			            },
			            onComplete: function(id, fileName, responseJSON) {
			            //	alert('onComplete');
			            	 if(responseJSON.success){
			            		 
			            		 $('select').prop('disabled',true);
			            		// $('li.qq-file-id-'+id).append("<span style='color:green' class='qq-upload-status-text'>Successfully Uploaded</span>");
			            	 } 
			            	 if (responseJSON.length != 0){
			            		//var document = {"documentlocation":"", "documentCode":"", "documentStatus":""};
			            		var loc = JSON.parse(responseJSON.doucment).documentlocation;
			            		var code = JSON.parse(responseJSON.doucment).documentCode;
			            		var status = JSON.parse(responseJSON.doucment).documentStatus;
			            		var fileName = JSON.parse(responseJSON.doucment).fileName;
			            		var fileSize = JSON.parse(responseJSON.doucment).fileSize;
			            		var uuId = JSON.parse(responseJSON.doucment).uuId;
			            		var policyNum = claimFactory.getClaimId();
			            		var doc = new Document(loc,code,status,policyNum,id,fileName,fileSize,uuId,claimFactory.getPartnerName());
			            		var idval = '.qq-file-id-'+id;
			            		$(idval).find("span.qq-upload-size-selector.qq-upload-size").html(this._options.callbacks._formatSize(fileSize));
			            		docList.push(doc);
			            		
			            		//console.log(responseJSON.doucment);
			            		//console.log(JSON.parse(responseJSON.doucment).documentlocation);			            														 
			            	}
			            },
			            onAllComplete: function(id, fileName, responseJSON) {
			            	//console.log("onAllComplete"+id.length+' fileName '+fileName+' responseJSON '+responseJSON);
			            	var claim = claimFactory.getClaimData();           
			                var claimData = new ClaimData(claim.uniqueId,claim.claimId,claim.surName,claim.dob,claim.status,claim.statusDate,claim.firstName,claim.fundId,claim.fundName,claim.claimAssesor,claim.productType,docList);
			                var response = $http.post($scope.urls.urlSubmitFile,claimData);			            	
							response.success(function(data, status, headers, config) {
								
							});
							response.error(function(data, status, headers, config) {
								
							});
							docList = new Array();
							ids = new Array();
			               
			            },onValidate: function(data,buttonContainer){
			            	//this._options.validation.sizeLimit='26214400';			            	
			            	//data.size='34567';
			            	//alert(data.size);
			            	this._options.messages.sizeError = data.name+" is too large, maximum file size is "+this._options.callbacks._formatSize(this._options.validation.sizeLimit)+"."
			            	//return this._options.callbacks._formatSize(this._options.validation.sizeLimit);		           
			            	
			            },
			            onSessionRequestComplete: function(responseJSON,status) {
			            	//console.log(JSON.stringify(responseJSON));	
			            	if (status === true ){
				            	 for (var i = 0;i < responseJSON.length; i++) {
				            		 var uuid = this.getUuid(i);
				            		 for (var j = 0;j < responseJSON.length; j++) {
				            			 if (uuid == responseJSON[j].uuid) {
				            				 //console.log('uuid :'+uuid);
							            		var  qqEle = ".qq-file-id-"+j+" select option[value='"+responseJSON[j].description+"']";
							            		var  qqSel = ".qq-file-id-"+j+" select";
							            		//console.log('qqEle :'+qqEle);
							            		var idval = '.qq-file-id-'+responseJSON[j].id;
							            		$(idval).find("span.qq-upload-size-selector.qq-upload-size").html(this._options.callbacks._formatSize(responseJSON[j].size));							            		
								            	//$(qqEle).val("1");
								            	$(qqEle).attr('selected','selected');
								            	$(qqSel).prop('disabled',true);
				            			 }
				            		 }
				            		 
				                }
			            	}
			            	
			            },
			            onSuccess: function(id, fileName, responseJSON) {
			            //	 alert('onSuccess'); 
			            }
			        }

				});

               qq(document.getElementById("trigger-upload")).attach("click", function() {
            	   var isValid = false;
            	 // alert('Hi');            	   
            	   
            	   for (var i=0;i<ids.length;i++) { 
            		   var currentId = ids[i]; 
            		  // alert('currentId :'+currentId);
            		   $('.qq-file-id-'+currentId).find("span.qq-upload-status-text").remove(); 
            		  // alert($('.qq-file-id-'+currentId).children('span.qq-document-type').children().val());
            	if($('.qq-file-id-'+currentId).children('span.qq-document-type').children() != 'undefined'){
					if($('.qq-file-id-'+currentId).children('span.qq-document-type').children().val() != '0'){
						var  qqEle = '.qq-file-id-'+currentId+' select :selected';
						//manualUploader.setParams({description:$(qqEle).text()});
						//manualUploader.uploadStoredFiles();
						isValid = true;
						//manualUploader.setParams({description:$(qqEle).text()});
						//alert('Hi1'+currentId);
						} else{		
						//	alert('Hi2'+currentId);
							alert("Please select the document type for the file "+"'"+manualUploader.getName(currentId)+"'");							
							//if ($('.qq-file-id-'+currentId).children('span.qq-upload-status-text') == 'undefined'){
							 $('.qq-file-id-'+currentId).append("<span style='color:red' class='qq-upload-status-text'>Please select the document type</span>");
							//}
							isValid = false;
							break;
							//manualUploader._storedIds.pop(currentId);
							//console.log('qqEle2 :'+qqEle);
		            		//$('li.qq-file-id-'+currentId).append("<span style='color:red' class='qq-upload-status-text'>Please select the document type</span>");
		            		//throw new Error("Please select the document type");
						}
            	   }           	
            	
               }
            	   if (isValid) {               		
   					manualUploader.uploadStoredFiles();
               	} else {
               		//alert("Please select the document(s) type "+this.getFile(currentId));	
               	}
				});               
           });
   }]);
    
   function removeErrMsg(obj){
	   $(obj).parent().parent().children('span.qq-upload-status-text').html('')
	   //alert('test'+$(obj).parent().parent().children('span.qq-upload-status-text').html(''));
   }
    function Document(loc,code,status,policyNum,fileId,fileName,fileSize,uuId,partnerName) {
	   this.documentlocation = loc;
	   this.documentCode = code;
	   this.documentStatus = status;
	   this.policyNum =  policyNum;	
	   this.fileId =  fileId;	
	   this.fileName =  fileName;	
	   this.fileSize =  fileSize;	
	   this.uuId =  uuId;	
	   this.partnerName= partnerName;
		
	 }
    function ClaimData(uniqueId,claimId,surName,dob,status,statusDate,firstName,fundId,fundName,claimAssesor,productType,documentList) {
    
    	this.uniqueId = uniqueId;
    	this. claimId = claimId;
    	this. surName = surName;
    	this. dob = dob;
    	this. status = status;
    	this. statusDate = statusDate;
    	this. firstName = firstName;
    	this. fundId = fundId;
    	this. fundName = fundName;
    	this. claimAssesor = claimAssesor;
    	this. productType = productType;	
    	this. documentList = documentList;
 		
 	 }   
   