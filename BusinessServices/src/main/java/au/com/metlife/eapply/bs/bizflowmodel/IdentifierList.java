package au.com.metlife.eapply.bs.bizflowmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentifierList")
@XmlRootElement
public class IdentifierList {
	
	private List<IdentifierValue>  IdentifierValue ;

	public List<IdentifierValue> getIdentifierValue() {
		return IdentifierValue;
	}

	public void setIdentifierValue(List<IdentifierValue> identifierValue) {
		IdentifierValue = identifierValue;
	}



	
	

	
	
}
