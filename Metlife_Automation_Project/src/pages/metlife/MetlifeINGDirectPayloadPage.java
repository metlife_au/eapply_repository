package pages.metlife;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.MetlifeINGDirPayloadPageObjects;

public class MetlifeINGDirectPayloadPage extends MasterPage{

	public MetlifeINGDirectPayloadPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	public void enterINGPayloadAndClickNewApplication(){
		try{
			String inputString=dataTable.getData("General_Data", "InputString");
			String work15hrs=dataTable.getData("MetLife_Identity", "Work 15 hours and more");
			String industry=dataTable.getData("MetLife_Identity", "Industry");
			String currentOccupation=dataTable.getData("MetLife_Identity", "current occupation");
			String annualSal=dataTable.getData("MetLife_Identity", "AnnualSalary");
			String deathSelCover=dataTable.getData("MetLife_Identity", "DeathSelectCover");
			String ipSelCover=dataTable.getData("MetLife_Identity", "incomeprotectionselectcover");
			
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.txtInputstring), "Set", "Enter Input String",inputString);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.btnProceednextpage), "click", "Proceed to next page");
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.btnNewapplication), "click", "New Application");
			waitForMs(2000);
			for (String winHandle : driver.getWindowHandles())
			{
				driver.switchTo().window(winHandle);
			}
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkChangeyourcover), "click", "Change Insurance - Cover Details");
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkChangeyourcover), "click", "Change Insurance - Cover Details");
			tryAction(fluentWaitElement(work15hrs.equalsIgnoreCase("Yes")?MetlifeINGDirPayloadPageObjects.rdWork15hrsmoreYes:MetlifeINGDirPayloadPageObjects.rdWork15hrsmoreNo), "click", "Do you work 15 hours or more per week? -"+work15hrs);
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.selIndustry), "DropDownSelect", "What industry do you work in?",industry);
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.selCurroccupation), "DropDownSelect", "What is your current occupation?",currentOccupation);
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.txtCurrannualsal), "Set", "What is your gross annual salary (excl. super)?",annualSal);
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.selDeathselectcovertype), "DropDownSelect", "Death Select Cover",deathSelCover);
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.selIncomeprotectionselectcovertype), "DropDownSelect", "Income Protection Select Cover",ipSelCover);
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkCalculatequote), "click", "Calculate Quote");
			waitForMs(2000);
			tryAction(fluentWaitElement(MetlifeINGDirPayloadPageObjects.lnkContinue), "click", "Continue");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
