package businesscomponents;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;

import pages.metlife.Angular.Statewide.Statewide_Angular_Login;
import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Components class
 * @author Cognizant
 */
public class FunctionalComponents extends ReusableLibrary
{
	/**
	 * Constructor to initialize the component library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public FunctionalComponents(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void invokeApplication()
	{
		String strApplicationURL = dataTable.getData("MetLife_Data", "_url");
		//driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(strApplicationURL);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
				strApplicationURL, Status.DONE);
	}
	
	public void invokeSTATEapplication()
	{
		String strApplicationURL = dataTable.getData("STATEWIDE_Login","URL");
		//driver.get(properties.getProperty("ApplicationUrl"));
			
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		sleep(500);
		properties = Settings.getInstance();
		String StrBrowseName=properties.getProperty("currentBrowser");
		
	  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
	  			System.out.println(StrBrowseName);
	  			driver.get(strApplicationURL);
	  			Dimension d = new Dimension(1410,1150);
	  			//Resize the current window to the given dimension
	  			driver.manage().window().setSize(d);	
	  		}
	  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
	  			System.out.println(StrBrowseName);
	  			driver.get(strApplicationURL);
	  		}
	  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
	  			System.out.println(StrBrowseName);
	  			driver.get(strApplicationURL);
	  		}
		//report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
				//strApplicationURL, Status.DONE);
		report.updateTestLog("Invoke Application", "Invoked the application under test", Status.DONE);
	}
	
	public void login_STATEWIDE() {
		Statewide_Angular_Login Login = new Statewide_Angular_Login(scriptHelper);
		Login.STATEWIDE_Login();
	}



}