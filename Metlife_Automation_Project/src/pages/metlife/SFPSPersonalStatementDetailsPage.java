/**
 * 
 */
package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ScriptHelper;
import uimap.metlife.PersonalStatementPageObjects;

import com.cognizant.framework.Status;

/**
 * @author sampath
 * 
 */
public class SFPSPersonalStatementDetailsPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public SFPSPersonalStatementDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public SFPSPersonalStatementDetailsPage enterSFPSPersonalStatementDetails() {
		fillInSFPSPersonalStatementDetails();
		return new SFPSPersonalStatementDetailsPage(scriptHelper);
	} 
	
	private void fillInInsuranceDetails() {
		
		String strInsuTPD = dataTable.getData("MetLife_Data", "personal.insuTPD");
		String strReceivedClaim = dataTable.getData("MetLife_Data", "personal.receivedClaim");
		
		
		try{
			List<WebElement> radInsuranceDetailsNo = null;
			radInsuranceDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsNo);
			List<WebElement> radInsuranceDetailsYes = null;
			radInsuranceDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radListInsuranceDetailsYes);
			tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(0)):fluentWaitElement(radInsuranceDetailsNo.get(0)),"Click","Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined? - "+strInsuTPD);
			tryAction(strReceivedClaim.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(1)):fluentWaitElement(radInsuranceDetailsNo.get(1)),"Click","Are you contemplating or have you ever made a claim for or received sickness? - "+strReceivedClaim);
			if(radInsuranceDetailsYes.size()==3)
				tryAction(strInsuTPD.equalsIgnoreCase("yes")?fluentWaitElement(radInsuranceDetailsYes.get(2)):fluentWaitElement(radInsuranceDetailsNo.get(2)),"Click","Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund? - " +strInsuTPD);
				
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	private void fillInLifeStyleDetails() {
		
		
		String strAlcoholConsuQuantity = dataTable.getData("MetLife_Data", "personal.alcoholConsumptionQuantity");
		String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
		String strAlcoholConsu = dataTable.getData("MetLife_Data", "personal.alcohlConsumption");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strAbtHIV = dataTable.getData("MetLife_Data", "personal.HIV");
		
		
		try{
			
			List<WebElement> radLifeStyleDetailsNo = null;
			radLifeStyleDetailsNo = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsNo);
			List<WebElement> radLifeStyleDetailsYes = null;
			radLifeStyleDetailsYes = fluentWaitListElements(PersonalStatementPageObjects.radLifeStyleDeatilsYes);
			
			tryAction(strTravelPlan.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(0)):fluentWaitElement(radLifeStyleDetailsNo.get(0)),"Click","Do you have firm plans to travel or reside in another country other than NZ?");
			tryAction(strDrugs.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(1)):fluentWaitElement(radLifeStyleDetailsNo.get(1)),"Click","Have you within the last 5 years used any drugs that were not prescribed to you? - "+strDrugs);
			tryAction(strAlcoholConsu.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(2)):fluentWaitElement(radLifeStyleDetailsNo.get(2)),"Click","Have you ever been advised by a health professional to reduce your alcohol consumption? - "+strAlcoholConsu);
			tryAction(strAbtHIV.equalsIgnoreCase("yes")?fluentWaitElement(radLifeStyleDetailsYes.get(3)):fluentWaitElement(radLifeStyleDetailsNo.get(3)),"Click","Do you have HIV (Human Immunodeficiency Virus) that causes AIDS? - "+strAbtHIV);
			if(strAbtHIV.equalsIgnoreCase("no")){
				String strHIV = "#table_Lifestyle_Questions input[name='"+radLifeStyleDetailsYes.get(3).getAttribute("name")+"_1_0'][value='No']";
				//Click On No for HIV
				tryAction(fluentWaitElement(By.cssSelector(strHIV)),"Click","Are you in a high risk category for contracting HIV (Human Immunodeficiency Virus) that causes AIDS?");
			}
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkSportsNota),"Click","Do you regularly engage in or intend to engage in any of the following hazardous activities?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtAlcoholicDrinksConsumption),"SET","On average, how many standard alcoholic drinks do you consume each week?",strAlcoholConsuQuantity);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnAlcoholicEnter),"CLICK","Enter");
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}

	private void fillInSFPSPersonalStatementDetails() {
		
		String strHeight = dataTable.getData("MetLife_Data", "personal.height");
		String strHeightUnits = dataTable.getData("MetLife_Data", "personal.heightunits");
		String strWeight = dataTable.getData("MetLife_Data", "personal.weight");
		String strWeightUnits = dataTable.getData("MetLife_Data", "personal.weightunits");
		String strLungDese = dataTable.getData("MetLife_Data", "personal.lung");
		String strLungAsthma = dataTable.getData("MetLife_Data", "personal.asthma");
		String strLungCondition = dataTable.getData("MetLife_Data", "personal.condition");
		String strPregnent = dataTable.getData("MetLife_Data", "personal.prebnent");
		String strRegulatVisit = dataTable.getData("MetLife_Data", "personal.regVisit");
		String strFamHist = dataTable.getData("MetLife_Data", "personal.famHist");
		String strTravelPlan = dataTable.getData("MetLife_Data", "personal.travelPlans");
		String strDrugs = dataTable.getData("MetLife_Data", "personal.drugs");
		String strAlcohol = dataTable.getData("MetLife_Data", "personal.alcohol");
		String strDisclosed = dataTable.getData("MetLife_Data", "personal.disclosed");
		
		
		try{
			tryAction(waitForClickableElement(PersonalStatementPageObjects.txtHeight),"SET","Height",strHeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstHeightUnits),"DropDownSelect","Height Units",strHeightUnits);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.txtWeight),"SET","Weight",strWeight);
			tryAction(fluentWaitElements(PersonalStatementPageObjects.lstWeightUnits),"DropDownSelect","Weight Units",strWeightUnits);
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkLung),"chkCheck","Lung or breathing conditions");
			tryAction(fluentWaitElements(PersonalStatementPageObjects.chkAsthma),"chkCheck","Asthma");
			if(strLungCondition.equalsIgnoreCase("mild")){
				tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition - "+strLungCondition);
			}else if(strLungCondition.equalsIgnoreCase("Moderate")){
				tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaModerate),"Click","Mild Asthma Condition - "+strLungCondition);
			}else if(strLungCondition.equalsIgnoreCase("Severe")){
				tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaSevere),"Click","Mild Asthma Condition - "+strLungCondition);
			}else{
				tryAction(fluentWaitElements(PersonalStatementPageObjects.radAsthmaMild),"Click","Mild Asthma Condition - "+strLungCondition);
			}
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radAsthmaWorseNo),"Click","Is your asthma worsened by your occupation?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBPCholNota),"Click","In the last 5 years have you suffered from");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radPregnentNo),"Click","Are you currently pregnant?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radVisitDocNo),"Click","Do you have a usual doctor or medical centre you regularly visit?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.chkBrainBoneNota),"Click","Have you ever suffered from, been diagnosed with or sought medical advice or treatment for?");
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radFamHistNo),"Click","Family History");
			
			fillInLifeStyleDetails();


			//General Details
			tryAction(waitForClickableElement(PersonalStatementPageObjects.radSufferFromAnyConditionNo),"CLICK","do you presently suffer from any condition?");
			fillInInsuranceDetails();
			sleep();
			tryAction(waitForClickableElement(PersonalStatementPageObjects.btnContinuePersonalStatement),"Click","Calculate Quote");
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		

	}
}
