package businesscomponents;

import com.cognizant.framework.Status;

import pages.metlife.MetlifeINGDirectPayloadPage;
import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

/**
 * Functional Library class
 * 
 * @author Cognizant
 */
public class MetLifeINGDirFunctionalComponents extends ReusableLibrary {
	/**
	 * Constructor to initialize the functional library
	 * 
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object passed from the
	 *            {@link DriverScript}
	 */
	public MetLifeINGDirFunctionalComponents(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	public void invokeMINGDirectApplication() {
		driver.get(properties.getProperty("ApplicationUrl.INGDirect"));
		System.out.println("In invokeMINGDirectApplication for ING Direct");
		report.updateTestLog(
				"Invoke Application",
				"Invoke the application under test @ "
						+ properties.getProperty("ApplicationUrl.INGDirect"), Status.PASS);
	}
	
	public void enterINGPayloadAndClickNewApplication(){
		MetlifeINGDirectPayloadPage metlifeINGDirectPayloadPage = new MetlifeINGDirectPayloadPage(
				scriptHelper);
		metlifeINGDirectPayloadPage.enterINGPayloadAndClickNewApplication();
	}
	

}