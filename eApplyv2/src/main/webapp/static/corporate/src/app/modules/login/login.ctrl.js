(function (angular) {
	'use strict';
	/* global channel, inputData, token, moment, auraId */
	/**
	 * @ngdoc controller
	 * @name CorporateApp.controller:loginCtrl
	 *
	 * @description
	 * This is loginCtrl description
	 */

	angular.module('CorporateApp').controller('loginCtrl', ['$scope', '$location', 'inputDataService', 'APP_CONSTANTS', 'fetchUrlSvc', 'fetchIndustryList', 'fetchTokenNumSvc', 'fetchAppNumberSvc', 'PersistenceService', 'CheckSavedAppService', 'RetrieveAppDetailsService', 'CancelSavedAppService', 'ngDialog', 'appData', 'CheckUWDuplicateAppService', 'extendAppModel','fetchCategoryDetailsService', '$q', 'corporateFund',
		function ($scope, $location, inputDataService, APP_CONSTANTS, fetchUrlSvc, fetchIndustryList, fetchTokenNumSvc, fetchAppNumberSvc, PersistenceService, CheckSavedAppService, RetrieveAppDetailsService, CancelSavedAppService, ngDialog, appData, CheckUWDuplicateAppService, extendAppModel,fetchCategoryDetailsService, $q, corporateFund) {
			var vm = this;
			vm.channel = channel || '';
			vm.token = token || '';
			vm.inputData = {};
			vm.inputData.membershipCategory = '';
			vm.inputData = inputData || '{}';
			vm.appNum = null;
			vm.urlList = fetchUrlSvc.getUrlList();
			inputDataService.setCoverDetails(vm.inputData);
			vm.fName = vm.inputData.firstName;
			vm.lName = null;
			if(vm.channel === 'email'|| vm.channel ==='eviewsave'){
				vm.dob = null;
			}
//			fetchAppNumberSvc.setAppNumber(vm.appNum);
			fetchTokenNumSvc.setTokenId(vm.token);
			vm.constants = {
				phoneNumber: APP_CONSTANTS.onboardDetails.phoneNumber,
				emailFormat: APP_CONSTANTS.onboardDetails.emailFormat,
				dateFormat: APP_CONSTANTS.onboardDetails.dateFormat
			};
			vm.fundCat=[];
			vm.modelOptions = {updateOn: 'blur'};
			/**
			 * @ngdoc method
			 * @name loginUser
			 * @methodOf CorporateApp.controller:loginCtrl
			 * @description
			 * This method authenticates the users and allow them to access the application upon successful login
			 *
			 * @param {Object} temp temp object to be updated
		
			*/
			vm.urlList = fetchUrlSvc.getUrlList();
			fetchIndustryList.getObj(vm.urlList.quoteUrl);
			vm.init = function() {
				var defer = $q.defer();
				var fundId = vm.inputData.AFUNDID ? vm.inputData.AFUNDID : vm.inputData.fundId;
				corporateFund.getDetails(vm.urlList.corporatePost, '', fundId).then(function(res) {
					appData.setFundCategoryDetails(res.data);
					if(!res.data.partner.broker) {
						defer.reject({});
					}
					vm.brokerCnctNum = res.data.partner.broker.brokerCnctNum;
					vm.brokerName = res.data.partner.broker.brokerName;
					vm.fundCat=res.data.partner.fundCat;
					console.log('vm.fundCat>>'+JSON.stringify(vm.fundCat));
					defer.resolve(res);
				}, function(err) {
					defer.resolve({});
				});
				return defer.promise;
			};

			vm.init().then(function() {
				vm.servicefailureFlag = false;
			}, function() {
				vm.servicefailureFlag = true;
			});

			vm.checkDOB = function() {
				if(moment(vm.dob, 'DD/MM/YYYY').isValid() && !moment(vm.dob, 'DD/MM/YYYY').isAfter()) {
					vm.eviewForm.dob.$setValidity('valid', true);
				} else {
					vm.eviewForm.dob.$setValidity('valid', false);
				}		
			};

			vm.showSavedAppPopup = function(manageType) {
				vm.servicefailureFlag=false;
				if(vm.channel === 'eview') {
					vm.inputData.fundId = vm.inputData.AFUNDID;
					fetchCategoryDetailsService.getCategoryDetails(vm.urlList.fetchCategoryDetailUrl, vm.inputData.fundId, vm.inputData.membershipCategory).then(function(res) {
						appData.setFundCategoryDetails(res.data);
						vm.brokerCnctNum = res.data.partner.broker.brokerCnctNum;
						vm.brokerName = res.data.partner.broker.brokerName;
						vm.proceedtoCover(manageType);
					}, function(err) {
						vm.servicefailureFlag = true;
					});
				} else {
					fetchCategoryDetailsService.getCategoryDetails(vm.urlList.fetchCategoryDetailUrl, vm.inputData.fundId, vm.inputData.membershipCategory).then(function(res) {
						appData.setFundCategoryDetails(res.data);
						vm.brokerCnctNum = res.data.partner.broker.brokerCnctNum;
						vm.brokerName = res.data.partner.broker.brokerName;
						vm.proceedtoCover(manageType);
					}, function(err) {
						vm.servicefailureFlag = true;
					});
				}
			};
			
			vm.proceedtoCover = function(manageType) {	
				var emp;
	
				if (vm.channel === 'eview') {
					vm.appNum = parseInt(auraId) || 0;
					inputDataService.setCoverDetails(vm.inputData);
					emp=vm.appNum;
					vm.fName = vm.inputData.firstName;
//					vm.lName = vm.inputData.surname;
					vm.inputData.dob =moment(vm.dob, 'DD/MM/YYYY');
					inputDataService.setCoverDetails(vm.inputData);
					fetchAppNumberSvc.setAppNumber(vm.appNum);
				}
				if(vm.channel === 'email'|| vm.channel === 'eviewsave' ){
					emp= vm.inputData.employeeNumber;
					fetchAppNumberSvc.setAppNumber(vm.appNum);
				}
				CheckSavedAppService.checkSavedApps(vm.urlList.savedAppUrl, 'CORP', emp, manageType).then(function(res) {
					vm.apps = res.data;
					if (vm.apps.length > 0) {
						var newScope = $scope.$new();
						for (var i = 0; i < vm.apps.length; i++) {
							vm.tempDate = new Date(vm.apps[i].createdDate);
							vm.apps[i].createdDate = moment(vm.tempDate).format('DD/MM/YYYY');
							if (vm.apps[i].requestType === 'CCOVER') {
								vm.apps[i].requestType = 'Change Cover';
							}
						}
						// Created a new scope for the modal popup as the parent controller variables are not accessible to the modal
						newScope.apps = vm.apps;
						var savePopupTemplate = '<div class="ngdialog-content">';
						savePopupTemplate +=			'<div class="modal-header">';
						savePopupTemplate += 				'<h4 id="myModalLabel" class="modal-title">You have previously saved application(s).</h4>';
						savePopupTemplate +=			'</div>';
						savePopupTemplate += 			'<div class="modal-body">';
						savePopupTemplate += 				'<!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-12"><p> Please continue with the saved application or cancel saved application to start a new application. </p> </div> </div><!-- Row ends -->';
						savePopupTemplate += '<div class="row d-block d-sm-none"><div class="col-sm-12">';
						savePopupTemplate += '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th class="coverbg"><h5 class=" ">Application no</h5></th>';
						savePopupTemplate += '<th class="coverbg     "><h5 class=" ">Date saved</h5></th>';
						savePopupTemplate += '<th class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr>';
						savePopupTemplate += '<tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div></div>';
						savePopupTemplate += '<div class="row d-none d-sm-block"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody>';
						savePopupTemplate += '<tr class=""><th width="20%" class="coverbg"><h5 class=" ">Application no</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Date saved</h5></th>';
						savePopupTemplate += '<th width="20%" class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div> </div></div>';
						savePopupTemplate += '</br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons text-center">';
						savePopupTemplate += '<button type="button" class="btn metlife-btn-primary mr-1 avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
						savePopupTemplate += '<button type="button" class="btn metlife-btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button></div></div></div>';
						ngDialog.openConfirm({
							template: savePopupTemplate,
							plain: true,
							className: 'ngdialog-theme-plain custom-width',
							scope: newScope
						}).then(function (value) {
							fetchAppNumberSvc.setAppNumber(vm.apps[0].applicationNumber);
							PersistenceService.setAppNumToBeRetrieved(vm.apps[0].applicationNumber);
							PersistenceService.setAppNumber(vm.apps[0].applicationNumber);
							RetrieveAppDetailsService.retrieveAppDetails(vm.urlList.retrieveAppUrl, vm.apps[0].applicationNumber).then(function(res) {
								appData.setAppData(extendAppModel.extendObj(res.data[0]));
								switch (vm.apps[0].lastSavedOnPage.toLowerCase()) {
									case 'quotepage':
										$location.path('/cover');
										break;
									case 'aurapage':
										$location.path('/aura');
										break;
									case 'summarypage':
										$location.path('/summary');
										break;
									default:
										break;
								}
							});
						}, function (value) {
							if (value === 'oncancel') {
								CancelSavedAppService.cancelSavedApps(vm.urlList.cancelAppUrl, vm.apps[0].applicationNumber).then(function(result) {
									appData.setAppData(extendAppModel.extendObj());
									switch (vm.apps[0].lastSavedOnPage.toLowerCase()) {
										case 'quotepage':
										case 'aurapage':
										case 'summarypage':
											$location.path('/cover', true);
											break;
										default:
											break;
									}
								}, function (err) {
									console.log('Error while cancelling the saved app ' + err);
								});
							}
						});
					} else {
						appData.setAppData(extendAppModel.extendObj());
						switch (manageType.toUpperCase()) {
							case 'CCOVER':
								vm.checkDupApplication('CCOVER');
								break;
							default:
								break;
						}
					}
				}, function (err) {
					console.log('Error while fetching saved apps ' + err);
				});		
			};
			/* Change for Duplicate Application UW message*/
			vm.checkDupApplication = function (manageTypeCC) {
				var emp;
				if (vm.channel === 'eview') {
					emp=vm.appNum;
				}
				else{
					emp=vm.inputData.employeeNumber;
				}
				var fundId = vm.inputData.AFUNDID ? vm.inputData.AFUNDID : vm.inputData.fundId;
				CheckUWDuplicateAppService.checkDuplicateApps(vm.urlList.dupAppUrl, fundId,emp, manageTypeCC, vm.dob, vm.fName, vm.lName).then(function (res) {
					vm.check = res.data;
					if (vm.check) {
						var dupAppPopUp = '<div class="ngdialog-contentdup"><div class="modal-header">';
						dupAppPopUp += '<h6 id="myModalLabel" class="modal-title">As you have already submitted your application, this link is no longer available.Please contact your broker if you wish to apply for insurance. </h6></div><div class="ngdialog-footer mt0"><div class="ngdialog-buttons text-center"> ';
						dupAppPopUp += '<br><button type="button" class="btn metlife-btn-primary mr-1 avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Ok</button>';
						ngDialog.openConfirm({
							template: dupAppPopUp,
							plain: true,
							className: 'ngdialog-theme-plain custom-width'
						}).then(function () {
							$location.path('/cover', true);
						}, function (e) {
							if (e === 'oncancel') {
								return false;
							}
						});
					} else {
						$location.path('/cover', true);
					}
				}, function (err) {
					console.log('Error ' + err);
				});
			};
		}]);
})(angular);
