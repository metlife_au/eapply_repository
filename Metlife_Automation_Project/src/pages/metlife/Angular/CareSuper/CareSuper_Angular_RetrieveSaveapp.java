package pages.metlife.Angular.CareSuper;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class CareSuper_Angular_RetrieveSaveapp extends MasterPage {
	WebDriverUtil driverUtil = null;

	public CareSuper_Angular_RetrieveSaveapp RetreiveApp_CareSuper() {
		RetreiveApp();		
		
		
		return new CareSuper_Angular_RetrieveSaveapp(scriptHelper);
	}

	public CareSuper_Angular_RetrieveSaveapp(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void RetreiveApp() {

		
		
		try{			
    

			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[text()='Retrieve or track previous applications ']"))).click();
			sleep(250);
			report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
			sleep(3500);
			
			for(int i=1;i<400;i++){
				sleep(250);
				String Status=driver.findElement(By.xpath("((//table)[2]/tbody/tr[@class='ng-scope']/td[5])["+i+"]")).getText();
				sleep(500);
				
				if(Status.equalsIgnoreCase("Pending")){
					
					driver.findElement(By.xpath("((//table)[2]/tbody/tr[@class='ng-scope']/td[1]/a)["+1+"]")).click();
					sleep(500);
					
					break;
				}
			}
			sleep(6000);
		if(driver.getPageSource().contains("Insured's duty of disclosure")){
			report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		}
		else{
			report.updateTestLog("Retrieve Saved Application", "No Applications to Retrieve", Status.PASS);
		}
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}			
	
	}

/*

try{
	
         //****Click on Save and Retrieve Link*****\\
	sleep(2500);
	WebDriverWait wait=new WebDriverWait(driver,18);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[text()='Retrieve or track previous applications ']"))).click();		
	report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
	sleep(1000);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()=' Your saved applications ']")));
	int totalapp=driver.findElements(By.xpath("(//table)[2]/tbody/tr[@class='ng-scope']")).size();
	sleep(500);
	if(totalapp<1){
	
	report.updateTestLog("Retrieve Saved Application", "Applications to Retrieve", Status.PASS);
		
	}
	for(int i=1;i<=totalapp;i++){
		
		String Status=driver.findElement(By.xpath("((//table)[2]/tbody/tr[@class='ng-scope']/td[5])["+i+"]")).getText();
		sleep(250);
		
		if(Status.equalsIgnoreCase("Pending")){
		
			driver.findElement(By.xpath("((//table)[2]/tbody/tr[@class='ng-scope']/td[1]/a)["+1+"]")).click();
			sleep(500);
			
			
			break;
		}
	}
	
	sleep(1000);
if(driver.getPageSource().contains("Duty of disclosure")){
	
	report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
}

	
	}catch(Exception e){
		report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
	}
}




}*/


