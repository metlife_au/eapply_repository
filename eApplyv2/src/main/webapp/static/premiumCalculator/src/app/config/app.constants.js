const AppConstants = {
  // api: 'https://conduit.productionready.io/api',
  //api: 'http://localhost:8087/',
  appName: 'Premium Calculator',
  urlFilePath: 'CareSuperUrls.properties',
  logoPath: './static/premiumCalculator/src/assets/'+fundID+'/images/logo.png',
  calIconPath: './static/premiumCalculator/src/assets/common/css/images/calendar.png',
  appUrls: {
    "clientDataUrl":"getcustomerdata",
    "quoteUrl":"getIndustryList",
    "occupationUrl":"getOccupationName",
    "newOccupationUrl":"getNewOccupationList",
    "downloadUrl":"download",
    "printQuotePageNew":"printQuotePageNew",
    "calculateUrl":"calculateAll"
    }
};

export default AppConstants;
