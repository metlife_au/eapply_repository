package pages.metlife;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import supportlibraries.ScriptHelper;
import uimap.metlife.VICSManageInsurancePageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class VICSManageInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public VICSManageInsurancePage selectInsuranceCoverOption() {
		manageVICSInsuranceCover();
		return new VICSManageInsurancePage(scriptHelper);
	}
	
	public VICSManageInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void manageVICSInsuranceCover(){
		try {
			List<WebElement> tblData = null;
			String[] arrDB = null;
			String[] arrTPD = null;
			tblData = fluentWaitListElements(By.cssSelector(".tableProfile"));
			String dynDeathBenefit = null;
			dynDeathBenefit=getCellValue(tblData.get(0), "2#1");
			String dynTPD = null;
			dynTPD=getCellValue(tblData.get(1), "2#1");
			
			if(dynDeathBenefit!=null)
				arrDB=dynDeathBenefit.split(" ");
			if(dynTPD!=null)
				arrTPD = dynTPD.split(" ");
			
			storeSessionThreadData("dyn_previousDB",arrDB[0]);
			storeSessionThreadData("dyn_previousTPD",arrTPD[0]);
			
			tryAction(waitForClickableElement(VICSManageInsurancePageObjects.lnkChgInsuranceCover),"Click", "Change Insurance Cover Link");
			
			tryAction(waitForClickableElement(VICSManageInsurancePageObjects.chkBoxConsent),"chkCheck","Consent for duty of disclosure.");
			sleep();
			tryAction(waitForClickableElement(VICSManageInsurancePageObjects.btnOk),"Click", "OK");
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action manageVICSInsuranceCover " + e.getMessage(), Status.FAIL);
		}
	}
}
