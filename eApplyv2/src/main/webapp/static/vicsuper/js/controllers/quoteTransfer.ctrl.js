/* Transfer Cover Controller,Progressive and Mandatory validations Starts  */
VicsuperApp.controller('quotetransfer',['$scope','$rootScope', '$routeParams','$location','$http','$timeout','$window','persoanlDetailService','MaxLimitService','QuoteService','OccupationService','deathCoverService','tpdCoverService','ipCoverService', 'TransferCalculateService', 'NewOccupationService','auraInputService','PersistenceService','ngDialog','auraResponseService','Upload','urlService','saveEapply','RetrieveAppDetailsService','DownloadPDFService','printQuotePage','tokenNumService', '$q','$filter','APP_CONSTANTS',
                                         function($scope,$rootScope,$routeParams,$location,$http,$timeout,$window,persoanlDetailService,MaxLimitService,QuoteService,OccupationService,deathCoverService,tpdCoverService,ipCoverService, TransferCalculateService, NewOccupationService, auraInputService,PersistenceService,ngDialog,auraResponseService,Upload,urlService,saveEapply,RetrieveAppDetailsService,DownloadPDFService,printQuotePage,tokenNumService, $q, $filter,APP_CONSTANTS){
  
	/* Code for appD starts */
    var pageTracker = null;
    if(ADRUM) {
      pageTracker = new ADRUM.events.VPageView();
      pageTracker.start();
    }
  
    $scope.$on('$destroy', function() {
      pageTracker.end();
      ADRUM.report(pageTracker);
    });
    /* Code for appD ends */
	
	$scope.urlList = urlService.getUrlList();
    $scope.phoneNumbrTrans = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
    $scope.emailFormatTrans = APP_CONSTANTS.emailFormat;
    // ($scope.deathCoverTransDetails.amount=='0')
    $scope.indexation= {
        death: false,
        disable: false
    };
    $scope.premiumFrequencyOptions = ['Weekly', 'Monthly', 'Yearly'];
    $scope.showWithinOfficeTransferQuestion = false;
    $scope.showTertiaryTransferQuestion = false;
    $scope.showHazardousTransferQuestion = false;
    $scope.showOutsideOffice = false;
    $scope.premTransFreq = "Weekly";
    $scope.contactTypeOptions = ["Home phone", "Work phone", "Mobile"];
    $scope.isIPCoverRequiredDisabled = false;
    $scope.occupUpgradeNotEligible = false;
    $scope.modelOptions = {updateOn: 'blur'};
    var deathTransDBCategory, tpdTransDBCategory, ipTransDBCategory;
    
    var  standard = 'general';
	var  whitecollor = 'white collar';
	var  ownoccupation = 'own occupation';
	var  professional = 'professional';

    $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
    $scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
    $scope.ipCoverTransDetails = ipCoverService.getIpCover();
    
    /*$scope.TransIPRequireCover = $scope.ipCoverTransDetails.amount;*/
    
    $rootScope.$broadcast('enablepointer');
    $scope.fileNotUploadedError = false;
    $scope.fileFormatError = false;
    $scope.prevOtherOcc = null;
    /*Error Flags*/
    $scope.dodFlagErr = null;
    $scope.privacyFlagErr = null;
    $scope.changeDeathReqCover = false;
    $scope.addtionalDeathTransfer = "Yes";
    $scope.changeTPDReqCover = false;
    $scope.addtionalTPDTransfer = "Yes";
    $scope.inputMsgOccRating;
    $scope.disableGender = false;
    $scope.ipUnitcalculated = 0;
    $scope.ownOccuptionDeath = false;
	$scope.ownOccuptionTpd = false;
	$scope.transferUnits = false;
	$scope.hideCoverOnNo = false;
	$scope.continueButtonHit = false;
	/*$scope.ipOwnOccButton = false;
	$scope.finalRating = 1;
	$scope.ownOccuptionIp = "No";*/
	$scope.corruptedFile = false;
	$scope.corruptedFileMsg = '';
	$scope.displayCoverSec = false;
	
    $scope.setIndexation = function ($event) {
      $event.stopPropagation();
      $event.preventDefault();
      $scope.indexation.death = $scope.indexation.disable = !$scope.indexation.death;
      if(!$scope.indexation.death) {
        $("#indexation-death").parent().removeClass('active');
        $("#indexation-disable").parent().removeClass('active');
      }
    }
   /* QuoteService.getList($scope.urlList.quoteUrl,"VICT").then(function(res){
      $scope.IndustryOptions = res.data;
    }, function(err){
      //console.log("Error while getting industry options " + JSON.stringify(err));
    });*/

    var annualSalForTransUpgradeVal;

    //Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
    $scope.isEmpty = function(value){
      return ((value == "" || value == null) || value == "0");
    };

    
    $scope.isUnitized = function(value){
        return (value == "1");
      };
    
     /*var unitCheck = false;
      $scope.checkUnitisedCheckbox = function(){
        $timeout(function(){
          unitCheck = $('#equivalentUnit').hasClass('active');
        },1);
      };*/
    /*$scope.getOccupations = function(){
      if($scope.otherOccupationObj)
          $scope.otherOccupationObj.transferotherOccupation = '';
      $scope.occupUpgradeNotEligible = false;
      OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.transferIndustry).then(function(res){
        $scope.OccupationList = res.data;
          $scope.occupationTransfer = '';
          if($scope.toggleThree)
    	  {
    	  $scope.toggleThree(false);
          $scope.toggleFour(false);
    	  }
      }, function(err){
        //console.log("Error while fetching occupation options " + JSON.stringify(err));
      });
    };
    
    $scope.getOtherOccupationAS = function(entered) {

    	return $http.get('./occupation.json').then(function(response) {
		      $scope.occupationList=[];
		  if(response.data.Other) {
			  for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	               obj.name=response.data.Other[key];
	               $scope.occupationList.push(obj.name);
	        }
	      }
		  return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });
	  };  */

    $scope.go = function ( path ) {
      $location.path( path );
    };

   /* if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
	{
    $scope.deathUnitsForTransfer = $scope.deathCoverTransDetails.units;
    $scope.tpdUnitsForTransfer = $scope.tpdCoverTransDetails.units;
    	$scope.unitRoundMsg = "The total cover amount has been rounded up to the nearest number of units. ";
    	$scope.roundingIndDeath = false;
    	$scope.roundingIndTPD = false;
	}*/

 // Added to get user details and Maximum limits from Rulesheet
    var DCTransMaxAmount, TPDTransMaxAmount, IPTransMaxAmount;
    var mode3Flag = false;
    var inputDetails = persoanlDetailService.getMemberDetails();
    $scope.personalDetails = inputDetails.personalDetails;


    /*MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"VICT",inputDetails.memberType,"TCOVER").then(function(res){
    var limits = res.data;
    annualSalForTransUpgradeVal = limits[0].annualSalForUpgradeVal;
  }, function(error){
    console.info('Something went wrong while fetching limits ' + error);
  });*/

  if(inputDetails && inputDetails.contactDetails.emailAddress){
    $scope.transferEmail = inputDetails.contactDetails.emailAddress;
  }
  if(inputDetails && inputDetails.contactDetails.prefContactTime){
    if(inputDetails.contactDetails.prefContactTime == "1"){
      $scope.TransferTime= "Morning (9am - 12pm)";
    }else{
      $scope.TransferTime= "Afternoon (12pm - 6pm)";
    }
  }
  if(inputDetails.contactDetails.prefContact == null || inputDetails.contactDetails.prefContact == "")
	{
		inputDetails.contactDetails.prefContact =1;
	}
    if(inputDetails && inputDetails.contactDetails.prefContact){
      if(inputDetails.contactDetails.prefContact == "1"){
        $scope.preferredContactType= "Mobile";
        $scope.transferPhone = inputDetails.contactDetails.mobilePhone;
      }else if(inputDetails.contactDetails.prefContact == "2"){
        $scope.preferredContactType= "Home phone";
        $scope.transferPhone = inputDetails.contactDetails.homePhone;
      }else if(inputDetails.contactDetails.prefContact == "3"){
        $scope.preferredContactType= "Work phone";
        $scope.transferPhone = inputDetails.contactDetails.workPhone;
      }
     }
    $scope.changePrefContactType = function(){
      if($scope.preferredContactType == "Home phone"){
        $scope.transferPhone = inputDetails.contactDetails.homePhone;
      } else if($scope.preferredContactType == "Work phone"){
        $scope.transferPhone = inputDetails.contactDetails.workPhone;
      } else if($scope.preferredContactType == "Mobile"){
        $scope.transferPhone = inputDetails.contactDetails.mobilePhone;
      } else {
        $scope.transferPhone = '';
      }
    }
    //console.log($scope.personalDetails);
    var fetchAppnum = true;
    var appNum;
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
    $scope.otherOccupationObj = {'transferotherOccupation': ''};

    $scope.navigateToLandingPage = function (){
      /*if(window.confirm('Are you sure you want to navigate to Home Page?')){
        $location.path("/landing");
      }*/
      ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
          $location.path("/landing");
        }, function(e){
          if(e=='oncancel'){
            return false;
          }
        });
    }


  //disabling death tpd IP based on age
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
	$scope.ageLimit=anb;
	/*if($scope.deathCoverTransDetails && $scope.deathCoverTransDetails.benefitType && $scope.deathCoverTransDetails.benefitType == 1 ){
		if($scope.ageLimit <= 11 || $scope.ageLimit > 70){
			$('#deathsection').removeClass('active');
			$("#death").css("display", "none");
			$scope.isDeathDisabled = true;
		}
	}
	if($scope.tpdCoverTransDetails && $scope.tpdCoverTransDetails.benefitType && $scope.tpdCoverTransDetails.benefitType == 2 ){
		if($scope.ageLimit <= 11 || $scope.ageLimit > 65){
			$('#tpdsection').removeClass('active');
			$("#tpd").css("display", "none");
			$scope.isTPDDisabled = true;
		}
	}
	if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitType && $scope.ipCoverTransDetails.benefitType == 4 ){
		if($scope.ageLimit < 15 || $scope.ageLimit > 65){
			$('#ipsection').removeClass('active');
			$("#sc").css("display", "none");
			$scope.isIPDisabled = true;
		}
	}*/

    //end

  //maximum validation
    $scope.validateDeathMaxAmount = function(){
      $scope.coverAmtErrFlag = false;
      $scope.maxDeathErrorFlag = false;
      $scope.maxTotalDeathErrorFlag = false;
      $scope.maxTotalTPDAmt = false;
       // if(parseInt($scope.deathCoverTransDetails.amount) > 2000000){
       //  $scope.TransDeathRequireCover = 0;
       //  $scope.maxDeathErrorFlag = true;
       // } else 
      if(parseInt($scope.TransDeathRequireCover) > 2000000) {
        $scope.maxDeathErrorFlag = true;
      }
      else if( !$scope.changeDeathReqCover && (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount)) > 3500000)
    	  {
    	  $scope.maxTotalDeathErrorFlag = true;
    	  }
    	  
      else if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)) {
           $scope.coverAmtErrFlag = true;
           $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
      }
      if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount)))) {
    	  $scope.maxTotalTPDAmt = true;
          $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
    	  }
      if(parseInt($scope.TransTPDRequireCover) > 0)
    	  {
    	  $scope.validateTpdMaxAmount();
    	  }
      $scope.autoCalculate();
     };

     //15 hour question check to disable IP Start
     $scope.checkFifteenHourQuestion = function(){
    	 
			if($scope.fifteenHrsTransferQuestion == 'No'){
				/*$scope.isIPCoverNameDisabled = true;*/
				$scope.isIPCoverRequiredDisabled = true;
				$scope.TransIPRequireCover =null;
				//$scope.TransIPRequireCover =null;
				$scope.maxIpErrorFlag = false;

			} else {
				/*$scope.isIPCoverNameDisabled = false;*/
				$scope.isIPCoverRequiredDisabled = false;
			}
     }
   //15 hour question check to disable IP End

     $scope.validateTpdMaxAmount = function(){
      $timeout(function() {
        $scope.coverAmtErrFlag = false;
        $scope.maxTpdErrorFlag = false;
        $scope.maxTpdCoverErrorFlag = false;
        $scope.maxTotalTPDAmt = false;
        var maxTpdvalues = 1500000;
        /*if($scope.TransTPDRequireCover != null && $scope.TransDeathRequireCover == null ){
          $scope.coverAmtErrFlag = true;
          $scope.coverAmtErrMsg="You cannot apply for TPD cover without Death Cover.";
        } else*/
        if($scope.TransDeathRequireCover != null)
        	{
	        if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
	          $scope.coverAmtErrFlag = true;
	          $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
	          
	          if((!($scope.changeTPDReqCover) && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) <= (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount))))
	        		  || ($scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) <= (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount))))
	        	  {
	        	  $scope.coverAmtErrFlag = false;
	              $scope.coverAmtErrMsg="";
	        	  
	        	  }
	          else  if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount)))) {
	        	  $scope.maxTotalTPDAmt = true;
	              $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
	              $scope.coverAmtErrFlag = false;
	              $scope.coverAmtErrMsg="";
	        	  }
	          
	        	}else  if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount)))) {
	        		$scope.maxTotalTPDAmt = true;
	        		$scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
	        	}
	        	else if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues) {
	        		$scope.maxTpdErrorFlag = true;
	        	} else if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount) > 2500000) {
	        		$scope.maxTpdCoverErrorFlag = true;
	        }
       }
        else
        	{
        	if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.deathCoverTransDetails.amount)){
  	          $scope.coverAmtErrFlag = true;
  	          $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
  	          
  	          if((!($scope.changeTPDReqCover) && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) <= parseInt($scope.deathCoverTransDetails.amount)))
  	        		  || ($scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) <= (parseInt($scope.deathCoverTransDetails.amount))))
  	        	  {
  	        	  $scope.coverAmtErrFlag = false;
  	              $scope.coverAmtErrMsg="";
  	        	  
  	        	  }
  	          else  if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.deathCoverTransDetails.amount)))) {
  	        	  $scope.maxTotalTPDAmt = true;
  	              $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
  	              $scope.coverAmtErrFlag = false;
  	              $scope.coverAmtErrMsg="";
  	        	  }
  	          
  	        	}else  if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) > 0 && ((parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount)) > (parseInt($scope.deathCoverTransDetails.amount)))) {
  	        		$scope.maxTotalTPDAmt = true;
  	        		$scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
  	        	}
  	        	else if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues) {
  	        		$scope.maxTpdErrorFlag = true;
  	        	} else if(!$scope.changeTPDReqCover && parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount) > 2500000) {
  	        		$scope.maxTpdCoverErrorFlag = true;
  	        }
        	}
        
        $scope.autoCalculate();
      });
    	 // $scope.coverAmtErrFlag = false;
      //    $scope.maxTpdErrorFlag = false;
      //    var maxTpdvalues = 1500000;
         
      //    if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues) {
      //        $scope.TransTPDRequireCover = 1500000;
      //        $scope.maxTpdErrorFlag = true;
             
      //        if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
      //       	 $scope.coverAmtErrFlag = true;
      //       	 $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
      //       }
      //    }else if($scope.TransTPDRequireCover != null && $scope.TransDeathRequireCover == null ){
      //      $scope.coverAmtErrFlag = true;
      //      $scope.coverAmtErrMsg="You cannot apply for TPD cover without Death Cover.";
      //    } else if(parseInt($scope.tpdCoverTransDetails.amount) >= 5000000){
      //      $scope.TransTPDRequireCover = 0;
      //        $scope.maxTpdErrorFlag = true;
      //    } else if(parseInt($scope.TransTPDRequireCover) > parseInt($scope.TransDeathRequireCover)){
      //   	 $scope.coverAmtErrFlag = true;
      //   	 $scope.coverAmtErrMsg="TPD amount should not be greater than your Death amount.";
        	 
      //   }
      //    else if(parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount) > 5000000){
      //      $scope.TransTPDRequireCover = 5000000 - $scope.tpdCoverTransDetails.amount;
      //      $scope.maxTpdErrorFlag = true;
      //    }
      //    else if(parseInt($scope.TransTPDRequireCover) > maxTpdvalues && parseInt($scope.TransDeathRequireCover)== 1500000 ){
      //        $scope.maxTpdErrorFlag = true;
      //        $scope.TransTPDRequireCover=maxTpdvalues;
           
      //    }
      //   $scope.autoCalculate();
    };
    
        
    $scope.validateIpMaxAmount = function(){
      var ipMaxAmount = 15000;
      $scope.ipCoverAmtErrFlag = false;
      $scope.maxIpErrorFlag = false;
      var totalAllowableTransfer = 30000;
      var eightyFivePercentOfSal = parseInt(0.85 * (parseInt($scope.annualTransferSalary)/12));
     /* var tempTotalCover = parseInt($scope.TransIPRequireCover) + parseInt($scope.ipCoverTransDetails.amount);*/
      var tempTotalCover = parseInt($scope.TransIPRequireCover);
      if (eightyFivePercentOfSal < totalAllowableTransfer)
    	  {
    	  totalAllowableTransfer = eightyFivePercentOfSal;
    	  }
      
     /* if(eightyFivePercentOfSal < parseInt($scope.ipCoverTransDetails.amount)) 
      {
          $scope.TransIPRequireCover = 0;
          $scope.maxIpErrorFlag = true;
      }*/
     if(tempTotalCover > totalAllowableTransfer) 
      {
          /*$scope.TransIPRequireCover = totalAllowableTransfer - parseInt($scope.ipCoverTransDetails.amount);*/
    	  $scope.TransIPRequireCover = totalAllowableTransfer;
          $scope.maxIpErrorFlag = true;
      } else if(parseInt($scope.TransIPRequireCover) > ipMaxAmount) 
      {
          $scope.maxIpErrorFlag = true;
          $scope.TransIPRequireCover = ipMaxAmount;
      } 
      
      
      $scope.autoCalculate();
    };

    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));

    $scope.calculateTransfer = function(){
      
    	$scope.maxTotalTPDAmt = false;
    	$scope.transferUnits = false;
    	$scope.deathCoverTypeTransfer = "DcFixed";
        $scope.tpdCoverTypeTransfer = "TpdFixed";
        $scope.deathUnitsTransfer = 0;
        $scope.tpdUnitsTransfer = 0;
        if((typeof($scope.TransDeathRequireCover) == "undefined" || $scope.TransDeathRequireCover=='' || $scope.TransDeathRequireCover == null)
        		&& (typeof($scope.TransTPDRequireCover) == "undefined" || $scope.TransTPDRequireCover=='' || $scope.TransTPDRequireCover== null)
        		&& ($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1"))
        	{
        	$scope.deathCoverTypeTransfer = "DcUnitised";
            $scope.tpdCoverTypeTransfer = "TPDUnitised";
            $scope.deathUnitsTransfer = $scope.deathCoverTransDetails.units;
            $scope.tpdUnitsTransfer = $scope.tpdCoverTransDetails.units;
            $scope.transferUnits = true;
        	}
    	
    	
    	if(typeof($scope.TransDeathRequireCover) == "undefined" || $scope.TransDeathRequireCover=='' || $scope.TransDeathRequireCover == null)
    	{
    		$scope.TransDeathRequireCover = 0;
    	}
    	if(typeof($scope.TransTPDRequireCover) == "undefined" || $scope.TransTPDRequireCover=='' || $scope.TransTPDRequireCover== null)
      	{
    	  $scope.TransTPDRequireCover = 0;
      	}
//    if(typeof($scope.TransIPRequireCover) == "undefined" || $scope.TransIPRequireCover==''){
//        $scope.TransIPRequireCover = 0;
//      }
      if(($scope.deathCoverTransDetails && $scope.deathCoverTransDetails.amount && typeof($scope.deathCoverTransDetails.amount) == "undefined") || $scope.deathCoverTransDetails.amount==''){
        $scope.deathCoverTransDetails.amount = 0;
      }
      if(($scope.tpdCoverTransDetails && $scope.tpdCoverTransDetails.amount && typeof($scope.tpdCoverTransDetails.amount) == "undefined") || $scope.tpdCoverTransDetails.amount==''){
        $scope.tpdCoverTransDetails.amount = 0;
      }
      if(($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.amount && typeof($scope.ipCoverTransDetails.amount) == "undefined") || $scope.ipCoverTransDetails.amount==''){
        $scope.ipCoverTransDetails.amount = 0;
      }

      /*if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitPeriod && $scope.ipCoverTransDetails.benefitPeriod != ''){
          $scope.benefitPeriodTransAddnl = $scope.ipCoverTransDetails.benefitPeriod;
      } else{
        $scope.benefitPeriodTransAddnl = $scope.benefitPeriodTransPer == 'Age 65' ? $scope.benefitPeriodTransPer : '2 Years';
      }*/
      
      var deathFixedAmt = 0;
      var tpdFixedAmt = 0;
      if(!$scope.changeDeathReqCover)
    	  {
    	  deathFixedAmt = parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount);
    	  }
      else if( $scope.TransDeathRequireCover == 0)
    	  {
    	  deathFixedAmt = parseInt($scope.deathCoverTransDetails.amount);
    	  }
      else
    	  {
    	  deathFixedAmt =parseInt($scope.TransDeathRequireCover);
    	  }
      
      if(!$scope.changeTPDReqCover)
	  	{
    	  tpdFixedAmt = parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount);
	  	}
      else if($scope.TransTPDRequireCover == 0)
    	  {
    	  tpdFixedAmt =parseInt($scope.tpdCoverTransDetails.amount);
    	  }
      else
	  	{
    	  tpdFixedAmt =parseInt($scope.TransTPDRequireCover);
	  	}
      
      $scope.ipNoReplace;
      if((typeof($scope.TransIPRequireCover) == "undefined") || ($scope.TransIPRequireCover === '') || $scope.TransIPRequireCover == null){
    	  $scope.ipNoReplace = $scope.ipCoverTransDetails.amount;
    	  $scope.TransIPRequireCover = null;
      } else {
    	  $scope.ipNoReplace = $scope.TransIPRequireCover;
      }
      
      
      var ruleModel = {
            "age": anb,
            "fundCode": "VICT",
            "gender": $scope.gender,
            "deathOccCategory": $scope.transferDeathOccupationCategory,
            "tpdOccCategory": $scope.transferTpdOccupationCategory,
            "ipOccCategory": $scope.transferIpOccupationCategory,
            "manageType": "TCOVER",
            "deathCoverType": $scope.deathCoverTypeTransfer,
            "tpdCoverType": $scope.tpdCoverTypeTransfer,
            "ipCoverType": "IpFixed",
            "deathUnits": $scope.deathUnitsTransfer,
            "tpdUnits": $scope.tpdUnitsTransfer,
            "deathFixedAmount":deathFixedAmt,
            "tpdFixedAmount":tpdFixedAmt,
            "premiumFrequency": $scope.premTransFreq,
            "ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
            "ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
            "deathTransferAmount": parseInt($scope.TransDeathRequireCover),
            "tpdTransferAmount": parseInt($scope.TransTPDRequireCover),
            "ipTransferAmount": parseInt($scope.ipNoReplace),
            "deathExistingAmount": parseInt($scope.deathCoverTransDetails.amount),
            "tpdExistingAmount": parseInt($scope.tpdCoverTransDetails.amount),
            "ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
          };

      TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
      //TransferCalculateService.calculate({}, ruleModel, function(res){
        var premium = res.data;
        autoCalculate = true;
        for(var i = 0; i < premium.length; i++){
          if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
            $scope.dcTransCoverAmount = premium[i].coverAmount;
            $scope.dcTransCost = premium[i].cost;
            if($scope.dcTransCost == null){
                $scope.dcTransCost = 0.00;
              }
          } else if(premium[i].coverType == 'TpdFixed' || premium[i].coverType == 'TPDUnitised'){
            $scope.tpdTransCoverAmount = premium[i].coverAmount;
            $scope.tpdTransCost = premium[i].cost;
            if($scope.tpdTransCost == null){
                $scope.tpdTransCost = 0.00;
              }
          } else if(premium[i].coverType == 'IpFixed'){
            $scope.ipTransCoverAmount = premium[i].coverAmount||0.00;
            $scope.ipTransCost = premium[i].cost;
            if($scope.ipTransCost == null){
                $scope.ipTransCost = 0.00;
              }
            $scope.ipUnitcalculated=0;
            if(parseInt($scope.ipTransCoverAmount) > 0)
            	{
            $scope.ipUnitcalculated = Math.ceil(parseInt($scope.ipTransCoverAmount)/500);
            	}
          }
      }

        $scope.totalTransCost = parseFloat($scope.dcTransCost) + parseFloat($scope.tpdTransCost) + parseFloat($scope.ipTransCost);
        if(parseInt($scope.tpdTransCoverAmount) > parseInt($scope.dcTransCoverAmount))
        	{
        	$scope.maxTotalTPDAmt = true;
            $scope.coverAmtTPDErrMsg="Total TPD cover amount should not be greater than your Total Death amount.";
        	}
        if(fetchAppnum){
          fetchAppnum = false;
          appNum = PersistenceService.getAppNumber();
        }
        if($scope.TransDeathRequireCover == 0)
    	{
    	$scope.TransDeathRequireCover = null;
    	}
    if($scope.TransTPDRequireCover == 0)
    	{
    	$scope.TransTPDRequireCover= null;
    	}
      }, function(err){
        //console.log("Error while calculating transfer premium " + JSON.stringify(err));
      });
    };
    $scope.coverAmtErrFlag= false;
    var autoCalculate = false;
    $scope.autoCalculate = function(){
      if(autoCalculate && !$scope.coverAmtErrFlag && !$scope.maxTotalTPDAmt && !$scope.maxDeathErrorFlag && !$scope.maxTotalDeathErrorFlag && !$scope.maxTpdErrorFlag && !$scope.maxTpdCoverErrorFlag){
        $scope.calculateTransfer();
      }
    };

    $scope.getTransCategoryFromDB = function(fromOccupation){
    	
		     if(fromOccupation && $scope.occupationTransfer!=="Other"){
		    	 $scope.otherOccupationObj.transferotherOccupation = '';
		      }
		     if( $scope.prevOtherOcc !== $scope.otherOccupationObj.transferotherOccupation){
		      if($scope.occupationTransfer != undefined || $scope.otherOccupationObj.transferotherOccupation!= undefined){
		    	  if(fromOccupation) {
		    	        $scope.withinOfficeTransferQuestion = null;
		    	        $scope.tertiaryTransferQuestion = null;
		    	        $scope.hazardousTransferQuestion = null;
		    	        $scope.outsideOffice = null;                                                     
		    	   }
		        /*var occName = $scope.transferIndustry + ":" + $scope.occupationTransfer;*/
		    	  if($scope.occupationTransfer != undefined && ($scope.otherOccupationObj.transferotherOccupation == null || $scope.otherOccupationObj.transferotherOccupation == '')){
		    		  var occName = $scope.transferIndustry + ":" + $scope.occupationTransfer;
		          }else if ($scope.otherOccupationObj.transferotherOccupation != undefined){
		        	  	/*$scope.prevOtherOcc = $scope.otherOccupationObj.transferotherOccupation;
			  	    	if(($scope.OccupationList && $scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.transferotherOccupation))!== undefined){
			  	    		 var occName = $scope.transferIndustry + ":" + $scope.otherOccupationObj.transferotherOccupation;
			  	    	}else{
			  	    		var occName = $scope.transferIndustry + ":" + $scope.occupationTransfer;
			  	    	}*/
		        	 
		          }
		    	  
		    	  
		        NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "VICT", occName).then(function(res){
		          deathTransDBCategory = res.data[0].deathfixedcategeory;
		          tpdTransDBCategory = res.data[0].tpdfixedcategeory;
		          ipTransDBCategory = res.data[0].ipfixedcategeory;
		          $scope.renderOccupationQuestions();
		        }, function(err){
		          console.info("Error while getting transfer category from DB " + JSON.stringify(err));
		        });
		      }
    	}
    };

    $scope.renderOccupationQuestions = function(){

    	var occQustnsRating = 1; 
        var overAllOccRating = 1;
       
    	if (($scope.occRating1a 
    			&& $scope.occRating1a == 'Yes') 
    				&&($scope.occRating2 && $scope.occRating2 == 'Yes') 
    			&& $scope.annualTransferSalary > 100000){
    		occQustnsRating=3;
    	} else if (($scope.occRating1a && $scope.occRating1a == 'Yes') ||
    			($scope.occRating1b && $scope.occRating1b == 'Yes')){
    		occQustnsRating = 2;
    	}   else {
    		occQustnsRating = 1;
    	}   	
        
        if ($scope.inputMsgOccRating && occQustnsRating){
        	 overAllOccRating = Math.max($scope.inputMsgOccRating,occQustnsRating);
        } else if (occQustnsRating){
        	overAllOccRating = occQustnsRating;
        } 
       /* $scope.finalRating = overAllOccRating;*/
        if (overAllOccRating == 3){
    		$scope.transferDeathOccupationCategory = 'Professional';
    		$scope.transferTpdOccupationCategory = 'Professional';
        	$scope.transferIpOccupationCategory = 'Professional';
        	
        } else if (overAllOccRating == 2) {
        	$scope.transferDeathOccupationCategory = 'White Collar';
    		$scope.transferTpdOccupationCategory = 'White Collar';
        	$scope.transferIpOccupationCategory = 'White Collar';
        } else {
        	$scope.transferDeathOccupationCategory = 'General';
        	$scope.transferTpdOccupationCategory = 'General';
        	$scope.transferIpOccupationCategory = 'General';
        }
        if(overAllOccRating == 2 || overAllOccRating == 3)
    	{
    	if($scope.ownOccuptionDeath)
		{
    		$scope.transferDeathOccupationCategory = 'Own Occupation';
		}
    	if ($scope.ownOccuptionTpd)
    	{
		$scope.transferTpdOccupationCategory = 'Own Occupation';
    	}
    	if($scope.ipCoverTransDetails.occRating.toLowerCase() == ownoccupation && (($scope.benefitPeriodTransAddnl === '5 Years' || $scope.benefitPeriodTransAddnl === 'Age 65')))
		{
		$scope.transferIpOccupationCategory  = 'Own Occupation';
		}
    	}
        
       /* 
        if(overAllOccRating != 1 && ($scope.benefitPeriodTransAddnl === '5 Years' || $scope.benefitPeriodTransAddnl === 'Age 65'))
		{
		$scope.ipOwnOccButton = true;
		}
        else
        	{
        	$scope.ipOwnOccButton = false;
        	$scope.ownOccuptionIp = "No";
        	
        	
        	var radios = $('label[radio-sync]');
	    	var data = $('input[data-sync]');
			data.filter('[data-sync="' + $scope.ownOccuptionIp + '"]').attr('checked','checked');
			radios.filter('[radio-sync="' + "Yes" + '"]').removeClass('active');
			radios.filter('[radio-sync="' + $scope.ownOccuptionIp + '"]').addClass('active');
        	
        	}*/
        
      $scope.checkFifteenHourQuestion();
      $scope.autoCalculate();
    };

$scope.citizenshipChange = function(){
		$scope.hideCoverOnNo = false;
		if($scope.areyouperCitzTransferQuestion == 'No')
			{
				$scope.hideCoverOnNo = true;
				 $scope.toggleThree(false);
		         $scope.toggleFour(false);
			}
		else
			{
			if(this.occupationDetailsTransferForm.$valid && $scope.continueButtonHit)
				{
				$scope.toggleThree(true);
				}
			if(this.occupationDetailsTransferForm.$valid && $scope.continueButtonHit && this.previousCoverForm.$valid)
				{
				$scope.toggleFour(true);
				}
			}
	};

   /* Check if your is allowed to proceed to the next accordion */
  // TBC
  // Need to revisit, need better implementation
  $scope.isCollapsible = function(targetEle, event) {
    if( targetEle == 'collapseprivacy' && !$('#dodCkBoxLblId').hasClass('active')) {
      if($('#dodCkBoxLblId').is(':visible'))
          $scope.dodFlagErr = true;
      event.stopPropagation();
      return false;
    } else if( targetEle == 'collapseOne' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active'))) {
      if($('#privacyCkBoxLblId').is(':visible'))
          $scope.privacyFlagErr = true;
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseTwo' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
      if($("#collapseOne form").is(':visible'))
          $scope.CoverDetailsTransferFormSubmit($scope.coverDetailsTransferForm);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseThree' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid') || $scope.hideCoverOnNo)) {
      if($("#collapseTwo form").is(':visible'))
          $scope.CoverDetailsTransferFormSubmit($scope.occupationDetailsTransferForm);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseFour' && (!$('#dodCkBoxLblId').hasClass('active') || !$('#privacyCkBoxLblId').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid') || $("#collapseThree form").hasClass('ng-invalid') || $scope.fileNotUploadedError || $scope.hideCoverOnNo)) {
      if($("#collapseThree form").is(':visible'))
          $scope.CoverDetailsTransferFormSubmit($scope.previousCoverForm);
      event.stopPropagation();
      return false;
    }
  }

   // added for toggle and collapse the sections
    $scope.privacyCol = false;
    $scope.contactCol = false;
    $scope.occupationCol = false;
    $scope.previousSectionCol = false;
    $scope.transferSectionCol = false;
    var dodCheck;
    var privacyCheck;
    var privacyVal = 0;
    var contactVal = 0;
    var occupationVal = 0;
    var previousSectionVal = 0;
    var transferCoverVal = 0;

    // // privacy section
    // $scope.togglePrivacy = function(flag) {
    //     $scope.privacyCol = flag;
    //     $("a[data-target='#collapseprivacy']").click();

    // };

    // // contact section
    // $scope.toggleContact = function(flag) {
    //     $scope.contactCol = flag;
    //     $("a[data-target='#collapseOne']").click();

    // };

    /* TBC */
    $scope.togglePrivacy = function(checkFlag) {
        $scope.privacyCol = checkFlag;
        if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
    };

    $scope.toggleContact = function(checkFlag) {
        $scope.contactCol = checkFlag;
        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseOne']").click(); /* Can be improved */

    };

    // occupation section
    $scope.toggleTwo = function(checkFlag) {
        $scope.occupationCol = checkFlag;
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };

    // previous cover section
    $scope.toggleThree = function(checkFlag) {
        $scope.previousSectionCol = checkFlag;
        if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseThree']").click(); /* Can be improved */
    };

    // transfer quote section
    $scope.toggleFour = function(checkFlag) {
        $scope.transferSectionCol = checkFlag;
        if((checkFlag && $('#collapseFour').hasClass('collapse in')) || (!checkFlag && !$('#collapseFour').hasClass('collapse in')))
          return false;
        $("a[data-target='#collapseFour']").click(); /* Can be improved */
    };

   //  // occupation section
    // $scope.toggleTwo = function(flag) {
   //      $scope.occupationCol = flag;
   //      occupationVal++;
   //      $("a[data-target='#collapseTwo']").click();
   //  };

   //  // previous cover section
    // $scope.toggleThree = function(flag) {
   //      $scope.previousSectionCol = flag;
   //      previousSectionVal++;
   //      $("a[data-target='#collapseThree']").click();
   //  };

   //  // transfer quote section
    // $scope.toggleFour = function(flag) {
   //      $scope.transferSectionCol = flag;
   //      transferCoverVal++;
   //      $("a[data-target='#collapseFour']").click();
   //  };

  // validation for DOD checkbox
    $scope.checkDodState = function(){
      $timeout(function() {
        $scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodCkBoxLblId').hasClass('active') : !$scope.dodFlagErr;
        if($('#dodCkBoxLblId').hasClass('active')) {
          $scope.togglePrivacy(true);
        } else {
          $scope.togglePrivacy(false);
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
      }, 1);
      // $timeout(function(){
      //  dodCheck = $('#dodCkBoxLblId').hasClass('active');
      //  if(dodCheck){
      //    $scope.dodFlagErr = false;
      //    privacyVal++;
      //    if(privacyVal > 0){
      //      $scope.togglePrivacy(true);
      //    } else{
      //      $scope.togglePrivacy(false);
      //    }
      //    if(contactVal > 0){
      //      $scope.toggleContact(true);
      //    } else{
      //      $scope.toggleContact(false);
      //    }
      //    if(occupationVal > 0){
      //      $scope.toggleTwo(true);
      //    }else{
      //      $scope.toggleTwo(false);
      //    }
      //    if(previousSectionVal > 0){
      //      $scope.toggleThree(true);
      //    }else{
      //      $scope.toggleThree(false);
      //    }
      //    if(transferCoverVal > 0){
      //      $scope.toggleFour(true);
      //    }else{
      //      $scope.toggleFour(false);
      //    }
      //  } else{
      //    $scope.dodFlagErr = true;
      //    $scope.togglePrivacy(false);
      //    $scope.toggleContact(false);
      //    $scope.toggleTwo(false);
      //    $scope.toggleThree(false);
      //    $scope.toggleFour(false);
      //  }
      // }, 1);
    };

    // validation for Privacy checkbox
    $scope.checkPrivacyState  = function(){
      $timeout(function() {
        $scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyCkBoxLblId').hasClass('active') : !$scope.privacyFlagErr;
        if($('#privacyCkBoxLblId').hasClass('active')) {
          $scope.toggleContact(true);
        } else {
          $scope.toggleContact(false);
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
      }, 1);
    //   if(dodCheck){
   //     $timeout(function(){
    //      privacyCheck = $('#privacyCkBoxLblId').hasClass('active');
    //      if(privacyCheck){
    //        $scope.privacyFlagErr = false;
     //       contactVal++;
    //        if(contactVal > 0){
    //          $scope.toggleContact(true);
    //        } else{
    //          $scope.toggleContact(false);
    //        }
    //        if(occupationVal > 0){
    //          $scope.toggleTwo(true);
    //        }else{
    //          $scope.toggleTwo(false);
    //        }
    //        if(previousSectionVal > 0){
    //          $scope.toggleThree(true);
    //        }else{
    //          $scope.toggleThree(false);
    //        }
    //        if(transferCoverVal > 0){
    //          $scope.toggleFour(true);
    //        }else{
    //          $scope.toggleFour(false);
    //        }
    //      } else{
    //        $scope.privacyFlagErr = true;
    //        $scope.toggleContact(false);
    //        $scope.toggleTwo(false);
    //        $scope.toggleThree(false);
    //        $scope.toggleFour(false);
    //      }
    //    }, 1);
    //   }else{
    //    $scope.dodFlagErr = true;
    //    $scope.togglePrivacy(false);
      // $scope.toggleContact(false);
      // $scope.toggleTwo(false);
      // $scope.toggleThree(false);
      // $scope.toggleFour(false);
    //   }
    };

  if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
    $scope.gender ='';
  }else{
    $scope.gender = $scope.personalDetails.gender;
  }
  
  if($scope.gender && (($scope.gender).toLowerCase() === 'female' || ($scope.gender).toLowerCase() === 'male' ))
	{
	$scope.disableGender = true;
	}
	else
	{
	$scope.disableGender = false;
	}
  
  var occupationDetailsTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','occRating1a','occRating1b','occgovrating2','annualTransferSalary'];
    var occupationDetailsOtherTransferFormFields = ['fifteenHrsTransferQuestion','areyouperCitzTransferQuestion','occRating1a','occRating1b','occgovrating2','annualTransferSalary'];
    var coverDetailsTransferFormFields = ['coverDetailsTransferEmail', 'coverDetailsTransferPhone','coverDetailsTransferPrefTime','gender'];

    var previousCoverFormFields = ['previousFundName','membershipNumber','upldFile'];
    var previousCoverFormFieldsWithChkBox = ['previousFundName','membershipNumber'];
    var TranscoverCalculatorFormFields = ['TransDeathRequireCover','TransTPDRequireCover','TransIPRequireCover'];
    $scope.checkCoverDetailsTransferFormPreviousMandatoryFields  = function (elementName,formName){
      var transferFormFields;
      if(formName == 'coverDetailsTransferForm'){
        transferFormFields = coverDetailsTransferFormFields;
      } else if(formName == 'occupationDetailsTransferForm'){
        /*if($scope.occupationTransfer != undefined && $scope.occupationTransfer == 'Other'){
          transferFormFields = occupationDetailsOtherTransferFormFields;
        } else{*/
          transferFormFields = occupationDetailsTransferFormFields;
       /* }*/
          if(elementName == 'occRating1a' && $scope.occRating1a == 'Yes') {
	    	  $scope.occRating1b = 'No'
	      }
	      if(elementName == 'occRating1b' && $scope.occRating1b == 'Yes') {
	    	  $scope.occRating1a = 'No'
	      }
	      if (elementName == 'occRating1a' || elementName == 'occRating1b' || elementName == 'occgovrating2') {
	    	  $scope.renderOccupationQuestions();
	      }
      } else if(formName == 'previousCoverForm'){
    	/*$scope.documentName ? $scope.toggleFour(false) : '' ;*/
       /* if($scope.documentName != undefined && $scope.documentName =='No'){
          transferFormFields = previousCoverFormFieldsWithChkBox;
          $scope.files = [];
          PersistenceService.setUploadedFileDetails($scope.files);
        } else{*/
          transferFormFields = previousCoverFormFields;
        /*}*/
      }else if(formName == 'TranscoverCalculatorForm'){
  		transferFormFields = TranscoverCalculatorFormFields;
  	}
      var inx = transferFormFields.indexOf(elementName);
      if(inx > 0){
        for(var i = 0; i < inx ; i++){
          $scope[formName][transferFormFields[i]].$touched = true;
        }
      }
    };

    $scope.CoverDetailsTransferFormSubmit =  function (form){
      if(form.$name == 'previousCoverForm' && $("#transferitrId_div_id").is(":visible")) {
    	  
    	  $scope.checkCoverDetailsTransferFormPreviousMandatoryFields('upldFile','previousCoverForm');
    	  
        if($scope.files && $scope.files.length) {
          $scope.fileNotUploadedError = false;
        } else {
          $scope.fileNotUploadedError = true;
          return false;
        }
      }

      if(!form.$valid){
        form.$submitted=true;
        if(form.$name == 'coverDetailsTransferForm')
        {
          $scope.toggleTwo(false);
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
        else if(form.$name == 'occupationDetailsTransferForm')
        {
          $scope.toggleThree(false);
          $scope.toggleFour(false);
        }
        else if(form.$name == 'previousCoverForm'){
          $scope.toggleFour(false);
        }
     } else {
      if(form.$name == 'coverDetailsTransferForm')
        {
          $scope.toggleTwo(true);
        }
      else if(form.$name == 'occupationDetailsTransferForm')
        {
    	  $scope.continueButtonHit = true;
    	  if($scope.hideCoverOnNo)
    		  {
    		  return false;
    		  }
        $scope.toggleThree(true);
        }
      else if(form.$name == 'previousCoverForm'){
       // if($scope.tpdClaimTrans == 'No' && $scope.terminalIllnessClaimTrans == 'No'){
          $scope.toggleFour(true);
          $scope.displayCoverSec = true;
      //    toggleFlag = true;
      //  }
      }else if(form.$name == 'TranscoverCalculatorForm'){
        if(!$scope.coverAmtErrFlag && !$scope.maxTotalTPDAmt && !$scope.maxDeathErrorFlag && !$scope.maxTotalDeathErrorFlag && !$scope.maxTpdErrorFlag && !$scope.maxTpdCoverErrorFlag){
          $scope.calculateTransfer();
        }
      }
       }
      //console.log("Form Validation");
    };

  /*  $scope.industryTransferOptions = [{
      options: 'option1',
      industryName: 'Industry 1'
    }, {
      options: 'option2',
      industryName: 'Industry 2'
    }
    ];*/

    $scope.occupationTransferOptions = [{
      options: 'option1',
      occupationName: 'Occupation 1'
      }, {
        options: 'option2',
        occupationName: 'Occupation 2'
      }
    ];


    $scope.files = [];
    $scope.selectedFile = null;
    $scope.uploadFiles = function(files, errFiles) {
      $scope.fileSizeErrFlag = false;
      $scope.fileFormatError = errFiles.length > 0 ? true : false;
      $scope.selectedFile =  files[0];
      
    };
    $scope.addFilesToStack = function () {
    	$scope.corruptedFile = false;
 		$scope.corruptedFileMsg = '';
      var fileSize = ($scope.selectedFile.size / 1048576).toFixed(3);
      if(fileSize > 10) {
        $scope.fileSizeErrFlag=true;
        $scope.fileSizeErrorMsg ="File size should not be more than 10MB";
        $scope.selectedFile = null;
        return;
      }else{
        $scope.fileSizeErrFlag=false;
      }
      if(!$scope.files)
        $scope.files = [];
      $scope.files.push($scope.selectedFile);
      //PersistenceService.setUploadedFileDetails($scope.files);
      $scope.fileNotUploadedError = false;
      $scope.selectedFile = null;
      if($scope.displayCoverSec)
	  {
	  $scope.toggleFour(true);
	  }
    }

  $scope.removeFile = function(index) {
	  $scope.corruptedFile = false;
		$scope.corruptedFileMsg = '';
    $scope.files.splice(index, 1);
    PersistenceService.setUploadedFileDetails($scope.files);
    if($scope.files.length < 1) {
      $scope.fileNotUploadedError = true;
      if($scope.displayCoverSec)
    	  {
    	  $scope.toggleFour(false);
    	  }
    }
  }

  
  $scope.submitFiles = function () {
	  $scope.corruptedFile = false;
		$scope.corruptedFileMsg = '';
	    $scope.uploadedFiles = $scope.uploadedFiles || [];
	    var defer = $q.defer();
	    if(!$scope.files){
	      $scope.files = [];
	    }
	    if(!$scope.files.length) {
	      defer.resolve({});
	    }
	    var upload;
	    var numOfFiles = $scope.files.length;
	    angular.forEach($scope.files, function(file, index) {
	      if(Upload.isFile(file)) {
	        upload = Upload.http({
	          url: $scope.urlList.fileUploadPostUrl,
	          headers : {
	            'Content-Type': file.name,
	            'Authorization':tokenNumService.getTokenId()
	          },
	          data: file
	        });
	        upload.then(function(res){
	        	numOfFiles--;
	          $scope.uploadedFiles[index] = res.data;
	          if(numOfFiles == 0){
	            PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
	            defer.resolve(res);
	          }
	        }, function(err){
	          //console.log("Error uploading the file " + err);
	        	if (err.status == '403'){
	        		PersistenceService.setUploadedFileDetails($scope.files);
	        	    if($scope.files.length < 1) {
	        	      $scope.fileNotUploadedError = true;
	        	    }	        	    
	        		$scope.corruptedFile = true;
	        		$scope.corruptedFileMsg = 'Your document is not a valid document type or corrupted,  Please edit your document and resubmit';
	        	}
	          defer.reject(err);
	        });
	      } else {
	    	  numOfFiles--;
	        if(numOfFiles == 0) {
	          PersistenceService.setUploadedFileDetails($scope.uploadedFiles);
	          defer.resolve({});
	        }
	      }
	    });
	    return defer.promise;
	  };




  $scope.displayExistingWpBp = false;
  if(parseFloat($scope.ipCoverTransDetails.amount) > 0.0){
    $scope.displayExistingWpBp = true;
  } else{
    $scope.displayExistingWpBp = false;
  }


  /*$scope.transferDeathOccupationCategory = $scope.deathCoverTransDetails.occRating;
  $scope.transferTpdOccupationCategory = $scope.tpdCoverTransDetails.occRating;
  $scope.transferIpOccupationCategory = $scope.ipCoverTransDetails.occRating;*/
  
  
  var deathoccRating = $scope.deathCoverTransDetails.occRating;
	if (deathoccRating) {
		
		if(deathoccRating.toLowerCase() == ownoccupation)
		{
		$scope.ownOccuptionDeath = true;
		}
		
		if (deathoccRating.toLowerCase() == standard){
			deathoccRating = 1;
		}
		else if (deathoccRating.toLowerCase() == whitecollor || deathoccRating.toLowerCase() == ownoccupation){
			deathoccRating = 2;
		}
		else if (deathoccRating.toLowerCase() == professional){
			deathoccRating = 3;
		}
	}
	
	var tpdoccRating = $scope.tpdCoverTransDetails.occRating;
	if (tpdoccRating) {	
		
		if(tpdoccRating.toLowerCase() == ownoccupation)
		{
		$scope.ownOccuptionTpd = true;
		}
		
		if (tpdoccRating.toLowerCase() == standard){
			tpdoccRating = 1;
		}
		else if (tpdoccRating.toLowerCase() == whitecollor || tpdoccRating.toLowerCase() == ownoccupation){
			tpdoccRating = 2;
		}
		else if (tpdoccRating.toLowerCase() == professional){
			tpdoccRating = 3;
		}
	}
	
	var ipoccRating = $scope.ipCoverTransDetails.occRating;
	if (ipoccRating) {			
		if (ipoccRating.toLowerCase() == standard){
			ipoccRating = 1;
		}
		else if (ipoccRating.toLowerCase() == whitecollor || ipoccRating.toLowerCase() == ownoccupation){
			ipoccRating = 2;
		}
		else if (ipoccRating.toLowerCase() == professional){
			ipoccRating = 3;
		}
	}
	
	$scope.inputMsgOccRating = Math.max(deathoccRating,tpdoccRating,ipoccRating);		
	
	
	$scope.transferDeathOccupationCategory = $scope.inputMsgOccRating || 'General';
	$scope.transferTpdOccupationCategory = $scope.inputMsgOccRating || 'General';
	$scope.transferIpOccupationCategory = $scope.inputMsgOccRating || 'General';
	

  $scope.waitingPeriodTransOptions = ["14 Days", "30 Days", "45 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodTransOptions = ["2 Years", "5 Years", "Age 60", "Age 65","Age 67","Age 70"];

    $scope.waitingPeriodTransAdlnOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodTransAdlnOptions = ['2 Years', "5 Years",'Age 65'];

    $scope.preferredContactTransOptions = ['Mobile','Office','Home'];

    $scope.regex = /[0-9]{1,3}/;
    $scope.dcTransCoverAmount = 0.00;
  $scope.dcTransCost = 0.00;
  $scope.tpdTransCoverAmount = 0.00;
  $scope.tpdTransCost = 0.00;
  $scope.ipTransCoverAmount = 0.00;
  $scope.ipTransCost = 0.00;
  $scope.totalTransCost = 0.00;
  
  
  switch($scope.ipCoverTransDetails.waitingPeriod.toLowerCase().trim().substr(0,2)){
	
	case "30":
		 $scope.ipCoverTransDetails.waitingPeriod = "30 Days";
		break;
	case "60":
		 $scope.ipCoverTransDetails.waitingPeriod = "60 Days";
		break;
	case "90":
		 $scope.ipCoverTransDetails.waitingPeriod = "90 Days";
		break;
	}
switch($scope.ipCoverTransDetails.benefitPeriod.toLowerCase().trim().substr(0,1)){
	
	case "2":
		$scope.ipCoverTransDetails.benefitPeriod = "2 Years";
		break;
	case "5":
		$scope.ipCoverTransDetails.benefitPeriod = "5 Years";
		break;
	case "A":
		$scope.ipCoverTransDetails.benefitPeriod = "Age 65";
		break;
	case "a":
		$scope.ipCoverTransDetails.benefitPeriod = "Age 65";
		break;
	}

  if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.waitingPeriod && $scope.ipCoverTransDetails.waitingPeriod != ''){
      $scope.waitingPeriodTransPer = $scope.ipCoverTransDetails.waitingPeriod;
      $scope.waitingPeriodTransAddnl = $scope.ipCoverTransDetails.waitingPeriod;
  } else {
    $scope.waitingPeriodTransPer = $scope.waitingPeriodTransPer ? $scope.waitingPeriodTransPer : '90 Days';
    $scope.waitingPeriodTransAddnl = $scope.waitingPeriodTransPer;
  }

  if($scope.ipCoverTransDetails && $scope.ipCoverTransDetails.benefitPeriod && $scope.ipCoverTransDetails.benefitPeriod != ''){
      $scope.benefitPeriodTransPer = $scope.ipCoverTransDetails.benefitPeriod;
      $scope.benefitPeriodTransAddnl = $scope.ipCoverTransDetails.benefitPeriod;
  } else{
    $scope.benefitPeriodTransPer = $scope.benefitPeriodTransPer ? $scope.benefitPeriodTransPer : '2 Years';
    $scope.benefitPeriodTransAddnl = $scope.benefitPeriodTransPer;
  }

   $scope.changeWaitingPeriod = function() {
     if(($scope.waitingPeriodTransPer == '14 Days') || ($scope.waitingPeriodTransPer == '30 Days')){
        $scope.waitingPeriodTransAddnl = '30 Days';
     }else  if(($scope.waitingPeriodTransPer == '45 Days') || ($scope.waitingPeriodTransPer == '60 Days')){
        $scope.waitingPeriodTransAddnl = '60 Days';
     }else if($scope.waitingPeriodTransPer == '90 Days'){
          $scope.waitingPeriodTransAddnl = '90 Days';
     } else if($scope.waitingPeriodTransPer == 'Not Listed'){
       $scope.waitingPeriodTransAddnl = '90 Days';
     }
     $scope.autoCalculate();
      };

      $scope.clickToOpen = function (hhText) {

      var dialog = ngDialog.open({
        /*template: '<p>'+hhText+'</p>' +
          '<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
            template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
          className: 'ngdialog-theme-plain',
          plain: true
      });
      dialog.closePromise.then(function (data) {
        //console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
      });
    };

    $scope.changeBenefitPeriod = function() {
    	/*$scope.ipOwnOccupation = false;
    	$scope.ipOwnOccButton = false;*/
        $timeout(function(){
      	  if(($scope.benefitPeriodTransPer == '2 Years')){
            $scope.benefitPeriodTransAddnl = '2 Years';
          } else if($scope.benefitPeriodTransPer == '5 Years'){
            $scope.benefitPeriodTransAddnl = '5 Years';
          } else if($scope.benefitPeriodTransPer == 'Not Listed'){
            $scope.benefitPeriodTransAddnl = '2 Years';
          } else if(($scope.benefitPeriodTransPer == 'Age 65') || ($scope.benefitPeriodTransPer == 'Age 67') || ($scope.benefitPeriodTransPer == 'Age 70')){
            $scope.benefitPeriodTransAddnl = 'Age 65';
          } else if($scope.benefitPeriodTransPer == 'Age 60'){
            $scope.benefitPeriodTransAddnl = '5 Years';
          }
      	  
      	 /* if($scope.benefitPeriodTransAddnl === '5 Years' || $scope.benefitPeriodTransAddnl === 'Age 65')
      		  {
      		  $scope.ipOwnOccupation = true;
      		  if($scope.ipCoverTransDetails.occRating.toLowerCase() == ownoccupation)
      			{
      			  $scope.ownOccuptionIp = 'Yes';
      			}
      		  else
      		  	{
      			  $scope.ownOccuptionIp = 'No';
      		  	}
      		  if($scope.finalRating!=1)
      			  {
      			  	$scope.ipOwnOccButton = true;
      			  }
      		  }
      	  */
      	$scope.renderOccupationQuestions();
          $scope.autoCalculate();
        }, 10);
      };  

/* $scope.toggleOwnOccupation = function(val) {
  		$scope.ownOccuptionIp = 'No';
  		var val1 = "Yes";
  		if (val === 'Yes' )
  		{
  			$scope.ownOccuptionIp = 'Yes';
  			val1="No";
  		}
  		var radios = $('label[radio-sync]');
      	var data = $('input[data-sync]');
  		data.filter('[data-sync="' + val + '"]').attr('checked','checked');
  		radios.filter('[radio-sync="' + val1 + '"]').removeClass('active');
  		radios.filter('[radio-sync="' + val + '"]').addClass('active');
  		$scope.autoCalculate();
  };*/
      
      
      
          $scope.transferAckFlag = false;
        var transferAckCheck;

        $scope.checkTransferAckState = function(){
            $timeout(function(){
              transferAckCheck = $('#transferTermsLabel').hasClass('active');
              if(transferAckCheck){
                $scope.transferAckFlag = false;
              } else{
                $scope.transferAckFlag = true;
              }
            }, 10);
          };
          var ackDocument;
          $scope.saveDataForPersistence = function(){
            var defer = $q.defer();
            var coverObj = {};
            var coverStateObj = {};
            var transferOccObj={};
            var transferDeathAddnlObj={};
            var transferTpdAddnlObj={};
            var transferIpAddnlObj={};
           /* var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
              return $scope.transferIndustry == obj.key;
            });*/


                  coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
                  coverObj['firstName'] = $scope.personalDetails.firstName;
                  coverObj['lastName'] = $scope.personalDetails.lastName;
                  coverObj['dob'] = $scope.personalDetails.dateOfBirth;
                  coverObj['country'] =persoanlDetailService.getMemberDetails().address.country;
                  coverObj['email'] = $scope.transferEmail;
                  transferOccObj['gender'] = $scope.gender;
                  coverObj['contactType']=$scope.preferredContactType;
                  coverObj['contactPhone'] = $scope.transferPhone;
                  coverObj['contactPrefTime'] = $scope.TransferTime;

                  transferOccObj['fifteenHr'] = $scope.fifteenHrsTransferQuestion;
                  transferOccObj['citizenQue'] = $scope.areyouperCitzTransferQuestion;
                  transferOccObj['occRating1a'] = $scope.occRating1a;
                  transferOccObj['occRating1b'] = $scope.occRating1b;
                  transferOccObj['occRating2'] = $scope.occRating2;
                  /*transferOccObj['industryName'] = selectedIndustry[0].value;
                  transferOccObj['industryCode'] = selectedIndustry[0].key;
                  transferOccObj['occupation'] = $scope.occupationTransfer;
                  if( !($scope.showWithinOfficeTransferQuestion && $scope.showTertiaryTransferQuestion))
                	  {
                	  $scope.withinOfficeTransferQuestion = null;
                	  $scope.tertiaryTransferQuestion = null;
                	  }
                  if(!$scope.showHazardousTransferQuestion && !$scope.showOutsideOffice)
                	  {
                	  $scope.outsideOffice = null;
                	  $scope.hazardousTransferQuestion = null;
                	  }
                  transferOccObj['withinOfficeQue']= $scope.withinOfficeTransferQuestion;
                  transferOccObj['tertiaryQue']= $scope.tertiaryTransferQuestion;
                  
                  transferOccObj['managementRoleQue']= $scope.outsideOffice;
                  transferOccObj['hazardousQue']= $scope.hazardousTransferQuestion;*/
                  transferOccObj['salary'] = $scope.annualTransferSalary;
                  /*transferOccObj['otherOccupation'] = $scope.otherOccupationObj.transferotherOccupation;*/

                  coverObj['previousFundName'] = $scope.previousFundName;
                  coverObj['membershipNumber'] = $scope.membershipNumber;
                  coverObj['spinNumber'] = $scope.spinNumber;
                  /*coverObj['documentName'] = $scope.documentName;*/
                //  coverObj['uploadedDocument'] = $scope.selectedFile.name;
                  /*ackDocument = $('#acknowledgeDocAdressCheck').hasClass('active');
                  if(ackDocument){
                	coverObj['documentAck2'] = "Yes";
                    coverObj['documentAddress'] = "Postal address:\n\VICSUPER,\nLocked Bag 5046,\nParramatta,\nNSW 2124";
                    
                  }*/
                //  coverObj['documentAddressCheckbox'] =acknowledgeDocAdressCheckState;
                  /*coverObj['previousTpdClaimQue'] = $scope.tpdClaimTrans;
                  coverObj['terminalIllClaimQue'] = $scope.terminalIllnessClaimTrans;*/

                  coverObj['transferDeathExistingAmt'] = $scope.deathCoverTransDetails.amount;
                  coverObj['transferTpdExistingAmt'] = $scope.tpdCoverTransDetails.amount;
                  
                  
                  coverObj['indexationDeath'] =  $scope.changeDeathReqCover;
                  coverObj['indexationTpd'] =  $scope.changeTPDReqCover;
                  
                  if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
              	{
                  coverObj['existingDeathUnits'] = $scope.deathCoverTransDetails.units;
                  coverObj['existingTPDUnits'] = $scope.tpdCoverTransDetails.units;
              	}
                  coverObj['deathOccCategory'] = $scope.transferDeathOccupationCategory;
                  coverObj['tpdOccCategory'] = $scope.transferTpdOccupationCategory;
                  coverObj['ipOccCategory'] = $scope.transferIpOccupationCategory;
                  coverObj['transferIpExistingAmt'] = $scope.ipCoverTransDetails.amount;
                  
                  /*coverObj['transferIpWaitingPeriod'] = $scope.waitingPeriodTransPer;
                  coverObj['transferIpBenefitPeriod'] = $scope.benefitPeriodTransPer;*/
                  
                  coverObj['addtionalDeathTransfer'] = $scope.addtionalDeathTransfer;
                  coverObj['addtionalTPDTransfer'] = $scope.addtionalTPDTransfer;
                  
                  
                  
                  transferDeathAddnlObj['deathTransferAmt'] = $scope.TransDeathRequireCover!=null?$scope.TransDeathRequireCover:0;
                  transferDeathAddnlObj['deathTransferCovrAmt'] = parseFloat($scope.dcTransCoverAmount);
                  transferDeathAddnlObj['deathTransferWeeklyCost'] = parseFloat($scope.dcTransCost);
                  /*if($scope.deathCoverTransDetails.type == "1" && $scope.tpdCoverTransDetails.type == "1")
            	  {
                	  transferDeathAddnlObj['deathTransferCoverType'] = 'DcUnitised';
                	  transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDUnitised';
                	  transferDeathAddnlObj['deathTransferUnits'] = $scope.deathUnitsForTransfer;
                	  transferTpdAddnlObj['tpdTransferUnits'] = $scope.tpdUnitsForTransfer;
            	  }
                  else
            	  {
            		  transferDeathAddnlObj['deathTransferCoverType'] = 'DcFixed';
            		  transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDFixed';
            	  }*/
            		  
            	if($scope.transferUnits)
            		{
            		transferDeathAddnlObj['deathTransferCoverType'] = 'DcUnitised';
          		  	transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDUnitised';
            		}
            	else
            		{
            		transferDeathAddnlObj['deathTransferCoverType'] = 'DcFixed';
          		  transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDFixed';
            		}

                  transferTpdAddnlObj['tpdTransferAmt'] = $scope.TransTPDRequireCover!=null?$scope.TransTPDRequireCover:0; 
                  transferTpdAddnlObj['tpdTransferCovrAmt'] = parseFloat($scope.tpdTransCoverAmount);
                  transferTpdAddnlObj['tpdTransferWeeklyCost'] = parseFloat($scope.tpdTransCost);
                  //transferTpdAddnlObj['tpdTransferCoverType'] = 'TPDFixed';


                  transferIpAddnlObj['ipTransferAmt'] = $scope.TransIPRequireCover!=null?$scope.TransIPRequireCover:0;                  
                  transferIpAddnlObj['ipTransferCovrAmt'] =parseFloat($scope.ipTransCoverAmount);
                  transferIpAddnlObj['ipAddnlUnits'] =parseInt($scope.ipUnitcalculated);
                  transferIpAddnlObj['ipTransferWeeklyCost'] = parseFloat($scope.ipTransCost);
                  
                  transferIpAddnlObj['addnlTransferWaitingPeriod'] = $scope.waitingPeriodTransPer;
                  transferIpAddnlObj['addnlTransferBenefitPeriod'] = $scope.benefitPeriodTransPer;
                  transferIpAddnlObj['totalipwaitingperiod'] = $scope.waitingPeriodTransAddnl;
                  transferIpAddnlObj['totalipbenefitperiod'] = $scope.benefitPeriodTransAddnl;
                  
                  transferIpAddnlObj['ipTransferCoverType'] = 'IpUnitised';

                  coverObj['totalPremium']=parseFloat($scope.totalTransCost);
                  coverObj['autoCalculateFlag']=autoCalculate;
                  coverObj['appNum'] = parseInt(appNum);
                  coverObj['transferAckCheck'] = transferAckCheck;
                  coverObj['lastSavedOn'] = 'Transferpage';
                  coverObj['manageType'] = 'TCOVER';
	              coverObj['partnerCode'] = 'VICT';
	              coverObj['freqCostType'] = $scope.premTransFreq;
	              coverObj['age'] = anb;
	              coverObj['dodCheck'] = $('#dodCkBoxLblId').hasClass('active');
	              coverObj['privacyCheck'] = $('#privacyCkBoxLblId').hasClass('active');
	              coverObj['ownOccuptionDeath'] = $scope.ownOccuptionDeath;
	              coverObj['ownOccuptionTpd'] = $scope.ownOccuptionTpd;
	              //coverObj['unitisedCovers'] = unitCheck;

	             /* coverStateObj['showWithinOfficeTransferQuestion'] = $scope.showWithinOfficeTransferQuestion;
	              coverStateObj['showTertiaryTransferQuestion'] =  $scope.showTertiaryTransferQuestion;
	              coverStateObj['showHazardousTransferQuestion'] = $scope.showHazardousTransferQuestion;
	              coverStateObj['showOutsideOffice'] = $scope.showOutsideOffice;*/
	              /*uploaded transfer documents*/
	              $scope.submitFiles().then(function(res) {
                  PersistenceService.settransferCoverDetails(coverObj);
                  PersistenceService.settransferCoverStateDetails(coverStateObj);
                  PersistenceService.setTransferCoverOccDetails(transferOccObj);
                  PersistenceService.setTransferDeathAddnlDetails(transferDeathAddnlObj);
                  PersistenceService.setTransferTpdAddnlDetails(transferTpdAddnlObj);
                  PersistenceService.setTransferIpAddnlDetails(transferIpAddnlObj);
                  defer.resolve(res);
                }, function(err) {
                  defer.reject(err);
                });
                return defer.promise;
          };

          if($routeParams.mode == 2){
            var existingDetails = PersistenceService.gettransferCoverDetails();
            var stateDetails = PersistenceService.gettransferCoverStateDetails();
            var occDetails =PersistenceService.getTransferCoverOccDetails();
            var deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
            var tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
            var ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();

            if(!existingDetails || !occDetails || !deathAddnlCvrDetails || !tpdAddnlCvrDetails || !ipAddnlCvrDetails) {
              $location.path("/quotetransfer/1");
              return false;
            }

            $scope.transferEmail = existingDetails.email;
            $scope.preferredContactType = existingDetails.contactType;
            $scope.transferPhone = existingDetails.contactPhone;
            $scope.TransferTime = existingDetails.contactPrefTime;
            $scope.premTransFreq = existingDetails.freqCostType;

            $scope.fifteenHrsTransferQuestion = occDetails.fifteenHr;
            $scope.areyouperCitzTransferQuestion = occDetails.citizenQue;
            $scope.occRating1a = occDetails.occRating1a;
	    	$scope.occRating1b = occDetails.occRating1b;
	    	$scope.occRating2 = occDetails.occRating2;
           /* $scope.transferIndustry = occDetails.industryCode;
            //$scope.occupationTransfer = occDetails.occupation;
            $scope.withinOfficeTransferQuestion = occDetails.withinOfficeQue;
            $scope.tertiaryTransferQuestion = occDetails.tertiaryQue;
            $scope.hazardousTransferQuestion = occDetails.hazardousQue;
            $scope.outsideOffice = occDetails.managementRoleQue;*/
            $scope.annualTransferSalary = occDetails.salary;
            $scope.gender = occDetails.gender;
            /*$scope.otherOccupationObj.transferotherOccupation = occDetails.otherOccupation;*/

            $scope.previousFundName=existingDetails.previousFundName;
            $scope.membershipNumber=existingDetails.membershipNumber;
            $scope.spinNumber=existingDetails.spinNumber;
            /*$scope.documentName=existingDetails.documentName;*/
          //  $scope.selectedFile.name=existingDetails.uploadedDocument;
            ackDocument=existingDetails.documentAddress;
          //  $scope.acknowledgeDocAdressCheckState=existingDetails.documentAddressCheckbox;
            /*$scope.tpdClaimTrans=existingDetails.previousTpdClaimQue;
            $scope.terminalIllnessClaimTrans=existingDetails.terminalIllClaimQue;*/
            
           /* if(existingDetails.documentName === 'No'){
            	$scope.ackDocument2 = true;
            }*/
            
            $scope.ownOccuptionDeath = existingDetails.ownOccuptionDeath;
			$scope.ownOccuptionTpd = existingDetails.ownOccuptionTpd;

            $scope.deathCoverTransDetails.amount=existingDetails.transferDeathExistingAmt;
            $scope.transferDeathOccupationCategory=existingDetails.deathOccCategory;
            $scope.transferTpdOccupationCategory=existingDetails.tpdOccCategory;
            $scope.transferIpOccupationCategory=existingDetails.ipOccCategory;
            $scope.tpdCoverTransDetails.amount=existingDetails.transferTpdExistingAmt;
            $scope.ipCoverTransDetails.amount=existingDetails.transferIpExistingAmt;
            /*$scope.waitingPeriodTransPer=existingDetails.transferIpWaitingPeriod;
            $scope.benefitPeriodTransPer=existingDetails.transferIpBenefitPeriod;*/

            $scope.TransDeathRequireCover=deathAddnlCvrDetails.deathTransferAmt;
            $scope.dcTransCoverAmount=deathAddnlCvrDetails.deathTransferCovrAmt;
            $scope.dcTransCost=deathAddnlCvrDetails.deathTransferWeeklyCost;

            $scope.TransTPDRequireCover=tpdAddnlCvrDetails.tpdTransferAmt;
            $scope.tpdTransCoverAmount=tpdAddnlCvrDetails.tpdTransferCovrAmt;
            $scope.tpdTransCost=tpdAddnlCvrDetails.tpdTransferWeeklyCost;


            $scope.TransIPRequireCover=ipAddnlCvrDetails.ipTransferAmt;
            $scope.ipTransCoverAmount=ipAddnlCvrDetails.ipTransferCovrAmt;
            $scope.ipTransCost=ipAddnlCvrDetails.ipTransferWeeklyCost;
            $scope.ipUnitcalculated = ipAddnlCvrDetails.ipAddnlUnits;
            
            $scope.waitingPeriodTransPer=ipAddnlCvrDetails.addnlTransferWaitingPeriod;
            $scope.benefitPeriodTransPer=ipAddnlCvrDetails.addnlTransferBenefitPeriod;
            $scope.waitingPeriodTransAddnl=ipAddnlCvrDetails.totalipwaitingperiod;
            $scope.benefitPeriodTransAddnl=ipAddnlCvrDetails.totalipbenefitperiod;

           /* $scope.showWithinOfficeTransferQuestion = stateDetails.showWithinOfficeTransferQuestion;
            $scope.showTertiaryTransferQuestion =  stateDetails.showTertiaryTransferQuestion;
            $scope.showHazardousTransferQuestion = stateDetails.showHazardousTransferQuestion;
            $scope.showOutsideOffice = stateDetails.showOutsideOffice;*/
            
            $scope.addtionalDeathTransfer = existingDetails.addtionalDeathTransfer;
            $scope.addtionalTPDTransfer = existingDetails.addtionalTPDTransfer;
            
            if($scope.addtionalDeathTransfer === 'Yes')
   		 		{
            		$scope.changeDeathReqCover = false;
   		 		}
            else if($scope.addtionalDeathTransfer === 'No')
   		 		{
            		$scope.changeDeathReqCover = true;
   		 		}
            
            if($scope.addtionalTPDTransfer === 'Yes')
   		 		{
            		$scope.changeTPDReqCover = false;
   		 		}
            else if($scope.addtionalTPDTransfer === 'No')
   		 		{
            		$scope.changeTPDReqCover = true;
   		 		}
            
            
            $scope.totalTransCost=existingDetails.totalPremium;
            autoCalculate=existingDetails.autoCalculateFlag;
            appNum = existingDetails.appNum;
            transferAckCheck = existingDetails.transferAckCheck;
            ackCheck = existingDetails.ackCheck;
            dodCheck = existingDetails.dodCheck;
            privacyCheck = existingDetails.privacyCheck;
            //unitCheck = existingDetails.unitisedCovers;
            $scope.files = PersistenceService.getUploadedFileDetails();
            $scope.uploadedFiles = $scope.files;

           /* OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.transferIndustry).then(function(res){
              //OccupationService.getOccupationList({fundId:"HOST", induCode:$scope.transferIndustry}, function(occupationList){
              $scope.OccupationList = res.data;
              var temp = $scope.OccupationList.filter(function(obj){
                return obj.occupationName == occDetails.occupation;
              });
              $scope.occupationTransfer = temp[0].occupationName;
              $scope.getTransCategoryFromDB(false);
            }, function(err){
              //console.log("Error while getting occupatio list " + JSON.stringify(err));
            });*/
           
            if((parseInt($scope.ipTransCoverAmount) > 0 && parseInt($scope.TransIPRequireCover) == 0)
	    			|| (parseInt($scope.ipTransCoverAmount) == 0 && parseInt($scope.ipCoverTransDetails.amount) == 0)
	    			|| ($scope.fifteenHrsTransferQuestion).toLowerCase() === 'no')
	    		{
            	$scope.TransIPRequireCover = null;
	    		}
            $scope.renderOccupationQuestions();
          /*  if($scope.benefitPeriodTransAddnl === '5 Years' || $scope.benefitPeriodTransAddnl === 'Age 65')
			{
            	$scope.ipOwnOccupation = true;
        		  if($scope.ipCoverTransDetails.occRating.toLowerCase() == ownoccupation)
        			{
        			  $scope.ownOccuptionIp = 'Yes';
        			}
        		  else
        		  	{
        			  $scope.ownOccuptionIp = 'No';
        		  	}
            
            }
            */
            if(transferAckCheck){
                $timeout(function(){
                $('#transferTermsLabel').addClass('active');
                   });
                 }
            /*if(acknowledgeDocAdressCheckState){
              $('#acknowledgeDocAdressCheck').parent().addClass('active');
            }*/

            if(ackDocument){
              $timeout(function(){
              $('#acknowledgeDocAdressCheck').addClass('active');
              });
              }

            if(dodCheck){
                $timeout(function(){
                $('#dodCkBoxLblId').addClass('active');
             });
           }
            if(privacyCheck){
                $timeout(function(){
                $('#privacyCkBoxLblId').addClass('active');
             });
           }
            /*if(unitCheck){
              $('#equivalentUnit').addClass('active');
            }*/
            if($scope.TransDeathRequireCover == 0)
            	{
            	$scope.TransDeathRequireCover = null;
            	}
            if($scope.TransTPDRequireCover == 0)
            	{
            	$scope.TransTPDRequireCover= null;
            	}
            if($scope.TransIPRequireCover == 0)
            	{
            	$scope.TransIPRequireCover = null;
            	}
            $scope.togglePrivacy(true);
            $scope.toggleContact(true);
            $scope.toggleTwo(true);
            $scope.toggleThree(true);
            $scope.toggleFour(true);
           
            

          };

          if($routeParams.mode == 3){
             mode3Flag = true;
             var num = PersistenceService.getAppNumToBeRetrieved();
             RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
              var appDetails = res.data[0];

              $scope.transferEmail = appDetails.email;
              $scope.preferredContactType = appDetails.contactType;
              $scope.transferPhone = appDetails.contactPhone;
              $scope.TransferTime = appDetails.contactPrefTime;
              $scope.premTransFreq = appDetails.freqCostType;
              $scope.files = appDetails.transferDocuments;
              $scope.uploadedFiles = $scope.files;

              $scope.fifteenHrsTransferQuestion = appDetails.occupationDetails.fifteenHr;
              $scope.areyouperCitzTransferQuestion = appDetails.occupationDetails.citizenQue;
              $scope.occRating1a = appDetails.occupationDetails.occRating1a;
  	    	  $scope.occRating1b = appDetails.occupationDetails.occRating1b;
  	    	  $scope.occRating2 = appDetails.occupationDetails.occRating2;
              /*$scope.transferIndustry = appDetails.occupationDetails.industryCode;*/
              //$scope.occupationTransfer = occupationDetails.occupation;
             /* $scope.withinOfficeTransferQuestion = appDetails.occupationDetails.withinOfficeQue;
              $scope.tertiaryTransferQuestion = appDetails.occupationDetails.tertiaryQue;
              $scope.hazardousTransferQuestion = appDetails.occupationDetails.hazardousQue;
              $scope.outsideOffice = appDetails.occupationDetails.managementRoleQue;*/
              $scope.annualTransferSalary = appDetails.occupationDetails.salary;
              $scope.gender = appDetails.occupationDetails.gender;
              /*$scope.otherOccupationObj.transferotherOccupation = appDetails.occupationDetails.otherOccupation;*/

              $scope.previousFundName=appDetails.previousFundName;
              $scope.membershipNumber=appDetails.membershipNumber;
              $scope.spinNumber=appDetails.spinNumber;
             /* $scope.documentName=appDetails.documentName;
              if(appDetails.documentName == "No"){
                $scope.ackDocument2 = true;
              }*/
            //  $scope.selectedFile.name=appDetails.uploadedDocument;
              ackDocument=appDetails.documentAddress;
            //  $scope.acknowledgeDocAdressCheckState=appDetails.documentAddressCheckbox;
              /*$scope.tpdClaimTrans=appDetails.previousTpdClaimQue;
              $scope.terminalIllnessClaimTrans=appDetails.terminalIllClaimQue;*/

              $scope.ownOccuptionDeath = appDetails.ownOccuptionDeath;
  			  $scope.ownOccuptionTpd = appDetails.ownOccuptionTpd;
              
  			  $scope.deathCoverTransDetails.amount=appDetails.existingDeathAmt;
              $scope.transferDeathOccupationCategory=appDetails.deathOccCategory;
              $scope.transferTpdOccupationCategory=appDetails.tpdOccCategory;
              $scope.transferIpOccupationCategory=appDetails.ipOccCategory;
              $scope.tpdCoverTransDetails.amount=appDetails.existingTpdAmt;
              $scope.ipCoverTransDetails.amount=appDetails.existingIPAmount;
              /*$scope.waitingPeriodTransPer=appDetails.transferIpWaitingPeriod;
              $scope.benefitPeriodTransPer=appDetails.transferIpBenefitPeriod;*/

              $scope.TransDeathRequireCover=appDetails.addnlDeathCoverDetails.deathTransferAmt;
              $scope.dcTransCoverAmount=appDetails.addnlDeathCoverDetails.deathTransferCovrAmt;
              $scope.dcTransCost=appDetails.addnlDeathCoverDetails.deathTransferWeeklyCost;

              $scope.TransTPDRequireCover=appDetails.addnlTpdCoverDetails.tpdTransferAmt;
              $scope.tpdTransCoverAmount=appDetails.addnlTpdCoverDetails.tpdTransferCovrAmt;
              $scope.tpdTransCost=appDetails.addnlTpdCoverDetails.tpdTransferWeeklyCost;


              $scope.TransIPRequireCover=appDetails.addnlIpCoverDetails.ipTransferAmt;
              $scope.ipTransCoverAmount=appDetails.addnlIpCoverDetails.ipTransferCovrAmt;
              $scope.ipTransCost=appDetails.addnlIpCoverDetails.ipTransferWeeklyCost;
              $scope.waitingPeriodTransPer=appDetails.addnlIpCoverDetails.addnlTransferWaitingPeriod;
              $scope.benefitPeriodTransPer=appDetails.addnlIpCoverDetails.addnlTransferBenefitPeriod;
              $scope.waitingPeriodTransAddnl=appDetails.addnlIpCoverDetails.totalipwaitingperiod;
              $scope.benefitPeriodTransAddnl=appDetails.addnlIpCoverDetails.totalipbenefitperiod;
              $scope.ipUnitcalculated = appDetails.addnlIpCoverDetails.ipAddnlUnits;
              $scope.deathUnitsForTransfer = appDetails.addnlDeathCoverDetails.deathTransferUnits||0;
              $scope.tpdUnitsForTransfer = appDetails.addnlTpdCoverDetails.tpdTransferUnits||0;
              
              $scope.totalTransCost=appDetails.totalPremium;
              autoCalculate=true;
              appNum = appDetails.appNum;
              transferAckCheck = appDetails.transferAckCheck;
              ackCheck = appDetails.ackCheck;
              dodCheck = appDetails.dodCheck;
              privacyCheck = appDetails.privacyCheck;
             /* $scope.waitingPeriodTransPer = '30 Days';
              $scope.benefitPeriodTransPer = '2 Years';*/
              $scope.changeDeathReqCover = appDetails.indexationDeath ==='true' ? true : false;
              $scope.changeTPDReqCover = appDetails.indexationTpd ==='true' ? true : false;
              
              $scope.addtionalDeathTransfer = $scope.changeDeathReqCover ? "No" : "Yes";
              $scope.addtionalTPDTransfer = $scope.changeTPDReqCover ? "No" : "Yes";
              /*if($scope.changeDeathReqCover)
            	  {
            	  $scope.addtionalDeathTransfer = "No";
            	  }
              else
            	  {
            	  $scope.addtionalDeathTransfer = "Yes";
            	  }
              
              if($scope.changeTPDReqCover)
        	  	{
            	  $scope.addtionalTPDTransfer = "No";
        	  	}
              else
              	{
            	  $scope.addtionalTPDTransfer = "Yes";
              	}*/
              

              /*OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.transferIndustry).then(function(res){
              //OccupationService.getOccupationList({fundId:"HOST", induCode:$scope.transferIndustry}, function(occupationList){
                  $scope.OccupationList = res.data;
                  var temp = $scope.OccupationList.filter(function(obj){
                    return obj.occupationName == appDetails.occupationDetails.occupation;
                  });
                  $scope.occupationTransfer = temp[0].occupationName;
                  $scope.renderOccupationQuestions();
                  $scope.getTransCategoryFromDB(false);
                }, function(err){
                  //console.log("Error while getting occupatio list " + JSON.stringify(err));
                });*/
              
              if((parseInt($scope.ipTransCoverAmount) > 0 && parseInt($scope.TransIPRequireCover) == 0)
  	    			|| (parseInt($scope.ipTransCoverAmount) == 0 && parseInt($scope.ipCoverTransDetails.amount) == 0)
  	    			|| ($scope.fifteenHrsTransferQuestion).toLowerCase() === 'no')
  	    		{
              	$scope.TransIPRequireCover = null;
  	    		}
              $scope.renderOccupationQuestions();
              /*if(parseFloat($scope.ipCoverTransDetails.amount) > 0.0){
                $scope.displayExistingWpBp = true;
              } else{
                $scope.displayExistingWpBp = false;
              }*/

              if(transferAckCheck){
                  $timeout(function(){
                  $('#transferTermsLabel').addClass('active');
                     });
                   }
              /*if(acknowledgeDocAdressCheckState){
                $('#acknowledgeDocAdressCheck').parent().addClass('active');
              }*/

              if($scope.ackDocument2){
                $timeout(function(){
                $('#acknowledgeDocAdressCheck').addClass('active');
                });
                }


                

              $('#dodCkBoxLblId').addClass('active');
              $('#privacyCkBoxLblId').addClass('active');

              $scope.togglePrivacy(true);
              $scope.toggleContact(true);
              $scope.toggleTwo(true);
              $scope.toggleThree(true);
              $scope.toggleFour(true);
              
              if(!$scope.isDeathDisabled)
            	  {
            	  $scope.validateDeathMaxAmount();
            	  }
              if(!$scope.isTPDDisabled)
            	  {
            	  $scope.validateTpdMaxAmount();
            	  }
              if(!$scope.isIPDisabled)
              	{
            	  $scope.validateIpMaxAmount();
              	}
              if($scope.TransDeathRequireCover == 0)
          	{
          	$scope.TransDeathRequireCover = null;
          	}
          if($scope.TransTPDRequireCover == 0)
          	{
          	$scope.TransTPDRequireCover= null;
          	}
          if($scope.TransIPRequireCover == 0)
          	{
          	$scope.TransIPRequireCover = null;
          	}

             },function(err){
                console.info("Error fetching the saved app details " + err);
             });
          }

          $scope.goToAura = function(){
            if(this.coverDetailsTransferForm.$valid && this.occupationDetailsTransferForm.$valid && this.previousCoverForm.$valid && this.TranscoverCalculatorForm.$valid){
              if((!$scope.coverAmtErrFlag) && !$scope.maxTotalTPDAmt && (!$scope.maxDeathErrorFlag) && !$scope.maxTotalDeathErrorFlag && !$scope.maxTpdErrorFlag && !$scope.maxTpdCoverErrorFlag){
              $timeout(function(){
                $scope.saveDataForPersistence().then(function() {
                    $scope.go('/auratransfer/1');
                }, function(err) {
                    //console.log(err);
                });
                // submit uploaded to server
                //$scope.submitFiles();
            }, 10);

	            }else{
	            	return false;
	            }
            }
          };

          $scope.invalidSalAmount = false;
          $scope.checkValidSalary = function(){
        	  $scope.invalidSalAmount = false;
            if(parseInt($scope.annualTransferSalary) > 1000000){
              $scope.invalidSalAmount = true;
            } 
            if(!$scope.invalidSalAmount)
            	{
            		$scope.validateIpMaxAmount();
            		$scope.renderOccupationQuestions();
            	}
          };
          $scope.saveQuoteTransfer = function() {
            $scope.saveDataForPersistence().then(function() {
              var quoteTransferObject =  PersistenceService.gettransferCoverDetails();
              var transferOccDetails =PersistenceService.getTransferCoverOccDetails();
              var deathAddnlTransferCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
              var tpdAddnlTransferCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
              var ipAddnlTransferCvrDetails=PersistenceService.getTransferIpAddnlDetails();
              var personalDetails = persoanlDetailService.getMemberDetails();
              var transferUploadedFiles = PersistenceService.getUploadedFileDetails();
              if(quoteTransferObject != null && transferOccDetails != null && deathAddnlTransferCvrDetails != null && tpdAddnlTransferCvrDetails != null &&
                  ipAddnlTransferCvrDetails != null && personalDetails != null) {
                var details = {};
                details.addnlDeathCoverDetails = deathAddnlTransferCvrDetails;
                details.addnlTpdCoverDetails = tpdAddnlTransferCvrDetails;
                details.addnlIpCoverDetails = ipAddnlTransferCvrDetails;
                details.occupationDetails = transferOccDetails;
                //added for uploaded file details
                if(transferUploadedFiles != null) {
                   details.transferDocuments = transferUploadedFiles;
                }
                var temp = angular.extend(quoteTransferObject,details);
                var saveQuoteTransferObject = angular.extend(temp, personalDetails);
                auraResponseService.setResponse(saveQuoteTransferObject);
                saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
                  //console.log(response.data);
                  $scope.transferQuoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
                },function(err) {
                  //console.log("Something went wrong while saving..."+JSON.stringify(err));
                });
              }
            }, function(err) {
                //console.log(err);
            });
          };

          $scope.transferQuoteSaveAndExitPopUp = function (hhText) {

          var dialog1 = ngDialog.open({
                template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
              className: 'ngdialog-theme-plain custom-width',
              preCloseCallback: function(value) {
                     var url = "/landing"
                     $location.path( url );
                     return true
              },
              plain: true
          });
          dialog1.closePromise.then(function (data) {
            //console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
          });
        };

        $scope.generatePDF = function(){
        	$scope.saveDataForPersistence().then(function() { 
            var quoteTransferObjectPrint =  PersistenceService.gettransferCoverDetails();
            var transferOccDetailsPrint =PersistenceService.getTransferCoverOccDetails();
            var deathAddnlTransferCvrDetailsPrint =PersistenceService.getTransferDeathAddnlDetails();
            var tpdAddnlTransferCvrDetailsPrint=PersistenceService.getTransferTpdAddnlDetails();
            var ipAddnlTransferCvrDetailsPrint=PersistenceService.getTransferIpAddnlDetails();
            var transferUploadedFiles = PersistenceService.getUploadedFileDetails();
              var personalDetailsPrint = persoanlDetailService.getMemberDetails();
              if(quoteTransferObjectPrint != null && transferOccDetailsPrint != null && deathAddnlTransferCvrDetailsPrint != null && tpdAddnlTransferCvrDetailsPrint != null &&
                  ipAddnlTransferCvrDetailsPrint != null && personalDetailsPrint != null){
                var details = {};
                details.addnlDeathCoverDetails = deathAddnlTransferCvrDetailsPrint;
                details.addnlTpdCoverDetails = tpdAddnlTransferCvrDetailsPrint;
                details.addnlIpCoverDetails = ipAddnlTransferCvrDetailsPrint;
                details.occupationDetails = transferOccDetailsPrint;
                if(transferUploadedFiles != null) {
                    details.transferDocuments = transferUploadedFiles;
                 }
                var temp = angular.extend(quoteTransferObjectPrint,details)
                  var printQuoteTransferObject = angular.extend(temp,personalDetailsPrint);
                auraResponseService.setResponse(printQuoteTransferObject);
                  printQuotePage.reqObj($scope.urlList.printQuotePage).then(function(response){
                    PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                  $scope.downloadPDF();
                  },function(err){
                    //console.log("Something went wrong while saving..."+JSON.stringify(err));
                  });
              }
        	}, function(err) {
                //console.log(err);
            });
        };
          $scope.downloadPDF = function(){
              var pdfLocation =null;
              var filename = null;
              var a = null;
            pdfLocation = PersistenceService.getPDFLocation();
            //console.log(pdfLocation+"pdfLocation");
            filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
            a = document.createElement("a");
              document.body.appendChild(a);
              DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
              //DownloadPDFService.download({file_name: pdfLocation}, function(res){
              if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
            	  var extension = res.data.response.type;
            	  extension = extension.substring(extension.lastIndexOf('/')+1);
            	  filename = filename+"."+extension;
                      window.navigator.msSaveBlob(res.data.response,filename);
                  }else{
                    var fileURL = URL.createObjectURL(res.data.response);
                    a.href = fileURL;
                    a.download = filename;
                    a.click();
                  }
            }, function(err){
              //console.log("Error downloading the PDF " + err);
            });
          };

          $scope.calculateForUnits = function() {
       	   
      	    var deathtotal = parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount);
      	    var tpdtotal = parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount);
      	  $scope.roundingIndDeath = false;
      	$scope.roundingIndTPD = false;
      	 $scope.ipNoReplace;
         if(typeof($scope.TransIPRequireCover) == "undefined" || $scope.TransIPRequireCover==''){
       	  $scope.ipNoReplace = 0;
         } else {
       	  $scope.ipNoReplace = $scope.TransIPRequireCover;
         }
      	    var ruleModel = {
      	            "age": anb,
      	            "fundCode": "VICT",
      	            "gender": $scope.gender,
      	            "deathOccCategory": $scope.transferDeathOccupationCategory,
      	            "tpdOccCategory": $scope.transferTpdOccupationCategory,
      	            "ipOccCategory": $scope.transferIpOccupationCategory,
      	            "manageType": "TCOVER",
      	            "deathCoverType": "DcUnitised",
      	            "tpdCoverType": "TPDUnitised",
      	            "ipCoverType": "IpFixed",
      	            "deathUnits": 1,
      	            "tpdUnits": 1,
      	            "premiumFrequency": $scope.premTransFreq,
      	            "ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
      	            "ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
      	            "deathFixedAmount": parseInt(deathtotal),
      	            "tpdFixedAmount": parseInt(tpdtotal),
      	            "ipTransferAmount": parseInt($scope.ipNoReplace),
      	            "ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
      	          };
      	   
      	    
      	    TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
      	    	var coverAmtPerUnit = 0;
      	    	var premium = res.data;
      	      
      	      for(var i = 0; i < premium.length; i++){
      	        if(premium[i].coverType == 'DcUnitised'){
      	        	coverAmtPerUnit = premium[i].coverAmount || 0;
      	          if(coverAmtPerUnit > 0)
      	        	  {
      	        	$scope.deathUnitsForTransfer = Math.floor(deathtotal/coverAmtPerUnit);
      	        	if($scope.deathUnitsForTransfer>0 && !(deathtotal%coverAmtPerUnit == 0))
      	        		{
      	        		$scope.deathUnitsForTransfer = $scope.deathUnitsForTransfer + 1;
      	        		$scope.roundingIndDeath = true;
      	        		}
      	        	  }
      	          else
      	        	  {
      	        	  $scope.deathUnitsForTransfer = 0;
      	        	  }
      	        	
      	        } else if(premium[i].coverType == 'TPDUnitised'){
      	        	coverAmtPerUnit = premium[i].coverAmount || 0;
      	        	if(coverAmtPerUnit > 0)
      	        		{
      	        		$scope.tpdUnitsForTransfer = Math.floor(tpdtotal/coverAmtPerUnit);
      	        		if($scope.tpdUnitsForTransfer > 0 && !(tpdtotal % coverAmtPerUnit == 0))
      	        			{
      	        			$scope.tpdUnitsForTransfer = $scope.tpdUnitsForTransfer + 1;
      	        			$scope.roundingIndTPD = true;
      	        			}
      	        		}
      	        	else
      	        		{
      	        		$scope.tpdUnitsForTransfer = 0;
      	        		}
      	        }
      	        
      	        $scope.calculateUnits(); 
      	        
      	      }
      	      //$scope.QPD.totalPremium = parseFloat($scope.QPD.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.QPD.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.QPD.addnlIpCoverDetails.ipCoverPremium);
      	    }, function(err){
      	      console.info("Something went wrong while calculating..." + JSON.stringify(err));
      	    });
      	  }; 

 $scope.changeRequiredDeathCover = function ()
 {
	 if($scope.addtionalDeathTransfer === 'Yes')
		 {
		 $scope.changeDeathReqCover = false;
		 }
	 else if($scope.addtionalDeathTransfer === 'No')
		 {
		 $scope.changeDeathReqCover = true;
		 }
	 $scope.validateDeathMaxAmount();
 };
 
 $scope.changeRequiredTPDCover = function ()
 {
	 if($scope.addtionalTPDTransfer === 'Yes')
		 {
		 $scope.changeTPDReqCover = false;
		 }
	 else if($scope.addtionalTPDTransfer === 'No')
		 {
		 $scope.changeTPDReqCover = true;
		 }
	 $scope.validateTpdMaxAmount();
 };
      	  
$scope.calculateUnits = function() {
			 $scope.ipNoReplace;
		     if(typeof($scope.TransIPRequireCover) == "undefined" || $scope.TransIPRequireCover==''){
		   	  $scope.ipNoReplace = 0;
		     } else {
		   	  $scope.ipNoReplace = $scope.TransIPRequireCover;
		     }
         	   
        	    var deathtotal = parseInt($scope.TransDeathRequireCover) + parseInt($scope.deathCoverTransDetails.amount);
        	    var tpdtotal = parseInt($scope.TransTPDRequireCover) + parseInt($scope.tpdCoverTransDetails.amount);
        	  var ruleModel = {
    	            "age": anb,
    	            "fundCode": "VICT",
    	            "gender": $scope.gender,
    	            "deathOccCategory": $scope.transferDeathOccupationCategory,
    	            "tpdOccCategory": $scope.transferTpdOccupationCategory,
    	            "ipOccCategory": $scope.transferIpOccupationCategory,
    	            "manageType": "TCOVER",
    	            "deathUnits": $scope.deathUnitsForTransfer,
    	            "deathFixedAmount": parseInt(deathtotal),
    	            "tpdUnits": $scope.tpdUnitsForTransfer,
    	            "tpdFixedAmount": parseInt(tpdtotal),
    	            "deathCoverType": "DcUnitised",
    	            "tpdCoverType": "TPDUnitised",
    	            "ipCoverType": "IpFixed",
    	            "premiumFrequency": $scope.premTransFreq,
    	            "ipWaitingPeriod": $scope.waitingPeriodTransAddnl,
    	            "ipBenefitPeriod": $scope.benefitPeriodTransAddnl,
    	            "ipTransferAmount": parseInt( $scope.ipNoReplace),
    	            "ipExistingAmount": parseInt($scope.ipCoverTransDetails.amount)
    	          };

TransferCalculateService.calculate(ruleModel,$scope.urlList.transferCalculateUrl).then(function(res){
    	      //TransferCalculateService.calculate({}, ruleModel, function(res){
    	        var premium = res.data;
    	        autoCalculate = true;
    	        for(var i = 0; i < premium.length; i++){
    	          if(premium[i].coverType == 'DcUnitised'){
    	            $scope.dcTransCoverAmount = premium[i].coverAmount;
    	            $scope.dcTransCost = premium[i].cost || 0;
    	          } else if(premium[i].coverType == 'TPDUnitised'){
    	            $scope.tpdTransCoverAmount = premium[i].coverAmount;
    	            $scope.tpdTransCost = premium[i].cost || 0;
    	          } else if(premium[i].coverType == 'IpFixed'){
    	            $scope.ipTransCoverAmount = premium[i].coverAmount||0.00;
    	            $scope.ipTransCost = premium[i].cost || 0;
    	          }
    	      }

    	        $scope.totalTransCost = parseFloat($scope.dcTransCost) + parseFloat($scope.tpdTransCost) + parseFloat($scope.ipTransCost);
    	        if(fetchAppnum){
    	          fetchAppnum = false;
    	          appNum = PersistenceService.getAppNumber();
    	        }
    	      }, function(err){
    	        //console.log("Error while calculating transfer premium " + JSON.stringify(err));
    	      });
        	  };
          
          
    }]);

 /* Transfer Cover Controller,Progressive and Mandatory validations Ends  */
