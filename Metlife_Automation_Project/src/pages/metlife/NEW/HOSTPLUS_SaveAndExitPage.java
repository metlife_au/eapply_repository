package pages.metlife.NEW;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_SaveAndExitPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_SaveAndExitPage HOSTPLUSSaveandExit() {
		SaveandExit();		
		
		
		return new HOSTPLUS_SaveAndExitPage(scriptHelper);
	}

	public HOSTPLUS_SaveAndExitPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void SaveandExit() {

		String EmailId = dataTable.getData("HOSTPLUS_SaveAndExit", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_SaveAndExit", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_SaveAndExit", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_SaveAndExit", "TimeofContact");
		
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_SaveAndExit", "FifteenHoursWork");
		
		String Citizen = dataTable.getData("HOSTPLUS_SaveAndExit", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_SaveAndExit", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_SaveAndExit", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_SaveAndExit", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_SaveAndExit", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_SaveAndExit", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_SaveAndExit", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_SaveAndExit", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_SaveAndExit", "AnnualSalary");
		
		
		try{
			
			
			
			//****Click on Update Your Occupation  Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Update Your Occupation')]"))).click();
			
			report.updateTestLog("Update Your Occupation", "Update Your Occupation Link is Clicked", Status.PASS);
			
            sleep(1500);
			
			
			if(driver.getPageSource().contains("Cancel Application")){
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("(//span[contains(text(),'Cancel Application')])[1]")).click();
				report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
				sleep(1000);
		
			}
		
			//*****Agree to Duty of disclosure*******\\
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dod_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			
           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='privacy_chckbox_id__xc_c']/span"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			
			//*****Enter the Email Id*****\\	
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).clear();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("emailId"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(200);

			//*****Click on the Contact Number Type****\\			
				
				if(!TypeofContact.equalsIgnoreCase("Mobile")){			
							
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Mobile')]"))).click();
				sleep(300);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'"+TypeofContact+"')])[1]"))).click();			
				report.updateTestLog("Contact Type", "Preferred contact: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);
				}
				
				
				//****Enter the Contact Number****\\			
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact NUmber", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
			
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					wait.until(ExpectedConditions.elementToBeClickable(By.id("mrngRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					wait.until(ExpectedConditions.elementToBeClickable(By.id("aftrNoonRadBtnId__xc_r"))).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}		
				
				
				
							
				//****Contact Details-Continue Button*****\\			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continueContDetlID']/span"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				sleep(1200);			
							
							
				//***************************************\\			
				//**********OCCUPATION SECTION***********\\			
				//****************************************\\			
							
					//*****Select the 15 Hours Question*******\\		
					
				if(FifteenHoursWork.equalsIgnoreCase("Yes")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhr_yesId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
							
					}		
				else{		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("fifteenhr_noId__xc_r"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);
							
					}	
					
					
					
					//*****Resident of Australia****\\	
					
					if(Citizen.equalsIgnoreCase("Yes")){		
						driver.findElement(By.id("valCitizenAus_yes_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
							
						}	
					else{	
						driver.findElement(By.id("valCitizenAus_no_id__xc_r")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
							
						}	
							
					//*****Industry********\\		
				
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@data-id='industryId']/span)[1]"))).click();
					sleep(800);				
					driver.findElement(By.xpath("//span[contains(text(),'"+IndustryType+"')]")).click();		
					sleep(800);		
					report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),' What is your occupation?')]"))).click();
					sleep(500);
					
					//*****Occupation*****\\		
					
					driver.findElement(By.xpath("(//button[@data-id='occupationId']/span)[1]")).click();
					//wait.until(ExpectedConditions.elementToBeClickable(By.id("(//button[@data-id='occupationId']/span)[2]"))).click();	
		            sleep(800);							
					driver.findElement(By.xpath("//span[contains(text(),'"+OccupationType+"')]")).click();		
					sleep(800);	
					report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
					sleep(500);
							
							
					//*****Other Occupation *****\\	
					
					if(OccupationType.equalsIgnoreCase("Other")){		
							
						wait.until(ExpectedConditions.elementToBeClickable(By.id("otherOccupationId"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);	
					}		
					
					
					//*****Extra Questions*****\\							
					
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
				   
				    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
											
				}							
				else{							

					wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
											
				}							
											
				      //*****Do You Hold a Tertiary Qualification******\\							
											
				        if(TertiaryQual.equalsIgnoreCase("Yes")){							
				       
				        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}							
				   else{							
				       
					   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}							
				}							
											
											
											
				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){							
											
											
											
				//******Do you work in hazardous environment*****\\		
					
				if(HazardousEnv.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_yes_id__xc_r"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("askmanualQuestion_no_id__xc_"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}							
											
				//******Do you spend 20% time working outside office*******\\	
				
				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_yes_id__xc_r"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}							
				else{							
				
					wait.until(ExpectedConditions.elementToBeClickable(By.id("spend_no_id__xc_"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}							
				}							
							
				    
                  sleep(800);
				    if(driver.getPageSource().contains("Are your duties entirely undertaken within an office environment?")){							
				    
											
				    	//***Select if your office Duties are Undertaken within an Office Environment?\\	
				    	
					    if(OfficeEnvironment.equalsIgnoreCase("Yes")){							
					   
					    	wait.until(ExpectedConditions.elementToBeClickable(By.id("work_yes_id__xc_r"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
												
					}							
					else{							

						wait.until(ExpectedConditions.elementToBeClickable(By.id("work_no_id__xc_r"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
												
					}							
												
					      //*****Do You Hold a Tertiary Qualification******\\							
												
					        if(TertiaryQual.equalsIgnoreCase("Yes")){							
					       
					        	wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_yes_id__xc_r"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					}							
					   else{							
					       
						   wait.until(ExpectedConditions.elementToBeClickable(By.id("teri_no_id__xc_r"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					}						
				}		
				    
											
				//*****What is your annual Salary******							
											
				
				    wait.until(ExpectedConditions.elementToBeClickable(By.id("annualSalId"))).sendKeys(AnnualSalary);
				    sleep(500);		
				    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
											
				//***Load the Salary Amount****\\	
				    				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Insert gross salary (i.e.your salary before tax)')]"))).click();
				sleep(1500);	
				
		
			//******Click on Save and Exit******\\
			
		   
			sleep(300);
			driver.findElement(By.xpath("(//span[contains(text(),'Save & Exit')])[1]")).click();
			report.updateTestLog("Continue", "Save and Exit button is Clicked", Status.PASS);
			sleep(2000);
			
			//****Fetch the Application No******\\
			
			quickSwitchWindows();
			sleep(800);
			String AppNo=driver.findElement(By.xpath("(//div[@class='col-sm-12']/strong)[2]")).getText();
			report.updateTestLog("Application Saved", AppNo+" is your application reference number", Status.PASS);
			
			//******Finish and Close Window******\\
			
			sleep(500);
			driver.findElement(By.xpath("(//span[contains(text(),'Finish & Close Window')])[1]")).click();
			report.updateTestLog("Continue", "Finish and Close button is clicked", Status.PASS);
			sleep(1500);

			 
			
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	
	
	}





