(function(angular) {
    'use strict';

    /* global channel, moment, auraId */
    angular
        .module('CorporateApp')
        .controller('coverCtrl', coverCtrl);
    coverCtrl.$inject = ['$scope', '$location', '$timeout', 'APP_CONSTANTS', 'inputDataService', 'fetchUrlSvc', 'fetchAppNumberSvc', 'fetchCategoryDetailsService', 'fetchQuoteSvc', 'fetchOccupationSvc', 'saveEapplyData', 'ngDialog', 'fetchTokenNumSvc', 'printPageService', 'DownloadPDFService', 'appData', '$q', 'fetchIndustryList'];

    function coverCtrl($scope, $location, $timeout, APP_CONSTANTS, inputDataService, fetchUrlSvc, fetchAppNumberSvc, fetchCategoryDetailsService, fetchQuoteSvc, fetchOccupationSvc, saveEapplyData, ngDialog, fetchTokenNumSvc, printPageService, DownloadPDFService, appData, $q, fetchIndustryList) {
        var vm = this;
        vm.channel = channel || '';
        vm.coverDetails = inputDataService.getCoverDetails(); //personal details aftr upload
        vm.deathCover = parseInt(vm.coverDetails.eligibleDeathCover);
        vm.tpdCover = parseInt(vm.coverDetails.eligibleTpdCover);
        vm.ipCover = parseInt(vm.coverDetails.eligibleIpCover);
        vm.modelOptions = { updateOn: 'blur' };
        // Single data model under vm

        vm.getDataType = function(value) {
            var maps = {
                'NaN': NaN,
                'null': null,
                'undefined': undefined,
                'Infinity': Infinity,
                '-Infinity': -Infinity
            };
            return ((value in maps) ? maps[value] : value);
        };

        vm.coverData = {
            title: vm.coverDetails.brokerTitle || null,
            age: null,
            gender: vm.coverDetails.gender || null,
            email: vm.getDataType(vm.coverDetails.email) || null,
            dob: moment(vm.coverDetails.dob, 'DD/MM/YYYY').format('DD/MM/YYYY') || null,
            contactPrefTime: null,
            contactType: '1',
            clientRefNumber: vm.getDataType(vm.coverDetails.employeeNumber) || null,
            otherContactType: '3',
            appNum: null,
            existingDeathAmt: parseFloat(vm.coverDetails.totalDeathCover) || 0.00,
            existingTpdAmt: parseFloat(vm.coverDetails.totalTpdCover) || 0.00,
            existingIPAmount: parseFloat(vm.coverDetails.totalIpCover) || 0.00,
            ipcheckbox: null,
            contactDetails: {
                fundEmailAddress: vm.coverDetails.brokerEmail || null,
                fundBrokerNum: vm.coverDetails.brokerCnctNum || null
            },
            personalDetails: {
                gender: vm.coverDetails.gender || null,
                firstName: vm.coverDetails.firstName || null,
                lastName: vm.coverDetails.surname || null
            },
            occupationDetails: {
                industryCode: null,
                salary: parseInt(vm.coverDetails.annualSalary) || null,
                otherOccupation: null,
                occupation: null,
                gender: vm.coverDetails.gender || null
            },
            addnlDeathCoverDetails: {
                deathCoverType: 'DcFixed',
                deathFixedAmt: parseInt(vm.coverDetails.eligibleDeathCover) || 0,
                deathInputTextValue: parseInt(vm.coverDetails.eligibleDeathCover) || 0
            },
            addnlTpdCoverDetails: {
                tpdCoverType: 'TPDFixed',
                tpdFixedAmt: parseInt(vm.coverDetails.eligibleTpdCover) || 0,
                tpdInputTextValue: parseInt(vm.coverDetails.eligibleTpdCover) || 0
            },
            addnlIpCoverDetails: {
                ipCoverType: 'IpFixed',
                ipFixedAmt: parseInt(vm.coverDetails.eligibleIpCover) || 0,
                ipInputTextValue: parseInt(vm.coverDetails.eligibleIpCover) || 0,
                waitingPeriod: '',
                benefitPeriod: ''
            },
            address: {},
            manageType: 'CCOVER',
            partnerCode: 'CORP',
            lastSavedOn: 'Quotepage',
            fulCalculation: null,
            dodCheck: null,
            fulCheck: null,
            auraSessionId: auraId || null,
            auraDisabled: false,
            partnerName: '',
            productnumber: vm.coverDetails.membershipCategory
        };
        // Variables for data manipulation

        vm.constants = {
            image: APP_CONSTANTS.path.images,
            phoneNumber: APP_CONSTANTS.onboardDetails.phoneNumber,
            emailFormat: APP_CONSTANTS.onboardDetails.emailFormat,
            postCodeFormat: APP_CONSTANTS.onboardDetails.postCodeFormat,
            waitingPeriodOptions: APP_CONSTANTS.onboardDetails.waitingPeriodOptions,
            benefitPeriodOptions: APP_CONSTANTS.onboardDetails.benefitPeriodOptions,
            contactTypeOption: APP_CONSTANTS.onboardDetails.contactTypeOption,
            titleOption: APP_CONSTANTS.onboardDetails.titleOption,
            addressTypeOption: APP_CONSTANTS.onboardDetails.addressTypeOption,
            stateOptions: APP_CONSTANTS.onboardDetails.stateOptions
        };
        vm.coverData.appNum = fetchAppNumberSvc.getAppNumber();
        vm.token = fetchTokenNumSvc.getTokenId();
        vm.coverData.age = parseInt(moment().diff(moment(vm.coverDetails.dob, 'DD/MM/YYYY'), 'years')) + 1;
        vm.urlList = fetchUrlSvc.getUrlList();

        vm.init = function() {
            var defer = $q.defer();
            vm.deathDisabled = false;
            vm.tpdDisabled = false;
            vm.ipDisabled = false;
            vm.deathErrorFlag = false;
            vm.tpdErrorFlag = false;
            vm.ipErrorFlag = false;
            vm.invalidSalAmount = false;
            vm.saveDetails = appData.getAppData();
            if (vm.saveDetails !== null && vm.saveDetails !== undefined) {
                angular.extend(vm.coverData, vm.saveDetails);
            }

            if (vm.coverData.dodCheck) {
                $('#dodLabel').addClass('active');
            }

            if (vm.coverData.fulCheck === 'true' || vm.coverData.fulCheck) {
                $('.ful-calculation-yes').parent('button').addClass('active');
                $('.ful-calculation-no').parent('button').removeClass('active');
            }
            if (vm.coverData.fulCheck === 'false' || vm.coverData.fulCheck === false) {
                $('.ful-calculation-no').parent('button').addClass('active');
                $('.ful-calculation-yes').parent('button').removeClass('active');
            }

            $('input[value="' + vm.coverData.contactPrefTime + '"]').parent('label').addClass('active');
            $('input[value="' + vm.coverData.occupationDetails.gender + '"]').parent('label').addClass('active');


            vm.coverData.contactType = vm.coverData.contactType || '1';
            vm.coverData.otherContactType = vm.coverData.otherContactType || '3';
            vm.changePrefContactType();
            vm.changeOtherContactType();
            vm.servicefailureFlag = false;
            fetchCategoryDetailsService.getCategoryDetails(vm.urlList.fetchCategoryDetailUrl, vm.coverDetails.fundId, vm.coverDetails.membershipCategory).then(function(res) {
                var response = res.data;
                vm.categoryDetails = response;
                vm.coverData.partnerName = vm.categoryDetails.partner.partnerName;
                if (vm.categoryDetails.partner.fundCat[0].ipReqd === 'Y') {
                    vm.ipInsuredPercentage = parseFloat(vm.categoryDetails.partner.fundCat[0].ipCover.ipFormula);
                    vm.ipFormula = parseFloat(vm.categoryDetails.partner.fundCat[0].ipCover.ipFormula / 100).toFixed(2);
                    vm.ipMax = parseInt(vm.categoryDetails.partner.fundCat[0].ipCover.ipMaxSumIns);
                    vm.coverData.addnlIpCoverDetails.waitingPeriod = vm.categoryDetails.partner.fundCat[0].ipCover.ipWp;
                    vm.coverData.addnlIpCoverDetails.benefitPeriod = vm.categoryDetails.partner.fundCat[0].ipCover.ipBp;
                }
                if ((vm.categoryDetails.partner.fundCat[0].deathReqd === 'Y' && ((parseInt(vm.coverData.age) < parseInt(vm.categoryDetails.partner.fundCat[0].deathCover.deathMinAge)) ||
                        (parseInt(vm.coverData.age) > parseInt(vm.categoryDetails.partner.fundCat[0].deathCover.deathMaxAge)))) ||
                    vm.categoryDetails.partner.fundCat[0].deathReqd === 'N') {
                    vm.deathDisabled = true;
                    $('#death').css('display', 'none');
                    if (vm.categoryDetails.partner.fundCat[0].deathReqd === 'N') {
                        vm.deathDisabledMsg = 'The fund does not have Death cover';
                    } else {
                        vm.deathDisabledMsg = 'You are not eligible for Death cover due to your age.';
                    }
                    vm.coverData.addnlDeathCoverDetails.deathFixedAmt = 0;
                }
                if ((vm.categoryDetails.partner.fundCat[0].tpdReqd === 'Y' && ((parseInt(vm.coverData.age) < parseInt(vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMinAge)) ||
                        (parseInt(vm.coverData.age) > parseInt(vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMaxAge)))) ||
                    vm.categoryDetails.partner.fundCat[0].tpdReqd === 'N') {
                    vm.tpdDisabled = true;
                    $('#tpd').css('display', 'none');
                    if (vm.categoryDetails.partner.fundCat[0].tpdReqd === 'N') {
                        vm.tpdDisabledMsg = 'The fund does not have TPD cover';
                    } else {
                        vm.tpdDisabledMsg = 'You are not eligible for TPD cover due to your age.';
                    }
                    vm.coverData.addnlTpdCoverDetails.tpdFixedAmt = 0;
                }
                if ((vm.categoryDetails.partner.fundCat[0].ipReqd === 'Y' && ((parseInt(vm.coverData.age) < parseInt(vm.categoryDetails.partner.fundCat[0].ipCover.ipMinAge)) ||
                        (parseInt(vm.coverData.age) > parseInt(vm.categoryDetails.partner.fundCat[0].ipCover.ipMaxAge)))) ||
                    vm.categoryDetails.partner.fundCat[0].ipReqd === 'N') {
                    vm.ipDisabled = true;
                    $('#sc').css('display', 'none');
                    if (vm.categoryDetails.partner.fundCat[0].ipReqd === 'N') {
                        vm.ipDisabledMsg = 'The fund does not have Income Protection cover';
                    } else {
                        vm.ipDisabledMsg = 'You are not eligible for Income Protection cover due to your age.';
                    }
                    vm.coverData.addnlIpCoverDetails.ipFixedAmt = 0;
                }
                vm.coverData.contactDetails.fundBrokerNum = vm.categoryDetails.partner.broker.brokerCnctNum;
                defer.resolve(res);
            }, function(err) {
                vm.servicefailureFlag = true;
                console.log('Error while getting fund category details...' + JSON.stringify(err));
                defer.reject(err);
            });

            return defer.promise;
        };

        fetchQuoteSvc.getList(vm.urlList.quoteUrl, 'HOST').then(function(res) {
            vm.IndustryOptions = res.data;
        }, function(err) {
            console.info('Error while fetching industry list ' + JSON.stringify(err));
        });



        vm.getOccupations = function() {
            vm.coverData.occupationDetails.occupation = '';
            vm.coverData.occupationDetails.otherOccupation = '';
            vm.coverData.occupationDetails.industryCode = vm.coverData.occupationDetails.industryCode === '' ? null : vm.coverData.occupationDetails.industryCode;
            fetchOccupationSvc.getOccupationList(vm.urlList.occupationUrl, 'HOST', vm.coverData.occupationDetails.industryCode).then(function(res) {
                vm.OccupationList = res.data;
            }, function(err) {
                console.info('Error while fetching occupations' + JSON.stringify(err));
            });
        };
        vm.FormIntroFields = ['brokerTitle', 'fname', 'lname', 'dob', 'gender'];
        vm.FormOneFields = ['contactEmail', 'contactPrefTime', 'preferredContactType', 'contactPhone'];
        vm.FormAddressFields = ['lineOne', 'suburb', 'state', 'postCode'];
        vm.OccupationFormFields = ['industry', 'occupation', 'annualSalary'];
        vm.OccupationOtherFormFields = ['industry', 'occupation', 'otherOccupation', 'annualSalary'];

        vm.checkPreviousMandatoryFields = function(elementName, formName) {
            var formFields;
            if (formName === 'formIntro') {
                formFields = vm.FormIntroFields;
            } else if (formName === 'formOne') {
                formFields = vm.FormOneFields;
            } else if (formName === 'addressForm') {
                formFields = vm.FormAddressFields;
            } else if (formName === 'occupationForm') {
                if (vm.coverData.occupationDetails.occupation !== undefined && vm.coverData.occupationDetails.occupation === 'Other') {
                    formFields = vm.OccupationOtherFormFields;
                } else {
                    formFields = vm.OccupationFormFields;
                }
            }
            var inx = formFields.indexOf(elementName);
            if (inx > 0) {
                for (var i = 0; i < inx; i++) {
                    if (vm[formName][formFields[i]]) {
                        vm[formName][formFields[i]].$touched = true;
                    }
                }
            }
        };

        vm.checkAnnualSalary = function() {
            if (!parseInt(vm.coverData.occupationDetails.salary)) {
                vm.invalidSalAmount = true;
                if (vm.occupationForm.annualSalary.$dirty) {
                    vm.occupationForm.annualSalary.$setValidity('invalid', false);
                }
            } else {
                vm.invalidSalAmount = false;
                if (vm.occupationForm.annualSalary.$dirty) {
                    vm.occupationForm.annualSalary.$setValidity('invalid', true);
                }
            }
            vm.insurePercentIp();
        };

        vm.insurePercentIp = function() {
            vm.ipWarningFlag = false;
            vm.validateIpAmount();
        };

        vm.validateDeathAmount = function() {
            if (vm.categoryDetails.partner.fundCat[0].deathReqd === 'Y') {
                var addnlDeathCover = parseInt(vm.coverData.addnlDeathCoverDetails.deathInputTextValue);
                var addnlTpdCover = parseInt(vm.coverData.addnlTpdCoverDetails.tpdInputTextValue);

                if (addnlDeathCover > 0 && addnlDeathCover === parseInt(vm.coverData.existingDeathAmt)) {
                    vm.deathErrorFlag = true;
                    vm.deathErrorMsg = 'Existing and additional cover amounts are same.';
                } else if (addnlDeathCover < parseInt(vm.coverData.existingDeathAmt)) {
                    vm.deathErrorFlag = true;
                    vm.deathErrorMsg = 'You cannot reduce your Death Only cover. Please re-enter your cover amount.';
                } else if (addnlDeathCover < parseInt(vm.categoryDetails.partner.fundCat[0].deathCover.deathMinSumIns)) {
                    vm.deathErrorFlag = true;
                    vm.deathErrorMsg = 'Your total death cover less than eligibility limit, minimum allowed for this product is  ' + vm.categoryDetails.partner.fundCat[0].deathCover.deathMinSumIns + '. Please re-enter your cover amount.';
                } else if (addnlDeathCover > parseInt(vm.categoryDetails.partner.fundCat[0].deathCover.deathMaxSumIns)) {
                    vm.deathErrorFlag = true;
                    vm.deathErrorMsg = 'Your total death cover exceeds eligibility limit, maximum allowed for this product is  ' + vm.categoryDetails.partner.fundCat[0].deathCover.deathMaxSumIns + '. Please re-enter your cover amount.';
                } else if (addnlTpdCover > addnlDeathCover) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'Your TPD cover amount should not be greater than Death cover amount.';
                } else {
                    vm.deathErrorFlag = false;
                    vm.deathErrorMsg = '';
                    vm.coverData.addnlDeathCoverDetails.deathFixedAmt = addnlDeathCover;
                    if (vm.tpdErrorFlag) {
                        vm.validateTpdAmount();
                    }
                }
            }
        };

        vm.validateTpdAmount = function() {
            if (vm.categoryDetails.partner.fundCat[0].tpdReqd === 'Y') {
                if (parseInt(vm.coverData.addnlTpdCoverDetails.tpdInputTextValue) > parseInt(vm.coverData.addnlDeathCoverDetails.deathInputTextValue)) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'Your TPD cover amount should not be greater than Death cover amount.';
                } else if (parseInt(vm.coverData.addnlTpdCoverDetails.tpdInputTextValue) < parseInt(vm.coverData.existingTpdAmt)) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'You cannot reduce your TPD cover. Please re-enter your cover amount.';
                } else if (parseInt(vm.coverData.addnlTpdCoverDetails.tpdInputTextValue) < parseInt(vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMinSumIns)) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'Your total TPD cover less than eligibility limit, minimum allowed for this product is  ' + vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMinSumIns + '. Please re-enter your cover amount.';
                } else if (parseInt(vm.coverData.addnlTpdCoverDetails.tpdInputTextValue) > parseInt(vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMaxSumIns)) {
                    vm.tpdErrorFlag = true;
                    vm.tpdErrorMsg = 'Your total TPD cover exceeds eligibility limit, maximum allowed for this product is  ' + vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMaxSumIns + '. Please re-enter your cover amount.';
                } else {
                    vm.tpdErrorFlag = false;
                    vm.tpdErrorMsg = '';
                    vm.coverData.addnlTpdCoverDetails.tpdFixedAmt = parseInt(vm.coverData.addnlTpdCoverDetails.tpdInputTextValue);
                }
            }
        };

        vm.validateIpAmount = function() {
            vm.ipWarningMsg = false;
            vm.ipErrorFlag = false;
            if (vm.categoryDetails.partner.fundCat[0].ipReqd === 'Y') {
                var insuredPercentIp = Math.round(parseFloat((vm.categoryDetails.partner.fundCat[0].ipCover.ipFormula * (vm.coverData.occupationDetails.salary / 12)) / 100));
                var ipMaxAmount = parseInt(vm.categoryDetails.partner.fundCat[0].ipCover.ipMaxSumIns);
                var tempTotalIpCover = parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue);
                if (ipMaxAmount > insuredPercentIp) {
                    ipMaxAmount = insuredPercentIp;
                }
                if (parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue) < parseInt(vm.coverData.existingIPAmount)) {
                    vm.coverData.addnlIpCoverDetails.ipFixedAmt = parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue);
                    vm.ipErrorFlag = true;
                    vm.ipErrorMsg = 'You cannot reduce your Income protection cover. Please re-enter your cover amount.';
                } else if (parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue) < parseInt(vm.categoryDetails.partner.fundCat[0].ipCover.ipMinSumIns)) {
                    vm.coverData.addnlIpCoverDetails.ipFixedAmt = parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue);
                    vm.ipErrorFlag = true;
                    vm.ipErrorMsg = 'Your total IP cover less than eligibility limit, minimum allowed for this product is  ' + vm.categoryDetails.partner.fundCat[0].ipCover.ipMinSumIns + '. Please re-enter your cover.';
                } else if (parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue) > parseInt(ipMaxAmount)) {
                    if (vm.channel === 'email') {
                        vm.coverData.addnlIpCoverDetails.ipInputTextValue = parseInt(ipMaxAmount);
                        vm.ipWarningMsg = true;
                        vm.coverData.addnlIpCoverDetails.ipFixedAmt = parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue);
                    } else {
                        vm.ipErrorFlag = true;
                        vm.ipErrorMsg = 'Your total IP cover exceeds eligibility limit, maximum allowed for this product is  ' + parseInt(ipMaxAmount) + '. Please re-enter your cover.';
                    }
                } else if (parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue) > parseInt(insuredPercentIp)) {
                    vm.coverData.addnlIpCoverDetails.ipInputTextValue = parseInt(insuredPercentIp);
                    vm.ipWarningMsg = true;
                    vm.coverData.addnlIpCoverDetails.ipFixedAmt = parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue);
                } else {
                    vm.ipWarningMsg = false;
                    vm.ipErrorFlag = false;
                    vm.ipErrorMsg = '';
                    vm.coverData.addnlIpCoverDetails.ipFixedAmt = parseInt(vm.coverData.addnlIpCoverDetails.ipInputTextValue);
                }
            }
        };

        vm.changePrefContactType = function() {
            if (vm.coverData.contactType === '1') {
                vm.coverData.contactDetails.mobilePhone = vm.coverData.contactPhone;
            } else if (vm.coverData.contactType === '2') {
                vm.coverData.contactDetails.homePhone = vm.coverData.contactPhone;
            } else if (vm.coverData.contactType === '3') {
                vm.coverData.contactDetails.workPhone = vm.coverData.contactPhone;
            } else {
                vm.coverData.contactPhone = '';
            }
        };

        vm.changeOtherContactType = function() {
            if (vm.coverData.otherContactType === '1') {
                vm.coverData.otherContactPhone = vm.coverData.otherContactPhone;
            } else if (vm.coverData.otherContactType === '2') {
                vm.coverData.otherContactPhone = vm.coverData.otherContactPhone;
            } else if (vm.coverData.otherContactType === '3') {
                vm.coverData.otherContactPhone = vm.coverData.otherContactPhone;
            } else {
                vm.coverData.otherContactPhone = '';
            }
        };

        vm.save = function() {
            vm.validateTpdAmount();
            vm.validateDeathAmount();
            vm.validateIpAmount();
            if (vm.formduty.$invalid || vm.formOne.$invalid || vm.addressForm.$invalid || vm.occupationForm.$invalid || vm.invalidSalAmount || vm.coverCalculatorForm.$invalid || vm.deathErrorFlag || vm.tpdErrorFlag || vm.ipErrorFlag) {
                return false;
            }
            vm.coverData.corpFundCode = vm.categoryDetails.partner.fundCode;
            vm.coverData.lastSavedOn = 'Quotepage';
            saveEapplyData.reqObj(vm.urlList.saveCorporateEapply, vm.coverData).then(function(response) {
                vm.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>' + vm.coverData.appNum + '</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR>');
            });
        };

        vm.generatePDF = function() {
            vm.coverData.corpFundCode = vm.categoryDetails.partner.fundCode;
            vm.coverData.industryName = fetchIndustryList.getIndustryName(vm.coverData.occupationDetails.industryCode);
            printPageService.getPrintObject(vm.urlList.printQuotePage, vm.coverData, vm.token).then(function(res) {
                var response = res.data;
                vm.printDetails = response;
                vm.downloadPDF();
            }, function(err) {
                console.log('Error while Printing details...' + JSON.stringify(err));
            });
        };

        vm.downloadPDF = function() {
            var pdfLocation = null;
            var filename = null;
            var a = null;
            pdfLocation = vm.printDetails.clientPDFLocation;
            filename = pdfLocation.substring(pdfLocation.lastIndexOf('/') + 1);
            a = document.createElement('a');
            document.body.appendChild(a);
            DownloadPDFService.download(vm.urlList.downloadUrl, pdfLocation).then(function(res) {
                if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
                    window.navigator.msSaveBlob(res.data.response, filename);
                } else {
                    var fileURL = URL.createObjectURL(res.data.response);
                    a.href = fileURL;
                    a.download = filename;
                    a.click();
                }
            }, function(err) {
                console.log('Error downloading the PDF ' + err);
            });
        };

        vm.quoteSaveAndExitPopUp = function(hhText) {
            var dialog1 = ngDialog.open({
                template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">' + hhText + '</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons text-center"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
                className: 'ngdialog-theme-plain custom-width',
                preCloseCallback: function(value) {
                    var url = '/login';
                    $location.path(url);
                    return true;
                },
                plain: true
            });
            dialog1.closePromise.then(function(data) {
                console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
            });
        };
        vm.gotoAura = function() {
            vm.validateTpdAmount();
            vm.validateDeathAmount();
            vm.validateIpAmount();
            if (vm.formduty.$valid && vm.formOne.$valid && vm.addressForm.$valid && vm.occupationForm.$valid && !vm.invalidSalAmount && vm.coverCalculatorForm.$valid && !vm.deathErrorFlag && !vm.tpdErrorFlag && !vm.ipErrorFlag) {
                if ((parseInt(vm.coverDetails.totalDeathCover) === parseInt(vm.coverDetails.eligibleDeathCover)) &&
                    (parseInt(vm.coverDetails.totalTpdCover) === parseInt(vm.coverDetails.eligibleTpdCover)) &&
                    (parseInt(vm.coverDetails.totalIpCover) === parseInt(vm.coverDetails.eligibleIpCover))) {
                    vm.coverData.auraDisabled = true;
                    vm.continueToNextPage('/summary');
                } else {
                    vm.coverData.auraDisabled = false;
                    vm.continueToNextPage('/aura');
                }
            } else {
                return false;
            }
        };
        vm.continueToNextPage = function(path) {
            vm.coverData.corpFundCode = vm.categoryDetails.partner.fundCode;
            if (vm.categoryDetails.partner.fundCat[0].deathReqd === 'Y') {
                vm.coverData.deathMaxAmount = vm.categoryDetails.partner.fundCat[0].deathCover.deathMaxSumIns;
            }
            if (vm.categoryDetails.partner.fundCat[0].deathReqd === 'N') {
                vm.coverData.deathMaxAmount = 0;
            }
            if (vm.categoryDetails.partner.fundCat[0].tpdReqd === 'Y') {
                vm.coverData.tpdMaxAmount = vm.categoryDetails.partner.fundCat[0].tpdCover.tpdMaxSumIns;
            }
            if (vm.categoryDetails.partner.fundCat[0].tpdReqd === 'N') {
                vm.coverData.tpdMaxAmount = 0;
            }
            if (vm.categoryDetails.partner.fundCat[0].ipReqd === 'Y') {
                vm.coverData.ipMaxAmount = vm.categoryDetails.partner.fundCat[0].ipCover.ipMaxSumIns;
            }
            if (vm.categoryDetails.partner.fundCat[0].ipReqd === 'N') {
                vm.coverData.ipMaxAmount = 0;
            }
            vm.coverData.addnlIpCoverDetails.waitingPeriod = vm.coverData.addnlIpCoverDetails.waitingPeriod || '90 Days';
            vm.coverData.addnlIpCoverDetails.benefitPeriod = vm.coverData.addnlIpCoverDetails.benefitPeriod || '2 Years';
            appData.setAppData(vm.coverData);
            $location.path(path);
        };

        vm.isCollapsible = function(targetEle, event) {
            var dodLabelCheck = $('#dodLabel').hasClass('active');
            if (targetEle === 'collapseIntro' && !dodLabelCheck) {
                event.stopPropagation();
                vm.formduty.$submitted = true;
                return false;
            } else if (targetEle === 'collapseOne' && !dodLabelCheck || $('#collapseIntro form').hasClass('ng-invalid')) {
                event.stopPropagation();
                if ($('#collapseIntro').hasClass('show')) {
                    vm.formIntro.$submitted = true;
                }
                return false;
            } else if (targetEle === 'collapseAddress' && (!dodLabelCheck || $('#collapseIntro form').hasClass('ng-invalid') || $('#collapseOne form').hasClass('ng-invalid'))) {
                event.stopPropagation();
                if ($('#collapseOne').hasClass('show')) {
                    vm.formOne.$submitted = true;
                }
                return false;
            } else if (targetEle === 'collapseTwo' && (!dodLabelCheck || $('#collapseIntro form').hasClass('ng-invalid') || $('#collapseOne form').hasClass('ng-invalid') || $('#collapseAddress form').hasClass('ng-invalid'))) {
                event.stopPropagation();
                if ($('#collapseAddress').hasClass('show')) {
                    vm.addressForm.$submitted = true;
                }
                return false;
            } else if (targetEle === 'collapseThree' && (!dodLabelCheck || $('#collapseIntro form').hasClass('ng-invalid') || $('#collapseOne form').hasClass('ng-invalid') || $('#collapseAddress form').hasClass('ng-invalid') || $('#collapseTwo form').hasClass('ng-invalid'))) {
                event.stopPropagation();
                if ($('#collapseTwo').hasClass('show')) {
                    vm.occupationForm.$submitted = true;
                }
                return false;
            }
        };

        vm.init().then(function() {
            $scope.$watch('vm.coverData.dodCheck', function(newVal, oldVal) {
                if (!newVal) {
                    $('#collapseIntro').collapse('hide');
                    $('#collapseOne').collapse('hide');
                    $('#collapseAddress').collapse('hide');
                    $('#collapseTwo').collapse('hide');
                    $('#collapseThree').collapse('hide');
                } else {
                    $('#collapseIntro').collapse('show');
                    // $('html,body').animate({
                    // 	scrollTop: $('#collapseIntro').offset().top
                    // }, 500);
                    $('#collapseOne').collapse(!vm.formOne.$invalid ? 'show' : 'hide');
                    $('#collapseAddress').collapse(!vm.addressForm.$invalid ? 'show' : 'hide');
                    $('#collapseTwo').collapse(!vm.occupationForm.$invalid ? 'show' : 'hide');
                    $('#collapseThree').collapse(!vm.coverCalculatorForm.$invalid ? 'show' : 'hide');
                }
            });

            $scope.$watch('vm.formIntro.$valid', function(newVal, oldVal) {
                if (!newVal) {
                    $('#collapseOne').collapse('hide');
                    $('#collapseAddress').collapse('hide');
                    $('#collapseTwo').collapse('hide');
                    $('#collapseThree').collapse('hide');
                } else {
                    $('#collapseOne').collapse('show');
                    // $('html,body').animate({
                    // 	scrollTop: $('#collapseOne').offset().top
                    // }, 500);
                    $('#collapseAddress').collapse(!vm.addressForm.$invalid ? 'show' : 'hide');
                    $('#collapseTwo').collapse(!vm.occupationForm.$invalid ? 'show' : 'hide');
                    $('#collapseThree').collapse(!vm.coverCalculatorForm.$invalid ? 'show' : 'hide');
                }
            });

            $scope.$watch('vm.formOne.$valid', function(newVal, oldVal) {
                if (!newVal) {
                    $('#collapseAddress').collapse('hide');
                    $('#collapseTwo').collapse('hide');
                    $('#collapseThree').collapse('hide');
                } else {
                    $('#collapseAddress').collapse('show');
                    // $('html,body').animate({
                    // 	scrollTop: $('#collapseAddress').offset().top
                    // }, 500);
                    $('#collapseTwo').collapse(!vm.occupationForm.$invalid ? 'show' : 'hide');
                    $('#collapseThree').collapse(!vm.coverCalculatorForm.$invalid ? 'show' : 'hide');
                }
            });

            $scope.$watch('vm.addressForm.$valid', function(newVal, oldVal) {
                if (!newVal) {
                    $('#collapseTwo').collapse('hide');
                    $('#collapseThree').collapse('hide');
                } else {
                    $('#collapseTwo').collapse('show');
                    // $('html,body').animate({
                    // 	scrollTop: $('#collapseTwo').offset().top
                    // }, 500);
                    $('#collapseThree').collapse(!vm.coverCalculatorForm.$invalid ? 'show' : 'hide');
                }
            });

            $scope.$watch('vm.occupationForm.$valid', function(newVal, oldVal) {
                if (!newVal) {
                    $('#collapseThree').collapse('hide');
                } else {
                    $('#collapseThree').collapse('show');
                    // $('html,body').animate({
                    // 	scrollTop: $('#collapseThree').offset().top
                    // }, 500);
                }
            });

            if (vm.coverData.contactType === '1') {
                vm.coverData.contactPhone = vm.coverData.contactDetails.mobilePhone;
            } else if (vm.coverData.contactType === '2') {
                vm.coverData.contactPhone = vm.coverData.contactDetails.homePhone;
            } else if (vm.coverData.contactType === '3') {
                vm.coverData.contactPhone = vm.coverData.contactDetails.workPhone;
            } else {
                vm.coverData.contactPhone = '';
            }
            if (vm.coverData.otherContactType === '1') {
                vm.coverData.otherContactPhone = vm.coverData.otherContactPhone;
            } else if (vm.coverData.otherContactType === '2') {
                vm.coverData.otherContactPhone = vm.coverData.otherContactPhone;
            } else if (vm.coverData.otherContactType === '3') {
                vm.coverData.otherContactPhone = vm.coverData.otherContactPhone;
            } else {
                vm.coverData.otherContactPhone = '';
            }
            vm.coverData.ipcheckbox = (vm.coverData.ipcheckbox === 'true' || vm.coverData.ipcheckbox === true) ? true : false;
            if (vm.channel === 'email') {
                vm.validateIpAmount();
            }
            if (vm.coverData.occupationDetails.industryCode) {
                fetchOccupationSvc.getOccupationList(vm.urlList.occupationUrl, 'HOST', vm.coverData.occupationDetails.industryCode).then(function(res) {
                    vm.OccupationList = res.data;
                }, function(err) {
                    console.info('Error while fetching occupations' + JSON.stringify(err));
                });
            }
        });
        vm.clickToOpen = function(hhText) {
            var dialog = ngDialog.open({
                template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">' + hhText + '</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-12 text-center"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div></div></div>',
                className: 'ngdialog-theme-plain',
                plain: true
            });
            dialog.closePromise.then(function(data) {
                console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
            });
        };
    }
})(angular);