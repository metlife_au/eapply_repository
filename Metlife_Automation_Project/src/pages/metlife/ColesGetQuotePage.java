package pages.metlife;

import java.util.List;

import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.HostplusPersonalDetailsPageObjects;

public class ColesGetQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public ColesGetQuotePage enterColesCalculateQuote() {
		fillAndCalculateColesQuote();
		return new ColesGetQuotePage(scriptHelper);
	}
	
	public ColesGetQuotePage getQuota() {
		premiumCalculation();
		return new ColesGetQuotePage(scriptHelper);
	}

	public ColesGetQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		//driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}

	public void fillAndCalculateColesQuote() {
		String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String lastName = dataTable.getData("MetLife_Data", "LastName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String flybuysmemeber = dataTable.getData("MetLife_Data", "FlyBuysMemeber");
		String joinflybuys = dataTable.getData("MetLife_Data", "JoinFlyBuys");
		String phoneNumber = dataTable.getData("MetLife_Data", "PreferredNumber");
		String strState = dataTable.getData("MetLife_Data", "State");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCoverAmt = dataTable.getData("MetLife_Data", "CoverAmount");
		String strPaymentFreq = dataTable.getData("MetLife_Data", "PaymentFrequency");
		String StrAddkid = dataTable.getData("MetLife_Data", "KidsCover");
		String StrNoofKids = dataTable.getData("MetLife_Data", "NoofKids");
		String StrAccCover = dataTable.getData("MetLife_Data", "AccidentCover");
		String StrLevel = dataTable.getData("MetLife_Data", "Level");
		System.out.println("The Premium Date is :: " + getPremiumDOB(-3));
		try {
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.btnNewApplication), "Click", "New Application");
			sleep(3000);
			switchToActiveWindow();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName+getRandomText(7));
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			sleep(1000);
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtLastName), "SET", "Last Name", lastName+getRandomText(3));
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtLastName), "TAB", "Last Name");
			sleep(2000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "SET", "Phone Number", phoneNumber);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "TAB", "Phone Number");
			sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtDOB), "SET", "Date of birth", getPremiumDOB(Integer.parseInt("-"+dob)));
			sleep(2000);
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
				sleep(1000);
			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFemale), "clkradio", "Gender -" + gender);
				sleep(1000);	
			}
			
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsYes), "clkradio",
						"Have you Smoked in 12 months - Yes");
								
			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsNo), "clkradio",
						"Have you Smoked in 12 months - No");
				
			}
		    sleep(1000);
			driver.findElement(By.cssSelector("span[class='filter-option pull-left']")).click();
			sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.btnState), "specialDropdown", "State", strState);
			//driver.findElement(ColesGetQuotePageObjects.btnState).click();
			//driver.findElement(By.cssSelector(".btn-group.bootstrap-select button[data-id='qtestateId']")).click();
			sleep(1000);
			//driver.findElement(By.linkText(strState)).click();
			//sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selCoverAmt), "specialDropdown", "Cover Amount", strCoverAmt);
			sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selFrequency), "specialDropdown", "Payment Frequency",
					strPaymentFreq);
			//tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysYes), "Click","Are you a flybuys member - Yes");
			if(flybuysmemeber.equalsIgnoreCase("yes"))
			{
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysYes), "Click","Are you a flybuys member - Yes");
			}
			else{
				
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysNo), "Click",
						"Are you a flybuys member - No");
				tryAction(joinflybuys.equalsIgnoreCase("yes")?waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysJoinYes):waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysJoinNo), "Click", "Would you like to join flybuys - "+joinflybuys);
				
			}
			
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ColesGetQuotePageObjects.lnkCalculateQuoteId)).click().perform();
			if(StrAccCover.equalsIgnoreCase("yes"))
			{
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.btnAccCover), "Click","Add Accident Cover");
				sleep(3000);
			}
			sleep(1000);
			if(StrAddkid.equalsIgnoreCase("yes"))
			{
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.btnAddKid), "Click","Add Kid Cover");
				tryAction(fluentWaitElement(ColesGetQuotePageObjects.selNoofKids), "specialDropdown", "No of Kids", StrNoofKids);
			}
			if(StrLevel.equalsIgnoreCase("yes"))
			{
				System.out.println("Entered Level Condition");
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.linkLevel), "Click","Switch to Level");
				sleep(3000);
			}
			
			sleep(1000);
			capturePremiumData();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.lnkApply), "Click", "Apply");

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	public void clickApply() {
		try {
			tryAction(driver.findElement(ColesGetQuotePageObjects.lnkApply), "Click", "Apply Quote");
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private static Parameter getOutflowConfiguration(String username) {
		OutflowConfiguration ofc = new OutflowConfiguration();
		ofc.setActionItems("UsernameToken");
		ofc.setPasswordType("PasswordText");
		ofc.setUser(username);
		return ofc.getProperty();
	}

	public void premiumCalculation() {

		String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String flybuysmemeber = dataTable.getData("MetLife_Data", "FlyBuysMemeber");
		String joinflybuys = dataTable.getData("MetLife_Data", "JoinFlyBuys");
		String phoneNumber = dataTable.getData("MetLife_Data", "PreferredNumber");
		String strState = dataTable.getData("MetLife_Data", "State");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCoverAmt = dataTable.getData("MetLife_Data", "CoverAmount");
		String strPaymentFreq = dataTable.getData("MetLife_Data", "PaymentFrequency");
		
		try {
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.btnNewApplication), "Click", "New Application");
			switchToActiveWindow();
			driver.manage().window().maximize();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "SET", "Phone Number", phoneNumber);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "TAB", "Phone Number");
			sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtDOB), "SET", "Date of birth", getPremiumDOB(Integer.parseInt("-"+dob)));
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFemale), "clkradio", "Gender -" + gender);
			}
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsYes), "Click",
						"Have you Smoked in 12 months - Yes");

			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsNo), "Click",
						"Have you Smoked in 12 months - No");
			}
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.btnState), "specialDropdown", "State", strState);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selCoverAmt), "specialDropdown", "Cover Amount",strCoverAmt);

			sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selFrequency), "specialDropdown", "Payment Frequency",strPaymentFreq);
			if("Yes".equalsIgnoreCase(flybuysmemeber))
			{
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysYes), "Click","Are you a flybuys member - Yes");
			}
			else{
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysNo), "Click","Are you a flybuys member - No");
				if("Yes".equalsIgnoreCase(joinflybuys)){
					tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysJoinYes), "Click","Would you like to join flybuys - Yes");
				}
				else{
					tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysJoinNo), "Click","Would you like to join flybuys - No");
				}
				
			}
			sleep(1000);
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ColesGetQuotePageObjects.lnkCalculateQuoteId)).click().perform();
			capturePremiumData();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.lnkApply), "Click", "Apply");

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	public void capturePremiumData() {
		
		String strPremiumFromExcel = dataTable.getData("MetLife_Data", "expectedStepPremium");
		String strLevelPremiumFromExcel = dataTable.getData("MetLife_Data", "expectedlevelPremium");
		String StrLevel1 = dataTable.getData("MetLife_Data", "Level");
		if(!strPremiumFromExcel.equalsIgnoreCase("none"))
		{
			assertContainsTextwithoutSpecialChars(fluentWaitElement(ColesGetQuotePageObjects.lblPremium),strPremiumFromExcel);
			if(!StrLevel1.equalsIgnoreCase("Yes")){
				assertContainsTextwithoutSpecialChars(fluentWaitElement(ColesGetQuotePageObjects.lblLevelPremium),strLevelPremiumFromExcel);
			
		         	}
				else{
									
					assertContainsTextwithoutSpecialChars(fluentWaitElement(ColesGetQuotePageObjects.lblLevelPremium1),strLevelPremiumFromExcel);
					}
			}
		
		}
	}
	

