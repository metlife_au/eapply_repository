(function (angular) {
	'use strict';
	/* global auraId */
	angular
		.module('CorporateApp')
		.controller('auraCtrl', auraCtrl);
	auraCtrl.$inject = ['$location', '$timeout', '$window', 'auraTransferInitiateSvc', 'fetchAuraTransferData', 'auraRespSvc', 'auraPostSvc', 'auraInputSvc', 'PersistenceService', 'submitAuraSvc', 'ngDialog', 'saveEapplyData', 'clientMatchSvc', 'submitEapplySvc', 'fetchUrlSvc', 'appData', 'fetchAppNumberSvc', 'fetchIndustryList', 'calculateFUL', 'fetchTokenNumSvc', 'inputDataService'];
	function auraCtrl($location, $timeout, $window, auraTransferInitiateSvc, fetchAuraTransferData, auraRespSvc, auraPostSvc, auraInputSvc, PersistenceService, submitAuraSvc, ngDialog, saveEapplyData, clientMatchSvc, submitEapplySvc, fetchUrlSvc, appData, fetchAppNumberSvc, fetchIndustryList, calculateFUL, fetchTokenNumSvc, inputDataService) {
		var vm = this;
		vm.urlList = fetchUrlSvc.getUrlList();
		vm.datePattern = '/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i';
		vm.freeText = {};
		vm.isAuraErroneous = false;
		vm.erroneousSections = [];
		vm.categoryDetails = appData.getFundCategoryDetails();
		vm.fundBrokerNum = vm.categoryDetails.partner.broker.brokerCnctNum;
		vm.go = function (path) {
			vm.setHeightWeight();
			$timeout(function () {
				$location.path(path);
			}, 10);
		};

		vm.setHeightWeight = function () {
			vm.heightval = vm.heightval || null;
			vm.weightVal = vm.weightVal || null;
			if (vm.heightval !== null || vm.weightVal !== null) {
				vm.submitHeightWeightQuestion(2);
			}
		};
		vm.serachText = '';
		vm.heightDropdownOpt = ['cm', 'Feet'];
		vm.heightDropDown = vm.heightDropdownOpt[0];
		vm.heightOptions = vm.heightOptions === undefined ? 'm.cm' : vm.heightOptions;

		vm.weightDropdownOpt = ['Kilograms', 'Pound', 'Stones'];
		vm.weightDropDown = vm.weightDropdownOpt[0];
		vm.weighOptions = vm.weighOptions === undefined ? 'kg' : vm.weighOptions;
		vm.weightMaxLen = 3;
		vm.navigateToLandingPage = function () {
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/

			var templateContent = '<div class="ngdialog-content"><div class="modal-body">';
			templateContent += '<div class="row  rowcustom  "><div class="col-sm-8">';
			templateContent += '<p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br>';
			templateContent += '<div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter">';
			templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button>';
			templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
			templateContent += ' </div></div>';

			ngDialog.openConfirm({
				template: templateContent,
				plain: true,
				className: 'ngdialog-theme-plain custom-width'
			}).then(function () {
				$location.path('/landing');
			}, function (e) {
				if (e === 'oncancel') {
					return false;
				}
			});
		};

		// added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

		/* var timer;
		 angular.element($window).bind('mouseover', function(){
			 timer = $timeout(function(){
				 sessionStorage.clear();
				 localStorage.clear();
				 $location.path("/sessionTimeOut");
			 }, 900000);
		 }).bind('mouseout', function(){
			 $timeout.cancel(timer);
		 });*/

		vm.quotePageDetails = {}; // will be replaced
		vm.coverDetails = {}; // will be replaced
		vm.deathAddnlDetails = {}; // will be replaced
		vm.tpdAddnlDetails = {}; // will be replaced
		vm.ipAddnlDetails = {}; // will be replaced
		vm.changeCoverOccDetails = {}; // will be replaced
		angular.extend(vm.quotePageDetails, appData.getAppData());
		angular.extend(vm.coverDetails, vm.quotePageDetails);
		angular.extend(vm.deathAddnlDetails, vm.quotePageDetails.addnlDeathCoverDetails);
		angular.extend(vm.tpdAddnlDetails, vm.quotePageDetails.addnlTpdCoverDetails);
		angular.extend(vm.ipAddnlDetails, vm.quotePageDetails.addnlIpCoverDetails);
		angular.extend(vm.changeCoverOccDetails, vm.quotePageDetails.occupationDetails);
		// Industry name

		
		if (vm.quotePageDetails.auraSessionId === undefined || vm.quotePageDetails.auraSessionId === null) {
			vm.auraId = auraId || '';
		}
		else {
			vm.auraId = vm.quotePageDetails.auraSessionId;
		}
		if (vm.quotePageDetails.occupationDetails.smoker === undefined) {
			if (vm.changeCoverOccDetails.smokerQuestion === '1') {
				vm.quotePageDetails.occupationDetails.smoker = 'Yes';
			} else {
				vm.quotePageDetails.occupationDetails.smoker = 'No';
			}
		}

		auraInputSvc.setSmoker(vm.quotePageDetails.occupationDetails.smoker);
		auraInputSvc.setFund('CORP');
		auraInputSvc.setMode('change');
		//setting deafult vaues for testing
		auraInputSvc.setName(vm.quotePageDetails.personalDetails.firstName + '' + vm.quotePageDetails.personalDetails.lastName);
		//auraInputSvc.setAge(moment().diff(moment(vm.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
		auraInputSvc.setAge(vm.quotePageDetails.age);
		//  	auraInputSvc.setAppnumber(vm.quotePageDetails.appNum);
		if (parseFloat(vm.deathAddnlDetails.deathFixedAmt) > parseFloat(vm.coverDetails.existingDeathAmt)) {
			auraInputSvc.setDeathAmt(parseFloat(vm.deathAddnlDetails.deathFixedAmt));
		} else {
			auraInputSvc.setDeathAmt(0);
		}

		if (parseFloat(vm.tpdAddnlDetails.tpdFixedAmt) > parseFloat(vm.coverDetails.existingTpdAmt)) {
			auraInputSvc.setTpdAmt(parseFloat(vm.tpdAddnlDetails.tpdFixedAmt));
		} else {
			auraInputSvc.setTpdAmt(0);
		}

		if (parseFloat(vm.ipAddnlDetails.ipInputTextValue) > parseFloat(vm.coverDetails.existingIPAmount)) {
			auraInputSvc.setIpAmt(parseFloat(vm.ipAddnlDetails.ipFixedAmt));
		} else if ((parseFloat(vm.ipAddnlDetails.ipInputTextValue) <= parseFloat(vm.coverDetails.existingIPAmount)) && vm.ipIncreaseFlag) {
			auraInputSvc.setIpAmt(parseFloat(vm.ipAddnlDetails.ipFixedAmt));
		} else {
			auraInputSvc.setIpAmt(0);
		}

		auraInputSvc.setWaitingPeriod(vm.ipAddnlDetails.waitingPeriod);
		auraInputSvc.setBenefitPeriod(vm.ipAddnlDetails.benefitPeriod);
		auraInputSvc.setMemberType('INDUSTRY OCCUPATION');
		if (vm.changeCoverOccDetails && vm.changeCoverOccDetails.gender) {
			auraInputSvc.setGender(vm.changeCoverOccDetails.gender);
		} else if (vm.quotePageDetails.personalDetails && vm.quotePageDetails.personalDetails.gender) {
			auraInputSvc.setGender(vm.quotePageDetails.personalDetails.gender);
		}

		auraInputSvc.setClientname('metaus');
		auraInputSvc.setIndustryOcc(vm.quotePageDetails.occupationDetails.industryCode + ':' + vm.quotePageDetails.occupationDetails.occupation);
		
		auraInputSvc.setSalary(vm.quotePageDetails.occupationDetails.salary.toString());
		auraInputSvc.setFifteenHr('Yes');
		auraInputSvc.setLastName(vm.quotePageDetails.personalDetails.lastName);
		auraInputSvc.setFirstName(vm.quotePageDetails.personalDetails.firstName);
		auraInputSvc.setDob(vm.quotePageDetails.dob);
		auraInputSvc.setAppnumber(parseInt(vm.auraId));
		var termFlag = false;
		if (termFlag) {
			auraInputSvc.setExistingTerm(true);
		} else {
			auraInputSvc.setExistingTerm(false);
		}
		var ruleModel = {
			'age': vm.quotePageDetails.age,
			'fundCode': 'CORP',
			'gender': vm.changeCoverOccDetails.gender,
			'deathFixedAmount': parseInt(auraInputSvc.getDeathAmt()),
			'deathFixedCost': null,
			'deathUnitsCost': null,
			'tpdFixedAmount': parseInt(auraInputSvc.getTpdAmt()),
			'tpdFixedCost': null,
			'tpdUnitsCost': null,
			'ipUnits': null,
			'ipFixedAmount': parseInt(auraInputSvc.getIpAmt()),
			'ipFixedCost': null,
			'ipUnitsCost': null,
			'manageType': 'CCOVER',
			'deathCoverType': 'Fixed',
			'tpdCoverType': 'Fixed',
			'ipCoverType': 'IpFixed',
			'ipWaitingPeriod': vm.ipAddnlDetails.waitingPeriod,
			'ipBenefitPeriod': vm.ipAddnlDetails.benefitPeriod,
			'corpFundCode': vm.quotePageDetails.corpFundCode,
			'deathMaxAmount': parseInt(vm.quotePageDetails.deathMaxAmount),
			'tpdMaxAmount': parseInt(vm.quotePageDetails.tpdMaxAmount),
			'ipMaxAmount': parseInt(vm.quotePageDetails.ipMaxAmount)
		};
		
		calculateFUL.calculate(vm.urlList.calculatFUL, ruleModel).then(function (response) {
			vm.fulData = response.data;
			if (vm.fulData.fulDeathAmount !== null && vm.fulData.fulDeathAmount !== undefined) {
				auraInputSvc.setDeathAmt(vm.fulData.fulDeathAmount);
			}
			else if (vm.fulData.fulTPDAmount !== null && vm.fulData.fulTPDAmount !== undefined) {
				auraInputSvc.setTpdAmt(vm.fulData.fulTPDAmount);
			}
			else if (vm.fulData.ipFulAmount !== null && vm.fulData.ipFulAmount !== undefined) {
				auraInputSvc.setIpAmt(vm.fulData.ipFulAmount);
			}

		});
		/*}*/

		fetchAuraTransferData.requestObj(vm.urlList.auraTransferDataUrl).then(function (response) {
			vm.auraResponseDataList = response.data.sections;
			angular.forEach(vm.auraResponseDataList, function (Object, index) {
				vm.questionAlias = Object.sectionName;
				vm.questionlist = Object.questions;
				if (vm.questionAlias === 'QG=Personal Details') {
					vm.auraResponseDataList[index].sectionStatus = true;
				} else {
					var keepGoing = true;
					angular.forEach(vm.questionlist, function (Object, index1) {
						if (Object.questionId === '4') {
							Object.branchComplete = false;
						}
						if (keepGoing && Object.branchComplete) {
							vm.auraResponseDataList[index].sectionStatus = true;
							keepGoing = false;
						}
					});
				}
			});
			vm.updateHeightWeightValue();
		});

		vm.inchValue = function (value) {
			vm.heightin = value;
		};
		vm.meterValue = function (value, answer, questionObj) {
			if (!angular.isUndefined(value)) {
				vm.heightinMeter = value;
				vm.heightval = value;
				questionObj.error = false;
			} else {
				questionObj.error = true;
			}


		};

		vm.heighOptions = function (value) {
			if (value === 'cm') {
				vm.heightOptions = 'm.cm';
			} else {
				vm.heightval = '';
				vm.heightOptions = 'ft.in';
			}
		};
		vm.weightValue = function (value, answer, questionObj) {

			if (!angular.isUndefined(value)) {
				vm.weightVal = value;
				questionObj.error = false;
			} else if (!angular.isUndefined(value) && value === '') {
				questionObj.error = true;
			}

		};
		vm.weightOptions = function (value, answer) {
			if (value === 'Kilograms') {
				vm.weighOptions = 'kg';
				vm.weightDropDown = 'Kilograms';
				vm.weightMaxLen = 3;
			} else if (value === 'Pound') {
				vm.weighOptions = 'lb';
				vm.weightDropDown = 'Pound';
				vm.weightMaxLen = 3;
			} else if (value === 'Stones') {
				vm.weighOptions = 'st.lb';
				vm.weightDropDown = 'Stones';
				vm.weightMaxLen = 100;
			}
		};

		vm.lbsValue = function (value) {
			vm.lbsval = value;
		};

		vm.submitHeightWeightQuestion = function (sectionIndex) {
			angular.forEach(vm.auraResponseDataList, function (Object, index) {
				angular.forEach(Object.questions, function (Object) {
					var serviceCallRequired = false;
					var questionObj = Object;
					if (Object.classType === 'HeightQuestion') {
						questionObj = Object;
						if (vm.heightOptions === 'm.cm') {
							questionObj.arrAns = [];
							questionObj.arrAns[0] = vm.heightOptions;
							questionObj.arrAns[1] = '0';
							questionObj.arrAns[2] = vm.heightval;
						} else if (vm.heightOptions === 'ft.in') {
							questionObj.arrAns = [];
							questionObj.arrAns[0] = vm.heightOptions;
							questionObj.arrAns[1] = vm.heightval;
							questionObj.arrAns[2] = vm.heightin;
						}

						serviceCallRequired = true;
					} else if (Object.classType === 'WeightQuestion') {
						questionObj = Object;
						if (vm.weightDropDown === 'Stones') {
							questionObj.arrAns = [];
							questionObj.arrAns[0] = 'st.lb';
							questionObj.arrAns[1] = vm.weightVal;
							questionObj.arrAns[2] = vm.lbsval;
						} else if (vm.weightDropDown === 'Kilograms') {
							questionObj.arrAns = [];
							questionObj.arrAns[0] = 'kg';
							questionObj.arrAns[1] = vm.weightVal;
						} else if (vm.weightDropDown === 'Pound') {
							questionObj.arrAns = [];
							questionObj.arrAns[0] = 'lb';
							questionObj.arrAns[1] = vm.weightVal;
						}

						serviceCallRequired = true;
					}
					if (serviceCallRequired && questionObj.arrAns.length > 1) {
						vm.auraRes = { 'questionID': questionObj.questionId, 'auraAnswers': questionObj.arrAns };
						auraRespSvc.setResponse(vm.auraRes);
						auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function (response) {
							vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
							vm.updateHeightWeightValue();
							var keepGoing = true;
							angular.forEach(vm.auraResponseDataList[index].questions, function (Object, index1) {
								vm.setprogressiveError(vm.auraResponseDataList[sectionIndex].questions[index1]);
								if (Object.questionId === '4') {
									Object.branchComplete = true;
								}
								if (keepGoing && !Object.branchComplete) {
									keepGoing = false;
								}

							});
							if (keepGoing) {
								index = index + 1;
								vm.auraResponseDataList[index].sectionStatus = true;
							}
						}, function () {
							console.log('failed');
						});
					} else {
						angular.forEach(vm.auraResponseDataList[sectionIndex].questions, function (Object, index1) {
							vm.setprogressiveError(vm.auraResponseDataList[sectionIndex].questions[index1]);
						});

					}

				});
			});
		};
		//updating height and weight questions
		vm.updateHeightWeightValue = function () {

			angular.forEach(vm.auraResponseDataList, function (Object, index) {
				vm.questionAlias = Object.sectionName;
				vm.questionlist = Object.questions;
				if (vm.questionAlias === 'QG=Health Questions') {
					vm.healthQuestion = vm.auraResponseDataList[index].questions;
					angular.forEach(vm.healthQuestion, function (question) {
						if (question.classType === 'HeightQuestion') {
							if (question.answerTest.indexOf('Metres') !== -1) {

								vm.heightval = parseInt(question.answerTest.split(',')[0].split('Metres')[0]) * 100 + parseInt(question.answerTest.split(',')[1].split('cm')[0]);
								vm.heightDropDown = 'cm';
								vm.heightOptions = 'm.cm';
							}
							if (question.answerTest.indexOf('Feet') !== -1) {
								vm.heightDropDown = 'Feet';
								vm.heightval = question.answerTest.substring(0, question.answerTest.indexOf('Feet') - 1);
								vm.inches = question.answerTest.substring(question.answerTest.indexOf(',') + 2, question.answerTest.indexOf('in') - 1);
								vm.heightOptions = 'ft.in';
							}

						} else if (question.classType === 'WeightQuestion') {
							if (question.answerTest.indexOf('Kilograms') !== -1) {
								vm.weightDropDown = 'Kilograms';
								vm.weightVal = question.answerTest.substring(0, question.answerTest.indexOf('Kilograms') - 1);
								vm.weighOptions = 'kg';
							}
							if (question.answerTest.indexOf('Pound') !== -1) {
								vm.weightDropDown = 'Pound';
								vm.weightVal = question.answerTest.substring(0, question.answerTest.indexOf('Pound') - 1);
								vm.weighOptions = 'lb';
							}
							if (question.answerTest.indexOf('Stones') !== -1) {
								vm.weightDropDown = 'Stones';
								vm.weightVal = question.answerTest.substring(0, question.answerTest.indexOf('Stones') - 1);
								vm.weighOptions = 'st.lb';
								vm.lbsval = question.answerTest.substring(question.answerTest.indexOf(',') + 2, question.answerTest.indexOf('lbs') - 1);
							}
						}
					});
				}
			});


		};

		vm.collapseUncollapse = function (index, sectionName) {
			if (sectionName === 'QG=Health Questions') {
				vm.submitHeightWeightQuestion(index);
			} else {
				var keepGoing = true;
				angular.forEach(vm.auraResponseDataList[index].questions, function (Object, index1) {
					vm.setprogressiveError(vm.auraResponseDataList[index].questions[index1]);
					if (keepGoing && !Object.branchComplete) {
						keepGoing = false;
					}

				});
				if (keepGoing) {
					index = index + 1;
					if (vm.auraResponseDataList[index].sectionStatus) {
						index = index + 1;
					}
					vm.auraResponseDataList[index].sectionStatus = true;

				}
			}
		};

		vm.stateChanged = function (qId) {
			if (vm.answers[qId]) { //If it is checked
				console.log('id>>' + qId);
			}
		};
		vm.updateFreeText = function (answerValue, questionObj) {
			questionObj.arrAns = [];
			questionObj.arrAns[0] = answerValue;
			questionObj.arrAns[1] = 'accept';
			vm.auraRes = { 'questionID': questionObj.questionId, 'auraAnswers': questionObj.arrAns };
			auraRespSvc.setResponse(vm.auraRes);
			auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function (response) {
				vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
			});
		};
		vm.updateQuestionList = function (questionId, changedQuestion, questionObj) {
			vm.isAuraErroneous = false;
			angular.forEach(vm.auraResponseDataList, function (Object, index) {
				vm.questionlist = Object.questions;
				angular.forEach(vm.questionlist, function (Obj, index1) {
					if (Obj && changedQuestion) {
						if (Obj.questionId === changedQuestion.questionId) {
							vm.auraResponseDataList[index].questions[index1] = changedQuestion;
							vm.progressive(questionId, changedQuestion, questionObj);
						}
					}
				});
			});
		};

		vm.updateRadios = function (answerValue, questionObj) {
			if (questionObj.classType === 'DateQuestion') {
				var pattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

				if (!pattern.test(answerValue)) {
					questionObj.error = true;
					return false;
				}
			}	else if (!answerValue && answerValue !== 0) {
				questionObj.error = true;
				return false;
			}

			questionObj.answerTest = answerValue;

			questionObj.arrAns[0] = answerValue;
			vm.auraRes = { 'questionID': questionObj.questionId, 'auraAnswers': questionObj.arrAns };
			auraRespSvc.setResponse(vm.auraRes);

			auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function (response) {
				vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
				//auraQuestionlist
			}, function () {
				console.log('failed');
			});
		};

		vm.progressive = function (questionid, changedQuestion, questionObj) {
			angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
				if (changedQuestion.questionAlias === Object.sectionName) {
					vm.questionIndex = vm.auraResponseDataList[selectedIndex].questions.indexOf(changedQuestion);
					angular.forEach(vm.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
						if (index <= vm.questionIndex) {
							if (index === vm.questionIndex) {
								var keepGoing = true;
								vm.subprogressive(vm.auraResponseDataList[selectedIndex].questions[index], questionid, keepGoing, questionObj);
							} else {
								vm.setprogressiveError(vm.auraResponseDataList[selectedIndex].questions[index]);
							}


						}

					});
				}

			});
		};

		vm.setprogressiveError = function (baseQuestionObj) {
			if (baseQuestionObj && baseQuestionObj.questionComplete) {
				angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
					angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
						if (subauraQustion.questionComplete) {
							vm.setprogressiveError(subauraQustion);
						} else {
							baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error = true;
							vm.keepGoing = false;
						}
					});

				});
			} else if (baseQuestionObj && baseQuestionObj.classType === 'HeightQuestion' && (!angular.isUndefined(vm.heightval) && vm.heightval.length > 0)) {
				baseQuestionObj.error = false;
			} else if (baseQuestionObj && baseQuestionObj.classType === 'WeightQuestion' && (!angular.isUndefined(vm.weightVal) && vm.weightVal.length > 0)) {
				baseQuestionObj.error = false;
			} else if (baseQuestionObj) {
				baseQuestionObj.error = true;
			}

		};
		vm.subprogressive = function (baseQuestionObj, questionid, keepGoing, updatedQuestionObj) {
			if (baseQuestionObj && baseQuestionObj.questionId < questionid) {
				angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
					angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
						if (!subauraQustion.questionComplete && subauraQustion.questionId < questionid) {
							baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error = true;
						} else if (subauraQustion.questionComplete && subauraQustion.questionId < questionid) {
							vm.subprogressive(subauraQustion, questionid, keepGoing, updatedQuestionObj);
						} else if (subauraQustion.questionComplete && subauraQustion.questionId === questionid) {
							var arrayAnswer = null;
							if (subauraQustion.answerTest.indexOf(',') !== -1) {
								arrayAnswer = subauraQustion.answerTest.split(',');
							} else if (!angular.isUndefined(subauraQustion.answerTest)) {
								arrayAnswer = [];
								arrayAnswer[0] = subauraQustion.answerTest;
							}
							angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].auraAnswer, function (answer, answerIndex) {
								if (arrayAnswer[arrayAnswer.length - 1].indexOf(answer.answerValue) !== -1) {
									vm.checkboxProgressive(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex], answerIndex);
								}
							});
						}
					});
				});
			} else if (baseQuestionObj && baseQuestionObj.questionId === questionid && baseQuestionObj.answerTest === updatedQuestionObj.answerTest) {
				var arrayAnswer = null;
				if (baseQuestionObj.answerTest.indexOf(',') !== -1) {
					arrayAnswer = baseQuestionObj.answerTest.split(',');
				} else if (!angular.isUndefined(baseQuestionObj.answerTest)) {
					arrayAnswer = [];
					arrayAnswer[0] = baseQuestionObj.answerTest;
				}
				angular.forEach(baseQuestionObj.auraAnswer, function (answer, answerIndex) {
					if (arrayAnswer[arrayAnswer.length - 1].indexOf(answer.answerValue) !== -1) {
						vm.checkboxProgressive(baseQuestionObj, answerIndex);
					}
				});
			}

		};

		vm.checkboxProgressive = function (baseQuestionObj, answerIndex) {
			if (baseQuestionObj) {
				angular.forEach(baseQuestionObj.auraAnswer, function (answer, index) {
					if (index < answerIndex) {
						angular.forEach(baseQuestionObj.auraAnswer[index].childQuestions, function (subauraQustion, subSelectedIndex) {
							vm.setprogressiveError(subauraQustion);

						});
					}
				});
			}
		};




		vm.searchValSubmit = function (answerValue, questionObj, parentQuestionObj) {
			questionObj.arrAns = [];
			questionObj.arrAns[0] = questionObj.answerValue.answerValue;
			vm.auraRes = { 'questionID': parentQuestionObj.questionId, 'auraAnswers': questionObj.arrAns };
			auraRespSvc.setResponse(vm.auraRes);
			auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function (response) {
				vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);
			});
		};

		vm.updateCheckBox = function (answerValue, questionObj, index, $event) {
			if((questionObj.answerTest !== '') && (questionObj.answerTest.indexOf('None of the above') < 0) && (answerValue.indexOf('None of the above') > -1)) {
				var savePopupTemplate = '';
				savePopupTemplate += '<div class="ngdialog-content">';
					savePopupTemplate += '<div class="modal-body">';
						savePopupTemplate += '<div class="row  rowcustom ">';
							savePopupTemplate += '<div class="col-sm-12">';
								savePopupTemplate += '<div id="tips_text">';
									savePopupTemplate += '<p>Clicking on "None of the above" will clear your medical disclosures in the above section.</p>';
									savePopupTemplate += '<p class="mt-2">Do you wish to proceed?</p>';
								savePopupTemplate += '</div>';
							savePopupTemplate += '</div>';
						savePopupTemplate += '</div>';
					savePopupTemplate += '</div>';
					savePopupTemplate += '<div class="ngdialog-buttons text-center">';
						savePopupTemplate += '<button type="button" class="btn metlife-btn-primary mr-1 avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
						savePopupTemplate += '<button type="button" class="btn metlife-btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button>';
					savePopupTemplate += '</div>';
				savePopupTemplate += '</div>';
				ngDialog.openConfirm({
					template: savePopupTemplate,
					plain: true,
					className: 'ngdialog-theme-plain custom-width'
				}).then(function (value) {
					vm.submitQuestionOnCheckBoxClick(answerValue, questionObj, index);
				}, function (value) {
					if (value === 'oncancel') {
						angular.element($event.currentTarget).removeClass('active');
						return false;
					}
				});
			} else {
				vm.submitQuestionOnCheckBoxClick(answerValue, questionObj, index);
			}
		};

		vm.submitQuestionOnCheckBoxClick = function(answerValue, questionObj, index) {
			if (questionObj.arrAns.length === 0) {
				var answerArray = questionObj.answerTest.split(',');
				if (answerArray.length > 1) {
					questionObj.arrAns = answerArray;
				} else {
					questionObj.arrAns[0] = questionObj.answerTest;
				}
			}


			var addtoArray = true;
			if (answerValue.indexOf('None of the above') !== -1) {
				questionObj.arrAns = [];
				questionObj.arrAns[0] = answerValue;
			} else {
				for (var i = 0; i < questionObj.arrAns.length; i++) {
					//to remove white space
					questionObj.arrAns[i] = $.trim(questionObj.arrAns[i]);
					if (questionObj.arrAns[i] === answerValue) {
						questionObj.arrAns.splice(i, 1);
						addtoArray = false;
					}
				}
				if (addtoArray === true) {
					questionObj.arrAns.splice(index, 0, answerValue);
				}
			}
			//removing none of the above, if there are other answers
			if (questionObj.arrAns.length > 1) {
				for (var j = 0; j < questionObj.arrAns.length; j++) {
					if (questionObj.arrAns[j].indexOf('None of the above') !== -1) {
						questionObj.arrAns.splice(j, 1);
					}
				}
			}
			vm.auraRes = { 'questionID': questionObj.questionId, 'auraAnswers': questionObj.arrAns };
			auraRespSvc.setResponse(vm.auraRes);
			auraPostSvc.reqObj(vm.urlList.auraPostUrl).then(function (response) {
				vm.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data);

			}, function () {
				console.log('failed');
			});
		};
		vm.checkIncompleteQuestion = function (baseQuestionObj, keepGoing) {
			if (baseQuestionObj.questionComplete && keepGoing) {
				angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
					angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
						if (subauraQustion.questionComplete) {
							vm.setprogressiveError(subauraQustion);
						} else if (!subauraQustion.questionComplete) {
							vm.keepGoing = false;
						}
					});

				});
			} else if (baseQuestionObj.classType === 'HeightQuestion' && (!angular.isUndefined(vm.heightval) && vm.heightval.length > 0)) {
				vm.keepGoing = true;
			} else if (baseQuestionObj.classType === 'WeightQuestion' && (!angular.isUndefined(vm.weightVal) && vm.weightVal.length > 0)) {
				vm.keepGoing = true;
			} else {
				vm.keepGoing = false;
			}

		};

		vm.proceedToNext = function () {
			//check if questions are answered
			vm.coverDetails.lastSavedOn = '';
			vm.erroneousSections = [];
			angular.forEach(vm.auraResponseDataList, function (Object, selectedIndex) {
				vm.keepGoing = true;
				angular.forEach(vm.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
					if (vm.keepGoing) {
						vm.checkIncompleteQuestion(vm.auraResponseDataList[selectedIndex].questions[index], vm.keepGoing);
					}

				});
				if (!vm.keepGoing) {
					vm.erroneousSections.push(Object.sectionName);
				}
			});
			vm.isAuraErroneous = vm.erroneousSections.length > 0;
			if (!vm.isAuraErroneous) {
				submitAuraSvc.requestObj(vm.urlList.submitAuraUrl).then(function (response) {
					vm.auraResponses = response.data;
					if (response.status === 200) {
						if (vm.auraResponses.overallDecision !== 'DCL') {
							clientMatchSvc.reqObj(vm.urlList.clientMatchUrl).then(function (clientMatchResponse) {
								if (clientMatchResponse.data.matchFound) {
									vm.clientmatchreason = '';
									vm.auraResponses.clientMatched = clientMatchResponse.data.matchFound;
									vm.auraResponses.overallDecision = 'RUW';
									vm.information = clientMatchResponse.data.information;
									angular.forEach(vm.information, function (infoObj, index) {
										if (index === vm.information.length - 1) {
											vm.clientmatchreason = vm.clientmatchreason + infoObj.system + ' client match : ' + infoObj.key + ', ';
											vm.clientmatchreason = vm.clientmatchreason.substring(0, vm.clientmatchreason.lastIndexOf(','));
										} else {
											vm.clientmatchreason = vm.clientmatchreason + infoObj.system + ' client match : ' + infoObj.key + ', ';
										}

									});
									vm.auraResponses.clientMatchReason = vm.clientmatchreason;
									vm.auraResponses.specialTerm = false;
								}
								if (vm.fulData !== undefined) {
									if (vm.fulData.fulDeathAmount !== null || vm.fulData.fulDeathAmount !== undefined) {
										vm.auraResponses.deathFul = vm.fulData.fulDeathAmount;
									}
									if (vm.fulData.fulTPDAmount !== null || vm.fulData.fulTPDAmount !== undefined) {
										vm.auraResponses.tpdFul = vm.fulData.fulTPDAmount;
									}
									if (vm.fulData.ipFulAmount !== null || vm.fulData.ipFulAmount !== undefined) {
										vm.auraResponses.ipFul = vm.fulData.ipFulAmount;
									}
								}
								appData.setChangeCoverAuraDetails(vm.auraResponses);
								vm.setHeightWeight();
								$location.path('/summary');
							});
						} else if (vm.auraResponses.overallDecision === 'DCL') {
							if (vm.coverDetails !== null && vm.changeCoverOccDetails !== null && vm.deathAddnlDetails !== null &&
								vm.tpdAddnlDetails !== null && vm.ipAddnlDetails !== null && vm.auraResponses !== null && vm.quotePageDetails.personalDetails !== null) {
								vm.coverDetails.lastSavedOn = '';
								vm.details = {};
								if (vm.fulData !== undefined) {
									if (vm.fulData.fulDeathAmount !== null || vm.fulData.fulDeathAmount !== undefined) {
										vm.auraResponses.deathFul = vm.fulData.fulDeathAmount;
									}
									if (vm.fulData.fulTPDAmount !== null || vm.fulData.fulTPDAmount !== undefined) {
										vm.auraResponses.tpdFul = vm.fulData.fulTPDAmount;
									}
									if (vm.fulData.ipFulAmount !== null || vm.fulData.ipFulAmount !== undefined) {
										vm.auraResponses.ipFul = vm.fulData.ipFulAmount;
									}
								}
								vm.details.addnlDeathCoverDetails = vm.deathAddnlDetails;
								vm.details.addnlTpdCoverDetails = vm.tpdAddnlDetails;
								vm.details.addnlIpCoverDetails = vm.ipAddnlDetails;
								vm.details.occupationDetails = vm.changeCoverOccDetails;
								var temp = angular.extend(vm.details, vm.coverDetails);
								var aura = angular.extend(temp, vm.auraResponses);
								if (aura.corpFundCode === null || aura.corpFundCode === undefined) {
									vm.coverDetails = inputDataService.getCoverDetails();
									aura.corpFundCode = vm.coverDetails.fundId;
								}
								var submitObject = angular.extend(aura, vm.quotePageDetails.personalDetails);
								submitObject.smoker = vm.details.occupationDetails.smoker;
								submitObject.industryName=fetchIndustryList.getIndustryName(vm.quotePageDetails.occupationDetails.industryCode);
								auraRespSvc.setResponse(submitObject);
								submitEapplySvc.submitObj(vm.urlList.submitEapplyUrl).then(function (response) {
									appData.setPDFLocation(response.data.clientPDFLocation);
									PersistenceService.setPDFLocation(response.data.clientPDFLocation);
									appData.setNpsUrl(response.data.npsTokenURL);
									appData.setSummaryPageAuradecision(vm.auraResponses);
									$location.path('/decision');
								}, function (err) {
									console.log('Error saving the cover ' + err);
								});
							}
						}
					}
				});
			}
			if (vm.erroneousSections.length) {
				vm.formatErrorMsg();
			}
		};

		vm.formatErrorMsg = function () {
			for (var v = 0; v < vm.erroneousSections.length; v++) {
				vm.erroneousSections[v] = vm.erroneousSections[v].replace('QG=', '');
			}

		};
		// Save
		var appNum;
		appNum = fetchAppNumberSvc.getAppNumber();
		vm.saveAura = function () {
			if (vm.coverDetails != null && vm.changeCoverOccDetails !== null && vm.deathAddnlDetails !== null && vm.tpdAddnlDetails !== null && vm.ipAddnlDetails !== null && vm.quotePageDetails.personalDetails !== null) {
				if (vm.coverDetails.corpFundCode === null || vm.coverDetails.corpFundCode === undefined) {
					var fundDetails = inputDataService.getCoverDetails();
					vm.coverDetails.corpFundCode = fundDetails.fundId;
				}
				vm.coverDetails.lastSavedOn = 'AuraPage';
				vm.details = {};
				vm.details.addnlDeathCoverDetails = vm.deathAddnlDetails;
				vm.details.addnlTpdCoverDetails = vm.tpdAddnlDetails;
				vm.details.addnlIpCoverDetails = vm.ipAddnlDetails;
				vm.details.occupationDetails = vm.changeCoverOccDetails;
				//vm.auraDetails
				var temp = angular.extend(vm.details, vm.coverDetails);
				var saveAuraObject = angular.extend(temp, vm.quotePageDetails.personalDetails);

				if (vm.changeCoverOccDetails.smoker === 'Yes') {
					saveAuraObject.occupationDetails.smokerQuestion = 1;
				} else {
					saveAuraObject.occupationDetails.smokerQuestion = 2;
				}
				auraRespSvc.setResponse(saveAuraObject);
				saveEapplyData.reqObj(vm.urlList.saveCorporateEapply, saveAuraObject).then(function (response) {
					vm.auraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>' + appNum + '</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
				});
			}
			vm.heightval = vm.heightval || null;
			vm.weightVal = vm.weightVal || null;
			if (vm.heightval !== null || vm.weightVal !== null) {
				vm.submitHeightWeightQuestion(2);
			}
		};

		vm.auraSaveAndExitPopUp = function (hhText) {
			var popUpContent = '<div class="ngdialog-content"><div class="modal-body">';
			popUpContent += '<h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4>';
			popUpContent += '<!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">' + hhText + '</div>  <p></p>  </div></div><!-- Row ends --></div>';
			popUpContent += '<div class="ngdialog-buttons aligncenter">';
			popUpContent += '<button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button>';
			popUpContent += '</div></div>';
			var dialog1 = ngDialog.open({
				template: popUpContent,
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function (value) {
					var url = '/landing';
					$location.path(url);
					return true;
				},
				plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};


		vm.clickToOpen = function (hhText) {
			var helpfulHintContent = '<div class="ngdialog-content">';
			helpfulHintContent += '<div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4>';
			helpfulHintContent += '<!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">' + hhText + '</div><p></p></div></div><!-- Row ends --></div>';
			helpfulHintContent += '<div class="row"><div class="col-sm-4"></div>';
			helpfulHintContent += '<div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" style="width: 100%;font-weight: normal !important; font-size: 18px !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Close</button></div>';
			helpfulHintContent += '<div class="col-sm-4"></div></div></div>';
			var dialog = ngDialog.open({
			template: helpfulHintContent,
				className: 'ngdialog-theme-plain',
				plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};

		vm.isCollapsible = function (event, sectionStatus) {

			if (sectionStatus === false) {
				event.stopPropagation();
				return false;
			}

		};


	}
})(angular);

   /*Aura Page Controller Ends*/
