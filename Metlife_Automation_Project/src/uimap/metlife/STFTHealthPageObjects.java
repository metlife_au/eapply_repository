package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTHealthPageObjects extends PageObjectLibrary{
		
	//public static final By txtheight = getObjectIdentifaction("stft.yourhealth.height.text.id");
	//public static final By txtweight = getObjectIdentifaction("stft.yourhealth.weight.text.id");
	//public static final By chklast3yrs = getObjectIdentifaction("stft.yourhealth.abturhealth.last3yrs.none.checkbox.css");
	//public static final By chklast5yrs = getObjectIdentifaction("stft.yourhealth.abturhealth.last5yrs.none.checkbox.css");
	
	
	// exclusion
	public static final By chkmedicaladvicebone = getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.bone.checkbox.css");
	public static final By chkmedicaladvicelossbone = getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.lossbone.checkbox.css");
	public static final By rdadvicepractitionerYes= getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.yes.radio.css");
	public static final By rdfracturesNo= getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.no.radio.css");
	
	// loading
	public static final By chklast3yrslungissue = getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.lung.checkbox.css");
	public static final By chklast3yrslungissueasthma = getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.asthma.checkbox.css");
	public static final By rdasthmaModerate= getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.condition.moderate.radio.css");
	public static final By rdasthmaworsenedNo= getObjectIdentifaction("stft.yourhealth.abturhealth.medicaladvice.condition.worsened.no.radio.css");
	
	
/*	public static final By rdlifestyleNo = getObjectIdentifaction("stft.yourhealth.lifestyle.no.radio.css");
	public static final By txtalchohol = getObjectIdentifaction("stft.yourhealth.alchoholicdrink.text.id");
	public static final By rdalchoholreduceNo = getObjectIdentifaction("stft.yourhealth.alchoholicdrink.no.radio.css");
	public static final By rdhivNo = getObjectIdentifaction("stft.yourhealth.hiv.no.radio.css");
	public static final By rdhivYes = getObjectIdentifaction("stft.yourhealth.hiv.yes.radio.css");
	public static final By rdhivriskNo = getObjectIdentifaction("stft.yourhealth.hivrisk.no.radio.css");*/
	
/*	public static final By rdgeneralNo = getObjectIdentifaction("stft.yourhealth.generalquestion.no.radio.css");
	public static final By rdinsuranceexclusionNo = getObjectIdentifaction("stft.yourhealth.insurancedetails.exclusion.no.radio.css");
	public static final By rdinsuranceclaimNo = getObjectIdentifaction("stft.yourhealth.insurancedetails.claim.no.radio.css");
	public static final By btnNext = getObjectIdentifaction("stft.yourhealth.next.button.css");
	public static final By btnsummaryNext = getObjectIdentifaction("stft.summary.next.button.css");*/
}
