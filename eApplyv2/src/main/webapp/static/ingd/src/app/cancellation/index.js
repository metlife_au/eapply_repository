import angular from 'angular';

// Create the module where our functionality can attach to
let cancellationModule = angular.module('app.cancellation', []);

// Include our UI-Router config settings
import CancellationConfig from './cancellation.config';
cancellationModule.config(CancellationConfig);


// Controllers
import CancellationCtrl from './cancellation.controller';
cancellationModule.controller('CancellationCtrl', CancellationCtrl);

import CancelAckCtrl from './cancellation.acknowledge.controller';
cancellationModule.controller('CancelAckCtrl', CancelAckCtrl);

import CancelDecisionCtrl from './cancellation.decision.controller';
cancellationModule.controller('CancelDecisionCtrl', CancelDecisionCtrl);


export default cancellationModule;
