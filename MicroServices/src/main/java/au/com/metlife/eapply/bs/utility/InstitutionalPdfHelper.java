package au.com.metlife.eapply.bs.utility;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.pdf.CostumPdfAPI;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureQuestionAns;
import au.com.metlife.eapply.bs.pdf.pdfObject.DisclosureStatement;
import au.com.metlife.eapply.bs.pdf.pdfObject.MemberProductDetails;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.bs.pdf.pdfObject.TransferPreviousQuestionAns;
import au.com.metlife.eapply.helper.CorporateHelper;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.Rating;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;
import au.com.metlife.eapply.underwriting.response.model.Rule;


public class InstitutionalPdfHelper {
	private InstitutionalPdfHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(InstitutionalPdfHelper.class);
	private static final String DEATH = "Death";
public static void displayInstitutionalDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException, MalformedURLException, IOException{
	log.info("Display Institutional details start");

	

	if("coverDetails".equalsIgnoreCase(pdfObject.getChannel())){		
				if(!"insurancecover".equalsIgnoreCase(pdfObject.getCalculatorFlag())){
					displayHeaderNote(pdfAPI, document, pdfObject,pdfType,"Quotation Summary");
				}
				
				/*displayPersonalDetails(pdfAPI, document, pdfObject, pdfType); */
	            if(!pdfObject.isQuickQuoteRender()){
	            	displayPersonalDetails(pdfAPI, document, pdfObject, pdfType); 
	            }
	            if("insurancecover".equalsIgnoreCase(pdfObject.getCalculatorFlag())){
	            	displayInsuranceCoverSummary(pdfAPI, document, pdfObject, pdfType);
	            	displayFinancialDetails(pdfAPI, document, pdfObject, pdfType);
	            	displayInsurancePersonalDetails(pdfAPI, document, pdfObject, pdfType);
	            }
	            else{
	            	displayCoverSummary(pdfAPI, document, pdfObject, pdfType);
	            }
				
				
				if(pdfObject.getRequestType() != null && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())
						&& null!=pdfObject.getFundId() && (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()))){		
					displayPreviousCoverDetails(pdfAPI, document, pdfObject, pdfType);
				}
				if(!"insurancecover".equalsIgnoreCase(pdfObject.getCalculatorFlag())){
				displayQuotationDetails(pdfAPI, document, pdfObject, pdfType);
				}
				//Added for premium calculator
				if(pdfObject.isQuickQuoteRender() && "HOST".equalsIgnoreCase(pdfObject.getFundId())){
					premiumDeclarationDetails(pdfObject.getDisclosureStatementListPc(), pdfAPI, document,pdfObject);
				}
			/*}*/

		}else{
			if(!"INGD".equalsIgnoreCase(pdfObject.getFundId())) {
			displayHeaderNote(pdfAPI, document, pdfObject,pdfType,"Summary of information provided");
			}
			else {
				displayINGDHeaderNote(pdfAPI, document, pdfObject,pdfType,"Summary of your Application");
			}
			if(!"INGD".equalsIgnoreCase(pdfObject.getFundId())) {
				displayPersonalDetails(pdfAPI, document, pdfObject, pdfType);
				}
				else {
					displayINGDPersonalDetails(pdfAPI, document, pdfObject, pdfType);
				}

			/*if(null!=pdfObject.getFundId() && "HOST".equalsIgnoreCase(pdfObject.getFundId())){
				 declarationDetails( pdfObject.getDisclosureStatementList(), pdfAPI, document);  
			}*/			
			if(!"INGD".equalsIgnoreCase(pdfObject.getFundId())) {
				CommonPDFHelper.declarationDetails( pdfObject.getDisclosureStatementList(), pdfAPI, document);  
				}
				else {
					CommonPDFHelper.iNGDdeclarationDetails( pdfObject.getDisclosureStatementList(), pdfAPI, document);  
				}
			
			if(!"INGD".equalsIgnoreCase(pdfObject.getFundId())) {
				displayCoverSummary(pdfAPI, document, pdfObject, pdfType);	
				}
				else {
					displayINGDCoverSummary(pdfAPI, document, pdfObject, pdfType);	
				}
			
			if(pdfObject.getRequestType() != null && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())
					&& !"INGD".equalsIgnoreCase(pdfObject.getFundId())/*&& null!=pdfObject.getFundId() && ("HOST".equalsIgnoreCase(pdfObject.getFundId()) || "FIRS".equalsIgnoreCase(pdfObject.getFundId()) || "SFPS".equalsIgnoreCase(pdfObject.getFundId()))*/){		
				displayPreviousCoverDetails(pdfAPI, document, pdfObject, pdfType);
			}
			if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) && (pdfObject.isLoadingInd() || pdfObject.isExclusionInd()))
			{
				float[] f = { .9f, 0.1f }; 
	       		PdfPCell pdfPCelAck = null;
	       		PdfPTable pdfPTableAck = new PdfPTable( f);
	       		String ackText = "I acknowledge the <%special%> applied to my premium.I further understand that these <%special%> only applies to the increased portion of the cover associated with this application for Death, TPD or IP cover,unless I have updated the benefit and/or waiting period for my IP cover, in which case it will apply to the full IP cover.";
	       		
	       		ackText = ackText.replace("<%special%>", (pdfObject.isLoadingInd() && pdfObject.isExclusionInd())?"exclusion(s) and loading(s)":(pdfObject.isLoadingInd() && !pdfObject.isExclusionInd())?"loading(s)": "exclusion(s)");
	       		pdfPCelAck = new PdfPCell(
	            pdfAPI.addParagraph( ackText, new Font(pdfAPI.getSwsLig() , 8,0,pdfAPI.getFont_color())));                  
	       		
	            pdfAPI.disableBorders( pdfPCelAck);
	            pdfPCelAck.setPadding( 4);
	            pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
	            pdfPTableAck.addCell( pdfPCelAck);
	            PdfPCell pdfPCell1 = new PdfPCell(
	            pdfAPI.addParagraph( "Yes", new Font(pdfAPI.getSwsMed(), 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell1);
	            pdfPCell1.setPadding( 4);
	            pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
	            pdfPTableAck.addCell( pdfPCell1);
	            document.add( pdfPTableAck);
	               
	            PdfPTable pdfPEmptyTable = new PdfPTable( 1);
	   	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	   	        pdfAPI.disableBorders( pdfPEmptyCell3);
	   	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	   	        document.add( pdfPEmptyTable);
	   	        
	   	        pdfPEmptyTable = new PdfPTable( 1);
	   	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	   	        pdfAPI.disableBorders( pdfPEmptyCell3);
	   	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	   	        document.add( pdfPEmptyTable);
			}
			else
			{
			
			if(pdfObject.isLoadingInd()){
        		float[] f = { .9f, 0.1f }; 
        		 PdfPCell pdfPCelAck = null;
        		PdfPTable pdfPTableAck = new PdfPTable( f);    		                    
        		pdfPCelAck = new PdfPCell(
                        pdfAPI.addParagraph( "I acknowledge the loading(s) applied to my premium.", new Font(pdfAPI.getSwsLig() , 8,0,pdfAPI.getFont_color())));                  
        		
                pdfAPI.disableBorders( pdfPCelAck);
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                PdfPCell pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes", new Font(pdfAPI.getSwsMed(), 8,0, pdfAPI.getFont_color())));
                pdfAPI.disableBorders( pdfPCell1);
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
                
              	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
    	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
    	        
    	        pdfPEmptyTable = new PdfPTable( 1);
    	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
        	}
			if(pdfObject.isExclusionInd()){
        		float[] f = { .9f, 0.1f }; 
        		 PdfPCell pdfPCelAck = null;
        		PdfPTable pdfPTableAck = new PdfPTable( f);    		                    
        		pdfPCelAck = new PdfPCell(
                        pdfAPI.addParagraph( "I acknowledge the applied exclusion(s) applied to my cover.", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));                  
                
                pdfAPI.disableBorders( pdfPCelAck);
                pdfPCelAck.setPadding( 4);
                pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
                pdfPTableAck.addCell( pdfPCelAck);
                PdfPCell pdfPCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Yes",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                pdfAPI.disableBorders( pdfPCell1);
                pdfPCell1.setPadding( 4);
                pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
                pdfPTableAck.addCell( pdfPCell1);
                document.add( pdfPTableAck);
                
              	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
    	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
    	        
    	        pdfPEmptyTable = new PdfPTable( 1);
    	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
    	        pdfAPI.disableBorders( pdfPEmptyCell3);
    	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
    	        document.add( pdfPEmptyTable);
        	}
			}
		}
		
		PdfPTable pdfSingleSpaceTable = new PdfPTable( 1);
        PdfPCell pdfPSingleSpaceCell = new PdfPCell( pdfAPI.addSingleCell());
        pdfAPI.disableBorders( pdfPSingleSpaceCell);
        pdfSingleSpaceTable.addCell( pdfPSingleSpaceCell);
        document.add(pdfSingleSpaceTable);  
        
		if("UW".equalsIgnoreCase(pdfType)){		
			 displayPreExistingTerm(pdfAPI, document, pdfObject);
			 
			 
			 if(null!=pdfObject.getMemberDetails().getResponseObject()){
				 CommonPDFHelper.dispalyRuleDecision(pdfAPI, document, pdfObject, pdfObject.getMemberDetails().getResponseObject());
			 }			
		}
		if(!"coverDetails".equalsIgnoreCase(pdfObject.getChannel())){		
			if(!"INGD".equalsIgnoreCase(pdfObject.getFundId())) {
				displayQuotationDetails(pdfAPI, document, pdfObject, pdfType);
				}
				else {
				displayINGDQuotationDetails(pdfAPI, document, pdfObject, pdfType);  
				}
			
			
		}
		/*if(!"coverDetails".equalsIgnoreCase(pdfObject.getChannel()) && pdfObject.getRequestType() != null && pdfObject.getRequestType() != "" && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){		
			displayPreviousCoverDetails(pdfAPI, document, pdfObject, pdfType);
		}*/
		log.info("Display Institutional details finish");
	
}
	
	/**
	 * Description: displayPreExistingTerm in the PDF Section
	 * 
	 * @param
	 * java.lang.String,java.lang.String,,java.lang.String,,java.lang.String,
	 * com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
	 * @return void
	 * @throws DocumentException
	 * @throws DocumentException,MetlifeException,
	 *             IOException
	 */
	private static void displayPreExistingTerm(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject) throws DocumentException {     
	    log.info("Display pre existing term start");
	    PdfPTable pdfPEmptyTable = new PdfPTable( 1);
	    PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    float[] decFloat = { 0.8f, 0.2f };
	    
	    PdfPTable pdfDecisionTable = new PdfPTable( decFloat);
	    PdfPCell pdfPDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "Pre-Existing terms", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	    pdfPDecisionCell.setBorderWidth( 1);
	    pdfPDecisionCell.setBorderColor( pdfAPI.getBorder_color());
	    pdfAPI.disableBorders( pdfPDecisionCell);
	    if("INGD".equalsIgnoreCase(pdfObject.getFundId())) {
	    	 pdfPDecisionCell.setBackgroundColor(pdfAPI.getHeader_color());
	    }
	    else {
	    	 pdfPDecisionCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
	    }
	    pdfPDecisionCell.setPaddingBottom( 6);
	    pdfPDecisionCell.setVerticalAlignment( Element.ALIGN_CENTER);
	    pdfDecisionTable.addCell( pdfPDecisionCell);
	    pdfPDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	    pdfAPI.disableBorders( pdfPDecisionCell);
	    pdfDecisionTable.addCell( pdfPDecisionCell);
	    document.add( pdfDecisionTable);
	    
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    float[] f1 = { 0.8f };
	    PdfPTable pdfDecisionTable1 = new PdfPTable( f1);
	    PdfPCell pdfDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "Exclusions:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	    pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    document.add( pdfDecisionTable1);
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    pdfDecisionTable1 = new PdfPTable( f1);	 
	     pdfDecisionCell = new PdfPCell(
	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getExistingExclusion(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    document.add( pdfDecisionTable1);
	    
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    pdfDecisionTable1 = new PdfPTable( f1);
	    pdfDecisionCell = new PdfPCell(
	            pdfAPI.addParagraph( "Loadings:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	    pdfDecisionCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    document.add( pdfDecisionTable1);
	    pdfPEmptyTable = new PdfPTable( 1);
	    pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	    pdfAPI.disableBorders( pdfPEmptyCell3);
	    pdfPEmptyTable.addCell( pdfPEmptyCell3);
	    document.add( pdfPEmptyTable);
	    
	    pdfDecisionTable1 = new PdfPTable( f1);
	    pdfDecisionCell = new PdfPCell(
	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getExistingLoading(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	    pdfAPI.disableBorders( pdfDecisionCell);
	    pdfDecisionTable1.addCell( pdfDecisionCell);
	    
	    document.add( pdfDecisionTable1);   
	    
	    log.info("Display pre existing term finish");
	   
	}
	
	public static void displayPreviousCoverDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		log.info("Display Transfer Previous cover details");
		PdfPTable pdfPrevTransferTable = null;
		TransferPreviousQuestionAns transferPreviousQuestionAns = null;
		PdfPCell pdfCell = null;
		
		   	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);
	        
	        pdfPEmptyTable = new PdfPTable( 1);
	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);
	        
	        if(!"UW".equalsIgnoreCase(pdfType)){
	        	 pdfPEmptyTable = new PdfPTable( 1);
	 	        PdfPCell pdfPCell;
	        		 pdfPCell= new PdfPCell(
		 	                pdfAPI.addParagraph( "Your previous cover details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        	 
                /*PdfPCell pdfPCell = new PdfPCell(*/
                /*pdfAPI.addParagraph( "Your application details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));*/
	 	        pdfAPI.disableBorders( pdfPCell);
	 	        pdfPCell.setPadding( 4);
	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	        pdfPEmptyTable.addCell( pdfPCell);
	 	        document.add( pdfPEmptyTable);
	        }
	       
	        
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] prevFloat = { 0.8f, 0.2f };
	        	  pdfPrevTransferTable = new PdfPTable( prevFloat);
	        }else{
	        	  pdfPrevTransferTable = new PdfPTable( 1);
	        }
	        
	        PdfPCell pdfPrevTransferCell = new PdfPCell(
	                pdfAPI.addParagraph( "Transfer Cover Details",  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	        pdfPrevTransferCell.setBorderWidth( 1);
	        /*pdfPrevTransferCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
	        pdfAPI.disableBorders( pdfPrevTransferCell);
	        pdfPrevTransferCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
	        pdfPrevTransferCell.setPaddingBottom( 6);
	        pdfPrevTransferCell.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPrevTransferTable.addCell( pdfPrevTransferCell);
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 pdfPrevTransferCell = new PdfPCell(
	 	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS, new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	 	        pdfPrevTransferCell.setBorderWidth( 1);
	 	        /*pdfPrevTransferCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
	 	        pdfAPI.disableBorders( pdfPrevTransferCell);
	 	        pdfPrevTransferCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
	 	        pdfPrevTransferCell.setPaddingBottom( 6);
	 	        pdfPrevTransferCell.setVerticalAlignment( Element.ALIGN_CENTER);
	 	        pdfPrevTransferTable.addCell( pdfPrevTransferCell);
	        }
	       
	        document.add( pdfPrevTransferTable);
	       
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = {0.05f, 0.3f, 0.45f, .2f  };
	        	 pdfPrevTransferTable = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.6f, 0.35f };
	        	 pdfPrevTransferTable = new PdfPTable( f1);
	        }        
		
		if(null!=pdfObject.getTransferPrevQuestionList()){
			for(int itr =0 ;itr<pdfObject.getTransferPrevQuestionList().size();itr++){
				transferPreviousQuestionAns = pdfObject.getTransferPrevQuestionList().get(itr);
				
				pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfCell);
	            
	            pdfPrevTransferTable.addCell( pdfCell);
	            pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( transferPreviousQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfCell);
	            pdfPrevTransferTable.addCell( pdfCell);
	            
	            pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( transferPreviousQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfCell.setNoWrap(Boolean.FALSE);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	pdfCell.setBorderColor( pdfAPI.getBorder_color());
	 	            pdfCell.disableBorderSide( 1);
	 	            pdfCell.disableBorderSide( 3);
	 	            pdfCell.disableBorderSide( 4);
	            }else{
	            	pdfAPI.disableBorders( pdfCell);
	            }      
	            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
	            /*pdfCell.setNoWrap( Boolean.TRUE);*/
	            pdfPrevTransferTable.addCell( pdfCell);
	            
	            if("UW".equalsIgnoreCase(pdfType)){
	            	pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            pdfPrevTransferTable.addCell( pdfCell);
		            /*if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !transferPreviousQuestionAns.getQuestiontext().equalsIgnoreCase("Reason fosr application for additional cover:")){
		            	pdfPrevTransferTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPrevTransferTable.addCell( pdfCell);
		            }*/
	            }
	            
			}
		}
		document.add(pdfPrevTransferTable);
	    log.info("Display Transfer Previous cover details finish");
	}

	public static void displayQuotationDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		log.info("Display quotation details start");
		PdfPTable pdfPOccupationTable = null;
		DisclosureQuestionAns disclosureQuestionAns = null;
		PdfPCell pdfCell = null;
		
		   	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
	        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);
	        
	        pdfPEmptyTable = new PdfPTable( 1);
	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);
	        
	        if(!"UW".equalsIgnoreCase(pdfType)){
	        	 pdfPEmptyTable = new PdfPTable( 1);
	 	        PdfPCell pdfPCell;
	        	 if(pdfObject.isQuickQuoteRender()){
	        		 pdfPCell= new PdfPCell(
		 	                pdfAPI.addParagraph( "Your quotation details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        	 }else{
	        		 pdfPCell= new PdfPCell(
	        				 pdfAPI.addParagraph( "Your application details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color() )));
	        	 }
	        	 
                /*PdfPCell pdfPCell = new PdfPCell(*/
                /*pdfAPI.addParagraph( "Your application details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));*/
	 	        pdfAPI.disableBorders( pdfPCell);
	 	        pdfPCell.setPadding( 4);
	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	        pdfPEmptyTable.addCell( pdfPCell);
	 	        document.add( pdfPEmptyTable);
	        }
	       
	        
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] occupationFloat = { 0.8f, 0.2f };
	        	  pdfPOccupationTable = new PdfPTable( occupationFloat);
	        }else{
	        	  pdfPOccupationTable = new PdfPTable( 1);
	        }
	        
	        PdfPCell pdfPOccupationCell = new PdfPCell(
	                pdfAPI.addParagraph( "Quotation details",  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	        pdfPOccupationCell.setBorderWidth( 1);
	        /*pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
	        pdfAPI.disableBorders( pdfPOccupationCell);
	        pdfPOccupationCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
	        pdfPOccupationCell.setPaddingBottom( 6);
	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPOccupationTable.addCell( pdfPOccupationCell);
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 pdfPOccupationCell = new PdfPCell(
	 	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS, new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	 	        pdfPOccupationCell.setBorderWidth( 1);
	 	        /*pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
	 	        pdfAPI.disableBorders( pdfPOccupationCell);
	 	        pdfPOccupationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
	 	        pdfPOccupationCell.setPaddingBottom( 6);
	 	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
	 	        pdfPOccupationTable.addCell( pdfPOccupationCell);
	        }
	       
	        document.add( pdfPOccupationTable);
	       
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	 pdfPOccupationTable = new PdfPTable( f1);
	        }        
		
		if(null!=pdfObject.getDisclosureQuestionList()){
			for(int itr =0 ;itr<pdfObject.getDisclosureQuestionList().size();itr++){
				disclosureQuestionAns = pdfObject.getDisclosureQuestionList().get(itr);
				
				pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfCell);
	            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }
	            /*pdfPOccupationTable.addCell( pdfCell);*/
	            pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfCell);
	           /* pdfPOccupationTable.addCell( pdfCell);*/
	            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }
	            pdfCell = new PdfPCell(
	                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfCell.setNoWrap(Boolean.FALSE);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
	 	            pdfCell.disableBorderSide( 1);
	 	            pdfCell.disableBorderSide( 3);
	 	            pdfCell.disableBorderSide( 4);
	            }else{
	            	pdfAPI.disableBorders( pdfCell);
	            }      
	            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
	            /*pdfCell.setNoWrap( Boolean.TRUE);*/
	            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
	            	pdfPOccupationTable.addCell( pdfCell);
	            }
	            /*pdfPOccupationTable.addCell( pdfCell);*/
	            if("UW".equalsIgnoreCase(pdfType)){
	            	pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
	            }
	            
			}
		}
		document.add(pdfPOccupationTable);
	    log.info("Display quotation details finish");
	}

	private static String getOccupationDecision(ResponseObject responseObject){
		log.info("Get occupation decision start");
		StringBuilder strBuff = new StringBuilder();
        if (null != responseObject && responseObject.getRules()!=null) {          
            for (int itr = 0; itr < responseObject.getRules().getListRule().size(); itr++) {
                Rule rule =  responseObject.getRules().getListRule().get( itr);
                if (null != rule
                        && "QG=Occupation Details".equalsIgnoreCase( rule.getAlias())) {
                    if (null != rule.getQuestionTypeId()
                            && !rule.getQuestionTypeId().equalsIgnoreCase( "40")
                            && rule.isVisible())
                        if (null != rule
                                && null != rule.getAnswer()
                                && null != rule.getAnswer().getImpId()
                                && "27822446".equalsIgnoreCase( rule.getAnswer().getImpId())
                                && null != rule.getAnswer().getRiskType()
                                && "OCCUPATION".equalsIgnoreCase( rule.getAnswer().getRiskType())) {
                            
                            if (null != rule.getAnswer().getListUWDecisions()) {
                                for (int decItr = 0; decItr < rule.getAnswer().getListUWDecisions().size(); decItr++) {
                                    Decision decision =  rule.getAnswer().getListUWDecisions().get( decItr);
                                    if (!"ACC".equalsIgnoreCase( decision.getDecision())) {
                                        if (null != decision.getProductName()
                                                && "".equalsIgnoreCase( decision.getProductName())
                                                && decision.getProductName().length() == 0) {
                                            strBuff.append( "All Product:");
                                        } else {
                                            strBuff.append( decision.getProductName());
                                            strBuff.append( ":");
                                        }
                                        strBuff.append( decision.getDecision());
                                        strBuff.append( ",");
                                        for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
                                            Rating rating =  decision.getListRating().get( ratingItr);
                                            strBuff.append( rating.getValue());
                                            strBuff.append( ",");
                                            strBuff.append( rating.getReason());
                                            strBuff.append( ",");
                                        }
                                        if (null != decision.getReasons()
                                                && decision.getReasons().length > 0) {
                                            strBuff.append( "Reason:");
                                            String[] reason = decision.getReasons();
                                            for (int reasonItr = 0; reasonItr < reason.length; reasonItr++) {
                                                strBuff.append( reason[reasonItr]);
                                            }
                                            
                                        }
                                    }
                                    
                                }
                                
                            }
                            
                        }
                    
                }
            }
        }
        log.info("Get occupation decision finish");
        return strBuff.toString();
	}

	public static void displayHeaderNote(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType, String headerText) throws MalformedURLException, IOException, DocumentException{
		log.info("Display header note start");
		Image gif;
		 PdfPTable pdfPTable2 = null;
		 PdfPCell pdfPCell4 = null;
		 PdfPTable pdfSpaceTable1 = null;
		 PdfPCell pdfPSpaceCell1 = null;
		 PdfPTable pdfPreffConDtsTable = null; 
         PdfPCell pdfPreffConDetsCell3 = null;
		float[] tableSize = { .7f, .3f };
	    PdfPTable pdfPTable = new PdfPTable( tableSize);
	    PdfPCell pdfPCell = new PdfPCell(
	            pdfAPI.addParagraph(headerText, new Font(pdfAPI.getSwsMed(),18,0,pdfAPI.getHeader_color())));
	    pdfAPI.disableBorders( pdfPCell);
	    pdfPCell.setPadding( 2);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	    pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	    pdfPTable.addCell( pdfPCell);
	    pdfPCell = new PdfPCell();
	    if (null != pdfObject.getLogPath()) {
	    	 gif = Image.getInstance(pdfObject.getLogPath());
	    	  gif.setAlignment( Image.ALIGN_RIGHT);
	    	  if(MetlifeInstitutionalConstants.FUND_MTAA.equalsIgnoreCase(pdfObject.getFundId())){
	    		  pdfPCell.setFixedHeight(35f);
	    	  }else {
	    		  pdfPCell.setFixedHeight(53f);
	    	  }
	    	  pdfPCell.setImage( gif);
	    	  if(MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(pdfObject.getFundId())){
	    		  pdfPCell.setPaddingLeft(100f);
	    	  }
	    	 
	    }	
	    
	    pdfPCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	    
	   /* pdfPCell.setPaddingRight( 4);
        pdfPCell.setPadding( 4);
        pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);*/
        pdfAPI.disableBorders( pdfPCell);
        pdfPTable.addCell( pdfPCell);
        document.add(pdfPTable);
        
        float[] f1 = { 0.2f, 0.8f };		
        pdfPTable = new PdfPTable(f1);
        if(pdfObject.isQuickQuoteRender()){
        	pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_QUOT, new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
        }else{
        	pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
        }
		/*pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, pdfObject.getColorStyle(),7));*/
		pdfAPI.disableBorders(pdfPCell);
		pdfPCell.setPadding(4);
		/*pdfPCell.setBackgroundColor(pdfAPI.getBackground_color());*/
		pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);		
		pdfPTable.addCell(pdfPCell);	
		pdfPCell = new PdfPCell(pdfAPI.addParagraph(CommonPDFHelper.getTodaysDate(),new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
		pdfAPI.disableBorders(pdfPCell);
		pdfPCell.setPadding(4);
		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);
		/*pdfPCell.setBackgroundColor(pdfAPI.getBackground_color());*/
		pdfPTable.addCell(pdfPCell);
		
	
		if((!pdfObject.isQuickQuoteRender()) || (pdfObject.isQuickQuoteRender() && pdfObject.getApplicationNumber()!=null &&pdfObject.getApplicationNumber()!="" )){
		pdfPCell = new PdfPCell(pdfAPI.addParagraph("Application number:", new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
		pdfAPI.disableBorders(pdfPCell);
		/*pdfPCell.setPadding(4);*/
		pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);	
		/*pdfPCell.setBackgroundColor(pdfAPI.getBackground_color());*/
		pdfPTable.addCell(pdfPCell);	
		pdfPCell = new PdfPCell(pdfAPI.addParagraph(pdfObject.getApplicationNumber(),new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
		pdfAPI.disableBorders(pdfPCell);
		/*pdfPCell.setPadding(4);*/
		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);	
		/*pdfPCell.setBackgroundColor(pdfAPI.getBackground_color());*/
		pdfPTable.addCell(pdfPCell);
		}
		pdfPCell.setNoWrap(Boolean.FALSE);
		document.add(pdfPTable);

		pdfPTable = new PdfPTable(1);
		PdfPCell pdfPSpaceCell = new PdfPCell(pdfAPI.addSingleCell());		
		pdfAPI.disableBorders(pdfPSpaceCell);
		pdfPTable.addCell(pdfPSpaceCell);
		document.add(pdfPTable);    
		
		
		if ("RUW".equalsIgnoreCase( pdfObject.getApplicationDecision()) || ("VICS1".equalsIgnoreCase(pdfObject.getFundId()) && "UW".equalsIgnoreCase(pdfType))) {			
			
			 if("UW".equalsIgnoreCase(pdfType)){
				 float[] prefConFloat = { .8f, .2f };
				 pdfPTable2 = new PdfPTable( prefConFloat);
			 }else{
				 pdfPTable2 = new PdfPTable( 1);
			 }
                      
            pdfPCell4 = new PdfPCell(
                    pdfAPI.addParagraph( "Preferred contact details", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
            pdfPCell4.setBorderWidth( 1);
            pdfPCell4.setBorderColor( pdfAPI.getBorder_color());
            pdfAPI.disableBorders( pdfPCell4);
            pdfPCell4.setBackgroundColor(  pdfAPI.getHeading_color_fill());
            pdfPCell4.setPaddingBottom( 6);
            pdfPCell4.setVerticalAlignment( Element.ALIGN_CENTER);
            pdfPTable2.addCell( pdfPCell4);
            if("UW".equalsIgnoreCase(pdfType)){
            	pdfPCell4 = new PdfPCell(
                        pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
                pdfAPI.disableBorders( pdfPCell4);
                pdfPTable2.addCell( pdfPCell4);
            }            
            document.add( pdfPTable2);
            
            pdfSpaceTable1 = new PdfPTable( 1);
            pdfPSpaceCell1 = new PdfPCell( pdfAPI.addSingleCell());
            pdfAPI.disableBorders( pdfPSpaceCell1);
            pdfSpaceTable1.addCell( pdfPSpaceCell1);
            document.add( pdfSpaceTable1);            
           
            if("UW".equalsIgnoreCase(pdfType)){
            	 float[] prefConDts = { .2f, .2f, .2f, .2f, .2f };
            	 pdfPreffConDtsTable = new PdfPTable( prefConDts);
            }else{
                float[] prefConDts = { .2f, .3f, .2f, .2f };
                pdfPreffConDtsTable = new PdfPTable( prefConDts);
            }            
            
            pdfPreffConDetsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( "Preferred contact method:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getPrefferedContactType(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            
            pdfPreffConDetsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( "Preferred contact time:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            if (null != pdfObject.getMemberDetails() && null != pdfObject.getMemberDetails().getPrefferedContactType() && !pdfObject.getMemberDetails().getPrefferedContactType().contains( "Email")){
            	pdfPreffConDetsCell3 = new PdfPCell(
                        pdfAPI.addParagraph( pdfObject.getMemberDetails().getPrefferedContactTime(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
            }                
            else{
            	pdfPreffConDetsCell3 = new PdfPCell(
                        pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
            }
                
            pdfAPI.disableBorders( pdfPreffConDetsCell3);
            pdfPreffConDetsCell3.setPadding( 4);
            pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);            
            pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            if("UW".equalsIgnoreCase(pdfType)){
            	pdfPreffConDetsCell3 = new PdfPCell(
                        pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
                pdfAPI.disableBorders( pdfPreffConDetsCell3);
                pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
            }          
            
            document.add( pdfPreffConDtsTable);
            log.info("pdfObject.getFundId()>> {}",pdfObject.getFundId());
            if("UW".equalsIgnoreCase(pdfType) && "VICS1".equalsIgnoreCase(pdfObject.getFundId()) && null!=pdfObject.getMemberDetails().getPrefContactDetails() && pdfObject.getMemberDetails().getPrefContactDetails().length()>0){
	           	
            	log.info("pdfObject.getMemberDetails().getClientEmailId()>> {}",pdfObject.getMemberDetails().getClientEmailId());
            	float[] prefConDts ={ .2f, .2f, .2f, .2f, .2f };
	           	 pdfPreffConDtsTable = new PdfPTable( prefConDts);
	           	 pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Preferred contact details:", pdfObject.getColorStyle(), 7));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( pdfObject.getMemberDetails().getPrefContactDetails(), FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             
	          	 pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Preferred email address:", pdfObject.getColorStyle(), 7));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( pdfObject.getMemberDetails().getClientEmailId(), FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDetsCell3.setPadding( 4);
	             pdfPreffConDetsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             
	             pdfPreffConDetsCell3 = new PdfPCell(
	                     pdfAPI.addParagraph("", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	             pdfAPI.disableBorders( pdfPreffConDetsCell3);
	             pdfPreffConDtsTable.addCell( pdfPreffConDetsCell3);
	             
	             document.add( pdfPreffConDtsTable);
           } 
            
        }
        log.info("Display header note finish");
	}
	
	public static void displayPersonalDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		log.info("Display personal details start");
		 PdfPTable pdfPTable = null;
		 PdfPCell pdfPCell2 = null;
		 PdfPTable fundInfoPdfPTable = null;
		 PdfPTable fundPdfPTable = null;
		 PdfPTable bmiPdfPTable = null;
		 PdfPCell fundInfoPdfPCell = null;
		 
		 boolean titleRender = true;
         boolean firstNameRender = true;
         boolean surNameRender = true;
         boolean dobRender = true;
         String exWaitingPeriod = "";
         String exBenefitPeriod = "";
         
         if(pdfObject.isQuickQuoteRender()){
         	if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getTitle() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getTitle())){
	            	titleRender = true;
	            }else{
	            	titleRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getMemberFName() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getMemberFName())){
	            	firstNameRender = true;
	            }else{
	            	firstNameRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getMemberLName() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getMemberLName())){
	            	surNameRender = true;
	            }else{
	            	surNameRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getDob() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getDob())){
	            	dobRender = true;
	            }else{
	            	dobRender = false;
	            }
         }
		 
		 if("UW".equalsIgnoreCase(pdfType)){
			 float[] prefConFloat = { .8f, .2f };
			  pdfPTable = new PdfPTable( prefConFloat);
		 }else{
			  pdfPTable = new PdfPTable( 1);
		 }	 
		 if(titleRender || firstNameRender || surNameRender || dobRender){
	        pdfPCell2 = new PdfPCell(
	                pdfAPI.addParagraph( "Member details", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	        pdfPCell2.setBorderWidth( 1);
	       /* pdfPCell2.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
	        pdfAPI.disableBorders( pdfPCell2);
	        pdfPCell2.setBackgroundColor(pdfAPI.getHeading_color_fill());
	        pdfPCell2.setPaddingBottom( 6);
	        pdfPCell2.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPTable.addCell( pdfPCell2);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	pdfPCell2 = new PdfPCell(
		                pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 10));
		        pdfAPI.disableBorders( pdfPCell2);
		        pdfPTable.addCell( pdfPCell2);
	        }	        
	        document.add( pdfPTable);
		 }
	        if("UW".equalsIgnoreCase(pdfType)){
	        	String localFundid = null;
	        	if(null != pdfObject.getProductName() && "PWCS".equalsIgnoreCase(pdfObject.getProductName())){
	        		localFundid = "PWCP";
	        	}else{
	        		localFundid = pdfObject.getProductName();
	        	}
	        	
	        	float[] fundInfo = { 0.2f, 0.2f, .6f };
		        fundPdfPTable = new PdfPTable( fundInfo);
		        
		        fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph( "Fund:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		        fundPdfPTable.addCell( fundInfoPdfPCell);		        
		        fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph(localFundid,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		        fundPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundPdfPTable.addCell( fundInfoPdfPCell);	
		        
		       document.add( fundPdfPTable);
		        String calculatedBMI = null;
		        if(pdfObject.getMemberDetails().getResponseObject()!=null && pdfObject.getMemberDetails().getResponseObject().getClientData() != null){
		        	for (Iterator iter = pdfObject.getMemberDetails().getResponseObject().getClientData().getListInsured().iterator(); iter.hasNext();) {
						Insured insured = (Insured) iter.next();
						log.info("insured.getBmi()>> {}",insured.getBmi());
						calculatedBMI = insured.getBmi();
						
						
					}
		        }
		        
		        if(calculatedBMI!=null){
		        	float[] fundInfo1 = { 0.2f, 0.2f, .6f };
		        	bmiPdfPTable = new PdfPTable( fundInfo1);
			        
			        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph( "Calculated BMI:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        bmiPdfPTable.addCell( fundInfoPdfPCell);		        
			        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph(calculatedBMI,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        bmiPdfPTable.addCell( fundInfoPdfPCell);
			        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        bmiPdfPTable.addCell( fundInfoPdfPCell);
			        document.add( bmiPdfPTable);
		        }		        
		       
	        }
	        
	        if(null!=pdfObject.getMemberDetails().getClientEmailId() && 
	        		("HOST".equalsIgnoreCase(pdfObject.getProductName()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId())/**vicsuper changes Made by Purna**/
	        				||"FIRS".equalsIgnoreCase(pdfObject.getProductName())
            				||"SFPS".equalsIgnoreCase(pdfObject.getProductName())
            				|| "MTAA".equalsIgnoreCase(pdfObject.getProductName())
	        				/*|| "PSUP".equalsIgnoreCase(pdfObject.getProductName())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getProductName())
	    					*/)){
	        	
	        	/*String localFundid = null;
	        	if(null != pdfObject.getProductName() && "PWCS".equalsIgnoreCase(pdfObject.getProductName())){
	        		localFundid = "PWCP";
	        	}else{
	        		localFundid = pdfObject.getProductName();
	        	}*/
	        	if("UW".equalsIgnoreCase(pdfType)){
					  float[] fundInfo = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
					  fundInfoPdfPTable = new PdfPTable( fundInfo);
					 
				}else{
					  float[] fundInfo = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
					  fundInfoPdfPTable = new PdfPTable( fundInfo);
				}
	        	/*float[] fundInfo = {  0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f  };
		        fundInfoPdfPTable = new PdfPTable( fundInfo);*/
		        
	        	fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph( "Client Email", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);		        
		        fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph(pdfObject.getMemberDetails().getClientEmailId(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);


		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            
	            fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_RIGHT);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	        }        
	        
	        if(("UW".equalsIgnoreCase(pdfType) && fundInfoPdfPTable!=null) || (fundInfoPdfPTable!=null && null!=pdfObject.getMemberDetails().getClientEmailId() && 
	        		("HOST".equalsIgnoreCase(pdfObject.getProductName())|| MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId())/**vicsuper changes Made by Purna**/
	        				||"FIRS".equalsIgnoreCase(pdfObject.getProductName())
            				||"SFPS".equalsIgnoreCase(pdfObject.getProductName())
            				||"AEIS".equalsIgnoreCase(pdfObject.getProductName())
            				|| "MTAA".equalsIgnoreCase(pdfObject.getProductName())
	        				/*|| "PSUP".equalsIgnoreCase(pdfObject.getProductName())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getProductName())
	    					*/))){
	        	document.add( fundInfoPdfPTable);	
	        }
	        
	        if(("UW".equalsIgnoreCase(pdfType))){
	        
	        	 float[] fundInfo = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
			     fundInfoPdfPTable = new PdfPTable( fundInfo);
					 
				fundInfoPdfPCell = new PdfPCell(
		                pdfAPI.addParagraph( "Preferred contact number", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph(pdfObject.getMemberDetails().getPrefContactDetails(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			        
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPCell.setPadding( 4);
		        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( fundInfoPdfPCell);
		        fundInfoPdfPTable.addCell( fundInfoPdfPCell);


		        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            
	            fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	            fundInfoPdfPCell = new PdfPCell(
	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( fundInfoPdfPCell);
	            fundInfoPdfPCell.setPadding( 4);
	            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_RIGHT);
	            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
	             document.add( fundInfoPdfPTable);	
	       
	        }
	       
			if("UW".equalsIgnoreCase(pdfType)){
				  float[] f3 = { 0.08f, 0.08f, 0.1f, 0.12f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
				  pdfPTable = new PdfPTable( f3);
				 
			}else{
				  float[] f3 = { 0.04f, 0.08f, 0.1f, 0.12f, 0.1f, 0.1f, 0.12f, 0.1f};
				  pdfPTable = new PdfPTable( f3);
			}    
	            
	            PdfPCell pdfPCell3 = null;
	            
	           
	            
	           
	            
	            if(titleRender){
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Title", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }else{
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            
	            /*pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Title", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));*/
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getTitle(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(firstNameRender){
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "Firstname", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }else{
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
           /*   pdfPCell3 = new PdfPCell(
	            pdfAPI.addParagraph( "Firstname", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));*/
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getMemberFName(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            
	            pdfPTable.addCell( pdfPCell3);
	            	            
	            if(surNameRender){
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Surname", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }else{
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
 	            /*pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Surname", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));*/
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getMemberLName(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(dobRender){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "Date of birth", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }else{
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	           /* pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Date of Birth", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));*/
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getDob(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPTable.addCell( pdfPCell3);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            pdfPTable.addCell( pdfPCell3);
	            }          
	            
	            document.add( pdfPTable);
	            
	           	            
	            if("UW".equalsIgnoreCase(pdfType)){
					  float[] f4 = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
					  pdfPTable = new PdfPTable( f4);
					 
				}else{
					  float[] f4 = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
					  pdfPTable = new PdfPTable( f4);
				}    
	            if(!"CORP".equalsIgnoreCase(pdfObject.getFundId())) {
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Date joined fund",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getDateJoinedFund(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
	            pdfPTable.addCell( pdfPCell3);
	            }
	            if("AEIS".equalsIgnoreCase(pdfObject.getProductName())) {
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Sex", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }else if(pdfObject.getMemberDetails().getGender() != null) {
	            	 pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Gender", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }else {
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getGender(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(null != pdfObject.getMemberDetails().getClientRefNum() && null != pdfObject.getApplicationNumber() && !pdfObject.getMemberDetails().getClientRefNum().equalsIgnoreCase(pdfObject.getApplicationNumber())){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "Member number",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            /*pdfPCell3.setPadding(4);
	 	            pdfPCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);*/
	 	            pdfPTable.addCell( pdfPCell3);
	 	            pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getClientRefNum(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            /*pdfPCell3.setPadding(4);
	 	            pdfPCell3.setVerticalAlignment(Element.ALIGN_RIGHT);*/
	 	            pdfPTable.addCell( pdfPCell3);
	 	            pdfPCell3.setNoWrap(Boolean.FALSE);
	 	            if("UW".equalsIgnoreCase(pdfType)){
	 	            	pdfPCell3 = new PdfPCell(
	 		                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	 		            pdfAPI.disableBorders( pdfPCell3);
	 		            pdfPTable.addCell( pdfPCell3);
	 	            }	            
	            }
	            
	            document.add( pdfPTable);
	           
	            
	            if("VICS1".equalsIgnoreCase(pdfObject.getFundId())){
	            	if("UW".equalsIgnoreCase(pdfType)){
						  float[] f4 = { 0.2f, 0.6f, 0.2f };
						  pdfPTable = new PdfPTable( f4);
						 
					}else{
						  float[] f4 = { 0.2f, 0.81f};
						  pdfPTable = new PdfPTable( f4);
					}    
		            
		            pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "Date Joined Company", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            pdfPTable.addCell( pdfPCell3);
		            pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getDateJoinedCompany(), pdfObject.getColorStyle(), 7));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
		            pdfPTable.addCell( pdfPCell3);            
		         
	 	            if("UW".equalsIgnoreCase(pdfType)){
	 	            	pdfPCell3 = new PdfPCell(
	 		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 		            pdfAPI.disableBorders( pdfPCell3);
	 		            pdfPTable.addCell( pdfPCell3);
	 	            } 	            
	 	           document.add( pdfPTable);
	 	          /* Amp number*/
	 	           
	 	          if("UW".equalsIgnoreCase(pdfType)){
					  float[] f4 = { 0.3f, 0.5f, 0.2f };
					  pdfPTable = new PdfPTable( f4);
					 
	 	          }else{
					  float[] f4 = { 0.3f, 0.70f};
					  pdfPTable = new PdfPTable( f4);
	 	          } 
	 	           
	 	           	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "AMP Reference Number", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            pdfPTable.addCell( pdfPCell3);
		            pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getAmpReferenceNumber(), pdfObject.getColorStyle(), 7));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPCell3.setPadding( 4);
		            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            pdfPTable.addCell( pdfPCell3);
		            
		            if("UW".equalsIgnoreCase(pdfType)){
	 	            	pdfPCell3 = new PdfPCell(
	 		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	 		            pdfAPI.disableBorders( pdfPCell3);
	 		            pdfPTable.addCell( pdfPCell3);
	 	            }
		            
		            document.add( pdfPTable);
	 	           
	 	           /*address*/
	 	           if("UW".equalsIgnoreCase(pdfType)){
	 	        	  float[] f4 = { 0.2f, 0.6f, 0.2f };
					  pdfPTable = new PdfPTable( f4);
					 
					}else{
						  float[] f4 = { 0.2f, 0.8f };
						  pdfPTable = new PdfPTable( f4);
					}    
	            
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Address", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            pdfPCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getMemberAddress(), pdfObject.getColorStyle(), 7));
	            pdfAPI.disableBorders( pdfPCell3);
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
	            pdfPTable.addCell( pdfPCell3);
	            
	         
	            if("UW".equalsIgnoreCase(pdfType)){
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
		            pdfAPI.disableBorders( pdfPCell3);
		            pdfPTable.addCell( pdfPCell3);
	            } 	            
		          document.add( pdfPTable);
	 	         
	 	           
		          for (int itr = 0; itr < pdfObject.getMemberDetails().getMemberProductList().size(); itr++) {
		 	        	 MemberProductDetails	memberProductDetails = pdfObject.getMemberDetails().getMemberProductList().get(itr);
		 	        	if(MetlifeInstitutionalConstants.INCOME_PROTECTION.equalsIgnoreCase(memberProductDetails.getProductName()) && (memberProductDetails.getExistingCover()!=null && !"$0".equalsIgnoreCase(memberProductDetails.getExistingCover()))){
		 	        		exWaitingPeriod = memberProductDetails.getExWaitingPeriod();
			 	        	exBenefitPeriod = memberProductDetails.getExBenefitPeriod();
		 	        	}		 	        	 
		 	       }
		          
		        	  if("UW".equalsIgnoreCase(pdfType)){
						  float[] f4 = { 0.25f, 0.18f, 0.25f, 0.12f, 0.2f };
						  pdfPTable = new PdfPTable( f4);
						 
					}else{
						  float[] f4 = { 0.32f, 0.18f, 0.32f, 0.18f };
						  pdfPTable = new PdfPTable( f4);
					} 
		        	  
		        	  pdfPCell3 = new PdfPCell(
			                    pdfAPI.addParagraph( "Current IP Waiting Period", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
			            pdfPTable.addCell( pdfPCell3);
			            pdfPCell3 = new PdfPCell(
			                    pdfAPI.addParagraph( exWaitingPeriod, pdfObject.getColorStyle(), 7));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_RIGHT);
			            pdfPTable.addCell( pdfPCell3);
			            
			            pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "Current IP Benefit Period", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
			            pdfPTable.addCell( pdfPCell3);
			            pdfPCell3 = new PdfPCell(
			                    pdfAPI.addParagraph( exBenefitPeriod, pdfObject.getColorStyle(), 7));
			            pdfAPI.disableBorders( pdfPCell3);
			            pdfPCell3.setPadding( 4);
			            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
			            pdfPTable.addCell( pdfPCell3);            
			            
			           
			            if("UW".equalsIgnoreCase(pdfType)){
			            	pdfPCell3 = new PdfPCell(
				                    pdfAPI.addParagraph( "", FontFactory.HELVETICA, 7, Font.BOLD, pdfObject.getColorStyle()));
				            pdfAPI.disableBorders( pdfPCell3);
				            pdfPTable.addCell( pdfPCell3);
			            }
			            document.add( pdfPTable);
		               
		            
		            
	            }           
	            
	          
	           
	           	
	            if(null!=pdfObject.getMemberDetails().getMemberType() &&
	            		("HOST".equalsIgnoreCase(pdfObject.getProductName()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId())/**vicsuper changes Made by Purna**/
	            				||"FIRS".equalsIgnoreCase(pdfObject.getProductName())
	            				||"SFPS".equalsIgnoreCase(pdfObject.getProductName())
	            				||"AEIS".equalsIgnoreCase(pdfObject.getProductName())
	            				|| "MTAA".equalsIgnoreCase(pdfObject.getProductName())
	            				|| "GUIL".equalsIgnoreCase(pdfObject.getProductName())
	            				/*||  "PSUP".equalsIgnoreCase(pdfObject.getProductName())
		    					|| "NSFS".equalsIgnoreCase(pdfObject.getProductName())
		    					|| "MTAA".equalsIgnoreCase(pdfObject.getProductName()))&& !pdfObject.isNonValidatedMemberFlow()*/)){				        	   	
		        	/*float[] fundInfo = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
			        fundInfoPdfPTable = new PdfPTable( fundInfo);*/
	            	
	            	if("UW".equalsIgnoreCase(pdfType)){
						  float[] fundInfo = { 0.16f, 0.22f, 0.1f, 0.11f, 0.12f, 0.15f, 0.2f };
						  fundInfoPdfPTable = new PdfPTable( fundInfo);
						 
					}else{
						  float[] fundInfo = { 0.12f, 0.21f, 0.06f, 0.1f, 0.12f, 0.1f };
						  fundInfoPdfPTable = new PdfPTable( fundInfo);
					}
			        
		        	fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph( "Member Type",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        fundInfoPdfPTable.addCell( fundInfoPdfPCell);		        
			        fundInfoPdfPCell = new PdfPCell(
			                pdfAPI.addParagraph(toTitleCase(pdfObject.getMemberDetails().getMemberType()),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			        
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPCell.setPadding( 4);
			        fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
			        fundInfoPdfPTable.addCell( fundInfoPdfPCell);
			        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			        pdfAPI.disableBorders( fundInfoPdfPCell);
			        fundInfoPdfPTable.addCell( fundInfoPdfPCell);	
			        
			        
			        
			        fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		            fundInfoPdfPCell = new PdfPCell(
		                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		            
		            fundInfoPdfPCell = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_MIDDLE);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
		            fundInfoPdfPCell = new PdfPCell(
		                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( fundInfoPdfPCell);
		            fundInfoPdfPCell.setPadding( 4);
		            fundInfoPdfPCell.setVerticalAlignment( Element.ALIGN_RIGHT);
		            fundInfoPdfPTable.addCell( fundInfoPdfPCell);
			        
			        document.add( fundInfoPdfPTable);
			        
		        }  
		       /*if("UW".equalsIgnoreCase(pdfType) || (null!=pdfObject.getMemberDetails().getMemberType() && "HOST".equalsIgnoreCase(pdfObject.getProductName()))){
		        	document.add( fundInfoPdfPTable);
		        }*/
	            
	   		log.info("Display personal details finish");
	}
	
	
	public static void displayCoverSummary(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		   log.info("Display cover summary start");
		   PdfPTable pdfPCoverTable = null;
		   PdfPCell pdfPCoverCell = null;
		   PdfPTable pdfPEmptyTable = null;
		   PdfPCell pdfPEmptyCell3 = null;
		   PdfPTable pdfPSummaryDtsTable = null;
		   PdfPCell pdfcoverCostCell1 = null;
		   MemberProductDetails memberProductDetails = null;
		   /*Boolean corpFund = Boolean.FALSE;*/		  
		   
		   /*costColRq = ConfigurationHelper.getConfigurationValue("CORPINST", pdfObject.getProductName());		   
		   String hdCorpFunds = ConfigurationHelper.getConfigurationValue("HD_CORP_FUNDS", "HD_CORP_FUNDS");*/		   
		   if("UW".equalsIgnoreCase(pdfType)){
			   float[] coverSumm = { .8f, .2f };
		       pdfPCoverTable = new PdfPTable( coverSumm);
		   }else{
			   pdfPCoverTable = new PdfPTable(1);
		   }  
	        
	        pdfPCoverCell = new PdfPCell(
	                pdfAPI.addParagraph( "Cover summary", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	        pdfPCoverCell.setBorderWidth( 1);
	        /*pdfPCoverCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
	        pdfAPI.disableBorders( pdfPCoverCell);
	        pdfPCoverCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
	        pdfPCoverCell.setPaddingBottom( 6);
	        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPCoverTable.addCell( pdfPCoverCell);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	pdfPCoverCell = new PdfPCell(
		                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS,  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		        pdfPCoverCell.setBorderWidth( 1);
		        pdfPCoverCell.setBorderColor(pdfAPI.getBorder_color());
		        pdfAPI.disableBorders( pdfPCoverCell);
		        pdfPCoverCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
		        pdfPCoverCell.setPaddingBottom( 6);
		        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPCoverTable.addCell( pdfPCoverCell);
	        }	        
	        document.add( pdfPCoverTable);
	        
	        pdfPEmptyTable = new PdfPTable( 1);
	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);	     	        	       
	      
	        if("UW".equalsIgnoreCase(pdfType)){

	        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
	        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) &&*/ null!=pdfObject.getRequestType() 

	        	

	        			 && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.20f,0.2f };
	        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else{
	        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
		        		float[] f5 = {0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.09f,0.11f};
		        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        		}
		        		else {
		        			 float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
				        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        		}
	        	}	
	        	/*float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
	        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());*/
	        }else{
	        	 /*float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
	        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());*/

	        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
	        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId()) 
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && */null!=pdfObject.getRequestType() 

	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.2f };
	        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else{
	        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
		        		float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f,0.1f};
		        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        		}
		        		else {
		        			 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
				        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        		}
	        		
	        	}	
	        }
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	/* float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
	        	 pdfPSummaryDtsTable = new PdfPTable( f6);*/
	        	

	        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId())
	        			||"CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && */null!=pdfObject.getRequestType() 

	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f6 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.20f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else{
	        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
		        		float[] f6 = {  0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.09f,0.11f};
			        	pdfPSummaryDtsTable = new PdfPTable( f6);
		        		}
	        		else {
	        		 float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        		}
	        	}
	        }else{
	        	 /*float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
	        	 pdfPSummaryDtsTable = new PdfPTable( f6);*/

	        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
	        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
	        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& */null!=pdfObject.getRequestType() 

	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f6 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.2f };
		        	pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else{
	        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
	        		float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f,0.1f};
		        	pdfPSummaryDtsTable = new PdfPTable( f6);
	        		}
	        		else {
	        			float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
			        	pdfPSummaryDtsTable = new PdfPTable( f6);	
	        		}
	        	}
	        }
	        for (int itr = 0; itr < pdfObject.getMemberDetails().getMemberProductList().size(); itr++) {
	        	memberProductDetails = pdfObject.getMemberDetails().getMemberProductList().get(itr);
	        	if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
	        		displayCorporateColumnValue( pdfAPI, document,pdfObject,memberProductDetails, pdfType,pdfPSummaryDtsTable,pdfObject.getProductName());	 
	        	}
	        	else {
	        		displayColumnValue( pdfAPI, document,pdfObject,memberProductDetails, pdfType,pdfPSummaryDtsTable,pdfObject.getProductName());	 
	        	}
	        	
	        }
	        
	       
	        	if(!"CORP".equalsIgnoreCase(pdfObject.getFundId())) {
	        pdfcoverCostCell1 = new PdfPCell(
	                pdfAPI.addParagraph(pdfObject.getMemberDetails().getFrquencyOpted()
	                        + " cost of your requested cover",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
	        	
		        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_RIGHT);
		        pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());

		        if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
		        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
		        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && */null!=pdfObject.getRequestType() 

	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        	pdfcoverCostCell1.setColspan(7);
		        }/*else if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        	pdfcoverCostCell1.setColspan(5);
		        }*/else{
		        	pdfcoverCostCell1.setColspan(6);
		        }
		        if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        	 pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        	 
		        	 pdfcoverCostCell1 = new PdfPCell(
			 	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getTotalPremium(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			 	        pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());
			 	        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_CENTER);
			 	        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
			 	      
			 	       if("UW".equalsIgnoreCase(pdfType)){
				        	pdfcoverCostCell1 = new PdfPCell(
					                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
					        pdfAPI.disableBorders( pdfcoverCostCell1);
					        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
				        }
		        }
		       
	        	}
	        	
	      
	       
	        
	        if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
	        	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/      
	        	 /*pdfcoverCostCell1 = new PdfPCell(
	                     pdfAPI.addParagraph( "Additional cover alteration:", new Font(pdfAPI.getSwsMed() , 7,0, pdfAPI.getFont_color())));

	        	 if (null != pdfObject.getFundId() && !"".equalsIgnoreCase(pdfObject.getFundId())
							&& "CARE".equalsIgnoreCase(pdfObject.getFundId()) && null != pdfObject.getRequestType()

							&& !"".equalsIgnoreCase(pdfObject.getRequestType())
							&& MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())) {
	        		 	pdfcoverCostCell1.setColspan(8);
		        	 }else{
		        		 pdfcoverCostCell1.setColspan(7);
		        	 }
	        	
	        	 pdfcoverCostCell1.disableBorderSide(pdfcoverCostCell1.BOTTOM);
	        	 pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());
	        	 pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
	        	 if("UW".equalsIgnoreCase(pdfType)){
			        	pdfcoverCostCell1 = new PdfPCell(
				                pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 6,0, pdfAPI.getFont_color())), false));
				        pdfAPI.disableBorders( pdfcoverCostCell1);
				        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
			        }
	            
	        	pdfcoverCostCell1 = new PdfPCell(
		                pdfAPI.addParagraph(MetlifeInstitutionalConstants.DCL_TEXT_OVERALL+pdfObject.getDeclReasons(), new Font(pdfAPI.getSwsMed() , 6,0, pdfAPI.getFont_color())));*/
		        /* pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_RIGHT);*/
		        /*pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());

		        if (null != pdfObject.getFundId() && !"".equalsIgnoreCase(pdfObject.getFundId())
						&& "CARE".equalsIgnoreCase(pdfObject.getFundId()) && null != pdfObject.getRequestType()

						&& !"".equalsIgnoreCase(pdfObject.getRequestType())
						&& MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())) {
		        	 pdfcoverCostCell1.setColspan(8);
		         }else{
		        		 pdfcoverCostCell1.setColspan(7);
		         }
		        pdfcoverCostCell1.disableBorderSide(pdfcoverCostCell1.TOP);
		        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        if("UW".equalsIgnoreCase(pdfType)){
		        	pdfcoverCostCell1 = new PdfPCell(
			                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 6,0, pdfAPI.getFont_color())), false));
			        pdfAPI.disableBorders( pdfcoverCostCell1);
			        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        }*/
	        	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/      
	        }
	        document.add( pdfPSummaryDtsTable);
	        
	        
	        /* added for Indexation Flag */
	        //Refined the logic as part of sonarqube fix - vicsuper by purna
	        if (("CARE".equalsIgnoreCase(pdfObject.getFundId()) || (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId())
	        		&& !(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType()))))) {
	        	if (!pdfObject.isQuickQuoteRender() && (MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType()) || "CCOVER".equalsIgnoreCase(pdfObject.getRequestType()) || "NCOVER".equalsIgnoreCase(pdfObject.getRequestType())
	        			|| MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(pdfObject.getRequestType()))){
		       		String label = null;
		        	if(!(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(pdfObject.getRequestType())))
		        	{
		       		 if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) && !("NA".equalsIgnoreCase(pdfObject.getMemberDetails().getIndexationFlag()))){
		        		label ="Indexation applied at lower of CPI and 7.5% per year";
		        		PdfPTable pdfPIndexTable = null;
			        	float[] f = { .9f, 0.1f }; 
			        	pdfPIndexTable = new PdfPTable(f);
			 	        PdfPCell pdfPCell;
			 	        pdfPCell= new PdfPCell(
			        				 pdfAPI.addParagraph(label,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	        pdfAPI.disableBorders( pdfPCell);
			 	        pdfPCell.setPadding(4);
			 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
			 	        pdfPIndexTable.addCell( pdfPCell);
			 	        PdfPCell pdfPCellAns = new PdfPCell(
			                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getIndexationFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	        pdfAPI.disableBorders( pdfPCellAns);
			 	        pdfPCellAns.setPadding(4);
			 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
			 	        pdfPIndexTable.addCell( pdfPCellAns);
			 	       document.add(pdfPIndexTable);
		        	}
		       		else if ("CARE".equalsIgnoreCase(pdfObject.getFundId())){
		        		  label ="Would you like your cover to be indexed by 5% on 1 July?";
		        	
		        	PdfPTable pdfPIndexTable = null;
		        	float[] f = { .9f, 0.1f }; 
		        	pdfPIndexTable = new PdfPTable(f);
		 	        PdfPCell pdfPCell;
		 	        pdfPCell= new PdfPCell(
		        				 pdfAPI.addParagraph(label,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	        pdfAPI.disableBorders( pdfPCell);
		 	        pdfPCell.setPadding(4);
		 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	        pdfPIndexTable.addCell( pdfPCell);
		 	        PdfPCell pdfPCellAns = new PdfPCell(
		                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getIndexationFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	        pdfAPI.disableBorders( pdfPCellAns);
		 	        pdfPCellAns.setPadding(4);
		 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
		 	        pdfPIndexTable.addCell( pdfPCellAns);
		 	       document.add(pdfPIndexTable);
		       		}
		        	}
		 	       if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) && !(pdfObject.isConvertCheckPdf()) && "Yes".equalsIgnoreCase(pdfObject.getMemberDetails().getIpDisclaimer())){
		 	    	  label ="I am not a casual or contractor employee";
		 	    	  PdfPTable pdfPIndexTableIp = null;
		 	    	  float[] f1 = { .9f, 0.1f }; 
			        	pdfPIndexTableIp = new PdfPTable(f1);
			 	        PdfPCell pdfPCell1;
			 	        pdfPCell1= new PdfPCell(
			        				 pdfAPI.addParagraph(label,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	        pdfAPI.disableBorders( pdfPCell1);
			 	       pdfPCell1.setPadding(4);
			 	      pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
			 	     pdfPIndexTableIp.addCell( pdfPCell1);
			 	        PdfPCell pdfPCellAns1 = new PdfPCell(
			                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getIpDisclaimer(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	        pdfAPI.disableBorders( pdfPCellAns1);
			 	       pdfPCellAns1.setPadding(4);
			 	       pdfPCellAns1.setVerticalAlignment( Element.ALIGN_RIGHT);
			 	       pdfPIndexTableIp.addCell( pdfPCellAns1);
			 	       document.add(pdfPIndexTableIp);
		 	       }
		 	       
		        }	      
	       }
		     
	        /* added for Indexation Flag */
	        
		        
		 /* added for Unitised Covers Flag */
	 	        if( null != pdfObject.getRequestType()  && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType()) && !("CARE".equalsIgnoreCase(pdfObject.getFundId()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) || "HOST".equalsIgnoreCase(pdfObject.getFundId())) ){
	 	        	PdfPTable pdfPUnitisedCvrsTable = null;
	 	        	float[] f = { .9f, 0.1f }; 
	 	        	pdfPUnitisedCvrsTable = new PdfPTable(f);
	 	 	        PdfPCell pdfPCell = null;
	 	 	        if((null!=pdfObject.getFundId() && "CARE".equalsIgnoreCase(pdfObject.getFundId()))){
	 	 	        	 pdfPCell= new PdfPCell(
	 	        				 pdfAPI.addParagraph("Equivalent units of CareSuper scale ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        }else if(null!=pdfObject.getFundId() && ("HOST".equalsIgnoreCase(pdfObject.getFundId()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()))/**vicsuper changes Made by Purna**/){
	 	 	        	pdfPCell= new PdfPCell(
	 	        				 pdfAPI.addParagraph("Equivalent units of HostPlus scale ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        }else if((null!=pdfObject.getFundId() && "FIRS".equalsIgnoreCase(pdfObject.getFundId()))){
	 	 	        	pdfPCell= new PdfPCell(
	 	        				 pdfAPI.addParagraph("Equivalent value in unitised scale? ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        }else if((null!=pdfObject.getFundId() && "SFPS".equalsIgnoreCase(pdfObject.getFundId()))){
	 	 	        	pdfPCell= new PdfPCell(
	 	        				 pdfAPI.addParagraph("Equivalent value in unitised scale? ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        }
	 	 	        /*MTAA - Code - Check with Chordiant Code */
	 	 	        else if((null!=pdfObject.getFundId() && "MTAA".equalsIgnoreCase(pdfObject.getFundId()))){
	 	 	        	pdfPCell= new PdfPCell(
	 	        				 pdfAPI.addParagraph("Equivalent units of MTAASuper scale ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        }
	 	 	       /*MTAA code - check with Chordiant Code*/
	 	 	        
	 	 	        pdfAPI.disableBorders( pdfPCell);
	 	 	        if(null!= pdfPCell){
	 	 	        pdfPCell.setPadding(4);
	 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	 	        }
	 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCell);
	 	 	        PdfPCell pdfPCellAns = new PdfPCell(
	 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getUnitisedCvrsFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        pdfAPI.disableBorders( pdfPCellAns);
	 	 	        pdfPCellAns.setPadding(4);
	 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
	 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCellAns);
	 	 	        document.add(pdfPUnitisedCvrsTable);
	 	        }
	 	        /*added for rounding message*/
	 	        if(pdfObject.isDeathRoundMsg() || pdfObject.isTpdRoundMsg())
	 	        {
	 	        	PdfPTable pdfPUnitisedCvrsTable = null;
	 	        	float[] f = { .9f, 0.1f }; 
	 	        	pdfPUnitisedCvrsTable = new PdfPTable(f);
	 	 	        PdfPCell pdfPCell = null;
	 	 	        String msg = "The total cover amount for <%Covers%> has been rounded up to the nearest number of units.";
	 	 	        String covers = pdfObject.isDeathRoundMsg() && pdfObject.isTpdRoundMsg()?"Death and TPD": (pdfObject.isDeathRoundMsg() && !pdfObject.isTpdRoundMsg())?DEATH:"TPD"; 
	 	 	        		
	 	 	        		
	 	 	        msg = msg.replace("<%Covers%>", covers);
	 	 	        
	 	 	        pdfPCell= new PdfPCell(
	 	        				 pdfAPI.addParagraph(msg,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        
	 	 	        pdfAPI.disableBorders( pdfPCell);
	 	 	        pdfPCell.setPadding(4);
	 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCell);
	 	 	        PdfPCell pdfPCellAns = new PdfPCell(
	 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getUnitisedCvrsFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        pdfAPI.disableBorders( pdfPCellAns);
	 	 	        pdfPCellAns.setPadding(4);
	 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
	 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCellAns);
	 	 	        document.add(pdfPUnitisedCvrsTable);
	 	        	
	 	        }
	 	        
	 	       if(pdfObject.isIpRoundMsg())
 	        	{
	 	    	  PdfPTable pdfPUnitisedCvrsTable = null;
	 	        	float[] f = { .9f, 0.1f }; 
	 	        	pdfPUnitisedCvrsTable = new PdfPTable(f);
	 	 	        PdfPCell pdfPCell = null;
	 	 	        pdfPCell= new PdfPCell(
	        				 pdfAPI.addParagraph("Your IP cover has been rounded up to the next multiple of $425 ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        
	 	 	        pdfAPI.disableBorders( pdfPCell);
	 	 	        pdfPCell.setPadding(4);
	 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCell);
	 	 	        PdfPCell pdfPCellAns = new PdfPCell(
	 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getUnitisedCvrsFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        pdfAPI.disableBorders( pdfPCellAns);
	 	 	        pdfPCellAns.setPadding(4);
	 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
	 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCellAns);
	 	 	        document.add(pdfPUnitisedCvrsTable);
 	        	}
	 	        
	        /* added for Unitised Covers Flag */
 	       /* added for Cancel cover reason */    
	 	       if( null!=pdfObject.getRequestType()  && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType()) && (null!=pdfObject.getFundId() && !"CARE".equalsIgnoreCase(pdfObject.getFundId())) ){
	 	        	PdfPTable pdfCancelReasonTable = null;
	 	        	float[] f = { .5f, 0.5f }; 
	 	        	pdfCancelReasonTable = new PdfPTable(f);
	 	 	        PdfPCell pdfPCell = null;
	 	 	        PdfPCell pdfPCellAns = null;
	 	 	        pdfPCell= new PdfPCell(pdfAPI.addParagraph("Reason for cancelling your cover ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        pdfAPI.disableBorders( pdfPCell);
	 	 	        pdfPCell.setPadding(4);
	 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	 	        pdfCancelReasonTable.addCell( pdfPCell);
	 	 	        if(pdfObject.getMemberDetails().getCancelreason() != null && pdfObject.getMemberDetails().getCancelreason() != ""){
	 	 	        	if(pdfObject.getMemberDetails().getCancelreason().equalsIgnoreCase("Other") && pdfObject.getMemberDetails().getCancelotherreason() != "" && pdfObject.getMemberDetails().getCancelotherreason() != null){
		 	 	        	pdfPCellAns = new PdfPCell(pdfAPI.addParagraph(pdfObject.getMemberDetails().getCancelotherreason(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }else{
		 	 	        	pdfPCellAns = new PdfPCell(pdfAPI.addParagraph(pdfObject.getMemberDetails().getCancelreason(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }
	 	 	        }
	 	 	        pdfAPI.disableBorders( pdfPCellAns);
	 	 	        if(null!= pdfPCellAns){
	 	 	        pdfPCellAns.setPadding(4);
	 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
	 	 	        }
	 	 	        pdfCancelReasonTable.addCell( pdfPCellAns);
	 	 	        document.add(pdfCancelReasonTable);
	 	        }
	 	      /* added for Cancel cover reason */    
	        
	        
	        
	          
			    log.info("Display personal details finish");
	}
	
	private static void displayColumnName(CostumPdfAPI pdfAPI, Document document,float[] f5, PDFObject pdfObject,String pdfType, String productName)
            throws DocumentException {
		log.info("Display column name start");
		 PdfPTable pdfPSummaryTable = new PdfPTable( f5);
		 PdfPCell pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Cover type",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		 if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
			 pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Existing cover", new Font(pdfAPI.getSwsMed(),8,0, pdfAPI.getHeading_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        if(pdfObject.getMemberDetails().isReplaceCvrOption() && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
		        	 pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Additional cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);
		         }        
		        else if(!"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
		        	 pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Total cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        }else if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
		        	pdfPSummaryCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "Total cover", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		            pdfAPI.disableBorders( pdfPSummaryCell3);
		            pdfPSummaryCell3.setPadding( 4);
		            pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		            pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		            pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        }
		        pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Waiting period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        
		        pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Benefit period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        
		        pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Forward Underwriting Limit cover amount", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        if( "UW".equalsIgnoreCase(pdfType)) {
		        	 pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Final decision after post processing",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        }
		      
		 }
		 else {
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Existing cover", new Font(pdfAPI.getSwsMed(),8,0, pdfAPI.getHeading_color())));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        

        if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
				|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
				|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& */null!=pdfObject.getRequestType() 

    			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
        	 pdfPSummaryCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "Transfer cover", new Font(pdfAPI.getSwsMed(),8,0, pdfAPI.getHeading_color())));
             pdfAPI.disableBorders( pdfPSummaryCell3);
             pdfPSummaryCell3.setPadding( 4);
             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
             pdfPSummaryTable.addCell( pdfPSummaryCell3);
        }
        
         if(pdfObject.getMemberDetails().isReplaceCvrOption() && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
        	 pdfPSummaryCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "Additional cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
             pdfAPI.disableBorders( pdfPSummaryCell3);
             pdfPSummaryCell3.setPadding( 4);
             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
             pdfPSummaryTable.addCell( pdfPSummaryCell3);
         }        
        else if(!"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
        	 pdfPSummaryCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "Total cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
             pdfAPI.disableBorders( pdfPSummaryCell3);
             pdfPSummaryCell3.setPadding( 4);
             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
             pdfPSummaryTable.addCell( pdfPSummaryCell3);
        }else if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
        	pdfPSummaryCell3 = new PdfPCell(
                    pdfAPI.addParagraph( "Total cover", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
            pdfAPI.disableBorders( pdfPSummaryCell3);
            pdfPSummaryCell3.setPadding( 4);
            pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
            pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
            pdfPSummaryTable.addCell( pdfPSummaryCell3);
        }
       
      
        
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Waiting period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Benefit period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
        /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Occupation rating", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor(  pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);  
     
        
	    	 if(pdfObject.getMemberDetails().isReplaceCvrOption()){
	         	pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Estimated additional " +  pdfObject.getMemberDetails().getFrquencyOpted().toLowerCase() + " cost", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
	         }else if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	         	pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Estimated total " +  pdfObject.getMemberDetails().getFrquencyOpted().toLowerCase() + " cost", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
	         }
	   
        
           
        pdfPSummaryCell3 = new PdfPCell(
                pdfAPI.addParagraph( "Final decision after post processing",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
        pdfAPI.disableBorders( pdfPSummaryCell3);
        pdfPSummaryCell3.setPadding( 4);
        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		 }
        document.add( pdfPSummaryTable);

         log.info("Display column name finish");
        
        
    }
	
	
	private static void displayCorporateColumnValue(CostumPdfAPI pdfAPI, Document document,
            PDFObject pdfObject,MemberProductDetails memberProductDetails , String pdfType, PdfPTable pdfPSummaryDtsTable, String productName)
            throws DocumentException { 
		    log.info("Display column value start");
            PdfPCell pdfSummaryDtsCell3 = null;   
            PdfPCell pdfPExclusionCell1 = null;
            PdfPCell pdfPEmptyCell1 = null;
            pdfSummaryDtsCell3 = new PdfPCell(
                    pdfAPI.addParagraph( memberProductDetails.getProductName(),  new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            
            log.info("inside the column value>> {}",memberProductDetails.getExistingCover());
            
            pdfSummaryDtsCell3 = new PdfPCell(
                    pdfAPI.addParagraph(  memberProductDetails.getExistingCover(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            

            if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
            		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
            		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
    				|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
    				|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& */null!=pdfObject.getRequestType() 

        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
            	if(null!=memberProductDetails.getTransferCoverAmnt()){
            		pdfSummaryDtsCell3 = new PdfPCell(
                            pdfAPI.addParagraph(  memberProductDetails.getTransferCoverAmnt(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	}else{
            		pdfSummaryDtsCell3 = new PdfPCell(
                            pdfAPI.addParagraph(  "$0", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	}
            	
                pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
                pdfSummaryDtsCell3.setPadding( 4);
                pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
                pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
                pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            }        
           
           if("UWCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
        	   if(memberProductDetails.getProductAmount()!=null){
            	   pdfSummaryDtsCell3 = new PdfPCell(
                           pdfAPI.addParagraph(memberProductDetails.getProductAmount(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	   }else{
            	   pdfSummaryDtsCell3 = new PdfPCell(
                           pdfAPI.addParagraph(memberProductDetails.getExistingCover(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	   }

           }else{
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph(memberProductDetails.getProductAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
           }        
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            
           if("VICS1".equalsIgnoreCase(pdfObject.getFundId()) &&  MetlifeInstitutionalConstants.INCOME_PROTECTION.equalsIgnoreCase(memberProductDetails.getProductName()) && ("$0".equalsIgnoreCase(memberProductDetails.getProductAmount()) || memberProductDetails.getProductAmount()==null)){
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph("", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
           }else{
        	   if(!CorporateHelper.isNullOrEmpty(memberProductDetails.getWaitingPeriod())) {
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph( memberProductDetails.getWaitingPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	   }
        	   else {
        		   pdfSummaryDtsCell3 = new PdfPCell(
                           pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	   }
           }   
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            
            if("VICS1".equalsIgnoreCase(pdfObject.getFundId()) &&  MetlifeInstitutionalConstants.INCOME_PROTECTION.equalsIgnoreCase(memberProductDetails.getProductName()) && ("$0".equalsIgnoreCase(memberProductDetails.getProductAmount()) || memberProductDetails.getProductAmount()==null)){
            	 pdfSummaryDtsCell3 = new PdfPCell(
                         pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            }else{
            	  if(!CorporateHelper.isNullOrEmpty(memberProductDetails.getBenefitPeriod())) {
            		  pdfSummaryDtsCell3 = new PdfPCell(
                              pdfAPI.addParagraph( memberProductDetails.getBenefitPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
               	   }
               	   else {
               		   pdfSummaryDtsCell3 = new PdfPCell(
                                  pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
               	   }
            	 
            }
         
           
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            
            if(DEATH.equalsIgnoreCase(memberProductDetails.getProductName()) ) {
            	if(pdfObject.getFulDeathAmount()!=null) {
            		pdfSummaryDtsCell3 = new PdfPCell(
                          pdfAPI.addParagraph( pdfObject.getFulDeathAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	}
            	  
            	  else {
                	  pdfSummaryDtsCell3 = new PdfPCell(
                              pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                }
            }
            
             if("Total & Permanent Disability".equalsIgnoreCase(memberProductDetails.getProductName()) ) {
            	  if(pdfObject.getFulTPDAmount()!=null) {
            		  pdfSummaryDtsCell3 = new PdfPCell(
                          pdfAPI.addParagraph( pdfObject.getFulTPDAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	  }
            	  else {
                	  pdfSummaryDtsCell3 = new PdfPCell(
                              pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                }
            }
             if(MetlifeInstitutionalConstants.INCOME_PROTECTION.equalsIgnoreCase(memberProductDetails.getProductName())) {
            	if(pdfObject.getFulIPAmount()!=null) {
            	 pdfSummaryDtsCell3 = new PdfPCell(
                          pdfAPI.addParagraph( pdfObject.getFulIPAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	}
            	  else {
                	  pdfSummaryDtsCell3 = new PdfPCell(
                              pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                }
            }
          
            
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
            /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
            
            if("UW".equalsIgnoreCase(pdfType)){    
            	if(pdfObject.getMemberDetails().isClientMatched()){
            		if(pdfObject.getMemberDetails().isShortFormDcl()){
            			/*SRKR:06/09/2016: WR14370 - Update RUW message to RUW due to client match and declined on short form.*/
            			pdfSummaryDtsCell3 = new PdfPCell(
                                pdfAPI.addParagraph("RUW due to client match and declined on short form", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            		}else{
            			pdfSummaryDtsCell3 = new PdfPCell(
                                pdfAPI.addParagraph("RUW with reason client match",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            		}
            		 
            	}else{
            		 pdfSummaryDtsCell3 = new PdfPCell(
                             pdfAPI.addParagraph(getCoverUWDetails(memberProductDetails.getProductName(), pdfObject.getMemberDetails().getResponseObject(), memberProductDetails,pdfObject.isDownSaleInd(),pdfObject), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	}
            	
                 pdfAPI.disableBorders( pdfSummaryDtsCell3);
                 pdfSummaryDtsCell3.setPadding( 4);
                 pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_CENTER);
                 pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);  
            }      
            
                if (null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length() > 0) {
                	                         
                    
                    
                    if(null != memberProductDetails.getExclusions() && !memberProductDetails.getExclusions().contains("Revised Offer")){
                    	pdfPExclusionCell1 = new PdfPCell(
                                pdfAPI.addParagraph( "Exclusion(s):", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                    }else{
                    	pdfPExclusionCell1 = new PdfPCell(
                                pdfAPI.addParagraph( " ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
                    }
                    
                    
                    pdfPExclusionCell1.disableBorderSide( 3);
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPExclusionCell1.setColspan(8);
                    }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                    	pdfPExclusionCell1.setColspan(6);
                    } else{
                    	pdfPExclusionCell1.setColspan(8);
                    }
                    
                                 
                    pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPExclusionCell1 = new PdfPCell(
                                 pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                         pdfAPI.disableBorders( pdfPExclusionCell1);
                         pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                         pdfPEmptyCell1 = new PdfPCell(
                                 pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getExclusions(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                         pdfPEmptyCell1.disableBorderSide( 1);
                         pdfPEmptyCell1.setBorderColor( pdfAPI.getBorder_color());
                         if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                         	pdfPEmptyCell1.setColspan(8);
                         }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                         	pdfPEmptyCell1.setColspan(6);
                         }else{
                         	pdfPEmptyCell1.setColspan(5);
                         }
                    }                        
                    else {
                    pdfPEmptyCell1 = new PdfPCell(
                            pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getExclusions(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                    pdfPEmptyCell1.disableBorderSide( 1);
                    pdfPEmptyCell1.setBorderColor( pdfAPI.getBorder_color());
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPEmptyCell1.setColspan(8);
                    }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                    	pdfPEmptyCell1.setColspan(6);
                    }else{
                    	pdfPEmptyCell1.setColspan(7);
                    }
                    }
    	                  
                    pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPEmptyCell1 = new PdfPCell(
                                 pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                         pdfAPI.disableBorders( pdfPEmptyCell1);
                         pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    }             
                  
                } 
                if (null!=memberProductDetails.getPrductDecReason() && memberProductDetails.getPrductDecReason().length() > 0 && "CORP".equalsIgnoreCase(pdfObject.getFundId())) {
                	 
                	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/             
                   pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "Additional cover alteration:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                    pdfPExclusionCell1.disableBorderSide( 3);
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPExclusionCell1.setColspan(8);
                    }else{ 
                    	pdfPExclusionCell1.setColspan(7);
                    }
                   
                     
                    pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPExclusionCell1 = new PdfPCell(
                                 pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                         pdfAPI.disableBorders( pdfPExclusionCell1);
                         pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                    }          
                    pdfPEmptyCell1 = new PdfPCell(
                            pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getPrductDecReason(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                    pdfPEmptyCell1.disableBorderSide( 1);
                    pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPEmptyCell1.setColspan(8);
                    }else{
                    	 pdfPEmptyCell1.setColspan(7);
                    }               
                     
                    pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	  pdfPEmptyCell1 = new PdfPCell(
                                  pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                          pdfAPI.disableBorders( pdfPEmptyCell1);
                          pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    }    
                	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/   
                   
                }
                
                if (null!=memberProductDetails.getLoadings() && memberProductDetails.getLoadings().length() > 0) {
                	                         
                    pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                    pdfPExclusionCell1.disableBorderSide( 3);
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPExclusionCell1.setColspan(8);
                    }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                    	pdfPExclusionCell1.setColspan(6);
                    }else{
                    	pdfPExclusionCell1.setColspan(7);
                    }
                    
                                 
                    pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPExclusionCell1 = new PdfPCell(
                                 pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                         pdfAPI.disableBorders( pdfPExclusionCell1);
                         pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                         pdfPEmptyCell1 = new PdfPCell(
                                 pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getLoadings(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                         pdfPEmptyCell1.disableBorderSide( 1);
                         pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                         if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                         	pdfPEmptyCell1.setColspan(8);
                         }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                         	pdfPEmptyCell1.setColspan(6);
                         }else{
                         	pdfPEmptyCell1.setColspan(5);
                         }
                    }                        
                    
                    else {
                    pdfPEmptyCell1 = new PdfPCell(
                            pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getLoadings(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                    pdfPEmptyCell1.disableBorderSide( 1);
                    pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPEmptyCell1.setColspan(8);
                    }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                    	pdfPEmptyCell1.setColspan(6);
                    }else{
                    	pdfPEmptyCell1.setColspan(7);
                    }
                    }    
                    pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPEmptyCell1 = new PdfPCell(
                                 pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                         pdfAPI.disableBorders( pdfPEmptyCell1);
                         pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    }             
                  
                }
                
                /**
                 * Display the FulAmounts
                 */
               if (null!=memberProductDetails.getFulCoverAmount() && memberProductDetails.getFulCoverAmount().length() > 1 && "UW".equalsIgnoreCase(pdfType)) {
                    
                    pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "FUL cover amount:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                    pdfPExclusionCell1.disableBorderSide( 3);
                    pdfPExclusionCell1.setBorderColor(pdfAPI.getBorder_color());
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPExclusionCell1.setColspan(8);
                    }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                    	pdfPEmptyCell1.setColspan(6);
                    }else{
                    	pdfPEmptyCell1.setColspan(7);
                    }
                    
                                 
                    pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPExclusionCell1 = new PdfPCell(
                                 pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                         pdfAPI.disableBorders( pdfPExclusionCell1);
                         pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                    }                        
                    
                    pdfPEmptyCell1 = new PdfPCell(
                            pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getFulCoverAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                    pdfPEmptyCell1.disableBorderSide( 1);
                    pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                    if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                    	pdfPEmptyCell1.setColspan(8);
                    }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                    	pdfPEmptyCell1.setColspan(6);
                    }else{
                    	pdfPEmptyCell1.setColspan(7);
                    }
    	              
    	                  
                    pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    if("UW".equalsIgnoreCase(pdfType)){
                    	 pdfPEmptyCell1 = new PdfPCell(
                                 pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                         pdfAPI.disableBorders( pdfPEmptyCell1);
                         pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                    }             
                  
                }
                

                log.info("Display column value finish");
            }
	
	private static void displayColumnValue(CostumPdfAPI pdfAPI, Document document,
            PDFObject pdfObject,MemberProductDetails memberProductDetails , String pdfType, PdfPTable pdfPSummaryDtsTable, String productName)
            throws DocumentException {
		  log.info("Display column value start");
        PdfPCell pdfSummaryDtsCell3 = null;   
        PdfPCell pdfPExclusionCell1 = null;
        PdfPCell pdfPEmptyCell1 = null;
        pdfSummaryDtsCell3 = new PdfPCell(
                pdfAPI.addParagraph( memberProductDetails.getProductName(),  new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
        log.info("inside the column value>> {}",memberProductDetails.getExistingCover());
        
        pdfSummaryDtsCell3 = new PdfPCell(
                pdfAPI.addParagraph(  memberProductDetails.getExistingCover(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        

        if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
				|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
				|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& */null!=pdfObject.getRequestType() 

    			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
        	if(null!=memberProductDetails.getTransferCoverAmnt()){
        		pdfSummaryDtsCell3 = new PdfPCell(
                        pdfAPI.addParagraph(  memberProductDetails.getTransferCoverAmnt(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	}else{
        		pdfSummaryDtsCell3 = new PdfPCell(
                        pdfAPI.addParagraph(  "$0", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	}
        	
            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
            pdfSummaryDtsCell3.setPadding( 4);
            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        }        
       
       if("UWCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
    	   if(memberProductDetails.getProductAmount()!=null){
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph(memberProductDetails.getProductAmount(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
    	   }else{
        	   pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph(memberProductDetails.getExistingCover(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
    	   }

       }else{
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph(memberProductDetails.getProductAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
       }        
        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
       if("VICS1".equalsIgnoreCase(pdfObject.getFundId()) &&  MetlifeInstitutionalConstants.INCOME_PROTECTION.equalsIgnoreCase(memberProductDetails.getProductName()) && ("$0".equalsIgnoreCase(memberProductDetails.getProductAmount()) || memberProductDetails.getProductAmount()==null)){
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph("", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
       }else if("MTAA".equalsIgnoreCase(pdfObject.getFundId())  && memberProductDetails.getExWaitingPeriod() != null && !MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())) {
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph( memberProductDetails.getExWaitingPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
       }else{
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph( memberProductDetails.getWaitingPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
       }   
        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        
        if("VICS1".equalsIgnoreCase(pdfObject.getFundId()) &&  MetlifeInstitutionalConstants.INCOME_PROTECTION.equalsIgnoreCase(memberProductDetails.getProductName()) && ("$0".equalsIgnoreCase(memberProductDetails.getProductAmount()) || memberProductDetails.getProductAmount()==null)){
        	 pdfSummaryDtsCell3 = new PdfPCell(
                     pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        }else if("MTAA".equalsIgnoreCase(pdfObject.getFundId())  && memberProductDetails.getExBenefitPeriod() != null && !MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())) {
    	   pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph( memberProductDetails.getExBenefitPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
       }else{
        	 pdfSummaryDtsCell3 = new PdfPCell(
                     pdfAPI.addParagraph( memberProductDetails.getBenefitPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        }
        
       
        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
        pdfSummaryDtsCell3.setPadding( 4);
        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
        /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
	        pdfSummaryDtsCell3 = new PdfPCell(
	                pdfAPI.addParagraph( memberProductDetails.getOccupationRating(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
	        pdfSummaryDtsCell3.setPadding( 4);
	        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
	        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);    
	        if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        	pdfSummaryDtsCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( CommonPDFHelper.formatAmount( memberProductDetails.getProductPremiumCost()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
	            pdfSummaryDtsCell3.setPadding( 4);
	            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
	            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
	        }
        	
       
        
        
        if("UW".equalsIgnoreCase(pdfType)){    
        	if(pdfObject.getMemberDetails().isClientMatched()){
        		if(pdfObject.getMemberDetails().isShortFormDcl()){
        			/*SRKR:06/09/2016: WR14370 - Update RUW message to RUW due to client match and declined on short form.*/
        			pdfSummaryDtsCell3 = new PdfPCell(
                            pdfAPI.addParagraph("RUW due to client match and declined on short form", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        		}else{
        			pdfSummaryDtsCell3 = new PdfPCell(
                            pdfAPI.addParagraph("RUW with reason client match",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        		}
        		 
        	}else{
        		 pdfSummaryDtsCell3 = new PdfPCell(
                         pdfAPI.addParagraph(getCoverUWDetails(memberProductDetails.getProductName(), pdfObject.getMemberDetails().getResponseObject(), memberProductDetails,pdfObject.isDownSaleInd(),pdfObject), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
        	}
        	
             pdfAPI.disableBorders( pdfSummaryDtsCell3);
             pdfSummaryDtsCell3.setPadding( 4);
             pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_CENTER);
             pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);  
        }      
        
            if (null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length() > 0) {
            	                         
                
                
                if(null != memberProductDetails.getExclusions() && !memberProductDetails.getExclusions().contains("Revised Offer")){
                	pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( "Exclusion(s):", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                }else{
                	pdfPExclusionCell1 = new PdfPCell(
                            pdfAPI.addParagraph( " ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
                }
                
                
                pdfPExclusionCell1.disableBorderSide( 3);
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                	pdfPExclusionCell1.setColspan(6);
                } else{
                	pdfPExclusionCell1.setColspan(7);
                }
                
                             
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }                        
                
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getExclusions(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor( pdfAPI.getBorder_color());
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                	pdfPEmptyCell1.setColspan(6);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
	              
	                  
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPEmptyCell1 = new PdfPCell(
                             pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                     pdfAPI.disableBorders( pdfPEmptyCell1);
                     pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }             
              
            } 
            if (null!=memberProductDetails.getPrductDecReason() && memberProductDetails.getPrductDecReason().length() > 0) {
            	 
            	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/             
               /* pdfPExclusionCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "Additional cover alteration:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                pdfPExclusionCell1.disableBorderSide( 3);
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else{ 
                	pdfPExclusionCell1.setColspan(7);
                }
               
                 
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }          
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getPrductDecReason(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else{
                	 pdfPEmptyCell1.setColspan(7);
                }               
                 
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	  pdfPEmptyCell1 = new PdfPCell(
                              pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                      pdfAPI.disableBorders( pdfPEmptyCell1);
                      pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }    */    
            	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/   
               
            }
            
            if (null!=memberProductDetails.getLoadings() && memberProductDetails.getLoadings().length() > 0) {
            	                         
                pdfPExclusionCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                pdfPExclusionCell1.disableBorderSide( 3);
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                	pdfPExclusionCell1.setColspan(6);
                }else{
                	pdfPExclusionCell1.setColspan(7);
                }
                
                             
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }                        
                
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getLoadings(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                	pdfPEmptyCell1.setColspan(6);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
	              
	                  
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPEmptyCell1 = new PdfPCell(
                             pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                     pdfAPI.disableBorders( pdfPEmptyCell1);
                     pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }             
              
            }
            
            /**
             * Display the FulAmounts
             */
            if (null!=memberProductDetails.getFulCoverAmount() && memberProductDetails.getFulCoverAmount().length() > 1 && "UW".equalsIgnoreCase(pdfType)) {
                
                pdfPExclusionCell1 = new PdfPCell(
                        pdfAPI.addParagraph( "FUL cover amount:", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                pdfPExclusionCell1.disableBorderSide( 3);
                pdfPExclusionCell1.setBorderColor(pdfAPI.getBorder_color());
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPExclusionCell1.setColspan(8);
                }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                	pdfPEmptyCell1.setColspan(6);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
                
                             
                pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPExclusionCell1 = new PdfPCell(
                             pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                     pdfAPI.disableBorders( pdfPExclusionCell1);
                     pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
                }                        
                
                pdfPEmptyCell1 = new PdfPCell(
                        pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getFulCoverAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                pdfPEmptyCell1.disableBorderSide( 1);
                pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
                if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
                	pdfPEmptyCell1.setColspan(8);
                }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
                	pdfPEmptyCell1.setColspan(6);
                }else{
                	pdfPEmptyCell1.setColspan(7);
                }
	              
	                  
                pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                if("UW".equalsIgnoreCase(pdfType)){
                	 pdfPEmptyCell1 = new PdfPCell(
                             pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                     pdfAPI.disableBorders( pdfPEmptyCell1);
                     pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
                }             
              
            }
            
            
            
            log.info("Display column value finish");
        
    }
	
	 public static String getCoverUWDetails(String coverType,
	            ResponseObject responseObject, MemberProductDetails memberProductDetails,boolean isDownsale, PDFObject pdfObject) {     	
		 log.info("Get cover UW details start");
		 String covType = coverType;
		 if (covType.equalsIgnoreCase( DEATH)) {
			 covType = "Term-Group";
	        } else if (covType.contains("Total")) {
	        	covType = "TPD-Group";
	        } else if (covType.equalsIgnoreCase( "Trauma")) {
	        	covType = "Trauma";
	        } else {
	        	covType = "IP";
	        }
		  StringBuilder strBuff = new StringBuilder();	        
	            if (null != responseObject
	                    && null != responseObject.getClientData()
	                    && null != responseObject.getClientData().getListInsured()) {
	                for (int insuredItr = 0; insuredItr < responseObject.getClientData().getListInsured().size(); insuredItr++) {
	                    Insured insured =  responseObject.getClientData().getListInsured().get( insuredItr);
	                    if (null != insured) {
	                        for (int overallItr = 0; overallItr < insured.getListOverallDec().size(); overallItr++) {
	                            Decision decision =  insured.getListOverallDec().get( overallItr);
	                            if (null != decision) {
	                                if (decision.getProductName().contains( covType)) {
	                                    strBuff.append( decision.getDecision());
	                                    /**
	                                     * PDF display is wrong in postprocessing  - contact bala 
	                                     */
	                                    
	                                   /* if(null!=decision.getListRating() && decision.getListRating().size()>0){
	                                    	for (int ratingItr = 0; ratingItr < decision.getListRating().size(); ratingItr++) {
		                                        Rating rating = (Rating) decision.getListRating().get( ratingItr);
		                                        if (null != rating) {
		                                            strBuff.append( " with rating: ");
		                                            strBuff.append( rating.getValue());
		                                            strBuff.append( ",");
		                                            strBuff.append( " with reason :");
		                                            strBuff.append( rating.getReason());
		                                        }
		                                        
		                                    }
	                                    }else*/ 
	                                    
	                                    log.info("How much decision >> {} ",decision.getReasons().length);
	                                    
	                                    
	                                    if(null!=decision.getReasons() && decision.getReasons().length>0){	                                    	
	                                    	
	                                    	 if(null!=decision.getTotalDebitsValue() && !"".equalsIgnoreCase(decision.getTotalDebitsValue().trim()) && new BigDecimal(decision.getTotalDebitsValue()).doubleValue() > 0){
	                                    		 	strBuff.append( " with rating: ");
		                                            strBuff.append( decision.getTotalDebitsValue());
		                                            strBuff.append( ",");
	                                    	 }
	                                    	for (int reasonItr = 0; reasonItr <decision.getReasons().length; reasonItr++) {
	                                    		strBuff.append( " with reason: ");
	                                    		strBuff.append( decision.getReasons()[reasonItr]);
	                                    		strBuff.append( ",");
	                                    		
	                                    	}
	                                    	
	                                    }
	                                }
	                            }
	                            
	                        }
	                    }
	                    
	                }
	            }else if("NUW".equalsIgnoreCase(memberProductDetails.getAalLevelStatus())){
	            	strBuff.append("ACC");
	            }
	            /*ING downsale*/
	            if("Term-Group".equalsIgnoreCase(covType) && isDownsale){
	            	strBuff = new StringBuilder();
	            	 strBuff.append("ACC");
	            }
	            if(pdfObject!=null && pdfObject.getFormLength()!=null && pdfObject.getFormLength().equalsIgnoreCase("Long") &&
	            		pdfObject.getProductName()!=null && pdfObject.getProductName().equalsIgnoreCase("VICS1") && strBuff!=null &&  strBuff.toString().length()>0){
	            	strBuff = new StringBuilder();
	            	strBuff.append("RUW");
	            }
	            log.info("Get cover UW details finish");
	        return strBuff.toString();
	    }
	 
	 	public static String toTitleCase(String input) {
	 		log.info("To title case start");
	 		String inpt = input;
		    StringBuilder titleCase = new StringBuilder();
		    boolean nextTitleCase = true;
		    if(null!=inpt && inpt.trim().length()>0){
		    	inpt = inpt.toLowerCase();
		        for (char c : inpt.toCharArray()) {
			        if (Character.isSpaceChar(c)) {
			            nextTitleCase = true;
			        } else if (nextTitleCase) {
			            c = Character.toTitleCase(c);
			            nextTitleCase = false;
			        }

			        titleCase.append(c);
			    }
		    }
		
		    log.info("To title case finish");
		    return titleCase.toString();
		}
	 	/**
	     * Description: display declaration details in the PDF Section
	     * 
	     * @param com.metlife.eapplication.dto.ApplicationDTO
	     *            com.metlife.eapplication.pdf.CostumPdfAPI,com.lowagie.text.Document
	     * @return void
	     * @throws DocumentException,MetlifeException,
	     *             IOException
	     */
	    
	   /* public static void declarationDetails(List disclosureStatementList,
	            CostumPdfAPI pdfAPI, Document document) throws DocumentException {
	    	log.info("Declaration details start");

	        
	       
	        PdfPTable pdfPDeclarationTable = null;
	        DisclosureStatement disclosureStatement = null;
	        PdfPCell pdfPDeclarationCell = null;
	        PdfPTable pdfPTable = null;
	        PdfPTable pdfPTableAck = null;
	        PdfPTable pdfPTableStatement = null;
	        PdfPCell pdfPCell = null;
	        PdfPCell pdfPCell1 = null;
	        PdfPCell pdfPCelAck = null;
	        
	        pdfPTable = new PdfPTable( 1);
	        pdfPCell = new PdfPCell(
	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPCell);
	        pdfPCell.setPadding( 4);
	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	        pdfPTable.addCell( pdfPCell);
	        document.add( pdfPTable);
	        
	        if ( null != disclosureStatementList && disclosureStatementList.size()>0 && !disclosureStatementList.isEmpty()) {
	            
	            pdfPDeclarationTable = new PdfPTable( 1);
	            pdfPDeclarationCell = new PdfPCell(
	                    pdfAPI.addParagraph( "Declaration", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
	            pdfPDeclarationCell.setBorderWidth( 1);
	            pdfPDeclarationCell.setBorderColor( pdfAPI.getColor( sectionBackground));
	            pdfPDeclarationCell.setBorderColor(pdfAPI.getBorder_color());
	            pdfAPI.disableBorders( pdfPDeclarationCell);
	            pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
	            pdfPDeclarationCell.setPaddingBottom( 6);
	            pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
	            pdfPDeclarationTable.addCell( pdfPDeclarationCell);
	            document.add( pdfPDeclarationTable);
	            
	            for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
	                disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
	                pdfPTable = new PdfPTable( 1);
	                pdfPCell = new PdfPCell(
	                        pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, FontFactory.HELVETICA, 8, Font.NORMAL, colorStyle));
	                pdfAPI.disableBorders( pdfPCell);
	                pdfPCell.setPadding( 4);
	                pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	                pdfPTable.addCell( pdfPCell);
	                document.add( pdfPTable);
	                
	                float[] f = { .9f, 0.1f };               
	                if (null != disclosureStatement
	                        && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
	                    pdfPTableStatement = new PdfPTable( 1);
	                    pdfPCell1 = new PdfPCell(
	                            pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	                    pdfAPI.disableBorders( pdfPCell1);
	                    pdfPCell1.setPadding( 4);
	                    pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
	                    pdfPTableStatement.addCell( pdfPCell1);
	                    document.add( pdfPTableStatement);
	                    
	                    pdfPTableAck = new PdfPTable( f);
	                    
	                   if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
	                	   pdfPCelAck = new PdfPCell(
	                               pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	                   }else{
	                    	pdfPCelAck = new PdfPCell(
	                                pdfAPI.addParagraph( "I acknowledge I have read, understood and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	                        
	                    }                
	                    
	                    
	                    
	                    pdfAPI.disableBorders( pdfPCelAck);
	                    pdfPCelAck.setPadding( 4);
	                    pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
	                    pdfPTableAck.addCell( pdfPCelAck);
	                    pdfPCell1 = new PdfPCell(
	                            pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
	                    pdfAPI.disableBorders( pdfPCell1);
	                    pdfPCell1.setPadding( 4);
	                    pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
	                    pdfPTableAck.addCell( pdfPCell1);
	                    document.add( pdfPTableAck);
	                }
	                
	            }
	        }
	        pdfPDeclarationTable = null;
	        disclosureStatement = null;
	        pdfPDeclarationCell = null;
	        pdfPTable = null;
	        pdfPTableAck = null;
	        pdfPTableStatement = null;
	        pdfPCell = null;
	        pdfPCell1 = null;
	        pdfPCelAck = null;
	        log.info("Declaration details finish");
	        
	    }*/
	 	//Added for INGD PDF by 561132
	 	public static void displayINGDHeaderNote(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType, String headerText) throws MalformedURLException, IOException, DocumentException{
		log.info("Display INGD header note start");
		Image gif;
		 PdfPTable pdfPTable2 = null;
		 PdfPCell pdfPCell4 = null;
		 PdfPTable pdfSpaceTable1 = null;
		 PdfPCell pdfPSpaceCell1 = null;
		 PdfPTable pdfPreffConDtsTable = null; 
        PdfPCell pdfPreffConDetsCell3 = null;
		float[] tableSize = { .7f, .3f };
	    PdfPTable pdfPTable = new PdfPTable( tableSize);
	    PdfPCell pdfPCell = new PdfPCell(
	            pdfAPI.addParagraph("", new Font(pdfAPI.getSwsMed(),18,0,pdfAPI.getHeader_color())));
	    pdfAPI.disableBorders( pdfPCell);
	    pdfPCell.setPadding( 2);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	    pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	    pdfPTable.addCell( pdfPCell);
	    pdfPCell = new PdfPCell();
	    if (null != pdfObject.getLogPath()) {
	    	 gif = Image.getInstance(pdfObject.getLogPath());
	    	  gif.setAlignment( Image.ALIGN_RIGHT);	 
	    	  pdfPCell.setFixedHeight(53f);
	    	  pdfPCell.setImage( gif);
	    }	
	    
	    pdfPCell.setHorizontalAlignment( Element.ALIGN_LEFT);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	    
	   /* pdfPCell.setPaddingRight( 4);
       pdfPCell.setPadding( 4);
       pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);*/
       pdfAPI.disableBorders( pdfPCell);
       pdfPTable.addCell( pdfPCell);
       document.add(pdfPTable);
      //float[] tableSize1 = { .7f, .3f };
   	pdfPTable = new PdfPTable(1);
    pdfPCell = new PdfPCell(
           pdfAPI.addParagraph(" "));
    pdfAPI.disableBorders( pdfPCell);
    pdfPCell.setPadding( 4);
    pdfPTable.addCell( pdfPCell);
   document.add(pdfPTable);
       /*pdfPTable = new PdfPTable(1);
   	PdfPCell pdfPSpaceHead = new PdfPCell(pdfAPI.addSingleCell());		
   	pdfAPI.disableBorders(pdfPSpaceHead);
   	pdfPTable.addCell(pdfPSpaceHead);
   	document.add(pdfPTable); */
   	
       
       
   	pdfPTable = new PdfPTable(1);
     pdfPCell = new PdfPCell(
            pdfAPI.addParagraph(headerText,new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
     pdfAPI.disableBorders( pdfPCell);
     pdfPCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
     pdfPCell.setPadding( 4);
	    pdfPCell.setVerticalAlignment( Element.ALIGN_CENTER);
	    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
     pdfPTable.addCell( pdfPCell);
    document.add(pdfPTable);
    //float[] f1 = {  0.2f, 0.4f,0.2f,0.4f };	
    float[] f1 = { 0.2f, 0.8f };
    pdfPTable = new PdfPTable(f1);
    if(pdfObject.isQuickQuoteRender()){
    	pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_QUOT, pdfObject.getColorStyle(),7));
    }else{
    	pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, pdfObject.getColorStyle(),7));
    }
	//pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, pdfObject.getColorStyle(),7));
	pdfAPI.disableBorders(pdfPCell);
	pdfPCell.setPadding(4);
	pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);		
	pdfPTable.addCell(pdfPCell);	
	pdfPCell = new PdfPCell(pdfAPI.addParagraph(CommonPDFHelper.getTodaysDate(),pdfObject.getColorStyle(),7));
	pdfAPI.disableBorders(pdfPCell);
	pdfPCell.setPadding(4);
	pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);		
	pdfPTable.addCell(pdfPCell);

	if((!pdfObject.isQuickQuoteRender()) || (pdfObject.isQuickQuoteRender() && pdfObject.getApplicationNumber()!=null &&pdfObject.getApplicationNumber()!="" )){
	pdfPCell = new PdfPCell(pdfAPI.addParagraph("Application Number:", pdfObject.getColorStyle(),7));
	pdfAPI.disableBorders(pdfPCell);
	pdfPCell.setPadding(4);
	pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);		
	pdfPTable.addCell(pdfPCell);	
	pdfPCell = new PdfPCell(pdfAPI.addParagraph(pdfObject.getApplicationNumber(),pdfObject.getColorStyle(),7));
	pdfAPI.disableBorders(pdfPCell);
	pdfPCell.setPadding(4);
	pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);		
	pdfPTable.addCell(pdfPCell);
	}
	document.add(pdfPTable);
	
	pdfPTable = new PdfPTable(1);
	PdfPCell pdfPSpaceCell = new PdfPCell(pdfAPI.addSingleCell());		
	pdfAPI.disableBorders(pdfPSpaceCell);
	pdfPTable.addCell(pdfPSpaceCell);
	document.add(pdfPTable);    
        log.info("Display INGD header note finish");
	}

	 	public static void displayINGDPersonalDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		log.info("Display personal details start");
		 PdfPTable pdfPTable = null;
		 PdfPCell pdfPCell2 = null;
		 boolean titleRender = true;
         boolean firstNameRender = true;
         boolean surNameRender = true;
         boolean dobRender = true;
         
         if(pdfObject.isQuickQuoteRender()){
         	if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getTitle() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getTitle())){
	            	titleRender = true;
	            }else{
	            	titleRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getMemberFName() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getMemberFName())){
	            	firstNameRender = true;
	            }else{
	            	firstNameRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getMemberLName() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getMemberLName())){
	            	surNameRender = true;
	            }else{
	            	surNameRender = false;
	            }
	            
	            if(null!=pdfObject && null!=pdfObject.getMemberDetails() && null!=pdfObject.getMemberDetails().getDob() && !"".equalsIgnoreCase(pdfObject.getMemberDetails().getDob())){
	            	dobRender = true;
	            }else{
	            	dobRender = false;
	            }
         }
		 
			  pdfPTable = new PdfPTable( 1);
		 if(titleRender || firstNameRender || surNameRender || dobRender){
	        pdfPCell2 = new PdfPCell(
	                pdfAPI.addParagraph( "Your Details", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	        pdfPCell2.setBorderWidth( 1);
	        pdfAPI.disableBorders( pdfPCell2);
	        pdfPCell2.setBackgroundColor(pdfAPI.getHeader_color());
	        pdfPCell2.setPaddingBottom( 6);
	        pdfPCell2.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPTable.addCell( pdfPCell2);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	pdfPCell2 = new PdfPCell(
		                pdfAPI.addParagraph( "", pdfObject.getColorStyle(), 10));
		        pdfAPI.disableBorders( pdfPCell2);
		        pdfPTable.addCell( pdfPCell2);
	        }	        
	        document.add( pdfPTable);
		 }
			if("UW".equalsIgnoreCase(pdfType)){
				  float[] f3 = { 0.04f, 0.08f, 0.1f, 0.12f };
				  pdfPTable = new PdfPTable( f3);
				 
			}else{
				  float[] f3 = { 0.04f, 0.08f, 0.1f, 0.12f};
				  pdfPTable = new PdfPTable( f3);
			}    
	            PdfPCell pdfPCell3 = null;
	            if(titleRender){
	            	Paragraph p = new Paragraph( "Title:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk(pdfObject.getMemberDetails().getTitle(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            }else{
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(firstNameRender){
	            	Paragraph p = new Paragraph( "Firstname:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk(pdfObject.getMemberDetails().getMemberFName(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            }else{
	            	pdfPCell3 = new PdfPCell(
		                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            	            
	            if(surNameRender){
	            	Paragraph p = new Paragraph( "Last Name:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk(pdfObject.getMemberDetails().getMemberLName(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            }else{
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            
	            if(dobRender){
	            	Paragraph p = new Paragraph( "Date of birth:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk( pdfObject.getMemberDetails().getDob(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            }else{
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            if("UW".equalsIgnoreCase(pdfType)){
	            	 pdfPCell3 = new PdfPCell(
	 	                    pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	 	            pdfAPI.disableBorders( pdfPCell3);
	 	            pdfPTable.addCell( pdfPCell3);
	            }          
	            
	            document.add( pdfPTable);
	            
	           	            
	            if("UW".equalsIgnoreCase(pdfType)){
					  float[] f4 = { 0.04f, 0.08f, 0.1f, 0.12f};
					  pdfPTable = new PdfPTable( f4);
					 
				}else{
					  float[] f4 = { 0.04f, 0.08f, 0.1f, 0.12f};
					  pdfPTable = new PdfPTable( f4);
				}    
	            if(pdfObject.getMemberDetails().getDateJoinedFund()!=null) {
	            	Paragraph p = new Paragraph( "Date joined:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk(pdfObject.getMemberDetails().getDateJoinedFund(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            }
	            else {
	            	Paragraph p = new Paragraph( "Date joined:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk("", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            }
	            if(pdfObject.getMemberDetails().getGender() != null) {
	            	Paragraph p = new Paragraph( "Gender:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk(pdfObject.getMemberDetails().getGender(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            }else {
	            	pdfPCell3 = new PdfPCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            }
	            pdfPCell3.setPadding( 4);
	            pdfPCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPTable.addCell( pdfPCell3);
	            if(null != pdfObject.getMemberDetails().getClientRefNum() && null != pdfObject.getApplicationNumber() && !pdfObject.getMemberDetails().getClientRefNum().equalsIgnoreCase(pdfObject.getApplicationNumber())){
	            	
	            	if("UW".equalsIgnoreCase(pdfType)) {
	            	Paragraph p = new Paragraph( "Member number:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p.add(new Chunk(pdfObject.getMemberDetails().getClientRefNum(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p);
	            	}
	            	else {
	            		Paragraph p = new Paragraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            		p.add(new Chunk("", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            		pdfPCell3 = new PdfPCell(p);
	            	}
	            	
	 	            pdfPTable.addCell( pdfPCell3);
	 	            pdfPCell3.setNoWrap(Boolean.FALSE);
	 	            if("UW".equalsIgnoreCase(pdfType)){
	 			        String calculatedBMI = null;
	 			        if(pdfObject.getMemberDetails().getResponseObject()!=null && pdfObject.getMemberDetails().getResponseObject().getClientData() != null){
	 			        	for (Iterator iter = pdfObject.getMemberDetails().getResponseObject().getClientData().getListInsured().iterator(); iter.hasNext();) {
	 							Insured insured = (Insured) iter.next();
	 							log.info("insured.getBmi()>> {}",insured.getBmi());
	 							calculatedBMI = insured.getBmi();
	 						}
	 			        }
	 	               Paragraph p1 = new Paragraph( "Calculated BMI:  ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
		            if(calculatedBMI!=null) {
	 	               p1.add(new Chunk(calculatedBMI, new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            }
		            else {
		            	p1.add(new Chunk("", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            }
		            	pdfPCell3 = new PdfPCell(p1);
		 	            pdfPTable.addCell( pdfPCell3);
	 	            }	
	 	            else {
	 	           Paragraph p1 = new Paragraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color()));
	            	p1.add(new Chunk("", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	            	pdfPCell3 = new PdfPCell(p1);
	 	            pdfPTable.addCell( pdfPCell3);
	 	            }
	            }
	            document.add( pdfPTable);
	   		log.info("Display INGD personal details finish");
	}
	 	public static void displayINGDCoverSummary(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
		   log.info("Display INGD cover summary start");
		   PdfPTable pdfPCoverTable = null;
		   PdfPCell pdfPCoverCell = null;
		   PdfPTable pdfPEmptyTable = null;
		   PdfPCell pdfPEmptyCell3 = null;
		   PdfPTable pdfPSummaryDtsTable = null;
		   PdfPCell pdfcoverCostCell1 = null;
		   MemberProductDetails memberProductDetails = null;
		   
		   pdfPCoverTable = new PdfPTable(1);
			PdfPCell pdfPSpaceCell = new PdfPCell(pdfAPI.addSingleCell());		
			pdfAPI.disableBorders(pdfPSpaceCell);
			pdfPCoverTable.addCell(pdfPSpaceCell);
			document.add(pdfPCoverTable);    
		   if("UW".equalsIgnoreCase(pdfType)){
			   float[] coverSumm = { .8f, .2f };
		       pdfPCoverTable = new PdfPTable( coverSumm);
		   }else{
			   pdfPCoverTable = new PdfPTable(1);
		   }  
	        
	        pdfPCoverCell = new PdfPCell(
	                pdfAPI.addParagraph( "Your Cover", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
	        pdfPCoverCell.setBorderWidth( 1);
	        pdfAPI.disableBorders( pdfPCoverCell);
	        pdfPCoverCell.setBackgroundColor(pdfAPI.getHeader_color());
	        pdfPCoverCell.setPaddingBottom( 6);
	        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPCoverTable.addCell( pdfPCoverCell);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	pdfPCoverCell = new PdfPCell(
		                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS,  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		        pdfPCoverCell.setBorderWidth( 1);
		        pdfPCoverCell.setBorderColor(pdfAPI.getBorder_color());
		        pdfAPI.disableBorders( pdfPCoverCell);
		        pdfPCoverCell.setBackgroundColor(pdfAPI.getHeader_color());
		        pdfPCoverCell.setPaddingBottom( 6);
		        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPCoverTable.addCell( pdfPCoverCell);
	        }	        
	        document.add( pdfPCoverTable);
	        
	        pdfPEmptyTable = new PdfPTable( 1);
	        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
	        pdfAPI.disableBorders( pdfPEmptyCell3);
	        pdfPEmptyTable.addCell( pdfPEmptyCell3);
	        document.add( pdfPEmptyTable);	     	        	       
	      
	        if("UW".equalsIgnoreCase(pdfType)){

	        	if( null!=pdfObject.getRequestType() 
	        			 && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
	        		 displayINGDColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.20f,0.2f };
	        		 displayINGDColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else{
		        			 float[] f5 = { 0.15f, 0.14f, 0.15f, 0.09f, 0.08f, 0.13f, 0.09f, 0.17f };
		        			 displayINGDColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}	
	        }else{
	        	if(null!=pdfObject.getRequestType() 

	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
	        		 displayINGDColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.2f };
	        		 displayINGDColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        	}else{
		        			 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
		        			 displayINGDColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
	        		
	        	}	
	        }
	        
	        if("UW".equalsIgnoreCase(pdfType)){
	        	if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f6 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.20f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else{
	        		 float[] f6 = { 0.15f, 0.14f, 0.15f, 0.09f, 0.08f, 0.13f, 0.09f, 0.17f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}
	        }else{
	        	if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		 float[] f6 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        		float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.2f };
		        	pdfPSummaryDtsTable = new PdfPTable( f6);
	        	}else{
	        			float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
			        	pdfPSummaryDtsTable = new PdfPTable( f6);	
	        	}
	        }
	        for (int itr = 0; itr < pdfObject.getMemberDetails().getMemberProductList().size(); itr++) {
	        	memberProductDetails = pdfObject.getMemberDetails().getMemberProductList().get(itr);
	        	displayINGDColumnValue( pdfAPI, document,pdfObject,memberProductDetails, pdfType,pdfPSummaryDtsTable,pdfObject.getProductName());	 
	        	
	        }
	        pdfcoverCostCell1 = new PdfPCell(
	                pdfAPI.addParagraph("The monthly premium of your requested cover is",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
	        	
		        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_RIGHT);
		        pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());

		        if(null!=pdfObject.getRequestType() 
	        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        	pdfcoverCostCell1.setColspan(7);
		        }
		        else{
		        	pdfcoverCostCell1.setColspan(6);
		        }
		        if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        	 pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        	 
		        	 pdfcoverCostCell1 = new PdfPCell(
			 	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getTotalPremium(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			 	        pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());
			 	        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_CENTER);
			 	        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
			 	      
			 	       if("UW".equalsIgnoreCase(pdfType)){
				        	pdfcoverCostCell1 = new PdfPCell(
					                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
					        pdfAPI.disableBorders( pdfcoverCostCell1);
					        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
				        }
		        }
		        document.add( pdfPSummaryDtsTable);
		        /* added for Cancel cover reason */    
	 	       if( null!=pdfObject.getRequestType()  && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType()) && (null!=pdfObject.getFundId() && !"CARE".equalsIgnoreCase(pdfObject.getFundId())) ){
	 	        	PdfPTable pdfCancelReasonTable = null;
	 	        	float[] f = { .5f, 0.5f }; 
	 	        	pdfCancelReasonTable = new PdfPTable(f);
	 	 	        PdfPCell pdfPCell = null;
	 	 	        PdfPCell pdfPCellAns = null;
	 	 	        pdfPCell= new PdfPCell(pdfAPI.addParagraph("Reason for cancelling your cover ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
	 	 	        pdfAPI.disableBorders( pdfPCell);
	 	 	        pdfPCell.setPadding(4);
	 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	 	        pdfCancelReasonTable.addCell( pdfPCell);
	 	 	        if(pdfObject.getMemberDetails().getCancelreason() != null && pdfObject.getMemberDetails().getCancelreason() != ""){
	 	 	        	if(pdfObject.getMemberDetails().getCancelreason().equalsIgnoreCase("Other") && pdfObject.getMemberDetails().getCancelotherreason() != "" && pdfObject.getMemberDetails().getCancelotherreason() != null){
		 	 	        	pdfPCellAns = new PdfPCell(pdfAPI.addParagraph(pdfObject.getMemberDetails().getCancelotherreason(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }else{
		 	 	        	pdfPCellAns = new PdfPCell(pdfAPI.addParagraph(pdfObject.getMemberDetails().getCancelreason(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }
	 	 	        }
	 	 	        pdfAPI.disableBorders( pdfPCellAns);
	 	 	        if(null!= pdfPCellAns){
	 	 	        pdfPCellAns.setPadding(4);
	 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
	 	 	        }
	 	 	        pdfCancelReasonTable.addCell( pdfPCellAns);
	 	 	        document.add(pdfCancelReasonTable);
	 	        }
	 	      /* added for Cancel cover reason */    
			    log.info("Display INGD personal details finish");
	}
	 	private static void displayINGDColumnName(CostumPdfAPI pdfAPI, Document document,float[] f5, PDFObject pdfObject,String pdfType, String productName)
	            throws DocumentException {
			log.info("Display INGD column name start");
			 PdfPTable pdfPSummaryTable = new PdfPTable( f5);
			 PdfPCell pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Cover type",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);
		        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Existing cover", new Font(pdfAPI.getSwsMed(),8,0, pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        

	        if(null!=pdfObject.getRequestType() 
	    			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        	 pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Transfer cover", new Font(pdfAPI.getSwsMed(),8,0, pdfAPI.getFont_color())));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        }
	        
	         if(pdfObject.getMemberDetails().isReplaceCvrOption() && !"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
	        	 pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Additional cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);
	         }        
	        else if(!"DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
	        	 pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Total cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        }else if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
	        	pdfPSummaryCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( "Total cover", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
	            pdfAPI.disableBorders( pdfPSummaryCell3);
	            pdfPSummaryCell3.setPadding( 4);
	            pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	            pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	            pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        }
	       
	      
	        
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Waiting period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Benefit period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
	        pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Occupation rating", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor(  pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);  
	     
	        
		    	 if(pdfObject.getMemberDetails().isReplaceCvrOption()){
		         	pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Total Monthly Premium", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
		         }else if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		         	pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Total Monthly Premium", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
		         }
		   
	        
	           
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Final decision after post processing",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        document.add( pdfPSummaryTable);

	         log.info("Display INGD column name finish");
	        
	        
	    }
	 	
	 	private static void displayINGDColumnValue(CostumPdfAPI pdfAPI, Document document,
	            PDFObject pdfObject,MemberProductDetails memberProductDetails , String pdfType, PdfPTable pdfPSummaryDtsTable, String productName)
	            throws DocumentException {
		  log.info("Display INGD column value start");
      PdfPCell pdfSummaryDtsCell3 = null;   
      PdfPCell pdfPExclusionCell1 = null;
      PdfPCell pdfPEmptyCell1 = null;
      pdfSummaryDtsCell3 = new PdfPCell(
              pdfAPI.addParagraph( memberProductDetails.getProductName(),  new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getFont_color())));
      pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
      pdfSummaryDtsCell3.setPadding( 4);
      pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
      pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
      pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
      
      log.info("inside the column value>> {}",memberProductDetails.getExistingCover());
      
      pdfSummaryDtsCell3 = new PdfPCell(
              pdfAPI.addParagraph(  memberProductDetails.getExistingCover(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
      pdfSummaryDtsCell3.setPadding( 4);
      pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
      pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
      pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
      

      if(null!=pdfObject.getRequestType() 
  			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
      	if(null!=memberProductDetails.getTransferCoverAmnt()){
      		pdfSummaryDtsCell3 = new PdfPCell(
                      pdfAPI.addParagraph(  memberProductDetails.getTransferCoverAmnt(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      	}else{
      		pdfSummaryDtsCell3 = new PdfPCell(
                      pdfAPI.addParagraph(  "$0", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      	}
      	
          pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
          pdfSummaryDtsCell3.setPadding( 4);
          pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
          pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
          pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
      }        
     
     if("UWCOVER".equalsIgnoreCase(pdfObject.getRequestType())){
  	   if(memberProductDetails.getProductAmount()!=null){
      	   pdfSummaryDtsCell3 = new PdfPCell(
                     pdfAPI.addParagraph(memberProductDetails.getProductAmount(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
  	   }else{
      	   pdfSummaryDtsCell3 = new PdfPCell(
                     pdfAPI.addParagraph(memberProductDetails.getExistingCover(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
  	   }

     }else{
  	   pdfSummaryDtsCell3 = new PdfPCell(
                 pdfAPI.addParagraph(memberProductDetails.getProductAmount(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
     }        
      pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
      pdfSummaryDtsCell3.setPadding( 4);
      pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
      pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
      pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
  	   pdfSummaryDtsCell3 = new PdfPCell(
                 pdfAPI.addParagraph( memberProductDetails.getWaitingPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
      pdfSummaryDtsCell3.setPadding( 4);
      pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
      pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
      pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
      	 pdfSummaryDtsCell3 = new PdfPCell(
                   pdfAPI.addParagraph( memberProductDetails.getBenefitPeriod(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      
     
      pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
      pdfSummaryDtsCell3.setPadding( 4);
      pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
      pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
      pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
      /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
	        pdfSummaryDtsCell3 = new PdfPCell(
	                pdfAPI.addParagraph( memberProductDetails.getOccupationRating(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	        pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
	        pdfSummaryDtsCell3.setPadding( 4);
	        pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
	        pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	        pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);    
	        if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
	        	pdfSummaryDtsCell3 = new PdfPCell(
	                    pdfAPI.addParagraph( CommonPDFHelper.formatAmount( memberProductDetails.getProductPremiumCost()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
	            pdfSummaryDtsCell3.setBorderColor(pdfAPI.getBorder_color());
	            pdfSummaryDtsCell3.setPadding( 4);
	            pdfSummaryDtsCell3.setHorizontalAlignment( Element.ALIGN_CENTER);
	            pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_MIDDLE);
	            pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);
	        }
      	
     
      
      
      if("UW".equalsIgnoreCase(pdfType)){    
      	if(pdfObject.getMemberDetails().isClientMatched()){
      		if(pdfObject.getMemberDetails().isShortFormDcl()){
      			/*SRKR:06/09/2016: WR14370 - Update RUW message to RUW due to client match and declined on short form.*/
      			pdfSummaryDtsCell3 = new PdfPCell(
                          pdfAPI.addParagraph("RUW due to client match and declined on short form", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      		}else{
      			pdfSummaryDtsCell3 = new PdfPCell(
                          pdfAPI.addParagraph("RUW with reason client match",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      		}
      		 
      	}else{
      		 pdfSummaryDtsCell3 = new PdfPCell(
                       pdfAPI.addParagraph(getCoverUWDetails(memberProductDetails.getProductName(), pdfObject.getMemberDetails().getResponseObject(), memberProductDetails,pdfObject.isDownSaleInd(),pdfObject), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
      	}
      	
           pdfAPI.disableBorders( pdfSummaryDtsCell3);
           pdfSummaryDtsCell3.setPadding( 4);
           pdfSummaryDtsCell3.setVerticalAlignment( Element.ALIGN_CENTER);
           pdfPSummaryDtsTable.addCell( pdfSummaryDtsCell3);  
      }      
      
          if (null!=memberProductDetails.getExclusions() && memberProductDetails.getExclusions().length() > 0) {
          	                         
              
              
              if(null != memberProductDetails.getExclusions() && !memberProductDetails.getExclusions().contains("Revised Offer")){
            	  if("INGD".equalsIgnoreCase(pdfObject.getFundId())){
            		  pdfPExclusionCell1 = new PdfPCell(
                              pdfAPI.addParagraph( "Exclusion(s) applicable to new tailored cover", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	  }else{
              	pdfPExclusionCell1 = new PdfPCell(
                          pdfAPI.addParagraph( "Exclusion(s):", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
            	  }
              }else{
              	pdfPExclusionCell1 = new PdfPCell(
                          pdfAPI.addParagraph( " ", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
              }
              
              
              pdfPExclusionCell1.disableBorderSide( 3);
              if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
              	pdfPExclusionCell1.setColspan(8);
              }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
              	pdfPExclusionCell1.setColspan(6);
              } else{
              	pdfPExclusionCell1.setColspan(7);
              }
              
                           
              pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
              if("UW".equalsIgnoreCase(pdfType)){
              	 pdfPExclusionCell1 = new PdfPCell(
                           pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                   pdfAPI.disableBorders( pdfPExclusionCell1);
                   pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
              }                        
              
              pdfPEmptyCell1 = new PdfPCell(
                      pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getExclusions(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
              pdfPEmptyCell1.disableBorderSide( 1);
              pdfPEmptyCell1.setBorderColor( pdfAPI.getBorder_color());
              if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
              	pdfPEmptyCell1.setColspan(8);
              }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
              	pdfPEmptyCell1.setColspan(6);
              }else{
              	pdfPEmptyCell1.setColspan(7);
              }
	              
	                  
              pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
              if("UW".equalsIgnoreCase(pdfType)){
              	 pdfPEmptyCell1 = new PdfPCell(
                           pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                   pdfAPI.disableBorders( pdfPEmptyCell1);
                   pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
              }             
            
          } 
          if (null!=memberProductDetails.getLoadings() && memberProductDetails.getLoadings().length() > 0) {
          	                         
              pdfPExclusionCell1 = new PdfPCell(
                      pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
              pdfPExclusionCell1.disableBorderSide( 3);
              if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
              	pdfPExclusionCell1.setColspan(8);
              }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
              	pdfPExclusionCell1.setColspan(6);
              }else{
              	pdfPExclusionCell1.setColspan(7);
              }
              
                           
              pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
              if("UW".equalsIgnoreCase(pdfType)){
              	 pdfPExclusionCell1 = new PdfPCell(
                           pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
                   pdfAPI.disableBorders( pdfPExclusionCell1);
                   pdfPSummaryDtsTable.addCell( pdfPExclusionCell1);
              }                        
              
              pdfPEmptyCell1 = new PdfPCell(
                      pdfAPI.addSingleCell( pdfAPI.addParagraph( memberProductDetails.getLoadings(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
              pdfPEmptyCell1.disableBorderSide( 1);
              pdfPEmptyCell1.setBorderColor(pdfAPI.getBorder_color());
              if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){                	
              	pdfPEmptyCell1.setColspan(8);
              }else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
              	pdfPEmptyCell1.setColspan(6);
              }else{
              	pdfPEmptyCell1.setColspan(7);
              }
	              
	                  
              pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
              if("UW".equalsIgnoreCase(pdfType)){
              	 pdfPEmptyCell1 = new PdfPCell(
                           pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
                   pdfAPI.disableBorders( pdfPEmptyCell1);
                   pdfPSummaryDtsTable.addCell( pdfPEmptyCell1);
              }             
            
          }
          
          log.info("Display INGD column value finish");
      
  }

	 	
	 	public static void displayINGDQuotationDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
	 		PdfPTable pdfPOccupationTable = null;
			DisclosureQuestionAns disclosureQuestionAns = null;
			PdfPCell pdfCell = null;
			
			   	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
		        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);
		        
		        pdfPEmptyTable = new PdfPTable( 1);
		        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);
		        
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 float[] occupationFloat = { 0.8f, 0.2f };
		        	  pdfPOccupationTable = new PdfPTable( occupationFloat);
		        }else{
		        	  pdfPOccupationTable = new PdfPTable( 1);
		        }
		        
		        PdfPCell pdfPOccupationCell = new PdfPCell(
		                pdfAPI.addParagraph( "Your Response",  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		        pdfPOccupationCell.setBorderWidth( 1);
		        pdfAPI.disableBorders( pdfPOccupationCell);
		        pdfPOccupationCell.setBackgroundColor(pdfAPI.getHeader_color());
		        pdfPOccupationCell.setPaddingBottom( 6);
		        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPOccupationTable.addCell( pdfPOccupationCell);
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 pdfPOccupationCell = new PdfPCell(
		 	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS, new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		 	        pdfPOccupationCell.setBorderWidth( 1);
		 	        pdfAPI.disableBorders( pdfPOccupationCell);
		 	        pdfPOccupationCell.setBackgroundColor(pdfAPI.getHeader_color());
		 	        pdfPOccupationCell.setPaddingBottom( 6);
		 	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		 	        pdfPOccupationTable.addCell( pdfPOccupationCell);
		        }
		       
		        document.add( pdfPOccupationTable);
		       
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
		        	 pdfPOccupationTable = new PdfPTable( f1);
		        }else{
		        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
		        	 pdfPOccupationTable = new PdfPTable( f1);
		        }        
			
			if(null!=pdfObject.getDisclosureQuestionList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureQuestionList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureQuestionList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable);
		    log.info("Display quotation details finish");
	 	}
	 	//Added for premium calculator
	 	  public static void premiumDeclarationDetails(List disclosureStatementList,
	 	            CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject) throws DocumentException {
	 	    	log.info("Premium calculator - Disclaimer start");

	 	        
	 	       
	 	        PdfPTable pdfPDeclarationTable = null;
	 	        DisclosureStatement disclosureStatement = null;
	 	        PdfPCell pdfPDeclarationCell = null;
	 	        PdfPTable pdfPTable = null;
	 	        PdfPTable pdfPTableAck = null;
	 	        PdfPTable pdfPTableStatement = null;
	 	        PdfPCell pdfPCell = null;
	 	        PdfPCell pdfPCell1 = null;
	 	        PdfPCell pdfPCelAck = null;
	 	        
	 	        pdfPTable = new PdfPTable( 1);
	 	        pdfPCell = new PdfPCell(
	 	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	 	        pdfAPI.disableBorders( pdfPCell);
	 	        pdfPCell.setPadding( 4);
	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	        pdfPTable.addCell( pdfPCell);
	 	        document.add( pdfPTable);
	 	        
	 	        /*if ( null != disclosureStatementList && disclosureStatementList.size()>0 && !disclosureStatementList.isEmpty()) {*/
	 	        if ( null != disclosureStatementList && !(disclosureStatementList.isEmpty())) {
	 	            pdfPDeclarationTable = new PdfPTable( 1);
	 	            pdfPDeclarationCell = new PdfPCell(
	 	                    pdfAPI.addParagraph( "Premium calculator - Disclaimer", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
	 	            pdfPDeclarationCell.setBorderWidth( 1);
	 	            /*pdfPDeclarationCell.setBorderColor( pdfAPI.getColor( sectionBackground));*/
	 	            pdfAPI.disableBorders( pdfPDeclarationCell);
	 	            pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
	 	            pdfPDeclarationCell.setPaddingBottom( 6);
	 	            pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
	 	            pdfPDeclarationTable.addCell( pdfPDeclarationCell);
	 	            
	 	            if("insurancecover".equalsIgnoreCase(pdfObject.getCalculatorFlag())){
	 	            	pdfPDeclarationTable = new PdfPTable( 1);
		 	            pdfPDeclarationCell = new PdfPCell(
		 	                    pdfAPI.addParagraph( "Insurance calculator - Disclaimer", new Font( pdfAPI.getSwsMed(), 10,0, pdfAPI.getHeading_color())));
		 	            pdfPDeclarationCell.setBorderWidth( 1);
		 	            /*pdfPDeclarationCell.setBorderColor( pdfAPI.getColor( sectionBackground));*/
		 	            pdfAPI.disableBorders( pdfPDeclarationCell);
		 	            pdfPDeclarationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
		 	            pdfPDeclarationCell.setPaddingBottom( 6);
		 	            pdfPDeclarationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		 	            pdfPDeclarationTable.addCell( pdfPDeclarationCell);
	 	            }
	 	            document.add( pdfPDeclarationTable);
	 	            
	 	            for (int itr = 0; itr < disclosureStatementList.size(); itr++) {
	 	                disclosureStatement = (DisclosureStatement) disclosureStatementList.get( itr);
	 	              /*  pdfPTable = new PdfPTable( 1);
	 	                pdfPCell = new PdfPCell(
	 	                        pdfAPI.addParagraph( MetlifeInstitutionalConstants.BLANK, FontFactory.HELVETICA, 8, Font.NORMAL, colorStyle));
	 	                pdfAPI.disableBorders( pdfPCell);
	 	                pdfPCell.setPadding( 4);
	 	                pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
	 	                pdfPTable.addCell( pdfPCell);
	 	                document.add( pdfPTable);*/
	 	                
	 	                float[] f = { .9f, 0.1f };               
	 	                if (null != disclosureStatement
	 	                        && disclosureStatement.getAnswer().equalsIgnoreCase( "true")) {
	 	                    pdfPTableStatement = new PdfPTable( 1);
	 	                    pdfPCell1 = new PdfPCell(
	 	                            pdfAPI.addParagraph( HTMLToString.convertHTMLToString( disclosureStatement.getStatement()), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	 	                    pdfAPI.disableBorders( pdfPCell1);
	 	                    pdfPCell1.setPadding( 4);
	 	                    pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
	 	                    pdfPTableStatement.addCell( pdfPCell1);
	 	                    document.add( pdfPTableStatement);
	 	                    
	 	                    pdfPTableAck = new PdfPTable( f);
	 	                    
	 	                   if(null!=disclosureStatement.getAckText() && disclosureStatement.getAckText().trim().length()>0){
	 	                	   pdfPCelAck = new PdfPCell(
	 	                               pdfAPI.addParagraph(disclosureStatement.getAckText(), new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	 	                   }else if(null!=disclosureStatement.getStatement() && (disclosureStatement.getStatement().contains("I/") || disclosureStatement.getStatement().contains("I /"))){
	 	                    	pdfPCelAck = new PdfPCell(
	 	                                pdfAPI.addParagraph( "I/We acknowledge and consent to the above", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	 	                        
	 	                    }else{
	 	                    	pdfPCelAck = new PdfPCell(
	 	                                pdfAPI.addParagraph( "I have read the disclaimer and assumptions", new Font( pdfAPI.getSwsLig(), 8,0,  pdfAPI.getFont_color())));
	 	                        
	 	                    }                  
	 	                    
	 	                    
	 	                    
	 	                    pdfAPI.disableBorders( pdfPCelAck);
	 	                    pdfPCelAck.setPadding( 4);
	 	                    pdfPCelAck.setVerticalAlignment( Element.ALIGN_LEFT);
	 	                    pdfPTableAck.addCell( pdfPCelAck);
	 	                    pdfPCell1 = new PdfPCell(
	 	                            pdfAPI.addParagraph( "Yes",  new Font( pdfAPI.getSwsMed(), 8,0,  pdfAPI.getFont_color())));
	 	                    pdfAPI.disableBorders( pdfPCell1);
	 	                    pdfPCell1.setPadding( 4);
	 	                    pdfPCell1.setVerticalAlignment( Element.ALIGN_RIGHT);
	 	                    pdfPTableAck.addCell( pdfPCell1);
	 	                    document.add( pdfPTableAck);
	 	                }
	 	                
	 	            }
	 	        }

	 	        log.info("Declaration details finish");
	 	        
	 	    }
	 	 public static void displayInsuranceCoverSummary(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws MalformedURLException, IOException, DocumentException{
			   log.info("Display cover summary start");
			   Image gif;
			   PdfPTable pdfPCoverTable = null;
			   PdfPCell pdfPCoverCell = null;
			   PdfPTable pdfPEmptyTable = null;
			   PdfPCell pdfPEmptyCell3 = null;
			   PdfPTable pdfPSummaryDtsTable = null;
			   PdfPTable pdfPSummaryDtsInnerTable = null;
			   PdfPCell pdfcoverCostCell1 = null;
			   PdfPCell pdfcoverCostCell2 = null;
			   MemberProductDetails memberProductDetails = null;
			   /*Boolean corpFund = Boolean.FALSE;*/		  
			   
			   /*costColRq = ConfigurationHelper.getConfigurationValue("CORPINST", pdfObject.getProductName());		   
			   String hdCorpFunds = ConfigurationHelper.getConfigurationValue("HD_CORP_FUNDS", "HD_CORP_FUNDS");*/	
			   //Added for header
			   float[] tableSize = { .7f, .3f };
			    PdfPTable pdfPHeaderTable = new PdfPTable( tableSize);
			    PdfPCell pdfPHeadCell = new PdfPCell(
			            pdfAPI.addParagraph("", new Font(pdfAPI.getSwsMed(),18,0,pdfAPI.getHeader_color())));
			    pdfAPI.disableBorders( pdfPHeadCell);
			    pdfPHeadCell.setPadding( 2);
			    pdfPHeadCell.setVerticalAlignment( Element.ALIGN_LEFT);
			    pdfPHeadCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			    pdfPHeaderTable.addCell( pdfPHeadCell);
			     pdfPHeadCell = new PdfPCell();
			    if (null != pdfObject.getLogPath()) {
			    	 gif = Image.getInstance(pdfObject.getLogPath());
			    	  gif.setAlignment( Image.ALIGN_RIGHT);
			    	  if(MetlifeInstitutionalConstants.FUND_MTAA.equalsIgnoreCase(pdfObject.getFundId())){
			    		  pdfPHeadCell.setFixedHeight(35f);
			    	  }else {
			    		  pdfPHeadCell.setFixedHeight(53f);
			    	  }
			    	  pdfPHeadCell.setImage( gif);
			    	  if(MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(pdfObject.getFundId())){
			    		  pdfPHeadCell.setPaddingLeft(100f);
			    	  }
			    	 
			    }	
			    
			    pdfPHeadCell.setHorizontalAlignment( Element.ALIGN_LEFT);
			    pdfPHeadCell.setVerticalAlignment( Element.ALIGN_LEFT);
			    
			   /* pdfPCell.setPaddingRight( 4);
		        pdfPCell.setPadding( 4);
		        pdfPCell.setHorizontalAlignment( Element.ALIGN_RIGHT);*/
		        pdfAPI.disableBorders( pdfPHeadCell);
		        pdfPHeaderTable.addCell( pdfPHeadCell);
		        document.add(pdfPHeaderTable);
		        
		        float[] quof1 = { .9f, .1f };		
		        pdfPHeaderTable = new PdfPTable(quof1);
		        pdfPHeadCell = new PdfPCell(); 
		        if(pdfObject.isQuickQuoteRender()){
		        	pdfPHeadCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_PRIN, new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
		        }else{
		        	pdfPHeadCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
		        }
				/*pdfPCell = new PdfPCell(pdfAPI.addParagraph(MetlifeInstitutionalConstants.DATE_OF_APPL, pdfObject.getColorStyle(),7));*/
				pdfAPI.disableBorders(pdfPHeadCell);
				pdfPHeadCell.setPadding(4);
				/*pdfPCell.setBackgroundColor(pdfAPI.getBackground_color());*/
				pdfPHeadCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
				pdfPHeadCell.setVerticalAlignment(Element.ALIGN_RIGHT);		
				pdfPHeaderTable.addCell(pdfPHeadCell);	
				pdfPHeadCell = new PdfPCell(pdfAPI.addParagraph(CommonPDFHelper.getTodaysDate(),new Font(pdfAPI.getSwsLig(),8,0,pdfAPI.getFont_color())));
				pdfAPI.disableBorders(pdfPHeadCell);
				pdfPHeadCell.setPadding(4);
				  pdfPHeadCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
				    pdfPHeadCell.setVerticalAlignment( Element.ALIGN_RIGHT);
				/*pdfPCell.setBackgroundColor(pdfAPI.getBackground_color());*/
				pdfPHeaderTable.addCell(pdfPHeadCell);
				document.add(pdfPHeaderTable);
				//Header section end
				   pdfPEmptyTable = new PdfPTable( 1);
			        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
			        pdfAPI.disableBorders( pdfPEmptyCell3);
			        pdfPEmptyTable.addCell( pdfPEmptyCell3);
			        document.add( pdfPEmptyTable);	
			   if("UW".equalsIgnoreCase(pdfType)){
				   float[] coverSumm = { .8f, .2f };
			       pdfPCoverTable = new PdfPTable( coverSumm);
			   }else{
				   pdfPCoverTable = new PdfPTable(1);
			   }  
		        
		        pdfPCoverCell = new PdfPCell(
		                pdfAPI.addParagraph( "Insurance Calculator", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		        pdfPCoverCell.setBorderWidth( 1);
		        /*pdfPCoverCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
		        pdfAPI.disableBorders( pdfPCoverCell);
		        pdfPCoverCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
		        pdfPCoverCell.setPaddingBottom( 6);
		        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPCoverTable.addCell( pdfPCoverCell);
		        /*if("UW".equalsIgnoreCase(pdfType)){
		        	pdfPCoverCell = new PdfPCell(
			                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS,  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
			        pdfPCoverCell.setBorderWidth( 1);
			        pdfPCoverCell.setBorderColor(pdfAPI.getBorder_color());
			        pdfAPI.disableBorders( pdfPCoverCell);
			        pdfPCoverCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
			        pdfPCoverCell.setPaddingBottom( 6);
			        pdfPCoverCell.setVerticalAlignment( Element.ALIGN_CENTER);
			        pdfPCoverTable.addCell( pdfPCoverCell);
		        }*/	        
		        document.add( pdfPCoverTable);
		        
		        pdfPEmptyTable = new PdfPTable( 1);
		        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);	     	        	       
		      
		        /*if("UW".equalsIgnoreCase(pdfType)){

		        	if(("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
		        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
		        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && null!=pdfObject.getRequestType() 

		        	

		        			 && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		 float[] f5 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
			        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        	}else if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		 float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.20f,0.2f };
		        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        	}else{
		        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
			        		float[] f5 = {0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.09f,0.11f};
			        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
			        		}
			        		else {
			        			 float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
					        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
			        		}
		        	}	
		        	float[] f5 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        }*///else{
		        	 /*float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
		        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());*/

		        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
		        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
		        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId()) 
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && */null!=pdfObject.getRequestType() 

		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		 float[] f5 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
			        	 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        	}else if(null!=pdfObject.getRequestType() && !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.2f };
		        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
		        	}else{
		        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
			        		float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f,0.1f};
			        		 displayColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
			        		}
			        		else {
			        			 float[] f5 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
					        	 displayInsuranceColumnName( pdfAPI, document, f5, pdfObject, pdfType,pdfObject.getProductName());
			        		}
		        		
		        	}	
		        //}
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	/* float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);*/
		        	

		        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId())
		        			||"CARE".equalsIgnoreCase(pdfObject.getFundId())
		        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && */null!=pdfObject.getRequestType() 

		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		 float[] f6 = { 0.12f, 0.10f, 0.11f, 0.10f, 0.08f, 0.06f, 0.13f,0.1f, 0.2f };
			        	 pdfPSummaryDtsTable = new PdfPTable( f6);
		        	}else if(null!=pdfObject.getRequestType() 
		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.20f, 0.2f };
			        	 pdfPSummaryDtsTable = new PdfPTable( f6);
		        	}else{
		        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
			        		float[] f6 = {  0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.09f,0.11f};
				        	pdfPSummaryDtsTable = new PdfPTable( f6);
			        		}
		        		else {
		        		 float[] f6 = { 0.15f, 0.12f, 0.15f, 0.09f, 0.09f, 0.11f, 0.09f, 0.2f };
			        	 pdfPSummaryDtsTable = new PdfPTable( f6);
		        		}
		        	}
		        }else{
		        	 /*float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
		        	 pdfPSummaryDtsTable = new PdfPTable( f6);*/

		        	if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
		        			|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
		        			|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId()))&& */null!=pdfObject.getRequestType() 

		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		 float[] f6 = { 0.15f, 0.14f, 0.12f, 0.15f, 0.11f, 0.10f, 0.13f, 0.1f };
			        	 pdfPSummaryDtsTable = new PdfPTable( f6);
		        	}else if(null!=pdfObject.getRequestType() 
		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		        		float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.2f };
			        	pdfPSummaryDtsTable = new PdfPTable( f6);
		        	}else{
		        		if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
		        		float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f,0.1f};
			        	pdfPSummaryDtsTable = new PdfPTable( f6);
		        		}
		        		else {
		        			float[] f6 = { 0.12f, 0.11f, 0.15f, 0.1f, 0.1f, 0.1f, 0.1f };
				        	pdfPSummaryDtsTable = new PdfPTable( f6);
				        	float[] innerf6 = {0.34f,0.34f};
				        	pdfPSummaryDtsInnerTable = new PdfPTable(innerf6);
				        	//pdfPSummaryDtsInnerTable.setWidthPercentage(100);
		        		}
		        	}
		        }
		        for (int itr = 0; itr < pdfObject.getMemberDetails().getMemberProductList().size(); itr++) {
		        	memberProductDetails = pdfObject.getMemberDetails().getMemberProductList().get(itr);
		        	if("CORP".equalsIgnoreCase(pdfObject.getFundId())) {
		        		displayCorporateColumnValue( pdfAPI, document,pdfObject,memberProductDetails, pdfType,pdfPSummaryDtsTable,pdfObject.getProductName());	 
		        	}
		        	else {
		        		displayColumnValue( pdfAPI, document,pdfObject,memberProductDetails, pdfType,pdfPSummaryDtsTable,pdfObject.getProductName());	 
		        	}
		        	
		        }
		        PdfPCell cell;
		        pdfcoverCostCell1 = new PdfPCell(
		                pdfAPI.addParagraph(/*pdfObject.getMemberDetails().getFrquencyOpted()*/
		                        " Additional amount for ongoing nursing/medical costs "+pdfObject.getIncludeNursingCosts(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
		        	
			        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_LEFT);
			        pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());
			        //pdfcoverCostCell1.setColspan(4);
			        pdfPSummaryDtsInnerTable.addCell( pdfcoverCostCell1);
			        
			        pdfcoverCostCell1 = new PdfPCell(
			                pdfAPI.addParagraph(/*pdfObject.getMemberDetails().getFrquencyOpted()*/
			                        " Show TPD cover without Salary Continuance deducted "+pdfObject.getExcludeIP(),new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			        pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_LEFT);
			        pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());
			        //pdfcoverCostCell1.setColspan(3);
			        pdfPSummaryDtsInnerTable.addCell( pdfcoverCostCell1);
			        cell = new PdfPCell(pdfPSummaryDtsInnerTable);
			        cell.setColspan(7);
			        pdfAPI.disableBorders(cell);
			       pdfPSummaryDtsTable.addCell(cell);
		        	if(!"CORP".equalsIgnoreCase(pdfObject.getFundId())) {
		        		pdfcoverCostCell2 = new PdfPCell(
		                pdfAPI.addParagraph(/*pdfObject.getMemberDetails().getFrquencyOpted()*/
		                        " The total estimated weekly premium is: ",new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));	
		        	
		        		pdfcoverCostCell2.setHorizontalAlignment( Element.ALIGN_RIGHT);
		        		pdfcoverCostCell2.setBorderColor( pdfAPI.getBorder_color());

			        if(/*("HOST".equalsIgnoreCase(pdfObject.getFundId()) 
			        		|| "CARE".equalsIgnoreCase(pdfObject.getFundId())
			        		|| "PSUP".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "NSFS".equalsIgnoreCase(pdfObject.getFundId())
	    					|| "MTAA".equalsIgnoreCase(pdfObject.getFundId())) && */null!=pdfObject.getRequestType() 

		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
			        	pdfcoverCostCell2.setColspan(7);
			        }/*else if(null!=pdfObject.getRequestType() 
		        			&& !"".equalsIgnoreCase(pdfObject.getRequestType()) && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
			        	pdfcoverCostCell1.setColspan(5);
			        }*/else{
			        	pdfcoverCostCell2.setColspan(6);
			        }
			        if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
			        	 pdfPSummaryDtsTable.addCell( pdfcoverCostCell2);
			        	 
			        	 pdfcoverCostCell2 = new PdfPCell(
				 	                pdfAPI.addParagraph( pdfObject.getMemberDetails().getTotalPremium(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			        	 pdfcoverCostCell2.setBorderColor( pdfAPI.getBorder_color());
			        	 pdfcoverCostCell2.setHorizontalAlignment( Element.ALIGN_CENTER);
				 	        pdfPSummaryDtsTable.addCell( pdfcoverCostCell2);
				 	      
				 	       if("UW".equalsIgnoreCase(pdfType)){
				 	    	  pdfcoverCostCell2 = new PdfPCell(
						                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())), false));
						        pdfAPI.disableBorders( pdfcoverCostCell2);
						        pdfPSummaryDtsTable.addCell( pdfcoverCostCell2);
					        }
			        }
			       
		        	}
		        	
		      
		       
		        
		        if("DCL".equalsIgnoreCase(pdfObject.getApplicationDecision())){
		        	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/      
		        	 /*pdfcoverCostCell1 = new PdfPCell(
		                     pdfAPI.addParagraph( "Additional cover alteration:", new Font(pdfAPI.getSwsMed() , 7,0, pdfAPI.getFont_color())));

		        	 if (null != pdfObject.getFundId() && !"".equalsIgnoreCase(pdfObject.getFundId())
								&& "CARE".equalsIgnoreCase(pdfObject.getFundId()) && null != pdfObject.getRequestType()

								&& !"".equalsIgnoreCase(pdfObject.getRequestType())
								&& MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())) {
		        		 	pdfcoverCostCell1.setColspan(8);
			        	 }else{
			        		 pdfcoverCostCell1.setColspan(7);
			        	 }
		        	
		        	 pdfcoverCostCell1.disableBorderSide(pdfcoverCostCell1.BOTTOM);
		        	 pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());
		        	 pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
		        	 if("UW".equalsIgnoreCase(pdfType)){
				        	pdfcoverCostCell1 = new PdfPCell(
					                pdfAPI.addSingleCell( pdfAPI.addParagraph( "",new Font(pdfAPI.getSwsMed() , 6,0, pdfAPI.getFont_color())), false));
					        pdfAPI.disableBorders( pdfcoverCostCell1);
					        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
				        }
		            
		        	pdfcoverCostCell1 = new PdfPCell(
			                pdfAPI.addParagraph(MetlifeInstitutionalConstants.DCL_TEXT_OVERALL+pdfObject.getDeclReasons(), new Font(pdfAPI.getSwsMed() , 6,0, pdfAPI.getFont_color())));*/
			        /* pdfcoverCostCell1.setHorizontalAlignment( Element.ALIGN_RIGHT);*/
			        /*pdfcoverCostCell1.setBorderColor( pdfAPI.getBorder_color());

			        if (null != pdfObject.getFundId() && !"".equalsIgnoreCase(pdfObject.getFundId())
							&& "CARE".equalsIgnoreCase(pdfObject.getFundId()) && null != pdfObject.getRequestType()

							&& !"".equalsIgnoreCase(pdfObject.getRequestType())
							&& MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType())) {
			        	 pdfcoverCostCell1.setColspan(8);
			         }else{
			        		 pdfcoverCostCell1.setColspan(7);
			         }
			        pdfcoverCostCell1.disableBorderSide(pdfcoverCostCell1.TOP);
			        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
			        if("UW".equalsIgnoreCase(pdfType)){
			        	pdfcoverCostCell1 = new PdfPCell(
				                pdfAPI.addSingleCell( pdfAPI.addParagraph( "", new Font(pdfAPI.getSwsMed() , 6,0, pdfAPI.getFont_color())), false));
				        pdfAPI.disableBorders( pdfcoverCostCell1);
				        pdfPSummaryDtsTable.addCell( pdfcoverCostCell1);
			        }*/
		        	/*CSO-457 - Commented for fix as Remove "additional cover alteration' on pdf - 169682*/      
		        }
		        document.add( pdfPSummaryDtsTable);
		        
		        
		        /* added for Indexation Flag */
		        //Refined the logic as part of sonarqube fix - vicsuper by purna
		        if (("CARE".equalsIgnoreCase(pdfObject.getFundId()) || (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId())
		        		&& !(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType()))))) {
		        	if (!pdfObject.isQuickQuoteRender() && (MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType()) || "CCOVER".equalsIgnoreCase(pdfObject.getRequestType()) || "NCOVER".equalsIgnoreCase(pdfObject.getRequestType())
		        			|| MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(pdfObject.getRequestType()))){
			       		String label = null;
			        	if(!(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(pdfObject.getRequestType())))
			        	{
			       		 if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) && !("NA".equalsIgnoreCase(pdfObject.getMemberDetails().getIndexationFlag()))){
			        		label ="Indexation applied at lower of CPI and 7.5% per year";
			        		PdfPTable pdfPIndexTable = null;
				        	float[] f = { .9f, 0.1f }; 
				        	pdfPIndexTable = new PdfPTable(f);
				 	        PdfPCell pdfPCell;
				 	        pdfPCell= new PdfPCell(
				        				 pdfAPI.addParagraph(label,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
				 	        pdfAPI.disableBorders( pdfPCell);
				 	        pdfPCell.setPadding(4);
				 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
				 	        pdfPIndexTable.addCell( pdfPCell);
				 	        PdfPCell pdfPCellAns = new PdfPCell(
				                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getIndexationFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
				 	        pdfAPI.disableBorders( pdfPCellAns);
				 	        pdfPCellAns.setPadding(4);
				 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
				 	        pdfPIndexTable.addCell( pdfPCellAns);
				 	       document.add(pdfPIndexTable);
			        	}
			       		else if ("CARE".equalsIgnoreCase(pdfObject.getFundId())){
			        		  label ="Would you like your cover to be indexed by 5% on 1 July?";
			        	
			        	PdfPTable pdfPIndexTable = null;
			        	float[] f = { .9f, 0.1f }; 
			        	pdfPIndexTable = new PdfPTable(f);
			 	        PdfPCell pdfPCell;
			 	        pdfPCell= new PdfPCell(
			        				 pdfAPI.addParagraph(label,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	        pdfAPI.disableBorders( pdfPCell);
			 	        pdfPCell.setPadding(4);
			 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
			 	        pdfPIndexTable.addCell( pdfPCell);
			 	        PdfPCell pdfPCellAns = new PdfPCell(
			                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getIndexationFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	        pdfAPI.disableBorders( pdfPCellAns);
			 	        pdfPCellAns.setPadding(4);
			 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
			 	        pdfPIndexTable.addCell( pdfPCellAns);
			 	       document.add(pdfPIndexTable);
			       		}
			        	}
			 	       if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) && !(pdfObject.isConvertCheckPdf()) && "Yes".equalsIgnoreCase(pdfObject.getMemberDetails().getIpDisclaimer())){
			 	    	  label ="I am not a casual or contractor employee";
			 	    	  PdfPTable pdfPIndexTableIp = null;
			 	    	  float[] f1 = { .9f, 0.1f }; 
				        	pdfPIndexTableIp = new PdfPTable(f1);
				 	        PdfPCell pdfPCell1;
				 	        pdfPCell1= new PdfPCell(
				        				 pdfAPI.addParagraph(label,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
				 	        pdfAPI.disableBorders( pdfPCell1);
				 	       pdfPCell1.setPadding(4);
				 	      pdfPCell1.setVerticalAlignment( Element.ALIGN_LEFT);
				 	     pdfPIndexTableIp.addCell( pdfPCell1);
				 	        PdfPCell pdfPCellAns1 = new PdfPCell(
				                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getIpDisclaimer(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
				 	        pdfAPI.disableBorders( pdfPCellAns1);
				 	       pdfPCellAns1.setPadding(4);
				 	       pdfPCellAns1.setVerticalAlignment( Element.ALIGN_RIGHT);
				 	       pdfPIndexTableIp.addCell( pdfPCellAns1);
				 	       document.add(pdfPIndexTableIp);
			 	       }
			 	       
			        }	      
		       }
			     
		        /* added for Indexation Flag */
		        
			        
			 /* added for Unitised Covers Flag */
		 	        if( null != pdfObject.getRequestType()  && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(pdfObject.getRequestType()) && !("CARE".equalsIgnoreCase(pdfObject.getFundId()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()) || "HOST".equalsIgnoreCase(pdfObject.getFundId())) ){
		 	        	PdfPTable pdfPUnitisedCvrsTable = null;
		 	        	float[] f = { .9f, 0.1f }; 
		 	        	pdfPUnitisedCvrsTable = new PdfPTable(f);
		 	 	        PdfPCell pdfPCell = null;
		 	 	        if((null!=pdfObject.getFundId() && "CARE".equalsIgnoreCase(pdfObject.getFundId()))){
		 	 	        	 pdfPCell= new PdfPCell(
		 	        				 pdfAPI.addParagraph("Equivalent units of CareSuper scale ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }else if(null!=pdfObject.getFundId() && ("HOST".equalsIgnoreCase(pdfObject.getFundId()) || MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(pdfObject.getFundId()))/**vicsuper changes Made by Purna**/){
		 	 	        	pdfPCell= new PdfPCell(
		 	        				 pdfAPI.addParagraph("Equivalent units of HostPlus scale ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }else if((null!=pdfObject.getFundId() && "FIRS".equalsIgnoreCase(pdfObject.getFundId()))){
		 	 	        	pdfPCell= new PdfPCell(
		 	        				 pdfAPI.addParagraph("Equivalent value in unitised scale? ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }else if((null!=pdfObject.getFundId() && "SFPS".equalsIgnoreCase(pdfObject.getFundId()))){
		 	 	        	pdfPCell= new PdfPCell(
		 	        				 pdfAPI.addParagraph("Equivalent value in unitised scale? ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }
		 	 	        /*MTAA - Code - Check with Chordiant Code */
		 	 	        else if((null!=pdfObject.getFundId() && "MTAA".equalsIgnoreCase(pdfObject.getFundId()))){
		 	 	        	pdfPCell= new PdfPCell(
		 	        				 pdfAPI.addParagraph("Equivalent units of MTAASuper scale ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        }
		 	 	       /*MTAA code - check with Chordiant Code*/
		 	 	        
		 	 	        pdfAPI.disableBorders( pdfPCell);
		 	 	        if(null!= pdfPCell){
		 	 	        pdfPCell.setPadding(4);
		 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	 	        }
		 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCell);
		 	 	        PdfPCell pdfPCellAns = new PdfPCell(
		 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getUnitisedCvrsFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        pdfAPI.disableBorders( pdfPCellAns);
		 	 	        pdfPCellAns.setPadding(4);
		 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
		 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCellAns);
		 	 	        document.add(pdfPUnitisedCvrsTable);
		 	        }
		 	        /*added for rounding message*/
		 	        if(pdfObject.isDeathRoundMsg() || pdfObject.isTpdRoundMsg())
		 	        {
		 	        	PdfPTable pdfPUnitisedCvrsTable = null;
		 	        	float[] f = { .9f, 0.1f }; 
		 	        	pdfPUnitisedCvrsTable = new PdfPTable(f);
		 	 	        PdfPCell pdfPCell = null;
		 	 	        String msg = "The total cover amount for <%Covers%> has been rounded up to the nearest number of units.";
		 	 	        String covers = pdfObject.isDeathRoundMsg() && pdfObject.isTpdRoundMsg()?"Death and TPD": (pdfObject.isDeathRoundMsg() && !pdfObject.isTpdRoundMsg())?DEATH:"TPD"; 
		 	 	        		
		 	 	        		
		 	 	        msg = msg.replace("<%Covers%>", covers);
		 	 	        
		 	 	        pdfPCell= new PdfPCell(
		 	        				 pdfAPI.addParagraph(msg,new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        
		 	 	        pdfAPI.disableBorders( pdfPCell);
		 	 	        pdfPCell.setPadding(4);
		 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCell);
		 	 	        PdfPCell pdfPCellAns = new PdfPCell(
		 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getUnitisedCvrsFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        pdfAPI.disableBorders( pdfPCellAns);
		 	 	        pdfPCellAns.setPadding(4);
		 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
		 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCellAns);
		 	 	        document.add(pdfPUnitisedCvrsTable);
		 	        	
		 	        }
		 	        
		 	       if(pdfObject.isIpRoundMsg())
	 	        	{
		 	    	  PdfPTable pdfPUnitisedCvrsTable = null;
		 	        	float[] f = { .9f, 0.1f }; 
		 	        	pdfPUnitisedCvrsTable = new PdfPTable(f);
		 	 	        PdfPCell pdfPCell = null;
		 	 	        pdfPCell= new PdfPCell(
		        				 pdfAPI.addParagraph("Your IP cover has been rounded up to the next multiple of $425 ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        
		 	 	        pdfAPI.disableBorders( pdfPCell);
		 	 	        pdfPCell.setPadding(4);
		 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCell);
		 	 	        PdfPCell pdfPCellAns = new PdfPCell(
		 	                    pdfAPI.addParagraph( pdfObject.getMemberDetails().getUnitisedCvrsFlag(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        pdfAPI.disableBorders( pdfPCellAns);
		 	 	        pdfPCellAns.setPadding(4);
		 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
		 	 	        pdfPUnitisedCvrsTable.addCell( pdfPCellAns);
		 	 	        document.add(pdfPUnitisedCvrsTable);
	 	        	}
		 	        
		        /* added for Unitised Covers Flag */
	 	       /* added for Cancel cover reason */    
		 	       if( null!=pdfObject.getRequestType()  && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType()) && (null!=pdfObject.getFundId() && !"CARE".equalsIgnoreCase(pdfObject.getFundId())) ){
		 	        	PdfPTable pdfCancelReasonTable = null;
		 	        	float[] f = { .5f, 0.5f }; 
		 	        	pdfCancelReasonTable = new PdfPTable(f);
		 	 	        PdfPCell pdfPCell = null;
		 	 	        PdfPCell pdfPCellAns = null;
		 	 	        pdfPCell= new PdfPCell(pdfAPI.addParagraph("Reason for cancelling your cover ",new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		 	 	        pdfAPI.disableBorders( pdfPCell);
		 	 	        pdfPCell.setPadding(4);
		 	 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	 	        pdfCancelReasonTable.addCell( pdfPCell);
		 	 	        if(pdfObject.getMemberDetails().getCancelreason() != null && pdfObject.getMemberDetails().getCancelreason() != ""){
		 	 	        	if(pdfObject.getMemberDetails().getCancelreason().equalsIgnoreCase("Other") && pdfObject.getMemberDetails().getCancelotherreason() != "" && pdfObject.getMemberDetails().getCancelotherreason() != null){
			 	 	        	pdfPCellAns = new PdfPCell(pdfAPI.addParagraph(pdfObject.getMemberDetails().getCancelotherreason(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	 	        }else{
			 	 	        	pdfPCellAns = new PdfPCell(pdfAPI.addParagraph(pdfObject.getMemberDetails().getCancelreason(),new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
			 	 	        }
		 	 	        }
		 	 	        pdfAPI.disableBorders( pdfPCellAns);
		 	 	        if(null!= pdfPCellAns){
		 	 	        pdfPCellAns.setPadding(4);
		 	 	        pdfPCellAns.setVerticalAlignment( Element.ALIGN_RIGHT);
		 	 	        }
		 	 	        pdfCancelReasonTable.addCell( pdfPCellAns);
		 	 	        document.add(pdfCancelReasonTable);
		 	        }
		 	      /* added for Cancel cover reason */    
		        
		        
		        
		          
				    log.info("Display personal details finish");
		}
	 	private static void displayInsuranceColumnName(CostumPdfAPI pdfAPI, Document document,float[] f5, PDFObject pdfObject,String pdfType, String productName)
	            throws DocumentException {
			log.info("Display column name start");
			 PdfPTable pdfPSummaryTable = new PdfPTable( f5);
			 float[] coverCostf5 = { 0.78f };
			 PdfPTable pdfPSummaryTopTable = new PdfPTable( coverCostf5);
			 PdfPCell pdfPCoverCostCell = new PdfPCell(
		                pdfAPI.addParagraph( "Covers & Costs", new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
			 pdfPCoverCostCell.setBorderWidth( 1);
			 //pdfPSummaryCell3.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));
		        pdfAPI.disableBorders( pdfPCoverCostCell);
		        pdfPCoverCostCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
		        pdfPCoverCostCell.setPaddingBottom( 6);
		        pdfPCoverCostCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryTopTable.addCell( pdfPCoverCostCell);
		        document.add( pdfPSummaryTopTable);
		        PdfPCell pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Cover Type",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);
			 
			 
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Calculated cover", new Font(pdfAPI.getSwsMed(),8,0, pdfAPI.getHeading_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        
       	        
	         
	        	 pdfPSummaryCell3 = new PdfPCell(
	                     pdfAPI.addParagraph( "Cover required", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	             pdfAPI.disableBorders( pdfPSummaryCell3);
	             pdfPSummaryCell3.setPadding( 4);
	             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	             pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        
	             
	        
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Waiting period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        
	        pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Benefit period", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor(pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);
	        /*169682: 24/04/2015 : Removed Occupation rating column for corporate eapply partners*/
	        pdfPSummaryCell3 = new PdfPCell(
		                pdfAPI.addParagraph( "Occupation rating", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		        pdfAPI.disableBorders( pdfPSummaryCell3);
		        pdfPSummaryCell3.setPadding( 4);
		        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPSummaryCell3.setBackgroundColor(  pdfAPI.getTable_fill_color());
		        pdfPSummaryTable.addCell( pdfPSummaryCell3);  
	     
	        
		    	 if(pdfObject.getMemberDetails().isReplaceCvrOption()){
		         	pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Weekly Estimated Premium " +  pdfObject.getMemberDetails().getFrquencyOpted().toLowerCase() + " cost", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
		         }else if(!MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(pdfObject.getRequestType())){
		         	pdfPSummaryCell3 = new PdfPCell(
		                     pdfAPI.addParagraph( "Estimated total " +  pdfObject.getMemberDetails().getFrquencyOpted().toLowerCase() + " cost", new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
		             pdfAPI.disableBorders( pdfPSummaryCell3);
		             pdfPSummaryCell3.setPadding( 4);
		             pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
		             pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
		             pdfPSummaryTable.addCell( pdfPSummaryCell3);  
		         }
		   
	        
	           
	       /* pdfPSummaryCell3 = new PdfPCell(
	                pdfAPI.addParagraph( "Final decision after post processing",new Font(pdfAPI.getSwsMed(),8,0,pdfAPI.getHeading_color())));
	        pdfAPI.disableBorders( pdfPSummaryCell3);
	        pdfPSummaryCell3.setPadding( 4);
	        pdfPSummaryCell3.setVerticalAlignment( Element.ALIGN_CENTER);
	        pdfPSummaryCell3.setBackgroundColor( pdfAPI.getTable_fill_color());
	        pdfPSummaryTable.addCell( pdfPSummaryCell3);*/
			 
	        document.add( pdfPSummaryTable);

	         log.info("Display column name finish");
	        
	        
	    }
	 	public static void displayFinancialDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
			log.info("Display quotation details start");
			PdfPTable pdfPOccupationTable = null;
			PdfPTable pdfPOccupationTable1 = null;
			PdfPTable pdfPOccupationTable2 = null;
			PdfPTable pdfPOccupationTable3 = null;
			PdfPTable pdfPOccupationTable4 = null;
			PdfPTable pdfPOccupationTable5 = null;
			PdfPTable pdfPOccupationTable6 = null;
			DisclosureQuestionAns disclosureQuestionAns = null;
			PdfPCell pdfCell = null;
			
			   	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
		        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);
		        
		        pdfPEmptyTable = new PdfPTable( 1);
		        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);
		        
		       /* if(!"UW".equalsIgnoreCase(pdfType)){
		        	 pdfPEmptyTable = new PdfPTable( 1);
		 	        PdfPCell pdfPCell;
		        	 if(pdfObject.isQuickQuoteRender()){
		        		 pdfPCell= new PdfPCell(
			 	                pdfAPI.addParagraph( "Your quotation details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
		        	 }else{
		        		 pdfPCell= new PdfPCell(
		        				 pdfAPI.addParagraph( "Your application details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color() )));
		        	 }
		        	 
	                PdfPCell pdfPCell = new PdfPCell(
	                pdfAPI.addParagraph( "Your application details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
		 	        pdfAPI.disableBorders( pdfPCell);
		 	        pdfPCell.setPadding( 4);
		 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	        pdfPEmptyTable.addCell( pdfPCell);
		 	        document.add( pdfPEmptyTable);
		        }
		       */
		        
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 float[] occupationFloat = { 0.8f, 0.2f };
		        	  pdfPOccupationTable = new PdfPTable( occupationFloat);
		        }else{
		        	  pdfPOccupationTable = new PdfPTable( 1);
		        }
		        
		        PdfPCell pdfPOccupationCell = new PdfPCell(
		                pdfAPI.addParagraph( "Financial details",  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		        pdfPOccupationCell.setBorderWidth( 1);
		        /*pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
		        pdfAPI.disableBorders( pdfPOccupationCell);
		        pdfPOccupationCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
		        pdfPOccupationCell.setPaddingBottom( 6);
		        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPOccupationTable.addCell( pdfPOccupationCell);
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 pdfPOccupationCell = new PdfPCell(
		 	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS, new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		 	        pdfPOccupationCell.setBorderWidth( 1);
		 	        /*pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
		 	        pdfAPI.disableBorders( pdfPOccupationCell);
		 	        pdfPOccupationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
		 	        pdfPOccupationCell.setPaddingBottom( 6);
		 	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		 	        pdfPOccupationTable.addCell( pdfPOccupationCell);
		        }
		       
		        document.add( pdfPOccupationTable);
		        pdfPOccupationTable1 = new PdfPTable( 1);
		        PdfPCell pdfPincomeCell;
		        pdfPincomeCell= new PdfPCell(
	 	                pdfAPI.addParagraph( "Income", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
		        pdfAPI.disableBorders( pdfPincomeCell);
		        pdfPincomeCell.setPadding( 4);
		        pdfPincomeCell.setVerticalAlignment( Element.ALIGN_LEFT);
		        pdfPOccupationTable1.addCell( pdfPincomeCell);
	 	        document.add( pdfPOccupationTable1);
	 	        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
		        	 pdfPOccupationTable1 = new PdfPTable( f1);
		        }else{
		        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
		        	  pdfPOccupationTable1 = new PdfPTable( f1);
		        }        
			
			if(null!=pdfObject.getDisclosureincomeQuestionAnsList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureincomeQuestionAnsList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureincomeQuestionAnsList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable1.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable1.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable1.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable1.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable1.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable1.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable1.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable1.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable1);
			//Expenses
			PdfPCell pdfPexpenseCell;
			pdfPOccupationTable2 = new PdfPTable( 1);
			pdfPexpenseCell= new PdfPCell(
 	                pdfAPI.addParagraph( "Expenses", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPexpenseCell);
	        pdfPexpenseCell.setPadding( 4);
	        pdfPexpenseCell.setVerticalAlignment( Element.ALIGN_LEFT);
	        pdfPOccupationTable2.addCell( pdfPexpenseCell);
 	        document.add( pdfPOccupationTable2);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable2 = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	  pdfPOccupationTable2 = new PdfPTable( f1);
	        } 
 	       if(null!=pdfObject.getDisclosureExpenseQuestionAnsList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureExpenseQuestionAnsList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureExpenseQuestionAnsList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable2.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable2.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable2.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable2.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable2.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable2.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable2.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable2.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable2);
			//Other Expenses
			if(pdfObject.isShowExpenseDetails()){
		
			PdfPCell pdfPotherexpenseCell;
			pdfPOccupationTable5 = new PdfPTable( 1);
			pdfPotherexpenseCell= new PdfPCell(
 	                pdfAPI.addParagraph( "Other Expenses", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPotherexpenseCell);
	        pdfPotherexpenseCell.setPadding( 4);
	        pdfPotherexpenseCell.setVerticalAlignment( Element.ALIGN_LEFT);
	        pdfPOccupationTable5.addCell( pdfPotherexpenseCell);
 	        document.add( pdfPOccupationTable5);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable5 = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	  pdfPOccupationTable5 = new PdfPTable( f1);
	        } 
 	       if(null!=pdfObject.getDisclosureOtherExpenseQuestionAnsList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureOtherExpenseQuestionAnsList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureOtherExpenseQuestionAnsList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable5.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable5.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable5.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable5.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable5.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable5.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable5.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable5.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable5);
	 	}
			//Other Financial details
			PdfPCell pdfPotherfinanceCell;
			pdfPOccupationTable3 = new PdfPTable( 1);
			pdfPotherfinanceCell= new PdfPCell(
 	                pdfAPI.addParagraph( "Other Financial Details", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPotherfinanceCell);
	        pdfPotherfinanceCell.setPadding( 4);
	        pdfPotherfinanceCell.setVerticalAlignment( Element.ALIGN_LEFT);
	        pdfPOccupationTable3.addCell( pdfPotherfinanceCell);
 	        document.add( pdfPOccupationTable3);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable3 = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	  pdfPOccupationTable3 = new PdfPTable( f1);
	        }
 	       if(null!=pdfObject.getDisclosureOtherFinanceQuestionAnsList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureOtherFinanceQuestionAnsList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureOtherFinanceQuestionAnsList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable3.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable3.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable3.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable3.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable3.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable3.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable3.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable3.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable3);
			//Debts Details
			if(pdfObject.isShowDebtsDetails()){
			PdfPCell pdfPdebtdetailsCell;
			pdfPOccupationTable6 = new PdfPTable( 1);
			pdfPdebtdetailsCell= new PdfPCell(
 	                pdfAPI.addParagraph( "Debts Details", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPdebtdetailsCell);
	        pdfPdebtdetailsCell.setPadding( 4);
	        pdfPdebtdetailsCell.setVerticalAlignment( Element.ALIGN_LEFT);
	        pdfPOccupationTable6.addCell( pdfPdebtdetailsCell);
 	        document.add( pdfPOccupationTable6);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable6 = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	  pdfPOccupationTable6 = new PdfPTable( f1);
	        } 
 	       if(null!=pdfObject.getDisclosureDebtDetailsQuestionAnsList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureDebtDetailsQuestionAnsList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureDebtDetailsQuestionAnsList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable6.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable6.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable6.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable6.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable6.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable6.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable6.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable6.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable6);
			}
			//Assumptions
			PdfPCell pdfPassumptionsCell;
			pdfPOccupationTable4 = new PdfPTable( 1);
			pdfPassumptionsCell= new PdfPCell(
 	                pdfAPI.addParagraph( "Assumptions", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
	        pdfAPI.disableBorders( pdfPassumptionsCell);
	        pdfPassumptionsCell.setPadding( 4);
	        pdfPassumptionsCell.setVerticalAlignment( Element.ALIGN_LEFT);
	        pdfPOccupationTable4.addCell( pdfPassumptionsCell);
 	        document.add( pdfPOccupationTable4);
	        if("UW".equalsIgnoreCase(pdfType)){
	        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
	        	 pdfPOccupationTable4 = new PdfPTable( f1);
	        }else{
	        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
	        	  pdfPOccupationTable4 = new PdfPTable( f1);
	        }
 	       if(null!=pdfObject.getDisclosureAssumptionQuestionAnsList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureAssumptionQuestionAnsList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureAssumptionQuestionAnsList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable4.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable4.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable4.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable4.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable4.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable4.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable4.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable4.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable4);
		    log.info("Display quotation details finish");
		}
	 	public static void displayInsurancePersonalDetails(CostumPdfAPI pdfAPI, Document document,PDFObject pdfObject,String pdfType) throws DocumentException{
			log.info("Display quotation details start");
			PdfPTable pdfPOccupationTable = null;
			PdfPTable pdfPchildTable = null;
			DisclosureQuestionAns disclosureQuestionAns = null;
			PdfPCell pdfCell = null;
			
			   	PdfPTable pdfPEmptyTable = new PdfPTable( 1);
		        PdfPCell pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);
		        
		        pdfPEmptyTable = new PdfPTable( 1);
		        pdfPEmptyCell3 = new PdfPCell( pdfAPI.addSingleCell());
		        pdfAPI.disableBorders( pdfPEmptyCell3);
		        pdfPEmptyTable.addCell( pdfPEmptyCell3);
		        document.add( pdfPEmptyTable);
		        
		       /* if(!"UW".equalsIgnoreCase(pdfType)){
		        	 pdfPEmptyTable = new PdfPTable( 1);
		 	        PdfPCell pdfPCell;
		        	 if(pdfObject.isQuickQuoteRender()){
		        		 pdfPCell= new PdfPCell(
			 	                pdfAPI.addParagraph( "Your quotation details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color())));
		        	 }else{
		        		 pdfPCell= new PdfPCell(
		        				 pdfAPI.addParagraph( "Your application details and responses.", new Font(pdfAPI.getSwsMed() , 10,0,pdfAPI.getFont_color() )));
		        	 }
		        	 
	                PdfPCell pdfPCell = new PdfPCell(
	                pdfAPI.addParagraph( "Your application details and responses.", FontFactory.HELVETICA, 8, Font.BOLD, pdfObject.getColorStyle()));
		 	        pdfAPI.disableBorders( pdfPCell);
		 	        pdfPCell.setPadding( 4);
		 	        pdfPCell.setVerticalAlignment( Element.ALIGN_LEFT);
		 	        pdfPEmptyTable.addCell( pdfPCell);
		 	        document.add( pdfPEmptyTable);
		        }*/
		       
		        
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 float[] occupationFloat = { 0.8f, 0.2f };
		        	  pdfPOccupationTable = new PdfPTable( occupationFloat);
		        }else{
		        	  pdfPOccupationTable = new PdfPTable( 1);
		        }
		        
		        PdfPCell pdfPOccupationCell = new PdfPCell(
		                pdfAPI.addParagraph( "Personal details",  new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		        pdfPOccupationCell.setBorderWidth( 1);
		        /*pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
		        pdfAPI.disableBorders( pdfPOccupationCell);
		        pdfPOccupationCell.setBackgroundColor( pdfAPI.getHeading_color_fill());
		        pdfPOccupationCell.setPaddingBottom( 6);
		        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		        pdfPOccupationTable.addCell( pdfPOccupationCell);
		        
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 pdfPOccupationCell = new PdfPCell(
		 	                pdfAPI.addParagraph( MetlifeInstitutionalConstants.UNDERWRITING_DETAILS, new Font(pdfAPI.getSwsMed(),10,0,pdfAPI.getHeading_color())));
		 	        pdfPOccupationCell.setBorderWidth( 1);
		 	        /*pdfPOccupationCell.setBorderColor( pdfAPI.getColor( pdfObject.getSectionBackground()));*/
		 	        pdfAPI.disableBorders( pdfPOccupationCell);
		 	        pdfPOccupationCell.setBackgroundColor(pdfAPI.getHeading_color_fill());
		 	        pdfPOccupationCell.setPaddingBottom( 6);
		 	        pdfPOccupationCell.setVerticalAlignment( Element.ALIGN_CENTER);
		 	        pdfPOccupationTable.addCell( pdfPOccupationCell);
		        }
		       
		        document.add( pdfPOccupationTable);
		       
		        if("UW".equalsIgnoreCase(pdfType)){
		        	 float[] f1 = { 0.05f, 0.5f, 0.25f, .2f };
		        	 pdfPOccupationTable = new PdfPTable( f1);
		        }else{
		        	  float[] f1 = { 0.05f, 0.7f, 0.25f };
		        	 pdfPOccupationTable = new PdfPTable( f1);
		        }        
			
			if(null!=pdfObject.getDisclosureQuestionList()){
				for(int itr =0 ;itr<pdfObject.getDisclosureQuestionList().size();itr++){
					disclosureQuestionAns = pdfObject.getDisclosureQuestionList().get(itr);
					
					pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( Integer.toString( itr+1), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		            if(("REIS".equalsIgnoreCase(pdfObject.getFundId())|| "VICS1".equalsIgnoreCase(pdfObject.getFundId())) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!("REIS".equalsIgnoreCase(pdfObject.getFundId()) || "VICS1".equalsIgnoreCase(pdfObject.getFundId()))){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getQuestiontext(), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
		            pdfAPI.disableBorders( pdfCell);
		           /* pdfPOccupationTable.addCell( pdfCell);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
		            pdfCell = new PdfPCell(
		                    pdfAPI.addParagraph( disclosureQuestionAns.getAnswerText(), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
		            pdfCell.setNoWrap(Boolean.FALSE);
		            
		            
		            
		            if("UW".equalsIgnoreCase(pdfType)){
		            	 pdfCell.setBorderColor( pdfAPI.getBorder_color());
		 	            pdfCell.disableBorderSide( 1);
		 	            pdfCell.disableBorderSide( 3);
		 	            pdfCell.disableBorderSide( 4);
		            }else{
		            	pdfAPI.disableBorders( pdfCell);
		            }      
		            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
		            /*pdfCell.setNoWrap( Boolean.TRUE);*/
		            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
		            	pdfPOccupationTable.addCell( pdfCell);
		            }
		            if("Do you have any dependent children?".equalsIgnoreCase(disclosureQuestionAns.getQuestiontext()) && 
		            		"Yes".equalsIgnoreCase(disclosureQuestionAns.getAnswerText())){
		            	 //float[] f1 = { 0.05f, 0.7f, 0.25f };
			        	// pdfPchildTable = new PdfPTable( 1);
		            	for(int itr1 =0 ;itr1<pdfObject.getDisclosureChildListQuestionAnsList().size();itr1++){
							/*disclosureQuestionAns = pdfObject.getDisclosureChildListQuestionAnsList().get(itr1);*/
									pdfCell = new PdfPCell(
						                    pdfAPI.addParagraph( " ", new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
						            pdfAPI.disableBorders( pdfCell);
						            pdfPOccupationTable.addCell( pdfCell);
						            pdfCell = new PdfPCell(
						                    pdfAPI.addParagraph( String.valueOf(pdfObject.getDisclosureChildListQuestionAnsList().get(itr1).getQuestiontext()), new Font(pdfAPI.getSwsLig() , 8,0, pdfAPI.getFont_color())));
						            pdfAPI.disableBorders( pdfCell);
						            pdfPOccupationTable.addCell( pdfCell);
						            pdfCell = new PdfPCell(
						                    pdfAPI.addParagraph( String.valueOf(pdfObject.getDisclosureChildListQuestionAnsList().get(itr1).getAnswerText()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
						            pdfCell.setNoWrap(Boolean.FALSE);
						            pdfAPI.disableBorders( pdfCell);
						            pdfCell.setHorizontalAlignment( Element.ALIGN_RIGHT);
						            pdfPOccupationTable.addCell( pdfCell);
						            pdfCell.setNoWrap(Boolean.FALSE);
						            //pdfCell = new PdfPCell(pdfPchildTable);
						           // pdfPOccupationTable.addCell(pdfPchildTable);
		            	}
		            	//document.add(pdfPchildTable);
		            	
		            }
		            /*pdfPOccupationTable.addCell( pdfCell);*/
		            if("UW".equalsIgnoreCase(pdfType)){
		            	pdfCell = new PdfPCell(
			                    pdfAPI.addParagraph( getOccupationDecision(pdfObject.getMemberDetails().getResponseObject()), new Font(pdfAPI.getSwsMed() , 8,0, pdfAPI.getFont_color())));
			            pdfAPI.disableBorders( pdfCell);
			            /*pdfPOccupationTable.addCell( pdfCell);*/
			            if("REIS".equalsIgnoreCase(pdfObject.getFundId()) && !disclosureQuestionAns.getQuestiontext().equalsIgnoreCase(MetlifeInstitutionalConstants.REASON_FOR_APPLICATION_ADDITIONAL_COVER)){
			            	pdfPOccupationTable.addCell( pdfCell);
			            }else if(!"REIS".equalsIgnoreCase(pdfObject.getFundId())){
			            	pdfPOccupationTable.addCell( pdfCell);
			            }
		            }
		            
				}
			}
			document.add(pdfPOccupationTable);
		    log.info("Display quotation details finish");
		}

}

