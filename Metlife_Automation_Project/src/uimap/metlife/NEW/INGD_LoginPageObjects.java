package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class INGD_LoginPageObjects extends PageObjectLibrary{
		
	public static final By INGDXMLInput = getObjectIdentifaction("INGDirect.Login.XMLInput.id");
	public static final By INGDNextButton = getObjectIdentifaction("INGDirect.Login.NextButton.xpath");
	public static final By INGDNewApplication = getObjectIdentifaction("INGDirect.Login.NewApplication.xpath");
	
}
