/*Summary Page Controller Starts*/
FirstSuperApp.controller('workRatingSummary',['$scope', '$location', '$rootScope', '$timeout','$window','$routeParams','auraInputService','getAuraTransferData','submitAura','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService','saveEapply','RetrieveAppDetailsService',
                         function($scope, $location, $rootScope, $timeout,$window,$routeParams,auraInputService,getAuraTransferData,submitAura,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService,saveEapply,RetrieveAppDetailsService){
	$scope.urlList = urlService.getUrlList();

    $rootScope.$broadcast('enablepointer');

    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	if(($routeParams.mode == 3 || $scope.workRatingCoverDetails.svRt) && path=='/auraocc/2'){
    		path="/auraocc/3";
    	}
    	if(($routeParams.mode == 3 || $scope.workRatingCoverDetails.svRt) && path=='/quoteoccchange/2'){
    		path="/quoteoccchange/3";
    	}
    	
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;
    };


    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
     };

  // added for session expiry
	   /* $timeout(callAtTimeout, 900000);
	  	function callAtTimeout() {
	  		$location.path("/sessionTimeOut");
	  	}*/

	  	/* var timer;
	     angular.element($window).bind('mouseover', function(){
	     	timer = $timeout(function(){
	     		sessionStorage.clear();
	     		localStorage.clear();
	     		$location.path("/sessionTimeOut");
	     	}, 900000);
	     }).bind('mouseout', function(){
	     	$timeout.cancel(timer);
	     }); */

    /* var ackCheckWRDD;
     var ackCheckWRPP;*/
     var ackCheckWRGC;


    $scope.workRatingCoverDetails=PersistenceService.getworkRatingCoverDetails();
    console.log($scope.workRatingCoverDetails);

    $scope.workRatingAuraDetails=PersistenceService.getWorkRatingAuraDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();

    $scope.workRatingOccDetails =PersistenceService.getWorkRatingCoverOccDetails();
    if($scope.workRatingCoverDetails!= null && $scope.workRatingCoverDetails.auraDisabled){
    	$scope.auraDisabled = $scope.workRatingCoverDetails.auraDisabled;
	}
    	



	/* added to get  gender in occupation confirmation page */
    var personalDtl = $scope.personalDetails;
	$scope.genderFlag = false;
	if( personalDtl.personalDetails.gender != "" &&  personalDtl.personalDetails.gender != null &&  personalDtl.personalDetails.gender != '' &&  personalDtl.personalDetails.gender != " "){
		$scope.genderFlag = true;
	}

    $scope.submitWorkRating = function(){
    	/*ackCheckWRDD = $('#DutyOfDisclosureLabelWR').hasClass('active');
    	ackCheckWRPP = $('#privacyPolicyLabelWR').hasClass('active');*/
    	ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');


    	if(/*ackCheckWRDD && ackCheckWRPP &&*/ ackCheckWRGC){
    		/*$scope.WRDDackFlag = false;
    		$scope.WRPPackFlag = false;*/
    		$scope.WRGCackFlag = false;
    		if($scope.workRatingCoverDetails != null && $scope.workRatingOccDetails != null && $scope.workRatingAuraDetails != null && $scope.personalDetails != null){
    			$scope.workRatingCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.addnlDeathCoverDetails = {};
    			$scope.details.addnlTpdCoverDetails = {};
    			$scope.details.addnlIpCoverDetails = {};
    			$scope.details.occupationDetails=$scope.workRatingOccDetails;
    			$scope.details.addnlDeathCoverDetails.deathCoverPremium = $scope.workRatingCoverDetails.deathCoverPremium;
    			$scope.details.addnlTpdCoverDetails.tpdCoverPremium = $scope.workRatingCoverDetails.tpdCoverPremium;
    			$scope.details.addnlIpCoverDetails.ipCoverPremium = $scope.workRatingCoverDetails.ipCoverPremium;

    			var personalDetails=angular.extend($scope.details,$scope.personalDetails);
        		var temp = angular.extend(personalDetails,$scope.workRatingCoverDetails);
        		var submitObject = angular.extend(temp,$scope.workRatingAuraDetails);
        		auraResponseService.setResponse(submitObject);
                $rootScope.$broadcast('disablepointer');
        		submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);

            		if($scope.workRatingAuraDetails.deathDecision!=null &&  $scope.workRatingAuraDetails.tpdDecision!=null){
            			if(($scope.workRatingAuraDetails.deathDecision=="DCL") ||($scope.workRatingAuraDetails.tpdDecision=="DCL")){
            				$scope.workRatingAuraDetails.overallDecision = 'DCL';
    					}else{
    						$scope.workRatingAuraDetails.overallDecision = 'ACC';
    					}
            		}
            		if($scope.workRatingAuraDetails.overallDecision == 'ACC'){
                		$scope.go('/workRatingAccept');
                	}else if($scope.workRatingAuraDetails.overallDecision == 'DCL'){
                		$scope.go('/workRatingDecline');
                	}
            	}, function() {
                    $window.scrollTo(0, 0);
                    $rootScope.$broadcast('enablepointer');
                    throw {message: 'submitEapply request failed'};
                });
        	}else{
        		if($scope.workRatingCoverDetails != null && $scope.workRatingOccDetails != null && $scope.personalDetails != null){
        			$scope.workRatingCoverDetails.lastSavedOn = '';
        			$scope.details={};
        			$scope.details.addnlDeathCoverDetails = {};
        			$scope.details.addnlTpdCoverDetails = {};
        			$scope.details.addnlIpCoverDetails = {};
        			$scope.details.occupationDetails=$scope.workRatingOccDetails;
        			$scope.details.addnlDeathCoverDetails.deathCoverPremium = $scope.workRatingCoverDetails.deathCoverPremium;
        			$scope.details.addnlTpdCoverDetails.tpdCoverPremium = $scope.workRatingCoverDetails.tpdCoverPremium;
        			$scope.details.addnlIpCoverDetails.ipCoverPremium = $scope.workRatingCoverDetails.ipCoverPremium;

        			var personalDetails=angular.extend($scope.details,$scope.personalDetails);
            		var submitObject = angular.extend(personalDetails,$scope.workRatingCoverDetails);
            		//var submitObject = angular.extend(temp,$scope.workRatingAuraDetails);
            		auraResponseService.setResponse(submitObject);
                    $rootScope.$broadcast('disablepointer');
            		submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
                		console.log(response.data);
                		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                		PersistenceService.setNpsUrl(response.data.npsTokenURL);
                		$scope.go('/workRatingMaintain');
                	}, function() {
                        $window.scrollTo(0, 0);
                        $rootScope.$broadcast('enablepointer');
                        throw {message: 'submitEapply request failed'};
                    });
        		}

        	}
    	}else{
    		//$scope.WRackFlag = true;
    		//$scope.WRDDackFlag = true;
    		//$scope.WRPPackFlag = true;
    		//$scope.WRGCackFlag = true;
    		/*if(ackCheckWRDD){
        		$scope.WRDDackFlag = false;
        	}else{
        		$scope.WRDDackFlag = true;
        	}

        	if(ackCheckWRPP){
        		$scope.WRPPackFlag = false;
        	}else{
        		$scope.WRPPackFlag = true;
        	}*/

        	if(ackCheckWRGC){
        		$scope.WRGCackFlag = false;
        	}else{
        		$scope.WRGCackFlag = true;
        	}
        	$scope.scrollToUncheckedElement();
    	}

    };
    $scope.scrollToUncheckedElement = function(){
		var elements = [/*ackCheckWRDD, ackCheckWRPP,*/ ackCheckWRGC];
		var ids = [/*'DutyOfDisclosureLabelWR', 'privacyPolicyLabelWR',*/ 'generalConsentLabelWR'];
    	for(var k = 0; k < elements.length; k++){
    		if(!elements[k]){
    			$('html, body').animate({
        	        scrollTop: $("#" + ids[k]).offset().top
        	    }, 1000);
    			break;
    		}
    	}
    };
    var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveSummaryWorkRating = function() {
    	$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.workRatingCoverDetails != null && $scope.workRatingOccDetails != null && $scope.personalDetails != null){
    		$scope.workRatingCoverDetails.lastSavedOn = 'SummaryUpdatePage';
    		$scope.details={};
    		$scope.details.addnlDeathCoverDetails = {};
			$scope.details.addnlTpdCoverDetails = {};
			$scope.details.addnlIpCoverDetails = {};
			$scope.details.occupationDetails=$scope.workRatingOccDetails;
			$scope.details.addnlDeathCoverDetails.deathCoverPremium = $scope.workRatingCoverDetails.deathCoverPremium;
			$scope.details.addnlTpdCoverDetails.tpdCoverPremium = $scope.workRatingCoverDetails.tpdCoverPremium;
			$scope.details.addnlIpCoverDetails.ipCoverPremium = $scope.workRatingCoverDetails.ipCoverPremium;

    		var temp = angular.extend($scope.workRatingCoverDetails,$scope.details);
    		if($scope.workRatingAuraDetails != null){
    			var aura = angular.extend(temp,$scope.workRatingAuraDetails);
    			var saveUpdateSummaryObject = angular.extend(aura, $scope.personalDetails);
    		}else{
    			var saveUpdateSummaryObject = angular.extend(temp, $scope.personalDetails);
    		}
        	auraResponseService.setResponse(saveUpdateSummaryObject);
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                console.log(response.data)
	        });
    	}
    };

   $scope.saveAndExitPopUp = function (hhText) {

		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

	 if($routeParams.mode == 3){
     	var num = PersistenceService.getAppNumToBeRetrieved();
     	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
     		var result = res.data[0];
     		var coverDet={},occDet={};

     		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
     		coverDet.firstName = result.personalDetails.firstName;
     		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactType = result.contactType;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		occDet.gender = result.personalDetails.gender;
    		coverDet.appNum = result.appNum;
    		coverDet.manageType = "UWCOVER";
    		coverDet.partnerCode ="HOST";
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.waitingPeriod=result.waitingPeriod || '';
    		coverDet.benefitPeriod=result.benefitPeriod || '';
    		if(result.auraDisabled && result.auraDisabled =="true"){
    			$scope.auraDisabled = true;
    		}else{
    			$scope.auraDisabled = false;
    		}
			$scope.workRatingCoverDetails = coverDet;

    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		/*occDet.citizenQue = result.occupationDetails.citizenQue;*/
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		/*occDet.hazardousQue = result.occupationDetails.hazardousQue;*/
    		occDet.salary = result.occupationDetails.salary;
    		occDet.otherOccupation = result.occupationDetails.otherOccupation;
    		$scope.workRatingOccDetails = occDet;
    		coverDet.deathCoverPremium = parseFloat(result.addnlDeathCoverDetails.deathCoverPremium);
    		coverDet.tpdCoverPremium = parseFloat(result.addnlTpdCoverDetails.tpdCoverPremium);
    		coverDet.ipCoverPremium = parseFloat(result.addnlIpCoverDetails.ipCoverPremium);
    		coverDet.deathAmt = parseFloat(result.existingDeathAmt);
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.tpdAmt = parseFloat(result.existingTpdAmt);
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.ipAmt = parseFloat(result.existingIPAmount);
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.totalPremium = parseFloat(result.totalPremium);
    		$scope.getAuradetails();
     	},function(err){
     		console.log("something went wrong while saving..."+JSON.stringify(err));
     	})
	 }

	 
	 $scope.getAuradetails = function()
	    {

		 	auraInputService.setFund('HOST')
		  	auraInputService.setMode('WorkRating')
		  	auraInputService.setName($scope.workRatingCoverDetails.name);
		  	auraInputService.setAge(moment().diff(moment($scope.workRatingCoverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
		  	auraInputService.setAppnumber($scope.workRatingCoverDetails.appNum);
		  	auraInputService.setDeathAmt(parseInt($scope.workRatingCoverDetails.deathAmt));
		  	auraInputService.setTpdAmt(parseInt($scope.workRatingCoverDetails.tpdAmt));
		  	auraInputService.setIpAmt(parseInt($scope.workRatingCoverDetails.ipAmt));
		  	auraInputService.setWaitingPeriod($scope.workRatingCoverDetails.waitingPeriod);
		  	auraInputService.setBenefitPeriod($scope.workRatingCoverDetails.benefitPeriod);
		  	if($scope.workRatingOccDetails && $scope.workRatingOccDetails.gender ){
		  		auraInputService.setGender($scope.workRatingOccDetails.gender);
		  	}
		  	auraInputService.setIndustryOcc($scope.workRatingOccDetails.industryCode+":"+$scope.workRatingOccDetails.occupation);
		  	auraInputService.setCountry('Australia');
		  	auraInputService.setSalary($scope.workRatingOccDetails.salary);
		  	auraInputService.setFifteenHr($scope.workRatingOccDetails.fifteenHr);
		  	auraInputService.setClientname('metaus')
		  	auraInputService.setLastName($scope.workRatingCoverDetails.lastName)
		  	auraInputService.setFirstName($scope.workRatingCoverDetails.firstName)
		  	auraInputService.setDob($scope.workRatingCoverDetails.dob)
		  	auraInputService.setExistingTerm(false);
		  	if($scope.personalDetails.memberType=="Personal"){
		  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
		  	}else{
		  		auraInputService.setMemberType("None")
		  	}
			 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
		  		$scope.auraResponseDataList = response.data.questions;
		  		console.log($scope.auraResponseDataList)
		  		angular.forEach($scope.auraResponseDataList, function(Object) {
					$scope.sectionname = Object.questionAlias.substring(3);

					});
		  	});
	  	
	    	 
	    	 submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
	    		 $scope.workRatingAuraDetails = response.data;
	    	 });
	    	 
	    };
	 
    /*$scope.checkAckStateWR = function(){
    	$timeout(function(){
    		ackCheckWRDD = $('#DutyOfDisclosureLabelWR').hasClass('active');
        	ackCheckWRPP = $('#privacyPolicyLabelWR').hasClass('active');
        	ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');

        	if(ackCheckWRDD){
        		$scope.WRDDackFlag = false;
        	}else{
        		$scope.WRDDackFlag = true;
        	}

        	if(ackCheckWRPP){
        		$scope.WRPPackFlag = false;
        	}else{
        		$scope.WRPPackFlag = true;
        	}

        	if(ackCheckWRGC){
        		$scope.WRGCackFlag = false;
        	}else{
        		$scope.WRGCackFlag = true;
        	}
    	}, 10);
    };*/

    /*$scope.checkAckStateDDWR = function(){
    	$timeout(function(){
    		ackCheckWRDD = $('#DutyOfDisclosureLabelWR').hasClass('active');
        	if(ackCheckWRDD){
        		$scope.WRDDackFlag = false;
        	}else{
        		$scope.WRDDackFlag = true;
        	}
    	}, 10);
    };*/

    /*$scope.checkAckStatePPWR = function(){
    	$timeout(function(){
    		ackCheckWRPP = $('#privacyPolicyLabelWR').hasClass('active');
        	if(ackCheckWRPP){
        		$scope.WRPPackFlag = false;
        	}else{
        		$scope.WRPPackFlag = true;
        	}
    	}, 10);
    };*/

    $scope.checkAckStateGCWR = function(){
    	$timeout(function(){
    		ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');
        	if(ackCheckWRGC){
        		$scope.WRGCackFlag = false;
        	}else{
        		$scope.WRGCackFlag = true;
        	}

    	}, 10);
    };


    }]);
   /*Summary Page Controller Ends*/
