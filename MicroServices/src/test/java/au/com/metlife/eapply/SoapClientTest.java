package au.com.metlife.eapply;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetails;
import au.com.finsyn.acuritywebservices.services.UpdateInsuranceDetailsResponse;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.utility.EapplyHelper;
import au.com.metlife.eapply.webservices.EapplicationtRequestMapper;

public class SoapClientTest {
	
	@Test(expected = SoapFaultClientException.class)
	public void shouldCallStatewideServiceUsingSoapClient()   {
		//given
		SoapClient soapClient = new SoapClient();
		String fileName = "eapplySubmitSfps.json";
		JSONParser parser = new JSONParser();
		UpdateInsuranceDetails request;
		UpdateInsuranceDetailsResponse response=null;;
		try {
			Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
			JSONObject jsonObject = (JSONObject) object;		
			EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
			EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
			Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
			EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
			 if(eapplyInput.getOverallDecision()==null){
				 eapplyInput.setOverallDecision("ACC");
				 applicationDTO.setAppdecision("ACC");
			 }		 
			//when
			  request= eapplicationrequestMapper.doMapForSFPSFynSyn(applicationDTO, eapplyInput);			 		
			  response = soapClient.callStateWideService(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			if(e instanceof SoapFaultClientException) {
				SoapFaultClientException soapFaultClientException = (SoapFaultClientException)e;
				System.out.println(soapFaultClientException.getMessage());
				throw soapFaultClientException;
			}
		}
		 //System.out.println(response);
	}
	@Test(expected = SoapFaultClientException.class)
	public void shouldCallStatewideTransferServiceUsingSoapClient()   {
		//given
		SoapClient soapClient = new SoapClient();
		String fileName = "SfpsTransferAccept.json";
		JSONParser parser = new JSONParser();
		UpdateInsuranceDetails request;
		UpdateInsuranceDetailsResponse response=null;;
		try {
			Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
			JSONObject jsonObject = (JSONObject) object;		
			EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
			EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
			Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
			EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
			 if(eapplyInput.getOverallDecision()==null){
				 eapplyInput.setOverallDecision("ACC");
				 applicationDTO.setAppdecision("ACC");
			 }		 
			//when
			  request= eapplicationrequestMapper.doMapForSFPSFynSynTransfer(applicationDTO, eapplyInput);			 		
			  response = soapClient.callStateWideService(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			if(e instanceof SoapFaultClientException) {
				SoapFaultClientException soapFaultClientException = (SoapFaultClientException)e;
				System.out.println(soapFaultClientException.getMessage());
				throw soapFaultClientException;
			}
		}
		 //System.out.println(response);
	}
	
	@Test
	public void shouldCallStatewideCancelFixedServiceUsingSoapClient()   {
		//given
		SoapClient soapClient = new SoapClient();
		String fileName = "SfpsCancellation_deathTpd_fixed.json";
		JSONParser parser = new JSONParser();
		UpdateInsuranceDetails request;
		UpdateInsuranceDetailsResponse response=null;;
		try {
			Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
			JSONObject jsonObject = (JSONObject) object;		
			EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
			EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
			Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
			EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
			 if(eapplyInput.getOverallDecision()==null){
				 eapplyInput.setOverallDecision("ACC");
				 applicationDTO.setAppdecision("ACC");
			 }		 
			//when
			  request= eapplicationrequestMapper.doMapForSFPSFynSynCancellation(applicationDTO, eapplyInput);			 		
			  response = soapClient.callStateWideService(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			if(e instanceof SoapFaultClientException) {
				SoapFaultClientException soapFaultClientException = (SoapFaultClientException)e;
				System.out.println(soapFaultClientException.getMessage());
				throw soapFaultClientException;
			}
		}
		 //System.out.println(response);
	}
	
	
	@Test
	public void shouldCallStatewideCancelUnitsServiceUsingSoapClient()   {
		//given
		SoapClient soapClient = new SoapClient();
		String fileName = "sfps_cancellation_deathtpd_units.json";
		JSONParser parser = new JSONParser();
		UpdateInsuranceDetails request;
		UpdateInsuranceDetailsResponse response=null;;
		try {
			Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
			JSONObject jsonObject = (JSONObject) object;		
			EapplyInput eapplyInput =new ObjectMapper().readValue(jsonObject.toString(), EapplyInput.class);
			EapplicationtRequestMapper eapplicationrequestMapper = new EapplicationtRequestMapper();		
			Eapplication applicationDTO = EapplyHelper.convertToEapplication(eapplyInput,"Submitted");	
			EapplyHelper.setdefaultDecision(applicationDTO, eapplyInput);		 
			 if(eapplyInput.getOverallDecision()==null){
				 eapplyInput.setOverallDecision("ACC");
				 applicationDTO.setAppdecision("ACC");
			 }		 
			//when
			 request= eapplicationrequestMapper.doMapForSFPSFynSynCancellation(applicationDTO, eapplyInput);		 		
			  response = soapClient.callStateWideService(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e instanceof SoapFaultClientException) {
				SoapFaultClientException soapFaultClientException = (SoapFaultClientException)e;
				System.out.println(soapFaultClientException.getMessage());
				throw soapFaultClientException;
			}
		}
		 //System.out.println(response);
	}

}
