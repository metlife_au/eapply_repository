class AppFooterCtrl {
  constructor(AppConstants, ConfigData) {
    'ngInject';
    this.appName = AppConstants.appName;
    this.config = ConfigData.getPartnerBasedConfiguration(inputData.partnerID);
   
    // Get today's date to generate the year
    this.date = new Date();
  }
}

let AppFooter = {
  controller: AppFooterCtrl,
  controllerAs: '$footer',
  templateUrl: 'layout/footer.html'
};

export default AppFooter;
