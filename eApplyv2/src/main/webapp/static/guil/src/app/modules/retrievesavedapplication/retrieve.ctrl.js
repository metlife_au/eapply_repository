(function(angular){
	'use strict';
    /* global channel, inputData, token, moment */
	angular
	.module('guildApp')
	.controller('retrievesavedapplication', retrievesavedapplication);
	retrievesavedapplication.$inject=['$scope','$rootScope','$window','$q','fetchUrlSvc','guilUIConstants','$location','urlService','fetchClientDataSvc','ngDialog','$timeout','RetrieveAppDetailsService','PersistenceService','fetchAppNumberSvc','appData','RetrieveAppService','fetchPersoanlDetailSvc','persoanlDetailService','extendAppModelGuild'];
	function retrievesavedapplication($scope, $rootScope, $window, $q, fetchUrlSvc, guilUIConstants, $location, urlService,fetchClientDataSvc, ngDialog, $timeout, RetrieveAppDetailsService, PersistenceService, fetchAppNumberSvc, appData, RetrieveAppService, fetchPersoanlDetailSvc,persoanlDetailService, extendAppModelGuild){

		
		var vm=this;		
		vm.urlList = fetchUrlSvc.getUrlList();
		
		vm.init = function(){
	    	console.log('retrieve init');

	    	var defer = $q.defer();

	        fetchClientDataSvc.requestObj(vm.urlList.clientDataUrl).then(function(response) {
	    	vm.partnerRequest = response.data;
	    	vm.applicantlist = vm.partnerRequest.applicant;
			
	    	angular.forEach(vm.applicantlist, function (applicant) {        		
	    		vm.personalDetails = applicant.personalDetails;
	    		vm.inputDetails = applicant;
	    		vm.coverList = applicant.existingCovers;
	    	});
	    	
	    	fetchPersoanlDetailSvc.setMemberDetails(vm.applicantlist[0]);
	    	appData.setAppData(extendAppModelGuild.extendObj());
	    	
	    	var memberDetails = fetchPersoanlDetailSvc.getMemberDetails();
			var refNo = memberDetails.clientRefNumber;
			
			RetrieveAppService.retrieveApp(vm.urlList.retrieveSavedAppUrl,'GUIL',refNo).then(function(res){
				
				vm.apps = res.data;
				
				for(var i = 0; i < vm.apps.length; i++){
					
					vm.apps[i].hyperlink = false;
					var tempDate = new Date(vm.apps[i].createdDate);
					vm.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');
					
					if(vm.apps[i].requestType === 'CCOVER'){
						vm.apps[i].requestType = 'Change Cover';
					}else if(vm.apps[i].requestType === 'TCOVER'){
						vm.apps[i].requestType = 'Transfer Cover';
					}else if(vm.apps[i].requestType === 'UWCOVER'){
						vm.apps[i].requestType = 'Update Work Rating';
					}else if(vm.apps[i].requestType === 'NCOVER'){
						vm.apps[i].requestType = 'New Member Cover';
					}else if(vm.apps[i].requestType === 'CANCOVER'){
						vm.apps[i].requestType = 'Cancel Cover';
					}else if(vm.apps[i].requestType === 'ICOVER'){
						vm.apps[i].requestType = 'Life Event Cover';
					}else if(vm.apps[i].requestType === 'SCOVER'){
						vm.apps[i].requestType = 'Special Cover';
					}else if(vm.apps[i].requestType === 'NMCOVER'){
						vm.apps[i].requestType = 'Non Member Cover';
					}
					
					if(vm.apps[i].applicationStatus.toLowerCase() === 'pending'){
						vm.apps[i].hyperlink = true;
					}
				}
			}, function(err){
				console.log('Error while fetching the apps ' + err);
			});
	    	defer.resolve(response);
	    },function(err){
	        defer.reject(err);
	    	});
	        return defer.promise; 
	        
	    
			
			
		};
		
		vm.init();
		
		
		
		vm.go = function (path) {
			$timeout(function(){
				$location.path(path);
			}, 10);
		};
		
		vm.navigateToLandingPage = function (){
			
			var templateContent = '<div class="ngdialog-content">';
			templateContent += '<div class="modal-body">';
			templateContent += '<div class="row  rowcustom  "><div class="col-sm-8">';
			templateContent += '<p> Are you sure you want to navigate to Home Page?</p>';
			templateContent += '</div> </div></div></br>';
			templateContent += '<div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter">';
			templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button>';
			templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button>';
			templateContent += '</div></div>';
			
			
			ngDialog.openConfirm({
	            template:templateContent,
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
	    };
	    
	    vm.goToSavedApp = function(app){
	    	fetchAppNumberSvc.setAppNumber(app.applicationNumber);
	    	PersistenceService.setAppNumToBeRetrieved(app.applicationNumber);
	    	PersistenceService.setAppNumber(app.applicationNumber);
	    	
	    	RetrieveAppDetailsService.retrieveAppDetails(vm.urlList.retrieveAppUrlNew,app.applicationNumber).then(function(res){
	    		
	    		appData.setAppData(extendAppModelGuild.extendObj(res.data[0]));
				
				switch(app.lastSavedOnPage.toLowerCase()){
                case 'quotepage':
                    vm.go('/cover/3');
                    break;
                case 'aurapage':
                    vm.go('/aura');
                    break;
                case 'summarypage':
                    vm.go('/summary');
                    break;
                case 'transferpage':
                	PersistenceService.setTransferCoverDetails(res.data[0]);
                    vm.go('/quotetransfer/3');
                    break;
                case 'auratransferpage':
                	PersistenceService.setTransferCoverDetails(res.data[0]);
                    vm.go('/auratransfer/3');
                    break;
                case 'summarytransferpage':
                	PersistenceService.setTransferCoverDetails(res.data[0]);
                    vm.go('/transferSummary/3');
                    break;
                case 'quoteupdatepage':
                    vm.go('/quoteoccchange/3');
                    break;
                case 'auraupdatepage':
                    vm.go('/auraocc/3');
                    break;
                case 'summaryupdatepage':
                    vm.go('/workRatingSummary/3');
                    break;
                case 'lifeeventpage':
                	PersistenceService.setLifeEventDetails(res.data[0]);
                    vm.go('/quoteLife/3');
                    break;
                case 'auralifeeventpage':
                	PersistenceService.setLifeEventDetails(res.data[0]);
                    vm.go('/auraLife/3');
                    break;
                case 'summarylifeeventpage':
                	PersistenceService.setLifeEventDetails(res.data[0]);
                    vm.go('/summaryLife/3');
                    break;
                case 'specialquotepage':
                    vm.go('/quotespecial/3');
                    break;
                case 'specialcoveraurapage':
                    vm.go('/auraspecial/3');
                    break;
                case 'specialsummarypage':
                    vm.go('/specialoffersummary/3');
                    break;
                case 'quotecancelpage':
                    vm.go('/quotecancel/3');
                    break;
                case 'auracancelpage':
                    vm.go('/auracancel/3');
                    break;
                case 'summarycancelpage':
                    vm.go('/cancelConfirmation/3');
                    break;
                case 'nonmemberquotepage':
    	     		vm.go('/nonmember/3');
    	     		break;
    	     	case 'nonmemberaurapage':
    	     		vm.go('/auraNonMember/3');
    	     		break;
    	     	case 'nonmembersummarypage':
    	     		vm.go('/nonMemberSummary/3');
    	     		break;
                default:
                    break;
            }
			});
		};
		
	}
	
})(angular);

 /* Retrieve Controller,Progressive and Mandatory validations Ends  */
