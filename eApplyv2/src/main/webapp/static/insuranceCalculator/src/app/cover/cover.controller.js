class CoverCtrl {
  constructor(AppConstants, CommonAPI, $scope, $state, ngDialog, $q, moment, ConfigData, $rootScope, $timeout, $sce) {
    'ngInject';
    
    this._AppConstants = AppConstants;
    this._CommonAPI = CommonAPI;
    this._$scope = $scope;
    this._$state = $state;
    this._ngDialog = ngDialog;
    this._$q = $q;
    this._MomentAPI = moment;
    this._$rootScope = $rootScope;
    this._ConfigData = ConfigData;
    this._$timeout = $timeout;  
    this._$sce = $sce;
    
    this.modelOptions = {updateOn: "blur"};
    this.showCoverSection = false;
    this.calculateFlag = false;
    this.firstTimeCalculation = false;
    this.calculationData = {
    		'calculatorFlag': 'insurancecover',
    		'requestType': 'CCOVER',
    		'quickQuoteRender': true,
    		'applicant' : {
    			'totalExpense' : '',
    			'totalDebts' : '',
    			'showDebtsDetails' : false,
    			'showExpenseDetails' : false,
    			'cover':[
    				{
    					'benefitType':'1'
    				},
    				{
    					'benefitType':'2'
    				},
    				{
    					'benefitType':'4'
    				}
    			]
    		}
    };
  }
  
  init(){
	 
	  this.premiumCalURL = window.location.href.replace("insuranceCalculator", "premiumCalculator");
	  this.fundID = inputData.partnerID;
	  this.appicantData = inputData.applicant[0];
	  
	  this.urlList = this._CommonAPI.getUrlList();
	  this.industryList();
	  this.initalizeRules();
	  this.initalizeChild();
	  
	  this.config = this._ConfigData.getPartnerBasedConfiguration(this.fundID);
	  this.expenseFieldList = this._ConfigData.expenseFields;
	  this.debtsFieldList = this._ConfigData.debtsFields;
	  this.termOptions = this._ConfigData.termOptions;
	  
	  this.calculationData.partnerCode = this.fundID;
	  this.calculationData.applicant.salaryFrequency = 'Annually';
	  this.calculationData.applicant.spouseSalaryFrequency = 'Annually';
	  this.calculationData.applicant.totalExpenseFrequency = 'Monthly';
	  this.calculationData.applicant.mortgageFrequency = 'Monthly';
	  this.calculationData.applicant.age = this._MomentAPI().diff(this._MomentAPI(this.appicantData.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years');
	  this.calculationData.applicant.gender = this.appicantData.personalDetails.gender;
	  
	  this.calculationData.applicant.cover[2].additionalIpWaitingPeriod = this.config.defaultWaitingperiod;
	  this.calculationData.applicant.cover[2].additionalIpBenefitPeriod = '2 Years';
	  
	  this.calculationData.applicant.cover[0].frequencyCostType = this.config.defaultFrequency;
	  this.calculationData.applicant.cover[1].frequencyCostType = this.config.defaultFrequency;
	  this.calculationData.applicant.cover[2].frequencyCostType = this.config.defaultFrequency;
	  
	  this.calculationData.applicant.cover[1].nursingCosts = false;
	  this.calculationData.applicant.cover[1].tpdWithoutIP = false;
	  
	  this.calculationData.applicant.mortage = this.config.haveMortage;
	  
	  console.log(this.calculationData);
  }
  
  initalizeChild(){
	  this.childList = [];
  }  
  
  addChild(){
	  
	   var obj = {
	    	'age'  : '',
	    	'name' : 'childAge'+Math.round(Math.random()*100)+Math.round(Math.random()*100)
	    };
	   
	   this.childList.push(obj);
  }
  
  removeChild(index){
	  this.childList.splice(index, 1);
	  this.checkCalculation(); 
  }
  
  checkWorkHrs(questionName, answer){
	  
	  if(questionName == 'workHours' && answer == 'No'){
		  this.disableIP = true;
	  }else if(questionName == 'workHours' && answer == 'Yes'){
		  this.disableIP = false; 
	  }
	  this.checkCalculation();  
  }
  
  validateChildAge(index, age){
	  
	  var validAge = '';
	  
	  if(age >= 0 && age<19){
		  validAge = true;
	  }
	  this.childList[index].isValid = validAge;
	  this.childList[index].age = parseInt(age);
	  if(this.childList[index].isValid){
		  this.checkCalculation(); 
	  }
  }
  
  initalizeRules(){
	  
	  this._CommonAPI.getRulesList(this.urlList.insuranceRuleURL, this.fundID).then(
	      (res) => {
	    	this.rules = res[0];
	    	//console.log(this.rules);
	      },
	      (err) => {
	        console.log(err.data.errors);
	      }
      );
  }
  
  storeCurrentRuleValues(){	  
	  this.OldRules = angular.extend({},this.OldRules, this.rules);
	  console.log(this.OldRules); 
  }
  
  calculateForNewRules(){
	  $('#rulesModal').modal('hide');
	  console.log(this.rules);
	  this.checkCalculation();
  }
  
  resetRuleValues(){
	  console.log(this.rules);
	  this.rules = angular.extend({},this.rules, this.OldRules);
	  console.log(this.rules);
	  $('#rulesModal').modal('hide');	  
  }
  
  industryList() {
	  
	  this._CommonAPI.fetchIndustryList(this.urlList.quoteUrl, this.fundID).then(
	      (res) => {
	    	this._CommonAPI.setIndustryList(res);
	        this.IndustryOptions = res;
	      },
	      (err) => {
	        console.log(err.data.errors);
	      });
  }
  
  getOccupations() {
	  this.calculationData.applicant.occupation = '';
	  this.calculationData.applicant.otherOccupation = '';
	  this.calculationData.applicant.industryType = this.calculationData.applicant.industryType === '' ? null : this.calculationData.applicant.industryType;
	  
	  this._CommonAPI.fetchOccupationList(this.urlList.occupationUrl, this.calculationData.applicant.industryType, this.fundID).then(
	      (res) => {
	        this.occupationList = res;
	      },
	      (err) => {
	        this.errors = err.data.errors;
	      }
	  );
  }
  
  checkAge(){
	  this.invalidAge = true;
	  
	  if(parseInt(this.calculationData.applicant.age) >= 11 && parseInt(this.calculationData.applicant.age) <= 69){
		  this.invalidAge = false;
	  }
	  
	  this.checkCalculation(); 
  }
  
  checkExpense(){
	  if(!parseInt(this.calculationData.applicant.totalExpense)>0 && parseInt(this.calculationData.applicant.totalExpense) !== 0){
		  this.calculationData.applicant.totalExpense = '';
	  }
	  this.checkCalculation(); 
  }
  
  checkDebts(){
	  if(!parseInt(this.calculationData.applicant.totalDebts)>0 && parseInt(this.calculationData.applicant.totalDebts) !== 0){
		  this.calculationData.applicant.totalDebts = '';
	  }
	  this.checkCalculation(); 
  }
  
  calculateExpense(){
	  
	  const _this = this;
	  
	  _this.totalExpenseCalculated = 0 ;
	  
	  angular.forEach(_this.expenseFieldList, function (obj, index) {		 
		  let expense = obj.fieldValue || 0;
		  let term = obj.termValue;
		  let frequency = _this.getFrequency(term);
		  let finalAmount = parseInt(expense) * frequency;
		  _this.totalExpenseCalculated = parseInt(_this.totalExpenseCalculated) + parseInt(finalAmount);		  
	  });
	  
	  if(_this.totalExpenseCalculated > 0){
		  _this.calculationData.applicant.totalExpenseFrequency = 'Annually';
		  _this.calculationData.applicant.totalExpense = _this.totalExpenseCalculated;
		  _this.calculationData.applicant.showExpenseDetails = true;
	  }else{
		  _this.calculationData.applicant.totalExpenseFrequency = 'Monthly';
		  _this.calculationData.applicant.totalExpense = '';
		  _this.calculationData.applicant.showExpenseDetails = false;
	  }
	  
	  var expenseListArray = [];
	  angular.forEach(_this.expenseFieldList, function (obj, index) {
		  var currentobj = {};
		  if(obj.fieldValue){
			  currentobj.answerText = '$'+obj.fieldValue+' '+obj.termValue;
		  }else{
			  currentobj.answerText = '$0 '+ obj.termValue;
		  }
		  
		  currentobj.questiontext = obj.labelText;
		  expenseListArray.push(currentobj);
	  });
	  
	  _this.calculationData.applicant.expenses = expenseListArray; 
  }
  
  calculateDebts(){
	  
	  const _this = this;
	  
	  _this.totalDebtsCalculated = 0 ;
	  
	  angular.forEach(_this.debtsFieldList, function (obj, index) {
		  let debts = obj.fieldValue || 0;
		  _this.totalDebtsCalculated = parseInt(_this.totalDebtsCalculated) + parseInt(debts);
	  });
	  
	  if(_this.totalDebtsCalculated > 0){
		  _this.calculationData.applicant.showDebtsDetails = true;
		  _this.calculationData.applicant.totalDebts = _this.totalDebtsCalculated;
	  }else{
		  _this.calculationData.applicant.showDebtsDetails = false;
		  _this.calculationData.applicant.totalDebts = '';
	  }
	  
	  var debtsListArray = [];
	  angular.forEach(_this.debtsFieldList, function (obj, index) {
		  var currentobj = {};
		  if(obj.fieldValue){
			  currentobj.answerText = '$'+obj.fieldValue;
		  }else{
			  currentobj.answerText = '$0';
		  }
		  currentobj.questiontext = obj.labelText;		  
		  debtsListArray.push(currentobj);
	  });
	  
	  _this.calculationData.applicant.debts = debtsListArray;
	  
	 // _this.checkCalculation(); 
  }
  
  getFrequency(term){
	  
	  let frequency;
	  
	  switch(term) {
	  
	  	case 'Annually':
	  		frequency = 1;	  	    
	  		break;
	  	case 'Quarterly':
	  		frequency = 4;	  	    
	  		break;
	  	case 'Monthly':
	  		frequency = 12;	  	    
	  		break;
	  	case 'Fortnightly':
	  		frequency = 26;	  	    
	  		break;
	  	case 'Weekly':
	  		frequency = 52;	  	    
	  		break; 
	  		
	  	default:
	  		break;
	  }
		return frequency;
  
  }
  
  validateDeathAmount(){
	  
	  this.deathMaxErrorFlag = false;
	  this.tpdGreaterThanDeathErrorFlag = false;
	  
	  if(this.calculationData.applicant.cover[0].additionalCoverAmount > this.config.deathMaxAmount){
		  this.deathMaxErrorFlag = true;
	  }
	  
	  if(this.config.tpdLessThanDeath){
		  if(parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount) > parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount)){
			  this.tpdGreaterThanDeathErrorFlag = true;  
		  }else{
			  this.tpdGreaterThanDeathErrorFlag = false; 
		  }
	  }else{
		  this.tpdGreaterThanDeathErrorFlag = false;
	  }
	  
	  if(!this.deathMaxErrorFlag && !this.tpdGreaterThanDeathErrorFlag){
		  this.calculationData.applicant.cover[0].additionalCoverAmount = parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount);
		  this.calculationData.applicant.cover[0].additionalCoverAmountOld = this.calculationData.applicant.cover[0].additionalCoverAmount;
		  this.calculateCost();
	  } 
	  
  }
  
  validateTPDAmount(){
	  this.tpdMaxErrorFlag = false;
	  this.tpdGreaterThanDeathErrorFlag = false;
	  
	  if(this.calculationData.applicant.cover[1].additionalCoverAmount > this.config.tpdMaxAmount){
		  this.tpdMaxErrorFlag = true;
	  }
	  
	  if(this.config.tpdLessThanDeath){
		  if(parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount) > parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount)){
			  this.tpdGreaterThanDeathErrorFlag = true;  
		  }else{
			  this.tpdGreaterThanDeathErrorFlag = false; 
		  }
	  }else{
		  this.tpdGreaterThanDeathErrorFlag = false;
	  }
	  
	  if(!this.tpdMaxErrorFlag && !this.tpdGreaterThanDeathErrorFlag){
		  this.calculationData.applicant.cover[1].additionalCoverAmount = parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount);
		  this.calculationData.applicant.cover[1].additionalCoverAmountOld = this.calculationData.applicant.cover[1].additionalCoverAmount;
		  this.calculateCost();
	  }
	  
  }
  
  getTotalAnnualSalary(){
	  
	  var totalAnnualSalary = 0;	  
	  
	  var salaryFrequency = this.getFrequency(this.calculationData.applicant.salaryFrequency);	  
	  totalAnnualSalary = this.calculationData.applicant.annualSalary*salaryFrequency;
	  
	  if(this.calculationData.applicant.livingWithWife && this.calculationData.applicant.spouseSalary){
		  var spouseSalaryFrequency = this.getFrequency(this.calculationData.applicant.spouseSalaryFrequency);
		  totalAnnualSalary = totalAnnualSalary + (this.calculationData.applicant.spouseSalary*spouseSalaryFrequency);
	  }
	  
	  return totalAnnualSalary;
  }
  
  validateIPAmount(){
	  
	  var totalAnnualSalary = this.getTotalAnnualSalary();
	  
	  this.ipRoundedFlag = false;
	  
	  var calculatedAmount = Math.round((this.config.insuredIpValue/100) * (totalAnnualSalary/ 12));
	  
	  var allowedAmount;
	  
	  if(this.config.ipMaxAmount > calculatedAmount){
		  allowedAmount = calculatedAmount;
	  }else{
		  allowedAmount = this.config.ipMaxAmount;
	  }
	  
	  if(this.calculationData.applicant.cover[2].additionalCoverAmount > allowedAmount){
		  this.ipRoundedFlag = true;
		  this.calculationData.applicant.cover[2].additionalCoverAmount  = allowedAmount;
	  }
	  this.calculationData.applicant.cover[2].additionalCoverAmountOld = this.calculationData.applicant.cover[2].additionalCoverAmount;	  
	  this.setOccupationRating();
	  
  }
  
  checkCalculation(){
	  if(this.calculateFlag && this._$scope.userDetailsForm.$valid && this._$scope.financialForm.$valid){
		  this.calculate();
	  }
  }
  
  calculate(){
	  
	  let calculateJSON = {
			  'fundId': this.fundID,
			  'applicant':{
				  'age': parseInt(this.calculationData.applicant.age),
				  'gender': this.calculationData.applicant.gender,
				  'totalExpense' : this.calculationData.applicant.totalExpense,
				  'totalExpenseFrequency' : this.calculationData.applicant.totalExpenseFrequency,
				  'totalDebts' : this.calculationData.applicant.totalDebts,
				  'mortage' : this.calculationData.applicant.mortage ? this.calculationData.applicant.mortage : '',
				  'mortgageAmount' : this.calculationData.applicant.mortgageAmount ? this.calculationData.applicant.mortgageAmount : '',
				  'mortgageFrequency' : this.calculationData.applicant.mortgageAmount ? this.calculationData.applicant.mortgageFrequency : '',
				  'salary':this.calculationData.applicant.annualSalary,
			      'salaryFrequency':this.calculationData.applicant.salaryFrequency,
			      'livingWithWife': this.calculationData.applicant.livingWithWife,
			      'spouseSalary':this.calculationData.applicant.spouseSalary,
			      'spouseSalaryFrequency':this.calculationData.applicant.spouseSalaryFrequency,
			      'dependentChild': this.calculationData.applicant.dependentChild,
			      'childList':this.childList,
			      'nursingCosts':this.calculationData.applicant.cover[1].nursingCosts,
			      'tpdWithoutIP':this.calculationData.applicant.cover[1].tpdWithoutIP
			  },
		      'insuranceRuleDetails' : this.rules
	  };
	  
	  console.log(JSON.stringify(calculateJSON));
	  
	  this._CommonAPI.calculateInsurance(this.urlList.insuranceCalcUrl, calculateJSON).then(
		      (res) => {
		    	  var premium = res[0];
		    	  
		    	  this.calculationData.applicant.cover[0].existingCoverAmount = premium.recommendedDeathCover;
		    	  this.calculationData.applicant.cover[0].additionalCoverAmount = premium.requiredDeathCover;
		    	  this.calculationData.applicant.cover[0].coverCategory = 'DcFixed';
		    	  
		    	  this.calculationData.applicant.cover[1].existingCoverAmount = premium.recommendedTpdCover;
		    	  this.calculationData.applicant.cover[1].coverCategory = 'TPDFixed';
		    	  this.calculationData.applicant.cover[1].additionalCoverAmount = premium.requiredTpdCover; 
		    	  
		    	  
		    	  if(this.disableIP){
		    		  this.calculationData.applicant.cover[2].existingCoverAmount = '0';
		    		  this.calculationData.applicant.cover[2].additionalCoverAmount = '0';		    	  
		    		  this.calculationData.applicant.cover[2].coverCategory = 'IpFixed';
		    	  }else{
		    		  this.calculationData.applicant.cover[2].existingCoverAmount = premium.recommendedIpCover;		    		  	
		    		  this.calculationData.applicant.cover[2].additionalCoverAmount = premium.requiredIpCover;
		    		  this.calculationData.applicant.cover[2].coverCategory = 'IpFixed';
		    	  }
		    	  
		    	  //to maintain Old values
		    	  if(!this.firstTimeCalculation){		    		  
		    		  this.calculationData.applicant.cover[0].additionalCoverAmountOld = premium.requiredDeathCover;
		    		  this.calculationData.applicant.cover[1].additionalCoverAmountOld = premium.requiredTpdCover;
		    		  if(!this.disableIP){
		    			  this.calculationData.applicant.cover[2].additionalCoverAmountOld = premium.requiredIpCover;
		    		  }
		    		  this.firstTimeCalculation = true;
		    	  }else{
		    		  this.calculationData.applicant.cover[0].additionalCoverAmount = this.calculationData.applicant.cover[0].additionalCoverAmountOld;
		    		  this.calculationData.applicant.cover[1].additionalCoverAmount = this.calculationData.applicant.cover[1].additionalCoverAmountOld;
		    		  if(!this.disableIP){
		    			  this.calculationData.applicant.cover[2].additionalCoverAmount = this.calculationData.applicant.cover[2].additionalCoverAmountOld;
		    		  }
		    	  } 	    	  
		    	  
		    	  this.calculateCost();
		    	  
		      },
		      (err) => {
			        this.errors = err.data.errors;
			      }
	  );
  }
  
  calculateCost(){
	  console.log('Calculation goes here');
	  
	  let calculateJSON = {
			  'age': parseInt(this.calculationData.applicant.age)+1,
		      'fundCode': this.fundID,
		      'gender': this.calculationData.applicant.gender,
		      'deathOccCategory': this.calculationData.applicant.cover[0].occupationRating,
			  'tpdOccCategory': this.calculationData.applicant.cover[1].occupationRating,
			  'ipOccCategory': this.calculationData.applicant.cover[2].occupationRating,
		      'smoker': false,
		      'deathUnits': parseInt(this.calculationData.applicant.cover[0].additionalUnit),
		      'deathFixedAmount': parseInt(this.calculationData.applicant.cover[0].additionalCoverAmount),
		      'tpdUnits': parseInt(this.calculationData.applicant.cover[1].additionalUnit),
		      'tpdFixedAmount': parseInt(this.calculationData.applicant.cover[1].additionalCoverAmount),
		      'ipFixedAmount': parseInt(this.calculationData.applicant.cover[2].additionalCoverAmount),
		      'ipUnits': parseInt(this.calculationData.applicant.cover[2].additionalUnit),
		      'premiumFrequency': this.calculationData.applicant.cover[0].frequencyCostType,
		      'memberType': '',
		      'manageType': 'CCOVER',
		      'deathCoverType': this.calculationData.applicant.cover[0].coverCategory,
		      'tpdCoverType': this.calculationData.applicant.cover[1].coverCategory,
		      'ipCoverType': this.calculationData.applicant.cover[2].coverCategory,
		      'ipBenefitPeriod': this.calculationData.applicant.cover[2].additionalIpBenefitPeriod,
		      'ipWaitingPeriod': this.calculationData.applicant.cover[2].additionalIpWaitingPeriod,
		      'deathExistingAmount':0,
		      'tpdExistingAmount':0
		    };
	  
	  console.log(JSON.stringify(calculateJSON));
	    
	    this._CommonAPI.calculateAll(this.urlList.calculateUrl, calculateJSON).then(
	      (res) => {
	    	  
	    	  var premium = res;	    	  
	    	  
	    	  for(var i = 0; i < premium.length; i++){
				   if(premium[i].coverType === 'DcFixed' || premium[i].coverType === 'DcUnitised'){
					   this.calculationData.applicant.cover[0].additionalCoverAmount = premium[i].coverAmount;
					   this.calculationData.applicant.cover[0].cost = premium[i].cost || 0;
				   }else if(premium[i].coverType === 'TPDFixed' || premium[i].coverType === 'TPDUnitised'){
					   this.calculationData.applicant.cover[1].additionalCoverAmount = premium[i].coverAmount;
					   this.calculationData.applicant.cover[1].cost = premium[i].cost || 0;
				   }else if(premium[i].coverType === 'IpFixed' || premium[i].coverType === 'IpUnitised'){
					   this.calculationData.applicant.cover[2].additionalCoverAmount = premium[i].coverAmount||0.00;
					   this.calculationData.applicant.cover[2].cost = premium[i].cost || 0;
				   }
				}
	    	  
	    	  this.calculationData.totalMonthlyPremium = parseFloat(this.calculationData.applicant.cover[0].cost) + parseFloat(this.calculationData.applicant.cover[1].cost) + parseFloat(this.calculationData.applicant.cover[2].cost);
			   
	    	  console.log(this.calculationData.applicant.cover[0].additionalCoverAmount, this.calculationData.applicant.cover[1].additionalCoverAmount, this.calculationData.applicant.cover[2].additionalCoverAmount);
			  console.log(this.calculationData.applicant.cover[0].cost, this.calculationData.applicant.cover[1].cost, this.calculationData.applicant.cover[2].cost);
	        
	      },
	      (err) => {
	        this.errors = err.data.errors;
	      }
	    );
  }
  
checkPreviousMandatoryFields(currentField, formName){
	  
	  const _this = this;
	  
	  var currentFormFields = [];
	  
	  if(formName == 'financialForm'){
		  _this._$scope.userDetailsForm.$submitted = true;
	  }
	  
	  if(formName == 'ackForm'){
		  _this._$scope.userDetailsForm.$submitted = true;
		  _this._$scope.financialForm.$submitted = true;
	  }
	  
	  $("#"+formName).each(function(){
		  var allElements = $(this).find(':input');
		  for(var k = 0; k < allElements.length ; k++){
			  if(allElements[k].nodeName == 'INPUT' || allElements[k].nodeName == 'SELECT'){
			    	var fieldName = allElements[k].name;
			    	if(currentFormFields.indexOf(fieldName) == -1){
			    		currentFormFields.push(fieldName);
			    	}
			    }
		  }
	  });
	  
	  //console.log(currentFormFields);
	  
	  _this._$timeout(function(){
	  
		  var inx = currentFormFields.indexOf(currentField);
		  
		  if(inx > 0){		  
			  for(var i = 0; i < inx ; i++){
				  if(_this._$scope[formName][currentFormFields[i]]){
					  _this._$scope[formName][currentFormFields[i]].$touched = true;  
				  }			  
			  }		  
		  }
	  },100);
  }

	setOccupationRating(){
		  
		  switch(this.fundID) {
		  	case 'CARE':
		  		this.setOccupationRatingCARE();
		  		break;
		  	case 'SFPS':
		  		this.setOccupationRatingCommon().then((res) => {
		  			this.setOccupationRatingSFPS();
		  		});
		  		break;
		  	case 'HOST':
		  		this.setOccupationRatingCommon().then((res) => {
		  			this.setOccupationRatingHOST();
		  		});
		  		break;
		  	/*case 'VICT':
		  		this.setOccupationRatingCommon().then((res) => {
		  			this.checkCalculation();
		  		});
		  		break;*/
		  	case 'FIRS':
		  		this.setOccupationRatingCommon().then((res) => {
		  			this.checkCalculationFIRS();
		  		});
		  		break;
		  	case 'GUIL':
		  		this.setOccupationRatingCommon().then((res) => {
		  			this.setOccupationRatingGUIL();
		  		});
		  		break;
		  	default:
		  		break;
	    }
	  }
	
	setOccupationRatingCommon(){
		  
		  let deferred = this._$q.defer();
		 
		  if(this.calculationData.applicant.occupation !== undefined){
			  
			  var occName = this.calculationData.applicant.industryType + ':' + this.calculationData.applicant.occupation;
			  
			  this._CommonAPI.getOccupationRating(this.urlList.newOccupationUrl, this.fundID , occName).then(
					  (res) => {
						  this.calculationData.applicant.cover[0].occupationRating = res[0].deathfixedcategeory;
						  this.calculationData.applicant.cover[1].occupationRating = res[0].tpdfixedcategeory;
						  this.calculationData.applicant.cover[2].occupationRating = res[0].ipfixedcategeory;
						  
						  this.deathDBOccCategory =  res[0].deathfixedcategeory;
						  this.tpdDBOccCategory =  res[0].tpdfixedcategeory;
						  this.ipDBOccCategory =   res[0].ipfixedcategeory;	
						  
						  deferred.resolve({});
					  },
				      (err) => {
				          this.errors = err.data.errors;
				          deferred.reject(err);
				        }
			  );
		   }
		  
		  return deferred.promise;
	  }
	
	setOccupationRatingCARE(){
		  
		  this.annualSalForUpgradeVal = 100000;
		  var totalAnnualSalary = this.getTotalAnnualSalary();
		  
		  if(this.config.finalOccupationQuestions[0].answerText == "Yes"){
			  
			  this.calculationData.applicant.cover[0].occupationRating = 'Office';
			  this.calculationData.applicant.cover[1].occupationRating = 'Office';
			  this.calculationData.applicant.cover[2].occupationRating = 'Office';
			  
			  if(parseFloat(totalAnnualSalary) >= parseFloat(this.annualSalForUpgradeVal) && (this.config.finalOccupationQuestions[1].answerText == "Yes" || this.config.finalOccupationQuestions[2].answerText == "Yes")){
				  this.calculationData.applicant.cover[0].occupationRating = 'Professional';
				  this.calculationData.applicant.cover[1].occupationRating = 'Professional';
				  this.calculationData.applicant.cover[2].occupationRating = 'Professional';
			  } 
			  
		  }else{
			  this.calculationData.applicant.cover[0].occupationRating = 'General';
			  this.calculationData.applicant.cover[1].occupationRating = 'General';
			  this.calculationData.applicant.cover[2].occupationRating = 'General';
		  }
		  
		  this.checkCalculation();
		  
	  }
	
	checkCalculationFIRS(){
		  
		  const _this = this;
		  
		  _this.annualSalForUpgradeVal = 125000;
		  var totalAnnualSalary = _this.getTotalAnnualSalary();
		  
		  if(_this.calculationData.applicant.occupation){
			  
			  var selectedOcc = _this.occupationList.filter(function(obj){
		          return obj.occupationName == _this.calculationData.applicant.occupation;
		       });
			  
			  var selectedOccObj = selectedOcc[0];
			  
			  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // occupationDuties	
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = false; // spendTimeInside	
				 
				  
				  _this.config.finalOccupationQuestions[2].answerText = ''; 
				  
			  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // occupationDuties	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside	
				  
				  if(_this.config.finalOccupationQuestions[2].answerText == 'Yes'){
					  
					  _this.config.finalOccupationQuestions[0].showQuestion = true; // occupationDuties	
					  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
				  			  
				  }else{
					  
					  _this.config.finalOccupationQuestions[0].showQuestion = false; // occupationDuties	
					  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
					  
					  _this.config.finalOccupationQuestions[0].answerText = ''; 
					  _this.config.finalOccupationQuestions[1].answerText = '';			  
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // occupationDuties	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside	
				 
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = false; // workDuties	
				  _this.config.finalOccupationQuestions[3].showQuestion = false; // spendTimeOutside
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  _this.config.finalOccupationQuestions[2].answerText = ''; 
			  }
			  
			  if(_this.config.finalOccupationQuestions[2].answerText == 'Yes' && _this.deathDBOccCategory == 'Standard'){
				  _this.calculationData.applicant.cover[0].occupationRating = 'Low Risk';
				  _this.calculationData.applicant.cover[1].occupationRating = 'Low Risk';
				  _this.calculationData.applicant.cover[2].occupationRating = 'Low Risk';
			  }
			  
			  if(_this.deathDBOccCategory.toLowerCase() == 'low risk' && _this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
				  
				  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
				  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
			  }
			  
			 this.checkCalculation(); 
		  }
	  }
	
	setOccupationRatingGUIL(){
		  
		  const _this = this;
		  
		  _this.annualSalForUpgradeVal = 100000;
		  var totalAnnualSalary = _this.getTotalAnnualSalary();
		  
		  if(_this.calculationData.applicant.occupation){
			  
			  var selectedOcc = _this.occupationList.filter(function(obj){
		          return obj.occupationName == _this.calculationData.applicant.occupation;
		       });
			  
			  var selectedOccObj = selectedOcc[0];
			  
			  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // tertiaryQue
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // occupationDuties
				  
				  _this.config.finalOccupationQuestions[1].answerText = ''; 
				  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
				  }else{
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
				  }
				  
			  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // occupationDuties	
				  
				  if(_this.config.finalOccupationQuestions[1].answerText == 'No'){
					  
					  _this.config.finalOccupationQuestions[0].showQuestion = true; // tertiaryQue	
				  
					  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
						  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';					  
					  }else{
						  
						  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
						  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
						  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';					  
					  }				  
				  }else{
					  
					  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	  
					  
					  _this.config.finalOccupationQuestions[0].answerText = ''; 			
					  
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // occupationDuties
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  
				  if(_this.config.finalOccupationQuestions[1].answerText == 'No'){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';
				  }else{			
					  
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // occupationDuties	
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
			  }
			  
			  if(_this.config.initialOccupationQuestions[0].answerText == 'No'){
				  
				  switch(_this.deathDBOccCategory) {
				  
					  case 'White Collar':				  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
						  break;
						  
					  case 'Professional':					  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';
						  break;
					
					  default:
						  console.log('Default');
						  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
						  break;
				  }
				  
			  }
			  
			 this.checkCalculation(); 
		  }
	  }
	
	setOccupationRatingSFPS(){
		  
		  const _this = this;
		  
		  _this.annualSalForUpgradeVal = 100000;
		  var totalAnnualSalary = _this.getTotalAnnualSalary();
		  
		  if(_this.calculationData.applicant.occupation){
			  
			  var selectedOcc = _this.occupationList.filter(function(obj){
		          return obj.occupationName == _this.calculationData.applicant.occupation;
		       });
			  
			  var selectedOccObj = selectedOcc[0];
			  
			  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = false; // workDuties	
				  _this.config.finalOccupationQuestions[3].showQuestion = false; // spendTimeOutside
				  
				  _this.config.finalOccupationQuestions[2].answerText = ''; 
				  _this.config.finalOccupationQuestions[3].answerText = '';
				  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
				  }else{
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
				  }
				  
			  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // workDuties	
				  _this.config.finalOccupationQuestions[3].showQuestion = true; // spendTimeOutside
				  
				  if(_this.config.finalOccupationQuestions[2].answerText == 'No' && _this.config.finalOccupationQuestions[3].answerText == 'No'){
					  
					  _this.config.finalOccupationQuestions[0].showQuestion = true; // spendTimeInside	
					  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
				  
					  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
						  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';					  
					  }else{
						  
						  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
						  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
						  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';					  
					  }				  
				  }else{
					  
					  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
					  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
					  
					  _this.config.finalOccupationQuestions[0].answerText = ''; 
					  _this.config.finalOccupationQuestions[1].answerText = '';
					  
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // workDuties	
				  _this.config.finalOccupationQuestions[3].showQuestion = true; // spendTimeOutside
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  
				  if(_this.config.finalOccupationQuestions[2].answerText == 'No' && _this.config.finalOccupationQuestions[3].answerText == 'No'){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';
				  }else{			
					  
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // spendTimeInside	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = false; // workDuties	
				  _this.config.finalOccupationQuestions[3].showQuestion = false; // spendTimeOutside
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  _this.config.finalOccupationQuestions[2].answerText = ''; 
				  _this.config.finalOccupationQuestions[3].answerText = '';
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
			  }
			  
			  if(_this.config.initialOccupationQuestions[0].answerText == 'No'){
				  
				  switch(_this.deathDBOccCategory) {
				  
					  case 'White Collar':				  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
						  break;
						  
					  case 'Professional':					  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';
						  break;
					
					  default:
						  console.log('Default');
						  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
						  break;
				  }
				  
			  }
			  
			 this.checkCalculation(); 
		  }
	  }
	
	setOccupationRatingHOST(){
		  
		  const _this = this;
		  
		  _this.annualSalForUpgradeVal = 150000;
		  var totalAnnualSalary = _this.getTotalAnnualSalary();
		  
		  if(_this.calculationData.applicant.occupation){
			  
			  var selectedOcc = _this.occupationList.filter(function(obj){
		          return obj.occupationName == _this.calculationData.applicant.occupation;
		       });
			  
			  var selectedOccObj = selectedOcc[0];
			  
			  if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // workDuties
				  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside		
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  
				  if(_this.config.finalOccupationQuestions[1].answerText == 'Yes' && _this.config.finalOccupationQuestions[2].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
					  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';				  
				  }else{
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
				  }
				  
			  }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // workDuties
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside			  
				  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'No' && _this.config.finalOccupationQuestions[2].answerText == 'Yes'){
					  
					  _this.config.finalOccupationQuestions[1].showQuestion = true; // tertiaryQue	
				  
					  if(_this.config.finalOccupationQuestions[0].answerText == 'Yes' && _this.config.finalOccupationQuestions[1].answerText == 'Yes' && parseFloat(totalAnnualSalary) >= parseFloat(_this.annualSalForUpgradeVal)){
						  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';					  
					  }else{
						  
						  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
						  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
						  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';					  
					  }				  
				  }else{
					  
					  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue
					  
					  _this.config.finalOccupationQuestions[1].answerText = '';
					  
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;				  
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = true; // workDuties
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = true; // spendTimeInside	
				  
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  
				  if(_this.config.finalOccupationQuestions[0].answerText == 'No' && _this.config.finalOccupationQuestions[2].answerText == 'Yes'){
					  
					  _this.calculationData.applicant.cover[0].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[1].occupationRating = 'White Collar';
					  _this.calculationData.applicant.cover[2].occupationRating = 'White Collar';
				  }else{			
					  
					  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
					  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
					  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
				  }
				  
			  } else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
				  
				  _this.config.finalOccupationQuestions[0].showQuestion = false; // workDuties	
				  _this.config.finalOccupationQuestions[1].showQuestion = false; // tertiaryQue	
				  _this.config.finalOccupationQuestions[2].showQuestion = false; // spendTimeInside	
				  
				  _this.config.finalOccupationQuestions[0].answerText = ''; 
				  _this.config.finalOccupationQuestions[1].answerText = '';
				  _this.config.finalOccupationQuestions[2].answerText = ''; 
				  
				  _this.calculationData.applicant.cover[0].occupationRating = _this.deathDBOccCategory;
				  _this.calculationData.applicant.cover[1].occupationRating = _this.tpdDBOccCategory;
				  _this.calculationData.applicant.cover[2].occupationRating = _this.ipDBOccCategory;
			  }
			  
			  if(_this.config.initialOccupationQuestions[0].answerText == 'No'){
				  
				  switch(_this.deathDBOccCategory) {
				  
					  case 'White Collar':				  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
						  break;
						  
					  case 'Professional':					  
						  _this.calculationData.applicant.cover[0].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Professional';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Professional';
						  break;
					
					  default:
						  console.log('Default');
						  _this.calculationData.applicant.cover[0].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[1].occupationRating = 'Standard';
						  _this.calculationData.applicant.cover[2].occupationRating = 'Standard';
						  break;
				  }
				  
			  }
			  
			 this.checkCalculation(); 
		  }
	  }
	
	print(){
		  
		  const _this = this;
		  
		  //initial Occupation Questions
		  var initialOccupationQuestions = [];
		  angular.forEach(_this.config.initialOccupationQuestions, function (obj, index) {
			  if(obj.answerText == 'Yes' || obj.answerText == 'No'){
				  var currentObj = {};
				  currentObj.answerText = obj.answerText;
				  currentObj.questiontext = obj.labelText.toString();
				  initialOccupationQuestions.push(currentObj);
			  }
			  
		  });
		  
		  //final Occupation Questions
		  var finalOccupationQuestions = [];
		  angular.forEach(_this.config.finalOccupationQuestions, function (obj, index) {
			  if(obj.answerText == 'Yes' || obj.answerText == 'No'){
				  var currentObj = {};
				  currentObj.answerText = obj.answerText;
				  currentObj.questiontext = obj.labelText.toString();
				  finalOccupationQuestions.push(currentObj);
			  }
		  });
		  
		  _this.calculationData.initialOccupationQuestions = initialOccupationQuestions;
		  _this.calculationData.finalOccupationQuestions = finalOccupationQuestions;
		  
		  //occupation code
		  var industryType = _this.calculationData.applicant.industryType;		  
		  var selectedIndustry = _this.IndustryOptions.filter(function(obj){
	          return  industryType === obj.key;
	      });		  
		  _this.calculationData.applicant.occupationCode = selectedIndustry[0].value;
		  
		  //disclaimer Text		  
		  _this.calculationData.disclaimerText = _this.config.disclaimerText.toString();
		  
		  //Child List		  
		  _this.calculationData.applicant.childList = _this.childList;
		  
		  //child List PDF
		  var childListPDF = [];
		  angular.forEach(_this.childList, function (obj, index) {
			  var currentObj = {};
			  currentObj.questiontext = 'Child ' + parseInt(index+1) +' Age';
			  currentObj.answerText = obj.age + ' Years';
			  childListPDF.push(currentObj);
		  });
		  _this.calculationData.applicant.childListPDF = childListPDF;
		  
		  //Rule List
		  _this.calculationData.insuranceRuleDetails = _this.rules;
		  
		  console.log(JSON.stringify(_this.calculationData));
		  
		  _this._CommonAPI.printEapply(this.urlList.printQuotePageNew, _this.calculationData).then(
			      (res) => {
			    	  _this.downloadPDF(res.clientPDFLocation);
			    	  console.log(res);
			      },
			      (err) => {
			    	  _this.errors = err.data.errors;
			      }
		   );
	}
	
	downloadPDF(pdfLocation){
	  	var filename = null;
	  	var a = null;
	  	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
	  	a = document.createElement('a');
	  	document.body.appendChild(a);
	  	
	  	this._CommonAPI.downloadPDF(this.urlList.downloadUrl, pdfLocation).then(
				  (res) => {
					  if(navigator.appVersion.toString().indexOf('.NET') > 0) {
						  window.navigator.msSaveBlob(res.response,filename);
					  }else{
						  var fileURL = URL.createObjectURL(res.response);
						  a.href = fileURL;
						  a.download = filename;
						  a.click();
					  }
				  }, 
	            (err) => {
	              console.log('Error while Downloading the PDF' + err);
	            }
		  );
	  	
	}
	
	roundOff(fieldName){
		
		var fixedvalue;
		
		switch(fieldName) {
		  
	  	case 'inflationRate':
	  		if(this.rules.inflationRate){
	  			fixedvalue = parseFloat(this.rules.inflationRate).toFixed(2);
	  			this.rules.inflationRate = parseFloat(fixedvalue);
	  		}
	  		break;
	  	case 'realInterestRate':
	  		if(this.rules.realInterestRate){
	  			fixedvalue = parseFloat(this.rules.realInterestRate).toFixed(2);
	  			this.rules.realInterestRate = parseFloat(fixedvalue);
	  		}
	  		break;
	  	case 'superContribRate':
	  		if(this.rules.superContribRate){
	  			fixedvalue = parseFloat(this.rules.superContribRate).toFixed(2);
	  			this.rules.superContribRate = parseFloat(fixedvalue);
	  		}
	  		break;
	  	case 'ipBenefitPaidToSA':
	  		if(this.rules.ipBenefitPaidToSA){
	  			fixedvalue = parseFloat(this.rules.ipBenefitPaidToSA).toFixed(2);
	  			this.rules.ipBenefitPaidToSA = parseFloat(fixedvalue);
	  		}
	  		break;
	  	case 'ipReplaceRatio':
	  		if(this.rules.ipReplaceRatio){
	  			fixedvalue = parseFloat(this.rules.ipReplaceRatio).toFixed(2);
	  			this.rules.ipReplaceRatio = parseFloat(fixedvalue);
	  		}
	  		break;
	  	case 'dependentSuppAge':
	  		if(this.rules.dependentSuppAge){
	  			fixedvalue = Math.round(this.rules.dependentSuppAge);
	  			this.rules.dependentSuppAge = fixedvalue;
	  		}
	  		break;
	  	case 'retirementAge':
	  		if(this.rules.retirementAge){
		  		fixedvalue = Math.round(this.rules.retirementAge);
		  		this.rules.retirementAge = fixedvalue;
	  		}
	  		break;
	  	default:
	  		break;
	  }

	}
	
	clickToOpen(hhText) {
		  this._ngDialog.open({
		      template: '<div class="p-5"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel">INSURANCE CALCULATOR - DISCLAIMER</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn submit-button text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
		      className: 'ngdialog-theme-plain',
		      showClose: false,
		      plain: true
		    });
	  }
  
}

export default CoverCtrl;
