(function(angular){
'use strict';

angular
    .module('Metlife.guil.uicomponentsheader', ['guil.ui.constants'])
    .directive('metlifeGuilHeaderNav', metlifeGuilHeaderNav);

function metlifeGuilHeaderNav(guilUIConstants) {
    var directive = {
        restrict: 'EA',
        templateUrl: guilUIConstants.path.uiComponents + 'metlife-guil-header-nav/metlife-guil-header-nav.tmpl.html',
        controller: headerNavCtrl,
        controllerAs: 'self',
        bindToController: true
    };

    return directive;
}

headerNavCtrl.$inject = ['$scope', 'guilUIConstants','$location','persoanlDetailService','$window','ngDialog'];

function headerNavCtrl($scope, guilUIConstants, $location, persoanlDetailService, $window, ngDialog) {
    var self = this;
    
    self.constants = {
    		aboutUsUrl: guilUIConstants.navUrls.aboutUsUrl,
    		contactUsUrl: guilUIConstants.navUrls.contactUsUrl,
    		logoImgPath: guilUIConstants.path.images + 'logo.png',
    };
    
    self.init = function(){
		
		var url = $location.absUrl().split('?')[0];
		url = url.split('#/')[1];
		self.isSessionExpired = false;
		
		if(url === '' || url === 'landingpage'){
			self.isHomePage = true;
		}else{
			self.isHomePage = false;
		}
		
		if(url === 'sessionTimeOut'){
			self.isHomePage = true;
			self.isSessionExpired = true;
		}
		
		self.userDetails = persoanlDetailService.getMemberDetails();
	};
	
	self.init();
self.goToHome = function(){
    	
    	ngDialog.openConfirm({
          template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row"><div class="col-12"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer"><div class="ngdialog-buttons text-center"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button></div></div>',
          plain: true,
          showClose: false,
          className: 'ngdialog-theme-plain custom-width'
        }).then(function(value) {        	
        	self.go('/landing');
        },function(value) {
        	if(value === 'oncancel') {
                return false;
              }
        });
      
    }
    self.go = function(path){
    	 $location.path(path);
    };
    
    self.logout = function(path){
   	 //window.location = sessionStorage.loginPath;
   	 $window.close();
   }
}

})(angular);