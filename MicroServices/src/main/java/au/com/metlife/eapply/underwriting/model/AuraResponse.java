package au.com.metlife.eapply.underwriting.model;

import java.io.Serializable;
import java.util.List;

public class AuraResponse implements Serializable{
	
private String auraID;
private int questionID;
private List<String>  auraAnswers;

public String getAuraID() {
	return auraID;
}
public void setAuraID(String auraID) {
	this.auraID = auraID;
}
public int getQuestionID() {
	return questionID;
}
public void setQuestionID(int questionID) {
	this.questionID = questionID;
}
public List<String> getAuraAnswers() {
	return auraAnswers;
}
public void setAuraAnswers(List<String> auraAnswers) {
	this.auraAnswers = auraAnswers;
}

}
