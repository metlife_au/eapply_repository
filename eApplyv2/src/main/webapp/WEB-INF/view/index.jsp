<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="eapplymain" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="../../eapply/static/care/images/favicon.ico">
<title>CareSuper</title>

<!-- Bootstrap core CSS -->
<link href="../../eapply/static/care/css/bootstrap.min.css" rel="stylesheet">
<link href="../../eapply/static/care/css/bootstrap-select.css" rel="stylesheet">
<link href="../../eapply/static/care/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="../../eapply/static/care/css/datepicker.css" />


<!-- Custom styles for this template -->
<link href="../../eapply/static/care/css/custom.css" rel="stylesheet">
<link href="../../eapply/static/care/css/ngDialog.css" rel="stylesheet">
<link rel="stylesheet" href="../../eapply/static/care/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="../../eapply/static/care/css/ngDialog-theme-plain.css">

<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TSZZFQH');</script>
<!-- End Google Tag Manager -->
		
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../eapply/static/care/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.../../eapply/static/care/js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var baseUrl = '<%= domainName%>';
</script>


<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSZZFQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div ng-view autoscroll="true"></div>
<div id="body-mask" data-loading>
	<div class="body-mask-loader"></div>
</div>
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../../eapply/resources/js/lib/angular.min.js"></script>
<script src="../../eapply/resources/js/lib/angular-idle.min.js"></script>
<script src="../../eapply/static/care/js/angular-route.js"></script>
<script src="../../eapply/static/care/js/angular-resource.min.js"></script>
<script src="../../eapply/static/care/js/jquery.min.js"></script> 
<!--  <script src="../../eapply/static/care/js/jquery-ui.js"></script> -->
<script src="../../eapply/static/care/js/bootstrap.min.js"></script> 
<script src="../../eapply/static/care/js/docs.min.js"></script>
<script src="../../eapply/static/care/js/ng-file-upload-shim.js"></script>
<script src="../../eapply/static/care/js/ng-file-upload.js"></script>
 <script src="../../eapply/static/care/js/ngDialog.js"></script> 
 <script src="../../eapply/static/host/js/ui-bootstrap-tpls-2.5.0.min.js"></script>

<script src="../../eapply/static/common/services/sessionTimeoutService.js"></script>
<script src="../../eapply/static/common/services/exceptionHandler.js"></script>
<script src="../../eapply/static/common/services/google-analytics-service.js"></script>
<script src="../../eapply/static/common/services/eApplyInterceptor.js"></script>
<script src="../../eapply/static/care/js/angular-sanitize.js"></script>
<script src="../../eapply/static/care/js/app.js"></script>

<!-- CareSuperApp application file -->


<!-- <script src="https://assets.adobedtm.com/7bd378b63ae642286f5e272876265df98dfca74c/satelliteLib-9d49fffd8e96ae995aa8e242a15f1acf1428ecc8-staging.js"></script>
 -->
<!-- CareSuperApp controllers -->
<script type="text/javascript" src="../../eapply/static/care/js/controllers/aura.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/auraOcc.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/auraSpecialOffer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/auraTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/decision.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/landing.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/login.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/newMemberQuote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/quote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/quoteCancel.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/quoteOccUpdate.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/quoteTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/retrieveSavedApp.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/summary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/newMemberSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/workRatingSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/transferSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/DecisionController.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/cancelConfirmation.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/controllers/timeOutController.ctrl.js"></script>

<script type="text/javascript" src="../../eapply/static/care/js/moment.js"></script> 
<script type="text/javascript" src="../../eapply/static/care/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="../../eapply/static/care/js/bootstrap-select.js"></script> 

<!-- CareSuperApp Services -->
<script type="text/javascript" src="../../eapply/static/care/js/services/service.js"></script>

<!-- CareSuperApp utilities -->
<script type="text/javascript" src="../../eapply/static/care/js/utils/careSuperApp.filters.js"></script>
<script type="text/javascript" src="../../eapply/static/care/js/utils/directives.js"></script>

<script src="../../eapply/static/care/js/custom.js"></script> 
<script src="../../eapply/static/care/js/validation.js"></script> 
<!--<script type="text/javascript">_satellite.pageBottom();</script> -->

<!-- Google Analytics code snippet -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60148568-9', 'auto');

</script>

<script>
        var clientData;
        var applicantData = {
            clientRefNumber: null,
            age: null,
            gender: null,
            partnerID: null,
            app_number: null,
            dateOfBirth: null
        };
        window['adrum-start-time'] = new Date().getTime();
        window['adrum-config'] = {
          userEventInfo: {
            "PageView": function(context) {
                return {
                    userPageName: window.location.hash,
                    userData: {
                        partner_id: applicantData.partnerID.toString(),
                        member_number: applicantData.clientRefNumber.toString(),
                        gender: applicantData.gender,
                        //dateOfBirth: applicantData.dateOfBirth.toString()
                    }
                }
            },
            "VPageView": function(context) {
                return {
                    userPageName: window.location.hash,
                    userData: {
                        partner_id: applicantData.partnerID.toString(),
                        member_number: applicantData.clientRefNumber.toString(),
                        gender: applicantData.gender,
                        //dateOfBirth: applicantData.dateOfBirth.toString(),
                        app_number: applicantData.app_number.toString(),
                        age: applicantData.age.toString()
                    }
                }
            }
          }
        };
        (function(config){
            if(baseUrl === 'https://www.eapplication.metlife.com.au/'){
                config.appKey = 'EUM-AAB-AUR';
                config.beaconUrlHttp = 'https://eum.metlife.com.au';
                config.beaconUrlHttps = 'https://eum.metlife.com.au';
            } else{
                config.appKey = 'EUM-AAB-AVB';
                config.beaconUrlHttp = 'https://uat.eum.metlife.com.au';
                config.beaconUrlHttps = 'https://uat.eum.metlife.com.au';
            }
            config.adrumExtUrlHttp = 'http://cdn.appdynamics.com';
            config.adrumExtUrlHttps = 'https://cdn.appdynamics.com';
            config.xd = {enable : true};
        })(window['adrum-config'] || (window['adrum-config'] = {}));
        if ('https:' === document.location.protocol) {
        var s = document.createElement( 'script' );
            s.setAttribute( 'src', 'https://cdn.appdynamics.com/adrum/adrum-4.4.2.452.js' );
            document.body.appendChild( s );


        } else {
        var s = document.createElement( 'script' );
            s.setAttribute( 'src', 'http://cdn.appdynamics.com/adrum/adrum-4.4.2.452.js' );
            document.body.appendChild( s );
        }
        </script>

</body>
</html>