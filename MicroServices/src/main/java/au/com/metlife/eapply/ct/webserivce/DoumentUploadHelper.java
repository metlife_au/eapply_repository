package au.com.metlife.eapply.ct.webserivce;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.ct.exception.MetlifeException;
import au.com.metlife.eapply.ct.model.UserInfoBO;
import au.com.metlife.webservices.PicsLodgementStub;

public class DoumentUploadHelper {
	private DoumentUploadHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(DoumentUploadHelper.class);
	static PicsLodgementStub.ReturnObject retObj = null;
	 
/*	public static void main(String[] args) {		
		try {
			userInfo = new UserInfoBO();
			userInfo.setFundType("SFPS");
			//userInfo.setId("345678");
			userInfo.setFirstname("Monica Louise");
			userInfo.setLastname("BATES");
			//userInfo.setTitle("Mr");
			userInfo.setGender("female");
			userInfo.setDateOfBirth(new Date("26/04/1986"));
			userInfo.setClientRefNumber("9970");
			//userInfo.setDateJoinedFund(new Date());
			//userInfo.setAdditionalInfo("test");
			userInfo.setApplicationId1("16660");
			
			DocumentDto claimDocument=new DocumentDto();
			Vector claimDocumentVector = new Vector();
			claimDocument.setDocumentlocation("/shared/eLodg/4485801_25022016_173948.pdf");
			claimDocument.setDocumentCode("Birth Certificate");
			
			claimDocument.setDocumentStatus("Pending");
			claimDocumentVector.add(claimDocument);
			userInfo.setElodgeDocument(claimDocumentVector);
			//retObj = doLodgeNewClaims(userInfo,"","","Monica Louise BATES");eNewClaims(userInfo, email, phoneNumber, fullName)
			retObj = doLodgeExistingClaims(userInfo, "", "", "", "GL", "");
		} catch (MetlifeException e) {
			e.printStackTrace();
		}
	}
*/	
	/**
     * Description: This method call the web servise method for craeting claims
     * 
     * @param au.com.metlife.model.UserInfoBO userInfo
     * 
     * @return  PicsLodgementStub.ReturnObject retObj
     * @throws MetlifeException
     */
/*	public  static PicsLodgementStub.ReturnObject  doLodgeNewClaims(UserInfoBO userInfo,String email,String phoneNumber,String fullName)throws MetlifeException {
		final String METHOD_NAME = "doLodgeNewClaims"; //$NON-NLS-1$
		
		PicsLodgementStub.LodgeNewClaims lodgeNewClaims = new PicsLodgementStub.LodgeNewClaims();
		
				
		lodgeNewClaims.setMetaData(ClaimRequestMapper
				.doMapForLodgeNewClaims(userInfo,email,phoneNumber,fullName));
       
       
		ELodgementResponseObjects res = new ELodgementResponseObjects();
		 PicsLodgementStub.ReturnObject retObj=null;
		try{
				
		 retObj = res.execLodgeNewClaims(lodgeNewClaims); 
		}catch (MetlifeException e) {
			
			
			throw e;
		}
		
		//PerformanceLog.logExitTime(PACKAGE_NAME, CLASS_NAME, METHOD_NAME, System.currentTimeMillis());
		return retObj;

	}
*/	
	public static PicsLodgementStub.ReturnObject doLodgeExistingClaims(UserInfoBO userInfo,String email,String phoneNumber,String fullName,String productType, String randomNum,String clientConfig,String userName,String password,String picsUrl)throws MetlifeException {
		log.info("Do lodge existing claims start");
		PicsLodgementStub.LodgeExistingClaims lodgeExistingClaims = new PicsLodgementStub.LodgeExistingClaims();

		lodgeExistingClaims.setMetaData(ClaimRequestMapper
				.doMapForLodgeExistingClaims(userInfo,email,phoneNumber,fullName,productType,randomNum));
   
		ELodgementResponseObjects res = new ELodgementResponseObjects();
		PicsLodgementStub.ReturnObject retobj = null;
		try{
		
			retobj= res.execLodgeExistingClaims(lodgeExistingClaims,userName,password,clientConfig,picsUrl);
		}catch (MetlifeException e) {			
			throw e;
		}
		log.info("Do lodge existing claims finish");
		return retobj;
	}

}
