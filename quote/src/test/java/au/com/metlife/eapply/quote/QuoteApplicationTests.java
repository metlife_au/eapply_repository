package au.com.metlife.eapply.quote;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class QuoteApplicationTests {
	
	 @Autowired
	 private MockMvc mockMvc;

	@Test
	public void contextLoads() {
	}
	
//	@Test
//	public void testPostAPI() throws Exception{
//		
//		this.mockMvc.perform(post("/caluclateDeath").content(
//				"{ \"age\": \"20\"}"));
//		//.andExpect(status().isCreated());
//		
//	}
	
	@Test
	public void testRestAPI2(){
        //String input = "{ \"id\":1234 ,\"fname\": \"purna\",\"lname\": \"chandra\"}";CCOVER
       // String input = "{ \"age\": 30}";
        String input = "{\"age\":20,\"fundCode\":\"CARE\",\"gender\":\"Male\",\"deathOccCategory\":\"Professional\",\"tpdOccCategory\":\"Professional\",\"ipOccCategory\":\"Professional\",\"smoker\":false,\"deathUnits\":0,\"deathFixedAmount\":100000,\"deathFixedCost\":null,\"deathUnitsCost\":null,\"tpdUnits\":0,\"tpFixedAmount\":100000,\"tpFixedCost\":null,\"tpUnitsCost\":null,\"ipUnits\":10,\"ipFixedAmount\":0,\"ipFixedCost\":null,\"ipUnitsCost\":null,\"premiumFrequency\":\"Weekly\",\"memberType\":null,\"manageType\":\"CCOVER\",\"deathCoverType\":\"DcFixed\",\"tpdCoverType\":\"TpdFixed\",\"ipCoverType\":\"IpUnitised\",\"ipWaitingPeriod\":\"30 Days\",\"ipBenefitPeriod\":\"2 Years\"}";                
//		 String input = "{ \"fundCode\": CARE}";
        URL obj;
        try {
        obj = new URL("http://localhost:8086/calculateAll");
        //obj = new URL("http://10.173.62.192:8083/SimleJavaProject/service/aura2");
//        obj = new URL("http://localhost:8086/getIndustryList");
        
        HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setUseCaches(false);
        httpCon.setRequestProperty( "Content-Type", "application/json" );
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestMethod("POST");
//            httpCon.setRequestMethod("GET");
        httpCon.connect(); 
        OutputStream os = httpCon.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
        osw.write(input);
        osw.flush();
        osw.close();
        
        int responseCode = httpCon.getResponseCode();
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(httpCon.getInputStream()));
        String inputLine;
        StringBuffer responseBuff = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
                        System.out.println(inputLine);
                        responseBuff.append(inputLine);
        }
        in.close();
        
        
        } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
        } catch (ProtocolException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
        } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
        }
}



}
