package au.com.metlife.eapply.corporate.business;

import java.util.Arrays;

import au.com.metlife.eapply.business.entity.CorpPartner;
import au.com.metlife.eapply.business.entity.CorpMember;
import au.com.metlife.eapply.common.ResponseLink;

public class CorpEntityRequest {

	
	private CorpMember member;
	private CorpPartner partner;
	private ResponseLink[] links;
	public CorpMember getMember() {
		return member;
	}
	public void setMember(CorpMember member) {
		this.member = member;
	}
	public CorpPartner getPartner() {
		return partner;
	}
	public void setPartner(CorpPartner partner) {
		this.partner = partner;
	}
	public ResponseLink[] getLinks() {
		return links;
	}
	public void setLinks(ResponseLink[] links) {
		this.links = links;
	}
	@Override
	public String toString() {
		return "CorpEntityRequest [member=" + member + ", partner=" + partner + ", links=" + Arrays.toString(links)
				+ "]";
	}
	
	
	
	
	
}
