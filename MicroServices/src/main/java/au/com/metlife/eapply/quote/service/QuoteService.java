package au.com.metlife.eapply.quote.service;

import java.util.List;

import au.com.metlife.eapply.quote.model.ConvertModel;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;
import au.com.metlife.eapply.quote.model.SelectItem;

public interface QuoteService {
	
	QuoteResponse calculateDeath(RuleModel ruleModel);
	
	QuoteResponse calculateTpd(RuleModel ruleModel);
	
	QuoteResponse calculateIp(RuleModel ruleModel);
	
	QuoteResponse calculateFUL(RuleModel ruleModel);
	
	//Added by 561132 for INGD
	QuoteResponse calculateINGDDeath(RuleModel ruleModel);
	
	QuoteResponse calculateINGDTpd(RuleModel ruleModel);
	
	QuoteResponse calculateINGDIp(RuleModel ruleModel);
	
	List<SelectItem> getIndustryList(String fundCode);
	
	InstProductRuleBO getProductConfig(String fundCode,String memberType,String manageType);
	
	ConvertModel convertDeathFixedToUnits(RuleModel ruleModel);
	
	ConvertModel convertTpdFixedToUnits(RuleModel ruleModel);
	//AAL logic
	QuoteResponse calculateAAL(RuleModel ruleModel);
	//For INGD Life Stage Cover Amount
	QuoteResponse calculateLifeStageDeathCoverAmount(String fundCode,String age);

	QuoteResponse calculateLifeStageTPDCoverAmount(String fundCode,String age);
		//Added by 561132 to get the INGD Aura Type
	String getAuraType(RuleModel ruleModel);
}
