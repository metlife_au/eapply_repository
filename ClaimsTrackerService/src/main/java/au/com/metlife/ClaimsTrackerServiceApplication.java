package au.com.metlife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaimsTrackerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimsTrackerServiceApplication.class, args);
	}
}
