/*Login Page Controller Starts*/  
CareSuperApp.controller('login',['$scope','$routeParams', '$location','$window', 'getclientData', 'tokenNumService','deathCoverService','tpdCoverService',
                                 'ipCoverService','newMemberOfferService', 'persoanlDetailService', '$http', '$window','appNumberService',
                                 'auraInputService', 'PersistenceService','ngDialog', 'CheckSavedAppService', 'CancelSavedAppService','$timeout','SessionTimeoutService','urlService','urls',
                                 function($scope, $routeParams, $location,$window, getclientData, tokenNumService, deathCoverService, tpdCoverService,
                                		 ipCoverService, newMemberOfferService, persoanlDetailService, $http, $window,appNumberService,auraInputService,
                                		 PersistenceService, ngDialog, CheckSavedAppService, CancelSavedAppService,$timeout,SessionTimeoutService,urlService,urls){
	
	//$rootScope.urlList = urls.data;
	urlService.setUrlList(urls.data);
	$scope.urlList = urlService.getUrlList();
	//Added to fix defect 169682: Fixed is not displayed, if cover is passed in fixed
  	$scope.isEmpty = function(value){
  		return ((value == "" || value == null) || value == "0");
  	};
  	
 // added for session expiry
  	SessionTimeoutService.stopWatch();
  	SessionTimeoutService.setup();
	$scope.$on('$destroy', function() {
		SessionTimeoutService.stopWatch();
		if(!$scope.$$phase){
			$scope.$apply();
		}
	});
	
     
	$scope.message = 'Look! I am an about page.';
	var clientRefNum;
	console.log(inputData)
    tokenNumService.setTokenId(inputData)     
    //$location.search( 'InputData', null );
      getclientData.requestObj($scope.urlList.clientDataUrl).then(function(response) {    	
    	$scope.partnerRequest = response.data;
    	$scope.calcEncrpytURL = $scope.partnerRequest.calcEncrpytURL;
    	$scope.applicantlist = $scope.partnerRequest.applicant;
    	if($scope.applicantlist[0] && $scope.applicantlist[0].clientRefNumber){
    		clientRefNum = $scope.applicantlist[0].clientRefNumber;
    	}
    	angular.forEach($scope.applicantlist, function (applicant) {        		
    		$scope.personalDetails = applicant.personalDetails          	
    		$scope.coverList = applicant.existingCovers;         		
    		var age = moment().diff(moment(applicant.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years') + 1;  
    		$scope.maxAge=age;
    		$scope.newMemberOffer = false;
    		/*var date = moment().diff(moment(applicant.welcomeLetterDate, 'DD-MM-YYYY'), 'days')+1;
    		console.log("date"+date);*/
    		if(moment().diff(moment(applicant.welcomeLetterDate, 'DD-MM-YYYY'), 'days') +1<91 && moment(moment(moment(applicant.welcomeLetterDate,'DD-MM-YYYY')).format('MM-DD-YYYY')).isAfter('01-31-2017', 'day') && age<61 && (applicant.memberType=='Industry' || applicant.memberType=='Corporate')){
    			$scope.newMemberOffer = true;
    		}else{
    			$scope.newMemberOffer = false;
    		}
    		newMemberOfferService.setNewMemberOfferFlag($scope.newMemberOffer);        		
    		angular.forEach($scope.coverList, function (cover) {            		
        		angular.forEach(cover, function(value, key) {
        			$scope.benefitType = value.benefitType;            			
        			 if($scope.benefitType =='1'){
        				 $scope.deathCover = value;
        				 deathCoverService.setDeathCover($scope.deathCover)            				          				 
        			 } else if($scope.benefitType =='2'){
        				 $scope.tpdCover = value;     
        				 tpdCoverService.setTpdCover($scope.tpdCover)            				 
        			 }else  if($scope.benefitType =='4'){
        				 $scope.ipCover = value;   
        				 ipCoverService.setIpCover( $scope.ipCover)            				
        			 }
        			});
        		
        	});
    		persoanlDetailService.setMemberDetails($scope.applicantlist);
    	});
    });
    $scope.go = function (path, generateAppnum, num) {
    	if(generateAppnum){
    		//generate app number
    		appNumberService.requestObj($scope.urlList.appNumUrl).then(function(response){
        		PersistenceService.setAppNumber(response.data);
        		$location.path(path);
        	 });
    	} else{
    		PersistenceService.setAppNumber(num);
    		$location.path(path);
    	}
  	};
  	$scope.tokenid = tokenNumService.getTokenId();
    $scope.openWindow = function(url) {
        $window.open(url);
    }
    
    // added for new member popup on landing page
    $scope.showNewMemberPopUp = function(val){	
   		if(val == null || val == "" || val == " "){
   			hideTips();
   		}else{
   		 document.getElementById('mymodalNewMem').style.display = 'block';
   		 document.getElementById('mymodalNewMemFade').style.display = 'block';
   		 document.getElementById('newMember_text').innerHTML=val;	
   		}
   	}
    $scope.hideTips =function(){
   		if(document.getElementById('help_div')){
   			document.getElementById('help_div').style.display = "none";
   		}						
   	};
   	
   	$scope.showSavedAppPopup = function(manageType){
   		CheckSavedAppService.checkSavedApps($scope.urlList.savedAppUrl,"METL",clientRefNum,manageType).then(function(res){
   		//CheckSavedAppService.checkSavedApps({fundCode:"CARE", clientRefNo:clientRefNum, manageType:manageType}, function(res){
   			//$scope.apps = res;
   			$scope.apps = res.data;
   			if($scope.apps.length > 0){
	   			var newScope = $scope.$new();
	   			for(var i = 0; i < $scope.apps.length; i++){
	   				var tempDate = new Date($scope.apps[i].createdDate);
	   				$scope.apps[i].createdDate = moment(tempDate).format('DD/MM/YYYY');
	   				
	   				if($scope.apps[i].requestType == "CCOVER"){
	   					$scope.apps[i].requestType = "Change Cover";
	   				} else if($scope.apps[i].requestType == "TCOVER"){
	   					$scope.apps[i].requestType = "Transfer Cover";
	   				} else if($scope.apps[i].requestType == "UWCOVER"){
	   					$scope.apps[i].requestType = "Update Work Rating";
	   				} else if($scope.apps[i].requestType == "NCOVER"){
	   					$scope.apps[i].requestType = "New Member Cover";
	   				}
	   			}
	   			// Created a new scope for the modal popup as the parent controller variables are not accessible to the modal
	   			newScope.apps = $scope.apps;
	   			
	   			ngDialog.openConfirm({
	   	            template:'<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title">You have previously saved application(s).</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-12"><p> Please continue with the saved application or cancel saved application to start a new application. </p> </div> </div><!-- Row ends --> <div class="row visible-xs"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th width="20%" class="coverbg"><h5 class=" ">Application no.</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Date saved</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div></div> <div class="row hidden-xs"><div class="col-sm-12"> <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grid"><tbody><tr class=""><th width="20%" class="coverbg"><h5 class=" ">Application no.</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Date saved</h5></th><th width="20%" class="coverbg     "><h5 class=" ">Service request type</h5> </th></tr> <tr ng-repeat="app in apps"><td>{{app.applicationNumber}}</td><td>{{app.createdDate}}</td><td>{{app.requestType}}</td></tr></tbody> </table></div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div></div>',
	   	            plain: true,
	   	            className: 'ngdialog-theme-plain custom-width',
	   	            scope: newScope
	   	        }).then(function (value) {
	   	        	PersistenceService.setAppNumToBeRetrieved($scope.apps[0].applicationNumber);
	   	        	//$scope.go('/quote/3');
	   	        	switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
		   	     	case "quotepage":
		   	     		$scope.go('/quote/3', false, $scope.apps[0].applicationNumber);
		   	     		break;
		   	     	case "aurapage":
		   	     		$scope.go('/aura/3', false, $scope.apps[0].applicationNumber);
		   	     		break;
		   	     	case "summarypage":
		   	     		$scope.go('/summary/3', false, $scope.apps[0].applicationNumber);
		   	     		break;
		   	     	default:
		   	     		break;
		   	     	}
	   	        }, function (value) {
	   	        	if(value == 'oncancel'){
	   	        		CancelSavedAppService.cancelSavedApps($scope.urlList.cancelAppUrl,$scope.apps[0].applicationNumber).then(function(result){
	   	        		//CancelSavedAppService.cancelSavedApps({applicationNumber:$scope.apps[0].applicationNumber}, {}, function(result){
		   	        		if($scope.newMemberOffer){
		   	        			$scope.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
		   	        		} else{
			   	        		switch($scope.apps[0].lastSavedOnPage.toLowerCase()){
			   	        			case "quotepage":
			   	        			case "aurapage":
			   	        			case "summarypage":
			   	        				$scope.go('/quote/1', true);
			   	        				break;
			   	        			default:
			   	        				break;
			   	        		}
		   	        		}
		   	        	}, function(err){
		   	        		console.log("Error while cancelling the saved app " + err);
		   	        	});
	   	        	}
	   	        });
   			} else{
   				if($scope.newMemberOffer){
   					$scope.showNewMemberPopUp('As a new employer-sponsored member, you may be eligible to increase your default Death and TPD cover and /or take out Income protection.This wont require medical evidence but you will need to answer some questions on your health.Would you like to continue?');
   				} else{
	   				switch(manageType.toUpperCase()){
	       			case "CCOVER":
	       				$scope.go('/quote/1', true);
	       				break;
	       			default:
	       				break;
	   				}
   				}
   			}
   		}, function(err){
   			console.log("Error while fetching saved apps " + err);
   		});
   	};
 }]); 
 /*Login Page Controller Ends*/