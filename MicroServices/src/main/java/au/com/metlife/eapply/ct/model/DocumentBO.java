package au.com.metlife.eapply.ct.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
/**
 * An entity User composed by three fields (id, email, name).
 * The Entity annotation indicates that this class is a JPA entity.
 * The Table annotation specifies the name for the table in the db.
 *
 * @author netgloo
 */
@Entity
@Table(name = "TBL_Document", schema="EAPPDB")
public class DocumentBO {
	
	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private long id;
	  
	  @NotNull
	  @Column(length=50)
	  private String policyNum;
	  
	  
	  @Column(length=500)
	  private String documentlocation;
	  
	 
	  @Column(length=200)
	  private String documentCode;
	  
	 
	  @Column(length=20)
	  private String documentStatus;
	  
	 
	  @Column(length=20)
	  private String fileId;
	  
	 
	  @Column(length=500)
	  private String fileName;
	  
	 
	  @Column(length=50)
	  private String fileSize;
	  
	 
	  @Column(length=200)
	  private String uuId;
	  
	  @Column(length=20)
	  private String partnerName;
	  
	  
	private java.util.Date CREATEDDATE;
	
	  public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getPolicyNum() {
		return policyNum;
	}



	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}



	public String getDocumentlocation() {
		return documentlocation;
	}



	public void setDocumentlocation(String documentlocation) {
		this.documentlocation = documentlocation;
	}



	public String getDocumentCode() {
		return documentCode;
	}



	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}



	public String getDocumentStatus() {
		return documentStatus;
	}



	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}



	public String getFileId() {
		return fileId;
	}



	public void setFileId(String fileId) {
		this.fileId = fileId;
	}



	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getFileSize() {
		return fileSize;
	}



	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}



	public String getUuId() {
		return uuId;
	}



	public void setUuId(String uuId) {
		this.uuId = uuId;
	}



	public java.util.Date getCREATEDDATE() {
		return CREATEDDATE;
	}



	public void setCREATEDDATE(java.util.Date cREATEDDATE) {
		CREATEDDATE = cREATEDDATE;
	}
	
	 public String getPartnerName() {
			return partnerName;
		  }



		public void setPartnerName(String partnerName) {
			this.partnerName = partnerName;
		}

	  
	  public DocumentBO(){
		  //Do nothing as written for future use
		  
	  }
	  
	
}
