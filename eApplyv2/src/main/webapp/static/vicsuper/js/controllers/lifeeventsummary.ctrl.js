/*Summary Page Controller Starts*/
VicsuperApp.controller('lifeeventsummary',['$scope','$rootScope', '$location','$timeout','$routeParams','$window','auraInputService','getAuraTransferData','submitAura','PersistenceService', 'submitEapply','auraResponseService','ngDialog', 'persoanlDetailService','saveEapply','RetrieveAppDetailsService','CalculateDeathService','CalculateTPDService','CalculateIPService','urlService',
                         function($scope,$rootScope, $location, $timeout, $routeParams, $window, auraInputService,getAuraTransferData,submitAura,PersistenceService,submitEapply,auraResponseService,ngDialog, persoanlDetailService,saveEapply, RetrieveAppDetailsService, CalculateDeathService,CalculateTPDService,CalculateIPService,urlService){

	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    $scope.errorOccured = false;
    $scope.confirmationWaitingPeriod = "N/A";
	$scope.confirmationBenefitPeriod = "N/A";
	$scope.confirmationOccRating = "N/A";
    $scope.eventList = [{
        "cde": "MARR",
        "desc": "Marriage or registration of de facto relationship",
        "docDesc": "Marriage or registration of de facto relationship.",
        "evidenceHelpText": "Marriage or registration of de facto relationship."
    			},
    			{
        "cde": "ANNMARR",
        "desc": "1st anniversary of marriage or registration of de facto relationship",
        "docDesc": "1st anniversary of marriage or registration of de facto relationship.",
        "evidenceHelpText": "1st anniversary of marriage or registration of de facto relationship."
    			},
    			{
        "cde": "DIVO",
        "desc": "Divorce or registration of separation from marriage or de facto relationship",
        "docDesc": "Divorce or registration of separation from marriage or de facto relationship.",
        "evidenceHelpText": "Divorce or registration of separation from marriage or de facto relationship:"
    			},
    			{
        "cde": "ANNDIVO",
        "desc": "1st anniversary of divorce or registation of separation from marriage or de factor relationship",
        "docDesc": "1st anniversary of divorce or registation of separation from marriage or de factor relationship.",
        "evidenceHelpText": "1st anniversary of divorce or registation of separation from marriage or de factor relationship."
    			},
    			{
    	  "cde": "BRTH",
    	  "desc": "Birth or adoption of a child",
    	  "docDesc": "Adopting or becoming the natural parent of a child.",
    	  "evidenceHelpText": "A certified copy of the birth certificate or adoption papers."
    		  	},
    		  	{
    	   "cde": "FRST",
    	   "desc": "New mortgage on initial purchase of primary place of residence",
    	   "docDesc": "New mortgage on initial purchase of primary place of residence.",
    	   "evidenceHelpText": "A certified copy of all the following:"
    		  	},
    			{
        "cde": "CUGA",
        "desc": "Increasing existing mortgage by at least $50,000 to renovate / extend primary place of residence",
        "docDesc": "Increasing existing mortgage by at least $50,000 to renovate / extend primary place of residence.",
        "evidenceHelpText": "Increasing existing mortgage by at least $50,000 to renovate / extend primary place of residence."
    			},
    			{
        "cde": "DCSS",
        "desc": "12th birthday of a child",
        "docDesc": "12th birthday of a child.",
        "evidenceHelpText": "12th birthday of a child."
    			}];
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	var decidedMode;
    	if($routeParams.mode == 3 || $scope.lifeEventCoverDetails.svRt){
    		decidedMode = 3
    	} else if($routeParams.mode == 1 || $routeParams.mode == 2){
    		decidedMode = 2;
    	}
    	$timeout(function(){
    		$location.path(path + decidedMode);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;
    };
    $window.scrollTo(0, 0);


    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
     };

     var ackCheckCCGC;



    $scope.lifeEventCoverDetails=PersistenceService.getlifeEventCoverDetails();
    $scope.lifeEventCoverOccDetails = PersistenceService.getLifeEventCoverOccDetails();
    $scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();
    $scope.auraDetails = PersistenceService.getLifeEventCoverAuraDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    $scope.deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	$scope.tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
	/*if($scope.lifeEventCoverDetails!= null && $scope.lifeEventCoverDetails.auraDisabled){
		$scope.auraDisabled = $scope.lifeEventCoverDetails.auraDisabled;
	}*/
    $scope.auraDisabled = true;

    
    if($scope.lifeEventCoverDetails && $scope.lifeEventCoverDetails.ipNewAmt && parseInt($scope.lifeEventCoverDetails.ipNewAmt) > 0)
	{
	$scope.confirmationWaitingPeriod = $scope.lifeEventCoverDetails.waitingPeriod;
	$scope.confirmationBenefitPeriod = $scope.lifeEventCoverDetails.benefitPeriod;
	$scope.confirmationOccRating = $scope.lifeEventCoverDetails.ipOccCategory;
	}
    
    $scope.navigateToDecision = function(){
	    ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
    	if(ackCheckCCGC){
    		$scope.CCGCackFlag = false;
    		if($scope.lifeEventCoverDetails != null && $scope.lifeEventCoverOccDetails != null && $scope.personalDetails != null
    				&& $scope.deathAddnlDetails!=null && $scope.tpdAddnlDetails !=null){
    			$rootScope.$broadcast('disablepointer');
    			$scope.lifeEventCoverDetails.lastSavedOn = '';
    			$scope.lifeEventCoverDetails.waitingPeriod = $scope.confirmationWaitingPeriod;
    			$scope.lifeEventCoverDetails.benefitPeriod = $scope.confirmationBenefitPeriod;
    			$scope.lifeEventCoverDetails.ipOccCategory = $scope.confirmationOccRating;
    			if(!($scope.lifeEventCoverDetails.deathCoverPremium > 0))
    				{
    				$scope.lifeEventCoverDetails.deathOccCategory = "N/A";
    				}
    			if(!($scope.lifeEventCoverDetails.tpdCoverPremium > 0))
					{
    				$scope.lifeEventCoverDetails.tpdOccCategory = "N/A";
					}
    			$scope.details={};
    			$scope.details.occupationDetails = $scope.lifeEventCoverOccDetails;
    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.lifeEventDocuments = $scope.uploadedFileDetails;

    			var temp = angular.extend($scope.details,$scope.lifeEventCoverDetails);
    			/*var submitObject = null;
    			if($scope.auraDetails != null){
    				var aura = angular.extend(temp,$scope.auraDetails);
    				submitObject = angular.extend(aura, $scope.personalDetails);
    			}else{
    				submitObject = angular.extend(temp, $scope.personalDetails);
    			}*/
    			var submitObject = angular.extend(temp, $scope.personalDetails);

    			auraResponseService.setResponse(submitObject);
    			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
            		//console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
            		if($scope.auraDetails!=null){
            			if($scope.auraDetails.overallDecision == 'ACC'){
                    			$location.path('/lifeeventaccept');
                    	} else if($scope.auraDetails.overallDecision == 'DCL'){
                    		$location.path('/lifeeventdecline');
                    	} else if($scope.auraDetails.overallDecision == 'RUW'){
                    		$location.path('/lifeeventunderwriting');
                    	}
            		}else{
            			$location.path('/lifeeventaccept');
            		}

            	}, function(err){
            		$scope.errorOccured = true;
            		$window.scrollTo(0, 0);
            		$rootScope.$broadcast('enablepointer');
            	});
            }
    	} else{
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
    	}
    };


    var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveSummary = function(){
    	$scope.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
    	if($scope.lifeEventCoverDetails != null && $scope.lifeEventCoverOccDetails != null &&  $scope.personalDetails != null
    			&& $scope.deathAddnlDetails!=null && $scope.tpdAddnlDetails !=null){
    		$scope.lifeEventCoverDetails.lastSavedOn = 'SummaryLifeEventPage';
    		$scope.details={};
			$scope.details.occupationDetails = $scope.lifeEventCoverOccDetails;
			$scope.details.lifeEventDocuments = $scope.uploadedFileDetails;
			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    		var temp = angular.extend( $scope.details,$scope.lifeEventCoverDetails);
    		var saveSummaryObject = angular.extend(temp, $scope.personalDetails);
    		/*if($scope.auraDetails!=null){
    			var aura = angular.extend(temp,$scope.auraDetails);
    			saveSummaryObject = angular.extend(aura, $scope.personalDetails);
    		}else{
    			saveSummaryObject = angular.extend(temp, $scope.personalDetails);
    		}*/

        	auraResponseService.setResponse(saveSummaryObject);
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                //console.log(response.data)
	        });
    	}

    };

    $scope.summarySaveAndExitPopUp = function (hhText) {
		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

    $scope.checkAckStateGC = function(){
    	$timeout(function(){
    		ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
        	if(ackCheckCCGC){
        		$scope.CCGCackFlag = false;
        	}else{
        		$scope.CCGCackFlag = true;
        	}
    	}, 10);
    };

    if($routeParams.mode == 3){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    	//RetrieveAppDetailsService.retrieveAppDetails({applicationNumber: num}, function(res){
    		var result = res.data[0];
    		var coverDet = {},
    			occDet = {},
    		    auraLoadingDet ={};
    		deathAddDet = {},
			tpdAddDet = {},

    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		coverDet.contactType = result.contactType;
    		coverDet.deathAmt = result.existingDeathAmt;
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.tpdAmt = result.existingTpdAmt;
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.ipAmt = result.existingIPAmount;
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium = result.totalPremium;
    		coverDet.deathCoverPremium = result.deathCoverPremium;
    		coverDet.tpdCoverPremium = result.tpdCoverPremium;
    		coverDet.ipCoverPremium = result.ipCoverPremium;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.eventAlreadyApplied = result.eventAlreadyApplied;
    		coverDet.eventDate = result.eventDate;
    		coverDet.eventName = result.eventName;
    		coverDet.deathNewAmt = result.deathNewAmt;
    		coverDet.tpdNewAmt = result.tpdNewAmt;
    		coverDet.ipNewAmt = parseFloat(result.ipNewAmt);
    		coverDet.waitingPeriod = result.waitingPeriod;
    		coverDet.benefitPeriod = result.benefitPeriod;
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.manageType = result.manageType;
    		coverDet.partnerCode = result.partnerCode;
    		coverDet.ipLifeCoverType = 'IpUnitised';
    		coverDet.appNum = result.appNum;
    		$scope.uploadedFileDetails = result.lifeEventDocuments ? result.lifeEventDocuments : [];	
    		
    		$scope.auraDisabled = true;
    		var tempEvent = $scope.eventList.filter(function(obj){
	    		return obj.cde == result.eventName;
	    	});
    		coverDet.eventDesc = tempEvent[0].desc;
    		$scope.lifeEventCoverDetails = coverDet;
    		
	    	deathAddDet.deathCoverType = result.addnlDeathCoverDetails.deathCoverType
	    	deathAddDet.deathFixedAmt = parseFloat(result.addnlDeathCoverDetails.deathFixedAmt);
	    	deathAddDet.deathInputTextValue = parseInt(result.addnlDeathCoverDetails.deathInputTextValue);
	    	deathAddDet.deathCoverPremium = parseFloat(result.addnlDeathCoverDetails.deathCoverPremium);
	    	deathAddDet.totalDeathUnits =result.addnlDeathCoverDetails.totalDeathUnits;
    		$scope.deathAddnlDetails = deathAddDet;

    		tpdAddDet.tpdCoverType = result.addnlTpdCoverDetails.tpdCoverType;
    		tpdAddDet.tpdFixedAmt =  parseFloat(result.addnlTpdCoverDetails.tpdFixedAmt);
    		tpdAddDet.tpdInputTextValue = parseInt(result.addnlTpdCoverDetails.tpdInputTextValue);
    		tpdAddDet.tpdCoverPremium =  parseFloat(result.addnlTpdCoverDetails.tpdCoverPremium);
    		tpdAddDet.totalTPDUnits = result.addnlTpdCoverDetails.totalTPDUnits;
    		$scope.tpdAddnlDetails = tpdAddDet;
    		
	    	/*$scope.event = tempEvent[0];*/
    		occDet.gender = result.personalDetails.gender;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.occRating1a = result.occupationDetails.occRating1a;
    		occDet.occRating1b = result.occupationDetails.occRating1b;
    		occDet.occRating2 = result.occupationDetails.occRating2;
    		
    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.salary = result.occupationDetails.salary;
    		
    		$scope.lifeEventCoverOccDetails = occDet;
    		//$scope.getAuradetails();
    		
    		if(parseInt(coverDet.ipNewAmt) > 0)
    		{
    			$scope.confirmationWaitingPeriod = coverDet.waitingPeriod;
    			$scope.confirmationBenefitPeriod = coverDet.benefitPeriod;
    			$scope.confirmationOccRating = coverDet.ipOccCategory;
    		}
    		
    		
    		$scope.svdRtrv = true;
    	}, function(err){
    		//console.log("Encountered an error while fetchimg the app details " + err);
    	});
    }
    
    $scope.getAuradetails = function()
    {
    	auraInputService.setFund('VICT');
      	auraInputService.setMode('LifeEvent');
      	auraInputService.setName($scope.lifeEventCoverDetails.name);
      	auraInputService.setAge(moment().diff(moment($scope.lifeEventCoverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
      	auraInputService.setAppnumber($scope.lifeEventCoverDetails.appNum);
      	auraInputService.setDeathAmt(parseInt($scope.lifeEventCoverDetails.deathAmt));
      	auraInputService.setTpdAmt(parseInt($scope.lifeEventCoverDetails.tpdAmt));
      	auraInputService.setIpAmt(parseInt($scope.lifeEventCoverDetails.ipAmt));
      	auraInputService.setWaitingPeriod($scope.lifeEventCoverDetails.waitingPeriod);
      	auraInputService.setBenefitPeriod($scope.lifeEventCoverDetails.benefitPeriod);
      	if($scope.lifeEventCoverOccDetails && $scope.lifeEventCoverOccDetails.gender ){
      		auraInputService.setGender($scope.lifeEventCoverOccDetails.gender);
      	}
      	auraInputService.setIndustryOcc("None");
      	auraInputService.setCountry('Australia');
      	auraInputService.setSalary('150000');
      	auraInputService.setFifteenHr('Yes');
      	auraInputService.setClientname('metaus')
      	auraInputService.setLastName($scope.lifeEventCoverDetails.lastName)
      	auraInputService.setFirstName($scope.lifeEventCoverDetails.firstName)
      	auraInputService.setDob($scope.lifeEventCoverDetails.dob)
      	auraInputService.setExistingTerm(false);
      	if($scope.personalDetails.memberType=="Personal"){
      		auraInputService.setMemberType("INDUSTRY OCCUPATION")
      	}else{
      		auraInputService.setMemberType("None")
      	}
    	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
      		$scope.auraResponseDataList = response.data.questions;
      		angular.forEach($scope.auraResponseDataList, function(Object) {
    			$scope.sectionname = Object.questionAlias.substring(3);

    			});
      	});
    	 
    	 submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
    		 $scope.auraDetails = response.data;
    	 });
    	 
    };

    }]);
   /*Summary Page Controller Ends*/
