package au.com.metlife.eapply.calculator.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CalcExpensesVO implements Serializable{

private BigDecimal expenses;
	
	private BigDecimal childPercentage;
	
	private BigDecimal reducedExpenses;
	
	private BigDecimal npvAmount;
	
	private BigDecimal ipBenefitAmount;

	private BigDecimal netExpenses;
	
	private BigDecimal netNPVAmt;
	
	private BigDecimal nursingCost;

	public BigDecimal getNursingCost() {
		return nursingCost;
	}

	public void setNursingCost(BigDecimal nursingCost) {
		this.nursingCost = nursingCost;
	}

	public BigDecimal getChildPercentage() {
		return childPercentage;
	}

	public void setChildPercentage(BigDecimal childPercentage) {
		this.childPercentage = childPercentage;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}

	public BigDecimal getIpBenefitAmount() {
		return ipBenefitAmount;
	}

	public void setIpBenefitAmount(BigDecimal ipBenefitAmount) {
		this.ipBenefitAmount = ipBenefitAmount;
	}

	public BigDecimal getNetExpenses() {
		return netExpenses;
	}

	public void setNetExpenses(BigDecimal netExpenses) {
		this.netExpenses = netExpenses;
	}

	public BigDecimal getNetNPVAmt() {
		return netNPVAmt;
	}

	public void setNetNPVAmt(BigDecimal netNPVAmt) {
		this.netNPVAmt = netNPVAmt;
	}

	public BigDecimal getNpvAmount() {
		return npvAmount;
	}

	public void setNpvAmount(BigDecimal npvAmount) {
		this.npvAmount = npvAmount;
	}

	public BigDecimal getReducedExpenses() {
		return reducedExpenses;
	}

	public void setReducedExpenses(BigDecimal reducedExpenses) {
		this.reducedExpenses = reducedExpenses;
	}
}
