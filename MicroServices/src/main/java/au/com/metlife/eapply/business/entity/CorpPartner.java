package au.com.metlife.eapply.business.entity;

import java.io.Serializable;

public class CorpPartner extends BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2125995277815008712L;

	public String id;
	public String partnerName;
	public String adminCode;
	public BrokerDet broker;
	public EmployerDet employer;
	public String createdDate;
	public FundCategory[] fundCat;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getAdminCode() {
		return adminCode;
	}
	public void setAdminCode(String adminCode) {
		this.adminCode = adminCode;
	}
	public BrokerDet getBroker() {
		return broker;
	}
	public void setBroker(BrokerDet broker) {
		this.broker = broker;
	}
	public EmployerDet getEmployer() {
		return employer;
	}
	public void setEmployer(EmployerDet employer) {
		this.employer = employer;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public FundCategory[] getFundCat() {
		return fundCat;
	}
	public void setFundCat(FundCategory[] fundCat) {
		this.fundCat = fundCat;
	}
	
	
	
	
	
}
