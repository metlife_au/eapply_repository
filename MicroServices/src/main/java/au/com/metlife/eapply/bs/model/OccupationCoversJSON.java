package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class OccupationCoversJSON implements Serializable{
  /**
	 * 
	 */
	private static final long serialVersionUID = -7130532288076786175L;

private String citizenQue;
  
  private String fifteenHr;
  
  private String industryCode;
  
  private String industryName;
  
  private String occupation;
  
  private String managementRoleQue;
  
  private String salary;
  
  private String tertiaryQue;
  
  private String withinOfficeQue;
  
  private String gender;
  
  private String otherOccupation;
  
  private String hazardousQue;
  
  private String ownBussinessQues;
  
  private String ownBussinessYesQues;
  
  private String ownBussinessNoQues;
  
  private String smokerQuestion;
	
  private String permanentlyEmployedQuestion;
  
  private String hoursPerWeekQuestion;
  
  /** Vicsuper proeprties starts - Made by Purna**/
	private String occRating1a=null; 
	private String occRating1b=null;
	private String occRating2=null;
  /** Vicsuper proeprties ends- Made by Purna **/


public String getOwnBussinessQues() {
	return ownBussinessQues;
}

public void setOwnBussinessQues(String ownBussinessQues) {
	this.ownBussinessQues = ownBussinessQues;
}

public String getOwnBussinessYesQues() {
	return ownBussinessYesQues;
}

public void setOwnBussinessYesQues(String ownBussinessYesQues) {
	this.ownBussinessYesQues = ownBussinessYesQues;
}

public String getOwnBussinessNoQues() {
	return ownBussinessNoQues;
}

public void setOwnBussinessNoQues(String ownBussinessNoQues) {
	this.ownBussinessNoQues = ownBussinessNoQues;
}

public String getHazardousQue() {
	return hazardousQue;
}

public void setHazardousQue(String hazardousQue) {
	this.hazardousQue = hazardousQue;
}

public String getOtherOccupation() {
	return otherOccupation;
}

public void setOtherOccupation(String otherOccupation) {
	this.otherOccupation = otherOccupation;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getCitizenQue() {
	return citizenQue;
}

public void setCitizenQue(String citizenQue) {
	this.citizenQue = citizenQue;
}

public String getFifteenHr() {
	return fifteenHr;
}

public void setFifteenHr(String fifteenHr) {
	this.fifteenHr = fifteenHr;
}

public String getIndustryCode() {
	return industryCode;
}

public void setIndustryCode(String industryCode) {
	this.industryCode = industryCode;
}

public String getIndustryName() {
	return industryName;
}

public void setIndustryName(String industryName) {
	this.industryName = industryName;
}

public String getOccupation() {
	return occupation;
}

public void setOccupation(String occupation) {
	this.occupation = occupation;
}

public String getManagementRoleQue() {
	return managementRoleQue;
}

public void setManagementRoleQue(String managementRoleQue) {
	this.managementRoleQue = managementRoleQue;
}

public String getSalary() {
	return salary;
}

public void setSalary(String salary) {
	this.salary = salary;
}

public String getTertiaryQue() {
	return tertiaryQue;
}

public void setTertiaryQue(String tertiaryQue) {
	this.tertiaryQue = tertiaryQue;
}

public String getWithinOfficeQue() {
	return withinOfficeQue;
}

public void setWithinOfficeQue(String withinOfficeQue) {
	this.withinOfficeQue = withinOfficeQue;
}

public String getSmokerQuestion() {
	return smokerQuestion;
}

public void setSmokerQuestion(String smokerQuestion) {
	this.smokerQuestion = smokerQuestion;
}

public String getPermanentlyEmployedQuestion() {
	return permanentlyEmployedQuestion;
}

public void setPermanentlyEmployedQuestion(String permanentlyEmployedQuestion) {
	this.permanentlyEmployedQuestion = permanentlyEmployedQuestion;
}

public String getHoursPerWeekQuestion() {
	return hoursPerWeekQuestion;
}

public void setHoursPerWeekQuestion(String hoursPerWeekQuestion) {
	this.hoursPerWeekQuestion = hoursPerWeekQuestion;
}

public String getOccRating1a() {
	return occRating1a;
}

public String getOccRating1b() {
	return occRating1b;
}

public String getOccRating2() {
	return occRating2;
}

public void setOccRating1a(String occRating1a) {
	this.occRating1a = occRating1a;
}

public void setOccRating1b(String occRating1b) {
	this.occRating1b = occRating1b;
}

public void setOccRating2(String occRating2) {
	this.occRating2 = occRating2;
}

}
