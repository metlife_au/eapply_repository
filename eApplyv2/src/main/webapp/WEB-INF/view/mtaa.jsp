<!DOCTYPE html>
<html ng-app="MtaaApp">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" href="../../eapply/static/mtaa/src/app/assets/images/favicon.ico">
		<title ng-bind="$root.title"> </title>
		<link rel="stylesheet" href="../../eapply/resources/css/ngDialog.css">
		<link rel="stylesheet" href="../../eapply/static/mtaa/src/app/assets/styles/ngDialog-theme-plain.css">
		<link rel="stylesheet" href="../../eapply/resources/css/bootstrap-4.0.0-beta.min.css">		
		<!-- <link href="../../eapply/static/pages/assets/css/bootstrap.min.css" rel="stylesheet"> -->
		<link href="../../eapply/static/mtaa/src/app/assets/styles/custom.css" rel="stylesheet">
        <link href="../../eapply/static/mtaa/src/app/assets/styles/custom-ben.css" rel="stylesheet"> 
	        <%
				String headerData = response.getHeader("InputData");
	        String nonMvStatus = response.getHeader("nonmv");
	        String clntRefNo = response.getHeader("clientRefNo");
	        String domainName = response.getHeader("domainname");
			%>
			<script type="text/javascript">
				var inputData = '<%=headerData%>';
				var memberStatus =  '<%=nonMvStatus%>';
				var clintRef = '<%=clntRefNo%>';
				var baseUrl = '<%= domainName%>';
			</script>
		<script>
		if( self == top ) {
		document.documentElement.style.display = 'block' ;
		document.documentElement.style.visibility = 'visible' ;
		} else {
		top.location = self.location ;
		}
		</script>
		
		<script src="../../eapply/static/mtaa/dist/lib.min.js"></script>
		<script src="../../eapply/static/mtaa/dist/app.min.js"></script>
		
		
		<!-- <script src="../../eapply/static/mtaa/src/app/app.module.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/app.module.config.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/app.module.routes.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/appDataModel.js" type="text/javascript" ></script>
				
		<script src="../../eapply/static/common/services/restAPI.js" type="text/javascript" ></script> 
		<script src="../../eapply/static/mtaa/src/app/services/sessionTimeoutService.js" type="text/javascript" ></script>		
		<script src="../../eapply/static/mtaa/src/app/services/mtaa.svc.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/modules/landing/landing.ctrl.js" type="text/javascript" ></script>
				
		<script src="../../eapply/static/mtaa/src/app/modules/transfer/cover.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/transfer/aura.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/transfer/summary.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/transfer/decision.ctrl.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/modules/life/cover.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/life/summary.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/life/decision.ctrl.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/modules/cancel/cover.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/cancel/summary.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/cancel/decision.ctrl.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/modules/convert/cover.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/convert/summary.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/convert/decision.ctrl.js" type="text/javascript" ></script>		
		
		<script src="../../eapply/static/mtaa/src/app/modules/occupation/cover.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/occupation/aura.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/occupation/summary.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/occupation/decision.ctrl.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/modules/retrievesavedapplication/retrieve.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/timeout/timeOutController.ctrl.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/directives/app.directives.module.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/utils/filters.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/ui-components/metlife-corp-help-block/metlife-corp-help-block.dir.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/ui-components/metlife-mtaa-header-nav/metlife-mtaa-header-nav.dir.js" type="text/javascript" ></script>
		
		<script src="../../eapply/static/mtaa/src/app/modules/quote/cover.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/quote/aura.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/quote/summary.ctrl.js" type="text/javascript" ></script>
		<script src="../../eapply/static/mtaa/src/app/modules/quote/decision.ctrl.js" type="text/javascript" ></script> -->
		
		<script type="text/javascript" src="../../eapply/static/mtaa/src/app/services/custom.js"></script>
		<script type="text/javascript" src="../../eapply/static/mtaa/src/app/validation.js"></script>
	</head>
	
	<body class="tooltip-demo">
		<div ui-view autoscroll="true" class="fixed-container"></div>
		<div id="body-mask" data-loading>
		<div class="body-mask-loader"></div>
    </div>
    
    <div ng-include="'../../eapply/static/mtaa/src/app/ui-components/metlife-mtaa-footer/metlife-mtaa-footer.tmpl.html'"></div>
  </body>
		
	
</html>