(function(angular) {
    'use strict';

    /* global channel, inputData, token, moment */
	/**
	 * @ngdoc controller
	 * @name MtaaApp.controller:occUpgradeSummary
	 *
	 * @description
	 * This is occUpgradeSummary description
	 */
    
 angular
	.module('MtaaApp')
	.controller('occUpgradeSummary', occUpgradeSummary);
 occUpgradeSummary.$inject=['$scope', '$location', '$rootScope', '$timeout','$window','$stateParams','auraInputSvc','fetchAuraTransferData','submitAuraSvc','PersistenceService','persoanlDetailService','auraRespSvc','ngDialog','submitEapplySvc','urlService','saveEapplyData','RetrieveAppDetailsService','fetchPersoanlDetailSvc','appData','fetchIndustryListMtaa','fetchAppNumberSvc','$q'];
 function occUpgradeSummary($scope, $location, $rootScope, $timeout,$window,$stateParams,auraInputSvc,fetchAuraTransferData,submitAuraSvc,PersistenceService,persoanlDetailService,auraRespSvc,ngDialog,submitEapplySvc,urlService,saveEapplyData,RetrieveAppDetailsService,fetchPersoanlDetailSvc,appData,fetchIndustryListMtaa,fetchAppNumberSvc,$q){
	 	var vm = this;
		vm.urlList = urlService.getUrlList();

	    $rootScope.$broadcast('enablepointer');

	    vm.go = function (path) {
	    	if(($stateParams.mode === 3 || vm.UCS.applicant.svRt) && path==='/auraocc/2'){
	    		path='/auraocc/3';
	    	}
	    	if(($stateParams.mode === 3 || vm.UCS.applicant.svRt) && path==='/quoteoccchange/2'){
	    		path='/quoteoccchange/3';
	    	}
	    	
	    	$timeout(function(){
	    		$location.path(path);
	    	}, 10);
	  	};
	  	vm.collapse = false;
	  	vm.toggle = function() {
	        vm.collapse = !vm.collapse;
	    };

	    vm.UCS = {};
	    vm.navigateToLandingPage = function (){
	    	ngDialog.openConfirm({
	            template:'<div class=\'ngdialog-content\'><div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom\'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class=\'ngdialog-footer mt0\'>' + 
	            '<div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button> <button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path('/landing');
	        }, function(e){
	        	if(e==='oncancel'){
	        		return false;
	        	}
	        });
	     };
	     
	     vm.init = function(){
	    	 	vm.UCS.responseObject = vm.workRatingAuraDetails.responseObject;
			    vm.UCS.applicant.birthDate = moment(vm.UCS.applicant.birthDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
			    // Industry name
			    vm.UCS.applicant.occupationCode = fetchIndustryListMtaa.getIndustryName(vm.UCS.applicant.industryType);
			    vm.UCS.auraDisabled = false;
			    vm.WRGCackFlag = true;
			    vm.UCS.ackCheck = false;
	     };
	     
	     var ackCheckWRGC;
	     angular.extend(vm.UCS, appData.getAppData());
	     if(vm.UCS.applicant.contactDetails.preferedContacType === '1') {
				vm.contactType = 'Mobile';
			  }else if(vm.UCS.applicant.contactDetails.preferedContacType === '2') {
				vm.contactType = 'Home';
			  }else if(vm.UCS.applicant.contactDetails.preferedContacType === '3') {
				vm.contactType = 'Work';
			  }else{
				vm.contactType = '';
			  }
	      vm.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
		  vm.personalDetails = vm.inputDetails.personalDetails || {};
		  vm.contactDetails = vm.inputDetails.contactDetails || {};
	    
	    if(vm.UCS!= null && vm.UCS.auraDisabled){
	    	vm.auraDisabled = vm.UCS.auraDisabled;
		}
	    	
	    if(Object.keys(appData.getChangeCoverAuraDetails()).length) {
		    vm.workRatingAuraDetails = appData.getChangeCoverAuraDetails();
		    vm.init();
		  } else if(!vm.UCS.auraDisabled) {
		    vm.getAuraDetails().then(function(result) {
		      vm.workRatingAuraDetails = result;
		      vm.init();
		    });
		  }
		  else
			  {
			  // Industry name
			    vm.UCS.applicant.occupationCode = fetchIndustryListMtaa.getIndustryName(vm.UCS.applicant.industryType);
			  }

	    vm.submitWorkRating = function(){
	    	ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');


	    	if(ackCheckWRGC){
	    		vm.WRGCackFlag = false;
	    		var submitSummaryObject;
	    		if(vm.UCS != null && vm.UCS.responseObject != null && vm.personalDetails != null){
	    			vm.UCS.lastSavedOn = '';
	        		submitSummaryObject = angular.copy(vm.UCS);
	        		auraRespSvc.setResponse(submitSummaryObject);
	                $rootScope.$broadcast('disablepointer');
	                submitEapplySvc.submitObj(vm.urlList.submitEapplyUrlNew,submitSummaryObject).then(function(response) {
	            		console.log(response.data);
	            		appData.setPDFLocation(response.data.clientPDFLocation);
	            		appData.setNpsUrl(response.data.npsTokenURL);

	            		if(vm.workRatingAuraDetails.deathDecision!=null &&  vm.workRatingAuraDetails.tpdDecision!=null){
	            			if((vm.workRatingAuraDetails.deathDecision==='DCL') ||(vm.workRatingAuraDetails.tpdDecision==='DCL')){
	            				vm.workRatingAuraDetails.overallDecision = 'DCL';
	    					}else{
	    						vm.workRatingAuraDetails.overallDecision = 'ACC';
	    					}
	            		}
	            		if(vm.workRatingAuraDetails.overallDecision === 'ACC'){
	                		vm.go('/workRatingAccept');
	                	}else if(vm.workRatingAuraDetails.overallDecision === 'DCL'){
	                		vm.go('/workRatingDecline');
	                	}
	            	}, function() {
	                    $window.scrollTo(0, 0);
	                    $rootScope.$broadcast('enablepointer');
	                    throw {message: 'submitEapply request failed'};
	                });
	        	}else{
	        		if(vm.UCS != null && vm.personalDetails != null){
	        			vm.UCS.lastSavedOn = '';
	            		submitSummaryObject =  angular.copy(vm.UCS);
	            		auraRespSvc.setResponse(submitSummaryObject);
	                    $rootScope.$broadcast('disablepointer');
	                    submitEapplySvc.submitObj(vm.urlList.submitEapplyUrlNew,submitSummaryObject).then(function(response) {
	                		console.log(response.data);
	                		appData.setPDFLocation(response.data.clientPDFLocation);
	                		appData.setNpsUrl(response.data.npsTokenURL);
	                		vm.go('/workRatingMaintain');
	                	}, function() {
	                        $window.scrollTo(0, 0);
	                        $rootScope.$broadcast('enablepointer');
	                        throw {message: 'submitEapply request failed'};
	                    });
	        		}

	        	}
	    	}else{
	        	if(ackCheckWRGC){
	        		vm.WRGCackFlag = false;
	        	}else{
	        		vm.WRGCackFlag = true;
	        	}
	        	vm.scrollToUncheckedElement();
	    	}

	    };
	    vm.scrollToUncheckedElement = function(){
			var elements = [ackCheckWRGC];
			var ids = ['generalConsentLabelWR'];
	    	for(var k = 0; k < elements.length; k++){
	    		if(!elements[k]){
	    			$('html, body').animate({
	        	        scrollTop: $('#' + ids[k]).offset().top
	        	    }, 1000);
	    			break;
	    		}
	    	}
	    };
	    var appNum;
	    appNum = fetchAppNumberSvc.getAppNumber();
	    vm.saveSummaryWorkRating = function() {
	    	
	    	if(vm.UCS != null  && vm.personalDetails != null){
	    		vm.UCS.lastSavedOn = 'SummaryUpdatePage';
	    		var saveUpdateSummaryObject = angular.copy(vm.UCS);
	        	auraRespSvc.setResponse(saveUpdateSummaryObject);
	        	saveEapplyData.reqObj(vm.urlList.saveEapplyUrlNew,saveUpdateSummaryObject).then(function(response) {
		                console.log(response.data);
		                vm.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
		        });
	    	}
	    };

	   vm.saveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
				    template: '<div class=\'ngdialog-content\'><div class=\'modal-body\'><h4 class=\'modal-title aligncenter\' id=\'myModalLabel\'> Application saved </h4><!-- Row starts --><div class=\'row  rowcustom\'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>'+hhText+
				    '</div>  <p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog=\'secondDialogId\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-dialog-close-previous=\'\' ng-click=\'preCloseCallback()\'>Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = '/landing';
					       $location.path( url );
					       return true;
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};

		 if($stateParams.mode === 3){
	     	var num = fetchAppNumberSvc.getAppNumber();
	     	RetrieveAppDetailsService.retrieveAppDetails(vm.urlList.retrieveAppUrlNew,num).then(function(res){
	     		var result = res.data[0];
	     		angular.extend(vm.UCS.applicant, res.data[0].applicant);
	    		vm.getAuradetails();
	     	},function(err){
	     		console.log('something went wrong while saving...'+JSON.stringify(err));
	     	});
		 }

		 
		 vm.getAuradetails = function()
		    {
			    var defer = $q.defer();
			 	auraInputSvc.setFund('MTAA');
			  	auraInputSvc.setMode('WorkRating');
			  	auraInputSvc.setName(vm.personalDetails.firstName+' '+vm.personalDetails.lastName);
			  	auraInputSvc.setAge(moment().diff(moment(vm.UCS.applicant.birthDate, 'DD-MM-YYYY'), 'years')) ;
			  	auraInputSvc.setAppnumber(parseInt(vm.UCS.applicatioNumber));
			  	auraInputSvc.setDeathAmt(parseFloat(vm.UCS.applicant.cover[0].existingCoverAmount));
			  	auraInputSvc.setTpdAmt(parseFloat(vm.UCS.applicant.cover[1].existingCoverAmount));
			  	auraInputSvc.setIpAmt(vm.UCS.applicant.cover[2].existingCoverAmount === null ? 0 : parseFloat(vm.UCS.applicant.cover[2].existingCoverAmount));
			  	auraInputSvc.setWaitingPeriod(vm.UCS.applicant.cover[2].existingIpWaitingPeriod);
			  	auraInputSvc.setBenefitPeriod(vm.UCS.applicant.cover[2].existingIpBenefitPeriod);
			  	if(vm.UCS && vm.UCS.applicant.gender ){
			  		auraInputSvc.setGender(vm.UCS.applicant.gender);
			  	}
			  	auraInputSvc.setIndustryOcc(vm.UCS.applicant.industryType+':'+vm.UCS.applicant.occupation);
			  	auraInputSvc.setCountry('Australia');
			  	auraInputSvc.setSalary(vm.UCS.applicant.annualSalary);
			  	auraInputSvc.setFifteenHr(vm.UCS.applicant.workHours);
			  	auraInputSvc.setClientname('metaus');
			  	auraInputSvc.setLastName(vm.UCS.applicant.lastName);
			  	auraInputSvc.setFirstName(vm.UCS.applicant.firstName);
			  	auraInputSvc.setDob(vm.UCS.applicant.birthDate);
			  	auraInputSvc.setExistingTerm(false);
			  	if(vm.personalDetails.memberType==='Personal'){
			  		auraInputSvc.setMemberType('INDUSTRY OCCUPATION');
			  	}else{
			  		auraInputSvc.setMemberType('None');
			  	}
			  	fetchAuraTransferData.requestObj(vm.urlList.auraTransferDataUrl).then(function(response) {
			  		vm.auraResponseDataList = response.data.questions;
			  		console.log(vm.auraResponseDataList);
			  		angular.forEach(vm.auraResponseDataList, function(Object) {
						vm.sectionname = Object.questionAlias.substring(3);

						});
			  		defer.resolve(vm.auraResponseDataList);
			  	});
		  	
		    	 
				 submitAuraSvc.requestObj(vm.urlList.submitAuraUrl).then(function(response) {
		    		 vm.workRatingAuraDetails = response.data;
		    		 appData.setChangeCoverAuraDetails(vm.workRatingAuraDetails);
			          defer.resolve(vm.auraResponses);   
		    	 }, function(err) {
				      defer.reject(err);
				    });
		    	 
		    };

	    vm.checkAckStateGCWR = function(){
	    	$timeout(function(){
	    		ackCheckWRGC = $('#generalConsentLabelWR').hasClass('active');
	        	if(ackCheckWRGC){
	        		vm.WRGCackFlag = false;
	        		vm.UCS.ackCheck = true;
	        	}else{
	        		vm.WRGCackFlag = true;
	        		vm.UCS.ackCheck = false;
	        	}

	    	}, 10);
	    };
	    
 }
 })(angular);