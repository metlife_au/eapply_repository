/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import au.com.metlife.eapply.calculator.model.KidsVO;

/**
 * @author 199306
 *
 */
public class RuleModel implements Serializable{
	
	private int age;
	
	private String fundCode;
	
	private String gender;
	private String corpFundCode;
	private BigDecimal deathMaxAmount;
	private BigDecimal tpdMaxAmount;
	private BigDecimal ipMaxAmount;
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	private String deathOccCategory;
	
	private String tpdOccCategory;
	
	private String ipOccCategory;
	
	private boolean smoker;
	
	private int deathUnits;
	
	private BigDecimal deathFixedAmount;
	
	private BigDecimal deathFixedCost;
	
	private BigDecimal deathUnitsCost;
	
	private int tpdUnits;
	
	private BigDecimal tpdFixedAmount;
	
	private BigDecimal tpdFixedCost;
	
	private BigDecimal tpdUnitsCost;
	
	private String annualSalary;
	
	//Guild changes
	private boolean unitsed;
	
	public boolean isUnitsed() {
		return unitsed;
	}

	public void setUnitsed(boolean unitsed) {
		this.unitsed = unitsed;
	}
	//Guild changes
	public String getAnnualSalary() {
		return annualSalary;
	}

	public void setAnnualSalary(String annualSalary) {
		this.annualSalary = annualSalary;
	}

	public BigDecimal getTpdFixedAmount() {
		return tpdFixedAmount;
	}

	public void setTpdFixedAmount(BigDecimal tpdFixedAmount) {
		this.tpdFixedAmount = tpdFixedAmount;
	}

	public BigDecimal getTpdFixedCost() {
		return tpdFixedCost;
	}

	public void setTpdFixedCost(BigDecimal tpdFixedCost) {
		this.tpdFixedCost = tpdFixedCost;
	}

	public BigDecimal getTpdUnitsCost() {
		return tpdUnitsCost;
	}

	public void setTpdUnitsCost(BigDecimal tpdUnitsCost) {
		this.tpdUnitsCost = tpdUnitsCost;
	}

	private int ipUnits;
	
	private BigDecimal ipFixedAmount;
	
	private BigDecimal ipFixedCost;
	
	private BigDecimal ipUnitsCost;
	
	private String premiumFrequency;
	
	private String memberType;
	
	private String deathCoverType;
	
	private String tpdCoverType;
	
	private String ipCoverType;
	
	private String manageType;
	
	private String ipBenefitPeriod;
	
	private String ipWaitingPeriod;
	
	private BigDecimal deathTransferAmount;
	
	private BigDecimal tpdTransferAmount;
	
	private BigDecimal ipTransferAmount;
	
	private BigDecimal deathExistingAmount;
	
	private BigDecimal tpdExistingAmount;
	
	private BigDecimal ipExistingAmount;
	
	private String exDeathCoverType;
	
	private String exTpdCoverType;
	
	private String exIpCoverType;
	
	private String ipExBenefitPeriod;
	
	private String ipExWaitingPeriod;
	
	private String stateCode;
	
	private String ipCoverStartDate;
	
	private String tpdCoverStartDate;
	
	//Added for insurance calculator
	private int retirementAge;
	private BigDecimal grossSalVal;
	private BigDecimal inflationRate;
	private BigDecimal realInterestRate;
	private BigDecimal partnerGrossSalVal;
	private String livingWithSpouseVal;
	private BigDecimal expensesVal;
	private int kidsCount;
	private BigDecimal adultFactor;
	private BigDecimal childFactor;
	private BigDecimal miscFactor;
	private BigDecimal ipBenefitPaidToSA;
	private BigDecimal requiredIpCover;
	private int dependentSuppAge;
	private int ipBenefitPerVal;
	private boolean includeNursingCosts;
	private BigDecimal annualMedAndNursingCost;
	private List<KidsVO> kiddetails;
	private BigDecimal superContribRate;
	private boolean livingWithSpouse;
	
	public boolean isLivingWithSpouse() {
		return livingWithSpouse;
	}

	public void setLivingWithSpouse(boolean livingWithSpouse) {
		this.livingWithSpouse = livingWithSpouse;
	}

	public BigDecimal getSuperContribRate() {
		return superContribRate;
	}

	public void setSuperContribRate(BigDecimal superContribRate) {
		this.superContribRate = superContribRate;
	}

	public int getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(int retirementAge) {
		this.retirementAge = retirementAge;
	}

	public BigDecimal getGrossSalVal() {
		return grossSalVal;
	}

	public void setGrossSalVal(BigDecimal grossSalVal) {
		this.grossSalVal = grossSalVal;
	}

	public BigDecimal getInflationRate() {
		return inflationRate;
	}

	public void setInflationRate(BigDecimal inflationRate) {
		this.inflationRate = inflationRate;
	}

	public BigDecimal getRealInterestRate() {
		return realInterestRate;
	}

	public void setRealInterestRate(BigDecimal realInterestRate) {
		this.realInterestRate = realInterestRate;
	}

	public BigDecimal getPartnerGrossSalVal() {
		return partnerGrossSalVal;
	}

	public void setPartnerGrossSalVal(BigDecimal partnerGrossSalVal) {
		this.partnerGrossSalVal = partnerGrossSalVal;
	}

	public String getLivingWithSpouseVal() {
		return livingWithSpouseVal;
	}

	public void setLivingWithSpouseVal(String livingWithSpouseVal) {
		this.livingWithSpouseVal = livingWithSpouseVal;
	}

	public BigDecimal getExpensesVal() {
		return expensesVal;
	}

	public void setExpensesVal(BigDecimal expensesVal) {
		this.expensesVal = expensesVal;
	}

	public int getKidsCount() {
		return kidsCount;
	}

	public void setKidsCount(int kidsCount) {
		this.kidsCount = kidsCount;
	}

	public BigDecimal getAdultFactor() {
		return adultFactor;
	}

	public void setAdultFactor(BigDecimal adultFactor) {
		this.adultFactor = adultFactor;
	}

	public BigDecimal getChildFactor() {
		return childFactor;
	}

	public void setChildFactor(BigDecimal childFactor) {
		this.childFactor = childFactor;
	}

	public BigDecimal getMiscFactor() {
		return miscFactor;
	}

	public void setMiscFactor(BigDecimal miscFactor) {
		this.miscFactor = miscFactor;
	}

	public BigDecimal getIpBenefitPaidToSA() {
		return ipBenefitPaidToSA;
	}

	public void setIpBenefitPaidToSA(BigDecimal ipBenefitPaidToSA) {
		this.ipBenefitPaidToSA = ipBenefitPaidToSA;
	}

	public BigDecimal getRequiredIpCover() {
		return requiredIpCover;
	}

	public void setRequiredIpCover(BigDecimal requiredIpCover) {
		this.requiredIpCover = requiredIpCover;
	}

	public int getDependentSuppAge() {
		return dependentSuppAge;
	}

	public void setDependentSuppAge(int dependentSuppAge) {
		this.dependentSuppAge = dependentSuppAge;
	}

	public int getIpBenefitPerVal() {
		return ipBenefitPerVal;
	}

	public void setIpBenefitPerVal(int ipBenefitPerVal) {
		this.ipBenefitPerVal = ipBenefitPerVal;
	}

	public boolean isIncludeNursingCosts() {
		return includeNursingCosts;
	}

	public void setIncludeNursingCosts(boolean includeNursingCosts) {
		this.includeNursingCosts = includeNursingCosts;
	}

	public BigDecimal getAnnualMedAndNursingCost() {
		return annualMedAndNursingCost;
	}

	public void setAnnualMedAndNursingCost(BigDecimal annualMedAndNursingCost) {
		this.annualMedAndNursingCost = annualMedAndNursingCost;
	}

	public List<KidsVO> getKiddetails() {
		return kiddetails;
	}

	public void setKiddetails(List<KidsVO> kiddetails) {
		this.kiddetails = kiddetails;
	}

	public String getIpExBenefitPeriod() {
		return ipExBenefitPeriod;
	}

	public void setIpExBenefitPeriod(String ipExBenefitPeriod) {
		this.ipExBenefitPeriod = ipExBenefitPeriod;
	}

	public String getIpExWaitingPeriod() {
		return ipExWaitingPeriod;
	}

	public void setIpExWaitingPeriod(String ipExWaitingPeriod) {
		this.ipExWaitingPeriod = ipExWaitingPeriod;
	}

	public String getExDeathCoverType() {
		return exDeathCoverType;
	}

	public void setExDeathCoverType(String exDeathCoverType) {
		this.exDeathCoverType = exDeathCoverType;
	}

	public String getExTpdCoverType() {
		return exTpdCoverType;
	}

	public void setExTpdCoverType(String exTpdCoverType) {
		this.exTpdCoverType = exTpdCoverType;
	}

	public String getExIpCoverType() {
		return exIpCoverType;
	}

	public void setExIpCoverType(String exIpCoverType) {
		this.exIpCoverType = exIpCoverType;
	}

	public BigDecimal getDeathExistingAmount() {
		return deathExistingAmount;
	}

	public void setDeathExistingAmount(BigDecimal deathExistingAmount) {
		this.deathExistingAmount = deathExistingAmount;
	}

	public BigDecimal getTpdExistingAmount() {
		return tpdExistingAmount;
	}

	public void setTpdExistingAmount(BigDecimal tpdExistingAmount) {
		this.tpdExistingAmount = tpdExistingAmount;
	}

	public BigDecimal getIpExistingAmount() {
		return ipExistingAmount;
	}

	public void setIpExistingAmount(BigDecimal ipExistingAmount) {
		this.ipExistingAmount = ipExistingAmount;
	}

	public BigDecimal getDeathTransferAmount() {
		return deathTransferAmount;
	}

	public void setDeathTransferAmount(BigDecimal deathTransferAmount) {
		this.deathTransferAmount = deathTransferAmount;
	}

	public BigDecimal getTpdTransferAmount() {
		return tpdTransferAmount;
	}

	public void setTpdTransferAmount(BigDecimal tpdTransferAmount) {
		this.tpdTransferAmount = tpdTransferAmount;
	}

	public BigDecimal getIpTransferAmount() {
		return ipTransferAmount;
	}

	public void setIpTransferAmount(BigDecimal ipTransferAmount) {
		this.ipTransferAmount = ipTransferAmount;
	}

	public String getIpBenefitPeriod() {
		return ipBenefitPeriod;
	}

	public void setIpBenefitPeriod(String ipBenefitPeriod) {
		this.ipBenefitPeriod = ipBenefitPeriod;
	}

	public String getIpWaitingPeriod() {
		return ipWaitingPeriod;
	}

	public void setIpWaitingPeriod(String ipWaitingPeriod) {
		this.ipWaitingPeriod = ipWaitingPeriod;
	}

	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public String getDeathCoverType() {
		return deathCoverType;
	}

	public void setDeathCoverType(String deathCoverType) {
		this.deathCoverType = deathCoverType;
	}

	public String getTpdCoverType() {
		return tpdCoverType;
	}

	public void setTpdCoverType(String tpdCoverType) {
		this.tpdCoverType = tpdCoverType;
	}

	public String getIpCoverType() {
		return ipCoverType;
	}

	public void setIpCoverType(String ipCoverType) {
		this.ipCoverType = ipCoverType;
	}

	public String getDeathOccCategory() {
		return deathOccCategory;
	}

	public void setDeathOccCategory(String deathOccCategory) {
		this.deathOccCategory = deathOccCategory;
	}

	public String getTpdOccCategory() {
		return tpdOccCategory;
	}

	public void setTpdOccCategory(String tpdOccCategory) {
		this.tpdOccCategory = tpdOccCategory;
	}

	public String getIpOccCategory() {
		return ipOccCategory;
	}

	public void setIpOccCategory(String ipOccCategory) {
		this.ipOccCategory = ipOccCategory;
	}

	public boolean isSmoker() {
		return smoker;
	}

	public void setSmoker(boolean smoker) {
		this.smoker = smoker;
	}

	public int getDeathUnits() {
		return deathUnits;
	}

	public void setDeathUnits(int deathUnits) {
		this.deathUnits = deathUnits;
	}

	public BigDecimal getDeathFixedAmount() {
		return deathFixedAmount;
	}

	public void setDeathFixedAmount(BigDecimal deathFixedAmount) {
		this.deathFixedAmount = deathFixedAmount;
	}

	public BigDecimal getDeathFixedCost() {
		return deathFixedCost;
	}

	public void setDeathFixedCost(BigDecimal deathFixedCost) {
		this.deathFixedCost = deathFixedCost;
	}

	public BigDecimal getDeathUnitsCost() {
		return deathUnitsCost;
	}

	public void setDeathUnitsCost(BigDecimal deathUnitsCost) {
		this.deathUnitsCost = deathUnitsCost;
	}

	public int getTpdUnits() {
		return tpdUnits;
	}

	public void setTpdUnits(int tpdUnits) {
		this.tpdUnits = tpdUnits;
	}

	public int getIpUnits() {
		return ipUnits;
	}

	public void setIpUnits(int ipUnits) {
		this.ipUnits = ipUnits;
	}

	public BigDecimal getIpFixedAmount() {
		return ipFixedAmount;
	}

	public void setIpFixedAmount(BigDecimal ipFixedAmount) {
		this.ipFixedAmount = ipFixedAmount;
	}

	public BigDecimal getIpFixedCost() {
		return ipFixedCost;
	}

	public void setIpFixedCost(BigDecimal ipFixedCost) {
		this.ipFixedCost = ipFixedCost;
	}

	public BigDecimal getIpUnitsCost() {
		return ipUnitsCost;
	}

	public void setIpUnitsCost(BigDecimal ipUnitsCost) {
		this.ipUnitsCost = ipUnitsCost;
	}

	public String getPremiumFrequency() {
		return premiumFrequency;
	}

	public void setPremiumFrequency(String premiumFrequency) {
		this.premiumFrequency = premiumFrequency;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getCorpFundCode() {
		return corpFundCode;
	}

	public void setCorpFundCode(String corpFundCode) {
		this.corpFundCode = corpFundCode;
	}

	public BigDecimal getDeathMaxAmount() {
		return deathMaxAmount;
	}

	public void setDeathMaxAmount(BigDecimal deathMaxAmount) {
		this.deathMaxAmount = deathMaxAmount;
	}

	public BigDecimal getTpdMaxAmount() {
		return tpdMaxAmount;
	}

	public void setTpdMaxAmount(BigDecimal tpdMaxAmount) {
		this.tpdMaxAmount = tpdMaxAmount;
	}

	public BigDecimal getIpMaxAmount() {
		return ipMaxAmount;
	}

	public void setIpMaxAmount(BigDecimal ipMaxAmount) {
		this.ipMaxAmount = ipMaxAmount;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getIpCoverStartDate() {
		return ipCoverStartDate;
	}

	public void setIpCoverStartDate(String ipCoverStartDate) {
		this.ipCoverStartDate = ipCoverStartDate;
	}

	public String getTpdCoverStartDate() {
		return tpdCoverStartDate;
	}

	public void setTpdCoverStartDate(String tpdCoverStartDate) {
		this.tpdCoverStartDate = tpdCoverStartDate;
	}

	
}
