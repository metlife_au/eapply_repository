/* Change Cover Controller,Progressive and Mandatory validations Starts  */
VicsuperApp.controller('quote',['$scope', '$rootScope', '$routeParams', '$location','$http', '$timeout', '$window', 'ngDialog', 'fetchUrlSvc', 'fetchPersoanlDetailSvc', 'fetchQuoteSvc', 'fetchOccupationSvc', 'fetchDeathCoverSvc', 'fetchTpdCoverSvc', 'fetchIpCoverSvc', 'MaxLimitService', 'NewOccupationService', 'CalculateService', '$q', 'appData', 'fetchAppNumberSvc', 'saveEapplyData', 'printPageSvc', 'DownloadPDFService', 'auraRespSvc','propertyService', '$filter','APP_CONSTANTS',
function($scope, $rootScope, $routeParams, $location,$http, $timeout, $window, ngDialog, fetchUrlSvc, fetchPersoanlDetailSvc, fetchQuoteSvc, fetchOccupationSvc, fetchDeathCoverSvc, fetchTpdCoverSvc, fetchIpCoverSvc, MaxLimitService, NewOccupationService, CalculateService, $q, appData, fetchAppNumberSvc, saveEapplyData, printPageSvc, DownloadPDFService, auraRespSvc,propertyService, $filter,APP_CONSTANTS) {
	
	var  standard = 'general';
	var  whitecolor = 'white collar';
	var  ownoccupation = 'own occupation';
	var  professional = 'professional';
  
	/* Code for appD starts */
	  var quoteTracker = null;
	  if(ADRUM) {
	    quoteTracker = new ADRUM.events.VPageView();
	    quoteTracker.start();
	  }

	  $scope.$on('$destroy', function() {
	    quoteTracker.end();
	    ADRUM.report(quoteTracker);
	  });
	  /* Code for appD ends */
	
  $scope.QPD = {
    manageType: 'CCOVER',
    partnerCode: 'VICT',
    lastSavedOn: 'Quotepage',
    auraDisabled: true,
    email: null,
    contactType: null,
    contactPhone: null,
    contactPrefTime: null,
    dob: null,
    age: null,
    occupationDetails: {
      fifteenHr: null,
      citizenQue: null,
      industryCode: null,
      ownBussinessQues: null,
      ownBussinessYesQues: null,
      ownBussinessNoQues: null,
      withinOfficeQue: null,
      tertiaryQue: null,
      managementRoleQue: null,
      hazardousQue: null,
      gender: null,
      occRating1a:null, 
      occRating1b:null,
      occRating2:null, 
      salary: null,
      otherOccupation: null,
      occupation: null
    },
    auraDisabled: null,
    existingDeathAmt: '0',
    existingDeathUnits: '0',
    deathOccCategory: null,
    freqCostType: null,
    addnlDeathCoverDetails: {
      deathCoverName: null,
      deathCoverType: null,
      deathFixedAmt: null,
      deathInputTextValue: null,
      deathCoverPremium: null,
      indexationFlag:'Yes'
    },
    existingTpdAmt: '0',
    existingTPDUnits: '0',
    tpdOccCategory: null,
    addnlTpdCoverDetails: {
      tpdCoverName: null,
      tpdCoverType: null,
      tpdFixedAmt: null,
      tpdInputTextValue: null,
      tpdCoverPremium: null,
      indexationFlag:'Yes'
    },
    existingIPAmount: '0',
    existingIPUnits: null,
    ipOccCategory: null,
    ipcheckbox: null,
    indexationDeath: false,
    indexationTpd: false,
    ownOccuptionDeath:false,
    ownOccuptionTpd:false,
    ownOccuptionIp:'No',
    fulCheck: null,
    addnlIpCoverDetails: {
      ipCoverName: null,
      ipCoverType: 'IpUnitised',
      ipFixedAmt: null,
      ipInputTextValue: null,
      ipCoverPremium: null,
      waitingPeriod: null,
      benefitPeriod: null
    },
    totalPremium: 0,
    appNum: null,
    ackCheck: false,
    dodCheck: false,
    privacyCheck: false,
    ipDisclaimer:false
  };
  $scope.modelOptions = {updateOn: 'blur'};
	$scope.phoneNumbr = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
	$scope.emailFormat = APP_CONSTANTS.emailFormat;
	$scope.regex = /[0-9]{1,3}/;
  $scope.deathCvrRegex = /^\d{1,8}(,\d{1,8}){0,9}?$/;
	$scope.isDCCoverRequiredDisabled = false;
	$scope.isDCCoverTypeDisabled = false;
	$scope.isTPDCoverRequiredDisabled = false;
	$scope.isTPDCoverTypeDisabled = false;

	$scope.isWaitingPeriodDisabled = false;
	$scope.isBenefitPeriodDisabled = false;
	$scope.isIPCoverRequiredDisabled = false;
	
	$scope.dthBeyondAge = false;
    $scope.tpdBeyondAge = false;
    $scope.ipBeyondAge = false;
    
    $scope.dthWarnMsg = false;
    $scope.tpdWarnMsg = false;
    $scope.ipWarn1Msg = false;
	$scope.decreaseInd = false;

	$scope.isIpSalaryCheckboxDisabled = false;
	$scope.ipWarningFlag = false;
	$scope.ackFlag = false;
	$scope.modalShown = false;
	$scope.invalidSalAmount = false;
	$scope.dcIncreaseFlag = false;
	$scope.tpdIncreaseFlag = false;
	$scope.ipIncreaseFlag = false;
	$scope.QPD.auraDisabled = false;
	$scope.disclaimerFlag = true;
  $scope.IndustryOptions = null;
  $scope.OccupationList = null;
  	$scope.disableGender = false;
//	$scope.showWithinOfficeQuestion = false;
//  $scope.showTertiaryQuestion = false;
//  $scope.showHazardousQuestion = false;
//  $scope.showOutsideOfficeQuestion = false;
	$scope.urlList = fetchUrlSvc.getUrlList();
	$scope.QPD.freqCostType = 'Weekly';
	$scope.ipWarning = false;
	$scope.QPD.ipcheckbox = false;
	$scope.hideCoverOnNo = false;
	//$scope.manualIndexation = false;
	//$scope.QPD.eligibleFrStIp = false;

  $scope.QPD.isDeathDisabled = false;
  $scope.QPD.isTPDDisabled = false;
  $scope.QPD.isIPDisabled = false;
  $scope.prevOtherOcc = null;
  $scope.ipOwnOccButton = false;
  $scope.finalRating = 1;
  $scope.ipDiscFlag = false;
  
  $scope.QPD.ipOwnoccuption = false;
	/*Error Flags*/
	$scope.dodFlagErr = null;
	$scope.privacyFlagErr = null;
  $scope.deathErrorFlag = false;
  $scope.tpdErrorFlag = false;
  $scope.ipErrorFlag = false;
  //$scope.eligibleFrStIpFlag = false;
  $scope.tpdTapering = 1.00;
  $scope.QPD.isTPDIncreaseDisabled = false;
  $scope.occupationUpgraded = false;

  $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
  $scope.benefitPeriodOptions = ['2 Years','5 Years','Age 65'];
  $scope.premiumFrequencyOptions = ['Weekly', 'Monthly', 'Yearly'];
  $scope.contactTypeOptions = [{
    text: 'Home phone',
    value: '2'
  }, {
    text: 'Work phone',
    value: '3'
  }, {
    text: 'Mobile',
    value: '1'
  }];

  $scope.FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime','gender'];
  $scope.OccupationFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','occoffmangerating1A','occoffmangerating1B','occgovrating2','industry','occupation','annualSalary'];
  $scope.OccupationOtherFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
  $scope.CoverCalculatorFormFields =['coverName','coverType','requireCover','tpdCoverName','tpdCoverType','TPDRequireCover'];  
    
  $scope.personalDetails = {};
  $scope.contactDetails = {};
  $scope.isEmpty = function(value){
    return ((value == "" || value == null) || value == "0");
  };
  $scope.deathUnitsForFixed = 0;
  $scope.tpdUnitsForFixed = 0;
  $scope.deathUnitsForFixedOld = 0;
  $scope.tpdUnitsForFixedOld = 0;
  $scope.inputMsgOccRating;   
  
  $scope.init = function() {
    var defer = $q.defer();
    $scope.deathCoverDetails = fetchDeathCoverSvc.getDeathCover();
		$scope.tpdCoverDetails = fetchTpdCoverSvc.getTpdCover();
		$scope.ipCoverDetails = fetchIpCoverSvc.getIpCover();
		
		if($scope.deathCoverDetails.type == "2" || $scope.tpdCoverDetails.type == "2"){
			$scope.QPD.indexationDeath = $scope.deathCoverDetails.indexation;
			$scope.QPD.indexationTpd = $scope.tpdCoverDetails.indexation;
			if ($scope.deathCoverDetails.indexation == 'Y' || $scope.tpdCoverDetails.indexation == 'Y'){
				$scope.QPD.indexationDeath = 'true';
				$scope.QPD.indexationTpd = 'true';
			} else if ($scope.deathCoverDetails.indexation == 'N' && $scope.tpdCoverDetails.indexation == 'N'){
				$scope.QPD.indexationDeath = 'false';
				$scope.QPD.indexationTpd = 'false';
			} else {
				$scope.QPD.indexationDeath = 'true';
				$scope.QPD.indexationTpd = 'true';
			}
			if(!(parseInt($scope.deathCoverDetails.amount) > 0))
			{
				$scope.QPD.indexationDeath = 'true';
			}
			if(!(parseInt($scope.tpdCoverDetails.amount) > 0))
			{
				$scope.QPD.indexationTpd = 'true';
			}
		
		}
		
		
		
		var deathoccRating = $scope.deathCoverDetails.occRating;
		if (deathoccRating) {
			
			if(parseInt($scope.deathCoverDetails.amount) > 0  && (deathoccRating.toLowerCase() == ownoccupation))
				{
				$scope.QPD.ownOccuptionDeath = true;
				}
			
			if (deathoccRating.toLowerCase() == standard){
				deathoccRating = 1;
			}
			else if (deathoccRating.toLowerCase() == whitecolor || deathoccRating.toLowerCase() == ownoccupation){
				deathoccRating = 2;
			}
			else if (deathoccRating.toLowerCase() == professional){
				deathoccRating = 3;
			}
		}
		
		var tpdoccRating = $scope.tpdCoverDetails.occRating;
		if (tpdoccRating) {	
			if (parseInt($scope.tpdCoverDetails.amount) > 0  &&  (tpdoccRating.toLowerCase() == ownoccupation)){
				$scope.QPD.ownOccuptionTpd = true;
			}
			if (tpdoccRating.toLowerCase() == standard){
				tpdoccRating = 1;
			}
			else if (tpdoccRating.toLowerCase() == whitecolor || tpdoccRating.toLowerCase() == ownoccupation){
				tpdoccRating = 2;
			}
			else if (tpdoccRating.toLowerCase() == professional){
				tpdoccRating = 3;
			}
			
			
		}
		
		var ipoccRating = $scope.ipCoverDetails.occRating;
		if (ipoccRating) {	
			if (ipoccRating.toLowerCase() == ownoccupation){
				//$scope.QPD.ipOwnoccuption = true;
				 $scope.QPD.ownOccuptionIp = 'Yes';
				 $scope.QPD.fulCheck = true;
			}
			if (ipoccRating.toLowerCase() == standard){
				ipoccRating = 1;
			}
			else if (ipoccRating.toLowerCase() == whitecolor || ipoccRating.toLowerCase() == ownoccupation){
				ipoccRating = 2;
			}
			else if (ipoccRating.toLowerCase() == professional){
				ipoccRating = 3;
			}
			
			
		}
		
		$scope.inputMsgOccRating = Math.max(deathoccRating,tpdoccRating,ipoccRating);		
		
		
		$scope.QPD.deathOccCategory = $scope.inputMsgOccRating || 'General';
		$scope.QPD.tpdOccCategory = $scope.inputMsgOccRating || 'General';
		$scope.QPD.ipOccCategory = $scope.inputMsgOccRating || 'General';

    $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodOptions = ['2 Years','5 Years','Age 65'];
    
    
switch($scope.ipCoverDetails.waitingPeriod.toLowerCase().trim().substr(0,2)){
	
	case "30":
		$scope.ipCoverDetails.waitingPeriod = "30 Days";
		break;
	case "60":
		$scope.ipCoverDetails.waitingPeriod = "60 Days";
		break;
	case "90":
		$scope.ipCoverDetails.waitingPeriod = "90 Days";
		break;
	}
switch($scope.ipCoverDetails.benefitPeriod.toLowerCase().trim().substr(0,1)){
	
	case "2":
		$scope.ipCoverDetails.benefitPeriod = "2 Years";
		break;
	case "5":
		$scope.ipCoverDetails.benefitPeriod = "5 Years";
		break;
	case "A":
		$scope.ipCoverDetails.benefitPeriod = "Age 65";
		break;
	case "a":
		$scope.ipCoverDetails.benefitPeriod = "Age 65";
		break;
	}
    
    $scope.QPD.addnlIpCoverDetails.waitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.waitingPeriod) > -1 ? $scope.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
    $scope.QPD.addnlIpCoverDetails.benefitPeriod = $scope.benefitPeriodOptions.indexOf($scope.ipCoverDetails.benefitPeriod) > -1 ? $scope.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';
    
    
    if($scope.deathCoverDetails.type == "1") {
    	$scope.setUnitMaxLen = 3;
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = "DcUnitised";      
      $scope.QPD.exDcCoverType = "DcUnitised";
      $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = $scope.deathCoverDetails.units;
      $scope.QPD.existingDeathAmt = $scope.deathCoverDetails.amount || 0;
      $scope.QPD.existingDeathUnits = $scope.deathCoverDetails.units || 0;
      $timeout(function() {
        showhide('nodollar1','dollar1');
        showhide('nodollar','dollar');
      });
      
    } else if($scope.deathCoverDetails.type == "2") {
    	$scope.setUnitMaxLen = 8;
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = "DcFixed";
      $scope.QPD.exDcCoverType = "DcFixed";
      $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = $scope.deathCoverDetails.amount;
      $scope.QPD.existingDeathAmt = $scope.deathCoverDetails.amount || 0;
      $scope.isDCCoverTypeDisabled = false;
      $timeout(function() {
    	  showhide('dollar1','nodollar1');
          showhide('dollar','nodollar');
      });
      
    }

    if($scope.tpdCoverDetails.type == "1") {
    	$scope.setUnitTpdMaxLen = 3;
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = "TPDUnitised";
      $scope.QPD.exTpdCoverType = "TPDUnitised";
      $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdCoverDetails.units;
      $scope.QPD.existingTpdAmt = $scope.tpdCoverDetails.amount || 0;
      $scope.QPD.existingTPDUnits = $scope.tpdCoverDetails.units || 0;
      $timeout(function() {
        showhide('nodollar1','dollar1');
        showhide('nodollar','dollar');
      });
      
    } else if($scope.tpdCoverDetails.type == "2") {
    	$scope.setUnitTpdMaxLen = 8;
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = "TPDFixed";
      $scope.QPD.exTpdCoverType = "TPDFixed";
      $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdCoverDetails.amount;
      $scope.QPD.existingTpdAmt = $scope.tpdCoverDetails.amount || 0;
      $scope.isTPDCoverTypeDisabled = false;
      $timeout(function() {
    	  showhide('dollar1','nodollar1');
          showhide('dollar','nodollar');
      });
    }
    
    $scope.QPD.addnlIpCoverDetails.ipInputTextValue = $scope.ipCoverDetails.units;
    $scope.QPD.existingIPAmount = $scope.ipCoverDetails.amount || 0;
    $scope.QPD.existingIPUnits = $scope.ipCoverDetails.units;
    $scope.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
    
    $scope.promotioncode = $scope.inputDetails.logOutUrl;
    
    $scope.personalDetails = $scope.inputDetails.personalDetails || {};
    $scope.contactDetails = $scope.inputDetails.contactDetails || {};
    
    fetchQuoteSvc.getList($scope.urlList.quoteUrl,"VICT").then(function(res){
			$scope.IndustryOptions = res.data;
		}, function(err){
			console.info("Error while fetching industry list " + JSON.stringify(err));
		});
    //caluclating the current age -vicsuper changes
    $scope.QPD.age = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
    
   switch($scope.QPD.age){
    
    case 61 :
    	$scope.tpdTapering = 0.90;
    	break;
    case 62 :
    	$scope.tpdTapering = 0.80;
    	break;
    case 63 :
    	$scope.tpdTapering = 0.70;
    	break;
    case 64 :
    	$scope.tpdTapering = 0.60;
    	break;
    case 65 :
    	$scope.tpdTapering = 0.50;
    	break;
    case 66 :
    	$scope.tpdTapering = 0.40;
    	break;
    case 67 :
    	$scope.tpdTapering = 0.30;
    	break;
    case 68 :
    	$scope.tpdTapering = 0.20;
    	break;
    case 69 :
    	$scope.tpdTapering = 0.20;
    	break;
    
    }
   
   var tpdValidAge=64;
	if(parseInt($scope.tpdCoverDetails.amount) > 0 )
		{
		tpdValidAge = 69;
		}

    if($scope.deathCoverDetails && $scope.deathCoverDetails.benefitType && $scope.deathCoverDetails.benefitType == 1 ){
			if($scope.QPD.age < 14 || $scope.QPD.age > 69){
				$('#deathsection').removeClass('active');
				$("#death").css("display", "none");
//				$scope.dthBeyondAge = true;			
//				if (!$scope.deathCoverDetails.amount || $scope.deathCoverDetails.amount == '0' 
//					|| $scope.deathCoverDetails.amount =='' || $scope.deathCoverDetails.amount == '0.0'){
					$scope.QPD.isDeathDisabled = true;
					$scope.QPD.addnlDeathCoverDetails.deathInputTextValue = 0;
			//	}
			} 
		}
		if($scope.tpdCoverDetails && $scope.tpdCoverDetails.benefitType && $scope.tpdCoverDetails.benefitType == 2 ){
			/*if($scope.QPD.age < 14 || $scope.QPD.age > 64){*/
			if($scope.QPD.age < 14 || $scope.QPD.age > tpdValidAge){
				$('#tpdsection').removeClass('active');
				$("#tpd").css("display", "none");
//				$scope.tpdBeyondAge = true;
//				if (!$scope.tpdCoverDetails.amount || $scope.tpdCoverDetails.amount == '0' 
//					|| $scope.tpdCoverDetails.amount =='' || $scope.tpdCoverDetails.amount == '0.0'){
					$scope.QPD.isTPDDisabled = true;
					$scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = 0;					 
				//}
			}
			if($scope.QPD.age < 14 || $scope.QPD.age > 64){
				$scope.QPD.isTPDIncreaseDisabled = true;
			}
		}
		if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitType && $scope.ipCoverDetails.benefitType == 4 ){
			if($scope.QPD.age < 14 || $scope.QPD.age > 64){
				$('#ipsection').removeClass('active');
				$("#sc").css("display", "none");
//				$scope.ipBeyondAge = true;
//				if (!$scope.ipCoverDetails.amount || $scope.ipCoverDetails.amount == '0' 
//					|| $scope.ipCoverDetails.amount =='' || $scope.ipCoverDetails.amount == '0.0'){
					$scope.QPD.isIPDisabled = true;
					$scope.QPD.addnlIpCoverDetails.ipInputTextValue = 0;
			//	}
			} 
		}
		MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl, "VICT", $scope.inputDetails.memberType, "CCOVER").then(function(res){
			var limits = res.data;
			$scope.annualSalForUpgradeVal = limits[0].annualSalForUpgradeVal;
    	$scope.DCMaxAmount = limits[0].deathMaxAmount;
    	$scope.TPDMaxAmount = limits[0].tpdMaxAmount;
    	$scope.IPCvrMaxAmount = limits[0].ipMaxAmount;    	
    	$scope.IpUnitCostMulitiplier = limits[0].ipUnitCostMulitiplier;
    	$scope.IPMaxAmount = $scope.IPCvrMaxAmount/$scope.IpUnitCostMulitiplier;
      defer.resolve(res);
		}, function(error){
			console.info('Something went wrong while fetching limits ' + error);
      defer.reject(err);
		});

    return defer.promise;
  };

  $scope.init().then(function() {
    angular.extend($scope.QPD, appData.getAppData());
    /*if($scope.QPD.occupationDetails.occupation.has('Other')){
    	$scope.QPD.occupationDetails.occupation = 
    }
    if($scope.QPD.occupationDetails.occupation != undefined){
    var occupation = $scope.QPD.occupationDetails.occupation.split('-');
    if(occupation[0] == 'Other'){
    	$scope.QPD.occupationDetails.occupation = occupation[0];
    	$scope.QPD.occupationDetails.otherOccupation = occupation[1];
    }else {
    	$scope.QPD.occupationDetails.occupation = occupation[0];
    }
   }*/
    
    if($scope.QPD.ownOccuptionTpd)
    	{
    	$scope.QPD.ownOccuptionTpd = true;
    	}
    else
    	{
    	$scope.QPD.ownOccuptionTpd = false;
    	}
    if($scope.QPD.ownOccuptionDeath)
    	{
    	$scope.QPD.ownOccuptionDeath = true;
    	}
    else
    	{
    	$scope.QPD.ownOccuptionDeath = false;
    	}
    if($scope.QPD.contactType == null)
    	{
    $scope.QPD.contactType = $scope.contactDetails.prefContact || '1';
    	}
    if($scope.QPD.contactType == "1") {
      $scope.QPD.contactPhone = $scope.QPD.contactPhone?$scope.QPD.contactPhone:$scope.contactDetails.mobilePhone;
    } else if($scope.QPD.contactType == "2") {
      $scope.QPD.contactPhone = $scope.QPD.contactPhone?$scope.QPD.contactPhone:$scope.contactDetails.homePhone;
    } else if($scope.QPD.contactType == "3") {
      $scope.QPD.contactPhone = $scope.QPD.contactPhone?$scope.QPD.contactPhone:$scope.contactDetails.workPhone;
    } else {
      $scope.QPD.contactPhone = '';
    }
    
    if($scope.QPD.occupationDetails.gender && (($scope.QPD.occupationDetails.gender).toLowerCase() === 'female' || ($scope.QPD.occupationDetails.gender).toLowerCase() === 'male' ))
    	{
    	$scope.disableGender = true;
    	}
    else
    	{
    	$scope.disableGender = false;
    	}
    
    $scope.QPD.dob = moment($scope.QPD.dob, "DD/MM/YYYY").format('DD/MM/YYYY');
    if($scope.QPD.occupationDetails.fifteenHr == 'No') {
      $scope.disableIpCover();
    }
    if($scope.QPD.occupationDetails.industryCode) {
      fetchOccupationSvc.getOccupationList($scope.urlList.occupationUrl, "VICT", $scope.QPD.occupationDetails.industryCode).then(function(res){
        $scope.OccupationList = res.data;
        $scope.getCategoryFromDB();
      }, function(err){
        console.info("Error while fetching occupations " + JSON.stringify(err));
      });
    }
    $scope.QPD.ipcheckbox = ($scope.QPD.ipcheckbox == 'true' || $scope.QPD.ipcheckbox == true) ? true : false;
    if($scope.QPD.ipcheckbox) {
      $scope.insureEightyFivePercentIp();
    	$('#ipsalarycheck').parent().addClass('active');
		  $('#ipsalarycheck').attr('checked','checked');
    }
//    if ($scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65' || $scope.QPD.addnlIpCoverDetails.benefitPeriod == '5 Years'){
//    	$scope.enableIpDisclaimerSec = true;
//    	$scope.ipOwnoccuption = true;
//	    if ($scope.QPD.ipDisclaimer){	    	
//	    	$scope.ipDisclFlagErr = false;
//	    	$('#ipDisclaimer').parent().addClass('active');
//			  $('#ipDisclaimer').attr('checked','checked');
//	    } else {
//	    	$scope.ipDisclFlagErr = true;
//	    }
//    } else {
//    	$scope.enableIpDisclaimerSec = false;
//    	$scope.ipOwnoccuption = false;
//    }
    $scope.ipDiscFlag = $scope.QPD.ipDisclaimer;
    $scope.checkBenefitPeriod();
    
    
    if($scope.ipDiscFlag && (parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.QPD.existingIPUnits)))
    	{
    	$scope.ipDisclFlagErr = false;
    	$scope.QPD.ipDisclaimer = true;
    	$('#ipDisclaimer').parent().addClass('active');
		  $('#ipDisclaimer').attr('checked','checked');
    	} 
    else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.QPD.existingIPUnits))
    	{
    	$scope.ipDisclFlagErr = true;
    	}
    
    $scope.$watch('QPD.dodCheck', function(newVal, oldVal) {
      $scope.togglePrivacy(newVal);
      $scope.toggleContact(newVal && $scope.QPD.privacyCheck);
      $scope.toggleOccupation(newVal && $scope.formOne.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
      $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.occupationForm.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });

    $scope.$watch('QPD.privacyCheck', function(newVal, oldVal) {
      $scope.toggleContact(newVal);
      $scope.toggleOccupation(newVal && $scope.formOne.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
      $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.occupationForm.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });

    $scope.$watch('formOne.$valid', function(newVal, oldVal) {
       $scope.toggleOccupation(newVal && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
       $scope.toggleCoverCalc(newVal && $scope.occupationForm.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });

    $scope.$watch('occupationForm.$valid', function(newVal, oldVal) {
       $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck && !$scope.hideCoverOnNo);
    });
    $scope.validateDeathTpdIp();
    if( $scope.QPD.addnlDeathCoverDetails.deathCoverType === 'DcUnitised')
    {
    	 $timeout(function() {
    	        showhide('nodollar1','dollar1');
    	        showhide('nodollar','dollar');
    	      },500);
    }
    if( $scope.QPD.addnlDeathCoverDetails.deathCoverType === 'DcFixed')
    {
    	 $timeout(function() {
    		 	showhide('dollar1','nodollar1');
    		 	showhide('dollar','nodollar');
    	      },500);
    }
	
    //$scope.toggleIPCondition();
  }, function() {

  });
  
  $scope.changePrefContactType = function() {
    if($scope.QPD.contactType == "1") {
			$scope.QPD.contactPhone = $scope.contactDetails.mobilePhone;
		} else if($scope.QPD.contactType == "2") {
			$scope.QPD.contactPhone = $scope.contactDetails.homePhone;
		} else if($scope.QPD.contactType == "3") {
			$scope.QPD.contactPhone = $scope.contactDetails.workPhone;
		} else {
      $scope.QPD.contactPhone = '';
    }
  };

  $scope.isCollapsible = function(targetEle, event) {
    var dodLabelCheck = $('#dodLabel').hasClass('active');
    var privacyLabelCheck = $('#privacyLabel').hasClass('active');
    if( targetEle == 'collapseprivacy' && !dodLabelCheck) {
      if($('#dodLabel').is(':visible'))
        $scope.dodFlagErr = true;
      event.stopPropagation();
      return false;
    } else if( targetEle == 'collapseOne' && (!dodLabelCheck || !privacyLabelCheck)) {
      if($('#privacyLabel').is(':visible'))
        $scope.privacyFlagErr = true;
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseTwo' && (!dodLabelCheck || !privacyLabelCheck || $("#collapseOne form").hasClass('ng-invalid'))) {
      if($("#collapseOne form").is(':visible'))
        $scope.onFormContinue($scope.formOne);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseThree' && (!dodLabelCheck || !privacyLabelCheck || $scope.hideCoverOnNo || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))) {
      if($("#collapseTwo form").is(':visible'))
        $scope.onFormContinue($scope.occupationForm);
      event.stopPropagation();
      return false;
    }
  }
  
  $scope.toggleOccupation = function(checkFlag) {
      if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseTwo']").click(); /* Can be improved */
  };

  $scope.toggleCoverCalc = function(checkFlag) {
      if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseThree']").click(); /* Can be improved */
  };

  $scope.togglePrivacy = function(checkFlag) {
      if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
  };

  $scope.toggleContact = function(checkFlag) {
      if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseOne']").click(); /* Can be improved */

  };
  $scope.checkDodState = function() {
    $timeout(function() {
      var dodLabelCheck = $('#dodLabel').hasClass('active');
      $scope.QPD.dodCheck = dodLabelCheck;
      $scope.dodFlagErr = !dodLabelCheck;
      if(dodLabelCheck) {
        $scope.togglePrivacy(true);
      } else {
        $scope.togglePrivacy(false);
        $scope.toggleContact(false);
        $scope.toggleOccupation(false);
        $scope.toggleCoverCalc(false);
      }
    }, 1);
  };
  
  
  $scope.checkPrivacyState  = function() {
    $timeout(function() {
      var privacyLabelCheck = $('#privacyLabel').hasClass('active');
      $scope.QPD.privacyCheck = privacyLabelCheck;
      $scope.privacyFlagErr = !privacyLabelCheck;
      if(privacyLabelCheck) {
        $scope.toggleContact(true);
      } else {
        $scope.toggleContact(false);
        $scope.toggleOccupation(false);
        $scope.toggleCoverCalc(false);
      }
    }, 1);
  };
  
  $scope.checkIpDisclaimer = function() {
	  $timeout(function() { 
		  var ipdisclaimerCheck = $('#ipDisclaimerLabel').hasClass('active');	 
		  $scope.QPD.ipDisclaimer = ipdisclaimerCheck;
	      $scope.ipDisclFlagErr = !ipdisclaimerCheck;     
	  }, 1);
  };
  
  $scope.clickToOpen = function (hhText) {

	var dialog = ngDialog.open({
		template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
		className: 'ngdialog-theme-plain',
		plain: true
	});
	dialog.closePromise.then(function (data) {
		//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
	});
  };
  
  $scope.clickToOpenCCoverInfo = function (hhText) {

		var dialog = ngDialog.open({
			template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Change cover</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --><div class="row"><div class="col-sm-12 col-xs-12" style="text-align:center;"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Ok</button></div><div class="col-sm-4"></div></div></div></div>',
			className: 'ngdialog-theme-plain',
			plain: true
		});
		dialog.closePromise.then(function (data) {
			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	  };
 $scope.cancelPopUpMsg = function (hhText) {

			ngDialog.openConfirm({
				template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Change cover</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --><div class="row"><div class="col-sm-12 col-xs-12" style="text-align:center;"><button type="button" class="btn btn-primary mr10px pr20px avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Continue</button><button type="button" class="btn btn-primary mr10px pr20px avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div><div class="col-sm-4"></div></div></div></div>',
				plain: true,
		        className: 'ngdialog-theme-plain custom-width'
			}).then(function(){
				$scope.goLogin();
		    }, function(e){
		      if(e=='oncancel'){
		        return false;
		      }
		    });
		  };

$scope.goLogin = function() {
		   		$window.location.href = $scope.promotioncode;
		    };	  
  $scope.checkPreviousMandatoryFields  = function (elementName,formName) {
    var formFields;
    if(formName == 'formOne') {
      formFields = $scope.FormOneFields;
    } else if(formName == 'occupationForm') {
//      if($scope.QPD.occupationDetails.occupation != undefined && $scope.QPD.occupationDetails.occupation == 'Other') {
//        formFields = $scope.OccupationOtherFormFields;
//      } else {
        formFields = $scope.OccupationFormFields;
   //   }
      /*Vicsuper - occupation rating changes starts*/
      if(elementName == 'occoffmangerating1A' && $scope.QPD.occupationDetails.occRating1a == 'Yes') {
    	  $scope.QPD.occupationDetails.occRating1b = 'No'
      }
      if(elementName == 'occoffmangerating1B' && $scope.QPD.occupationDetails.occRating1b == 'Yes') {
    	  $scope.QPD.occupationDetails.occRating1a = 'No'
      }
      if (elementName == 'occoffmangerating1A' || elementName == 'occoffmangerating1B' || elementName == 'occgovrating2') {
    	  $scope.renderOccupationQuestions();
      }
      /*Vicsuper - occupation rating changes ends*/
    } else if(formName == 'coverCalculatorForm') {
      formFields = $scope.CoverCalculatorFormFields;
    }
    var inx = formFields.indexOf(elementName);
    if(inx > 0){
      for(var i = 0; i < inx ; i++) {
        if($scope[formName][formFields[i]])
          $scope[formName][formFields[i]].$touched = true;
      }
    }
  };
  
  
  $scope.checkForIpIncrease = function()
  {
	  $scope.ipNocoverFlag = false;  
	  if(($scope.QPD.occupationDetails.fifteenHr == 'No' /*|| $scope.ipEligFlag*/)
			&& parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.ipCoverDetails.units))
		  {
		  $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
		  }
	  else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) == 0)
		  {
		  $scope.ipNocoverFlag = true;
		  	$scope.QPD.addnlIpCoverDetails.waitingPeriod =  $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.waitingPeriod) > -1 ? $scope.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
			$scope.QPD.addnlIpCoverDetails.benefitPeriod = $scope.benefitPeriodOptions.indexOf($scope.ipCoverDetails.benefitPeriod) > -1 ? $scope.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';
			$scope.checkBenefitPeriod();
		  }
	  else
		  {
		  $scope.validateDeathTpdIp();
		  }
	  
  };

  $scope.disableIpCover = function() {
	  $scope.QPD.addnlIpCoverDetails.ipCoverName = 'No change';
	  $scope.QPD.ipcheckbox = false;
	  if($scope.QPD.addnlIpCoverDetails.ipInputTextValue != null && parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) >= parseInt($scope.ipCoverDetails.units))
		  {
		  $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
		  }
	  
	  $scope.isIPCoverNameDisabled = true; 
  
  /*$scope.isIPCoverRequiredDisabled = true;*/
	  $scope.isIpSalaryCheckboxDisabled = true;
	  $scope.ipWarning = true;
	  $('#ipsalarycheck').removeAttr('checked');
	  $('#ipsalarychecklabel').removeClass('active');
  
  if($scope.QPD.addnlIpCoverDetails.waitingPeriod === '30 Days')
  	{
  	$scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
  	}
  else if($scope.QPD.addnlIpCoverDetails.waitingPeriod === '60 Days')
  	{
  	$scope.waitingPeriodOptions = ["60 Days", "90 Days"];
  	}
  else
  	{
  	$scope.isWaitingPeriodDisabled = true;
  	}
  if($scope.QPD.addnlIpCoverDetails.benefitPeriod === 'Age 65')
		{
  	$scope.benefitPeriodOptions = ['2 Years','5 Years','Age 65'];
		}
  else if($scope.QPD.addnlIpCoverDetails.benefitPeriod === '5 Years')
		{
  	$scope.benefitPeriodOptions = ['2 Years','5 Years'];
		}
  else
		{
  	$scope.isBenefitPeriodDisabled = true;
		}
  }
  $scope.toggleIPCondition = function() {
    if($scope.QPD.isIPDisabled)
      return false;
    if($scope.QPD.occupationDetails.fifteenHr == 'No') {
      $scope.disableIpCover();
    } else {
      $scope.QPD.addnlIpCoverDetails.ipCoverName = '';
      $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
      $scope.isIPCoverNameDisabled = false;
      $scope.isWaitingPeriodDisabled = false;
      $scope.isBenefitPeriodDisabled = false;
      $scope.isIPCoverRequiredDisabled = false;
      $scope.isIpSalaryCheckboxDisabled = false;
      $scope.ipWarning = false;
      $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
      $scope.benefitPeriodOptions = ['2 Years','5 Years','Age 65'];
    }
    $scope.renderOccupationQuestions();
//    $scope.QPD.occupationDetails.withinOfficeQue = null;
//    $scope.QPD.occupationDetails.tertiaryQue = null;
//    $scope.QPD.occupationDetails.hazardousQue = null;
//    $scope.QPD.occupationDetails.managementRoleQue = null;
  };

  
  $scope.citizenshipChange = function(){
		$scope.hideCoverOnNo = false;
		if($scope.QPD.occupationDetails.citizenQue == 'No')
			{
				$scope.hideCoverOnNo = true;
				$scope.toggleCoverCalc(false);
			}
		else if($scope.QPD.occupationDetails.citizenQue == 'Yes' && this.occupationForm.$valid)
			{
				$scope.toggleCoverCalc(true);
			}
	};
  
  $scope.checkAnnualSalary = function() {
	  // Made chages specific to vicsuper,have to recheck this code
	  $scope.invalidSalAmount = false;
	 if ($scope.QPD.occupationDetails.salary && parseInt($scope.QPD.occupationDetails.salary) > 1000000 ){
		 $scope.occupationForm.$valid = false;
		 $scope.invalidSalAmount = true;
		 return;
	 } 	  
//    if(parseInt($scope.QPD.occupationDetails.salary) == 0){
//      $scope.invalidSalAmount = true;      
//    } else{
//      $scope.invalidSalAmount = false;      
//    }
    //if(!$scope.invalidSalAmount)
	 
      $scope.renderOccupationQuestions();

    if($scope.QPD.ipcheckbox) {
      $scope.insureEightyFivePercentIp();      
    }
	else if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid)
    	  {
    	  $scope.validateDeathTpdIpAmounts();
    	  }
  }

  $scope.insureEightyFivePercentIp = function() {
    $scope.ipWarningFlag = false;
    $timeout(function() {
      $scope.QPD.ipcheckbox = $('#ipsalarycheck').prop('checked');
      if($scope.QPD.ipcheckbox == true) {
    	 var eightyFiveAmnt =  Math.ceil(parseFloat((0.85 * ($scope.QPD.occupationDetails.salary/12)).toFixed(2)) / 100)*100;
    	 var eightyFiveInUnits = Math.ceil(eightyFiveAmnt/$scope.IpUnitCostMulitiplier);
        $scope.QPD.addnlIpCoverDetails.ipInputTextValue  = eightyFiveInUnits;
        if($scope.QPD.addnlIpCoverDetails.ipInputTextValue > $scope.IPMaxAmount) {
          $scope.QPD.addnlIpCoverDetails.ipInputTextValue = $scope.IPMaxAmount;
          $scope.ipWarningFlag = true;
        } else if($scope.QPD.addnlIpCoverDetails.ipInputTextValue < parseInt($scope.ipCoverDetails.units)) {
            $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
        } else {
          $scope.ipWarningFlag = false;
        }
        $scope.isIPCoverRequiredDisabled = true;
        
      } else {
        $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
        $scope.isIPCoverRequiredDisabled = false;
        $scope.ipWarningFlag = false;
      }      
      $scope.validateDeathTpdIp();
    });
  };
  
  $scope.checkOwnBusinessQuestion = function() {
    $scope.QPD.occupationDetails.ownBussinessYesQues = null;
    $scope.QPD.occupationDetails.ownBussinessNoQues = null;
    if($scope.QPD.occupationDetails.ownBussinessQues == 'Yes'){
      $scope.OccupationFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
      $scope.OccupationOtherFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
    } else if($scope.QPD.occupationDetails.ownBussinessQues == 'No'){
      $scope.OccupationFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
      $scope.OccupationOtherFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','otherOccupation','annualSalary'];
    }
  };

  $scope.getOccupations = function() {
    $scope.QPD.occupationDetails.occupation = "";
    $scope.QPD.occupationDetails.otherOccupation = '';
    $scope.QPD.occupationDetails.industryCode = $scope.QPD.occupationDetails.industryCode == '' ? null : $scope.QPD.occupationDetails.industryCode;
    fetchOccupationSvc.getOccupationList($scope.urlList.occupationUrl, "VICT", $scope.QPD.occupationDetails.industryCode).then(function(res){
      $scope.OccupationList = res.data;
    }, function(err){
      console.info("Error while fetching occupations " + JSON.stringify(err));
    });
  };
  
  $scope.getOtherOccupationAS = function(entered) {

	    return $http.get('./occupation.json').then(function(response) {
	      $scope.occupationList=[];
        if(response.data.Other) {
	        for (var key in response.data.Other) {
	              var obj={};
	              obj.id=key;
	               obj.name=response.data.Other[key];
                 //if(obj.name.indexOf(entered) > -1) {
                  $scope.occupationList.push(obj.name);
                 //}
	               
	        }
	      }
        return $filter('filter')($scope.occupationList, entered);
	    }, function(err){
	      console.info("Error while fetching occupations " + JSON.stringify(err));
	    });
	    
	  };
	  
	  $scope.toggleIndexation = function(val) {
		  
		  //$scope.manualIndexation = true;
		  if (val == 'true'){
			  $scope.QPD.indexationTpd = 'true';
			  $scope.QPD.indexationDeath = 'true';
		  } else {
			  $scope.QPD.indexationTpd = 'false';
			  $scope.QPD.indexationDeath = 'false';
		  }
		  
	  }  
	  
	  $scope.toggleOwnOccupation = function(val) {		  
		  
		 /* if (val == 'true' || val == true){
			  $scope.QPD.ownOccuptionIp = 'true';		
			  if ($scope.inputMsgOccRating != 3 && $scope.ipCoverDetails.occRating != 2){
				  $scope.QPD.ownOccuptionIp = 'false';
				  $scope.QPD.ipOwnoccuption = false;
			  }	
		  } else {
			  $scope.QPD.ownOccuptionIp = 'false';			  		  
		  }*/
		  
		  $scope.QPD.ownOccuptionIp = 'No';
			var val1 = "Yes";
			if(val == 'Yes')
				{
				$scope.QPD.ownOccuptionIp = 'Yes';
				val1="No";
				}
			var radiosIp = $('label[radio-syncip]');
	    	var dataIp = $('input[data-syncip]');
	    	dataIp.filter('[data-syncip="' + val + '"]').attr('checked','checked');
			radiosIp.filter('[radio-syncip="' + val1 + '"]').removeClass('active');
			radiosIp.filter('[radio-syncip="' + val + '"]').addClass('active');
			
		  $scope.QPD.fulCheck = $scope.QPD.ownOccuptionIp == 'Yes'?true:false;
	      $scope.renderOccupationQuestions();
	      $scope.validateDeathTpdIp();
	  } 

  $scope.syncRadios = function(val) {
    var radios = $('label[radio-sync]');
    var data = $('input[data-sync]');
    data.filter('[data-sync="' + val + '"]').attr('checked','checked');
    if(val == 'Fixed') {    	
      $scope.deathErrorFlag = false;
      $scope.QPD.deathErrorMsg="";
      $scope.tpdErrorFlag = false;
      $scope.QPD.tpdErrorMsg ="";
      showhide('dollar1','nodollar1');
      showhide('dollar','nodollar');
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = 'DcFixed';
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = 'TPDFixed';
      if($scope.deathCoverDetails.type == "2" && $scope.tpdCoverDetails.type == "2" && ((parseInt($scope.deathCoverDetails.amount) + parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)) > 0)
    		  && ((parseInt($scope.tpdCoverDetails.amount) + parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue)) > 0))
    	  {
    	  $scope.QPD.indexationDeath = $scope.deathCoverDetails.indexation === 'Y'?'true':'false';
    	  $scope.QPD.indexationTpd = $scope.tpdCoverDetails.indexation === 'Y'?'true':'false';
    	  
    	  }
      else
    	  {
    	 // $scope.manualIndexation = false;
    	  $scope.QPD.indexationTpd = 'true';
    	  $scope.QPD.indexationDeath = 'true';
    	  }
    } else if(val == 'Unitised') {    	
      $scope.deathErrorFlag = false;
      $scope.QPD.deathErrorMsg="";
      $scope.tpdErrorFlag = false;
      $scope.QPD.tpdErrorMsg ="";
      showhide('nodollar1','dollar1');
      showhide('nodollar','dollar');
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = 'DcUnitised';
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = 'TPDUnitised';
      
      $scope.QPD.indexationTpd = 'false';
	  $scope.QPD.indexationDeath = 'false';
      
    }
    if (val == 'Fixed' || val == 'Unitised'){
    	radios.removeClass('active');
    	radios.filter('[radio-sync="' + val + '"]').addClass('active');
    }
    $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = null;
    $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = null;
    $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = val == 'Unitised' ? $scope.deathCoverDetails.units : $scope.deathCoverDetails.amount;
    $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = val == 'Unitised' ? $scope.tpdCoverDetails.units : $scope.tpdCoverDetails.amount;
    if($scope.deathCoverDetails.type == "2" && $scope.tpdCoverDetails.type == "2")
    	{
    	$scope.QPD.addnlDeathCoverDetails.deathInputTextValue = val == 'Unitised' ? $scope.deathUnitsForFixed : $scope.deathCoverDetails.amount;
    	$scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = val == 'Unitised' ? $scope.tpdUnitsForFixed : $scope.tpdCoverDetails.amount;
    	$scope.deathUnitsForFixedOld = $scope.deathUnitsForFixed;
    	$scope.tpdUnitsForFixedOld = $scope.tpdUnitsForFixed;
    	}
    $scope.validateDeathTpdIp();
  };

  $scope.navigateToLandingPage = function() {
    ngDialog.openConfirm({
        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
    }).then(function(){
      $location.path("/landing");
    }, function(e){
      if(e=='oncancel'){
        return false;
      }
    });
  };

  $scope.getCategoryFromDB = function(fromSelect) {
	  
	  if(fromSelect && $scope.QPD.occupationDetails.occupation!=="Other")
		  {
		  $scope.QPD.occupationDetails.otherOccupation = '';
		  }
	  if( $scope.prevOtherOcc !== $scope.QPD.occupationDetails.otherOccupation)
	   {
    if($scope.QPD.occupationDetails.occupation != undefined || $scope.QPD.occupationDetails.otherOccupation != undefined) {
      if(fromSelect) {
        $scope.QPD.occupationDetails.withinOfficeQue = null;
        $scope.QPD.occupationDetails.tertiaryQue = null;
        $scope.QPD.occupationDetails.hazardousQue = null;
        $scope.QPD.occupationDetails.managementRoleQue = null;                                                     
      }
      var occName = $scope.QPD.occupationDetails.industryCode + ":" + $scope.QPD.occupationDetails.occupation;
      
//      NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "HOST", occName).then(function(res){
//        if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcFixed"){
//          $scope.QPD.deathDBCategory = res.data[0].deathfixedcategeory;
//        }else if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised"){
//          $scope.QPD.deathDBCategory = res.data[0].deathunitcategeory;
//        }
//        if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed"){
//          $scope.QPD.tpdDBCategory = res.data[0].tpdfixedcategeory;
//        }else if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"){
//          $scope.QPD.tpdDBCategory = res.data[0].tpdunitcategeory;
//        }
//        $scope.QPD.ipDBCategory = res.data[0].ipfixedcategeory;
//        $scope.eligibleForStIp();
//        $scope.renderOccupationQuestions();
//      }, function(err){
//        console.info("Error while getting category from DB " + JSON.stringify(err));
//      });
    }
  }
  };

  $scope.eligibleForStIp = function(){
	/*occ = $scope.QPD.occupationDetails.occupation;*/
	  if($scope.QPD.occupationDetails.occupation!=="Other"){
		  occ = $scope.QPD.occupationDetails.occupation;
	  }else{
		  occ = $scope.QPD.occupationDetails.otherOccupation; 
	  }
	var occSearch = true;
	 propertyService.getOccList().then(function(response){
		 $scope.decOccList = response.data;
//		 angular.forEach($scope.decOccList,function(value,key){
//			  if(occSearch){
//	    		  if(key.toLowerCase() === occ.toLowerCase()){
//	    			  if(value.toLowerCase() == 'ip'){
//	    				  $scope.QPD.eligibleFrStIp = true;
//		    			  occSearch = false;
//	    			  }
//	    		  }else{
//	    			  $scope.QPD.eligibleFrStIp = false;
//			      }
//			  }
//		 });
    	  if($scope.QPD.eligibleFrStIp && $scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65'){
    		  $scope.eligibleFrStIpFlag = true;
    	  }else{
    		  $scope.eligibleFrStIpFlag = false;
    	  }
	});
  };
  
  $scope.calculateOnChange = function(){
    if(this.formOne && this.formOne.$valid && this.occupationForm && this.occupationForm.$valid && this.coverCalculatorForm && this.coverCalculatorForm.$valid){
    	 if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag /**&& !$scope.eligibleFrStIpFlag**/){
    		 $scope.calculate();
    	 }
    }
  };

  $scope.renderOccupationQuestions = function() {
  
        $scope.OccupationFormFields = ['fourteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
                
        var occQustnsRating = 1; 
        var overAllOccRating = 1;
        $scope.ipOwnOccButton = false;
    	if (($scope.QPD.occupationDetails.occRating1a 
    			&& $scope.QPD.occupationDetails.occRating1a == 'Yes') 
    				&&($scope.QPD.occupationDetails.occRating2 && $scope.QPD.occupationDetails.occRating2 == 'Yes') 
    			&& $scope.QPD.occupationDetails.salary > 100000){
    		occQustnsRating=3;
    	} else if (($scope.QPD.occupationDetails.occRating1a && $scope.QPD.occupationDetails.occRating1a == 'Yes') ||
    			($scope.QPD.occupationDetails.occRating1b && $scope.QPD.occupationDetails.occRating1b == 'Yes')){
    		occQustnsRating = 2;
    	}   else {
    		occQustnsRating = 1;
    	}   	
        
        if ($scope.inputMsgOccRating && occQustnsRating){
        	 overAllOccRating = Math.max($scope.inputMsgOccRating,occQustnsRating);
        } else if (occQustnsRating){
        	overAllOccRating = occQustnsRating;
        }
        if($scope.inputMsgOccRating && $scope.inputMsgOccRating >= occQustnsRating)
        	{
        	$scope.occupationUpgraded = false;
        	}
        else
        {
        	$scope.occupationUpgraded = true;
        }
        $scope.finalRating = overAllOccRating;
        if (overAllOccRating == 3){
        	if($scope.QPD.ownOccuptionDeath)
        		{
        		$scope.QPD.deathOccCategory = 'Own Occupation';
        		}
        	else
        		{
        		$scope.QPD.deathOccCategory = 'Professional';
        		}
        	
        	if ($scope.QPD.ownOccuptionTpd){
        		$scope.QPD.tpdOccCategory = 'Own Occupation';
        	}else{
        		$scope.QPD.tpdOccCategory = 'Professional';
        	}     
        	if ($scope.QPD.ipOwnoccuption && ($scope.QPD.ownOccuptionIp == 'Yes' || $scope.QPD.ownOccuptionIp == true)){
        		$scope.QPD.ipOccCategory = 'Own Occupation';
        	}else {
        		$scope.QPD.ipOccCategory = 'Professional';
        	}
        	
        } else if (overAllOccRating == 2) {
        	
        	if($scope.QPD.ownOccuptionDeath)
    		{
    		$scope.QPD.deathOccCategory = 'Own Occupation';
    		}
        	else
    		{
        		$scope.QPD.deathOccCategory = 'White Collar';
    		}        	
        	
        	if ($scope.QPD.ownOccuptionTpd){
        		$scope.QPD.tpdOccCategory = 'Own Occupation';
        	}else{
        		$scope.QPD.tpdOccCategory = 'White Collar';
        	}     
        	if ($scope.QPD.ipOwnoccuption && ($scope.QPD.ownOccuptionIp == 'Yes' || $scope.QPD.ownOccuptionIp == true)){
        		$scope.QPD.ipOccCategory = 'Own Occupation';
        	}else {
        		$scope.QPD.ipOccCategory = 'White Collar';
        	}
        } else {
        	$scope.QPD.deathOccCategory = 'General';
            $scope.QPD.tpdOccCategory = 'General';
            $scope.QPD.ipOccCategory = 'General';
        }
        

  //  }
    
        if(($scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65' || $scope.QPD.addnlIpCoverDetails.benefitPeriod == '5 Years')
        		&& $scope.finalRating == 1)
		{
		$scope.ipOwnOccButton = true;
		$scope.QPD.ownOccuptionIp = 'No';
		
		var radios = $('label[radio-sync]');
    	var data = $('input[data-sync]');
		data.filter('[data-sync="' + $scope.QPD.ownOccuptionIp + '"]').attr('checked','checked');
		radios.filter('[radio-sync="' + "Yes" + '"]').removeClass('active');
		radios.filter('[radio-sync="' + $scope.QPD.ownOccuptionIp + '"]').addClass('active');
		
		}
  
    $scope.customDigest();
    var _this = this;
    $timeout(function(){
      if(_this.formOne.$valid && _this.occupationForm.$valid && _this.coverCalculatorForm.$valid){
        $scope.calculate();
      }
    });
  
  };

  // validate fields "on continue"
  $scope.onFormContinue =  function (form) {
   if(!form.$valid) {
     form.$submitted=true;
     if(form.$name == 'formOne'){
       $scope.toggleOccupation(false);
       $scope.toggleCoverCalc(false);
     } else if(form.$name == 'occupationForm'){
       $scope.toggleCoverCalc(false);
     }
   } else {
    if(form.$name == 'formOne') {
      $scope.toggleOccupation(true);
    } else if(form.$name == 'occupationForm') {
    	if($scope.hideCoverOnNo)
    		{
    		return false;
    		}
      $scope.toggleCoverCalc(true);
    } else if(form.$name == 'coverCalculatorForm') {
        if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag /**&& !$scope.eligibleFrStIpFlag**/){
          $scope.calculate();
        }
      }
    }
  };

  $scope.calculate = function() {
	  
	 /** Ignore the disabled covers for premium calculation - starts**/ 
	  if ($scope.QPD.isDeathDisabled){
		  $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = 0;
	  } else if ($scope.QPD.isTPDDisabled){
		  $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = 0;
	  } else if ($scope.QPD.isIPDisabled){
		  $scope.QPD.addnlIpCoverDetails.ipInputTextValue = 0;
	  }
	  /** Ignore the disabled covers for premium calculation - ends**/ 
	  
	  if($scope.deathCoverDetails.type == "2" && $scope.tpdCoverDetails.type == "2")
		{
		$scope.calculateForUnits();
		}
	  
    var ruleModel = {
      "age": $scope.QPD.age,
      "fundCode": "VICT",
      "gender": $scope.QPD.occupationDetails.gender,
      "deathOccCategory": $scope.QPD.deathOccCategory,
      "tpdOccCategory": $scope.QPD.tpdOccCategory,
      "ipOccCategory": $scope.QPD.ipOccCategory,
      "smoker": false,
      "deathUnits": parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue),
      "deathFixedAmount": parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue),
      "deathFixedCost": null,
      "deathUnitsCost": null,
      "tpdUnits": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
      "tpdFixedAmount": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
      "tpdFixedCost": null,
      "tpdUnitsCost": null,
      "ipUnits": parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue),
      "ipFixedAmount": null,
      "ipFixedCost": null,
      "ipUnitsCost": null,
      "premiumFrequency": $scope.QPD.freqCostType,
      "memberType": null,
      "manageType": "CCOVER",
      "deathCoverType": $scope.QPD.addnlDeathCoverDetails.deathCoverType,
      "tpdCoverType": $scope.QPD.addnlTpdCoverDetails.tpdCoverType,
      "ipCoverType": "IpUnitised",
      "ipWaitingPeriod": $scope.QPD.addnlIpCoverDetails.waitingPeriod,
      "ipBenefitPeriod": $scope.QPD.addnlIpCoverDetails.benefitPeriod
    };
    CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res) {
      var premium = res.data;
      $scope.dynamicFlag = true;
      for(var i = 0; i < premium.length; i++){
        if(premium[i].coverType == 'DcFixed'){
          $scope.QPD.addnlDeathCoverDetails.deathFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = premium[i].coverAmount || 0;
          $scope.QPD.addnlDeathCoverDetails.deathCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'TPDFixed'){
          $scope.QPD.addnlTpdCoverDetails.tpdFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = premium[i].coverAmount || 0;
          $scope.QPD.addnlTpdCoverDetails.tpdCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'IpFixed'){
          $scope.QPD.addnlIpCoverDetails.ipFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlIpCoverDetails.ipInputTextValue = premium[i].coverAmount || 0;
          $scope.QPD.addnlIpCoverDetails.ipCoverPremium = premium[i].cost || 0;
        }
        if(premium[i].coverType == 'DcUnitised'){
          $scope.QPD.addnlDeathCoverDetails.deathFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlDeathCoverDetails.deathCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'TPDUnitised'){
          $scope.QPD.addnlTpdCoverDetails.tpdFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlTpdCoverDetails.tpdCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'IpUnitised'){
          $scope.QPD.addnlIpCoverDetails.ipFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlIpCoverDetails.ipCoverPremium = premium[i].cost || 0;
        }
      }
      $scope.QPD.totalPremium = parseFloat($scope.QPD.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.QPD.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.QPD.addnlIpCoverDetails.ipCoverPremium);
    }, function(err){
      console.info("Something went wrong while calculating..." + JSON.stringify(err));
    });
  };

  $scope.checkBenefitPeriod = function() {
	  
	  	$scope.ipDisclFlagErr = false;
  		$scope.QPD.ipDisclaimer = false;
  		$scope.ipOwnOccButton = false;
  		$('#ipDisclaimerLabel').removeClass('active');
    $scope.tempWaitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.waitingPeriod) > -1 ? $scope.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
    $scope.tempBenefitPeriod = $scope.benefitPeriodOptions.indexOf($scope.ipCoverDetails.benefitPeriod) > -1 ? $scope.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';

    if($scope.tempWaitingPeriod == $scope.QPD.addnlIpCoverDetails.waitingPeriod && $scope.tempBenefitPeriod == $scope.QPD.addnlIpCoverDetails.benefitPeriod){
      $scope.ipIncreaseFlag = false;
      $scope.disclaimerFlag = true;
    } else if(($scope.tempBenefitPeriod == '2 Years' && $scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65') ||
    		($scope.tempBenefitPeriod == '5 Years' && $scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65') || //Added for Vicsuper 5 years question
    		($scope.tempBenefitPeriod == '2 Years' && $scope.QPD.addnlIpCoverDetails.benefitPeriod == '5 Years') || //Added for Vicsuper 5 years question
        ($scope.tempWaitingPeriod == '90 Days' && $scope.QPD.addnlIpCoverDetails.waitingPeriod == '60 Days') ||
        ($scope.tempWaitingPeriod == '90 Days' && $scope.QPD.addnlIpCoverDetails.waitingPeriod == '30 Days') ||
        ($scope.tempWaitingPeriod == '60 Days' && $scope.QPD.addnlIpCoverDetails.waitingPeriod == '30 Days')){
    if (parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) == 0){
    		$scope.ipNocoverFlag = true;
    	}else{
      $scope.ipNocoverFlag = false;
      $scope.ipIncreaseFlag = true;
      $scope.disclaimerFlag = true;
      $scope.QPD.auraDisabled = false;
    	}
    } else{
      $scope.ipIncreaseFlag = false;
      $scope.disclaimerFlag = false;
    }
    //$scope.eligibleForStIp();
   // $scope.validateDeathTpdIp();
//    if ($scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65' || $scope.QPD.addnlIpCoverDetails.benefitPeriod == '5 Years'){
//    	$scope.enableIpDisclaimerSec = true;
//    } else {
//    	$scope.enableIpDisclaimerSec = false;    	
//    }
    if ($scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65' || $scope.QPD.addnlIpCoverDetails.benefitPeriod == '5 Years'){
    	$scope.enableIpDisclaimerSec = true;
    	$scope.QPD.ipOwnoccuption = true;
    	
    	var ipoccRating = $scope.ipCoverDetails.occRating;
		if ($scope.ipCoverDetails.occRating) {	
			if ($scope.ipCoverDetails.occRating.toLowerCase() == ownoccupation){
				if($scope.QPD.fulCheck)
					{
					$scope.QPD.ownOccuptionIp = 'Yes';
					}
				else
				{
				$scope.QPD.ownOccuptionIp = 'No';
				}
				
			}
			else if($scope.QPD.fulCheck)
			{
				$scope.QPD.ownOccuptionIp = 'Yes';
				}
			else
				{
				$scope.QPD.ownOccuptionIp = 'No';
				}
			/*if($scope.finalRating == 1)
				{
				$scope.ipOwnOccButton = true;
				$scope.QPD.ownOccuptionIp = 'No';
				}*/
			
		}
    	
	    if ($scope.QPD.ipDisclaimer && (parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.QPD.existingIPUnits))){	    	
	    	$scope.ipDisclFlagErr = false;
	    	$('#ipDisclaimer').parent().addClass('active');
			  $('#ipDisclaimer').attr('checked','checked');
	    } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.QPD.existingIPUnits)) {
	    	$scope.ipDisclFlagErr = true;
	    }
    } else {
    	$scope.enableIpDisclaimerSec = false;
    	$scope.QPD.ipOwnoccuption = false;
    	$scope.QPD.ownOccuptionIp = 'No';
    }
    $scope.renderOccupationQuestions();
    $scope.validateDeathTpdIp();
  };

  $scope.checkIpCover = function() {
    //$scope.ipWarningFlag = false;
      if(parseInt($scope.QPD.occupationDetails.salary) <= 1000000){
        $scope.IPCvrAmount  = Math.ceil((0.85 * ($scope.QPD.occupationDetails.salary/12)));
        $scope.IPAmount = Math.ceil($scope.IPCvrAmount/$scope.IpUnitCostMulitiplier);//Cover amount in units
      } 
//      else if(parseInt($scope.QPD.occupationDetails.salary) > 1000000){
//        $scope.IPAmount = Math.ceil((Math.round(((parseInt($scope.QPD.occupationDetails.salary))/12)*0.6) ));
//      }
      $scope.IPAmount = $scope.IPMaxAmount < $scope.IPAmount ? $scope.IPMaxAmount : $scope.IPAmount;
      $scope.IPAmount = parseInt($scope.ipCoverDetails.units) > $scope.IPAmount ? parseInt($scope.ipCoverDetails.units) : $scope.IPAmount;
  };
  
  $scope.validateDeathTpdIp = function() {
//	  if($scope.canValidateDeathTpd() && parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)){
//	        $scope.tpdErrorFlag = true;
//	        $scope.tpdErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
//	      } 
	 // else {
		  
		  
		//  $scope.calculateForUnits = function() {
		  	    var ruleModel = {
		  	      "age": $scope.QPD.age,
		  	      "fundCode": "VICT",
		  	      "gender": $scope.QPD.occupationDetails.gender,
		  	      "deathOccCategory": $scope.QPD.deathOccCategory,
		  	      "tpdOccCategory": $scope.QPD.tpdOccCategory,
		  	      "ipOccCategory": $scope.QPD.ipOccCategory,
		  	      "smoker": false,
		  	      "deathUnits": 1,
		  	      "deathFixedAmount": parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue),
		  	      "deathFixedCost": null,
		  	      "deathUnitsCost": null,
		  	      "tpdUnits": 1,
		  	      "tpdFixedAmount": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
		  	      "tpdFixedCost": null,
		  	      "tpdUnitsCost": null,
		  	      "ipUnits":parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue),
		  	      "ipFixedAmount": null,
		  	      "ipFixedCost": null,
		  	      "ipUnitsCost": null,
		  	      "premiumFrequency": $scope.QPD.freqCostType,
		  	      "memberType": null,
		  	      "manageType": "CCOVER",
		  	      "deathCoverType": "DcUnitised",
		  	      "tpdCoverType": "TPDUnitised",
		  	      "ipCoverType": "IpUnitised",
		  	      "ipWaitingPeriod": $scope.QPD.addnlIpCoverDetails.waitingPeriod,
		  	      "ipBenefitPeriod": $scope.QPD.addnlIpCoverDetails.benefitPeriod
		  	    };
		  	    CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res) {
		  	    	var coverAmtPerUnit = 0;
		  	    	var premium = res.data;
		  	      
		  	      for(var i = 0; i < premium.length; i++){
		  	        if(premium[i].coverType == 'DcUnitised'){
		  	        	coverAmtPerUnit = premium[i].coverAmount || 0;
		  	        	if(coverAmtPerUnit>0)
		  	        		{
		  	        	$scope.deathUnitsForFixed = Math.ceil($scope.deathCoverDetails.amount/coverAmtPerUnit);
		  	        	if ($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised" || $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
		  	        		$scope.deathFixedForUnits = Math.ceil(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)*coverAmtPerUnit);
		  	        		}
		  	        	}
		  	        	else
		  	        		{
		  	        		$scope.deathUnitsForFixed = 0;
		  	        		$scope.deathFixedForUnits = 0;
		  	        		}
		  	        } else if(premium[i].coverType == 'TPDUnitised'){
		  	        	coverAmtPerUnit = premium[i].coverAmount || 0;
		  	        	if(coverAmtPerUnit>0)
			        		{
			  	        	$scope.tpdUnitsForFixed = Math.ceil($scope.tpdCoverDetails.amount/coverAmtPerUnit);
			  	        	if ($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised" || $scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised"){
			  	        		$scope.tpdFixedForUnits = Math.ceil(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue)*coverAmtPerUnit);
			  	        	}
			        		}
		  	        	else
		  	        		{
		  	        		$scope.tpdUnitsForFixed = 0;
		  	        		$scope.tpdFixedForUnits = 0;
		  	        		}
		  	        }
		  	      }
		  	    if ($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised" || $scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised"){
		  	    	if ($scope.canValidateDeathTpd() && parseInt($scope.tpdFixedForUnits) > parseInt($scope.deathFixedForUnits)){
		  	    		 $scope.tpdErrorFlag = true;
		  		        $scope.tpdErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
		  	    	}else if (parseInt($scope.tpdFixedForUnits) > parseInt($scope.TPDMaxAmount)){
		      	  		$scope.tpdErrorFlag = true;
		      	  		$scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is $" + $scope.TPDMaxAmount.toLocaleString() + ". Please re-enter your cover.";
		      	  	} else {
		      	  		$scope.tpdErrorFlag = false;
		      	  		$scope.validateDeathTpdIpAmounts();
		      	  	}
		  	    } else {		  	    	
	      	  		$scope.validateDeathTpdIpAmounts();
		  	    }
//		  	    if($scope.QPD.addnlDeathCoverDetails.deathInputTextValue == $scope.deathUnitsForFixedOld)
//		    	  		{
//		  	    		if(!$scope.deathUnitsForFixedOld == $scope.deathUnitsForFixed)
//		  	    			{
//		  	    				$scope.QPD.addnlDeathCoverDetails.deathInputTextValue =$scope.deathUnitsForFixed;
//		  	    			}
//		    	  		}
//		  	    if($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue == $scope.tpdUnitsForFixedOld)
//		  	    		{
//		  	    		if(!$scope.tpdUnitsForFixedOld == $scope.tpdUnitsForFixed)
//		  	    			{
//		  	    			$scope.QPD.addnlTpdCoverDetails.tpdInputTextValue =$scope.tpdUnitsForFixed;
//		  	    			}
//		  	    		}
		  	      //$scope.QPD.totalPremium = parseFloat($scope.QPD.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.QPD.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.QPD.addnlIpCoverDetails.ipCoverPremium);
		  	    }, function(err){
		  	      console.info("Something went wrong while calculating..." + JSON.stringify(err));
		  	    });
		  	 // };
    	  
//    	  var ruleModel = {
//      	  	      "age": $scope.QPD.age,
//      	  	      "fundCode": "VICT",
//      	  	      "gender": $scope.QPD.occupationDetails.gender,
//      	  	      "deathOccCategory": null,
//      	  	      "tpdOccCategory": $scope.QPD.tpdOccCategory,
//      	  	      "ipOccCategory": null,
//      	  	      "smoker": false,
//      	  	      "deathUnits": 1,
//      	  	      "deathFixedAmount": null,
//      	  	      "deathFixedCost": null,
//      	  	      "deathUnitsCost": null,
//      	  	      "tpdUnits": 1,
//      	  	      "tpdFixedAmount": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
//      	  	      "tpdFixedCost": null,
//      	  	      "tpdUnitsCost": null,
//      	  	      "ipUnits":null,
//      	  	      "ipFixedAmount": null,
//      	  	      "ipFixedCost": null,
//      	  	      "ipUnitsCost": null,
//      	  	      "premiumFrequency": $scope.QPD.freqCostType,
//      	  	      "memberType": null,
//      	  	      "manageType": "CCOVER",
//      	  	      "deathCoverType": null,
//      	  	      "tpdCoverType": "TPDUnitised",
//      	  	      "ipCoverType": null,
//      	  	      "ipWaitingPeriod": null,
//      	  	      "ipBenefitPeriod": null
//      	  	    };
//      	  	    CalculateService.calculate(ruleModel,$scope.urlList.calculateTpdUrl).then(function(res) {
//      	  	    	var premium = res.data;
//      	    	      
//      	    	      for(var i = 0; i < premium.length; i++){
//      	    	        if(premium[i].coverType == 'TPDUnitised'){
//      	    	        	var coverAmtPerUnit = premium[i].coverAmount || 0;
//      	      	        	if(coverAmtPerUnit>0)
//      	      	        		{
//      	      	        	$scope.tpdUnitsForFixed = Math.ceil(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue)*coverAmtPerUnit);
//      	      	        		}
//      	      	        	else
//      	      	        		{
//      	      	        	$scope.tpdUnitsForFixed = 0;	
//      	      	        		}
//      	    	        }
//      	    	      }
//      	    	    if (parseInt($scope.tpdUnitsForFixed) > parseInt($scope.TPDMaxAmount)){
//      	      	  		$scope.tpdErrorFlag = true;
//      	      	  		$scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + $scope.TPDMaxAmount + ". Please re-enter your cover.";
//      	      	  	} else {
//      	      	  		$scope.tpdErrorFlag = false;
//      	      	  		$scope.validateDeathTpdIpAmounts();
//      	      	  	}
//      	  	    });
		  
		  
      	  	
    //  } 
//	  else {
//    	  $scope.validateDeathTpdIpAmounts();
//      }
	  
  }

  $scope.validateDeathTpdIpAmounts = function() {
    $scope.QPD.auraDisabled = true;
    $scope.deathErrorFlag = false;
    $scope.tpdErrorFlag = false;
    $scope.ipErrorFlag = false;
    $scope.deathDecOrCancelFlag = false;
    $scope.tpdDecOrCancelFlag = false;
    $scope.ipDecOrCancelFlag = false;
    $scope.ipWarningFlag = false;
    var tpdTaperingAmt = Math.ceil(($scope.tpdTapering * ($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)));
    $scope.checkIpCover();
    if(parseInt(tpdTaperingAmt) > $scope.TPDMaxAmount)
    	{
    	tpdTaperingAmt = $scope.TPDMaxAmount;
    	}
    // error check
    if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.DCMaxAmount)){
      $scope.deathErrorFlag = true;
      $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is  $" + $scope.DCMaxAmount.toLocaleString() + ". Please re-enter your cover.";
    }
    //$timeout(function() {
    
    if($scope.QPD.isTPDIncreaseDisabled && ((parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > $scope.tpdCoverDetails.amount) 
    		|| (($scope.tpdCoverDetails.type == "2" && $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised")
    	    		&& (parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > $scope.tpdUnitsForFixed))
    	    		||  (($scope.tpdCoverDetails.type == "1" && $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised")
    	    	    		&& (parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > $scope.tpdCoverDetails.units))))
	  {
	  $scope.tpdErrorFlag = true;
	  $scope.tpdErrorMsg="Based on your age you are not eligible to increase your TPD cover. Please re-enter your cover.";
	  return;
	  }
    
      if($scope.canValidateDeathTpd() && parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)){
        $scope.tpdErrorFlag = true;
        $scope.tpdErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
      } 
      else if(($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed" 
    	  && parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.TPDMaxAmount))){    	  
        $scope.tpdErrorFlag = true;
        $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is $" + $scope.TPDMaxAmount.toLocaleString() + ". Please re-enter your cover.";
              
      }
      if(($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed" 
    	  && parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt(tpdTaperingAmt)))
      {    	  
        $scope.tpdErrorFlag = true;
        $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is $" + tpdTaperingAmt.toLocaleString() + ". Please re-enter your cover.";
      }
      
    //});
    
    if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > $scope.IPAmount && !$scope.QPD.ipcheckbox) {
      $scope.QPD.addnlIpCoverDetails.ipInputTextValue = $scope.IPAmount;
      $scope.ipErrorFlag = false;
      $scope.ipErrorMsg="";
      $scope.ipWarningFlag = true;
    }
    //cover name check
    if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag) {
      // Death cover
      //var eXDcAmount = Math.ceil(parseInt($scope.deathCoverDetails.amount)/1000)*1000;
      var eXDcAmount = parseInt($scope.deathCoverDetails.amount);
      if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcFixed") {
    	 /*var deathRoundVal =  Math.ceil(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)/1000)*1000;*/
    	  var deathRoundVal =  parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue);
        if(parseInt(deathRoundVal) < eXDcAmount ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
          $scope.deathDecOrCancelFlag = true;
        } else if(parseInt(deathRoundVal) > eXDcAmount ) {
        	//if (!$scope.dthBeyondAge){
		          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
		          $scope.QPD.auraDisabled = false;
//        	} else if (eXDcAmount && eXDcAmount != '0' && eXDcAmount !='' && eXDcAmount != '0.0') {
//        		$scope.dthWarnMsg = true;
//        		$scope.QPD.addnlDeathCoverDetails.deathInputTextValue = eXDcAmount;
//        	}
        } else if(parseInt(deathRoundVal) == eXDcAmount ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'No change';
        } else if(parseInt(deathRoundVal) == 0 && parseInt($scope.deathCoverDetails.amount) != 0) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
          $scope.deathDecOrCancelFlag = true;
        }
      } else if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised") {
        if(parseInt($scope.deathFixedForUnits) < parseInt(eXDcAmount) ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
          $scope.deathDecOrCancelFlag = true;
        } else if(parseInt($scope.deathFixedForUnits) > parseInt(eXDcAmount) ) {
        	//if (!$scope.dthBeyondAge){
	          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
	          $scope.QPD.auraDisabled = false;
//        	} else if ($scope.deathCoverDetails.units && $scope.deathCoverDetails.units != '0' && $scope.deathCoverDetails.units !='' && $scope.deathCoverDetails.units != '0.0'){
//        		$scope.dthWarnMsg = true;
//        		$scope.QPD.addnlDeathCoverDetails.deathInputTextValue = $scope.deathCoverDetails.units;
//        	}
        } else if(parseInt($scope.deathFixedForUnits) == parseInt(eXDcAmount) ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'No change';
        } else if(parseInt($scope.deathFixedForUnits) == 0 && parseInt(eXDcAmount)) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
          $scope.deathDecOrCancelFlag = true;
        }
      }
      

      // Tpd cover
      //var eXTpdAmount = Math.ceil(parseInt($scope.tpdCoverDetails.amount)/1000)*1000;
      var eXTpdAmount = parseInt($scope.tpdCoverDetails.amount);
      if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed") {
    	  /*var tpdRundAmount = Math.ceil(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue)/1000)*1000;*/
    	  var tpdRundAmount = parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue);
        if(parseInt(tpdRundAmount) < eXTpdAmount ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
          $scope.tpdDecOrCancelFlag = true;
        } else if(parseInt(tpdRundAmount) > eXTpdAmount ) {
        	//if (!$scope.tpdBeyondAge) {
	          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
	          $scope.QPD.auraDisabled = false;
//        	} else if (eXTpdAmount && eXTpdAmount != '0' && eXTpdAmount !='' && eXTpdAmount != '0.0'){
//        	  $scope.tpdWarnMsg = true;
//        	  $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = eXTpdAmount;
//        	}
        } else if(parseInt(tpdRundAmount) == eXTpdAmount ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'No change';
        } else if(parseInt(tpdRundAmount) == 0 && eXTpdAmount != 0) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
          $scope.tpdDecOrCancelFlag = true;
        }
      } else if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
        if(parseInt($scope.tpdFixedForUnits) < parseInt(eXTpdAmount) ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
          $scope.tpdDecOrCancelFlag = true;
        } else if(parseInt($scope.tpdFixedForUnits) > parseInt(eXTpdAmount) ) {
        	//if (!$scope.tpdBeyondAge) {
	          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
	          $scope.QPD.auraDisabled = false;
//        	} else if ($scope.tpdCoverDetails.units && $scope.tpdCoverDetails.units != '0' && $scope.tpdCoverDetails.units !='' && $scope.tpdCoverDetails.units != '0.0'){
//        		$scope.tpdWarnMsg = true;
//        		$scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdCoverDetails.units;
//        	}
        } else if(parseInt($scope.tpdFixedForUnits) == parseInt(eXTpdAmount) ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'No change';
        } else if(parseInt($scope.tpdFixedForUnits) == 0 && parseInt(eXTpdAmount)) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
          $scope.tpdDecOrCancelFlag = true;
        }
      }
      if(($scope.deathCoverDetails.type == "2" && $scope.tpdCoverDetails.type == "2") && ($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised" && $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"))
    	  {
    	  $scope.QPD.auraDisabled = true;
    	  $scope.deathDecOrCancelFlag = false;
    	  $scope.tpdDecOrCancelFlag = false;
    	  if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised") {
              if(parseInt($scope.deathFixedForUnits) < parseInt(eXDcAmount) ) {
                $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
                $scope.deathDecOrCancelFlag = true;
              } else if(parseInt($scope.deathFixedForUnits) > parseInt(eXDcAmount) ) {                
              //  if (!$scope.dthBeyondAge){
                	$scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
                    $scope.QPD.auraDisabled = false;
//              	} else if ($scope.deathUnitsForFixed && $scope.deathUnitsForFixed != '0' && $scope.deathUnitsForFixed !='' && $scope.deathUnitsForFixed != '0.0'){
//              		$scope.dthWarnMsg = true;
//              		$scope.QPD.addnlDeathCoverDetails.deathInputTextValue = $scope.deathUnitsForFixed;
//              	}
                
              } else if(parseInt($scope.deathFixedForUnits) == parseInt(eXDcAmount) ) {
                $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'No change';
              } else if(parseInt($scope.deathFixedForUnits) == 0 && parseInt(eXDcAmount != 0)) {
                $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
                $scope.deathDecOrCancelFlag = true;
              }
            }
    	  
    	  if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
    		 
              if(parseInt($scope.tpdFixedForUnits) < eXTpdAmount ) {
                $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
                $scope.tpdDecOrCancelFlag = true;
              } else if(parseInt($scope.tpdFixedForUnits) > eXTpdAmount ) {                
              //  if (!$scope.tpdBeyondAge) {
                	$scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
                    $scope.QPD.auraDisabled = false;
//              	} else if ($scope.tpdUnitsForFixed && $scope.tpdUnitsForFixed != '0' && $scope.tpdUnitsForFixed !='' && $scope.tpdUnitsForFixed != '0.0'){
//              	  $scope.tpdWarnMsg = true;
//              	  $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdUnitsForFixed;
//              	}
              } else if(parseInt($scope.tpdFixedForUnits) == eXTpdAmount ) {
                $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'No change';
              } else if(parseInt($scope.tpdFixedForUnits) == 0 && parseInt(eXTpdAmount != 0)) {
                $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
                $scope.tpdDecOrCancelFlag = true;
              }
            }
    	  
    	  }
      

      
      
      
      // Ip cover
      //var eXCvrIpAmount = Math.ceil(parseInt($scope.ipCoverDetails.amount)/1000)*1000;   
      var eXCvrIpAmount = parseInt($scope.ipCoverDetails.amount); 
      var eXIpAmount = Math.ceil(parseInt(eXCvrIpAmount)/$scope.IpUnitCostMulitiplier)/***$scope.IpUnitCostMulitiplier**/;
      if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) < eXIpAmount ) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'Decrease your cover';
        $scope.ipDecOrCancelFlag = true;
      } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > eXIpAmount ) {
    	 // if (!$scope.ipBeyondAge){
	        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'Increase your cover';
	        $scope.QPD.auraDisabled = false;
//    	  } else if (eXIpAmount && eXIpAmount != '0' && eXIpAmount !='' && eXIpAmount != '0.0'){
//    		  $scope.QPD.addnlIpCoverDetails.ipInputTextValue = eXIpAmount;
//    		  $scope.ipWarn1Msg = true;
//    	  }
      } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) == eXIpAmount ) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'No change';
      } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) == 0 && eXIpAmount != 0) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'Cancel your cover';
        $scope.ipDecOrCancelFlag = true;
      } 
      
      if($scope.ipIncreaseFlag) {
    	$scope.QPD.addnlIpCoverDetails.ipCoverName = 'Increase your cover';
        $scope.QPD.auraDisabled = false;
      }
      // ends here
      $scope.customDigest();
      $timeout(function(){
        $scope.calculateOnChange();
      });
    }
 };

$scope.canValidateDeathTpd = function() {

	
	if(($scope.deathCoverDetails.type == "2" && $scope.tpdCoverDetails.type == "2") && ($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised" && $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"))
	  {
		
	     if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised") {
	        if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) < parseInt($scope.deathUnitsForFixed) ) {
	          return true;
	        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.deathUnitsForFixed) ) {
	          return true;
	        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathUnitsForFixed != 0)) {
	          return true;
	        }
	      }

	      // Tpd cover
	      
	      if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
	        if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) < parseInt($scope.tpdUnitsForFixed) ) {
	          return true;
	        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.tpdUnitsForFixed) ) {
	          return true;
	        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == 0 && parseInt($scope.tpdUnitsForFixed != 0)) {
	          return true;
	        }
	      }
	  }
	else
		{
	
  // Death cover
     // var eXDcAmount = Math.ceil(parseInt($scope.deathCoverDetails.amount)/1000)*1000;
      var eXDcAmount = parseInt($scope.deathCoverDetails.amount);
      if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcFixed") {
        if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) < eXDcAmount ) {
          return true;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > eXDcAmount ) {
          return true;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathCoverDetails.amount) != 0) {
          return true;
        }
      } else if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised") {
        if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) < parseInt($scope.deathCoverDetails.units) ) {
          return true;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.deathCoverDetails.units) ) {
          return true;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathCoverDetails.units != 0)) {
          return true;
        }
      }

      // Tpd cover
     // var eXTpdAmount = Math.ceil(parseInt($scope.tpdCoverDetails.amount)/1000)*1000;
      var eXTpdAmount = parseInt($scope.tpdCoverDetails.amount);
      if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed") {
        if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) < eXTpdAmount ) {
          return true;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > eXTpdAmount ) {
          return true;
        }  else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == 0 && eXTpdAmount != 0) {
          return true;
        }
      } else if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
        if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) < parseInt($scope.tpdCoverDetails.units) ) {
          return true;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.tpdCoverDetails.units) ) {
          return true;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == 0 && parseInt($scope.tpdCoverDetails.units != 0)) {
          return true;
        }
      }
	}
  // return ($('#dollar1').hasClass('ng-dirty') || $('#nodollar1').hasClass('ng-dirty') || $('#dollar').hasClass('ng-dirty') || $('#nodollar').hasClass('ng-dirty'))
}

$scope.customDigest = function() {
  if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
      $scope.$digest();
  }
}

  $scope.quoteSaveAndExitPopUp = function (hhText) {
   var dialog1 = ngDialog.open({
       template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
       className: 'ngdialog-theme-plain custom-width',
       preCloseCallback: function(value) {
        var url = "/landing"
        $location.path( url );
        return true
       },
       plain: true
   });
   dialog1.closePromise.then(function (data) {
     console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
   });
  };

  $scope.saveQuote = function() {
    $scope.QPD.lastSavedOn = 'Quotepage';
    $rootScope.$broadcast('disablepointer');
    var selectedIndustry = $scope.IndustryOptions.filter(function(obj) {
      return $scope.QPD.occupationDetails.industryCode == obj.key;
    });
    $scope.QPD.occupationDetails.industryName = selectedIndustry[0].value;
    $scope.QPD.industryName = selectedIndustry[0].value;
    $scope.QPD.industryCode = selectedIndustry[0].key;
    $scope.QPD.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
    $scope.validateDeathTpdIp();
    $scope.QPD = angular.extend($scope.QPD, $scope.inputDetails);
    saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, $scope.QPD).then(function(response) {
      //console.log(response.data);
      $scope.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+$scope.QPD.appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR>');
    });
  };

  $scope.goToAura =function() {
    this.formOne.$submitted = true;
    this.occupationForm.$submitted = true;
    this.coverCalculatorForm.$submitted = true;
    
    if (!$scope.QPD.isIPDisabled && ($scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65' || $scope.QPD.addnlIpCoverDetails.benefitPeriod == '5 Years') && (parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.QPD.existingIPUnits))){
    	var ipdisclaimerCheck = $('#ipDisclaimerLabel').hasClass('active');	 
    	$scope.QPD.ipDisclaimer = ipdisclaimerCheck;
    	$scope.ipDisclFlagErr = !ipdisclaimerCheck;
    	if ($scope.ipDisclFlagErr){
    		return false;
    	}
    }
    
//    $scope.validateDeathTpdIpAmounts();   
    $scope.validateDeathTpdIp();
    
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
      if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag /**&& !$scope.eligibleFrStIpFlag**/) {
    	  
    	  if(!(($scope.QPD.totalPremium > 0) || ((parseInt($scope.QPD.addnlDeathCoverDetails.deathFixedAmt) + parseInt($scope.QPD.addnlTpdCoverDetails.tpdFixedAmt) + parseInt($scope.QPD.addnlIpCoverDetails.ipFixedAmt)) > 0) || $scope.deathDecOrCancelFlag || $scope.tpdDecOrCancelFlag || $scope.ipDecOrCancelFlag))
    		  {
    		  $scope.clickToOpenCCoverInfo("You have not applied for any cover or changed your existing cover, you need to add or update the cover amount to be able to proceed with this application.");
    		  return false;
    		  }
    	  
        if($scope.deathDecOrCancelFlag || $scope.tpdDecOrCancelFlag || $scope.ipDecOrCancelFlag){
        	if(($scope.QPD.totalPremium == 0) && (parseInt($scope.deathCoverDetails.amount) > 0 || parseInt($scope.deathCoverDetails.amount) == 0 ) && (parseInt($scope.tpdCoverDetails.amount) > 0 || parseInt($scope.tpdCoverDetails.amount) == 0)
        			&& (parseInt($scope.ipCoverDetails.amount) > 0 || parseInt($scope.ipCoverDetails.amount) == 0))
        		{
        		 $scope.cancelPopUpMsg("You are requesting to cancel your Death, TPD & IP cover. </br>If you decide to apply cover in future, you will need to supply health information as part of your application.</br></br> When you click continue, you will be taken to the VicSuper Member Portal to complete the cancellation of your cover. </br></br>Click cancel to go back to the Change you cover function.");
        		}
        	else{
        	if ($scope.ipNocoverFlag){
        		$scope.QPD.addnlIpCoverDetails.benefitPeriod = '2 Years';
        		$scope.QPD.addnlIpCoverDetails.waitingPeriod = '90 Days';
        	}
        	
			$scope.decreaseInd = true;
            $scope.decOrCancelCovers();
        	}
        } else if ($scope.ipNocoverFlag){
        	$scope.QPD.addnlIpCoverDetails.benefitPeriod = '2 Years';
    		$scope.QPD.addnlIpCoverDetails.waitingPeriod = '90 Days';
        	$scope.showDecreaseOrCancelPopUp("Please note that although you have changed your benefit and/or waiting period for your income protection cover, you have not added any monthly cover value.");
        } else {
          $scope.goTo();
        }
      }else{
        return false;
      }
    }
  };
  $scope.isUnitized = function(value){
      return (value == "1");
    };
  $scope.goTo = function() {
	  	  if($scope.QPD.auraDisabled && (!$scope.ipNocoverFlag && $scope.QPD.ownOccuptionIp === 'Yes' && !($scope.ipCoverDetails.occRating.toLowerCase() == ownoccupation)) )
	  		  {
	  		$scope.QPD.auraDisabled = false;
	  		  }
    if(!$scope.QPD.auraDisabled) {
      $timeout(function() {
    	$scope.continueToNextPage('/aura');
      }, 10);
    } else {
      $timeout(function() {
    	  if($scope.QPD.auraDisabled /**&& $scope.manualIndexation**/ && $scope.QPD.indexationDeath === 'true' && $scope.QPD.indexationTpd === 'true' &&  ($scope.deathCoverDetails.indexation == 'N' && $scope.tpdCoverDetails.indexation == 'N') && $scope.QPD.addnlDeathCoverDetails.deathCoverPremium > 0)
		  {
		  $scope.continueToNextPage('/auraIndex');
		  }
	  else if($scope.decreaseInd || (($scope.deathCoverDetails.type == "2" && $scope.tpdCoverDetails.type == "2") && ($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised" && $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"))
			  || (($scope.deathCoverDetails.type == "1" && $scope.tpdCoverDetails.type == "1") && ($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcFixed" && $scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed"))
			  || ($scope.ipCoverDetails.waitingPeriod != $scope.QPD.addnlIpCoverDetails.waitingPeriod) || ($scope.ipCoverDetails.benefitPeriod != $scope.QPD.addnlIpCoverDetails.benefitPeriod) 
			  || ($scope.occupationUpgraded))
		  {
        $scope.continueToNextPage('/summary');
		  }
	  else
		  {
		  $scope.clickToOpenCCoverInfo("You have not applied for any cover or changed your existing cover, you need to add or update the cover amount to be able to proceed with this application.");
		  return false;
		  }
      }, 10);
    }
  }
  $scope.continueToNextPage = function(path) {
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
      var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
        return $scope.QPD.occupationDetails.industryCode == obj.key;
      });
      $scope.QPD.occupationDetails.industryName = selectedIndustry[0].value;
      $scope.QPD.industryName = selectedIndustry[0].value;
      $scope.QPD.industryCode = selectedIndustry[0].key;
      $scope.QPD.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      appData.setAppData($scope.QPD);
      $location.path(path);
    }
  };

  $scope.generatePDF = function(){
    // var CCOccDetails = PersistenceService.getChangeCoverOccDetails();
    // var deathAddnlInfo = PersistenceService.getDeathAddnlCoverDetails();
    // var tpdAddnlInfo = PersistenceService.getTpdAddnlCoverDetails();
    // var ipAddnlInfo = PersistenceService.getIpAddnlCoverDetails();
    //   var personalInformation = persoanlDetailService.getMemberDetails();
    //   var quoteObj =  PersistenceService.getChangeCoverDetails();
    //   if(quoteObj != null && CCOccDetails != null && deathAddnlInfo != null && tpdAddnlInfo != null && ipAddnlInfo != null && personalInformation[0] != null ){
    //     var info={};
    //     info.addnlDeathCoverDetails = deathAddnlInfo;
    //     info.addnlTpdCoverDetails = tpdAddnlInfo;
    //     info.addnlIpCoverDetails = ipAddnlInfo;
    //     info.occupationDetails = CCOccDetails;
    //   var temp = angular.extend(info,quoteObj);
    //   var printObject = angular.extend(temp, personalInformation[0]);
    //   auraResponseService.setResponse(printObject);
      $rootScope.$broadcast('disablepointer');
    var selectedIndustry = $scope.IndustryOptions.filter(function(obj) {
      return $scope.QPD.occupationDetails.industryCode == obj.key;
    });
    $scope.QPD.occupationDetails.industryName = selectedIndustry[0].value;
    $scope.QPD.industryName = selectedIndustry[0].value;
    $scope.QPD.industryCode = selectedIndustry[0].key;
    $scope.QPD.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
    //$scope.validateDeathTpdIpAmounts();
    $scope.QPD = angular.extend($scope.QPD, $scope.inputDetails);
    auraRespSvc.setResponse($scope.QPD);
      printPageSvc.reqObj($scope.urlList.printQuotePage).then(function(response) {
        appData.setPDFLocation(response.data.clientPDFLocation);
        $scope.downloadPDF();
    }, function(err){
      $rootScope.$broadcast('enablepointer');
      console.info("Something went wrong while generating pdf..." + JSON.stringify(err));
    });
  }
  
  $scope.downloadPDF = function(){
          var pdfLocation =null;
          var filename = null;
          var a = null;
        pdfLocation = appData.getPDFLocation();
        //console.log(pdfLocation+"pdfLocation");
        filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
        a = document.createElement("a");
          document.body.appendChild(a);
          DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
          //DownloadPDFService.download({file_name: pdfLocation}, function(res){
        	  
          if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
        	  var extension = res.data.response.type;
        	  extension = extension.substring(extension.lastIndexOf('/')+1);
        	  filename = filename+"."+extension;
                 window.navigator.msSaveBlob(res.data.response,filename);
             }else{
              var fileURL = URL.createObjectURL(res.data.response);
              a.href = fileURL;
              a.download = filename;
              a.click();
             }
             $rootScope.$broadcast('enablepointer');
        }, function(err){
          //console.log("Error downloading the PDF " + err);
          $rootScope.$broadcast('enablepointer');
        });
      };
      
      // added for Decrease or Cancel cover popup "on continue" after calculate quote
		$scope.showDecreaseOrCancelPopUp = function (val){	
		   if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
			   if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag /**&& !$scope.eligibleFrStIpFlag**/){
				   if(val == null || val == "" || val == " "){
			   			hideTips();
			   		}else{
			   		 /*ackCheck = $('#termsLabel').hasClass('active');
			    		if(ackCheck){*/
			    			 $scope.ackFlag = false;
					   		 document.getElementById('mymodalDecCancel').style.display = 'block';
					   		 document.getElementById('mymodalDecCancelFade').style.display = 'block';
					   		 document.getElementById('decCancelMsg_text').innerHTML=val;	
			    		/*}else{
			    			$scope.ackFlag = true;
			    		}*/
			    		
			   		} 
			   }else{
				   return false;
			   }
		   }
	   	//	$scope.saveDataForPersistence();
	   	}
	    $scope.hideTips = function  (){
	   		if(document.getElementById('help_div')){
	   			document.getElementById('help_div').style.display = "none";
	   		}						
	   	}
      
  	$scope.decOrCancelCovers = function(){
  		if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death, TPD & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death & TPD";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag && $scope.ipDecOrCancelFlag){
  			$scope.decCancelCover = "Death & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "TPD & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.tpdDecOrCancelFlag == true){
  			$scope.decCancelCover = "TPD";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else{
  			$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
  		}					
  		/*$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";*/
  		$scope.showDecreaseOrCancelPopUp($scope.decCancelMsg);
  	};
  	
 $scope.calculateForUnits = function() {
  	    var ruleModel = {
  	      "age": $scope.QPD.age,
  	      "fundCode": "VICT",
  	      "gender": $scope.QPD.occupationDetails.gender,
  	      "deathOccCategory": $scope.QPD.deathOccCategory,
  	      "tpdOccCategory": $scope.QPD.tpdOccCategory,
  	      "ipOccCategory": $scope.QPD.ipOccCategory,
  	      "smoker": false,
  	      "deathUnits": 1,
  	      "deathFixedAmount": parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue),
  	      "deathFixedCost": null,
  	      "deathUnitsCost": null,
  	      "tpdUnits": 1,
  	      "tpdFixedAmount": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
  	      "tpdFixedCost": null,
  	      "tpdUnitsCost": null,
  	      "ipUnits":parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue),
  	      "ipFixedAmount": null,
  	      "ipFixedCost": null,
  	      "ipUnitsCost": null,
  	      "premiumFrequency": $scope.QPD.freqCostType,
  	      "memberType": null,
  	      "manageType": "CCOVER",
  	      "deathCoverType": "DcUnitised",
  	      "tpdCoverType": "TPDUnitised",
  	      "ipCoverType": "IpUnitised",
  	      "ipWaitingPeriod": $scope.QPD.addnlIpCoverDetails.waitingPeriod,
  	      "ipBenefitPeriod": $scope.QPD.addnlIpCoverDetails.benefitPeriod
  	    };
  	    CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res) {
  	    	var coverAmtPerUnit = 0;
  	    	var premium = res.data;
  	      
  	      for(var i = 0; i < premium.length; i++){
  	        if(premium[i].coverType == 'DcUnitised'){
  	        	coverAmtPerUnit = premium[i].coverAmount || 0;
  	        	if(coverAmtPerUnit>0)
  	        		{
  	        	$scope.deathUnitsForFixed = Math.ceil($scope.deathCoverDetails.amount/coverAmtPerUnit);
  	        		}
  	        	else
  	        		{
  	        		$scope.deathUnitsForFixed = 0;
  	        		}
  	        } else if(premium[i].coverType == 'TPDUnitised'){
  	        	coverAmtPerUnit = premium[i].coverAmount || 0;
  	        	if(coverAmtPerUnit>0)
	        		{
  	        	$scope.tpdUnitsForFixed = Math.ceil($scope.tpdCoverDetails.amount/coverAmtPerUnit);
	        		}
  	        	else
  	        		{
  	        		$scope.tpdUnitsForFixed = 0;
  	        		}
  	        }
  	      }
  	    if($scope.QPD.addnlDeathCoverDetails.deathInputTextValue == $scope.deathUnitsForFixedOld)
    	  		{
  	    		if(!$scope.deathUnitsForFixedOld == $scope.deathUnitsForFixed)
  	    			{
  	    				$scope.QPD.addnlDeathCoverDetails.deathInputTextValue =$scope.deathUnitsForFixed;
  	    			}
    	  		}
  	    if($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue == $scope.tpdUnitsForFixedOld)
  	    		{
  	    		if(!$scope.tpdUnitsForFixedOld == $scope.tpdUnitsForFixed)
  	    			{
  	    			$scope.QPD.addnlTpdCoverDetails.tpdInputTextValue =$scope.tpdUnitsForFixed;
  	    			}
  	    		}
  	      //$scope.QPD.totalPremium = parseFloat($scope.QPD.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.QPD.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.QPD.addnlIpCoverDetails.ipCoverPremium);
  	    }, function(err){
  	      console.info("Something went wrong while calculating..." + JSON.stringify(err));
  	    });
  	  };    

}]);
