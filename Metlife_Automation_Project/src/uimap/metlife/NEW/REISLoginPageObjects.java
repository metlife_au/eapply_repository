package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class REISLoginPageObjects extends PageObjectLibrary{
		
	public static final By MemberNumber = getObjectIdentifaction("REIS.login.MemberNo.id");
	public static final By Firstname = getObjectIdentifaction("REIS.login.FirstName.id");
	public static final By Surname = getObjectIdentifaction("REIS.login.SurName.id");
	public static final By DateofBirth = getObjectIdentifaction("REIS.login.DOB.id");
	public static final By Membertype = getObjectIdentifaction("REIS.login.MemberType.id");
	public static final By login = getObjectIdentifaction("REIS.login.Loginbutton.xpath");
}
