package pages.metlife.Angular.HOSTPLUS;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_ChangeYourInsuranceCover extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_ChangeYourInsuranceCover ChangeYourInsuranceCover_HOSTPLUS() {
		ChangeYourCover();
		HealthandLifeStyle();
		confirmation();

		return new HOSTPLUS_Angular_ChangeYourInsuranceCover(scriptHelper);
	}

	public HOSTPLUS_Angular_ChangeYourInsuranceCover EApplyRateCheck_HOSTPLUS() {
		EApplyRateValidation();

		return new HOSTPLUS_Angular_ChangeYourInsuranceCover(scriptHelper);
	}

	public HOSTPLUS_Angular_ChangeYourInsuranceCover(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void ChangeYourCover() {

		String EmailId = dataTable.getData("HOSTPLUS_ChangeInsurance", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_ChangeInsurance", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_ChangeInsurance", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_ChangeInsurance", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_ChangeInsurance", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_ChangeInsurance", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_ChangeInsurance", "RegularIncome");
		String Perform_WithoutRestriction = dataTable.getData("HOSTPLUS_ChangeInsurance", "Perform_WithoutRestriction");
		String Work_WithoutLimitation = dataTable.getData("HOSTPLUS_ChangeInsurance", "Work_WithoutLimitation");
		String Citizen = dataTable.getData("HOSTPLUS_ChangeInsurance", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_ChangeInsurance", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_ChangeInsurance", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_ChangeInsurance", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_ChangeInsurance", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_ChangeInsurance", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_ChangeInsurance", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_ChangeInsurance", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_ChangeInsurance", "AnnualSalary");
		String CostType=dataTable.getData("HOSTPLUS_ChangeInsurance", "CostType");
		String DeathYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathYN");
		String AmountType=dataTable.getData("HOSTPLUS_ChangeInsurance", "AmountType");
		String DeathAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAction");
		String DeathAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAmount");
		String DeathUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathUnits");
		String TPDYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDYN");
		String TPDAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAction");
		String TPDAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAmount");
		String TPDUnits=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDUnits");
		String IPYN=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPYN");
		String IPAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAction");
		String Insure90Percent=dataTable.getData("HOSTPLUS_ChangeInsurance", "Insure90Percent");
		String WaitingPeriod=dataTable.getData("HOSTPLUS_ChangeInsurance", "WaitingPeriod");
		String BenefitPeriod=dataTable.getData("HOSTPLUS_ChangeInsurance", "BenefitPeriod");
		String IPAmount=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAmount");


		try{



			//****Click on Change Your Insurance Cover Link*****\\
		//	waitForPageTitle("testing", 17);
//			waitForPageTitle("testing", 8);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Change your insurance cover']"))).click();

			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);
			sleep(2000);
			if(driver.getPageSource().contains("You have previously saved application(s).")){
				//		if(driver.findElement(By.xpath("//button[@ng-click='confirm()']")).isDisplayed()){ //button[text()='Continue']
							sleep(500);
							quickSwitchWindows();
							sleep(1000);
							driver.findElement(By.xpath("//button[text()='Cancel']")).click();
							report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
							sleep(2000);

						}

				//*****Agree to Duty of disclosure*******\\
				waitForPageTitle("testing", 1);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);



	           //*****Agree to Privacy Statement*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*"))).click();
				sleep(250);
				report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
				sleep(200);

				//*****Enter the Email Id*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
				report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
				sleep(500);

				//*****Click on the Contact Number Type****\\

				Select Contact=new Select(driver.findElement(By.name("contactType")));
				Contact.selectByVisibleText(TypeofContact);
				report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
				sleep(250);


			//****Enter the Contact Number****\\

				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

				//***Select the Preferred time of Contact*****\\

				if(TimeofContact.equalsIgnoreCase("Morning")){

					driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]")).click();

					sleep(400);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}
				else{
					driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]")).click();
					sleep(400);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}


				//***********Select the Gender******************\\

				if(Gender.equalsIgnoreCase("Male")){

					driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}
				else{
					driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
					sleep(250);
					report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
				}

				//****Contact Details-Continue Button*****\\
			/*
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(formOne);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
				sleep(1200);	*/

				//***************************************\\
				//**********OCCUPATION SECTION***********\\
				//****************************************\\

					//*****Select the 15 Hours Question*******\\

				if(FifteenHoursWork.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

					}
				else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion']/parent::*"))).click();
						sleep(500);
						report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

					}

			//***Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*****\\

				if(RegularIncome.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
					sleep(300);

					//*****Perform duty without restriction due to illness or injury?*****\\

					if(Perform_WithoutRestriction.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected Yes", Status.PASS);
						sleep(300);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected No", Status.PASS);
						sleep(300);
					}
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
					sleep(300);

	                   //*****Work Without Limitations *****\\

					if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Work Without Limitations", "Selected Yes", Status.PASS);
						sleep(300);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Work Without Limitations", "Selected No", Status.PASS);
						sleep(300);
					}

				}

				//*****Resident of Australia****\\

				/*if(Citizen.equalsIgnoreCase("Yes")){
					driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

					}
				else{
					driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]")).click();
					sleep(250);
					report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

					}*/

				//*****Industry********\\
			    sleep(500);
			    Select Industry=new Select(driver.findElement(By.name("industry")));
			    Industry.selectByVisibleText(IndustryType);
			    sleep(500);
				report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
			//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();


				//*****Occupation*****\\

				Select Occupation=new Select(driver.findElement(By.name("occupation")));
				Occupation.selectByVisibleText(OccupationType);
			    sleep(800);
				report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
				sleep(1000);


				//*****Other Occupation *****\\

				if(OccupationType.equalsIgnoreCase("Other")){

					wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
					report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
					sleep(1000);
				}


				//*****Extra Questions*****\\

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
					sleep(1000);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
				sleep(1000);
			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
						sleep(1000);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
					sleep(1000);
			}
			}



			else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



			//******Do you work in hazardous environment*****\\

			if(HazardousEnv.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[1]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				sleep(1000);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				sleep(1000);
			}

			//******Do you spend more than 10% of your working time outside of an office environment?*******\\

			if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				sleep(1000);
			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
				report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				sleep(1000);
			}
			 sleep(1800);

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    	 if(OfficeEnvironment.equalsIgnoreCase("Yes")){

					    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
							sleep(1000);

					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
						sleep(1000);

					}

					      //*****Do You Hold a Tertiary Qualification******\\

					        if(TertiaryQual.equalsIgnoreCase("Yes")){

					        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
								report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
								sleep(1000);
					}
					   else{

						   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
							sleep(1000);
					}
					}

		}

	      sleep(800);


		//*****What is your annual Salary******


		    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
		    sleep(3000);

		    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //label[text()='What is your annual salary?']"))).click();
		    sleep(500);
		    sleep(300);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,400)", "");
			sleep(500);

		//*****Click on Continue*******\\
	/*
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(occupationForm);']"))).click();
		report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
		*/


	//**************************************************************************************************************\\
	//****************************COVER CALCULATOR*****************************************************************\\
	//*************************************************************************************************************\\

					//******Click on Cost of  Insurance as*******\\
		sleep(1000);
		Select COSTTYPE=new Select(driver.findElement(By.name("premFreq")));
		COSTTYPE.selectByVisibleText(CostType);
		sleep(250);
		report.updateTestLog("Cost of Insurance", "Cost frequency selected is: "+CostType, Status.PASS);
		sleep(500);



					//*******Death Cover Section******\\

		//restricted 3 digits
		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
		sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
		sleep(500);

					if(DeathYN.equalsIgnoreCase("Yes")){

						Select deathaction=new Select(driver.findElement(By.name("coverName")));
						deathaction.selectByVisibleText(DeathAction);
						sleep(300);
						report.updateTestLog("Death Cover action", DeathAction+" is selected on Death cover", Status.PASS);
						sleep(600);


	               if(AmountType.equalsIgnoreCase("unit")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
							sleep(800);
							report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

						}
						if(AmountType.equalsIgnoreCase("Convert")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
							sleep(800);
							report.updateTestLog("Fixed radio button","Unitised to Fixed radio button is Selected", Status.PASS);

						}


					if(DeathAction.equalsIgnoreCase("Increase your cover")||DeathAction.equalsIgnoreCase("Decrease your cover")){

						if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert")){
					//	if(DeathCost.trim().equalsIgnoreCase("Fixed")){
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathAmount);
							sleep(800);
							report.updateTestLog("Death Cover Amount", DeathAmount+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
							waitForPageTitle("testing", 3);


						}
						else{
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathUnits);
							sleep(800);
							report.updateTestLog("Death Cover Amount", DeathUnits+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
							sleep(800);
							waitForPageTitle("testing", 3);
						}
					}
				//	}

					//*******TPD Cover Section******\\
				    sleep(800);


					if(TPDYN.equalsIgnoreCase("Yes"))
					{

						Select tpdaction=new Select(driver.findElement(By.name("tpdCoverName")));
						tpdaction.selectByVisibleText(TPDAction);
						sleep(500);
						report.updateTestLog("TPD Cover action", TPDAction+" is selected on TPD cover", Status.PASS);
						sleep(800);

						if(TPDAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")){

							if(AmountType.equalsIgnoreCase("Fixed")||AmountType.equalsIgnoreCase("Convert")) {


								sleep(250);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								sleep(500);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDAmount);
								sleep(800);
								report.updateTestLog("TPD Cover Amount", TPDAmount+" is entered", Status.PASS);
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
								sleep(800);
								waitForPageTitle("testing", 3);
							}else{
								sleep(250);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
								sleep(500);
								wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(TPDUnits);
								sleep(800);
								report.updateTestLog("TPD Cover Amount", TPDUnits+" is entered", Status.PASS);
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
								sleep(800);
								waitForPageTitle("testing", 3);
							}
						}

					}




				                 //*******IP Cover section********\\
				sleep(500);
				if(IPYN.equalsIgnoreCase("Yes")){
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("window.scrollBy(0,400)", "");
					sleep(500);

					Select ipaction=new Select(driver.findElement(By.name("ipCoverName")));
					ipaction.selectByVisibleText(IPAction);
					sleep(500);
					report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);

					//*****Insure 90% of My Salary******\\

					if(!IPAction.equalsIgnoreCase("No change")){
						if(Insure90Percent.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='ipsalarycheck']/parent::*"))).click();
						report.updateTestLog("Insure 90%", "Insure 90% of My Salary is Selected", Status.PASS);
						sleep(500);
						waitForPageTitle("testing", 1);
						}
					}
					sleep(500);
					if(!IPAction.equalsIgnoreCase("No change")){

						Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.waitingPeriod']")));
						waitingperiod.selectByVisibleText(WaitingPeriod);
						sleep(300);
						report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
						sleep(500);


						Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.benefitPeriod']")));
						benefitperiod.selectByVisibleText(BenefitPeriod);
						sleep(300);
						report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
						sleep(2000);
					}
					sleep(500);
					if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){

						if(!Insure90Percent.equalsIgnoreCase("Yes"))
					{
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(IPAmount);
							sleep(500);
							report.updateTestLog("IP Cover Amount", IPAmount+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
							sleep(800);
							waitForPageTitle("testing", 1);
						}

					}
			}
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,700)", "");
				sleep(500);


				//*****Click on Calculate Quote********\\

		/*		sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
				report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
				sleep(4000);*/




				//******Click on Continue button******\\

			    //jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			//	sleep(300);


			//	 driver.findElement(By.xpath("(//button[contains(text(),'Continue')])[3]")).click();

				if(DeathYN.equalsIgnoreCase("Yes")||TPDYN.equalsIgnoreCase("Yes")||IPYN.equalsIgnoreCase("Yes")||DeathAction.equalsIgnoreCase("No Change")&&TPDAction.equalsIgnoreCase("No Change")&&IPAction.equalsIgnoreCase("No Change")){
					driver.findElement(By.xpath("//button[@ng-click='goToAura()']")).click();
					report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
					sleep(500);

				}



				//*****Handling the pop-up*******\\

					 if(DeathAction.equalsIgnoreCase("Decrease your cover")||DeathAction.equalsIgnoreCase("Cancel your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")||TPDAction.equalsIgnoreCase("Cancel your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){
//						 driver.findElement(By.xpath("//*[@ng-click='showDecreaseOrCancelPopUp(decCancelMsg)']")).click();
//							report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
//							sleep(500);

//						quickSwitchWindows();
//						sleep(1000);

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='goTo()']"))).click();
						report.updateTestLog("Pop-Up", "Pop-up continue button is Clicked", Status.PASS);
						sleep(300);
//						driver.findElement(By.xpath("//button[@ng-click='goToAura()']")).click();
//						report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
						sleep(1000);


					 }
		//				else{
//						 driver.findElement(By.xpath("//*[@ng-click='goToAura()']")).click();
//						report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
//						sleep(100);
					 }
				sleep(1000);
				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
			}

		public void HealthandLifeStyle() {

			String Height=dataTable.getData("HOSTPLUS_ChangeInsurance", "Height");
			String HeightType=dataTable.getData("HOSTPLUS_ChangeInsurance", "HeightType");
			String Weight=dataTable.getData("HOSTPLUS_ChangeInsurance", "Weight");
			String WeightType=dataTable.getData("HOSTPLUS_ChangeInsurance", "WeightType");
			String Smoker=dataTable.getData("HOSTPLUS_ChangeInsurance", "Smoker");
			String Gender = dataTable.getData("HOSTPLUS_ChangeInsurance", "Gender");
			String Pregnant = dataTable.getData("HOSTPLUS_ChangeInsurance", "Pregnant");

			String Exclusion_3Years_Question_for_TPDandIP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Exclusion_3Years_Question_for_TPDandIP");
			String Loading_3Years_Questions_for_Death50_TPD50_IP100=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_3Years_Questions_for_Death50_TPD50_IP100");
			String Loading_3Years_Questions_for_Death100_TPD100_IP150=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_3Years_Questions_for_Death100_TPD100_IP150");


			String Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");

			String Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP");
			String Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP");


			String UsualDoctororMedicalCentre=dataTable.getData("HOSTPLUS_ChangeInsurance", "UsualDoctororMedicalCentre");
			String UsualDoctor_Name=dataTable.getData("HOSTPLUS_ChangeInsurance", "UsualDoctor_Name");
			String UsualDoctor_ContactNumber=dataTable.getData("HOSTPLUS_ChangeInsurance", "UsualDoctor_ContactNumber");
			String Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50=dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50");
			String Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
			String Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP=dataTable.getData("HOSTPLUS_ChangeInsurance", "Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP");
			String Drugs_Last5Years=dataTable.getData("HOSTPLUS_ChangeInsurance", "Drugs_Last5Years");
			String Alcohol_DrinksPerDay=dataTable.getData("HOSTPLUS_ChangeInsurance", "Alcohol_DrinksPerDay");
			String Alcohol_Professionaladvice=dataTable.getData("HOSTPLUS_ChangeInsurance", "Alcohol_Professionaladvice");
			String HIVInfected=dataTable.getData("HOSTPLUS_ChangeInsurance", "HIVInfected");
			String PriorApplication=dataTable.getData("HOSTPLUS_ChangeInsurance", "PriorApplication");
			String PreviousClaims=dataTable.getData("HOSTPLUS_ChangeInsurance", "PreviousClaims");
			String PreviousClaims_PaidBenefit_terminalillness=dataTable.getData("HOSTPLUS_ChangeInsurance", "PreviousClaims_PaidBenefit_terminalillness");
			String CurrentPolicies=dataTable.getData("HOSTPLUS_ChangeInsurance", "CurrentPolicies");
			String DeathAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "DeathAction");
			String TPDAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "TPDAction");
			String IPAction=dataTable.getData("HOSTPLUS_ChangeInsurance", "IPAction");



			try{
				if(DeathAction.equalsIgnoreCase("Increase your cover")||TPDAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Increase your cover"))
			{
				WebDriverWait waiting=new WebDriverWait(driver,20);


				//******Enter the Height*******\\


				waiting.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='heightval']"))).sendKeys(Height);
				sleep(200);
				report.updateTestLog("Height", Height+" is Entered", Status.PASS);
				sleep(250);

				WebDriverWait wait=new WebDriverWait(driver,18);
				//******Choose Height Type******\\

				Select heighttype=new Select(driver.findElement(By.xpath("//*[@ng-model='heightDropDown']")));
				heighttype.selectByVisibleText(HeightType);
				sleep(250);
				report.updateTestLog("Height Type", HeightType+" is Selected", Status.PASS);
				sleep(500);

				//******Enter the Weight*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='weightVal']"))).sendKeys(Weight);
				sleep(200);
				report.updateTestLog("Weight",Weight+" is Entered", Status.PASS);
				sleep(250);

				//******Choose Weight Type******\\

				Select weighttype=new Select(driver.findElement(By.xpath("//*[@ng-model='weightDropDown']")));
				weighttype.selectByVisibleText(WeightType);
				sleep(250);
				report.updateTestLog("Weight Type", WeightType+" is Selected", Status.PASS);
				sleep(500);

				//******Have you smoked in the past 12 months?*******\\

				if(Smoker.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Smoker Question", "Yes is Selected", Status.PASS);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you smoked in the past 12 months?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Smoker Question", "No is Selected", Status.PASS);
				}
				sleep(800);

				if(Gender.equalsIgnoreCase("Female")){

					//******Are you currently pregnant?*******\\

				if(Pregnant.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Are you currently pregnant?", "Yes is Selected", Status.PASS);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you currently pregnant?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Are you currently pregnant?", "No is Selected", Status.PASS);
				}
				sleep(800);
				}

				//**********************************************************************************************************************\\
				//*******In the last 3 years have you suffered from, been diagnosed with or sought medical advice or treatment for*******\\
				//***********************************************************************************************************************\\

				if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Headaches or migraines')]]/span"))).click();
					sleep(250);
					report.updateTestLog("Last 3 Years Question", "Headaches or migraines is Selected", Status.PASS);
					sleep(800);

				//******Are you currently under investigations or contemplating investigations for your headaches?*****\\

						driver.findElement(By.xpath(" //div/label[text()='Are you currently under investigations or contemplating investigations for your headaches?']/../following-sibling::div/div/div[2]/div/div/label")).click();
						sleep(500);
						report.updateTestLog("Headache or Migraines_Under Current Investigations", "No is Selected", Status.PASS);
						sleep(200);

					//******How would you describe your headaches?******\\

						Select headaches=new Select(driver.findElement(By.xpath("//*[@ng-model='selectedName']")));
						headaches.selectByVisibleText("Recurring or severe episodes");
						sleep(500);
						report.updateTestLog("Type of Headache", "Recurring or severe episodes is Selected", Status.PASS);
						sleep(800);

					//******Select the Headache Type*******\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='vm.updateRadio(selectedName.answerText,x)']"))).click();
						sleep(500);




				//*****In the last 6 months, have you had a headache lasting for 10 days or longer?*****\\
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='In the last 6 months, have you had a headache lasting for 10 days or longer?']/../following-sibling::div/div/div[1]/div/div"))).click();
						sleep(500);
						report.updateTestLog("Is headache fully investigated?","Yes is Selected", Status.PASS);
						sleep(800);

						//**************Are these headaches relieved with analgesics (eg. Paracetamol)?************\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are these headaches relieved with analgesics (eg. Paracetamol)?']/../following-sibling::div/div/div[1]/div/div"))).click();
						sleep(500);
						report.updateTestLog("Is headache fully investigated?","Yes is Selected", Status.PASS);
						sleep(800);



					//*****How many headaches do you suffer in a week?******\\

						Select headachesno=new Select(driver.findElement(By.xpath("(//*[@ng-model='selectedName'])[2]")));
						headachesno.selectByVisibleText("6 episodes or less");
						sleep(500);
						report.updateTestLog("Number of Headaches","6 episodes or less is Selected", Status.PASS);
						sleep(500);

						driver.findElement(By.xpath("//*[@ng-click='vm.updateRadio(selectedName.answerText,x)']")).click();
						sleep(800);



					//*******Is this easily controlled with over the counter medication?******\\

						/*
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is this easily controlled with over the counter medication?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
						sleep(500);
						report.updateTestLog("Can  your headache be controlled over Medication", "Yes is Selected", Status.PASS);
						sleep(500);*/

				}

				if(Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("Yes")||Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Lung or breathing conditions')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Last 3 Years Question", "Lung or breathing conditions is Selected", Status.PASS);
					sleep(500);

					//****What was Your Diagnosis*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Asthma')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Lung or breathing conditions", "Asthma is Selected", Status.PASS);
					sleep(1000);

					//****Is your condition?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Moderate  ']"))).click();
					sleep(500);
					report.updateTestLog("Lung or breathing conditions", "Moderate Asthma is Selected", Status.PASS);
					sleep(500);

				/*	//****Is your asthma worsened by your occupation?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Moderate  ']"))).click();
					sleep(500);
					report.updateTestLog("Lung or breathing conditions", "Moderate Asthma is Selected", Status.PASS);
					sleep(500);*/


					if(Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("Yes")){

						//****Is your Asthma worsened by your condition****\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[1]/div/div"))).click();
						sleep(500);
						report.updateTestLog("Lung or breathing conditions", "Asthma is worsened by current occupation", Status.PASS);
						sleep(500);

					}
					else{
	                    //****Is your Asthma worsened by your condition****\\

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is your asthma worsened by your occupation?']/../following-sibling::div/div/div[2]/div/div"))).click();
						sleep(500);
						report.updateTestLog("Lung or breathing conditions", "Asthma is not worsened by current occupation", Status.PASS);
						sleep(500);
					}


				}

				if(Exclusion_3Years_Question_for_TPDandIP.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death100_TPD100_IP150.equalsIgnoreCase("No")&&Loading_3Years_Questions_for_Death50_TPD50_IP100.equalsIgnoreCase("No")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[1]"))).click();
					sleep(500);
					report.updateTestLog("Last 3 Years Question?","None of the above is Selected", Status.PASS);
					sleep(500);
				}
				sleep(1000);

				//********************************************************************************\\
				//*****************************5 Years Question***********************************\\
				//********************************************************************************\\

				if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'High cholesterol')]]/span"))).click();
				sleep(500);
				report.updateTestLog("Last 5 Years Question", "High Cholesterol is Selected", Status.PASS);
				sleep(500);

			  //******How is your high Cholesterol being treated??******\\

				Select cholesteroltreatment=new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
				cholesteroltreatment.selectByVisibleText("Medication prescribed by your doctor");
				sleep(500);
				report.updateTestLog("High Cholesterol","Treated by medication prescribed by your doctor", Status.PASS);
				sleep(500);

			  //******Select the Treatment Type*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']"))).click();
					sleep(500);

			  //******When did you last have a reading taken?*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'12 months ago or less')]]"))).click();
					sleep(500);
					report.updateTestLog("High Cholesterol","Last reading taken 12 months ago or less", Status.PASS);
					sleep(500);

				//****How did your doctor describe your most recent cholesterol reading?*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Elevated  ']"))).click();
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Elevated  ']"))).click();
					report.updateTestLog("High Cholesterol","Last reading was described by doctor as Elevated", Status.PASS);
					sleep(500);

			   //****Do you recall your last reading?***\\


					driver.findElement(By.xpath("//div/label[text()='Do you recall your last reading?']/../following-sibling::div/div/div[1]/div/div/label")).click();
					sleep(500);
					report.updateTestLog("High Cholesterol","Do you remember the last Reading- Yes", Status.PASS);
					sleep(500);

					//*****What was your most recent cholesterol reading taken by your doctor?****\\

					driver.findElement(By.xpath("(//*[@ng-model='rangeText'])[1]")).sendKeys("7.1");
					sleep(500);
					report.updateTestLog("High Cholesterol","Last reading is 7.1 mmol/L", Status.PASS);
					sleep(500);

					driver.findElement(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]")).click();
					sleep(1000);

					//*****Have you been advised that your triglycerides are elevated?****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been advised that your triglycerides are elevated? ']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("High Cholesterol","Tryglycerides is not Elevated", Status.PASS);
					sleep(800);


				}
				if(Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore.equalsIgnoreCase("No")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[2]"))).click();
					sleep(1000);
					report.updateTestLog("Last 5 Years Question","None of the above is Selected", Status.PASS);
					sleep(800);
				}
				sleep(800);

			//****************************************************************************************************************************************\\
			//**********Have you ever suffered from, been diagnosed with or sought medical advice or treatment for (Please tick all that apply):*******\\
	        //*****************************************************************************************************************************************\\

				if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Bone, joint or limb conditions')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Prior Treatment or Diagnosis Question","Bone,Joint or limb conditions is Selected", Status.PASS);
					sleep(1000);

					//*******What was your diagnosis?********\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Amputation/missing or deformed limb')]]/span"))).click();
					sleep(500);
					report.updateTestLog("What was your diagnosis?","Amputation/missing or deformed limb is Selected", Status.PASS);
					sleep(1000);

					//*******Which limbs were amputated/missing or deformed?*****\\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Arm(s)')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Which limbs were amputated/missing or deformed?"," Arm(s) is Selected", Status.PASS);
					sleep(1200);

					//****was it?****\\

					Select arm=new Select(driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/select")));
					arm.selectByVisibleText("The left arm");
					sleep(500);
					report.updateTestLog("Which Limb?","The left arm is Selected", Status.PASS);
					sleep(500);

					driver.findElement(By.xpath("//*[@ng-click='updateRadio(selectedName.answerText,x)']")).click();
					sleep(1000);

				}

				if(Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Thyroid conditions')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Prior Treatment or Diagnosis Question","Thyroid Conditions is Selected", Status.PASS);
					sleep(800);

					//*****What was the specific condition*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Hyperthyroidism (Overactive)')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Thyroid Conditions"," Hyperthyroidism (Overactive) is Selected", Status.PASS);
					sleep(800);

					//****Has your condition been fully investigated?******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Has your condition been fully investigated?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Thyroid Conditions","Condition has been fully investigated is Selected", Status.PASS);
					sleep(800);

					//****Is your condition Fully Controlled*****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Is your condition fully controlled?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Thyroid Conditions","Condition is not fully controlled is Selected", Status.PASS);
					sleep(1000);


				}


				if(Exclusion_PriorTreatmentorDiagnosis_Question_for_TPD_IP.equalsIgnoreCase("No")&&Loading_PriorTreatmentorDiagnosis_Question_for_Death50_TPD50_RUWforIP.equalsIgnoreCase("No")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'None')]]/span)[3]"))).click();
					sleep(500);
					report.updateTestLog("Prior Treatment or Diagnosis?","None of the above is Selected", Status.PASS);
					sleep(2000);
				}
				sleep(500);



				//******Do you have a usual doctor or medical centre you regularly visit?******\\

				if(UsualDoctororMedicalCentre.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div/div/div/label)[1]"))).click();
					sleep(1500);
					report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","Yes is Selected", Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeTextArea']"))).sendKeys(UsualDoctor_Name);
					sleep(1500);
					report.updateTestLog("Usual Doctor Name",UsualDoctor_Name+" is Entered", Status.PASS);
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeTextArea,x)']"))).click();
					sleep(1500);


					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='freeText']"))).sendKeys(UsualDoctor_ContactNumber);
					sleep(1500);
					report.updateTestLog("Usual Doctor Contact NUmber",UsualDoctor_ContactNumber+" is Entered", Status.PASS);
					sleep(1000);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='updateRadio(freeText,x)']"))).click();
					sleep(1500);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Add another Doctor?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1500);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have a usual doctor or medical centre you regularly visit?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1500);
					report.updateTestLog("Is there a usual Doctor/Medical Centre you visit","No is Selected", Status.PASS);
				}


				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[1]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(7000);

				//******************************************************************************************************\\
				//*********************************Family History*********************************************************\\
				//******************************************************************************************************\\

				//****Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions***\\

				if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
					WebDriverWait waits=new WebDriverWait(driver,25);
					waits.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(2600);
					report.updateTestLog("Family History Question","Yes is Selected", Status.PASS);

					//***Select the health condition****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[text()[contains(.,'Diabetes')]]/span)[2]"))).click();
					sleep(2600);
					report.updateTestLog("Health condition diagnosed"," Diabetes is Selected", Status.PASS);
					sleep(500);

					//***How many family members were affected?****\\

					driver.findElement(By.xpath("//div/label[text()='How many family members were affected?']/../following-sibling::div/div[1]/input")).sendKeys("3");
					sleep(1000);
					report.updateTestLog("Number of Family members affected","3 is Entered", Status.PASS);
					sleep(400);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@ng-click='updateRadio(rangeText,x)'])[1]"))).click();
					sleep(500);

					//***were two or more family members diagnosed over age 19****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //div/label[text()='Were two or more family members diagnosed over the age of 19?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(2600);
					report.updateTestLog("Number of affected Family members over age 19","Two or more is Selected", Status.PASS);
					sleep(1000);

				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has your mother, father, any brother, or sister been diagnosed under the age of 55 years with any of the following conditions: Alzheimer�s disease, Cancer, Dementia, Diabetes, Familial Polyposis, Heart disease, Huntington�s disease, Motor Neurone disease, Multiple Sclerosis, Muscular Dystrophy, Polycystic Kidney disease, Stroke or any inherited or hereditary disease? Note: You are only required to disclose family history information pertaining to first degree blood related family members - living or deceased.']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(2600);
					report.updateTestLog("Family History Question","No is Selected", Status.PASS);
					sleep(500);
				}


				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[2]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(2600);

				//********************************************************************************************************\\
				//******************************Lifestyle Questions********************************************************\\
				//*********************************************************************************************************\\

				//*******Do you have firm plans to travel or reside in another country*******\\
				if(Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Plans to travel outside the country","Yes is Selected", Status.PASS);
					sleep(500);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you have firm plans to travel or reside in another country other than NZ, the United States of America, Canada, the United Kingdom or the European Union?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Plans to travel outside the country","No is Selected", Status.PASS);
					sleep(1000);
				}


				//******Do you regularly engage in or intend to engage in any of the following hazardous activities*****\\

				if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Water sports')]]/span"))).click();
					sleep(500);
					report.updateTestLog("Do you engage in any Hazardous activities","Water sports is Selected", Status.PASS);
					sleep(1000);



					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'Underwater diving')]]/span"))).click();
					sleep(3100);
					report.updateTestLog("Hazardous activity Type","Underwater diving is Selected", Status.PASS);
					sleep(2000);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()[contains(.,'More than 40 metres')]]"))).click();
					sleep(3100);
					report.updateTestLog("Dept of diving ","More than 40 metres is Selected", Status.PASS);
					sleep(2000);
				}

				if(Lifestyle_Exclusion_HazardousActivities_Question_for_Death_TPD_IP.equalsIgnoreCase("No")){
					sleep(1000);
					if(Loading_FamilyHealthHistory_Question_for_Death50_TPD50_IP50.equalsIgnoreCase("Yes")){
						driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[5]")).click();
						sleep(2000);
					}
					else{
						driver.findElement(By.xpath("(//*[text()[contains(.,'None')]]/span)[4]")).click();
						sleep(2000);
					}


					sleep(500);
					report.updateTestLog("Do you engage in any Hazardous activities","None of the above is Selected", Status.PASS);
					sleep(500);
				}






				//*****Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter)
				//or have you exceeded the recommended dosage for any medication?

				if(Drugs_Last5Years.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Have you sed any drugs in the last 5 years","Yes is Selected", Status.PASS);
					sleep(800);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you within the last 5 years used any drugs that were not prescribed to you (other than those available over the counter) or have you exceeded the recommended dosage for any medication?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Have you used any drugs in the last 5 years","No is Selected", Status.PASS);
					sleep(800);
				}


				//*****How many alcoholic drinks you have in a day?*****\\
				sleep(500);
				driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).click();
				sleep(700);
				driver.findElement(By.xpath("//div[@class='col-sm-5  col-xs-10']/input")).sendKeys(Alcohol_DrinksPerDay);
				sleep(700);
				report.updateTestLog("How many alcoholic drinks per day","1 is Selected", Status.PASS);
				sleep(700);
				driver.findElement(By.xpath("//button[text()='Enter']")).click();
				sleep(1500);

				//****Have you ever been advised by a health professional to reduce your alcohol consumption?****\\

				if(Alcohol_Professionaladvice.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Advised to reduce alcohol consumption","Yes is Selected", Status.PASS);
					sleep(1000);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you ever been advised by a health professional to reduce your alcohol consumption?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(1000);
					report.updateTestLog("Advised to reduce alcohol consumption","No is Selected", Status.PASS);
					sleep(1000);
				}


				//******Are you infected with HIV (Human Immunodeficiency Virus******\\

				if(HIVInfected.equalsIgnoreCase("Yes")){
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1500);
					report.updateTestLog("Are you infected with HIV","Yes is Selected", Status.PASS);
					sleep(1500);

				}
				else{
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you infected with HIV (Human Immunodeficiency Virus), the virus which can cause/lead to AIDS (Acquired Immune Deficiency Syndrome)?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(5000);
					report.updateTestLog("Are you infected with HIV","No is Selected", Status.PASS);
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been referred for or waiting on an HIV test result and/or are taking preventative medication?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(4500);
				}



				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[3]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(1000);

				//******************************************************************************************************\\
				//****************************General Questions******************************************************\\
				//*******************************************************************************************************\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Other than already disclosed in this application, do you presently suffer from any condition, injury or illness which you suspect may require medical advice or treatment in the future?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
				sleep(1000);
				report.updateTestLog("Do you presently suffer from any condition?","No is Selected", Status.PASS);

				//******Click on Continue button*****\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[text()='Continue'])[4]"))).click();
				report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
				sleep(3600);

				//***********************************************************************************************\\
				//***************************INSURANCE DETAILS****************************************************\\
				//**************************************************************************************************\\

				//*****Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or
				//accepted with a loading or exclusion or any other special conditions or terms?

				if(PriorApplication.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='collapseOne']/div/div[1]/div/div[2]/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","Yes is Selected", Status.PASS);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(1000);
				}

				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Has an application for Life, Trauma, TPD or Disability Insurance on your life ever been declined, deferred or accepted with a loading or exclusion or any other special conditions or terms?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep();
					report.updateTestLog("Has your prior application beend Delined/Accepted with Loading?","No is Selected", Status.PASS);
				}




				//****Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers�
				//Compensation, or any other form of compensation due to illness or injury?
				sleep(200);
				if(PreviousClaims.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[1]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Have you made a prior claim?","Yes is Selected", Status.PASS);

					if(PreviousClaims_PaidBenefit_terminalillness.equalsIgnoreCase("Yes")){
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[1]/div/div/label/span"))).click();
						sleep(800);
					}
					else{
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Have you been paid, are you currently claiming for or are you contemplating a claim for terminal illness benefit? ']/../following-sibling::div/div/div[2]/div/div/label/span"))).click();
						sleep(800);
					}

				}
				else{
					sleep(400);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Are you contemplating or have you ever made a claim for or received sickness, accident or disability benefits, Workers� Compensation, or any other form of compensation due to illness or injury?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
					sleep(500);
					report.updateTestLog("Have you made a prior claim?","No is Selected", Status.PASS);
				}


				if(driver.getPageSource().contains("Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?")){

					//*****Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?
					 if(CurrentPolicies.equalsIgnoreCase("yes")){
						 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='radio1-0-19:0']"))).click();
							sleep(500);
							report.updateTestLog("Do you currently have any insurance policies?","Yes is Selected", Status.PASS);

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='check1-0-19_0_0:0']"))).click();
							sleep(1500);

							wait.until(ExpectedConditions.elementToBeClickable(By.id("ran1-19_0_0_0_0"))).sendKeys("90000");
							sleep(1500);

							wait.until(ExpectedConditions.elementToBeClickable(By.id("ran219_0_0_0_0-Insurance_Details-Insurance_Details-19"))).click();
							sleep(1500);
					 }
					 else{
						 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[text()='Do you currently have or are you applying for any Life, Trauma, TPD or Disability Insurance policies with us or any other insurance company or superannuation fund?']/../following-sibling::div/div/div[2]/div/div/label"))).click();
							sleep(1500);
							report.updateTestLog("Do you currently have any insurance policies?","No is Selected", Status.PASS);
					 }

				}




						//****insurance detail click on Continue****\\

				/*		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@ng-click='collapseUncollapse($index,section.sectionName)'])[5]"))).click();
						report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
						sleep(500);

						  sleep(300);
							JavascriptExecutor jse = (JavascriptExecutor) driver;
							jse.executeScript("window.scrollBy(0,500)", "");
							sleep(500);*/

		        //****Treatment question****\\

				/*	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@class='ipradio']/parent::*)[11]"))).click();
					report.updateTestLog("Treatment Question","No button is Clicked", Status.PASS);
					sleep(1000);*/

					//****Click on Continue****\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedToNext()']"))).click();
					report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
					sleep(4000);

		//		sleep(20000);
//				waitForPageTitle("testing", 8);

				}
			}
			catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

		public void confirmation() {
			String occupationdecline = dataTable.getData("HOSTPLUS_ChangeInsurance", "occupationdecline");
			String Exclusiontext = dataTable.getData("HOSTPLUS_ChangeInsurance", "Exclusiontext");
			String Lifestylequestion= dataTable.getData("HOSTPLUS_ChangeInsurance", "Lifestyle_Exclusion_TravelPlans_Question_for_Death_TPD_IP");
			String Loadingquestion= dataTable.getData("HOSTPLUS_ChangeInsurance", "Loading_5Years_Question_for_Death_TPD_IP_75or50_Age30orMore");
			String LoadingtextDeath = dataTable.getData("HOSTPLUS_ChangeInsurance", "LoadingtextDeath");
			String LoadingtextTPD = dataTable.getData("HOSTPLUS_ChangeInsurance", "LoadingtextTPD");


			WebDriverWait wait=new WebDriverWait(driver,20);

			//****Loading/exclusion acknowledge checkbox*****\\

				try{

					if(driver.getPageSource().contains("I acknowledge I have read and understood the above exclusion(s) and/or loading(s) which apply to this cover.")){
						String exclusion = driver.findElement(By.xpath("//p[@class='normal ng-binding']")).getText();
						System.out.println("Exclusion message--->" +exclusion);
						sleep(100);
						System.out.println("Exclusion message--->" +Exclusiontext);
						sleep(100);
						if(Lifestylequestion.equalsIgnoreCase("Yes")){
						if(Exclusiontext.equalsIgnoreCase(exclusion)){
							report.updateTestLog("Exclusion(s) message: ", "Exclusion(s) message is dispalyed as expected", Status.PASS);
						}else{
							report.updateTestLog("Exclusion(s) message: ", "Exclusion(s) message is dispalyed as expected", Status.FAIL);
						}
						}
						if(Loadingquestion.equalsIgnoreCase("Yes")){
							String loadingdeath = driver.findElement(By.xpath("(//p[@class='normal ng-binding'])[1]")).getText();
							String loadingTpd = driver.findElement(By.xpath("(//p[@class='normal ng-binding'])[2]")).getText();
							if(LoadingtextDeath.equalsIgnoreCase(loadingdeath)){
								report.updateTestLog("loading(s) Death message: ", "Loading(s) Death message is dispalyed as expected", Status.PASS);
							}else{
								report.updateTestLog("loading(s) Death message: ", "Loading(s) Death message is dispalyed as expected", Status.FAIL);
							}
							if(LoadingtextTPD.equalsIgnoreCase(loadingTpd)){
								report.updateTestLog("loading(s)TPD message: ", "Loading(s) TPD message is dispalyed as expected", Status.PASS);
							}else{
								report.updateTestLog("loading(s)TPD message: ", "Loading(s) TPD message is dispalyed as expected", Status.FAIL);
							}
						}

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@id='lodadingExclusionLabel']/span"))).click();
						report.updateTestLog("Exclusion(s) and/or loading(s) acknowledgement checkbox: ", "Checkbox is clicked", Status.PASS);

					}

					if(driver.getPageSource().contains("I acknowledge and consent to the above.")){

				//****Loading consent*****\\
	/*			sleep(1000);
				if(driver.getPageSource().contains("We wish to note that in considering the information disclosed in your personal statement")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='loading_chckbox_id__xc_c']/span"))).click();
					report.updateTestLog("Loading Consent", "selected the Checkbox", Status.PASS);
					sleep(500);
				}	*/


				//******Agree to general Consent*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='generalConsentLabel']/span"))).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(500);




				//******Click on Submit*******\\
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
					report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
					waitForPageTitle("testing", 12);

				}



					//*******Feedback popup******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
					report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
					sleep(2000);

					//*****Fetching the Application Status*****\\

					String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
					sleep(500);
					report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);
					sleep(600);


					 if(occupationdecline.equalsIgnoreCase("Yes")){
						  String expectedUrl = "https://www.e2e.eapplication.metlife.com.au/eapply/hostab2bresponse#/changedecline";
	                      driver.get(expectedUrl);
						  try{
						    Assert.assertEquals(expectedUrl, driver.getCurrentUrl());
						    System.out.println("Navigated to correct webpage");
						    report.updateTestLog("Application Declined", "Decline due to Industry and Occupation selected",Status.PASS);
						  }
						  catch(Throwable pageNavigationError){
						    System.out.println("Didn't navigate to correct webpage");
						  }

					  }
					if(driver.getPageSource().contains("APPLICATION NUMBER")){


					//*****Fetching the Application Number******\\

					String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
					sleep(1000);
					report.updateTestLog("Application Number: ", "Application No: "+App,Status.PASS);

					System.out.println(App);


					//******Download the PDF***********\\
					properties = Settings.getInstance();
					String StrBrowseName=properties.getProperty("currentBrowser");
					if (StrBrowseName.equalsIgnoreCase("Chrome")){
						wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
						sleep(500);
			  		}

				  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
				  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
				  			sleep(2000);
				  			Robot roboobj=new Robot();
				  			roboobj.keyPress(KeyEvent.VK_ENTER);
				  		}

				  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
				  			Robot roboobj=new Robot();
				  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();

				  			sleep(4000);
				  			roboobj.keyPress(KeyEvent.VK_TAB);
				  			roboobj.keyPress(KeyEvent.VK_TAB);
				  			roboobj.keyPress(KeyEvent.VK_TAB);
				  			roboobj.keyPress(KeyEvent.VK_TAB);
				  			roboobj.keyPress(KeyEvent.VK_ENTER);

				  		}

					report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);

					sleep(1000);

					}

					}catch(Exception e){
						report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
					}

		}




	public void EApplyRateValidation() {

		String EmailId = dataTable.getData("HOSTPLUS_EApplyPremium", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_EApplyPremium", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_EApplyPremium", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_EApplyPremium", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_EApplyPremium", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_EApplyPremium", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_EApplyPremium", "RegularIncome");
		String Perform_WithoutRestriction = dataTable.getData("HOSTPLUS_EApplyPremium", "Perform_WithoutRestriction");
		String Work_WithoutLimitation = dataTable.getData("HOSTPLUS_EApplyPremium", "Work_WithoutLimitation");
		String Citizen = dataTable.getData("HOSTPLUS_EApplyPremium", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_EApplyPremium", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_EApplyPremium", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_EApplyPremium", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_EApplyPremium", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_EApplyPremium", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_EApplyPremium", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_EApplyPremium", "OutsideOfcPercent");
		String AnnualSalary = dataTable.getData("HOSTPLUS_EApplyPremium", "AnnualSalary");
		String WaitingPeriod=dataTable.getData("HOSTPLUS_EApplyPremium", "IPWaitingPeriod");
		String BenefitPeriod=dataTable.getData("HOSTPLUS_EApplyPremium", "IPBenefitPeriod");
		String DeathCost = dataTable.getData("HOSTPLUS_EApplyPremium", "DeathCost");
		String DeathCover = dataTable.getData("HOSTPLUS_EApplyPremium", "DeathCover");
		String TPDCover = dataTable.getData("HOSTPLUS_EApplyPremium", "TPDCover");
		String CoverRequired = dataTable.getData("HOSTPLUS_EApplyPremium", "CoverRequired");
		String ExpressType = dataTable.getData("HOSTPLUS_EApplyPremium", "ExpressType");
		String premiumtype = dataTable.getData("HOSTPLUS_EApplyPremium", "premiumtype");
		String Weeklyycost = dataTable.getData("HOSTPLUS_EApplyPremium", "Weeklyycost");
		String ExpectedDeathcover=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedYearlyDeathcover");
		String ExpectedTPDcover=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedYearlyTPDcover");
		String ExpectedIPcover=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedYearlyIPcover");
		String ExpectedDeathcoverMonthly=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedMonthlyDeathcover");
		String ExpectedTPDcoverMonthly=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedMonthlyTPDcover");
		String ExpectedIPcoverMonthly=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedMonthlyIPcover");
		String ExpectedDeathcoverWeekly=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedWeeklyDeathcover");
		String ExpectedTPDcoverWeekly=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedWeeklyTPDcover");
		String ExpectedIPcoverWeekly=dataTable.getData("HOSTPLUS_EApplyPremium", "ExpectedWeeklyIPcover");
		String ValidateIP = dataTable.getData("HOSTPLUS_EApplyPremium", "ValidateIP");
		String ValidateYN = dataTable.getData("HOSTPLUS_EApplyPremium", "ValidateYN");


		try{


			//****Click on Change Your Insurance Cover Link*****\\
			waitForPageTitle("testing", 10);
			WebDriverWait wait=new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Change your insurance cover']"))).click();

			report.updateTestLog("Change Your Insurance Cover", "Change Your Insurance Cover Link is Clicked", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				if(driver.findElement(By.xpath("//button[@ng-click='confirm()']")).isDisplayed()){
					sleep(500);
					quickSwitchWindows();
					sleep(1000);
					driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
					report.updateTestLog("Saved App Pop-up", "Cancelled the saved app", Status.PASS);
					sleep(2000);

				}
				}

			//*****Agree to Duty of disclosure*******\\
			waitForPageTitle("testing", 5);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='dodlabelCheck']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);



           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='privacylabelCheck']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);
			sleep(200);

			//*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\

			Select Contact=new Select(driver.findElement(By.name("contactType")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


		//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("preferrednumCCId"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

			//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[1]")).click();

				sleep(400);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath("(.//input[@name='contactPrefTime']/parent::*)[2]")).click();
				sleep(400);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}


			//***********Select the Gender******************\\

			if(Gender.equalsIgnoreCase("Male")){

				driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}

			//****Contact Details-Continue Button*****\\
		/*
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(formOne);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);	*/

			//***************************************\\
			//**********OCCUPATION SECTION***********\\
			//****************************************\\

				//*****Select the 15 Hours Question*******\\

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion ']/parent::*"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

				}
			else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='fifteenHrsQuestion']/parent::*"))).click();
					sleep(500);
					report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

				}

		//***Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*****\\

			if(RegularIncome.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
				sleep(500);
				report.updateTestLog("Regular Income Question", "Selected Yes", Status.PASS);
				sleep(300);

				//*****Perform duty without restriction due to illness or injury?*****\\

				if(Perform_WithoutRestriction.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected Yes", Status.PASS);
					sleep(300);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Perform duty without restriction due to illness or injury", "Selected No", Status.PASS);
					sleep(300);
				}
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
				sleep(500);
				report.updateTestLog("Regular Income Question", "Selected No", Status.PASS);
				sleep(300);

                   //*****Work Without Limitations *****\\

				if(Work_WithoutLimitation.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
					sleep(500);
					report.updateTestLog("Work Without Limitations", "Selected Yes", Status.PASS);
					sleep(300);
				}
				else{
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
					sleep(500);
					report.updateTestLog("Work Without Limitations", "Selected No", Status.PASS);
					sleep(300);
				}

			}

			//*****Resident of Australia****\\

			/*if(Citizen.equalsIgnoreCase("Yes")){
				driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[1]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

				}
			else{
				driver.findElement(By.xpath("(//input[@name='areyouperCitzQuestion']/parent::*)[2]")).click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

				}*/

			//*****Industry********\\
		    sleep(500);
		    Select Industry=new Select(driver.findElement(By.name("industry")));
		    Industry.selectByVisibleText(IndustryType);
		    sleep(1500);
			report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
		//	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()=' What is your current occupation?']"))).click();


			//*****Occupation*****\\

			Select Occupation=new Select(driver.findElement(By.name("occupation")));
			Occupation.selectByVisibleText(OccupationType);
		    sleep(800);
			report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
			  sleep(2500);


			//*****Other Occupation *****\\

			if(OccupationType.equalsIgnoreCase("Other")){

				wait.until(ExpectedConditions.elementToBeClickable(By.name("otherOccupation"))).sendKeys(OtherOccupation);
				report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
				sleep(1500);
			}


			//*****Extra Questions*****\\

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
				sleep(1000);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
			sleep(1000);
		}

		      //*****Do You Hold a Tertiary Qualification******\\

		        if(TertiaryQual.equalsIgnoreCase("Yes")){

		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
					sleep(1000);
		}
		   else{

			   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				sleep(1000);
		}
		}



		else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



		//******Do you work in hazardous environment*****\\

		if(HazardousEnv.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[1]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
			sleep(1000);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='hazardousQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
			sleep(1000);
		}

		//******Do you spend more than 10% of your working time outside of an office environment?*******\\

		if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[1]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
			sleep(1000);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='outsideOffice']/parent::*)[2]"))).click();
			report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
			sleep(1000);
		}
		 sleep(800);

		    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


		//***Select if your office Duties are Undertaken within an Office Environment?\\

		    	 if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);
						sleep(1000);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);
					sleep(1000);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
							sleep(1000);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
						sleep(1000);
				}
				}

	}

      sleep(800);


	//*****What is your annual Salary******


	    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
	    sleep(500);

	    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);

	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //label[text()='What is your annual salary?']"))).click();
	    sleep(5000);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,400)", "");
		sleep(500);

	//*****Click on Continue*******\\
/*
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='onFormContinue(occupationForm);']"))).click();
	report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
	*/



//**************************************************************************************************************\\
//****************************COVER CALCULATOR*****************************************************************\\
//*************************************************************************************************************\\

				//******Click on Cost of  Insurance as*******\\
		//******Click on Cost of  Insurance as*******\\




				//*******Death Cover Section******\\

	//restricted 3 digits
	/*sleep(500);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[2]"))).click();
	sleep(1500);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='coverType']/parent::*)[1]"))).click();
	sleep(1500);*/

			//	if(DeathYN.equalsIgnoreCase("Yes")){


	//*******Death Cover Section******\\


			    if(DeathCost.trim().equalsIgnoreCase("Fixed")){

							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar"))).sendKeys(DeathCover);
							sleep(800);
							report.updateTestLog("Death Cover Amount", DeathCover+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
							sleep(800);
							waitForPageTitle("testing", 3);
						}else{
							sleep(250);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
							sleep(500);
							wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar"))).sendKeys(DeathCover);
							sleep(800);
							report.updateTestLog("Death Cover Amount", DeathCover+" is entered", Status.PASS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[1]"))).click();
							sleep(800);
							waitForPageTitle("testing", 3);
						}

			    if(DeathCost.trim().equalsIgnoreCase("Fixed")){

					sleep(250);
					wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.id("dollar1"))).sendKeys(TPDCover);
					sleep(800);
					report.updateTestLog("TPD Cover Amount", TPDCover+" is entered", Status.PASS);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
					sleep(800);
					waitForPageTitle("testing", 3);
				}else{
					sleep(250);
					wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
					sleep(500);
					wait.until(ExpectedConditions.elementToBeClickable(By.id("nodollar1"))).sendKeys(TPDCover);
					sleep(800);
					report.updateTestLog("TPD Cover Amount", TPDCover+" is entered", Status.PASS);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h5[contains(text(),'Cover amount')])[2]"))).click();
					sleep(800);
					waitForPageTitle("testing", 3);
				}


			//	}




			                 //*******IP Cover section********\\
			sleep(500);
		//	if(IPYN.equalsIgnoreCase("Yes")){
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,400)", "");
				sleep(500);
		/*
				Select ipaction=new Select(driver.findElement(By.name("ipCoverName")));
				ipaction.selectByVisibleText(IPAction);
				sleep(500);
				report.updateTestLog("IP Cover action", IPAction+" is selected on IP cover", Status.PASS);*/

				//*****Insure 90% of My Salary******\\

				/*if(!IPAction.equalsIgnoreCase("No change")){
					if(Insure90Percent.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='ipsalarycheck']/parent::*"))).click();
					report.updateTestLog("Insure 90%", "Insure 90% of My Salary is Selected", Status.PASS);
					sleep(500);
					waitForPageTitle("testing", 1);
					}
				}*/
				sleep(500);
			//	if(!IPAction.equalsIgnoreCase("No change")){

					Select waitingperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.waitingPeriod']")));
					waitingperiod.selectByVisibleText(WaitingPeriod);
					sleep(300);
					report.updateTestLog("Waiting Period", "Waiting Period of: "+WaitingPeriod+" is Selected", Status.PASS);
					sleep(500);


					Select benefitperiod=new Select(driver.findElement(By.xpath("//select[@ng-model='QPD.addnlIpCoverDetails.benefitPeriod']")));
					benefitperiod.selectByVisibleText(BenefitPeriod);
					sleep(300);
					report.updateTestLog("Benefit Period", "Benefit Period: "+BenefitPeriod+" is Selected", Status.PASS);
					sleep(2000);
			//	}
				sleep(500);
			//	if(IPAction.equalsIgnoreCase("Increase your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){

				//	if(!Insure90Percent.equalsIgnoreCase("Yes"))
			//		{
						sleep(250);
						wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
						sleep(500);
						wait.until(ExpectedConditions.elementToBeClickable(By.name("IPRequireCover"))).sendKeys(CoverRequired);
						sleep(500);
						report.updateTestLog("IP Cover Amount", CoverRequired+" is entered", Status.PASS);
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[contains(text(),' Waiting period ')])[1]"))).click();
						sleep(800);
						waitForPageTitle("testing", 1);


			//*****Click on Calculate Quote********\\

	/*		sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
			report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
			sleep(4000);*/




			//******Click on Continue button******\\

		    //jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		//	sleep(300);


		//	 driver.findElement(By.xpath("(//button[contains(text(),'Continue')])[3]")).click();

			/*if(DeathYN.equalsIgnoreCase("Yes")||TPDYN.equalsIgnoreCase("Yes")||IPYN.equalsIgnoreCase("Yes")||DeathAction.equalsIgnoreCase("No Change")&&TPDAction.equalsIgnoreCase("No Change")&&IPAction.equalsIgnoreCase("No Change")){
				driver.findElement(By.xpath("//button[@ng-click='goToAura()']")).click();
				report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
				sleep(3000);

			}



			//*****Handling the pop-up*******\\

				 if(DeathAction.equalsIgnoreCase("Decrease your cover")||DeathAction.equalsIgnoreCase("Cancel your cover")||TPDAction.equalsIgnoreCase("Decrease your cover")||TPDAction.equalsIgnoreCase("Cancel your cover")||IPAction.equalsIgnoreCase("Decrease your cover")||IPAction.equalsIgnoreCase("Cancel your cover")){
//					 driver.findElement(By.xpath("//*[@ng-click='showDecreaseOrCancelPopUp(decCancelMsg)']")).click();
//						report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
//						sleep(1500);

//					quickSwitchWindows();
//					sleep(1000);

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='goTo()']"))).click();
					report.updateTestLog("Pop-Up", "Pop-up continue button is Clicked", Status.PASS);
					sleep(2000);
//					driver.findElement(By.xpath("//button[@ng-click='goToAura()']")).click();
//					report.updateTestLog("Continue", "Continue button is clicked after fetching the premiums", Status.PASS);
					sleep(1000);


				 }*/


//			//*****Click on Calculate Quote********\\
//
//			sleep(500);
//			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='formOneSubmit(coverCalculatorForm);']"))).click();
//			report.updateTestLog("Calculate Quote", "Calculate Quote button is Clicked", Status.PASS);
//			sleep(3000);
//

			//*** *****************************************************************************************\\
					//************Capturing the premiums displayed************************************************\\
					//********************************************************************************************\\

					//*******Capturing the Death Cost********\\
         /*   if(DeathYN.equalsIgnoreCase("yes")){
					WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
					Death.click();
					sleep(300);
					JavascriptExecutor je = (JavascriptExecutor) driver;
					je.executeScript("window.scrollBy(0,-350)", "");
					sleep(500);
					String DeathCost=Death.getText();

					if(DeathCost.equalsIgnoreCase(ExpectedDeathcover)){

						report.updateTestLog("Death Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedDeathcover+" Actual Premium: "+DeathCost, Status.PASS);
					}
					else{

						report.updateTestLog("Death Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedDeathcover+" Actual Premium: "+DeathCost, Status.FAIL);
					}

            }

					//*******Capturing the TPD Cost********\\

					sleep(500);
					if(TPDYN.equalsIgnoreCase("Yes")){
					WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
					tpd.click();
					sleep(250);
					String TPDCost=tpd.getText();
					if(TPDCost.equalsIgnoreCase(ExpectedTPDcover)){

						report.updateTestLog("TPD Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedTPDcover+" Actual Premium: "+TPDCost, Status.PASS);
					}
					else{

						report.updateTestLog("TPD Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedTPDcover+" Actual Premium: "+TPDCost, Status.FAIL);
					}
					}

					//*******Capturing the IP Cost********\\

					sleep(250);
					if(IPYN.equalsIgnoreCase("Yes")){
					WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
					ip.click();
					sleep(500);
					String IPCost=ip.getText();
					if(IPCost.equalsIgnoreCase(ExpectedIPcover)){

						report.updateTestLog("IP Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedIPcover+" Actual Premium: "+IPCost, Status.PASS);
					}
					else{

						report.updateTestLog("IP Cover Premium", CostType+" cost: "+"Expected Premium: "+ExpectedIPcover+" Actual Premium: "+IPCost, Status.FAIL);
					}
					}*/

			sleep(1500);


			 if(ExpressType.equalsIgnoreCase("Yearly")) {
					/* try {
						 pagescrollup(18);
						} catch (AWTException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/

					 sleep(500);
					    Select yearly=new Select(driver.findElement(By.name("premFreq")));
					    yearly.selectByVisibleText(ExpressType);
					    sleep(500);
						report.updateTestLog("Insurance Cost Frequency", "Insurance Cost Frequency Selected is: "+ExpressType, Status.PASS);

						if(ValidateYN.equalsIgnoreCase("Yes")){
								WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
								Death.click();
								sleep(300);

								String Deathpremium=Death.getText();

								if(Deathpremium.equalsIgnoreCase(ExpectedDeathcover)){

									report.updateTestLog("Death Cover Premium", ExpressType+" cost: "+"Expected Premium: "+ExpectedDeathcover+" Actual Premium: "+Deathpremium, Status.PASS);
								}
								else{

									report.updateTestLog("Death Cover Premium", ExpressType+" cost: "+"Expected Premium: "+ExpectedDeathcover+" Actual Premium: "+Deathpremium, Status.FAIL);
								}



								//*******Capturing the TPD Cost********\\

								sleep(500);

								WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
								tpd.click();
								sleep(250);
								String TPDpremium=tpd.getText();
								if(TPDpremium.equalsIgnoreCase(ExpectedTPDcover)){

									report.updateTestLog("TPD Cover Premium", ExpressType+" cost: "+"Expected Premium: "+ExpectedTPDcover+" Actual Premium: "+TPDpremium, Status.PASS);
								}
								else{

									report.updateTestLog("TPD Cover Premium", ExpressType+" cost: "+"Expected Premium: "+ExpectedTPDcover+" Actual Premium: "+TPDpremium, Status.FAIL);

								}
				 }


						        	if(ValidateIP.equalsIgnoreCase("Yes")){
								//*******Capturing the IP Cost********\\

								try {
									pagescrolldown(18);
								} catch (AWTException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								sleep(250);

								WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
								ip.click();
								sleep(500);
								String IPpremium=ip.getText();
								if(IPpremium.equalsIgnoreCase(ExpectedIPcover)){
									report.updateTestLog("IP Cover Premium", ExpressType+" cost: "+"Expected Premium: "+ExpectedIPcover+" Actual Premium: "+IPpremium, Status.PASS);
								}
								else{

									report.updateTestLog("IP Cover Premium", ExpressType+" cost: "+"Expected Premium: "+ExpectedIPcover+" Actual Premium: "+IPpremium, Status.FAIL);

								}

						        	}
				 }

				 if(premiumtype.equalsIgnoreCase("Monthly")) {
					 try {
						pagescrollup(28);
					} catch (AWTException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 sleep(500);
					    Select yearly=new Select(driver.findElement(By.name("premFreq")));
					    yearly.selectByVisibleText(premiumtype);
					    sleep(500);


					    if(ValidateYN.equalsIgnoreCase("Yes")){
								WebElement Death=driver.findElement(By.xpath(" (//*[@id='death']//h4)[2]"));
								sleep(1000);
								Death.click();
								sleep(500);
								String Deathpremium=Death.getText();
								if(Deathpremium.equalsIgnoreCase(ExpectedDeathcoverMonthly)){

									report.updateTestLog("Death Cover Premium", premiumtype+" cost: "+"Expected Premium: "+ExpectedDeathcoverMonthly+" Actual Premium: "+Deathpremium, Status.PASS);
									sleep(1000);
								}
								else{
									sleep(1000);
									report.updateTestLog("Death Cover Premium", premiumtype+" cost: "+"Expected Premium: "+ExpectedDeathcoverMonthly+" Actual Premium: "+Deathpremium, Status.FAIL);
							}

								//*******Capturing the TPD Cost********\\

								sleep(500);
								WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
								sleep(1000);
								tpd.click();
								sleep(250);
								String TPDpremium=tpd.getText();
								if(TPDpremium.equalsIgnoreCase(ExpectedTPDcoverMonthly)){
									sleep(1000);
									report.updateTestLog("TPD Cover Premium", premiumtype+" cost: "+"Expected Premium: "+ExpectedTPDcoverMonthly+" Actual Premium: "+TPDpremium, Status.PASS);
								}
								else{
									report.updateTestLog("TPD Cover Premium", premiumtype+" cost: "+"Expected Premium: "+ExpectedTPDcoverMonthly+" Actual Premium: "+TPDpremium, Status.FAIL);
									sleep(1000);
								}
				 }
								try {
									pagescrolldown(18);
								} catch (AWTException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								//*******Capturing the IP Cost********\\
								if(ValidateIP.equalsIgnoreCase("Yes")){
								sleep(250);

								WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
								sleep(1000);
								ip.click();
								sleep(500);
								String IPpremium=ip.getText();
								if(IPpremium.equalsIgnoreCase(ExpectedIPcoverMonthly)){
									sleep(1000);
									report.updateTestLog("IP Cover Premium", premiumtype+" cost: "+"Expected Premium: "+ExpectedIPcoverMonthly+" Actual Premium: "+IPpremium, Status.PASS);
								}
								else{
									sleep(1000);
									report.updateTestLog("IP Cover Premium", premiumtype+" cost: "+"Expected Premium: "+ExpectedIPcoverMonthly+" Actual Premium: "+IPpremium, Status.FAIL);
								}

								}

					}


				 if(Weeklyycost.equalsIgnoreCase("Weekly")) {
					 try {
						pagescrollup(28);
					} catch (AWTException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 sleep(500);
					    Select yearly=new Select(driver.findElement(By.name("premFreq")));
					    yearly.selectByVisibleText(Weeklyycost);
					    sleep(500);
					    if(ValidateYN.equalsIgnoreCase("Yes")){
								WebElement Death=driver.findElement(By.xpath("(//*[@id='death']//h4)[2]"));
								sleep(1000);
								Death.click();
								sleep(300);
								String Deathpremium=Death.getText();
								if(Deathpremium.equalsIgnoreCase(ExpectedDeathcoverWeekly)){
									sleep(1000);
									report.updateTestLog("Death Cover Premium", Weeklyycost+" cost: "+"Expected Premium: "+ExpectedDeathcoverWeekly+" Actual Premium: "+Deathpremium, Status.PASS);
								}
								else{
									sleep(1000);
									report.updateTestLog("Death Cover Premium", Weeklyycost+" cost: "+"Expected Premium: "+ExpectedDeathcoverWeekly+" Actual Premium: "+Deathpremium, Status.FAIL);
								}

								//*******Capturing the TPD Cost********\\

								sleep(500);
								WebElement tpd=driver.findElement(By.xpath("(//*[@id='tpd']//h4)[2]"));
								sleep(1000);
								tpd.click();
								sleep(250);
								String TPDpremium=tpd.getText();
								if(TPDpremium.equalsIgnoreCase(ExpectedTPDcoverWeekly)){
									sleep(1000);
									report.updateTestLog("TPD Cover Premium", Weeklyycost+" cost: "+"Expected Premium: "+ExpectedTPDcoverWeekly+" Actual Premium: "+TPDpremium, Status.PASS);
								}
								else{
									sleep(1000);
									report.updateTestLog("TPD Cover Premium", Weeklyycost+" cost: "+"Expected Premium: "+ExpectedTPDcoverWeekly+" Actual Premium: "+TPDpremium, Status.FAIL);
								}
				 }
								try {
									pagescrolldown(18);
								} catch (AWTException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								//*******Capturing the IP Cost********\\

								if(ValidateIP.equalsIgnoreCase("Yes")){
									sleep(250);

								WebElement ip=driver.findElement(By.xpath("(//*[@id='sc']//h4)[2]"));
								sleep(1000);
								ip.click();
								sleep(500);
								String IPpremium=ip.getText();
								if(IPpremium.equalsIgnoreCase(ExpectedIPcoverWeekly)){
									sleep(1000);
									report.updateTestLog("IP Cover Premium", Weeklyycost+" cost: "+"Expected Premium: "+ExpectedIPcoverWeekly+" Actual Premium: "+IPpremium, Status.PASS);
								}
								else{
									sleep(1000);
									report.updateTestLog("IP Cover Premium", Weeklyycost+" cost: "+"Expected Premium: "+ExpectedIPcoverWeekly+" Actual Premium: "+IPpremium, Status.FAIL);
								}
								}
								try {
									pagescrollup(55);
								} catch (AWTException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
					}

		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}
}



