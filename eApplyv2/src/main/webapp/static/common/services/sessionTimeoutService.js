angular.module('sessionTimeOut',['ngDialog', 'ngIdle']);
angular.module('sessionTimeOut').config(function(IdleProvider, KeepaliveProvider) {
    IdleProvider.idle(60*40);
    IdleProvider.timeout(30);
    //KeepaliveProvider.interval(10);
});

angular.module('sessionTimeOut').run(['Idle', '$rootScope', '$routeParams', '$location', 'ngDialog', function(Idle, $rootScope, $routeParams, $location, ngDialog) {
  Idle.watch();

  $rootScope.$on('IdleStart', function () {
    $rootScope.$apply();
    $rootScope.warning = ngDialog.open({
      template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="text-center" idle-countdown="countdown" ng-init="countdown=5">Your session will time out in {{countdown}} seconds</div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary avoid-arrow" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Exit</button></div></div>',
      plain: true,
      className: 'ngdialog-theme-plain custom-width'
    });
  });

  $rootScope.$on('IdleTimeout', function() {
    closeModals();
    $location.path("/sessionTimeOut");
    $rootScope.timedout = ngDialog.openConfirm({
        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="text-center">Your session is timed out</div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-session" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Ok</button></div></div>',
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
    }).then(function(){
      $location.path("/sessionTimeOut");
    }, function(e){
      if(e=='oncancel'){
        $location.path("/sessionTimeOut");
      }
    });
  });

  $rootScope.$on('IdleEnd', function() {
    closeModals();
    watchSession();
  });

  $rootScope.$on("$routeChangeSuccess", function(scope, current, pre){
    watchSession();
  });

  function closeModals() {
    if ($rootScope.warning) {
      ngDialog.close();
      $rootScope.warning = null;
    }
    if ($rootScope.timedout) {
      ngDialog.close();
      $rootScope.timedout = null;
    }
  }

  function watchSession() {
    if($location.path() == '/sessionTimeOut')
      Idle.unwatch();
    else
      Idle.watch();
  }
}]);
