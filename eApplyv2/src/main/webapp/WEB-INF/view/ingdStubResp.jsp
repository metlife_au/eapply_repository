<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.Base64"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style> html{display : none ; } </style>
<script> if( self == top ) { document.documentElement.style.display = 'block' ; } else { document.write(''); }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%
        String stubResponse=null;
         String sess_secureId=null;
         HttpServletResponse httpResponse = (HttpServletResponse) response;
        if(session.getAttribute("sess_web_serv_resp")!=null){
     	   stubResponse=session.getAttribute("sess_web_serv_resp").toString();
        }
         if(session.getAttribute("sess_secureId")!=null){
     	   sess_secureId=session.getAttribute("sess_secureId").toString();
        }
         httpResponse.setHeader("InputData", sess_secureId );
         httpResponse.setHeader("Authorization", new String(Base64.getEncoder().encode(("Bearer "+sess_secureId).getBytes())));

        // httpResponse.sendRedirect("http://localhost:8080/eapply/b2bresponse");
       	%>
       	<title>INGD</title>
<link rel="shortcut icon" href="static/ingd/images/favicon.ico">
<link rel="stylesheet" href="https://www.e2e.eapplication.metlife.com.au/eApplication/xAdvisorWeb/CMS/Group/HOST/css/eservicestyle.css" media="screen"  />

<script type="text/javascript">

	
	function formSubmitRwd(){
		var strResp = "<%= sess_secureId %>";
		var ipAddress="<%= request.getServerName()%>"	
		//alert('ipAddress>>'+ipAddress);		
		var respUrl=null;
		if(ipAddress=='localhost'){
			respUrl="http://localhost:8080/eapply/ingdab2bresponse";
		}else{
			respUrl="https://"+ipAddress+"/eapply/ingdab2bresponse";	
		}
		
		/* if(strResp!=null && strResp!=""){
			//respUrl=respUrl.replace("InputDataValue", strResp)
				document.getElementById('sess_secureId').value=strResp;
			document.instform.submit(); 
			 
			//window.open(respUrl, '_blank','directories=no, status=yes, menubar=yes, scrollbars=yes, resizable=yes, copyhistory=no, width=1000, height=1000');
		}else{
			alert("Invalid secureID");
		} */
		openPopupPage(respUrl,strResp);		
	}
	
	function openPopupPage(relativeUrl, strResp)
	{
	 var param = { 'sess_secureId' : strResp};
	 OpenWindowWithPost(relativeUrl, "width=1000, height=600, left=100, top=100, resizable=yes, scrollbars=yes", "NewFile", param);
	}	 
	 
	function OpenWindowWithPost(url, windowoption, name, params)
	{
	 var form = document.createElement("form");
	 form.setAttribute("method", "post");
	 form.setAttribute("action", url);
	 form.setAttribute("target", name);
	 for (var i in params)
	 {
	   if (params.hasOwnProperty(i))
	   {
	     var input = document.createElement('input');
	     input.type = 'hidden';
	     input.name = i;
	     input.value = params[i];
	     form.appendChild(input);
	   }
	 }
	 document.body.appendChild(form);
	 //note I am using a post.htm page since I did not want to make double request to the page
	 //it might have some Page_Load call which might screw things up.
	 window.open("", name, windowoption);
	 form.submit();
	 document.body.removeChild(form);
	}
		
function openLegalNoticenew(value)
{
//alert(value);
var sUrl = '/eApplication/faces/xAdvisorWeb/bundles/institutional/login/LegalNotice.jsp?fundID=NSFS';
window.open(sUrl,"Window1","menubar=no,width=1000,height=1200,toolbar=no,resizable=yes,scrollbars=yes");

//alert(value);
// write to window
//Window1.document.writeln(value); 
}
</script>
</head>

<body>
<div class="outerWrap"> 
  <!-- Top navigation container starts -->
  <div class="topNavCont">
    <div class="cornerTopLeft"></div>
    <div class="topNavContent">
      <ul class="topNav">
        <li class="cornerLeft"></li>
        <li> <a href="https://www.ing.com.au/about-us.html" target="_new" title="About us"> About us </a> </li>
        <li>|</li>
        <li> <a href="https://www.ing.com.au/contact-us.html" target="_new" title="Contact us"> Contact us </a> </li>
        <li class="cornerRight"></li>
      </ul>
    </div>
    <div class="cornerTopRight"></div>
    <!-- Top navigation container ends --> 
  </div>
  
  <!-- Main Container starts -->
  <div class="mainCont"> 
    <!-- Logo container starts -->
    <div class="logoCont">
      <div class="logoLeft"> <a href="#"> <img src="static/ingd/src/assets/images/logo.png" alt="" title="Hostplus" /></a> </div>
      <div class="logoRight none"> </div>
    </div>
    <!-- Logo container ends --> 
    
    <!-- Content container starts -->
    <div class="contentCont">

      <div class="leftCol">
        <div class="titleCont">
          <h1 class="title">WebService Response</h1>
        </div>
        <div class="blueLine"></div>
       <form name="instform" id="instform"  action="/eapply/ingdab2bresponse" method="post">

    
	    <input type="hidden" name="sess_secureId" id="sess_secureId" value=""/>
	   
          <!-- Forms started -->
         			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td valign="top" style="padding-left:5px; padding-right:5px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						style="height: 5px">
						<tr> 
							<td>
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tbody>
									
										<tr class="td_alt_white">
											<td width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:20px;"><label class="label">Response</label></td>
											<td><div id="errorMessage1" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:red;font-size:13px;"> </div></td>
										</tr>
										<tr>
											<td colspan="2" width="90" style="font-family:Arial,Verdana,Helvetica,sans-serif;color:#333333;font-size:13px;"><textarea class="medium" rows="8" cols="80" id="input_string_id" name="inputString"><%= stubResponse %> </textarea></td>
										</tr>
																		
									</tbody>
								</table>
								<br />

							</td>
						</tr>

						

					</table> 
					</td>
				</tr>
			</table>
          <div class="btnHolder fright">
          <a href="#" class="submit" onclick="formSubmitRwd();"><span>Rwd Application</span></a>             
          </div>
          <!-- Forms ends -->
        </form>

      </div>
     <div class="rightCol"> 
        <!-- Rounded edge box starts -->
        <div class="boxCont">
          <div class="popupCont">
          	<div class="popupContRight">
                <h2 class="subHead">Customer service</h2>
                <div class="greyWhite"></div>
                <span class="icoPhone"></span>
                <h2 class="tel">133 464</h2>
            </div>
          </div>
          <div class="bottomCont">
            <div></div>
          </div>
        </div>
        <!-- Rounded edge box ends --> 
      </div>
      <!-- Content container ends --> 
    </div>
    <!-- Main Container ends --> 
    <div class="footerCont">
	    <ul class="footer">
	      <li><a href="https://www.ing.com.au/privacy.html" target="_new" title="Privacy policy">Privacy policy</a></li>
	      <li>|</li>
	     <li><a href="https://www.ing.com.au/legal.html" class="" target="_new" >Legal Notice</a></li>
	    </ul>
	    <p class="footeTxt"> ING is a business name of ING Bank (Australia) Limited ABN 24 000 893 292 AFSL/ACL 229823.</p>
	  </div>
  </div>
  
  
  <!-- Bottom container starts -->
  <div class="footerCurve">
    <div class="cornerBottomLeft"></div>
    <div class="BottomMid">&nbsp;</div>
    <div class="cornerBottomRight"></div>
    <!-- Bottom container ends --> 
  </div>
</div>
</body>
</html>
