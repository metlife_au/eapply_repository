/**
 * 
 */
package au.com.metlife.eapply.b2b.service;

import au.com.metlife.eapply.b2b.model.AuthToken;
import au.com.metlife.eapply.b2b.model.Eappstring;

/**
 * @author akishore
 *
 */
public interface B2bService {
	
	Eappstring generateToken(String input, String outToken);
	Eappstring retriveClientData(String token);
	AuthToken retriveAuthData(String token);
	AuthToken saveAuthToken(String token);

}
