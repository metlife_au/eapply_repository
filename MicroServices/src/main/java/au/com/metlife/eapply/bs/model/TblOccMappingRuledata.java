package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the TBL_OCC_MAPPING_RULEDATA database table.
 * 
 */
@Entity
@Table(name = "TBL_OCC_MAPPING_RULEDATA", schema="EAPPDB")
public class TblOccMappingRuledata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private int id;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Column(name="FUND_CODE", nullable=false, length=250)
	private String fundCode;

	

	@Column(name="INDUSTRY_CODE", nullable=false, length=250)
	private String industryCode;

	@Column(name="MANUAL_FLAG", length=250)
	private String manualFlag;

	@Column(name="OCCUP_ALIAS_CODE", length=250)
	private String occupAliasCode;

	@Column(name="OCCUPATION_NAME", nullable=false, length=250)
	private String occupationName;

	@Column(name="PROFESSIONAL_FLAG", length=250)
	private String professionalFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public TblOccMappingRuledata() {
	}
	public TblOccMappingRuledata(String fundCode,String manualFlag, String occupAliasCode, String industryCode, Date createdate, Date updatedDate,String occupationName,String professionalFlag) {
		this.fundCode = fundCode;
		this.manualFlag = manualFlag;
		this.occupAliasCode = occupAliasCode;
		this.industryCode = industryCode;
		this.createDate = createdate;
		this.updatedDate=updatedDate;
		this.occupationName=occupationName;
		this.professionalFlag=professionalFlag;
		
	}
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getFundCode() {
		return this.fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIndustryCode() {
		return this.industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getManualFlag() {
		return this.manualFlag;
	}

	public void setManualFlag(String manualFlag) {
		this.manualFlag = manualFlag;
	}

	public String getOccupAliasCode() {
		return this.occupAliasCode;
	}

	public void setOccupAliasCode(String occupAliasCode) {
		this.occupAliasCode = occupAliasCode;
	}

	public String getOccupationName() {
		return this.occupationName;
	}

	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}

	public String getProfessionalFlag() {
		return this.professionalFlag;
	}

	public void setProfessionalFlag(String professionalFlag) {
		this.professionalFlag = professionalFlag;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
