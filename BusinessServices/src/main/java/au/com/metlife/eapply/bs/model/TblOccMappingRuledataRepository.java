package au.com.metlife.eapply.bs.model;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
public interface  TblOccMappingRuledataRepository extends CrudRepository<TblOccMappingRuledata, Long>{
	public List<TblOccMappingRuledata> findByFundCode(String fundCode);	
	public List<TblOccMappingRuledata> findByFundCodeAndIndustryCode(String fundCode, String indusCode);

}