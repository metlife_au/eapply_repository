package supportlibraries;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CraftUtility
{

	public static String getRelativePath()
	{
		URL location = CraftUtility.class.getProtectionDomain().getCodeSource().getLocation();
		String relativePath = location.getFile();
		if (relativePath.contains("allocator"))
		{
			relativePath = new File(location.getFile()).getParent();
		}
		if (relativePath.contains("bin"))
		{
			relativePath = new File(location.getFile()).getParent();
		}
		if (relativePath.contains("src"))
		{
			relativePath = new File(location.getFile()).getParent();
		}
		System.setProperty("CraftRelativePath", relativePath);
		return relativePath;
	}

	public static void setDriversToRelative(Properties properties)
	{
		String relativePath = getRelativePath();
		if(properties.getProperty("UseRealtivePathsForDrivers").equalsIgnoreCase("true")){
			properties.setProperty("ChromeDriverPath", relativePath + "/drivers/chromedriver.exe");
			properties.setProperty("InternetExplorerDriverPath", relativePath + "/drivers/IEDriverServer.exe");
			properties.setProperty("OperaDriverPath", relativePath + "/drivers/operadriver-1.5.jar");
		}

		System.setProperty("webdriver.chrome.driver", properties.getProperty("ChromeDriverPath"));
		System.setProperty("webdriver.ie.driver", properties.getProperty("InternetExplorerDriverPath"));
	}

	public static String capitalizeFirstLetter(String myString)
	{
		StringBuilder stringBuilder = new StringBuilder(myString);
		stringBuilder.setCharAt(0, Character.toUpperCase(stringBuilder.charAt(0)));
		return stringBuilder.toString();
	}

	public static String nonCapitalizeFirstLetter(String myString)
	{
		StringBuilder stringBuilder = new StringBuilder(myString);
		stringBuilder.setCharAt(0, Character.toLowerCase(stringBuilder.charAt(0)));
		return stringBuilder.toString();
	}

	public static String replaceTokens(String replacingString, String replacementName, String replacement)
	{
		Map<String, Object> valuesByKey = new HashMap<String, Object>();
		valuesByKey.put(replacementName, replacement);
		return replaceTokens(replacingString, valuesByKey);
	}

	public static String replaceTokens(String text, Map<String, Object> valuesByKey)
	{
		Pattern tokenPattern = Pattern.compile("\\{([^}]+)\\}");
		StringBuilder output = new StringBuilder();
		Matcher tokenMatcher = tokenPattern.matcher(text);

		int cursor = 0;
		while (tokenMatcher.find())
		{
			int tokenStart = tokenMatcher.start();
			int tokenEnd = tokenMatcher.end();
			int keyStart = tokenMatcher.start(1);
			int keyEnd = tokenMatcher.end(1);

			output.append(text.substring(cursor, tokenStart));

			String token = text.substring(tokenStart, tokenEnd);
			String key = text.substring(keyStart, keyEnd);

			if (valuesByKey.containsKey(key))
			{
				String value = (String) valuesByKey.get(key);
				output.append(value);
			}
			else
			{
				output.append(token);
			}

			cursor = tokenEnd;
		}
		output.append(text.substring(cursor));

		return output.toString();
	}

	public static String getDateTime(String inFormat)
	{
		SimpleDateFormat sdfTime = new SimpleDateFormat(inFormat);
		Date now = new Date();
		String strTime = sdfTime.format(now);
		return strTime;
	}
	public static String getDateTimeAheadYr(String inFormat)
	{
		SimpleDateFormat sdfTime = new SimpleDateFormat(inFormat);
		Calendar cal = Calendar.getInstance();		
		cal.add(Calendar.YEAR, 1);
		Date nextYear = cal.getTime();
		String strTime = sdfTime.format(nextYear);
		return strTime;
	}

	public static String formatDateTime(String inVal, String inFormat, String outFormat)
	{
		String strTime = null;
		try
		{
			SimpleDateFormat sdfTime = new SimpleDateFormat(inFormat);
			SimpleDateFormat outSdfTime = new SimpleDateFormat(outFormat);
			Date date = sdfTime.parse(inVal);
			strTime = outSdfTime.format(date);
		}
		catch (Exception e)
		{

		}
		return strTime;
	}

	public static boolean isDateFormatValid(String dateToValidate, String dateFromat)
	{

		if (dateToValidate == null)
		{
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try
		{
			sdf.parse(dateToValidate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean isInteger(String s)
	{
		try
		{
			Integer.parseInt(s);
		}
		catch (NumberFormatException e)
		{
			return false;
		}
		catch (NullPointerException e)
		{
			return false;
		}
		return true;
	}
}
