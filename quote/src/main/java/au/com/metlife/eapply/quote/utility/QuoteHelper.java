package au.com.metlife.eapply.quote.utility;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.drools.KnowledgeBase;

import au.com.metlife.eapply.quote.model.CoverDetailsRuleBO;
import au.com.metlife.eapply.quote.model.InstProductRuleBO;
import au.com.metlife.eapply.quote.model.QuoteResponse;
import au.com.metlife.eapply.quote.model.RuleModel;


public class QuoteHelper {
	
	
	public static BigDecimal calculateIPUntCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		BigDecimal ipCostBD=null;
//		BigDecimal ipUnts=null;
//		BigDecimal ipUnitsCvrAmount;
		try{			 			 			
//			ipUnts=new BigDecimal(ruleModel.getIpUnits());
//			System.out.println("ipUnts--->"+ipUnts);
//			ipUnitsCvrAmount=ipUnts.multiply(new BigDecimal(instProductRuleBO.getIpUnitCostMultipFactor())).setScale(0, BigDecimal.ROUND_CEILING);
////			ruleModel.setIpFixedAmount(ipUnitsCvrAmount);
//			System.out.println("ipUnitsCvrAmount--->"+ipUnitsCvrAmount);
			String ipCost=calculateIPFixedCost(ruleModel, instProductRuleBO, quoteResponse, ruleInfoMap, ruleFilePath);
			if(ipCost!=null){
				ipCostBD=new BigDecimal(ipCost);
				quoteResponse.setCoverAmount(ruleModel.getIpFixedAmount());
				quoteResponse.setCost(ipCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			System.out.println("ipCost--->"+ipCost);		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return ipCostBD;
	}
	
	
	public static String calculateIPFixedCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath) throws Exception {
		final String METHOD_NAME = "calculateIPFixedCost";
		String ruleNameAsPerFund;
		double ipYearlyCost=0;
		String ipCost =null;
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleModel.isSmoker()){
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
			}else{
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
			}

			if(ruleModel.getMemberType()!=null){
				coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
			}

			if(ruleModel.getIpBenefitPeriod()!=null){
				if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_2_YEARS)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_2);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_5_YEARS)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_5);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_60)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_60);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_65)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_65);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_AGE_67)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
				}else if(ruleModel.getIpBenefitPeriod().equalsIgnoreCase(QuoteConstants.BENEFIT_PERIOD_TO_AGE_67)){
					coverDetailsRuleBO.setBenefitPeriod(CoverDetailsRuleBO.BENEFIT_PERIOD_AGE_67);
				}
			}
			
			if(ruleModel.getIpWaitingPeriod()!=null ){
				if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_30_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_30);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_60_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_60);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_90_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_90);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_1_YEAR)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_1_YEAR);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_3_YEAR)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_3_YEARS);
				}else if(ruleModel.getIpWaitingPeriod().equalsIgnoreCase(QuoteConstants.WAITING_PERIOD_45_DAYS)){
					coverDetailsRuleBO.setWaitingPeriod(CoverDetailsRuleBO.WAITING_PERIOD_45);
				}
			}
			
			coverDetailsRuleBO.setCoverAmount(ruleModel.getIpFixedAmount().doubleValue());

			if(ruleModel.getFundCode()!=null){
				ruleNameAsPerFund=ruleModel.getFundCode()+QuoteConstants.RULE_NAME_INST_IP_FXD_COST;
				coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getIpOccCategory());
				
				if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
					coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
				}
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
				
				System.out.println("coverDetailsRuleBO.getYearlyCost()"+coverDetailsRuleBO.getYearlyCost());
				
				coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.IP_COVER, ruleModel.getIpOccCategory(), coverDetailsRuleBO,ruleModel.getFundCode(),ruleFilePath);	
					
				ipYearlyCost=coverDetailsRuleBO.getYearlyCost();
				
				ipCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, ipYearlyCost, ruleModel);
			}
				
				
			
			System.out.println("IP cost in coverDetailsHelper>>>"+ipCost);
		}catch(Exception e){
			throw e;
		}
	
		return ipCost;
	}
	
	public static String calculateTPDFixedCost(RuleModel ruleModel,HashMap ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse,InstProductRuleBO instProductRuleBO){
		BigDecimal tpdAddnlCvrBd=null;
		String ruleNameAsPerFund;
		double tpdYearlyCost;
		String tpdCost=null;
		try{
			System.out.println("inside calculateTPDFixedCost");
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			tpdAddnlCvrBd=createMultiplesByFactor(ruleModel.getTpdFixedAmount(), instProductRuleBO.getTpdMultipleFactor());
			ruleModel.setTpdFixedAmount(tpdAddnlCvrBd);

			boolean deathAndTpdCoverTypeSame=true;
			if(instProductRuleBO.isBreakDeathTpdRelation()){
				if(ruleModel.getDeathCoverType()!=null && ruleModel.getDeathCoverType().equalsIgnoreCase("DcFixed")){
					deathAndTpdCoverTypeSame=true;
				}else{
					deathAndTpdCoverTypeSame=false;
				}
			}
			//boolean isDeathAndTpdApplied=(coverQuoteDetailsVO.isBothDeathAndTpdApplied() && deathAndTpdCoverTypeSame);
	
			
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_FXD_COST;
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleModel.isSmoker()){
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
			}else{
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
			}
			
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			
			/*In case of AUSW/NSFS first we need to calculate based on white collar,
			 * then call other rule to convert the cost based on the respective 
			 * occupation category. calculateCoverCostByOccup(...) will do this*/
			if(QuoteConstants.PARTNER_AUSW.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_NSFS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_HOST.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())){
				coverDetailsRuleBO.setInsuranceCategoryStr(QuoteConstants.INS_CATEGORY_WHITECOLLAR);
			}else{
				coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			}
			
			coverDetailsRuleBO.setCoverAmount(ruleModel.getTpdFixedAmount().doubleValue());
			coverDetailsRuleBO.setDeathAndTpdApplied(true);
			if(ruleModel.getMemberType()!=null){
				coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
			}
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);
			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_TPD, ruleModel.getTpdOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			tpdYearlyCost=coverDetailsRuleBO.getYearlyCost();
			tpdCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, tpdYearlyCost, ruleModel);
			if(tpdCost!=null){
				quoteResponse.setCost(new BigDecimal(tpdCost).setScale(2, BigDecimal.ROUND_HALF_UP));
				quoteResponse.setCoverAmount(ruleModel.getTpdFixedAmount());
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return tpdCost;
	}
	
	
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @return
	 * @throws Exception
	 */
	public static String computeTpdUnitCost(RuleModel ruleModel, InstProductRuleBO instProductRuleBO) throws Exception {
		double tpdWeeklyCost=0.0;
		String tpdCost;
		/*SRKR: FOR FIRS super we need to calculate units cost based on occupation category*/
		if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_FIRS)){
			if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_LOW_RISK)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultFactProff())).doubleValue();
			}
		}else if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && 
				(instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_HOST)
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_SFPS)
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL)
						|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_ACCS))){
			if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_WHITECOLLAR)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getTpdOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultFactProff())).doubleValue();
			}
		}else{
			tpdWeeklyCost=new BigDecimal(ruleModel.getTpdUnits()).multiply(new BigDecimal(instProductRuleBO.getTpdUnitCostMultipFactor())).doubleValue();
		}
		tpdCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_WEEKLY, tpdWeeklyCost, ruleModel);
		return tpdCost;
	}
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @param quoteResponse
	 * @param ruleInfoMap
	 * @param ruleFilePath
	 */
	public static void calculateTpdUnitisedAmtAndCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		final String METHOD_NAME = "calculateTpdUnitisedAmtAndCost";
		String tpdCost=null;
		BigDecimal tpdCostBD=null;
		System.out.println("inside calculateTpdUnitisedAmtAndCost");
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			String ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT;
			coverDetailsRuleBO.setUnits(ruleModel.getTpdUnits());
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				System.out.println("TPD amount--- " +coverDetailsRuleBO.getCoverAmount());
				quoteResponse.setCoverAmount(new BigDecimal("" +coverDetailsRuleBO.getCoverAmount()).setScale(0, BigDecimal.ROUND_HALF_UP));
			}
			/*Calculate TPD cost*/			
			tpdCost = computeTpdUnitCost(ruleModel, instProductRuleBO);
			if(tpdCost != null){
				tpdCostBD=new BigDecimal(tpdCost);
				quoteResponse.setCost(tpdCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
				System.out.println("??tpdCost?"+tpdCost);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @param ruleInfoMap
	 * @param productType
	 * @param insuranceCategory
	 * @param coverRuleBO
	 * @param inputIdentifier
	 * @param ruleFileLocation
	 * @return
	 * @throws Exception
	 */
	private static CoverDetailsRuleBO calculateCoverCostByOccup(Map ruleInfoMap,int productType, String insuranceCategory,CoverDetailsRuleBO coverRuleBO,String inputIdentifier,String ruleFileLocation) throws Exception{
		final String METHOD_NAME = "calculateCoverCostByOccup";
		CoverDetailsRuleBO coverDetailsRuleBO = new CoverDetailsRuleBO();
		String ruleNameAsPerFund=null;
		try{
			if(ruleInfoMap!=null && insuranceCategory!=null && inputIdentifier!=null){
				coverDetailsRuleBO.setCoverType(productType);
				coverDetailsRuleBO.setInsuranceCategoryStr(insuranceCategory);
				coverDetailsRuleBO.setDeathCostperUnit(coverRuleBO.getDeathCostperUnit());
				coverDetailsRuleBO.setTpdCostperUnit(coverRuleBO.getTpdCostperUnit());
				coverDetailsRuleBO.setCalcWhiteCollarAmount(coverRuleBO.getYearlyCost());
				if(null == coverRuleBO.getLevelOfCommision()){
					coverRuleBO.setLevelOfCommision(new Double(0));
				}else{
					coverDetailsRuleBO.setLevelOfCommision(coverRuleBO.getLevelOfCommision());
				}
				coverDetailsRuleBO.setFundCode(inputIdentifier);
				coverDetailsRuleBO.setDeathAndTpdApplied(coverRuleBO.isDeathAndTpdApplied());
				ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_NORM_ALL_CVR_COST;
				coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
				if (ruleInfoMap != null && ruleInfoMap.get(ruleNameAsPerFund) != null) {
					coverDetailsRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(ruleNameAsPerFund));
				}
				coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);
			}
		}catch(Exception e){
			throw e;
		}
		return coverDetailsRuleBO;
	}	
	
	/**
	 * @param ruleModel
	 * @param ruleInfoMap
	 * @param ruleFileLocation
	 * @param quoteResponse
	 * @return
	 * @throws Exception
	 */
	public static String calculateDeathFixedCost(InstProductRuleBO instProductRuleBO,RuleModel ruleModel,HashMap ruleInfoMap,String ruleFileLocation,QuoteResponse quoteResponse) throws Exception {
		final String METHOD_NAME = "calculateDeathFixedCost";
		BigDecimal deathAddnlCvrBd=null;
		String ruleNameAsPerFund;
		double dcYearlyCost;
		String dcCost;
		CoverDetailsRuleBO coverDetailsRuleBO=null;
		try{
			coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			deathAddnlCvrBd=createMultiplesByFactor(ruleModel.getDeathFixedAmount(), instProductRuleBO.getDeathMultipleFactor());
			ruleModel.setDeathFixedAmount(deathAddnlCvrBd);
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_DEATH_FXD_COST;
			if(ruleModel.getGender()!=null){
				if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_MALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_MALE);
				}else if(ruleModel.getGender().equalsIgnoreCase(QuoteConstants.GENDER_FEMALE)){
					coverDetailsRuleBO.setGender(CoverDetailsRuleBO.GENDER_FEMALE);
				}
			}
			if(ruleModel.isSmoker()){
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.SMOKER);
			}else{
				coverDetailsRuleBO.setSmokerFlg(CoverDetailsRuleBO.NON_SMOKER);
			}
			if(ruleModel.getMemberType()!=null){
				coverDetailsRuleBO.setMemberType(ruleModel.getMemberType());
			}
			
//			if(coverQuoteDetailsVO.getLevelOfCommision()!=null && !coverQuoteDetailsVO.getLevelOfCommision().equalsIgnoreCase("")){
//				coverDetailsRuleBO.setLevelOfCommision(Double.parseDouble(coverQuoteDetailsVO.getLevelOfCommision()));
//			}
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			/*In case of AUSW/NSFS first we need to calculate based on white collar,
			 * then call other rule to convert the cost based on the respective 
			 * occupation category. calculateCoverCostByOccup(...) will do this*/
			if(QuoteConstants.PARTNER_AUSW.equalsIgnoreCase(coverDetailsRuleBO.getFundCode()) 
					|| QuoteConstants.PARTNER_NSFS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_AEIS.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())
					|| QuoteConstants.PARTNER_HOST.equalsIgnoreCase(coverDetailsRuleBO.getFundCode())){
				coverDetailsRuleBO.setInsuranceCategoryStr(QuoteConstants.INS_CATEGORY_WHITECOLLAR);
			}else{
				coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			}
			coverDetailsRuleBO.setCoverAmount(ruleModel.getDeathFixedAmount().doubleValue());
			
			/*SRKR In case of INGD we need pass the flag if both death and tpd are applied. If both death and tpd are applied
			 * then we have different set of rates*/
//			coverDetailsRuleBO.setDeathAndTpdApplied(bothDeathTPDApplied);
			
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFileLocation);	
			
			coverDetailsRuleBO=calculateCoverCostByOccup(ruleInfoMap, CoverDetailsRuleBO.DEATH_ONLY, ruleModel.getDeathOccCategory(), coverDetailsRuleBO, ruleModel.getFundCode(),ruleFileLocation);
			
			dcYearlyCost=coverDetailsRuleBO.getYearlyCost();
			System.out.println("coverDetailsRuleBO.getYearlyCost()?>>"+coverDetailsRuleBO.getYearlyCost());
			dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_YEARLY, dcYearlyCost, ruleModel);
			if(dcCost != null){
			quoteResponse.setCost(new BigDecimal(dcCost).setScale(2, BigDecimal.ROUND_HALF_UP));
			quoteResponse.setCoverAmount(ruleModel.getDeathFixedAmount());
			}
		}catch(Exception e){
			throw e;
		}
	
		return dcCost;
	}
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @param quoteResponse
	 * @param ruleInfoMap
	 * @param ruleFilePath
	 * @return
	 */
	public static RuleModel calculateDcUnitisedAmtAndCost(RuleModel ruleModel,InstProductRuleBO instProductRuleBO,QuoteResponse quoteResponse,Map ruleInfoMap,String ruleFilePath){
		final String METHOD_NAME = "calculateDcUnitisedAmtAndCost";
		String dcCost=null;
		BigDecimal deathCostBD=null;
		System.out.println("inside calculateDcUnitisedAmtAndCost");
		//String RULE_FILE_LOC="E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			//PrimeSuperFundRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue(QuoteConstants.RULE_FILE_LOC, QuoteConstants.RULE_FILE_LOC);
			coverDetailsRuleBO.setDeathUnits(ruleModel.getDeathUnits());
			coverDetailsRuleBO.setTpdUnits(ruleModel.getTpdUnits());

			if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT));
			}
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			System.out.println("InsuranceCategoryStr Death Calc------" +coverDetailsRuleBO.getInsuranceCategoryStr());
			
			coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				System.out.println("Death Amount----" +coverDetailsRuleBO.getCoverAmount());
				//ruleModel.setDeathFixedAmount(new BigDecimal("" +coverDetailsRuleBO.getCoverAmount()).setScale(2, BigDecimal.ROUND_HALF_UP));
				quoteResponse.setCoverAmount(new BigDecimal("" +coverDetailsRuleBO.getCoverAmount()).setScale(0, BigDecimal.ROUND_HALF_UP));
			}

			
			dcCost = computeDcUnitsCost(ruleModel, instProductRuleBO);
			if(dcCost != null){
				deathCostBD=new BigDecimal(dcCost);
				//ruleModel.setDeathUnitsCost(deathCostBD);
				quoteResponse.setCost(deathCostBD.setScale(2, BigDecimal.ROUND_HALF_UP));
				System.out.println("??dcCost?"+dcCost);
			}
			System.out.println("deathCostBD"+deathCostBD);
		}catch(Exception e){
			e.printStackTrace();
		}
		return ruleModel;
	}
	
	/**
	 * @param ruleModel
	 * @param instProductRuleBO
	 * @return
	 * @throws Exception
	 */
	private static String computeDcUnitsCost(RuleModel ruleModel, InstProductRuleBO instProductRuleBO) throws Exception {
		double dcWeeklyCost=0.0;
		String dcCost;
		BigDecimal dcAddnlUnitsBD=null,tpdAddnlUnitsBD=null,dcOnlyUnitsBD=null;
		/*SRKR: FOR FIRS super we need to calculate units cost based on occupation category*/
		if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_FIRS)){
			if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_LOW_RISK)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultFactProff())).doubleValue();
			}
		}else if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null 
				&& (instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_HOST) 
						||instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_SFPS)
						||instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL)
						||instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_ACCS))){
			if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_STANDARD)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultFacStandard())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_WHITECOLLAR)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultFacWhiteCollar())).doubleValue();
			}else if(ruleModel.getDeathOccCategory().equalsIgnoreCase(QuoteConstants.INS_CATEGORY_PROFESSIONAL)){
				dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultFactProff())).doubleValue();
			}
		}else if(instProductRuleBO!=null && instProductRuleBO.getProductCode()!=null && (
/*				instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_GUIL)
				|| instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_ACCS)|| */
				instProductRuleBO.getProductCode().equalsIgnoreCase(QuoteConstants.PARTNER_REIS))){/*SRKR: For Guild and child care calculating unitised cost is different*/
			/*SRKR Guild and child care Units cost calculation.
			 * Assume 25 units Dth, 20 TPD
			 * Death:
			 * $0.50 x 20 units Death = $10 
			 * $1 x (25 Dth - 20 TPD) = $5
			 * Death:  $10 + $5 = $15 per week
			 * TPD:  $0.50 x 20 TPD = $10 per week*/
			
			/*SRKR:WR8820: 13/08/2014 REIS Units cost calculation.
			 * Assume 25 units Dth, 20 TPD
			 * Death:
			 * $0.55 x 20 units Death = $11 
			 * $0.75 x (25 Dth - 20 TPD) = $3.75
			 * Death:  $11 + $3.75 = $14.75 per week
			 * TPD:  $0.50 x 20 TPD = $10 per week*/			
			dcAddnlUnitsBD=new BigDecimal(ruleModel.getDeathUnits());
			tpdAddnlUnitsBD=new BigDecimal(ruleModel.getTpdUnits());
	
			if(dcAddnlUnitsBD!=null && tpdAddnlUnitsBD!=null){
				dcOnlyUnitsBD=dcAddnlUnitsBD.subtract(tpdAddnlUnitsBD);
				/*e.g death 10 tpd 4 units
				 * then (10-4)*1 + 4 * 0.5
				 * dcWeeklcost=*/
				dcWeeklyCost=(dcOnlyUnitsBD.multiply(new BigDecimal(instProductRuleBO.getDeathOnlyMultipFactor()))).add(tpdAddnlUnitsBD.multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultipFactor()))).doubleValue();
			}else if(dcAddnlUnitsBD!=null){
				dcOnlyUnitsBD=dcAddnlUnitsBD;
				dcWeeklyCost=(dcOnlyUnitsBD.multiply(new BigDecimal(instProductRuleBO.getDeathOnlyMultipFactor()))).doubleValue();
			}
			
		}else{
			dcWeeklyCost=new BigDecimal(ruleModel.getDeathUnits()).multiply(new BigDecimal(instProductRuleBO.getDeathUnitCostMultipFactor())).doubleValue();
		}
		dcCost=calculateAddnlCostByFreq(QuoteConstants.PREM_FREQ_WEEKLY, dcWeeklyCost, ruleModel);
		return dcCost;
	}

	/**
	 * @param presentCostType
	 * @param presentCoverCost
	 * @param ruleModel
	 * @return
	 * @throws Exception
	 */
	public static String calculateAddnlCostByFreq(String presentCostType,double presentCoverCost,RuleModel ruleModel) throws Exception{
		final String METHOD_NAME = "calculateAddnlCostByFreq";
		double addnlCost=0;
		try{
			if(ruleModel!=null && ruleModel.getPremiumFrequency()!=null && presentCostType!=null){
				if(presentCostType.equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
					if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
						addnlCost=(presentCoverCost*52)/12;
					} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
						addnlCost=presentCoverCost;
					}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
						addnlCost=presentCoverCost*52;
					}
				}else if(presentCostType.equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
					if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
						addnlCost=presentCoverCost;
					} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
						addnlCost=(presentCoverCost*12)/52;
					}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
						addnlCost=presentCoverCost*12;
					}
				}else if(presentCostType.equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
					if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_MONTHLY)){
						addnlCost=presentCoverCost/12;
					} else 	if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_WEEKLY)){
						addnlCost=presentCoverCost/52;
					}else  if(ruleModel.getPremiumFrequency().equalsIgnoreCase(QuoteConstants.PREM_FREQ_YEARLY)){
						addnlCost=presentCoverCost;
					}
				}
			}
		}catch(Exception e){
			throw e;
		}

		return ""+addnlCost;
	}
	
	/**
	 * @param lodgeFundId
	 * @param memberType
	 * @param managetype
	 * @param ruleInfoMap
	 * @param ruleFilePath
	 * @return
	 * @throws Exception
	 */
	public static InstProductRuleBO invokeInstProductBusinessRule(String lodgeFundId,String memberType,String managetype,Map ruleInfoMap,String ruleFilePath) throws Exception{
		final String METHOD_NAME = "invokeInstProductBusinessRule";
		InstProductRuleBO instProductRuleBO = new InstProductRuleBO();
		try{
			if(lodgeFundId!=null){
				//String RULE_FILE_LOC="E:\\workspace\\RockingeApply\\_Metlife_Drools\\rules\\eapply\\CARE\\";
				instProductRuleBO.setProductCode(lodgeFundId);
				instProductRuleBO.setMemberType(memberType);
				instProductRuleBO.setManageType(managetype);
				if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID)!=null){
					instProductRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID));
				}
				instProductRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_PRODUCT_RULE_SET_BY_PRODID);
				instProductRuleBO = DroolsHelper.invokeInstProductBusinessRule(instProductRuleBO,ruleFilePath);
			}
		}catch(Exception e){
			throw e;
		}
		return instProductRuleBO;
	}
	

	
	/**
	 * @param obj
	 * @return
	 */
	public static boolean isNull(Object obj){
		if(obj == null){
			return false;
		}else{
			if(obj.toString().equalsIgnoreCase("") || obj.toString().equalsIgnoreCase("null")){
				return false;
			}else{
				return true;
			}
		}
	}
	public static BigDecimal createMultiplesByFactor(BigDecimal cvrAmountBD,BigDecimal factor){
		final String METHOD_NAME = "createMultiplesByFactor";
		try{
			BigDecimal quotient=null;
			if(cvrAmountBD!=null && null!=factor){
				quotient=cvrAmountBD.divide(factor);
				if(quotient.scale()>0){
					quotient=quotient.setScale(0, BigDecimal.ROUND_UP);
					cvrAmountBD=quotient.multiply(factor);
					cvrAmountBD=cvrAmountBD.setScale(0, BigDecimal.ROUND_HALF_UP);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return cvrAmountBD;
	}
	
	public static BigDecimal convertDeathFixedToUnits(RuleModel ruleModel,Map ruleInfoMap,String ruleFilePath){
		final String METHOD_NAME = "convertDeathFixedToUnits";
		BigDecimal convertedUnits=null;
		BigDecimal oneUnitBD=null;
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_ONLY);
			//PrimeSuperFundRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue(QuoteConstants.RULE_FILE_LOC, QuoteConstants.RULE_FILE_LOC);
			coverDetailsRuleBO.setDeathUnits(1);
			coverDetailsRuleBO.setTpdUnits(0);

			if(ruleInfoMap!=null && ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT));
			}
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getDeathOccCategory());
			System.out.println("InsuranceCategoryStr Death Calc------" +coverDetailsRuleBO.getInsuranceCategoryStr());
			
			coverDetailsRuleBO.setRuleFileName(QuoteConstants.RULE_NAME_INST_DEATH_UNT_CVR_AMT);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				System.out.println("Death Amount----" +coverDetailsRuleBO.getCoverAmount());
				oneUnitBD=new BigDecimal("" +coverDetailsRuleBO.getCoverAmount()).setScale(0, BigDecimal.ROUND_HALF_UP);
				if(ruleModel.getDeathExistingAmount()!=null && oneUnitBD!=null){
					convertedUnits=ruleModel.getDeathExistingAmount().divide(oneUnitBD,0,BigDecimal.ROUND_UP);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedUnits;
	}
	
	public static BigDecimal convertTpdFixedToUnits(RuleModel ruleModel,Map ruleInfoMap,String ruleFilePath){
		final String METHOD_NAME = "convertTpdFixedToUnits";
		BigDecimal convertedUnits=null;
		BigDecimal oneUnitBD=null;
		try{
			CoverDetailsRuleBO coverDetailsRuleBO=new CoverDetailsRuleBO();
			coverDetailsRuleBO.setFundCode(ruleModel.getFundCode());
			coverDetailsRuleBO.setAge(ruleModel.getAge());
			coverDetailsRuleBO.setCoverType(CoverDetailsRuleBO.DEATH_TPD);
			String ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_TPD_UNT_CVR_AMT;
			coverDetailsRuleBO.setUnits(1);
			if(ruleInfoMap!=null && ruleInfoMap.get(ruleNameAsPerFund)!=null){
				coverDetailsRuleBO.setKbase((KnowledgeBase)ruleInfoMap.get(ruleNameAsPerFund));
			}
			coverDetailsRuleBO.setInsuranceCategoryStr(ruleModel.getTpdOccCategory());
			coverDetailsRuleBO.setRuleFileName(ruleNameAsPerFund);
			coverDetailsRuleBO = DroolsHelper.invokeUC16BusinessRules(coverDetailsRuleBO,ruleFilePath);
			if(coverDetailsRuleBO!=null){
				System.out.println("TPD Amount----" +coverDetailsRuleBO.getCoverAmount());
				oneUnitBD=new BigDecimal("" +coverDetailsRuleBO.getCoverAmount()).setScale(0, BigDecimal.ROUND_HALF_UP);
				if(ruleModel.getTpdExistingAmount()!=null && oneUnitBD!=null){
					convertedUnits=ruleModel.getTpdExistingAmount().divide(oneUnitBD,0,BigDecimal.ROUND_UP);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedUnits;
	}
}
