class AppHeaderCtrl {
  constructor(AppConstants, ConfigData) {
    'ngInject';    
    this.AppConstants = AppConstants;
    this.config = ConfigData.getPartnerBasedConfiguration(inputData.partnerID);
  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  controllerAs: '$header',
  templateUrl: 'layout/header.html'
};

export default AppHeader;
