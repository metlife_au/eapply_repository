package au.com.metlife.eapply.ct.serviceimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.ct.model.ClaimOutput;
import au.com.metlife.eapply.ct.service.CmsService;


@Service("quoteService")
/* @Transactional
 @Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode =
 ScopedProxyMode.TARGET_CLASS)
 @Service
 @Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON,
 proxyMode=ScopedProxyMode.INTERFACES)*/
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "request")

public class CmsServiceImpl implements CmsService {
	/*private static final Logger log = LoggerFactory.getLogger(CmsServiceImpl.class);
	 @Autowired
	 JdbcTemplate jdbcTemplate; 
	@Autowired(required = true)
	CmsBase cmsBase;

	
	public HashMap<String, Object> refreshCache(){
		return cmsBase.getCmsData();
	}
	
	public List<ClaimOutput> getClaimEvents(String claimNo){
		log.info("Get claim events start");
		ClaimOutput claimOutput =null;
		make a connection to GL/SCI
		Connection conn = null;
		Statement stmt = null;
		List<ClaimOutput> claimOutputList = new ArrayList<ClaimOutput>();
		try {
			 STEP 2: Register JDBC driver
			      Class.forName("com.ibm.db2.jcc.DB2Driver");

			     STEP 3: Open a connection
			      log.info("Connecting to database62...");
			      conn = DriverManager.getConnection("jdbc:db2://10.173.60.142:60000/MADWODS","db2inst1","metlife123");

			      STEP 4: Execute a query
			      log.info("Creating statement...");
			      stmt = conn.createStatement();
			      String sql;			   
			      sql = "SELECT ACT.START_DATE, ACT.DESCRIPTION, CASE WHEN EACT.ACTIVITY_TYPE_ID in (5, 23, 24) THEN ACT.COMMENTS ELSE ACT.ADDITIONAL_INFORMATION END AS ADDITIONAL_INFORMATION, ACT.MAIL_COMMENT FROM DBUSDWW.ACTIVITY_CLM_FCT ACT JOIN  DBUSDWW.EVIEW_ACTIVITY EACT ON ACT.DESCRIPTION = EACT.ACTIVITY_DESCRIPTION WHERE ACT.SRC_TABLE_NAME = 'FILENOTE' AND"
			      		+ " ACT.CLAIM  = "+claimNo+" ORDER BY ACT.START_DATE DESC";
			      		
			      ResultSet rs = stmt.executeQuery(sql);

			      STEP 5: Extract data from result set
			      while(rs.next()){		
			    	  claimOutput = new ClaimOutput();
			    	  claimOutput.setDescription( rs.getString("DESCRIPTION"));
			    	  claimOutput.setAdditionalInformation( rs.getString("ADDITIONAL_INFORMATION"));
			    	  claimOutput.setMailComment( rs.getString("MAIL_COMMENT"));
			    	  claimOutput.setStartdate( rs.getDate("START_DATE"));  	  
			    	  claimOutputList.add(claimOutput);
			      		        
			      }
			      STEP 6: Clean-up environment
			      log.info("size>> {}",claimOutputList.size());
			      rs.close();
			      stmt.close();
			      conn.close();
		} catch(SQLException se){
		      Handle errors for JDBC
		     log.error("Error in sql: {} ",se.getMessage());
		   }catch(Exception e){
		      Handle errors for Class.forName
			   log.error("Error in get claim: {} ",e.getMessage());
		   }finally{
		      finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      } nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		    	  log.error("Error in sql :: {} ",se.getMessage());
		      }end finally try
		   }
		log.info("Get claim events finish");
		   return claimOutputList;
		
	}

	
	public List<ClaimOutput> getClaimRequirements(String claimNo){
		log.info("Get claim requirements start");
		ClaimOutput claimOutput =null;
		make a connection to GL/SCI
		Connection conn = null;
		Statement stmt = null;
		List<ClaimOutput> claimOutputList = new ArrayList<ClaimOutput>();
		try {
			 STEP 2: Register JDBC driver
			      Class.forName("com.ibm.db2.jcc.DB2Driver");

			      STEP 3: Open a connection
			      log.info("Connecting to database62...");
			      conn = DriverManager.getConnection("jdbc:db2://10.173.60.142:60000/MADWODS","db2inst1","metlife123");

			      STEP 4: Execute a query
			      log.info("Creating statement...");
			      stmt = conn.createStatement();
			      String sql;			   
			      sql = "SELECT DISTINCT FCT.FUNDNAME, FCT.CLAIM_ID, INT(FCT.CLAIMNO) AS CLAIMNO, FCT.PRODUCTTYPE, FCT.FIRSTNAME, FCT.SURNAME, FCT.DATEOFBIRTH, FCT.REFERENCE, FCT.DATESUBMITTED, ST.STATUSDESCRIPTION, ACT.ADDITIONAL_INFORMATION, ACT.MAIL_COMMENT, ACT.START_DATE FROM DBUSDWW.FCT_CLM_DTL FCT JOIN DBUSDWW.TBLCLAIMSTATUS ST ON FCT.STATUS_CODE = ST.CLAIMSTATUS AND FCT.PRODUCTTYPE = ST.PRODUCTTYPE JOIN (SELECT REQ.CLAIM, REQ.START_DATE, REQ.ADDITIONAL_INFORMATION, REQ.MAIL_COMMENT FROM DBUSDWW.ACTIVITY_CLM_FCT REQ LEFT  JOIN DBUSDWW.ACTIVITY_CLM_FCT RCD ON REQ.CLAIM=RCD.CLAIM AND REQ.ADDITIONAL_INFORMATION=RCD.ADDITIONAL_INFORMATION AND RCD.DESCRIPTION='Information Received' AND     RCD.START_DATE>=REQ.START_DATE WHERE REQ.DESCRIPTION='Information Requested' AND RCD.START_DATE IS NULL) ACT ON FCT.CLAIM_ID=ACT.CLAIM WHERE ST.CLAIMSTATUS NOT IN('C','D','Z') AND "
			      		+ "INT(CLAIMNO) = "+claimNo+" order by INT(CLAIMNO) asc,  surname, firstname,producttype";
			      ResultSet rs = stmt.executeQuery(sql);

			      STEP 5: Extract data from result set
			      while(rs.next()){
			    	  claimOutput = new ClaimOutput();
			    	  claimOutput.setFundname( rs.getString("FUNDNAME"));				    	 			    	 
			    	  claimOutput.setClaimId(rs.getString("CLAIM_ID"));			    	 
			    	  claimOutput.setClaimNo( rs.getString("CLAIMNO"));			    	
			    	  claimOutput.setProductType(rs.getString("PRODUCTTYPE"));			    	  
			    	  claimOutput.setFirstName(rs.getString("FIRSTNAME"));			    	
			    	  claimOutput.setSurName( rs.getString("SURNAME"));			    	 
			    	  claimOutput.setDob(rs.getDate("DATEOFBIRTH"));   			    	
			    	  claimOutput.setReference( rs.getString("REFERENCE"));				    			    	
			    	  claimOutput.setDatesubmitted(rs.getDate("DATESUBMITTED"));			    	  
			    	  claimOutput.setStatusDescription(rs.getString("STATUSDESCRIPTION"));	
			    	  claimOutput.setAdditionalInformation( rs.getString("ADDITIONAL_INFORMATION"));
			    	  claimOutput.setMailComment( rs.getString("MAIL_COMMENT"));
			    	  claimOutput.setStartdate( rs.getDate("START_DATE"));  	  
			    	  claimOutputList.add(claimOutput);
			      		        
			      }
			      STEP 6: Clean-up environment
			     log.info("size>> {}",claimOutputList.size());
			      rs.close();
			      stmt.close();
			      conn.close();
		} catch(SQLException se){
		      Handle errors for JDBC
			 log.error("Error in sql: {} ",se.getMessage());
		   }catch(Exception e){
		      Handle errors for Class.forName
		      log.error("error in get claim requirement : {}",e.getMessage());
		   }finally{
		     finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		    	  log.error("Error in sql: {} ",se.getMessage());
		      }end finally try
		   }
		log.info("Get claim requirements finish");
		   return claimOutputList;
	}
	
	public  List<ClaimOutput> getClaimStatus(String claimNo,String surName,String dob){
		log.info("Get claim status start");
			ClaimOutput claimOutput =null;
			make a connection to GL/SCI
			Connection conn = null;
		   Statement stmt = null;
		   List<ClaimOutput> claimOutputList = new ArrayList<ClaimOutput>();
		   try {
			   String sql1="SELECT DISTINCT FCT.FUNDNAME,FCT.FUNDOWNERID,FCT.POLICYNO,FCT.CLAIM_ID,INT(FCT.CLAIMNO) AS CLAIMNO,RTRIM(FCT.PRODUCTTYPE) PRODUCTTYPE, UCASE(FCT.FIRSTNAME) AS FIRSTNAME,UCASE(FCT.SURNAME) AS SURNAME,FCT.DATEOFBIRTH,FCT.GENDER,FCT.REFERENCE,FCT.SCHEME,FCT.WAITPERIOD, FCT.BENEFITPERIOD,FCT.STATUS_CODE, ST.STATUSDESCRIPTION,FCT.DATESUBMITTED,FCT.DISABILITYDATE,FCT.CLAIMTYPE_DES,FCT.SUMINSURED, (FCT.TOTALBENEFITPAID_GROSS-COALESCE(TOTALBENEFITPAID_DEDUCTIONS,0)) AS BENEFITPAID,FCT.DATELASTPAID, FCT.CLAIMCAUSE_DES, '' as ADMITTED_DATE,FCT.SALARYINSURED,FCT.SUPERCONTRIBUTION,FCT.MONTHLYBENEFIT,FCT.CLAIMASSESSOR, FCT.LASTUPDDATE FROM DBUSDWW.FCT_CLM_DTL FCT JOIN DBUSDWW.TBLCLAIMSTATUS ST ON  FCT.STATUS_CODE = ST.CLAIMSTATUS AND FCT.PRODUCTTYPE = ST.PRODUCTTYPE where FCT.CLAIMNO = ? AND FCT.SURNAME = ? AND DATEOFBIRTH=?";
			  return jdbcTemplate.query(sql1,new Object[] { claimNo,surName,dob }, new ResultSetExtractor<List<ClaimOutput>>(){

				@Override
				public List<ClaimOutput> extractData(ResultSet rs) throws SQLException, DataAccessException {
					
					 while(rs.next()){
						 ClaimOutput claimOutput = new ClaimOutput();
				    	  claimOutput.setFundname( rs.getString("FUNDNAME"));	
				    	  claimOutput.setFundownerId(rs.getString("FUNDOWNERID"));			    	  
				    	  claimOutput.setPolicyNo(rs.getString("POLICYNO"));			    	 
				    	  claimOutput.setClaimId(rs.getString("CLAIM_ID"));			    	 
				    	  claimOutput.setClaimNo( rs.getString("CLAIMNO"));			    	
				    	  claimOutput.setProductType(rs.getString("PRODUCTTYPE"));			    	  
				    	  claimOutput.setFirstName(rs.getString("FIRSTNAME"));			    	
				    	  claimOutput.setSurName( rs.getString("SURNAME"));			    	 
				    	  claimOutput.setDob(rs.getDate("DATEOFBIRTH"));			    	 
				    	  claimOutput.setGender( rs.getString("GENDER"));			    	
				    	  claimOutput.setReference( rs.getString("REFERENCE"));			    	  
				    	  claimOutput.setScheme(rs.getString("SCHEME"));			    	  
				    	  claimOutput.setWaitperiod(rs.getString("WAITPERIOD"));			    	 
				    	  claimOutput.setBenefitperiod(rs.getString("BENEFITPERIOD"));			    
				    	  claimOutput.setStatusCode( rs.getString("STATUS_CODE"));			    	 
				    	  claimOutput.setStatusDescription(rs.getString("STATUSDESCRIPTION"));			    	
				    	  claimOutput.setDatesubmitted(rs.getDate("DATESUBMITTED"));			    	
				    	  claimOutput.setDisabilitydate(rs.getDate("DISABILITYDATE"));			    	
				    	  claimOutput.setClaimTypeDescription(rs.getString("CLAIMTYPE_DES"));			    	 
				    	  claimOutput.setSuminsured( rs.getString("SUMINSURED"));			    	
				    	  claimOutput.setTotalBenPaid( rs.getString("BENEFITPAID"));			    	
				    	  claimOutput.setDateLastPaid( rs.getDate("DATELASTPAID"));			    	 
				    	  claimOutput.setClaimCauseDescription(rs.getString("CLAIMCAUSE_DES"));			    	  
				    	  claimOutput.setAdmittedDate(rs.getString("ADMITTED_DATE"));			    	 
				    	  claimOutput.setSalaryInsured( rs.getDouble("SALARYINSURED"));			    	  
				    	  claimOutput.setSuperContribution(rs.getDouble("SUPERCONTRIBUTION"));			    	  
				    	  claimOutput.setMonthlyBenefit(rs.getDouble("MONTHLYBENEFIT"));
				    	  claimOutput.setClaimAccessor(rs.getString("CLAIMASSESSOR"));			    	 			    	
				    	  claimOutput.setLastupdateDate(rs.getDate("LASTUPDDATE"));
				    	  claimOutputList.add(claimOutput);
				      		        
				      }
					 log.info("Get claim status finish");
					return claimOutputList;
				}  
				
			   });
			 STEP 2: Register JDBC driver
			      Class.forName("com.ibm.db2.jcc.DB2Driver");

			      //STEP 3: Open a connection
			      log.info("Connecting to database62...");
			      conn = DriverManager.getConnection("jdbc:db2://10.173.60.142:60000/MADWODS","db2inst1","metlife123");

			      //STEP 4: Execute a query
			      log.info("Creating statement...");
			      stmt = conn.createStatement();
			      String sql;			   
			      //sql = "SELECT DISTINCT FCT.FUNDNAME,FCT.FUNDOWNERID,FCT.POLICYNO,FCT.CLAIM_ID,INT(FCT.CLAIMNO) AS CLAIMNO,RTRIM(FCT.PRODUCTTYPE) PRODUCTTYPE, UCASE(FCT.FIRSTNAME) AS FIRSTNAME,UCASE(FCT.SURNAME) AS SURNAME,FCT.DATEOFBIRTH,FCT.GENDER,FCT.REFERENCE,FCT.SCHEME,FCT.WAITPERIOD, FCT.BENEFITPERIOD,FCT.STATUS_CODE, ST.STATUSDESCRIPTION,FCT.DATESUBMITTED,FCT.DISABILITYDATE,FCT.CLAIMTYPE_DES,FCT.SUMINSURED, (FCT.TOTALBENEFITPAID_GROSS-COALESCE(TOTALBENEFITPAID_DEDUCTIONS,0)) AS BENEFITPAID,FCT.DATELASTPAID, FCT.CLAIMCAUSE_DES, '' as ADMITTED_DATE,FCT.SALARYINSURED,FCT.SUPERCONTRIBUTION,FCT.MONTHLYBENEFIT,FCT.CLAIMASSESSOR, FCT.LASTUPDDATE FROM DBUSDWW.FCT_CLM_DTL FCT JOIN DBUSDWW.TBLCLAIMSTATUS ST ON  FCT.STATUS_CODE = ST.CLAIMSTATUS AND FCT.PRODUCTTYPE = ST.PRODUCTTYPE where FCT.CLAIMNO = '"+claimNo+"'";
			      sql="SELECT DISTINCT FCT.FUNDNAME,FCT.FUNDOWNERID,FCT.POLICYNO,FCT.CLAIM_ID,INT(FCT.CLAIMNO) AS CLAIMNO,RTRIM(FCT.PRODUCTTYPE) PRODUCTTYPE, UCASE(FCT.FIRSTNAME) AS FIRSTNAME,UCASE(FCT.SURNAME) AS SURNAME,FCT.DATEOFBIRTH,FCT.GENDER,FCT.REFERENCE,FCT.SCHEME,FCT.WAITPERIOD, FCT.BENEFITPERIOD,FCT.STATUS_CODE, ST.STATUSDESCRIPTION,FCT.DATESUBMITTED,FCT.DISABILITYDATE,FCT.CLAIMTYPE_DES,FCT.SUMINSURED, (FCT.TOTALBENEFITPAID_GROSS-COALESCE(TOTALBENEFITPAID_DEDUCTIONS,0)) AS BENEFITPAID,FCT.DATELASTPAID, FCT.CLAIMCAUSE_DES, '' as ADMITTED_DATE,FCT.SALARYINSURED,FCT.SUPERCONTRIBUTION,FCT.MONTHLYBENEFIT,FCT.CLAIMASSESSOR, FCT.LASTUPDDATE FROM DBUSDWW.FCT_CLM_DTL FCT JOIN DBUSDWW.TBLCLAIMSTATUS ST ON  FCT.STATUS_CODE = ST.CLAIMSTATUS AND FCT.PRODUCTTYPE = ST.PRODUCTTYPE where FCT.CLAIMNO = '"+42966+"'"+" AND FCT.SURNAME = '"+surName+"' AND DATEOFBIRTH='"+dob+"'";
			      ResultSet rs = stmt.executeQuery(sql);

			      //STEP 5: Extract data from result set
			      while(rs.next()){
			    	  claimOutput = new ClaimOutput();
			    	  claimOutput.setFundname( rs.getString("FUNDNAME"));	
			    	  claimOutput.setFundownerId(rs.getString("FUNDOWNERID"));			    	  
			    	  claimOutput.setPolicyNo(rs.getString("POLICYNO"));			    	 
			    	  claimOutput.setClaimId(rs.getString("CLAIM_ID"));			    	 
			    	  claimOutput.setClaimNo( rs.getString("CLAIMNO"));			    	
			    	  claimOutput.setProductType(rs.getString("PRODUCTTYPE"));			    	  
			    	  claimOutput.setFirstName(rs.getString("FIRSTNAME"));			    	
			    	  claimOutput.setSurName( rs.getString("SURNAME"));			    	 
			    	  claimOutput.setDob(rs.getDate("DATEOFBIRTH"));			    	 
			    	  claimOutput.setGender( rs.getString("GENDER"));			    	
			    	  claimOutput.setReference( rs.getString("REFERENCE"));			    	  
			    	  claimOutput.setScheme(rs.getString("SCHEME"));			    	  
			    	  claimOutput.setWaitperiod(rs.getString("WAITPERIOD"));			    	 
			    	  claimOutput.setBenefitperiod(rs.getString("BENEFITPERIOD"));			    
			    	  claimOutput.setStatusCode( rs.getString("STATUS_CODE"));			    	 
			    	  claimOutput.setStatusDescription(rs.getString("STATUSDESCRIPTION"));			    	
			    	  claimOutput.setDatesubmitted(rs.getDate("DATESUBMITTED"));			    	
			    	  claimOutput.setDisabilitydate(rs.getDate("DISABILITYDATE"));			    	
			    	  claimOutput.setClaimTypeDescription(rs.getString("CLAIMTYPE_DES"));			    	 
			    	  claimOutput.setSuminsured( rs.getString("SUMINSURED"));			    	
			    	  claimOutput.setTotalBenPaid( rs.getString("BENEFITPAID"));			    	
			    	  claimOutput.setDateLastPaid( rs.getDate("DATELASTPAID"));			    	 
			    	  claimOutput.setClaimCauseDescription(rs.getString("CLAIMCAUSE_DES"));			    	  
			    	  claimOutput.setAdmittedDate(rs.getString("ADMITTED_DATE"));			    	 
			    	  claimOutput.setSalaryInsured( rs.getDouble("SALARYINSURED"));			    	  
			    	  claimOutput.setSuperContribution(rs.getDouble("SUPERCONTRIBUTION"));			    	  
			    	  claimOutput.setMonthlyBenefit(rs.getDouble("MONTHLYBENEFIT"));
			    	  claimOutput.setClaimAccessor(rs.getString("CLAIMASSESSOR"));			    	 			    	
			    	  claimOutput.setLastupdateDate(rs.getDate("LASTUPDDATE"));
			    	  claimOutputList.add(claimOutput);
			      		        
			      }
			      //STEP 6: Clean-up environment
			     log.info("size>> {}",claimOutputList.size());
			      rs.close();
			      stmt.close();
			      conn.close();
		} catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      Handle errors for Class.forName
	          log.error("Error in get claim status: {} ",e.getMessage());
		   }finally{
		      finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		    	  log.error("Error in sql: {} ",se.getMessage());
		      }end finally try
		   }
		   log.info("Get claim status finish");
		   return claimOutputList;
	}
	
*/
	

}
