package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class MTAA_UpdateYourOccupationPageObjects extends PageObjectLibrary{
		
	public static final By UpdateYourOccupationLink = getObjectIdentifaction("MTAA.UpdateOccupation.Link.xpath");
	public static final By	Email	=	getObjectIdentifaction("MTAA.UpdateOccupation.Email.id");
	public static final By	ContactTypeClick	=	getObjectIdentifaction("MTAA.UpdateOccupation.ContactTypeClick.xpath");
	public static final By	ContactNumber	=	getObjectIdentifaction("MTAA.UpdateOccupation.ContactNumber.id");
	public static final By	MorningContact	=	getObjectIdentifaction("MTAA.UpdateOccupation.MorningContact.id");
	public static final By	AfterNoonContact	=	getObjectIdentifaction("MTAA.UpdateOccupation.AfterNoonContact.id");
	public static final By	ContactDetailsContinue	=	getObjectIdentifaction("MTAA.UpdateOccupation.ContactDetailsContinue.xpath");
	public static final By	FifteenHoursYes	=	getObjectIdentifaction("MTAA.UpdateOccupation.FifteenHoursYes.id");
	public static final By	FifteenHoursNo	=	getObjectIdentifaction("MTAA.UpdateOccupation.FifteenHoursNo.id");
	public static final By	CitizenYes	=	getObjectIdentifaction("MTAA.UpdateOccupation.CitizenYes.id");
	public static final By	CitizenNo	=	getObjectIdentifaction("MTAA.UpdateOccupation.CitizenNo.id");
	public static final By	ClickIndustry	=	getObjectIdentifaction("MTAA.UpdateOccupation.ClickIndustry.xpath");
	public static final By	ClickOccupation	=	getObjectIdentifaction("MTAA.UpdateOccupation.ClickOccupation.xpath");
	public static final By	OtherOccupation	=	getObjectIdentifaction("MTAA.UpdateOccupation.OtherOccupation.id");
	public static final By	OfficeEnvironmentYes	=	getObjectIdentifaction("MTAA.UpdateOccupation.OfficeEnvironmentYes.id");
	public static final By	OfficeEnvironmentNo	=	getObjectIdentifaction("MTAA.UpdateOccupation.OfficeEnvironmentNo.id");
	public static final By	TertiaryQualYes	=	getObjectIdentifaction("MTAA.UpdateOccupation.TertiaryQualYes.id");
	public static final By	TertiaryQualNo	=	getObjectIdentifaction("MTAA.UpdateOccupation.TertiaryQualNo.id");
	public static final By	HazardousEncYes	=	getObjectIdentifaction("MTAA.UpdateOccupation.HazardousEncYes.id");
	public static final By	HazardousEncNo	=	getObjectIdentifaction("MTAA.UpdateOccupation.HazardousEncNo.id");
	public static final By	OutsideOfcPercentYes	=	getObjectIdentifaction("MTAA.UpdateOccupation.OutsideOfcPercentYes.id");
	public static final By	OutsideOfcPercentNo	=	getObjectIdentifaction("MTAA.UpdateOccupation.OutsideOfcPercentNo.id");
	public static final By	AnnualSalary	=	getObjectIdentifaction("MTAA.UpdateOccupation.AnnualSalary.id");
	public static final By	LoadSalaryLabel	=	getObjectIdentifaction("MTAA.UpdateOccupation.LoadSalaryLabel.xpath");
	public static final By	ContinueOccupationDetails	=	getObjectIdentifaction("MTAA.UpdateOccupation.ContinueOccupationDetails.xpath");
	public static final By	RestrictedillnessYes=getObjectIdentifaction("MTAA.UpdateOccupation.RestrictedillnessYes.xpath");
	public static final By	RestrictedillnessNo=getObjectIdentifaction("MTAA.UpdateOccupation.RestrictedillnessNo.xpath");
	public static final By	HazardousEnvYes=getObjectIdentifaction("MTAA.UpdateOccupation.HazardousEnvYes.xpath");
	public static final By	HazardousEnvNo=getObjectIdentifaction("MTAA.UpdateOccupation.HazardousEnvNo.xpath");
	public static final By	WorkOutsideOfcYes=getObjectIdentifaction("MTAA.UpdateOccupation.WorkOutsideOfcYes.xpath");
	public static final By	WorkOutsideOfcNo=getObjectIdentifaction("MTAA.UpdateOccupation.WorkOutsideOfcNo.xpath");
	public static final By	ContinueHealthPage=getObjectIdentifaction("MTAA.UpdateOccupation.ContinueHealthPage.xpath");
	public static final By	Declarationcheckbox=getObjectIdentifaction("MTAA.UpdateOccupation.declarationcheckbox.xpath");
	public static final By	DeclarationpageSubmit=getObjectIdentifaction("MTAA.UpdateOccupation.SubmitAftDeclaration.xpath");

	
}
