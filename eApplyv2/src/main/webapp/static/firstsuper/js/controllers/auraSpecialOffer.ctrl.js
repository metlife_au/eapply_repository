/*aura special offer*/
/*aura transfer controller*/    
FirstSuperApp.controller('auraspecialoffer',['$scope','$rootScope', '$location', '$timeout','$window', 'auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService','PersistenceService', 'submitAura','persoanlDetailService','auraResponseService','ngDialog','urlService','saveEapply',
                                     function($scope,$rootScope ,$location, $timeout,$window, auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService,PersistenceService,submitAura,persoanlDetailService,auraResponseService,ngDialog,urlService,saveEapply){
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	  //$location.path(path);
  	};
  	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
  		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
 // added for session expiry
    /*$timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}
  	*/
  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
     
  	$scope.proceedNext = function() {
  		$scope.keepGoing = true;
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 
  			if(!Object.questionComplete){
  				$scope.auraResponseDataList[selectedIndex].error=true;  
  				$scope.keepGoing = false;
  			}else{
  				$scope.auraResponseDataList[selectedIndex].error=false;
  			}
  		});
  		if($scope.keepGoing){
  			$rootScope.$broadcast('disablepointer');
  			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) { 
  				console.log(response.data);
  				PersistenceService.setChangeCoverAuraDetails(response.data);
  				$scope.go('/specialoffersummary/1');
  	  		});
  		}
  	}
  	
	$scope.personalDetails = persoanlDetailService.getMemberDetails();
  	$scope.coverDetails = PersistenceService.getChangeCoverDetails();
	$scope.specialOccDetails = PersistenceService.getChangeCoverOccDetails();
	$scope.deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
	$scope.tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
	$scope.ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
  	
    console.log($scope.coverDetails);
  	
  	auraInputService.setFund('FIRS')
  	auraInputService.setMode('SpecialOffer')
  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ; 
  	auraInputService.setAppnumber($scope.coverDetails.appNum)
  	auraInputService.setDeathAmt($scope.deathAddnlDetails.deathFixedAmt)
  	auraInputService.setTpdAmt($scope.tpdAddnlDetails.tpdFixedAmt)
  	auraInputService.setIpAmt($scope.ipAddnlDetails.ipFixedAmt)
  	auraInputService.setWaitingPeriod($scope.ipAddnlDetails.waitingPeriod)
  	auraInputService.setBenefitPeriod($scope.ipAddnlDetails.benefitPeriod)  
  	if($scope.specialOccDetails && $scope.specialOccDetails.gender ){
  		auraInputService.setGender($scope.specialOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
  	auraInputService.setIndustryOcc($scope.specialOccDetails.industryCode+":"+$scope.specialOccDetails.occupation)
  	auraInputService.setClientname('metaus')
  	/*if($scope.specialOccDetails.areyouperCitzNewMemberQuestion=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.specialOccDetails.areyouperCitzNewMemberQuestion)
  	} */ 	
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary($scope.specialOccDetails.salary)
  	auraInputService.setFifteenHr($scope.specialOccDetails.fifteenHr)
  	auraInputService.setName($scope.coverDetails.name) 
	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
  	auraInputService.setExistingTerm(false);
  	if($scope.personalDetails[0].memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	} 	
  	
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
  		$scope.auraResponseDataList = response.data.questions;
  		console.log($scope.auraResponseDataList)
  		angular.forEach($scope.auraResponseDataList, function(Object) {
			$scope.sectionname = Object.questionAlias.substring(3);            			
			 
			});
  	});
  	 var appNum;
     appNum = PersistenceService.getAppNumber();
  	 $scope.saveSpecialCoverAura = function (){
  		$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    		if($scope.coverDetails != null && $scope.specialOccDetails != null && $scope.deathAddnlDetails != null &&  $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.personalDetails[0] != null){
      		$scope.coverDetails.lastSavedOn = 'SpecialCoverAuraPage';
      		// $scope.auraDetails
      		$scope.details={};
			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
			$scope.details.occupationDetails = $scope.specialOccDetails;
      		var temp = angular.extend($scope.details,$scope.coverDetails);
          	var saveSpecialCoverAuraObject = angular.extend(temp, $scope.personalDetails[0]);
          	auraResponseService.setResponse(saveSpecialCoverAuraObject)
          	$rootScope.$broadcast('disablepointer');
  	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {  
  	                console.log(response.data)
  	        });
      	}
     };
    
     $scope.saveAndExitPopUp = function (hhText) {
       	
 		var dialog1 = ngDialog.open({
 			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
 				className: 'ngdialog-theme-plain custom-width',
 				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
 				plain: true
 		});
 		dialog1.closePromise.then(function (data) {
 			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
 		});
 	};
 	
 	 $scope.updateRadio = function (answerValue, questionObj){  		
    		
    		questionObj.arrAns[0]=answerValue;
    		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
    		  auraResponseService.setResponse($scope.auraRes)  		  
    		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {  			 
    			  $scope.selectedIndex = $scope.auraResponseDataList.indexOf(questionObj)
    			  $scope.auraResponseDataList[$scope.selectedIndex]=response.data
    	  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 	  			
    	  			if($scope.selectedIndex>selectedIndex && !Object.questionComplete){
    	  				//branch complete false for previous questions  	  				
    	  				$scope.auraResponseDataList[selectedIndex].error=true;  	  				
    	  			}else if(Object.questionComplete){
    	  				$scope.auraResponseDataList[selectedIndex].error=false;
    	  			}
    	  			
    	  		});
    			  
        	}, function () {
        		//console.log('failed');
        	});
    	 };
  	 
  	
  	
}]);


/////