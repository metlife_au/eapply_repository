package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyNewBusinessOrderProcess")

@XmlRootElement (name="PolicyNewBusinessOrderProcess")
public class PolicyNewBusinessOrderProcess {
	
	private MessageHeader MessageHeader;
	private PolicyOrder PolicyOrder;
	
	public MessageHeader getMessageHeader() {
		return MessageHeader;
	}
	public void setMessageHeader(MessageHeader messageHeader) {
		MessageHeader = messageHeader;
	}
	public PolicyOrder getPolicyOrder() {
		return PolicyOrder;
	}
	public void setPolicyOrder(PolicyOrder policyOrder) {
		PolicyOrder = policyOrder;
	}
	
	
	
}
